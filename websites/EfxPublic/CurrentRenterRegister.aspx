﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CurrentRenterRegister.aspx.cs" Inherits="EfxPublic.CurrentRenterRegister" %>

<%@ Register Src="~/UserControls/TopNavMenu2.ascx" TagPrefix="uc1" TagName="TopNavMenu2" %>
<%@ Register Src="~/UserControls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Domus - Premier Multifamily Financial Services</title>
    <link id="PublicFavicon" rel="icon" type="image/x-icon" href="/DomusIcon.ico" />
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/agency.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <uc1:TopNavMenu2 runat="server" ID="TopNavMenu2" />


        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <header id="Residents" class="text-center tall text-center" style="background-position: center; color: #FFFFFF; background-image: url(/img/RegisterHeader2.png); background-repeat: no-repeat;">
                    <div class="container text-center">

                        <div class="row">

                            <div class="intro-text">
                                <div class="intro-lead-in text-shadow">If you need to Register you have come to the right place!</div>

                            </div>
                        </div>

                    </div>
                </header>


                <div class="clearfix"></div>

                <!-- Services Section -->
                <section id="RegisterHelp">
                    <div class="container text-center">

                        <h3>Current Renter Registration</h3>
                        <p>Registration is quick and simple.  Follow the instructions below.</p>


                        <div class="col-md-4">
                            <span class="fa-stack fa-3x">
                                <i class="fa fa-circle-o fa-stack-2x text-primary"></i>
                                <strong class="fa-stack-1x">1</strong>
                            </span>
                            <h4 class="service-heading">Find Your Property</h4>
                            <p class="text-muted">Search for your property by name and zip code then select it</p>
                        </div>

                        <div class="col-md-4">
                            <span class="fa-stack fa-3x">
                                <i class="fa fa-circle-o fa-stack-2x text-primary"></i>
                                <strong class="fa-stack-1x">2</strong>
                            </span>
                            <h4 class="service-heading">Fill Out Online Form</h4>
                            <p class="text-muted">Tell us who you are and create a password</p>
                        </div>

                        <div class="col-md-4">
                            <span class="fa-stack fa-3x">
                                <i class="fa fa-circle-o fa-stack-2x text-primary"></i>
                                <strong class="fa-stack-1x">3</strong>
                            </span>
                            <h4 class="service-heading">Check your email</h4>
                            <p class="text-muted">Once submited your property manager will review and grant access</p>
                        </div>

                    </div>
                </section>



                <section>

                    <!--Grid Section -->
                    <div class="container">

                        <div class="col-lg-10 center">


                            <asp:MultiView ID="MultiView1" runat="server" ViewStateMode="Enabled" ActiveViewIndex="0">

                                <asp:View ID="SearchPropertyView" runat="server">
                                    <div class="col-lg-12">
                                        <div class="col-lg-10 center">
                                            <p>Search for your property by name.  Enter 4 or more characters for property name and 5 digits for zip code.</p>
                                            <div class="row form-group">
                                                <div class="col-lg-6">
                                                    <label>
                                                        Property Name
                                                    </label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="PropertyNametxt" ErrorMessage="!" ValidationGroup="FindProperty"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="!" ValidationGroup="FindProperty" OnServerValidate="CustomValidator1_ServerValidate"><i class="fa fa-warning validationStyle"></i></asp:CustomValidator>
                                                    <asp:TextBox ID="PropertyNametxt" runat="server" CssClass="form-control WideBox" ValidationGroup="FindProperty"></asp:TextBox>

                                                </div>

                                                <div class="col-lg-6">
                                                    <label>
                                                        Zip Code
                                                    </label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ZipCodetxt" ErrorMessage="!" ValidationGroup="FindProperty"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="!" ValidationGroup="FindProperty"><i class="fa fa-warning validationStyle"></i></asp:CustomValidator>
                                                    <asp:TextBox ID="ZipCodetxt" runat="server" CssClass="form-control WideBox" MaxLength="5" ValidationGroup="FindProperty"></asp:TextBox>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label></label>
                                                <div class="col-lg-4">
                                                    <asp:Button ID="FindPropertybtn" runat="server" CssClass="btn btn-primary" Text="Find" ValidationGroup="FindProperty" OnClick="FindPropertybtn_Click" />
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </asp:View>


                                <asp:View ID="ResultPropertyView" runat="server">

                                    <h3>
                                        <asp:Label ID="PropertyHeaderlbl" runat="server"></asp:Label>
                                    </h3>

                                    <asp:GridView ID="GridView1" runat="server" CssClass="table table-responsive" DataSourceID="FIndPropertyListSql" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="PropertyId" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="True" />
                                            <asp:BoundField DataField="PropertyId" HeaderText="PropertyId" InsertVisible="False" ReadOnly="True" SortExpression="PropertyId" Visible="False" />
                                            <asp:BoundField DataField="PropertyName" HeaderText="PropertyName" SortExpression="PropertyName" />
                                            <asp:BoundField DataField="PropertyName2" HeaderText="PropertyName2" SortExpression="PropertyName2" Visible="False" />
                                            <asp:BoundField DataField="CityState" HeaderText="CityState" ReadOnly="True" SortExpression="CityState" />
                                            <asp:BoundField DataField="PostalCode" HeaderText="PostalCode" SortExpression="PostalCode" />
                                        </Columns>
                                        <SelectedRowStyle BackColor="#BADC02" />
                                    </asp:GridView>
                                    <asp:Button ID="SearchAgainbtn" runat="server" CssClass="btn btn-primary" Text="Search Again" OnClick="SearchAgainbtn_Click" />
                                    <asp:SqlDataSource ID="FIndPropertyListSql" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="usp_Property_GetListForRegistration" SelectCommandType="StoredProcedure">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="ZipCodetxt" Name="Zip" PropertyName="Text" Type="String" />
                                            <asp:ControlParameter ControlID="PropertyNametxt" Name="PropName" PropertyName="Text" Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>

                                </asp:View>

                                <asp:View ID="CompleteRegisterView" runat="server">

                                    <div class="col-lg-12">
                                        <h3>
                                            <asp:Label ID="CompleteRegHeaderlbl" runat="server" Text="Label"></asp:Label>
                                        </h3>


                                        <div class="row form-group">
                                            <div class="col-lg-6">
                                                <label>
                                                    First Name
                                                </label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="!" ControlToValidate="FirstNametxt"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="FirstNametxt" runat="server" CssClass="form-control WideBox"></asp:TextBox>
                                            </div>
                                            <div class="col-lg-6">
                                                <label>
                                                    Last Name
                                                </label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="!" ControlToValidate="LastNametxt"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="LastNametxt" runat="server" CssClass="form-control WideBox"></asp:TextBox>
                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col-lg-3">
                                                <label>
                                                    Unit
                                                </label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="!" ControlToValidate="unittxt"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="unittxt" runat="server" CssClass="form-control WideBox"></asp:TextBox>

                                            </div>
                                            <div id="IntegratedSearch" runat="server" class="col-lg-6">
                                                <label>
                                                    Resident ID
                                                </label>
                                                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" CausesValidation="False">

                                        <i class="fa fa-search" title="Search for Your Resident ID"></i>
                                                
                                                </asp:LinkButton>
                                                <asp:CustomValidator ID="CustomValidator3" runat="server" ErrorMessage="!"><i class="fa fa-warning validationStyle"></i></asp:CustomValidator>
                                                <asp:TextBox ID="RPOidtxt" runat="server" CssClass="form-control WideBox"></asp:TextBox>
                                            </div>

                                        </div>

                                        <div class="row form-group">
                                            <div class="col-lg-6">
                                                <label>
                                                    Address
                                                </label>
                                                <asp:TextBox ID="Addresstxt" CssClass="form-control WideBox" runat="server"></asp:TextBox>
                                            </div>

                                            <div class="col-lg-">


                                            </div>

                                        </div>

                                        <div class="row form-group">
                                            <div class="col-lg-6">
                                                <label>
                                                    City
                                                </label>
                                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="!" ControlToValidate="citytxt"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="citytxt" runat="server" CssClass="form-control WideBox"></asp:TextBox>
                                            </div>
                                            <div class="col-lg-6">
                                                <label>
                                                    State
                                                </label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="!" ControlToValidate="StateDDL" InitialValue="0"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="StateDDL" runat="server" CssClass="form-control WideBox" AppendDataBoundItems="True"></asp:DropDownList>
                                            </div>
                                        </div>


                                        <div class="row form-group">

                                            <div class="col-lg-6">
                                                <label>
                                                   Home Phone
                                                </label>
                                                <asp:TextBox ID="PhoneNumbertxt" runat="server" CssClass="form-control WideBox"></asp:TextBox>
                                            </div>

                                            <div class="col-lg-6">
                                                <label>Email (UserName)</label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="!" ControlToValidate="Emailtxt"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="!" ControlToValidate="Emailtxt" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"><i class="fa fa-warning validationStyle"></i></asp:RegularExpressionValidator>
                                                <asp:TextBox ID="Emailtxt" runat="server" CssClass="form-control WideBox"></asp:TextBox>
                                            </div>

                                        </div>

                                        <div class="row form-group">
                                             <div class="col-lg-6">
                                                 <label>Password</label>
                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="!" ControlToValidate="pwtxt"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                                 <asp:TextBox ID="pwtxt" runat="server" CssClass="form-control WideBox" TextMode="Password"></asp:TextBox>
                                                 </div>

                                             <div class="col-lg-6">
                                                  <label>Confirm Password</label>
                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="!" ControlToValidate="confpwtxt"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                                 <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="!" ControlToValidate="confpwtxt" ControlToCompare="pwtxt"><i class="fa fa-warning validationStyle"></i></asp:CompareValidator>
                                                 <asp:TextBox ID="confpwtxt" runat="server" CssClass="form-control WideBox" TextMode="Password"></asp:TextBox>
                                                 </div>
                                            </div>

                                        <div class="row form-group">
                                            <asp:Button ID="ComplRegbtn" CssClass="btn btn-primary" runat="server" Text="Complete Registration" OnClick="ComplRegbtn_Click"/>
                                            &nbsp;
                                            <asp:Button ID="CanelRegbtn" CssClass="btn btn-primary" runat="server" Text="Cancel" OnClick="CanelRegbtn_Click" CausesValidation="False" />
                                            </div>
                                        <div class="row form-group col-lg-12">
                                            <asp:Label ID="EmailChecklbl" runat="server" Font-Bold="True"></asp:Label>

                                        </div>


                                    </div>



                                </asp:View>
                                <asp:View ID="integratedlookupView" runat="server">
                                    <div class="col-lg-12">


                                        <h3>Search for Your RPO ID
                                        </h3>
                                        <p>
                                            Please provide your First Name, Last Name and confirm the rent amount
                                        </p>
                                        <div class="form-group">
                                            <div class="col-lg-4">
                                                <label>
                                                    First Name
                                                </label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="!" ControlToValidate="FirstNameSearchtxt"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="FirstNameSearchtxt" runat="server" CssClass="form-control WideBox"></asp:TextBox>

                                            </div>
                                            <div class="col-lg-4">
                                                <label>
                                                    Last Name
                                                </label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="!" ControlToValidate="LastNameSearchtxt"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="LastNameSearchtxt" runat="server" CssClass="form-control WideBox"></asp:TextBox>

                                            </div>
                                            <div class="col-lg-4">
                                                <label>
                                                    Rent Amount
                                                </label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="!" ControlToValidate="RentAmountSearchtxt"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="CustomValidator4" runat="server" ErrorMessage="!" OnServerValidate="CustomValidator4_ServerValidate"><i class="fa fa-warning validationStyle"></i></asp:CustomValidator>
                                                <asp:TextBox ID="RentAmountSearchtxt" runat="server" CssClass="form-control WideBox"></asp:TextBox>

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-4">
                                                <asp:Button ID="Button1" runat="server" Text="Search" CssClass="btn btn-primary" OnClick="Button1_Click" />
                                                &nbsp;
                                                <asp:Button ID="Button2" runat="server" Text="Cancel" CssClass="btn btn-primary" OnClick="Button2_Click" CausesValidation="False" />
                                            </div>
                                        </div>

                                        <div class="row form-group col-lg-12">
                                               <asp:Label ID="Resultlbl" runat="server"></asp:Label>
                                        </div>

                                        <div class="col-lg-8 center">
                                         
                                            <asp:GridView ID="RPOIDSearchResultGrid" CssClass="table table-responsive" runat="server" DataKeyNames="PmsID" AutoGenerateColumns="False" OnSelectedIndexChanged="RPOIDSearchResultGrid_SelectedIndexChanged">
                                                <Columns>
                                                    <asp:BoundField DataField="Propertyid" HeaderText="Propertyid" Visible="False" />
                                                    <asp:BoundField DataField="PropertyName" HeaderText="PropertyName" />
                                                    <asp:TemplateField HeaderText="FirstName">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                                                            &nbsp;<asp:Label ID="Label2" runat="server" Text='<%# Eval("LastName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="LastName" HeaderText="LastName" Visible="False" />
                                                    <asp:BoundField DataField="RentAmount" DataFormatString="{0:c}" HeaderText="RentAmount" />
                                                    <asp:BoundField DataField="PmsID" HeaderText="RPO ID" />
                                                    <asp:CommandField ShowSelectButton="True" />
                                                </Columns>
                                                <SelectedRowStyle BackColor="#BADC02" />
                                            </asp:GridView>


                                        </div>
                                        





                                    </div>
                                </asp:View>

                            </asp:MultiView>

                        </div>
                    </div>


                </section>



                           </ContentTemplate>
        </asp:UpdatePanel>


                <uc1:Footer runat="server" ID="Footer" />


                <!-- jQuery -->
                <script src="js/jquery.js"></script>

                <!-- Bootstrap Core JavaScript -->
                <script src="js/bootstrap.min.js"></script>

                <!-- Plugin JavaScript -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
                <script src="js/classie.js"></script>
                <script src="js/cbpAnimatedHeader.js"></script>

                <!-- Contact Form JavaScript -->
                <script src="js/jqBootstrapValidation.js"></script>
                <script src="js/contact_me.js"></script>

                <!-- Custom Theme JavaScript -->
                <script src="js/agency.js"></script>



 

    </form>


</body>

</html>
