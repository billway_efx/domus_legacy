﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrivacyPolicy.aspx.cs" Inherits="EfxPublic.PrivacyPolicy" %>


<%@ Register Src="~/UserControls/TopNavMenu2.ascx" TagPrefix="uc1" TagName="TopNavMenu2" %>
<%@ Register Src="~/UserControls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Domus - Premier Multifamily Financial Services</title>
      <link id="PublicFavicon" rel="icon" type="image/x-icon" href="/DomusIcon.ico" />  
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/agency.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">
    <form id="form1" runat="server">

    <uc1:TopNavMenu2 runat="server" id="TopNavMenu2" />

        <div class="clearfix"></div>

               <section style="background-color:#badc02; min-height:60px;">


        </section>

    <!-- Services Section -->
    <section id="PrivacyPolicy">
        <div class="container">


                <div id="pageCenterContainer" class="ppPage pageContainer residentsPage">
        <div class="pageTitle frPageTitle fr1Color" style="border-color: white;"><h2>Privacy Policy</h2></div>
        <div class="sectionEighty legalText frBlack">
            <article class="fullrow">
                <p class="frBlack">Domus a company of EFX Financial Services Inc. respects the privacy of every individual who visits our web site. In general, 
                    you can visit Domus without telling us who you are or revealing any information about yourself. Domus will not collect 
                    any personal information about you by means of this site unless you have provided such personal data to us. If you do not want your 
                    personal data collected, please don’t submit it. When you engage in certain activities on Domus.com, such as requesting information, 
                    electronic payments, and marketing we will ask you to provide certain information about yourself by filling out and submitting an online form. 
                    It is completely optional for you to engage in these activities. If you elect to engage in these activities, however, Domus will 
                    require that you provide your name, mailing address, e-mail address, and other personal information. Domus also uses information that 
                    you provide as part of our effort to keep you informed about product upgrades, special offers, and other Domus services. 
                    Domus does not share any information with third-party marketers. The information we collect is for Domus internal use only.</p>
            </article>
            
        </div>
    </div>




        </div>
    </section>

 

<uc1:Footer runat="server" id="Footer" />


    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/agency.js"></script>


            </form>


</body>

</html>
