﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PropertyManagers.aspx.cs" Inherits="EfxPublic.PropertyManagers" %>


<%@ Register Src="~/UserControls/TopNavMenu2.ascx" TagPrefix="uc1" TagName="TopNavMenu2" %>
<%@ Register Src="~/UserControls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>
<%@ Register Src="~/UserControls/PublicContactForm.ascx" TagPrefix="uc1" TagName="PublicContactForm" %>




<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Property Managers - Domus</title>
          <link id="PublicFavicon" rel="icon" type="image/x-icon" href="/DomusIcon.ico" />  
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/agency.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <form id="form1" runat="server">

        <uc1:TopNavMenu2 runat="server" id="TopNavMenu2" />

                        <header id="Residents" class="text-center tall text-center" style="background-position: center; background-color: #662E6B; color: #FFFFFF; background-image: url(/img/Flux/header_managers_web.jpg); background-repeat: no-repeat;">
            <div class="container text-center">
     
            <div class="row">


            
            <div class="intro-text">
                
                <div class="row center intro-lead-in text-shadow text-Green col-lg-6">Stay Focused on What Matters, Keeping Residents Happy.</div>
                
              
            </div>
                </div>
       


            </div>


        </header>

        <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">spend time with your residents, not bank deposit slips.</h2>
                    <h3 class="section-subheading text-muted">
                       DOMUS lets you and your staff be friendly property
                        representatives instead of chasing down rent
                        payments. Providing convenience for residents gives
                        them another reason to extend their leases and
                        empowers them to manage their own experience.

                    </h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-money fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Make Residents Happy</h4>
                    <p class="text-muted">Allow tenants to pay anyway they
                                            choose; AutoPay, Mobile Apps for iOS
                                            and Android, PayByText, or personto-
                                            person with our toll-free bilingual
                                            customer service.
                                            </p>
                </div>

                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-bar-chart-o fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Stay Focused on Signing New Leases</h4>
                    <p class="text-muted">Spend your time sourcing prospective
                        tenants instead of managing the monthly
                        rent cycle.</p>
                </div>

                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Give Residents More Flexibility</h4>
                    <p class="text-muted">DOMUS supports all payment methods
                            residents want including credit/debit
                            cards, E-check and cash payments
                            through RentByCash.</p>
                </div>

                 <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-plug fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Enable Safe and Secure Cash Transactions</h4>
                    <p class="text-muted">RentByCash keeps cash and money order payments out of your office.</p>
                </div>
            </div>

                       <div class="row text-center">
                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-money fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Feel Secure in Your Transactions</h4>
                    <p class="text-muted">DOMUS is backed by EFX, a leading financial services company with broad experience in the payments industry, specifically ACH, credit/debit card and ATM processing.</p>
                </div>

                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-bar-chart-o fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Reduce Tenant Turnover</h4>
                    <p class="text-muted">Free up time to focus on customer service and the overall resident experience.</p>
                </div>

                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Maximize the Advantages</h4>
                    <p class="text-muted">DOMUS provides a full range of financial and adoption support resources.</p>
                </div>

                 <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-plug fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Allow Tenants to Self-Manage</h4>
                    <p class="text-muted">Now residents can create and manage
                            their own online rent profiles and use
                            them to pay their rent online and on time
                            every month.</p>
                </div>
            </div>



        </div>
    </section>

        <section id="infographic" style="background-color: #662e6b">
            <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <img src="img/infographic1.jpg" class="img-responsive img-centered" style="max-height: 500px" />
                </div>

                </div>
                </div>

        </section>

        <section class="text-center">
            <div class="container">
                <h2 class="section-heading">Why Everyone loves Domus!</h2>
                <div class="row text-center">
                    <h3 class="text-Green">
                       “Anything that saves us both time and money is great for us - and DOMUS does exactly that.”
                    </h3>
                </div>
                <div class="row text-center">

                
                <div class="col-lg-4">
                    <h3 class="section-subheading text-muted">
                       “The customer service our residents have received from DOMUS has been excellent. Our residents hardly ever have to come to the office with questions about paying their rent!”
                    </h3>
                </div>

                <div class="col-lg-4">
                    <h3 class="section-subheading text-muted">
                       “I love DOMUS. It integrates with our property management software, removing any need to post any on-site payments. It saves both myself and the residents time. They don’t have to come into the office to pay their rent and I don’t have to deposit the checks at the bank.”
                    </h3>
                </div>
                    <div class="col-lg-4">
                        <h3 class="section-subheading text-muted">
                            “DOMUS has let us provide our tenants with a more flexible way to pay their rent that fits in with their busy lives.”
                        </h3>
                        <a href="#contact" class="page-scroll btn btn-xl">Contact Us</a>
                </div>


                    </div>
            </div>
        </section>



        <uc1:PublicContactForm runat="server" id="PublicContactForm" />


        <uc1:Footer runat="server" id="Footer" />

            <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/agency.js"></script>

    </form>
</body>
</html>
