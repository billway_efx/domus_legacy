﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using EfxFramework;

namespace EfxPublic
{
    public partial class ResidentLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                bool AuthenticatedRenter = Page.User.Identity.IsAuthenticated;
                HttpCookie _Session = Request.Cookies["RenterSession"];
                HttpCookie _Renter = Request.Cookies["RenterID"];
                if (AuthenticatedRenter == true && _Session != null)
                {
                    Response.Redirect("~/SecurePages/Account.aspx", false);
                }
            }
        }


        protected void Loginbtn_Click(object sender, EventArgs e)
        {
            string un = untxt.Text;
            string pw = pwtxt.Text;

            string _auth = DataAccess.Login.Auth(un, pw);

            ErrorLbl.Text = _auth;

            byte[] byteArray = Encoding.ASCII.GetBytes(_auth);

            using (var OutputStream = new MemoryStream(byteArray))
            {
                string LoginError = "";
                string _Result = "";
                string RenterID = "";
                string Message = "";
                string _sessionID = "";

                try
                {
                    var XmlResp = XDocument.Load(OutputStream);
                    var RespElement = DataAccess.Login.GetRootElement(XmlResp.Root, "AuthenticationResponse");

                    foreach (XElement AuthResponse in RespElement.Elements())
                    {
                        string _node = AuthResponse.Name.LocalName;

                        ErrorLbl.Text = _node.ToString();

                        if (_node == "AuthenticatedRenter")
                        {
                            foreach (XElement AuthRenter in AuthResponse.Elements())
                            {
                                string _node2 = AuthRenter.Name.LocalName;

                                if (_node2 == "RenterId")
                                {
                                    RenterID = AuthRenter.Value.ToString();
                                }
                            }

                        }

                        if (_node == "Message")
                        {
                            ErrorLbl.Text = Message;
                            
                            Message = AuthResponse.Value.ToString();
                            if (Message == "The user failed to login.")
                            {
                                LoginError = "User Name and or Password is Incorrect";
                                ErrorLbl.Text = LoginError;
                                return;
                            }
                        }

                        if (_node == "Result")
                        {
                            _Result = AuthResponse.Value.ToString();

                        }

                        if (_node == "SessionId")
                        {
                            _sessionID = AuthResponse.Value.ToString();

                            Session["RenterID"] = RenterID;
                            //FormsAuthentication.SetAuthCookie(un, true);
                            Session["RenterSession"] = _sessionID;

                            //Salcedo - testing new method of storing authentication ticket - 6/25/2016
                            // instantiate new ticket 
                            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(un, false, 10);
                            // encrypt ticket
                            string encTicket = FormsAuthentication.Encrypt(ticket);
                            // write ticket to cookie collection
                            Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));
                            //Salcedo - end test code

                            //Set Cookie for Renter Session
                            HttpCookie DomusUser = new HttpCookie("RenterSession");
                            HttpCookie CurentRenterID = new HttpCookie("RenterID");

                            DomusUser.Expires = DateTime.Now.AddMinutes(10);
                            CurentRenterID.Expires = DateTime.Now.AddMinutes(10);
                            DomusUser.Values.Add("Session", _sessionID);
                            CurentRenterID.Values.Add("ID", RenterID);
                            Response.Cookies.Add(DomusUser);
                            Response.Cookies.Add(CurentRenterID);
                            //Send user to account page
                            Response.Redirect("~/SecurePages/Account.aspx", false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoginError = "Error - System not available: Please Contact Customer Service";
                    ErrorLbl.Text += "\r\n" + LoginError;
                }
            }
        }

        //CMallory - 00650 - Added method.  Event will send an email to the user to reset their password.
        protected void Resetpwbtn_Click(object sender, EventArgs e)
        {
            string URL = Request.Url.GetLeftPart(UriPartial.Authority).ToString();
            DataAccess.Miscellaneous.SendPasswordResetEmail(ForgotPwUserNametxt.Value, URL);
            Response.Redirect("/PasswordEmailSent.aspx");
        }
    }
}