﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxPublic
{
    public partial class Residents : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RenterLoginbtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ResidentLogin.aspx", false);
        }

        protected void RenterRegisterBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CurrentRenterRegister.aspx", false);
        }

        protected void ApplicantRegisterBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FutureResidents.aspx", false);
        }
    }
}