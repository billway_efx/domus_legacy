﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PasswordUpdate.ascx.cs" Inherits="EfxPublic.UserControls.PasswordUpdate" %>

<%-- CMallory - Task 00650 - Added.  User will reset their password using the controls on this screen. --%>
<div>

    <asp:Label ID="lblPassword" runat="server" Text="New Password"></asp:Label><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="New Password is required" ControlToValidate="NewPasswordtxt"  ValidationGroup="pwUpdate" Text=" * Required *" Font-Bold="True" Font-Size="10px" ForeColor="#FF3300"></asp:RequiredFieldValidator>
    <asp:TextBox ID="NewPasswordtxt" runat="server" CssClass="form-control" ValidationGroup="pwUpdate"></asp:TextBox>
    <asp:Label ID="lblPasswordConfirmation" runat="server" Text="Confirm Password"></asp:Label>
    <asp:TextBox ID="ConfirmPasswordtxt" runat="server" CssClass="form-control" ValidationGroup="pwUpdate"></asp:TextBox>
    <asp:Label ID="lblExpiration" runat="server" Text="The time limit to reset your password has expired.  Please try again."></asp:Label>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Need to confirm password" ControlToValidate="ConfirmPasswordtxt"  ValidationGroup="pwUpdate" Text=" * Required *" Font-Bold="True" Font-Size="10px" ForeColor="#FF3300"></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Passwords do not match" ControlToValidate="ConfirmPasswordtxt" ControlToCompare="NewPasswordtxt" ValidationGroup="pwUpdate" Text="Does not match" Font-Bold="True" Font-Size="10px" ForeColor="#FF3300"></asp:CompareValidator>
    

    <br />
    <asp:Button ID="UpdatePasswordbtn" runat="server" Text="Update Password" ValidationGroup="pwUpdate" CausesValidation="true" CssClass="btn btn-default" OnClick="UpdatePasswordbtn_Click" /><br />
            <asp:Label ID="checkPasswordlbl" runat="server"></asp:Label>
</div>
