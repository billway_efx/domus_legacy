﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.Security.Authentication;
using System.Data;
using System.Data.SqlClient;
using RPO;

namespace EfxPublic.UserControls
{
    //CMallory - Task 00650 - Added
    public partial class PasswordUpdate : System.Web.UI.UserControl
    {

        public byte[] PasswordHash { get; set; }
        public byte[] Salt { get; set; }
        private static string RenterID;
        private static string EmailAddress;
        private static string Guid;
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataReader DR;
            DateTime StartTime = new DateTime();

            lblPassword.Visible = false;
            lblPasswordConfirmation.Visible = false;
            NewPasswordtxt.Visible = false;
            ConfirmPasswordtxt.Visible = false;
            UpdatePasswordbtn.Visible = false;
            lblExpiration.Visible = false;

            Guid = HttpContext.Current.Request.QueryString["User"].ToString();
            
            try
            {
                //Get Active password reset record.
                DR = DataAccess.Miscellaneous.GetPasswordReset(Guid);
                while(DR.Read()) 
                {
                    EmailAddress = DR.GetValue(1).ToString(); // On first iteration will be hello2
                    StartTime = (DateTime)DR.GetValue(2); // On first iteration will be hello3
                    RenterID = DR.GetValue(4).ToString();

                }
            }

            catch(Exception ex)
            {
                ActivityLog AL = new ActivityLog();
                AL.WriteLog("EFX Administrator", "Unable to find active GUID for password reset. " + ex.Message, (short)LogPriority.LogAlways);
            }   
            
            double Minutes = Convert.ToDouble(EfxFramework.EfxSettings.ResetPasswordTimeLimit);
            TimeSpan TimeDifference =  DateTime.Now - StartTime; 
            //If the time difference between when the password reset was requested and the current time is less than a specified time in the System Setting table.
            if (TimeDifference.TotalMinutes <= Minutes)
            {
                lblPassword.Visible = true;
                lblPasswordConfirmation.Visible = true;
                NewPasswordtxt.Visible = true;
                ConfirmPasswordtxt.Visible = true;
                UpdatePasswordbtn.Visible = true;
            }

            else
            {
                DataAccess.Miscellaneous.DisablePasswordReset(Guid);
                lblExpiration.Visible = true;
            }
        } 

        
        protected void UpdatePasswordbtn_Click(object sender, EventArgs e)
        {
            string _RenterID = RenterID;
            string UserName = EmailAddress;
            if (_RenterID != null)
            {
                if (NewPasswordtxt.Text == null)
                {
                    checkPasswordlbl.Text = "";
                }
                int renterID = int.Parse(_RenterID);
                string NewPassword = ConfirmPasswordtxt.Text;

                //Generate a new salt and set it
                Salt = AuthenticationTools.GenerateSalt();

                //Hash the new password and set it
                PasswordHash = AuthenticationTools.HashPassword(NewPassword, Salt);

                SetNewPassword(renterID, PasswordHash, Salt);
            }

        }

        //Updates the user's password.
        private void SetNewPassword(int renterID, byte[] pw, byte[] salt)
        {
            try
            {
                //Reset the user's password.
                DataAccess.Miscellaneous.ResetPassword(renterID, pw, salt);
                //Deactivate Password reset record.
                DataAccess.Miscellaneous.DisablePasswordReset(Guid);
                checkPasswordlbl.ForeColor = System.Drawing.Color.ForestGreen;
                checkPasswordlbl.Text = "Password Changed!";
                NewPasswordtxt.Text = string.Empty;
                ConfirmPasswordtxt.Text = string.Empty;

                //Hide the password reset controls once the user reset's their password
                lblPassword.Visible = false;
                lblPasswordConfirmation.Visible = false;
                NewPasswordtxt.Visible = false;
                ConfirmPasswordtxt.Visible = false;
                UpdatePasswordbtn.Visible = false;
            }
            catch
            {
                checkPasswordlbl.ForeColor = System.Drawing.Color.Red;
                checkPasswordlbl.Text = "Error";
            }
        }
    }
}