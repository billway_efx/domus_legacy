﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="EfxPublic.UserControls.Footer" %>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Domusme.com 2016 </span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="https://twitter.com/DomusMe"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="https://www.facebook.com/DomusMe"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="https://www.linkedin.com/company/domus-payment-solutions"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="/PrivacyPolicy.aspx">Privacy Policy</a>
                        </li>
                        <li><a href="/TermsAndConditions.aspx">Terms of Use</a>
                        </li>

                        

                    </ul>
                </div>

                <div class="col-md-4" id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        

            </div>
        </div>
    </footer>