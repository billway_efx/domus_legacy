﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxPublic.UserControls
{
    public partial class Covid : System.Web.UI.UserControl
    {
       public string FirstAnswer
        {
            get
            {
                DropDownList ddl = this.FindControl("ddlFirstQuestion") as DropDownList;
                return ddl.SelectedItem.Text;
            }
        }

       public string SecondAnswer
       {
           get
           {
               DropDownList ddl = this.FindControl("ddlSecondQuestion") as DropDownList;
               return ddl.SelectedItem.Text;
           }
       }

       public string ThirdAnswer
       {
           get
           {
               DropDownList ddl = this.FindControl("ddlThirdQuestion") as DropDownList;
               return ddl.SelectedItem.Text;
           }
       }

       public string FourthAnswer
       {
           get
           {
               DropDownList ddl = this.FindControl("ddlFourthQuestion") as DropDownList;
               return ddl.SelectedItem.Text;
           }
       }

       public string ContactName
       {
           get
           {
               TextBox txt = FindControl("txtContact") as TextBox;
               return txt.Text;
           }
       }

       public string MethodOfContact
       {
           get
           {
               TextBox txt = FindControl("txtMethodOfContact") as TextBox;
               return txt.Text;
           }
       }

       public string Day
       {
           get
           {
               TextBox txt = FindControl("txtDay") as TextBox;
               return txt.Text;
           }
       }

       public string Time
       {
           get
           {
               TextBox txt = FindControl("txtTime") as TextBox;
               return txt.Text;
           }
       }

       public void ResetFields()
       {
          txtContact.Text = String.Empty;
          txtDay.Text = String.Empty;
          txtMethodOfContact.Text = string.Empty;
          txtTime.Text = string.Empty;

          ddlfirstQuestion.SelectedIndex = 0;
          ddlSecondQuestion.SelectedIndex = 0;
          ddlThirdQuestion.SelectedIndex = 0;
          ddlFourthQuestion.SelectedIndex = 0;
       }
    }


}
