﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Covid.ascx.cs" Inherits="EfxPublic.UserControls.Covid" %>

<table style="width: 100%; border: none solid 2px;" align="center">
    <tr>
        <td align="center">
            <p style="font-family: Verdana; font-size: large; font-weight: bold; font-style: normal; font-variant: normal">COVID-19 Visitor Questionnaire</p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="font-family: Verdana; font-size: small; font-style: normal;">
                The safety of our team members, residents, families and visitors is our top priority.  
                As the Coronavirus Disease of 2019 (COVID-19) continues to spread, we will continue 
                to closely monitor the situation and adjust our processes accordingly on recommendations 
                received from the Center for Disease Control (CDC) and World Health Organization (WHO).
            </p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p style="font-family: Verdana; font-size: small; font-style: normal;">
                To prevent the spread and reduce the risk of COVID-19 to our employees and residents, 
                we are administering this questionnaire in advance of entering your home to complete 
                service requests received. 
            </p>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p style="font-family: Verdana; font-size: small; font-style: normal;">
                Thank you for taking the time to fill this out as it contributes to everyone’s health and safety. 
            </p>
        </td>
    </tr>
</table>
<br/>
<table style="width: 100%; border: none solid 2px;" align="center">
    <tr>
        <td width="75%">
            <p style="font-family: Verdana; font-size: small; font-style: normal;">
                1.    Have you or anyone who resides in the apartment returned from any countries within 
                the past 21 days?
            </p>
        </td>
        <td width="25%" style="font-family: Verdana; font-size: small; font-style: normal;">
            <asp:DropDownList runat="server" id="ddlfirstQuestion" AutoPostBack="False">
                <asp:ListItem value=""> Choose Yes/No </asp:ListItem>
                <asp:ListItem value="Yes" Selected="False"> Yes </asp:ListItem>
                <asp:ListItem value="No" Selected="False"> No </asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td width="75%">
            <p style="font-family: Verdana; font-size: small; font-style: normal;">
                2.    Have you or anyone who resides in the apartment been in close contact with people 
                potentially exposed or diagnosed with COVID-19 within the past 14 days?
            </p>
        </td>
        <td width="25%" style="font-family: Verdana; font-size: small; font-style: normal;">
            <asp:DropDownList runat="server" id="ddlSecondQuestion" AutoPostBack="False">
                <asp:ListItem value="">Choose Yes/No</asp:ListItem>
                <asp:ListItem value="Yes" Selected="False">Yes</asp:ListItem>
                <asp:ListItem value="No" Selected="False">No</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td width="75%">
            <p style="font-family: Verdana; font-size: small; font-style: normal;">
                3.    Have you or anyone who resides in the apartment been in close contact with anyone 
                who has traveled to any countries within the past 14 days?  
            </p>
        </td>
        <td width="25%" style="font-family: Verdana; font-size: small; font-style: normal;">
            <asp:DropDownList runat="server" id="ddlThirdQuestion" AutoPostBack="False">
                <asp:ListItem value="">Choose Yes/No</asp:ListItem>
                <asp:ListItem value="Yes" Selected="False">Yes</asp:ListItem>
                <asp:ListItem value="No" Selected="False">No</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td width="75%">
            <p style="font-family: Verdana; font-size: small; font-style: normal;">
                4.    Have you or anyone who resides in the apartment experienced any cold or flu-like 
                symptoms in the last 14 days (fever, cough, sore throat, respiratory illness, or 
                difficulty breathing)?  
            </p>
        </td>
        <td width="25%" style="font-family: Verdana; font-size: small; font-style: normal;">
            <asp:DropDownList runat="server" id="ddlFourthQuestion" AutoPostBack="False">
                <asp:ListItem value="">Choose Yes/No</asp:ListItem>
                <asp:ListItem value="Yes" Selected="False">Yes</asp:ListItem>
                <asp:ListItem value="No" Selected="False">No</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
</table>
<br/>
<table style="width: 100%; border: none solid 2px;" align="center">
    <tr>
        <td>
            <p style="font-family: Verdana; font-size: small; font-style: normal;">
                The office will be scheduling work orders for the Team and will communicate with the resident to confirm a date and 
                time for any work order to be completed.  Please provide contact name and best method of communication preference
                so that we may setup an appointment when the apartment will not be occupied.
            </p>
        </td>
    </tr>
</table>
<br/>
<table style="width: 50%; border: none solid 2px;" align="center">
    <tr style="font:; font-family: Verdana; font-size: small; font-style: normal;">
        <td width="40%" align="right">
            Contact Person:
        </td>
        <td width="60%" align="left">
            <asp:TextBox runat="server" id="txtContact" type="text" style="font-family: verdana; font-size:small; border: black 1px solid;"></asp:TextBox>
        </td>
    </tr>
    <tr style="font-family: Verdana; font-size: small; font-style: normal;">
        <td width="40%" align="right">
            Method of Contact:
        </td>
        <td width="60%" align="left">
            <asp:TextBox runat="server" id="txtMethodOfContact" style="font-family: verdana; font-size: small; border: black 1px solid;"></asp:TextBox>
        </td>
    </tr>
    <tr style="font-family: Verdana; font-size: small; font-style: normal;">
        <td width="40%" align="right">
            Day:
        </td>
        <td width="60%" align="left">
            <asp:TextBox runat="server" id="txtDay" type="text" style="font-family: verdana; font-size: small; border: black 1px solid;"></asp:TextBox>
        </td>
    </tr>
    <tr style="font-family: Verdana; font-size: small; font-style: normal;">
        <td width="40%" align="right">
            Time:
        </td>
        <td width="60%" align="left">
            <asp:TextBox runat="server" id="txtTime" type="text" style="font-family: verdana; font-size: small; border: black 1px solid;"></asp:TextBox>
        </td>
    </tr>
    <tr style="font-family: Verdana; font-size: small; font-style: normal;"><td colspan="2">&nbsp;</td></tr>
    <tr style="font-family: Verdana; font-size: small; font-style: normal;">
        <td colspan="2">
            Your participation is crucial in helping us protect everyone.
        </td>
    </tr>
</table>
