﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecuredSitePasswordUpdate.ascx.cs" Inherits="EfxPublic.UserControls.SecuredSitePasswordUpdate" %>
<div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>


    <asp:Label ID="Label1" runat="server" Text="New Password"></asp:Label><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="New Password is required" ControlToValidate="NewPasswordtxt"  ValidationGroup="pwUpdate" Text=" * Required *" Font-Bold="True" Font-Size="10px" ForeColor="#FF3300"></asp:RequiredFieldValidator>
    <asp:TextBox ID="NewPasswordtxt" runat="server" CssClass="form-control" ValidationGroup="pwUpdate"></asp:TextBox>
    <asp:Label ID="Label2" runat="server" Text="Confirm Password"></asp:Label>
    <asp:TextBox ID="ConfirmPasswordtxt" runat="server" CssClass="form-control" ValidationGroup="pwUpdate"></asp:TextBox>

    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Need to confirm password" ControlToValidate="ConfirmPasswordtxt"  ValidationGroup="pwUpdate" Text=" * Required *" Font-Bold="True" Font-Size="10px" ForeColor="#FF3300"></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Passwords do not match" ControlToValidate="ConfirmPasswordtxt" ControlToCompare="NewPasswordtxt" ValidationGroup="pwUpdate" Text="Does not match" Font-Bold="True" Font-Size="10px" ForeColor="#FF3300"></asp:CompareValidator>
    

    <br />
    <asp:Button ID="UpdatePasswordbtn" runat="server" Text="Update Password" ValidationGroup="pwUpdate" CausesValidation="true" CssClass="btn btn-default" OnClick="UpdatePasswordbtn_Click" /><br />
            <asp:Label ID="checkPasswordlbl" runat="server"></asp:Label>

                    </ContentTemplate>
    </asp:UpdatePanel>
</div>
