﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.Security.Authentication;
using System.Data;
using System.Data.SqlClient;

namespace EfxPublic.UserControls
{
    public partial class SecuredSitePasswordUpdate : System.Web.UI.UserControl
    {
        public byte[] PasswordHash { get; set; }
        public byte[] Salt { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        { 
        }
          
        protected void UpdatePasswordbtn_Click(object sender, EventArgs e)
        {
            string UserName = Page.User.Identity.Name.ToString();
            DataAccess.RenterDetails RD = DataAccess.RenterDataAccess.GetRenterDetailsByUserName(UserName);
            string _RenterID = RD.renterID.ToString();

            if (_RenterID != null)
            {
                if (NewPasswordtxt.Text == null)
                {
                    checkPasswordlbl.Text = "";
                }
                int renterID = int.Parse(_RenterID);
                string NewPassword = ConfirmPasswordtxt.Text;

                //Generate a new salt and set it
                Salt = AuthenticationTools.GenerateSalt();

                //Hash the new password and set it
                PasswordHash = AuthenticationTools.HashPassword(NewPassword, Salt);

                SetNewPassword(renterID, PasswordHash, Salt);
            }

        }


        private void SetNewPassword(int renterID, byte[] pw, byte[] salt)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Renter_SetNewPassword", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@id", SqlDbType.Int).Value = renterID;
            com.Parameters.Add("@pw", SqlDbType.Binary, 64).Value = pw;
            com.Parameters.Add("@salt", SqlDbType.Binary, 64).Value = salt;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                checkPasswordlbl.ForeColor = System.Drawing.Color.ForestGreen;
                checkPasswordlbl.Text = "Password Changed!";
                NewPasswordtxt.Text = string.Empty;
                ConfirmPasswordtxt.Text = string.Empty;
            }
            catch
            {
                //con.Close();
                checkPasswordlbl.ForeColor = System.Drawing.Color.Red;
                checkPasswordlbl.Text = "Error";
            }
            //cakel: BUGID00280
            finally
            {
                con.Close();
            }
        }



        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }
    }
}
