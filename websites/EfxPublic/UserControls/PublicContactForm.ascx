﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PublicContactForm.ascx.cs" Inherits="EfxPublic.UserControls.PublicContactForm" %>


    <!-- Contact Section -->
    <!-- CMallory - Changed controls fom normal html controls to ASP.Net controls and linked the button to a stored procedure that sends an email -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Contact Us</h2>
                    <h3 class="section-subheading text-muted">Find out how DOMUS makes payment processing easier.</h3>
                    <p>Send us a message or call our toll-free bilingual customer service representative.</p>
                    <h4>855-PMY-RENT (855-769-7368)</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!-- <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name."> -->
                                    <label>Your Name</label>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="NameTxt" ErrorMessage="RequiredFieldValidator"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="NameTxt" runat="server" CssClass="form-control" ValidationGroup="contact" ></asp:TextBox>
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <!-- <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address."> -->
                                    <label>Email</label>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="EmailTxt" ErrorMessage="RegularExpressionValidator" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"><i class="fa fa-warning validationStyle"></i></asp:RegularExpressionValidator>
                                    <asp:TextBox ID="EmailTxt" runat="server" CssClass="form-control" ValidationGroup="contact" ></asp:TextBox>
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <!-- <input type="tel" class="form-control" placeholder="Your Phone *" id="phone" required data-validation-required-message="Please enter your phone number."> -->
                                    <label>Phone</label>

                                    <asp:TextBox ID="PhoneTxt" runat="server" CssClass="form-control" ValidationGroup="contact"></asp:TextBox>

                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!-- <textarea class="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea> -->
                                    <label>Message</label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="MessageTxt" runat="server" ErrorMessage="RequiredFieldValidator"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="MessageTxt" runat="server" CssClass="form-control" ValidationGroup="Contact" Rows="10" TextMode="MultiLine"></asp:TextBox>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <asp:Button ID="btnSend" CssClass="btn btn-primary btn-lg" runat="server" OnClick="btnSend_Click" ValidationGroup="contact" Text="Send" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>