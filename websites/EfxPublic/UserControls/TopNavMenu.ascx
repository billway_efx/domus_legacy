﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopNavMenu.ascx.cs" Inherits="EfxPublic.UserControls.TopNavMenu" %>



<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="Default.aspx">
                <img src="../img/RPO-01-01_Logo_Vertical_white.png" style="max-height: 45px;" class="text-shadow" />
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right text-shadow">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>


                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown">Services<span class="caret"></span></a>
                    <ul class="dropdown-menu dropdownColorPurple">
                        <li><a class="page-scroll" href="#services">Our Core Service</a></li>
                        <li><a href="60DaysThatPay.aspx">60 Days that Pay!</a></li>
                    </ul>
                </li>

                <li>
                    <a class="page-scroll " href="#about">About</a>
                </li>
                <li>
                    <a class="page-scroll" href="#contact">Contact</a>
                </li>

                <li class="dropdown">

                    <a class="dropdown-toggle" data-toggle="dropdown">Properties<span class="caret"></span></a>
                    <ul class="dropdown-menu dropdownColorPurple">
                        <li><a href="Executives.aspx">Executives</a></li>
                        <li><a href="Accountants.aspx">Accountants</a></li>
                        <li><a href="PropertyManagers.aspx">Managers</a></li>
                    </ul>
                </li>




                <li>
                    <a href="Residents.aspx">Residents</a>
                </li>

                <li class="dropdown">

                    <a class="dropdown-toggle" data-toggle="dropdown">Login
                  <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdownColorPurple">
                        <li><a href="ResidentLogin.aspx">Residents</a></li>
                        <li><a href="https://localhost:443/account/login.aspx">Managers</a></li>
                    </ul>
                </li>



            </ul>



        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
