﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="60DaysThatPay.aspx.cs" Inherits="EfxPublic._60DaysThatPay" %>



<%@ Register Src="~/UserControls/TopNavMenu2.ascx" TagPrefix="uc1" TagName="TopNavMenu2" %>
<%@ Register Src="~/UserControls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>
<%@ Register Src="~/UserControls/PublicContactForm.ascx" TagPrefix="uc1" TagName="PublicContactForm" %>




<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Property Management Executives - Domus</title>
          <link id="PublicFavicon" rel="icon" type="image/x-icon" href="/DomusIcon.ico" />  
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/agency.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <form id="form1" runat="server">

        <uc1:TopNavMenu2 runat="server" id="TopNavMenu2" />

        <header id="Residents" class="text-center tall text-center" style="background-position: center; background-color: #662E6B; color: #FFFFFF; background-image: url(/img/Flux/60DaysThatPay/header2.jpg); background-repeat: no-repeat;">
            <div class="container text-center">






          
            <div class="row">


      
            <div class="intro-text">
                <div class="col-lg-6 pull-right text-Purple">
                     <h1 style="font-size:70px; font-weight:900; font-style:italic" class="section-heading text-right">60 DAYS<br />
                         THAT PAY
                     </h1>
                </div>
                
            </div>
                </div>



            </div>


        </header>

                <section id="infographic2" style="background-color: #662e6b">
            <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">

                  <h2 class="text-Green">
                      EXPERIENCE THE BENEFITS OF AUTOMATED PAYMENT PROCESSING WITH A DOMUS 60 DAY FREE TRIAL.
                  </h2>
                    <h5 class="text-white">
                        At DOMUS, we want you to experience first hand how automated rent payment processing can give you a return on your investment within 60 days. Our personalized Onsite Activation Program gets qualified properties up and running with DOMUS FREE for 60 days. Best of all, your very own Account Manager will come to you.
                    </h5>
                    <h5 class="text-white">
                        Backed by EFX Financial Services, our tenant payment platform is stable, proven and secure, and our rates are seldom matched.
                    </h5>

                </div>

                </div>
                </div>

        </section>




        <section id="services">
        <div class="container">

            <div class="row text-center">
                <h1 class="text-Purple" style="font-style:italic">With DOMUS the benefits are automatic.</h1>
                <div class="col-md-3 text-center">

                    <img src="img/Flux/60DaysThatPay/RPO_Icon_1.png" class="img-responsive center" style="max-height: 100px;" />

                    <h4 class="service-heading">Fully integrated and certified with MRI, AMSI, Yardi, RealPage and QuickBooks</h4>

                </div>

                <div class="col-md-3">
                    <img src="img/Flux/60DaysThatPay/RPO_Icon_2.png" class="img-responsive center" style="max-height: 100px;" />
                    <h4 class="service-heading">Pre and Partial Payments accepted</h4>

                </div>

                <div class="col-md-3">
                    <img src="img/Flux/60DaysThatPay/RPO_Icon_3.png" class="img-responsive center" style="max-height: 100px;" />
                    <h4 class="service-heading">Custom Line Items Add-ons available</h4>

                </div>

                <div class="col-md-3">
                    <img src="img/Flux/60DaysThatPay/RPO_Icon_4.png" class="img-responsive center" style="max-height: 100px;" />
                    <h4 class="service-heading">Split Payments available</h4>

                </div>
            </div>

                       <div class="row text-center">
                <div class="col-md-3">
                    <img src="img/Flux/60DaysThatPay/RPO_Icon_5.png" class="img-responsive center" style="max-height: 100px;" />
                    <h4 class="service-heading">Pay By Text, Android and IOs</h4>
                    
                </div>

                <div class="col-md-3">
                    <img src="img/Flux/60DaysThatPay/RPO_Icon_6.png" class="img-responsive center" style="max-height: 100px;" />
                    <h4 class="service-heading">Highest PCI Compliance in the industry</h4>

                </div>

                <div class="col-md-3">
                    <img src="img/Flux/60DaysThatPay/RPO_Icon_7.png" class="img-responsive center" style="max-height: 100px;" />
                    <h4 class="service-heading">Consolidated Reporting</h4>

                </div>

                <div class="col-md-3">
                    <img src="img/Flux/60DaysThatPay/RPO_Icon_8.png" class="img-responsive center" style="max-height: 100px;" />
                    <h4 class="service-heading">Adoption Strategy</h4>

                </div>
            </div>



        </div>
    </section>

        <section id="infographic" style="background-color: #662e6b">
            <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <img src="img/infographic1.jpg" class="img-responsive img-centered" style="max-height: 500px" />
                </div>

                </div>
                </div>

        </section>

        <section class="text-center">
            <div class="container">
                <h2 class="section-heading">Discover the DOMUS advantage and treat your tenants to a better way to pay.</h2>
                <div class="row text-center">
                    <h3 class="text-Purple">
                       To find out if you qualify for our 60 DAY FREE TRIAL contact an account representative today.
                    </h3>
                </div>
                <div class="row text-center">

          


                    </div>
            </div>
        </section>



        <uc1:PublicContactForm runat="server" id="PublicContactForm" />


        <uc1:Footer runat="server" id="Footer" />

            <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/agency.js"></script>

    </form>
</body>
</html>