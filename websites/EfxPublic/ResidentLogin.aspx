﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResidentLogin.aspx.cs" Inherits="EfxPublic.ResidentLogin" %>


<%@ Register Src="~/UserControls/TopNavMenu2.ascx" TagPrefix="uc1" TagName="TopNavMenu2" %>
<%@ Register Src="~/UserControls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Domus - Premier Multifamily Financial Services</title>
    <link id="PublicFavicon" rel="icon" type="image/x-icon" href="/DomusIcon.ico" />
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/agency.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">
    <form id="form1" runat="server">

        <uc1:TopNavMenu2 runat="server" ID="TopNavMenu2" />

        <div class="clearfix"></div>


        <section style="background-color: #badc02; min-height: 60px;">
        </section>

        <!-- Login Section -->
        <section id="ResidentLogin">
            <div class="container">
                <div class="col-lg-12">

                    <div class="col-lg-6 center">
                        <h2>Resident Login
                        </h2>
                        <div class="form-group">
                            <label>User Name</label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="untxt" ValidationGroup="logingrp">
                                 <i class="fa fa-warning validationStyle"></i>User Name is Required
                            </asp:RequiredFieldValidator>
                            <asp:TextBox ID="untxt" CssClass="form-control WideBox" runat="server"></asp:TextBox>


                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="pwtxt" ValidationGroup="logingrp" ErrorMessage="RequiredFieldValidator">
                                <i class="fa fa-warning validationStyle"></i>Password is Required
                            </asp:RequiredFieldValidator>
                            <asp:TextBox ID="pwtxt" runat="server" CssClass="form-control WideBox" TextMode="Password"></asp:TextBox>

                        </div>
                        <div class="form-group">
                            <asp:Button ID="Loginbtn" runat="server" Text="Login" CssClass="btn btn-xl" ValidationGroup="logingrp" OnClick="Loginbtn_Click" />
                        </div>
                        <div class="col-lg-12">
                            <asp:Label ID="ErrorLbl" runat="server"></asp:Label>
                        </div>

                    </div>
                </div>

            </div>
        </section>

        <section id="forgotpassword" style="background-color: #662E6B">
            <div class="container">
                <div class="col-lg-12">
                    <div class="col-lg-6 center">
                        <h2 style="color: #ffffff;">Forgot Password?
                        </h2>
                        <div class="form-group">
                            <input type="text" class="form-control WideBox" placeholder="User Name" id="ForgotPwUserNametxt" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="ForgotPwUserNametxt"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <!-- CMallory - 00650 - Added event to button -->
                            <asp:Button ID="Resetpwbtn" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="Resetpwbtn_Click" />
                        </div>
                    </div>

                </div>
            </div>
        </section>


        <uc1:Footer runat="server" ID="Footer" />


        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="js/classie.js"></script>
        <script src="js/cbpAnimatedHeader.js"></script>

        <!-- Contact Form JavaScript -->
        <script src="js/jqBootstrapValidation.js"></script>
        <script src="js/contact_me.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="js/agency.js"></script>


    </form>


</body>

</html>
