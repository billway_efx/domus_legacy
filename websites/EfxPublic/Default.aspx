﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EfxPublic.Default" %>


<%@ Register Src="~/UserControls/TopNavMenu.ascx" TagPrefix="uc1" TagName="TopNavMenu" %>
<%@ Register Src="~/UserControls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>
<%@ Register Src="~/UserControls/PublicContactForm.ascx" TagPrefix="uc1" TagName="PublicContactForm" %>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="executive property management, electronic payments processor">
    <meta name="author" content="">

    <title>Domus - Premier Multifamily Financial Services</title>
     <link id="PublicFavicon" rel="icon" type="image/x-icon" href="/DomusIcon.ico" />   
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/agency.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">
    <form id="form1" runat="server">

    <uc1:TopNavMenu runat="server" id="TopNavMenu" />



    <!-- Header -->
    <header class="headerImage" style="background-image: url('/img/Flux/Home_Web.jpg')">
        <div class="container">
            <div class="row">


            
            <div class="intro-text">
                
                <div class="row center intro-lead-in text-shadow text-Green col-lg-6">REVOLUTIONIZE THE WAY YOU PROCESS PAYMENTS</div>
                <div class="col-lg-8 center text-center">
                    <h3 class="text-shadow">DOMUS offers a full range of financial services designed to elevate the payment experience.</h3>
                </div>
                
                <a href="#contact" class="page-scroll btn btn-xl">Contact Us</a>
            </div>
                <div class="row col-lg-6 center">

                </div>
                </div>
        </div>
    </header>

        <div class="clearfix"></div>



    <section>
        <div class="container">
            <div class="col-lg-12 text-center">

                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">MAKE YOUR ROI<br /> GO OMG</h2>
                        <h3 class="section-subheading text-muted">
                            DOMUS makes it easy and fun for residents to pay their rent, giving you time to focus on  
                            revenue generation and signing new leases. Automating the rent collection process provides 
                            valuable insights into the financial status of your properties. By increasing the efficiency 
                            of how you receive, process and analyze payments, DOMUS accelerates your cash flow, giving 
                            your ROI something to write home about.

                        </h3>
                    </div>
                </div>

            </div>
        </div>
    </section>

        <section style="background-color:#662e6b">
            <div class="container">

                <div class="col-lg-12 text-center">
                     <img class="img-responsive img-centered" src="img/Flux/infographic.jpg" style="max-height: 500px"  />
                </div>
            </div>
              

        </section>
               
          

  
    <!-- Services Section -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="section-heading">Our Core Services</h3>
                    <h3 class="section-subheading text-muted">Financial Services Backed by EFX</h3>
                    <h3 class="section-subheading text-muted">
                        More than just a rent payment processor, DOMUS offers a full range of financial services designed to elevate the payment experience. Personalized on-site training and adoption services, automatic payment options and detailed reporting helps executives keep a watchful eye on all their properties.
                    </h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-money fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Administration</h4>
                    <p class="text-muted">Automated Rent Payment Processing</p>
                </div>

                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-bar-chart-o fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Accounting Audits</h4>
                    <p class="text-muted">Daily Deposits<br />Collectables
                    </p>

                </div>

                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Multiple Payment Processing</h4>
                    <p class="text-muted">
                            Android and iOS Mobile App<br />
                            Credit Cards<br />
                            Debit Cards<br />
                            Electronic Checks<br />
                            Cash
                        </p>
                </div>

                 <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-plug fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Customer Service</h4>
                    <p class="text-muted">Dedicated client support through our toll-free bilingual support & email support.</p>
                </div>




            </div>
        </div>
    </section>

        <section id="infographic" style="background-color: #662e6b">
              <div class="container">

                    <div class="col-lg-12 text-center">
                    <h2 class="text-Green">Fully Optimized on any platform</h2>
                        <h3 style="color:#fff">
                            You choose how and when you access.
                        </h3>
 
                </div>

            <div class="row">
                <div class="col-lg-12 text-center">
                    <img src="img/mobileimage.jpg" class="img-responsive img-centered" style="max-height: 500px" />
                </div>

                </div>
                </div>
            </section>

        <section style="background-color: #662e6b">
            <div class="container">
                <div class="col-lg-12 text-center">
                    <h3 class="text-white">Download the Domus app today</h3>
                </div>
                 <div class="col-lg-12 text-center">
                     <div class="col-lg-6">
                         <asp:HyperLink ID="AdroidLink" runat="server" Target="_blank" NavigateUrl="/AppDownLoad/Adroid/app.apk">
                            <img src="AppDownLoad/android-icon_white.png" class="img-responsive img-centered" style="max-height: 200px" />
                         </asp:HyperLink>
                     </div>
                     <div class="col-lg-6">
                         <asp:HyperLink ID="AppleLink" runat="server" NavigateUrl="http://app.domusme.com/landing/Desktop#.V8lNWOR4eUk">
                            <img src="AppDownLoad/apple-icon_white.png" class="img-responsive img-centered" style="max-height: 200px" />
                         </asp:HyperLink>
                     </div>

                </div>


            </div>
        </section>


        <aside class="clients" style="background-color:#F3F3F3">
        <div class="container text-center">
            <div class="col-lg-10 center">

            
            <h2>Integrated Partners</h2>
                <p>Keep your current property management software.  Let us help you manage your payables and collectables better by integrating with your current provider.</p>
            <div class="row">

                    <div class="col-md-4">
                    <a href="#">
                        <img src="img/AMSI.png" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="#">
                        <img src="img/Yardi.png" class="img-responsive img-centered" alt="">
                    </a>
                </div>
            
                <div class="col-md-4">
                    <a href="#">
                        <img src="img/MRI.png" class="img-responsive img-centered" alt="">
                    </a>
                </div>


            </div>
                </div>
        </div>
    </aside>


    

    <!-- About Section -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-center center">
                    <h2 class="section-heading">About</h2>
                    <h3 class="section-subheading text-muted">EXECUTIVE PROPERTY MANAGEMENT TOOLS BACKED BY GENIUS.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 text-center center">
                    <p>
                       Developed by EFX Financial Services, an electronic payments processor with over a decade of experience, DOMUS upgrades your existing platform, freeing up valuable time for you to focus on keeping residents happy.
                    </p>

                    <p>
                        Formerly Rent Paid Online, DOMUS makes it easy for residents to pay their rent each month with multiple options and ways to pay. Property executives, managers, and accountants enjoy the benefits of safe and secure transactions, streamlined accounting processes, consolidated reporting and the ability to track performance across your entire portfolio of properties.

                    </p>
                </div>
            </div>
        </div>
    </section>



<uc1:PublicContactForm runat="server" id="PublicContactForm" />


<uc1:Footer runat="server" id="Footer" />


    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!--CMallory - Commented out implementation of agency.js file -->
    <!-- Custom Theme JavaScript 
    <script src="js/agency.js"></script>
    -->

            </form>
</body>

</html>
