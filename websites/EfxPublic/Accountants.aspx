﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Accountants.aspx.cs" Inherits="EfxPublic.Accountants" %>


<%@ Register Src="~/UserControls/TopNavMenu2.ascx" TagPrefix="uc1" TagName="TopNavMenu2" %>
<%@ Register Src="~/UserControls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>
<%@ Register Src="~/UserControls/PublicContactForm.ascx" TagPrefix="uc1" TagName="PublicContactForm" %>




<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Accountants for Property Management - Domus</title>
          <link id="PublicFavicon" rel="icon" type="image/x-icon" href="/DomusIcon.ico" />  
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/agency.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <form id="form1" runat="server">

        <uc1:TopNavMenu2 runat="server" id="TopNavMenu2" />

                        <header id="Residents" class="text-center tall text-center" style="background-position: center; background-color: #662E6B; color: #FFFFFF; background-image: url(/img/Flux/header_accountants_web.jpg); background-repeat: no-repeat;">
            <div class="container text-center">
     
            <div class="row">


            
            <div class="intro-text">
                
                <div class="row center intro-lead-in text-shadow text-Green col-lg-6">your bank teller will miss you.</div>
                 <a href="#contact" class="page-scroll btn btn-xl">Contact Us</a>
              
            </div>
                </div>
       


            </div>


        </header>

        <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">WITH DOMUS RENT PAYMENT PROCESSING THE BENEFITS ARE AUTOMATIC.</h2>
                    <h3 class="section-subheading text-muted">
                       Electronic deposits eliminate the need for property
                        staff to make time-consuming trips to the bank.
                        Processing payments, making deposits, managing
                        returned items and providing both accounts
                        receivable aging and collectibles reports yield
                        valuable insight into the financial status of
                        your properties.

                    </h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-money fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Certified Integration</h4>
                    <p class="text-muted">DOMUS merges easily with your property
                        management software systems including
                        Yardi, MRI, AMSI and RealPage.
                                                                    </p>
                </div>

                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-bar-chart-o fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Get the Right Information Faster</h4>
                    <p class="text-muted">Real-time access to payment information
                            with mobile capability gives you the
                            details you need instantly.</p>
                </div>

                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Provide Seamless Migration</h4>
                    <p class="text-muted">
                        DOMUS offers check readers for your
                        properties so residents can still pay
                        their rent by check as they transition to
                        online payments.

                    </p>
                </div>

                 <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-plug fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Accept Cash Safely</h4>
                    <p class="text-muted">Cash payments and money orders
                    are handled safely and efficiently
                    with RentByCash.</p>
                </div>
            </div>

                       <div class="row text-center">
                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-money fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Streamline Your Accounting Process</h4>
                    <p class="text-muted">Clear and easy-to-use reporting tools allow for easier bank reconciliations</p>
                </div>

                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-bar-chart-o fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Accelerate Check Return Resolutions</h4>
                    <p class="text-muted">E-check returns are reported to the property the same day they are received</p>
                </div>

                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Reduce Exposure to Theft and Loss</h4>
                    <p class="text-muted">DOMUS fully automates the process
                                between your resident’s online payments
                                and deposits to your bank account safely
                                and securely.</p>
                </div>

                 <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-plug fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Consolidate Your Reporting</h4>
                    <p class="text-muted">DOMUS provides the specifics of each
                        deposit including date, time, amount,
                        resident name, property name, and
                        type of payment for all residents, for all
                        properties, all in one place.
                    </p>
                </div>
            </div>



        </div>
    </section>

        <section id="infographic" style="background-color: #662e6b">
            <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <img src="img/infographic1.jpg" class="img-responsive img-centered" style="max-height: 500px" />
                </div>

                </div>
                </div>

        </section>

        <section class="text-center">
            <div class="container">
                <h2 class="section-heading">Why Everyone loves Domus!</h2>
                <div class="row text-center">
                    <h3 class="text-Green">
                       “Anything that saves us both time and money is great for us - and DOMUS does exactly that.”
                    </h3>
                </div>
                <div class="row text-center">

                
                <div class="col-lg-4">
                    <h3 class="section-subheading text-muted">
                       “The customer service our residents have received from DOMUS has been excellent. Our residents hardly ever have to come to the office with questions about paying their rent!”
                    </h3>
                </div>

                <div class="col-lg-4">
                    <h3 class="section-subheading text-muted">
                       “I love DOMUS. It integrates with our property management software, removing any need to post any on-site payments. It saves both myself and the residents time. They don’t have to come into the office to pay their rent and I don’t have to deposit the checks at the bank.”
                    </h3>
                </div>
                    <div class="col-lg-4">
                        <h3 class="section-subheading text-muted">
                            “DOMUS has let us provide our tenants with a more flexible way to pay their rent that fits in with their busy lives.”
                        </h3>
                       
                </div>


                    </div>
            </div>
        </section>



        <uc1:PublicContactForm runat="server" id="PublicContactForm" />


        <uc1:Footer runat="server" id="Footer" />

            <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/agency.js"></script>

    </form>
</body>
</html>
