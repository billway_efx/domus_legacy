﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework;
using System.Text.RegularExpressions;
namespace EfxPublic
{
    public partial class CurrentRenterRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if(!Page.IsPostBack)
            {
                PopulateStateList();

            }


        }

        protected void FindPropertybtn_Click(object sender, EventArgs e)
        {
            bool isValid = CustomValidator1.IsValid;

            if(isValid == true)
            {
                GridView1.DataBind();
                int RowCount = GridView1.Rows.Count;

                MultiView1.SetActiveView(ResultPropertyView);
                if (RowCount >= 1)
                {
                    PropertyHeaderlbl.Text = "Select Your Property";

                }
                else
                {
                    PropertyHeaderlbl.Text = "No Properties Found";
                }
            }
            
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            int PropertyNameLength = PropertyNametxt.Text.Length;
            if (PropertyNameLength < 4)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }

        }



        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int PropertyID = (int)GridView1.SelectedDataKey[0];
            var p = new EfxFramework.Property(PropertyID);
            bool IntegratedCheck = IsPropertyIntegrated(PropertyID);
            CompleteRegHeaderlbl.Text = p.PropertyName;

            if(IntegratedCheck == false)
            {
                IntegratedSearch.Visible = false;
            }
            else
            {
                IntegratedSearch.Visible = true;
            }

            MultiView1.SetActiveView(CompleteRegisterView);
            
        }



        private static bool IsPropertyIntegrated(int propertyId)
        {
            //Get all properties
            var Prop = new Property(propertyId);
            return !string.IsNullOrEmpty(Prop.PmsId) && Prop.PmsTypeId.HasValue;
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            MultiView1.SetActiveView(integratedlookupView);
        }

        protected void SearchAgainbtn_Click(object sender, EventArgs e)
        {

            MultiView1.SetActiveView(SearchPropertyView);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            string PropertyID = GridView1.SelectedDataKey[0].ToString();
            decimal RA = 0.00m;
            string FN = FirstNameSearchtxt.Text;
            string LN = LastNameSearchtxt.Text;
            if(CustomValidator4.IsValid)
            {
                RA = decimal.Parse(RentAmountSearchtxt.Text);
            }
            else
            {
                return;
            }
            

            int propID = Int32.Parse(PropertyID);

            RPOIDSearchResultGrid.DataSource = DataAccess.Register.GetRPOIdSearchResults(propID, FN, LN, RA);
            RPOIDSearchResultGrid.DataBind();

            if (RPOIDSearchResultGrid.Rows.Count == 0)
            {
                Resultlbl.Text = "We're sorry, but we are unable to locate a profile based on the information you provided.<br />Please contact our support department at 1-855-PMY-RENT or your Property Manager to obtain your ID.";
            }
            else
            {
                Resultlbl.Text = "Please select your name to continue";
            }



        }

        protected void CanelRegbtn_Click(object sender, EventArgs e)
        {
            MultiView1.SetActiveView(SearchPropertyView);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            FirstNameSearchtxt.Text = string.Empty;
            LastNameSearchtxt.Text = string.Empty;
            RentAmountSearchtxt.Text = string.Empty;
            MultiView1.SetActiveView(CompleteRegisterView);
        }

        protected void CustomValidator4_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool isValid = false;
            string RentAmount = RentAmountSearchtxt.Text;
            decimal RentDec = 0.00m;

            isValid = Decimal.TryParse(RentAmount, out RentDec);

            if(isValid == true)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }

        }

        protected void RPOIDSearchResultGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            string PMsID = RPOIDSearchResultGrid.SelectedDataKey[0].ToString();

            string FirstName = (RPOIDSearchResultGrid.SelectedRow.FindControl("Label1") as Label).Text;
            string LastName = (RPOIDSearchResultGrid.SelectedRow.FindControl("Label2") as Label).Text;

            RPOidtxt.Text = PMsID;
            FirstNametxt.Text = FirstName;
            LastNametxt.Text = LastName;

            MultiView1.SetActiveView(CompleteRegisterView);

        }

        protected void ComplRegbtn_Click(object sender, EventArgs e)
        {
            var CheckRenter = DataAccess.Register.IsCurrentRenter(Emailtxt.Text);

            if(CheckRenter.IsExisting == "False")
            {
                int PropertyID = (int)GridView1.SelectedDataKey[0];
                var p = new EfxFramework.Property(PropertyID);
                bool IntegratedCheck = IsPropertyIntegrated(PropertyID);


                if (Page.IsValid)
                {
                    string FirstName = FirstNametxt.Text;
                    string LastName = LastNametxt.Text;
                    string Unit = unittxt.Text;
                    string Phone = CleanPhoneNumber(PhoneNumbertxt.Text);
                    string Address = Addresstxt.Text;
                    string city = citytxt.Text;
                    int stateid = int.Parse(StateDDL.SelectedValue);
                    string zip = ZipCodetxt.Text;
                    string EmailAddress = Emailtxt.Text;
                    string pw = pwtxt.Text;
                    string RPOID = RPOidtxt.Text;

                    int RenterID = 0;

                    if(IntegratedCheck == false)
                    {
                        //this will execute if renter is not part of integrated property
                       RenterID = DataAccess.Register.Renter_RegisterNonIntegratedRenter(FirstName, LastName, Phone, EmailAddress, Address, Unit, city, stateid, zip, true, PropertyID, pw);
                        if(RenterID >= 1)
                        {
                            Response.Redirect("/ResidentLogin.aspx", false);
                        }
                    }
                    else
                    {
                        if(RPOID != "")
                        {
                            //this will execute if renter is part of integrated property.
                            //The renter will already be in system and we will need to update email address and password if pmsid matches
                            RenterID = DataAccess.Register.Renter_RegisterIntegratedRenter(PropertyID, EmailAddress, RPOID, pw);

                            if (RenterID >= 1)
                            {
                                Response.Redirect("/ResidentLogin.aspx", false);
                            }

                        }
                        else
                        {
                            //Display Warning message for user to search for pmsid
                            EmailChecklbl.Text = "Please include your RPO ID";

                        }
                        

                    }


                }

            }
            else
            {
                EmailChecklbl.Text = "Sorry, that email is not available. If you need help, contact Customer Service";
            }

        }

        private void PopulateStateList()
        {
            StateDDL.Items.Clear();
            StateDDL.Items.Add(new ListItem("Select State", "0"));
            StateDDL.DataTextField = "StateProvinceName";
            StateDDL.DataValueField = "StateProvinceId";
            StateDDL.DataSource = DataAccess.Register.StateList();
            StateDDL.DataBind();
        }

        private string CleanPhoneNumber(string Phone)
        {
            string _newPhone = Phone;
            string _AltPhone = "0000000000";

            _newPhone = _newPhone.Replace("(", "");
            _newPhone = _newPhone.Replace(")", "");
            _newPhone = _newPhone.Replace("-", "");
            _newPhone = _newPhone.Replace(" ", "");

            //cakel: 01/26/2016 Added new replace code because we are getting real phone numbers back with text at end
            _newPhone = _newPhone.Replace("cell", "");
            _newPhone = _newPhone.Replace("Cell", "");
            _newPhone = _newPhone.Replace("home", "");
            _newPhone = _newPhone.Replace("Home", "");

            //cakel: 01/26/2016 added regex to check for letters in Phone


            bool CheckForLetters = Regex.IsMatch(_newPhone, @"[A-Za-z]+");//@"[A-Za-z]+",

            if (CheckForLetters == true)
            {
                _newPhone = _AltPhone;
            }
            //Check the length and if longer or shorter than 10 replace.
            int PhoneLength = _newPhone.Length;
            if (PhoneLength > 10 || PhoneLength < 10)
            {
                _newPhone = _AltPhone;
            }


            return _newPhone;
        }

  
    }
}