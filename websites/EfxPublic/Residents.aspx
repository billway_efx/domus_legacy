﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Residents.aspx.cs" Inherits="EfxPublic.Residents" %>


<%@ Register Src="~/UserControls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>
<%@ Register Src="~/UserControls/TopNavMenu2.ascx" TagPrefix="uc1" TagName="TopNavMenu2" %>




<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Residents - Domus</title>
      <link id="PublicFavicon" rel="icon" type="image/x-icon" href="/DomusIcon.ico" />  
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/agency.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css' />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body id="page-top" class="index">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <uc1:TopNavMenu2 runat="server" ID="TopNavMenu2" />


        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>





        <!-- Residents Section -->

        <header id="Residents" class="text-center tall text-center" style="background-position: center; background-color: #662E6B; color: #FFFFFF; background-image: url(/img/Flux/header_residents_web.jpg); background-repeat: no-repeat;">
            <div class="container text-center">

                <div class="row">



                    <div class="intro-text">

                        <div class="intro-lead-in text-shadow">Giving you more flexibility than your yoga instructor</div>
                        <div class="intro-heading text-shadow text-Green">Your Terms</div>

                    </div>
                </div>



            </div>


        </header>


        <section style="background-color: #badc02">

            <div class="container text-center">
                <div class="col-lg-4">
                    <h3>CuRrent Residents</h3>
                 
                  <%--  <div class="form-group">
                        <input type="text" class="form-control WideBox" placeholder="User Name" id="untxt" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="untxt"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control WideBox" placeholder="Password" id="pwtxt" runat="server" />
                    </div>--%>
                    <div class="form-group">
                        <asp:Button ID="RenterLoginbtn" runat="server" CssClass="btn btn-xl btn-block" Text="Login" OnClick="RenterLoginbtn_Click" />
                    </div>

                </div>


                <div class="col-lg-4">
                    <h3>Need To Register?</h3>
                 
                     <asp:Button ID="RenterRegisterBtn" runat="server" CssClass="btn btn-xl btn-block" Text="Register" OnClick="RenterRegisterBtn_Click" />
                </div>

                <div class="col-lg-4">
                    <h3>Want To Apply?</h3>
                 
                     <asp:Button ID="ApplicantRegisterBtn" runat="server" CssClass="btn btn-xl btn-block" Text="Apply Here" OnClick="ApplicantRegisterBtn_Click" />
                </div>

            </div>

        </section>


        <section id="services">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">Benefits for Residents</h2>
                        <h3 class="section-subheading text-muted">DOMUS understands that today’s tenants have busy lives and hectic schedules. Offering automatic payments, including AutoPay, is a welcomed convenience. Now, residents can manage their rent payment options on their own schedule from the privacy of their own home or while on the go with their mobile device.</h3>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-3">
                        <span class="fa-stack fa-4x">
                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
                            <i class="fa fa-money fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 class="service-heading">More Freedom</h4>
                        <p class="text-muted">Enable residents to pay anyway they choose by offering AutoPay, Mobile Apps for IOS and Android and PayByText.</p>
                    </div>

                    <div class="col-md-3">
                        <span class="fa-stack fa-4x">
                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
                            <i class="fa fa-bar-chart-o fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 class="service-heading">A Safer Solution</h4>
                        <p class="text-muted">Conveniently located at select retail locations, residents can pay rent with cash securely with RentByCash.</p>
                    </div>

                    <div class="col-md-3">
                        <span class="fa-stack fa-4x">
                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
                            <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 class="service-heading">Ability to Authorize</h4>
                        <p class="text-muted">Whether using their computer, tablet or smartphone, residents can pay their rent electronically or they can call our bilingual customer service representatives and pay over the phone.</p>
                    </div>

                    <div class="col-md-3">
                        <span class="fa-stack fa-4x">
                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
                            <i class="fa fa-plug fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 class="service-heading">Save Time</h4>
                        <p class="text-muted">DOMUS makes your experience hassle-free by eliminating trips to the property management office to drop off a check or process a credit card payment.</p>
                    </div>
                </div>

                <div class="row text-center">
                    <div class="col-md-3">
                        <span class="fa-stack fa-4x">
                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
                            <i class="fa fa-money fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 class="service-heading">More Control</h4>
                        <p class="text-muted">Allow tenants to run their own rent profile without involving property personnel.</p>
                    </div>

                    <div class="col-md-3">
                        <span class="fa-stack fa-4x">
                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
                            <i class="fa fa-bar-chart-o fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 class="service-heading">More Choices</h4>
                        <p class="text-muted">DOMUS supports a full range of payment options including credit/debit cards, E-checks, phone and even cash payments.</p>
                    </div>

                    <div class="col-md-3">
                        <span class="fa-stack fa-4x">
                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
                            <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 class="service-heading">Maintenance Request</h4>
                        <p class="text-muted">We give the ability to send a request from anywhere at any time.</p>
                    </div>

                    <div class="col-md-3">
                        <span class="fa-stack fa-4x">
                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
                            <i class="fa fa-plug fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 class="service-heading">Reminders</h4>
                        <p class="text-muted">We know you are busy.  That is why we can send alerts via email or text to let you know when payment is due</p>
                    </div>
                </div>

            </div>
        </section>
                                          </ContentTemplate>
        </asp:UpdatePanel>

        <uc1:Footer runat="server" ID="Footer" />


        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="js/classie.js"></script>
        <script src="js/cbpAnimatedHeader.js"></script>

        <!-- Contact Form JavaScript -->
        <script src="js/jqBootstrapValidation.js"></script>
        <script src="js/contact_me.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="js/agency.js"></script>


  

    </form>
</body>
</html>
