﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxPublic.SecurePages
{
    public partial class ResidentCalendar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            var RenterID = Session["RenterID"];
            var RenterSession = Session["RenterSession"];


            CalendarListSQL.SelectParameters["RenterID"].DefaultValue = RenterID.ToString();
            CalendarGrid.DataBind();


        }
    }
}