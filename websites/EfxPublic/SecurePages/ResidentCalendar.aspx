﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SecurePages/DomusMaster.Master" AutoEventWireup="true" CodeBehind="ResidentCalendar.aspx.cs" Inherits="EfxPublic.SecurePages.ResidentCalendar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>



    <section>
        <div class="container">

        </div>

                <section>
        <div class="container">

            <div class="col-lg-10 center">
                <h3>Calendar Event</h3>
                <div class="well col-lg-10 center" style="overflow:scroll">
                <h3>Events</h3>
                <asp:GridView ID="CalendarGrid" CssClass="table table-responsive" runat="server" AutoGenerateColumns="False" DataKeyNames="CalendarEventId" DataSourceID="CalendarListSQL" AllowPaging="True" AllowSorting="True" PageSize="15">
                    <Columns>
                        <asp:BoundField DataField="CalendarEventId" HeaderText="CalendarEventId" InsertVisible="False" ReadOnly="True" SortExpression="CalendarEventId" Visible="false" />
                        <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
                        <asp:BoundField DataField="Location" HeaderText="Location" SortExpression="Location" />
                        <asp:BoundField DataField="Details" HeaderText="Details" SortExpression="Details" HtmlEncode="false" />
                        <asp:BoundField DataField="StartDate" HeaderText="StartDate" SortExpression="StartDate" />
                        <asp:BoundField DataField="EndDate" HeaderText="EndDate" SortExpression="EndDate" />
                        <asp:CheckBoxField DataField="Recurring" HeaderText="Recurring" SortExpression="Recurring" Visible="false" />
                    </Columns>

                    <PagerStyle VerticalAlign="Middle" CssClass="GridPager" HorizontalAlign="Left" Font-Size="14px" Font-Bold="True" Width="10px" />
                </asp:GridView>

                <asp:SqlDataSource ID="CalendarListSQL" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="usp_CalendarEvent_GetAllForRenter" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:Parameter Name="RenterId" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </div>




            </div>



            </div>

                    </section>


        </ContentTemplate>
        </asp:UpdatePanel>


</asp:Content>
