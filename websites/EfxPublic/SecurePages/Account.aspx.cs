﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace EfxPublic.SecurePages
{
    public partial class Account : System.Web.UI.Page
    {

   
        protected void Page_Load(object sender, EventArgs e)
        {


            if(!Page.IsPostBack)
            {

                string GetUser = Page.User.Identity.Name;
                if (GetUser != null)
                {
                    populateRenterDetails(GetUser);
                    var _Session = Session["RenterSession"];

                }
                else if (GetUser != "")
                {
                    populateRenterDetails(GetUser);
                    var _Session = Session["RenterSession"];
                }

            }

        }




        private void populateRenterDetails(string _UserName)
        {
            
            
            DataAccess.RenterDetails RD = DataAccess.RenterDataAccess.GetRenterDetailsByUserName(_UserName);
            
            PropertyHeaderlbl.Text = RD.PropertyName;
            RenterNamelbl.Text = RD.FirstName + " " + RD.LastName;
            RenterAddresslbl.Text = RD.RenterStreetAddress;
            RenterCitylbl.Text = RD.RenterCity;
            RenterStatelbl.Text = RD.RenterState;
            RenterUnitlbl.Text = RD.RenterUnit;
            LeaseStartDatelbl.Text = RD.LeaseStart.ToShortDateString();
            LeaseEndDatelbl.Text = RD.LeaseEnd.ToShortDateString();
            RentAmounttxt.Text = RD.rentAmount.ToString("c");

            DataAccess.RenterLeaseDetails RL = DataAccess.RenterDataAccess.GetLeaseDetails(RD.renterID);
            TotalFeeslbl.Text = RL.TotalFees.ToString("c");
            CurrentBalanceDuelbl.Text = RL.CurrentBalanceDue.ToString("c");

            //MaintenanceSQL.SelectParameters["RenterID"].DefaultValue = RD.renterID.ToString();
            //MaintenanceGrid.DataBind();

            PaymentHistSql.SelectParameters["RenterID"].DefaultValue = RD.renterID.ToString();
            PaymentGrid.DataBind();

            DueDatelbl.Text = RD.paymentDueDate.ToShortDateString();

            CalendarListSQL.SelectParameters["RenterID"].DefaultValue = RD.renterID.ToString();
            CalendarGrid.DataBind();




        }

        protected void MakePaymentbtn_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/SecurePages/ResidentPayment.aspx", false);
        }






    }
}