﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace EfxPublic.SecurePages
{


   public partial class ResidentPayment
   {

      /// <summary>
      /// UpdatePanel1 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.UpdatePanel UpdatePanel1;

      /// <summary>
      /// ModalPanel control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Panel ModalPanel;

      /// <summary>
      /// mpe control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::AjaxControlToolkit.ModalPopupExtender mpe;

      /// <summary>
      /// RoundedCornersExtender1 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::AjaxControlToolkit.RoundedCornersExtender RoundedCornersExtender1;

      /// <summary>
      /// DropShadowExtender1 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::AjaxControlToolkit.DropShadowExtender DropShadowExtender1;

      /// <summary>
      /// RenterFullNamelbl control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Label RenterFullNamelbl;

      /// <summary>
      /// RenterAddresslbl control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Label RenterAddresslbl;

      /// <summary>
      /// RenterStatelbl control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Label RenterStatelbl;

      /// <summary>
      /// RenterEmailAddresslbl control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Label RenterEmailAddresslbl;

      /// <summary>
      /// GridView1 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.GridView GridView1;

      /// <summary>
      /// PaymentSQL control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.SqlDataSource PaymentSQL;

      /// <summary>
      /// FeeListArea control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.HtmlControls.HtmlGenericControl FeeListArea;

      /// <summary>
      /// FeeDetailGrid control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.GridView FeeDetailGrid;

      /// <summary>
      /// CloseFeeListBtn control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Button CloseFeeListBtn;

      /// <summary>
      /// WalletGridSection control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.HtmlControls.HtmlGenericControl WalletGridSection;

      /// <summary>
      /// ApplicationSummary control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.ValidationSummary ApplicationSummary;

      /// <summary>
      /// paybuttonArea control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.HtmlControls.HtmlGenericControl paybuttonArea;

      /// <summary>
      /// PaySingleItemNewCreditCardbtn control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Button PaySingleItemNewCreditCardbtn;

      /// <summary>
      /// PaySingleItemNewBankAcctbtn control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Button PaySingleItemNewBankAcctbtn;

      /// <summary>
      /// PayWithCashbtn control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Button PayWithCashbtn;

      /// <summary>
      /// CancelPaybtn control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Button CancelPaybtn;

      /// <summary>
      /// PaymethodTypeHiddenField control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.HiddenField PaymethodTypeHiddenField;

      /// <summary>
      /// SelectMultiplePaymentOptionDDL control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.DropDownList SelectMultiplePaymentOptionDDL;

      /// <summary>
      /// GridView2 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.GridView GridView2;

      /// <summary>
      /// Label5 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Label Label5;

      /// <summary>
      /// CurrentBalanceDuelbl control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Label CurrentBalanceDuelbl;

      /// <summary>
      /// AmountArea control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.HtmlControls.HtmlGenericControl AmountArea;

      /// <summary>
      /// Label6 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Label Label6;

      /// <summary>
      /// ConvFeeTotallbl control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Label ConvFeeTotallbl;

      /// <summary>
      /// Label9 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Label Label9;

      /// <summary>
      /// AmountValidator control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.CustomValidator AmountValidator;

      /// <summary>
      /// AmountBeingPaidtxt control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.TextBox AmountBeingPaidtxt;

      /// <summary>
      /// UpdatetotalBtn control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Button UpdatetotalBtn;

      /// <summary>
      /// Label7 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Label Label7;

      /// <summary>
      /// TotalAmountlbl control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Label TotalAmountlbl;

      /// <summary>
      /// CVVArea control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.HtmlControls.HtmlGenericControl CVVArea;

      /// <summary>
      /// CSClbl control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Label CSClbl;

      /// <summary>
      /// CustomValidator1 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.CustomValidator CustomValidator1;

      /// <summary>
      /// CSCtxt control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.TextBox CSCtxt;

      /// <summary>
      /// TermsCheckbox control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.CheckBox TermsCheckbox;

      /// <summary>
      /// AgreementLink control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.HyperLink AgreementLink;

      /// <summary>
      /// PaywithSinglePayMethodBtn control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Button PaywithSinglePayMethodBtn;

      /// <summary>
      /// PaywithMultiplePayMethodbtn control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Button PaywithMultiplePayMethodbtn;

      /// <summary>
      /// PaymentErrorArea control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.HtmlControls.HtmlGenericControl PaymentErrorArea;

      /// <summary>
      /// PaymentErrorlbl control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Label PaymentErrorlbl;

      /// <summary>
      /// AddnewWalletItemSection control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.HtmlControls.HtmlGenericControl AddnewWalletItemSection;

      /// <summary>
      /// EcheckADD control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.HtmlControls.HtmlGenericControl EcheckADD;

      /// <summary>
      /// EcheckHeaderlbl control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Label EcheckHeaderlbl;

      /// <summary>
      /// BankAccountTypeDDL control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.DropDownList BankAccountTypeDDL;

      /// <summary>
      /// RequiredFieldValidator1 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;

      /// <summary>
      /// RegularExpressionValidator1 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator1;

      /// <summary>
      /// NewRoutingNumbertxt control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.TextBox NewRoutingNumbertxt;

      /// <summary>
      /// RequiredFieldValidator2 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator2;

      /// <summary>
      /// RegularExpressionValidator2 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator2;

      /// <summary>
      /// NewAccountnumbertxt control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.TextBox NewAccountnumbertxt;

      /// <summary>
      /// CreditCardAdd control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.HtmlControls.HtmlGenericControl CreditCardAdd;

      /// <summary>
      /// CCHeaderlbl control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Label CCHeaderlbl;

      /// <summary>
      /// RequiredFieldValidator3 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator3;

      /// <summary>
      /// FullNametxt control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.TextBox FullNametxt;

      /// <summary>
      /// RequiredFieldValidator4 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator4;

      /// <summary>
      /// RegularExpressionValidator3 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator3;

      /// <summary>
      /// CardAccountnumtxt control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.TextBox CardAccountnumtxt;

      /// <summary>
      /// FilteredTextBoxExtender1 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::AjaxControlToolkit.FilteredTextBoxExtender FilteredTextBoxExtender1;

      /// <summary>
      /// ExpMonthDDL control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.DropDownList ExpMonthDDL;

      /// <summary>
      /// ExpYearDDL control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.DropDownList ExpYearDDL;

      /// <summary>
      /// RequiredFieldValidator5 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator5;

      /// <summary>
      /// RegularExpressionValidator4 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator4;

      /// <summary>
      /// CVVtxt control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.TextBox CVVtxt;

      /// <summary>
      /// FilteredTextBoxExtender2 control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::AjaxControlToolkit.FilteredTextBoxExtender FilteredTextBoxExtender2;

      /// <summary>
      /// PayByCashHeader control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.HtmlControls.HtmlGenericControl PayByCashHeader;

      /// <summary>
      /// PayByCashArea control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.HtmlControls.HtmlGenericControl PayByCashArea;

      /// <summary>
      /// PaybyCashConflbl control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.Label PaybyCashConflbl;

      /// <summary>
      /// PayByCashLink control.
      /// </summary>
      /// <remarks>
      /// Auto-generated field.
      /// To modify move field declaration from designer file to code-behind file.
      /// </remarks>
      protected global::System.Web.UI.WebControls.HyperLink PayByCashLink;
   }
}
