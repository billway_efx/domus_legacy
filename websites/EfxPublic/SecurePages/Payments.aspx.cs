﻿using System;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using EfxFramework;


//This file is not in use and was originally used for sample app.  the new page is ResidentPayment.aspx.  Please use that one



namespace EfxPublic.SecurePages
{
    public partial class Payments : System.Web.UI.Page
    {
        
        public class Payment_Items
        {
            public string PayableItemID;
            public float PaymentAmount;
            public float PaymentConvFee;
            public float PaymentTotal;
            public string PaymentMethod;
            public string PayerWalletID;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //Added code to redirect to new page - This page is not in use and is being save for a ref
            Response.Redirect("SecurePages/ResidentPayment.aspx");

            if (!Page.IsPostBack)
            {

                var RenterSession = Session["RenterSession"];
                HttpCookie _RenterID = Request.Cookies["RenterID"];
                HttpCookie _Session = Request.Cookies["RenterSession"];
                if(_RenterID != null || _Session != null)
                {
                    string DomusEndPoint = EfxFramework.EfxSettings.DomusAPIURL;

                    string RenterSessionCookie = Request.Cookies["RenterSession"]["Session"];
                    string RenterIDCookie = Request.Cookies["RenterID"]["ID"];

                    string GetRenterByID = DomusEndPoint + "/GetRenterByRenterId/";
                    string GetWalletItems = DomusEndPoint + "/Wallet_GetPaymentMethodsForRenter/";

                    string RenterXML = GetRenterDetailsRequest(RenterSessionCookie, RenterIDCookie, GetRenterByID);
                    string PayableItems = GetPaymentItemsForGrid(RenterSessionCookie, RenterIDCookie);
                    string WalletItems = GetRenterDetailsRequest(RenterSessionCookie, RenterIDCookie, GetWalletItems);

                    BindPayableGrid(PayableItems);
                    BindWalletGrid(WalletItems);
                    AddnewWalletItemSection.Visible = false;
                    SubmitPay1.Visible = false;
                    PopulateRenterDetails(RenterXML);

                }
                else
                {
                    Response.Redirect("~/ResidentLogin.aspx");
                }

            }

        }


        public string GetRenterDetailsRequest(string RenterSession, string RenterID, string EndPoint)
        {

            //Endpoint will come from property table this is for tesitng only
            string ApiEndPoint = EndPoint;

            string ApiEndPoint2 = RenterSession + "/" + RenterID;

            //empty string for response
            string RespString = "";

            string ApiFullString = ApiEndPoint + ApiEndPoint2;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            HttpWebRequest request;
            HttpWebResponse response = null;
            StreamReader reader;
            StringBuilder sbSource;
            string returnString = "";

            try
            {
                // Create and initialize the web request  
                request = WebRequest.Create(ApiFullString) as HttpWebRequest;

                request.KeepAlive = false;
                request.Method = "GET";

                // Set timeout to 15 seconds  
                request.Timeout = 15 * 1000;

                // Get response  
                response = request.GetResponse() as HttpWebResponse;

                if (request.HaveResponse == true && response != null)
                {
                    // Get the response stream  
                    reader = new StreamReader(response.GetResponseStream());

                    // Read it into a StringBuilder  
                    sbSource = new StringBuilder(reader.ReadToEnd());

                    returnString = sbSource.ToString();
                }

                return returnString.ToString();
            }
            catch (WebException wex)
            {
                string fullerror = "";
                // This exception will be raised if the server didn't return 200 - OK  
                // Try to retrieve more information about the network error  
                if (wex.Response != null)
                {

                    using (HttpWebResponse errorResponse = (HttpWebResponse)wex.Response)
                    {
                        string ErrResp = wex.Response.Headers.ToString();

                        fullerror = "ERROR " + errorResponse.StatusDescription + " " + errorResponse.StatusCode + " " + errorResponse.StatusCode;
                    }
                }
                return "";

            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }

        }


        public string GetPaymentItemsForGrid(string RenterSession, string RenterID)
        {
            string DomusEndPoint = EfxFramework.EfxSettings.DomusAPIURL;
            //Endpoint will come from property table this is for tesitng only
            string ApiEndPoint = DomusEndPoint + "/GetPayableItemsForRenter/";

            string ApiEndPoint2 = RenterSession + "/" + RenterID;

            //Return Response as XML
            string ApiFormat = "&$format=xml";

            //empty string for response
            string RespString = "";

            string ApiFullString = ApiEndPoint + ApiEndPoint2;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest request;
            HttpWebResponse response = null;
            StreamReader reader;
            StringBuilder sbSource;
            string returnString = "";

            try
            {
                // Create and initialize the web request  
                request = WebRequest.Create(ApiFullString) as HttpWebRequest;

                request.KeepAlive = false;
                request.Method = "GET";

                // Set timeout to 15 seconds  
                request.Timeout = 15 * 1000;

                // Get response  
                response = request.GetResponse() as HttpWebResponse;

                if (request.HaveResponse == true && response != null)
                {
                    // Get the response stream  
                    reader = new StreamReader(response.GetResponseStream());

                    // Read it into a StringBuilder  
                    sbSource = new StringBuilder(reader.ReadToEnd());

                    returnString = sbSource.ToString();
                }

                return returnString.ToString();
            }
            catch (WebException wex)
            {
                string fullerror = "";
                // This exception will be raised if the server didn't return 200 - OK  
                // Try to retrieve more information about the network error  
                if (wex.Response != null)
                {

                    using (HttpWebResponse errorResponse = (HttpWebResponse)wex.Response)
                    {
                        string ErrResp = wex.Response.Headers.ToString();

                        fullerror = "ERROR " + errorResponse.StatusDescription + " " + errorResponse.StatusCode + " " + errorResponse.StatusCode;
                    }
                }
                return "";

            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }

        }


        public void BindPayableGrid(string _xml)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(_xml);

            using (var OutputStream = new MemoryStream(byteArray))
            {
                var XmlResp = XDocument.Load(OutputStream);
                var RespElement = GetRootElement(XmlResp.Root, "PayablesResponse");

                foreach (XElement Response in RespElement.Elements())
                {
                    string node = Response.Name.LocalName;

                    foreach (XElement Resp2 in Response.Elements())
                    {
                        string node2 = Resp2.Name.LocalName;

                        if (node2 == "PayableItem")
                        {
                            string test = Resp2.ToString();

                            XmlTextReader reader = new XmlTextReader(new StringReader(test));
                            DataSet dataSet = new DataSet();
                            dataSet.ReadXml(reader);

                            GridView1.DataSource = dataSet.Tables[0];
                            GridView1.DataBind();

                        }

                    }

                }

            }

        }

        public void BindWalletGrid(string _xml)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(_xml);

            using (var OutputStream = new MemoryStream(byteArray))
            {
                var XmlResp = XDocument.Load(OutputStream);
                var RespElement = GetRootElement(XmlResp.Root, "Wallet_GetPaymentMethodsResponse");
                string Msg = "";
                foreach (XElement Response in RespElement.Elements())
                {
                    string node = Response.Name.LocalName;

                    if(node == "Message")
                    {
                        Msg = Response.Value.ToString();
                        if(Msg == "Session timed out")
                        {
                            FormsAuthentication.SignOut();
                            HttpContext.Current.Response.Redirect("~/ResidentLogin.aspx");
                        }

                        if(Msg.Contains("Error reading database"))
                        {
                            string Te = "";
                        }

                    }

                    string _Resp = Response.ToString();
                    if (node == "Wallet_PaymentMethods")
                    {
                        XmlTextReader reader = new XmlTextReader(new StringReader(_Resp));
                        DataSet dataSet = new DataSet();
                        dataSet.ReadXml(reader);

                        GridView2.DataSource = dataSet.Tables[0];
                        GridView2.DataBind();

                    }

                }

            }

        }



        //Find Root Element 
        public static XElement GetRootElement(XElement element, string localName)
        {
            XElement Element = null;

            if (element != null && element.Name.LocalName == localName)
                return element;

            if (element != null && element.HasElements)
            {
                foreach (var E in element.Elements())
                {
                    Element = GetRootElement(E, localName);
                }
            }

            return Element;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label Editlbl = (Label)e.Row.FindControl("EditableLBL") as Label;
                CheckBox ck = (CheckBox)e.Row.FindControl("CheckBox1") as CheckBox;

                string EnableSelect = Editlbl.Text;

                if (EnableSelect == "false")
                {
                    ck.Checked = true;
                    ck.Enabled = false;

                }

            }

        }

        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //DescTypelbl
                Label PayMethodLabel = (Label)e.Row.FindControl("PaymentMethodlbl") as Label;
                Label DescTypeLabel = (Label)e.Row.FindControl("DescTypelbl") as Label;
                Label Exp_Date = (Label)e.Row.FindControl("EXP_Datelbl") as Label;
                Label ExpDateLabel = (Label)e.Row.FindControl("ExpDatelbl") as Label;
                Label PayDesc = (Label)e.Row.FindControl("Label4") as Label;

                Button usebtn = (Button)e.Row.FindControl("Button1") as Button;
                Button edit = (Button)e.Row.FindControl("EditWalletItembtn") as Button;
                Button AddNew = (Button)e.Row.FindControl("Addnewbtn") as Button;

                if (PayMethodLabel.Text == "false")
                {
                    DescTypeLabel.Text = "E-Check";
                }
                else
                {
                    ExpDateLabel.Text = "Exp " + Exp_Date.Text;
                    DescTypeLabel.Text = "Credit Card";
                }

                bool ChecknoPay = PayDesc.Text.Contains("No payment methods found.");

                if (ChecknoPay == false)
                {
                    usebtn.Visible = true;
                    edit.Visible = true;
                    AddNew.Visible = false;
                }
                else
                {
                    AddNew.Visible = true;
                    usebtn.Visible = false;
                    edit.Visible = false;
                }

            }

        }

        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {

            AgreeCkbx1.Checked = false;
            SubmitPaybtn1.Enabled = false;
            if (GridView2.SelectedIndex != -1)
            {
                //float ConvFee = float.Parse((GridView2.SelectedRow.FindControl("ConvFeeGridlbl") as Label).Text);
                string PaymentMethod = (GridView2.SelectedRow.FindControl("PaymentMethodlbl") as Label).Text;

                SubmitPay1.Visible = true;
                //SubmitPaybtn1.Text = "Pay $" + GetPaymentTotal(ConvFee).ToString();

                if(PaymentMethod == "true")
                {
                    cscarea.Visible = true;
                }
                else
                {
                    cscarea.Visible = false;
                }


            }

        }

        protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            float Fee = 0f;
            string PaymentMeth = e.CommandArgument.ToString();
            GridViewRow row = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            //Fee = float.Parse((row.FindControl("ConvFeeGridlbl") as Label).Text);

            //true
            if (PaymentMeth != "")
            {
                if (PaymentMeth == "true")
                {
                    //show Credit Card Edit
                    AddnewWalletItemSection.Visible = true;
                    CreditCardAdd.Visible = true;
                    EcheckADD.Visible = false;
                    CCHeaderlbl.Text = "Update Credit Card On File";
                    //AddNewCCandPaybtn.Text = "Update and Pay $" + GetPaymentTotal(Fee);
                    GridView2.Visible = false;
                    WalletGridSection.Visible = false;

                }
                else if (PaymentMeth == "false")
                {
                    //Show E-check Edit
                    AddnewWalletItemSection.Visible = true;
                    CreditCardAdd.Visible = false;
                    EcheckADD.Visible = true;
                    //AddNewEcheckandPaybtn.Text = "Update and Pay $" + GetPaymentTotal(Fee);
                    GridView2.Visible = false;
                    WalletGridSection.Visible = false;
                }
            }

            if (PaymentMeth == "Use")
            {
                AgreeCkbx1.Checked = false;
                SubmitPaybtn1.Enabled = false;
                if (GridView2.SelectedIndex != -1)
                {
                    float ConvFee = float.Parse((GridView2.SelectedRow.FindControl("ConvFeeGridlbl") as Label).Text);
                    SubmitPay1.Visible = true;
                    SubmitPaybtn1.Text = "Pay $" + GetPaymentTotal(ConvFee).ToString();
                }
            }

        }



        private string GetPaymentTotal(float _ConvFee)
        {
            float GTotal = 0f;
            float ConvFee = _ConvFee;
            string _PayItemID = "";
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {

                bool IsChecked = (GridView1.Rows[i].FindControl("CheckBox1") as CheckBox).Checked;
                
                if (IsChecked == true)
                {
                    string total = (GridView1.Rows[i].FindControl("currentBalanceDuelbl") as Label).Text;
                    GTotal += Convert.ToSingle(total);
                    _PayItemID = (GridView1.Rows[i].FindControl("PayableItemIdlbl") as Label).Text;
                }
            }

            string PaymentTotal = (GTotal + ConvFee).ToString();
            return PaymentTotal;
        }

        public Payment_Items GetItems(float ConvFee = 0f)
        {
            
            Payment_Items p = new Payment_Items();
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                bool IsChecked = (GridView1.Rows[i].FindControl("CheckBox1") as CheckBox).Checked;

                if (IsChecked == true)
                {
                    
                    string _amount = (GridView1.Rows[i].FindControl("currentBalanceDuelbl") as Label).Text;
                    p.PaymentAmount = float.Parse(_amount);
                    p.PayableItemID = (GridView1.Rows[i].FindControl("PayableItemIdlbl") as Label).Text;
                    p.PaymentTotal = (p.PaymentAmount + p.PaymentConvFee);
                }

            }
            p.PaymentConvFee = ConvFee;
            p.PaymentMethod = ((GridView2.SelectedRow.FindControl("PaymentMethodlbl") as Label).Text);
            p.PayerWalletID = ((GridView2.SelectedRow.FindControl("PayerWalletIDlbl") as Label).Text);


            return p;

        }


        private void CancelAddUpdateWallet()
        {
            AddnewWalletItemSection.Visible = false;
            GridView2.Visible = true;
            WalletGridSection.Visible = true;
        }



        protected void AddNewCCandPaybtn_Click(object sender, EventArgs e)
        {
            string RenterSession = Session["RenterSession"].ToString();
            string RenterID = Session["RenterID"].ToString();
            string CCholderName = FullNametxt.Text;
            string AccountNumber = CardAccountnumtxt.Text;
            string ExpMonth = ExpMonthDDL.SelectedValue;
            string ExpYear = ExpYearDDL.SelectedValue;
            string CVV = CVVtxt.Text;

            float ConvFee = 0f;
            ConvFee = float.Parse((GridView2.SelectedRow.FindControl("ConvFeeGridlbl") as Label).Text);

            var p = GetItems(ConvFee);
            string PayableItemID = p.PayableItemID;
            string PaymentTotal = p.PaymentTotal.ToString();
            string WalletID = p.PayerWalletID;


            //string Resp = DataAccess.Payments.Wallet_PaySingleItemForRenterWithNewCC(RenterSession, RenterID, PayableItemID, PaymentTotal, CCholderName, AccountNumber, ExpMonth, ExpYear, CVV);


        }

        protected void AddNewEcheckandPaybtn_Click(object sender, EventArgs e)
        {
            string RenterSession = Session["RenterSession"].ToString();
            string RenterID = Session["RenterID"].ToString();

            float ConvFee = 0f;
            ConvFee = float.Parse((GridView2.SelectedRow.FindControl("ConvFeeGridlbl") as Label).Text);

            var p = GetItems(ConvFee);

            string PayableItemID = p.PayableItemID;
            string PaymentTotal = p.PaymentTotal.ToString();

            string AccountType = "1";
            string AccountNumber = NewAccountnumbertxt.Text;
            string RoutingNumber = NewRoutingNumbertxt.Text;

            //string resp = DataAccess.Payments.AddACHPaymentMethodForRenter(RenterSession, RenterID, PayableItemID, PaymentTotal, AccountType, AccountNumber, RoutingNumber);
        }

       


        protected void CancelAddNewEcheckbtn_Click(object sender, EventArgs e)
        {
            CancelAddUpdateWallet();
            GridView2.SelectedIndex = -1;
            SubmitPay1.Visible = false;
            //SubmitPaybtn1.Visible = false;
            //AgreeCkbx1.Visible = false;
        }

        protected void CancelAddNewCCbtn_Click(object sender, EventArgs e)
        {
            CancelAddUpdateWallet();
            GridView2.SelectedIndex = -1;
            SubmitPay1.Visible = false;
            //SubmitPaybtn1.Visible = false;
            //AgreeCkbx1.Visible = false;
        }

        protected void AddNewCCbtn_Click(object sender, EventArgs e)
        {
            AddnewWalletItemSection.Visible = true;
            CreditCardAdd.Visible = true;
            EcheckADD.Visible = false;
            CCHeaderlbl.Text = "Add Credit Card On File";
            //AddNewCCandPaybtn.Text = "Add Credit Card and Pay $" + GetPaymentTotal();
            GridView2.Visible = false;
            WalletGridSection.Visible = false;

        }

        protected void AddNewECheckbtn_Click(object sender, EventArgs e)
        {
            AddnewWalletItemSection.Visible = true;
            CreditCardAdd.Visible = false;
            EcheckADD.Visible = true;
            EcheckHeaderlbl.Text = "Add E-Check on File";
            //AddNewEcheckandPaybtn.Text = "Add E-Check and Pay $" + GetPaymentTotal();
            GridView2.Visible = false;
            WalletGridSection.Visible = false;
        }

        public void PopulateRenterDetails(string RenterXml)
        {
            DataAccess.RenterResponse RR = DataAccess.RenterDataAccess.GetResponse(RenterXml);
            RenterFullNamelbl.Text = RR.RenterFirstName + " " + RR.RenterLastName;
            RenterAddresslbl.Text = RR.RenterAddress;
            RenterEmailAddresslbl.Text = RR.RenterEmail;
        }

        protected void AgreeCkbx1_CheckedChanged(object sender, EventArgs e)
        {
            if(AgreeCkbx1.Checked == true)
            {
                SubmitPaybtn1.Enabled = true;
            }
            else
            {
                SubmitPaybtn1.Enabled = false;
            }
        }

        protected void SubmitPaybtn1_Click(object sender, EventArgs e)
        {
            string _session = Session["RenterSession"].ToString();
            string _RenterID = Session["RenterID"].ToString();
            float ConvFee = 0f;
            ConvFee = float.Parse((GridView2.SelectedRow.FindControl("ConvFeeGridlbl") as Label).Text);
            
            var p = GetItems(ConvFee);

            string _PayItemID = p.PayableItemID;
            string _CurrentAmountDue = p.PaymentAmount.ToString();
            string _PaymentTotal = p.PaymentTotal.ToString();
            string _PaymentMethod = p.PaymentMethod;
            string _walletID = p.PayerWalletID;
            string _response = "";
            string _csc = CSCcodetxt.Text;
            if(_PaymentMethod == "true")
            {
                //_response = DataAccess.Payments.PaySingleItemForRenterWithCSC(_session,_RenterID,_walletID,_PayItemID,_PaymentTotal,_csc);
            }
            else
            {
               // _response = DataAccess.Payments.PaySingleItemForRenter(_session, _RenterID, _walletID, _PayItemID, _PaymentTotal);
            }



            
            
        }

        protected void AgreeCkbx2_CheckedChanged(object sender, EventArgs e)
        {
            if (AgreeCkbx2.Checked == true)
            {
                AddNewEcheckandPaybtn.Enabled = true;
            }
            else
            {
                AddNewEcheckandPaybtn.Enabled = false;
            }
        }

        protected void AgreeCkbx3_CheckedChanged(object sender, EventArgs e)
        {
            if (AgreeCkbx3.Checked == true)
            {
                AddNewCCandPaybtn.Enabled = true;
            }
            else
            {
                AddNewCCandPaybtn.Enabled = false;
            }
        }


    }
}