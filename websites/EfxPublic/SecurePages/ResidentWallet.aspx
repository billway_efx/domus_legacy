﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SecurePages/DomusMaster.Master" AutoEventWireup="true" CodeBehind="ResidentWallet.aspx.cs" Inherits="EfxPublic.SecurePages.ResidentWallet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <section>
        <div class="container">
        </div>
    </section>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>


    <section>
        <div class="container">
               <div class="col-lg-6 center text-center well">
                <h4>
                    <asp:Label ID="RenterFullNamelbl" runat="server"></asp:Label><br />
                    <asp:Label ID="RenterAddresslbl" runat="server"></asp:Label><br />
                    <asp:Label ID="RenterStatelbl" runat="server"></asp:Label><br />
                    <asp:Label ID="RenterEmailAddresslbl" runat="server"></asp:Label>
                </h4>
                <h3>Wallet Items</h3>
            </div>


        </div>
    </section>
    <section>
        <div class="container">
            <div class="col-lg-10">
                <asp:Button ID="AddNewCCbtn" CssClass="btn btn-primary" runat="server" Text="Add New Credit Card" OnClick="AddNewCCbtn_Click" CausesValidation="False" />&nbsp;
                <asp:Button ID="AddNewEcheckbtn" CssClass="btn btn-primary" runat="server" Text="Add New Bank Account" OnClick="AddNewEcheckbtn_Click" CausesValidation="False" />
                
            </div>

            <div class="col-lg-10">
                <asp:GridView ID="WalletGrid" CssClass="table table-responsive" runat="server" DataKeyNames="PayerId,PaymentMethod,PayerMethodId,PayerWalletId" AutoGenerateColumns="False" OnRowDataBound="WalletGrid_RowDataBound" OnRowCommand="WalletGrid_RowCommand">
                
                <Columns>
                        <asp:TemplateField HeaderText="AutoSet" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="autosetlbl" runat="server" Text='<%# Bind("AutoSet") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Wallet Item">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                  <asp:Label ID="DescTypelbl" runat="server" Text="Label"></asp:Label><br />
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("Description") %>'></asp:Label>&nbsp;
                            <asp:Label ID="ExpDatelbl" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Exp Date" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("EXP_Date") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="EXP_Datelbl" runat="server" Text='<%# Bind("EXP_Date") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Editable" HeaderText="Editable" Visible="False" />
                        <asp:BoundField DataField="IsPrimary" HeaderText="IsPrimary" Visible="False" />
                      
                        <asp:BoundField DataField="PayChoose" HeaderText="PayChoose" Visible="False" />
                        <asp:TemplateField HeaderText="PayerId" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PayerId") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("PayerId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PayerMethodId" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("PayerMethodId") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("PayerMethodId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PayerWalletID" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("PayerWalletId") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="PayerWalletIDlbl" runat="server" Text='<%# Bind("PayerWalletId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="PayerWalletId" HeaderText="PayerWalletId" Visible="false" />
                        <asp:TemplateField HeaderText="PaymentMethod" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="PaymentMethodtxt" runat="server" Text='<%# Bind("PaymentMethod") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="PaymentMethodlbl" runat="server" Text='<%# Bind("PaymentMethod") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="RequestSecurityCode" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("RequestSecurityCode") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("RequestSecurityCode") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ProgramID" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="ProgramIDlbl" runat="server" Text='<%# Bind("ProgramId") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>

               <asp:TemplateField HeaderText="">
                   <ItemTemplate>
                       <asp:Button ID="EditItembtn" CssClass="btn btn-primary" CommandName="Select" runat="server" CommandArgument='<%# "Edit" + ";" + Eval("PaymentMethod") + ";" + Eval("PayerWalletId") + ";" + Eval("PayerMethodId") %>' Text="Edit" CausesValidation="False" />
                       <asp:Button ID="Deletebtn" CssClass="btn btn-primary" runat="server" CommandArgument='<%# "Delete" + ";" + Eval("PaymentMethod") + ";" + Eval("PayerWalletId") + ";" + Eval("PayerMethodId") %>' Text="Delete" CausesValidation="False" />
                       <asp:Button ID="AutoPaySetbtn" runat="server" Text="Set AutoPayment" CssClass="btn btn-primary" CommandName="Select" CommandArgument='<%# "SetAutoPayment" + ";" + Eval("PaymentMethod") + ";" + Eval("PayerId") + ";" + Eval("PayerMethodId") %>' />
                       <asp:Button ID="CancelAutoPaybtn" CssClass="btn btn-primary" runat="server" Text="Cancel AutoPayment" CommandArgument='<%# "CancelAutoPayment" + ";" + Eval("PaymentMethod") + ";" + Eval("PayerId") + ";" + Eval("PayerMethodId") %>' />
                   </ItemTemplate>
               </asp:TemplateField>

                    </Columns>
                    <EmptyDataTemplate>
                        <div class="col-lg-6">
                            <h3>No Wallet Items Found</h3>
                            <h4>
                                Use Add Wallet items buttons to add payment method to your Wallet.
                            </h4> 
                        </div>
                    </EmptyDataTemplate>

                    <SelectedRowStyle BackColor="#CCCCCC" />
            </asp:GridView>

            </div>
            
            <!--Add / Edit New Wallet items -->
            <div class="col-lg-12" id="AddnewWalletItemSection" runat="server" visible="false">

                    <!--Add New ECheck -->
                    <div class="col-lg-8 text-left" id="EcheckADD" runat="server" visible="false">
                        <h4>
                            <asp:Label ID="EcheckHeaderlbl" runat="server" Text="New Bank Account"></asp:Label></h4>
                        <div class="col-lg-10">
                            <label>Account Type</label>
                            <asp:DropDownList ID="BankAccountTypeDDL" CssClass="form-control" runat="server">
                                <asp:ListItem Value="1">Checking Account</asp:ListItem>
                                <asp:ListItem Value="2">Savings Account</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-lg-10">
                            <label>Routing Number</label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="echeck" ControlToValidate="NewRoutingNumbertxt" runat="server" ErrorMessage="RequiredFieldValidator"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ValidationGroup="echeck" ControlToValidate="NewRoutingNumbertxt" ValidationExpression="^[0-9]*$" runat="server" ErrorMessage="RegularExpressionValidator"><i class="fa fa-warning validationStyle"></i></asp:RegularExpressionValidator>
                            <asp:TextBox ID="NewRoutingNumbertxt" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-lg-10">
                            <label>Account Number</label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="echeck" ControlToValidate="NewAccountnumbertxt" ErrorMessage="RequiredFieldValidator"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                             <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ValidationGroup="echeck" ControlToValidate="NewAccountnumbertxt" ValidationExpression="^[0-9]*$" runat="server" ErrorMessage="RegularExpressionValidator"><i class="fa fa-warning validationStyle"></i></asp:RegularExpressionValidator>
                            <asp:TextBox ID="NewAccountnumbertxt" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-lg-10" style="margin-top:15px;">
                            <asp:Button ID="AddNewEcheckandPaybtn" CssClass="btn btn-primary" ValidationGroup="echeck" Text="Submit New Account" runat="server" OnClick="AddNewEcheckandPaybtn_Click" />
                            &nbsp;
                            <asp:Button ID="AddEcheckCancel" CssClass="btn btn-primary" CausesValidation="false" runat="server" Text="Cancel" OnClick="AddEcheckCancel_Click" />
                        </div>
                    </div>
                    <!--End Add New ECheck -->

                    <!--Add New CC -->
                    <div class="col-lg-8 text-left" id="CreditCardAdd" runat="server" visible="false">
                        
                        <h4>
                            <asp:Label ID="CCHeaderlbl" runat="server" Text="New Credit Card"></asp:Label>
                        </h4>
                        <div class="col-lg-12">
                            <label>Name on Card</label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="CreditCard" ControlToValidate="FullNametxt" runat="server" ErrorMessage="RequiredFieldValidator"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                            <asp:TextBox ID="FullNametxt" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-12">
                            <label>Account Number</label>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationGroup="CreditCard" ControlToValidate="CardAccountnumtxt" ValidationExpression="^[0-9]*$" runat="server" ErrorMessage="RegularExpressionValidator"><i class="fa fa-warning validationStyle"></i></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="CreditCard" ControlToValidate="CardAccountnumtxt" runat="server" ErrorMessage="RequiredFieldValidator"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                            <asp:TextBox ID="CardAccountnumtxt" runat="server" CssClass="form-control" MaxLength="16"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" ValidChars="0123456789" TargetControlID="CardAccountnumtxt" runat="server" />
                        </div>

                        <div class="col-lg-12">
                            <div class="col-lg-4">
                                <label>Exp Month</label>
                                <asp:DropDownList ID="ExpMonthDDL" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="1">Jan</asp:ListItem>
                                    <asp:ListItem Value="2">Feb</asp:ListItem>
                                    <asp:ListItem Value="3">Mar</asp:ListItem>
                                    <asp:ListItem Value="4">Apr</asp:ListItem>
                                    <asp:ListItem Value="5">May</asp:ListItem>
                                    <asp:ListItem Value="6">Jun</asp:ListItem>
                                    <asp:ListItem Value="7">Jul</asp:ListItem>
                                    <asp:ListItem Value="8">Aug</asp:ListItem>
                                    <asp:ListItem Value="9">Sep</asp:ListItem>
                                    <asp:ListItem Value="10">Oct</asp:ListItem>
                                    <asp:ListItem Value="11">Nov</asp:ListItem>
                                    <asp:ListItem Value="12">Dec</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-lg-4">
                                <label>Exp Year</label>
                                <asp:DropDownList ID="ExpYearDDL" runat="server" CssClass="form-control">
                                    <asp:ListItem>2016</asp:ListItem>
                                    <asp:ListItem>2017</asp:ListItem>
                                    <asp:ListItem>2018</asp:ListItem>
                                    <asp:ListItem>2019</asp:ListItem>
                                    <asp:ListItem>2020</asp:ListItem>
                                    <asp:ListItem>2021</asp:ListItem>
                                    <asp:ListItem>2022</asp:ListItem>
                                    <asp:ListItem>2023</asp:ListItem>
                                    <asp:ListItem>2024</asp:ListItem>
                                    <asp:ListItem>2025</asp:ListItem>
                                    <asp:ListItem>2026</asp:ListItem>
                                    <asp:ListItem>2027</asp:ListItem>
                                    <asp:ListItem>2028</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-lg-3">
                                <label>CVV</label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="CVVtxt" ValidationGroup="CreditCard" runat="server" ErrorMessage="RequiredFieldValidator"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="CreditCard" runat="server" ValidationExpression="^[0-9]*$" ErrorMessage="RegularExpressionValidator" ControlToValidate="CVVtxt"><i class="fa fa-warning validationStyle"></i></asp:RegularExpressionValidator>
                                <asp:TextBox ID="CVVtxt" runat="server" CssClass="form-control" MaxLength="4"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-lg-8" style="margin-top:15px;">
                            <asp:Button ID="AddNewCCandPaybtn" CssClass="btn btn-primary" ValidationGroup="CreditCard" Text="Submit New Credit Card" runat="server" OnClick="AddNewCCandPaybtn_Click" />
                            &nbsp;
                            <asp:Button ID="AddccCancel" CssClass="btn btn-primary" runat="server" CausesValidation="false" Text="Cancel" OnClick="AddccCancel_Click" />
                        </div>
                    </div>
                    <!--End Add New CC -->

                </div>

            <!--Update AutoPayment -->
            <div class="col-lg-12" id="UpdateAutoPaymentArea" runat="server" visible="false">
                 <h4>Add Wallet Item For AutoPayment</h4>
                <div class="col-lg-10">
                <p>The above selected payment method will be used to process your payments automatically. </p>
                    <p>Please select the day of the month you would like us to make your payment</p>
                    <div class="col-lg-6">
                        <label>Select Day</label>
                         <asp:DropDownList ID="DayofMonthDDL" CssClass="form-control" runat="server">
                             <asp:ListItem>1</asp:ListItem>
                             <asp:ListItem>2</asp:ListItem>
                             <asp:ListItem>3</asp:ListItem>
                             <asp:ListItem>4</asp:ListItem>
                             <asp:ListItem>5</asp:ListItem>
                             <asp:ListItem>5</asp:ListItem>
                             <asp:ListItem>6</asp:ListItem>
                             <asp:ListItem>7</asp:ListItem>
                             <asp:ListItem>8</asp:ListItem>
                             <asp:ListItem>9</asp:ListItem>
                             <asp:ListItem>10</asp:ListItem>
                             <asp:ListItem>11</asp:ListItem>
                             <asp:ListItem>12</asp:ListItem>
                             <asp:ListItem>13</asp:ListItem>
                             <asp:ListItem>14</asp:ListItem>
                             <asp:ListItem>15</asp:ListItem>
                             <asp:ListItem>16</asp:ListItem>
                             <asp:ListItem>17</asp:ListItem>
                             <asp:ListItem>18</asp:ListItem>
                             <asp:ListItem>19</asp:ListItem>
                             <asp:ListItem>20</asp:ListItem>
                             <asp:ListItem>21</asp:ListItem>
                             <asp:ListItem>22</asp:ListItem>
                             <asp:ListItem>23</asp:ListItem>
                             <asp:ListItem>24</asp:ListItem>
                             <asp:ListItem>25</asp:ListItem>
                             <asp:ListItem>26</asp:ListItem>
                             <asp:ListItem>27</asp:ListItem>
                             <asp:ListItem>28</asp:ListItem>
                         </asp:DropDownList>

                        <br />
                        <br />
                        <asp:Button ID="SubmitAutoPay" CssClass="btn btn-primary" runat="server" Text="Add Autopayment" OnClick="SubmitAutoPay_Click" />&nbsp;&nbsp;
                        <asp:Button ID="CancelAutoPay" CssClass="btn btn-primary" runat="server" Text="Cancel" OnClick="CancelAutoPay_Click" />

                    </div>
                   

                </div>
                
            </div>
            <!--End Update AutoPayment -->

            <!--End Add / Edit Wallet Items -->
            

        </div>
    </section>

            </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
