﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace EfxPublic.SecurePages
{
    public partial class ResidentWallet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                var RenterSession = Session["RenterSession"];
                HttpCookie _RenterID = Request.Cookies["RenterID"];
                HttpCookie _Session = Request.Cookies["RenterSession"];
                if (_RenterID != null || _Session != null)
                {
                    string DomusEndPoint = EfxFramework.EfxSettings.DomusAPIURL;

                    string RenterSessionCookie = Request.Cookies["RenterSession"]["Session"];
                    string RenterIDCookie = Request.Cookies["RenterID"]["ID"];

                    string GetWalletItems = DomusEndPoint + "/Wallet_GetPaymentMethodsForRenter/";

                    string RenterXML = DataAccess.RenterDataAccess.GetRenterDetailsRequest(RenterSessionCookie, RenterIDCookie);
                    PopulateRenterDetails(RenterXML);

                    string WalletItems = DataAccess.Wallet.GetPaymentMethodsForRenter(RenterSessionCookie, RenterIDCookie);
                    BindWalletGrid(WalletItems);

                }
                else
                {
                    Response.Redirect("~/ResidentLogin.aspx");
                }


            }
        }


        public void PopulateRenterDetails(string RenterXml)
        {
            DataAccess.RenterResponse RR = DataAccess.RenterDataAccess.GetResponse(RenterXml);
            RenterFullNamelbl.Text = RR.RenterFirstName + " " + RR.RenterLastName;
            RenterAddresslbl.Text = RR.RenterAddress;
            RenterEmailAddresslbl.Text = RR.RenterEmail;
        }


        public void BindWalletGrid(string _xml)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(_xml);

            using (var OutputStream = new MemoryStream(byteArray))
            {
                var XmlResp = XDocument.Load(OutputStream);
                var RespElement = DataAccess.RenterDataAccess.GetRootElement(XmlResp.Root, "Wallet_GetPaymentMethodsResponse");
                string Msg = "";
                foreach (XElement Response in RespElement.Elements())
                {
                    string node = Response.Name.LocalName;

                    if (node == "Message")
                    {
                        Msg = Response.Value.ToString();
                        if (Msg == "Session timed out")
                        {
                            FormsAuthentication.SignOut();
                            HttpContext.Current.Response.Redirect("~/ResidentLogin.aspx");
                        }

                        if (Msg.Contains("Error reading database"))
                        {
                            string Error = "";
                        }

                        if(Msg == "No payment methods found.")
                        {
                            WalletGrid.DataBind();
                            return;
                        }

                    }

                    string _Resp = Response.ToString();
                    if (node == "Wallet_PaymentMethods")
                    {
                        XmlTextReader reader = new XmlTextReader(new StringReader(_Resp));
                        DataSet dataSet = new DataSet();
                        dataSet.ReadXml(reader);

                        WalletGrid.DataSource = dataSet.Tables[0];
                        WalletGrid.DataBind();

                    }

                }

            }

        }

        protected void WalletGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                //DescTypelbl
                Label PayMethodLabel = (Label)e.Row.FindControl("PaymentMethodlbl") as Label;
                Label DescTypeLabel = (Label)e.Row.FindControl("DescTypelbl") as Label;
                Label Exp_Date = (Label)e.Row.FindControl("EXP_Datelbl") as Label;
                Label ExpDateLabel = (Label)e.Row.FindControl("ExpDatelbl") as Label;
                Label PayDesc = (Label)e.Row.FindControl("Label4") as Label;
                Label AutoSet = (Label)e.Row.FindControl("autosetlbl") as Label;

                Button usebtn = (Button)e.Row.FindControl("Button1") as Button;
                Button edit = (Button)e.Row.FindControl("EditWalletItembtn") as Button;
                Button AutoPaySet = (Button)e.Row.FindControl("AutoPaySetbtn") as Button;

                Button AutoPayCancel = (Button)e.Row.FindControl("CancelAutoPaybtn") as Button;

                CheckBox MultiWalletCkBx = (CheckBox)e.Row.FindControl("UseWalletItemCkbx") as CheckBox;
                Button SelectBtn = (Button)e.Row.FindControl("SelectWalletitemBtn") as Button;

                TextBox CvvTextBox = (TextBox)e.Row.FindControl("CVVtxt") as TextBox;

                string AutoLabel = AutoSet.Text;
                if(AutoLabel == "true")
                {
                    AutoPaySet.Enabled = false;
                    AutoPaySet.Visible = false;
                    AutoPayCancel.Visible = true;
                    
                }
                else
                {
                    AutoPaySet.Enabled = true;
                    AutoPayCancel.Visible = false;
                }


                if (PayMethodLabel.Text == "false")
                {
                    DescTypeLabel.Text = "E-Check";

                }
                else
                {
                    ExpDateLabel.Text = "Exp " + Exp_Date.Text;
                    DescTypeLabel.Text = "Credit Card";
                }

                bool ChecknoPay = PayDesc.Text.Contains("No payment methods found.");

            }
        }

        protected void AddNewEcheckbtn_Click(object sender, EventArgs e)
        {
            AddnewWalletItemSection.Visible = true;
            WalletGrid.SelectedIndex = -1;
            EcheckADD.Visible = true;
            CreditCardAdd.Visible = false;
            AddNewEcheckandPaybtn.Text = "Submit New Bank Account";
        }

        protected void AddNewCCbtn_Click(object sender, EventArgs e)
        {
            AddnewWalletItemSection.Visible = true;
            WalletGrid.SelectedIndex = -1;
            EcheckADD.Visible = false;
            CreditCardAdd.Visible = true;
            AddNewCCandPaybtn.Text = "Submit New Credit Card";
        }

        protected void WalletGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string RenterSessionCookie = Request.Cookies["RenterSession"]["Session"];
            string RenterIDCookie = Request.Cookies["RenterID"]["ID"];

            string _payerID = "";
            string XmlResp = "";
            string CommandType = "";
            string WalletID = "";
            string _method = "";
            string _methodID = "";
            string[] arg = new string[4];
            arg = e.CommandArgument.ToString().Split(';');
            CommandType = arg[0];
            _method = arg[1];
            WalletID = arg[2];
            _payerID = arg[2];
            _methodID = arg[3];
            if(CommandType == "Edit")
            {
                if (_method == "true")
                {
                    AddnewWalletItemSection.Visible = true;
                    CreditCardAdd.Visible = true;
                    EcheckADD.Visible = false;
                    CCHeaderlbl.Text = "Edit Credit Card";
                    AddNewCCandPaybtn.Text = "Submit Changes";

                }
                if (_method == "false")
                {
                    AddnewWalletItemSection.Visible = true;
                    CreditCardAdd.Visible = false;
                    EcheckADD.Visible = true;
                    EcheckHeaderlbl.Text = "Edit Bank Account";
                    AddNewEcheckandPaybtn.Text = "Submit Changes";
                }

            }


            if (CommandType == "Delete")
            {
                XmlResp = DataAccess.Wallet.RemovePaymentMethodForRenter(RenterSessionCookie, RenterIDCookie, _methodID, _method);

                var WR = DataAccess.Wallet.WalletReadResponse("Wallet_RemovePaymentMethodResponse", XmlResp);

                string WalletItems = DataAccess.Wallet.GetPaymentMethodsForRenter(RenterSessionCookie, RenterIDCookie);
                BindWalletGrid(WalletItems);
               
                //TODO:
                //Add message for when Wallet itme has been removed

            }

            if(CommandType == "SetAutoPayment")
            {
                UpdateAutoPaymentArea.Visible = true;
                AddnewWalletItemSection.Visible = false;
                
            }

            if(CommandType == "CancelAutoPayment")
            { 
                int RenterID = int.Parse(RenterIDCookie);
                DataAccess.Wallet.AutoPay_Cancel(RenterID);
                string WalletItems = DataAccess.Wallet.GetPaymentMethodsForRenter(RenterSessionCookie, RenterIDCookie);
                BindWalletGrid(WalletItems);
            }



        }

        protected void AddEcheckCancel_Click(object sender, EventArgs e)
        {
            WalletGrid.SelectedIndex = -1;
            AddnewWalletItemSection.Visible = false;
            CreditCardAdd.Visible = false;
            EcheckADD.Visible = false;
        }

        protected void AddccCancel_Click(object sender, EventArgs e)
        {
            WalletGrid.SelectedIndex = -1;
            AddnewWalletItemSection.Visible = false;
            CreditCardAdd.Visible = false;
            EcheckADD.Visible = false;
        }

        protected void AddNewEcheckandPaybtn_Click(object sender, EventArgs e)
        {
            string XmlResp = "";

            int GridIndex = WalletGrid.SelectedIndex;
            string AccountNumber = NewAccountnumbertxt.Text;
            string RoutingNumber = NewRoutingNumbertxt.Text;
            string AccountType = BankAccountTypeDDL.SelectedValue;
            string RenterSessionCookie = Request.Cookies["RenterSession"]["Session"];
            string RenterIDCookie = Request.Cookies["RenterID"]["ID"];
            string WalletID = "";

            //Edit Wallet item
            if(GridIndex > -1)
            {
                WalletID = WalletGrid.SelectedDataKey[2].ToString();
                //Wallet_UpdateACHPaymentMethodResponse
                XmlResp = DataAccess.Wallet.UpdateACHPaymentMethodForRenter(RenterSessionCookie, RenterIDCookie, WalletID, AccountType, AccountNumber, RoutingNumber);

                var WalletResp = DataAccess.Wallet.WalletReadResponse("Wallet_UpdateCCPaymentMethodResponse", XmlResp);

                string WalletItems = DataAccess.Wallet.GetPaymentMethodsForRenter(RenterSessionCookie, RenterIDCookie);
                BindWalletGrid(WalletItems);

            }
            //Add New Wallet Item
            if(GridIndex == -1)
            {
                //Wallet_AddACHPaymentMethodResponse
                XmlResp = DataAccess.Wallet.AddACHPaymentMethodForRenter(RenterSessionCookie, RenterIDCookie, AccountType, AccountNumber, RoutingNumber);

                string WalletItems = DataAccess.Wallet.GetPaymentMethodsForRenter(RenterSessionCookie, RenterIDCookie);
                BindWalletGrid(WalletItems);
            }

        }

        protected void AddNewCCandPaybtn_Click(object sender, EventArgs e)
        {

            string XmlResp = "";
            int GridIndex = WalletGrid.SelectedIndex;
            string FullName = FullNametxt.Text;
            string AccountNumber = CardAccountnumtxt.Text;
            string ExpMonth = ExpMonthDDL.SelectedValue;
            string ExpYear = ExpYearDDL.SelectedValue;
            string CSC = CVVtxt.Text;
            string RenterSessionCookie = Request.Cookies["RenterSession"]["Session"];
            string RenterIDCookie = Request.Cookies["RenterID"]["ID"];
            string WalletID = "";
            string PaymentMethodID = "";

            //Edit Wallet item
            if (GridIndex > -1)
            {
                PaymentMethodID = WalletGrid.SelectedDataKey[2].ToString();
                XmlResp = DataAccess.Wallet.UpdateCCPaymentMethodForRenter(RenterSessionCookie, RenterIDCookie, PaymentMethodID, FullName, AccountNumber, ExpMonth, ExpYear, CSC);
                var WalletResp = DataAccess.Wallet.WalletReadResponse("Wallet_UpdateCCPaymentMethodResponse", XmlResp);

                string WalletItems = DataAccess.Wallet.GetPaymentMethodsForRenter(RenterSessionCookie, RenterIDCookie);
                BindWalletGrid(WalletItems);

                if (WalletResp.Result == "Success")
                {
                    ClearAddUpdateCreditCard();
                }
                
            }
            //Add New Wallet Item
            if (GridIndex == -1)
            {
                XmlResp = DataAccess.Wallet.AddCCPaymentMethodForRenter(RenterSessionCookie, RenterIDCookie, FullName, AccountNumber, ExpMonth, ExpYear, CSC);
                var WalletResp = DataAccess.Wallet.WalletReadResponse("Wallet_AddCCPaymentMethodResponse", XmlResp);

                string WalletItems = DataAccess.Wallet.GetPaymentMethodsForRenter(RenterSessionCookie, RenterIDCookie);
                BindWalletGrid(WalletItems);

                if (WalletResp.Result == "Success")
                {
                    ClearAddUpdateCreditCard();
                }
            }
        }

        private void ClearAddUpdateCreditCard()
        {
            FullNametxt.Text = string.Empty;
            CardAccountnumtxt.Text = string.Empty;
            CVVtxt.Text = string.Empty;
            AddnewWalletItemSection.Visible = false;
            WalletGrid.SelectedIndex = -1;
        }

        private void ClearAddUpdateACH()
        {
            AddnewWalletItemSection.Visible = false;
            NewRoutingNumbertxt.Text = string.Empty;
            NewAccountnumbertxt.Text = string.Empty;
            WalletGrid.SelectedIndex = -1;
        }

        protected void CancelAutoPay_Click(object sender, EventArgs e)
        {
            UpdateAutoPaymentArea.Visible = false;
            WalletGrid.SelectedIndex = -1;
        }

        protected void SubmitAutoPay_Click(object sender, EventArgs e)
        {
            int PaymentTypeID = 0;
            string RenterSessionCookie = Request.Cookies["RenterSession"]["Session"];
            string RenterIDCookie = Request.Cookies["RenterID"]["ID"];
            
            int PayDayOfMonth = int.Parse(DayofMonthDDL.SelectedValue);

            int RenterID = int.Parse(RenterIDCookie);
            //PaymentMethod true = credit card false = check
            bool PaymentMethod = bool.Parse(WalletGrid.SelectedDataKey[1].ToString());
            int _methodID = int.Parse(WalletGrid.SelectedDataKey[2].ToString());
            int _payerID = int.Parse(WalletGrid.SelectedDataKey[0].ToString());

            if (PaymentMethod == true)
            {
                PaymentTypeID = 1;
            }
            if (PaymentMethod == false)
            {
                PaymentTypeID = 2;
            }

            int PayerID = _payerID;

            try
            {
                DataAccess.Wallet.AutoPayment_InsertUpdate(RenterID, PayerID, PaymentTypeID, _methodID, PayDayOfMonth);
                UpdateAutoPaymentArea.Visible = false;
                string WalletItems = DataAccess.Wallet.GetPaymentMethodsForRenter(RenterSessionCookie, RenterIDCookie);
                BindWalletGrid(WalletItems);
                
            }
            catch(Exception ex)
            {
                throw ex;
            }


        }



    }
}