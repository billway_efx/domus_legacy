﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SecurePages/DomusMaster.Master" AutoEventWireup="true" CodeBehind="MaintenanceRequest.aspx.cs" Inherits="EfxPublic.SecurePages.MaintenanceRequest" %>

<%@ Register Src="~/UserControls/Covid.ascx" TagPrefix="uc1" TagName="Covid" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>



    <section>
        <div class="container">

        </div>


    </section>

        <section>
        <div class="container">
           <div class="col-lg-6">
                <h3>Recent Maintenance Request</h3>
                <div class="well col-lg-12">
                    <asp:GridView ID="MaintenanceGrid" CssClass="table table-responsive" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="MaintenanceSQL" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="RequestDate" DataFormatString="{0:d}" HeaderText="Date" ReadOnly="True" SortExpression="RequestDate" />
                            <asp:BoundField DataField="RequestTyp" HeaderText="Type" SortExpression="RequestTyp" />
                            <asp:BoundField DataField="RequestDesc" HeaderText="Description" SortExpression="RequestDesc" />
                        </Columns>
                        <EmptyDataTemplate>
                            <div>
                                <h4>You have no requests</h4>
                            </div>
                        </EmptyDataTemplate>
                    </asp:GridView>

                    <asp:SqlDataSource ID="MaintenanceSQL" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="usp_Renter_GetMaintenanceRequestListBYRenterID" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:Parameter Name="RenterID" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </div>
            <br/>
            <br/>
            <uc1:Covid runat="server" id="Covid" />
            <div class="col-lg-6">
                  <h3>Submit New Maintenance Request</h3>
                <div class="col-lg-12">
                    <label>
                    Maintenance Type
                </label>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="MaintenanceTypeDDL" ValidationGroup="MaintenanceGrp" InitialValue="0">
                                 <i class="fa fa-warning validationStyle"></i>
                             </asp:RequiredFieldValidator>
                    <asp:DropDownList ID="MaintenanceTypeDDL" CssClass="form-control" runat="server" DataSourceID="SqlDataSource1" DataTextField="MaintenanceRequestTypeName" DataValueField="MaintenanceRequestTypeId" AppendDataBoundItems="True">
                        <asp:ListItem Value="0">Select Type</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="usp_MaintenanceRequest_GetRequestTypeList" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                </div>
                 <div class="col-lg-12">
                    <label>
                    Priority
                </label>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="PriorityDDL" ValidationGroup="MaintenanceGrp" InitialValue="0">
                                 <i class="fa fa-warning validationStyle"></i>
                             </asp:RequiredFieldValidator>
                    <asp:DropDownList ID="PriorityDDL" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="MaintenanceRequestPriorityName" DataValueField="MaintenanceRequestPriorityId" AppendDataBoundItems="True">
                        <asp:ListItem Value="0">Select Priority</asp:ListItem>
                     </asp:DropDownList>
                     <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="usp_MaintenanceRequest_GetPriorityList" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                </div>
                 <div class="col-lg-12">
                    <label>
                    Description
                </label>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="Desctxt" ValidationGroup="MaintenanceGrp">
                                 <i class="fa fa-warning validationStyle"></i>
                             </asp:RequiredFieldValidator>
                     <asp:TextBox ID="Desctxt" CssClass="form-control" runat="server" Rows="6" TextMode="MultiLine"></asp:TextBox>
                </div>
                <div class="col-lg-12">
                    <label>
                    Unit Number
                </label>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="UnitNumtxt" ValidationGroup="MaintenanceGrp">
                                 <i class="fa fa-warning validationStyle"></i>
                             </asp:RequiredFieldValidator>
                     <asp:TextBox ID="UnitNumtxt" CssClass="form-control" runat="server"></asp:TextBox>
                </div>

                  <div class="col-lg-12">
                    <label>
                    Permission to Enter
                </label>
                      <asp:DropDownList ID="PermissionDDL" CssClass="form-control" runat="server">
                          <asp:ListItem Value="Yes">Yes</asp:ListItem>
                          <asp:ListItem Value="No">No</asp:ListItem>
                      </asp:DropDownList>
                </div>
                <div class="col-lg-12">
                    <asp:Button ID="SubmitRequestbtn" runat="server" CssClass="btn btn-primary" Text="Submit" ValidationGroup="MaintenanceGrp" OnClick="SubmitRequestbtn_Click" />
                </div>
                <div class="col-lg-12">
                    <asp:Label ID="Statuslbl" runat="server"></asp:Label>
                </div>
                <div class="col-lg-12">
                    <p>****Please Note: <strong style="color:red">Do not use for emergencies!</strong> 
                        - If there is an after hours maintenance emergency, please call the property office immediatly.  
                        If this is a life threatening or health related emergency, please dial 911.</p>
                </div>
            </div>


        </div>


    </section>



            </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
