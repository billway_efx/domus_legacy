﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RenterTopNavMenu.ascx.cs" Inherits="EfxPublic.SecurePages.UserControls.RenterTopNavMenu" %>



    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" style="background-color:#662E6B;">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/SecurePages/Account.aspx">
                <img src="../img/RPO-01-01_Logo_Vertical_white.png" style="max-height: 45px;" />
            </a>
        </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right text-shadow">
                <li class="hidden">
                    <%-- <a href="#page-top"></a>--%>
                    </li>
                <li>
                    <a href="Account.aspx">Account</a>
                </li>

                <li>
                    <a href="ResidentPayment.aspx">Payment</a>
                </li>
                <li>
                    <a href="ResidentWallet.aspx">Wallet</a>

                </li>
                <li>
                    <a href="ResidentCalendar.aspx">Calendar</a>
                </li>
                <li id="MaintenanceLI" runat="server">
                    <a href="MaintenanceRequest.aspx">Maintenance</a>
                </li>
                <li>
                    <a href="Contact.aspx">Contact</a>
                </li>


                <li>
                    <asp:Button ID="Button1" CssClass="btn btn-sm btn-primary" runat="server" Text="Logout" OnClick="Button1_Click" />
                </li>
            </ul>



        </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>