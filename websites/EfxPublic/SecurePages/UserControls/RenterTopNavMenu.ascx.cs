﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace EfxPublic.SecurePages.UserControls
{
    public partial class RenterTopNavMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string _user = Page.User.Identity.Name.ToString();
            var r = DataAccess.RenterDataAccess.GetRenterDetailsByUserName(_user);

            if(r.CanViewMaintenance == false)
            {
                MaintenanceLI.Visible = false;
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            HttpCookie _Renter = new HttpCookie("RenterID");
            HttpCookie _Session = new HttpCookie("RenterSession");
            _Renter.Expires = DateTime.Now.AddMinutes(-30);
            _Session.Expires = DateTime.Now.AddMinutes(-30);
            FormsAuthentication.SignOut();
            Response.Redirect("/ResidentLogin.aspx");
        }
    }
}