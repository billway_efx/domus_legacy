﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RenterFooter.ascx.cs" Inherits="EfxPublic.SecurePages.UserControls.RenterFooter" %>


   <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Domusme.com 2016 </span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                       <%-- <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>--%>

                <img class="img-responsive img-centered" src="/img/Flux/smarterPayments.png" width="300" />

                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="/PrivacyPolicy.aspx">Privacy Policy</a>
                        </li>
                        <li><a href="/SecurePages/TermsAndConditions.aspx">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>