﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SecurePages/DomusMaster.Master" AutoEventWireup="true" CodeBehind="PaymentConfirmation.aspx.cs" Inherits="EfxPublic.SecurePages.PaymentConfirmation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <section>
        <div class="container">
        </div>
    </section>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>


             <section class="text-center">
                <div class="container">
                    <div class="col-lg-12">
                        <div class="col-lg-6 center text-center well">
                        <h4> 
                            Payment Sucessful
                        </h4>
                      
                        </div>

                        <div class="col-lg-6 center">
                            <p>
                                Thank You! Your rent payment has been received by Domus. Please allow time for processing. As a reminder, answers to your most frequently asked questions can be found by visiting our Knowledge Base at <strong><a href="http://support.rentpaidonline.com">Support.RentPaidOnline.com</a> </strong> 
                            </p>
                            <div class="col-lg-12">
                                 <table class="table table-responsive">
                                <tr>
                                    <td class="text-left">
                                        <label>Payment Amount:</label>
                                    </td>
                                    <td>
                                        <asp:Label ID="PaymentAmountlbl" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-left">
                                        <label>Convenience Fee:</label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ConvenienceFeelbl" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                
                                     <tr>
                                    <td class="text-left">
                                        <label>Total Payment:</label>
                                    </td>
                                    <td>
                                        <asp:Label ID="totalPaymentlbl" runat="server"></asp:Label>
                                    </td>
                                </tr>

                                        <tr>
                                    <td class="text-left">
                                        <label>Remaining Balance:</label>
                                    </td>
                                    <td>
                                        <asp:Label ID="RemainBalance" runat="server"></asp:Label>
                                    </td>
                                </tr>


                                <tr>
                                    <td class="text-left">
                                           <label>Date:</label>
                                    </td>
                                    <td>
                                         <asp:Label ID="paymentDatelbl" runat="server"></asp:Label>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="text-left">
                                           <label>Transaction ID Number:</label>
                                    </td>
                                    <td>
                                        <asp:Label ID="TransactionIDlbl" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                                <asp:Button ID="PayRemainingBalancebtn" runat="server" CssClass="btn btn-primary" Text="Pay Remaining Balance" OnClick="PayRemainingBalancebtn_Click" /> &nbsp;
                                <asp:Button ID="Continuebtn" CssClass="btn btn-primary" runat="server" Text="Continue to Account Page" OnClick="Continuebtn_Click" />

                            </div>
                           


                        </div>




                    </div>
                    


                    </div>
                 </section>

            </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>
