﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SecurePages/DomusMaster.Master" AutoEventWireup="true" CodeBehind="ResidentPayment.aspx.cs" Inherits="EfxPublic.SecurePages.ResidentPayment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=19.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
      <ContentTemplate>
         <asp:Panel ID="ModalPanel" runat="server" Width="500px" BackColor="Purple" BorderColor="Green" ForeColor="White">
            <br />
            <div align="center" width="480px">
               <img src="../img/Header_domus.png" alt="DOMUS"/>
            <br />
               <p style="font-family: Verdana; font-size: x-large; font-weight: bold; color: #00FF00">
                  is processing your payment<br />
                  Please be patient
               </p>
               <br />
            </div>
            <br />
            <ajaxToolkit:ModalPopupExtender ID="mpe" runat="server" TargetControlID="ModalPanel"
               PopupControlID="ModalPanel" />
            <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtender1" runat="server" Radius="20"
               TargetControlID="ModalPanel" />
            <ajaxToolkit:DropShadowExtender ID="DropShadowExtender1" runat="server" Radius="20" Opacity="25"
               TrackPosition="True" TargetControlID="ModalPanel" Width="20" />
         </asp:Panel>
         <!--Resident Details Section -->
         <section>


                <div class="container">

                    <div class="col-lg-12" style="padding: 15px; border: solid 1px #c2bebe;">
                        <div class="col-lg-3">

                            <h4>Renter Details</h4>
                            <asp:Label ID="RenterFullNamelbl" runat="server"></asp:Label><br />
                            <asp:Label ID="RenterAddresslbl" runat="server"></asp:Label><br />
                            <asp:Label ID="RenterStatelbl" runat="server"></asp:Label><br />
                            <asp:Label ID="RenterEmailAddresslbl" runat="server"></asp:Label>

                        </div>


                        <div class="col-lg-6">

                            <asp:GridView ID="GridView1" CssClass="table table-responsive" runat="server" DataKeyNames="PayableItemId,RenterId" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="PayableItemsId" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="PayableItemIdlbl" runat="server" Text='<%# Bind("PayableItemId") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="RenterID" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="RenterIDlbl" runat="server" Text='<%# Bind("RenterId") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:BoundField DataField="LeaseId" HeaderText="LeaseId" Visible="False" />
                                    <asp:BoundField DataField="PropertyId" HeaderText="PropertyId" Visible="False" />

                                    <asp:TemplateField HeaderText="Due Date">
                                        <ItemTemplate>
                                            <asp:Label ID="PayableDueDatelbl" runat="server" Text='<%# string.Format("{0:d}",  Convert.ToDateTime(Eval("PayableDueDate"))) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Editable" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Editable") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="EditableLBL" runat="server" Text='<%# Bind("Editable") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rent Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="RentAmountlbl" runat="server" Text='<%# string.Format("{0:c}", Convert.ToDecimal(Eval("RentAmount"))) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Fee Total">
                                        <ItemTemplate>
                                            <asp:Label ID="FeeTotallbl" runat="server" Text='<%# string.Format("{0:c}", Convert.ToDecimal(Eval("FeeTotal"))) %>'></asp:Label>
                                            <asp:LinkButton ID="FeeDetailbtn" runat="server" Text="Detail" CommandName="Select" CommandArgument='<%# Eval("PayableItemId") %>'></asp:LinkButton>
                                            <%-- <asp:Button ID="FeeDetailbtn" runat="server" CssClass="btn btn-sm" CommandName="Select" CommandArgument='<%# Eval("PayableItemId") %>' Text="Detail" />--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Balance Due">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("CurrentBalanceDue") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="currentBalanceDuelbl" runat="server" Text='<%# string.Format("{0:c}", Convert.ToDecimal(Eval("CurrentBalanceDue"))) %>' ToolTip='<%# Bind("PayMessage") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="desclbl" runat="server" Text="Rent and fees" ToolTip='<%# Bind("Description") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Select" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PayChoose") %>' ToolTip='<%# Bind("PayMessage") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBox1" runat="server" ToolTip='<%# Bind("PayMessage") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PartialPay" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="AllowPartialPaylbl" runat="server" Text='<%# Bind("AllowPartialPayments") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PayLimit" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="PayLimitlbl" runat="server" Text='<%# Bind("PayLimit") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AcceptedPaymentType" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="AcceptedPaymentTypelbl" runat="server" Text='<%# Bind("AcceptedPaymentTypeID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>

                            </asp:GridView>

                            <asp:SqlDataSource ID="PaymentSQL" runat="server"></asp:SqlDataSource>

                        </div>

                        <div class="col-lg-6" id="FeeListArea" runat="server" visible="false">
                            <h4>Fee List</h4>
                            <asp:GridView ID="FeeDetailGrid" CssClass="table table-responsive" runat="server" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="FeeName" HeaderText="Fee Name" />
                                    <asp:BoundField DataField="FeeAmount" DataFormatString="{0:c}" HeaderText="Amount" />
                                </Columns>

                                <EmptyDataTemplate>
                                    <div class="col-lg-6">
                                        <h3>No Fees Exist
                                        </h3>

                                    </div>

                                </EmptyDataTemplate>

                            </asp:GridView>
                            <asp:Button ID="CloseFeeListBtn" runat="server" CssClass="btn btn-primary" Text="Close Fee List" OnClick="CloseFeeListBtn_Click" />
                        </div>

                    </div>
                </div>
            </section>
         <!--End Resident Details Section -->

         <section id="WalletGridSection" runat="server">
                <div class="container">

                    <div class="col-lg-12" style="padding: 15px; border: solid 1px #c2bebe;">
                        <asp:ValidationSummary ID="ApplicationSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="Payment" />
                        <div class="col-lg-3">
                            <div class="col-lg-4" id="paybuttonArea" runat="server">
                                <div class="col-lg-8">
                                    <asp:Button ID="PaySingleItemNewCreditCardbtn" runat="server" CssClass="btn btn-sm" Text="Pay with New Credit Card" OnClick="PaySingleItemNewCreditCardbtn_Click" BackColor="#BADC02" Font-Bold="True" />

                                </div>
                                <div class="col-lg-8">
                                    <asp:Button ID="PaySingleItemNewBankAcctbtn" runat="server" CssClass="btn btn-sm" Text="Pay with New Bank Acount" OnClick="PaySingleItemNewBankAcctbtn_Click" />
                                </div>
                                <div class="col-lg-8">
                                    <asp:Button ID="PayWithCashbtn" CssClass="btn btn-sm" runat="server" Text="RentByCash" OnClick="PayWithCashbtn_Click" />
                                </div>
                                <div class="col-lg-8">
                                    <asp:Button ID="CancelPaybtn" CssClass="btn btn-primary" runat="server" Text="Cancel" OnClick="CancelPaybtn_Click" Visible="false" />
                                </div>
                            </div>

                            <asp:HiddenField ID="PaymethodTypeHiddenField" runat="server" />
                        </div>

                        <div class="col-lg-6">
                            <%-- Not using this control or the code that goes with it - it will be used at a later time --%>
                            <asp:DropDownList ID="SelectMultiplePaymentOptionDDL" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="SelectMultiplePaymentOptionDDL_SelectedIndexChanged" Visible="false">
                                <asp:ListItem Value="false">Pay with Single Wallet item</asp:ListItem>
                                <asp:ListItem Value="true">Pay with multiple wallet items</asp:ListItem>
                                <asp:ListItem Value="NewCC">Pay with New Credit Card</asp:ListItem>
                                <asp:ListItem Value="NewEcheck">Pay with New Bank Acount</asp:ListItem>
                            </asp:DropDownList>
                        </div>

                        <div class="col-lg-4">


                            <asp:GridView ID="GridView2" CssClass="table table-responsive" runat="server" DataKeyNames="PayerId,PaymentMethod,ProgramId,PayerWalletId" AutoGenerateColumns="False" OnRowDataBound="GridView2_RowDataBound" OnSelectedIndexChanged="GridView2_SelectedIndexChanged">

                                <Columns>
                                    <asp:BoundField DataField="AutoSet" HeaderText="AutoSet" Visible="False" />
                                    <asp:TemplateField HeaderText="Wallet Item">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="DescTypelbl" runat="server" Text="Label"></asp:Label><br />
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("Description") %>'></asp:Label>&nbsp;
                            <asp:Label ID="ExpDatelbl" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Exp Date" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("EXP_Date") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="EXP_Datelbl" runat="server" Text='<%# Bind("EXP_Date") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Editable" HeaderText="Editable" Visible="False" />
                                    <asp:BoundField DataField="IsPrimary" HeaderText="IsPrimary" Visible="False" />
                                    <asp:BoundField DataField="PayChoose" HeaderText="PayChoose" Visible="False" />
                                    <asp:TemplateField HeaderText="PayerId" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PayerId") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("PayerId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PayerMethodId" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("PayerMethodId") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("PayerMethodId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PayerWalletID" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("PayerWalletId") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="PayerWalletIDlbl" runat="server" Text='<%# Bind("PayerWalletId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="PayerWalletId" HeaderText="PayerWalletId" Visible="false" />
                                    <asp:TemplateField HeaderText="PaymentMethod" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="PaymentMethodtxt" runat="server" Text='<%# Bind("PaymentMethod") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="PaymentMethodlbl" runat="server" Text='<%# Bind("PaymentMethod") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="RequestSecurityCode" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("RequestSecurityCode") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("RequestSecurityCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ProgramID" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="ProgramIDlbl" runat="server" Text='<%# Bind("ProgramId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Select for Payment" Visible="true">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="UseWalletItemCkbx" CausesValidation="false" CssClass="form-control" runat="server" AutoPostBack="True" OnCheckedChanged="UseWalletItemCkbx_CheckedChanged" />
                                            <asp:Button ID="SelectWalletitemBtn" CssClass="btn btn-primary" CommandName="Select" runat="server" Text="Use" OnClick="SelectWalletitemBtn_Click" />:                                     
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Enter Amount to Pay" Visible="true">
                                        <ItemTemplate>
                                            <div class="col-lg-6">
                                                <asp:TextBox ID="AmountToPaytxt" CssClass="form-control" runat="server"></asp:TextBox>

                                                <cc1:TextBoxWatermarkExtender ID="AmountToPaytxt_TextBoxWatermarkExtender" WatermarkText="$0.00" runat="server" BehaviorID="AmountToPaytxt_TextBoxWatermarkExtender" TargetControlID="AmountToPaytxt" />
                                            </div>
                                            <div class="col-lg-6">
                                                <asp:TextBox ID="CVVtxt" CssClass="form-control" runat="server" MaxLength="4"></asp:TextBox>

                                                <cc1:TextBoxWatermarkExtender ID="CVVtxt_TextBoxWatermarkExtender" runat="server" WatermarkText="CVV Code" BehaviorID="CVVtxt_TextBoxWatermarkExtender" TargetControlID="CVVtxt" />

                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <EmptyDataTemplate>
                                    <div class="col-lg-6">
                                        <h3>No Payment Methods found</h3>
                                        <h4>Select Option above to pay with new credit card or new E-Check
                                        </h4>

                                    </div>

                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#CCCCCC" />

                            </asp:GridView>
                        </div>

                        <!--Pay Details -->
                        <div class="col-lg-4">
                            <div class="col-lg-6">
                                <asp:Label ID="Label5" runat="server" Text="Balance Due:" Font-Bold="True"></asp:Label>
                            </div>
                            <div class="col-lg-4">
                                <asp:Label ID="CurrentBalanceDuelbl" runat="server" Text="$0.00"></asp:Label>
                            </div>

                            <!-- Amount Area -->
                            <div class="col-lg-12" id="AmountArea" runat="server" visible="false">

                                <%-- ConvFee --%>
                                <div class="col-lg-6">
                                    <asp:Label ID="Label6" runat="server" Text="Convenience Fee:" Font-Bold="True"></asp:Label>
                                </div>
                                <div class="col-lg-6">
                                    <asp:Label ID="ConvFeeTotallbl" runat="server" Text="$0.00"></asp:Label>
                                </div>

                                <div class="col-lg-6">
                                    <asp:Label ID="Label9" runat="server" Font-Bold="true" Text="Rent Being Paid"></asp:Label>
                                    <asp:CustomValidator ID="AmountValidator" runat="server" ErrorMessage="CustomValidator" OnServerValidate="AmountValidator_ServerValidate"><i class="fa fa-warning validationStyle"></i></asp:CustomValidator>
                                </div>
                                <div class="col-lg-3">
                                    <asp:TextBox ID="AmountBeingPaidtxt" CssClass="form-control input-sm" runat="server" MaxLength="7"></asp:TextBox>
                                </div>
                                <div class="col-lg-3">
                                    <asp:Button ID="UpdatetotalBtn" CssClass="btn btn-sm" runat="server" Text="Update" OnClick="UpdatetotalBtn_Click" />
                                </div>
                                <%-- Total AMount --%>
                                <div class="col-lg-6">
                                    <asp:Label ID="Label7" runat="server" Text="Total Amount:" Font-Bold="True"></asp:Label>

                                </div>
                                <div class="col-lg-4">
                                    <asp:Label ID="TotalAmountlbl" runat="server" Text="$0.00"></asp:Label>
                                </div>


                                <div class="col-lg-12" id="CVVArea" runat="server">

                                    <div class="col-lg-6">
                                        <asp:Label ID="CSClbl" runat="server" Text="Card Security Code" Visible="False" Font-Bold="True"></asp:Label>
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator" ValidationGroup="SinglePayment" OnServerValidate="CustomValidator1_ServerValidate"><i class="fa fa-warning validationStyle"></i></asp:CustomValidator>
                                    </div>
                                    <div class="col-lg-3">
                                        <asp:TextBox ID="CSCtxt" CssClass="form-control input-sm" runat="server" MaxLength="4" Visible="false"></asp:TextBox>
                                    </div>

                                </div>


                            </div>
                            <!-- SWitherspoon: Changed from col-lg-8 to 9 to fit elements better -->
                            <div class="col-lg-9" style="margin-top: 10px;">
                                <asp:CheckBox ID="TermsCheckbox" CssClass="form-control" runat="server" AutoPostBack="True" Text="Accept Terms of Agreement" Visible="false" />
                                <asp:HyperLink runat="server" ID="AgreementLink" Text="Click Here to Terms of Agreement" NavigateUrl="~/SecurePages/TermsAndConditions.html" Visible="false" Target="_blank" Font-Bold="true"></asp:HyperLink>
                            </div>
                            <div class="col-lg-6" style="margin-top: 10px;">
                                <asp:Button ID="PaywithSinglePayMethodBtn" CssClass="btn btn-primary" runat="server" Text="Single" ValidationGroup="SinglePayment" Visible="false" OnClick="PaywithSinglePayMethodBtn_Click" />

                                <asp:Button ID="PaywithMultiplePayMethodbtn" CssClass="btn btn-primary" Visible="false" runat="server" ValidationGroup="MultiPayment" Text="Multi" OnClick="PaywithMultiplePayMethodbtn_Click" />

                            </div>
                            <div class="col-lg-6" id="PaymentErrorArea" runat="server" visible="false">
                                <asp:Label ID="PaymentErrorlbl" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                            </div>

                            <!-- ENd Amount Area -->

                        </div>
                        <!--End Pay Details -->

                        <!--Add New Wallet Item -->
                        <div class="col-lg-8" id="AddnewWalletItemSection" runat="server" visible="false">

                            <!--Add New ECheck -->
                            <div class="col-lg-8 text-left" id="EcheckADD" runat="server" visible="false">
                                <h4>
                                    <asp:Label ID="EcheckHeaderlbl" runat="server" Text="Add New E-Check and Submit Payment"></asp:Label></h4>

                                <div class="col-lg-10">
                                    <label>Account Type</label>
                                    <asp:DropDownList ID="BankAccountTypeDDL" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="1">Checking Account</asp:ListItem>
                                        <asp:ListItem Value="2">Savings Account</asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div class="col-lg-10">
                                    <label>Routing Number</label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="NewRoutingNumbertxt" ValidationGroup="EcheckADD"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="RegularExpressionValidator" ValidationExpression="^[0-9]*$" ControlToValidate="NewRoutingNumbertxt" ValidationGroup="EcheckADD"><i class="fa fa-warning validationStyle"></i></asp:RegularExpressionValidator>
                                    <asp:TextBox ID="NewRoutingNumbertxt" CssClass="form-control" ValidationGroup="EcheckADD" runat="server" MaxLength="17"></asp:TextBox>
                                </div>
                                <div class="col-lg-10">
                                    <label>Account Number</label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="NewAccountnumbertxt" ValidationGroup="EcheckADD"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="RegularExpressionValidator" ValidationExpression="^[0-9]*$" ControlToValidate="NewAccountnumbertxt" ValidationGroup="EcheckADD"><i class="fa fa-warning validationStyle"></i></asp:RegularExpressionValidator>
                                    <asp:TextBox ID="NewAccountnumbertxt" CssClass="form-control" runat="server" MaxLength="30"></asp:TextBox>
                                </div>

                            </div>
                            <!--End Add New ECheck -->

                            <!--Add New CC -->
                            <div class="col-lg-8" id="CreditCardAdd" runat="server" visible="false">
                                <h4>
                                    <asp:Label ID="CCHeaderlbl" runat="server" Text="Add New Credit Card and Submit Payment"></asp:Label>
                                </h4>
                                <div class="col-lg-12">
                                    <label>Name on Card</label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="FullNametxt" ValidationGroup="CreditCardAdd"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>

                                    <asp:TextBox ID="FullNametxt" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-12">
                                    <label>Card Number</label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="CardAccountnumtxt" ValidationGroup="CreditCardAdd" ErrorMessage="RequiredFieldValidator"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="RegularExpressionValidator" ValidationExpression="^[0-9]*$" ControlToValidate="CardAccountnumtxt" ValidationGroup="CreditCardAdd"><i class="fa fa-warning validationStyle"></i></asp:RegularExpressionValidator>
                                    <asp:TextBox ID="CardAccountnumtxt" runat="server" CssClass="form-control" MaxLength="16"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" ValidChars="0123456789" TargetControlID="CardAccountnumtxt" runat="server" />
                                </div>

                                <div class="col-lg-12">
                                    <div class="col-lg-3">
                                        <label>Exp Month</label>
                                        <asp:DropDownList ID="ExpMonthDDL" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="1">Jan</asp:ListItem>
                                            <asp:ListItem Value="2">Feb</asp:ListItem>
                                            <asp:ListItem Value="3">Mar</asp:ListItem>
                                            <asp:ListItem Value="4">Apr</asp:ListItem>
                                            <asp:ListItem Value="5">May</asp:ListItem>
                                            <asp:ListItem Value="6">Jun</asp:ListItem>
                                            <asp:ListItem Value="7">Jul</asp:ListItem>
                                            <asp:ListItem Value="8">Aug</asp:ListItem>
                                            <asp:ListItem Value="9">Sep</asp:ListItem>
                                            <asp:ListItem Value="10">Oct</asp:ListItem>
                                            <asp:ListItem Value="11">Nov</asp:ListItem>
                                            <asp:ListItem Value="12">Dec</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Exp Year</label>
                                        <asp:DropDownList ID="ExpYearDDL" runat="server" CssClass="form-control">
                                            <asp:ListItem>2016</asp:ListItem>
                                            <asp:ListItem>2017</asp:ListItem>
                                            <asp:ListItem>2018</asp:ListItem>
                                            <asp:ListItem>2019</asp:ListItem>
                                            <asp:ListItem>2020</asp:ListItem>
                                            <asp:ListItem>2021</asp:ListItem>
                                            <asp:ListItem>2022</asp:ListItem>
                                            <asp:ListItem>2023</asp:ListItem>
                                            <asp:ListItem>2024</asp:ListItem>
                                            <asp:ListItem>2025</asp:ListItem>
                                            <asp:ListItem>2026</asp:ListItem>
                                            <asp:ListItem>2027</asp:ListItem>
                                            <asp:ListItem>2028</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="col-lg-12">
                                            <label>Card Security Number</label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="CVVtxt" ValidationExpression="^[0-9]*$" ValidationGroup="CreditCardAdd" ErrorMessage="RequiredFieldValidator"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="RegularExpressionValidator" ValidationExpression="^[0-9]*$" ControlToValidate="CVVtxt" ValidationGroup="CreditCardAdd"><i class="fa fa-warning validationStyle"></i></asp:RegularExpressionValidator>
                                        </div>
                                        <div class="col-lg-6">
                                            <asp:TextBox ID="CVVtxt" runat="server" CssClass="form-control" MaxLength="4"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" ValidChars="0123456789" TargetControlID="CVVtxt" runat="server" />
                                        </div>
                                    </div>

                                </div>


                            </div>
                            <!--End Add New CC -->

                        </div>
                        <!--End Add New Wallet Item -->

                    </div>

                    <!--Payment Summary Area -->

                    <div class="col-lg-4 center">

                        <div class="col-lg-12" id="PayByCashHeader" runat="server" visible="false">
                            <h3>Submit a ticket to pay with cash</h3>
                            <p>Once the ticket is submitted you will receive an email or text message with your confirmation and location where you can make your payment.</p>
                        </div>

                        <div class="col-lg-12" id="PayByCashArea" runat="server" visible="false">
                            <div class="col-lg-12">
                                <h4>
                                    <asp:Label ID="PaybyCashConflbl" runat="server" ForeColor="#00CC00"></asp:Label>
                                </h4>

                            </div>
                            <div class="col-lg-12">
                                <h4>
                                    <asp:HyperLink ID="PayByCashLink" runat="server" Target="_blank" Font-Bold="True" ForeColor="Red">HyperLink</asp:HyperLink>
                                </h4>
                            </div>

                        </div>

                    </div>
                    <!--End Payment Summary Area -->
                </div>



            </section>


      </ContentTemplate>

   </asp:UpdatePanel>


</asp:Content>
