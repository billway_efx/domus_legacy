﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxPublic.SecurePages
{
    public partial class Contact : System.Web.UI.Page
    {
        private static string RenterXML;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                var RenterSession = Session["RenterSession"];
                var RenterID = Session["RenterID"];

                HttpCookie _RenterID = Request.Cookies["RenterID"];
                if (_RenterID != null || RenterSession != null)
                {
                    string DomusEndPoint = EfxFramework.EfxSettings.DomusAPIURL;

                    string RenterSessionCookie = Request.Cookies["RenterSession"]["Session"];
                    string RenterIDCookie = Request.Cookies["RenterID"]["ID"];

                    RenterXML = DataAccess.RenterDataAccess.GetRenterDetailsRequest(RenterSessionCookie, RenterIDCookie);
                    PopulateRenterDetails(RenterXML);
                }
            }
        }

        public void PopulateRenterDetails(string RenterXml)
        {
            DataAccess.RenterResponse RR = DataAccess.RenterDataAccess.GetResponse(RenterXml);
            Nametxt.Text = RR.RenterFirstName + " " + RR.RenterLastName;
        }

        //CMallory - Added event below.
        protected void btnSend_Click(object sender, EventArgs e)
        {
            DataAccess.RenterResponse RR = DataAccess.RenterDataAccess.GetResponse(RenterXML);

            //If "Your Property Manager is selected from dropdown."
            if (SendToDDL.SelectedValue=="0")
                DataAccess.Miscellaneous.SendContactEmail(RR.RenterID, RR.RenterEmail, 
                RR.RenterFirstName + ' ' + RR.RenterLastName, 
                msgtxt.Text, Phonetxt.Text, 
                "ResidentPropertyEmail");

            //If "Domus Support" is selected from dropdown."
            else if (SendToDDL.SelectedValue=="1")
                DataAccess.Miscellaneous.SendContactEmail(RR.RenterID, RR.RenterEmail, 
                RR.RenterFirstName + ' ' + RR.RenterLastName, 
                msgtxt.Text, Phonetxt.Text, 
                "ResidentEFXSupport");

            Nametxt.Text = "";
            Phonetxt.Text = "";
            msgtxt.Text = "";

            Response.Redirect("ContactUsSent.aspx");
        }
    }
}