﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxPublic.SecurePages
{
    public partial class PaymentConfirmation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string RenterIDCookie = Request.Cookies["RenterID"]["ID"];
            int RenterID = int.Parse(RenterIDCookie);
            PopulatePaymentDetails(RenterID);
        }

        private void PopulatePaymentDetails(int RenterID)
        {
            string RenterSessionCookie = Request.Cookies["RenterSession"]["Session"];
            string RenterIDCookie = Request.Cookies["RenterID"]["ID"];
            decimal RBD = 0m;
            string PayableItems = DataAccess.Payments.GetPayableItemsForRenter(RenterSessionCookie, RenterIDCookie);

            var payables = DataAccess.Payments.GetItems(PayableItems);

            var p = DataAccess.Payments.GetDetailsForConfirmPage(RenterID);

            PaymentAmountlbl.Text = p.PaymentAmount.ToString("c");
            ConvenienceFeelbl.Text = p.ConvFee.ToString("c");
            totalPaymentlbl.Text = p.TotalPayment.ToString("c");
            paymentDatelbl.Text = p.TransDate.ToShortDateString();
            TransactionIDlbl.Text = p.TransID;

            RBD = payables.CurrentBalanceDue;
            if(RBD > 0)
            {
                RemainBalance.Text = payables.CurrentBalanceDue.ToString("c");
                PayRemainingBalancebtn.Visible = true;
            }
            else
            {
                RemainBalance.Text = "$0.00";
                PayRemainingBalancebtn.Visible = false;
            }
        }

        protected void Continuebtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("/SecurePages/Account.aspx");
        }

        protected void PayRemainingBalancebtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("/SecurePages/ResidentPayment.aspx");
        }



    }
}