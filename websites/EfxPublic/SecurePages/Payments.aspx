﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SecurePages/DomusMaster.Master" AutoEventWireup="true" CodeBehind="Payments.aspx.cs" Inherits="EfxPublic.SecurePages.Payments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

    <div>
        <%-- This page is not used  --%>
         <%-- This page is not used  --%>
         <%-- This page is not used  --%>
         <%-- This page is not used  --%>
         <%-- This page is not used  --%>
         <%-- This page is not used  --%>
         <%-- This page is not used  --%>
         <%-- This page is not used  --%>
         <%-- This page is not used  --%>
         <%-- This page is not used  --%>
         <%-- This page is not used  --%>
         <%-- This page is not used  --%>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <section style="padding-bottom: 10px;">
                    <div>
                    </div>
                </section>

                <!--Resident Details Section -->
                <section class="text-center">
                    <div class="container">

                        <div class="col-lg-6 center text-center">
                            <h4>
                                <asp:Label ID="RenterFullNamelbl" runat="server"></asp:Label><br />
                                <asp:Label ID="RenterAddresslbl" runat="server"></asp:Label><br />
                                <asp:Label ID="RenterStatelbl" runat="server"></asp:Label><br />
                                <asp:Label ID="RenterEmailAddresslbl" runat="server"></asp:Label>
                            </h4>

                        </div>


                    </div>
                    <div class="col-lg-6 center">

                        <asp:GridView ID="GridView1" CssClass="table table-responsive" runat="server" DataKeyNames="PayableItemId,RenterId" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="PayableItemsId" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="PayableItemIdlbl" runat="server" Text='<%# Bind("PayableItemId") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                    
                                <asp:BoundField DataField="RenterId" HeaderText="RenterId" Visible="False" />
                                <asp:BoundField DataField="LeaseId" HeaderText="LeaseId" Visible="False" />
                                <asp:BoundField DataField="PropertyId" HeaderText="PropertyId" Visible="False" />

                                <asp:TemplateField HeaderText="Due Date">
                                    <ItemTemplate>
                                        <asp:Label ID="PayableDueDatelbl" runat="server" Text='<%# string.Format("{0:d}",  Convert.ToDateTime(Eval("PayableDueDate"))) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Description" HeaderText="Description" />
                                <asp:BoundField DataField="Amount_PAY" DataFormatString="{0:c}" HeaderText="Amount" />
                                <asp:TemplateField HeaderText="Editable" Visible="False">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Editable") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="EditableLBL" runat="server" Text='<%# Bind("Editable") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="AverageMonthlyRent" HeaderText="AverageMonthlyRent" Visible="False" />
                                <asp:TemplateField HeaderText="Balance Due">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("CurrentBalanceDue") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="currentBalanceDuelbl" runat="server" Text='<%# Bind("CurrentBalanceDue") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Select">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PayChoose") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBox1" runat="server" />
                                        <%--  <asp:Label ID="Label1" runat="server" Text='<%# Bind("PayChoose") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <asp:SqlDataSource ID="PaymentSQL" runat="server"></asp:SqlDataSource>

                    </div>


                </section>
                <!--End Resident Details Section -->



                <!--Wallet Section -->


                <section class="text-center" id="WalletGridSection" runat="server">
                    <div class="container">


                        <div class="col-lg-6 center">

                            <asp:GridView ID="GridView2" CssClass="table table-responsive" runat="server" AutoGenerateColumns="False" DataKeyNames="PayerId,PayerMethodId" OnRowDataBound="GridView2_RowDataBound" OnSelectedIndexChanged="GridView2_SelectedIndexChanged" ShowHeader="False" OnRowCommand="GridView2_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="AutoSet" HeaderText="AutoSet" Visible="False" />
                                    <asp:TemplateField HeaderText="Description">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("Description") %>'></asp:Label>&nbsp;
                            <asp:Label ID="ExpDatelbl" runat="server"></asp:Label>
                                            <br />
                                            <asp:Label ID="DescTypelbl" runat="server" Text="Label"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Exp Date" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("EXP_Date") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="EXP_Datelbl" runat="server" Text='<%# Bind("EXP_Date") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Editable" HeaderText="Editable" Visible="False" />
                                    <asp:BoundField DataField="IsPrimary" HeaderText="IsPrimary" Visible="False" />
                                    <asp:BoundField DataField="PayChoose" HeaderText="PayChoose" Visible="False" />
                                    <asp:TemplateField HeaderText="PayerId" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PayerId") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("PayerId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PayerMethodId" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("PayerMethodId") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("PayerMethodId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PayerWalletID" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("PayerWalletId") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="PayerWalletIDlbl" runat="server" Text='<%# Bind("PayerWalletId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="PayerWalletId" HeaderText="PayerWalletId" Visible="false" />
                                    <asp:TemplateField HeaderText="PaymentMethod" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("PaymentMethod") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="PaymentMethodlbl" runat="server" Text='<%# Bind("PaymentMethod") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="RequestSecurityCode" Visible="False">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("RequestSecurityCode") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("RequestSecurityCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
      
                                    <asp:BoundField DataField="UsedLastTime" HeaderText="UsedLastTime" Visible="false" />

                                    <asp:TemplateField ShowHeader="True" HeaderText="Choose Payment">
                                        <ItemTemplate>
                                            <asp:Button ID="Button1" runat="server" Text="Use" CommandName="Select" CommandArgument="Use" CssClass="btn btn-sm btn-primary" />
                                            <asp:Button ID="EditWalletItembtn" runat="server" CommandName="Select" Text="Edit" CssClass="btn btn-sm btn-primary" CommandArgument='<%# Eval("PaymentMethod") %>' CausesValidation="False" />
                                            <asp:Button ID="Addnewbtn" runat="server" Text="Add" CssClass="btn btn-sm btn-primary" CommandArgument="AddNew" CausesValidation="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>

                        </div>

                        <div class="col-lg-6 center" id="SubmitPay1" runat="server">
                            <div class="col-lg-12" style="padding-bottom:10px;">
                                <asp:CheckBox ID="AgreeCkbx1" runat="server" AutoPostBack="True" OnCheckedChanged="AgreeCkbx1_CheckedChanged" />&nbsp;&nbsp;&nbsp;<a href="/SecurePages/TermsAndConditions.aspx">Agree to Terms and Conditions</a>&nbsp;
                            </div>

                             <div class="col-lg-6 center" id="cscarea" runat="server" visible="false">
                                 <div class="col-lg-12">
                                     <label>Card Security Code</label>
                                 <asp:TextBox ID="CSCcodetxt" CssClass="form-control" runat="server"></asp:TextBox>
                                 </div>
                                 </div>
                                 

                            <div class="col-lg-12">
                            <asp:Button ID="SubmitPaybtn1" CssClass="btn btn-primary" runat="server" Enabled="False" OnClick="SubmitPaybtn1_Click" />
                                </div>
                        </div>

                    </div>
                </section>





                <!--Add New Wallet Item -->
                <div class="col-lg-12" id="AddnewWalletItemSection" runat="server">

                    <!--Add New ECheck -->
                    <div class="col-lg-6 center" id="EcheckADD" runat="server">
                        <h4>
                            <asp:Label ID="EcheckHeaderlbl" runat="server" Text="Label"></asp:Label></h4>
                        <div class="col-lg-8">
                            <label>Routing Number</label>
                            <asp:TextBox ID="NewRoutingNumbertxt" CssClass="form-control" Text="063000047" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-lg-8">
                            <label>Account Number</label>
                            <asp:TextBox ID="NewAccountnumbertxt" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-lg-8">
                            <asp:CheckBox ID="AgreeCkbx2" runat="server" AutoPostBack="True" OnCheckedChanged="AgreeCkbx2_CheckedChanged" />&nbsp;<a href="/SecurePages/TermsAndConditions.aspx">Agree to Terms and Conditions</a>&nbsp;
                            <asp:Button ID="AddNewEcheckandPaybtn" CssClass="btn btn-primary" runat="server" OnClick="AddNewEcheckandPaybtn_Click" Enabled="False" />
                            <asp:Button ID="CancelAddNewEcheckbtn" runat="server" Text="Cancel" CssClass="btn btn-primary" OnClick="CancelAddNewEcheckbtn_Click" />
                        </div>
                    </div>
                    <!--End Add New ECheck -->

                    <!--Add New CC -->
                    <div class="col-lg-6 center" id="CreditCardAdd" runat="server">
                        <h4>
                            <asp:Label ID="CCHeaderlbl" runat="server" Text="Label"></asp:Label>
                        </h4>
                        <div class="col-lg-8">
                            <label>Name on Card</label>
                            <asp:TextBox ID="FullNametxt" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-8">
                            <label>Account Number</label>
                            <asp:TextBox ID="CardAccountnumtxt" runat="server" CssClass="form-control" MaxLength="16"></asp:TextBox>
                        </div>

                        <div class="row col-lg-8">
                            <div class="col-lg-4">
                                <label>Exp Month</label>
                                <asp:DropDownList ID="ExpMonthDDL" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="1">Jan</asp:ListItem>
                                    <asp:ListItem Value="2">Feb</asp:ListItem>
                                    <asp:ListItem Value="3">Mar</asp:ListItem>
                                    <asp:ListItem Value="4">Apr</asp:ListItem>
                                    <asp:ListItem Value="5">May</asp:ListItem>
                                    <asp:ListItem Value="6">Jun</asp:ListItem>
                                    <asp:ListItem Value="7">Jul</asp:ListItem>
                                    <asp:ListItem Value="8">Aug</asp:ListItem>
                                    <asp:ListItem Value="9">Sep</asp:ListItem>
                                    <asp:ListItem Value="10">Oct</asp:ListItem>
                                    <asp:ListItem Value="11">Nov</asp:ListItem>
                                    <asp:ListItem Value="12">Dec</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-lg-4">
                                <label>Exp Year</label>
                                <asp:DropDownList ID="ExpYearDDL" runat="server" CssClass="form-control">
                                    <asp:ListItem>2016</asp:ListItem>
                                    <asp:ListItem>2017</asp:ListItem>
                                    <asp:ListItem>2018</asp:ListItem>
                                    <asp:ListItem>2019</asp:ListItem>
                                    <asp:ListItem>2020</asp:ListItem>
                                    <asp:ListItem>2021</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-lg-3">
                                <label>CVV</label>
                                <asp:TextBox ID="CVVtxt" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-lg-8">
                            <asp:CheckBox ID="AgreeCkbx3" runat="server" AutoPostBack="True" OnCheckedChanged="AgreeCkbx3_CheckedChanged" />&nbsp;<a href="/SecurePages/TermsAndConditions.aspx">Agree to Terms and Conditions</a>&nbsp;
                            <asp:Button ID="AddNewCCandPaybtn" CssClass="btn btn-primary" runat="server" OnClick="AddNewCCandPaybtn_Click" Enabled="False" />
                            <asp:Button ID="CancelAddNewCCbtn" runat="server" Text="Cancel" CssClass="btn btn-primary" OnClick="CancelAddNewCCbtn_Click" />
                        </div>
                    </div>
                    <!--End Add New CC -->

                </div>
                <!--End Add New Wallet Item -->

                <!--Update Wallet Items -->
                <section id="UpdateWalletSection" runat="server">
                </section>
                <!--End Update Wallet Items -->

                <!--End Wallet Section -->



            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
</asp:Content>
