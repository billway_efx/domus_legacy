﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SecurePages/DomusMaster.Master" AutoEventWireup="true" CodeBehind="TermsAndConditions.aspx.cs" Inherits="EfxPublic.SecurePages.TermsAndConditions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section style="margin-top:20px;">

        
    <h2 class="termsAndConditionsTitle">Terms of Use and Conditions</h2>
    <article class="fullrow">
        <h3>Access of Web Site:</h3>
        <p>
            Before using this web site, please browse these terms and conditions of Domus. If you won't settle for these terms, please don't access or use this website for any purposes.
            By accessing, using, and browsing Domus.com, you settle for these terms. You signify your acceptance of these terms and conditions of service by clicking the "Accept" button
            and continuing to access or use this web site, or any service obtainable via Domus.
        </p>
    </article>

    <article class="fullrow">
        <h3>Domus Services Provided:</h3>
        <p>
            Domus provides access to an internet-based credit card, on-line ACH, and a phone payments service to rental property owners, rental property managers and their individual resident’s,
            which facilitates numerous electronic payment services for monthly rents, security deposits, alternative fixed payments and all fees and charges associated with leased space, fixed or variable.
            You, as the resident of a collaborating landlord, are eligible to use the services to pay your rent and any related fees covered underneath our service agreement. Your credit card, on-line ACH
            or by phone payments created through the service are remitted to your collaborating landlord in accordance with our service agreement with the participating landlord. You'll receive a real time
            email payment confirmation from Domus acknowledging receipt of credit card, on-line ACH or phone payments. You need to have access to the Internet in order to use the service, either
            directly or through alternative devices that access web-based content. Additionally, you need to pay all fees related to every transaction and provide all necessary instrumentation to get on-line.
        </p>
    </article>

    <article class="fullrow">
        <h3>Resident Fees:</h3>
        <p>
            Domus reserves the right at any time to alter fees for access to parts of the service or the service as a whole. In no event will you be charged for access to parts of the service or to the service unless
            Domus has sent out a notice. You shall pay all fees and charges incurred through your account at the rates in effect for the billing amount in which such fees and charges are incurred, including but not
            restricted to charges for any services offered. Late fees could also be assessed for rent payments made by credit card, on-line ACH or phone payments and other channels provide by Domus, that occur after
            the late dates and or grace periods as established in our agreements together with your landlord.
        </p>
    </article>

    <article class="fullrow">
        <h3>Domus Property:</h3>
        <p>
            This Web Site contains text, images/video (including photos, electronic art, animations, graphics), sounds/audio (including voice and music), information and data, communications programs, and other materials,
            methods of operation, and Domus data formatted, organized, and collected in a variety of forms, including layouts, pages, screens, directories, and databases. Any unauthorized use of the Domus
            content on this Web Site will violate copyright laws, trademark laws, and will subject you to legal proceedings.
        </p>
        <p>
            The content on this Web Site is either the property of Domus or is used by Domus with permission. You may not modify, publish, transmit, participate in the transfer or sale of, create derivative works,
            or in any way exploit any of the Domus Content, in whole or in part. You may not upload, post, reproduce, or distribute any Domus content unless licensed by Domus via a formal license agreement.
            Nothing contained on this Web Site shall be construed as granting, by implication, estoppel, or otherwise any license or right to use any Domus content displayed on this Web Site. Unauthorized use of any content
            by anyone is strictly prohibited with the express written consent of Domus and will violate the legal rights of Domus and will subject you to legal proceedings. All of the Domus trademarks, logos,
            and service marks displayed on this Web Site are registered and unregistered Trademarks of Domus. You may not upload, post, reproduce, or distribute any Trademarks unless licensed by Domus to do so via a
            formal trademark license agreement. Unauthorized use of the Trademarks is strictly prohibited and will violate the legal rights of Domus and will subject you to legal proceedings.
        </p>
    </article>

    <article class="fullrow">
        <h3>Domus & Partner Warranty:</h3>
        <p>
            By accessing, downloading from, and browsing on Domus.com you are doing so at your own risk. Everything on this Web Site is provided without warranty of any kind, either expressed or implied. Without limiting the generality of the foregoing,
            Domus and its partners make no warranty, representation, or guaranty of merchantability, fitness for a particular purpose, non-infringement, accuracy, correctness, currency, sanitation, availability, reliability, performance, suitability,
            compatibility, or non-infringement with regard to any aspect of this web site. Residents or the property management companies will not hold Domus partners liable for any damages occurring from misuse. Domus utilizes Parallel Solutions
            for all credit card transactions via the web and its other payment avenues. Domus fields all calls related to the credit card transactions executed on the web or through any other payment channel offered. Domus and its partners shall
            not be liable for any damages of any type caused by viruses that may infect your computer equipment on account of your access to, use of, downloading from, or browsing in this Web Site.
        </p>
    </article>

    <article class="fullrow">
        <h3>Security:</h3>
        <p>
            Domus is committed to protecting the security of your personal information. We use a variety of security technologies and procedures to help protect your personal information from unauthorized access, use, or disclosure. For example, we store the personal
            information you provide on computer systems with limited access, which are located in controlled facilities. When we transmit highly confidential information (such as a credit card number) over the Internet, we protect it through the use of encryption, such as the
            Secure Socket Layer (SSL) protocol and tokenization.
        </p>
    </article>
    <article class="fullrow">
        <h3>Changes to Terms:</h3>
        <p>
            By accepting these terms you acknowledge that Domus has the right to change the content or technical specifications of any aspect of the Service at any time at our sole discretion. You further accept that any such changes may result in your being
            unable to access RetPaidOnline.com or its other features.
        </p>
    </article>
    <article class="fullrow">
        <h3>Resident ID and Password:</h3>
        <p>
            You will produce a password and account designation when finishing the sign up process for this service. You're solely accountable for maintaining the confidentiality of your password and account, and are absolutely accountable for all activities occurring under your password or account.
            You agree to give notice Domus immediately if you have unauthorized use of your password or account or any other breach of security. Domus cannot and will not be responsible for any harm or loss arising from your failure to comply with this rule.
        </p>
    </article>
    <article class="fullrow">
        <h3>Cancelation and Refunds:</h3>
        <p>
            For all payments you believe were improperly made, Domus in its sole discretion may, void, rescind or issue a credit for your rent payment made through the service at any time prior to the remittance of such rent payment to your landlord. If a rent payment dispute arises after payment
            is forwarded to your landlord, the responsibility to settle the rent payment dispute rests with you and the landlord. You waive the right to cancel credit card payments, which have been cleared through Domus.
        </p>
    </article>
    <article class="fullrow">
        <h3>Governing law:</h3>
        <p>You agree that all actions or proceedings arising out of, or related to these terms and conditions of our service or the service shall be litigated in local, state or federal court within the state of Florida.</p>
    </article>
    <article class="fullrow">
        <h3>Entirety of the Agreement:</h3>
        <p>
            The terms and conditions of service together with the sign up info contain the complete understanding and agreement between you and Domus with relevance its material, superseding all previous or contemporaneous representations,
            understandings, and the other oral or written agreements between the parties with relevance to such material.
        </p>
    </article>




    </section>



</asp:Content>
