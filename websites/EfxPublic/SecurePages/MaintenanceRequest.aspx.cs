﻿using System;
using System.Xml;
using System.Web.UI;
using System.Data;
using System.Data.SqlClient;


namespace EfxPublic.SecurePages
{
    public partial class MaintenanceRequest : System.Web.UI.Page
    {
        private string streetAddress, cityState;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                string DomusEndPoint = EfxFramework.EfxSettings.DomusAPIURL;
                string _session = Session["RenterSession"].ToString();
                string _RenterID = Session["RenterID"].ToString();
                string UnitNum = "";

                string RenterXML = DataAccess.RenterDataAccess.GetRenterDetailsRequest(_session, _RenterID);
                XmlDocument _RenterDocument = new XmlDocument();
                _RenterDocument.LoadXml(RenterXML);
                XmlNodeList xnList = _RenterDocument.SelectNodes("/GetRenterByRenterIdResponse");
                XmlNode RenterNode = xnList.Item(1);

                streetAddress = _RenterDocument.ChildNodes.Item(0).ChildNodes.Item(1).ChildNodes.Item(2).ChildNodes.Item(3).InnerText;
                cityState = _RenterDocument.ChildNodes.Item(0).ChildNodes.Item(1).ChildNodes.Item(2).ChildNodes.Item(0).InnerText + ", " +
                            _RenterDocument.ChildNodes.Item(0).ChildNodes.Item(1).ChildNodes.Item(2).ChildNodes.Item(2).InnerText;
                UnitNumtxt.Text = _RenterDocument.ChildNodes.Item(0).ChildNodes.Item(1).ChildNodes.Item(2).ChildNodes.Item(4).InnerText;

                MaintenanceSQL.SelectParameters["RenterID"].DefaultValue = _RenterID;
                MaintenanceGrid.DataBind();
            }
        }

        protected void SubmitRequestbtn_Click(object sender, EventArgs e)
        {
            
            string _session = Session["RenterSession"].ToString();
            string _Renter = Session["RenterID"].ToString();
            string _type = MaintenanceTypeDDL.SelectedValue.ToString();
            string _Priority = PriorityDDL.SelectedValue.ToString();
            string _desc = Desctxt.Text;
            string p = PermissionDDL.SelectedValue;
            string _UnitNum = UnitNumtxt.Text.ToString();

            // Added in response to IH-1007 - Adding a COVID-19 Questionnaire
            // 05.11.2020 - BW Adding a new User Control for a Questionnaire for COVID-19 per AGPM's request
            string firstQuestion = Covid.FirstAnswer ;
            string secondQuestion = Covid.SecondAnswer;
            string thirdQuestion = Covid.ThirdAnswer;
            string fourthQuestion = Covid.FourthAnswer;
            string contactName = Covid.ContactName;
            string methodOfContact = Covid.MethodOfContact;
            string day = Covid.Day;
            string time = Covid.Time;

            string Address = this.streetAddress;
            string City_State = this.cityState;

            int RenterID = int.Parse(_Renter);
            int ReqType = int.Parse(_type);
            int Priorityint = int.Parse(_Priority);

            try
            {
                SubmitReq(RenterID, ReqType, Priorityint, _desc, p, _UnitNum, firstQuestion,
                          secondQuestion, thirdQuestion, fourthQuestion, contactName, methodOfContact, day, time);
                ClearFields();
                Statuslbl.Text = "Request Submitted!";
                MaintenanceGrid.DataBind();
            }
            catch (Exception ex)
            {
                Statuslbl.Text = "Error! - the Web admin will look into this";
            }
        }

        private void ClearFields()
        {
            MaintenanceTypeDDL.SelectedIndex = 0;
            PriorityDDL.SelectedIndex = 0;
            Desctxt.Text = string.Empty;
            UnitNumtxt.Text = string.Empty;

            Covid.ResetFields();
        }

        private void SubmitReq(int RenterID, int ReqType, int ReqPriority, string desc, string Permission, string UnitNum,
                                string firstQuestion, string secondQuestion, string thirdQuestion, string fourthQuestion,
                                string contactName, string methodOfContact, string day, string time)
        {
            SqlConnection con = new SqlConnection(ConnString);
            //SWitherspoon: Changed procedure to _v2 to match newly created procedure
            SqlCommand com = new SqlCommand("Email_SendDomusMaintenanceRequest_WithCovidQuestions", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@RenterID", SqlDbType.Int).Value = RenterID;
            com.Parameters.Add("@RequestType", SqlDbType.Int).Value = ReqType;
            com.Parameters.Add("@Priority", SqlDbType.Int).Value = ReqPriority;
            com.Parameters.Add("@Request", SqlDbType.NVarChar, 2000).Value = desc;
            com.Parameters.Add("@Permission", SqlDbType.NVarChar, 20).Value = Permission;
            //SWitherspoon: Added new parameter to match additional parameter in new Stored Procedure
            com.Parameters.Add("@UnitNum", SqlDbType.NVarChar, 30).Value = UnitNum;

            // BWay: Added for ticket IH-1007 COVID-19 questionnaire - 05.11.2020
            com.Parameters.Add("@firstQuestion", SqlDbType.NVarChar, 5).Value = firstQuestion;
            com.Parameters.Add("@secondQuestion", SqlDbType.NVarChar, 5).Value = secondQuestion;
            com.Parameters.Add("@thirdQuestion", SqlDbType.NVarChar, 5).Value = thirdQuestion;
            com.Parameters.Add("@fourthQuestion", SqlDbType.NVarChar, 5).Value = fourthQuestion;
            com.Parameters.Add("@ContactName", SqlDbType.NVarChar, 50).Value = contactName;
            com.Parameters.Add("@methodOfContact", SqlDbType.NVarChar, 50).Value = methodOfContact;
            com.Parameters.Add("@day", SqlDbType.NVarChar, 50).Value = day;
            com.Parameters.Add("@time", SqlDbType.NVarChar, 50).Value = time;
            

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            catch (SqlException sqlEx)
            {
                con.Close();
                throw;
            }
        }

        private static string ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }



    }
}