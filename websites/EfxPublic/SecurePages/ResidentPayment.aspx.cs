﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using System.Net;
using System.Windows.Forms;
using EfxPublic.DataAccess;
using RPO;
using Button = System.Web.UI.WebControls.Button;
using CheckBox = System.Web.UI.WebControls.CheckBox;
using Label = System.Web.UI.WebControls.Label;
using TextBox = System.Web.UI.WebControls.TextBox;

//using System.Windows.Forms;

namespace EfxPublic.SecurePages
{
   public partial class ResidentPayment : System.Web.UI.Page
   {
      public Page ParentPage { get { return this; } }

      public class ResidentPaymentItems
      {
         public string renterid;
      }

      public class GetPayableItemDetails
      {
         public int RenterID;
         public int LeaseID;
         public int propertyID;
         public int PayableItemID;
         public decimal RentAmount;
         public decimal CurrentBalanceDue;
         public bool AllowPartialPayments;
         // Added 05.29.2020 BW
         public bool AllowPartialPaymentsResidentPortal;
         public decimal PayLimit;
         public int AcceptedPaymentTypeID;
      }


      protected void Page_Load(object sender, EventArgs e)
      {
         if (!Page.IsPostBack)
         {
            HttpCookie _RenterID = Request.Cookies["RenterID"];
            HttpCookie _Session = Request.Cookies["RenterSession"];

            if (_RenterID != null || _Session != null)
            {
               string DomusEndPoint = EfxFramework.EfxSettings.DomusAPIURL;

               string RenterSessionCookie = Request.Cookies["RenterSession"]["Session"];
               string RenterIDCookie = Request.Cookies["RenterID"]["ID"];

               string GetWalletItems = DomusEndPoint + "/Wallet_GetPaymentMethodsForRenter/";

               string RenterXML = DataAccess.RenterDataAccess.GetRenterDetailsRequest(RenterSessionCookie, RenterIDCookie);
               string PayableItems = DataAccess.Payments.GetPayableItemsForRenter(RenterSessionCookie, RenterIDCookie);

               BindPayableGrid(PayableItems);

               string WalletItems = GetRenterDetailsRequest(RenterSessionCookie, RenterIDCookie, GetWalletItems);
               BindWalletGrid(WalletItems);


               PopulateRenterDetails(RenterXML);
               PaymethodTypeHiddenField.Value = "";

               var p = GetPayables();

               if (p.AcceptedPaymentTypeID == 4)
               {
                  PaySingleItemNewCreditCardbtn.Enabled = false;
                  PaySingleItemNewBankAcctbtn.Enabled = false;
                  PayWithCashbtn.Enabled = false;
               }

               if (p.AcceptedPaymentTypeID == 3)
               {
                  PaySingleItemNewBankAcctbtn.Enabled = false;
               }
            }
            else
            {
               Response.Redirect("~/ResidentLogin.aspx");
            }

         }
      }

      private GetPayableItemDetails GetPayables()
      {
         GetPayableItemDetails GPD = new GetPayableItemDetails();
         foreach (GridViewRow GR in GridView1.Rows)
         {
            Label PayableItemlbl = (Label)GR.FindControl("PayableItemIdlbl");
            Label RentAmountlbl = (Label)GR.FindControl("RentAmountlbl");
            CheckBox SelectedCk = (CheckBox)GR.FindControl("CheckBox1");
            Label CBDlbl = (Label)GR.FindControl("currentBalanceDuelbl");
            Label AllowPartiallbl = (Label)GR.FindControl("AllowPartialPaylbl");
            Label RenterIDlbl = (Label)GR.FindControl("RenterIDlbl");
            Label PayLImitlbl = (Label)GR.FindControl("PayLimitlbl");
            Label AcceptedPaymentType = (Label)GR.FindControl("AcceptedPaymentTypelbl");

            if (SelectedCk.Checked == true)
            {
               GPD.PayableItemID = int.Parse(PayableItemlbl.Text);
               GPD.RentAmount = decimal.Parse(CleanString(RentAmountlbl.Text));
               GPD.CurrentBalanceDue = decimal.Parse(CleanString(CBDlbl.Text));
               GPD.AllowPartialPayments = bool.Parse(AllowPartiallbl.Text);
               GPD.RenterID = int.Parse(RenterIDlbl.Text);
               GPD.PayLimit = decimal.Parse(PayLImitlbl.Text);
               GPD.AcceptedPaymentTypeID = int.Parse(AcceptedPaymentType.Text);

               if (GPD.CurrentBalanceDue < 1 || GPD.CurrentBalanceDue > 1500)
               {
                  PayWithCashbtn.Enabled = false;
                  PayWithCashbtn.ToolTip = "Payment Exceeds Cash Limit";
               }
            }

         }

         return GPD;
      }

      //This is to make sure no special characters are passed in when using "$"
      private string CleanString(string MyString)
      {
         MyString = MyString.Replace("$", "");
         MyString = MyString.Replace("(", "-");
         MyString = MyString.Replace(")", "");
         return MyString;
      }


      public void PopulateRenterDetails(string RenterXml)
      {
         DataAccess.RenterResponse RR = DataAccess.RenterDataAccess.GetResponse(RenterXml);
         RenterFullNamelbl.Text = RR.RenterFirstName + " " + RR.RenterLastName;
         RenterAddresslbl.Text = RR.RenterAddress;
         RenterEmailAddresslbl.Text = RR.RenterEmail;
      }

      public void BindPayableGrid(string _xml)
      {
         byte[] byteArray = Encoding.ASCII.GetBytes(_xml);

         using (var OutputStream = new MemoryStream(byteArray))
         {
            var XmlResp = XDocument.Load(OutputStream);
            var RespElement = DataAccess.RenterDataAccess.GetRootElement(XmlResp.Root, "PayablesResponse");

            string Msg = "";
            foreach (XElement Response in RespElement.Elements())
            {
               string node = Response.Name.LocalName;

               if (node == "Message")
               {
                  Msg = Response.Value.ToString();

               }

               string _Resp = Response.ToString();
               if (node == "PayableItems")
               {
                  XmlTextReader reader = new XmlTextReader(new StringReader(_Resp));
                  DataSet dataSet = new DataSet();
                  dataSet.ReadXml(reader);

                  GridView1.DataSource = dataSet.Tables[0];
                  GridView1.DataBind();
               }
            }
         }
      }

      public void BindFeeListGrid(string _xml)
      {
         byte[] byteArray = Encoding.ASCII.GetBytes(_xml);

         using (var OutputStream = new MemoryStream(byteArray))
         {
            var XmlResp = XDocument.Load(OutputStream);
            var RespElement = DataAccess.RenterDataAccess.GetRootElement(XmlResp.Root, "GetPayableFeeDetailResponse");
            string Msg = "";
            foreach (XElement Response in RespElement.Elements())
            {
               string node = Response.Name.LocalName;

               if (node == "Message")
               {
                  Msg = Response.Value.ToString();
                  if (Msg == "Session timed out")
                  {
                     FormsAuthentication.SignOut();
                     HttpContext.Current.Response.Redirect("~/ResidentLogin.aspx");
                  }

                  if (Msg.Contains("Error reading database"))
                  {
                     string Error = "";
                  }

               }

               string _Resp = Response.ToString();
               if (node == "PayableItem_FeeDetailItems")
               {
                  XmlTextReader reader = new XmlTextReader(new StringReader(_Resp));
                  DataSet dataSet = new DataSet();
                  dataSet.ReadXml(reader);
                  string t = reader.Value.ToString();

                  FeeDetailGrid.DataSource = dataSet.Tables[0];
                  FeeDetailGrid.DataBind();

               }
            }
         }
      }

      public void BindWalletGrid(string _xml)
      {
         byte[] byteArray = Encoding.ASCII.GetBytes(_xml);

         using (var OutputStream = new MemoryStream(byteArray))
         {
            var XmlResp = XDocument.Load(OutputStream);
            var RespElement = DataAccess.RenterDataAccess.GetRootElement(XmlResp.Root, "Wallet_GetPaymentMethodsResponse");
            string Msg = "";
            foreach (XElement Response in RespElement.Elements())
            {
               string node = Response.Name.LocalName;

               if (node == "Message")
               {
                  Msg = Response.Value.ToString();
                  if (Msg == "Session timed out")
                  {
                     FormsAuthentication.SignOut();
                     HttpContext.Current.Response.Redirect("~/ResidentLogin.aspx");
                  }

                  if (Msg.Contains("Error reading database"))
                  {
                     string Error = "";
                  }

                  if (Msg == "No payment methods found.")
                  {
                     GridView2.DataBind();
                     return;
                  }
               }

               string _Resp = Response.ToString();
               if (node == "Wallet_PaymentMethods")
               {
                  XmlTextReader reader = new XmlTextReader(new StringReader(_Resp));
                  DataSet dataSet = new DataSet();
                  dataSet.ReadXml(reader);

                  GridView2.DataSource = dataSet.Tables[0];
                  GridView2.DataBind();
               }
            }
         }
      }

      protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
      {
         if (e.Row.RowType == DataControlRowType.DataRow)
         {
            Label Editlbl = (Label)e.Row.FindControl("EditableLBL") as Label;
            CheckBox ck = (CheckBox)e.Row.FindControl("CheckBox1") as CheckBox;
            Label CBDlbl = (Label)e.Row.FindControl("currentBalanceDuelbl") as Label;
            string EnableSelect = Editlbl.Text;
            CurrentBalanceDuelbl.Text = CBDlbl.Text;

            if (EnableSelect == "false")
            {
               ck.Checked = true;
               ck.Enabled = false;
            }
         }
      }

      private decimal ReturnCurrentBalanceDue()
      {
         decimal Total = 0m;
         foreach (GridViewRow GR in GridView1.Rows)
         {
            CheckBox SelectedCk = (CheckBox)GR.FindControl("CheckBox1");
            Label CBDlbl = (Label)GR.FindControl("currentBalanceDuelbl");

            if (SelectedCk.Checked == true)
            {
               Total += decimal.Parse(CBDlbl.Text);
            }
         }

         return Total;
      }

      private string GetPayableItemID()
      {
         string PayableItemID = "";
         foreach (GridViewRow GR in GridView1.Rows)
         {
            CheckBox SelectedCk = (CheckBox)GR.FindControl("CheckBox1");
            Label PayItemlbl = (Label)GR.FindControl("PayableItemIdlbl");

            if (SelectedCk.Checked == true)
            {
               PayableItemID += PayItemlbl.Text;
            }
         }

         return PayableItemID;
      }

      public string GetRenterDetailsRequest(string RenterSession, string RenterID, string EndPoint)
      {
         //Endpoint will come from property table this is for tesitng only
         string ApiEndPoint = EndPoint;

         string ApiEndPoint2 = RenterSession + "/" + RenterID;

         //empty string for response
         string RespString = "";

         string ApiFullString = ApiEndPoint + ApiEndPoint2;
         ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

         HttpWebRequest request;
         HttpWebResponse response = null;
         StreamReader reader;
         StringBuilder sbSource;
         string returnString = "";

         try
         {
            // Create and initialize the web request  
            request = WebRequest.Create(ApiFullString) as HttpWebRequest;

            request.KeepAlive = false;
            request.Method = "GET";

            // Set timeout to 15 seconds  
            request.Timeout = 15 * 1000;

            // Get response  
            response = request.GetResponse() as HttpWebResponse;

            if (request.HaveResponse == true && response != null)
            {
               // Get the response stream  
               reader = new StreamReader(response.GetResponseStream());

               // Read it into a StringBuilder  
               sbSource = new StringBuilder(reader.ReadToEnd());
               returnString = sbSource.ToString();
            }
            return returnString.ToString();
         }
         catch (WebException wex)
         {
            string fullerror = "";
            // This exception will be raised if the server didn't return 200 - OK  
            // Try to retrieve more information about the network error  
            if (wex.Response != null)
            {
               using (HttpWebResponse errorResponse = (HttpWebResponse)wex.Response)
               {
                  string ErrResp = wex.Response.Headers.ToString();

                  fullerror = "ERROR " + errorResponse.StatusDescription + " " + errorResponse.StatusCode + " " + errorResponse.StatusCode;
               }
            }
            return "";
         }
         finally
         {
            if (response != null)
            {
               response.Close();
            }
         }
      }

      protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
      {
         if (e.Row.RowType == DataControlRowType.DataRow)
         {
            var p = GetPayables();
            //string SelectMultiWallet = SelectMultiplePaymentOptionDDL.SelectedValue;

            string SelectMultiWallet = "false";

            //DescTypelbl
            Label PayMethodLabel = (Label)e.Row.FindControl("PaymentMethodlbl") as Label;
            Label DescTypeLabel = (Label)e.Row.FindControl("DescTypelbl") as Label;
            Label Exp_Date = (Label)e.Row.FindControl("EXP_Datelbl") as Label;
            Label ExpDateLabel = (Label)e.Row.FindControl("ExpDatelbl") as Label;
            Label PayDesc = (Label)e.Row.FindControl("Label4") as Label;

            Button usebtn = (Button)e.Row.FindControl("Button1") as Button;
            Button edit = (Button)e.Row.FindControl("EditWalletItembtn") as Button;
            Button SelectBtn = (Button)e.Row.FindControl("SelectWalletitemBtn") as Button;

            CheckBox MultiWalletCkBx = (CheckBox)e.Row.FindControl("UseWalletItemCkbx") as CheckBox;

            TextBox CvvTextBox = (TextBox)e.Row.FindControl("CVVtxt") as TextBox;

            if (p.AcceptedPaymentTypeID == 4)
            {
               SelectBtn.Enabled = false;
            }

            switch (SelectMultiWallet)
            {
               case "false":
                  MultiWalletCkBx.Visible = false;
                  SelectBtn.Visible = true;
                  e.Row.Cells[14].Visible = false;
                  GridView2.HeaderRow.Cells[14].Visible = false;
                  break;

               case "true":
                  MultiWalletCkBx.Visible = true;
                  SelectBtn.Visible = false;
                  e.Row.Cells[14].Visible = true;
                  break;
            }

            if (PayMethodLabel.Text == "false")
            {
               DescTypeLabel.Text = "E-Check";
               CvvTextBox.Visible = false;

               if (p.AcceptedPaymentTypeID == 3)
               {
                  SelectBtn.Enabled = false;
               }

               //dedcdc
               SelectBtn.BackColor = System.Drawing.ColorTranslator.FromHtml("#dedcdc");
            }
            else
            {
               ExpDateLabel.Text = "Exp " + Exp_Date.Text;
               DescTypeLabel.Text = "Credit Card";
               CvvTextBox.Visible = true;
               SelectBtn.BackColor = System.Drawing.ColorTranslator.FromHtml("#badc02");
            }

            bool ChecknoPay = PayDesc.Text.Contains("No payment methods found.");
         }
      }

      protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
      {
         PaymethodTypeHiddenField.Value = "UseWallet";
         PaywithSinglePayMethodBtn.ValidationGroup = "SinglePayment";
         AmountValidator.ValidationGroup = "SinglePayment";
         UpdatetotalBtn.ValidationGroup = "SinglePayment";

         var Payable = GetPayables();

         decimal Amount = Payable.CurrentBalanceDue;
         decimal ConvFee = 0m;
         string PayMethod = GridView2.SelectedDataKey[1].ToString();
         int Program = int.Parse(GridView2.SelectedDataKey[2].ToString());

         if (PayMethod == "true")
         {
            CSClbl.Visible = true;
            CSCtxt.Visible = true;
         }
         if (PayMethod == "false")
         {
            CSClbl.Visible = false;
            CSCtxt.Visible = false;
         }

         if (Payable.AllowPartialPayments == true)
         {
            UpdatetotalBtn.Visible = true;
         }
         else
         {
            UpdatetotalBtn.Visible = false;
         }
         if (Payable.CurrentBalanceDue >= 1)
         {
            PaywithSinglePayMethodBtn.Enabled = true;
         }
         else
         {
            PaywithSinglePayMethodBtn.Enabled = false;
         }

         ConvFee = DataAccess.Payments.GetConvFee(Program, PayMethod, Amount);

         CurrentBalanceDuelbl.Text = Amount.ToString("c");
         AmountBeingPaidtxt.Text = Payable.CurrentBalanceDue.ToString("c");

         if (Payable.AllowPartialPayments == false)
         {
            AmountBeingPaidtxt.Enabled = false;
         }
         else
         {
            AmountBeingPaidtxt.Enabled = true;
         }

         if (Amount < 0)
         {
            TotalAmountlbl.Text = "$0.00";
            ConvFeeTotallbl.Text = "$0.00";
         }
         else
         {
            TotalAmountlbl.Text = (Amount + ConvFee).ToString("c");
            ConvFeeTotallbl.Text = ConvFee.ToString("c");
         }

         if (GridView2.SelectedIndex > -1)
         {
            PaywithSinglePayMethodBtn.Visible = true;
            PaywithSinglePayMethodBtn.Text = "Submit Payment " + TotalAmountlbl.Text;
            AmountArea.Visible = true;
         }
         else
         {
            AmountArea.Visible = false;
         }
      }

      protected void UseWalletItemCkbx_CheckedChanged(object sender, EventArgs e)
      {

      }

      protected void SelectMultiplePaymentOptionDDL_SelectedIndexChanged(object sender, EventArgs e)
      {
         var payable = GetPayables();

         bool isGrid2Visible = GridView2.Visible;
         if (isGrid2Visible == false)
         {
            GridView2.Visible = true;
         }

         string DomusEndPoint = EfxFramework.EfxSettings.DomusAPIURL;
         string GetWalletItems = DomusEndPoint + "/Wallet_GetPaymentMethodsForRenter/";

         string RenterSessionCookie = Request.Cookies["RenterSession"]["Session"];
         string RenterIDCookie = Request.Cookies["RenterID"]["ID"];
         int RenterIDint = int.Parse(RenterIDCookie);
         decimal CBD = payable.CurrentBalanceDue;
         decimal convFee = 0m;
         decimal RBD = payable.CurrentBalanceDue;
         string paymentMethod = "";
         GridView2.SelectedIndex = -1;
         string WalletItems = DataAccess.Wallet.GetPaymentMethodsForRenter(RenterSessionCookie, RenterIDCookie);
         BindWalletGrid(WalletItems);

         ConvFeeTotallbl.Text = "$0.00";
         TotalAmountlbl.Text = "$0.00";

         string multipay = SelectMultiplePaymentOptionDDL.SelectedValue;

         switch (multipay)
         {
            case "true":
               PaywithSinglePayMethodBtn.Visible = false;
               CreditCardAdd.Visible = false;
               EcheckADD.Visible = false;
               CSClbl.Visible = false;
               CSCtxt.Visible = false;
               break;


            case "false":
               PaywithMultiplePayMethodbtn.Visible = false;
               CreditCardAdd.Visible = false;
               EcheckADD.Visible = false;
               break;


            case "NewCC":
               PaywithSinglePayMethodBtn.Visible = false;
               PaywithMultiplePayMethodbtn.Visible = false;
               GridView2.Visible = false;
               AddnewWalletItemSection.Visible = true;
               CreditCardAdd.Visible = true;
               EcheckADD.Visible = false;
               paymentMethod = "true";
               convFee = DataAccess.Payments.GetConvFeeByRenterID(RenterIDint, paymentMethod, CBD);
               ConvFeeTotallbl.Text = convFee.ToString("c");
               TotalAmountlbl.Text = (convFee + CBD).ToString("c");
               CSClbl.Visible = false;
               CSCtxt.Visible = false;
               break;

            case "NewEcheck":
               PaywithSinglePayMethodBtn.Visible = false;
               PaywithMultiplePayMethodbtn.Visible = false;
               GridView2.Visible = false;
               AddnewWalletItemSection.Visible = true;
               EcheckADD.Visible = true;
               CreditCardAdd.Visible = false;
               paymentMethod = "false";
               convFee = DataAccess.Payments.GetConvFeeByRenterID(RenterIDint, paymentMethod, CBD);
               ConvFeeTotallbl.Text = convFee.ToString("c");
               TotalAmountlbl.Text = (convFee + CBD).ToString("c");
               CSClbl.Visible = false;
               CSCtxt.Visible = false;
               break;
         }
      }

      protected void CancelAddNewEcheckbtn_Click(object sender, EventArgs e)
      {
         SelectMultiplePaymentOptionDDL.SelectedIndex = 0;
         AddnewWalletItemSection.Visible = false;
         GridView2.DataBind();
         GridView2.Visible = true;
      }

      protected void CancelAddNewCCbtn_Click(object sender, EventArgs e)
      {
         SelectMultiplePaymentOptionDDL.SelectedIndex = 0;
         AddnewWalletItemSection.Visible = false;
         GridView2.DataBind();
         GridView2.Visible = true;
      }

      protected void FeeDetailbtn_Click(object sender, EventArgs e)
      {

      }

      protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
      {
         string PayableItemID = e.CommandArgument.ToString();
         string RenterSessionCookie = Request.Cookies["RenterSession"]["Session"];
         string RenterIDCookie = Request.Cookies["RenterID"]["ID"];

         FeeListArea.Visible = true;
         string FeeXml = DataAccess.RenterDataAccess.GetPayableFeeDetail(RenterSessionCookie, RenterIDCookie);
         BindFeeListGrid(FeeXml);
      }

      protected void CloseFeeListBtn_Click(object sender, EventArgs e)
      {
         FeeListArea.Visible = false;
         FeeDetailGrid.SelectedIndex = -1;
      }

      //This function will pay with a single wallet item.  
      protected void PaywithSinglePayMethodBtn_Click(object sender, EventArgs e)
      {
         mpe.Show();
         ActivityLog Log = new ActivityLog();

         string ValGroup = PaywithSinglePayMethodBtn.ValidationGroup;

         string HiddenFieldValue = PaymethodTypeHiddenField.Value.ToString();
         var gp = GetPayables();
         decimal AmountBeingPaid = 0m;
         decimal currentBalanceDue = gp.CurrentBalanceDue;

         string RenterID = Request.Cookies["RenterID"]["ID"];
         string _Session = Request.Cookies["RenterSession"]["Session"];

         string CSC = CSCtxt.Text;

         if (!TermsCheckbox.Checked)
         {
            CustomValidator err = new CustomValidator();
            err.ValidationGroup = "Payment";
            err.IsValid = false;
            //Salcedo - 2/24/2015 - task 321 - fixed wording in error message
            err.ErrorMessage = "You must accept the Terms of Agreement before paying";
            ParentPage.Validators.Add(err);
            return;
         }

         if (Page.IsValid)
         {
            //need to record start payment activity into log

            Log.WriteLog("RenterID=" + RenterID, "User Start payment Process.", (short)LogPriority.LogAlways, RenterID.ToString(), false);
            if (HiddenFieldValue != "PayWithCash")
            {
               string _amountBeingPaid = AmountBeingPaidtxt.Text;
               _amountBeingPaid = _amountBeingPaid.Replace("$", "");
               AmountBeingPaid = decimal.Parse(_amountBeingPaid);
            }

            //Process Payment using Wallet Items
            if (HiddenFieldValue == "UseWallet")
            {
               string PayerID = GridView2.SelectedDataKey[0].ToString();
               string PaymentMethod = GridView2.SelectedDataKey[1].ToString();
               int programID = int.Parse(GridView2.SelectedDataKey[2].ToString());
               string WalletID = GridView2.SelectedDataKey[3].ToString();
               string PayableItemID = gp.PayableItemID.ToString();
               bool canProcessECheck = false;

               //E-Check
               if (PaymentMethod == "false")
               {
                  // Verifying that there has only been one attempt in the last 24 hours, if so exit (return) out with a message - IH-310 - BW 05.20.2020
                  canProcessECheck = RenterDataAccess.VerifyECheck(RenterID, DateTime.Now, AmountBeingPaid);
                  if (!canProcessECheck)
                  {
                     System.Windows.Forms.MessageBox.Show("To many EChecks have been tried in 24 hours.", "ECheck",
                         System.Windows.Forms.MessageBoxButtons.OK);
                     return;
                  }
                  var PayResponse = DataAccess.Payments.PaySingleItemForRenter(_Session, RenterID, WalletID, PayableItemID, AmountBeingPaid.ToString());

                  if (PayResponse.Result == "Success")
                  {
                     //Redirect to Payment Confirmation Page
                     Response.Redirect("/SecurePages/PaymentConfirmation.aspx");
                     //mpe.Hide();
                     Log.WriteLog("RenterID=" + RenterID, "Success on E-check Payment.", (short)LogPriority.LogAlways, RenterID.ToString(), false);
                     //need to record activity into log
                  }
                  else
                  {
                     PaymentErrorArea.Visible = true;
                     //mpe.Hide();
                     PaymentErrorlbl.Text = "Payment not processed: " + PayResponse.Message;
                     //need to record activity into log
                     Log.WriteLog("RenterID=" + RenterID, "Payment failure using Wallet item on E-check Payment." + PayResponse.Result + " " + PayResponse.Message, (short)LogPriority.LogAlways, RenterID.ToString(), false);
                  }
               }
               //Credit Card
               if (PaymentMethod == "true")
               {
                  var PayResponse = DataAccess.Payments.PaySingleItemForRenterWithCSC(_Session, RenterID, WalletID, PayableItemID, AmountBeingPaid.ToString(), CSC);

                  if (PayResponse.Result == "Success")
                  {
                     //Redirect to Payment Confirmation Page
                     //mpe.Hide();
                     Response.Redirect("/SecurePages/PaymentConfirmation.aspx");
                     //need to record activity into log
                     Log.WriteLog("RenterID=" + RenterID, "Sucess on Credit Card Payment using Wallet item.", (short)LogPriority.LogAlways, RenterID.ToString(), false);
                  }
                  else
                  {
                     //mpe.Hide();
                     PaymentErrorArea.Visible = true;
                     PaymentErrorlbl.Text = "Payment not processed: " + PayResponse.Message;
                     //need to record activity into log
                     Log.WriteLog("RenterID=" + RenterID, "Payment failure using Wallet item on Credit Card Payment." + PayResponse.Result + " " + PayResponse.Message, (short)LogPriority.LogAlways, RenterID.ToString(), false);
                  }

               }
            }

            //Process Payment using new credit card
            if (HiddenFieldValue == "UseNewCC")
            {
               string NewWalletResp = "";
               string CardHolderName = FullNametxt.Text;
               string AcctNum = CardAccountnumtxt.Text;
               string PayableItemID = gp.PayableItemID.ToString();
               string _amount = AmountBeingPaid.ToString();
               string ExpMonth = ExpMonthDDL.SelectedValue;
               string ExpYear = ExpYearDDL.SelectedValue;
               string _CSC = CVVtxt.Text;

               var PayResponse = DataAccess.Payments.Wallet_PaySingleItemForRenterWithNewCC(_Session, RenterID, PayableItemID, _amount, CardHolderName, AcctNum, ExpMonth, ExpYear, _CSC);

               if (PayResponse.Result == "Success")
               {

                  //if payment is a sucess we will add new wallet
                  ///Wallet_PaySingleItemForRenterWithNewCC/{SessionId}/{RenterId}/{PayableItemId}/{AmountToPay}/{CCHolderName}/{CCAccountNumber}/{CCExpMonth}/{CCExpYear}/{CardSecurityCode}
                  //NewWalletResp = DataAccess.Wallet.AddCCPaymentMethodForRenter(_Session, RenterID, CardHolderName, AcctNum, ExpMonth, ExpYear, _CSC);

                  //Redirect to Payment Confirmation Page
                  //mpe.Hide();
                  Response.Redirect("/SecurePages/PaymentConfirmation.aspx");
                  //need to record activity into log

               }
               else
               {
                  //mpe.Hide();
                  PaymentErrorArea.Visible = true;
                  PaymentErrorlbl.Text = "Payment not processed: " + PayResponse.Message;
                  //need to record activity into log
               }

            }

            //process payment using new bank acct
            if (HiddenFieldValue == "UseNewCK")
            {
               string NewWalletResp = "";
               string _amount = AmountBeingPaid.ToString();
               //Account Type: 1 for Checking 2 for Savings
               string AccountType = BankAccountTypeDDL.SelectedValue;
               string AccountNumber = NewAccountnumbertxt.Text;
               string PayableItemID = gp.PayableItemID.ToString();
               string Rounting = NewRoutingNumbertxt.Text;
               var PayResponse = DataAccess.Payments.PaySingleItemForRenterWithNewAch(_Session, RenterID, PayableItemID, _amount, AccountType, AccountNumber, Rounting);

               if (PayResponse.Result == "Success")
               {
                  //if payment is a sucess we will add new wallet
                  //NewWalletResp = DataAccess.Wallet.AddACHPaymentMethodForRenter(_Session, RenterID, AccountType, AccountNumber, Rounting);
                  //Redirect to Payment Confirmation Page
                  //mpe.Hide();
                  Response.Redirect("/SecurePages/PaymentConfirmation.aspx");
                  //need to record activity into log

               }
               else
               {
                  //mpe.Hide();
                  PaymentErrorArea.Visible = true;
                  PaymentErrorlbl.Text = "Payment not processed: " + PayResponse.Message;
                  //need to record activity into log
               }

            }

            //*****Pay with Cash**************
            if (HiddenFieldValue == "PayWithCash")
            {
               var PayResponse = DataAccess.Payments.PayWithCash(RenterID, currentBalanceDue.ToString());


               string _Result = PayResponse.Result;
               string _status = PayResponse.Status;
               string _msg = PayResponse.Message;


               if (PayResponse.Result == "Success")
               {
                  PayByCashArea.Visible = true;
                  //mpe.Hide();
                  PaybyCashConflbl.Text = "Rent By Cash Ticket Created! Please click link below.";
                  PayByCashLink.Text = "Print Your Ticket";
                  PayByCashLink.NavigateUrl = PayResponse.Message;
                  //need to record activity into log
                  Log.WriteLog("RenterID=" + RenterID, "Sucess on Rent By Cash Submission.", (short)LogPriority.LogAlways, RenterID.ToString(), false);
               }
               else
               {
                  PaymentErrorArea.Visible = true;
                  //mpe.Hide();
                  PaymentErrorlbl.Text = "Rent By Cash Ticket not processed: " + PayResponse.Message;
                  //need to record activity into log
                  Log.WriteLog("RenterID=" + RenterID, "Rent By Cash Ticket Failure" + PayResponse.Result + " " + PayResponse.Message, (short)LogPriority.LogAlways, RenterID.ToString(), false);
               }

            }

         }

         mpe.Hide();

      }

      //This function will use multiple items to pay rent.
      protected void PaywithMultiplePayMethodbtn_Click(object sender, EventArgs e)
      {
         string GroupID = new Guid().ToString();

         int PaymentCount = 0;

         string Resp = "";
         string PayMethodStr = "";
         bool UseRow = false;
         string RenterID = Request.Cookies["RenterID"]["ID"];
         string _Session = Request.Cookies["RenterSession"]["Session"];
         int Program = 0;

         foreach (GridViewRow GR in GridView2.Rows)
         {
            CheckBox UseCkBx = (CheckBox)GR.FindControl("UseWalletItemCkbx");
            TextBox AmountTxt = (TextBox)GR.FindControl("AmountToPaytxt");
            Label PayMethod = (Label)GR.FindControl("PaymentMethodlbl");
            Label ProgramID = (Label)GR.FindControl("ProgramIDlbl");
            Program = int.Parse(ProgramID.Text);
            UseRow = UseCkBx.Checked;
            PayMethodStr = PayMethod.Text;
            if (PayMethodStr == "true" && UseRow == true)
            {
               PaymentCount += 1;

            }
         }
      }

      protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
      {
         int CSCLength = CSCtxt.Text.Length;

         string Hidden = PaymethodTypeHiddenField.Value.ToString();
         string PaymentMethod = "";
         switch (Hidden)
         {
            case "UseWallet":
               PaymentMethod = GridView2.SelectedDataKey[1].ToString();
               break;
            case "UseNewCC":
               PaymentMethod = "true";
               break;
            case "UseNewCK":
               PaymentMethod = "false";
               break;
         }

         if (Hidden == "UseWallet")
         {
            if (PaymentMethod == "true")
            {
               if (CSCLength < 1)
               {
                  args.IsValid = false;
               }
            }
            else
            {
               args.IsValid = true;
            }
         }
         else
         {
            args.IsValid = true;
         }
      }

      protected void UpdatetotalBtn_Click(object sender, EventArgs e)
      {
         bool checkAmount = AmountValidator.IsValid;
         if (checkAmount == false)
         {
            return;
         }

         var payable = GetPayables();
         string Hidden = PaymethodTypeHiddenField.Value.ToString();
         string PaymentMethod = "";
         switch (Hidden)
         {
            case "UseWallet":
               PaymentMethod = GridView2.SelectedDataKey[1].ToString();
               break;
            case "UseNewCC":
               PaymentMethod = "true";
               break;
            case "UseNewCK":
               PaymentMethod = "false";
               break;
         }

         string _amount = AmountBeingPaidtxt.Text;
         decimal _PayLimit = payable.PayLimit;


         decimal amount = decimal.Parse(_amount.Replace("$", ""));
         decimal convFee = DataAccess.Payments.GetConvFeeByRenterID(payable.RenterID, PaymentMethod, amount);
         ConvFeeTotallbl.Text = convFee.ToString("c");
         TotalAmountlbl.Text = (amount + convFee).ToString("c");

         if (amount >= 1)
         {
            if (amount > _PayLimit)
            {
               PaywithSinglePayMethodBtn.Enabled = false;
               PaymentErrorArea.Visible = true;
               PaymentErrorlbl.Text = "Payment exceeds limit, please enter smaller amount or call Customer Service";
            }
            else
            {
               PaymentErrorArea.Visible = false;
               PaywithSinglePayMethodBtn.Enabled = true;
            }

         }
         else
         {

            PaywithSinglePayMethodBtn.Enabled = false;
         }
         PaywithSinglePayMethodBtn.Text = "Submit Payment " + (amount + convFee).ToString("c");
      }

      protected void AmountValidator_ServerValidate(object source, ServerValidateEventArgs args)
      {
         decimal _decimal;
         string _AmountBeingPaid = AmountBeingPaidtxt.Text;
         _AmountBeingPaid = _AmountBeingPaid.Replace("$", "");
         bool CheckAmount = decimal.TryParse(_AmountBeingPaid, out _decimal);

         if (CheckAmount == true)
         {
            args.IsValid = true;
         }
         else
         {
            args.IsValid = false;
            return;
         }
      }

      protected void PaySingleItemNewCreditCardbtn_Click(object sender, EventArgs e)
      {
         TermsCheckbox.Visible = true;
         CancelPaybtn.Visible = true;
         PaySingleItemNewCreditCardbtn.Visible = false;
         PaySingleItemNewBankAcctbtn.Visible = false;
         PayWithCashbtn.Visible = false;
         PaymethodTypeHiddenField.Value = "UseNewCC";
         PaywithSinglePayMethodBtn.ValidationGroup = "CreditCardAdd";
         AmountValidator.ValidationGroup = "CreditCardAdd";
         UpdatetotalBtn.ValidationGroup = "CreditCardAdd";


         var payable = GetPayables();

         bool isGrid2Visible = GridView2.Visible;
         if (isGrid2Visible == true)
         {
            GridView2.Visible = false;
         }

         if (payable.AllowPartialPayments == false)
         {
            AmountBeingPaidtxt.Enabled = false;
         }
         else
         {
            AmountBeingPaidtxt.Enabled = true;
         }

         if (payable.AllowPartialPayments == true)
         {
            UpdatetotalBtn.Visible = true;
         }
         else
         {
            UpdatetotalBtn.Visible = false;
         }
         if (payable.CurrentBalanceDue >= 1)
         {
            PaywithSinglePayMethodBtn.Enabled = true;
         }
         else
         {
            PaywithSinglePayMethodBtn.Enabled = false;
         }

         AmountArea.Visible = true;

         string DomusEndPoint = EfxFramework.EfxSettings.DomusAPIURL;

         string RenterSessionCookie = Request.Cookies["RenterSession"]["Session"];
         string RenterIDCookie = Request.Cookies["RenterID"]["ID"];
         int RenterIDint = int.Parse(RenterIDCookie);
         decimal CBD = payable.CurrentBalanceDue;
         decimal convFee = 0m;
         decimal RBD = payable.CurrentBalanceDue;
         string paymentMethod = "";

         AmountBeingPaidtxt.Text = payable.CurrentBalanceDue.ToString("c");
         PaywithSinglePayMethodBtn.Visible = false;
         PaywithMultiplePayMethodbtn.Visible = false;
         GridView2.Visible = false;
         AddnewWalletItemSection.Visible = true;
         CreditCardAdd.Visible = true;
         EcheckADD.Visible = false;
         paymentMethod = "true";

         CurrentBalanceDuelbl.Text = CBD.ToString("c");
         convFee = DataAccess.Payments.GetConvFeeByRenterID(RenterIDint, paymentMethod, CBD);
         ConvFeeTotallbl.Text = convFee.ToString("c");
         TotalAmountlbl.Text = (convFee + CBD).ToString("c");
         CSClbl.Visible = false;
         CSCtxt.Visible = false;

         PaywithSinglePayMethodBtn.Visible = true;
         PaywithSinglePayMethodBtn.Text = "Submit Payment";
      }

      protected void PaySingleItemNewBankAcctbtn_Click(object sender, EventArgs e)
      {

         TermsCheckbox.Visible = true;
         CancelPaybtn.Visible = true;

         PaySingleItemNewCreditCardbtn.Visible = false;
         PaySingleItemNewBankAcctbtn.Visible = false;
         PayWithCashbtn.Visible = false;

         PaymethodTypeHiddenField.Value = "UseNewCK";
         PaywithSinglePayMethodBtn.ValidationGroup = "EcheckADD";
         AmountValidator.ValidationGroup = "EcheckADD";
         AmountBeingPaidtxt.ValidationGroup = "EcheckADD";
         UpdatetotalBtn.ValidationGroup = "EcheckADD";

         var payable = GetPayables();

         bool isGrid2Visible = GridView2.Visible;
         if (isGrid2Visible == true)
         {
            GridView2.Visible = false;
         }

         if (payable.AllowPartialPayments == false)
         {
            AmountBeingPaidtxt.Enabled = false;
         }
         else
         {
            AmountBeingPaidtxt.Enabled = true;
         }

         if (payable.AllowPartialPayments == true)
         {
            UpdatetotalBtn.Visible = true;
         }
         else
         {
            UpdatetotalBtn.Visible = false;
         }

         if (payable.CurrentBalanceDue >= 1)
         {
            PaywithSinglePayMethodBtn.Enabled = true;
         }
         else
         {
            PaywithSinglePayMethodBtn.Enabled = false;
         }

         AmountArea.Visible = true;

         string DomusEndPoint = EfxFramework.EfxSettings.DomusAPIURL;

         string RenterSessionCookie = Request.Cookies["RenterSession"]["Session"];
         string RenterIDCookie = Request.Cookies["RenterID"]["ID"];
         int RenterIDint = int.Parse(RenterIDCookie);
         decimal CBD = payable.CurrentBalanceDue;
         decimal convFee = 0m;
         decimal RBD = payable.CurrentBalanceDue;
         string paymentMethod = "";

         AmountBeingPaidtxt.Text = payable.CurrentBalanceDue.ToString("c");
         PaywithMultiplePayMethodbtn.Visible = false;
         GridView2.Visible = false;
         AddnewWalletItemSection.Visible = true;
         CreditCardAdd.Visible = false;
         EcheckADD.Visible = true;
         paymentMethod = "false";

         CurrentBalanceDuelbl.Text = CBD.ToString("c");
         convFee = DataAccess.Payments.GetConvFeeByRenterID(RenterIDint, paymentMethod, CBD);
         ConvFeeTotallbl.Text = convFee.ToString("c");
         TotalAmountlbl.Text = (convFee + CBD).ToString("c");

         CSClbl.Visible = false;
         CSCtxt.Visible = false;

         PaywithSinglePayMethodBtn.Visible = true;
         PaywithSinglePayMethodBtn.Text = "Submit Payment ";
      }

      protected void CancelPaybtn_Click(object sender, EventArgs e)
      {
         paybuttonArea.Visible = true;
         CancelPaybtn.Visible = false;
         AddnewWalletItemSection.Visible = false;
         GridView2.Visible = true;
         AmountArea.Visible = false;
         ConvFeeTotallbl.Text = "$0.00";
         TotalAmountlbl.Text = "$0.00";
         GridView2.SelectedIndex = -1;
         PaywithSinglePayMethodBtn.Visible = false;
         PaySingleItemNewCreditCardbtn.Visible = true;
         PaySingleItemNewBankAcctbtn.Visible = true;
         PayWithCashbtn.Visible = true;
         PayByCashHeader.Visible = false;
      }

      protected void PayWithCashbtn_Click(object sender, EventArgs e)
      {
         TermsCheckbox.Visible = true;
         PaymethodTypeHiddenField.Value = "PayWithCash";
         paybuttonArea.Visible = true;
         CancelPaybtn.Visible = true;
         AddnewWalletItemSection.Visible = false;
         GridView2.Visible = false;
         AmountArea.Visible = false;
         ConvFeeTotallbl.Text = "$0.00";
         TotalAmountlbl.Text = "$0.00";
         GridView2.SelectedIndex = -1;
         PaywithSinglePayMethodBtn.Text = "Submit Pay By Cash Ticket: " + CurrentBalanceDuelbl.Text;
         PaywithSinglePayMethodBtn.Visible = true;
         PaySingleItemNewCreditCardbtn.Visible = false;
         PaySingleItemNewBankAcctbtn.Visible = false;
         PayWithCashbtn.Visible = false;
         PayByCashHeader.Visible = true;
      }

      protected void SelectWalletitemBtn_Click(object sender, EventArgs e)
      {
         TermsCheckbox.Visible = true;
         AgreementLink.Visible = true;
      }











   }
}