﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SecurePages/DomusMaster.Master" AutoEventWireup="true" CodeBehind="Account.aspx.cs" Inherits="EfxPublic.SecurePages.Account" %>

<%@ Register Src="~/UserControls/SecuredSitePasswordUpdate.ascx" TagPrefix="uc1" TagName="SecuredSitePasswordUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%--    <section style="background-color: #662E6B; min-height: 60px;">



    </section>--%>

    <!-- Main Page Section -->
    <section id="MainPage" style="padding-top:100px; padding-bottom:20px;">
        <div class="container">
           <div class="col-lg-12 text-center">
               <h2>
                   <asp:Label ID="PropertyHeaderlbl" runat="server" Text="Label"></asp:Label>
               </h2>

           </div>
          
        </div>
    </section>

    <section>
       
        <div class="container">
            <div class="col-lg-10 center">


                <div class="col-lg-6">
                    <h3>Resident Details</h3>
                    <div class="col-lg-12">
                        <h4>
                            <asp:Label ID="RenterNamelbl" runat="server"></asp:Label>
                        </h4>

                    </div>
                    <div class="col-lg-12">

                        <asp:Label ID="RenterAddresslbl" runat="server"></asp:Label>
                    </div>
                    <div class="col-lg-12">
                        <asp:Label ID="RenterUnitlbl" runat="server"></asp:Label>
                    </div>
                    <div class="col-lg-12">
                        <asp:Label ID="RenterCitylbl" runat="server"></asp:Label>
                    </div>
                    <div class="col-lg-12">
                        <asp:Label ID="RenterStatelbl" runat="server"></asp:Label>
                    </div>
                    <div class="col-lg-12">
                        <asp:Label ID="RenterZiplbl" runat="server"></asp:Label>
                    </div>
                    <div class="col-lg-12">
                        <label>Lease Start</label>
                        <asp:Label ID="LeaseStartDatelbl" runat="server"></asp:Label>
                    </div>
                    <div class="col-lg-12">
                        <label>Lease End</label>
                        <asp:Label ID="LeaseEndDatelbl" runat="server"></asp:Label>
                    </div>
                    <div class="col-lg-12">
                        <h3>Update Password</h3>
                        <uc1:SecuredSitePasswordUpdate runat="server" id="PasswordUpdate" />
                    </div>
                </div>
                <div class="col-lg-6">
                    <h3>Account Summary
                    </h3>

                    <div class="col-lg-12">
                        <label>Rent Amount</label>
                        <asp:Label ID="RentAmounttxt" runat="server"></asp:Label>
                    </div>
                    <div class="col-lg-12">
                        <label>Total Fees</label>
                        <asp:Label ID="TotalFeeslbl" runat="server"></asp:Label>
                    </div>
                    <div class="col-lg-12">
                        <label>Current Balance Due</label>
                        <asp:Label ID="CurrentBalanceDuelbl" runat="server"></asp:Label>
                    </div>
                    <div class="col-lg-12">
                        <label>Due Date</label>
                        <asp:Label ID="DueDatelbl" runat="server"></asp:Label>
                    </div>
                    <div class="col-lg-12">
                        <asp:Button ID="MakePaymentbtn" CssClass="btn btn-primary" runat="server" Text="Make a Payment" OnClick="MakePaymentbtn_Click" />
                    </div>
                </div>
            </div>
        </div>
    </section>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div class="container">
            <div class="well col-lg-10 center" style="overflow:scroll">
                <h3>Announcements</h3>
                <asp:GridView ID="CalendarGrid" CssClass="table table-responsive" runat="server" AutoGenerateColumns="False" DataSourceID="CalendarListSQL" AllowPaging="True" AllowSorting="True" PageSize="3">
                    <Columns>
                        <asp:BoundField DataField="AnnouncementDate" HeaderText="AnnouncementDate" SortExpression="AnnouncementDate" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="AnnouncementTitle" HeaderText="AnnouncementTitle" SortExpression="AnnouncementTitle" />
                        <asp:BoundField DataField="AnnouncementText" HeaderText="AnnouncementText" SortExpression="AnnouncementText" />
                        <asp:CheckBoxField DataField="IsActive" HeaderText="IsActive" SortExpression="IsActive" Visible="False" />
                        <asp:BoundField DataField="ExpirationDate" HeaderText="ExpirationDate" SortExpression="ExpirationDate" Visible="False" />
                        <asp:BoundField DataField="PropertyId" HeaderText="PropertyId" SortExpression="PropertyId" Visible="False" />
                        <asp:BoundField DataField="PropertyName" HeaderText="PropertyName" ReadOnly="True" SortExpression="PropertyName" Visible="False" />
                    </Columns>

                    <PagerStyle VerticalAlign="Middle" CssClass="GridPager" HorizontalAlign="Left" Font-Size="14px" Font-Bold="True" Width="10px" />
                </asp:GridView>

                <asp:SqlDataSource ID="CalendarListSQL" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="usp_Announcement_GetAllAnnouncementsRenterID" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:Parameter Name="RenterId" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </div>


<%--            <div class="col-lg-6">
                <h3>Recent Maintenance Request</h3>
                <div class="well col-lg-12">
                    <asp:GridView ID="MaintenanceGrid" CssClass="table table-responsive" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="MaintenanceSQL">
                        <Columns>
                            <asp:BoundField DataField="RequestDate" DataFormatString="{0:d}" HeaderText="Date" ReadOnly="True" SortExpression="RequestDate" />
                            <asp:BoundField DataField="RequestTyp" HeaderText="Type" SortExpression="RequestTyp" />
                            <asp:BoundField DataField="RequestDesc" HeaderText="Description" SortExpression="RequestDesc" />
                        </Columns>
                        <EmptyDataTemplate>
                            <div>
                                <h4>You have no requests</h4>
                            </div>
                        </EmptyDataTemplate>
                        <PagerStyle VerticalAlign="Middle" CssClass="GridPager" HorizontalAlign="Left" Font-Size="14px" Font-Bold="True" Width="10px" />
                    </asp:GridView>

                    <asp:SqlDataSource ID="MaintenanceSQL" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="usp_Renter_GetMaintenanceRequestListBYRenterID" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:Parameter Name="RenterID" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>

                </div>
                

            </div>--%>

             <div class="col-lg-10 center">
                 <h3>Payment History</h3>
                 <div class="well col-lg-12">
                     <asp:GridView ID="PaymentGrid" CssClass="table table-responsive" runat="server" AutoGenerateColumns="False" AllowPaging="True" DataSourceID="PaymentHistSql" AllowSorting="True">

                          <Columns>
                              <asp:BoundField DataField="TransactionDate" DataFormatString="{0:d}" HeaderText="Payment Date" ReadOnly="True" SortExpression="TransactionDate" />
                              <asp:BoundField DataField="TransactionId" HeaderText="TransactionId" SortExpression="TransactionId" />
                              <asp:BoundField DataField="Payment" HeaderText="Payment" ReadOnly="True" SortExpression="Payment" DataFormatString="{0:c}" />
                              <asp:BoundField DataField="PaymentMethod" HeaderText="Pay Method" ReadOnly="True" SortExpression="PaymentMethod" />
                              <asp:BoundField DataField="PaymentStatusName" HeaderText="Status" SortExpression="PaymentStatusName" />
                          </Columns>

                          <EmptyDataTemplate>
                            <div>
                                <h4>You have no payments</h4>
                            </div>
                        </EmptyDataTemplate>

                          <PagerStyle VerticalAlign="Middle" CssClass="GridPager" HorizontalAlign="Left" Font-Size="14px" Font-Bold="True" Width="10px" />

                     </asp:GridView>
                     <asp:SqlDataSource ID="PaymentHistSql" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="usp_Renter_GetPaymentHistoryBYRenterID" SelectCommandType="StoredProcedure">
                         <SelectParameters>
                             <asp:Parameter Name="RenterID" Type="Int32" />
                         </SelectParameters>
                     </asp:SqlDataSource>
                 </div>
            </div>

        </div>

        </ContentTemplate>

    </asp:UpdatePanel>

    <section>
        
    </section>

</asp:Content>
