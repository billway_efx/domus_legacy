﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SecurePages/DomusMaster.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="EfxPublic.SecurePages.Contact" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <section style="padding-bottom: 10px;">
                    <div>
                    </div>
                </section>


                <section>

          
                <div class="container">

                    <div class="co-lg-12">
                        <div class="col-lg-10 center">
                            <asp:ValidationSummary ID="RequiredBodySummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="Contact" />
                            <h3>Contact</h3>
                        </div>

                        
                        <div class="col-lg-8">
                            <label>Send To</label>
                            <asp:DropDownList ID="SendToDDL" runat="server" CssClass="form-control">
                                <asp:ListItem Value="0">Your Property Manager</asp:ListItem>
                                <asp:ListItem Value="1">Domus Support</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                         <div class="col-lg-8">
                            <label>Name</label>
                             <!-- Cmallory - Made Nametxt field be readonly -->
                            <asp:TextBox ID="Nametxt" runat="server" CssClass="form-control" ValidationGroup="contact" ReadOnly="true" ></asp:TextBox>
                        </div>
                        <div class="col-lg-8">
                            <label>Phone Number (Optional)</label>
                           
                            <asp:TextBox ID="Phonetxt" runat="server" CssClass="form-control"></asp:TextBox>
                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="Phonetxt" ValidChars="123456789-" />
                        </div>

                        <div class="col-lg-8">
                            <label>Message</label>
                            <asp:TextBox ID="msgtxt" runat="server" CssClass="form-control" ValidationGroup="Contact" Rows="10" TextMode="MultiLine" AutoPostBack="false"></asp:TextBox>
                        </div>

                        <!-- CMallory - Added click event to button -->
                        <div class="col-lg-8">
                            <asp:Button ID="btnSend" CssClass="btn btn-primary btn-lg" runat="server" OnClick="btnSend_Click" ValidationGroup="contact" Text="Send" />

                        </div>
                    </div>

                </div>
                      </section>

            </ContentTemplate>

        </asp:UpdatePanel>

        <asp:RequiredFieldValidator ID="RequiredBodyValidator"
                ErrorMessage="* Required field"
                Display="None"
                ControlToValidate="msgtxt"
                runat="server"
                ValidationGroup="Contact" />

        <asp:ValidationSummary ID="ValidationSummary"
                                    DisplayMode="BulletList"
                                    runat="server"
                                    ValidationGroup="Contact" />
    </div>

</asp:Content>
