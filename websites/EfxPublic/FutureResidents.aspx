﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FutureResidents.aspx.cs" Inherits="EfxPublic.FutureResidents" %>

<%@ Register Src="~/UserControls/TopNavMenu2.ascx" TagPrefix="uc1" TagName="TopNavMenu2" %>
<%@ Register Src="~/UserControls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Domus - Premier Multifamily Financial Services</title>
    <link id="PublicFavicon" rel="icon" type="image/x-icon" href="/DomusIcon.ico" />
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/agency.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body id="page-top" class="index">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

            <uc1:TopNavMenu2 runat="server" ID="TopNavMenu2" />

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                     <header id="Residents" class="text-center tall text-center" style="background-position: center; color: #FFFFFF; background-image: url(/img/RegisterHeader2.png); background-repeat: no-repeat;">
                        <div class="container text-center">
                            <div class="row">
                                <div class="intro-text">
                                    <div class="intro-lead-in text-shadow">Apply for your new home below!</div>
                                </div>
                            </div>
                        </div>
                    </header>
                    
                    <section id="applySection">
                        <div class="container">
                            <div class="col-lg-12 text-center"><h3>Fill out the form below to send your application to the property.</h3></div>
                            <div class="col-lg-12"><br /></div>
                            <div class="col-md-12">
                                <div class=" form-group col-lg-6">
                                    <div class="col-lg-12"><label>First Name: </label> <asp:RequiredFieldValidator ControlToValidate="firstNametxt" ID="requiredField2" runat="server" ErrorMessage="<i class='fa fa-exclamation-circle' aria-hidden='true'></i>" /></div>
                                    <div class="col-lg-12"><asp:TextBox runat="server" ID="firstNametxt" CssClass="form-control"/></div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <div class="col-lg-12"><label>Last Name: </label> <asp:RequiredFieldValidator ControlToValidate="lastNametxt" ID="requiredField3" runat="server" ErrorMessage="<i class='fa fa-exclamation-circle' aria-hidden='true'></i>" /></div>
                                    <div class="col-lg-12"><asp:TextBox runat="server" ID="lastNametxt" CssClass="form-control"/></div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <div class="col-lg-12"><label>Email Address: </label> <asp:RequiredFieldValidator ControlToValidate="emailAddresstxt" ID="requiredField4" runat="server" ErrorMessage="<i class='fa fa-exclamation-circle' aria-hidden='true'></i>" /></div>
                                    <div class="col-lg-12"><asp:TextBox runat="server" ID="emailAddresstxt" CssClass="form-control"/></div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <div class="col-lg-12">
                                        <label>Phone Number: </label> 
                                        <asp:RequiredFieldValidator ControlToValidate="phoneNumtxt" ID="requiredField5" runat="server" ErrorMessage="<i class='fa fa-exclamation-circle' aria-hidden='true'></i>" />
                                        <asp:CustomValidator ControlToValidate="phoneNumtxt" ID="customValidation1" runat="server" ErrorMessage="" />
                                    </div>
                                    <div class="col-lg-12"><asp:TextBox runat="server" ID="phoneNumtxt" CssClass="form-control"/></div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-lg-12">
                                        <label>Property Name: </label> 
                                        <asp:RequiredFieldValidator ValidationGroup="propertyNameGroup" ControlToValidate="propertyNametxt" ID="requiredField1" runat="server" ErrorMessage="<i class='fa fa-exclamation-circle' aria-hidden='true'> </i>" />
                                        <asp:RegularExpressionValidator ValidationGroup="propertyNameGroup" ID="regexValidator1" ValidationExpression="[a-z A-Z0-9]{5,}" runat="server" ControlToValidate="propertyNametxt" ErrorMessage="<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Please enter 5 or more characters" />
                                        <asp:CustomValidator ValidationGroup="propertyNameGroup" ControlToValidate="propertyNametxt" ID="customValidation2" runat="server" ErrorMessage="" />
                                    </div>
                                    <div class="col-lg-6"><asp:TextBox runat="server" ID="propertyNametxt" CssClass="form-control"/></div>
                                    <div class="col-lg-3"><asp:Button runat="server" ID="searchPropertybtn" ValidationGroup="propertyNameGroup" Text="Search for a Property" OnClick="searchPropertybtn_Click" CssClass="btn btn-primary form-control" /></div>
                                </div>
                                <div class="col-lg-12">
                                    <asp:GridView ID="propertyListGrid" runat="server" CssClass="table table-responsive" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" visible="false" OnDataBound="propertyListGrid_DataBound" >    
                                        <Columns>
                                            <asp:TemplateField  SortExpression="PropertyId" HeaderText="PropertyId" Visible="False" >
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="propertyIDlbl" Text='<%# Eval("PropertyId") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField SortExpression="PropertyName" HeaderText="Property Name">
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" ID="propertyNamelnkbtn" Text='<%# Eval("PropertyName") %>' OnClick="propertyNamelnkbtn_Click" CausesValidation="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="City" HeaderText="City" ReadOnly="True" SortExpression="City" />
                                            <asp:BoundField DataField="StateProvinceName" HeaderText="State" SortExpression="StateProvinceName" />
                                            <asp:BoundField DataField="PostalCode" HeaderText="Zip Code" SortExpression="PostalCode" />
                                        </Columns>
                                        <SelectedRowStyle BackColor="#BADC02" />
                                        <EmptyDataTemplate>
                                            No properties were found
                                        </EmptyDataTemplate>
                                    </asp:GridView>

                                    <asp:SqlDataSource ID="propertyListSql" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="usp_Property_GetListForApplicant_Search" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="false">
                                        <SelectParameters>
                                            <asp:Parameter Name="propName" Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </div>

                                
                            </div>

                        </div>
                    </section>
            </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="downloadlnkbtn" />
                    <asp:PostBackTrigger ControlID="submitbtn" />
                </Triggers>
            </asp:UpdatePanel>
                <div class="container">
                    <div class="col-md-12">
                        <div class="form-group col-lg-12">
                            <div class="col-lg-6">
                                <div class="col-lg-12">
                                    <label>Upload Application</label>
                                    <asp:CustomValidator ControlToValidate="applicationUpload" ID="customValidation3" runat="server" ErrorMessage="" />
                                </div>
                                <div class="col-lg-12">
                                    <%--<i class="fa fa-upload fa-5x"></i>--%>
                                    <asp:FileUpload ID="applicationUpload" runat="server" CssClass="btn btn-default" /><br />
                                    <%--<asp:Button ID="uploadbtn" runat="server" Text="Upload" CausesValidation="False" CssClass="btn btn-default" OnClick="uploadbtn_Click" />--%>
                                </div>
                            </div>
                            <asp:updatePanel runat="server" ID="UpdatePanel2">
                                <ContentTemplate>
                                    <div class="col-lg-6" runat="server" id="applicationArea" visible="false">
                                        <div class="col-lg-5"><label>Need the Application?<br />Click Here to download it </label></div>
                                        <div class="col-lg-7"><asp:LinkButton runat="server" ID="downloadlnkbtn" OnClick="downloadApplnkbtn_Click" CausesValidation="false"><i class="fa fa-download fa-3x"></i></asp:LinkButton></div>
                                    </div>
                                </ContentTemplate>
                            </asp:updatePanel>
                            

                        </div>
                        <div class="form-group col-lg-12" id="submitArea" runat="server">
                            <asp:Button runat="server" ID="submitbtn" Text="Submit Application" OnClick="submitbtn_Click" CssClass="btn btn-primary" />
                            <asp:Label runat="server" ID="errorlbl" Text="" />
                        </div>
                    </div>
                </div>







             <uc1:Footer runat="server" ID="Footer" />

                <!-- jQuery -->
                <script src="js/jquery.js"></script>

                <!-- Bootstrap Core JavaScript -->
                <script src="js/bootstrap.min.js"></script>

                <!-- Plugin JavaScript -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
                <script src="js/classie.js"></script>
                <script src="js/cbpAnimatedHeader.js"></script>

                <!-- Contact Form JavaScript -->
                <script src="js/jqBootstrapValidation.js"></script>
                <script src="js/contact_me.js"></script>

                <!-- Custom Theme JavaScript -->
                <script src="js/agency.js"></script>

        </div>
    </form>
</body>
</html>
