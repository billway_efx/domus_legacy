﻿using System;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using EfxFramework.Mobile;

namespace EfxPublic.DataAccess
{

    public class PaySubmitItems
    {
        private string _session;
        private string _renterID;
        private string _paymentMethodID;
        private string _convFee;
        public string session { get; set; }
        public string renterid { get; set; }
        public string paymentMethodID { get; set; }
        public string ConvFee { get; set; }
    }

    public class PaymentResponse
    {
        public string Message;
        public string Result;
        public string Status;
    }


    public class PaymentDetails
    {
        public string FullName;
        public int PaymentID;
        public DateTime TransDate;
        public decimal PaymentAmount;
        public decimal ConvFee;
        public decimal TotalPayment;
        public string TransID;

    }

    public class PayableItems
    {
        public decimal CurrentBalanceDue;
        public decimal FeeTotal;
        public decimal PayLimit;
        public int RenterID;
    }


    public class Payments
    {

        public static string GetPayableItemsForRenter(string session, string RenterID)
        {
            string EndPoint = session + "/" + RenterID;
            string Method = "/GetPayableItemsForRenter/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);
            return Resp;
        }



        ///Wallet_PaySingleItemForRenterWithNewCC/{SessionId}/{RenterId}/{PayableItemId}/{AmountToPay}/{CCHolderName}/{CCAccountNumber}/{CCExpMonth}/{CCExpYear}/{CardSecurityCode}
        public static PaymentResponse Wallet_PaySingleItemForRenterWithNewCC(string RenterSession, string RenterID, string PayableItemID, string amount, string CCHolderName, string CCAcount, string CCExpMonth, string CCExpYear, string CVV)
        {
            string EndPoint = RenterSession + "/" + RenterID + "/" + PayableItemID + "/" + amount + "/" + CCHolderName + "/" + CCAcount + "/" + CCExpMonth + "/" + CCExpYear + "/" + CVV;
            string Method = "/Wallet_PaySingleItemForRenterWithNewCC/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);

            var p = PaymentReadResponse("Wallet_PaySingleItemResponse", Resp);

            return p;
        }


        ///Wallet_PaySingleItemForRenter/{SessionId}/{RenterId}/{PaymentMethodId}/{PayableItemId}/{AmountToPay}
        public static PaymentResponse PaySingleItemForRenter(string RenterSession, string RenterID, string PayerWalletID, string PayableItemID, string amount)
        {
            string EndPoint = RenterSession + "/" + RenterID + "/" + PayerWalletID + "/" + PayableItemID + "/" + amount;
            string Method = "/Wallet_PaySingleItemForRenter/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);

            var p = PaymentReadResponse("Wallet_PaySingleItemResponse", Resp);

            return p;
        }


        ///Wallet_PaySingleItemForRenterWithUpdateToCCMethod/{SessionId}/{RenterId}/{PaymentMethodId}/{PayableItemId}/{AmountToPay}/{CCHolderName}/{CCAccountNumber}/
        ///{CCExpMonth}/{CCExpYear}/{CardSecurityCode}
        public static string PaySingleItemForRenterWithUpdateToCCMethod(string _session, string _renterID, string paymethodid, string payableitemid,string amount, string ccholdername, string ccaccountnumber, 
           string CCexpMonth, string expyear, string csc)
        {
            string EndPoint = _session + "/" + _renterID + "/" + paymethodid + "/" + payableitemid + "/" + amount + "/" + ccholdername + "/" + ccaccountnumber + "/" + CCexpMonth + "/" + expyear + "/" + csc;
            string Method = "/Wallet_PaySingleItemForRenterWithUpdateToCCMethod/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);
            return Resp;
        }


        
        ///Wallet_PaySingleItemForRenterWithCSC/{SessionId}/{RenterId}/{PayerWalletID}/{PayableItemId}/{AmountToPay}/{CardSecurityCode}
        public static PaymentResponse PaySingleItemForRenterWithCSC(string session, string RenterID, string WalletID, string PayableItemID, string Amount, string CSC)
        {
            string EndPoint = session + "/" + RenterID + "/" + WalletID + "/" + PayableItemID + "/" + Amount + "/" + CSC;
            string Method = "/Wallet_PaySingleItemForRenterWithCSC/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);

            var p = PaymentReadResponse("Wallet_PaySingleItemResponse", Resp);

            return p;
        }


        //Wallet_PaySingleItemForRenterWithNewAch/{SessionId}/{RenterId}/{PayableItemId}/{A mountToPay}/{AccountTypeId}/{AccountNumber}/{RoutingNumber}
        public static PaymentResponse PaySingleItemForRenterWithNewAch(string session, string renterID, string PayableItemID, string Amount, string AccountTypeID, string AccountNumber, string RoutingNumber)
        {
            string EndPoint = session + "/" + renterID + "/" + PayableItemID + "/" + Amount + "/" + AccountTypeID + "/" + AccountNumber + "/" + RoutingNumber;
            string Method = "/Wallet_PaySingleItemForRenterWithNewAch/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);

            var p = PaymentReadResponse("Wallet_PaySingleItemResponse", Resp);

            return p;
        }

        //"/Wallet_PayWithCash/{RenterID}/{Amount}"

        public static PaymentResponse PayWithCash(string RenterID, string Amount)
        {
            //string EndPoint = RenterID + "/" + Amount;
            //string Method = "/Wallet_PayWithCash/";
            //string Resp = DataAccess.Response.WebResponse(Method + EndPoint);

            //var p = PayNearMeResp("Wallet_PayByCash", Resp);

            //return p;


            //SWitherspoon: Copied PayWithCash function from Websites2 to circumvent the above API call
            Wallet_PayByCash Response = new Wallet_PayByCash();

            PaymentResponse pmResponse = new PaymentResponse();
            int _RenterID = int.Parse(RenterID);

            EfxFramework.Renter Resident = new EfxFramework.Renter(_RenterID);

            EfxFramework.Company currentCompany = EfxFramework.Company.GetCompanyByRenterId(Resident.RenterId);
            var Property = EfxFramework.Property.GetPropertyByRenterId(Resident.RenterId);

            var Address = EfxFramework.Address.GetAddressByRenterIdAndAddressType(Resident.RenterId, (int)EfxFramework.TypeOfAddress.Billing);
            Address.AddressTypeId = (int)EfxFramework.TypeOfAddress.Billing;
            Address.AddressLine1 = Resident.StreetAddress;
            Address.City = Resident.City;
            Address.StateProvinceId = (int)Resident.StateProvinceId;
            Address.PostalCode = Resident.PostalCode;
            Address.PrimaryEmailAddress = Resident.PrimaryEmailAddress;
            Address.PrimaryPhoneNumber = Resident.MainPhoneNumber;

            RPO.PayNearMeRequest pnmRequest = new RPO.PayNearMeRequest();
            pnmRequest.Amount = decimal.Parse(Amount);
            pnmRequest.CreatorIdentifier = Resident.PublicRenterId.ToString();
            pnmRequest.Currency = "USD";
            pnmRequest.CustomerCity = Address.City;
            pnmRequest.CustomerEmail = Address.PrimaryEmailAddress;
            pnmRequest.CustomerIdentifier = Resident.PublicRenterId.ToString();
            pnmRequest.CustomerName = Resident.FirstName + " " + Resident.LastName;
            pnmRequest.CustomerPhone = Address.PrimaryPhoneNumber;
            pnmRequest.CustomerPostalCode = Address.PostalCode;
            pnmRequest.CustomerState = Address.GetStateName();
            pnmRequest.CustomerStreet = Address.AddressLine1;
            pnmRequest.MinimumPaymentAmount = 1;
            pnmRequest.MinimumPaymentCurrency = "USD";
            pnmRequest.OrderDescription = "Resident Payment";
            pnmRequest.OrderDuration = 1;
            pnmRequest.OrderIdentifier = Guid.NewGuid().ToString();
            pnmRequest.OrderType = RPO.PayNearMeRequest.PNMOrderType.Exact;
            pnmRequest.RenterId = Resident.RenterId;
            pnmRequest.PropertyId = (int)Property.PropertyId;
            pnmRequest.CompanyId = currentCompany.CompanyId;
            pnmRequest.SiteIdentifier = currentCompany.PNMSiteId;
            pnmRequest.SecretKey = currentCompany.PNMSecretKey;
            pnmRequest.Version = EfxFramework.EfxSettings.PNMVersion;

            var Log = new RPO.ActivityLog();
            Log.WriteLog(Resident.PrimaryEmailAddress, "PNM Create New Ticket", 0, Resident.RenterId.ToString(), false);


            RPO.RentByCash rentByCash = new RPO.RentByCash();
            rentByCash.SandBoxMode = bool.Parse(EfxFramework.EfxSettings.PNMUseSandboxMode);
            string[] arg = new string[1];
            string PNM_Response = rentByCash.CreatePayNearMeRequest(pnmRequest, rentByCash.SandBoxMode);
            arg = PNM_Response.ToString().Split(';');

            Response.Status = arg[0];
            Response.OrderUrl = arg[1];

            if (Response.OrderUrl == "Error")
            {

                pmResponse.Result = "Error";
                pmResponse.Status = Response.Status;
            }
            else
            {
                pmResponse.Message = Response.OrderUrl;
                pmResponse.Status = Response.Status;
                pmResponse.Result = "Success";
            }

            return pmResponse;
        }



        public static decimal GetConvFee(int ProgramID, string PaymentMethod, decimal amount)
        {
            string PM = "";
            switch(PaymentMethod)
            {
                case "true":
                        PM = "CC";
                        break;
                case "false":
                        PM = "CK";
                        break;
                default:
                    PM = "CK";
                    break;
            }

            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("Wallet_GetConvFeeByProgramID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ProgramID", SqlDbType.Int).Value = ProgramID;
            com.Parameters.Add("@PaymentMethod", SqlDbType.NVarChar, 30).Value = PM;
            com.Parameters.Add("@Amount", SqlDbType.Decimal).Value = amount;
            com.Parameters.Add("@ConvFee_output", SqlDbType.Money).Direction = ParameterDirection.Output;

            decimal ConvFeeAmount = 0m;
            try
            {

                con.Open();
                com.ExecuteNonQuery();
                ConvFeeAmount = (decimal)com.Parameters["@ConvFee_output"].Value;
                con.Close();
            }
            catch(Exception ex)
            {
                con.Close();
                throw ex;
            }

            return ConvFeeAmount;

        }

        public static decimal GetConvFeeByRenterID(int renterID, string PaymentMethod, decimal amount)
        {
            string PM = "";
            switch (PaymentMethod)
            {
                case "true":
                    PM = "CC";
                    break;
                case "false":
                    PM = "CK";
                    break;
                default:
                    PM = "CK";
                    break;

            }

            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("Wallet_GetConvFeeByRenterID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@RenterID", SqlDbType.Int).Value = renterID;
            com.Parameters.Add("@PaymentMethod", SqlDbType.NVarChar, 30).Value = PM;
            com.Parameters.Add("@Amount", SqlDbType.Decimal).Value = amount;
            com.Parameters.Add("@ConvFee_output", SqlDbType.Money).Direction = ParameterDirection.Output;

            decimal ConvFeeAmount = 0m;
            try
            {

                con.Open();
                com.ExecuteNonQuery();
                ConvFeeAmount = (decimal)com.Parameters["@ConvFee_output"].Value;
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

            return ConvFeeAmount;

        }

        public static PayableItems GetItems(string xml)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(xml);

            using (var OutputStream = new MemoryStream(byteArray))
            {
                PayableItems p = new PayableItems();
                var XmlResp = XDocument.Load(OutputStream);
                XElement RespElement = null;

                try
                {
                    RespElement = DataAccess.RenterDataAccess.GetRootElement(XmlResp.Root, "PayablesResponse");

                }
                catch
                {
                   
                }

                try
                {
                    foreach (XElement Response in RespElement.Elements())
                    {
                        string node = Response.Name.LocalName;

                        if (node == "PayableItems")
                        {
                            foreach(XElement PayableItems in Response.Elements())
                            {
                                string node2 = PayableItems.Name.LocalName;
                                if (node2 == "PayableItem")
                                {
                                    foreach(XElement Items in PayableItems.Elements())
                                    {
                                        string node3 = Items.Name.LocalName;

                                        if(node3 == "CurrentBalanceDue")
                                        {
                                            p.CurrentBalanceDue = decimal.Parse(Items.Value);
                                        }

                                    }
                                }
                            }
                            
                        }

                    }

                    return p;

                }
                catch(Exception ex)
                {
                    throw ex;
                }

               
            }


        }

        public static PaymentResponse PaymentReadResponse(string _Element, string _xml)
        {
            PaymentResponse P = new PaymentResponse();
            byte[] byteArray = Encoding.ASCII.GetBytes(_xml);

            if(_xml == "Error")
            {
                P.Message = "Error";
                P.Result = "Error";
                return P;
            }

            using (var OutputStream = new MemoryStream(byteArray))
            {

                
                var XmlResp = XDocument.Load(OutputStream);
                XElement RespElement = null;

                try
                {
                    RespElement = DataAccess.RenterDataAccess.GetRootElement(XmlResp.Root, _Element);
                }
                catch
                {
                    P.Message = "Error";
                    P.Result = "Error";
                    return P;
                }

                string Msg = "";

                try
                {
                    foreach (XElement Response in RespElement.Elements())
                    {
                        string node = Response.Name.LocalName;

                        if (node == "Message")
                        {
                            Msg = Response.Value.ToString();
                            if (Msg == "Session timed out")
                            {
                                FormsAuthentication.SignOut();
                                HttpContext.Current.Response.Redirect("~/ResidentLogin.aspx");
                            }

                            if (Msg.Contains("Error reading database"))
                            {
                                string Error = "Error";
                                P.Message = Error;
                            }

                            if (Msg.Contains("success"))
                            {
                                P.Message = Msg;
                            }

                        }

                        if (node == "Result")
                        {
                            P.Result = Response.Value.ToString();
                        }

                        

                    }
                }
                catch (Exception ex)
                {
                    //Return the Error to caller
                    P.Message = "Error";
                    P.Result = "Error" + ex.Message;
                }

                return P;
            }

        }

        public static PaymentResponse PayNearMeResp(string _Element, string _xml)
        {
            PaymentResponse P = new PaymentResponse();
            byte[] byteArray = Encoding.ASCII.GetBytes(_xml);

            if (_xml == "Error")
            {
                P.Message = "Error";
                P.Result = "Error";
                return P;
            }

            using (var OutputStream = new MemoryStream(byteArray))
            {


                var XmlResp = XDocument.Load(OutputStream);
                XElement RespElement = null;

                try
                {
                    RespElement = DataAccess.RenterDataAccess.GetRootElement(XmlResp.Root, _Element);
                }
                catch
                {
                    P.Message = "Error";
                    P.Result = "Error";
                    return P;
                }

                string Msg = "";

                try
                {
                    foreach (XElement Response in RespElement.Elements())
                    {
                        string node = Response.Name.LocalName;

                        if (node == "Message")
                        {
                            Msg = Response.Value.ToString();
                            if (Msg.Contains("https"))
                            {
                                P.Message = Response.Value.ToString();
                            }
                           
                        }

                        if (node == "Result")
                        {
                            P.Result = Response.Value.ToString();
                        }

                        if(node == "Status")
                        {
                            P.Status = Response.Value.ToString();
                        }

                    }
                }
                catch (Exception ex)
                {
                    //Return the Error to caller
                    P.Message = "Error";
                    P.Result = "Error" + ex.Message;
                }

                return P;
            }



        }

        //usp_PaymentGetlastPaymentByRenterID
        public static PaymentDetails GetDetailsForConfirmPage(int RenterID)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_PaymentGetlastPaymentByRenterID", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@RenterID", SqlDbType.Int).Value = RenterID;

            try
            {
                con.Open();
                using (SqlDataReader Reader = com.ExecuteReader())
                {
                    PaymentDetails pd = new PaymentDetails();
                    Reader.Read();

                    if(Reader.HasRows)
                    {
                        pd.FullName = Reader["FullName"].ToString();
                        pd.PaymentAmount = (decimal)Reader["RentAmount"];
                        pd.ConvFee = (decimal)Reader["ConvenienceFee"];
                        pd.TotalPayment = (decimal)Reader["TotalAmount"];
                        pd.TransID = Reader["TransactionId"].ToString();
                        pd.TransDate = (DateTime)Reader["TransactionDate"];

                        return pd;
                    }

                    return pd;
                }

            }
            catch(Exception ex)
            {
                con.Close();
                throw ex;
            }

        }



        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }



    }//END Class Payments



}