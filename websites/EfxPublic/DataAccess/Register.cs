﻿using System;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using EfxFramework;
using EfxFramework.Security.Authentication;

//cakel: This class file is for the DOMUS site.  This will replace the existing RPO stuff


namespace EfxPublic.DataAccess
{

    public class ExistingRenter
    {
        public string IsExisting;
    }


    public class Register
    {


        public static SqlDataReader GetPublicPropertyList(string PropertyName, string zipCode)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Property_GetListForRegistration", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@PropName", SqlDbType.NVarChar, 50).Value = PropertyName;
            com.Parameters.Add("@Zip", SqlDbType.NVarChar, 20).Value = zipCode;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);

            }
            catch(Exception ex)
            {
                con.Close();
                throw ex;
            }

        }

        public static ExistingRenter IsCurrentRenter(string EmailAddress)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Renter_CheckExistingStatus", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@EmailAddress", SqlDbType.NVarChar, 150).Value = EmailAddress;
            try
            {
                con.Open();
                using (SqlDataReader Reader = com.ExecuteReader())
                {
                    ExistingRenter ER = new ExistingRenter();
                    Reader.Read();
                    if (Reader.HasRows)
                    {
                        ER.IsExisting = Reader["RenterStatus"].ToString();

                        Reader.Close();
                        con.Close();
                        return ER;
                    }
                    else
                    {

                        return null;

                    }

                }
                
            }
            catch(Exception ex)
            {
                con.Close();
                throw ex;
            }
           
        }


        public static SqlDataReader StateList()
        {
            //usp_StateProvince_GetAll
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_StateProvince_GetAll", con);
            com.CommandType = CommandType.StoredProcedure;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);

            }
            catch
            {
                con.Close();
                return null;
            }

        }




        public static SqlDataReader GetRPOIdSearchResults(int propid, string firstname, string lastname, decimal rentamount)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Renter_GetRenterPMSID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropID", SqlDbType.Int).Value = propid;
            com.Parameters.Add("@FirstName", SqlDbType.NVarChar, 100).Value = firstname;
            com.Parameters.Add("@LastName", SqlDbType.NVarChar, 100).Value = lastname;
            com.Parameters.Add("@RentAmount", SqlDbType.Decimal).Value = rentamount;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);

            }
            catch
            {
                con.Close();
                return null;
            }

        }


        //Imports Single Renter into DB (Non-Integrated)
        public static int Renter_RegisterNonIntegratedRenter(string FirstName, string LastName, string MainPhoneNumber,  string PrimaryEmailAddress, string StreetAddress, 
            string Unit, string City,  int StateId,  string PostalCode, bool IsParticipating, int RenterPropertyId, string Password)
        {
            //Generate a new random password
            //var TempPassword = AuthenticationTools.GenerateRandomPassword();

            //Generate a new salt and set it
            byte[] _Salt = AuthenticationTools.GenerateSalt();

            //Hash the new password and set it
            byte[] _PasswordHash = AuthenticationTools.HashPassword(Password, _Salt);

            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Renter_RegisterNonIntegratedRenter", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@FirstName", SqlDbType.NVarChar, 100).Value = FirstName;
            com.Parameters.Add("@LastName", SqlDbType.NVarChar, 100).Value = LastName;
            com.Parameters.Add("@MainPhoneNumber", SqlDbType.NVarChar, 50).Value = MainPhoneNumber;
            com.Parameters.Add("@PrimaryEmailAddress", SqlDbType.NVarChar, 100).Value = PrimaryEmailAddress;
            com.Parameters.Add("@StreetAddress", SqlDbType.NVarChar, 100).Value = StreetAddress;
            com.Parameters.Add("@Unit", SqlDbType.NVarChar, 20).Value = Unit;
            com.Parameters.Add("@City", SqlDbType.NVarChar, 100).Value = City;
            com.Parameters.Add("@StateID", SqlDbType.Int).Value = StateId;
            com.Parameters.Add("@PostalCode", SqlDbType.NVarChar, 20).Value = PostalCode;
            com.Parameters.Add("@IsParticipating", SqlDbType.Bit).Value = IsParticipating;
            com.Parameters.Add("@PasswordHash", SqlDbType.Binary).Value = _PasswordHash;
            com.Parameters.Add("@Salt", SqlDbType.Binary).Value = _Salt;
            com.Parameters.Add("@RenterPropertyId", SqlDbType.Int).Value = RenterPropertyId;
            com.Parameters.Add("@RenterIdOutput", SqlDbType.Int).Direction = ParameterDirection.Output;
            try
            {
                int _RenterID = 0;
                con.Open();
                com.ExecuteNonQuery();
                _RenterID = (int)com.Parameters["@RenterIdOutput"].Value;

                return _RenterID;
            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                con.Close();
            }

        }

        //cakel: This will register Integrated residents to have access to the site;
        public static int Renter_RegisterIntegratedRenter(int propertyid, string emailaddress, string pmsid, string password)
        {
            //Generate a new salt and set it
            byte[] _Salt = AuthenticationTools.GenerateSalt();

            //Hash the new password and set it
            byte[] _PasswordHash = AuthenticationTools.HashPassword(password, _Salt);

            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Renter_RegisterIntegratedRenter", con);
             com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PrimaryEmailAddress", SqlDbType.NVarChar, 100).Value = emailaddress;
            com.Parameters.Add("@pmsid", SqlDbType.NVarChar, 50).Value = pmsid;
            com.Parameters.Add("@PasswordHash", SqlDbType.Binary).Value = _PasswordHash;
            com.Parameters.Add("@Salt", SqlDbType.Binary).Value = _Salt;
            com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = propertyid;
            com.Parameters.Add("@RenterID_Out", SqlDbType.Int).Direction = ParameterDirection.Output;

            try
            {
                int _RenterID = 0;
                con.Open();
                com.ExecuteNonQuery();
                _RenterID = (int)com.Parameters["@RenterID_Out"].Value;

                return _RenterID;
            }
            catch (Exception ex)
            {
                string innerEx = "";
                innerEx = ex.InnerException.ToString();
                throw ex;

            }
            finally
            {
                con.Close();
            }


        }


        public static void SendRenterWelcomeEmail(int RenterID)
        {
            string MailBody = "";
           

            MailMessage WelcomeEmail = new MailMessage();
            WelcomeEmail.Subject = "Welcome to Domus";
            WelcomeEmail.Body = MailBody;
            WelcomeEmail.From = new MailAddress("support@rentpaidonline.com", "RPO Support");
            WelcomeEmail.To.Add(new MailAddress(EfxSettings.SupportEmail, "RPO Support Admin"));
            WelcomeEmail.IsBodyHtml = true;
            SmtpClient RpoSmtp = new SmtpClient();

            //cakel: Using this for local testing
            RpoSmtp.Host = EfxSettings.SmtpHostLocal;

            RpoSmtp.Send(WelcomeEmail);

        }




        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

    }
}