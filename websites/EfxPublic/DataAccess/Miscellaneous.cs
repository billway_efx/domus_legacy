﻿using RPO;
using System;
using System.Data;
using System.Data.SqlClient;

namespace EfxPublic.DataAccess
{
   public class Miscellaneous
    {
        public static SqlDataReader SendContactEmail(int RenterID, string UsersEmailAddress, string Name, string Body, string PhoneNumber, string SourceDestination)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("Email_SendDomusContactPage", con);
            ActivityLog log = new ActivityLog();

            // Added to debug where emails are coming from.  
            log.WriteLog("Renter ID = " + RenterID.ToString(), "Sending Contact EMail", (short)LogPriority.LogDebugging, RenterID.ToString(), false);
            
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@RenterID", SqlDbType.Int).Value = RenterID;
            com.Parameters.Add("@UsersEmailAddress", SqlDbType.NVarChar, 100).Value = UsersEmailAddress;
            com.Parameters.Add("@Name", SqlDbType.NVarChar, 100).Value = Name;
            com.Parameters.Add("@EmailBody", SqlDbType.NVarChar).Value = Body;
            com.Parameters.Add("@PhoneNumber", SqlDbType.NVarChar, 20).Value = PhoneNumber;
            com.Parameters.Add("@SourceDestination", SqlDbType.NVarChar, 50).Value = SourceDestination;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch(Exception ex)
            {
                con.Close();
                throw ex;
            }
        }

        //CMallory - 00650 - Added - Send's a link to the user's email address that'll allow them to reset their password.
        public static SqlDataReader SendPasswordResetEmail(string EmailAddress, string BaseURL)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("Email_SendPasswordReset", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@Email", SqlDbType.NVarChar, 100).Value = EmailAddress;
            com.Parameters.Add("@BaseURL", SqlDbType.NVarChar, 200).Value = BaseURL;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch(Exception ex)
            {
                con.Close();
                throw ex;
            }
        }

        //CMallory - 00650 - Added - Retrieve all of the data related to a password reset record.
        public static SqlDataReader GetPasswordReset(string GUID)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_PasswordReset_GetPasswordReset", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@GUID", SqlDbType.NVarChar, 200).Value = GUID;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch(Exception ex)
            {
                con.Close();
                throw ex;
            }
        }

        //CMallory - 00650 - Added - Deactivate password reset record so it can't be used again.
        public static SqlDataReader DisablePasswordReset(string GUID)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_PasswordReset_DisablePasswordReset", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@GUID", SqlDbType.NVarChar, 200).Value = GUID;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }
        }

        //CMallory - 00650 - Added - Reset's user's password.
        public static void ResetPassword(int renterID, byte[] pw, byte[] salt)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Renter_SetNewPassword", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@id", SqlDbType.Int).Value = renterID;
            com.Parameters.Add("@pw", SqlDbType.Binary, 64).Value = pw;
            com.Parameters.Add("@salt", SqlDbType.Binary, 64).Value = salt;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

            finally
            {
                con.Close();
            }
        }
        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }
    }
}