﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Text;
using System.IO;
using System.Net;

namespace EfxPublic.DataAccess
{



    public class RenterDetails
    {
        public int renterID;
        public string leaseID;
        public decimal rentAmount;
        public DateTime paymentDueDate;
        public DateTime LeaseStart;
        public DateTime LeaseEnd;
        public int IsIntegrated;  //value = 1 for integrated or NO for non-integrated
        public string AcceptedPaymentTypeId;
        public string FirstName;
        public string LastName;
        public string RenterEmail;
        public string RenterStreetAddress;
        public string RenterUnit;
        public string RenterCity;
        public string RenterState;
        public byte[] Password;
        public byte[] salt;

        public int AcceptedPaymentType;
        public string PropertyName;
        public string PropertyManagerName;
        public bool CanViewMaintenance;
    }


    public class RenterLeaseDetails
    {
        public int LeaseID;
        public decimal CurrentBalanceDue;
        public decimal RentAmount;
        public decimal TotalFees;

    }


    public class RenterResponse
    {
        public int RenterID;
        public string RenterFirstName;
        public string RenterLastName;
        public string RenterEmail;
        public string RenterAddress;
        public string RenterCity;
        public string RenterStateAbr;
        public string RenterZip;
    }


    public class RenterDataAccess
    {
        public static RenterDetails GetRenterDetailsByUserName(string Email)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Renter_GetRenterDetailsByUserName", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@UserName", SqlDbType.NVarChar, 150).Value = Email;
            con.Open();
            SqlDataReader reader = com.ExecuteReader();
            reader.Read();

            RenterDetails CurrentRenter = new RenterDetails();

            if (reader.HasRows)
            {
                CurrentRenter.leaseID = reader["LeaseId"].ToString();
                CurrentRenter.renterID = (int)reader["RenterID"];
                CurrentRenter.rentAmount = (decimal)reader["RentAmount"];
                CurrentRenter.paymentDueDate = Convert.ToDateTime(reader["PaymentDueDate"]);
                CurrentRenter.LeaseStart = Convert.ToDateTime(reader["StartDate"]);
                CurrentRenter.LeaseEnd = Convert.ToDateTime(reader["EndDate"]);
                CurrentRenter.IsIntegrated = (int)reader["IsIntegerated"];
                CurrentRenter.AcceptedPaymentTypeId = reader["AcceptedPaymentTypeId"].ToString();
                CurrentRenter.FirstName = reader["FirstName"].ToString();
                CurrentRenter.LastName = reader["LastName"].ToString();
                CurrentRenter.RenterState = reader["StateProvinceName"].ToString();
                CurrentRenter.RenterCity = reader["City"].ToString();
                CurrentRenter.RenterStreetAddress = reader["StreetAddress"].ToString();
                CurrentRenter.RenterUnit = reader["Unit"].ToString();
                CurrentRenter.Password = (byte[])reader["PasswordHash"];
                CurrentRenter.salt = (byte[])reader["Salt"];
                CurrentRenter.PropertyName = reader["PropertyName"].ToString();
                CurrentRenter.PropertyManagerName = reader["PropertManagerName"].ToString();
                CurrentRenter.CanViewMaintenance = (bool)reader["CanViewMaintenance"];

            }
            else
            {
                con.Close();
                reader.Close();
                return null;
            }

            con.Close();
            reader.Close();

            return CurrentRenter;
        }

        public static RenterLeaseDetails GetLeaseDetails(int RenterID)
        {
            //usp_Lease_GetTotalDueByRenterID
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Lease_GetTotalDueByRenterID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@RenterID", SqlDbType.Int).Value = RenterID;
            try
            {
                con.Open();
                using(SqlDataReader Reader = com.ExecuteReader())
                {
                    RenterLeaseDetails RL = new RenterLeaseDetails();
                    Reader.Read();

                    if(Reader.HasRows)
                    {
                        RL.LeaseID = (int)Reader["LeaseID"];
                        RL.CurrentBalanceDue = (decimal)Reader["CurrentBalanceDue"];
                        RL.TotalFees = (decimal)Reader["Calc_TotalFees"];

                        return RL;
                    }
                    else
                    {
                        return null;
                    }

                }

            }
            catch
            {
                return null;
            }
        }

        public static SqlDataReader Renter_GetMaintenanceRequestListBYRenterID(int RenterID)
        {
            //usp_Renter_GetMaintenanceRequestListBYRenterID
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Renter_GetMaintenanceRequestListBYRenterID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@RenterID", SqlDbType.Int).Value = RenterID;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch(Exception ex)
            {
                con.Close();
                throw ex;
            }
        }

        public static RenterResponse GetResponse(string xml)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(xml);
            RenterResponse RR = new RenterResponse();

            using (var OutputStream = new MemoryStream(byteArray))
            {
                var XmlResp = XDocument.Load(OutputStream);
                var RespElement = GetRootElement(XmlResp.Root, "GetRenterByRenterIdResponse");

                foreach (XElement Response in RespElement.Elements())
                {
                    string node = Response.Name.LocalName;
                    if (node == "Renter")
                    {
                        foreach (XElement RenterX in Response.Elements())
                        {
                            string RenterNode = RenterX.Name.LocalName;
                            if (RenterNode == "RenterId")
                            {
                                RR.RenterID = int.Parse(RenterX.Value.ToString());
                            }

                            if(RenterNode == "FirstName")
                            {
                                RR.RenterFirstName = RenterX.Value.ToString();

                            }
                            if(RenterNode == "LastName")
                            {
                                RR.RenterLastName = RenterX.Value.ToString();
                            }
                            if(RenterNode == "Address")
                            {
                                foreach(XElement AddressX in RenterX.Elements())
                                {
                                    string AddressNode = AddressX.Name.LocalName;
                                    if (AddressNode == "StreetAddress")
                                    {
                                        RR.RenterAddress = AddressX.Value.ToString();
                                    }
                                    if(AddressNode == "City")
                                    {
                                        RR.RenterCity = AddressX.Value.ToString();
                                    }
                                      
                                }

                            }
                            if(RenterNode == "EmailAddress")
                            {
                                RR.RenterEmail = RenterX.Value.ToString();
                            }

                        }

                    }

                }

                return RR;
            }
        }

        public static string GetRenterDetailsRequest(string RenterSession, string RenterID)
        {
            string EndPoint = RenterSession + "/" + RenterID;
            string Method = "/GetRenterByRenterId/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);
            return Resp;

        }
        
        public static string GetPayableFeeDetail(string sessionID, string renterid)
        {
            string EndPoint = sessionID + "/" + renterid;
            string Method = "/GetPayableFeeDetail/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);
            return Resp;
        }

        //Find Root Element 
        public static XElement GetRootElement(XElement element, string localName)
        {
            XElement Element = null;

            if (element != null && element.Name.LocalName == localName)
                return element;

            if (element != null && element.HasElements)
            {
                foreach (var E in element.Elements())
                {
                    Element = GetRootElement(E, localName);
                }
            }
            return Element;
        }

        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

        public static bool VerifyECheck(string RenterId, DateTime CurrentTime, decimal Amount)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand cmd = new SqlCommand("usp_VerifyECheckCanBeProcessed", con);
            SqlDataReader dr;

            cmd.Parameters.AddWithValue("@RenterId", RenterId);
            cmd.Parameters.AddWithValue("@EndDate", CurrentTime);
            cmd.Parameters.AddWithValue("@Amount", Amount);

            dr = cmd.ExecuteReader();

            return dr.HasRows;
        }


    }//End RenterDataAccess

} //End namespace