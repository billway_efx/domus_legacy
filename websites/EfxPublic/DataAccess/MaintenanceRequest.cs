﻿using System;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using EfxFramework;


namespace EfxPublic.DataAccess
{
    public class MaintenanceRequest
    {

        public static string GetMaintenanceResponse(string _session, string _renterID)
        {
            string EndPoint = _session + "/" + _renterID;
            string Method = "/GetMaintenanceHistory/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);
            return Resp;
        }
      

        public static SqlDataReader GetPriorityList()
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_MaintenanceRequest_GetPriorityList", con);
            com.CommandType = CommandType.StoredProcedure;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch(Exception ex)
            {
                con.Close();
                throw ex;
            }

        }

        public static SqlDataReader GetRequestTypeList()
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_MaintenanceRequest_GetRequestTypeList", con);
            com.CommandType = CommandType.StoredProcedure;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }
        }


        public static string SubmitMaintenanceRequestResponse(string _session, string _renterID, string TypeID, string PriorityID, string MaintenanceDesc, string CCResident)
        {
            //("yyyy-MM-dd HH:mm:ss");
            string SubmissionDate = DateTime.Now.ToString("yyyy-MM-dd");
            
            string EndPoint = _session + "/" + _renterID + "/" + TypeID + "/" + PriorityID + "/" + SubmissionDate + "/" + MaintenanceDesc + "/" + "0";
            string Method = "/SubmitMaintenanceRequest/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);
            return Resp;

        }



        public static string SubmitResponse(string _xml)
        {
            string ResponseString = "";
            byte[] byteArray = Encoding.ASCII.GetBytes(_xml);


            using (var OutputStream = new MemoryStream(byteArray))
            {
                var XmlResp = XDocument.Load(OutputStream);
                var RespElement = DataAccess.RenterDataAccess.GetRootElement(XmlResp.Root, "MaintenanceResponse");

                foreach (XElement Response in RespElement.Elements())
                {
                    string node = Response.Name.LocalName;
                    if (node == "Result")
                    {
                        ResponseString = Response.Value.ToString();
                    }

                }

                return ResponseString;

            }

        }


        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }


    }
}