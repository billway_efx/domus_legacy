﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;


namespace EfxPublic.DataAccess
{
    public class Response
    {

        public static string WebResponse(string EndPoint)
        {
            //This value is stored in the settings table
            string DomusEndPoint = EfxFramework.EfxSettings.DomusAPIURL;

            string ApiFullString = DomusEndPoint + EndPoint;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            HttpWebRequest request;

            HttpWebResponse response = null;
            StreamReader reader;
            StringBuilder sbSource;



            string returnString = "";

            try

            {
                // Create and initialize the web request  
                request = WebRequest.Create(ApiFullString) as HttpWebRequest;
                request.KeepAlive = false;
                request.Method = "GET";

                // Set timeout to 15 seconds  

                // TODO: Put me back, extended timeout so I can debug.
                // request.Timeout = 150000 * 1000;


                request.Timeout = 15 * 1000;



                // Get response  
                response = request.GetResponse() as HttpWebResponse;

                if (request.HaveResponse == true && response != null)
                {
                    // Get the response stream  
                    reader = new StreamReader(response.GetResponseStream());

                    // Read it into a StringBuilder  
                    sbSource = new StringBuilder(reader.ReadToEnd());

                    returnString = sbSource.ToString();
                }

                return returnString.ToString();
            }
            catch (WebException wex)
            {
                string fullerror = "";
                // This exception will be raised if the server didn't return 200 - OK  
                // Try to retrieve more information about the network error  
                if (wex.Response != null)
                {

                    using (HttpWebResponse errorResponse = (HttpWebResponse)wex.Response)
                    {
                        string ErrResp = wex.Response.Headers.ToString();

                        fullerror = "ERROR " + errorResponse.StatusDescription + " " + errorResponse.StatusCode + " " + errorResponse.StatusCode;
                    }
                }
                return "Error";

            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }

        }




    }
}