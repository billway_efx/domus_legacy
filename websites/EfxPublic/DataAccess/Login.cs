﻿using System;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Web.Security;
using EfxFramework;
using Microsoft.Ajax.Utilities;

namespace EfxPublic.DataAccess
{
    public class Login
    {
        public static string Auth(string un, string pw)
        {
            string DomusEndPoint = EfxFramework.EfxSettings.DomusAPIURL;
            //Endpoint will come from property table this is for tesitng only
            string ApiEndPoint = DomusEndPoint + "/Authenticate/";

            // string ApiEndPoint2 = un + "/" + pw;
            
            // FIX JBane 01/16/2019: Add UrlEncode so "#" in passwords doesn't break URL
            string ApiEndPoint2 = un + "/" + System.Net.WebUtility.UrlEncode(pw);

            //Return Response as XML
            //string ApiFormat = "&$format=xml";

            //empty string for response
            string RespString = "";

            string ApiFullString = ApiEndPoint + ApiEndPoint2;
            
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            HttpWebRequest request;
            HttpWebResponse response = null;
            StreamReader reader;
            StringBuilder sbSource;
            string returnString = "";

            try
            {
                // Create and initialize the web request 
                request = WebRequest.Create(ApiFullString) as HttpWebRequest;

                request.KeepAlive = true;
                request.Method = "GET";

                // Set timeout to 15 seconds  
                //request.Timeout = 30 * 1000;
                request.Timeout = Timeout.Infinite;

                // Get response  
                response = request.GetResponse() as HttpWebResponse;

                if (request.HaveResponse == true && response != null)
                {
                    // Get the response stream  
                    reader = new StreamReader(response.GetResponseStream());

                    // Read it into a StringBuilder  
                    sbSource = new StringBuilder(reader.ReadToEnd());

                    returnString = sbSource.ToString();
                }
                
                return returnString;
            }
            catch (WebException wex)
            {
                string fullerror = "";
                // This exception will be raised if the server didn't return 200 - OK  
                // Try to retrieve more information about the network error  
                if (wex.Response != null)
                {

                    using (HttpWebResponse errorResponse = (HttpWebResponse)wex.Response)
                    {
                        string ErrResp = wex.Response.Headers.ToString();

                        fullerror = "ERROR " + errorResponse.StatusDescription + " " + errorResponse.StatusCode + " " + errorResponse.StatusCode;
                    }
                }

                if (wex.InnerException != null)
                {
                    return wex.Message + "\r\n\r\n" + wex.InnerException.InnerException;
                }
                else
                {
                    return wex.Message;
                }
                
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }
        }

        //Find Root Element 
        public static XElement GetRootElement(XElement element, string localName)
        {
            XElement Element = null;

            if (element != null && element.Name.LocalName == localName)
                return element;

            if (element != null && element.HasElements)
            {
                foreach (var E in element.Elements())
                {
                    Element = GetRootElement(E, localName);
                }
            }

            return Element;
        }
        //DomusAPIEndPoint





    }
}