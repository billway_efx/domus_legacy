﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EfxPublic.DataAccess
{
    public class CalendarEvents
    {

        public static string GetCalendarItemsForRenter(string session, string RenterID)
        {
            string EndPoint = session + "/" + RenterID;
            string Method = "/GetCalendarEventsForRenter/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);
            return Resp;
        }


    }
}