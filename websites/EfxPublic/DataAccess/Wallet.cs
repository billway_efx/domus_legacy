﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Text;
using System.IO;
using System.Net;

namespace EfxPublic.DataAccess
{

    public class WResponse
    {
        public string Message;
        public string Result;
    }


    public class Wallet
    {
        //Get
        public static string GetPaymentMethodsForRenter(string session, string renterID)
        {
            string EndPoint = session + "/" + renterID;
            string Method = "/Wallet_GetPaymentMethodsForRenter/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);
            return Resp;
        }

        //Add CC
        public static string AddCCPaymentMethodForRenter(string session, string renterID, string CCholderName, string CCAccountNumber, string CCexpMonth, string CCExpYear, string CardSecurityCode)
        {
            string EndPoint = session + "/" + renterID + "/" + CCholderName + "/" + CCAccountNumber + "/" + CCexpMonth + "/" + CCExpYear + "/" + CardSecurityCode;
            string Method = "/Wallet_AddCCPaymentMethodForRenter/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);
            return Resp;
        }

        //Update CC
        public static string UpdateCCPaymentMethodForRenter(string session, string renterID, string PayerMethodID, string CCholderName, string CCAccountNumber, string CCexpMonth, string CCExpYear, string CardSecurityCode)
        {
            string EndPoint = session + "/" + renterID + "/" + PayerMethodID + "/" + CCholderName + "/" + CCAccountNumber + "/" + CCexpMonth + "/" + CCExpYear + "/" + CardSecurityCode;
            string Method = "/Wallet_UpdateCCPaymentMethodForRenter/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);
            return Resp;
        }

        //Add CK
        public static string AddACHPaymentMethodForRenter(string session, string renterID, string AccountTypeId, string AccountNumber, string RoutingNumber)
        {
            //AccountTypeID - Specify a 1 (Checking Account) or a 2 (Savings Account) for the AccountTypeId parameter.
            string EndPoint = session + "/" + renterID + "/" + AccountTypeId + "/" + AccountNumber + "/" + RoutingNumber;
            string Method = "/Wallet_AddACHPaymentMethodForRenter/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);
            return Resp;
        }

        //Update CK
        public static string UpdateACHPaymentMethodForRenter(string session, string renterID, string PayerWalletID, string AccountTypeId, string AccountNumber, string RoutingNumber)
        {
            //AccountTypeID - Specify a 1 (Checking Account) or a 2 (Savings Account) for the AccountTypeId parameter.
            string EndPoint = session + "/" + renterID + "/" + PayerWalletID + "/" + AccountTypeId + "/" + AccountNumber + "/" + RoutingNumber;
            string Method = "/Wallet_UpdateACHPaymentMethodForRenter/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);
            return Resp;
        }

        // "/Wallet_RemovePaymentMethodForRenter/{SessionId}/{RenterId}/{PaymentMethodId}/{PaymentMethod}"
        public static string RemovePaymentMethodForRenter(string SessionID, string RenterID,string PaymentMethodID, string PaymentMethod)
        {
            string EndPoint = SessionID + "/" + RenterID + "/" + PaymentMethodID + "/" + PaymentMethod;
            string Method = "/Wallet_RemovePaymentMethodForRenter/";
            string Resp = DataAccess.Response.WebResponse(Method + EndPoint);
            return Resp;
        }




        public static WResponse WalletReadResponse(string _Element, string _xml)
        {
            
            byte[] byteArray = Encoding.ASCII.GetBytes(_xml);

            using (var OutputStream = new MemoryStream(byteArray))
            {

                WResponse R = new WResponse();
                var XmlResp = XDocument.Load(OutputStream);
                XElement RespElement = null;

                try
                {
                    RespElement = DataAccess.RenterDataAccess.GetRootElement(XmlResp.Root, _Element);
                }
                catch
                {
                    R.Message = "Error";
                    R.Result = "Error";
                    return R;
                }

                string Msg = "";

                try
                {
                    foreach (XElement Response in RespElement.Elements())
                    {
                        string node = Response.Name.LocalName;

                        if (node == "Message")
                        {
                            Msg = Response.Value.ToString();
                            if (Msg == "Session timed out")
                            {
                                FormsAuthentication.SignOut();
                                HttpContext.Current.Response.Redirect("~/ResidentLogin.aspx");
                            }

                            if (Msg.Contains("Error reading database"))
                            {
                                string Error = "Error";
                                R.Message = Error;
                            }

                            if (Msg.Contains("success"))
                            {
                                R.Message = Msg;
                            }

                        }

                        if (node == "Result")
                        {
                            R.Result = Response.Value.ToString();
                        }

                    }
                }
                catch(Exception ex)
                {
                    //Return the Error to caller
                    R.Message = "Error";
                    R.Result = "Error" + ex.Message;
                }

                return R;
            }

        }

        public static string TestResponse()
        {
            string Resp = @"<Wallet_UpdateCCPaymentMethodResponse xmlns=""http://schemas.datacontract.org/2004/07/EfxFramework.Mobile"" xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
                              <Message>Payment method successfully updated.</Message>
                              <Result>Success</Result>
                            </Wallet_UpdateCCPaymentMethodResponse>";

            return Resp;

        }



        public static void AutoPayment_InsertUpdate(int RenterID, int PayerID, int PaymentypeID, int PaymentMethodID, int PayDayOfMonth)
        {
            SqlConnection con = new SqlConnection(EfxFramework.EfxSettings.ConnectionString);
            SqlCommand com = new SqlCommand("usp_AutoPayment_InsertUpdate", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@RenterID", SqlDbType.Int).Value = RenterID;
            com.Parameters.Add("@PayerID", SqlDbType.Int).Value = PayerID;
            com.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = PaymentypeID;
            com.Parameters.Add("@PaymentMethodID", SqlDbType.Int).Value = PaymentMethodID;
            com.Parameters.Add("@PaymentDayOfMonth", SqlDbType.Int).Value = PayDayOfMonth;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            catch(Exception ex)
            {
                con.Close();
                throw ex;
            }

        }

        //usp_AutoPayment_DeleteByRenterId
        public static void AutoPay_Cancel(int RenterID)
        {
            SqlConnection con = new SqlConnection(EfxFramework.EfxSettings.ConnectionString);
            SqlCommand com = new SqlCommand("usp_AutoPayment_DeleteByRenterId", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@RenterID", SqlDbType.Int).Value = RenterID;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

        }


    }
}