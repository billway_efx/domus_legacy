﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Net.Mail;
using EfxFramework;


namespace EfxPublic
{
    public partial class FutureResidents : System.Web.UI.Page
    {
        public class propertyManagerInfo
        {
            public int propertyID;
            public String propertyName;
            public String firstName;
            public String lastName;
            public String primaryEmailAddress;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["applicationUpload"] == null && applicationUpload.HasFile)
                Session["applicationUpload"] = applicationUpload;
            else if (Session["applicationUpload"] != null && (!applicationUpload.HasFile))
                applicationUpload = (FileUpload)Session["applicationUpload"];
            else if (applicationUpload.HasFile)
                Session["applicationUpload"] = applicationUpload;
        }

        protected void searchPropertybtn_Click(object sender, EventArgs e)
        {
            propertyListSql.SelectParameters[0].DefaultValue = propertyNametxt.Text;
            propertyListSql.DataBind();
            propertyListGrid.DataSourceID = "propertyListSql";
            propertyListGrid.Visible = true;
        }

        protected void propertyNamelnkbtn_Click(object sender, EventArgs e)
        {
            propertyNametxt.Text = ((LinkButton)sender).Text;
            propertyListGrid.Visible = false;
            propertyListGrid.SelectedIndex = ((GridViewRow)((DataControlFieldCell)((LinkButton)sender).Parent).Parent).RowIndex;
            applicationArea.Visible = true;
        }

        public static int DownLoadPropertyApplication(int PropID)
        {
            int FileStatus = 0;
            using (SqlDataReader Reader = Document_GetPropertyApplication(PropID))
            {
                Reader.Read();
                if (Reader.HasRows)
                {
                    string _FileName = Reader["DocumentName"].ToString();
                    string _ContentType = "application/pdf";
                    byte[] _BinaryData = (byte[])Reader["DocumentBinary"];

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.Buffer = true;
                    HttpContext.Current.Response.Charset = "";
                    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    HttpContext.Current.Response.ContentType = _ContentType;
                    HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=" + _FileName);
                    HttpContext.Current.Response.BinaryWrite(_BinaryData);
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();

                    FileStatus = 1;
                }
                else
                {
                    FileStatus = 0;
                }

                return FileStatus;
            }

        }

        private static SqlDataReader Document_GetPropertyApplication(int PropertyID)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Property_GetApplicationForDownLoad", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = PropertyID;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);

            }
            catch (Exception)
            {
                con.Close();
                throw;
            }

        }

        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

        protected void downloadApplnkbtn_Click(object sender, EventArgs e)
        {
            if (propertyListGrid.SelectedIndex >= 0)
            {
                int propertyID = int.Parse(((Label)propertyListGrid.SelectedRow.FindControl("propertyIDlbl")).Text);
                DownLoadPropertyApplication(propertyID);
            }
        }

        protected void propertyListGrid_DataBound(object sender, EventArgs e)
        {
            propertyListGrid.SelectedIndex = -1;
            applicationArea.Visible = false;
        }


        bool validateForm()
        {
            String phoneNum = phoneNumtxt.Text;
            bool badPhone = false;

            foreach (char Char in phoneNum)
            {
                if (char.IsLetter(Char))
                {
                    badPhone = true;
                    break;
                }
                else if (!char.IsDigit(Char))
                {
                    phoneNum = phoneNum.Remove(phoneNum.IndexOf(Char), 1);
                }
            }

            if (badPhone)
            {
                customValidation1.ErrorMessage = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Phone number may not contain letters";
                customValidation1.IsValid = false;
                return false;
            }
            if (phoneNum.Length != 10)
            {
                customValidation1.ErrorMessage = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Phone number must have 10 digits";
                customValidation1.IsValid = false;
                return false;
                //Invalid phone number
            }
            if (propertyListGrid.SelectedIndex < 0 || propertyListGrid.Rows.Count == 0)
            {
                customValidation2.ErrorMessage = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Please select a property";
                customValidation2.IsValid = false;
                return false;
            }

            if (!applicationUpload.HasFile)
            {
                customValidation3.ErrorMessage = "<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Please upload your application";
                customValidation3.IsValid = false;
                return false;
            }

            return true;
        }

        protected void submitbtn_Click(object sender, EventArgs e)
        {
            if (!validateForm())
            {
                return;
            }
            errorlbl.Text = string.Empty;

            String emailAddress = emailAddresstxt.Text.Trim();         
            
            bool existingRenterEmail = EfxFramework.Applicant_V2.CheckforExistingRenterEmail(emailAddress);

            if (existingRenterEmail)
            {
                //Email address is already a Renter
                errorlbl.Text = "The entered email is not available";
            }
            else
            {
                //Grab uploaded file and user info, construct and send email to Property Manager and user
                bool IsValidated = false;
                string PmsID = "";
                int AppID = 0;
                int AppStatus = 3;
                int AppSubmission = 1;
                String lastUpdatedBy = "Renter Application Page";
                int propertyID = int.Parse(((Label)propertyListGrid.SelectedRow.FindControl("propertyIDlbl")).Text);
                String firstName = firstNametxt.Text.Trim();
                String lastName = lastNametxt.Text.Trim();

                //Gets application file
                Stream fs = applicationUpload.PostedFile.InputStream;
                BinaryReader br = new BinaryReader(fs);
                Byte[] application_bytes = br.ReadBytes((Int32)fs.Length);

                String phoneNum = phoneNumtxt.Text;

                foreach (char Char in phoneNum)
                {
                    if (!char.IsDigit(Char))
                    {
                        phoneNum = phoneNum.Remove(phoneNum.IndexOf(Char), 1);
                    }
                }

                List<propertyManagerInfo> PMInfo = getPropertyManagers_ByPropertyID(propertyID);

                //Insert new Applicant or Update existing Applicant in Applicant Table
                AppID = Applicant_V2.Applicant_InsertUpdate_v2(AppID, propertyID, firstName, lastName, PmsID, IsValidated, emailAddress, phoneNum);

                if (AppID == 0)
                {
                    //Error, Applicant already exists
                    errorlbl.Text = "An application for this email already exists";
                    return;
                }

                //Insert / Update ApplicantApplicantion 
                Applicant_V2.ApplicantApplication_InsertUpdate(AppID, propertyID, AppStatus, false, AppSubmission, DateTime.Now, DateTime.Now, lastUpdatedBy);

                Applicant_V2.ApplicantApplication_InsertUploadedApplication(AppID, application_bytes);



                //Create Email and send to PMs and Applicant
                MailAddress fromAddress = new MailAddress("support@DomusMe.com");
                String subject = "New Applicant";

                String messageTemplate = Applicant_V2.get_EmailTemplate_ByTemplateID(41);
                String messageBody = messageTemplate;

                messageBody = messageBody.Replace("##NAME##", "Property Manager");
                messageBody = messageBody.Replace("##APPFNAME##", firstName);
                messageBody = messageBody.Replace("##APPLNAME##", lastName);
                messageBody = messageBody.Replace("##APPEMAIL##", emailAddress);
                messageBody = messageBody.Replace("##APPPROP##", propertyID.ToString());


                List<MailAddress> recipients = new List<MailAddress>();
                recipients.Add(new MailAddress(emailAddress));

                String disseminationGroup = "Applicant: " + firstName + " " + lastName + ";" ;
                String dissemGroupPM = " Property Manager(s): ";

                foreach (propertyManagerInfo PM in PMInfo)
                {
                    dissemGroupPM += PM.firstName + " " + PM.lastName + ", ";
                    recipients.Add(new MailAddress(PM.primaryEmailAddress));
                }

                dissemGroupPM = dissemGroupPM.Substring(0, dissemGroupPM.Length - 2);
                disseminationGroup += dissemGroupPM;

                EfxFramework.BulkMail.BulkMail.SendMailMessage(propertyID, disseminationGroup, false, Settings.SendMailUsername, Settings.SendMailPassword,
                            fromAddress, subject, messageBody, recipients);

                Response.Redirect("~/ApplicationConfirmation.aspx");


            }

        }

        public static List<propertyManagerInfo> getPropertyManagers_ByPropertyID(int propertyID)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_PropertyStaff_GetPropertyManagers_ByPropertyID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.CommandTimeout = 360;
            com.Parameters.Add("@propertyID", SqlDbType.Int).Value = propertyID;

            List<propertyManagerInfo> PMInfoList = new List<propertyManagerInfo>();

            con.Open();
            SqlDataAdapter SqlAdapt = new SqlDataAdapter(com);
            //SqlAdapt.SelectCommand = com;
            DataSet DS = new DataSet();
            SqlAdapt.Fill(DS, "Data");
            con.Close();


            foreach (DataRow row in DS.Tables[0].Rows)
            {
                propertyManagerInfo PM = new propertyManagerInfo();

                PM.propertyID = int.Parse(row["PropertyId"].ToString());
                PM.propertyName = row["PropertyName"].ToString();
                PM.firstName = row["FirstName"].ToString();
                PM.lastName = row["LastName"].ToString();
                PM.primaryEmailAddress = row["PrimaryEmailAddress"].ToString();

                PMInfoList.Add(PM);
            }

            return PMInfoList;
        }

    }
}