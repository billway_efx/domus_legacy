﻿var menuModule = angular.module('menu', ['API'])
    .config(function ($locationProvider) {
        var html5Ok = navigator.appVersion.indexOf('MSIE') == -1;
        $locationProvider.html5Mode(html5Ok).hashPrefix('');
    })
    .directive(
    'rpoMenu', function () {
        return {
            restrict: 'A',
            templateUrl: '/menu.html',
            replace: true,
            transclude: true
        };
    });
function MenuController($scope, companyService, propertyService, userService, calendarEventService) {
	$scope.isRPOAdmin = false;
	$scope.isCorporateAdmin = false;
	$scope.displayAddCompanyMenu = false;
	$scope.urlPrefix = navigator.appVersion.indexOf('MSIE') == -1 ? "" : "/rpo#";
	$scope.eventUrlPrefix = navigator.appVersion.indexOf('MSIE') == -1 ? "" : "/rpo/events#";
    //determine if user is admin
    userService.userIsAdmin().then(function (isAdmin) {
        $scope.isRPOAdmin = isAdmin;
        userService.getUserId().then(function (userId) {
            $scope.userId = userId;
            $scope.companies = isAdmin == 'true' ? companyService.getAllCompanies() : null;
            $scope.properties = isAdmin == 'true' ? propertyService.getAllProperties() : propertyService.getUserProperties(userId);
            if ($scope.isRPOAdmin === 'true') {
                $scope.displayAddCompanyMenu = true;
            }
            //determine if user is corp admin instead
            userService.userIsCorporateAdmin().then(function (isCorporateAdmin) {
            	$scope.isCorporateAdmin = isCorporateAdmin;
            	if ($scope.isRPOAdmin === 'true' || $scope.isCorporateAdmin === 'true') {
            	    $scope.canAddPropertyAndStaff = true;
            	}
            });
        });
    });
    $scope.showAddEventLink = window.location.href.indexOf('rpo/event') === -1;
    $scope.launchEventModal = function () {
        calendarEventService.createEvent();
        $('#eventCreate').modal('show');
    };
}
angular.bootstrap($('#menu'), ["menu"]);