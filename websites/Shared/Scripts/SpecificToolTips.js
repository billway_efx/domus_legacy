﻿/* This holds the content for all the tool tips in the site, this way nothing is directly written into the markup*/


//This function is for the "what is a CVV code" tool tip, which 
$(function () {
    $('.cvvTip').data('powertip', '<div class="toolTipContent"><h4>What is a CVV Number?</h4><div class="cvvImage"></div></div>').powerTip({ placement: 'e' });
    // 'e' means that the tool tip will appear to the eastern side of the element
});

//This function is for the "echeck routing and account numbers" tool tip, which 
$(function () {
    $('.checkNumbersTip').data('powertip', '<div class="toolTipContent"><h4>Where Do I Find Routing and Checking Account Numbers?</h4><div class="eCheckImage"></div></div>').powerTip({ placement: 'e' });
    // 'e' means that the tool tip will appear to the eastern side of the element
});

//This function is for the "Auto Pay" tool tip, which 
$(function () {
    $('.autoPayTip').data('powertip', '<div class="toolTipContent"><h4>What is AutoPay?</h4><p>Auto Payments will draft the current balance due each month. <br />The amount you see reflected here is a representation of your <br />current rent and monthly fees. This amount is subject to change <br />and may not be the amount paid every month.</p></div>').powerTip({ placement: 'nw' });
    // 'nw' means that the tool tip will appear to the north western side of the element
});