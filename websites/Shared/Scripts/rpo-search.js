﻿
//Setup app
var app = angular.module('rpoSearch', ['ngRoute', '$strap.directives', 'pagination', 'API', 'ngAnimate', 'ajoslin.promise-tracker']).config(function ($routeProvider, $locationProvider, $httpProvider) {
    var html5Ok = navigator.appVersion.indexOf('MSIE') == -1;
    $locationProvider.html5Mode(html5Ok).hashPrefix('');
    $routeProvider.
        when('/rpo/search', { controller: DefaultController, templateUrl: '../../search.html', reloadOnSearch: false }).
        when('/rpo/search/residents', { controller: ResidentSearchController, templateUrl: '../../search-results-residents.html', reloadOnSearch: false }).
        when('/rpo/search/payments', { controller: PaymentSearchController, templateUrl: '../../search-results-payments.html', reloadOnSearch: false }).
        otherwise({ redirectTo: '/rpo/search' });
})
.factory('searchService', function ($rootScope, $http, $q, $filter, $location) {
    //var searchText = $location.search().q;
    //var searchFilter = $location.search().f;
    var searchType = null;
    var searchStartDate = null;
    var searchEndDate = null;

    var paymentSearch = function () {
        var deferred = $q.defer();
        $http.get('/api/payment/search?text=' + $location.search().q + '&filter=' + $location.search().f + '&startDate=' + $location.search().s + '&endDate=' + $location.search().e).success(function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    };

    var residentSearch = function () {
        var deferred = $q.defer();
        $http.get('/api/renter/search?text=' + $location.search().q + '&filter=' + $location.search().f).success(function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    };
    var searchCriteria = function (searchText, searchFilter, searchType) {
        searchText = searchText;
        searchFilter = searchFilter;
        searchType = searchType;
    };
    return {
        searchPayments: paymentSearch,
        searchResidents: residentSearch,
        setSearchCriteria: searchCriteria,
        getSearchText: function () {
            return searchText;
        },
        getSearchFilter: function() {
            return searchFilter;
        },
        getSearchType: function() {
            return searchType;
        },
        getSearchStartDate: function () {
            return searchStartDate;
        },
        getSearchEndDate: function() {
            return searchEndDate;
        }
    };
});
//Controllers
function DefaultController($scope, $rootScope, $http, $routeParams) {
    $rootScope.$broadcast('Default');
    
}

function FilterController($scope, $rootScope, $location, $filter, searchService) {
    $scope.urlPrefix = navigator.appVersion.indexOf('MSIE') == -1 ? "" : "/rpo/search#";
    $scope.residentFilterOptions = [
    { text: 'First Name', value: 'FirstName' },
    { text: 'Last Name', value: 'LastName' },
    { text: 'ID (PMS ID or RPO ID)', value: 'RenterID' },
    { text: 'Phone Number', value: 'MainPhoneNumber' },
    { text: 'Email', value: 'Email' },
    { text: 'Property', value: 'PropertyName' },
    { text: 'Rent Due Amount', value: 'RentAmount' }
    ];
    $scope.paymentFilterOptions = [
    { text: 'Transaction ID', value: 'TransactionID' },
    { text: 'First Name', value: 'FirstName' },
    { text: 'Last Name', value: 'LastName' },
    { text: 'ID (PMS ID or RPO ID)', value: 'RenterID' },
    { text: 'Property', value: 'PropertyName' },
    { text: 'Property Management Company', value: 'CompanyName' },
    { text: 'Amount Paid', value: 'RentAmount' },
    { text: 'Payment Portal', value: 'Portal' }
    ];
    $rootScope.$on('Residents', function () {
        $scope.filterOptions = $scope.residentFilterOptions;
        $scope.searchType = 'Residents';

        angular.forEach($scope.filterOptions, function (option, key) {
            if (option.value == $location.search().f) {
                $scope.searchFilter = option;
            }
        });
    });
    $rootScope.$on('Payments', function () {
        $scope.filterOptions = $scope.paymentFilterOptions;
        $scope.searchType = 'Payments';
        angular.forEach($scope.filterOptions, function (option, key) {
            if (option.value == $location.search().f) {
                $scope.searchFilter = option;
            }
        });
    });
    if (!isEmpty($location.search().q)) {
        $scope.searchText = $location.search().q;
    }
    if (!isEmpty($location.search().s)) {
        var startDate = new Date(Date.parse($location.search().s));
        $scope.startDate = { date: startDate };
    }
    if (!isEmpty($location.search().e)) {
        var endDate = new Date(Date.parse($location.search().e));
        $scope.endDate = { date: endDate };
    }
    $scope.search = function () {
        //searchService.setSearchCriteria($scope.searchText, $scope.searchFilter,$scope.startDate.date,$scope.endDate.date);

        if ($scope.searchType == 'Payments') {
            var startDate = '';
            if (!isEmpty($scope.startDate)) {
                startDate = new Date($scope.startDate.date);
                if (startDate == 'Invalid Date') {//IE fix for when no date is selected. Only occurs if initialized date is used.
                    startDate = '';
                } else {
                    startDate.setDate(startDate.getDate() + 1);
                    if ($filter('date')(startDate, 'MM/dd/yyyy') > $filter('date')(new Date(), 'MM/dd/yyyy')) {
                        startDate.setDate(startDate.getDate() - 1);
                    }
                    startDate = $filter('date')(startDate, 'MM-dd-yyyy');
                }
                
            }
            var endDate = '';
            if (!isEmpty($scope.endDate)) {
                endDate = new Date($scope.endDate.date);
                if (endDate == 'Invalid Date') {//IE fix for when no date is selected. Only occurs if initialized date is used.
                    endDate = '';
                } else {
                    endDate.setDate(endDate.getDate() + 1);
                    if ($filter('date')(endDate, 'MM/dd/yyyy') > $filter('date')(new Date(), 'MM/dd/yyyy')) {
                        endDate =new Date();
                    }
                    endDate = $filter('date')(endDate, 'MM-dd-yyyy');
                }
            }

            $location.search({ q: $scope.searchText, f: isEmpty($scope.searchFilter) ? "" : $scope.searchFilter.value, s: startDate, e: endDate });
            $rootScope.$broadcast('PaymentSearch');
        } else if ($scope.searchType == 'Residents') {
            $location.search({ q: $scope.searchText, f: isEmpty($scope.searchFilter) ? "" : $scope.searchFilter.value });
            $rootScope.$broadcast('ResidentSearch');
        }
    };
}
function PaymentSearchController($scope, $rootScope, $http, $routeParams, $location, searchService, promiseTracker) {
	$scope.showDetails = false;
    $rootScope.$broadcast('Payments');
    $scope.viewTitle = 'Payment Search Results';
    $scope.loading = promiseTracker('search');
    $scope.search = function () {
        if (!isEmpty($location.search().q) || !isEmpty($location.search().f)) {
            var searchPromise = searchService.searchPayments();
            $scope.loading.addPromise(searchPromise);
            searchPromise.then(function (results) {
                $scope.payments = results;
            });
        }
    }
    $rootScope.$on('PaymentSearch', function () {
        $rootScope.$broadcast('Payments');
        $scope.search();
    });
    $scope.togglePayment = function (row) {
    	row.showDetails = !row.showDetails;
    };
    $scope.search();
}
function ResidentSearchController($scope, $rootScope, $http, $routeParams, $location, searchService, promiseTracker) {
    $rootScope.$broadcast('Residents');
    $scope.viewTitle = 'Resident Search Results';
    $scope.loading = promiseTracker('search');
    
    $scope.search = function () {
        $rootScope.$broadcast('Residents');
        if (!isEmpty($location.search().q) || !isEmpty($location.search().f)) {
            var searchPromise = searchService.searchResidents();
            $scope.loading.addPromise(searchPromise);
            searchPromise.then(function (results) {
                $scope.renters = results;
                //Salcedo - 2/18/2015 - found that we needed to reset the page number after a new search - task # 000138
                $scope.paginator.setPage(0);
            });
        }
    }
    $rootScope.$on('ResidentSearch', function () {
        $scope.search();
    });
    $scope.search();
    
}
function TitleController($scope, $rootScope) {
    $rootScope.$on('Residents', function () {
        $scope.PageTitle = 'Resident Search';
    });
    $rootScope.$on('Payments', function () {
        $scope.PageTitle = 'Payment Search';
    });
    $rootScope.$on('Default', function () {
        $scope.PageTitle = 'Search';
    });
}
//Bootstrap app
angular.bootstrap($('#app'), ["rpoSearch"]);