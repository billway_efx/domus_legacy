﻿//helper functions
function isEmpty(value) {
    return angular.isUndefined(value) || value === '' || value === null || value !== value;
}
angular.module('API', [])
.filter('exact', function () {
    return function (items, match) {
        var matching = [], matches, falsely = true;
        // Return the items unchanged if all filtering attributes are falsy
        angular.forEach(match, function (value, key) {
            falsely = falsely && !value;
        });
        if (falsely) {
            return items;
        }

        angular.forEach(items, function (item) { // e.g. { title: "ball" }
            matches = true;
            angular.forEach(match, function (value, key) { // e.g. 'all', 'title'
                if (!!value) { // do not compare if value is empty
                    matches = matches && (item[key] === value);
                }
            });
            if (matches) {
                matching.push(item);
            }
        });
        return matching;
    }
})
.factory('companyService', function ($rootScope, $http, $q, $filter) {
    var companies = null;
    var allCompanies = function () {
        var deferred = $q.defer();
        if (isEmpty(companies)) {
            $http.get('/api/company/all').success(function (data) {
                companies = data;
                deferred.resolve(data);
            });
        } else {
            deferred.resolve(companies);
        }
        return deferred.promise;
    };
    var deleteCompany = function (id) {
    	var deferred = $q.defer();
    	$http.get('/api/company/delete/' + id).success(function (response) {
    		deferred.resolve(response);
    	});
    	return deferred.promise;
    };
    return {
        getAllCompanies: allCompanies,
        deleteCompany: deleteCompany
    };
})
.factory('propertyService', function ($rootScope, $http, $q, $filter) {
    var properties = null;
    var staffProperties = null;
    var staffPropertiesForDropdown = null;
    var propertiesForDropdown = null;
    var property = null;
    var currentPropertyId = null;
    var allProperties = function () {
        var deferred = $q.defer();
        if (isEmpty(properties)) {
            $http.get('/api/property/all?dropdown=false').success(function (data) {
                properties = data;
                deferred.resolve(data);
            });
        } else {
            deferred.resolve(properties);
        }
        return deferred.promise;
    };
    var allPropertiesForDropdown = function () {
        var deferred = $q.defer();
        if (isEmpty(propertiesForDropdown)) {
            $http.get('/api/property/all?dropdown=true').success(function (data) {
                propertiesForDropdown = data;
                deferred.resolve(data);
            });
        } else {
            deferred.resolve(propertiesForDropdown);
        }
        return deferred.promise;
    };
    var companyProperties = function (companyId) {
        var deferred = $q.defer();
        if (isEmpty(properties)) {
            allProperties().then(function (propertyList) {
                deferred.resolve($filter('exact')(propertyList, { CompanyId: companyId }));
            });
        } else {
            deferred.resolve($filter('exact')(properties, { CompanyId: companyId }));
        }
        return deferred.promise;
    };
    var companyPropertiesForDropdown = function (companyId) {
        var deferred = $q.defer();
        if (isEmpty(propertiesForDropdown)) {
            allPropertiesForDropdown().then(function (propertyList) {
                deferred.resolve($filter('exact')(propertyList, { CompanyId: companyId }));
            });
        } else {
            deferred.resolve($filter('exact')(propertiesForDropdown, { CompanyId: companyId }));
        }
        return deferred.promise;
    };
    var userProperties = function (userId, forDropdown) {
        var deferred = $q.defer();
        if (isEmpty(staffProperties)) {
            $http.get('/api/property/byuser/' + userId + '?dropdown=false').success(function (data) {
                staffProperties = data;

                deferred.resolve(data);
            });
        } else {
            deferred.resolve(staffProperties);
        }
        return deferred.promise;
    };
    var userPropertiesForDropdown = function (userId) {
        var deferred = $q.defer();
        if (isEmpty(staffPropertiesForDropdown)) {
            $http.get('/api/property/byuser/' + userId + '?dropdown=true').success(function (data) {
                staffPropertiesForDropdown = data;

                deferred.resolve(data);
            });
        } else {
            deferred.resolve(staffPropertiesForDropdown);
        }
        return deferred.promise;
    };
    var propertyEntity = function (propertyId) {

        var deferred = $q.defer();
        if (isEmpty(property) || currentPropertyId !== propertyId) {
            $http.get('/api/property/one/' + propertyId).success(function (data) {
                currentPropertyId = propertyId;
                property = data;
                deferred.resolve(data);
            });
        } else {
            deferred.resolve(property);
        }
        return deferred.promise;
    };
    var propertyToggleActive = function (propertyToToggle) {
        var deferred = $q.defer();
        $http.post('/api/property/toggleactive', propertyToToggle).success(function (response) {
            property.isActive = !property.isActive;
            deferred.resolve(response);
        });
        return deferred.promise;
    };
    var propertyDelete = function (propertyId) {
    	var deferred = $q.defer();
    	$http.get('/api/property/delete/' + propertyId).success(function (response) {
    		deferred.resolve(response);
    	});
    	return deferred.promise;
    };
    return {
        getAllProperties: allProperties,
        getAllPropertiesForDropdown: allPropertiesForDropdown,
        getUserPropertiesForDropdown: userPropertiesForDropdown,
        getCompanyPropertiesForDropdown: companyPropertiesForDropdown,
        getCompanyProperties: companyProperties,
        getUserProperties: userProperties,
        getProperty: propertyEntity,
        toggleActive: propertyToggleActive,
        deleteProperty: propertyDelete
    };
})
.factory('renterService', function ($rootScope, $http, $q, $filter) {
    var renter = null;
    var currentRenterId = null;
    var allRenters = function (search) {
        var deferred = $q.defer();
        $http.get('/api/renter/all?q=' + (isEmpty(search.q) ? '' : search.q) + '&p=' + (isEmpty(search.p) ? '' : search.p) +
                '&o=' + (isEmpty(search.o) ? '' : search.o) + '&d=' + (isEmpty(search.d) ? '' : search.d) + '&pid=' + (isEmpty(search.pid) ? '' : search.pid) +
                '&active=' + (isEmpty(search.active) ? '' : search.active)).success(function (data) {
            renters = data;
            deferred.resolve(data);
            });
        return deferred.promise;
    };
    var propertyRenters = function (propertyId,search) {
        var deferred = $q.defer();
        allRenters(search).then(function (rentersList) {
            deferred.resolve($filter('exact')(rentersList, { PropertyId: propertyId }));
        });
        return deferred.promise;
    };
    var renterEntity = function (renterId) {

        var deferred = $q.defer();
        if (isEmpty(renter) || currentRenterId !== renterId) {
            $http.get('/api/renter/one/' + renterId).success(function (data) {
                currentRenterId = renterId;
                renter = data;
                deferred.resolve(data);
            });
        } else {
            deferred.resolve(renter);
        }
        return deferred.promise;
    };
    var renterToggleActive = function (renterToToggle) {
        var deferred = $q.defer();
        $http.post('/api/renter/toggleactive', renterToToggle).success(function (response) {
            deferred.resolve(response);
        });
        return deferred.promise;
    };
    return {
        getAllRenters: allRenters,
        getPropertyRenters: propertyRenters,
        getRenter: renterEntity,
        toggleActive: renterToggleActive
    };
})
.factory('userService', function ($rootScope, $http, $q, $filter) {
    var isAdmin = null;
    var isCorporateAdmin = null;
    var userId = null;

    return {
        getUserId:
            function () {
                var deferred = $q.defer();
                if (isEmpty(userId)) {
                    $http.get('/api/user/id').success(function (data) {
                        userId = data;
                        deferred.resolve(userId);
                    });
                } else {
                    deferred.resolve(userId);
                }
                return deferred.promise;
            },
        userIsAdmin:
            function () {
                var deferred = $q.defer();
                if (isEmpty(isAdmin)) {
                    $http.get('/api/user/isadmin').success(function (data) {
                        isAdmin = data;
                        deferred.resolve(isAdmin);
                    });
                } else {
                    deferred.resolve(isAdmin);
                }
                return deferred.promise;
            },
        userIsCorporateAdmin:
			function () {
			    var deferred = $q.defer();
			    if (isEmpty(isCorporateAdmin)) {
			        $http.get('/api/user/iscorporateadmin').success(function (data) {
			            isCorporateAdmin = data;
			            deferred.resolve(isCorporateAdmin);
			        });
			    } else {
			        deferred.resolve(isCorporateAdmin);
			    }
			    return deferred.promise;
			},
        getAdmins:
            function (search) {
                var deferred = $q.defer();
                $http.get('/api/user/alladmin?q=' + (isEmpty(search.q) ? '' : search.q) + '&p=' + (isEmpty(search.p) ? '' : search.p) + '&o=' + (isEmpty(search.o) ? '' : search.o) + '&d=' + (isEmpty(search.d) ? '' : search.d)).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        getApiUsers:
            function (search) {
                var deferred = $q.defer();
                $http.get('/api/user/allapi?q=' + (isEmpty(search.q) ? '' : search.q) + '&p=' + (isEmpty(search.p) ? '' : search.p) + '&o=' + (isEmpty(search.o) ? '' : search.o) + '&d=' + (isEmpty(search.d) ? '' : search.d)).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        getStaff:
            function (search) {
                var deferred = $q.defer();
                $http.get('/api/propertystaff/all?q=' + (isEmpty(search.q) ? '' : search.q) + '&p=' + (isEmpty(search.p) ? '' : search.p) + '&o=' + (isEmpty(search.o) ? '' : search.o) + '&d=' + (isEmpty(search.d) ? '' : search.d)).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        deleteAdmin:
            function (admin) {
                var deferred = $q.defer();
                $http.post('/api/user/deleteadmin', admin).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        deleteApiUser:
            function (apiUser) {
                var deferred = $q.defer();
                $http.post('/api/user/deleteapiuser', apiUser).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        toggleApiUser:
            function (apiUser) {
                var deferred = $q.defer();
                $http.post('/api/user/toggleapiuser', apiUser).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            }
    };
})
.factory('calendarEventService', function ($rootScope, $http, $q) {
    var calendarEvent = null;
    var eventId = null;
    return {
        createEvent:
                function () {
                    $rootScope.$broadcast('event:create');
                },
        saveNewEvent:
            function (calendarEvent) {
                $rootScope.$broadcast('event:saved');
                var deferred = $q.defer();
                $http.post('/api/calendarevent/addevent', calendarEvent).success(function (data) {
                        deferred.resolve(data);
                    });
                return deferred.promise;
            },
        saveExistingEvent:
            function (calendarEvent) {
                $rootScope.$broadcast('event:saved');
                var deferred = $q.defer();
                $http.post('/api/calendarevent/updateevent', calendarEvent).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        deleteEvent:
            function (eventId) {
                var deferred = $q.defer();
                $http.post('/api/calendarevent/deleteevent/' + eventId).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        editEvent:
            function (id) {
                var deferred = $q.defer();
                $http.get('/api/calendarevent/editevent/' + id).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        getEvents:
			function () {
				var deferred = $q.defer();
				$http.get('/api/calendarevent/all').success(function (data) {
				    deferred.resolve(data);
				});
				return deferred.promise;
			},
        getEvent:
            function(id) {
                var deferred = $q.defer();
                $http.get('/api/calendarevent/getbyeventid/'+id).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        showEvent:
            function (id) {
                eventId = id;
                $rootScope.$broadcast('Event:Show');
            },
        getEventId:
            function () {
                return eventId;
            },
        getEventTypes:
            function () {
                var deferred = $q.defer();
                $http.get('/api/calendarevent/geteventtypes').success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            }
    };
})
.factory('staffService', function ($rootScope, $http, $q) {
    return {
        createStaff:
            function (staff) {
                var deferred = $q.defer();
                $http.post('/api/propertystaff/addpropertystaff', staff).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        deleteStaff:
            function (staff) {
                var deferred = $q.defer();
                $http.post('/api/propertystaff/deletestaff', staff).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        editStaff:
            function (staff) {
                var deferred = $q.defer();
                $http.post('/api/propertystaff/updatepropertystaff', staff).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        getStaff:
            function (id) {
                var deferred = $q.defer();
                $http.get('/api/propertystaff/getstaff/'+id).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        getStates:
            function () {
                var deferred = $q.defer();
                $http.get('/api/propertystaff/getstatelist').success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            }
        
    }
})
.directive(
    'loading', function () {
        return {
            restrict: 'E',
            templateUrl: '/loading.html',
            replace: true,
            transclude: true
        };
    });