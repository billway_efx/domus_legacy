﻿angular.module('transactionServices', [])
    .factory('Payment', function ($rootScope, $http, $q) {
        var paymentEntity = null;

        return {
            voidPayment:
                function (payment) {
                    paymentEntity = payment;
                    $rootScope.$broadcast('payment:void');
                },
            refundPayment:
                function (payment) {
                    paymentEntity = payment;

                    $rootScope.$broadcast('payment:refund');
                },
            getPaymentObject:
                function () {
                    return paymentEntity;
                },
            getUpdatedPaymentObject:
                function (paymentId) {
                    var deferred = $q.defer();
                    $http.get('/api/achpayment/get/' + paymentId).success(function (data) {
                        paymentEntity = data;
                        deferred.resolve(data);
                        $rootScope.$broadcast('payment:updated');
                    });
                    return deferred.promise;
                },
            processRefund:
                function (paymentId, refundAmount) {
                    var deferred = $q.defer();
                    var params = { PaymentId: paymentId, Amount: refundAmount };
                    if (paymentEntity.CreditCardHolderName != null) {
                        $http.post('/api/creditcardpayment/refund', params).success(function (data) {
                            deferred.resolve(data);
                        });
                    } else {
                        $http.post('/api/achpayment/refund', params).success(function (data) {
                            deferred.resolve(data);
                        });
                    }
                    return deferred.promise;
                },
            processVoid:
                function (paymentId, voidAmount) {
                    //dWillis 00232 added popup menus to show when a transaction voids. 
                    var deferred = $q.defer();
                    var params = { PaymentId: paymentId, Amount: voidAmount };
                    if (paymentEntity.CreditCardHolderName != null) {
                        $http.post('/api/creditcardpayment/void', params).success(function (data) {
                            if (deferred.resolve(data)) {
                                alert("Your Transaction has Been Voided.");
                            }
                            else {
                               alert("Your Transaction has Been Voided.");
                            }
                        }),
                            $http.post('/api/creditcardpayment/void', params).error(function (data) {
                                alert("Your Credit Card Transaction could not be Voided.");
                        });
                    }
                    else {
                        //Patrick Whittingham - 03/25/2015 - task 00317 fix alerts to show different if success or error. 
                        $http.post('/api/achpayment/void', paymentEntity).success(function (data) {
                            deferred.resolve(data);
                            //alert("Your Payment Transaction Could Not Be Voided.");
                            alert("Your Payment Transaction has been Voided.");
                        }),
                        $http.post('/api/achpayment/void', params).error(function (data) {
                            alert("Your Payment Transaction could not Be Voided.");
                         });
                    }

                    return deferred.promise;
                }
        };
    });
