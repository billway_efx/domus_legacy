﻿//app
var residentApp = angular.module('rpoResidentDetail', ['ngRoute', '$strap.directives', 'transactionServices', 'pagination', 'API', 'ngAnimate', 'ajoslin.promise-tracker']);
//directives for validation
residentApp.directive('decimal', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            //initialize to valid
            scope.$watch('decimal', function () {
                ctrl.$setViewValue(ctrl.$viewValue);
            });
            var decimalValidator = function (value) {
                var DECIMAL_REGEXP = /^\d+(\.\d{1,2})?$/;
                if (!isEmpty(value) && !DECIMAL_REGEXP.test(value)) {
                    // it is valid
                    ctrl.$setValidity('decimal', false);
                    return undefined;
                } else {
                    // it is invalid, return undefined (no model update)
                    ctrl.$setValidity('decimal', true);
                    return value;
                }
            };

            ctrl.$parsers.push(decimalValidator);
            ctrl.$formatters.push(decimalValidator);

            //ctrl.$parsers.unshift(function (viewValue) {
            //    var DECIMAL_REGEXP = /^\d+(\.\d{1,2})?$/;
            //    if (!DECIMAL_REGEXP.test(viewValue)) {
            //        // it is valid
            //        ctrl.$setValidity('decimal', false);
            //        return undefined;
            //    } else {
            //        // it is invalid, return undefined (no model update)
            //        ctrl.$setValidity('decimal', true);
            //        return viewValue;
            //    }
            //});

        }
    };
});
residentApp.directive('decimalPlaces', function () {
    return {
        link: function (scope, ele, attrs) {
            ele.bind('keypress', function (e) {
                var newVal = $(this).val() + (e.charCode !== 0 ? String.fromCharCode(e.charCode) : '');
                if ($(this).val().search(/(.*)\.[0-9][0-9]/) === 0 && newVal.length > $(this).val().length) {
                    e.preventDefault();
                }
            });
        }
    };
});
residentApp.directive('ngMin', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            scope.$watch(attr.ngMin, function () {
                ctrl.$setViewValue(ctrl.$viewValue);
            });
            var minValidator = function (value) {
                var min = scope.$eval(attr.ngMin) || 0;
                if (!isEmpty(value) && value < min) {
                    ctrl.$setValidity('ngMin', false);
                    return undefined;
                } else {
                    ctrl.$setValidity('ngMin', true);
                    return value;
                }
            };

            ctrl.$parsers.push(minValidator);
            ctrl.$formatters.push(minValidator);
        }
    };
});

residentApp.directive('ngMax', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            scope.$watch(attr.ngMax, function () {
                ctrl.$setViewValue(ctrl.$viewValue);
            });
            var maxValidator = function (value) {
                var max = scope.$eval(attr.ngMax) || Infinity;
                if (!isEmpty(value) && value > max) {
                    ctrl.$setValidity('ngMax', false);
                    return undefined;
                } else {
                    ctrl.$setValidity('ngMax', true);
                    return value;
                }
            };

            ctrl.$parsers.push(maxValidator);
            ctrl.$formatters.push(maxValidator);
        }
    };
});
//controllers
function ResidentController($scope, $http, $routeParams, $location, $q, $modal, $rootScope, $filter, Payment, renterService, promiseTracker) {
    $scope.urlPrefix = navigator.appVersion.indexOf('MSIE') == -1 ? "" : "/rpo/#!";
    $scope.params = $routeParams;
    $scope.payment = Payment;
    

    $scope.viewTitle = 'Resident Data';
    
    var renterId = $routeParams.residentId != null ? $routeParams.residentId : $.getUrlVar('renterId');
    var renterPromise = renterService.getRenter(renterId);
    if (navigator.appVersion.indexOf('MSIE 8.0') == -1) {
        $scope.loading = promiseTracker('renter');
        $scope.loading.addPromise(renterPromise);
    }
    renterPromise.then(function (renter) {
        $scope.renter = renter;
    });
    $scope.hideTabs = function () {
        $('#NameTab').hide();
        $('#PayersInfo').hide();
        $('#PaymentInformationTab').hide();
        $('#LeaseInformationTab').hide();
        $('#HistoryTab').hide();
        $('#NotesTab').hide();
        //Salcedo - task 00076
        $('#LogTab').hide();
    };
    $scope.hideTransactions = function () {
        $scope.showAllTransactions = false;
        angular.forEach($scope.renter.PaymentHistory, function (value) {
            value.showTransactions = false;
        });
    };
    $scope.refundTransaction = function (payment) {
        $scope.payment.refundPayment(payment);
    };

    //cakel: BUGID00232 Added code to set visibilty to Void Button
    $scope.setVoidButtonVisibility = function (TransactionDateTime, PaymentStatus, PaymentTypeId) {
        var TransactionTime = new Date(TransactionDateTime);
        var transactionTimeUTC = new Date(TransactionTime.getUTCFullYear(), TransactionTime.getUTCMonth(), TransactionTime.getUTCDate(), TransactionTime.getUTCHours(), TransactionTime.getUTCMinutes());
        var Today = new Date();
        var TodayUTC = new Date(Today.getUTCFullYear(), Today.getUTCMonth(), Today.getUTCDate(), Today.getUTCHours(), Today.getUTCMinutes());
        var cutOff = new Date();
        cutOff = new Date(cutOff.getUTCFullYear(), cutOff.getUTCMonth(), cutOff.getUTCDate(), 23, 0);
        var EOD = new Date();
        EOD = new Date(EOD.getUTCFullYear(), EOD.getUTCMonth(), EOD.getUTCDate() + 1, 5, 0);
        var yesterday = TodayUTC;
        var NextSettleTime;
        var yesterDaySettle;
        if (TodayUTC < cutOff && TodayUTC > EOD) {
            yesterday.setDate(yesterday.getDate() - 1);
            NextSettleTime = new Date(yesterday.getUTCFullYear(), yesterday.getUTCMonth(), yesterday.getUTCDate(), 23, 0);
        }
        else {

            NextSettleTime = new Date(Today.getUTCFullYear(), Today.getUTCMonth(), Today.getUTCDate(), 23, 0);
        }

        yesterDaySettle = new Date(yesterday.getUTCFullYear(), yesterday.getUTCMonth(), yesterday.getUTCDate() - 1, 23, 0);

        if (transactionTimeUTC < NextSettleTime && transactionTimeUTC > yesterDaySettle) {
            //Show Void Button - Do not show refund button
            //cakel: BUGID00232 Added variable  PaymentTypeID == 3 - Do not show if RBC
            if (PaymentStatus == "Voided" || PaymentStatus == "Refunded" || PaymentStatus == "Partially Refunded" & PaymentTypeId == 3) {
                
                    return { display: "none" }
                
                
            }
            else {
                
                    return { display: "block" }
                }
                
        }
        else {
            //Show Refund Button - Do not show Void button
            return { display: "none" }
        }

    }

    //cakel: BUGID00232 Added code to set visibilty to Refund Button
    $scope.setRefundButtonVisibility = function (TransactionDateTime, PaymentStatus, PaymentTypeId) {
        var TransactionTime = new Date(TransactionDateTime);
        var transactionTimeUTC = new Date(TransactionTime.getUTCFullYear(), TransactionTime.getUTCMonth(), TransactionTime.getUTCDate(), TransactionTime.getUTCHours(), TransactionTime.getUTCMinutes());
        var Today = new Date();
        var TodayUTC = new Date(Today.getUTCFullYear(), Today.getUTCMonth(), Today.getUTCDate(), Today.getUTCHours(), Today.getUTCMinutes());
        var cutOff = new Date();
        cutOff = new Date(cutOff.getUTCFullYear(), cutOff.getUTCMonth(), cutOff.getUTCDate(), 23, 0);
        var EOD = new Date();
        EOD = new Date(EOD.getUTCFullYear(), EOD.getUTCMonth(), EOD.getUTCDate() + 1, 5, 0);
        var yesterday = TodayUTC;
        var NextSettleTime;
        var yesterDaySettle;
        if (TodayUTC < cutOff && TodayUTC > EOD) {
            yesterday.setDate(yesterday.getDate() - 1);
            NextSettleTime = new Date(yesterday.getUTCFullYear(), yesterday.getUTCMonth(), yesterday.getUTCDate(), 23, 0);
        }
        else {

            NextSettleTime = new Date(Today.getUTCFullYear(), Today.getUTCMonth(), Today.getUTCDate(), 23, 0);
        }

        yesterDaySettle = new Date(yesterday.getUTCFullYear(), yesterday.getUTCMonth(), yesterday.getUTCDate() - 1, 23, 0);

        //cakel: BUGID00232 Added variable  PaymentTypeID == 3 - Do not show if RBC
        if (transactionTimeUTC < NextSettleTime && transactionTimeUTC > yesterDaySettle) {
            //Do not show refund button
            return { display: "none" }
            
        }
        else {
            //cakel: BUGID00232 Added variable  PaymentTypeID == 3 - Do not show if RBC
            //Only show if Partially Refunded, exclude others
            if (PaymentStatus == "Voided" || PaymentStatus == "Refunded") {
                return { display: "none" }
            }
            else {
                //Show Refund Button - Do not show Void button
                if (PaymentTypeId == 3) {
                    return { display: "none" }
                }
                else {
                    return { display: "block" }
                }
                
            }
        }
    }


    $scope.showTab = function (tab) {
        $scope.hideTabs();
        $('#' + tab).show();
        $scope.activeTab = tab;

    };
    $scope.showTransactions = function () {
        $scope.showAllTransactions = true;
        angular.forEach($scope.renter.PaymentHistory, function (Payment) {

        });
    };
    $scope.toggleActive = function () {
        renterService.toggleActive($scope.renter).then(function (success) {
            if (success) {
                renterService.getRenter(renterId).then(function (renter) {
                    angular.extend($scope.renter, renter);
                });
            } else {
                alert('Failed to toggle Resident Active/InActive');
            }
        });


    };
    $scope.togglePaymentTransactions = function (payment) {
        payment.showTransactions = !payment.showTransactions;
    };
    $scope.voidTransaction = function (payment) {
        $scope.payment.voidPayment(payment);
    };

    $scope.hideTabs();
    $scope.showTab('NameTab');
}
function VoidTransactionController($scope, $http, $rootScope, Payment) {
    $scope.payment = Payment;
    $scope.refundButtonText = "Void Transaction";
    $rootScope.$on('payment:void', function () {
        $scope.paymentToVoid = Payment.getPaymentObject();
        $scope.amount = $scope.paymentToVoid.Balance;
    });
    $scope.success = false;
   
    $scope.submit = function () {
        $scope.submitDisabled = true;

        Payment.processVoid($scope.paymentToVoid.PaymentId, $scope.amount).then(function (response) {
             Payment.getUpdatedPaymentObject($scope.paymentToVoid.PaymentId).then(function (payment) {
                 angular.extend($scope.paymentToVoid, payment);
        });
        }) 
       
        
    };
}
function RefundController($scope, $http, $rootScope, Payment) {
    $scope.payment = Payment;
    $scope.refundButtonText = "Issue Refund";
    $rootScope.$on('payment:refund', function () {
        $scope.paymentToRefund = Payment.getPaymentObject();
        $scope.amount = $scope.paymentToRefund.Balance;
    });
    $scope.success = false;
    $scope.submit = function () {
        $scope.submitDisabled = true;
        $scope.refundButtonText = "Processing...";
        Payment.processRefund($scope.paymentToRefund.PaymentId, $scope.amount).then(function (response) {
            Payment.getUpdatedPaymentObject($scope.paymentToRefund.PaymentId).then(function (payment) {
                $scope.success = true;
                $scope.refundButtonText = "Processed";
                setTimeout(function () {
                    $scope.success = false;
                    $('#refundTransactionModal').modal('hide');
                    $scope.submitDisabled = false;
                    $scope.refundButtonText = "Issue Refund";
                }, 1000);
                angular.extend($scope.paymentToRefund, payment);
            });
        });
    };
    //validation
}
angular.bootstrap($('#renterManager'), ["rpoResidentDetail"]);