﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopNavMenu2.ascx.cs" Inherits="RPO_New2.UserControls.TopNavMenu2" %>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="Default.aspx"><i class="fa fa-building-o"></i>&nbsp;RentPaidOnline</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right text-shadow">
            <li class="hidden">
                <%-- <a href="#page-top"></a>--%>
                    </li>
            <li>
                <a href="Default.aspx#services">Services</a>
            </li>

            <li>
                <a href="Default.aspx#about">About</a>
            </li>
            <li>
                <a href="Default.aspx#contact">Contact</a>
            </li>
            <li>
                <a href="Residents.aspx#Residents">Residents</a>
            </li>
            <li>
                <a href="PropertyManagers.aspx">Property Managers</a>
            </li>



            <li>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Login<span class="caret"></span></button>
                    <ul class="dropdown-menu dropdownColorDark">
                        <li>
                            <a href="Login.aspx">Residents</a>
                        </li>
                        <li>
                            <a href="Login.aspx">Property Managers</a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
               
        
                
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>