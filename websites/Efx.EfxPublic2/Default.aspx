﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="RPO_New2.Default" %>

<%@ Register Src="~/UserControls/TopNavMenu.ascx" TagPrefix="uc1" TagName="TopNavMenu" %>
<%@ Register Src="~/UserControls/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>RentPaidOnline - Premier Multifamily Financial Services</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/agency.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">
    <form id="form1" runat="server">

    <uc1:TopNavMenu runat="server" id="TopNavMenu" />



    <!-- Header -->
    <header>
        <div class="container">
            <div class="row">


            
            <div class="intro-text">
                
                <div class="intro-lead-in text-shadow">Premier Multifamily financial Services</div>
                <div class="intro-heading text-shadow text-yellow">FAST, SECURE and AWESOME!</div>
                <a href="#services" class="page-scroll btn btn-xl">Tell Me More</a>
            </div>
                </div>
        </div>
    </header>

        <div class="clearfix"></div>



    <!-- Services Section -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Services</h2>
                    <h3 class="section-subheading text-muted">We have the solutions you need to help manage your properties and residents</h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-money fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Streamline Payments</h4>
                    <p class="text-muted">We will help your property achieve complete automation by offering multiple ways to capture your payments. You can finally say goodbye to manual entry and time wasting trips to the bank.</p>
                </div>

                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-bar-chart-o fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Advanced Reporting</h4>
                    <p class="text-muted">RentPaidOnline® knows how important it is to track and reconcile payments efficiently. This is why we designed a customized report approach to meet the needs of every department in your organization!</p>
                </div>

                <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Email Marketing</h4>
                    <p class="text-muted">RentPaidOnline® offers a revolutionary marketing engine to ensure you get the adoption you want. With on-demand marketing resources built in, there’s no more waiting on marketing departments!</p>
                </div>

                 <div class="col-md-3">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-plug fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Integration Spcialist</h4>
                    <p class="text-muted">RentPaidOnline® offers a streamlined solution for integrating with today’s most popular property management software. Take the hassle out of rent week by using one of our innovative, easy to use solutions!</p>
                </div>




            </div>
        </div>
    </section>

        <aside class="clients" style="background-color:rgb(204, 204, 204)">
        <div class="container text-center">
            <div class="col-lg-10 center">

            
            <h2>Integrated Partners</h2>
                <p>Keep your current property management software.  Let us help you manage your payables and collectables better by integrating with your current provider.</p>
            <div class="row">

                    <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img src="img/AMSI.png" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img src="img/Yardi.png" class="img-responsive img-centered" alt="">
                    </a>
                </div>
            
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img src="img/MRI.png" class="img-responsive img-centered" alt="">
                    </a>
                </div>

            </div>
                </div>
        </div>
    </aside>


    

    <!-- About Section -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">About</h2>
                    <h3 class="section-subheading text-muted">Our mission statement is just three words ... Make It Easy</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p>
                        We want to make the process of paying rent for residents to be the easiest thing they do all month, so we provide the simplest to use applications for rent payments, accessible through the most convenient methods possible.

On-line, mobile, AutoPay, text messaging, cash, even by telephone. Whatever Makes It Easy for residents to pay their rent.

We have the same mission statement for our Property Managers, Make it Easy for them to track, account for and report on rent payments, so they can focus more on the business of customer service, rather than collecting rent checks.

Help us with our mission - let us know how we can Make It Easy for you.

                    </p>
                </div>
            </div>
        </div>
    </section>




    
    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Contact Us</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" placeholder="Your Phone *" id="phone" required data-validation-required-message="Please enter your phone number.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<uc1:Footer runat="server" id="Footer" />








 
    



    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/agency.js"></script>


            </form>


</body>

</html>
