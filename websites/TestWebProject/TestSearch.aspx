﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestSearch.aspx.cs" Inherits="RentPaidOnline.TestSearch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h2>Testing Search Popup</h2>

        <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />&nbsp;
        <br />
        <br />

<asp:Panel ID="ModalPanel" runat="server" Width="500px">
 ASP.NET AJAX is a free framework for quickly creating a new generation of more 
 efficient, more interactive and highly-personalized Web experiences that work 
 across all the most popular browsers.<br />
 <asp:Button ID="OKButton" runat="server" Text="Close" />
</asp:Panel>

        <ajaxToolkit:modalpopupextender ID="mpe" runat="server" TargetControlId="ClientButton" 
 PopupControlID="ModalPanel" OkControlID="OKButton" />

        <asp:ScriptManager ID="asm" runat="server" />

    </div>

    </form>
</body>
</html>
