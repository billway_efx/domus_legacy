﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Text;
using EfxFramework.Scheduler;
using System.Net;
using System.Net.Mail;
using EfxFramework;

namespace RentPaidOnline
{
    public partial class TestSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        public string mysqlstring()
        {
            string sqlstring = "Select 'Detail', RenterID, RentAmount from [dbo].[Payment] Where IsPmsProcessed = 0 and TransactionDateTime  > '01/04/2014'";
           // string sqlstring2 = "SELECT (Select RenterID, RentAmount from Payment as a Where IsPmsProcessed = 0 and a.PaymentId = b.PaymentId and a.RenterId = b.RenterId FOR XML AUTO, TYPE) as RTServiceTransactions, [TransactionDateTime] FROM [dbo].[Payment] b Where [TransactionDateTime] > '07/31/2014' and IsPmsProcessed = 0 and RenterID = 52738 FOR XML AUTO, TYPE";
            return sqlstring;
        }


        public DataSet GetDataSet()
        {
            SqlConnection con = new SqlConnection(ConnString);
            string _sql = mysqlstring();
            

            con.Open();
            SqlDataAdapter SqlAdapt = new SqlDataAdapter(_sql, con);
            DataSet ds = new DataSet();
            SqlAdapt.Fill(ds, "MyData");

            return  ds;
        }




        protected void Button1_Click(object sender, EventArgs e)
        {
              
        }


        public SqlDataReader GetSqlReader()
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Payment_GetAllUnprocessYARDIPaymentsWithDepositDate",con);
            com.CommandType = CommandType.StoredProcedure;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                ex.ToString();
                con.Close();
                return null;
            }

        }

        public SqlDataReader GetSqlReader2()
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Payment_GetPaymentsForGroupAchbyProperty",con);
            com.CommandType = CommandType.StoredProcedure;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                ex.ToString();
                con.Close();
                return null;
            }
        }

        public DataSet MyNewDataSet()
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                SqlCommand sqlComm = new SqlCommand("usp_Payment_GetPaymentsForGroupAchbyProperty", conn);
                sqlComm.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;
                da.Fill(ds);
            }
            return ds;
        }


  
 




        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

      

 
        }
    }
