﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RPO
{
    public partial class PropertyStaff
    {
        public string DisplayName { get { return FirstName + " " + LastName; } }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public List<int> PropertyIds { get; set; }
        [DataMember]
		public int RoleId { get; set; }
        [DataMember]
		public bool IsMainContact { get; set; }
    }
}
