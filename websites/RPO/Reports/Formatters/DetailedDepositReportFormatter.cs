﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports.Formatters
{
	public class DetailedDepositReportFormatter : IReportFormatter
	{
		private List<ColumnFormat> columnFormats;

		public DetailedDepositReportFormatter()
		{
			columnFormats = new List<ColumnFormat>();
			columnFormats.Add(new ColumnFormat { ColumnName = "DepositDate", ColumnRename = "Deposit Date", FormatString = "MM/dd/yyyy" });
			columnFormats.Add(new ColumnFormat { ColumnName = "PropertyId", ColumnRename = "Prop. Id", FormatString = "" });
			columnFormats.Add(new ColumnFormat { ColumnName = "PropertyName", ColumnRename = "Property Name", FormatString = "" });
			columnFormats.Add(new ColumnFormat { ColumnName = "TransactionType", ColumnRename = "Transaction Type", FormatString = "" });
			columnFormats.Add(new ColumnFormat { ColumnName = "GroupGrandTotal", ColumnRename = "Total", FormatString = "C" });
			columnFormats.Add(new ColumnFormat { ColumnName = "BatchId", ColumnRename = "Batch Id", FormatString = "" });
			columnFormats.Add(new ColumnFormat { ColumnName = "PaymentDate", ColumnRename = "Payment Date", FormatString = "MM/dd/yyyy" });
			columnFormats.Add(new ColumnFormat { ColumnName = "IntegratedPropertyYesNo", ColumnRename = "Integrated", FormatString = "" });
			columnFormats.Add(new ColumnFormat { ColumnName = "Portal", ColumnRename = "Portal", FormatString = "" });
			columnFormats.Add(new ColumnFormat { ColumnName = "PayerName", ColumnRename = "Resident", FormatString = "" });
			columnFormats.Add(new ColumnFormat { ColumnName = "CreditCardType", ColumnRename = "Type", FormatString = "" });
			columnFormats.Add(new ColumnFormat { ColumnName = "PaymentAmount", ColumnRename = "Payment Amount", FormatString = "C" });
			columnFormats.Add(new ColumnFormat { ColumnName = "PaymentMethod", ColumnRename = "Payment Method", FormatString = "" });
			columnFormats.Add(new ColumnFormat { ColumnName = "InternalTransactionId", ColumnRename = "Id", FormatString = "" });
		}

		public List<ColumnFormat> ColumnFormats
		{
			get
			{
				return columnFormats;
			}
			set
			{
				columnFormats = value;
			}
		}
	}
}
