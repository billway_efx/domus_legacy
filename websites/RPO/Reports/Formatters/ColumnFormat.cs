﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports.Formatters
{
	public class ColumnFormat
	{
		public String ColumnName { get; set; }
		public String ColumnRename { get; set; }
		public String FormatString { get; set; }
		public Type ValueType { get; set; }
	}
}
