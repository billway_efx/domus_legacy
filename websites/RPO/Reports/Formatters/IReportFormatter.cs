﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports.Formatters
{
	public interface IReportFormatter
	{
		List<ColumnFormat> ColumnFormats { get; set; }
	}
}
