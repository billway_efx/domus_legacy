﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports
{
    public class RentersListing
    {
        public int RenterId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RenterName { get; set; }
    }
}
