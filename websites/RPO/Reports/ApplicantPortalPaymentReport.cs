﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports
{
    [DataContract(IsReference = true)]
    public class ApplicantPortalPaymentReport
    {
        [DataMember]
        public string GroupBy { get; set; }
        [DataMember]
        public int PropertyId { get; set; }
        [DataMember]
        public string PropertyName { get; set; }
        [DataMember]
        public int ApplicantId { get; set; }
        [DataMember]
        public string ApplicantName { get; set; }
        [DataMember]
        public string InternalTransactionId { get; set; }
        [DataMember]
        public DateTime TransactionDate { get; set; }
        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public string FeeName { get; set; }
        [DataMember]
        public decimal Total { get; set; }
        [DataMember]
        public string PaymentTypeName { get; set; }
        [DataMember]
        public List<ApplicantPortalPaymentReportDetails> Details { get; set; }

        public void copyValues(ApplicantPortalPaymentReport copy)
        {
            this.GroupBy = copy.GroupBy;
            this.PropertyId = copy.PropertyId;
            this.PropertyName = copy.PropertyName;
            this.ApplicantId = copy.ApplicantId;
            this.ApplicantName = copy.ApplicantName;
            this.InternalTransactionId = copy.InternalTransactionId;
            this.TransactionDate = copy.TransactionDate;
            this.Amount = copy.Amount;
            this.FeeName = copy.FeeName;
            this.Total = copy.Total;
            this.PaymentTypeName = copy.PaymentTypeName;
            this.Details = new List<ApplicantPortalPaymentReportDetails>();
        }
    }
    [DataContract(IsReference = true)]
    public class ApplicantPortalPaymentReportDetails
    {
        [DataMember]
        public string FeeName { get; set; }
        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public string PaymentTypeName { get; set; }
    }
}
