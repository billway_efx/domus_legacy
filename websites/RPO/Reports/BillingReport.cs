﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports
{
    public class BillingReport
    {
        public string PropertyName { get; set; }
        public string ProgramDescription { get; set; }
        public int NumberOfUnits { get; set; }
        public string RentalDepositBankAccountNumber { get; set; }
        public string RentalDepositBankRoutingNumber { get; set; }
    }
}
