﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports
{
    public class PropertiesListing
    {
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
    }
}
