﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports
{
    public class PaymentExportSummaryReport
    {
        public int PaymentExportSummary { get; set; }
        public DateTime DepositDate { get; set; }
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public decimal Amount { get; set; }
        public string Status { get; set; }
        public string BatchId { get; set; }
        public string BatchDetail { get; set; }
		public DateTime ExportDate { get; set; }
		public string ResponseXml { get; set; }
		public string RequestXml { get; set; }
    }
}
