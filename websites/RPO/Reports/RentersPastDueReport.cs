﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports
{
    public class RentersPastDueReport
    {
        public int PropertyId { get; set; }
	    public int PmsTypeId { get; set; }
	    public string PropertyName { get; set; }
	    public string RenterName { get; set; }
	    public DateTime LastDatePaid { get; set; }
	    public decimal RentCollected { get; set; }
        public decimal RentDue { get; set; }
        public string TimeZone { get; set; }
    }
}
