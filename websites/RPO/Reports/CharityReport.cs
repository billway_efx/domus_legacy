﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports
{
    public class CharityReport
    {
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public string CharityName { get; set; }
        public string RenterName { get; set; }
        public DateTime datetime { get; set; }
        public decimal CharityAmount { get; set; }
    }
}
