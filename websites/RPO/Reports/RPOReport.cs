﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports
{
    public class RPOReport
    {
        public List<ApplicantPortalPaymentReport> ApplicantPortalPaymentReport(ReportQuery query)
        {
            List<ApplicantPortalPaymentReport> returnList = null;
            List<ApplicantPortalPaymentReport> finalList = new List<ApplicantPortalPaymentReport>();
            using (var entity = new RPOEntities())
            {
                Object[] parameters = new Object[3];
                SqlParameter param1 = new SqlParameter("@StartDate", query.StartDate.ToShortDateString());
                SqlParameter param2 = new SqlParameter("@EndDate", query.EndDate.ToShortDateString());
                SqlParameter param3 = new SqlParameter("@PropertyIdList", string.Join(",", query.PropertyIdList));
                parameters[0] = param1;
                parameters[1] = param2;
                parameters[2] = param3;
                returnList = entity.Database.SqlQuery<ApplicantPortalPaymentReport>("usp_Report_ApplicantPortalPaymentReport @StartDate, @EndDate, @PropertyIdList", parameters).ToList();
            }

            if (!query.FlattenData)
            {
                foreach (IGrouping<string, ApplicantPortalPaymentReport> groupItem in returnList.ToLookup(t => t.InternalTransactionId))
                {
                    ApplicantPortalPaymentReport temp = new ApplicantPortalPaymentReport();
                    List<ApplicantPortalPaymentReportDetails> constructedDetailList = new List<ApplicantPortalPaymentReportDetails>();
                    foreach (ApplicantPortalPaymentReport t in groupItem)
                    {
                        temp.copyValues(t);
                        ApplicantPortalPaymentReportDetails details = new ApplicantPortalPaymentReportDetails();
                        details.FeeName = t.FeeName;
                        details.Amount = t.Amount;
                        details.PaymentTypeName = t.PaymentTypeName;
                        constructedDetailList.Add(details);
                    }
                    temp.Details.AddRange(constructedDetailList);
                    finalList.Add(temp);
                }
                return finalList;
            }
            return returnList;
        }

        public List<AutoPaymentReport> AutoPaymentReport(ReportQuery query)
        {
            //Salcedo - 4/19/2014 - To do: add support for the new PageNumber member variable of ReportQuery
            List<AutoPaymentReport> returnList = null;
            using (var entity = new RPOEntities())
            {
                Object[] parameters = new Object[2];
                SqlParameter param1 = new SqlParameter("@PropertyIdList", string.Join(",", query.PropertyIdList.ToArray()));

                //cakel: added new parameter
                //BugID 00018
                SqlParameter param2 = new SqlParameter("PageNumber", query.PageNumber);
                parameters[0] = param1;

                //cakel: added new parameter
                //BugID 00018
                parameters[1] = param2;
                //cakel: Updated stored procedure text with new parameter
                //BugID 00018
                returnList = entity.Database.SqlQuery<AutoPaymentReport>("usp_Report_AutoPaymentReport_v2 @PropertyIdList, @PageNumber", parameters).ToList();

            }
            return returnList;
        }


        public List<AutoPaymentReportExport> AutoPaymentReportExport(ReportQuery query)
        {
            //Salcedo - 4/19/2014 - To do: add support for the new PageNumber member variable of ReportQuery
            List<AutoPaymentReportExport> returnList = null;
            using (var entity = new RPOEntities())
            {
                Object[] parameters = new Object[2];
                SqlParameter param1 = new SqlParameter("@PropertyIdList", string.Join(",", query.PropertyIdList.ToArray()));

                //cakel: added new parameter
                //BugID 00018
                SqlParameter param2 = new SqlParameter("PageNumber", query.PageNumber);
                parameters[0] = param1;

                //cakel: added new parameter
                //BugID 00018
                parameters[1] = param2;
                //cakel: Updated stored procedure text with new parameter
                //BugID 00018
                returnList = entity.Database.SqlQuery<AutoPaymentReportExport>("usp_Report_AutoPaymentReport_Export @PropertyIdList, @PageNumber", parameters).ToList();

            }
            return returnList;
        }






        public List<BillingReport> BillingReport(ReportQuery query)
        {
            List<BillingReport> returnList = null;
            using (var entity = new RPOEntities())
            {
                Object[] parameters = new Object[1];
                SqlParameter param1 = new SqlParameter("@PropertyIdList", string.Join(",", query.PropertyIdList));
                parameters[0] = param1;
                returnList = entity.Database.SqlQuery<BillingReport>("usp_Report_BillingReport @PropertyIdList", parameters).ToList();
            }
            return returnList;
        }

        public List<CharityReport> CharityReport(ReportQuery query)
        {
            List<CharityReport> returnList = null;
            using (var entity = new RPOEntities())
            {
                Object[] parameters = new Object[3];
                SqlParameter param1 = new SqlParameter("@StartDate", query.StartDate.ToShortDateString());
                SqlParameter param2 = new SqlParameter("@EndDate", query.EndDate.ToShortDateString());
                SqlParameter param3 = new SqlParameter("@PropertyIdList", string.Join(",", query.PropertyIdList));
                parameters[0] = param1;
                parameters[1] = param2;
                parameters[2] = param3;
                returnList = entity.Database.SqlQuery<CharityReport>("usp_Report_CharityReport @StartDate, @EndDate, @PropertyIdList", parameters).ToList();
            }
            return returnList;
        }

        public List<DetailedDepositReport> DetailedDepositReport(ReportQuery query)
        {
            List<DetailedDepositReport> returnList = null;
            List<DetailedDepositReport> finalList = new List<DetailedDepositReport>();
            using (var entity = new RPOEntities())
            {
                Object[] parameters = new Object[3];
                SqlParameter param1 = new SqlParameter("@StartDate", query.StartDate.ToShortDateString());
                SqlParameter param2 = new SqlParameter("@EndDate", query.EndDate.ToShortDateString());
                SqlParameter param3 = new SqlParameter("@PropertyIdList", string.Join(",", query.PropertyIdList));
                parameters[0] = param1;
                parameters[1] = param2;
                parameters[2] = param3;
                //cakel: BUGID00234 - Updated SP to accomodate "RentByCash" Settlement
                returnList = entity.Database.SqlQuery<DetailedDepositReport>("usp_Report_DetailedDepositReport_V3 @StartDate, @EndDate, @PropertyIdList", parameters).ToList();
            }

            if (!query.FlattenData)
            {
                foreach (IGrouping<string, DetailedDepositReport> groupItem in returnList.ToLookup(t => t.GroupBy))
                {
                    DetailedDepositReport temp = new DetailedDepositReport();
                    List<DetailedDepositReportDetail> constructedDetailList = new List<DetailedDepositReportDetail>();
                    int count = 0;
                    foreach (DetailedDepositReport t in groupItem)
                    {
                        count++;
                        temp.copyValues(t);
                        DetailedDepositReportDetail details = new DetailedDepositReportDetail();
                        details.BatchId = t.BatchId;
                        details.PaymentDate = t.PaymentDate;
                        details.IntegratedPropertyYesNo = t.IntegratedPropertyYesNo;
                        details.Portal = t.Portal;
                        details.PayerName = t.PayerName;
                        details.CreditCardType = t.CreditCardType;
                        details.PaymentAmount = t.PaymentAmount;
                        details.TransactionId = t.TransactionId;
                        details.InternalTransactionId = t.InternalTransactionId;
                        details.DatePaidMmYyyy = t.DatePaidMmYyyy;
                        details.RenterId = t.RenterId;
                        details.PaymentStatusId = t.PaymentStatusId;
                        constructedDetailList.Add(details);
                    }
                    temp.Count = count;
                    temp.Details.AddRange(constructedDetailList);
                    finalList.Add(temp);
                }
                return finalList;
            }
            return returnList;
        }

        public List<PaymentType> AllPaymentTypes()
        {
            List<PaymentType> returnList = new List<PaymentType>();
            using (var entity = new RPOEntities())
            {
                returnList = entity.PaymentTypes.OrderBy(pt => pt.PaymentTypeName).ToList();
            }
            return returnList;
        }

        public List<PropertiesListing> AllProperties()
        {
            using (var entity = new RPOEntities())
            {
                return entity.Database.SqlQuery<PropertiesListing>("usp_Report_ListAllProperties").ToList();
            }
        }

        public List<RentersListing> AllRenters()
        {
            using (var entity = new RPOEntities())
            {
                return entity.Database.SqlQuery<RentersListing>("usp_Report_ListAllRenters").ToList();
            }
        }

        public List<PropertiesListing> PropertiesByStaffId(ReportQuery query)
        {
            List<PropertiesListing> returnList = null;
            using (var entity = new RPOEntities())
            {
                Object[] parameters = new Object[1];
                SqlParameter param1 = new SqlParameter("@PropertyStaffId", query.PropertyStaffId);
                parameters[0] = param1;
                //cakel: BUGID00059 - Added new SP to return full list of properties regardless of what is in the PropertyPropertyStaff Table
                returnList = entity.Database.SqlQuery<PropertiesListing>("usp_Report_ListPropertiesByPropertyStaffId_V2 @PropertyStaffId", parameters).ToList();
                //returnList = entity.Database.SqlQuery<PropertiesListing>("usp_Report_ListPropertiesByPropertyStaffId @PropertyStaffId", parameters).ToList();
            }
            return returnList;
        }

        public List<RentersListing> RentersByPropertyId(ReportQuery query)
        {
            List<RentersListing> returnList = null;
            using (var entity = new RPOEntities())
            {
                Object[] parameters = new Object[1];
                SqlParameter param1 = new SqlParameter("@PropertyIdList", string.Join(",", query.PropertyIdList));
                parameters[0] = param1;
                returnList = entity.Database.SqlQuery<RentersListing>("usp_Report_ListRentersByPropertyId @PropertyIdList", parameters).ToList();
            }
            return returnList;
        }

        public List<LeadReport> LeadReport(ReportQuery query)
        {
            List<LeadReport> returnList = null;
            using (var entity = new RPOEntities())
            {
                Object[] parameters = new Object[2];
                SqlParameter param1 = new SqlParameter("@StartDate", query.StartDate.ToShortDateString());
                SqlParameter param2 = new SqlParameter("@EndDate", query.EndDate.ToShortDateString());
                parameters[0] = param1;
                parameters[1] = param2;
                returnList = entity.Database.SqlQuery<LeadReport>("usp_Report_LeadReport @StartDate, @EndDate", parameters).ToList();
            }

            returnList.ForEach(lr => lr.DateCreated = Helpers.DateTimeHelper.setDateTimeZone(lr.DateCreated, "Eastern Standard Time"));
            return returnList;
        }

        public List<PaymentExportSummaryReport> PaymentExportSummaryReport(ReportQuery query)
        {
            List<PaymentExportSummaryReport> returnList = null;
            using (var entity = new RPOEntities())
            {
                Object[] parameters = new Object[3];
                SqlParameter param1 = new SqlParameter("@StartDate", query.StartDate.ToShortDateString());
                SqlParameter param2 = new SqlParameter("@EndDate", query.EndDate.ToShortDateString());
                SqlParameter param3 = new SqlParameter("@PropertyIdList", string.Join(",", query.PropertyIdList));
                parameters[0] = param1;
                parameters[1] = param2;
                parameters[2] = param3;
                returnList = entity.Database.SqlQuery<PaymentExportSummaryReport>("usp_Report_PaymentExportSummaryReport @StartDate, @EndDate, @PropertyIdList", parameters).ToList();
            }
            returnList.ForEach(pe => pe.DepositDate = Helpers.DateTimeHelper.setDateTimeZone(pe.DepositDate, "Eastern Standard Time"));
			returnList.ForEach(pe => pe.ExportDate = Helpers.DateTimeHelper.setDateTimeZone(pe.ExportDate, "Eastern Standard Time"));
            return returnList;
        }

        public List<PaymentReceiptReport> PaymentReceiptReport(ReportQuery query)
        {
            List<PaymentReceiptReport> returnList = null;
            using (var entity = new RPOEntities())
            {
                Object[] parameters = new Object[1];
                SqlParameter param1 = new SqlParameter("@TransactionId", query.TransactionId);
                parameters[0] = param1;
                returnList = entity.Database.SqlQuery<PaymentReceiptReport>("usp_Report_PaymentReceipt @TransactionId", parameters).ToList();
            }
            return returnList;
        }

        public List<PaymentSummaryReport> PaymentSummaryReport(ReportQuery query)
        {
            List<PaymentSummaryReport> returnList = null;
            List<PaymentSummaryReport> finalList = new List<PaymentSummaryReport>();
            using (var entity = new RPOEntities())
            {
                Object[] parameters = new Object[3];
                SqlParameter param1 = new SqlParameter("@StartDate", query.StartDate.ToShortDateString());
                SqlParameter param2 = new SqlParameter("@EndDate", query.EndDate.ToShortDateString());
                SqlParameter param3 = new SqlParameter("@PropertyIdList", string.Join(",", query.PropertyIdList));
                parameters[0] = param1;
                parameters[1] = param2;
                parameters[2] = param3;
                //cakel: BUGID00234 - Updated SP to new procedure to accomodate when User pays with "RentByCash"
                returnList = entity.Database.SqlQuery<PaymentSummaryReport>("usp_Report_PaymentSummaryReport_V2 @StartDate, @EndDate, @PropertyIdList", parameters).ToList();
            }
            returnList.ForEach(r => r.DepositDate = CorrectTheDate(r.DepositDate, r.TimeZone));
            returnList.ForEach(r => r.DatePaid = CorrectTheDate(r.DatePaid, r.TimeZone));

            if (!query.FlattenData)
            {
                foreach (IGrouping<string, PaymentSummaryReport> groupItem in returnList.ToLookup(t => t.GroupBy))
                {
                    PaymentSummaryReport temp = new PaymentSummaryReport();
                    List<PaymentSummaryReportDetail> constructedDetailList = new List<PaymentSummaryReportDetail>();
                    decimal total = 0;
                    foreach (PaymentSummaryReport t in groupItem)
                    {
                        temp.copyValues(t);
                        PaymentSummaryReportDetail details = new PaymentSummaryReportDetail();
                        details.PaymentMethod = t.PaymentMethod;
                        details.AccountNumber = t.AccountNumber;
                        details.InternalTransactionId = t.InternalTransactionId;
                        details.PropertyAddressLine1 = t.PropertyAddressLine1;
                        details.PropertyAddressLine2 = t.PropertyAddressLine2;
                        details.PropertyPhoneNumber = t.PropertyPhoneNumber;
                        details.PayerName = t.PayerName;
                        details.PayerAddressLine1 = t.PayerAddressLine1;
                        details.PayerAddressLine2 = t.PayerAddressLine2;
                        details.PayerPhoneNumber = t.PayerPhoneNumber;
                        details.RenterName = t.RenterName;
                        details.RenterAddressLine1 = t.RenterAddressLine1;
                        details.RenterAddressLine2 = t.RenterAddressLine2;
                        details.RenterPhoneNumber = t.RenterPhoneNumber;
                        details.PaymentStatus = t.PaymentStatus;
                        details.RentAmount = t.RentAmount;
                        total += t.RentAmount;
                        constructedDetailList.Add(details);
                    }
                    temp.Total = total;
                    if (temp.Details == null)
                        temp.Details = new List<PaymentSummaryReportDetail>();
                    temp.Details.AddRange(constructedDetailList);

                    finalList.Add(temp);
                }
                return finalList;
            }
            return returnList;
        }

        public List<ResidentDataImportSummaryReport> ResidentDataImportSummaryReport(ReportQuery query)
        {
            List<ResidentDataImportSummaryReport> returnList = null;
            using (var entity = new RPOEntities())
            {
                Object[] parameters = new Object[3];
                SqlParameter param1 = new SqlParameter("@StartDate", query.StartDate.ToShortDateString());
                SqlParameter param2 = new SqlParameter("@EndDate", query.EndDate.ToShortDateString());
                SqlParameter param3 = new SqlParameter("@PropertyIdList", string.Join(",", query.PropertyIdList));
                parameters[0] = param1;
                parameters[1] = param2;
                parameters[2] = param3;
                returnList = entity.Database.SqlQuery<ResidentDataImportSummaryReport>("usp_Report_ResidentDataImportSummaryReport @StartDate, @EndDate, @PropertyIdList", parameters).ToList();
            }

            //convert the dates to the specified time zone
            returnList.ForEach(r => r.ImportDatetime = CorrectTheDate(r.ImportDatetime, r.TimeZone));
            returnList.ForEach(r => r.LastUpdate = CorrectTheDate(r.LastUpdate, r.TimeZone));

            return returnList;
        }

        public List<RentersPastDueReport> RentersPastDueReport(ReportQuery query)
        {
            List<RentersPastDueReport> returnList = null;
            using (var entity = new RPOEntities())
            {
                Object[] parameters = new Object[1];
                SqlParameter param1 = new SqlParameter("@PropertyIdList", string.Join(",", query.PropertyIdList));
                parameters[0] = param1;
                returnList = entity.Database.SqlQuery<RentersPastDueReport>("usp_Report_RentersPastDueReport @PropertyIdList", parameters).ToList();
            }

            returnList.ForEach(r => r.LastDatePaid = CorrectTheDate(r.LastDatePaid, r.TimeZone));
            return returnList;
        }

        public List<TransactionSummaryReport> TransactionSummaryReport(ReportQuery query)
        {
            List<TransactionSummaryReport> returnList = null;
            using (var entity = new RPOEntities())
            {
                Object[] parameters = new Object[3];
                SqlParameter param1 = new SqlParameter("@StartDate", query.StartDate.ToShortDateString());
                SqlParameter param2 = new SqlParameter("@EndDate", query.EndDate.ToShortDateString());
                SqlParameter param3 = new SqlParameter("@PropertyIdList", string.Join(",", query.PropertyIdList));
                parameters[0] = param1;
                parameters[1] = param2;
                parameters[2] = param3;
                returnList = entity.Database.SqlQuery<TransactionSummaryReport>("usp_Report_TransactionSummaryReport @StartDate, @EndDate, @PropertyIdList", parameters).ToList();
            }
            return returnList;
        }

        private DateTime CorrectTheDate(DateTime dateTime, String timeZone)
        {
            if (!String.IsNullOrEmpty(timeZone))
                return Helpers.DateTimeHelper.setDateTimeZone(dateTime, timeZone);
            else
                return Helpers.DateTimeHelper.setDateTimeZone(dateTime, "Eastern Standard Time");
        }
    }
}
