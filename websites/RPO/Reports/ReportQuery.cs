﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports
{
    /// <summary>
    /// This is intended to be a general use query object for all the reports.  Add whatever parameters you need as
    /// a publically accessible property.
    /// </summary>
    public class ReportQuery
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string[] PropertyIdList { get; set; }
        public int PropertyStaffId { get; set; }
        public int TransactionId { get; set; }
        public bool FlattenData { get; set; }

        //Salcedo - 4/19/2014 - added PageNumber to the class
        public int PageNumber { get; set; }
    }
}
