﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports
{
    public class ResidentDataImportSummaryReport
    {
        public int ResidentImportSummaryId { get; set; }
	    public DateTime ImportDatetime { get; set; }
	    public int PropertyId { get; set; }
	    public string PropertyName { get; set; }
	    public int SuccessfulRecordCount { get; set; }
	    public int FailedRecordCount { get; set; }
        public DateTime LastUpdate { get; set; }
        public string TimeZone { get; set; }
    }
}
