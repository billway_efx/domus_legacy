﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports
{
    public class AutoPaymentReport
    {
        public int AutoPaymentId { get; set; }
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public int RenterId { get; set; }
        public string RenterName { get; set; }
        public int PayerId { get; set; }
        public string PayerName { get; set; }
        public string PaymentMethod { get; set; }
        public decimal RentAmount { get; set; }
        public decimal CharityAmount { get; set; }
        public decimal OtherAmount1 { get; set; }
        public decimal OtherAmount2 { get; set; }
        public decimal OtherAmount3 { get; set; }
        public decimal OtherAmount4 { get; set; }
        public decimal OtherAmount5 { get; set; }
        public int PaymentDayOfMonth { get; set; }
    }

    //cakel: BUGID00081 - Added additional class to export requested columns.  The web page will still present all columns 
    public class AutoPaymentReportExport
    {
        public int AutoPaymentId { get; set; }
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public int RenterId { get; set; }
        public string RenterName { get; set; }
        public int PayerId { get; set; }
        public string PayerName { get; set; }
        public string PaymentMethod { get; set; }
        public decimal RentAmount { get; set; }
        public decimal ConvenienceFee { get; set; }
        public int PaymentDayOfMonth { get; set; }
    }



}
