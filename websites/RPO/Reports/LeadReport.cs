﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports
{
    public class LeadReport
    {
        public int ContactUsRequestId { get; set; }
        public string PropertyName { get; set; }
        public string PropertyPhoneNumber { get; set; }
        public string PropertyEmailAddress { get; set; }
        public string PropertyContact { get; set; }
        public string TenantFullName { get; set; }
        public string TenantEmailAddress { get; set; }
        public string PostalCode { get; set; }
        public string Message { get; set; }
        public string ContactUsTypeName { get; set; }
        public DateTime DateCreated { get; set; }
        public bool Processed { get; set; }
    }
}
