﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports
{
    // should map to the values in the EFX.dbo.PaymentStatus table
    public enum PaymentStatus
    {
        Approved = 1,
        Declined = 2,
        Processing = 3,
        Cleared = 4,
        Pending = 5,
        Returned = 6,
        Refunded = 7,
        Voided = 8,
        Working = 9,
        Processed = 10,
        PartiallyRefunded = 11
    }
}
