﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPO.Reports.Formatters;

namespace RPO.Reports
{
    public class DetailedDepositReport
    {
        public string GroupBy { get; set; }
        public DateTime DepositDate { get; set; }
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public string TransactionType { get; set; }
        public decimal GroupGrandTotal { get; set; }
        public string BatchId { get; set; }
        public DateTime PaymentDate { get; set; }
        public string IntegratedPropertyYesNo { get; set; }
        public string Portal { get; set; }
        public string PayerName { get; set; }
        public string PaymentMethod { get; set; }
        public string CreditCardType { get; set; }
        public decimal PaymentAmount { get; set; }
        public int TransactionId { get; set; }
        public string InternalTransactionId { get; set; }
        public string DatePaidMmYyyy { get; set; }
        public List<DetailedDepositReportDetail> Details { get; set; }
        public int Count { get; set; }
        public int RenterId { get; set; }
        public int PaymentStatusId { get; set; }

        public void copyValues(DetailedDepositReport copy)
        {
            this.GroupBy = copy.GroupBy;
            this.DepositDate = copy.DepositDate;
            this.PropertyId = copy.PropertyId;
            this.PropertyName = copy.PropertyName;
            this.TransactionType = copy.TransactionType;
            this.GroupGrandTotal = copy.GroupGrandTotal;
            this.Details = new List<DetailedDepositReportDetail>();
        }
    }

    public class DetailedDepositReportDetail
    {
        public string BatchId { get; set; }
        public DateTime PaymentDate { get; set; }
        public string IntegratedPropertyYesNo { get; set; }
        public string Portal { get; set; }
        public string PayerName { get; set; }

        private string _creditCardType = string.Empty;
        public string CreditCardType 
        {
            get
            {
                if (IsRefund)
                    _creditCardType = string.Format("{0} - Refund", _creditCardType);
                else if (IsReturn)
                    _creditCardType = string.Format("{0} - Return", _creditCardType);

                return _creditCardType;
            }
            set { _creditCardType = value; }
        }

        public decimal PaymentAmount { get; set; }
        public int TransactionId { get; set; }
        public string InternalTransactionId { get; set; }
        public string DatePaidMmYyyy { get; set; }
        public int RenterId { get; set; }
        public int PaymentStatusId { get; set; }
        public bool IsRefund { get { return (PaymentStatus)PaymentStatusId == PaymentStatus.Refunded; } }
        public bool IsReturn { get { return (PaymentStatus)PaymentStatusId == PaymentStatus.Returned; } }
    }
}
