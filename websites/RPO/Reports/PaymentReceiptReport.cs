﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports
{
    public class PaymentReceiptReport
    {
        public int PaymentId { get; set; }
	    public string InternalTransactionId { get; set; }
	    public string RenterName { get; set; }
	    public string RenterUnit { get; set; }
	    public string RenterAddressLine1 { get; set; }
	    public string RenterAddressLine2 { get; set; }
	    public string RenterPhoneNumber { get; set; }
	    public string RenterPrimaryEmailAddress { get; set; }
	    public string PropertyName { get; set; }
	    public string PmsTypeName { get; set; }
	    public string PaymentStatus { get; set; }
	    public string PaymentTypeName { get; set; }
	    public string PayerName { get; set; }
	    public string PayerAddressLine1 { get; set; }
	    public string PayerAddressLine2 { get; set; }
	    public string PayerPhoneNumber { get; set; }
	    public string PayerPrimaryEmailAddress { get; set; }
	    public DateTime TransactionDateTime { get; set; }
        public decimal Amount { get; set; }
    }
}
