﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports
{
    public class TransactionSummaryReport
    {
		private int? _tranCount;
		private decimal? _tranVolume;
		private decimal? _tranAvg;

        public string PaymentType { get; set; }
	    public string PaymentMethod { get; set; }
		public int? TransactionCount
		{
			get
			{
				if (_tranCount.HasValue)
					return _tranCount.Value;
				else
					return 0;
			}
			set
			{
				_tranCount = value;
			}
		}
	    public decimal? TransactionVolume 
		{
			get
			{
				if (_tranVolume.HasValue)
					return _tranVolume.Value;
				else
					return 0;
			}
			set
			{
				_tranVolume = value;
			}
		}
        public decimal? TransactionAverage 
		{
			get
			{
				if (_tranAvg.HasValue)
					return _tranAvg.Value;
				else
					return 0;
			}
			set
			{
				_tranAvg = value;
			}
		}
    }
}
