﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.Reports
{
    public class PaymentSummaryReport
    {
        public string GroupBy { get; set; }
	    public int? PropertyId { get; set; }
	    public string PropertyName { get; set; }
	    public string PropertyAddressLine1 { get; set; }
	    public string PropertyAddressLine2 { get; set; }
	    public string PropertyPhoneNumber { get; set; }
	    public int? RenterId { get; set; }
	    public string RenterName { get; set; }
	    public string RenterAddressLine1 { get; set; }
	    public string RenterAddressLine2 { get; set; }
	    public string RenterPhoneNumber { get; set; }
	    public int? PayerId { get; set; }
	    public string PayerName { get; set; }
	    public string PayerAddressLine1 { get; set; }
	    public string PayerAddressLine2 { get; set; }
	    public string PayerPhoneNumber { get; set; }
	    public decimal RentAmount { get; set; }
	    public string DatePaidMmYyyy { get; set; }
	    public DateTime DatePaid { get; set; }
	    public string PaymentStatus { get; set; }
	    public string PaymentMethod { get; set; }
	    public string AccountNumber { get; set; }
	    public string InternalTransactionId { get; set; }
	    public int? PaymentId { get; set; }
	    public DateTime DepositDate { get; set; }
        public int TransactionId { get; set; }
        public decimal Total { get; set; }
        public List<PaymentSummaryReportDetail> Details { get; set; }
        public string TimeZone { get; set; }

        public void copyValues(PaymentSummaryReport copy)
        {
            this.DatePaid = copy.DatePaid;
            this.DepositDate = copy.DepositDate;
            this.PropertyName = copy.PropertyName;
            this.PaymentMethod = copy.PaymentMethod;
            this.TimeZone = copy.TimeZone;
        }
    }

    public class PaymentSummaryReportDetail
    {
        public string PaymentMethod { get; set; }
        public string AccountNumber { get; set; }
        public string InternalTransactionId { get; set; }
        public string PropertyAddressLine1 { get; set; }
        public string PropertyAddressLine2 { get; set; }
        public string PropertyPhoneNumber { get; set; }
        public string PayerName { get; set; }
        public string PayerAddressLine1 { get; set; }
        public string PayerAddressLine2 { get; set; }
        public string PayerPhoneNumber { get; set; }
        public string RenterName { get; set; }
        public string RenterAddressLine1 { get; set; }
        public string RenterAddressLine2 { get; set; }
        public string RenterPhoneNumber { get; set; }
        public string PaymentStatus { get; set; }
        public decimal RentAmount { get; set; }
    }
}
