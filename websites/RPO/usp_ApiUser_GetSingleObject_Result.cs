
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RPO
{

using System;
    
public partial class usp_ApiUser_GetSingleObject_Result
{

    public int ApiUserId { get; set; }

    public string Username { get; set; }

    public byte[] PasswordHash { get; set; }

    public byte[] Salt { get; set; }

    public Nullable<int> PartnerCompanyId { get; set; }

    public bool IsActive { get; set; }

}

}
