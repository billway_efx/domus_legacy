
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RPO
{

using System.Runtime.Serialization;
using System;
    using System.Collections.Generic;
    

[DataContract(IsReference = true)]
public partial class BankAccountType
{

    public BankAccountType()
    {

        this.PayerAches = new HashSet<PayerAch>();

        this.Transactions = new HashSet<Transaction>();

    }


    [DataMember]
	public int BankAccountTypeId { get; set; }

    [DataMember]
	public string BankAccountTypeName { get; set; }



    public virtual ICollection<PayerAch> PayerAches { get; set; }

    public virtual ICollection<Transaction> Transactions { get; set; }

}

}
