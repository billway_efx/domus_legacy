
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RPO
{

using System.Runtime.Serialization;
using System;
    using System.Collections.Generic;
    

[DataContract(IsReference = true)]
public partial class Renter
{

    public Renter()
    {

        this.ApplicantRenters = new HashSet<ApplicantRenter>();

        this.AutoPayments = new HashSet<AutoPayment>();

        this.LeaseFees = new HashSet<LeaseFee>();

        this.MaintenanceRequests = new HashSet<MaintenanceRequest>();

        this.Payments = new HashSet<Payment>();

        this.PaymentQueues = new HashSet<PaymentQueue>();

        this.RenterNotes = new HashSet<RenterNote>();

        this.Roommates = new HashSet<Roommate>();

        this.Properties = new HashSet<Property>();

        this.Addresses = new HashSet<Address>();

        this.LeaseRenters = new HashSet<LeaseRenter>();

        this.PayNearMeOrders = new HashSet<PayNearMeOrder>();

    }


    [DataMember]
	public int RenterId { get; set; }

    [DataMember]
	public Nullable<int> PayerId { get; set; }

    [DataMember]
	public string Prefix { get; set; }

    [DataMember]
	public string FirstName { get; set; }

    [DataMember]
	public string MiddleName { get; set; }

    [DataMember]
	public string LastName { get; set; }

    [DataMember]
	public string Suffix { get; set; }

    [DataMember]
	public string MainPhoneNumber { get; set; }

    [DataMember]
	public string MobilePhoneNumber { get; set; }

    [DataMember]
	public string FaxNumber { get; set; }

    [DataMember]
	public string PrimaryEmailAddress { get; set; }

    [DataMember]
	public string AlternateEmailAddress1 { get; set; }

    [DataMember]
	public string AlternateEmailAddress2 { get; set; }

    [DataMember]
	public byte[] Photo { get; set; }

    [DataMember]
	public string StreetAddress { get; set; }

    [DataMember]
	public string Unit { get; set; }

    [DataMember]
	public string City { get; set; }

    [DataMember]
	public Nullable<int> StateProvinceId { get; set; }

    [DataMember]
	public string PostalCode { get; set; }

    [DataMember]
	public Nullable<bool> EmailNotifications { get; set; }

    [DataMember]
	public Nullable<bool> TextNotifications { get; set; }

    [DataMember]
	public Nullable<bool> IsParticipating { get; set; }

    [DataMember]
	public byte[] PasswordHash { get; set; }

    [DataMember]
	public byte[] Salt { get; set; }

    [DataMember]
	public string AccountCodePin { get; set; }

    [DataMember]
	public bool IsCreditCardTestRenter { get; set; }

    [DataMember]
	public bool IsCreditCardApproved { get; set; }

    [DataMember]
	public string SmsPaymentPhoneNumber { get; set; }

    [DataMember]
	public Nullable<int> SmsPaymentTypeId { get; set; }

    [DataMember]
	public Nullable<int> SmsPaymentCarrierId { get; set; }

    [DataMember]
	public string PmsId { get; set; }

    [DataMember]
	public Nullable<int> PmsTypeId { get; set; }

    [DataMember]
	public bool IsActive { get; set; }

    [DataMember]
	public Nullable<System.DateTime> ImportDateTime { get; set; }

    [DataMember]
	public Nullable<System.DateTime> ExportDateTime { get; set; }

    [DataMember]
	public Nullable<int> SmsReminderDayOfMonth { get; set; }

    [DataMember]
	public string PmsPropertyId { get; set; }

    [DataMember]
	public decimal PublicRenterId { get; set; }

    [DataMember]
	public bool UserModified { get; set; }

    [DataMember]
	public int AcceptedPaymentTypeId { get; set; }

    [DataMember]
	public string RentReporterId { get; set; }

    [DataMember]
	public Nullable<int> RenterPropertyId { get; set; }

    [DataMember]
	public bool HasRegistered { get; set; }

    [DataMember]
	public string PNMCustomerIdentifier { get; set; }



    public virtual AcceptedPaymentType AcceptedPaymentType { get; set; }

    public virtual ICollection<ApplicantRenter> ApplicantRenters { get; set; }

    public virtual ICollection<AutoPayment> AutoPayments { get; set; }

    public virtual Carrier Carrier { get; set; }

    public virtual ICollection<LeaseFee> LeaseFees { get; set; }

    public virtual ICollection<MaintenanceRequest> MaintenanceRequests { get; set; }

    public virtual Payer Payer { get; set; }

    public virtual ICollection<Payment> Payments { get; set; }

    public virtual ICollection<PaymentQueue> PaymentQueues { get; set; }

    public virtual PaymentType PaymentType { get; set; }

    public virtual PmsType PmsType { get; set; }

    public virtual StateProvince StateProvince { get; set; }

    public virtual ICollection<RenterNote> RenterNotes { get; set; }

    public virtual ICollection<Roommate> Roommates { get; set; }

    public virtual ICollection<Property> Properties { get; set; }

    public virtual ICollection<Address> Addresses { get; set; }

    public virtual Property Property { get; set; }

    public virtual ICollection<LeaseRenter> LeaseRenters { get; set; }

    public virtual ICollection<PayNearMeOrder> PayNearMeOrders { get; set; }

}

}
