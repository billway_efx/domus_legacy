﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RPO
{
    public partial class PropertyListItem
    {
        [DataMember]
        public int PropertyId { get; set; }
        [DataMember]
        public string PropertyName { get; set; }
        //CMallory - Task 00521 - Added to Display 2nd Property Name Field
        [DataMember]
        public string PropertyName2 { get; set; }
        [DataMember]
        public Nullable<int> NumberOfUnits { get; set; }
        [DataMember]
        public string StreetAddress { get; set; }
        [DataMember]
        public string StreetAddress2 { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public Nullable<int> StateProvinceId { get; set; }
        [DataMember]
        public string PostalCode { get; set; }
        [DataMember]
        public string OfficeEmailAddress { get; set; }
        [DataMember]
        public string MainPhoneNumber { get; set; }
        [DataMember]
        public Nullable<bool> IsDeleted { get; set; }
        [DataMember]
        public Nullable<int> ProgramId { get; set; }
        [DataMember]
        public Nullable<int> PmsTypeId { get; set; }
        [DataMember]
        public string PmsId { get; set; }
        //secondary data
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string PmsTypeName { get; set; }
        [DataMember]
        public string MainContactName { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public System.DateTime DateAdded { get; set; }
        [DataMember]
        public int CompanyId { get; set; }
        [DataMember]
        public string TimeZone { get; set; }
    }
}
