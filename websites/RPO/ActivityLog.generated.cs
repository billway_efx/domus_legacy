
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RPO
{

using System.Runtime.Serialization;
using System;
    using System.Collections.Generic;
    

[DataContract(IsReference = true)]
public partial class ActivityLog
{

    [DataMember]
	public long LogId { get; set; }

    [DataMember]
	public System.DateTime DateTime { get; set; }

    [DataMember]
	public string UserName { get; set; }

    [DataMember]
	public string Location { get; set; }

    [DataMember]
	public string Description { get; set; }

}

}
