
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RPO
{

using System;
    
public partial class usp_Transaction_GetSingleObject_Result
{

    public int TransactionId { get; set; }

    public string InternalTransactionId { get; set; }

    public string ExternalTransactionId { get; set; }

    public int PaymentTypeId { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string StreetAddress { get; set; }

    public string StreetAddress2 { get; set; }

    public string City { get; set; }

    public int StateProvinceId { get; set; }

    public string PostalCode { get; set; }

    public string PhoneNumber { get; set; }

    public string EmailAddress { get; set; }

    public string Memo { get; set; }

    public decimal PaymentAmount { get; set; }

    public Nullable<decimal> ConvenienceFeeAmount { get; set; }

    public string CreditCardHolderName { get; set; }

    public string CreditCardAccountNumber { get; set; }

    public Nullable<int> CreditCardExpirationMonth { get; set; }

    public Nullable<int> CreditCardExpirationYear { get; set; }

    public string BankAccountNumber { get; set; }

    public string BankRoutingNumber { get; set; }

    public Nullable<int> BankAccountTypeId { get; set; }

    public Nullable<int> PaymentId { get; set; }

    public int PaymentStatusId { get; set; }

    public Nullable<int> ResponseResult { get; set; }

    public string ResponseMessage { get; set; }

    public System.DateTime TransactionDate { get; set; }

    public int PropertyId { get; set; }

}

}
