
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RPO
{

using System.Runtime.Serialization;
using System;
    using System.Collections.Generic;
    

[DataContract(IsReference = true)]
public partial class Lead
{

    [DataMember]
	public int LeadId { get; set; }

    [DataMember]
	public string FirstName { get; set; }

    [DataMember]
	public string LastName { get; set; }

    [DataMember]
	public string EmailAddress { get; set; }

    [DataMember]
	public string Description { get; set; }

    [DataMember]
	public System.DateTime DateSubmitted { get; set; }

    [DataMember]
	public string PhoneNumber { get; set; }

    [DataMember]
	public string PropertyName { get; set; }

    [DataMember]
	public int LeadTypeId { get; set; }



    public virtual LeadType LeadType { get; set; }

}

}
