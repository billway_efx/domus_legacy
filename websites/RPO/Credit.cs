﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO
{
    public partial class Credit
    {
        public static void AddNewCredit(Credit creditToSave)
        {
            using (var entity = new RPOEntities())
            {
                entity.Credits.Add(creditToSave);
                entity.SaveChanges();
            }
        }
    }
}
