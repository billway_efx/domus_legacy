﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RPO.PayNearMe
{
	public class AuthorizationHandler
	{
		public XmlDocument GetAuthorizationResponse(AuthorizationCallBackContainer container, out int renterId, out bool createPayment)
		{
			string order_identifier = container.pnm_order_identifier;
			string accept_payment = "";
			string receipt_text = "";
			string memo_text = DateTime.UtcNow.ToString();
			string site_payment_identifier = "";
			createPayment = true;
			renterId = 0;

			if (container != null || !string.IsNullOrEmpty(container.pnm_order_identifier))
			{
				//use pnm order id to get the PayNearMeOrder record
				PayNearMeOrder pnmOrder = PayNearMeOrder.GetOrderByPNMOrderId(container.pnm_order_identifier);

				if (pnmOrder != null)
				{
					//use the PropertyId to verify the property is accepting PNM
					Property currentProperty = Property.GetByPropertyId(pnmOrder.PropertyId);
					renterId = pnmOrder.RenterId;

					//use the RenterId to verify the AcceptedPaymentType is either 1 or 3
					Renter currentRenter = Renter.GetByRenterId(pnmOrder.RenterId);
					bool renterCanUseCash = VerifyRenterCanUseCash(currentRenter);

					//fill in the string values below
					accept_payment = (renterCanUseCash && currentProperty.PayNearMeEnabled) ? "yes" : "no";
					receipt_text = (renterCanUseCash && currentProperty.PayNearMeEnabled) ? "Approved" : "Payment Declined";
					site_payment_identifier = pnmOrder.PayNearMeOrderId.ToString();
				}
				else
				{
					accept_payment = "no";
					receipt_text = "Invalid Request";
				}
			}
			else
			{
				accept_payment = "no";
				receipt_text = "Invalid Request";
			}

			if (accept_payment == "no")
			{
				createPayment = false;
			}

			string source = "<t:payment_authorization_response xsi:schemaLocation='http://www.paynearme.com/api/pnm_xmlschema_v2_0 pnm_xmlschema_v2_0.xsd' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' version='2.0' xmlns:t='http://www.paynearme.com/api/pnm_xmlschema_v2_0'></t:payment_authorization_response>";
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(source);

			XmlElement paymentAuthorizationElement = doc.CreateElement("t:authorization", doc.DocumentElement.NamespaceURI);
			doc.DocumentElement.AppendChild(paymentAuthorizationElement);

			XmlElement orderIdentifierElement = doc.CreateElement("t:pnm_order_identifier", doc.DocumentElement.NamespaceURI);
			orderIdentifierElement.AppendChild(doc.CreateTextNode(order_identifier));
			paymentAuthorizationElement.AppendChild(orderIdentifierElement);

			XmlElement acceptPaymentElement = doc.CreateElement("t:accept_payment", doc.DocumentElement.NamespaceURI);
			acceptPaymentElement.AppendChild(doc.CreateTextNode(accept_payment));
			paymentAuthorizationElement.AppendChild(acceptPaymentElement);

			XmlElement receiptElement = doc.CreateElement("t:receipt", doc.DocumentElement.NamespaceURI);
			receiptElement.AppendChild(doc.CreateTextNode(receipt_text));
			paymentAuthorizationElement.AppendChild(receiptElement);

			XmlElement memoElement = doc.CreateElement("t:memo", doc.DocumentElement.NamespaceURI);
			memoElement.AppendChild(doc.CreateTextNode(memo_text));
			paymentAuthorizationElement.AppendChild(memoElement);

			XmlElement siteIdentifierElement = doc.CreateElement("t:site_payment_identifier", doc.DocumentElement.NamespaceURI);
			siteIdentifierElement.AppendChild(doc.CreateTextNode(site_payment_identifier));
			paymentAuthorizationElement.AppendChild(siteIdentifierElement);

			return doc;
		}

		private bool VerifyRenterCanUseCash(Renter currentRenter)
		{
			if (currentRenter.AcceptedPaymentType.AcceptedPaymentTypeName == "Any" || currentRenter.AcceptedPaymentType.AcceptedPaymentTypeName == "Cash Equivalent")
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
