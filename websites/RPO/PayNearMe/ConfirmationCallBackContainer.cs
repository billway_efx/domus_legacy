﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO.PayNearMe
{
	public class ConfirmationCallBackContainer
	{
		public string pnm_order_identifier { get; set; }
		public string pnm_payment_identifier { get; set; }
		public string site_payment_identifier { get; set; }
		public string site_order_identifier { get; set; }
		public string site_customer_identifier { get; set; }
		public string site_order_annotation { get; set; }
		public string payment_amount { get; set; }
		public string payment_currency { get; set; }
		public string net_payment_amount { get; set; }
		public string net_payment_currency { get; set; }
		public string pnm_withheld_amount { get; set; }
		public string pnm_withheld_currency { get; set; }
		public string due_to_site_amount { get; set; }
		public string due_to_site_currency { get; set; }
		public string site_processing_fee { get; set; }
		public string site_processing_currency { get; set; }
		public string pnm_processing_fee { get; set; }
		public string pnm_processing_currency { get; set; }
		public string status { get; set; }
	}
}
