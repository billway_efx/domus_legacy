
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RPO
{

using System;
    
public partial class usp_Announcement_GetSingleObject_Result
{

    public int AnnouncementId { get; set; }

    public int AnnouncementTypeId { get; set; }

    public Nullable<System.DateTime> AnnouncementDate { get; set; }

    public string AnnouncementText { get; set; }

    public bool IsActive { get; set; }

    public string AnnouncementTitle { get; set; }

    public string AnnouncementUrl { get; set; }

    public string PostedBy { get; set; }

}

}
