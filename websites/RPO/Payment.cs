﻿
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RPO
{
    public partial class Payment
    {
        [DataMember]
        public string PaymentStatus { get; set; }
        [DataMember]
        public List<Transaction> PaymentTransactions { get; set; }
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string PropertyName { get; set; }
        [DataMember]
        public string PayerName { get; set; }
        [DataMember]
        public int? PropertyId { get; set; }
        [DataMember]
        public int? CompanyId { get; set; }
        [DataMember]
        public string PaymentChannelName { get; set; }
		[DataMember]
		public string PayerFirstName { get; set; }
		[DataMember]
		public string PayerLastName { get; set; }
		[DataMember]
		public string Unit { get; set; }
    }
}
