﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RPO
{
	public class CalendarEventViewModel
	{
		[DataMember]
		public List<object> events { get; set; }
		[DataMember]
		public string color { get { return "yellow"; } }
		[DataMember]
		public string textColor { get { return "black"; } }
	}
}
