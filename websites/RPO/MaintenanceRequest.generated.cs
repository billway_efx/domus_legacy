
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RPO
{

using System.Runtime.Serialization;
using System;
    using System.Collections.Generic;
    

[DataContract(IsReference = true)]
public partial class MaintenanceRequest
{

    [DataMember]
	public int MaintenanceRequestId { get; set; }

    [DataMember]
	public int MaintenanceRequestTypeId { get; set; }

    [DataMember]
	public int MaintenanceRequestPriorityId { get; set; }

    [DataMember]
	public int PropertyId { get; set; }

    [DataMember]
	public int RenterId { get; set; }

    [DataMember]
	public System.DateTime MaintenanceRequestDate { get; set; }

    [DataMember]
	public string MaintenanceRequestDescription { get; set; }

    [DataMember]
	public bool IsActive { get; set; }



    public virtual MaintenanceRequestPriority MaintenanceRequestPriority { get; set; }

    public virtual MaintenanceRequestType MaintenanceRequestType { get; set; }

    public virtual Property Property { get; set; }

    public virtual Renter Renter { get; set; }

}

}
