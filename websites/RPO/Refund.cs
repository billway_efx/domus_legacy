﻿namespace RPO
{
    public partial class Refund
    {
        public int PaymentId { get; set; }
        public decimal Amount { get; set; }
    }
}