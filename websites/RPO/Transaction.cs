﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RPO
{
    public partial class Transaction
    {
        [DataMember]
        public string TransactionStatus { get; set; }
        /// <summary>
        /// Maps RPO Payment Statuses to ACH Statuses
        /// Description = RPO Enum Status ID
        /// See: EfxFramework.PublicApi.Payment.PaymentStatus for enum
        /// </summary>
        public enum AchStatus
        {
            [Description("5")]
            Pending = 1,
            [Description("9")]
            Working = 2,
            [Description("3")]
            InProcess = 3,
            [Description("4")]
            Cleared = 4,
            [Description("2")]
            Rejected = 5,
            [Description("6")]
            Returned = 6,
            [Description("6")]
            ChargedBack = 7,
            [Description("8")]
            Cancelled = 8
        }
        public static string GetDescription(Enum en)
        {
            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return en.ToString();
        }
    }

}
