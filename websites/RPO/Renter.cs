﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;

namespace RPO
{

    public partial class Renter
    {
        [DataMember]
        public List<Payment> PaymentHistory { get; set; }
        [DataMember]
        public string PropertyName { get; set; }
        [DataMember]
        public string FullName {get { return FirstName + " " + LastName; }}
        [DataMember]
        public decimal RentAmount { get; set; }
		[DataMember]
		public string CompanyName { get; set; }

		public static Renter GetByRenterId(int renterId)
		{
			Renter returnRenter = null;
			using (var entity = new RPOEntities())
			{
				returnRenter = entity.Renters.Include("AcceptedPaymentType").Where(r => r.RenterId == renterId).FirstOrDefault();
			}
			return returnRenter;
		}
    }
}
