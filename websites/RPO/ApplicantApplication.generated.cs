
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RPO
{

using System.Runtime.Serialization;
using System;
    using System.Collections.Generic;
    

[DataContract(IsReference = true)]
public partial class ApplicantApplication
{

    [DataMember]
	public int ApplicantApplicationId { get; set; }

    [DataMember]
	public int ApplicantId { get; set; }

    [DataMember]
	public byte[] CompletedApplication { get; set; }

    [DataMember]
	public int PropertyId { get; set; }

    [DataMember]
	public int ApplicationStatusId { get; set; }

    [DataMember]
	public bool FeesPaid { get; set; }

    [DataMember]
	public int ApplicationSubmissionTypeId { get; set; }

    [DataMember]
	public System.DateTime ApplicationDate { get; set; }

    [DataMember]
	public System.DateTime ApplicationStatusDate { get; set; }

    [DataMember]
	public string StatusUpdatedBy { get; set; }



    public virtual Applicant Applicant { get; set; }

    public virtual ApplicationStatu ApplicationStatu { get; set; }

    public virtual ApplicationSubmissionType ApplicationSubmissionType { get; set; }

    public virtual Property Property { get; set; }

}

}
