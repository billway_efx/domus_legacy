
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RPO
{

using System.Runtime.Serialization;
using System;
    using System.Collections.Generic;
    

[DataContract(IsReference = true)]
public partial class PropertyManagerProspect
{

    [DataMember]
	public int PropertyManagerProspectId { get; set; }

    [DataMember]
	public string ContactFullName { get; set; }

    [DataMember]
	public string ContactFirstName { get; set; }

    [DataMember]
	public string ContactLastname { get; set; }

    [DataMember]
	public string ContactEmailAddress { get; set; }

    [DataMember]
	public string PropertyPhoneNumber { get; set; }

    [DataMember]
	public string PropertyEmailAddress { get; set; }

    [DataMember]
	public string PropertyName { get; set; }

    [DataMember]
	public Nullable<int> NumberOfUnits { get; set; }

    [DataMember]
	public string PropertyManagementCompany { get; set; }

    [DataMember]
	public string EfxCode { get; set; }

    [DataMember]
	public Nullable<bool> WantsToSignUp { get; set; }

    [DataMember]
	public Nullable<bool> WantsToBeContacted { get; set; }

    [DataMember]
	public Nullable<bool> ContactWhenPropertyParticipating { get; set; }

    [DataMember]
	public Nullable<bool> SendListOfProperties { get; set; }

    [DataMember]
	public int ProspectSourceId { get; set; }

    [DataMember]
	public System.DateTime DateCreated { get; set; }

    [DataMember]
	public bool Processed { get; set; }

}

}
