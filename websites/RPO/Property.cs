﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RPO
{
    public partial class Property
    {
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string PmsTypeName { get; set; }
        [DataMember]
        public string MainContactName { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public int NumberOfLeasingAgents { get; set; }
        [DataMember]
        public bool RentersPastDue { get; set; }
        [DataMember]
        public string BillingTypes { get; set; }

		public static Property GetByPropertyId(int propertyId)
		{
			Property returnProperty = null;
			using (var entity = new RPOEntities())
			{
				returnProperty = entity.Properties.Where(p => p.PropertyId == propertyId).FirstOrDefault();
			}
			return returnProperty;
		}
        public string DisplayAddress
        {
            get
            {
                var Prov = "";
                if (StateProvince != null)
                    Prov = StateProvince.Abbreviation;

                return string.Format("{0} {1} {2} {3}", StreetAddress, City, Prov, PostalCode);
            }
        }

        public bool AllowPartialPaymentsResidentialPortal { get; set; }
    }
}
