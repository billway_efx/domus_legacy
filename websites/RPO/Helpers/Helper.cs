﻿using System;
using System.Linq;
using System.Web;

namespace RPO.Helpers
{
    public class Helper
    {
        public static bool IsRpoAdmin 
        {
            get
            {
                var isRpoAdmin = false;
                if (HttpContext.Current.Session != null && HttpContext.Current.Session["IsRPOAdmin"] != null)
                    bool.TryParse(HttpContext.Current.Session["IsRPOAdmin"].ToString(), out isRpoAdmin);
                return isRpoAdmin;
            }
        }

        public static bool IsCsrUser 
        {
            get
            {
                var isCsrUser = false;
                if (HttpContext.Current.Session != null && HttpContext.Current.Session["IsRPOCSR"] != null)
                    bool.TryParse(HttpContext.Current.Session["IsRPOCSR"].ToString(), out isCsrUser);
                return isCsrUser;
            }
        }
        public static int CurrentUserId => HttpContext.Current.User.Identity.IsAuthenticated ? Convert.ToInt32(HttpContext.Current.User.Identity.Name) : 0;

        public static bool IsIntegrated(string propertyPmsId)
        {
            var propPms = propertyPmsId ?? "";
            return propPms != "";
        }

        public static string GetCurrentUserName(bool isRpoAdmin, bool isRpoCsr)
        {
            if (HttpContext.Current.User == null) return "";

            if(!HttpContext.Current.User.Identity.IsAuthenticated) return "";

            if (HttpContext.Current.Session["CurrentUserName"] != null)
                return HttpContext.Current.Session["CurrentUserName"] == null
                    ? ""
                    : HttpContext.Current.Session["CurrentUserName"].ToString();

            using (var entity = new RPOEntities())
            {
                if (isRpoAdmin || isRpoCsr)
                {
                    var rpoAdmin =
                        entity.EfxAdministrators.FirstOrDefault(
                            admin => admin.EfxAdministratorId == CurrentUserId);
                    if (rpoAdmin != null)
                        HttpContext.Current.Session["CurrentUserName"] = rpoAdmin.FirstName + " " +
                                                                         rpoAdmin.LastName + " (" + rpoAdmin.PrimaryEmailAddress + ")";

                    HttpContext.Current.Session["CurrentUserNameEmail"] = rpoAdmin.PrimaryEmailAddress;
                }
                else
                {
                    var currentUser =
                        entity.PropertyStaffs.FirstOrDefault(
                            staff => staff.PropertyStaffId == CurrentUserId);
                    if (currentUser != null)
                    {
                        HttpContext.Current.Session["CurrentUserName"] = currentUser.FirstName + " " +
                                                                         currentUser.LastName + " (" + currentUser.PrimaryEmailAddress + ")";
                    }
                }
            }

            return HttpContext.Current.Session["CurrentUserName"] == null ? "" : HttpContext.Current.Session["CurrentUserName"].ToString();
        }
    }
}
