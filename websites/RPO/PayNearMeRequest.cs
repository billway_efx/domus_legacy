﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RPO
{
    public partial class PayNearMeRequest
    {
        public string CustomerIdentifier { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerName { get; set; }
        public string CustomerStreet { get; set; }
        public string CustomerCity { get; set; }
        public string CustomerState { get; set; }
        public string CustomerPostalCode { get; set; }
        public string OrderDescription { get; set; }
        public string CreatorIdentifier { get; set; }
        public decimal Amount { get; set; }
        public decimal MinimumPaymentAmount { get; set; }
        public string MinimumPaymentCurrency { get; set; }
        public string Currency { get; set; }
        public PNMOrderType OrderType { get; set; }
        public string OrderIdentifier { get; set; }
        public int OrderDuration { get; set; }
        public string SiteIdentifier { get; set; }
        public string Version { get; set; }
        public int TimeStamp { get; set; }
        public string SecretKey { get; set; }
        public int RenterId { get; set; }
        public int PropertyId { get; set; }
        public int CompanyId { get; set; }
        public int? EfxAdministratorId { get; set; }
        public int? PropertyStaffId { get; set; }

        public enum PNMOrderType 
        {
            [Description("exact")]
            Exact,
            [Description("up-to")]
            UpTo,
            [Description("any")]
            Any
        }
        public static string GetDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }
    }
}
