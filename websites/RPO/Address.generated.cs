
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RPO
{

using System.Runtime.Serialization;
using System;
    using System.Collections.Generic;
    

[DataContract(IsReference = true)]
public partial class Address
{

    public Address()
    {

        this.Renters = new HashSet<Renter>();

    }


    [DataMember]
	public int AddressId { get; set; }

    [DataMember]
	public int AddressTypeId { get; set; }

    [DataMember]
	public string CareOf { get; set; }

    [DataMember]
	public string AddressLine1 { get; set; }

    [DataMember]
	public string AddressLine2 { get; set; }

    [DataMember]
	public string AddressLine3 { get; set; }

    [DataMember]
	public string City { get; set; }

    [DataMember]
	public int StateProvinceId { get; set; }

    [DataMember]
	public string PostalCode { get; set; }

    [DataMember]
	public string PrimaryPhoneNumber { get; set; }

    [DataMember]
	public string PrimaryPhoneNumberExtension { get; set; }

    [DataMember]
	public string AlternatePhoneNumber { get; set; }

    [DataMember]
	public string AlternatePhoneNumberExtension { get; set; }

    [DataMember]
	public string MobilePhoneNumber { get; set; }

    [DataMember]
	public string FaxNumber { get; set; }

    [DataMember]
	public string PrimaryEmailAddress { get; set; }

    [DataMember]
	public string AlternateEmailAddress1 { get; set; }

    [DataMember]
	public string AlternateEmailAddress2 { get; set; }



    public virtual AddressType AddressType { get; set; }

    public virtual ICollection<Renter> Renters { get; set; }

}

}
