
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RPO
{

using System;
    
public partial class usp_Report_ListRentersByPropertyId_Result
{

    public int RenterId { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string RenterName { get; set; }

}

}
