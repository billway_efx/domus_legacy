﻿using RPO.PayNearMe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPO
{
	public partial class PayNearMeOrder
	{
		public static PayNearMeOrder GetOrderByPNMOrderId(string pnm_order_identifier)
		{
			PayNearMeOrder returnOrder = null;
			long orderId = 0;
			if(Int64.TryParse(pnm_order_identifier, out orderId))
			{
				using (var entity = new RPOEntities())
				{
					returnOrder = entity.PayNearMeOrders.Where(o => o.PNMOrderIdentifier == orderId).FirstOrDefault();
				}
			}
			return returnOrder;
		}

		public static PayNearMeOrder UpdatePNMOrderForConfirmationCallBack(ConfirmationCallBackContainer container)
		{
			PayNearMeOrder returnOrder = null;
			long orderId = 0;
			long paymentId = 0;
			long? pnmPaymentId = 0;
			bool isDuplicate = false;
			if (Int64.TryParse(container.pnm_payment_identifier, out paymentId))
			{
				pnmPaymentId = paymentId;
				using (var entity = new RPOEntities())
				{
					isDuplicate = entity.PayNearMeOrders.Any(o => o.PNMPaymentIdentifier.HasValue && o.PNMPaymentIdentifier == pnmPaymentId);
				}
			}

			if (Int64.TryParse(container.pnm_order_identifier, out orderId) && !isDuplicate)
			{
				using (var entity = new RPOEntities())
				{
					returnOrder = entity.PayNearMeOrders.Where(o => o.PNMOrderIdentifier == orderId).FirstOrDefault();

					//save the confirmed order information if we haven't already updated
                    if (string.IsNullOrEmpty(returnOrder.Status))
                    {
                        long PNMPaymentId = 0;
                        decimal siteProcessingFee = 0;
                        decimal pnmProcessingFee = 0;
                        decimal finalPaymentAmount = 0;
                        decimal netPaymentAmount = 0;
                        decimal withHeldAmount = 0;
                        decimal dueToSiteAmount = 0;
                        returnOrder.PNMPaymentIdentifier = Int64.TryParse(container.pnm_payment_identifier, out PNMPaymentId) ? PNMPaymentId : 0;
                        returnOrder.SitePaymentIdentifier = container.site_payment_identifier;
                        returnOrder.SiteOrderAnnotation = container.site_order_annotation;
                        returnOrder.FinalPaymentAmount = Decimal.TryParse(container.payment_amount, out finalPaymentAmount) ? finalPaymentAmount : 0;
                        returnOrder.NetPaymentAmount = Decimal.TryParse(container.net_payment_amount, out netPaymentAmount) ? netPaymentAmount : 0;
                        returnOrder.PNMWithheldAmount = Decimal.TryParse(container.pnm_withheld_amount, out withHeldAmount) ? withHeldAmount : 0;
                        returnOrder.DueToSiteAmount = Decimal.TryParse(container.due_to_site_amount, out dueToSiteAmount) ? dueToSiteAmount : 0;
                        returnOrder.SiteProcessingFee = Decimal.TryParse(container.site_processing_fee, out siteProcessingFee) ? siteProcessingFee : 0;
                        returnOrder.PNMProcessingFee = Decimal.TryParse(container.pnm_processing_fee, out pnmProcessingFee) ? pnmProcessingFee : 0;
                        returnOrder.Status = container.status;
                        entity.SaveChanges();
                    }
                    else 
                    {
                        // Salcedo - Task # 0000292 - 10/31/2014 
                        // If the previous/current status was "decline", we're going to allow a new "payment" status to override the existing "decline" status record.
                        // This logic was necessary to handle the scenario where a customer attempts a payment, realizes he doesn't have a money, so the store submits
                        // the transaction to us as a "decline" status. Then, the customer goes to the ATM and gets more cash, comes back later and finishes making the payment,
                        // which results in a new confirmation being sent to us as a "payment" status. Without this logic, the final "payment" confirmation was being ignored.
                        if (returnOrder.Status.ToUpper() == "DECLINE")
                        {
                            if (container.status.ToUpper() == "PAYMENT")
                            {
                                long PNMPaymentId = 0;
                                decimal siteProcessingFee = 0;
                                decimal pnmProcessingFee = 0;
                                decimal finalPaymentAmount = 0;
                                decimal netPaymentAmount = 0;
                                decimal withHeldAmount = 0;
                                decimal dueToSiteAmount = 0;
                                returnOrder.PNMPaymentIdentifier = Int64.TryParse(container.pnm_payment_identifier, out PNMPaymentId) ? PNMPaymentId : 0;
                                returnOrder.SitePaymentIdentifier = container.site_payment_identifier;
                                returnOrder.SiteOrderAnnotation = container.site_order_annotation;
                                returnOrder.FinalPaymentAmount = Decimal.TryParse(container.payment_amount, out finalPaymentAmount) ? finalPaymentAmount : 0;
                                returnOrder.NetPaymentAmount = Decimal.TryParse(container.net_payment_amount, out netPaymentAmount) ? netPaymentAmount : 0;
                                returnOrder.PNMWithheldAmount = Decimal.TryParse(container.pnm_withheld_amount, out withHeldAmount) ? withHeldAmount : 0;
                                returnOrder.DueToSiteAmount = Decimal.TryParse(container.due_to_site_amount, out dueToSiteAmount) ? dueToSiteAmount : 0;
                                returnOrder.SiteProcessingFee = Decimal.TryParse(container.site_processing_fee, out siteProcessingFee) ? siteProcessingFee : 0;
                                returnOrder.PNMProcessingFee = Decimal.TryParse(container.pnm_processing_fee, out pnmProcessingFee) ? pnmProcessingFee : 0;
                                returnOrder.Status = container.status;
                                entity.SaveChanges();
                            }
                        }
                        else
                        {
                            if (returnOrder.Status.ToUpper() == "PAYMENT")
                            {
                                //Salcedo - 11/14/2014 - task 302
                                // We don't want to return an order in this case because otherwise the client logic will try and make another payment,
                                // and we don't allow a new payment to be processed/confirmed for an order that already has a payment status (has been previously confirmed)
                                returnOrder = null;
                            }
                        }
                    }

				}
			}
			return returnOrder;
		}
	}
}
