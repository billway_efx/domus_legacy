
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RPO
{

using System.Runtime.Serialization;
using System;
    using System.Collections.Generic;
    

[DataContract(IsReference = true)]
public partial class Company
{

    public Company()
    {

        this.Properties = new HashSet<Property>();

        this.Properties1 = new HashSet<Property>();

        this.PayNearMeOrders = new HashSet<PayNearMeOrder>();

        this.CalendarEvents = new HashSet<CalendarEvent>();

        this.PropertyStaffs = new HashSet<PropertyStaff>();

        this.TestingTables = new HashSet<TestingTable>();

    }


    [DataMember]
	public int CompanyId { get; set; }

    [DataMember]
	public string CompanyName { get; set; }

    [DataMember]
	public string ContactFirstName { get; set; }

    [DataMember]
	public string ContactLastName { get; set; }

    [DataMember]
	public string Address { get; set; }

    [DataMember]
	public string Address2 { get; set; }

    [DataMember]
	public string City { get; set; }

    [DataMember]
	public Nullable<int> StateProvinceId { get; set; }

    [DataMember]
	public string PostalCode { get; set; }

    [DataMember]
	public string Phone { get; set; }

    [DataMember]
	public string Mobile { get; set; }

    [DataMember]
	public string Fax { get; set; }

    [DataMember]
	public string WebSite { get; set; }

    [DataMember]
	public string EmailAddress { get; set; }

    [DataMember]
	public byte[] Logo { get; set; }

    [DataMember]
	public bool IsDeleted { get; set; }

    [DataMember]
	public string SalesFirstName { get; set; }

    [DataMember]
	public string SalesLastName { get; set; }

    [DataMember]
	public string SalesPhone { get; set; }

    [DataMember]
	public string Status { get; set; }

    [DataMember]
	public string PNMSiteId { get; set; }

    [DataMember]
	public string PNMSecretKey { get; set; }



    public virtual StateProvince StateProvince { get; set; }

    public virtual ICollection<Property> Properties { get; set; }

    public virtual ICollection<Property> Properties1 { get; set; }

    public virtual ICollection<PayNearMeOrder> PayNearMeOrders { get; set; }

    public virtual ICollection<CalendarEvent> CalendarEvents { get; set; }

    public virtual ICollection<PropertyStaff> PropertyStaffs { get; set; }

    public virtual ICollection<TestingTable> TestingTables { get; set; }

}

}
