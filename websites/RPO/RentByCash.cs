﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;


namespace RPO
{
    public partial class RentByCash
    {
        public bool SandBoxMode { get; set; }
        public string CreateOrderUrl
        {
            get
            {
                if (SandBoxMode)
                    //cakel: 00478 - Updating API string
                    return "https://paynearme-sandbox.com/api/create_order?";
                   // return "https://sandbox.paynearme.com/api/create_order?";
                return "https://paynearme.com/api/create_order?";
            }
        }


        public string CreatePayNearMeRequest(PayNearMeRequest req) 
        {
            return CreatePayNearMeRequest(req, false);
        }

        public string CreatePayNearMeRequest(PayNearMeRequest req, bool testMode) 
        {
            //Salcedo - 7/7/2014 added logging around the PayNearMe request
            ActivityLog al = new ActivityLog();
            String ActivityDescription = "";

            SandBoxMode = testMode;
            var timestamp = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
            NameValueCollection vars1 = System.Web.HttpUtility.ParseQueryString(string.Empty);
            NameValueCollection vars2 = System.Web.HttpUtility.ParseQueryString(string.Empty);
            vars1.Add("order_amount", req.Amount.ToString());
            vars1.Add("order_currency", req.Currency);

            //Salcedo - 2/18/2015 - added 3 day expiration to new ticket - task # 00374 - order of variables matters- they need to be in alphabetical order
            DateTime ExpirationDate = DateTime.Now.AddDays(3);
            string ExpirationDateString = ExpirationDate.ToString("yyyy-MM-dd") + " " + ExpirationDate.ToString("HH:mm:ss") + " ET";
            vars1.Add("order_expiration_date", ExpirationDateString);

            vars1.Add("order_type", PayNearMeRequest.GetDescription(req.OrderType));

            //vars2.Add("site_creator_identifier", req.CreatorIdentifier);
            vars2.Add("site_customer_identifier", req.CustomerIdentifier);
            vars2.Add("site_identifier", req.SiteIdentifier);            
            vars2.Add("site_order_identifier", req.OrderIdentifier);
            vars2.Add("timestamp", timestamp.ToString());
            vars2.Add("version", req.Version);
            //vars.Add("site_customer_phone", req.CustomerPhone);
            //vars.Add("site_customer_name", req.CustomerName);
            //vars.Add("site_customer_street", req.CustomerStreet);
            //vars.Add("site_customer_city", req.CustomerCity);
            //vars.Add("site_customer_state", req.CustomerState);
            //vars.Add("site_customer_postal_code", req.CustomerPostalCode);

            //Salcedo - 2/18/2015 - task # 00374 - the hash needs to be performed on the 'non-http-ized’ version of the string to work properly when sent to PNM
            string BeforeHash = "order_amount" + req.Amount.ToString();
            BeforeHash += "order_currency" + req.Currency;
            BeforeHash += "order_expiration_date" + ExpirationDateString;
            BeforeHash += "order_type" + PayNearMeRequest.GetDescription(req.OrderType);
            BeforeHash += "site_customer_identifier" + req.CustomerIdentifier;
            BeforeHash += "site_identifier" + req.SiteIdentifier;
            BeforeHash += "site_order_identifier" + req.OrderIdentifier;
            BeforeHash += "timestamp" + timestamp.ToString();
            BeforeHash += "version" + req.Version;
            BeforeHash = BeforeHash.Replace("=", "").Replace("&", "") + req.SecretKey;

            //vars1.Add("signature", CalculateMD5Hash((vars1.ToString() + vars2.ToString()).Replace("=", "").Replace("&", "") + req.SecretKey).ToLower());
            vars1.Add("signature", CalculateMD5Hash(BeforeHash).ToLower());

            //vars.Add("order_duration", req.OrderDuration.ToString());
            
            
            string queryString = vars1.ToString() + "&"+ vars2.ToString();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(CreateOrderUrl + queryString);
            //HttpContext.Current.Response.Write(CreateOrderUrl + queryString);
            request.Method = "GET";

            //Salcedo - 7/7/2014 added logging around the PayNearMe request.
            //Also added exception handling.
            ActivityDescription = "Requesting new PayNearMe ticket using WebRequest: " + CreateOrderUrl + queryString;
            al.WriteLog(req.RenterId.ToString(), ActivityDescription, 3);

            try
            {    
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader sr = new StreamReader(response.GetResponseStream());
                string resp = sr.ReadToEnd();
                XmlDocument responseXml = new XmlDocument();

                ActivityDescription = "PayNearMe raw WebResponse: " + resp;
                al.WriteLog(req.RenterId.ToString(), ActivityDescription, 3);

                responseXml.LoadXml(resp);
                if (responseXml["result"].Attributes["status"].Value == "ok")
                {
                    ActivityDescription = "PayNearMe status ok. Saving WebResponse XML.";
                    al.WriteLog(req.RenterId.ToString(), ActivityDescription, 3);

                    var order = SavePNMResponse(req, responseXml);

                    ActivityDescription = "PayNearMe create order WebResponse XML saved. OrderIdentifier=" + order.PNMOrderIdentifier.ToString() + "; OrderTrackingUrl=" + order.OrderTrackingUrl;
                    al.WriteLog(req.RenterId.ToString(), ActivityDescription, 3);

                    return "PayNearMe status Ok;" + order.OrderTrackingUrl;
                }
                else
                {
                    ActivityDescription = "PayNearMe order creation failed. WebRequest was: " + CreateOrderUrl + queryString + ". Raw WebResponse was: " + resp;
                    al.WriteLog(req.RenterId.ToString(), ActivityDescription, 1);

                    return "PayNearMe status failed;Error";

                   // throw new Exception(responseXml.InnerXml);
                }
            }
            catch (Exception ex)
            {
                ActivityDescription = "Exception generated while processing PayNearMe order. WebRequest was: " + CreateOrderUrl + queryString + ". Exception details: " + ex.ToString();
                al.WriteLog(req.RenterId.ToString(), ActivityDescription, 1);

                throw ex;
            }
        }

        private string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            //HttpContext.Current.Response.Write(input + "<br/>");
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }

        private PayNearMeOrder SavePNMResponse(PayNearMeRequest request, XmlDocument response)
        {
            var order = response["result"].FirstChild;
            var customer = order.ChildNodes[1];
			PayNearMeOrder payNearMeOrder = new PayNearMeOrder();
            using (var entity = new RPOEntities())
            {
				payNearMeOrder.RenterId = request.RenterId;
				payNearMeOrder.CompanyId = request.CompanyId;
				payNearMeOrder.EfxAdministratorId = request.EfxAdministratorId;
				payNearMeOrder.MinimumPaymentAmount = request.MinimumPaymentAmount;
				payNearMeOrder.MinimumPaymentCurrency = request.MinimumPaymentCurrency;
				payNearMeOrder.OrderAmount = request.Amount;
				payNearMeOrder.OrderCreated = Convert.ToDateTime(order.Attributes["order_created"].Value);
				payNearMeOrder.OrderCurrency = request.Currency;
				payNearMeOrder.OrderDuration = request.OrderDuration;
				payNearMeOrder.OrderIsStanding = false;
				payNearMeOrder.OrderStatus = "open";
				payNearMeOrder.OrderTrackingUrl = order.Attributes["order_tracking_url"].Value;
				payNearMeOrder.OrderType = PayNearMeRequest.GetDescription(request.OrderType);
				payNearMeOrder.PNMBalanceDueAmount = Convert.ToDecimal(order.Attributes["pnm_balance_due_amount"].Value);
				payNearMeOrder.PNMBalanceDueCurrency = order.Attributes["pnm_balance_due_currency"].Value;
				payNearMeOrder.PNMCustomerIdentifier = customer.Attributes["pnm_customer_identifier"].Value;
				payNearMeOrder.PNMOrderIdentifier = Convert.ToInt64(order.Attributes["pnm_order_identifier"].Value);
				payNearMeOrder.PNMOrderShortIdentifier = order.Attributes["pnm_order_short_identifier"].Value;
				payNearMeOrder.PropertyId = request.PropertyId;
				payNearMeOrder.PropertyStaffId = request.PropertyStaffId;
				payNearMeOrder.RenterId = request.RenterId;
				payNearMeOrder.RPOOrderIdentifier = Guid.Parse(request.OrderIdentifier);
				payNearMeOrder.SiteLogoUrl = order.Attributes["site_logo_url"].Value;
				payNearMeOrder.SiteOrderDescription = request.OrderDescription;
				payNearMeOrder.SiteOrderIdentifier = Guid.Parse(order.Attributes["site_order_identifier"].Value);
				entity.PayNearMeOrders.Add(payNearMeOrder);
				entity.SaveChanges();
            }
			return payNearMeOrder;
        }
    }
}
