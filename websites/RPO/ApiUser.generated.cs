
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RPO
{

using System.Runtime.Serialization;
using System;
    using System.Collections.Generic;
    

[DataContract(IsReference = true)]
public partial class ApiUser
{

    public ApiUser()
    {

        this.Properties = new HashSet<Property>();

    }


    [DataMember]
	public int ApiUserId { get; set; }

    [DataMember]
	public string Username { get; set; }

    [DataMember]
	public byte[] PasswordHash { get; set; }

    [DataMember]
	public byte[] Salt { get; set; }

    [DataMember]
	public Nullable<int> PartnerCompanyId { get; set; }

    [DataMember]
	public bool IsActive { get; set; }



    public virtual PartnerCompany PartnerCompany { get; set; }

    public virtual ICollection<Property> Properties { get; set; }

}

}
