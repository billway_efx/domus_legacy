
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RPO
{

using System;
    
public partial class usp_Report_PaymentSummaryReport_Result
{

    public string GroupBy { get; set; }

    public Nullable<int> PropertyId { get; set; }

    public string PropertyName { get; set; }

    public string PropertyAddressLine1 { get; set; }

    public string PropertyAddressLine2 { get; set; }

    public string PropertyPhoneNumber { get; set; }

    public Nullable<int> RenterId { get; set; }

    public string RenterName { get; set; }

    public string RenterAddressLine1 { get; set; }

    public string RenterAddressLine2 { get; set; }

    public string RenterPhoneNumber { get; set; }

    public Nullable<int> PayerId { get; set; }

    public string PayerName { get; set; }

    public string PayerAddressLine1 { get; set; }

    public string PayerAddressLine2 { get; set; }

    public string PayerPhoneNumber { get; set; }

    public Nullable<decimal> RentAmount { get; set; }

    public string DatePaidMmYyyy { get; set; }

    public Nullable<System.DateTime> DatePaid { get; set; }

    public string PaymentStatus { get; set; }

    public string PaymentMethod { get; set; }

    public string AccountNumber { get; set; }

    public string InternalTransactionId { get; set; }

    public Nullable<int> PaymentId { get; set; }

    public Nullable<System.DateTime> DepositDate { get; set; }

    public Nullable<int> TransactionId { get; set; }

    public string TimeZone { get; set; }

}

}
