﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RPO
{
	public partial class CalendarEvent
	{
		[DataMember]
		public CalendarEventType IncludedCalendarEventType { get; set; }
        [DataMember]
        public string PropertyName { get; set; }
	}
}
