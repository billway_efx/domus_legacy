
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace RPO
{

using System;
    
public partial class usp_ResidentImportSummary_GetSingleObject_Result
{

    public int ResidentImportSummaryId { get; set; }

    public int PropertyId { get; set; }

    public System.DateTime ImportDatetime { get; set; }

    public int SuccessfulRecordCount { get; set; }

    public int FailedRecordCount { get; set; }

    public string RequestXml { get; set; }

    public string ResponseXml { get; set; }

}

}
