﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using System.Net;
using EnterpriseDT.Net.Ftp;
using EfxFramework;
using RPO;

namespace MRI_FTP_Sync_Receipt_Files
{
    class Program
    {
        static void Main(string[] args)
        {
            SyncReceiptFiles();
        }

        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

        private static void SyncReceiptFiles()
        {
            ActivityLog AL = new ActivityLog();
            String TheSql = "";

            SqlConnection con = new SqlConnection(ConnString);

            // Get list of files to work with ...
            // Should probably move this Sql to a stored procedure, to make maintenance easier. But hopefully this sql code will never need to be changed. Hope springs eternal.
            TheSql =
                "SELECT a.CompanyId, MRIFtpSiteAddress, MRIFtpPort, MRIFTPAccountUserName, MRIFTPAccountPassword, a.PropertyId, c.FileId, " +
                    "c.DateGenerated, isnull(c.FileSize,0) " +
                "FROM Property a " +
                    "JOIN MRIReceiptFiles c on a.CompanyId = c.CompanyId and c.FtpSyncStatus = 0 " +
                "WHERE MRIEnableFtpDownload = 1 " +
                    "AND ISNULL(MRIFTPAccountUserName,'') <> '' " +
                    "AND ISNULL(MRIFTPAccountPassword,'') <> '' " +
                    "AND ISNULL(MRIFtpSiteAddress,'') <> '' " +
                    "AND ISNULL(MRIFTPPort,'') <> '' " +
                    "AND ISNULL(IsDeleted,0) = 0 " +
                    "AND ISNULL(PMSTypeId,0) = 4 " +
                    "AND ISNULL(PmsId,'') <> '' ";

            SqlCommand com = new SqlCommand(TheSql, con);
            com.CommandType = CommandType.Text;
            con.Open();
            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(com);
            sda.Fill(dt);
            con.Close();

            //For each file in the list ...
            foreach (DataRow dr in dt.Rows)
            {
                int CompanyId = (int)dr[0];
                String MRIFtpSiteAddress = dr[1].ToString();
                String MRIFtpPort = dr[2].ToString();
                String MRIFTPAccountUserName = dr[3].ToString();
                String MRIFTPAccountPassword = dr[4].ToString();
                int PropertyId = (int)dr[5];
                long FileId = (long)dr[6];
                DateTime DateGenerated = (DateTime)dr[7];
                long FileSize = (long)dr[8];

                String FileName = "";
                byte[] ByteArray = DownloadMRIReceiptFile(FileId, ref FileName);

                Console.WriteLine("PropertyId " + PropertyId.ToString() + ": Uploading Receipts file '" + FileName + "' to ftp site.");
                AL.WriteLog("System", "PropertyId " + PropertyId.ToString() + ": Uploading Receipts file '" + FileName + "' to ftp site.", (short)LogPriority.LogAlways);

                FTPConnection MyFtpConnection = new FTPConnection();
                MyFtpConnection.ServerAddress = MRIFtpSiteAddress;
                MyFtpConnection.UserName = MRIFTPAccountUserName;
                MyFtpConnection.Password = MRIFTPAccountPassword;
                MyFtpConnection.TransferType = FTPTransferType.BINARY;
                MyFtpConnection.Connect();

                // Make sure we have an "receipts" folder - create one if necessary
                String ReceiptsDirectory = "receipts";
                try
                {
                    MyFtpConnection.ChangeWorkingDirectory(ReceiptsDirectory);
                }
                catch
                {
                    // If the directory isn't there, we'll get an exception, and fall into this code, so we'll attempt to create the directory

                    Console.WriteLine("PropertyId " + PropertyId.ToString() + ": " + ReceiptsDirectory + "' folder was not found. Attempting creation.");
                    AL.WriteLog("System", "PropertyId " + PropertyId.ToString() + ": " + ReceiptsDirectory + "' folder was not found. Attempting creation.", (short)LogPriority.LogAlways);
                    MyFtpConnection.CreateDirectory(ReceiptsDirectory);
                    Console.WriteLine("PropertyId " + PropertyId.ToString() + ": " + ReceiptsDirectory + "' folder was created.");
                    AL.WriteLog("System", "PropertyId " + PropertyId.ToString() + ": " + ReceiptsDirectory + "' folder was created.", (short)LogPriority.LogAlways);

                    MyFtpConnection.ChangeWorkingDirectory(ReceiptsDirectory);
                }

                //Write the file to the directory
                MyFtpConnection.UploadByteArray(ByteArray, FileName);
                Console.WriteLine("PropertyId " + PropertyId.ToString() + ": Receipts file '" + FileName + "' was uploaded successfully to ftp site.");
                AL.WriteLog("System", "PropertyId " + PropertyId.ToString() + ": Receipts file '" + FileName + "' was uploaded successfully to ftp site.", (short)LogPriority.LogAlways);

                MyFtpConnection.Close();

                //Update the database FtpSyncStatus
                TheSql = "UPDATE MRIReceiptFiles SET FtpSyncStatus = 1 WHERE FileID=" + FileId.ToString();
                com = new SqlCommand(TheSql, con);
                com.CommandType = CommandType.Text;
                con.Open();
                com.ExecuteNonQuery();
                con.Close();

            }

        }

        private static byte[] DownloadMRIReceiptFile(Int64 kFile, ref string FileName)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlParameter FileID = null;
            SqlParameter fileName = null;
            SqlDataReader dr = null;
            byte[] result = null;

            try
            {
                conn = new SqlConnection(EfxSettings.ConnectionString);
                cmd = new SqlCommand("DownloadMRIReceiptFile", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                FileID = new SqlParameter("@FileID", System.Data.SqlDbType.BigInt);
                FileID.Value = kFile;
                fileName = new SqlParameter("@FileName", SqlDbType.VarChar, 250);
                fileName.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(FileID);
                cmd.Parameters.Add(fileName);

                conn.Open();
                dr = cmd.ExecuteReader();
                dr.Read();

                // We are casting the value returned by the datareader to the byte[] data type.
                result = (byte[])dr.GetValue(0);

                FileName = (string)dr.GetValue(1);

                dr.Close();
                conn.Close();

                conn.Dispose();
                cmd.Dispose();

            }
            //catch (Exception e)
            catch
            {
                //Console.WriteLine(e.Message + " - " + e.StackTrace);
                result = null;
            }
            return result;
        }

    }
}
