Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports System.Data.SqlClient

Public Class ProcessorLog

    'AppID 1 = Transaction File Parser
    'AppID 2 = Returns Parser
    'AppID 3 = Create Statements
    'AppID 4 = Create Status Report Files
    Public Function WriteLog(ByVal JobID As Guid, ByVal Description As String, ByVal AppID As Int16) As Boolean

        Dim sql As String
        Dim dbMgr As Utilities.DBManager = Nothing

        Try
            'Make sure the string is not too long to place in the database ...
            'And, make sure any single apostrophes are replaced with two apostrophes ...

            'The column holds 7000 characters, but let's keep it to 6990 just to allow a little
            'breathing room ...
            If Len(Replace(Description, "'", "''")) > 6990 Then
                Description = Left(Replace(Description, "'", "''"), 6990)
            Else
                Description = Replace(Description, "'", "''")
            End If

            sql = "Insert into ProcessorLog (JobID, Description, AppID) values ('" & _
                JobID.ToString() & "','" & _
                Description & "', " & _
                AppID.ToString & ")"

            ' Execute statement ...return result to caller
            dbMgr = New Utilities.DBManager(APP_TYPE)
            WriteLog = dbMgr.TransactSql(sql, INSERT)

        Catch ex As Exception
            ' Log exception and propogate up to caller and ASP.Net app
            ExceptionManager.Publish(ex)
            Throw (ex)

        Finally
            ' Clean-up...
            If Not (dbMgr Is Nothing) Then
                dbMgr = Nothing
            End If
        End Try

    End Function

    'LogType = 0 = get all logs
    Public Function GetProcessorLogJobs(ByVal LogsToGet As Int16, ByVal LogType As Int16) As DataSet

        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("Jobs", SqlDbType.Int)
            Dim Param2 As New SqlParameter("@AppId", SqlDbType.Int)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = LogsToGet
            Param2.Value = LogType

            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetProcessorLogJobs", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)
            sqlDa.SelectCommand.Parameters.Add(Param2)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
        End Try

        Return ReturnDS
    End Function

    Public Function GetProcessorLogJobDetail(ByVal JobID As Guid) As DataSet

        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("JobID", SqlDbType.UniqueIdentifier)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = JobID

            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetProcessorLogJobDetail", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
        End Try

        Return ReturnDS
    End Function
End Class