Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports ACH_Objects.ContactInfo

Public Class ContactInfoList

    Inherits CollectionBase

    Private cParentID As Guid

    Public Function AddContactInfo(ByVal ContactInfo As ContactInfo) As Guid
        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim NewGuid As Guid = Guid.NewGuid

        Try
            'Add the new web user to the collection
            ContactInfo.ContactID = NewGuid
            Me.List.Add(ContactInfo)

            'Create SQL ...
            sSql = "INSERT INTO contactinfo " & _
                "(contactid, parentid, contacttypeid, companyname, firstname, lastname, " & _
                "address1, address2, city, state, zip, primaryphone, cellphone, title, email) " & _
                "VALUES ('" & _
                ContactInfo.ContactID.ToString & "', '" & _
                ContactInfo.ParentID.ToString & "', " & _
                ContactInfo.ContactType & ", '" & _
                SafeApos(ContactInfo.CompanyName) & "', '" & _
                SafeApos(ContactInfo.FirstName) & "', '" & _
                SafeApos(ContactInfo.LastName) & "', '" & _
                SafeApos(ContactInfo.Address1) & "', '" & _
                SafeApos(ContactInfo.Address2) & "', '" & _
                SafeApos(ContactInfo.City) & "', '" & _
                SafeApos(ContactInfo.State) & "', '" & _
                SafeApos(ContactInfo.Zip) & "', '" & _
                SafeApos(ContactInfo.PrimaryPhone) & "', '" & _
                SafeApos(ContactInfo.CellPhone) & "', '" & _
                SafeApos(ContactInfo.Title) & "', '" & _
                SafeApos(ContactInfo.Email) & "')"

            'Process SQL transaction ...
            dbMgr.TransactSql(sSql, INSERT)

        Catch ex As Exception
            NewGuid = Guid.Empty
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return NewGuid
    End Function

    Public Function UpdateContactInfo(ByVal ContactInfo As ContactInfo) As Boolean
        Dim TheParent As Guid
        'Dim TheType As ContactTypes
        Dim TheOriginal As ContactInfo
        Dim strSQL As String
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim TheReturn As Boolean

        TheParent = ContactInfo.ParentID
        'TheType = ContactInfo.ContactType
        'TheOriginal = GetContactInfo(TheMerchant, TheType)
        TheOriginal = GetContactInfo(ContactInfo.ContactID)

        'If the original doesn't exist, then we need to do an add
        If TheOriginal.ParentID = Guid.Empty Then
            If AddContactInfo(ContactInfo) <> Guid.Empty Then
                TheReturn = True
            Else
                TheReturn = False
            End If
        Else

            'Otherwise, let's compare them, and see if an update is necessary
            If IsEqualTo(TheOriginal, ContactInfo) Then
                'They are identical, no update necessary
                TheReturn = True
            Else
                'Otherwise, update the database
                Try

                    strSQL = _
                       "UPDATE contactinfo " & _
                       "set companyname = '" & SafeApos(ContactInfo.CompanyName) & "', " & _
                       "firstname = '" & SafeApos(ContactInfo.FirstName) & "', " & _
                       "lastname = '" & SafeApos(ContactInfo.LastName) & "', " & _
                       "address1 = '" & SafeApos(ContactInfo.Address1) & "', " & _
                       "address2 = '" & SafeApos(ContactInfo.Address2) & "', " & _
                       "city = '" & SafeApos(ContactInfo.City) & "', " & _
                       "state = '" & SafeApos(ContactInfo.State) & "', " & _
                       "zip = '" & SafeApos(ContactInfo.Zip) & "', " & _
                       "primaryphone = '" & SafeApos(ContactInfo.PrimaryPhone) & "', " & _
                       "cellphone = '" & SafeApos(ContactInfo.CellPhone) & "', " & _
                       "title = '" & SafeApos(ContactInfo.Title) & "', " & _
                       "email = '" & SafeApos(ContactInfo.Email) & "' " & _
                       "WHERE ContactID = '" & ContactInfo.ContactID.ToString() & "'"

                    Call dbMgr.TransactSql(strSQL, UPDATE)
                    TheReturn = True
                Catch ex As Exception
                    TheReturn = False
                    ExceptionManager.Publish(ex)
                    Throw (ex)
                Finally
                End Try
            End If

        End If

        Return TheReturn

    End Function

    'Get a specific type of ContactInfo record for a given parent
    'Note: if the parent is a webuser, then the ContactType should be 6
    Public Function GetContactInfo(ByVal ParentId As Guid, ByVal ContactType As ContactTypes) As ContactInfo
        Dim strSQL As String
        Dim objContactInfo As New ContactInfo

        Try

            strSQL = _
               "SELECT * FROM contactinfo " & _
               "WHERE parentid = '" & ParentId.ToString() & _
                "' and contacttypeid = " & ContactType

            objContactInfo = GetContactInfoHelper(strSQL)
            Return objContactInfo

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    'Get a specific ContactInfo record for the given unique ContactID key
    Public Function GetContactInfo(ByVal ContactID As Guid) As ContactInfo
        Dim strSQL As String
        Dim objContactInfo As New ContactInfo

        Try

            strSQL = _
               "SELECT * FROM contactinfo " & _
               "WHERE ContactID = '" & ContactID.ToString() & "'"

            objContactInfo = GetContactInfoHelper(strSQL)
            Return objContactInfo

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Private Function GetContactInfoHelper(ByVal TheSql As String) As ContactInfo
        Dim dsContactInfo As DataSet
        Dim drContactInfo As DataRow
        Dim objContactInfo As New ContactInfo
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)

        Try

            dsContactInfo = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(dsContactInfo) Then

                drContactInfo = dsContactInfo.Tables(0).Rows(0)

                ' Build contact info object
                objContactInfo.ContactID = SafeDBGuidField(drContactInfo, "ContactID")
                objContactInfo.ParentID = SafeDBGuidField(drContactInfo, "ParentID")
                objContactInfo.ContactType = SafeDBIntField(drContactInfo, "ContactTypeID")
                objContactInfo.CompanyName = SafeDBStringField(drContactInfo, "CompanyName")
                objContactInfo.FirstName = SafeDBStringField(drContactInfo, "FirstName")
                objContactInfo.LastName = SafeDBStringField(drContactInfo, "LastName")
                objContactInfo.Address1 = SafeDBStringField(drContactInfo, "Address1")
                objContactInfo.Address2 = SafeDBStringField(drContactInfo, "Address2")
                objContactInfo.City = SafeDBStringField(drContactInfo, "City")
                objContactInfo.State = SafeDBStringField(drContactInfo, "State")
                objContactInfo.Zip = SafeDBStringField(drContactInfo, "Zip")
                objContactInfo.PrimaryPhone = SafeDBStringField(drContactInfo, "PrimaryPhone")
                objContactInfo.CellPhone = SafeDBStringField(drContactInfo, "CellPhone")
                objContactInfo.Title = SafeDBStringField(drContactInfo, "Title")
                objContactInfo.Email = SafeDBStringField(drContactInfo, "Email")

            End If

            Return objContactInfo

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try


    End Function

    Public Function RemoveContactInfo(ByVal ContactID As Guid) As Boolean
        ' Error indicator...
        Dim errIndicator As Int16 = 0
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim objContactInfo As ContactInfo
        Dim strSQL As String

        Try

            ' Remove the Contact Info object from the collection...
            objContactInfo = New ContactInfo
            For Each objContactInfo In Me
                If ContactID = objContactInfo.ContactID Then
                    Me.List.Remove(objContactInfo)
                    Exit For
                End If
            Next

            ' Build delete query and execution type...
            strSQL = _
                    "DELETE FROM ContactInfo " & _
                    "WHERE " & _
                    "ContactID = '" & ContactID.ToString & "'"

            ' Remove the Contact Info record from the database...
            If dbMgr.TransactSql(strSQL, DELETE) > 0 Then
                ' Return true to caller...
                RemoveContactInfo = True
            Else
                ' Return false to caller...
                RemoveContactInfo = False
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Clean-up...
        End Try

    End Function

    'Fills the collection with all the ContactInfo objects (records) for the given ParentID
    Public Sub FillCollection(ByVal ParentID As Guid)
        Dim dbMgr As Utilities.DBManager
        Dim drContactInfo As DataRow
        Dim myContactInfo As ContactInfo
        Dim strGetFileTemps As String
        Dim dsContactInfos As DataSet = Nothing

        Try

            ' Build the select query...
            strGetFileTemps = "SELECT ContactId FROM ContactInfo where ParentID = '" & _
                ParentID.ToString() & "'"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsContactInfos = dbMgr.TransactSql(strGetFileTemps, FILL_DATASET)

            ' Insert each Contact Info record into the user list collection...
            For Each drContactInfo In dsContactInfos.Tables(0).Rows

                myContactInfo = GetContactInfo(drContactInfo("ContactId"))

                ' Add Web User to list...
                Me.List.Add(myContactInfo)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsContactInfos Is Nothing) Then
                dsContactInfos.Dispose()
            End If
        End Try

    End Sub

    Public Sub New()

    End Sub

    Public Sub New(ByVal ParentID As Guid)
        cParentID = ParentID
        Call FillCollection(ParentID)
    End Sub

    Public ReadOnly Property ItemByContactType(ByVal ContactType As ContactTypes) As ContactInfo
        Get
            Dim MyContactInfo As New ContactInfo
            ItemByContactType = New ContactInfo

            ItemByContactType.ParentID = cParentID
            ItemByContactType.ContactType = ContactType

            For Each MyContactInfo In Me
                If MyContactInfo.ContactType = ContactType Then
                    ItemByContactType = MyContactInfo
                    Exit For
                End If
            Next
        End Get
    End Property

End Class
