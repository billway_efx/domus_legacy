Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports System.Web.Caching

Public Class GeneratedACHFileList

    Inherits CollectionBase

    Public MyCache As Cache

    Public Function AddFile(ByVal GeneratedACHFile As GeneratedACHFile) As Guid
        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim NewGuid As Guid = Guid.NewGuid

        Try
            'Add the new Return to the collection
            GeneratedACHFile.FileID = NewGuid
            Me.List.Add(GeneratedACHFile)

            'Create SQL ...
            sSql = "INSERT INTO OutgoingODFIFiles " & _
                "(FileID, FileDate, " & _
                "FileName, FileSize, Status) " & _
                "VALUES ('" & _
                GeneratedACHFile.FileID.ToString & "', '" & _
                GeneratedACHFile.FileDate & "', '" & _
                GeneratedACHFile.FileName & "', " & _
                GeneratedACHFile.FileSize.ToString & ", " & _
                GeneratedACHFile.FileStatus.ToString() & ")"

            'Process SQL Return ...
            dbMgr.TransactSql(sSql, INSERT)

        Catch ex As Exception
            NewGuid = Guid.Empty
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return NewGuid
    End Function

    Public Function UpdateFile(ByVal GeneratedACHFile As GeneratedACHFile) As Boolean
        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim NewGuid As Guid = Guid.NewGuid
        Dim bReturn As Boolean = False

        Try

            'Create SQL ...
            sSql = "Update OutgoingODFIFiles " & _
                "Set FileDate = '" & GeneratedACHFile.FileDate.ToString() & "', " & _
                "FileName = '" & GeneratedACHFile.FileName & "', " & _
                "FileSize = " & GeneratedACHFile.FileSize.ToString() & ", " & _
                "Status = " & GeneratedACHFile.FileStatus.ToString() & _
                " Where FileID = '" & GeneratedACHFile.FileID.ToString() & "'"

            'Process SQL Return ...
            dbMgr.TransactSql(sSql, UPDATE)
            bReturn = True

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return bReturn
    End Function

    Public Function GetFile(ByVal FileID As Guid) As GeneratedACHFile
        Dim strSQL As String
        Dim objGeneratedACHFile As New GeneratedACHFile

        Try

            strSQL = _
               "SELECT FileID, FileDate, FileName, FileSize, Status, a.ODFIID, b.ODFIName FROM OutgoingODFIFiles a, ODFIs b " & _
               "WHERE FileID = '" & FileID.ToString & "' and a.ODFIID = b.ODFIID"

            objGeneratedACHFile = GetGeneratedACHFileHelper(strSQL)
            Return objGeneratedACHFile

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Private Function GetGeneratedACHFileHelper(ByVal TheSql As String) As GeneratedACHFile
        Dim dsGeneratedACHFile As DataSet
        Dim drGeneratedACHFile As DataRow
        Dim objGeneratedACHFile As New GeneratedACHFile
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)

        Try

            dsGeneratedACHFile = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(dsGeneratedACHFile) Then

                drGeneratedACHFile = dsGeneratedACHFile.Tables(0).Rows(0)

                ' Build GeneratedACHFile object
                objGeneratedACHFile.FileID = SafeDBGuidField(drGeneratedACHFile, "FileID")
                objGeneratedACHFile.FileDate = SafeDBDateTimeField(drGeneratedACHFile, "FileDate")
                objGeneratedACHFile.FileName = SafeDBStringField(drGeneratedACHFile, "FileName")
                objGeneratedACHFile.FileSize = SafeDBBigIntField(drGeneratedACHFile, "FileSize")
                objGeneratedACHFile.FileStatus = SafeDBIntField(drGeneratedACHFile, "Status")
                objGeneratedACHFile.ODFIId = SafeDBGuidField(drGeneratedACHFile, "ODFIID")
                objGeneratedACHFile.ODFIName = SafeDBStringField(drGeneratedACHFile, "ODFIName")
            End If

            Return objGeneratedACHFile

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function RemoveGeneratedACHFile(ByVal FileID As Guid) As Boolean
        ' Error indicator...
        Dim errIndicator As Int16 = 0
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim objGeneratedACHFile As GeneratedACHFile
        Dim strSQL As String

        Try

            ' Remove the Return from the collection...
            objGeneratedACHFile = New GeneratedACHFile
            For Each objGeneratedACHFile In Me
                If FileID = objGeneratedACHFile.FileID Then
                    Me.List.Remove(objGeneratedACHFile)
                    Exit For
                End If
            Next

            ' Build delete query and execution type...
            strSQL = _
                    "DELETE FROM OutgoingODFIFiles " & _
                    "WHERE " & _
                    "FileID = '" & FileID.ToString & "'"

            ' Remove the Return from the database...
            If dbMgr.TransactSql(strSQL, DELETE) > 0 Then
                ' Return true to caller...
                RemoveGeneratedACHFile = True
            Else
                ' Return false to caller...
                RemoveGeneratedACHFile = False
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Clean-up...
        End Try

    End Function

    Public Function RemoveTodaysFiles() As Boolean
        ' Error indicator...
        Dim errIndicator As Int16 = 0
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim objGeneratedACHFile As GeneratedACHFile
        Dim strSQL As String

        Try

            ' Remove todays files from the collection...
            objGeneratedACHFile = New GeneratedACHFile
            For Each objGeneratedACHFile In Me
                If objGeneratedACHFile.FileDate.Month = Now.Month And _
                        objGeneratedACHFile.FileDate.Day = Now.Day And _
                        objGeneratedACHFile.FileDate.Year = Now.Year Then
                    Me.List.Remove(objGeneratedACHFile)
                    Exit For
                End If
            Next

            ' Build delete query and execution type...
            strSQL = _
                    "DELETE FROM OutgoingODFIFiles " & _
                    "where month(filedate) = month(getdate()) and day(filedate) = day(getdate()) and " & _
                    "year(filedate) = year(getdate())"

            ' Remove the Return from the database...
            If dbMgr.TransactSql(strSQL, DELETE) > 0 Then
                ' Return true to caller...
                RemoveTodaysFiles = True
            Else
                ' Return false to caller...
                RemoveTodaysFiles = True
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Clean-up...
        End Try

    End Function

    Public Sub FillCollection()
        Dim dbMgr As Utilities.DBManager
        Dim drGeneratedACHFile As DataRow
        Dim myGeneratedACHFile As GeneratedACHFile
        Dim strSQL As String
        Dim dsOutgoingODFIFiles As DataSet = Nothing

        Try

            ' Build the select query...
            strSQL = "SELECT FileID FROM OutgoingODFIFiles"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsOutgoingODFIFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)

            ' Insert each user into the user list collection...
            For Each drGeneratedACHFile In dsOutgoingODFIFiles.Tables(0).Rows

                myGeneratedACHFile = GetFile(drGeneratedACHFile("FileID"))

                ' Add file to list...
                Me.List.Add(myGeneratedACHFile)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsOutgoingODFIFiles Is Nothing) Then
                dsOutgoingODFIFiles.Dispose()
            End If
        End Try


    End Sub

    Public Sub FillCollectionWithRecent()
        Dim dbMgr As Utilities.DBManager
        Dim drGeneratedACHFile As DataRow
        Dim myGeneratedACHFile As GeneratedACHFile
        Dim strSQL As String
        Dim dsOutgoingODFIFiles As DataSet = Nothing

        Try

            ' Build the select query...
            strSQL = "SELECT FileID FROM OutgoingODFIFiles WHERE (DATEADD(hour, 2.5, TimeReceived) >= GETDATE()) "

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsOutgoingODFIFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)

            ' Insert each user into the user list collection...
            For Each drGeneratedACHFile In dsOutgoingODFIFiles.Tables(0).Rows

                myGeneratedACHFile = GetFile(drGeneratedACHFile("FileID"))

                ' Add file to list...
                Me.List.Add(myGeneratedACHFile)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsOutgoingODFIFiles Is Nothing) Then
                dsOutgoingODFIFiles.Dispose()
            End If
        End Try
    End Sub
    Public Sub FillCollectionWithLast(ByVal Records As Integer)
        Dim dbMgr As Utilities.DBManager
        Dim drGeneratedACHFile As DataRow
        Dim myGeneratedACHFile As GeneratedACHFile
        Dim strSQL As String
        Dim dsOutgoingODFIFiles As DataSet = Nothing

        Try

            ' Build the select query...
            strSQL = "SELECT TOP (" & Records.ToString() & _
                ") FileID FROM OutgoingODFIFiles ORDER BY FileDate DESC"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsOutgoingODFIFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)

            ' Insert each user into the user list collection...
            For Each drGeneratedACHFile In dsOutgoingODFIFiles.Tables(0).Rows

                myGeneratedACHFile = GetFile(drGeneratedACHFile("FileID"))

                ' Add file to list...
                Me.List.Add(myGeneratedACHFile)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsOutgoingODFIFiles Is Nothing) Then
                dsOutgoingODFIFiles.Dispose()
            End If
        End Try
    End Sub

    Public Function GetDataset() As DataSet

        Dim MyDataset As New DataSet
        Dim MyTable As New DataTable("OutgoingODFIFiles")

        MyTable.Columns.Add("FileID", GetType(Guid))
        MyTable.Columns.Add("Date", GetType(DateTime))
        MyTable.Columns.Add("Name", GetType(String))
        MyTable.Columns.Add("Size", GetType(Long))
        MyTable.Columns.Add("Status", GetType(Integer))
        MyTable.Columns.Add("ODFIID", GetType(Guid))
        MyTable.Columns.Add("ODFIName", GetType(String))

        Dim TheRow(4) As Object
        Dim RF As GeneratedACHFile
        For Each RF In Me
            TheRow(0) = RF.FileID
            TheRow(1) = RF.FileDate
            TheRow(2) = RF.FileName
            TheRow(3) = RF.FileSize
            TheRow(4) = RF.FileStatus
            TheRow(5) = RF.ODFIId
            TheRow(6) = RF.ODFIName
            MyTable.Rows.Add(TheRow)
        Next

        MyDataset.Tables.Add(MyTable)
        Return MyDataset

    End Function

    Public ReadOnly Property ItemByFileID(ByVal FileID As Guid) As GeneratedACHFile
        Get
            Dim MyGeneratedACHFile As New GeneratedACHFile
            ItemByFileID = Nothing

            For Each MyGeneratedACHFile In Me
                If FileID = MyGeneratedACHFile.FileID Then
                    ItemByFileID = MyGeneratedACHFile
                    Exit For
                End If
            Next
        End Get
    End Property

    Public Function GetGeneratedACHFileList(StartRow As Long, MaximumRows As Long) As DataTable

        Dim OutputTable As New DataTable
        Try

            MyCache = System.Web.HttpContext.Current.Cache
            Dim TheTable As DataTable = MyCache.Get("GeneratedACHFileList")
            If TheTable Is Nothing Then
                'Need to populate the list
                Me.Clear()
                Call FillCollectionWithLast(500)
                'MyCache.Add("GeneratedACHFileList", Me.GetDataset().Tables(0), Nothing, Now.AddMinutes(5), Cache.NoSlidingExpiration, CacheItemPriority.Normal, Nothing)
                'We'll invalidate the cache once a minute
                MyCache.Insert("GeneratedACHFileList", Me.GetDataset().Tables(0), Nothing, Now.AddMinutes(1), Cache.NoSlidingExpiration)
                MyCache.Insert("GeneratedACHFileListCount", Me.GetDataset().Tables(0).Rows.Count, Nothing, Now.AddMinutes(1), Cache.NoSlidingExpiration)
                TheTable = MyCache.Get("GeneratedACHFileList")
            End If

            Dim TheFile As DataRow
            OutputTable = TheTable.Clone
            Dim RowCounter As Long = 0
            Dim RowsCollected As Long = 0
            For Each TheFile In TheTable.Rows
                RowCounter = RowCounter + 1

                If RowCounter >= StartRow + 1 Then
                    OutputTable.ImportRow(TheFile)
                    RowsCollected = RowsCollected + 1
                End If

                If RowsCollected = MaximumRows Then Exit For
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return OutputTable

    End Function

    Public Function GetGeneratedACHFileListCount() As Integer

        Dim OutputCount As New Integer
        Try

            MyCache = System.Web.HttpContext.Current.Cache
            Dim TheTable As DataTable = MyCache.Get("GeneratedACHFileList")
            If TheTable Is Nothing Then
                'Need to populate the list
                Me.Clear()
                Call FillCollectionWithLast(500)
                'MyCache.Add("GeneratedACHFileList", Me.GetDataset().Tables(0), Nothing, Now.AddMinutes(5), Cache.NoSlidingExpiration, CacheItemPriority.Normal, Nothing)
                'We'll invalidate the cache once a minute
                MyCache.Insert("GeneratedACHFileList", Me.GetDataset().Tables(0), Nothing, Now.AddMinutes(1), Cache.NoSlidingExpiration)
                MyCache.Insert("GeneratedACHFileListCount", Me.GetDataset().Tables(0).Rows.Count, Nothing, Now.AddMinutes(1), Cache.NoSlidingExpiration)
                TheTable = MyCache.Get("GeneratedACHFileList")
            End If
            OutputCount = MyCache.Get("GeneratedACHFileListCount")

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return OutputCount

    End Function

End Class
