Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports System.Web.Security

Public Class WebUserList

    Inherits CollectionBase

    'Thie function assumes the ASPMembership property is not populated,
    'but needs to be created with the user name/password/email specified
    Public Function AddWebUser(ByVal WebUser As WebUser, ByVal UserName As String, _
            ByVal Password As String, ByVal Email As String) As Guid

        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim NewGuid As Guid = Guid.NewGuid

        Try

            Call CheckRoles()

            'New key ...
            WebUser.WebUserID = NewGuid

            'First, add the new user to the ASP Membership Provider
            Dim TheStatus As MembershipCreateStatus

            'The ASPNet User and our own WebUsers record will share the same UserID value
            Dim NewUser As MembershipUser = Membership.CreateUser( _
                UserName, Password, Email, "No question", "No answer", False, WebUser.WebUserID, TheStatus)

            'If we successfully added the user
            If Not NewUser Is Nothing Then
                WebUser.ASPMembership = NewUser

                'Add the user to the appropriate role
                Dim RoleName As String
                If WebUser.AdminFlag Then
                    RoleName = "admin"
                Else
                    RoleName = "user"
                End If
                Call Roles.AddUserToRole(UserName, RoleName)

                If WebUser.AllowManualBatchEntry Then
                    Call Roles.AddUserToRole(UserName, "ManualBatchEntry")
                End If

                If WebUser.AllowRecurringItemEntry Then
                    Call Roles.AddUserToRole(UserName, "RecurringItemEntry")
                End If

                If WebUser.AllowUploadWebNachaFiles Then
                    Call Roles.AddUserToRole(UserName, "UploadWebNachaFiles")
                End If

                'Add the new web user to the collection
                Me.List.Add(WebUser)

                'Create SQL ...
                sSql = "INSERT INTO webusers " & _
                    "(webuserid, adminflag, defaultmerchantid, allowpasswordchange, " & _
                    "AllowManualBatchEntry, AllowRecurringItemEntry, AllowUploadWebNachaFiles, AllowMerchantCreate) " & _
                    "VALUES ('" & _
                    WebUser.WebUserID.ToString & "', " & _
                    CInt(WebUser.AdminFlag).ToString & ", '" & _
                    WebUser.DefaultMerchantID.ToString & "', " & _
                    CInt(WebUser.AllowPasswordChange).ToString & ", " & _
                    CInt(WebUser.AllowManualBatchEntry).ToString & ", " & _
                    CInt(WebUser.AllowRecurringItemEntry).ToString & ", " & _
                    CInt(WebUser.AllowUploadWebNachaFiles).ToString & ", " & _
                    CInt(WebUser.AllowMerchantCreate).ToString & ")"

                'Process SQL transaction ...
                dbMgr.TransactSql(sSql, INSERT)

                'Everything good so far, so approve the user ...
                NewUser.IsApproved = True
                Call Membership.UpdateUser(NewUser)

            Else
                NewGuid = Guid.Empty
            End If

        Catch ex As Exception
            NewGuid = Guid.Empty
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return NewGuid
    End Function

    Public Sub CheckRoles()

        'Make sure we've got the roles needed
        Dim AllRoles As String() = Roles.GetAllRoles()

        Dim bfoundAdmin As Boolean = False
        Dim bfoundMgr As Boolean = False
        Dim bfoundUser As Boolean = False
        Dim bfoundManualBatchEntry As Boolean = False
        Dim bfoundRecurringItemEntry As Boolean = False
        Dim bFoundUploadWebNachaFiles As Boolean = False

        For Each s As String In AllRoles
            If s = "admin" Then bfoundAdmin = True
            If s = "mgr" Then bfoundMgr = True
            If s = "user" Then bfoundUser = True
            If s = "ManualBatchEntry" Then bfoundManualBatchEntry = True
            If s = "RecurringItemEntry" Then bfoundRecurringItemEntry = True
            If s = "UploadWebNachaFiles" Then bFoundUploadWebNachaFiles = True
        Next

        If Not bfoundAdmin Then Roles.CreateRole("admin")
        If Not bfoundMgr Then Roles.CreateRole("mgr")
        If Not bfoundUser Then Roles.CreateRole("user")
        If Not bfoundManualBatchEntry Then Roles.CreateRole("ManualBatchEntry")
        If Not bfoundRecurringItemEntry Then Roles.CreateRole("RecurringItemEntry")
        If Not bFoundUploadWebNachaFiles Then Roles.CreateRole("UploadWebNachaFiles")

    End Sub

    Public Function UpdateWebUser(ByVal WebUser As WebUser, ByVal UserName As String, _
            Optional ByVal NewPassword As String = "", Optional ByVal NewEmail As String = "") As Boolean

        Try

            Dim ExistingUser As MembershipUser = Membership.GetUser(UserName)
            Dim TheStatus As MembershipCreateStatus

            Call CheckRoles()

            'The ASPNet User and our own WebUsers record will share the same UserID value
            If NewPassword <> "" Then
                'The password is being changed, so remove the old user and add the new
                'one with the new password.
                Dim TempEmail As String = ExistingUser.Email
                If Not Membership.DeleteUser(UserName) Then
                    Return False
                End If
                ExistingUser = Membership.CreateUser( _
                    UserName, NewPassword, TempEmail, "No question", "No answer", _
                    True, WebUser.WebUserID, TheStatus)

                'Add the user to the appropriate role(s)
                Dim RoleName As String
                If WebUser.AdminFlag Then
                    RoleName = "admin"
                Else
                    RoleName = "user"
                End If
                Call Roles.AddUserToRole(UserName, RoleName)

                If WebUser.AllowManualBatchEntry Then
                    Call Roles.AddUserToRole(UserName, "ManualBatchEntry")
                End If

                If WebUser.AllowRecurringItemEntry Then
                    Call Roles.AddUserToRole(UserName, "RecurringItemEntry")
                End If

                If WebUser.AllowUploadWebNachaFiles Then
                    Call Roles.AddUserToRole(UserName, "UploadWebNachaFiles ")
                End If

            End If

            Dim ExistingRoles As String() = Roles.GetRolesForUser(UserName)
            Dim bAdmin As Boolean = False
            Dim bUser As Boolean = False
            Dim bManualBatchEntry As Boolean = False
            Dim bRecurringItemEntry As Boolean = False
            Dim bUploadWebNachaFiles As Boolean = False
            For i As Int16 = 0 To ExistingRoles.Length - 1
                If ExistingRoles(i) = "admin" Then bAdmin = True
                If ExistingRoles(i) = "user" Then bUser = True
                If ExistingRoles(i) = "ManualBatchEntry" Then bManualBatchEntry = True
                If ExistingRoles(i) = "RecurringItemEntry" Then bRecurringItemEntry = True
                If ExistingRoles(i) = "UploadWebNachaFiles" Then bUploadWebNachaFiles = True
            Next

            If WebUser.AdminFlag Then
                'Make sure user has admin role, add it if they don't
                If Not bAdmin Then
                    Call Roles.AddUserToRole(UserName, "admin")
                End If
            Else
                If bAdmin Then
                    'Remove admin role if they were an admin
                    Call Roles.RemoveUserFromRole(UserName, "admin")
                End If
                If Not bUser Then
                    'And add user role if they don't already have it
                    Call Roles.AddUserToRole(UserName, "user")
                End If
            End If

            If WebUser.AllowManualBatchEntry Then
                'Make sure user has ManualBatchEntry role, add it if they don't
                If Not bManualBatchEntry Then
                    Call Roles.AddUserToRole(UserName, "ManualBatchEntry")
                End If
            Else
                If bManualBatchEntry Then
                    'Remove ManualBatchEntry role if they previously had it
                    Call Roles.RemoveUserFromRole(UserName, "ManualBatchEntry")
                End If
            End If

            If WebUser.AllowRecurringItemEntry Then
                'Make sure user has RecurringItemEntry role, add it if they don't
                If Not bRecurringItemEntry Then
                    Call Roles.AddUserToRole(UserName, "RecurringItemEntry")
                End If
            Else
                If bRecurringItemEntry Then
                    'Remove RecurringItemEntry role if they previously had it
                    Call Roles.RemoveUserFromRole(UserName, "RecurringItemEntry")
                End If
            End If

            If WebUser.AllowUploadWebNachaFiles Then
                'Make sure user has UploadWebNachaFiles role, add it if they don't
                If Not bUploadWebNachaFiles Then
                    Call Roles.AddUserToRole(UserName, "UploadWebNachaFiles")
                End If
            Else
                If bUploadWebNachaFiles Then
                    'Remove RecurringItemEntry role if they previously had it
                    Call Roles.RemoveUserFromRole(UserName, "UploadWebNachaFiles")
                End If
            End If

            If NewEmail <> "" Then
                If ExistingUser.Email <> NewEmail Then
                    ExistingUser.Email = NewEmail
                    Call Membership.UpdateUser(ExistingUser)
                End If
            End If

            'Update the miscellaneous fields in the WebUsers table
            Dim strSQL As String
            Dim dbMgr As New Utilities.DBManager(APP_TYPE)

            Try
                strSQL = _
                   "UPDATE WebUsers " & _
                   "SET AdminFlag = " & CInt(WebUser.AdminFlag).ToString & _
                      ", DefaultMerchantID = '" & WebUser.DefaultMerchantID.ToString & "'" & _
                      ", AllowPasswordChange = " & CInt(WebUser.AllowPasswordChange).ToString & _
                      ", AllowManualBatchEntry = " & CInt(WebUser.AllowManualBatchEntry).ToString & _
                      ", AllowRecurringItemEntry = " & CInt(WebUser.AllowRecurringItemEntry).ToString & _
                      ", AllowUploadWebNachaFiles = " & CInt(WebUser.AllowUploadWebNachaFiles).ToString & _
                      ", AllowMerchantCreate = " & CInt(WebUser.AllowMerchantCreate).ToString & _
                 " WHERE webuserid = '" & WebUser.WebUserID.ToString() & "'"

                Call dbMgr.TransactSql(strSQL, UPDATE)
            Catch ex As Exception
                ExceptionManager.Publish(ex)
                Throw (ex)
            Finally
            End Try

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return True
    End Function


    Public Function WebUserExists(ByVal LoginID As String) As Boolean
        Dim TempWebUser As WebUser
        TempWebUser = GetWebUser(LoginID)
        If TempWebUser.WebUserID <> Guid.Empty Then
            Return True
        Else
            Try
                'If we're able to get this user, then this userid already exists in the
                'security profile, and should not be used, so return True, indicating
                'the user exists
                Dim ExistingUser As MembershipUser = Membership.GetUser(LoginID)
                If Not ExistingUser Is Nothing Then
                    Return True
                End If

            Catch ex As Exception

            End Try

            'Otherwise, it does not exist anywhere
            Return False
        End If
    End Function

    Public Function WebUserExists(ByVal WebUserID As Guid) As Boolean
        Dim TempWebUser As WebUser
        TempWebUser = GetWebUser(WebUserID)
        If TempWebUser.WebUserID <> Guid.Empty Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function GetWebUser(ByVal LoginId As String) As WebUser
        Dim strSQL As String
        Dim objWebUser As New WebUser

        Try

            'Points to a view that ties the WebUsers and aspnet_Users tables together
            strSQL = _
               "SELECT * FROM ASPMembershipWebUser " & _
               "WHERE loginid = lower('" & LoginId & "')"

            objWebUser = GetWebUserHelper(strSQL)
            Return objWebUser

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function GetAdminUser() As WebUser
        Dim strSQL As String
        Dim objWebUser As New WebUser

        Try

            'Points to a view that ties the WebUsers and aspnet_Users tables together
            strSQL = _
               "SELECT * FROM ASPMembershipWebUser " & _
               "WHERE AdminFlag <> 0"

            objWebUser = GetWebUserHelper(strSQL)
            Return objWebUser

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function GetWebUser(ByVal WebUserID As Guid) As WebUser
        Dim strSQL As String
        Dim objWebUser As New WebUser

        Try

            strSQL = _
               "SELECT * FROM ASPMembershipWebUser " & _
               "WHERE webuserid = '" & WebUserID.ToString & "'"

            objWebUser = GetWebUserHelper(strSQL)
            Return objWebUser

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try


    End Function

    Private Function GetWebUserHelper(ByVal TheSql As String) As WebUser
        Dim dsWebUser As DataSet
        Dim drWebUser As DataRow
        Dim objWebUser As New WebUser
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)

        Try

            dsWebUser = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(dsWebUser) Then

                drWebUser = dsWebUser.Tables(0).Rows(0)

                ' Build web user object
                objWebUser.WebUserID = SafeDBGuidField(drWebUser, "WebUserID")
                objWebUser.AdminFlag = SafeDBBitField(drWebUser, "AdminFlag")
                objWebUser.DefaultMerchantID = SafeDBGuidField(drWebUser, "DefaultMerchantID")

                'Salcedo - 4/3/2015 - not needed in this context
                'objWebUser.ASPMembership = Membership.GetUser(objWebUser.WebUserID)
                objWebUser.ASPMembership = Nothing

                Dim CIL As New ContactInfoList
                objWebUser.ContactInfo = CIL.GetContactInfo(objWebUser.WebUserID, _
                    ContactInfo.ContactTypes.WebUserInformation)

                objWebUser.AllowPasswordChange = SafeDBBitField(drWebUser, "AllowPasswordChange")
                objWebUser.AllowManualBatchEntry = SafeDBBitField(drWebUser, "AllowManualBatchEntry")
                objWebUser.AllowRecurringItemEntry = SafeDBBitField(drWebUser, "AllowRecurringItemEntry")
                objWebUser.AllowUploadWebNachaFiles = SafeDBBitField(drWebUser, "AllowUploadWebNachaFiles")
                objWebUser.AllowMerchantCreate = SafeDBBitField(drWebUser, "AllowMerchantCreate")

            End If

            Return objWebUser

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try


    End Function

    Public Function RemoveWebUser(ByVal WebUserID As Guid) As Boolean
        ' Error indicator...
        Dim errIndicator As Int16 = 0
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim objWebUser As WebUser
        Dim strSQL As String

        Try

            'Get the user name
            Dim TheWebUser As WebUser = GetWebUser(WebUserID)
            Dim TheName As String = TheWebUser.ASPMembership.UserName

            ' Remove the Web User from the collection...
            objWebUser = New WebUser
            For Each objWebUser In Me
                If WebUserID = objWebUser.WebUserID Then
                    Me.List.Remove(objWebUser)
                    Exit For
                End If
            Next

            ' Build delete query and execution type...
            strSQL = _
                    "DELETE FROM webusers " & _
                    "WHERE " & _
                    "WebUserID = '" & WebUserID.ToString & "'"

            ' Remove the web user from the database...
            If dbMgr.TransactSql(strSQL, DELETE) > 0 Then

                'Remove the user from the ASP security tables
                Call Membership.DeleteUser(TheName, True)

                ' Return true to caller...
                RemoveWebUser = True
            Else

                ' Return false to caller...
                RemoveWebUser = False
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Clean-up...
        End Try

        Return RemoveWebUser
    End Function

    Public Sub FillCollection()
        Dim dbMgr As Utilities.DBManager
        Dim drWebUser As DataRow
        Dim myWebUser As WebUser
        Dim strGetFileTemps As String
        Dim dsWebUsers As DataSet = Nothing

        Try

            ' Build the select query...
            strGetFileTemps = "SELECT webuserid FROM webusers"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsWebUsers = dbMgr.TransactSql(strGetFileTemps, FILL_DATASET)

            ' Insert each user into the user list collection...
            For Each drWebUser In dsWebUsers.Tables(0).Rows

                myWebUser = GetWebUser(drWebUser("webuserid"))

                ' Add Web User to list...
                Me.List.Add(myWebUser)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsWebUsers Is Nothing) Then
                dsWebUsers.Dispose()
            End If
        End Try


    End Sub

    Public Sub FillCollectionWithManualBatchEntryUsers()
        Dim dbMgr As Utilities.DBManager
        Dim drWebUser As DataRow
        Dim myWebUser As WebUser
        Dim strGetFileTemps As String
        Dim dsWebUsers As DataSet = Nothing

        Try

            ' Build the select query...
            strGetFileTemps = "SELECT webuserid FROM webusers where AllowManualBatchEntry = 1"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsWebUsers = dbMgr.TransactSql(strGetFileTemps, FILL_DATASET)

            ' Insert each user into the user list collection...
            For Each drWebUser In dsWebUsers.Tables(0).Rows

                myWebUser = GetWebUser(drWebUser("webuserid"))

                ' Add Web User to list...
                Me.List.Add(myWebUser)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsWebUsers Is Nothing) Then
                dsWebUsers.Dispose()
            End If
        End Try


    End Sub

    Public ReadOnly Property ItemByLoginID(ByVal LoginID As String) As WebUser
        Get
            Dim MyWebUser As New WebUser
            ItemByLoginID = Nothing

            For Each MyWebUser In Me
                If LoginID = MyWebUser.ASPMembership.UserName Then
                    ItemByLoginID = MyWebUser
                    Exit For
                End If
            Next
        End Get
    End Property
End Class
