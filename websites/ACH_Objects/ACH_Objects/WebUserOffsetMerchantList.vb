Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports System.Web.Security

Public Class WebUserOffsetMerchantList

    Inherits CollectionBase

    Public Function AddWebUserOffsetMerchant(ByVal WebUserID As Guid, ByVal MerchantID As Guid) As Guid

        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim NewGuid As Guid = Guid.NewGuid

        Try

            'Add the new web user to the collection
            Dim NewWebUserOffsetMerchant As New WebUserOffsetMerchant
            NewWebUserOffsetMerchant.WebUserOffsetMerchantID = NewGuid
            NewWebUserOffsetMerchant.WebUserID = WebUserID
            NewWebUserOffsetMerchant.MerchantID = MerchantID
            Me.List.Add(NewWebUserOffsetMerchant)

            'Create SQL ...
            sSql = "INSERT INTO WebUserOffsetMerchants " & _
                "(WebUserOffsetMerchantID, WebUserID, MerchantID ) " & _
                "VALUES ('" & _
               NewGuid.ToString & "', '" & _
                WebUserID.ToString & "', '" & _
                MerchantID.ToString & "')"

            'Process SQL transaction ...
            dbMgr.TransactSql(sSql, INSERT)

        Catch ex As Exception
            NewGuid = Guid.Empty
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return NewGuid
    End Function

    Public Function GetWebUserOffsetMerchant(ByVal WebUserOffsetMerchantID As Guid) As WebUserOffsetMerchant
        Dim strSQL As String
        Dim objMerchant As New WebUserOffsetMerchant

        Try

            strSQL = _
               "SELECT b.UserMerchantID, a.WebUserOffsetMerchantID, a.WebUserID, a.MerchantID " & _
               "FROM WebUserOffsetMerchants a, Merchants b " & _
               "WHERE a.WebUserOffsetMerchantID = '" & WebUserOffsetMerchantID.ToString & "' and " & _
               "a.MerchantID = b.MerchantID"

            objMerchant = GetWebUserHelper(strSQL)

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

        Return objMerchant

    End Function

    Private Function GetWebUserHelper(ByVal TheSql As String) As WebUserOffsetMerchant
        Dim TheDataSet As DataSet
        Dim TheDataRow As DataRow
        Dim objMerchant As New WebUserOffsetMerchant
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)

        Try

            TheDataSet = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(TheDataSet) Then

                TheDataRow = TheDataSet.Tables(0).Rows(0)

                ' Build web user object
                objMerchant.WebUserOffsetMerchantID = SafeDBGuidField(TheDataRow, "WebUserOffsetMerchantID")
                objMerchant.WebUserID = SafeDBGuidField(TheDataRow, "WebUserID")
                objMerchant.MerchantID = SafeDBGuidField(TheDataRow, "MerchantID")
                objMerchant.UserMerchantID = SafeDBStringField(TheDataRow, "UserMerchantID")

            End If

            Return objMerchant

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try


    End Function

    Public Function RemoveWebUserOffsetMerchant(ByVal WebUserOffsetMerchantID As Guid) As Boolean
        ' Error indicator...
        Dim errIndicator As Int16 = 0
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim objMerchant As WebUserOffsetMerchant
        Dim strSQL As String

        Try

            ' Remove the Web User Offset Merchant from the collection...
            objMerchant = New WebUserOffsetMerchant
            For Each objMerchant In Me
                If WebUserOffsetMerchantID = objMerchant.WebUserOffsetMerchantID Then
                    Me.List.Remove(objMerchant)
                    Exit For
                End If
            Next

            ' Build delete query and execution type...
            strSQL = _
                    "DELETE FROM WebUserOffsetMerchants " & _
                    "WHERE " & _
                    "WebUserOffsetMerchantID = '" & WebUserOffsetMerchantID.ToString & "'"

            ' Remove the web user from the database...
            If dbMgr.TransactSql(strSQL, DELETE) > 0 Then

                ' Return true to caller...
                RemoveWebUserOffsetMerchant = True
            Else

                ' Return false to caller...
                RemoveWebUserOffsetMerchant = False
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Clean-up...
        End Try

        Return RemoveWebUserOffsetMerchant
    End Function

    'Fill collection with all Merchant IDs for the given Web User
    Public Sub FillCollection(ByVal WebUserID As Guid)
        Dim dbMgr As Utilities.DBManager
        Dim drWebUser As DataRow
        Dim myMerchant As WebUserOffsetMerchant
        Dim strGetFileTemps As String
        Dim dsMerchants As DataSet = Nothing

        Try

            ' Build the select query...
            strGetFileTemps = _
                "SELECT WebUserOffsetMerchantID FROM WebUserOffsetMerchants where WebUserID = " & _
                "'" & WebUserID.ToString & "'"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsMerchants = dbMgr.TransactSql(strGetFileTemps, FILL_DATASET)

            Me.List.Clear()

            ' Insert each user into the user list collection...
            For Each drWebUser In dsMerchants.Tables(0).Rows

                myMerchant = GetWebUserOffsetMerchant(drWebUser("WebUserOffsetMerchantID"))

                ' Add Merchant to list...
                Me.List.Add(myMerchant)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsMerchants Is Nothing) Then
                dsMerchants.Dispose()
            End If
        End Try


    End Sub

End Class
