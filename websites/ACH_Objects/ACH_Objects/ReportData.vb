Public Class ReportData

    Public Function GetSnapshotDebits() As DataSet

        Dim MyDataset As New DataSet
        Dim MyTable As New DataTable("SnapshotDebits")

        MyTable.Columns.Add("Debits", GetType(String))
        MyTable.Columns.Add("Count", GetType(Long))
        MyTable.Columns.Add("Amount", GetType(Double))

        Dim TheRow(2) As Object
        'Dim TF As SettlementStatementFile

        'Manufacture test data
        Dim TempCount As Long
        For TempCount = 0 To 5
            Select Case TempCount
                Case 0
                    TheRow(0) = "Submitted"
                Case 1
                    TheRow(0) = "Rejected"
                Case 2
                    TheRow(0) = "Pending"
                Case 3
                    TheRow(0) = "In Process"
                Case 4
                    TheRow(0) = "Returned"
                Case 5
                    TheRow(0) = "Cleared"
            End Select
            TheRow(1) = 1234567
            TheRow(2) = 123456789.99
            MyTable.Rows.Add(TheRow)
        Next

        MyDataset.Tables.Add(MyTable)
        Return MyDataset

    End Function

    Public Function GetSnapshotCredits() As DataSet

        Dim MyDataset As New DataSet
        Dim MyTable As New DataTable("SnapshotCredits")

        MyTable.Columns.Add("Credits", GetType(String))
        MyTable.Columns.Add("Count", GetType(Long))
        MyTable.Columns.Add("Amount", GetType(Double))

        Dim TheRow(2) As Object
        'Dim TF As SettlementStatementFile

        'Manufacture test data
        Dim TempCount As Long
        For TempCount = 0 To 5
            Select Case TempCount
                Case 0
                    TheRow(0) = "Submitted"
                Case 1
                    TheRow(0) = "Rejected"
                Case 2
                    TheRow(0) = "Pending"
                Case 3
                    TheRow(0) = "In Process"
                Case 4
                    TheRow(0) = "Returned"
                Case 5
                    TheRow(0) = "Cleared"
            End Select
            TheRow(1) = 1234567
            TheRow(2) = 123456789.99
            MyTable.Rows.Add(TheRow)
        Next

        MyDataset.Tables.Add(MyTable)
        Return MyDataset

    End Function

End Class
