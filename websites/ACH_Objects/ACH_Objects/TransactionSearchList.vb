Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports System.Data.SqlClient

Public Class TransactionSearchList

    Inherits CollectionBase

    'Return a populated SettlementStatementFile object for the specified FileID GUID
    Public Function GetTransactionSearch(ByVal SearchID As Guid) As TransactionSearch
        Dim strSQL As String
        Dim objTransactionSearch As New TransactionSearch

        Try

            strSQL = _
               "SELECT SearchID, MerchantID, SearchDate, FromDate, ToDate, TransactionStatus, " & _
               "TransactionAmount, AmountFrom, AmountTo, FirstName, LastName, " & _
               "MerchantTransactionID, MerchantCustomerID " & _
               "FROM TransactionSearches " & _
               "WHERE SearchID = '" & SearchID.ToString & "'"

            objTransactionSearch = GetTransactionSearchHelper(strSQL)
            Return objTransactionSearch

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    'Call the database and populate the object
    Private Function GetTransactionSearchHelper(ByVal TheSql As String) As TransactionSearch
        Dim dsTransactionSearch As DataSet
        Dim drTransactionSearch As DataRow
        Dim objTransactionSearch As New TransactionSearch
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)

        Try

            dsTransactionSearch = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(dsTransactionSearch) Then

                drTransactionSearch = dsTransactionSearch.Tables(0).Rows(0)

                ' Build TransactionSearch object
                objTransactionSearch.SearchID = SafeDBGuidField(drTransactionSearch, "SearchID")
                objTransactionSearch.MerchantID = SafeDBGuidField(drTransactionSearch, "MerchantID")
                objTransactionSearch.SearchDate = SafeDBDateTimeField(drTransactionSearch, "SearchDate")
                objTransactionSearch.FromDate = SafeDBDateTimeField(drTransactionSearch, "FromDate")
                objTransactionSearch.ToDate = SafeDBDateTimeField(drTransactionSearch, "ToDate")
                objTransactionSearch.TransactionStatus = SafeDBIntField(drTransactionSearch, "TransactionStatus")
                objTransactionSearch.TransactionAmount = SafeDBMoneyField(drTransactionSearch, "TransactionAmount")
                objTransactionSearch.AmountFrom = SafeDBMoneyField(drTransactionSearch, "AmountFrom")
                objTransactionSearch.AmountTo = SafeDBMoneyField(drTransactionSearch, "AmountTo")
                objTransactionSearch.FirstName = SafeDBStringField(drTransactionSearch, "FirstName")
                objTransactionSearch.LastName = SafeDBStringField(drTransactionSearch, "LastName")
                objTransactionSearch.MerchantTransactionID = SafeDBStringField(drTransactionSearch, "MerchantTransactionID")
                objTransactionSearch.MerchantCustomerID = SafeDBStringField(drTransactionSearch, "MerchantCustomerID")
            End If

            Return objTransactionSearch

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    ''Populate the collection with all the Files for the specified Merchant GUID
    'Public Sub FillCollection(ByVal MerchantID As Guid)
    '    Dim dbMgr As Utilities.DBManager
    '    Dim drReturnFile As DataRow
    '    Dim myReturnFile As SettlementStatementFile
    '    Dim strSQL As String
    '    Dim dsReturnFiles As DataSet = Nothing

    '    Try

    '        Me.List.Clear()

    '        ' Build the select query...
    '        strSQL = "SELECT FileID FROM SettlementStatementFiles where MerchantID = '" & _
    '            MerchantID.ToString & "' order by TimeCreated desc"

    '        ' Retrieve the rows from the database...
    '        dbMgr = New Utilities.DBManager(APP_TYPE)
    '        dsReturnFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)

    '        ' Insert each user into the user list collection...
    '        For Each drReturnFile In dsReturnFiles.Tables(0).Rows

    '            myReturnFile = GetFile(drReturnFile("FileID"))

    '            ' Add file to list...
    '            Me.List.Add(myReturnFile)
    '        Next

    '    Catch ex As Exception
    '        ExceptionManager.Publish(ex)
    '        Throw (ex)
    '    Finally
    '        ' Release resources
    '        If Not (dsReturnFiles Is Nothing) Then
    '            dsReturnFiles.Dispose()
    '        End If
    '    End Try
    'End Sub

    ''Return a dataset containing all the rows currently in the collection
    'Public Function GetDataset() As DataSet

    '    Dim MyDataset As New DataSet
    '    Dim MyTable As New DataTable("SettlementStatementFiles")

    '    MyTable.Columns.Add("FileID", GetType(Guid))
    '    MyTable.Columns.Add("MerchantID", GetType(Guid))
    '    MyTable.Columns.Add("TimeCreated", GetType(DateTime))
    '    MyTable.Columns.Add("FileSize", GetType(Long))
    '    MyTable.Columns.Add("Status", GetType(Int16))
    '    MyTable.Columns.Add("PeriodStartDate", GetType(DateTime))
    '    MyTable.Columns.Add("PeriodEndDate", GetType(DateTime))
    '    MyTable.Columns.Add("Description", GetType(String))
    '    MyTable.Columns.Add("ReportDataID", GetType(Guid))

    '    Dim TheRow(8) As Object
    '    Dim TF As SettlementStatementFile
    '    For Each TF In Me
    '        TheRow(0) = TF.FileID
    '        TheRow(1) = TF.MerchantID
    '        TheRow(2) = TF.TimeCreated
    '        TheRow(3) = TF.FileSize
    '        TheRow(4) = TF.Status
    '        TheRow(5) = TF.PeriodStartDate
    '        TheRow(6) = TF.PeriodEndDate
    '        TheRow(7) = TF.Description
    '        TheRow(8) = TF.ReportDataID
    '        MyTable.Rows.Add(TheRow)
    '    Next

    '    MyDataset.Tables.Add(MyTable)
    '    Return MyDataset

    'End Function

    'Returns true if the specified WebUser has authorized access to this Settlement Statement FileID

    Public Function AddSearch(ByVal NewSearch As TransactionSearch) As Guid
        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim NewGuid As Guid = Guid.NewGuid

        Try
            'Add the new search to the collection
            NewSearch.SearchID = NewGuid
            Me.List.Add(NewSearch)

            'Create SQL ...
            sSql = "INSERT INTO TransactionSearches " & _
                "(SearchID, MerchantID, SearchDate, FromDate, ToDate, " & _
                "TransactionStatus, TransactionAmount, AmountFrom, AmountTo, " & _
                "FirstName, LastName, MerchantTransactionID, MerchantCustomerID) " & _
                "VALUES ('" & _
                NewSearch.SearchID.ToString & "', '" & _
                NewSearch.MerchantID.ToString & "', '" & _
                Now & "', '" & _
                NewSearch.FromDate.ToShortDateString & "', '" & _
                NewSearch.ToDate.ToShortDateString & "', " & _
                NewSearch.TransactionStatus.ToString & ", " & _
                NewSearch.TransactionAmount.ToString & ", " & _
                NewSearch.AmountFrom.ToString & ", " & _
                NewSearch.AmountTo.ToString & ", '" & _
                SafeApos(NewSearch.FirstName) & "', '" & _
                SafeApos(NewSearch.LastName) & "', '" & _
                SafeApos(NewSearch.MerchantTransactionID) & "', '" & _
                SafeApos(NewSearch.MerchantCustomerID) & "')"

            'Process SQL transaction ...
            dbMgr.TransactSql(sSql, INSERT)

        Catch ex As Exception
            NewGuid = Guid.Empty
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return NewGuid

    End Function

    Public Function AuthorizeSearchAccess(ByVal WebUser As WebUser, ByVal SearchID As Guid) As Boolean
        Dim bReturn As Boolean = False

        Try

            Dim strSQL As String

            strSQL = _
               "SELECT count(*) TheCount " & _
               "FROM TransactionSearches ts, Merchants m " & _
               "WHERE ts.SearchID = '" & SearchID.ToString & "' and " & _
               "ts.MerchantID = ts.MerchantID and " & _
               "m.WebUserID = '" & WebUser.WebUserID.ToString & "'"

            Dim dsTransactionSearches As DataSet
            Dim drTransactionSearches As DataRow
            Dim dbMgr As New Utilities.DBManager(APP_TYPE)
            Dim RowCount As Long = 0

            dsTransactionSearches = dbMgr.TransactSql(strSQL, FILL_DATASET)

            If DataSetHasRows(dsTransactionSearches) Then

                drTransactionSearches = dsTransactionSearches.Tables(0).Rows(0)

                RowCount = SafeDBIntField(drTransactionSearches, "TheCount")

            End If

            If RowCount > 0 Then
                bReturn = True
            Else
                bReturn = False
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

        Return bReturn

    End Function

    Public Function GetSearchDataset(ByVal SearchID As Guid) As DataSet

        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("SearchID", SqlDbType.UniqueIdentifier)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = SearchID


            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetTransactionSearchResults", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
        End Try

        Return ReturnDS
    End Function

End Class
