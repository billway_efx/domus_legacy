Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports System.Data.SqlClient

Public Class MiscDatabase

    Public Sub New()

    End Sub

    Public Function GetUnprocessedTranasctionCount() As Long
        Dim MyDataset As DataSet
        Dim MyDataRow As DataRow
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim ReturnValue As Long
        Dim TheSql As String

        Try

            TheSql = "select count(*) TheCount from transactions where " & _
                "transactionstatus = 1"

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(MyDataset) Then

                MyDataRow = MyDataset.Tables(0).Rows(0)

                ReturnValue = SafeDBBigIntField(MyDataRow, "TheCount")

            End If

            Return ReturnValue

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function GetDatabaseServerTime() As DateTime
        Dim MyDataset As DataSet
        Dim MyDataRow As DataRow
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim ReturnValue As DateTime
        Dim TheSql As String

        Try

            TheSql = "select getdate() as TheTime"

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)
            MyDataRow = MyDataset.Tables(0).Rows(0)
            ReturnValue = SafeDBDateTimeField(MyDataRow, "TheTime")

            Return ReturnValue

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function GetUnprocessedTranasctionCount(ODFIID As Guid) As Long
        Dim MyDataset As DataSet
        Dim MyDataRow As DataRow
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim ReturnValue As Long
        Dim TheSql As String

        Try

            TheSql = "select count(*) TheCount from transactions a, " & _
                "merchants b where " & _
                "a.merchantid = b.merchantid and " & _
                "b.assignedodfiid = '" & ODFIID.ToString & "' and " & _
                "transactionstatus = 1"

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(MyDataset) Then

                MyDataRow = MyDataset.Tables(0).Rows(0)

                ReturnValue = SafeDBBigIntField(MyDataRow, "TheCount")

            End If

            Return ReturnValue

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function GetTodaysOutgoingODFIFileCount() As Int16
        Dim MyDataset As DataSet
        Dim MyDataRow As DataRow
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim ReturnValue As Int16
        Dim TheSql As String

        Try

            TheSql = "select count(*) TheCount " & _
                "from OutgoingODFIFiles " & _
                "where year(filedate) = year(getdate()) and " & _
                "month(filedate) = month(getdate()) and " & _
                "day(filedate) = day(getdate())"

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(MyDataset) Then

                MyDataRow = MyDataset.Tables(0).Rows(0)

                ReturnValue = SafeDBIntField(MyDataRow, "TheCount")

            End If

            Return ReturnValue

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function GetTransactionAnalysis(ByVal MerchantID As Guid, ByVal FromDate As Date, _
        ByVal ToDate As Date, TransactionType As Int16) As DataSet

        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("MerchantID", SqlDbType.UniqueIdentifier)
            Dim Param2 As New SqlParameter("FromDate", SqlDbType.DateTime)
            Dim Param3 As New SqlParameter("ToDate", SqlDbType.DateTime)
            Dim Param4 As New SqlParameter("TransactionType", SqlDbType.Int)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = MerchantID
            Param2.Value = FromDate
            Param3.Value = ToDate
            Param4.Value = TransactionType

            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetTransactionAnalysisData", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)
            sqlDa.SelectCommand.Parameters.Add(Param2)
            sqlDa.SelectCommand.Parameters.Add(Param3)
            sqlDa.SelectCommand.Parameters.Add(Param4)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
        End Try

        Return ReturnDS
    End Function

    Public Function GetTransactionAnalysisDetail(ByVal MerchantID As Guid, ByVal FromDate As Date, _
    ByVal ToDate As Date, ByVal AnalysisDataID As Int16, TransactionType As Int16) As DataSet

        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("MerchantID", SqlDbType.UniqueIdentifier)
            Dim Param2 As New SqlParameter("FromDate", SqlDbType.DateTime)
            Dim Param3 As New SqlParameter("ToDate", SqlDbType.DateTime)
            Dim Param4 As New SqlParameter("ID", SqlDbType.Int)
            Dim Param5 As New SqlParameter("TransactionType", SqlDbType.Int)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = MerchantID
            Param2.Value = FromDate
            Param3.Value = ToDate
            Param4.Value = AnalysisDataID
            Param5.Value = TransactionType

            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetTransactionAnalysisDataDetail", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)
            sqlDa.SelectCommand.Parameters.Add(Param2)
            sqlDa.SelectCommand.Parameters.Add(Param3)
            sqlDa.SelectCommand.Parameters.Add(Param4)
            sqlDa.SelectCommand.Parameters.Add(Param5)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
        End Try

        Return ReturnDS
    End Function

    Public Function GetTransactionSnapshot(ByVal MerchantID As Guid, ByVal FromDate As Date, _
        ByVal ToDate As Date) As DataSet

        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("MerchantID", SqlDbType.UniqueIdentifier)
            Dim Param2 As New SqlParameter("FromDate", SqlDbType.DateTime)
            Dim Param3 As New SqlParameter("ToDate", SqlDbType.DateTime)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = MerchantID
            Param2.Value = FromDate
            Param3.Value = ToDate


            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetTransactionSnapshotData", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)
            sqlDa.SelectCommand.Parameters.Add(Param2)
            sqlDa.SelectCommand.Parameters.Add(Param3)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
        End Try

        Return ReturnDS
    End Function

    Public Function GetTransactionSnapshotDetail(ByVal MerchantID As Guid, ByVal FromDate As Date, _
        ByVal ToDate As Date, ByVal SnapshotID As Double) As DataSet

        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("MerchantID", SqlDbType.UniqueIdentifier)
            Dim Param2 As New SqlParameter("FromDate", SqlDbType.DateTime)
            Dim Param3 As New SqlParameter("ToDate", SqlDbType.DateTime)
            Dim Param4 As New SqlParameter("ID", SqlDbType.Decimal)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = MerchantID
            Param2.Value = FromDate
            Param3.Value = ToDate
            Param4.Value = SnapshotID


            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetTransactionSnapshotDataDetail", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)
            sqlDa.SelectCommand.Parameters.Add(Param2)
            sqlDa.SelectCommand.Parameters.Add(Param3)
            sqlDa.SelectCommand.Parameters.Add(Param4)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return ReturnDS
    End Function

    Public Function GetBankingHolidays() As DataSet
        Dim MyDataset As DataSet
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim TheSql As String

        Try

            TheSql = "select HolidayID, HolidayDate, HolidayName from BankingHolidays " & _
                "where holidaydate >= '" & Now.ToShortDateString & "' order by HolidayDate "

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)

            Return MyDataset

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function GetActivityLog(ByVal startRowIndex As Int32, ByVal maximumRows As Int32) As DataTable

        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("@Start", SqlDbType.Int)
            Dim Param2 As New SqlParameter("@Count", SqlDbType.Int)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = startRowIndex + 1
            Param2.Value = maximumRows

            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetActivityLogPagedData", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)
            sqlDa.SelectCommand.Parameters.Add(Param2)

            dsInfo = New DataSet
            Dim dsTable As New DataTable

            sqlDa.Fill(dsTable)
            sqlDa.Dispose()

            Return dsTable

        Catch ex As Exception
            ReturnDS = Nothing
            ExceptionManager.Publish(ex)
            Throw (ex)

        End Try

    End Function
    Public Function GetActivityLog2(ByVal UserID As String) As DataTable

        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("@Start", SqlDbType.Int)
            Dim Param2 As New SqlParameter("@Count", SqlDbType.Int)
            Dim Param3 As New SqlParameter("@UserID", SqlDbType.VarChar, 50)

            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = 1
            Param2.Value = 1000
            If UserID = "[All Users]" Then UserID = ""
            Param3.Value = UserID

            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetActivityLogPagedData", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)
            sqlDa.SelectCommand.Parameters.Add(Param2)
            sqlDa.SelectCommand.Parameters.Add(Param3)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)
            sqlDa.Dispose()

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
            ExceptionManager.Publish(ex)
            Throw (ex)

        End Try
        Return ReturnDS.Tables(0)

    End Function

    Public Function CountActivityLogRecords() As Long
        Dim MyDataset As DataSet
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim TheSql As String

        Try

            TheSql = "select count(*) from ActivityLog"

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)

            Return CLng(MyDataset.Tables(0).Rows(0)(0))

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function GetBankingHolidays(ByVal RecordLimit As Int16) As DataSet
        Dim MyDataset As DataSet
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim TheSql As String

        Try

            TheSql = "select top " & RecordLimit.ToString & " HolidayID, HolidayDate, HolidayName from BankingHolidays " & _
                "where holidaydate >= '" & Now.ToShortDateString & "' order by HolidayDate "

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)

            Return MyDataset

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function DeleteBankingHoliday(ByVal HolidayID As Int32) As Boolean
        Dim TheSql As String
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim bReturn As Boolean

        Try

            TheSql = "delete BankingHolidays where HolidayID = " & HolidayID.ToString

            Call dbMgr.TransactSql(TheSql, DELETE)

            bReturn = True

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
            bReturn = False

        Finally
        End Try

        Return bReturn

    End Function

    Public Function AddBankingHoliday(ByVal HolidayDate As Date, ByVal HolidayName As String) As Boolean
        Dim TheSql As String
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim bReturn As Boolean

        Try

            Dim HolidayYear As Integer = HolidayDate.Year
            Dim HolidayMonth As Integer = HolidayDate.Month
            Dim HolidayDay As Integer = HolidayDate.Day
            Dim HolidayDayOfWeek As Integer = HolidayDate.DayOfWeek + 1

            TheSql = "insert BankingHolidays (HolidayName, HolidayDate, " & _
                "HolidayYear, HolidayMonth, HolidayDay, HolidayDayOfWeek) " & _
                "values ('" & SafeApos(HolidayName) & "', '" & HolidayDate.ToString & "', " & _
                HolidayYear.ToString & ", " & HolidayMonth.ToString & ", " & _
                HolidayDay.ToString & ", " & HolidayDayOfWeek.ToString & ")"

            Call dbMgr.TransactSql(TheSql, INSERT)

            bReturn = True

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
            bReturn = False

        Finally
        End Try

        Return bReturn

    End Function

    Public Function GetExternalApplication(ByVal AppID As Int16) As DataRow
        Dim MyDataset As DataSet
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim TheSql As String

        Try

            TheSql = "select * from ExternalApplications where AppID = " & AppID.ToString

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)

            Return MyDataset.Tables(0).Rows(0)

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function UpdateExternalApplication(ByVal ApplicationDataRow As DataRow) As Boolean
        Dim TheSql As String
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim bReturn As Boolean

        Try

            TheSql = "update ExternalApplications set CommandLine = '" & _
                SafeApos(ApplicationDataRow("CommandLine").ToString) & "' " & _
                "where AppID = " & ApplicationDataRow("AppID").ToString

            Call dbMgr.TransactSql(TheSql, UPDATE)

            bReturn = True

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
            bReturn = False

        Finally
        End Try

        Return bReturn

    End Function

    Public Function GetNachaFileErrors(FileID As Guid) As DataSet
        Dim MyDataset As DataSet
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim TheSql As String

        Try

            TheSql = "select LineNumber, LineText, ErrorText from NACHAFileErrors " & _
                "where FileID = '" & FileID.ToString & "' order by SequenceNumber, LineNumber "

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)

            Return MyDataset

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function
    Public Function AddNachaFileError(FileID As Guid, LineNumber As Long, LineText As String, ErrorText As String) As Boolean
        Dim TheSql As String
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim bReturn As Boolean

        Try

            TheSql = "insert NachaFileErrors (FileID, LineNumber, " & _
                "LineText, ErrorText) " & _
                "values ('" & FileID.ToString & "', " & LineNumber.ToString() & ", '" & SafeApos(LineText) & "', '" & SafeApos(ErrorText) & "')"

            Call dbMgr.TransactSql(TheSql, INSERT)

            bReturn = True

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
            bReturn = False

        Finally
        End Try

        Return bReturn
    End Function
    Public Function GetNachaFileSummary(FileID As Guid) As DataSet
        Dim MyDataset As DataSet
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim TheSql As String

        Try

            'TheSql = "select b.FileName, c.UserMerchantID, b.TimeReceived, b.FileSize, d.Description as 'FileStatus', BatchCount, EntryCount, TotalDebitAmount, TotalCreditAmount, " & _
            '    "TotalDebitCount, TotalCreditCount, e.LoginID from NACHAFileSummaryInfo a, TransactionFiles b, Merchants c, TransactionFileStatuses d, ASPMembershipWebUser e " & _
            '    "where a.FileID = '" & FileID.ToString & "' and a.FileID = b.FileID and b.MerchantID = c.MerchantID and b.Status = d.StatusID and c.WebUserID = e.WebUserID"
            TheSql = "select b.FileName, c.UserMerchantID, b.TimeReceived, b.FileSize, d.Description as FileStatus, " & _
                    "isnull(BatchCount,0) BatchCount, isnull(EntryCount,0) EntryCount, " & _
                    "isnull(TotalDebitAmount,0) TotalDebitAmount, ISNULL(TotalCreditAmount,0) TotalCreditAmount, " & _
                    "ISNULL(TotalDebitCount,0) TotalDebitCount, ISNULL(TotalCreditCount,0) TotalCreditCount, " & _
                    "e.LoginID " & _
                "from " & _
                    "TransactionFiles b " & _
                    "  left outer join NACHAFileSummaryInfo a on a.FileID = b.FileID, " & _
                    "Merchants c, " & _
                    "TransactionFileStatuses d, " & _
                    "ASPMembershipWebUser e " & _
                "where " & _
                    "b.FileID = '" & FileID.ToString & "' and " & _
                    "b.MerchantID = c.MerchantID and b.Status = d.StatusID and c.WebUserID = e.WebUserID"

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)

            Return MyDataset

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function
    Public Function GetNachaBatchSummary(FileID As Guid) As DataSet
        Dim MyDataset As DataSet
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim TheSql As String

        Try

            TheSql = "select BatchID, BatchNumber, TotalDebitAmount, TotalCreditAmount, " & _
                "TotalDebitCount, TotalCreditCount, CompanyName, CompanyDiscretionaryData, " & _
                "CompanyID, CompanyEntryDescription, CompanyDescriptiveDate, EffectiveEntryDate, " & _
                "SettlementDate from NACHABatchSummaryInfo " & _
                "where FileID = '" & FileID.ToString & "' order by BatchNumber "

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)

            Return MyDataset

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function
    Public Function GetNachaDetailRecords(FileID As Guid) As DataSet
        Dim MyDataset As DataSet
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim TheSql As String

        Try

            TheSql = "select EntryDetailID, FileID, BatchID, " & _
                "LineNumber, TransactionCode, ReceivingDFIId, " & _
                "CheckDigit, DFIAccountNumber, Amount, " & _
                "ReceiverID, ReceiverName, DiscretionaryData, " & _
                "AddendaRecordIndicator, TraceNumber " & _
                "from NACHAEntryDetailRecords " & _
                "where FileID = '" & FileID.ToString & "' order by BatchID, LineNumber "

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)

            Return MyDataset

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function GetNachaFileHistory(FileID As Guid) As DataSet
        Dim MyDataset As DataSet
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim TheSql As String

        Try

            TheSql = "select * from NachaFileHistory where FileID = '" & FileID.ToString & "' Order by EntryDate "

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)

            Return MyDataset

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function InsertNachaFileBatchSummaryInfo(FileID As Guid, FileLineNumber As Long, BatchNumber As Long, ServiceClassCode As String, CompanyName As String, _
            CompanyDiscretionaryData As String, CompanyID As String, CompanyEntryDescription As String, CompanyDescriptiveDate As String, _
            EffectiveEntryDate As String, SettlementDate As String, StandardEntryClassCode As String) As Boolean
        Dim TheSql As String
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim bReturn As Boolean


        Try


            TheSql = "insert NachaBatchSummaryInfo (" & _
                "FileID, FileLineNumber, BatchNumber, ServiceClassCode, CompanyName, " & _
                "CompanyDiscretionaryData, CompanyID, CompanyEntryDescription, CompanyDescriptiveDate, " & _
                "EffectiveEntryDate, SettlementDate, StandardEntryClassCode) " & _
                "values ('" & _
                FileID.ToString & "', " & FileLineNumber.ToString() & ", " & BatchNumber.ToString & ", '" & SafeApos(ServiceClassCode) & "', '" & SafeApos(CompanyName) & "', '" & _
                SafeApos(CompanyDiscretionaryData) & "', '" & SafeApos(CompanyID) & "', '" & SafeApos(CompanyEntryDescription) & "', '" & SafeApos(CompanyDescriptiveDate) & "', '" & _
                SafeApos(EffectiveEntryDate) & "', '" & SafeApos(SettlementDate) & "', '" & SafeApos(StandardEntryClassCode) & "')"

            Call dbMgr.TransactSql(TheSql, INSERT)

            bReturn = True

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
            bReturn = False

        Finally
        End Try

        Return bReturn
    End Function
    Public Function InsertNachaEntryDetailRecord(FileID As Guid, BatchNumber As Long, FileLineNumber As Long, TransactionCode As String, ReceivingDFIId As String, CheckDigit As String, _
        DFIAccountNumber As String, Amount As Double, ReceiverID As String, ReceiverName As String, DiscretionaryData As String, AddendaRecordIndicator As String, TraceNumber As String, _
        SubmitOnOrAfter As Date) As Guid

        Dim TheSql As String
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim MyDataset As DataSet
        Dim BatchID As String
        Dim NewID As Guid = Guid.NewGuid
        Dim ReturnGuid As Guid = Guid.Empty

        Try
            TheSql = "select BatchID from NachaBatchSummaryInfo where FileID = '" & FileID.ToString & "' and BatchNumber = " & BatchNumber
            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)
            If MyDataset.Tables(0).Rows.Count = 0 Then
                Return ReturnGuid
            Else
                BatchID = MyDataset.Tables(0).Rows(0).Item(0).ToString()
            End If

            TheSql = "insert NachaEntryDetailRecords (" & _
                "EntryDetailID, FileID, BatchID, FileLineNumber, TransactionCode, ReceivingDFIId, CheckDigit, " & _
                "DFIAccountNumber, Amount, ReceiverID, ReceiverName, " & _
                "DiscretionaryData, AddendaRecordIndicator, TraceNumber, SubmitOnOrAfter) " & _
                "values ('" & _
                NewID.ToString & "', '" & FileID.ToString & "', '" & BatchID & "', " & FileLineNumber.ToString() & ", '" & SafeApos(TransactionCode) & "', '" & SafeApos(ReceivingDFIId) & "', '" & SafeApos(CheckDigit) & "', '" & _
                SafeApos(DFIAccountNumber) & "', " & Amount.ToString & ", '" & SafeApos(ReceiverID) & "', '" & SafeApos(ReceiverName) & "', '" & _
                SafeApos(DiscretionaryData) & "', '" & SafeApos(AddendaRecordIndicator) & "', '" & SafeApos(TraceNumber) & "', '" & SubmitOnOrAfter.ToShortDateString & "')"

            Call dbMgr.TransactSql(TheSql, INSERT)

            ReturnGuid = NewID

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)

        Finally
        End Try

        Return ReturnGuid
    End Function

    Public Function InsertNachaAddendaDetailRecord(EntryDetailID As Guid, FileID As Guid, _
        PaymentRelatedInfo As String, AddendaSequenceNumber As Long, EntryDetailSequenceNumber As Long) As Boolean

        Dim TheSql As String
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim bReturn As Boolean = False

        Try
 
            TheSql = "insert NachaAddendaDetailRecords (" & _
                "EntryDetailID, FileID, PaymentRelatedInfo, AddendaSequenceNumber, EntryDetailSequenceNumber) " & _
                "values ('" & _
                EntryDetailID.ToString & "', '" & FileID.ToString & "', '" & _
                SafeApos(PaymentRelatedInfo) & "', " & AddendaSequenceNumber.ToString & ", " & EntryDetailSequenceNumber.ToString & ")"

            Call dbMgr.TransactSql(TheSql, INSERT)

            bReturn = True

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)

        Finally
        End Try

        Return bReturn
    End Function

    Public Function UpdateNachaFileBatchSummaryInfo(FileID As Guid, BatchNumber As Long, TotalDebitAmount As Double, TotalCreditAmount As Double, TotalDebitCount As Long, TotalCreditCount As Long) As Boolean
        Dim TheSql As String
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim bReturn As Boolean
        Dim MyDataset As DataSet
        Dim BatchID As String

        Try
            TheSql = "select BatchID from NachaBatchSummaryInfo where FileID = '" & FileID.ToString & "' and BatchNumber = " & BatchNumber
            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)
            If MyDataset.Tables(0).Rows.Count = 0 Then
                Return False
            Else
                BatchID = MyDataset.Tables(0).Rows(0).Item(0).ToString()
            End If

            TheSql = "update NachaBatchSummaryInfo " & _
                "set TotalDebitAmount = " & TotalDebitAmount.ToString & ", " & _
                "TotalCreditAmount = " & TotalCreditAmount.ToString & ", " & _
                "TotalDebitCount = " & TotalDebitCount.ToString & ", " & _
                "TotalCreditCount = " & TotalCreditCount.ToString & " " & _
                "where FileID = '" & FileID.ToString & "' and BatchID = '" & BatchID & "'"

            Call dbMgr.TransactSql(TheSql, INSERT)

            bReturn = True

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
            bReturn = False

        Finally
        End Try

        Return bReturn
    End Function
    Public Function InsertNachaFileSummaryInfo(FileID As Guid, BatchCount As Long, EntryCount As Long, TotalDebitAmount As Double, TotalCreditAmount As Double, _
            TotalDebitCount As Long, TotalCreditCount As Long) As Boolean
        Dim TheSql As String
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim bReturn As Boolean


        Try


            TheSql = "insert NachaFileSummaryInfo (" & _
                "FileID, BatchCount, EntryCount, TotalDebitAmount, TotalCreditAmount, " & _
                "TotalDebitCount, TotalCreditCount) " & _
                "values ('" & _
                FileID.ToString() & "', " & BatchCount.ToString() & ", " & EntryCount.ToString & ", " & TotalDebitAmount.ToString() & ", " & TotalCreditAmount.ToString() & ", " & _
                TotalDebitCount.ToString() & ", " & TotalCreditCount.ToString() & ")"

            Call dbMgr.TransactSql(TheSql, INSERT)

            bReturn = True

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
            bReturn = False

        Finally
        End Try

        Return bReturn
    End Function

    Public Function ValidateABA(RoutingNumber As String) As Boolean
        Dim TheSql As String
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim bReturn As Boolean = False
        Dim MyDataset As DataSet


        Try
            TheSql = "select count(*) from FedACHDirectory where RoutingNumber = '" & SafeApos(RoutingNumber) & "'"
            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)
            If MyDataset.Tables(0).Rows.Count = 0 Then
                bReturn = False
            Else
                If MyDataset.Tables(0).Rows(0).Item(0) = 0 Then
                    bReturn = False
                Else
                    bReturn = True
                End If
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
            bReturn = False

        Finally
        End Try

        Return bReturn
    End Function

    Public Function MoveApprovedWebFileTransactions() As Boolean
        Dim bReturn As Boolean = False
        Try
            Dim dbMgr As New Utilities.DBManager(APP_TYPE)
            Dim sSql As String = "EXEC MoveApprovedWebFileTransactions"
            Dim ReturnCode As Int16 = dbMgr.TransactSql(sSql, INSERT)
            bReturn = True

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
            bReturn = False
        End Try

        Return bReturn
    End Function

    Public Function GetNachaSearchResults(ListType As String, SearchList As String) As DataSet

        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("ListType", SqlDbType.VarChar, 50)
            Dim Param2 As New SqlParameter("SearchList", SqlDbType.VarChar, 7500)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = ListType
            Param2.Value = SearchList

            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetNachaSearchResults", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)
            sqlDa.SelectCommand.Parameters.Add(Param2)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
        End Try

        Return ReturnDS
    End Function

    Public Function GetDashboardData(ByVal MerchantID As Guid, ChartType As Int16, Optional ByRef TransactionType As String = "Unknown") As DataSet

        Dim ReturnDS As DataSet = Nothing
        Dim StoredProcedureName As String = ""

        Select Case ChartType

            Case 1 'Transaction Count
                StoredProcedureName = "Dashboard_TransactionCount"

            Case 2 'Transaction Amount
                StoredProcedureName = "Dashboard_TransactionAmount"

            Case 3 'Transaction Counts - Daily Averages by Month
                StoredProcedureName = "Dashboard_TransactionCountMonthlyAvg"

            Case 4 'Transaction Counts - Monthly Totals
                StoredProcedureName = "Dashboard_TransactionCountMonthlyTotal"

            Case 5 'Transaction Amounts - Daily Averages by Month
                StoredProcedureName = "Dashboard_TransactionAmountMonthlyAvg"

            Case 6 'Transaction Amounts - Monthly Totals
                StoredProcedureName = "Dashboard_TransactionAmountMonthlyTotal"

        End Select

        Try
            Dim Param1 As New SqlParameter("MerchantID", SqlDbType.UniqueIdentifier)
            Dim Param2 As New SqlParameter("TransactionType", SqlDbType.Int)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = MerchantID
            Param2.Direction = ParameterDirection.InputOutput

            'Adjust between UI and the database
            Select Case UCase(TransactionType)
                Case "DEBIT"
                    Param2.Value = 2
                Case "CREDIT"
                    Param2.Value = 1
                Case Else
                    Param2.Value = -1
            End Select

            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter(StoredProcedureName, DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)
            sqlDa.SelectCommand.Parameters.Add(Param2)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)
            TransactionType = Param2.Value

            'Adjust for the UI on the way back out
            Select Case Param2.Value
                Case 2
                    TransactionType = "Debit"
                Case 1
                    TransactionType = "Credit"
            End Select

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
        End Try

        Return ReturnDS
    End Function

    Public Function InsertNachaAddendaRecord(TransactionID As Guid, MerchantID As Guid, FileID As Guid, PaymentRelatedInfo As String, _
            ByRef AddendaSequenceNumber As Int16, EntryDetailSequenceNumber As Long) As Boolean

        Dim TheSql As String
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim bReturn As Boolean

        Try

            TheSql = "insert AddendaRecords (" & _
                "TransactionID, FileID, MerchantID, PaymentRelatedInfo, AddendaSequenceNumber, EntryDetailSequenceNumber) " & _
                "values ('" & _
                TransactionID.ToString() & "', '" & FileID.ToString & "', '" & MerchantID.ToString() & "', '" & SafeApos(PaymentRelatedInfo) & "', " & _
                AddendaSequenceNumber.ToString() & ", " & EntryDetailSequenceNumber.ToString() & ")"

            Call dbMgr.TransactSql(TheSql, INSERT)

            bReturn = True

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
            bReturn = False

        Finally
        End Try

        Return bReturn
    End Function

    Public Function GetTransactionsForReturnMatching(ByVal Amount As Double, TransactionType As Int16, ByVal StartDate As Date, _
     ByVal EndDate As Date, CustomerID As String, FirstName As String, LastName As String, RoutingNumber As String, AccountNumber As String, _
     StartRow As Int64, EndRow As Int64, Optional TransactionStatus As Int16 = 0) As DataSet

        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("Amount", SqlDbType.Money)
            Dim Param2 As New SqlParameter("TransactionType", SqlDbType.Int)
            Dim Param3 As New SqlParameter("StartDate", SqlDbType.Date)
            Dim Param4 As New SqlParameter("EndDate", SqlDbType.Date)
            Dim Param5 As New SqlParameter("CustomerID", SqlDbType.VarChar)
            Dim Param6 As New SqlParameter("FirstName", SqlDbType.VarChar)
            Dim Param7 As New SqlParameter("LastName", SqlDbType.VarChar)
            Dim Param8 As New SqlParameter("RoutingNumber", SqlDbType.VarChar)
            Dim Param9 As New SqlParameter("AccountNumber", SqlDbType.VarChar)
            Dim Param10 As New SqlParameter("StartRow", SqlDbType.BigInt)
            Dim Param11 As New SqlParameter("EndRow", SqlDbType.BigInt)
            Dim Param12 As New SqlParameter("TransactionStatus", SqlDbType.Int)

            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = Amount
            Param2.Value = TransactionType
            Param3.Value = StartDate
            Param4.Value = EndDate
            Param5.Value = CustomerID
            Param6.Value = FirstName
            Param7.Value = LastName
            Param8.Value = RoutingNumber
            Param9.Value = AccountNumber
            Param10.Value = StartRow
            Param11.Value = EndRow
            Param12.Value = TransactionStatus


            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetTransactionsForReturnMatching", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)
            sqlDa.SelectCommand.Parameters.Add(Param2)
            sqlDa.SelectCommand.Parameters.Add(Param3)
            sqlDa.SelectCommand.Parameters.Add(Param4)
            sqlDa.SelectCommand.Parameters.Add(Param5)
            sqlDa.SelectCommand.Parameters.Add(Param6)
            sqlDa.SelectCommand.Parameters.Add(Param7)
            sqlDa.SelectCommand.Parameters.Add(Param8)
            sqlDa.SelectCommand.Parameters.Add(Param9)
            sqlDa.SelectCommand.Parameters.Add(Param10)
            sqlDa.SelectCommand.Parameters.Add(Param11)
            sqlDa.SelectCommand.Parameters.Add(Param12)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
        End Try

        Return ReturnDS
    End Function

    Public Function GetTransactionCountForReturnMatching(ByVal Amount As Double, TransactionType As Int16, ByVal StartDate As Date, _
      ByVal EndDate As Date, CustomerID As String, FirstName As String, LastName As String, RoutingNumber As String, AccountNumber As String) As Long

        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("Amount", SqlDbType.Money)
            Dim Param2 As New SqlParameter("TransactionType", SqlDbType.Int)
            Dim Param3 As New SqlParameter("StartDate", SqlDbType.Date)
            Dim Param4 As New SqlParameter("EndDate", SqlDbType.Date)
            Dim Param5 As New SqlParameter("CustomerID", SqlDbType.VarChar)
            Dim Param6 As New SqlParameter("FirstName", SqlDbType.VarChar)
            Dim Param7 As New SqlParameter("LastName", SqlDbType.VarChar)
            Dim Param8 As New SqlParameter("RoutingNumber", SqlDbType.VarChar)
            Dim Param9 As New SqlParameter("AccountNumber", SqlDbType.VarChar)

            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = Amount
            Param2.Value = TransactionType
            Param3.Value = StartDate
            Param4.Value = EndDate
            Param5.Value = CustomerID
            Param6.Value = FirstName
            Param7.Value = LastName
            Param8.Value = RoutingNumber
            Param9.Value = AccountNumber

            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetTransactionCountForReturnMatching", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)
            sqlDa.SelectCommand.Parameters.Add(Param2)
            sqlDa.SelectCommand.Parameters.Add(Param3)
            sqlDa.SelectCommand.Parameters.Add(Param4)
            sqlDa.SelectCommand.Parameters.Add(Param5)
            sqlDa.SelectCommand.Parameters.Add(Param6)
            sqlDa.SelectCommand.Parameters.Add(Param7)
            sqlDa.SelectCommand.Parameters.Add(Param8)
            sqlDa.SelectCommand.Parameters.Add(Param9)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
            Return 0
        End Try

        Return ReturnDS.Tables(0).Rows(0).Item(0)

    End Function

    Public Function ReturnsProcessingJobLogInsert() As Long
        Dim strSQL As String
        Dim NewValue As Long
        Dim DB As New Utilities.DBManager

        strSQL = "insert into ReturnsProcessingJobLog (TimeStarted,Status) values (getdate(), 1); SELECT @@IDENTITY"
        Dim Cmd As New SqlCommand(strSQL, DB.GetConnection)
        NewValue = Cmd.ExecuteScalar()

        Return NewValue

    End Function

    Public Function Exec_Process_Returns(JobID As Long) As Boolean

        Try

            Dim Param1 As New SqlParameter("JobID", SqlDbType.BigInt)
            Dim Param2 As New SqlParameter("ExecDate", SqlDbType.DateTime)

            Dim DB As New Utilities.DBManager

            Param1.Value = JobID
            Param2.Value = Now

            Dim Cmd As New SqlCommand("Process_Returns", DB.GetConnection)
            Cmd.CommandType = CommandType.StoredProcedure

            Cmd.Parameters.Add(Param1)
            Cmd.Parameters.Add(Param2)

            Cmd.ExecuteNonQuery()


        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function

    Public Function Get_Process_Returns_Results(JobID As Long) As DataSet
        Dim ReturnDS As DataSet = Nothing

        Try

            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet


            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("select * from ReturnsProcessingJobLog where JobID = " & JobID.ToString(), DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.Text

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
            Return ReturnDS
        End Try

        Return ReturnDS
    End Function

    Public Function GetReturnsByJobID(ByVal JobID As Long) As DataSet

        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("JobID", SqlDbType.BigInt)
 
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = JobID

            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetReturnsByJobID", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
        End Try

        Return ReturnDS
    End Function

    Public Function Get_ReturnsNeedProcessing() As Boolean
        Dim bReturn As Boolean = False

        Try

            Dim DB As New Utilities.DBManager
            Dim strSQL As String
            Dim NewValue As Long

            strSQL = "select count(*) from Returns where ReturnStatus=1"
            Dim Cmd As New SqlCommand(strSQL, DB.GetConnection)
            NewValue = Cmd.ExecuteScalar()

            If NewValue > 0 Then
                bReturn = True
            End If

        Catch ex As Exception

            Return bReturn
        End Try

        Return bReturn
    End Function

    Public Function Get_ReturnDetail(ReturnID As Guid) As DataSet
        Dim ReturnDS As DataSet = Nothing

        Try

            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet


            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("select * from Returns where ReturnID = '" & ReturnID.ToString() & "'", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.Text

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
            Return ReturnDS
        End Try

        Return ReturnDS
    End Function

    Public Function UpdateReturnTransactionID(ReturnID As Guid, TransactionID As Guid, ReturnStatus As Int16) As Boolean
        Dim bReturn As Boolean = False

        Try

            Dim DB As New Utilities.DBManager
            Dim strSQL As String

            strSQL = "update Returns set TransactionID = '" & TransactionID.ToString & "', ReturnStatus =" & ReturnStatus.ToString & " where ReturnID = '" & ReturnID.ToString & "'"
            Dim Cmd As New SqlCommand(strSQL, DB.GetConnection)
            Call Cmd.ExecuteNonQuery()

            bReturn = True
        Catch ex As Exception
            bReturn = False

        End Try

        Return bReturn
    End Function

    Public Function GetReturnsReportData(ByVal JobID As Long) As DataSet

        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("JobID", SqlDbType.BigInt)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = JobID


            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetReturnsReportData", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return ReturnDS
    End Function

    Public Function GetChargebackReportData(ByVal JobID As Long) As DataSet

        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("JobID", SqlDbType.BigInt)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = JobID


            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetChargebackReportData", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return ReturnDS
    End Function
    Public Function GetUnmatchedReturnData(ByVal JobID As Long) As DataSet

        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("JobID", SqlDbType.BigInt)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = JobID


            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetUnmatchedReturnData", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return ReturnDS
    End Function
    Public Function Update_ReturnsProcessingJobLog(JobID As Long) As Boolean
        Dim bReturn As Boolean = False

        Try

            Dim DB As New Utilities.DBManager
            Dim strSQL As String

            strSQL = "exec Update_ReturnsProcessingJobLog @JobID =" & JobID.ToString
            Dim Cmd As New SqlCommand(strSQL, DB.GetConnection)
            Call Cmd.ExecuteNonQuery()

            bReturn = True
        Catch ex As Exception
            bReturn = False

        End Try

        Return bReturn
    End Function
    Public Function Exec_EmailReturnNotifications(JobID As Long) As Boolean
        Dim bReturn As Boolean = False

        Try

            Dim DB As New Utilities.DBManager
            Dim strSQL As String

            strSQL = "exec EmailReturnNotifications @JobID =" & JobID.ToString
            Dim Cmd As New SqlCommand(strSQL, DB.GetConnection)
            Call Cmd.ExecuteNonQuery()

            bReturn = True
        Catch ex As Exception
            bReturn = False

        End Try

        Return bReturn
    End Function

    'cakel: BUGID00282 - Added new function for stored procedure call
    Public Function Exec_ReturnProcess(JobID As Long)

        Try
            Dim DB As New Utilities.DBManager
            Dim strSQL As String

            strSQL = "exec Process_RPO_ReturnsByJobID @JobID =" & JobID.ToString
            Dim Cmd As New SqlCommand(strSQL, DB.GetConnection)
            Call Cmd.ExecuteNonQuery()

            Return Nothing
        Catch ex As Exception

        End Try
        Return Nothing
    End Function

    Public Function GetNextBusinessDay(StartDate As DateTime) As DateTime
        Dim MyDataset As DataSet
        Dim MyDataRow As DataRow
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim ReturnValue As DateTime
        Dim TheSql As String

        Try

            TheSql = "select dbo.fn_GetNextBusinessDay ('" & StartDate.ToShortDateString & "') as TheDate"

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)
            MyDataRow = MyDataset.Tables(0).Rows(0)
            ReturnValue = SafeDBDateTimeField(MyDataRow, "TheDate")

            Return ReturnValue

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function
    Public Function GetBusinessDays(StartDate As DateTime, EndDate As DateTime) As Long
        Dim MyDataset As DataSet
        Dim MyDataRow As DataRow
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim ReturnValue As Long
        Dim TheSql As String

        Try

            TheSql = "select dbo.fn_GetBusinessDays ('" & StartDate.ToShortDateString & "', '" & EndDate.ToShortDateString & "') as TheCount"

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(MyDataset) Then

                MyDataRow = MyDataset.Tables(0).Rows(0)

                ReturnValue = SafeDBBigIntField(MyDataRow, "TheCount")

            End If

            Return ReturnValue

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function
    Public Function NachaFileAlreadyExists(MerchantID As Guid, FileCreationDate As String, FileIDModifier As String) As Boolean
        Dim MyDataset As DataSet
        Dim MyDataRow As DataRow
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim RowCount As Long = 0
        Dim TheSql As String

        Try

            TheSql = "select count(*) TheCount from transactionfiles where " & _
                "merchantid = '" & MerchantID.ToString & _
                "' and NACHAFileCreationDate = '" & FileCreationDate & _
                "' and NACHAFileIdModifier = '" & FileIDModifier & "'"

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(MyDataset) Then

                MyDataRow = MyDataset.Tables(0).Rows(0)

                RowCount = SafeDBBigIntField(MyDataRow, "TheCount")

            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

        If RowCount > 0 Then
            Return True
        Else
            Return False
        End If

    End Function
    Public Function NachaTraceNumberAlreadyExistsInBatch(FileId As Guid, BatchNumber As Long, TraceNumber As String) As Boolean
        Dim MyDataset As DataSet
        Dim MyDataRow As DataRow
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim RowCount As Long = 0
        Dim TheSql As String
        Dim BatchID As String

        Try
            TheSql = "select BatchID from NachaBatchSummaryInfo where FileID = '" & FileId.ToString & "' and BatchNumber = " & BatchNumber
            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)
            If MyDataset.Tables(0).Rows.Count = 0 Then
                Return False
            Else
                BatchID = MyDataset.Tables(0).Rows(0).Item(0).ToString()
            End If

            TheSql = "select count(*) TheCount from NachaEntryDetailRecords where " & _
                "FileID = '" & FileId.ToString & "' and BatchId = '" & BatchID & "' and TraceNumber = '" & TraceNumber & "'"

            MyDataset = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(MyDataset) Then
                MyDataRow = MyDataset.Tables(0).Rows(0)
                RowCount = SafeDBBigIntField(MyDataRow, "TheCount")
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

        If RowCount > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

End Class
