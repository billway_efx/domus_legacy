Public Class ReturnFile

#Region "Private class variables"

    'This ID points to a unique record in the ReturnFiles table
    Private cFileID As Guid

    'File attributes
    Private cFileDate As DateTime
    Private cFileName As String
    Private cTimeReceived As DateTime
    Private cFileSize As Long
    Private cFileStatus As Integer

#End Region

#Region "Class attributes"

    Public Property FileID() As Guid
        Get
            Return cFileID
        End Get
        Set(ByVal value As Guid)
            cFileID = value
        End Set
    End Property
    Public Property FileDate() As DateTime
        Get
            Return cFileDate
        End Get
        Set(ByVal value As DateTime)
            cFileDate = value
        End Set
    End Property
    Public Property FileName() As String
        Get
            Return cFileName
        End Get
        Set(ByVal value As String)
            cFileName = value
        End Set
    End Property
    Public Property TimeReceived() As DateTime
        Get
            Return cTimeReceived
        End Get
        Set(ByVal value As DateTime)
            cTimeReceived = value
        End Set
    End Property
    Public Property FileSize() As Long
        Get
            Return cFileSize
        End Get
        Set(ByVal value As Long)
            cFileSize = value
        End Set
    End Property
    Public Property FileStatus() As Integer
        Get
            Return cFileStatus
        End Get
        Set(ByVal value As Integer)
            cFileStatus = value
        End Set
    End Property

#End Region

#Region "Public Class Methods"

    'Default constructor
    Public Sub New()
        cFileID = Guid.Empty
        cFileDate = New Date(1800, 1, 1)
        cFileName = ""
        cTimeReceived = New Date(2099, 1, 1)
        cFileSize = 0
        cFileStatus = 1
    End Sub


#End Region

End Class
