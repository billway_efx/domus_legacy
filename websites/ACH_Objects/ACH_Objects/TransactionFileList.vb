Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports System.Data.SqlClient
Imports System.Web.Caching
Imports System.Web


Public Class TransactionFileList

    Inherits CollectionBase

    'Private cTheCache As Cache
    'Public Property TheCache As Cache
    '    Get
    '        Return cTheCache
    '    End Get
    '    Set(ByVal value As Cache)
    '        cTheCache = value
    '    End Set
    'End Property

    Public Function AddFile(ByVal TransactionFile As TransactionFile) As Guid
        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim NewGuid As Guid = Guid.NewGuid

        Try
            'Add the new transaction to the collection
            TransactionFile.FileID = NewGuid
            Me.List.Add(TransactionFile)

            'Create SQL ...
            sSql = "INSERT INTO TransactionFiles " & _
                "(FileID, FileDate, MerchantID, " & _
                "FileName, TimeReceived, FileSize) " & _
                "VALUES ('" & _
                TransactionFile.FileID.ToString & "', '" & _
                TransactionFile.FileDate & "', '" & _
                TransactionFile.MerchantID.ToString & "', '" & _
                TransactionFile.FileName & "', '" & _
                TransactionFile.TimeReceived.ToString & "', " & _
                TransactionFile.FileSize.ToString & ")"

            'Process SQL transaction ...
            dbMgr.TransactSql(sSql, INSERT)

        Catch ex As Exception
            NewGuid = Guid.Empty
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return NewGuid
    End Function
    Public Function AddFileWithExistingFileID(ByVal TransactionFile As TransactionFile) As Guid
        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        'Dim NewGuid As Guid = Guid.NewGuid

        Try
            'Add the new transaction to the collection

            'This method assumes that a FileID has already been supplied
            If TransactionFile.FileID = Guid.Empty Then
                Throw New Exception("Missing FileID")
            End If

            'TransactionFile.FileID = NewGuid
            Me.List.Add(TransactionFile)

            'Create SQL ...
            sSql = "INSERT INTO TransactionFiles " & _
                "(FileID, FileDate, MerchantID, " & _
                "FileName, TimeReceived, FileSize) " & _
                "VALUES ('" & _
                TransactionFile.FileID.ToString & "', '" & _
                TransactionFile.FileDate & "', '" & _
                TransactionFile.MerchantID.ToString & "', '" & _
                TransactionFile.FileName & "', '" & _
                TransactionFile.TimeReceived.ToString & "', " & _
                TransactionFile.FileSize.ToString & ")"

            'Process SQL transaction ...
            dbMgr.TransactSql(sSql, INSERT)

        Catch ex As Exception
            'NewGuid = Guid.Empty
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return TransactionFile.FileID
    End Function

    Public Function UpdateFile(ByVal TransactionFile As TransactionFile) As Boolean
        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim NewGuid As Guid = Guid.NewGuid
        Dim bReturn As Boolean = False

        Try

            Dim RejectReason As String = ""
            RejectReason = SafeApos(TransactionFile.RejectReason)
            If RejectReason.Length > 2000 Then
                RejectReason = RejectReason.Substring(0, 2000)
            End If

            'Create SQL ...
            sSql = "Update TransactionFiles " & _
                "Set FileDate = '" & TransactionFile.FileDate.ToString() & "', " & _
                "MerchantID = '" & TransactionFile.MerchantID.ToString() & "', " & _
                "FileName = '" & TransactionFile.FileName & "', " & _
                "TimeReceived = '" & TransactionFile.TimeReceived & "', " & _
                "FileSize = " & TransactionFile.FileSize.ToString() & ", " & _
                "Status = " & TransactionFile.Status.ToString() & ", " & _
                "RejectReason = '" & RejectReason & "', " & _
                "NACHAFileCreationDate = '" & TransactionFile.NACHAFileCreationDate & "', " & _
                "NACHAFileIdModifier = '" & TransactionFile.NACHAFileIdModifier & "' " & _
                " Where FileID = '" & TransactionFile.FileID.ToString() & "'"

            'Process SQL transaction ...
            dbMgr.TransactSql(sSql, UPDATE)
            bReturn = True

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return bReturn
    End Function

    Public Function GetFile(ByVal FileID As Guid) As TransactionFile
        Dim strSQL As String
        Dim objTransactionFile As New TransactionFile

        Try

            strSQL = _
               "SELECT FileID, FileDate, MerchantID, FileName, TimeReceived, FileSize, Status, RejectReason, StatusChangeDate, NACHAFileCreationDate, NACHAFileIdModifier " & _
               "FROM TransactionFiles " & _
               "WHERE FileID = '" & FileID.ToString & "'"

            objTransactionFile = GetTransactionFileHelper(strSQL)
            Return objTransactionFile

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Private Function GetTransactionFileHelper(ByVal TheSql As String) As TransactionFile
        Dim dsTransactionFile As DataSet
        Dim drTransactionFile As DataRow
        Dim objTransactionFile As New TransactionFile
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)

        Try

            dsTransactionFile = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(dsTransactionFile) Then

                drTransactionFile = dsTransactionFile.Tables(0).Rows(0)

                ' Build TransactionFile object
                objTransactionFile.FileID = SafeDBGuidField(drTransactionFile, "FileID")
                objTransactionFile.FileDate = SafeDBDateTimeField(drTransactionFile, "FileDate")
                objTransactionFile.MerchantID = SafeDBGuidField(drTransactionFile, "MerchantID")
                objTransactionFile.FileName = SafeDBStringField(drTransactionFile, "FileName")
                objTransactionFile.TimeReceived = SafeDBDateTimeField(drTransactionFile, "TimeReceived")
                objTransactionFile.FileSize = SafeDBBigIntField(drTransactionFile, "FileSize")
                objTransactionFile.Status = SafeDBIntField(drTransactionFile, "Status")
                objTransactionFile.RejectReason = SafeDBStringField(drTransactionFile, "RejectReason")
                objTransactionFile.StatusChangeDate = SafeDBDateTimeField(drTransactionFile, "StatusChangeDate")
                objTransactionFile.NACHAFileCreationDate = SafeDBStringField(drTransactionFile, "NACHAFileCreationDate")
                objTransactionFile.NACHAFileIdModifier = SafeDBStringField(drTransactionFile, "NACHAFileIdModifier")
            End If

            Return objTransactionFile

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function RemoveTransactionFile(ByVal FileID As Guid) As Boolean
        ' Error indicator...
        Dim errIndicator As Int16 = 0
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim objTransactionFile As TransactionFile
        Dim strSQL As String

        Try

            ' Remove the Transaction from the collection...
            objTransactionFile = New TransactionFile
            For Each objTransactionFile In Me
                If FileID = objTransactionFile.FileID Then
                    Me.List.Remove(objTransactionFile)
                    Exit For
                End If
            Next

            ' Build delete query and execution type...
            strSQL = _
                    "DELETE FROM TransactionFiles " & _
                    "WHERE " & _
                    "FileID = '" & FileID.ToString & "'"

            ' Remove the transaction from the database...
            If dbMgr.TransactSql(strSQL, DELETE) > 0 Then
                ' Return true to caller...
                RemoveTransactionFile = True
            Else
                ' Return false to caller...
                RemoveTransactionFile = False
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Clean-up...
        End Try

    End Function

    Public Sub FillCollection()
        Dim dbMgr As Utilities.DBManager
        Dim drTransactionFile As DataRow
        Dim myTransactionFile As TransactionFile
        Dim strSQL As String
        Dim dsTransactionFiles As DataSet = Nothing

        Try
            Me.List.Clear()

            ' Build the select query...
            strSQL = "SELECT FileID FROM TransactionFiles"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsTransactionFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)

            ' Insert each user into the user list collection...
            For Each drTransactionFile In dsTransactionFiles.Tables(0).Rows

                myTransactionFile = GetFile(drTransactionFile("FileID"))

                ' Add file to list...
                Me.List.Add(myTransactionFile)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsTransactionFiles Is Nothing) Then
                dsTransactionFiles.Dispose()
            End If
        End Try


    End Sub
    Public Sub FillCollection(FileSourceType As Integer, FileStatus As Int16)
        Dim dbMgr As Utilities.DBManager
        Dim drTransactionFile As DataRow
        Dim myTransactionFile As TransactionFile
        Dim strSQL As String
        Dim dsTransactionFiles As DataSet = Nothing

        Try
            Me.List.Clear()

            ' Build the select query...
            strSQL = "SELECT FileID FROM TransactionFiles where Status = " & FileStatus.ToString & " and SourceType = " & FileSourceType.ToString

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsTransactionFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)

            ' Insert each user into the user list collection...
            For Each drTransactionFile In dsTransactionFiles.Tables(0).Rows

                myTransactionFile = GetFile(drTransactionFile("FileID"))

                ' Add file to list...
                Me.List.Add(myTransactionFile)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsTransactionFiles Is Nothing) Then
                dsTransactionFiles.Dispose()
            End If
        End Try


    End Sub

    Public Sub FillCollectionWithLast(ByVal NumberOfRows As Integer, Optional FileSourceType As Integer = -1, Optional UserID As String = "00000000-0000-0000-0000-000000000000")

        Dim dbMgr As Utilities.DBManager
        Dim drReturnFile As DataRow
        Dim myReturnFile As TransactionFile
        Dim strSQL As String
        Dim dsReturnFiles As DataSet = Nothing

        Try


            Dim WhereClause As String = ""
            If UserID <> "00000000-0000-0000-0000-000000000000" Then
                WhereClause = " b.WebUserID = '" & UserID & "' "
            End If
            Dim OrderByClause As String = " ORDER BY TimeReceived DESC"

            'Originally, SourceType was not in the table. Years later, we added the SourceType column and used the value of 2 to indicate WebUploaded files
            If FileSourceType = -1 Then
                ' Build the select query...
                strSQL = "SELECT TOP (" & NumberOfRows.ToString() & _
                    ") FileID FROM TransactionFiles a left outer join Merchants b on a.MerchantID = b.MerchantID "
                If WhereClause <> "" Then
                    strSQL = strSQL & " where " & WhereClause
                End If
            Else
                ' Build the select query...
                strSQL = "SELECT TOP (" & NumberOfRows.ToString() & _
                    ") FileID FROM TransactionFiles a left outer join Merchants b on a.MerchantID = b.MerchantID where a.SourceType = " & FileSourceType.ToString
                If WhereClause <> "" Then
                    strSQL = strSQL & " and " & WhereClause
                End If
            End If
            strSQL = strSQL & OrderByClause

            dbMgr = New Utilities.DBManager(APP_TYPE)


            ' Retrieve the rows from the database or from cache

            'If Not cTheCache Is Nothing Then
            '    If Not cTheCache(UserID) Is Nothing Then
            '        dsReturnFiles = cTheCache(UserID)
            '    Else
            '        dsReturnFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)

            '    End If
            'Else
            '    dsReturnFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)
            'End If

            dsReturnFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)
            Me.List.Clear()

            ' Insert each user into the user list collection...
            For Each drReturnFile In dsReturnFiles.Tables(0).Rows

                myReturnFile = GetFile(drReturnFile("FileID"))

                ' Add file to list...
                Me.List.Add(myReturnFile)
            Next


            'Dim SqlDep As SqlCacheDependency

            'If Not cTheCache Is Nothing Then
            '    If cTheCache(UserID) Is Nothing Then
            '        Try
            '            Dim cacheSql As String = "select top (" & NumberOfRows.ToString() & ") FileID, Status from " & _
            '                "TransactionFiles a left outer join Merchants b on a.MerchantID = b.MerchantID "
            '            If WhereClause <> "" Then
            '                cacheSql = cacheSql & " where " & WhereClause
            '            End If
            '            Dim cacheCommand As New SqlCommand(cacheSql, dbMgr.GetConnection)

            '            'I did this on the database to get SqlDependency.Start to work ...
            '            'alter database ACHAcquire set enable_broker WITH ROLLBACK IMMEDIATE
            '            Dim CS As String = System.Configuration.ConfigurationManager.ConnectionStrings("ACHAcquireConnectionString").ToString()

            '            SqlDependency.Start(CS)
            '            Dim DS As New DataSet
            '            Dim Adapt As New SqlDataAdapter(cacheCommand)
            '            Adapt.Fill(DS)
            '            SqlDep = New SqlCacheDependency(cacheCommand)


            '        Catch exDBDis As DatabaseNotEnabledForNotificationException
            '            Try
            '                SqlCacheDependencyAdmin.EnableNotifications(dbMgr.GetConnection.ConnectionString)

            '                ' If the database does not have permissions set for creating tables,
            '                ' the UnauthorizedAccessException is thrown. Handle it by redirecting
            '                ' to an error page.
            '            Catch exPerm As UnauthorizedAccessException
            '                Throw exPerm
            '            End Try

            '        Catch exTabDis As TableNotEnabledForNotificationException
            '            Try
            '                SqlCacheDependencyAdmin.EnableTableForNotifications(dbMgr.GetConnection.ConnectionString, "TransactionFiles")

            '                ' If a SqlException is thrown, redirect to an error page.
            '            Catch exc As SqlException
            '                Throw exc
            '            End Try

            '        Finally
            '            cTheCache.Insert(UserID, dsReturnFiles, SqlDep, System.Web.Caching.Cache.NoAbsoluteExpiration, New System.TimeSpan(0, 1, 0))

            '        End Try

            '    End If
            'End If


        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsReturnFiles Is Nothing) Then
                dsReturnFiles.Dispose()
            End If
        End Try
    End Sub

    Public Function GetDataset() As DataSet

        Dim MyDataset As New DataSet
        Dim MyTable As New DataTable("TransactionFiles")

        MyTable.Columns.Add("FileID", GetType(Guid))
        MyTable.Columns.Add("Date", GetType(DateTime))
        MyTable.Columns.Add("MerchantID", GetType(Guid))
        MyTable.Columns.Add("Name", GetType(String))
        MyTable.Columns.Add("Received", GetType(DateTime))
        MyTable.Columns.Add("Size", GetType(Long))
        'MyTable.Columns.Add("Status", GetType(Integer))

        Dim TheRow(5) As Object
        Dim TF As TransactionFile
        For Each TF In Me
            TheRow(0) = TF.FileID
            TheRow(1) = TF.FileDate
            TheRow(2) = TF.MerchantID
            TheRow(3) = TF.FileName
            TheRow(4) = TF.TimeReceived
            TheRow(5) = TF.FileSize
            'TheRow(6) = TF.FileStatus
            MyTable.Rows.Add(TheRow)
        Next

        MyDataset.Tables.Add(MyTable)
        Return MyDataset

    End Function

    Public Function GetDatasetForGridNoParams() As DataSet
        Return GetDatasetForGrid()
    End Function

    Public Function GetDatasetForGrid(Optional FileSourceType As Integer = -1, Optional UserID As String = "00000000-0000-0000-0000-000000000000") As DataSet

        Call FillCollectionWithLast(250, FileSourceType, UserID)

        Dim MyDataset As New DataSet
        Dim MyTable As New DataTable("TransactionFiles")

        MyTable.Columns.Add("FileID", GetType(Guid))
        MyTable.Columns.Add("FileDate", GetType(DateTime))
        MyTable.Columns.Add("MerchantID", GetType(Guid))
        MyTable.Columns.Add("FileName", GetType(String))
        MyTable.Columns.Add("TimeReceived", GetType(DateTime))
        MyTable.Columns.Add("FileSize", GetType(Long))
        MyTable.Columns.Add("Status", GetType(String))
        MyTable.Columns.Add("RejectReason", GetType(String))
        MyTable.Columns.Add("UserName", GetType(String))
        MyTable.Columns.Add("StatusChangeDate", GetType(DateTime))

        'MyTable.Columns.Add("Status", GetType(Integer))

        Dim TheRow(9) As Object
        Dim TF As TransactionFile
        For Each TF In Me
            TheRow(0) = TF.FileID
            TheRow(1) = TF.FileDate
            TheRow(2) = TF.MerchantID
            TheRow(3) = System.IO.Path.GetFileName(TF.FileName)
            TheRow(4) = TF.TimeReceived
            TheRow(5) = TF.FileSize
            Select Case TF.Status
                Case 0
                    TheRow(6) = "Downloaded"
                Case 1
                    TheRow(6) = "Processed"
                Case 2
                    TheRow(6) = "Rejected"
                Case 3
                    TheRow(6) = "Processed with errors"
                Case 4
                    TheRow(6) = "Manual batch"
                Case 5
                    TheRow(6) = "Uploaded"
                Case 6
                    TheRow(6) = "Processing"
                Case 7
                    TheRow(6) = "Ready"
                Case 8
                    TheRow(6) = "Suspended"
                Case 9
                    TheRow(6) = "Deleted"
                Case 10
                    TheRow(6) = "Approved"
                Case 11
                    TheRow(6) = "Error"
                Case 12
                    TheRow(6) = "Approved In Process"
                Case Else
                    TheRow(6) = "Unknown"
            End Select
            TheRow(7) = TF.RejectReason


            Dim TheMerchant As New Merchant(TF.MerchantID)
            Dim TheWebUser As WebUser = TheMerchant.WebUser
            Dim TheUser As String = ""
            If TheWebUser.WebUserID = Guid.Empty Then
                TheUser = "Unknown"
            Else
                TheUser = TheWebUser.ASPMembership.UserName
            End If

            TheRow(8) = TheUser
            TheRow(9) = TF.StatusChangeDate

            MyTable.Rows.Add(TheRow)
        Next

        'We don't need the collection anymore ...
        Me.List.Clear()

        MyDataset.Tables.Add(MyTable)
        Return MyDataset

    End Function

    Public Function GetUploadedNachaFileSearchResultsCount(UserID As String, StartDate As Date, EndDate As Date, _
            FileID As String) As Integer

        Dim NumberOfRows As Long = 500

        Dim dbMgr As Utilities.DBManager
        Dim strSQL As String
        Dim dsReturnFiles As DataSet = Nothing
        Dim TotalNumberOfRows As Integer
        TotalNumberOfRows = 0

        Try
            Dim WhereClause As String = ""
            If UserID <> "00000000-0000-0000-0000-000000000000" Then
                WhereClause = " b.WebUserID = '" & UserID & "' "
            End If
            Dim OrderByClause As String = " ORDER BY TimeReceived DESC"

            ' Build the select query...
            strSQL = "SELECT TOP (" & NumberOfRows.ToString() & _
                ") FileID FROM TransactionFiles a, Merchants b where a.MerchantID = b.MerchantID and a.SourceType = 2"      '2=Web uploaded NACHA files only
            If WhereClause <> "" Then
                strSQL = strSQL & " and " & WhereClause
            End If
            If FileID = "00000000-0000-0000-0000-000000000000" Then
                strSQL = strSQL & " and a.TimeReceived >= '" & StartDate.ToShortDateString & "' and a.TimeReceived < '" & EndDate.AddDays(1).ToShortDateString & "' " & _
                    " and a.Status = 1"     '1=Processed files only
            Else
                strSQL = strSQL & " and a.FileID = '" & FileID & "' and a.Status = 1"
            End If
            strSQL = strSQL & OrderByClause

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsReturnFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)

            TotalNumberOfRows = CInt(dsReturnFiles.Tables(0).Rows.Count)

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsReturnFiles Is Nothing) Then
                dsReturnFiles.Dispose()
            End If
        End Try

        Return TotalNumberOfRows

    End Function

    Public Function GetUploadedNachaFileSearchResults(UserID As String, StartDate As Date, EndDate As Date, _
            FileID As String, StartRow As Long, MaximumRows As Long) As DataSet
        'Call FillCollectionWithLast(500, 2, UserID)
        Dim NumberOfRows As Long = 500

        Dim dbMgr As Utilities.DBManager
        Dim drTransactionFile As DataRow
        Dim myTransactionFile As TransactionFile
        Dim strSQL As String
        Dim dsTransactionFiles As DataSet = Nothing

        Try
            Dim WhereClause As String = ""
            If UserID <> "00000000-0000-0000-0000-000000000000" Then
                WhereClause = " b.WebUserID = '" & UserID & "' "
            End If
            Dim OrderByClause As String = " ORDER BY TimeReceived DESC"

            ' Build the select query...
            strSQL = "SELECT TOP (" & NumberOfRows.ToString() & _
                ") FileID FROM TransactionFiles a, Merchants b where a.MerchantID = b.MerchantID and a.SourceType = 2"      '2=Web uploaded NACHA files only
            If WhereClause <> "" Then
                strSQL = strSQL & " and " & WhereClause
            End If
            If FileID = "00000000-0000-0000-0000-000000000000" Then
                strSQL = strSQL & " and a.TimeReceived >= '" & StartDate.ToShortDateString & "' and a.TimeReceived < '" & EndDate.AddDays(1).ToShortDateString & "' " & _
                    " and a.Status = 1"     '1=Processed files only
            Else
                strSQL = strSQL & " and a.FileID = '" & FileID & "' and a.Status = 1"
            End If
            strSQL = strSQL & OrderByClause

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsTransactionFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)

            Me.List.Clear()

            ' Insert each record into the collection...
            Dim RowCounter As Long = 0
            Dim RowsCollected As Long = 0

            For Each drTransactionFile In dsTransactionFiles.Tables(0).Rows
                RowCounter = RowCounter + 1

                If RowCounter >= StartRow + 1 Then
                    myTransactionFile = GetFile(drTransactionFile("FileID"))
                    Me.List.Add(myTransactionFile)
                    RowsCollected = RowsCollected + 1
                End If

                If RowsCollected = MaximumRows Then Exit For
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsTransactionFiles Is Nothing) Then
                dsTransactionFiles.Dispose()
            End If
        End Try

        Dim MyDataset As New DataSet
        Dim MyTable As New DataTable("TransactionFiles")

        MyTable.Columns.Add("FileID", GetType(Guid))
        MyTable.Columns.Add("FileDate", GetType(DateTime))
        MyTable.Columns.Add("MerchantID", GetType(Guid))
        MyTable.Columns.Add("FileName", GetType(String))
        MyTable.Columns.Add("TimeReceived", GetType(DateTime))
        MyTable.Columns.Add("FileSize", GetType(Long))
        MyTable.Columns.Add("Status", GetType(String))
        MyTable.Columns.Add("RejectReason", GetType(String))
        MyTable.Columns.Add("UserName", GetType(String))
        MyTable.Columns.Add("StatusChangeDate", GetType(DateTime))

        'MyTable.Columns.Add("Status", GetType(Integer))

        Dim TheRow(9) As Object
        Dim TF As TransactionFile
        For Each TF In Me
            TheRow(0) = TF.FileID
            TheRow(1) = TF.FileDate
            TheRow(2) = TF.MerchantID
            TheRow(3) = System.IO.Path.GetFileName(TF.FileName)
            TheRow(4) = TF.TimeReceived
            TheRow(5) = TF.FileSize
            Select Case TF.Status
                Case 0
                    TheRow(6) = "Downloaded"
                Case 1
                    TheRow(6) = "Processed"
                Case 2
                    TheRow(6) = "Rejected"
                Case 3
                    TheRow(6) = "Processed with errors"
                Case 4
                    TheRow(6) = "Manual batch"
                Case 5
                    TheRow(6) = "Uploaded"
                Case 6
                    TheRow(6) = "Processing"
                Case 7
                    TheRow(6) = "Ready"
                Case 8
                    TheRow(6) = "Suspended"
                Case 9
                    TheRow(6) = "Deleted"
                Case 10
                    TheRow(6) = "Approved"
                Case 11
                    TheRow(6) = "Error"
                Case 12
                    TheRow(6) = "Approved In Process"
                Case Else
                    TheRow(6) = "Unknown"
            End Select
            TheRow(7) = TF.RejectReason

            Dim TheMerchant As New Merchant(TF.MerchantID)
            Dim TheWebUser As WebUser = TheMerchant.WebUser
            Dim TheUser As String = ""
            If TheWebUser.WebUserID = Guid.Empty Then
                TheUser = "Unknown"
            Else
                TheUser = TheWebUser.ASPMembership.UserName
            End If

            TheRow(8) = TheUser
            TheRow(9) = TF.StatusChangeDate

            MyTable.Rows.Add(TheRow)
        Next

        MyDataset.Tables.Add(MyTable)
        Return MyDataset
    End Function
    Public Function GetUploadedNachaBatchSearchResults(UserID As String, StartDate As Date, EndDate As Date, CompanyName As String, _
            CompanyID As String, Optional FileID As String = "00000000-0000-0000-0000-000000000000", Optional BatchID As String = "00000000-0000-0000-0000-000000000000") As DataTable

        Dim NumberOfRows As Long = 500

        Dim dbMgr As Utilities.DBManager
        Dim strSQL As String
        Dim dsReturnFiles As DataSet = Nothing
        Dim returnTable As DataTable = Nothing

        Try

            ' Build the select query...
            strSQL = "SELECT TOP (" & NumberOfRows.ToString() & _
                ") a.FileID, c.BatchID, a.FileDate, a.MerchantID, a.FileName, a.TimeReceived, a.FileSize, d.Description as Status, e.LoginID as UserName, " & _
                "c.FileLineNumber, c.BatchNumber, c.TotalDebitAmount, c.TotalCreditAmount, c.TotalDebitCount, c.TotalCreditCount, c.ServiceClassCode, " & _
                "c.CompanyName, c.CompanyDiscretionaryData, c.CompanyID, c.CompanyEntryDescription, c.CompanyDescriptiveDate, c.EffectiveEntryDate, c.SettlementDate " & _
                "FROM TransactionFiles a, Merchants b, NachaBatchSummaryInfo c, TransactionFileStatuses d, ASPMembershipWebUser e " & _
                "where a.MerchantID = b.MerchantID and a.SourceType = 2 and c.FileID = a.FileID and a.Status=d.StatusID and e.WebUserID = b.WebUserID"      '2=Web uploaded NACHA files only

            If UserID <> "00000000-0000-0000-0000-000000000000" Then
                strSQL = strSQL & " and b.WebUserID = '" & UserID & "' "
            End If

            If FileID <> "00000000-0000-0000-0000-000000000000" Then
                'If we have a FileID then nothing else matters
                strSQL = strSQL & " and a.FileID ='" & FileID & "' and a.Status = 1"
            Else
                If BatchID <> "00000000-0000-0000-0000-000000000000" Then
                    'If we have a BatchID then nothing else matters
                    strSQL = strSQL & " and c.BatchID ='" & BatchID & "' and a.Status = 1"
                Else

                    If CompanyID <> "" Then
                        strSQL = strSQL & " and c.CompanyID like '%" & SafeApos(CompanyID) & "%' "
                    End If

                    If CompanyName <> "" Then
                        strSQL = strSQL & " and c.CompanyName like '%" & SafeApos(CompanyName) & "%' "
                    End If

                    strSQL = strSQL & " and a.TimeReceived >= '" & StartDate.ToShortDateString & "' and a.TimeReceived < '" & EndDate.AddDays(1).ToShortDateString & "' " & _
                    " and a.Status = 1"     '1=Processed files only
                End If

            End If

            Dim OrderByClause As String = " ORDER BY TimeReceived DESC, a.FileName, c.BatchNumber"
            strSQL = strSQL & OrderByClause

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsReturnFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)
            returnTable = dsReturnFiles.Tables(0)

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsReturnFiles Is Nothing) Then
                dsReturnFiles.Dispose()
            End If
        End Try

        Return returnTable

    End Function

    Public Function GetUploadedNachaTransactionSearchResultsCount(UserID As String, StartDate As Date, EndDate As Date, _
        CompanyName As String, CompanyID As String, AccountNumber As String, _
        Name As String, Optional BatchID As String = "00000000-0000-0000-0000-000000000000", Optional FileID As String = "00000000-0000-0000-0000-000000000000") As Integer

        Dim NumberOfRows As Long = 5000

        Dim dbMgr As Utilities.DBManager
        Dim strSQL As String
        Dim dsReturnFiles As DataSet = Nothing
        Dim returnTable As DataTable = Nothing
        Dim ReturnCount As Integer

        Try

            ' Build the select query...
            strSQL = "SELECT TOP (" & NumberOfRows.ToString() & _
                ") a.FileID, c.BatchID, a.FileDate, a.MerchantID, a.FileName, " & _
                "a.TimeReceived, a.FileSize, d.Description as Status, e.LoginID as UserName, " & _
                "c.FileLineNumber, c.BatchNumber, c.EffectiveEntryDate, c.CompanyName, " & _
                "c.TotalDebitAmount, c.TotalCreditAmount, c.TotalDebitCount, c.TotalCreditCount, " & _
                "c.CompanyDiscretionaryData, c.CompanyID, f.FileLineNumber, g.TransactionStatus, " & _
                "i.Description as TransactionStatusDescription, g.TransactionType, g.PrenoteFlag, " & _
                    "case when g.PrenoteFlag=0 then j.Description else " & _
                        "(case when g.TransactionType=1 then 'Prenote CR' else 'Prenote DR' end) end as TransactionTypeDescription, " & _
                "g.TransactionAmount, g.MerchantCustomerID, g.LastName, g.MerchantTransactionID, g.TransactionTime, g.AccountNumber, " & _
                "g.RoutingNumber, g.AccountType, k.Description as AccountTypeDescription, g.ReceivedDate, g.StatusChangeDate, " & _
                "g.ODFIFileID, h.FileDate as ODFIFileDate, g.TransactionID " & _
                "" & _
                "FROM TransactionFiles a, Merchants b, NachaBatchSummaryInfo c, TransactionFileStatuses d, " & _
                "ASPMembershipWebUser e, NACHAEntryDetailRecords f, TransactionStatuses i, " & _
                "TransactionTypes j, AccountTypes k, Transactions g " & _
                "left outer join OutgoingODFIFiles h on h.FileID = g.ODFIFileID " & _
                "" & _
                "where a.MerchantID = b.MerchantID and a.SourceType = 2 and c.FileID = a.FileID and " & _
                "a.Status = d.StatusID and e.WebUserID = b.WebUserID and f.BatchID = c.BatchID and f.FileID = a.FileID and " & _
                "f.BatchID = c.BatchID and g.FileID = a.FileID and g.MerchantTransactionID = f.TraceNumber and " & _
                "g.LastName = f.ReceiverName and g.TransactionAmount = f.Amount and " & _
                "g.TransactionStatus = i.TransactionStatusID and g.TransactionType = j.TransactionTypeID and " & _
                "g.AccountType = k.AccountTypeID " 'SourceType=2=Web uploaded NACHA files only

            If UserID <> "00000000-0000-0000-0000-000000000000" Then
                strSQL = strSQL & " and b.WebUserID = '" & UserID & "' "
            End If

            If FileID <> "00000000-0000-0000-0000-000000000000" Then
                strSQL = strSQL & " and a.FileID ='" & FileID & "' "   'We're going to simply get all the transactions for this file id and ignore the other criteria (except time received)
            Else
                If BatchID <> "00000000-0000-0000-0000-000000000000" Then
                    strSQL = strSQL & " and f.BatchID ='" & BatchID & "' "   'We're going to simply get all the transactions for this batch id and ignore the other criteria (except time received)
                Else
                    If CompanyID <> "" Then
                        strSQL = strSQL & " and c.CompanyID like '%" & SafeApos(CompanyID) & "%' "
                    End If
                    If CompanyName <> "" Then
                        strSQL = strSQL & " and c.CompanyName like '%" & SafeApos(CompanyName) & "%' "
                    End If
                    If AccountNumber <> "" Then
                        strSQL = strSQL & " and g.AccountNumber like '%" & SafeApos(AccountNumber) & "%' "
                    End If
                    If Name <> "" Then
                        strSQL = strSQL & " and g.LastName like '%" & SafeApos(Name) & "%' "
                    End If

                    strSQL = strSQL & " and a.TimeReceived >= '" & StartDate.ToShortDateString & "' and a.TimeReceived < '" & EndDate.AddDays(1).ToShortDateString & "' " & _
                        " and a.Status = 1"     '1=Processed files only

                End If
            End If

            'Dim OrderByClause As String = " ORDER BY TimeReceived DESC, a.FileName, c.BatchNumber, f.FileLineNumber"
            'strSQL = strSQL & OrderByClause

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsReturnFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)
            returnTable = dsReturnFiles.Tables(0)

            ReturnCount = returnTable.Rows.Count


        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsReturnFiles Is Nothing) Then
                dsReturnFiles.Dispose()
            End If
        End Try

        Return ReturnCount

    End Function

    Public Function GetUploadedNachaTransactionSearchResults(UserID As String, StartDate As Date, EndDate As Date, _
        CompanyName As String, CompanyID As String, AccountNumber As String, _
        Name As String, StartRow As Long, MaximumRows As Long, _
        Optional BatchID As String = "00000000-0000-0000-0000-000000000000", Optional FileID As String = "00000000-0000-0000-0000-000000000000") As DataTable

        Dim NumberOfRows As Long = 5000

        Dim dbMgr As Utilities.DBManager
        Dim strSQL As String
        Dim dsReturnFiles As DataSet = Nothing
        Dim returnTable As DataTable = Nothing
        Dim OutputTable As DataTable

        Try

            ' Build the select query...
            strSQL = "SELECT TOP (" & NumberOfRows.ToString() & _
                ") a.FileID, c.BatchID, a.FileDate, a.MerchantID, a.FileName, " & _
                "a.TimeReceived, a.FileSize, d.Description as Status, e.LoginID as UserName, " & _
                "c.FileLineNumber, c.BatchNumber, c.EffectiveEntryDate, c.CompanyName, " & _
                "c.TotalDebitAmount, c.TotalCreditAmount, c.TotalDebitCount, c.TotalCreditCount, " & _
                "c.CompanyDiscretionaryData, c.CompanyID, f.FileLineNumber, g.TransactionStatus, " & _
                "i.Description as TransactionStatusDescription, g.TransactionType, g.PrenoteFlag, " & _
                    "case when g.PrenoteFlag=0 then j.Description else " & _
                        "(case when g.TransactionType=1 then 'Prenote CR' else 'Prenote DR' end) end as TransactionTypeDescription, " & _
                "g.TransactionAmount, g.MerchantCustomerID, g.LastName, g.MerchantTransactionID, g.TransactionTime, g.AccountNumber, " & _
                "g.RoutingNumber, g.AccountType, k.Description as AccountTypeDescription, g.ReceivedDate, g.StatusChangeDate, " & _
                "g.ODFIFileID, h.FileDate as ODFIFileDate, g.TransactionID " & _
                "" & _
                "FROM TransactionFiles a, Merchants b, NachaBatchSummaryInfo c, TransactionFileStatuses d, " & _
                "ASPMembershipWebUser e, NACHAEntryDetailRecords f, TransactionStatuses i, " & _
                "TransactionTypes j, AccountTypes k, Transactions g " & _
                "left outer join OutgoingODFIFiles h on h.FileID = g.ODFIFileID " & _
                "" & _
                "where a.MerchantID = b.MerchantID and a.SourceType = 2 and c.FileID = a.FileID and " & _
                "a.Status = d.StatusID and e.WebUserID = b.WebUserID and f.BatchID = c.BatchID and f.FileID = a.FileID and " & _
                "f.BatchID = c.BatchID and g.FileID = a.FileID and g.MerchantTransactionID = f.TraceNumber and " & _
                "g.LastName = f.ReceiverName and g.TransactionAmount = f.Amount and " & _
                "g.TransactionStatus = i.TransactionStatusID and g.TransactionType = j.TransactionTypeID and " & _
                "g.AccountType = k.AccountTypeID " 'SourceType=2=Web uploaded NACHA files only

            If UserID <> "00000000-0000-0000-0000-000000000000" Then
                strSQL = strSQL & " and b.WebUserID = '" & UserID & "' "
            End If

            If FileID <> "00000000-0000-0000-0000-000000000000" Then
                strSQL = strSQL & " and a.FileID ='" & FileID & "' "   'We're going to simply get all the transactions for this file id and ignore the other criteria (except time received)
            Else
                If BatchID <> "00000000-0000-0000-0000-000000000000" Then
                    strSQL = strSQL & " and f.BatchID ='" & BatchID & "' "   'We're going to simply get all the transactions for this batch id and ignore the other criteria (except time received)
                Else
                    If CompanyID <> "" Then
                        strSQL = strSQL & " and c.CompanyID like '%" & SafeApos(CompanyID) & "%' "
                    End If
                    If CompanyName <> "" Then
                        strSQL = strSQL & " and c.CompanyName like '%" & SafeApos(CompanyName) & "%' "
                    End If
                    If AccountNumber <> "" Then
                        strSQL = strSQL & " and g.AccountNumber like '%" & SafeApos(AccountNumber) & "%' "
                    End If
                    If Name <> "" Then
                        strSQL = strSQL & " and g.LastName like '%" & SafeApos(Name) & "%' "
                    End If

                    strSQL = strSQL & " and a.TimeReceived >= '" & StartDate.ToShortDateString & "' and a.TimeReceived < '" & EndDate.AddDays(1).ToShortDateString & "' " & _
                        " and a.Status = 1"     '1=Processed files only

                End If
            End If

            Dim OrderByClause As String = " ORDER BY TimeReceived DESC, a.FileName, c.BatchNumber, f.FileLineNumber"
            strSQL = strSQL & OrderByClause

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsReturnFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)
            returnTable = dsReturnFiles.Tables(0)

            'Get only the correct subset of data ...
            Dim RowCounter As Long = 0
            Dim RowsCollected As Long = 0
            OutputTable = returnTable.Clone
            Dim drTransaction As DataRow

            For Each drTransaction In returnTable.Rows
                RowCounter = RowCounter + 1

                If RowCounter >= StartRow + 1 Then
                    OutputTable.ImportRow(drTransaction)
                    RowsCollected = RowsCollected + 1
                End If

                If RowsCollected = MaximumRows Then Exit For
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsReturnFiles Is Nothing) Then
                dsReturnFiles.Dispose()
            End If
        End Try

        Return OutputTable

    End Function

    Public Function GetUploadedNachaReturnedTransactionsCount(UserID As String, StartDate As Date, EndDate As Date) As Integer

        Dim NumberOfRows As Long = 500

        Dim dbMgr As Utilities.DBManager
        Dim strSQL As String
        Dim dsReturnFiles As DataSet = Nothing
        Dim returnTable As DataTable = Nothing
        Dim ReturnCount As Integer = 0

        Try

            ' Build the select query...
            strSQL = "SELECT TOP (" & NumberOfRows.ToString() & _
                ") a.FileID, c.BatchID, a.FileDate, a.MerchantID, a.FileName, " & _
                "a.TimeReceived, a.FileSize, d.Description as Status, e.LoginID as UserName, " & _
                "c.FileLineNumber, c.BatchNumber, c.EffectiveEntryDate, c.CompanyName, " & _
                "c.TotalDebitAmount, c.TotalCreditAmount, c.TotalDebitCount, c.TotalCreditCount, " & _
                "c.CompanyDiscretionaryData, c.CompanyID, f.FileLineNumber, g.TransactionStatus, " & _
                "i.Description as TransactionStatusDescription, g.TransactionType, g.PrenoteFlag, " & _
                    "case when g.PrenoteFlag=0 then j.Description else " & _
                        "(case when g.TransactionType=1 then 'Prenote CR' else 'Prenote DR' end) end as TransactionTypeDescription, " & _
                "g.TransactionAmount, g.MerchantCustomerID, g.LastName, g.MerchantTransactionID, g.TransactionTime, g.AccountNumber, " & _
                "g.RoutingNumber, g.AccountType, k.Description as AccountTypeDescription, g.ReceivedDate, g.StatusChangeDate, " & _
                "g.ODFIFileID, h.FileDate as ODFIFileDate, g.TransactionID, g.ReturnReasonCode, g.ReturnDate " & _
                "" & _
                "FROM TransactionFiles a, Merchants b, NachaBatchSummaryInfo c, TransactionFileStatuses d, " & _
                "ASPMembershipWebUser e, NACHAEntryDetailRecords f, TransactionStatuses i, " & _
                "TransactionTypes j, AccountTypes k, Transactions g " & _
                "left outer join OutgoingODFIFiles h on h.FileID = g.ODFIFileID " & _
                "" & _
                "where a.MerchantID = b.MerchantID and a.SourceType = 2 and c.FileID = a.FileID and " & _
                "a.Status = d.StatusID and e.WebUserID = b.WebUserID and f.BatchID = c.BatchID and f.FileID = a.FileID and " & _
                "f.BatchID = c.BatchID and g.FileID = a.FileID and g.MerchantTransactionID = f.TraceNumber and " & _
                "g.LastName = f.ReceiverName and g.TransactionAmount = f.Amount and " & _
                "g.TransactionStatus = i.TransactionStatusID and g.TransactionType = j.TransactionTypeID and " & _
                "g.AccountType = k.AccountTypeID " 'SourceType=2=Web uploaded NACHA files only

            If UserID <> "00000000-0000-0000-0000-000000000000" Then
                strSQL = strSQL & " and b.WebUserID = '" & UserID & "' "
            End If

            strSQL = strSQL & " and g.ReturnDate >= '" & StartDate.ToShortDateString & "' and g.ReturnDate < '" & EndDate.AddDays(1).ToShortDateString & "' " & _
                " and a.Status = 1 and LTRIM(RTRIM(g.ReturnReasonCode)) <> '' "     'Status=1=Processed files only


            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsReturnFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)
            returnTable = dsReturnFiles.Tables(0)
            ReturnCount = returnTable.Rows.Count

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsReturnFiles Is Nothing) Then
                dsReturnFiles.Dispose()
            End If
        End Try

        Return ReturnCount

    End Function


    Public Function GetUploadedNachaReturnedTransactions(UserID As String, StartDate As Date, EndDate As Date, StartRow As Integer, MaximumRows As Integer) As DataTable

        Dim NumberOfRows As Long = 500

        Dim dbMgr As Utilities.DBManager
        Dim strSQL As String
        Dim dsReturnFiles As DataSet = Nothing
        Dim returnTable As DataTable = Nothing
        Dim OutputTable As DataTable

        Try

            ' Build the select query...
            strSQL = "SELECT TOP (" & NumberOfRows.ToString() & _
                ") a.FileID, c.BatchID, a.FileDate, a.MerchantID, a.FileName, " & _
                "a.TimeReceived, a.FileSize, d.Description as Status, e.LoginID as UserName, " & _
                "c.FileLineNumber, c.BatchNumber, c.EffectiveEntryDate, c.CompanyName, " & _
                "c.TotalDebitAmount, c.TotalCreditAmount, c.TotalDebitCount, c.TotalCreditCount, " & _
                "c.CompanyDiscretionaryData, c.CompanyID, f.FileLineNumber, g.TransactionStatus, " & _
                "i.Description as TransactionStatusDescription, g.TransactionType, g.PrenoteFlag, " & _
                    "case when g.PrenoteFlag=0 then j.Description else " & _
                        "(case when g.TransactionType=1 then 'Prenote CR' else 'Prenote DR' end) end as TransactionTypeDescription, " & _
                "g.TransactionAmount, g.MerchantCustomerID, g.LastName, g.MerchantTransactionID, g.TransactionTime, g.AccountNumber, " & _
                "g.RoutingNumber, g.AccountType, k.Description as AccountTypeDescription, g.ReceivedDate, g.StatusChangeDate, " & _
                "g.ODFIFileID, h.FileDate as ODFIFileDate, g.TransactionID, g.ReturnReasonCode, g.ReturnDate " & _
                "" & _
                "FROM TransactionFiles a, Merchants b, NachaBatchSummaryInfo c, TransactionFileStatuses d, " & _
                "ASPMembershipWebUser e, NACHAEntryDetailRecords f, TransactionStatuses i, " & _
                "TransactionTypes j, AccountTypes k, Transactions g " & _
                "left outer join OutgoingODFIFiles h on h.FileID = g.ODFIFileID " & _
                "" & _
                "where a.MerchantID = b.MerchantID and a.SourceType = 2 and c.FileID = a.FileID and " & _
                "a.Status = d.StatusID and e.WebUserID = b.WebUserID and f.BatchID = c.BatchID and f.FileID = a.FileID and " & _
                "f.BatchID = c.BatchID and g.FileID = a.FileID and g.MerchantTransactionID = f.TraceNumber and " & _
                "g.LastName = f.ReceiverName and g.TransactionAmount = f.Amount and " & _
                "g.TransactionStatus = i.TransactionStatusID and g.TransactionType = j.TransactionTypeID and " & _
                "g.AccountType = k.AccountTypeID " 'SourceType=2=Web uploaded NACHA files only

            If UserID <> "00000000-0000-0000-0000-000000000000" Then
                strSQL = strSQL & " and b.WebUserID = '" & UserID & "' "
            End If

            strSQL = strSQL & " and g.ReturnDate >= '" & StartDate.ToShortDateString & "' and g.ReturnDate < '" & EndDate.AddDays(1).ToShortDateString & "' " & _
                " and a.Status = 1 and LTRIM(RTRIM(g.ReturnReasonCode)) <> '' "     'Status=1=Processed files only

            Dim OrderByClause As String = " ORDER BY TimeReceived DESC, a.FileName, c.BatchNumber, f.FileLineNumber"
            strSQL = strSQL & OrderByClause

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsReturnFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)
            returnTable = dsReturnFiles.Tables(0)

            'Get only the correct subset of data ...
            Dim RowCounter As Long = 0
            Dim RowsCollected As Long = 0
            OutputTable = returnTable.Clone
            Dim drTransaction As DataRow

            For Each drTransaction In returnTable.Rows
                RowCounter = RowCounter + 1

                If RowCounter >= StartRow + 1 Then
                    OutputTable.ImportRow(drTransaction)
                    RowsCollected = RowsCollected + 1
                End If

                If RowsCollected = MaximumRows Then Exit For
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsReturnFiles Is Nothing) Then
                dsReturnFiles.Dispose()
            End If
        End Try

        Return OutputTable

    End Function


    Public ReadOnly Property ItemByFileID(ByVal FileID As Guid) As TransactionFile
        Get
            Dim MyTransactionFile As New TransactionFile
            ItemByFileID = Nothing

            For Each MyTransactionFile In Me
                If FileID = MyTransactionFile.FileID Then
                    ItemByFileID = MyTransactionFile
                    Exit For
                End If
            Next
        End Get
    End Property

    Public Function ChangeFileStatus(FileID As Guid, Status As Int16) As Boolean
        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim NewGuid As Guid = Guid.NewGuid
        Dim bReturn As Boolean = False

        Try

            'Create SQL ...
            sSql = "Update TransactionFiles " & _
                "Set Status = " & Status.ToString & " Where FileID = '" & FileID.ToString() & "'"

            'Process SQL transaction ...
            dbMgr.TransactSql(sSql, UPDATE)
            bReturn = True

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return bReturn
    End Function

    Public Function WebFileApprove(FileID As Guid) As Boolean
        Return ChangeFileStatus(FileID, 10)
    End Function

    Public Function WebFileSuspend(FileID As Guid) As Boolean
        Return ChangeFileStatus(FileID, 8)
    End Function

    Public Function WebFileReady(FileID As Guid) As Boolean
        Return ChangeFileStatus(FileID, 7)
    End Function

    Public Function WebFileDelete(FileID As Guid) As Boolean
        Return ChangeFileStatus(FileID, 9)
    End Function

    'Public Sub New()
    '    Try
    '        cTheCache = HttpContext.Current.Cache
    '    Catch ex As Exception

    '    End Try


    'End Sub
End Class
