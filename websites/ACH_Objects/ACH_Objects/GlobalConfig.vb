Imports Microsoft.ApplicationBlocks.ExceptionManagement

Imports Microsoft.SqlServer.Management.Smo
Imports Microsoft.SqlServer.Management.Common
Imports Microsoft.SqlServer.Management.Smo.Mail

Imports System.Configuration

Public Class GlobalConfig

#Region "Private class variables"

    'File attributes
    Private cFTPLogFileName As String
    Private cFTPServerAddress As String
    Private cFTPServerUserName As String
    Private cFTPServerPassword As String
    Private cFTPRootDirectory As String
    Private cTempFileDirectory As String

    Private cContactEmailUser As String
    Private cContactEmailPassword As String
    Private cContactSMTPServer As String
    Private cContactEmailFrom As String
    Private cContactEmailTo As String

    Private cDeleteJobs As Boolean
    Private cDatabaseMailProfileName As String

#End Region

#Region "Class attributes"

    Public Property FTPLogFileName() As String
        Get
            Return cFTPLogFileName
        End Get
        Set(ByVal value As String)
            cFTPLogFileName = value
        End Set
    End Property
    Public Property FTPServerAddress() As String
        Get
            Return cFTPServerAddress
        End Get
        Set(ByVal value As String)
            cFTPServerAddress = value
        End Set
    End Property
    Public Property FTPServerUserName() As String
        Get
            Return cFTPServerUserName
        End Get
        Set(ByVal value As String)
            cFTPServerUserName = value
        End Set
    End Property
    Public Property FTPServerPassword() As String
        Get
            Return cFTPServerPassword
        End Get
        Set(ByVal value As String)
            cFTPServerPassword = value
        End Set
    End Property
    Public Property FTPRootDirectory() As String
        Get
            Return cFTPRootDirectory
        End Get
        Set(ByVal value As String)
            cFTPRootDirectory = value
        End Set
    End Property
    Public Property TempFileDirectory() As String
        Get
            Return cTempFileDirectory
        End Get
        Set(ByVal value As String)
            cTempFileDirectory = value
        End Set
    End Property

    Public Property ContactEmailUser() As String
        Get
            Return cContactEmailUser
        End Get
        Set(ByVal value As String)
            cContactEmailUser = value
        End Set
    End Property
    Public Property ContactEmailPassword() As String
        Get
            Return cContactEmailPassword
        End Get
        Set(ByVal value As String)
            cContactEmailPassword = value
        End Set
    End Property
    Public Property ContactSMTPServer() As String
        Get
            Return cContactSMTPServer
        End Get
        Set(ByVal value As String)
            cContactSMTPServer = value
        End Set
    End Property
    Public Property ContactEmailFrom() As String
        Get
            Return cContactEmailFrom
        End Get
        Set(ByVal value As String)
            cContactEmailFrom = value
        End Set
    End Property
    Public Property ContactEmailTo() As String
        Get
            Return cContactEmailTo
        End Get
        Set(ByVal value As String)
            cContactEmailTo = value
        End Set
    End Property

    Public Property DeleteJobs() As Boolean
        Get
            Return cDeleteJobs
        End Get
        Set(ByVal value As Boolean)
            cDeleteJobs = value
        End Set
    End Property
    Public Property DatabaseMailProfileName() As String
        Get
            Return cDatabaseMailProfileName
        End Get
        Set(ByVal value As String)
            cDatabaseMailProfileName = value
        End Set
    End Property
#End Region

#Region "Public Class Methods"

    'Default constructor
    Public Sub New()
        Dim strSQL As String

        Try

            strSQL = _
               "SELECT ftplogfilename, ftpserveraddress, ftpserverusername, ftpserverpassword, " & _
                "ftprootdirectory, tempfiledirectory, " & _
                "ContactEmailUser, ContactEmailPassword, ContactSMTPServer," & _
                "ContactEmailFrom, ContactEmailTo, DeleteJobs, DatabaseMailProfileName " & _
                "from globalconfigvalues where globalconfigid = 1"

            Dim dsConfigFile As DataSet
            Dim drConfigFile As DataRow
            Dim dbMgr As New Utilities.DBManager(APP_TYPE)

            Try

                dsConfigFile = dbMgr.TransactSql(strSQL, FILL_DATASET)

                If DataSetHasRows(dsConfigFile) Then

                    drConfigFile = dsConfigFile.Tables(0).Rows(0)

                    ' Build object
                    cFTPLogFileName = SafeDBStringField(drConfigFile, "FTPLogFileName")
                    cFTPServerAddress = SafeDBStringField(drConfigFile, "FTPServerAddress")
                    cFTPServerUserName = SafeDBStringField(drConfigFile, "FTPServerUserName")
                    cFTPServerPassword = SafeDBStringField(drConfigFile, "FTPServerPassword")
                    cFTPRootDirectory = SafeDBStringField(drConfigFile, "FTPRootDirectory")
                    cTempFileDirectory = SafeDBStringField(drConfigFile, "TempFileDirectory")

                    cContactEmailUser = SafeDBStringField(drConfigFile, "ContactEmailUser")
                    cContactEmailPassword = SafeDBStringField(drConfigFile, "ContactEmailPassword")
                    cContactSMTPServer = SafeDBStringField(drConfigFile, "ContactSMTPServer")
                    cContactEmailFrom = SafeDBStringField(drConfigFile, "ContactEmailFrom")
                    cContactEmailTo = SafeDBStringField(drConfigFile, "ContactEmailTo")

                    cDeleteJobs = SafeDBBitField(drConfigFile, "DeleteJobs")

                    cDatabaseMailProfileName = SafeDBStringField(drConfigFile, "DatabaseMailProfileName")

                End If

            Catch ex As Exception
                ExceptionManager.Publish(ex)
                Throw (ex)
            Finally
            End Try

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Sub

    Public Function SendEmail(ByVal EmailTo As String, ByVal EmailSubject As String, _
        ByVal EmailBody As String) As Boolean

        Return GlobalFunctions.SendEmail(EmailTo, EmailSubject, EmailBody)

    End Function

    Public Function SendDatabaseMail(ByVal EmailTo As String, ByVal EmailSubject As String, _
    ByVal EmailBody As String) As Boolean

        'If an EmailTo address was supplied, use it, otherwise
        'send it to the globally configured email address
        Dim sToEmail As String = ""
        If EmailTo = "" Then
            EmailTo = ContactEmailTo
        End If

        Return GlobalFunctions.SendDatabaseMail(EmailTo, EmailSubject, EmailBody, DatabaseMailProfileName)

    End Function

    Public Function Update(ByVal ChangedData As GlobalConfig) As Boolean
        Dim strSQL As String
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim TheReturn As Boolean

        Try

            strSQL = _
               "UPDATE globalconfigvalues " & _
               "set FTPLogFileName = '" & SafeApos(ChangedData.FTPLogFileName) & "', " & _
               "FTPServerAddress = '" & SafeApos(ChangedData.FTPServerAddress) & "', " & _
               "FTPServerUserName = '" & SafeApos(ChangedData.FTPServerUserName) & "', " & _
               "FTPServerPassword = '" & SafeApos(ChangedData.FTPServerPassword) & "', " & _
               "FTPRootDirectory = '" & SafeApos(ChangedData.FTPRootDirectory) & "', " & _
               "TempFileDirectory = '" & SafeApos(ChangedData.TempFileDirectory) & "', " & _
               "ContactEmailUser = '" & SafeApos(ChangedData.ContactEmailUser) & "', " & _
               "ContactEmailPassword = '" & SafeApos(ChangedData.ContactEmailPassword) & "', " & _
               "ContactSMTPServer = '" & SafeApos(ChangedData.ContactSMTPServer) & "', " & _
               "ContactEmailFrom = '" & SafeApos(ChangedData.ContactEmailFrom) & "', " & _
               "ContactEmailTo = '" & SafeApos(ChangedData.ContactEmailTo) & "', " & _
               "DeleteJobs = " & CInt(ChangedData.DeleteJobs).ToString & ", " & _
               "DatabaseMailProfileName = '" & SafeApos(ChangedData.DatabaseMailProfileName) & "' " & _
               "WHERE globalconfigid = 1"

            Call dbMgr.TransactSql(strSQL, "UPDATE")

            'Now update Sql Server Database Mail configuration
            'This logic needs sysadmin role for the current database user

            Dim conn As New ServerConnection
            conn.LoginSecure = False
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("ACHAcquireConnectionString").ToString()

            Dim srv As Server
            srv = New Server(conn)

            Dim sm As SqlMail
            sm = srv.Mail

            Dim ma As MailAccount
            Dim mac As MailAccountCollection
            mac = sm.Accounts

            Dim mp As MailProfile
            Dim mpc As MailProfileCollection
            mpc = sm.Profiles

            'Make sure this mail profile (mp) exists
            mp = mpc.Item(SafeApos(ChangedData.DatabaseMailProfileName))
            If mp Is Nothing Then

                'It doesn't, so we need to add this profile to the database
                mp = New MailProfile(sm, ChangedData.DatabaseMailProfileName)
                mp.Create()
            End If

            'See if the ma (mail account) already exists
            ma = mac.Item(SafeApos(ChangedData.ContactEmailUser))

            If ma Is Nothing Then

                'The ma (mail account) doesn't exist, so we need to create it
                ma = New MailAccount(sm, SafeApos(ChangedData.ContactEmailUser), SafeApos(ChangedData.ContactEmailUser), _
                                     SafeApos(ChangedData.ContactEmailUser), SafeApos(ChangedData.ContactEmailUser))
                ma.Create()

                'Then set the server details
                Call UpdateMailServerDetails(ma, ChangedData.ContactSMTPServer, _
                    ChangedData.ContactEmailUser, ChangedData.ContactEmailPassword, 25)

                'Associate the new mail account with the existing mail profile
                Call AssociateMailAccountWithMailProfile(mp, SafeApos(ChangedData.ContactEmailUser))

            Else
                'The account already exists, so let's update it's server details
                Call UpdateMailServerDetails(ma, ChangedData.ContactSMTPServer, _
                    ChangedData.ContactEmailUser, ChangedData.ContactEmailPassword, 25)

                'Now let's make sure this account is in this profile
                Dim ProfileNames() As String = ma.GetAccountProfileNames()
                If Array.IndexOf(ProfileNames, SafeApos(ChangedData.DatabaseMailProfileName)) = -1 Then

                    'It's not, so we need to associate the existing mail account with this existing profile
                    Call AssociateMailAccountWithMailProfile(mp, SafeApos(ChangedData.ContactEmailUser))

                End If
            End If

            TheReturn = True
        Catch ex As Exception
            TheReturn = False
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

        Return TheReturn
    End Function

    Private Sub AssociateMailAccountWithMailProfile(mp As MailProfile, MailAccountName As String)

        'Remove all previous mail account associations from this mail profile
        'Sql Server allows multiple mail accounts per profile, but our purposes we're going to limit it to one
        For Each dr As DataRow In mp.EnumAccounts.Rows
            mp.RemoveAccount(dr.ItemArray(2).ToString())
        Next
        mp.Alter()

        'Then, associate this mail account with this mp (mail profile)
        mp.AddAccount(MailAccountName, 1)
        mp.Alter()

    End Sub
    Private Sub UpdateMailServerDetails(ma As MailAccount, ServerName As String, UserName As String, Password As String, Port As Int16)
        Dim ms As MailServer = ma.MailServers(0)
        ms.Rename(SafeApos(ServerName))
        ms.SetAccount(SafeApos(UserName), SafeApos(Password))
        ms.Port = Port
        ms.Alter()
    End Sub
    Public Function GetData() As GlobalConfig

        Return Me

    End Function

#End Region

End Class
