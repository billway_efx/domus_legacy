Imports Microsoft.ApplicationBlocks.ExceptionManagement

Public Class ReturnFileList

    Inherits CollectionBase

    Public Function AddFile(ByVal ReturnFile As ReturnFile) As Guid
        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim NewGuid As Guid = Guid.NewGuid

        Try
            'Add the new Return to the collection
            ReturnFile.FileID = NewGuid
            Me.List.Add(ReturnFile)

            'Create SQL ...
            sSql = "INSERT INTO ReturnFiles " & _
                "(FileID, FileDate, MerchantID, " & _
                "FileName, TimeReceived, FileSize, Status) " & _
                "VALUES ('" & _
                ReturnFile.FileID.ToString & "', '" & _
                ReturnFile.FileDate & "', '" & _
                ReturnFile.FileName & "', '" & _
                ReturnFile.TimeReceived.ToString & "', " & _
                ReturnFile.FileSize.ToString & ", " & _
                ReturnFile.FileStatus.ToString() & ")"

            'Process SQL Return ...
            dbMgr.TransactSql(sSql, INSERT)

        Catch ex As Exception
            NewGuid = Guid.Empty
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return NewGuid
    End Function

    Public Function UpdateFile(ByVal ReturnFile As ReturnFile) As Boolean
        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim NewGuid As Guid = Guid.NewGuid
        Dim bReturn As Boolean = False

        Try

            'Create SQL ...
            sSql = "Update ReturnFiles " & _
                "Set FileDate = '" & ReturnFile.FileDate.ToString() & "', " & _
                "FileName = '" & ReturnFile.FileName & "', " & _
                "TimeReceived = '" & ReturnFile.TimeReceived & "', " & _
                "FileSize = " & ReturnFile.FileSize.ToString() & ", " & _
                "Status = " & ReturnFile.FileStatus.ToString() & _
                " Where FileID = '" & ReturnFile.FileID.ToString() & "'"

            'Process SQL Return ...
            dbMgr.TransactSql(sSql, UPDATE)
            bReturn = True

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return bReturn
    End Function

    Public Function GetFile(ByVal FileID As Guid) As ReturnFile
        Dim strSQL As String
        Dim objReturnFile As New ReturnFile

        Try

            strSQL = _
               "SELECT FileID, FileDate, FileName, TimeReceived, FileSize, Status FROM ReturnFiles " & _
               "WHERE FileID = '" & FileID.ToString & "'"

            objReturnFile = GetReturnFileHelper(strSQL)
            Return objReturnFile

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Private Function GetReturnFileHelper(ByVal TheSql As String) As ReturnFile
        Dim dsReturnFile As DataSet
        Dim drReturnFile As DataRow
        Dim objReturnFile As New ReturnFile
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)

        Try

            dsReturnFile = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(dsReturnFile) Then

                drReturnFile = dsReturnFile.Tables(0).Rows(0)

                ' Build ReturnFile object
                objReturnFile.FileID = SafeDBGuidField(drReturnFile, "FileID")
                objReturnFile.FileDate = SafeDBDateTimeField(drReturnFile, "FileDate")
                objReturnFile.FileName = SafeDBStringField(drReturnFile, "FileName")
                objReturnFile.TimeReceived = SafeDBDateTimeField(drReturnFile, "TimeReceived")
                objReturnFile.FileSize = SafeDBBigIntField(drReturnFile, "FileSize")
                objReturnFile.FileStatus = SafeDBIntField(drReturnFile, "Status")
            End If

            Return objReturnFile

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function RemoveReturnFile(ByVal FileID As Guid) As Boolean
        ' Error indicator...
        Dim errIndicator As Int16 = 0
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim objReturnFile As ReturnFile
        Dim strSQL As String

        Try

            ' Remove the Return from the collection...
            objReturnFile = New ReturnFile
            For Each objReturnFile In Me
                If FileID = objReturnFile.FileID Then
                    Me.List.Remove(objReturnFile)
                    Exit For
                End If
            Next

            ' Build delete query and execution type...
            strSQL = _
                    "DELETE FROM ReturnFiles " & _
                    "WHERE " & _
                    "FileID = '" & FileID.ToString & "'"

            ' Remove the Return from the database...
            If dbMgr.TransactSql(strSQL, DELETE) > 0 Then
                ' Return true to caller...
                RemoveReturnFile = True
            Else
                ' Return false to caller...
                RemoveReturnFile = False
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Clean-up...
        End Try

    End Function

    Public Sub FillCollection()
        Dim dbMgr As Utilities.DBManager
        Dim drReturnFile As DataRow
        Dim myReturnFile As ReturnFile
        Dim strSQL As String
        Dim dsReturnFiles As DataSet = Nothing

        Try

            ' Build the select query...
            strSQL = "SELECT FileID FROM ReturnFiles"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsReturnFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)

            ' Insert each user into the user list collection...
            For Each drReturnFile In dsReturnFiles.Tables(0).Rows

                myReturnFile = GetFile(drReturnFile("FileID"))

                ' Add file to list...
                Me.List.Add(myReturnFile)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsReturnFiles Is Nothing) Then
                dsReturnFiles.Dispose()
            End If
        End Try


    End Sub

    Public Sub FillCollectionWithRecent(ByVal Hours As Double)
        Dim dbMgr As Utilities.DBManager
        Dim drReturnFile As DataRow
        Dim myReturnFile As ReturnFile
        Dim strSQL As String
        Dim dsReturnFiles As DataSet = Nothing

        Try

            ' Build the select query...
            strSQL = "SELECT FileID FROM ReturnFiles " & _
                "WHERE (DATEADD(hour, " & Hours.ToString() & ", TimeReceived) >= GETDATE()) " & _
                "ORDER BY TimeReceived Desc"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsReturnFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)

            ' Insert each user into the return file list collection...
            For Each drReturnFile In dsReturnFiles.Tables(0).Rows

                myReturnFile = GetFile(drReturnFile("FileID"))

                ' Add file to list...
                Me.List.Add(myReturnFile)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsReturnFiles Is Nothing) Then
                dsReturnFiles.Dispose()
            End If
        End Try
    End Sub
    Public Sub FillCollectionWithLast(ByVal Records As Integer)
        Dim dbMgr As Utilities.DBManager
        Dim drReturnFile As DataRow
        Dim myReturnFile As ReturnFile
        Dim strSQL As String
        Dim dsReturnFiles As DataSet = Nothing

        Try

            ' Build the select query...
            strSQL = "SELECT TOP (" & Records.ToString() & _
                ") FileID FROM ReturnFiles ORDER BY TimeReceived DESC"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsReturnFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)

            ' Insert each user into the user list collection...
            For Each drReturnFile In dsReturnFiles.Tables(0).Rows

                myReturnFile = GetFile(drReturnFile("FileID"))

                ' Add file to list...
                Me.List.Add(myReturnFile)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsReturnFiles Is Nothing) Then
                dsReturnFiles.Dispose()
            End If
        End Try
    End Sub

    Public Function GetDataset() As DataSet

        Dim MyDataset As New DataSet
        Dim MyTable As New DataTable("ReturnFiles")

        MyTable.Columns.Add("FileID", GetType(Guid))
        MyTable.Columns.Add("Date", GetType(DateTime))
        MyTable.Columns.Add("Name", GetType(String))
        MyTable.Columns.Add("Received", GetType(DateTime))
        MyTable.Columns.Add("Size", GetType(Long))
        MyTable.Columns.Add("Status", GetType(Integer))

        Dim TheRow(5) As Object
        Dim RF As ReturnFile
        For Each RF In Me
            TheRow(0) = RF.FileID
            TheRow(1) = RF.FileDate
            TheRow(2) = RF.FileName
            TheRow(3) = RF.TimeReceived
            TheRow(4) = RF.FileSize
            TheRow(5) = RF.FileStatus
            MyTable.Rows.Add(TheRow)
        Next

        MyDataset.Tables.Add(MyTable)
        Return MyDataset

    End Function

    Public ReadOnly Property ItemByFileID(ByVal FileID As Guid) As ReturnFile
        Get
            Dim MyReturnFile As New ReturnFile
            ItemByFileID = Nothing

            For Each MyReturnFile In Me
                If FileID = MyReturnFile.FileID Then
                    ItemByFileID = MyReturnFile
                    Exit For
                End If
            Next
        End Get
    End Property
End Class
