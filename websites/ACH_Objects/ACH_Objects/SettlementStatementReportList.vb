Imports Microsoft.ApplicationBlocks.ExceptionManagement

Public Class SettlementStatementReportList

    Inherits CollectionBase

    Public Function GetSettlementStatement(ByVal ReportDataID As Guid) As SettlementStatementReport
        Dim strSQL As String
        Dim objReturnData As New SettlementStatementReport

        Try

            strSQL = _
               "SELECT * FROM SettlementStatementReportData " & _
               "WHERE ReportDataID = '" & ReportDataID.ToString & "'"

            objReturnData = GetSettlementStatementHelper(strSQL)
            Return objReturnData

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Private Function GetSettlementStatementHelper(ByVal TheSql As String) As SettlementStatementReport
        Dim dsReturnData As DataSet
        Dim drReturnData As DataRow
        Dim objReturnData As New SettlementStatementReport
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)

        Try

            dsReturnData = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(dsReturnData) Then

                drReturnData = dsReturnData.Tables(0).Rows(0)

                ' Build ReturnData object
                objReturnData.ReportDataID = SafeDBGuidField(drReturnData, "ReportDataID")
                objReturnData.MerchantID = SafeDBGuidField(drReturnData, "MerchantID")
                objReturnData.UserID = SafeDBStringField(drReturnData, "UserID")
                objReturnData.MerchantName = SafeDBStringField(drReturnData, "MerchantName")
                objReturnData.DateFrom = SafeDBDateField(drReturnData, "DateFrom")
                objReturnData.DateTo = SafeDBDateField(drReturnData, "DateTo")
                objReturnData.DrSubCount = SafeDBBigIntField(drReturnData, "DrSubCount")
                objReturnData.DrSubAmount = SafeDBMoneyField(drReturnData, "DrSubAmount")
                objReturnData.DrSubFees = SafeDBMoneyField(drReturnData, "DrSubFees")
                objReturnData.DrSubTotalFees = SafeDBMoneyField(drReturnData, "DrSubTotalFees")
                objReturnData.DrRejCount = SafeDBBigIntField(drReturnData, "DrRejCount")
                objReturnData.DrRejAmount = SafeDBMoneyField(drReturnData, "DrRejAmount")
                objReturnData.DrRejFees = SafeDBMoneyField(drReturnData, "DrRejFees")
                objReturnData.DrRejTotalFees = SafeDBMoneyField(drReturnData, "DrRejTotalFees")
                objReturnData.DrClrCount = SafeDBBigIntField(drReturnData, "DrClrCount")
                objReturnData.DrClrAmount = SafeDBMoneyField(drReturnData, "DrClrAmount")
                objReturnData.DrClrFees = SafeDBMoneyField(drReturnData, "DrClrFees")
                objReturnData.DrClrTotalFees = SafeDBMoneyField(drReturnData, "DrClrTotalFees")
                objReturnData.DrRetCount = SafeDBBigIntField(drReturnData, "DrRetCount")
                objReturnData.DrRetAmount = SafeDBMoneyField(drReturnData, "DrRetAmount")
                objReturnData.DrRetFees = SafeDBMoneyField(drReturnData, "DrRetFees")
                objReturnData.DrRetTotalFees = SafeDBMoneyField(drReturnData, "DrRetTotalFees")
                objReturnData.DrChgCount = SafeDBBigIntField(drReturnData, "DrChgCount")
                objReturnData.DrChgAmount = SafeDBMoneyField(drReturnData, "DrChgAmount")
                objReturnData.DrChgFees = SafeDBMoneyField(drReturnData, "DrChgFees")
                objReturnData.DrChgTotalFees = SafeDBMoneyField(drReturnData, "DrChgTotalFees")
                objReturnData.CrSubCount = SafeDBBigIntField(drReturnData, "CrSubCount")
                objReturnData.CrSubAmount = SafeDBMoneyField(drReturnData, "CrSubAmount")
                objReturnData.CrSubFees = SafeDBMoneyField(drReturnData, "CrSubFees")
                objReturnData.CrSubTotalFees = SafeDBMoneyField(drReturnData, "CrSubTotalFees")
                objReturnData.CrRejCount = SafeDBBigIntField(drReturnData, "CrRejCount")
                objReturnData.CrRejAmount = SafeDBMoneyField(drReturnData, "CrRejAmount")
                objReturnData.CrRejFees = SafeDBMoneyField(drReturnData, "CrRejFees")
                objReturnData.CrRejTotalFees = SafeDBMoneyField(drReturnData, "CrRejTotalFees")
                objReturnData.CrClrCount = SafeDBBigIntField(drReturnData, "CrClrCount")
                objReturnData.CrClrAmount = SafeDBMoneyField(drReturnData, "CrClrAmount")
                objReturnData.CrClrFees = SafeDBMoneyField(drReturnData, "CrClrFees")
                objReturnData.CrClrTotalFees = SafeDBMoneyField(drReturnData, "CrClrTotalFees")
                objReturnData.CrRetCount = SafeDBBigIntField(drReturnData, "CrRetCount")
                objReturnData.CrRetAmount = SafeDBMoneyField(drReturnData, "CrRetAmount")
                objReturnData.CrRetFees = SafeDBMoneyField(drReturnData, "CrRetFees")
                objReturnData.CrRetTotalFees = SafeDBMoneyField(drReturnData, "CrRetTotalFees")
                objReturnData.CrChgCount = SafeDBBigIntField(drReturnData, "CrChgCount")
                objReturnData.CrChgAmount = SafeDBMoneyField(drReturnData, "CrChgAmount")
                objReturnData.CrChgFees = SafeDBMoneyField(drReturnData, "CrChgFees")
                objReturnData.CrChgTotalFees = SafeDBMoneyField(drReturnData, "CrChgTotalFees")
                objReturnData.Reserves = SafeDBMoneyField(drReturnData, "Reserves")
                objReturnData.ReservesReleased = SafeDBMoneyField(drReturnData, "ReservesReleased")
                objReturnData.NetSettlement = SafeDBMoneyField(drReturnData, "NetSettlement")
                objReturnData.DateCreated = SafeDBDateField(drReturnData, "DateCreated")
                objReturnData.ReservesRate = SafeDBFloatField(drReturnData, "ReservesRate")
            End If

            Return objReturnData

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function GetDataset() As DataSet

        Dim MyDataset As New DataSet
        Dim MyTable As New DataTable("SettlementStatements")

        'To do - change column names, retrieve correct data 

        MyTable.Columns.Add("FileID", GetType(Guid))
        MyTable.Columns.Add("Date", GetType(DateTime))
        MyTable.Columns.Add("MerchantID", GetType(Guid))
        MyTable.Columns.Add("Name", GetType(String))
        MyTable.Columns.Add("Received", GetType(DateTime))
        MyTable.Columns.Add("Size", GetType(Long))
        'MyTable.Columns.Add("Status", GetType(Integer))

        Dim TheRow(5) As Object
        Dim TF As TransactionFile
        For Each TF In Me
            TheRow(0) = TF.FileID
            TheRow(1) = TF.FileDate
            TheRow(2) = TF.MerchantID
            TheRow(3) = TF.FileName
            TheRow(4) = TF.TimeReceived
            TheRow(5) = TF.FileSize
            'TheRow(6) = TF.FileStatus
            MyTable.Rows.Add(TheRow)
        Next

        MyDataset.Tables.Add(MyTable)
        Return MyDataset

    End Function

    Public ReadOnly Property ItemByReportDataID(ByVal ReportDataID As Guid) As SettlementStatementReport
        Get
            Dim MyReturnData As New SettlementStatementReport
            ItemByReportDataID = Nothing

            For Each MyReturnData In Me
                If ReportDataID = MyReturnData.ReportDataID Then
                    ItemByReportDataID = MyReturnData
                    Exit For
                End If
            Next
        End Get
    End Property
End Class
