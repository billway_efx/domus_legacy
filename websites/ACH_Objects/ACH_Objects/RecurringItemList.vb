Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports System.Data.SqlClient

Public Class RecurringItemList

    Inherits CollectionBase



    Public Function AddRecurringItem(ByVal RecurringItem As RecurringItem) As Guid
        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim NewRecurringItemGuid As Guid = Guid.NewGuid
        Dim NewCustomerIDGuid As Guid = Guid.NewGuid
        Dim NewCustomerAccountID As Guid = Guid.NewGuid

        Try
            'Add the new RecurringItem to the collection
            RecurringItem.InternalRecurringItemID = NewRecurringItemGuid
            Me.List.Add(RecurringItem)


            'For this version, we're going to assume that everything about this new item is unique,
            'including the Customer and his Bank Account information. Even if it's not unique, we're going
            'to assign unique ids in the database for this information. So, it's possible we might have
            'redundant information in the database, but that's ok because 99% of the time the data will
            'be unique. The structure of the database is such that we can add additional functionality,
            'like reusing/updating existing customer data for new items, in the future if necessary.


            '1. Create the new Customers Record - hold on to the CustomerID value
            RecurringItem.InternalCustomerID = NewCustomerIDGuid
            sSql = "insert into Customers " & _
                "(MerchantID, CustomerID, MerchantCustomerID, FirstName, LastName, Address1, " & _
                "Address2, City, State, Zip, SSNumber, DLNumber, DLState, BirthDate, " & _
                "EmailAddress, PhoneNumber) " & _
                "values ('" & _
                RecurringItem.InternalMerchantID.ToString & "', '" & _
                RecurringItem.InternalCustomerID.ToString & "', '" & _
                RecurringItem.MerchantCustomerID & "', '" & _
                SafeApos(RecurringItem.FirstName) & "', '" & _
                SafeApos(RecurringItem.LastName) & "', '" & _
                SafeApos(RecurringItem.Address1) & "', '" & _
                SafeApos(RecurringItem.Address2) & "', '" & _
                RecurringItem.City & "', '" & _
                RecurringItem.State & "', '" & _
                RecurringItem.Zip & "', '" & _
                RecurringItem.SSNumber & "', '" & _
                RecurringItem.DLNumber & "', '" & _
                RecurringItem.DLState & "', '" & _
                RecurringItem.BirthDate.ToString() & "', '" & _
                RecurringItem.EmailAddress & "', '" & _
                RecurringItem.PhoneNumber & "')"
            dbMgr.TransactSql(sSql, INSERT)

            '2. Create the new CustomerAccounts Record - hold on to the CustomerAccountID value
            RecurringItem.InternalCustomerAccountID = NewCustomerAccountID
            sSql = "insert into CustomerAccounts " & _
                "(CustomerID, CustomerAccountID, RoutingNumber, AccountNumber, AccountType) " & _
                "values ('" & _
                RecurringItem.InternalCustomerID.ToString & "', '" & _
                RecurringItem.InternalCustomerAccountID.ToString & "', '" & _
                RecurringItem.RoutingNumber & "', '" & _
                RecurringItem.AccountNumber & "', " & _
                RecurringItem.AccountType.ToString & ")"
            dbMgr.TransactSql(sSql, INSERT)

            '3. Create the new RecurringItem Record - hold on to the RecurringItemID - return to caller
            sSql = "insert into RecurringItems " & _
                "(RecurringItemID, MerchantID, CustomerID, ItemType, ItemFrequency, " & _
                "ItemAmount, CustomerAccountID, ActiveDate, Status) " & _
                "values ('" & _
                RecurringItem.InternalRecurringItemID.ToString & "', '" & _
                RecurringItem.InternalMerchantID.ToString & "', '" & _
                RecurringItem.InternalCustomerID.ToString & "', " & _
                CInt(RecurringItem.ItemType).ToString & ", " & _
                CInt(RecurringItem.ItemFrequency).ToString & ", " & _
                RecurringItem.ItemAmount.ToString & ", '" & _
                RecurringItem.InternalCustomerAccountID.ToString & "', '" & _
                RecurringItem.ActiveDate.ToString() & "', " & _
                CInt(RecurringItem.Status).ToString & ")"
            dbMgr.TransactSql(sSql, INSERT)

        Catch ex As Exception
            NewRecurringItemGuid = Guid.Empty
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return NewRecurringItemGuid
    End Function

    Public Function UpdateRecurringItem(ByVal RecurringItem As RecurringItem) As Boolean
        Dim TheReturn As Boolean = False

        Try

            'If the ItemFrequency, ItemType, or ActiveDate changed, we will create a new item and remove the old one
            Dim ItemList As New RecurringItemList
            Dim OriginalItem As RecurringItem = ItemList.GetRecurringItem(RecurringItem.InternalRecurringItemID)

            If OriginalItem.ItemFrequency <> RecurringItem.ItemFrequency Or _
                OriginalItem.ItemType <> RecurringItem.ItemType Or _
                OriginalItem.ActiveDate <> RecurringItem.ActiveDate Then

                Dim NewItemId As Guid = Guid.NewGuid
                Dim OldItemId As Guid = RecurringItem.InternalRecurringItemID
                NewItemId = AddRecurringItem(RecurringItem)
                If NewItemId = Guid.Empty Then
                    TheReturn = False
                Else
                    TheReturn = RemoveRecurringItem(OldItemId)
                End If

            Else

                Dim strSQL As String
                Dim dbMgr As New Utilities.DBManager(APP_TYPE)

                '1. Update the Customers table
                strSQL = "update Customers set " & _
                    "MerchantCustomerID = '" & SafeApos(RecurringItem.MerchantCustomerID) & "', " & _
                    "FirstName = '" & SafeApos(RecurringItem.FirstName) & "', " & _
                    "LastName = '" & SafeApos(RecurringItem.LastName) & "', " & _
                    "Address1 = '" & SafeApos(RecurringItem.Address1) & "', " & _
                    "Address2 = '" & SafeApos(RecurringItem.Address2) & "', " & _
                    "City = '" & SafeApos(RecurringItem.City) & "', " & _
                    "State = '" & SafeApos(RecurringItem.State) & "', " & _
                    "Zip = '" & SafeApos(RecurringItem.Zip) & "', " & _
                    "SSNumber = '" & SafeApos(RecurringItem.SSNumber) & "', " & _
                    "DLNumber = '" & SafeApos(RecurringItem.DLNumber) & "', " & _
                    "DLState = '" & SafeApos(RecurringItem.DLState) & "', " & _
                    "BirthDate = '" & RecurringItem.BirthDate.ToString() & "', " & _
                    "EmailAddress = '" & SafeApos(RecurringItem.EmailAddress) & "', " & _
                    "PhoneNumber = '" & SafeApos(RecurringItem.PhoneNumber) & "' " & _
                    "where CustomerID = '" & RecurringItem.InternalCustomerID.ToString & "'"
                dbMgr.TransactSql(strSQL, UPDATE)

                '2. Update the CustomerAccounts table
                strSQL = "update CustomerAccounts set " & _
                    "RoutingNumber = '" & SafeApos(RecurringItem.RoutingNumber) & "', " & _
                    "AccountNumber = '" & SafeApos(RecurringItem.AccountNumber) & "', " & _
                    "AccountType = " & RecurringItem.AccountType.ToString & " " & _
                    "where CustomerAccountID = '" & RecurringItem.InternalCustomerAccountID.ToString & "'"
                dbMgr.TransactSql(strSQL, UPDATE)

                '3. Update the RecurringItems table
                strSQL = "update RecurringItems set " & _
                    "ItemType = " & CInt(RecurringItem.ItemType).ToString & ", " & _
                    "ItemFrequency = " & CInt(RecurringItem.ItemFrequency).ToString & ", " & _
                    "ItemAmount = " & RecurringItem.ItemAmount.ToString & ", " & _
                    "ActiveDate = '" & RecurringItem.ActiveDate.ToString & "', " & _
                    "Status = " & CInt(RecurringItem.Status).ToString & " " & _
                    "where RecurringItemID = '" & RecurringItem.InternalRecurringItemID.ToString & "'"
                dbMgr.TransactSql(strSQL, UPDATE)

                TheReturn = True

            End If



        Catch ex As Exception
            TheReturn = False
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return TheReturn

    End Function

    Public Function GetRecurringItem(ByVal RecurringItemID As Guid) As RecurringItem
        Dim strSQL As String
        Dim objRecurringItem As New RecurringItem

        Try

            strSQL = _
               "Select a.RecurringItemID, a.MerchantID, a.CustomerID, b.MerchantCustomerID, " & _
                    "b.FirstName, b.LastName, b.Address1, b.Address2, b.City, b.State, b.Zip, b.SSNumber, " & _
                    "b.DLNumber, b.DLState, b.BirthDate, b.EmailAddress, b.PhoneNumber, a.ItemType, " & _
                    "a.ItemFrequency, a.ItemAmount, a.CustomerAccountID, " & _
                    "c.RoutingNumber, c.AccountNumber, c.AccountType, a.ActiveDate, a.Status " & _
                    "From " & _
                    "RecurringItems a, " & _
                    "Customers b, " & _
                    "CustomerAccounts c " & _
                    "where a.RecurringItemID = '" & RecurringItemID.ToString() & "' and " & _
                    "a.MerchantID = b.MerchantID and " & _
                    "a.CustomerID = b.CustomerID and " & _
                    "a.CustomerAccountID = c.CustomerAccountID and " & _
                    "a.CustomerID = c.CustomerID "

            objRecurringItem = GetRecurringItemHelper(strSQL)
            Return objRecurringItem

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Private Function GetRecurringItemHelper(ByVal TheSql As String) As RecurringItem
        Dim dsRecurringItem As DataSet
        Dim drRecurringItem As DataRow
        Dim objRecurringItem As New RecurringItem
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)

        Try

            dsRecurringItem = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(dsRecurringItem) Then

                drRecurringItem = dsRecurringItem.Tables(0).Rows(0)

                ' Build RecurringItem object
                objRecurringItem.InternalRecurringItemID = SafeDBGuidField(drRecurringItem, "RecurringItemID")
                objRecurringItem.InternalMerchantID = SafeDBGuidField(drRecurringItem, "MerchantID")
                objRecurringItem.InternalCustomerID = SafeDBGuidField(drRecurringItem, "CustomerID")
                objRecurringItem.MerchantCustomerID = SafeDBStringField(drRecurringItem, "MerchantCustomerID")

                objRecurringItem.FirstName = SafeDBStringField(drRecurringItem, "FirstName")
                objRecurringItem.LastName = SafeDBStringField(drRecurringItem, "LastName")
                objRecurringItem.Address1 = SafeDBStringField(drRecurringItem, "Address1")
                objRecurringItem.Address2 = SafeDBStringField(drRecurringItem, "Address2")
                objRecurringItem.City = SafeDBStringField(drRecurringItem, "City")
                objRecurringItem.State = SafeDBStringField(drRecurringItem, "State")
                objRecurringItem.Zip = SafeDBStringField(drRecurringItem, "Zip")
                objRecurringItem.SSNumber = SafeDBStringField(drRecurringItem, "SSNumber")
                objRecurringItem.DLNumber = SafeDBStringField(drRecurringItem, "DLNumber")
                objRecurringItem.DLState = SafeDBStringField(drRecurringItem, "DLState")
                objRecurringItem.BirthDate = SafeDBDateField(drRecurringItem, "BirthDate")
                objRecurringItem.EmailAddress = SafeDBStringField(drRecurringItem, "EmailAddress")
                objRecurringItem.PhoneNumber = SafeDBStringField(drRecurringItem, "PhoneNumber")

                objRecurringItem.ItemType = SafeDBIntField(drRecurringItem, "ItemType")
                objRecurringItem.ItemFrequency = SafeDBIntField(drRecurringItem, "ItemFrequency")
                objRecurringItem.ItemAmount = SafeDBMoneyField(drRecurringItem, "ItemAmount")

                objRecurringItem.InternalCustomerAccountID = SafeDBGuidField(drRecurringItem, "CustomerAccountID")

                objRecurringItem.RoutingNumber = SafeDBStringField(drRecurringItem, "RoutingNumber")
                objRecurringItem.AccountNumber = SafeDBStringField(drRecurringItem, "AccountNumber")
                objRecurringItem.AccountType = SafeDBIntField(drRecurringItem, "AccountType")

                objRecurringItem.ActiveDate = SafeDBDateField(drRecurringItem, "ActiveDate")

                objRecurringItem.Status = SafeDBIntField(drRecurringItem, "Status")
            End If

            Return objRecurringItem

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function RemoveRecurringItem(ByVal RecurringItem As RecurringItem) As Boolean
        Return RemoveRecurringItem(RecurringItem.InternalRecurringItemID)
    End Function

    Public Function RemoveRecurringItem(ByVal RecurringItemID As String) As Boolean
        Dim InternalRecurringItemID As New Guid(RecurringItemID)
        Return RemoveRecurringItem(InternalRecurringItemID)
    End Function

    Public Function RemoveRecurringItem(ByVal InternalRecurringItemID As Guid) As Boolean
        ' Error indicator...
        Dim errIndicator As Int16 = 0
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim objRecurringItem As RecurringItem
        Dim strSQL As String

        Try

            ' Remove the RecurringItem from the collection...
            objRecurringItem = New RecurringItem
            For Each objRecurringItem In Me
                If InternalRecurringItemID = objRecurringItem.InternalRecurringItemID Then
                    Me.List.Remove(objRecurringItem)
                    Exit For
                End If
            Next

            ' Build delete query and execution type...
            'strSQL = _
            '        "DELETE FROM RecurringItems " & _
            '        "WHERE " & _
            '        "RecurringItemID = '" & InternalRecurringItemID.ToString & "'"

            'Changing the status to 99 removes it from queries retrieving recurring items
            strSQL = _
                    "update RecurringItems set status = 99" & _
                    "WHERE " & _
                    "RecurringItemID = '" & InternalRecurringItemID.ToString & "'"

            ' Virtually remove the transaction from the database...
            If dbMgr.TransactSql(strSQL, UPDATE) > 0 Then
                ' Return true to caller...
                RemoveRecurringItem = True
            Else
                ' Return false to caller...
                RemoveRecurringItem = False
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Clean-up...
        End Try

    End Function

    Public Function GetRecurringItemListByMerchantID(ByVal MerchantID As String, Optional Filter As String = "") As RecurringItemList

        'Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("MerchantID", SqlDbType.UniqueIdentifier)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = New Guid(MerchantID)


            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetRecurringItemsForMerchant", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)
            Dim drRecurringItem As DataRow
            Dim myRecurringItem As RecurringItem
            Me.Clear()

            ' Insert each user into the user list collection...
            For Each drRecurringItem In dsInfo.Tables(0).Rows

                myRecurringItem = GetRecurringItem(drRecurringItem("RecurringItemID"))

                ' Add Transaction to list...
                If Filter <> "" Then

                    Dim ItemType As String
                    Dim ItemFrequency As String = ""
                    Dim AccountType As String
                    Dim Status As String

                    If myRecurringItem.ItemType = 1 Then
                        ItemType = "CREDIT"
                    Else
                        ItemType = "DEBIT"
                    End If

                    Select Case myRecurringItem.ItemFrequency
                        Case 1
                            ItemFrequency = "MONTHLY"
                        Case 2
                            ItemFrequency = "QUARTERLY"
                        Case 3
                            ItemFrequency = "WEEKLY"
                        Case 4
                            ItemFrequency = "DAILY"
                        Case 5
                            ItemFrequency = "EVERY 2 WEEKS"
                        Case 6
                            ItemFrequency = "TWICE MONTHLY"
                    End Select

                    If myRecurringItem.AccountType = 1 Then
                        AccountType = "CHECKING"
                    Else
                        AccountType = "SAVINGS"
                    End If

                    If myRecurringItem.Status = 0 Then
                        Status = "DISABLED"
                    Else
                        Status = "ENABLED"
                    End If

                    Filter = UCase(Filter)
                    If UCase(myRecurringItem.MerchantCustomerID).Contains(Filter) _
                        Or UCase(myRecurringItem.FirstName).Contains(Filter) _
                        Or UCase(myRecurringItem.LastName).Contains(Filter) _
                        Or UCase(myRecurringItem.Address1).Contains(Filter) _
                        Or UCase(myRecurringItem.Address2).Contains(Filter) _
                        Or UCase(myRecurringItem.City).Contains(Filter) _
                        Or UCase(myRecurringItem.State).Contains(Filter) _
                        Or UCase(myRecurringItem.Zip).Contains(Filter) _
                        Or UCase(myRecurringItem.PhoneNumber).Contains(Filter) _
                        Or UCase(myRecurringItem.RoutingNumber).Contains(Filter) _
                        Or UCase(myRecurringItem.AccountNumber).Contains(Filter) _
                        Or myRecurringItem.ActiveDate.ToString().Contains(Filter) _
                        Or myRecurringItem.ItemAmount.ToString("C").Contains(Filter) _
                        Or ItemType.Contains(Filter) _
                        Or ItemFrequency.Contains(Filter) _
                        Or AccountType.Contains(Filter) _
                        Or Status.Contains(Filter) Then

                        Me.List.Add(myRecurringItem)

                    End If

                Else
                    Me.List.Add(myRecurringItem)
                End If

            Next

            'ReturnDS = dsInfo

        Catch ex As Exception
            'ReturnDS = Nothing
        End Try

        Return Me
    End Function
    Public Function GetRecurringItemListRecordCountByMerchantID(ByVal MerchantID As String) As Long

        Dim dsRecurringItem As DataSet
        Dim objRecurringItem As New RecurringItem
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim TheSql As String = ""
        Dim TheCount As Long = 0

        Try
            TheSql = "select count(*) from RecurringItems where MerchantID = '" & MerchantID & "' and Status <> 99"

            dsRecurringItem = dbMgr.TransactSql(TheSql, FILL_DATASET)
            TheCount = CLng(dsRecurringItem.Tables(0).Rows(0).Item(0))

        Catch ex As Exception
            TheCount = 0

        End Try

        Return TheCount
    End Function

End Class
