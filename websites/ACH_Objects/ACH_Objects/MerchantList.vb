Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports ACH_Objects.WebUser

Public Class MerchantList

    Inherits CollectionBase

    Public Function UpdateMerchant(ByVal RevisedMerchant As Merchant) As Boolean
        Dim TheMerchantID As Guid
        Dim TheOriginalMerchant As Merchant
        Dim strSQL As String
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim TheReturn As Boolean

        TheMerchantID = RevisedMerchant.InternalMerchantID
        TheOriginalMerchant = GetMerchant(TheMerchantID)

        'If the original doesn't exist, then we need to do an add
        If TheOriginalMerchant.InternalMerchantID = Guid.Empty Then
            If AddMerchant(RevisedMerchant) <> Guid.Empty Then
                TheReturn = True
            Else
                TheReturn = False
            End If
        Else

            'Otherwise, let's compare them, and see if an update is necessary
            If IsEqualTo(TheOriginalMerchant, RevisedMerchant) Then
                'They are identical, no update necessary
                TheReturn = True
            Else
                'Otherwise, update the database
                Try
                    ' Patrick Whittingham - 4/15/2015 - task# 000172: update usermerchantid
                    strSQL = _
                       "UPDATE merchants " & _
                       "SET ClearDays = " & RevisedMerchant.ClearDays.ToString() & _
                          ",ProductDescription = '" & RevisedMerchant.ProductDescription & "'" & _
                          ",UserMerchantID = '" & RevisedMerchant.UserMerchantID & "'" & _
                          ",ProductPrice = '" & RevisedMerchant.ProductPrice & "'" & _
                          ",ProductSaleMethod = '" & RevisedMerchant.ProductSaleMethod & "'" & _
                          ",ProductCreditsAllowedFlag = " & CInt(RevisedMerchant.ProductCreditsAllowedFlag).ToString() & _
                          ",ProductMaxCrDrRatio = " & RevisedMerchant.ProductMaxCrDrRatio.ToString() & _
                          ",TransactionDescriptor = '" & RevisedMerchant.TransactionDescriptor & "'" & _
                          ",TransactionCSNumber = '" & RevisedMerchant.TransactionCSNumber & "'" & _
                          ",TransactionSecDr = '" & RevisedMerchant.TransactionSecDr & "'" & _
                          ",TransactionSecCr = '" & RevisedMerchant.TransactionSecCr & "'" & _
                          ",AssignedODFIID = '" & RevisedMerchant.AssignedODFIID.ToString & "'" & _
                          ",AccountVerificationServiceFlag = " & CInt(RevisedMerchant.AccountVerificationServiceFlag).ToString() & _
                          ",IDVerificationServiceFlag = " & CInt(RevisedMerchant.IDVerificationServiceFlag).ToString() & _
                          ",EnableTransactionProcessingFlag = " & CInt(RevisedMerchant.EnableTransactionProcessingFlag).ToString() & _
                          ",ReportFormat = '" & RevisedMerchant.ReportFormat & "'" & _
                          ",ReportDeliveryFTP = '" & RevisedMerchant.ReportDeliveryFTP & "'" & _
                          ",ReportDeliveryEmail = '" & RevisedMerchant.ReportDeliveryEmail & "'" & _
                          ",EFXSalesRep = '" & RevisedMerchant.EFXSalesRep & "'" & _
                          ",FTPFolder = '" & RevisedMerchant.FTPFolder & "'" & _
                          ",DrTxnSubmitFee = " & RevisedMerchant.DrTxnSubmitFee.ToString() & _
                          ",DrTxnRejectFee = " & RevisedMerchant.DrTxnRejectFee.ToString() & _
                          ",DrTxnClearFee = " & RevisedMerchant.DrTxnClearFee.ToString() & _
                          ",DrTxnReturnFee = " & RevisedMerchant.DrTxnReturnFee.ToString() & _
                          ",DrTxnChargeBackFee = " & RevisedMerchant.DrTxnChargeBackFee.ToString() & _
                          ",CrTxnSubmitFee = " & RevisedMerchant.CrTxnSubmitFee.ToString() & _
                          ",CrTxnRejectFee = " & RevisedMerchant.CrTxnRejectFee.ToString() & _
                          ",CrTxnClearFee = " & RevisedMerchant.CrTxnClearFee.ToString() & _
                          ",CrTxnReturnFee = " & RevisedMerchant.CrTxnReturnFee.ToString() & _
                          ",CrTxnChargeBackFee = " & RevisedMerchant.CrTxnChargeBackFee.ToString() & _
                          ",AccountVerificationFee = " & RevisedMerchant.AccountVerificationFee.ToString() & _
                          ",TxnSettlementFee = " & RevisedMerchant.TxnSettlementFee.ToString() & _
                          ",IDVerificationFee = " & RevisedMerchant.IDVerificationFee.ToString() & _
                          ",ClearedValueFeePercentage = " & RevisedMerchant.ClearedValueFeePercentage.ToString() & _
                          ",SettlementBankName = '" & RevisedMerchant.SettlementBankName & "'" & _
                          ",SettlementBankABA = '" & RevisedMerchant.SettlementBankABA & "'" & _
                          ",SettlementBankDDA = '" & RevisedMerchant.SettlementBankDDA & "'" & _
                          ",ReservesRate = " & RevisedMerchant.ReservesRate.ToString() & _
                          ",ReservesPeriod = " & RevisedMerchant.ReservesPeriod.ToString() & _
                          ",StatementPeriodEndDayOfWeek = " & RevisedMerchant.StatementPeriodEndDayOfWeek.ToString & _
                          ",MerchantActiveDate = '" & RevisedMerchant.MerchantActiveDate.ToString & "'" & _
                          ",LastStatusFileCreateDate = '" & RevisedMerchant.LastStatusFileCreateDate.ToString & "'" & _
                          ",SubmissionFileType = '" & RevisedMerchant.SubmissionFileType & "' " & _
                          ",CompanyNumber = '" & RevisedMerchant.CompanyNumber & "' " & _
                          ",AllowDuplicateTransactions = " & CInt(RevisedMerchant.AllowDuplicateTransactions).ToString & " " & _
                          ",AllowCreditTransactions = " & CInt(RevisedMerchant.AllowCreditTransactions).ToString & " " & _
                          ",AutoApproveValidNACHAFiles = " & CInt(RevisedMerchant.AutoApproveValidNACHAFiles).ToString & " " & _
                          ",RequireBalancedNACHAFile = " & CInt(RevisedMerchant.RequireBalancedNACHAFile).ToString & " " & _
                          ",EmailReturnNotificationFlag = " & CInt(RevisedMerchant.EmailReturnNotificationFlag).ToString & " " & _
                          ",KeepIncomingNACHABatchHeader = " & CInt(RevisedMerchant.KeepIncomingNACHABatchHeader).ToString & " " & _
                          ",CreditClearDaysUsesNACHAEffectiveDate = " & CInt(RevisedMerchant.CreditClearDaysUsesNACHAEffectiveDate).ToString & " " & _
                          ",CurrentReservesAmount = " & RevisedMerchant.CurrentReservesAmount.ToString & " " & _
                          ",ProcessBalancedNACHAFileDebitsBeforeEffDate = " & CInt(RevisedMerchant.ProcessBalancedNACHAFileDebitsBeforeEffDate).ToString & " " & _
                          ",NACHAFileMaxSubmitDays = " & RevisedMerchant.NACHAFileMaxSubmitDays.ToString & " " & _
                          ",ValidateBalancedNACHAFileBatchEffDate = " & CInt(RevisedMerchant.ValidateBalancedNACHAFileBatchEffDate).ToString & " " & _
                          " WHERE merchantid = '" & TheMerchantID.ToString() & "'"

                    Call dbMgr.TransactSql(strSQL, UPDATE)
                    TheReturn = True
                Catch ex As Exception
                    TheReturn = False
                    ExceptionManager.Publish(ex)
                    Throw (ex)
                Finally
                End Try
            End If

        End If

        Return TheReturn

    End Function

    Public Function AddMerchant(ByVal Merchant As Merchant) As Guid
        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim NewGuid As Guid = Guid.NewGuid

        Try
            'Add the new web user to the collection
            Merchant.InternalMerchantID = NewGuid
            Me.List.Add(Merchant)

            'Create SQL ...
            sSql = "INSERT INTO Merchants " & _
                "(Merchantid, WebUserID, UserMerchantID, ClearDays, ProductDescription, " & _
                "ProductPrice, ProductSaleMethod, ProductCreditsAllowedFlag, " & _
                "ProductMaxCrDrRatio, TransactionDescriptor, TransactionCSNumber, " & _
                "TransactionSecDr, TransactionSecCr, AssignedODFIID, AccountVerificationServiceFlag, " & _
                "IDVerificationServiceFlag, EnableTransactionProcessingFlag, " & _
                "ReportFormat, ReportDeliveryFTP, ReportDeliveryEmail, " & _
                "EFXSalesRep, FTPFolder, DrTxnSubmitFee, DrTxnRejectFee, DrTxnClearFee, " & _
                "DrTxnReturnFee, DrTxnChargeBackFee, CrTxnSubmitFee, CrTxnRejectFee, CrTxnClearFee, " & _
                "CrTxnReturnFee, CrTxnChargeBackFee, " & _
                "AccountVerificationFee, TxnSettlementFee, IDVerificationFee, " & _
                "ClearedValueFeePercentage, SettlementBankName, SettlementBankABA, " & _
                "SettlementBankDDA, ReservesRate, ReservesPeriod, StatementPeriodEndDayOfWeek, " & _
                "MerchantActiveDate, LastStatementPeriodEndDate, LastStatusFileCreateDate, " & _
                "SubmissionFileType, CompanyNumber, AllowDuplicateTransactions, AllowCreditTransactions, RequireBalancedNACHAFile, " & _
                "KeepIncomingNACHABatchHeader, CreditClearDaysUsesNACHAEffectiveDate, CurrentReservesAmount, EmailReturnNotificationFlag, " & _
                "ProcessBalancedNACHAFileDebitsBeforeEffDate, NACHAFileMaxSubmitDays, AutoApproveValidNACHAFiles, ValidateBalancedNACHAFileBatchEffDate) " & _
                "VALUES ('" & _
                Merchant.InternalMerchantID.ToString & "', '" & _
                Merchant.WebUser.WebUserID.ToString & "', '" & _
                Merchant.UserMerchantID & "', " & _
                Merchant.ClearDays.ToString & ", '" & _
                Merchant.ProductDescription & "', '" & _
                Merchant.ProductPrice & "', '" & _
                Merchant.ProductSaleMethod & "', " & _
                CInt(Merchant.ProductCreditsAllowedFlag) & ", " & _
                Merchant.ProductMaxCrDrRatio.ToString & ", '" & _
                Merchant.TransactionDescriptor & "', '" & _
                Merchant.TransactionCSNumber & "', '" & _
                Merchant.TransactionSecDr & "', '" & _
                Merchant.TransactionSecCr & "', '" & _
                Merchant.AssignedODFIID.ToString & "', " & _
                CInt(Merchant.AccountVerificationServiceFlag) & ", " & _
                CInt(Merchant.IDVerificationServiceFlag) & ", " & _
                CInt(Merchant.EnableTransactionProcessingFlag) & ", '" & _
                Merchant.ReportFormat & "', '" & _
                Merchant.ReportDeliveryFTP & "', '" & _
                Merchant.ReportDeliveryEmail & "', '" & _
                Merchant.EFXSalesRep & "', '" & _
                Merchant.FTPFolder & "', " & _
                Merchant.DrTxnSubmitFee.ToString & ", " & _
                Merchant.DrTxnRejectFee.ToString & ", " & _
                Merchant.DrTxnClearFee.ToString & ", " & _
                Merchant.DrTxnReturnFee.ToString & ", " & _
                Merchant.DrTxnChargeBackFee.ToString & ", " & _
                Merchant.CrTxnSubmitFee.ToString & ", " & _
                Merchant.CrTxnRejectFee.ToString & ", " & _
                Merchant.CrTxnClearFee.ToString & ", " & _
                Merchant.CrTxnReturnFee.ToString & ", " & _
                Merchant.CrTxnChargeBackFee.ToString & ", " & _
                Merchant.AccountVerificationFee.ToString & ", " & _
                Merchant.TxnSettlementFee.ToString & ", " & _
                Merchant.IDVerificationFee.ToString & ", " & _
                Merchant.ClearedValueFeePercentage.ToString & ", '" & _
                Merchant.SettlementBankName & "', '" & _
                Merchant.SettlementBankABA & "', '" & _
                Merchant.SettlementBankDDA & "', " & _
                Merchant.ReservesRate.ToString & ", " & _
                Merchant.ReservesPeriod.ToString & ", " & _
                Merchant.StatementPeriodEndDayOfWeek.ToString & ", '" & _
                Merchant.MerchantActiveDate.ToString & "', '" & _
                Merchant.LastStatementPeriodEndDate.ToString & "', '" & _
                Merchant.LastStatusFileCreateDate.ToString & "', '" & _
                Merchant.SubmissionFileType & "', '" & _
                Merchant.CompanyNumber & "', " & _
                CInt(Merchant.AllowDuplicateTransactions).ToString & ", " & _
                CInt(Merchant.AllowCreditTransactions).ToString & ", " & _
                CInt(Merchant.RequireBalancedNACHAFile).ToString & ", " & _
                CInt(Merchant.KeepIncomingNACHABatchHeader).ToString & ", " & _
                CInt(Merchant.CreditClearDaysUsesNACHAEffectiveDate).ToString & ", " & _
                Merchant.CurrentReservesAmount.ToString & ", " & _
                CInt(Merchant.EmailReturnNotificationFlag).ToString & ", " & _
                CInt(Merchant.ProcessBalancedNACHAFileDebitsBeforeEffDate).ToString & ", " & _
                Merchant.NACHAFileMaxSubmitDays.ToString & ", " & _
                CInt(Merchant.AutoApproveValidNACHAFiles).ToString & ", " & _
                CInt(Merchant.ValidateBalancedNACHAFileBatchEffDate).ToString & ")"

            'Process SQL transaction ...
            dbMgr.TransactSql(sSql, INSERT)

        Catch ex As Exception
            NewGuid = Guid.Empty
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return NewGuid
    End Function

    Public Function MerchantExists(ByVal UserMerchantID As String) As Boolean
        Dim TempMerchant As Merchant
        TempMerchant = GetMerchant(UserMerchantID)
        If TempMerchant.InternalMerchantID <> Guid.Empty Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function MerchantExists(ByVal MerchantID As Guid) As Boolean
        Dim TempMerchant As Merchant
        TempMerchant = GetMerchant(MerchantID)
        If TempMerchant.InternalMerchantID <> Guid.Empty Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function GetMerchant(ByVal MerchantID As Guid) As Merchant
        Dim strSQL As String
        Dim objMerchant As New Merchant

        Try

            strSQL = _
               "SELECT * FROM Merchants " & _
               "WHERE MerchantID = '" & MerchantID.ToString & "'"

            objMerchant = GetMerchantHelper(strSQL)
            Return objMerchant

        Catch ex As Exception
            'ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function GetMerchant(ByVal UserMerchantID As String) As Merchant
        Dim strSQL As String
        Dim objMerchant As New Merchant

        Try

            strSQL = _
               "SELECT * FROM Merchants " & _
               "WHERE UserMerchantID = '" & UserMerchantID & "'"

            objMerchant = GetMerchantHelper(strSQL)
            Return objMerchant

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Private Function GetMerchantHelper(ByVal TheSql As String) As Merchant
        Dim dsMerchant As DataSet
        Dim drMerchant As DataRow
        Dim objMerchant As New Merchant
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)

        Try

            dsMerchant = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(dsMerchant) Then

                drMerchant = dsMerchant.Tables(0).Rows(0)

                'Populate the WebUser object
                Dim objWebUser As New WebUser
                Dim objWebUserList As New WebUserList
                objWebUser = objWebUserList.GetWebUser(SafeDBGuidField(drMerchant, "WebUserID"))

                ' Build merchant object
                objMerchant.InternalMerchantID = SafeDBGuidField(drMerchant, "MerchantID")
                objMerchant.WebUser = objWebUser
                objMerchant.UserMerchantID = SafeDBStringField(drMerchant, "UserMerchantID")
                objMerchant.ClearDays = SafeDBIntField(drMerchant, "ClearDays")

                objMerchant.ProductDescription = SafeDBStringField(drMerchant, "ProductDescription")
                objMerchant.ProductPrice = SafeDBStringField(drMerchant, "ProductPrice")
                objMerchant.ProductSaleMethod = SafeDBStringField(drMerchant, "ProductSaleMethod")
                objMerchant.ProductCreditsAllowedFlag = SafeDBBitField(drMerchant, "ProductCreditsAllowedFlag")
                objMerchant.ProductMaxCrDrRatio = SafeDBFloatField(drMerchant, "ProductMaxCrDrRatio")

                objMerchant.TransactionDescriptor = SafeDBStringField(drMerchant, "TransactionDescriptor")
                objMerchant.TransactionCSNumber = SafeDBStringField(drMerchant, "TransactionCSNumber")
                objMerchant.TransactionSecDr = SafeDBStringField(drMerchant, "TransactionSecDr")
                objMerchant.TransactionSecCr = SafeDBStringField(drMerchant, "TransactionSecCr")

                objMerchant.AssignedODFIID = SafeDBGuidField(drMerchant, "AssignedODFIID")

                objMerchant.AccountVerificationServiceFlag = SafeDBBitField(drMerchant, "AccountVerificationServiceFlag")
                objMerchant.IDVerificationServiceFlag = SafeDBBitField(drMerchant, "IDVerificationServiceFlag")
                objMerchant.EnableTransactionProcessingFlag = SafeDBBitField(drMerchant, "EnableTransactionProcessingFlag")

                objMerchant.ReportFormat = SafeDBStringField(drMerchant, "ReportFormat")
                objMerchant.ReportDeliveryFTP = SafeDBStringField(drMerchant, "ReportDeliveryFTP")
                objMerchant.ReportDeliveryEmail = SafeDBStringField(drMerchant, "ReportDeliveryEmail")

                objMerchant.EFXSalesRep = SafeDBStringField(drMerchant, "EFXSalesRep")
                objMerchant.FTPFolder = SafeDBStringField(drMerchant, "FTPFolder")

                objMerchant.DrTxnSubmitFee = SafeDBMoneyField(drMerchant, "DrTxnSubmitFee")
                objMerchant.DrTxnRejectFee = SafeDBMoneyField(drMerchant, "DrTxnRejectFee")
                objMerchant.DrTxnClearFee = SafeDBMoneyField(drMerchant, "DrTxnClearFee")
                objMerchant.DrTxnReturnFee = SafeDBMoneyField(drMerchant, "DrTxnReturnFee")
                objMerchant.DrTxnChargeBackFee = SafeDBMoneyField(drMerchant, "DrTxnChargeBackFee")
                objMerchant.CrTxnSubmitFee = SafeDBMoneyField(drMerchant, "CrTxnSubmitFee")
                objMerchant.CrTxnRejectFee = SafeDBMoneyField(drMerchant, "CrTxnRejectFee")
                objMerchant.CrTxnClearFee = SafeDBMoneyField(drMerchant, "CrTxnClearFee")
                objMerchant.CrTxnReturnFee = SafeDBMoneyField(drMerchant, "CrTxnReturnFee")
                objMerchant.CrTxnChargeBackFee = SafeDBMoneyField(drMerchant, "CrTxnChargeBackFee")

                objMerchant.AccountVerificationFee = SafeDBMoneyField(drMerchant, "AccountVerificationFee")
                objMerchant.TxnSettlementFee = SafeDBMoneyField(drMerchant, "TxnSettlementFee")
                objMerchant.IDVerificationFee = SafeDBMoneyField(drMerchant, "IDVerificationFee")

                objMerchant.ClearedValueFeePercentage = SafeDBFloatField(drMerchant, "ClearedValueFeePercentage")

                objMerchant.SettlementBankName = SafeDBStringField(drMerchant, "SettlementBankName")
                objMerchant.SettlementBankABA = SafeDBStringField(drMerchant, "SettlementBankABA")
                objMerchant.SettlementBankDDA = SafeDBStringField(drMerchant, "SettlementBankDDA")

                objMerchant.ReservesRate = SafeDBFloatField(drMerchant, "ReservesRate")
                objMerchant.CurrentReservesAmount = SafeDBFloatField(drMerchant, "CurrentReservesAmount")

                objMerchant.ReservesPeriod = SafeDBIntField(drMerchant, "ReservesPeriod")

                objMerchant.Contacts = New ContactInfoList(objMerchant.InternalMerchantID)

                objMerchant.StatementPeriodEndDayOfWeek = SafeDBSmallIntField(drMerchant, "StatementPeriodEndDayOfWeek")
                objMerchant.MerchantActiveDate = SafeDBDateField(drMerchant, "MerchantActiveDate")
                objMerchant.LastStatementPeriodEndDate = SafeDBDateField(drMerchant, "LastStatementPeriodEndDate")
                objMerchant.LastStatusFileCreateDate = SafeDBDateField(drMerchant, "LastStatusFileCreateDate")

                objMerchant.SubmissionFileType = SafeDBStringField(drMerchant, "SubmissionFileType")
                objMerchant.CompanyNumber = SafeDBStringField(drMerchant, "CompanyNumber")

                objMerchant.AllowDuplicateTransactions = SafeDBBitField(drMerchant, "AllowDuplicateTransactions")
                objMerchant.AllowCreditTransactions = SafeDBBitField(drMerchant, "AllowCreditTransactions")
                objMerchant.KeepIncomingNACHABatchHeader = SafeDBBitField(drMerchant, "KeepIncomingNACHABatchHeader")
                objMerchant.CreditClearDaysUsesNACHAEffectiveDate = SafeDBBitField(drMerchant, "CreditClearDaysUsesNACHAEffectiveDate")
                objMerchant.RequireBalancedNACHAFile = SafeDBBitField(drMerchant, "RequireBalancedNACHAFile")

                objMerchant.EmailReturnNotificationFlag = SafeDBBitField(drMerchant, "EmailReturnNotificationFlag")
                objMerchant.ProcessBalancedNACHAFileDebitsBeforeEffDate = SafeDBBitField(drMerchant, "ProcessBalancedNACHAFileDebitsBeforeEffDate")
                objMerchant.NACHAFileMaxSubmitDays = SafeDBSmallIntField(drMerchant, "NACHAFileMaxSubmitDays")

                objMerchant.AutoApproveValidNACHAFiles = SafeDBBitField(drMerchant, "AutoApproveValidNACHAFiles")
                objMerchant.ValidateBalancedNACHAFileBatchEffDate = SafeDBBitField(drMerchant, "ValidateBalancedNACHAFileBatchEffDate")

            End If

            Return objMerchant

        Catch ex As Exception
            'ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function RemoveMerchant(ByVal MerchantID As Guid) As Boolean
        ' Error indicator...
        Dim errIndicator As Int16 = 0
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim objMerchant As Merchant
        Dim strSQL As String

        Try

            ' Remove the Merchant from the collection...
            objMerchant = New Merchant
            For Each objMerchant In Me
                If MerchantID = objMerchant.InternalMerchantID Then
                    Me.List.Remove(objMerchant)
                    Exit For
                End If
            Next

            ' Build delete query and execution type...
            strSQL = _
                    "DELETE FROM Merchant " & _
                    "WHERE " & _
                    "MerchantID = '" & MerchantID.ToString & "'"

            ' Remove the web user from the database...
            If dbMgr.TransactSql(strSQL, DELETE) > 0 Then
                ' Return true to caller...
                RemoveMerchant = True
            Else
                ' Return false to caller...
                RemoveMerchant = False
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Clean-up...
        End Try

    End Function

    Public Sub FillCollection()
        Dim dbMgr As Utilities.DBManager
        Dim drMerchant As DataRow
        Dim myMerchant As Merchant
        Dim strGetFileTemps As String
        Dim dsMerchants As DataSet = Nothing

        Try

            ' Build the select query...
            strGetFileTemps = "SELECT MerchantID FROM Merchants"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsMerchants = dbMgr.TransactSql(strGetFileTemps, FILL_DATASET)

            'Clear the existing collection
            Me.List.Clear()

            ' Insert each user into the user list collection...
            For Each drMerchant In dsMerchants.Tables(0).Rows

                myMerchant = GetMerchant(drMerchant("Merchantid"))

                ' Add Merchant to list...
                Me.List.Add(myMerchant)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsMerchants Is Nothing) Then
                dsMerchants.Dispose()
            End If
        End Try


    End Sub

    Public ReadOnly Property ItemByMerchantID(ByVal MerchantID As Guid) As Merchant
        Get
            Dim MyMerchant As New Merchant
            ItemByMerchantID = Nothing

            For Each MyMerchant In Me
                If MerchantID = MyMerchant.InternalMerchantID Then
                    ItemByMerchantID = MyMerchant
                    Exit For
                End If
            Next
        End Get
    End Property

    Public Sub New(ByVal LoginID As String)
        Dim dbMgr As Utilities.DBManager
        Dim drMerchant As DataRow
        Dim myMerchant As Merchant
        Dim strGetFileTemps As String
        Dim dsMerchants As DataSet = Nothing

        Try

            ' Build the select query...
            strGetFileTemps = "SELECT MerchantID FROM Merchants, ASPMembershipWebUser where " & _
                "Merchants.WebUserID = ASPMembershipWebUser.WebUserID and " & _
                "ASPMembershipWebUser.LoginID = '" & LoginID & "' " & _
                "ORDER BY UserMerchantID" ' added sort - task # 0000063

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsMerchants = dbMgr.TransactSql(strGetFileTemps, FILL_DATASET)

            ' Insert each user into the user list collection...
            For Each drMerchant In dsMerchants.Tables(0).Rows

                myMerchant = GetMerchant(drMerchant("Merchantid"))

                ' Add Merchant to list...
                Me.List.Add(myMerchant)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsMerchants Is Nothing) Then
                dsMerchants.Dispose()
            End If
        End Try


    End Sub

    Public Sub New()


    End Sub

    'Populates the collection with all merchants that are not already assigned to the 
    'specified Web User
    Public Sub FillCollectionWithAvailableOffsetAccounts(ByVal WebUserID As Guid)
        Dim dbMgr As Utilities.DBManager
        Dim drMerchant As DataRow
        Dim myMerchant As Merchant
        Dim strGetFileTemps As String
        Dim dsMerchants As DataSet = Nothing

        Try

            ' Build the select query...
            strGetFileTemps = "SELECT a.MerchantID FROM Merchants a where " & _
                "a.WebUserID = '" & WebUserID.ToString & "' and " & _
                "a.MerchantID not in (select MerchantID from WebUserOffsetMerchants where " & _
                "WebUserID = '" & WebUserID.ToString & "')"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsMerchants = dbMgr.TransactSql(strGetFileTemps, FILL_DATASET)

            'Clear the existing collection
            Me.List.Clear()

            ' Insert each user into the user list collection...
            For Each drMerchant In dsMerchants.Tables(0).Rows

                myMerchant = GetMerchant(drMerchant("Merchantid"))

                ' Add Merchant to list...
                Me.List.Add(myMerchant)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsMerchants Is Nothing) Then
                dsMerchants.Dispose()
            End If
        End Try


    End Sub

    'Returns true if the specified WebUser has authorized access to this MerchantID
    Public Function AuthorizeAccess(ByVal WebUser As WebUser, ByVal MerchantID As Guid) As Boolean
        Dim bReturn As Boolean = False

        Try

            Dim strSQL As String

            strSQL = _
               "SELECT count(*) TheCount " & _
               "FROM Merchants " & _
               "WHERE MerchantID = '" & MerchantID.ToString & "' and " & _
               "WebUserID = '" & WebUser.WebUserID.ToString & "'"

            Dim dsSettlementStatementFile As DataSet
            Dim drSettlementStatementFile As DataRow
            Dim dbMgr As New Utilities.DBManager(APP_TYPE)
            Dim RowCount As Long = 0

            dsSettlementStatementFile = dbMgr.TransactSql(strSQL, FILL_DATASET)

            If DataSetHasRows(dsSettlementStatementFile) Then

                drSettlementStatementFile = dsSettlementStatementFile.Tables(0).Rows(0)

                RowCount = SafeDBIntField(drSettlementStatementFile, "TheCount")

            End If

            If RowCount > 0 Then
                bReturn = True
            Else
                bReturn = False
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

        Return bReturn

    End Function

    'Fills the collection with those Merchants that have a statement that needs to be generated
    Public Sub FillCollectionWithMerchantsNeedingStatements()
        Dim dbMgr As Utilities.DBManager
        Dim drMerchant As DataRow
        Dim myMerchant As Merchant
        Dim strGetFileTemps As String
        Dim dsMerchants As DataSet = Nothing

        Try

            ' Build the select query...
            strGetFileTemps = "SELECT MerchantID FROM Merchants"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsMerchants = dbMgr.TransactSql(strGetFileTemps, FILL_DATASET)

            'Clear the existing collection
            Me.List.Clear()

            ' Insert each user into the user list collection...
            For Each drMerchant In dsMerchants.Tables(0).Rows

                myMerchant = GetMerchant(drMerchant("Merchantid"))

                Dim NewPeriodStartDate As Date
                Dim NewPeriodEndDate As Date
                If CanGenerateStatement(myMerchant, NewPeriodStartDate, NewPeriodEndDate) Then

                    ' Add Merchant to list...
                    Me.List.Add(myMerchant)

                End If

            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsMerchants Is Nothing) Then
                dsMerchants.Dispose()
            End If
        End Try


    End Sub

    Public Function CanGenerateStatement(ByVal TheMerchant As Merchant, ByRef PeriodStart As Date, ByRef PeriodEnd As Date) As Boolean

        Dim bReturn As Boolean

        Try

            Dim NewPeriodStartDate As Date
            Dim NewPeriodEndDate As Date

            If Not GetNextStatementDates(TheMerchant, NewPeriodStartDate, NewPeriodEndDate) Then
                bReturn = False
            Else
                PeriodStart = NewPeriodStartDate
                PeriodEnd = NewPeriodEndDate
                bReturn = True
            End If

        Catch ex As Exception
            bReturn = False
        End Try

        Return bReturn
    End Function

    Public Function GetNextStatementDates(ByVal TheMerchant As Merchant, ByRef PeriodStart As Date, ByRef PeriodEnd As Date) As Boolean
        Dim bReturn As Boolean

        Try

            'Calculate the new period start date: LastStatementPeriodEndDate + 1 day
            Dim NewPeriodStartDate As Date = TheMerchant.LastStatementPeriodEndDate.AddDays(1)

            'If this merchant has never had a generated statement, then the date will be 1/1/1800
            If NewPeriodStartDate < TheMerchant.MerchantActiveDate Then
                'Set the start date to their active date, which was the day they were entered into the system
                NewPeriodStartDate = TheMerchant.MerchantActiveDate
            End If

            'Calculate the new period end date: The next "StatementPeriodEndDayOfWeek" (from Merchants table) - 7 days
            Dim NewPeriodEndDate As Date
            Dim CurrentDay As DayOfWeek = Now.DayOfWeek

            'Find the most recent StatementPeriodEndDayOfWeek
            Dim i As Short
            For i = 1 To 7
                NewPeriodEndDate = Now.AddDays(i * -1) 'Subtract a day until we get to the right day of the week
                'StatementPeriodEndDayOfWeek: 0 = Sunday, 1 = Monday, etc.
                If NewPeriodEndDate.DayOfWeek = TheMerchant.StatementPeriodEndDayOfWeek Then
                    Exit For
                End If
            Next

            'Subtract 7 days from this date, to allow for enough time to clear transactions
            NewPeriodEndDate = NewPeriodEndDate.AddDays(-7)

            'If the calculated end date is still greater than the period start date,
            'then we have a valid date range for which we can generate a statement
            If NewPeriodEndDate > NewPeriodStartDate Then
                PeriodStart = NewPeriodStartDate
                PeriodEnd = NewPeriodEndDate
                bReturn = True
            Else
                PeriodStart = New Date(1, 1, 1800)
                PeriodEnd = New Date(1, 1, 1800)
                bReturn = False
            End If

        Catch ex As Exception
            bReturn = False
        End Try

        Return bReturn
    End Function

End Class
