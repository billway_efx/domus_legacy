Public Class RecurringItem

#Region "Private class variables"

    'This ID points to a unique record in the RecurringItems table
    Private cInternalRecurringItemID As Guid

    'This identifies the unique Merchant record in the Merchants table
    'This recurring item record belongs to this merchant
    Private cInternalMerchantID As Guid

    'This ID identifies the customer in the Customers table which will 
    'be debited/credited for this transaction
    Private cInternalCustomerID As Guid

    'This is the Merchant's own "friendly" unique identifier for their customer
    'receiving this recurring debit/credit ACH transaction
    Private cMerchantCustomerID As String

    'Customer attributes
    Private cFirstName As String
    Private cLastName As String
    Private cAddress1 As String
    Private cAddress2 As String
    Private cCity As String
    Private cState As String
    Private cZip As String
    Private cSSNumber As String
    Private cDLNumber As String
    Private cDLState As String
    Private cBirthDate As Date
    Private cEmailAddress As String
    Private cPhoneNumber As String

    'Recurrence item attributes
    Private cItemType As TransactionType
    Private cItemFrequency As FrequencyType
    Private cItemAmount As Double

    'Customer Account attributes
    Private cInternalCustomerAccountID 'Identifies this record in the CustomerAccounts table
    Private cRoutingNumber As String
    Private cAccountNumber As String
    Private cAccountType As AccountType

    Private cActiveDate As DateTime
    Private cStatus As ItemStatus

#End Region

    'Reflects the TransactionTypes table
    Public Enum TransactionType As Integer
        Credit = 1
        Debit = 2
    End Enum

    'Reflects the FrequencyTypes table
    Public Enum FrequencyType As Integer
        Monthly = 1
        Quarterly = 2
        Weekly = 3
        Daily = 4
        Every2Weeks = 5
        TwiceMonthly = 6
    End Enum

    Public Enum ItemStatus As Integer
        Enabled = -1
        Disabled = 0
    End Enum

#Region "Class attributes"

    Public Property InternalRecurringItemID() As Guid
        Get
            Return cInternalRecurringItemID
        End Get
        Set(ByVal value As Guid)
            cInternalRecurringItemID = value
        End Set
    End Property
    Public Property InternalMerchantID() As Guid
        Get
            Return cInternalMerchantID
        End Get
        Set(ByVal value As Guid)
            cInternalMerchantID = value
        End Set
    End Property
    Public Property InternalCustomerID() As Guid
        Get
            Return cInternalCustomerID
        End Get
        Set(ByVal value As Guid)
            cInternalCustomerID = value
        End Set
    End Property
    Public Property MerchantCustomerID() As String
        Get
            Return cMerchantCustomerID
        End Get
        Set(ByVal value As String)
            cMerchantCustomerID = value
        End Set
    End Property

    Public Property FirstName() As String
        Get
            Return cFirstName
        End Get
        Set(ByVal value As String)
            cFirstName = value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return cLastName
        End Get
        Set(ByVal value As String)
            cLastName = value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return cAddress1
        End Get
        Set(ByVal value As String)
            cAddress1 = value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return cAddress2
        End Get
        Set(ByVal value As String)
            cAddress2 = value
        End Set
    End Property
    Public Property City() As String
        Get
            Return cCity
        End Get
        Set(ByVal value As String)
            cCity = value
        End Set
    End Property
    Public Property State() As String
        Get
            Return cState
        End Get
        Set(ByVal value As String)
            cState = value
        End Set
    End Property
    Public Property Zip() As String
        Get
            Return cZip
        End Get
        Set(ByVal value As String)
            cZip = value
        End Set
    End Property

    Public Property SSNumber() As String
        Get
            Return cSSNumber
        End Get
        Set(ByVal value As String)
            cSSNumber = value
        End Set
    End Property
    Public Property DLNumber() As String
        Get
            Return cDLNumber
        End Get
        Set(ByVal value As String)
            cDLNumber = value
        End Set
    End Property
    Public Property DLState() As String
        Get
            Return cDLState
        End Get
        Set(ByVal value As String)
            cDLState = value
        End Set
    End Property
    Public Property BirthDate() As Date
        Get
            Return cBirthDate
        End Get
        Set(ByVal value As Date)
            cBirthDate = value
        End Set
    End Property

    Public Property EmailAddress() As String
        Get
            Return cEmailAddress
        End Get
        Set(ByVal value As String)
            cEmailAddress = value
        End Set
    End Property
    Public Property PhoneNumber() As String
        Get
            Return cPhoneNumber
        End Get
        Set(ByVal value As String)
            cPhoneNumber = value
        End Set
    End Property

    Public Property ItemType() As Short
        Get
            Return cItemType
        End Get
        Set(ByVal value As Short)
            cItemType = value
        End Set
    End Property
    Public Property ItemFrequency() As Short
        Get
            Return cItemFrequency
        End Get
        Set(ByVal value As Short)
            cItemFrequency = value
        End Set
    End Property

    Public Property ItemAmount() As Double
        Get
            Return cItemAmount
        End Get
        Set(ByVal value As Double)
            cItemAmount = value
        End Set
    End Property
    Public Property InternalCustomerAccountID() As Guid
        Get
            Return cInternalCustomerAccountID
        End Get
        Set(ByVal value As Guid)
            cInternalCustomerAccountID = value
        End Set
    End Property

    Public Property RoutingNumber() As String
        Get
            Return cRoutingNumber
        End Get
        Set(ByVal value As String)
            cRoutingNumber = value
        End Set
    End Property
    Public Property AccountNumber() As String
        Get
            Return cAccountNumber
        End Get
        Set(ByVal value As String)
            cAccountNumber = value
        End Set
    End Property
    Public Property AccountType() As Integer
        Get
            Return cAccountType
        End Get
        Set(ByVal value As Integer)
            cAccountType = value
        End Set
    End Property

    Public Property ActiveDate() As DateTime
        Get
            Return cActiveDate
        End Get
        Set(ByVal value As DateTime)
            cActiveDate = value
        End Set
    End Property

    Public Property Status() As Short
        Get
            Return cStatus
        End Get
        Set(ByVal value As Short)
            cStatus = value
        End Set
    End Property

#End Region

#Region "Public Class Methods"

    'Default constructor
    Public Sub New()
        cInternalRecurringItemID = Guid.Empty
        cInternalMerchantID = Guid.Empty
        cInternalCustomerID = Guid.Empty
        cMerchantCustomerID = ""
        cFirstName = ""
        cLastName = ""
        cAddress1 = ""
        cAddress2 = ""
        cCity = ""
        cState = ""
        cZip = ""
        cSSNumber = ""
        cDLNumber = ""
        cDLState = ""
        cBirthDate = New Date(1800, 1, 1)
        cEmailAddress = ""
        cPhoneNumber = "(000) 000-0000"
        cItemType = TransactionType.Debit
        cItemFrequency = FrequencyType.Monthly
        cItemAmount = 0
        cInternalCustomerAccountID = Guid.Empty
        cRoutingNumber = ""
        cAccountNumber = ""
        cAccountType = DbConstants.AccountType.Checking
        cActiveDate = New Date(1800, 1, 1)
        cStatus = ItemStatus.Disabled
    End Sub

#End Region

End Class
