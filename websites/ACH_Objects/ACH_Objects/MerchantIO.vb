'This is a pared down version of the Merchant class, removing the population of the WebUser and
'Contact objects.
'It is intended to be used for quickly retrieving the objects needed for reading locations of FTP
'directories, or anything else related to IO and customers. 

Public Class MerchantIO

#Region "Private class variables"

    'This ID identifies a unique record in the Merchants table
    Private cInternalMerchantID As Guid

    ''This identifies the WebUser associated with this Merchant
    ''The same WebUser can be associated with one or more Merchants
    'Private cWebUser As WebUser

    'This is the user friendly Merchant ID value known to the Merchant
    Private cUserMerchantID As String

    'Merchant attributes

    Private cClearDays As Int16
    Private cProductDescription As String
    Private cProductPrice As String
    Private cProductSaleMethod As String
    Private cProductCreditsAllowedFlag As Boolean
    Private cProductMaxCrDrRatio As Double
    Private cTransactionDescriptor As String
    Private cTransactionCSNumber As String
    Private cTransactionSecDr As String
    Private cTransactionSecCr As String
    Private cAssignedODFIID As Guid
    Private cAccountVerificationServiceFlag As Boolean
    Private cIDVerificationServiceFlag As Boolean
    Private cEnableTransactionProcessingFlag As Boolean
    Private cReportFormat As String
    Private cReportDeliveryFTP As String
    Private cReportDeliveryEmail As String

    Private cEFXSalesRep As String

    Private cFTPFolder As String

    Private cDrTxnSubmitFee As Double
    Private cDrTxnRejectFee As Double
    Private cDrTxnClearFee As Double
    Private cDrTxnReturnFee As Double
    Private cDrTxnChargeBackFee As Double

    Private cCrTxnSubmitFee As Double
    Private cCrTxnRejectFee As Double
    Private cCrTxnClearFee As Double
    Private cCrTxnReturnFee As Double
    Private cCrTxnChargeBackFee As Double

    Private cAccountVerificationFee As Double
    Private cTxnSettlementFee As Double
    Private cIDVerificationFee As Double

    Private cClearedValueFeePercentage As Double
    Private cSettlementBankName As String
    Private cSettlementBankABA As String
    Private cSettlementBankDDA As String

    Private cReservesRate As Double
    Private cReservesPeriod As Integer

    Private cMerchantActiveDate As Date
    Private cSubmissionFileType As String
    Private cCompanyNumber As String
    'Private cContacts As ContactInfoList

    'Added option to allow duplicate transactions, configured per Merchant
    Private cAllowDuplicateTransactions As Boolean

#End Region

#Region "Class attributes"
    Public Property InternalMerchantID() As Guid
        Get
            Return cInternalMerchantID
        End Get
        Set(ByVal value As Guid)
            cInternalMerchantID = value
        End Set
    End Property
    'Public Property WebUser() As WebUser
    '    Get
    '        Return cWebUser
    '    End Get
    '    Set(ByVal value As WebUser)
    '        cWebUser = value
    '    End Set
    'End Property
    Public Property UserMerchantID() As String
        Get
            Return cUserMerchantID
        End Get
        Set(ByVal value As String)
            cUserMerchantID = value
        End Set
    End Property
    Public Property ClearDays() As Int16
        Get
            Return cClearDays
        End Get
        Set(ByVal value As Int16)
            cClearDays = value
        End Set
    End Property
    Public Property ProductDescription() As String
        Get
            Return cProductDescription
        End Get
        Set(ByVal value As String)
            cProductDescription = value
        End Set
    End Property
    Public Property ProductPrice() As String
        Get
            Return cProductPrice
        End Get
        Set(ByVal value As String)
            cProductPrice = value
        End Set
    End Property
    Public Property ProductSaleMethod() As String
        Get
            Return cProductSaleMethod
        End Get
        Set(ByVal value As String)
            cProductSaleMethod = value
        End Set
    End Property
    Public Property ProductCreditsAllowedFlag() As Boolean
        Get
            Return cProductCreditsAllowedFlag
        End Get
        Set(ByVal value As Boolean)
            cProductCreditsAllowedFlag = value
        End Set
    End Property
    Public Property ProductMaxCrDrRatio() As Double
        Get
            Return cProductMaxCrDrRatio
        End Get
        Set(ByVal value As Double)
            cProductMaxCrDrRatio = value
        End Set
    End Property
    Public Property TransactionDescriptor() As String
        Get
            Return cTransactionDescriptor
        End Get
        Set(ByVal value As String)
            cTransactionDescriptor = value
        End Set
    End Property
    Public Property TransactionCSNumber() As String
        Get
            Return cTransactionCSNumber
        End Get
        Set(ByVal value As String)
            cTransactionCSNumber = value
        End Set
    End Property
    Public Property TransactionSecDr() As String
        Get
            Return cTransactionSecDr
        End Get
        Set(ByVal value As String)
            cTransactionSecDr = value
        End Set
    End Property
    Public Property TransactionSecCr() As String
        Get
            Return cTransactionSecCr
        End Get
        Set(ByVal value As String)
            cTransactionSecCr = value
        End Set
    End Property
    Public Property AssignedODFIID() As Guid
        Get
            Return cAssignedODFIID
        End Get
        Set(ByVal value As Guid)
            cAssignedODFIID = value
        End Set
    End Property
    Public Property AccountVerificationServiceFlag() As Boolean
        Get
            Return cAccountVerificationServiceFlag
        End Get
        Set(ByVal value As Boolean)
            cAccountVerificationServiceFlag = value
        End Set
    End Property
    Public Property IDVerificationServiceFlag() As Boolean
        Get
            Return cIDVerificationServiceFlag
        End Get
        Set(ByVal value As Boolean)
            cIDVerificationServiceFlag = value
        End Set
    End Property
    Public Property EnableTransactionProcessingFlag() As Boolean
        Get
            Return cEnableTransactionProcessingFlag
        End Get
        Set(ByVal value As Boolean)
            cEnableTransactionProcessingFlag = value
        End Set
    End Property
    Public Property ReportFormat() As String
        Get
            Return cReportFormat
        End Get
        Set(ByVal value As String)
            cReportFormat = value
        End Set
    End Property
    Public Property ReportDeliveryFTP() As String
        Get
            Return cReportDeliveryFTP
        End Get
        Set(ByVal value As String)
            cReportDeliveryFTP = value
        End Set
    End Property
    Public Property ReportDeliveryEmail() As String
        Get
            Return cReportDeliveryEmail
        End Get
        Set(ByVal value As String)
            cReportDeliveryEmail = value
        End Set
    End Property
    Public Property EFXSalesRep() As String
        Get
            Return cEFXSalesRep
        End Get
        Set(ByVal value As String)
            cEFXSalesRep = value
        End Set
    End Property
    Public Property FTPFolder() As String
        Get
            Return cFTPFolder
        End Get
        Set(ByVal value As String)
            cFTPFolder = value
        End Set
    End Property
    Public Property DrTxnSubmitFee() As Double
        Get
            Return cDrTxnSubmitFee
        End Get
        Set(ByVal value As Double)
            cDrTxnSubmitFee = value
        End Set
    End Property
    Public Property DrTxnRejectFee() As Double
        Get
            Return cDrTxnRejectFee
        End Get
        Set(ByVal value As Double)
            cDrTxnRejectFee = value
        End Set
    End Property
    Public Property DrTxnClearFee() As Double
        Get
            Return cDrTxnClearFee
        End Get
        Set(ByVal value As Double)
            cDrTxnClearFee = value
        End Set
    End Property
    Public Property DrTxnReturnFee() As Double
        Get
            Return cDrTxnReturnFee
        End Get
        Set(ByVal value As Double)
            cDrTxnReturnFee = value
        End Set
    End Property
    Public Property DrTxnChargeBackFee() As Double
        Get
            Return cDrTxnChargeBackFee
        End Get
        Set(ByVal value As Double)
            cDrTxnChargeBackFee = value
        End Set
    End Property
    Public Property CrTxnSubmitFee() As Double
        Get
            Return cCrTxnSubmitFee
        End Get
        Set(ByVal value As Double)
            cCrTxnSubmitFee = value
        End Set
    End Property
    Public Property CrTxnRejectFee() As Double
        Get
            Return cCrTxnRejectFee
        End Get
        Set(ByVal value As Double)
            cCrTxnRejectFee = value
        End Set
    End Property
    Public Property CrTxnClearFee() As Double
        Get
            Return cCrTxnClearFee
        End Get
        Set(ByVal value As Double)
            cCrTxnClearFee = value
        End Set
    End Property
    Public Property CrTxnReturnFee() As Double
        Get
            Return cCrTxnReturnFee
        End Get
        Set(ByVal value As Double)
            cCrTxnReturnFee = value
        End Set
    End Property
    Public Property CrTxnChargeBackFee() As Double
        Get
            Return cCrTxnChargeBackFee
        End Get
        Set(ByVal value As Double)
            cCrTxnChargeBackFee = value
        End Set
    End Property
    Public Property AccountVerificationFee() As Double
        Get
            Return cAccountVerificationFee
        End Get
        Set(ByVal value As Double)
            cAccountVerificationFee = value
        End Set
    End Property
    Public Property TxnSettlementFee() As Double
        Get
            Return cTxnSettlementFee
        End Get
        Set(ByVal value As Double)
            cTxnSettlementFee = value
        End Set
    End Property
    Public Property IDVerificationFee() As Double
        Get
            Return cIDVerificationFee
        End Get
        Set(ByVal value As Double)
            cIDVerificationFee = value
        End Set
    End Property
    Public Property ClearedValueFeePercentage() As Double
        Get
            Return cClearedValueFeePercentage
        End Get
        Set(ByVal value As Double)
            cClearedValueFeePercentage = value
        End Set
    End Property
    Public Property SettlementBankName() As String
        Get
            Return cSettlementBankName
        End Get
        Set(ByVal value As String)
            cSettlementBankName = value
        End Set
    End Property
    Public Property SettlementBankABA() As String
        Get
            Return cSettlementBankABA
        End Get
        Set(ByVal value As String)
            cSettlementBankABA = value
        End Set
    End Property
    Public Property SettlementBankDDA() As String
        Get
            Return cSettlementBankDDA
        End Get
        Set(ByVal value As String)
            cSettlementBankDDA = value
        End Set
    End Property
    Public Property ReservesRate() As Double
        Get
            Return cReservesRate
        End Get
        Set(ByVal value As Double)
            cReservesRate = value
        End Set
    End Property
    Public Property ReservesPeriod() As Int16
        Get
            Return cReservesPeriod
        End Get
        Set(ByVal value As Int16)
            cReservesPeriod = value
        End Set
    End Property
    Public Property MerchantActiveDate() As Date
        Get
            Return cMerchantActiveDate
        End Get
        Set(ByVal value As Date)
            cMerchantActiveDate = value
        End Set
    End Property
    Public Property SubmissionFileType() As String
        Get
            Return cSubmissionFileType
        End Get
        Set(ByVal value As String)
            cSubmissionFileType = value
        End Set
    End Property
    Public Property CompanyNumber() As String
        Get
            Return cCompanyNumber
        End Get
        Set(ByVal value As String)
            cCompanyNumber = value
        End Set
    End Property

    'Public Property Contacts() As ContactInfoList
    '    Get
    '        Return cContacts
    '    End Get
    '    Set(ByVal value As ContactInfoList)
    '        cContacts = value
    '    End Set
    'End Property

    Public Property AllowDuplicateTransactions() As Boolean
        Get
            Return cAllowDuplicateTransactions
        End Get
        Set(ByVal value As Boolean)
            cAllowDuplicateTransactions = value
        End Set
    End Property
#End Region

#Region "Public Class Methods"

    'Default constructor
    Public Sub New()
        cInternalMerchantID = Guid.Empty
        'cWebUser = New WebUser
        cUserMerchantID = ""
        cClearDays = 10
        cProductDescription = ""
        cProductPrice = 0
        cProductSaleMethod = ""
        cProductCreditsAllowedFlag = False
        cProductMaxCrDrRatio = 0
        cTransactionDescriptor = ""
        cTransactionCSNumber = "(000) 000-0000"
        cTransactionSecDr = ""
        cTransactionSecCr = ""
        cAssignedODFIID = Guid.Empty
        cAccountVerificationServiceFlag = False
        cIDVerificationServiceFlag = False
        cEnableTransactionProcessingFlag = False
        cReportFormat = ""
        cReportDeliveryFTP = ""
        cReportDeliveryEmail = ""
        cEFXSalesRep = ""
        cFTPFolder = ""
        cDrTxnSubmitFee = 0
        cDrTxnRejectFee = 0
        cDrTxnClearFee = 0
        cDrTxnReturnFee = 0
        cDrTxnChargeBackFee = 0
        cCrTxnSubmitFee = 0
        cCrTxnRejectFee = 0
        cCrTxnClearFee = 0
        cCrTxnReturnFee = 0
        cCrTxnChargeBackFee = 0
        cAccountVerificationFee = 0
        cTxnSettlementFee = 0
        cIDVerificationFee = 0
        cClearedValueFeePercentage = 0
        cSettlementBankName = ""
        cSettlementBankABA = ""
        cSettlementBankDDA = ""
        cReservesRate = 0
        cReservesPeriod = 150
        cSubmissionFileType = "EFX"
        cCompanyNumber = ""
        cAllowDuplicateTransactions = False
    End Sub

    'Populate new instance from Merchants table, based on MerchantID. Will be empty if record ID doesn't exist.
    Public Sub New(ByVal MerchantID As Guid)
        Dim objMerchantList As New MerchantIOList
        Dim objMerchant As MerchantIO
        objMerchant = objMerchantList.GetMerchant(MerchantID)

        Call FillMerchantObject(objMerchant)
    End Sub

    'Populate new instance from Merchants table, based on UserMerchantID. Will be empty if record ID doesn't exist.
    Public Sub New(ByVal UserMerchantID As String)
        Dim objMerchantList As New MerchantIOList
        Dim objMerchant As MerchantIO
        objMerchant = objMerchantList.GetMerchant(UserMerchantID)

        Call FillMerchantObject(objMerchant)
    End Sub

    Private Sub FillMerchantObject(ByVal TheMerchant As MerchantIO)
        cInternalMerchantID = TheMerchant.InternalMerchantID
        'cWebUser = TheMerchant.WebUser
        cUserMerchantID = TheMerchant.UserMerchantID
        cClearDays = TheMerchant.ClearDays
        cProductDescription = TheMerchant.ProductDescription
        cProductPrice = TheMerchant.ProductPrice
        cProductSaleMethod = TheMerchant.ProductSaleMethod
        cProductCreditsAllowedFlag = TheMerchant.ProductCreditsAllowedFlag
        cProductMaxCrDrRatio = TheMerchant.ProductMaxCrDrRatio
        cTransactionDescriptor = TheMerchant.TransactionDescriptor
        cTransactionCSNumber = TheMerchant.TransactionCSNumber
        cTransactionSecDr = TheMerchant.TransactionSecDr
        cTransactionSecCr = TheMerchant.TransactionSecCr
        cAssignedODFIID = TheMerchant.AssignedODFIID
        cAccountVerificationServiceFlag = TheMerchant.AccountVerificationServiceFlag
        cIDVerificationServiceFlag = TheMerchant.IDVerificationServiceFlag
        cEnableTransactionProcessingFlag = TheMerchant.EnableTransactionProcessingFlag
        cReportFormat = TheMerchant.ReportFormat
        cReportDeliveryFTP = TheMerchant.ReportDeliveryFTP
        cReportDeliveryEmail = TheMerchant.ReportDeliveryEmail
        cEFXSalesRep = TheMerchant.EFXSalesRep
        cFTPFolder = TheMerchant.FTPFolder
        cDrTxnSubmitFee = TheMerchant.DrTxnSubmitFee
        cDrTxnRejectFee = TheMerchant.DrTxnRejectFee
        cDrTxnClearFee = TheMerchant.DrTxnClearFee
        cDrTxnReturnFee = TheMerchant.DrTxnReturnFee
        cDrTxnChargeBackFee = TheMerchant.DrTxnChargeBackFee
        cCrTxnSubmitFee = TheMerchant.CrTxnSubmitFee
        cCrTxnRejectFee = TheMerchant.CrTxnRejectFee
        cCrTxnClearFee = TheMerchant.CrTxnClearFee
        cCrTxnReturnFee = TheMerchant.CrTxnReturnFee
        cCrTxnChargeBackFee = TheMerchant.CrTxnChargeBackFee
        cAccountVerificationFee = TheMerchant.AccountVerificationFee
        cTxnSettlementFee = TheMerchant.TxnSettlementFee
        cIDVerificationFee = TheMerchant.IDVerificationFee
        cClearedValueFeePercentage = TheMerchant.ClearedValueFeePercentage
        cSettlementBankName = TheMerchant.SettlementBankName
        cSettlementBankABA = TheMerchant.SettlementBankABA
        cSettlementBankDDA = TheMerchant.SettlementBankDDA
        cReservesRate = TheMerchant.ReservesRate
        cReservesPeriod = TheMerchant.ReservesPeriod
        cMerchantActiveDate = TheMerchant.MerchantActiveDate
        cSubmissionFileType = TheMerchant.SubmissionFileType
        cCompanyNumber = TheMerchant.CompanyNumber
        'cContacts = TheMerchant.Contacts
        cAllowDuplicateTransactions = TheMerchant.AllowDuplicateTransactions
    End Sub

#End Region

End Class
