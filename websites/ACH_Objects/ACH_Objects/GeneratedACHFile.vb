Public Class GeneratedACHFile

#Region "Private class variables"

    'This ID points to a unique record in the OutgoingODFIFiles table
    Private cFileID As Guid

    'File attributes
    Private cFileDate As DateTime
    Private cFileName As String
    Private cFileSize As Long
    Private cFileStatus As Integer
    Private cODFIId As Guid
    Private cODFIName As String
#End Region

#Region "Class attributes"

    Public Property FileID() As Guid
        Get
            Return cFileID
        End Get
        Set(ByVal value As Guid)
            cFileID = value
        End Set
    End Property
    Public Property FileDate() As DateTime
        Get
            Return cFileDate
        End Get
        Set(ByVal value As DateTime)
            cFileDate = value
        End Set
    End Property
    Public Property FileName() As String
        Get
            Return cFileName
        End Get
        Set(ByVal value As String)
            cFileName = value
        End Set
    End Property
    Public Property FileSize() As Long
        Get
            Return cFileSize
        End Get
        Set(ByVal value As Long)
            cFileSize = value
        End Set
    End Property
    Public Property FileStatus() As Integer
        Get
            Return cFileStatus
        End Get
        Set(ByVal value As Integer)
            cFileStatus = value
        End Set
    End Property
    Public Property ODFIId() As Guid
        Get
            Return cODFIId
        End Get
        Set(ByVal value As Guid)
            cODFIId = value
        End Set
    End Property
    Public Property ODFIName() As String
        Get
            Return cODFIName
        End Get
        Set(ByVal value As String)
            cODFIName = value
        End Set
    End Property

#End Region

#Region "Public Class Methods"

    'Default constructor
    Public Sub New()
        cFileID = Guid.Empty
        cFileDate = New Date(1800, 1, 1)
        cFileName = ""
        cFileSize = 0
        cFileStatus = 1
        cODFIId = Guid.Empty
        cODFIName = ""
    End Sub


#End Region

End Class
