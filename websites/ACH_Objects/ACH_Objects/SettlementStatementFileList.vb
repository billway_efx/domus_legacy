Imports Microsoft.ApplicationBlocks.ExceptionManagement

Public Class SettlementStatementFileList

    Inherits CollectionBase

    'Return a populated SettlementStatementFile object for the specified FileID GUID
    Public Function GetFile(ByVal FileID As Guid) As SettlementStatementFile
        Dim strSQL As String
        Dim objSettlementStatementFile As New SettlementStatementFile

        Try

            strSQL = _
               "SELECT FileID, MerchantID, TimeCreated, FileSize, Status, " & _
               "PeriodStartDate, PeriodEndDate, Description, ReportDataID " & _
               "FROM SettlementStatementFiles " & _
               "WHERE FileID = '" & FileID.ToString & "'"

            objSettlementStatementFile = GetSettlementStatementFileHelper(strSQL)
            Return objSettlementStatementFile

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    'Call the database and populate the object
    Private Function GetSettlementStatementFileHelper(ByVal TheSql As String) As SettlementStatementFile
        Dim dsSettlementStatementFile As DataSet
        Dim drSettlementStatementFile As DataRow
        Dim objSettlementStatementFile As New SettlementStatementFile
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)

        Try

            dsSettlementStatementFile = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(dsSettlementStatementFile) Then

                drSettlementStatementFile = dsSettlementStatementFile.Tables(0).Rows(0)

                ' Build SettlementStatementFile object
                objSettlementStatementFile.FileID = SafeDBGuidField(drSettlementStatementFile, "FileID")
                objSettlementStatementFile.MerchantID = SafeDBGuidField(drSettlementStatementFile, "MerchantID")
                objSettlementStatementFile.TimeCreated = SafeDBDateTimeField(drSettlementStatementFile, "TimeCreated")
                objSettlementStatementFile.FileSize = SafeDBBigIntField(drSettlementStatementFile, "FileSize")
                objSettlementStatementFile.Status = SafeDBIntField(drSettlementStatementFile, "Status")
                objSettlementStatementFile.PeriodStartDate = SafeDBDateTimeField(drSettlementStatementFile, "PeriodStartDate")
                objSettlementStatementFile.PeriodEndDate = SafeDBDateTimeField(drSettlementStatementFile, "PeriodEndDate")
                objSettlementStatementFile.Description = SafeDBStringField(drSettlementStatementFile, "Description")
                objSettlementStatementFile.ReportDataID = SafeDBGuidField(drSettlementStatementFile, "ReportDataID")
            End If

            Return objSettlementStatementFile

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    'Populate the collection with all the Files for the specified Merchant GUID
    Public Sub FillCollection(ByVal MerchantID As Guid)
        Dim dbMgr As Utilities.DBManager
        Dim drReturnFile As DataRow
        Dim myReturnFile As SettlementStatementFile
        Dim strSQL As String
        Dim dsReturnFiles As DataSet = Nothing

        Try

            Me.List.Clear()

            ' Build the select query...
            strSQL = "SELECT FileID FROM SettlementStatementFiles where MerchantID = '" & _
                MerchantID.ToString & "' order by TimeCreated desc"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsReturnFiles = dbMgr.TransactSql(strSQL, FILL_DATASET)

            ' Insert each user into the user list collection...
            For Each drReturnFile In dsReturnFiles.Tables(0).Rows

                myReturnFile = GetFile(drReturnFile("FileID"))

                ' Add file to list...
                Me.List.Add(myReturnFile)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsReturnFiles Is Nothing) Then
                dsReturnFiles.Dispose()
            End If
        End Try
    End Sub

    Public Function GetLast(ByVal HowMany As Integer) As DataSet
        Dim dbMgr As Utilities.DBManager
        Dim strSQL As String
        Dim TheDataset As DataSet = Nothing

        Try

            Me.List.Clear()

            ' Build the select query...
            strSQL = "SELECT top " & HowMany.ToString & " b.UserMerchantID AS Merchant, " & _
                "a.Description, a.TimeCreated, a.FileID " & _
                "FROM SettlementStatementFiles AS a INNER JOIN " & _
                      "Merchants AS b ON a.MerchantID = b.MerchantID " & _
                "ORDER BY a.TimeCreated DESC"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            TheDataset = dbMgr.TransactSql(strSQL, FILL_DATASET)

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally

        End Try

        Return TheDataset
    End Function

    'Return a dataset containing all the rows currently in the collection
    Public Function GetDataset() As DataSet

        Dim MyDataset As New DataSet
        Dim MyTable As New DataTable("SettlementStatementFiles")

        MyTable.Columns.Add("FileID", GetType(Guid))
        MyTable.Columns.Add("MerchantID", GetType(Guid))
        MyTable.Columns.Add("TimeCreated", GetType(DateTime))
        MyTable.Columns.Add("FileSize", GetType(Long))
        MyTable.Columns.Add("Status", GetType(Int16))
        MyTable.Columns.Add("PeriodStartDate", GetType(DateTime))
        MyTable.Columns.Add("PeriodEndDate", GetType(DateTime))
        MyTable.Columns.Add("Description", GetType(String))
        MyTable.Columns.Add("ReportDataID", GetType(Guid))

        Dim TheRow(8) As Object
        Dim TF As SettlementStatementFile
        For Each TF In Me
            TheRow(0) = TF.FileID
            TheRow(1) = TF.MerchantID
            TheRow(2) = TF.TimeCreated
            TheRow(3) = TF.FileSize
            TheRow(4) = TF.Status
            TheRow(5) = TF.PeriodStartDate
            TheRow(6) = TF.PeriodEndDate
            TheRow(7) = TF.Description
            TheRow(8) = TF.ReportDataID
            MyTable.Rows.Add(TheRow)
        Next

        MyDataset.Tables.Add(MyTable)
        Return MyDataset

    End Function

    'Returns true if the specified WebUser has authorized access to this Settlement Statement FileID
    Public Function AuthorizeFileAccess(ByVal WebUser As WebUser, ByVal FileID As Guid) As Boolean
        Dim bReturn As Boolean = False

        Try

            Dim strSQL As String

            If WebUser.AdminFlag Then
                bReturn = True
            Else

                strSQL = _
                   "SELECT count(*) TheCount " & _
                   "FROM SettlementStatementFiles s, Merchants m " & _
                   "WHERE FileID = '" & FileID.ToString & "' and " & _
                   "m.MerchantID = s.MerchantID and " & _
                   "m.WebUserID = '" & WebUser.WebUserID.ToString & "'"

                Dim dsSettlementStatementFile As DataSet
                Dim drSettlementStatementFile As DataRow
                Dim dbMgr As New Utilities.DBManager(APP_TYPE)
                Dim RowCount As Long = 0

                dsSettlementStatementFile = dbMgr.TransactSql(strSQL, FILL_DATASET)

                If DataSetHasRows(dsSettlementStatementFile) Then

                    drSettlementStatementFile = dsSettlementStatementFile.Tables(0).Rows(0)

                    RowCount = SafeDBIntField(drSettlementStatementFile, "TheCount")

                End If

                If RowCount > 0 Then
                    bReturn = True
                Else
                    bReturn = False
                End If

            End If


        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

        Return bReturn

    End Function


End Class
