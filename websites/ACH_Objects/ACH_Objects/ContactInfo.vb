Public Class ContactInfo

#Region "Private class variables"
    Private cContactID As Guid
    Private cParentID As Guid
    Private cContactType As Integer
    Private cCompanyName As String
    Private cFirstName As String
    Private cLastName As String
    Private cAddress1 As String
    Private cAddress2 As String
    Private cCity As String
    Private cState As String
    Private cZip As String
    Private cPrimaryPhone As String
    Private cCellPhone As String
    Private cTitle As String
    Private cEmail As String
#End Region

    'Reflects the ContactTypes table
    Public Enum ContactTypes As Integer
        ClientInformation = 1
        OperationalContact = 2
        PrincipalsInformation = 3
        TechnicalContact = 4
        SettlementContact = 5
        WebUserInformation = 6
        Other = 7
    End Enum

#Region "Class attributes"
    Public Property ContactID() As Guid
        Get
            Return cContactID
        End Get
        Set(ByVal value As Guid)
            cContactID = value
        End Set
    End Property
    Public Property ParentID() As Guid
        Get
            Return cParentID
        End Get
        Set(ByVal value As Guid)
            cParentID = value
        End Set
    End Property
    Public Property ContactType() As ContactTypes
        Get
            Return cContactType
        End Get
        Set(ByVal value As ContactTypes)
            cContactType = value
        End Set
    End Property
    Public Property CompanyName() As String
        Get
            Return cCompanyName
        End Get
        Set(ByVal value As String)
            cCompanyName = value
        End Set
    End Property
    Public Property FirstName() As String
        Get
            Return cFirstName
        End Get
        Set(ByVal value As String)
            cFirstName = value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return cLastName
        End Get
        Set(ByVal value As String)
            cLastName = value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return cAddress1
        End Get
        Set(ByVal value As String)
            cAddress1 = value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return cAddress2
        End Get
        Set(ByVal value As String)
            cAddress2 = value
        End Set
    End Property
    Public Property City() As String
        Get
            Return cCity
        End Get
        Set(ByVal value As String)
            cCity = value
        End Set
    End Property
    Public Property State() As String
        Get
            Return cState
        End Get
        Set(ByVal value As String)
            cState = value
        End Set
    End Property
    Public Property Zip() As String
        Get
            Return cZip
        End Get
        Set(ByVal value As String)
            cZip = value
        End Set
    End Property
    Public Property PrimaryPhone() As String
        Get
            Return cPrimaryPhone
        End Get
        Set(ByVal value As String)
            cPrimaryPhone = value
        End Set
    End Property
    Public Property CellPhone() As String
        Get
            Return cCellPhone
        End Get
        Set(ByVal value As String)
            cCellPhone = value
        End Set
    End Property
    Public Property Title() As String
        Get
            Return cTitle
        End Get
        Set(ByVal value As String)
            cTitle = value
        End Set
    End Property
    Public Property Email() As String
        Get
            Return cEmail
        End Get
        Set(ByVal value As String)
            cEmail = value
        End Set
    End Property
#End Region

#Region "Public Class Methods"

    'Default constructor
    Public Sub New()
        cContactID = Guid.Empty
        cParentID = Guid.Empty
        cContactType = 0
        cCompanyName = ""
        cFirstName = ""
        cLastName = ""
        cAddress1 = ""
        cAddress2 = ""
        cCity = ""
        cState = ""
        cZip = ""
        cPrimaryPhone = "(000) 000-0000"
        cCellPhone = "(000) 000-0000"
        cTitle = ""
        cEmail = ""
    End Sub

#End Region

End Class
