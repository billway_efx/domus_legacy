Imports System.Web.Security

Public Class WebUserOffsetMerchant

#Region "Private class variables"
    Private cWebUserOffsetMerchantID As Guid
    Private cWebUserID As Guid
    Private cMerchantID As Guid
    Private cUserMerchantID As String
#End Region

#Region "Class attributes"
    Public Property WebUserOffsetMerchantID() As Guid
        Get
            Return cWebUserOffsetMerchantID
        End Get
        Set(ByVal value As Guid)
            cWebUserOffsetMerchantID = value
        End Set
    End Property
    Public Property WebUserID() As Guid
        Get
            Return cWebUserID
        End Get
        Set(ByVal value As Guid)
            cWebUserID = value
        End Set
    End Property
    Public Property MerchantID() As Guid
        Get
            Return cMerchantID
        End Get
        Set(ByVal value As Guid)
            cMerchantID = value
        End Set
    End Property
    Public Property UserMerchantID() As String
        Get
            Return cUserMerchantID
        End Get
        Set(ByVal value As String)
            cUserMerchantID = value
        End Set
    End Property

#End Region

#Region "Public Class Methods"

    'Default constructor
    Public Sub New()
        cWebUserOffsetMerchantID = Guid.Empty
        cWebUserID = Guid.Empty
        cMerchantID = Guid.Empty
        cUserMerchantID = ""
    End Sub

    Private Sub FillWebUserObject(ByVal WebUserOffsetMerchant As WebUserOffsetMerchant)
        cWebUserOffsetMerchantID = WebUserOffsetMerchant.WebUserOffsetMerchantID
        cWebUserID = WebUserOffsetMerchant.WebUserID
        cMerchantID = WebUserOffsetMerchant.MerchantID
        cUserMerchantID = WebUserOffsetMerchant.UserMerchantID
    End Sub
#End Region

End Class
