Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Security.Cryptography
Imports System.IO
Imports System.Collections.Specialized

Public Class Data

    Public Const ENVIRONMENTS_CONFIG_SECTION = "environments"

    Private msServer As String = ""
    Private msUsername As String = ""
    Private msPassword As String = ""
    Private msDatabase As String = ""

    Public moConnection As SqlConnection
    Private UseTransaction As Boolean = False

    Public ReadOnly Property Connection() As SqlConnection
        Get
            Return moConnection
        End Get
    End Property
    'Public Property Server() As String
    '    Get
    '        Return msServer
    '    End Get
    '    Set(ByVal Value As String)
    '        msServer = Value
    '    End Set
    'End Property
    'Public Property Username() As String
    '    Get
    '        Return msUsername
    '    End Get
    '    Set(ByVal Value As String)
    '        msUsername = Value
    '    End Set
    'End Property
    'Public Property Password() As String
    '    Get
    '        Return msPassword
    '    End Get
    '    Set(ByVal Value As String)
    '        msPassword = Value
    '    End Set
    'End Property
    'Public Property Database() As String
    '    Get
    '        Return msDatabase
    '    End Get
    '    Set(ByVal Value As String)
    '        msDatabase = Value
    '    End Set
    'End Property

    Public Function ConnectionString() As String

        Dim sBuffer As String = ""

        'sBuffer += "Server=" & msServer & ";"
        'sBuffer += "Initial Catalog=" & msDatabase & ";"
        'sBuffer += "User ID=" & msUsername & ";"
        'sBuffer += "Password=" & msPassword & ";"
        sBuffer = _
            ConfigurationManager.ConnectionStrings("ACHAcquireConnectionString").ToString()

        '"DRIVER=SQL Server;SERVER=(servername|ip);UID=(username);PWD=(password);APP=Visual Basic;DATABASE=(databasename);"

        Return sBuffer

    End Function
    Public Function [Open]() As Boolean

        Me.Close()

        moConnection = New SqlConnection(ConnectionString())

        Try

            moConnection.Open()

        Catch exp As Exception

            System.Diagnostics.Debug.WriteLine("Type : " & exp.GetType().ToString)
            System.Diagnostics.Debug.WriteLine("Info : " & exp.Message.ToString)

            Throw New Exception(exp.Message.ToString, exp.InnerException)

        End Try

        If moConnection.State = ConnectionState.Open Then
            Return True
        End If

    End Function
    Public Function [Close]() As Boolean

        If Not (moConnection Is Nothing) Then
            If moConnection.State <> ConnectionState.Closed Then
                moConnection.Close()
            End If
        End If

        moConnection = Nothing

    End Function
    Public Function IsOpen() As Boolean
        If Not (moConnection Is Nothing) Then
            If moConnection.State = ConnectionState.Open Then
                Return True
            End If
        End If
    End Function

    Public Sub ExecuteNonQuery(ByVal sSQL As String)
        If Me.IsOpen = False Then Me.Open()
        Dim oCommand As New SqlCommand(sSQL, moConnection)
        oCommand.CommandType = CommandType.Text
        oCommand.ExecuteNonQuery()
        Me.Close()
    End Sub
    Public Sub ExecuteNonQuery(ByVal sSQL As String, ByVal CommandType As System.Data.CommandType)
        If Me.IsOpen = False Then Me.Open()
        Dim oCommand As New SqlCommand(sSQL, moConnection)
        oCommand.CommandType = CommandType
        oCommand.ExecuteNonQuery()
        Me.Close()
    End Sub
    Public Function ExecuteNonQuery(ByVal oCommand As SqlCommand) As Integer

        'Returns the number of records affected.
        'Raises and exception if there was an error

        Dim iRecordsAffected As Integer = 0

        'Is the connection open?
        If Me.IsOpen = False Then Me.Open()

        If Me.IsOpen() Then

            'Yes - is the command object NULL?
            If Not (oCommand Is Nothing) Then

                'No - set the connection up on it
                oCommand.Connection = moConnection

                Try
                    'Execute the query
                    iRecordsAffected = oCommand.ExecuteNonQuery()

                Catch exp As Exception

                    'Write debug info
                    System.Diagnostics.Debug.WriteLine("Type : " & exp.GetType().ToString)
                    System.Diagnostics.Debug.WriteLine("Info : " & exp.Message.ToString)

                    'Throw exception with useful error data
                    Throw New Exception(exp.Message.ToString, exp.InnerException)

                End Try

            End If
        End If

        'Destroy command (if it was created)
        oCommand = Nothing

        If UseTransaction = False Then Me.Close()

        Return iRecordsAffected

    End Function
    Public Function ExecuteDataSet(ByVal sSQL As String) As System.Data.DataSet
        If Me.IsOpen = False Then Me.Open()

        Dim da As New SqlDataAdapter(sSQL, moConnection)
        Dim ds As New DataSet 'We create our own DataSet to store the DataAdapter, so the user can update the database later.

        da.Fill(ds, "data")
        If UseTransaction = False Then Me.Close()
        Return ds

    End Function
    Public Function ExecuteReader(ByVal sSQL As String, ByVal CommandType As System.Data.CommandType) As SqlDataReader

        Dim oCommand As New SqlCommand(sSQL)

        oCommand.CommandText = sSQL

        oCommand.CommandType = CommandType

        Return ExecuteReader(oCommand)

    End Function
    Public Function ExecuteReader(ByVal sSQL As String) As SqlDataReader

        Return ExecuteReader(sSQL, CommandType.Text)

    End Function
    Public Function ExecuteReader(ByVal oCommand As SqlCommand) As SqlDataReader

        Dim oReader As SqlDataReader

        oReader = Nothing

        ' Is the connecton open?
        If Me.IsOpen = False Then Me.Open()

        If Me.IsOpen() Then

            'Is the command object null?
            If Not (oCommand Is Nothing) Then

                'Set the connection object
                oCommand.Connection = moConnection

                Try

                    'Execute the query and get the results
                    oReader = oCommand.ExecuteReader()

                Catch exp As Exception

                    'Write debug data
                    System.Diagnostics.Debug.WriteLine("Type : " & exp.GetType().ToString)
                    System.Diagnostics.Debug.WriteLine("Info : " & exp.Message.ToString)

                    'Throw exception
                    Throw New Exception(exp.Message.ToString, exp.InnerException)

                End Try

            End If
        End If

        'Destroy command
        oCommand = Nothing
        If UseTransaction = False Then Me.Close()
        Return oReader 'Even if oReader is nothing

    End Function
    Public Function ExecuteScalar(ByVal strSQL As String)
        Dim obj As Object
        If Me.IsOpen = False Then Me.Open()
        Dim Cmd As New SqlCommand(strSQL, moConnection)
        obj = Cmd.ExecuteScalar()
        If UseTransaction = False Then Me.Close()
        Return obj
    End Function

    Protected Overrides Sub Finalize()
        If Me.IsOpen Then Me.Close()
        MyBase.Finalize()
    End Sub

    Public Sub New()
        '
    End Sub
    Public Sub New(ByVal sServer As String, ByVal sDatabase As String, ByVal sUsername As String, ByVal sPassword As String)
        'msServer = sServer
        'msDatabase = sDatabase
        'msUsername = sUsername
        'msPassword = sPassword
    End Sub
    Public Sub New(ByVal appname As String)

        LoadFromMachineConfig(appname)
        'If LoadFromAppConfig(appname) = False Then
        '    LoadFromMachineConfig(appname)
        'End If

        EvaluateEncryption(appname)
    End Sub

    Public Shared Function GetConnection(ByVal dbname As String) As SqlConnection

        Dim appSettings As NameValueCollection = ConfigurationManager.AppSettings
        Dim TheConnectionString As String
        Dim TheConnection As SqlConnection

        'TheConnectionString = "Server=" & _
        '    appSettings(dbname & "_Server") & ";Initial Catalog=" & _
        '    appSettings(dbname & "_Database") & ";User ID=" & _
        '    appSettings(dbname & "_Username") & ";Password=" & _
        '    appSettings(dbname & "_Password") & ";"
        TheConnectionString = _
            ConfigurationManager.ConnectionStrings("ACHAcquireConnectionString").ToString()

        TheConnection = New SqlConnection(TheConnectionString)

        Return TheConnection

    End Function

    Private Sub LoadFromMachineConfig(ByVal appname As String)
        Try
            'msServer = ConfigurationManager.AppSettings(appname & "_Server")
            'msDatabase = ConfigurationManager.AppSettings(appname & "_Database")
            'msUsername = ConfigurationManager.AppSettings(appname & "_User")
            'msPassword = ConfigurationManager.AppSettings(appname & "_Password")

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Function LoadFromAppConfig(ByVal appname As String) As Boolean

        Try

            'in order to set the database we connect to, we have to determine if we
            'are running an application locally on a client machine vs. running on 
            'the web server.  we determine the application is running locally if 
            'a preferences xml file exists and the environment setting is stored.
            Dim objProfile As New MS.Profile.Xml
            Dim strEnvironment As String
            strEnvironment = objProfile.GetValue("preferences", "environment")

            If strEnvironment <> String.Empty Then

                Dim environments As IDictionary
                environments = ConfigurationManager.GetSection(ENVIRONMENTS_CONFIG_SECTION)

                Dim environment As IDictionary
                Dim settings As IDictionary

                For Each de As DictionaryEntry In environments
                    environment = de.Value

                    If UCase(environment.Item("name")) = UCase(strEnvironment) Then
                        For Each database As DictionaryEntry In environment
                            If TypeOf (database.Value) Is IDictionary Then
                                If UCase(appname) = UCase(database.Key) Then
                                    settings = database.Value
                                    'msServer = settings.Item("server")
                                    'msDatabase = settings.Item("database")
                                    'msUsername = settings.Item("user")
                                    'msPassword = settings.Item("password")
                                    Return True
                                End If
                            End If
                        Next
                    End If
                Next
            End If

            Return False

        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub EvaluateEncryption(ByVal appname As String)
        Dim blnUsesEncryption As Boolean
        Dim strUsesEncryption As String

        'if password is encrypted- decrypt it
        strUsesEncryption = ConfigurationManager.AppSettings(appname & "_PasswordEncrypted")
        'evaluate string
        If UCase(strUsesEncryption) = "TRUE" Then
            blnUsesEncryption = True
        ElseIf UCase(strUsesEncryption) = "FALSE" Or strUsesEncryption = String.Empty Then
            blnUsesEncryption = False
        End If
        'evaluate boolean
        If blnUsesEncryption = True Then
            DecryptUsernamePassword()
        End If
    End Sub
    Private Sub DecryptUsernamePassword()

        msUsername = Decrypt(msUsername)

        msPassword = Decrypt(msPassword)

    End Sub
    Private Function Decrypt(ByVal strText As String) As String

        Dim byKey() As Byte = {}
        Dim IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
        Dim inputByteArray(strText.Length) As Byte
        Dim sDecrKey As String

        Try

            'bury the key here
            sDecrKey = "[!#@%&0]"

            byKey = System.Text.Encoding.UTF8.GetBytes(Microsoft.VisualBasic.Left(sDecrKey, 8))

            Dim des As New DESCryptoServiceProvider

            inputByteArray = Convert.FromBase64String(strText)

            Dim ms As New MemoryStream

            Dim cs As New CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write)

            cs.Write(inputByteArray, 0, inputByteArray.Length)

            cs.FlushFinalBlock()

            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8

            Return encoding.GetString(ms.ToArray())

        Catch ex As Exception
            Throw (ex)
        End Try

    End Function

End Class
