Imports Microsoft.ApplicationBlocks.ExceptionManagement

Public Class ReturnObjectList

    Inherits CollectionBase

    Public Function AddReturnObject(ByVal ReturnObject As ReturnObject) As Guid
        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim NewGuid As Guid = Guid.NewGuid

        Try
            'Add the new ReturnObject to the collection
            ReturnObject.InternalReturnObjectID = NewGuid
            Me.List.Add(ReturnObject)

            'Create SQL ...
            sSql = "INSERT INTO Returns " & _
                "(ReturnID, ODFIID, ReturnStatus, ReferenceNumber, " & _
                "ODFITransactionID, MerchantID, BatchID, CollectionLevel, " & _
                "TransactionDate, RoutingNumber, AccountNumber, CheckNumber, " & _
                "Amount, ACHDate, ReturnDate, ReturnReasonCode, " & _
                "ReturnReasonDescription, ReturnTypeCode, " & _
                "NOCData1, NOCData2, NOCData3, " & _
                "CustomerID, ReturnFileID, ClearingJobID, " & _
                "TransactionType, IndividualIdNumber, " & _
                "IndividualName, DiscretionaryData, " & _
                "TraceNumber, AddendaInfo, EffectiveDate) " & _
                "VALUES ('" & _
                ReturnObject.InternalReturnObjectID.ToString & "', '" & _
                ReturnObject.ODFIID.ToString() & "', " & _
                ReturnObject.ReturnStatus.ToString & ", '" & _
                ReturnObject.ReferenceNumber & "', '" & _
                ReturnObject.ODFITransactionID & "', '" & _
                ReturnObject.MerchantID & "', '" & _
                ReturnObject.BatchID & "', '" & _
                SafeApos(ReturnObject.CollectionLevel) & "', '" & _
                ReturnObject.TransactionDate.ToShortDateString() & "', '" & _
                ReturnObject.RoutingNumber & "', '" & _
                ReturnObject.AccountNumber & "', '" & _
                ReturnObject.CheckNumber & "', " & _
                ReturnObject.Amount.ToString() & ", '" & _
                ReturnObject.ACHDate.ToShortDateString() & "', '" & _
                ReturnObject.ReturnDate.ToShortDateString() & "', '" & _
                ReturnObject.ReturnReasonCode & "', '" & _
                SafeApos(ReturnObject.ReturnReasonDescription) & "', '" & _
                ReturnObject.ReturnTypeCode & "', '" & _
                SafeApos(ReturnObject.NOCData1) & "', '" & _
                SafeApos(ReturnObject.NOCData2) & "', '" & _
                SafeApos(ReturnObject.NOCData3) & "', '" & _
                ReturnObject.CustomerID & "', '" & _
                ReturnObject.ReturnFileID.ToString() & "', " & _
                ReturnObject.ClearingJobId.ToString() & ", " & _
                ReturnObject.TransactionType.ToString() & ", '" & _
                ReturnObject.IndividualIdNumber & "', '" & _
                ReturnObject.IndividualName & "', '" & _
                ReturnObject.DiscretionaryData & "', '" & _
                ReturnObject.TraceNumber & "', '" & _
                ReturnObject.AddendaInfo & "', '" & _
                ReturnObject.EffectiveDate.ToShortDateString() & "')"

            'Process SQL ReturnObject ...
            dbMgr.TransactSql(sSql, INSERT)

        Catch ex As Exception
            NewGuid = Guid.Empty
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return NewGuid
    End Function

    Public Function UpdateReturnObject(ReturnObject As ReturnObject) As Boolean
        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim NewGuid As Guid = Guid.NewGuid
        Dim bReturn As Boolean = False

        Try

            'Create SQL ...
            sSql = "Update Returns set " & _
                "ODFIID = '" & ReturnObject.ODFIID.ToString() & "', " & _
                "ReturnStatus = " & ReturnObject.ReturnStatus.ToString() & ", " & _
                "ReferenceNumber = '" & ReturnObject.ReferenceNumber & "', " & _
                "ODFITransactionID = '" & ReturnObject.ODFITransactionID & "', " & _
                "MerchantID = '" & ReturnObject.MerchantID & "', " & _
                "BatchID = '" & ReturnObject.BatchID & "', " & _
                "CollectionLevel = '" & ReturnObject.CollectionLevel & "', " & _
                "TransactionDate = '" & ReturnObject.TransactionDate.ToShortDateString & "', " & _
                "RoutingNumber = '" & ReturnObject.RoutingNumber & "', " & _
                "AccountNumber = '" & ReturnObject.AccountNumber & "', " & _
                "CheckNumber = '" & ReturnObject.CheckNumber & "', " & _
                "Amount = " & ReturnObject.Amount.ToString() & ", " & _
                "ACHDate = '" & ReturnObject.ACHDate.ToShortDateString & "', " & _
                "ReturnDate = '" & ReturnObject.ReturnDate.ToShortDateString & "', " & _
                "ReturnReasonCode = '" & ReturnObject.ReturnReasonCode & "', " & _
                "ReturnReasonDescription = '" & ReturnObject.ReturnReasonDescription & "', " & _
                "ReturnTypeCode = '" & ReturnObject.ReturnTypeCode & "', " & _
                "NOCData1 = '" & ReturnObject.NOCData1 & "', " & _
                "NOCData2 = '" & ReturnObject.NOCData2 & "', " & _
                "NOCData3 = '" & ReturnObject.NOCData3 & "', " & _
                "CustomerID = '" & ReturnObject.CustomerID & "', " & _
                "ClearingJobID = " & ReturnObject.ClearingJobId.ToString() & ", " & _
                "TransactionType = " & ReturnObject.TransactionType.ToString() & ", " & _
                "IndividualIdNumber = '" & ReturnObject.IndividualIdNumber & "', " & _
                "IndividualName = '" & ReturnObject.IndividualName & "', " & _
                "DiscretionaryData = '" & ReturnObject.DiscretionaryData & "', " & _
                "TraceNumber = '" & ReturnObject.TraceNumber & "', " & _
                "AddendaInfo = '" & ReturnObject.AddendaInfo & "', " & _
                "EffectiveDate = '" & ReturnObject.EffectiveDate.ToShortDateString() & "' " & _
                "where ReturnID = '" & ReturnObject.InternalReturnObjectID.ToString() & "'"

            'Process SQL Return ...
            dbMgr.TransactSql(sSql, UPDATE)
            bReturn = True

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return bReturn

    End Function

    Public Function GetReturnObject(ByVal ReturnID As Guid) As ReturnObject
        Dim strSQL As String
        Dim objReturnObject As New ReturnObject

        Try

            strSQL = _
               "SELECT " & _
                    "ReturnID, ODFIID, ReturnStatus, ReferenceNumber, ODFITransactionID, MerchantID, " & _
                    "BatchID, CollectionLevel, TransactionDate, RoutingNumber, AccountNumber, CheckNumber," & _
                    "Amount, ACHDate, ReturnDate, a.ReturnReasonCode, b.ReasonDescription as ReturnReasonDescription, ReturnTypeCode," & _
                    "NOCData1, NOCData2, NOCData3, CustomerID, ReturnFileID, ClearingJobID, TransactionType," & _
                    "IndividualIdNumber, IndividualName, DiscretionaryData, TraceNumber, AddendaInfo, EffectiveDate " & _
               "FROM Returns a " & _
                    "left outer join ReturnReasonCodes b on a.ReturnReasonCode = b.ReasonCode " & _
               "WHERE ReturnID = '" & ReturnID.ToString & "'"

            objReturnObject = GetReturnObjectHelper(strSQL)
            Return objReturnObject

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function GetReturnObject(EffectiveDate As Date, TraceNumber As String) As ReturnObject
        Dim strSQL As String
        Dim objReturnObject As New ReturnObject

        Try

            strSQL = _
               "SELECT " & _
                    "ReturnID, ODFIID, ReturnStatus, ReferenceNumber, ODFITransactionID, MerchantID, " & _
                    "BatchID, CollectionLevel, TransactionDate, RoutingNumber, AccountNumber, CheckNumber," & _
                    "Amount, ACHDate, ReturnDate, a.ReturnReasonCode, b.ReasonDescription as ReturnReasonDescription, ReturnTypeCode," & _
                    "NOCData1, NOCData2, NOCData3, CustomerID, ReturnFileID, ClearingJobID, TransactionType," & _
                    "IndividualIdNumber, IndividualName, DiscretionaryData, TraceNumber, AddendaInfo, EffectiveDate " & _
               "FROM Returns a " & _
                    "left outer join ReturnReasonCodes b on a.ReturnReasonCode = b.ReasonCode " & _
               "WHERE EffectiveDate = '" & EffectiveDate.ToShortDateString() & _
                    "' and Ltrim(rtrim(TraceNumber)) = '" & _
                    LTrim(RTrim(TraceNumber)) & "'"

            objReturnObject = GetReturnObjectHelper(strSQL)
            Return objReturnObject

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function


    Private Function GetReturnObjectHelper(ByVal TheSql As String) As ReturnObject
        Dim dsReturnObject As DataSet
        Dim drReturnObject As DataRow
        Dim objReturnObject As New ReturnObject
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)

        Try

            dsReturnObject = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(dsReturnObject) Then

                drReturnObject = dsReturnObject.Tables(0).Rows(0)

                ' Build ReturnObject object
                objReturnObject.InternalReturnObjectID = SafeDBGuidField(drReturnObject, "ReturnID")
                objReturnObject.ODFIID = SafeDBGuidField(drReturnObject, "ODFIID")

                objReturnObject.ReturnStatus = SafeDBIntField(drReturnObject, "ReturnStatus")
                objReturnObject.ReferenceNumber = SafeDBStringField(drReturnObject, "ReferenceNumber")
                objReturnObject.ODFITransactionID = SafeDBStringField(drReturnObject, "ODFITransactionID")
                objReturnObject.MerchantID = SafeDBStringField(drReturnObject, "MerchantID")
                objReturnObject.BatchID = SafeDBStringField(drReturnObject, "BatchID")
                objReturnObject.CollectionLevel = SafeDBStringField(drReturnObject, "CollectionLevel")
                objReturnObject.TransactionDate = SafeDBDateField(drReturnObject, "TransactionDate")
                objReturnObject.RoutingNumber = SafeDBStringField(drReturnObject, "RoutingNumber")
                objReturnObject.AccountNumber = SafeDBStringField(drReturnObject, "AccountNumber")
                objReturnObject.CheckNumber = SafeDBStringField(drReturnObject, "CheckNumber")
                objReturnObject.Amount = SafeDBMoneyField(drReturnObject, "Amount")
                objReturnObject.ACHDate = SafeDBDateField(drReturnObject, "ACHDate")
                objReturnObject.ReturnDate = SafeDBDateField(drReturnObject, "ReturnDate")
                objReturnObject.ReturnReasonCode = SafeDBStringField(drReturnObject, "ReturnReasonCode")
                objReturnObject.ReturnReasonDescription = SafeDBStringField(drReturnObject, "ReturnReasonDescription")
                objReturnObject.ReturnTypeCode = SafeDBStringField(drReturnObject, "ReturnTypeCode")
                objReturnObject.NOCData1 = SafeDBStringField(drReturnObject, "NOCData1")
                objReturnObject.NOCData2 = SafeDBStringField(drReturnObject, "NOCData2")
                objReturnObject.NOCData3 = SafeDBStringField(drReturnObject, "NOCData3")
                objReturnObject.CustomerID = SafeDBStringField(drReturnObject, "CustomerID")

                objReturnObject.ClearingJobId = SafeDBBigIntField(drReturnObject, "ClearingJobID")
                objReturnObject.TransactionType = SafeDBIntField(drReturnObject, "TransactionType")
                objReturnObject.IndividualIdNumber = SafeDBStringField(drReturnObject, "IndividualIdNumber")
                objReturnObject.IndividualName = SafeDBStringField(drReturnObject, "IndividualName")
                objReturnObject.DiscretionaryData = SafeDBStringField(drReturnObject, "DiscretionaryData")
                objReturnObject.TraceNumber = SafeDBStringField(drReturnObject, "TraceNumber")
                objReturnObject.AddendaInfo = SafeDBStringField(drReturnObject, "AddendaInfo")
                objReturnObject.EffectiveDate = SafeDBDateField(drReturnObject, "EffectiveDate")
            End If

            Return objReturnObject

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    'Public Function RemoveReturnObject(ByVal ReturnObjectID As Guid) As Boolean
    '    ' Error indicator...
    '    Dim errIndicator As Int16 = 0
    '    Dim dbMgr As New Utilities.DBManager(APP_TYPE)
    '    Dim objReturnObject As ReturnObject
    '    Dim strSQL As String

    '    Try

    '        ' Remove the ReturnObject from the collection...
    '        objReturnObject = New ReturnObject
    '        For Each objReturnObject In Me
    '            If ReturnObjectID = objReturnObject.InternalReturnObjectID Then
    '                Me.List.Remove(objReturnObject)
    '                Exit For
    '            End If
    '        Next

    '        ' Build delete query and execution type...
    '        strSQL = _
    '                "DELETE FROM ReturnObjects " & _
    '                "WHERE " & _
    '                "ReturnObjectID = '" & ReturnObjectID.ToString & "'"

    '        ' Remove the ReturnObject from the database...
    '        If dbMgr.TransactSql(strSQL, DELETE) > 0 Then
    '            ' Return true to caller...
    '            RemoveReturnObject = True
    '        Else
    '            ' Return false to caller...
    '            RemoveReturnObject = False
    '        End If

    '    Catch ex As Exception
    '        ExceptionManager.Publish(ex)
    '        Throw (ex)
    '    Finally
    '        ' Clean-up...
    '    End Try

    'End Function

    Public Sub FillCollectionFromReferenceNumber(ByVal ReferenceNumber As Long)
        Dim dbMgr As Utilities.DBManager
        Dim drReturnObject As DataRow
        Dim myReturnObject As ReturnObject
        Dim strGetFileTemps As String
        Dim dsReturnObjects As DataSet = Nothing

        Try

            Me.List.Clear()

            ' Build the select query...
            strGetFileTemps = "SELECT ReturnID FROM Returns where ReferenceNumber = '" & _
                ReferenceNumber.ToString & "'"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsReturnObjects = dbMgr.TransactSql(strGetFileTemps, FILL_DATASET)

            ' Insert each user into the user list collection...
            For Each drReturnObject In dsReturnObjects.Tables(0).Rows

                myReturnObject = GetReturnObject(drReturnObject("ReturnID"))

                ' Add ReturnObject to list...
                Me.List.Add(myReturnObject)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsReturnObjects Is Nothing) Then
                dsReturnObjects.Dispose()
            End If
        End Try


    End Sub
 
    'Public ReadOnly Property ItemByReturnObjectID(ByVal ReturnObjectID As Guid) As ReturnObject
    '    Get
    '        Dim MyReturnObject As New ReturnObject
    '        ItemByReturnObjectID = Nothing

    '        For Each MyReturnObject In Me
    '            If ReturnObjectID = MyReturnObject.InternalReturnObjectID Then
    '                ItemByReturnObjectID = MyReturnObject
    '                Exit For
    '            End If
    '        Next
    '    End Get
    'End Property
End Class
