Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports System.Data.SqlClient

Public Class TransactionList

    Inherits CollectionBase

    Public Function AddTransaction(ByVal Transaction As Transaction) As Guid
        Dim sSql As String = ""
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim NewGuid As Guid = Guid.NewGuid

        Try
            'Add the new transaction to the collection
            Transaction.InternalTransactionID = NewGuid
            Me.List.Add(Transaction)

            'Create SQL ...
            sSql = "INSERT INTO Transactions " & _
                "(TransactionID, MerchantTransactionID, MerchantID, " & _
                "TransactionType, TransactionStatus, TransactionTime, TransactionAmount, " & _
                "ReceivedDate, EffectiveDate, OriginTypeID, FileID, " & _
                "ClearingJobID, StatusChangeDate, ClearDate, ReturnDate, " & _
                "MerchantCustomerID, FirstName, LastName, Address1, Address2, " & _
                "City, State, Zip, " & _
                "SSNumber, DLNumber, DLState, BirthDate, " & _
                "EmailAddress, PhoneNumber, RoutingNumber, AccountNumber, AccountType, " & _
                "RefundTransactionID, ReferenceTransactionID, ReturnReasonCode, RecurringItemID, ReturnID, SubmitOnOrAfter, " & _
                "PrenoteFlag, ZeroDollarFlag) " & _
                "VALUES ('" & _
                Transaction.InternalTransactionID.ToString & "', '" & _
                Transaction.MerchantTransactionID & "', '" & _
                Transaction.InternalMerchantID.ToString & "', " & _
                Transaction.TransactionType & ", " & _
                Transaction.TransactionStatus & ", '" & _
                Transaction.TransactionTime.ToString & "', " & _
                Transaction.TransactionAmount.ToString & ", '" & _
                Transaction.ReceivedDate.ToString & "', '" & _
                Transaction.EffectiveDate.ToString & "', " & _
                Transaction.OriginType.ToString & ", '" & _
                Transaction.FileID.ToString & "', " & _
                Transaction.ClearingJobID.ToString & ", '" & _
                Transaction.StatusChangeDate.ToString & "', '" & _
                Transaction.ClearDate.ToString & "', '" & _
                Transaction.ReturnDate.ToString & "', '" & _
                Transaction.MerchantCustomerID & "', '" & _
                SafeApos(Transaction.FirstName) & "', '" & _
                SafeApos(Transaction.LastName) & "', '" & _
                SafeApos(Transaction.Address1) & "', '" & _
                SafeApos(Transaction.Address2) & "', '" & _
                Transaction.City & "', '" & _
                Transaction.State & "', '" & _
                Transaction.Zip & "', '" & _
                Transaction.SSNumber & "', '" & _
                Transaction.DLNumber & "', '" & _
                Transaction.DLState & "', '" & _
                Transaction.BirthDate.ToString() & "', '" & _
                Transaction.EmailAddress & "', '" & _
                Transaction.PhoneNumber & "', '" & _
                Transaction.RoutingNumber & "', '" & _
                Transaction.AccountNumber & "', " & _
                Transaction.AccountType.ToString & ", '" & _
                Transaction.RefundTransactionID.ToString & "', '" & _
                Transaction.ReferenceTransactionID.ToString & "', '" & _
                Transaction.ReturnReasonCode & "', '" & _
                Transaction.RecurringItemID.ToString & "', '" & _
                Transaction.ReturnID.ToString & "', '" & _
                Transaction.SubmitOnOrAfter.ToShortDateString & " " & Transaction.SubmitOnOrAfter.ToShortTimeString & "', " & _
                CInt(Transaction.PrenoteFlag).ToString & ", " & _
                CInt(Transaction.ZeroDollarFlag).ToString & ")"

            'Process SQL transaction ...
            dbMgr.TransactSql(sSql, INSERT)

        Catch ex As Exception
            NewGuid = Guid.Empty
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return NewGuid
    End Function

    Public Function HasMatchingTransaction(ByRef Transaction As Transaction, ByRef HoursToGoBack As Int16) As Boolean
        Dim bReturn As Boolean = False

        Try

            Dim strSQL As String

            strSQL = _
               "SELECT count(*) TheCount from transactions where transactionamount=" & Transaction.TransactionAmount.ToString & _
                    " and AccountNumber='" & Transaction.AccountNumber & "'" & _
                    " and RoutingNumber='" & Transaction.RoutingNumber & "'" & _
                    " and TransactionType=" & Transaction.TransactionType & _
                    " and TransactionStatus in (1,2,3) " & _
                    " and datediff(hour, transactiontime, getdate()) < " & HoursToGoBack.ToString
            '(TransactionStatus: 1=Pending, 2=Working, 3=In Process, 4=Cleared, 5=Rejected, 6=Returned, 7=Chargedback, 8=Cancelled, 9=Temporary, 14=Clearing)

            Dim dsTransactionSearches As DataSet
            Dim drTransactionSearches As DataRow
            Dim dbMgr As New Utilities.DBManager(APP_TYPE)
            Dim RowCount As Long = 0

            dsTransactionSearches = dbMgr.TransactSql(strSQL, FILL_DATASET)

            If DataSetHasRows(dsTransactionSearches) Then

                drTransactionSearches = dsTransactionSearches.Tables(0).Rows(0)

                RowCount = SafeDBIntField(drTransactionSearches, "TheCount")

            End If

            If RowCount > 0 Then
                bReturn = True
            Else
                bReturn = False
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

        Return bReturn
    End Function


    Public Function UpdateTransaction(ByVal Transaction As Transaction) As Boolean
        Dim TheReturn As Boolean

        Try

            Dim strSQL As String
            Dim dbMgr As New Utilities.DBManager(APP_TYPE)

            strSQL = "Update Transactions set " & _
                "MerchantTransactionID = '" & Transaction.MerchantTransactionID & "', " & _
                "MerchantID = '" & Transaction.InternalMerchantID.ToString & "', " & _
                "TransactionType = " & Transaction.TransactionType.ToString & ", " & _
                "TransactionStatus = " & Transaction.TransactionStatus.ToString & ", " & _
                "TransactionTime = '" & Transaction.TransactionTime.ToString & "', " & _
                "TransactionAmount = " & Transaction.TransactionAmount.ToString & ", " & _
                "ReceivedDate = '" & Transaction.ReceivedDate.ToString & "', " & _
                "EffectiveDate = '" & Transaction.EffectiveDate.ToString & "', " & _
                "OriginTypeID = " & Transaction.OriginType.ToString & ", " & _
                "FileID = '" & Transaction.FileID.ToString & "', " & _
                "ClearingJobID = " & Transaction.ClearingJobID.ToString & ", " & _
                "StatusChangeDate = '" & Transaction.StatusChangeDate.ToString & "', " & _
                "ClearDate = '" & Transaction.ClearDate.ToString & "', " & _
                "ReturnDate = '" & Transaction.ReturnDate.ToString & "', " & _
                "MerchantCustomerID = '" & Transaction.MerchantCustomerID & "', " & _
                "FirstName = '" & SafeApos(Transaction.FirstName) & "', " & _
                "LastName = '" & SafeApos(Transaction.LastName) & "', " & _
                "Address1 = '" & SafeApos(Transaction.Address1) & "', " & _
                "Address2 = '" & SafeApos(Transaction.Address2) & "', " & _
                "City = '" & SafeApos(Transaction.City) & "', " & _
                "State = '" & Transaction.State & "', " & _
                "Zip = '" & Transaction.Zip & "', " & _
                "SSNumber = '" & SafeApos(Transaction.SSNumber) & "', " & _
                "DLNumber = '" & SafeApos(Transaction.DLNumber) & "', " & _
                "DLState = '" & Transaction.DLState & "', " & _
                "BirthDate = '" & Transaction.BirthDate.ToString() & "', " & _
                "EmailAddress = '" & Transaction.EmailAddress & "', " & _
                "PhoneNumber = '" & Transaction.PhoneNumber & "', " & _
                "RoutingNumber = '" & Transaction.RoutingNumber & "', " & _
                "AccountNumber = '" & Transaction.AccountNumber & "', " & _
                "AccountType = " & Transaction.AccountType.ToString & ", " & _
                "RefundTransactionID = '" & Transaction.RefundTransactionID.ToString & "', " & _
                "ReferenceTransactionID = '" & Transaction.ReferenceTransactionID.ToString & "', " & _
                "ReturnReasonCode = '" & Transaction.ReturnReasonCode & "', " & _
                "RecurringItemID = '" & Transaction.RecurringItemID.ToString & "', " & _
                "ReturnID = '" & Transaction.ReturnID.ToString & "', " & _
                "SubmitOnOrAfter = '" & Transaction.SubmitOnOrAfter.ToShortDateString & " " & Transaction.SubmitOnOrAfter.ToShortTimeString & "', " & _
                "PrenoteFlag = " & CInt(Transaction.PrenoteFlag).ToString & ", " & _
                "ZeroDollarFlag = " & CInt(Transaction.ZeroDollarFlag).ToString & " " & _
                "WHERE TransactionID = '" & Transaction.InternalTransactionID.ToString & "'"

            Call dbMgr.TransactSql(strSQL, UPDATE)
            TheReturn = True

        Catch ex As Exception
            TheReturn = False
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

        Return TheReturn

    End Function

    Public Function GetTransactionByTraceNumber(FileID As Guid, ByVal TraceNumber As String) As Transaction
        Dim strSQL As String
        Dim objTransaction As New Transaction

        Try

            strSQL = _
               "SELECT a.* from Transactions a, NACHAEntryDetailRecords b " & _
               "WHERE a.FileID = '" & FileID.ToString & "' and b.FileID = '" & FileID.ToString & "' " & _
               "and a.TransactionID = b.EntryDetailID and b.TraceNumber = '" & TraceNumber & "'"

            objTransaction = GetTransactionHelper(strSQL)
            Return objTransaction

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function GetTransaction(ByVal TransactionID As Guid) As Transaction
        Dim strSQL As String
        Dim objTransaction As New Transaction

        Try

            strSQL = _
               "SELECT * from Transactions " & _
               "WHERE TransactionID = '" & TransactionID.ToString & "'"

            objTransaction = GetTransactionHelper(strSQL)
            Return objTransaction

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function GetTransaction(ByVal ReferenceNumber As Long) As Transaction
        Dim strSQL As String
        Dim objTransaction As New Transaction

        Try

            strSQL = _
               "SELECT * FROM Transactions " & _
               "WHERE ReferenceNumber = " & ReferenceNumber.ToString

            objTransaction = GetTransactionHelper(strSQL)
            Return objTransaction

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function
    Public Function GetTransactionByMerchantTransactionId(ByVal MerchantTransactionId As String) As Transaction
        Dim strSQL As String
        Dim objTransaction As New Transaction

        Try

            strSQL = _
               "SELECT * FROM Transactions " & _
               "WHERE MerchantTransactionId = '" & MerchantTransactionId & "'"

            objTransaction = GetTransactionHelper(strSQL)
            Return objTransaction

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function
    Public Function AuthorizeTransactionAccess(ByVal WebUser As WebUser, ByVal ReferenceNumber As Long) As Boolean
        Dim bReturn As Boolean = False

        Try

            Dim strSQL As String

            strSQL = _
               "SELECT count(*) TheCount " & _
               "FROM Transactions t, Merchants m " & _
               "WHERE t.ReferenceNumber = " & ReferenceNumber.ToString & " and " & _
               "t.MerchantID = m.MerchantID and " & _
               "m.WebUserID = '" & WebUser.WebUserID.ToString & "'"

            Dim dsTransactionSearches As DataSet
            Dim drTransactionSearches As DataRow
            Dim dbMgr As New Utilities.DBManager(APP_TYPE)
            Dim RowCount As Long = 0

            dsTransactionSearches = dbMgr.TransactSql(strSQL, FILL_DATASET)

            If DataSetHasRows(dsTransactionSearches) Then

                drTransactionSearches = dsTransactionSearches.Tables(0).Rows(0)

                RowCount = SafeDBIntField(drTransactionSearches, "TheCount")

            End If

            If RowCount > 0 Then
                bReturn = True
            Else
                bReturn = False
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

        Return bReturn

    End Function

    Public Function AuthorizeClearedTransactionAccess(ByVal WebUser As WebUser, ByVal ReferenceNumber As Long) As Boolean
        Dim bReturn As Boolean = False

        Try

            Dim strSQL As String

            strSQL = _
               "SELECT count(*) TheCount " & _
               "FROM Transactions t, Merchants m " & _
               "WHERE t.ReferenceNumber = " & ReferenceNumber.ToString & " and " & _
               "t.MerchantID = m.MerchantID and " & _
               "t.TransactionStatus = 4 and " & _
               "m.WebUserID = '" & WebUser.WebUserID.ToString & "'"

            Dim dsTransactionSearches As DataSet
            Dim drTransactionSearches As DataRow
            Dim dbMgr As New Utilities.DBManager(APP_TYPE)
            Dim RowCount As Long = 0

            dsTransactionSearches = dbMgr.TransactSql(strSQL, FILL_DATASET)

            If DataSetHasRows(dsTransactionSearches) Then

                drTransactionSearches = dsTransactionSearches.Tables(0).Rows(0)

                RowCount = SafeDBIntField(drTransactionSearches, "TheCount")

            End If

            If RowCount > 0 Then
                bReturn = True
            Else
                bReturn = False
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

        Return bReturn

    End Function

    Public Function AuthorizePendingTransactionAccess(ByVal WebUser As WebUser, ByVal ReferenceNumber As Long) As Boolean
        Dim bReturn As Boolean = False

        Try

            Dim strSQL As String

            strSQL = _
               "SELECT count(*) TheCount " & _
               "FROM Transactions t, Merchants m " & _
               "WHERE t.ReferenceNumber = " & ReferenceNumber.ToString & " and " & _
               "t.MerchantID = m.MerchantID and " & _
               "t.TransactionStatus = 1 and " & _
               "m.WebUserID = '" & WebUser.WebUserID.ToString & "'"

            Dim dsTransactionSearches As DataSet
            Dim drTransactionSearches As DataRow
            Dim dbMgr As New Utilities.DBManager(APP_TYPE)
            Dim RowCount As Long = 0

            dsTransactionSearches = dbMgr.TransactSql(strSQL, FILL_DATASET)

            If DataSetHasRows(dsTransactionSearches) Then

                drTransactionSearches = dsTransactionSearches.Tables(0).Rows(0)

                RowCount = SafeDBIntField(drTransactionSearches, "TheCount")

            End If

            If RowCount > 0 Then
                bReturn = True
            Else
                bReturn = False
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

        Return bReturn

    End Function

    Public Function GetMerchantTransaction(ByVal MerchantID As Guid, _
            ByVal MerchantTransactionID As String) As Transaction

        Dim strSQL As String
        Dim objTransaction As New Transaction

        Try

            strSQL = _
               "SELECT * FROM Transactions " & _
               "WHERE MerchantID = '" & MerchantID.ToString & "' and " & _
               "MerchantTransactionID = '" & MerchantTransactionID & "'"

            objTransaction = GetTransactionHelper(strSQL)
            Return objTransaction

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Private Function GetTransactionHelper(ByVal TheSql As String) As Transaction
        Dim dsTransaction As DataSet
        Dim drTransaction As DataRow
        Dim objTransaction As New Transaction
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)

        Try

            dsTransaction = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(dsTransaction) Then

                drTransaction = dsTransaction.Tables(0).Rows(0)

                ' Build Transaction object
                objTransaction.InternalTransactionID = SafeDBGuidField(drTransaction, "TransactionID")
                objTransaction.MerchantTransactionID = SafeDBStringField(drTransaction, "MerchantTransactionID")
                objTransaction.InternalMerchantID = SafeDBGuidField(drTransaction, "MerchantID")
                objTransaction.TransactionType = SafeDBIntField(drTransaction, "TransactionType")
                objTransaction.TransactionStatus = SafeDBIntField(drTransaction, "TransactionStatus")
                objTransaction.TransactionTime = SafeDBDateTimeField(drTransaction, "TransactionTime")
                objTransaction.TransactionAmount = SafeDBMoneyField(drTransaction, "TransactionAmount")

                'Dim objCustomerList As New CustomerList
                'Dim objCustomer As New Customer
                'objCustomer = objCustomerList.GetCustomer(SafeDBGuidField(drTransaction, "CustomerID"))
                'objTransaction.Customer = objCustomer

                'Dim objCustomerAccountList As New CustomerAccountList
                'Dim objCustomerAccount As New CustomerAccount
                'objCustomerAccount = objCustomerAccountList.GetCustomerAccount(SafeDBGuidField(drTransaction, "CustomerAccountID"))
                'objTransaction.CustomerAccount = objCustomerAccount

                objTransaction.ReceivedDate = SafeDBDateField(drTransaction, "ReceivedDate")
                objTransaction.EffectiveDate = SafeDBDateField(drTransaction, "EffectiveDate")

                objTransaction.OriginType = SafeDBIntField(drTransaction, "OriginTypeID")
                objTransaction.FileID = SafeDBGuidField(drTransaction, "FileID")

                objTransaction.ReferenceNumber = SafeDBBigIntField(drTransaction, "ReferenceNumber")
                objTransaction.ClearingJobID = SafeDBBigIntField(drTransaction, "ClearingJobID")
                objTransaction.StatusChangeDate = SafeDBDateTimeField(drTransaction, "StatusChangeDate")
                objTransaction.ClearDate = SafeDBDateTimeField(drTransaction, "ClearDate")
                objTransaction.ReturnDate = SafeDBDateTimeField(drTransaction, "ReturnDate")

                objTransaction.MerchantCustomerID = SafeDBStringField(drTransaction, "MerchantCustomerID")
                objTransaction.FirstName = SafeDBStringField(drTransaction, "FirstName")
                objTransaction.LastName = SafeDBStringField(drTransaction, "LastName")
                objTransaction.Address1 = SafeDBStringField(drTransaction, "Address1")
                objTransaction.Address2 = SafeDBStringField(drTransaction, "Address2")
                objTransaction.City = SafeDBStringField(drTransaction, "City")
                objTransaction.State = SafeDBStringField(drTransaction, "State")
                objTransaction.Zip = SafeDBStringField(drTransaction, "Zip")
                objTransaction.SSNumber = SafeDBStringField(drTransaction, "SSNumber")
                objTransaction.DLNumber = SafeDBStringField(drTransaction, "DLNumber")
                objTransaction.DLState = SafeDBStringField(drTransaction, "DLState")
                objTransaction.BirthDate = SafeDBDateField(drTransaction, "BirthDate")
                objTransaction.EmailAddress = SafeDBStringField(drTransaction, "EmailAddress")
                objTransaction.PhoneNumber = SafeDBStringField(drTransaction, "PhoneNumber")

                objTransaction.RoutingNumber = SafeDBStringField(drTransaction, "RoutingNumber")
                objTransaction.AccountNumber = SafeDBStringField(drTransaction, "AccountNumber")
                objTransaction.AccountType = SafeDBIntField(drTransaction, "AccountType")

                objTransaction.RefundTransactionID = SafeDBGuidField(drTransaction, "RefundTransactionID")
                objTransaction.ReferenceTransactionID = SafeDBGuidField(drTransaction, "ReferenceTransactionID")

                objTransaction.ReturnReasonCode = SafeDBStringField(drTransaction, "ReturnReasonCode")

                objTransaction.RecurringItemID = SafeDBGuidField(drTransaction, "RecurringItemID")
                objTransaction.ReturnID = SafeDBGuidField(drTransaction, "ReturnID")
                objTransaction.SubmitOnOrAfter = CDate(SafeDBDateField(drTransaction, "SubmitOnOrAfter"))
                objTransaction.PrenoteFlag = SafeDBBitField(drTransaction, "PrenoteFlag")
                objTransaction.ZeroDollarFlag = SafeDBBitField(drTransaction, "ZeroDollarFlag")

            End If

            Return objTransaction

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function RemoveTransactionObject(ByVal Transaction As Transaction) As Boolean
        Return RemoveTransaction(Transaction.InternalTransactionID)
    End Function

    Public Function RemoveTransactionObjectByString(ByVal TransactionID As String) As Boolean
        Dim InternalTransactionID As New Guid(TransactionID)
        Return RemoveTransaction(InternalTransactionID)
    End Function

    Public Function RemoveTransaction(ByVal TransactionID As Guid) As Boolean
        ' Error indicator...
        Dim errIndicator As Int16 = 0
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim objTransaction As Transaction
        Dim strSQL As String

        Try

            ' Remove the Transaction from the collection...
            objTransaction = New Transaction
            For Each objTransaction In Me
                If TransactionID = objTransaction.InternalTransactionID Then
                    Me.List.Remove(objTransaction)
                    Exit For
                End If
            Next

            ' Build delete query and execution type...
            strSQL = _
                    "DELETE FROM Transactions " & _
                    "WHERE " & _
                    "TransactionID = '" & TransactionID.ToString & "'"

            ' Remove the transaction from the database...
            If dbMgr.TransactSql(strSQL, DELETE) > 0 Then
                ' Return true to caller...
                RemoveTransaction = True
            Else
                ' Return false to caller...
                RemoveTransaction = False
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Clean-up...
        End Try

    End Function

    Public Sub FillCollection()
        Dim dbMgr As Utilities.DBManager
        Dim drTransaction As DataRow
        Dim myTransaction As Transaction
        Dim strGetFileTemps As String
        Dim dsTransactions As DataSet = Nothing

        Try

            ' Build the select query...
            strGetFileTemps = "SELECT TransactionID FROM Transactions"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsTransactions = dbMgr.TransactSql(strGetFileTemps, FILL_DATASET)

            ' Insert each user into the user list collection...
            For Each drTransaction In dsTransactions.Tables(0).Rows

                myTransaction = GetTransaction(drTransaction("TransactionID"))

                ' Add Transaction to list...
                Me.List.Add(myTransaction)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsTransactions Is Nothing) Then
                dsTransactions.Dispose()
            End If
        End Try


    End Sub

    Public Sub FillCollectionWithChangedTransactions(ByVal MerchantID As Guid, _
        ByVal StatusChangeDate As Date)

        Dim dbMgr As Utilities.DBManager
        Dim drTransaction As DataRow
        Dim myTransaction As Transaction
        Dim strGetFileTemps As String
        Dim dsTransactions As DataSet = Nothing

        Try

            Me.List.Clear()

            ' Build the select query...
            strGetFileTemps = "SELECT TransactionID FROM Transactions where " & _
                "MerchantId = '" & MerchantID.ToString & "' and " & _
                "StatusChangeDate >= '" & StatusChangeDate.ToString & "'"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsTransactions = dbMgr.TransactSql(strGetFileTemps, FILL_DATASET)

            ' Insert each user into the user list collection...
            For Each drTransaction In dsTransactions.Tables(0).Rows

                myTransaction = GetTransaction(drTransaction("TransactionID"))

                ' Add Transaction to list...
                Me.List.Add(myTransaction)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsTransactions Is Nothing) Then
                dsTransactions.Dispose()
            End If
        End Try


    End Sub

    Public ReadOnly Property ItemByTransactionID(ByVal TransactionID As Guid) As Transaction
        Get
            Dim MyTransaction As New Transaction
            ItemByTransactionID = Nothing

            For Each MyTransaction In Me
                If TransactionID = MyTransaction.InternalTransactionID Then
                    ItemByTransactionID = MyTransaction
                    Exit For
                End If
            Next
        End Get
    End Property

    Public Function GetTransactionDataset(ByVal ReferenceNumber As Long) As DataSet

        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("ReferenceNumber", SqlDbType.BigInt)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = ReferenceNumber


            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetTransactionByReferenceNumber", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
        End Try

        Return ReturnDS
    End Function

    Public Function GetTransactionDatasetByFileID(ByVal FileID As String) As DataSet

        Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("FileID", SqlDbType.UniqueIdentifier)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = New Guid(FileID)


            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetTransactionsByFileID", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)

            ReturnDS = dsInfo

        Catch ex As Exception
            ReturnDS = Nothing
        End Try

        Return ReturnDS
    End Function

    Public Function GetTransactionListByFileID(ByVal FileID As String) As TransactionList

        'Dim ReturnDS As DataSet = Nothing

        Try

            Dim Param1 As New SqlParameter("FileID", SqlDbType.UniqueIdentifier)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = New Guid(FileID)


            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetTransactionsByFileID", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)
            Dim drTransaction As DataRow
            Dim myTransaction As Transaction
            Me.Clear()

            ' Insert each user into the user list collection...
            For Each drTransaction In dsInfo.Tables(0).Rows

                myTransaction = GetTransaction(drTransaction("TransactionID"))

                ' Add Transaction to list...
                Me.List.Add(myTransaction)
            Next

            'ReturnDS = dsInfo

        Catch ex As Exception
            'ReturnDS = Nothing
        End Try

        Return Me
    End Function

    Public Function GetTransactionListByDateRange(ClientID As String, ByVal StartDate As Date, EndDate As Date) As TransactionList

        Try

            Dim Param1 As New SqlParameter("ClientID", SqlDbType.UniqueIdentifier)
            Dim Param2 As New SqlParameter("StartDate", SqlDbType.Date)
            Dim Param3 As New SqlParameter("EndDate", SqlDbType.Date)
            Dim DB As New Utilities.DBManager
            Dim dsInfo As DataSet

            Param1.Value = New Guid(ClientID)

            'Moved this logic to stored procedure
            ''Make the StartDate have only the month/day/year portion of the date
            'Param2.Value = New DateTime(StartDate.Year, StartDate.Month, StartDate.Day, 0, 0, 0)
            Param2.Value = StartDate

            'Moved this logic to stored procedure
            ''Make the EndDate include the last millisecond before midnight
            'Param3.Value = New DateTime(EndDate.Year, EndDate.Month, EndDate.Day, 12, 59, 59, 999)
            Param3.Value = EndDate

            Dim sqlDa As SqlDataAdapter
            sqlDa = New SqlClient.SqlDataAdapter("GetTransactionListByDateRange", DB.GetConnection)
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure
            sqlDa.SelectCommand.Parameters.Add(Param1)
            sqlDa.SelectCommand.Parameters.Add(Param2)
            sqlDa.SelectCommand.Parameters.Add(Param3)

            dsInfo = New DataSet

            sqlDa.Fill(dsInfo)
            Dim drTransaction As DataRow
            Dim myTransaction As Transaction
            Me.Clear()

            ' Insert each user into the user list collection...
            For Each drTransaction In dsInfo.Tables(0).Rows

                myTransaction = GetTransaction(drTransaction("TransactionID"))

                ' Add Transaction to list...
                Me.List.Add(myTransaction)
            Next


        Catch ex As Exception
            'ReturnDS = Nothing
        End Try

        Return Me
    End Function

    '''<summary>
    ''' Returns the total transaction amount for the given FileID. 
    ''' 
    ''' If the returned TransactionType value is a 1, then this indicates that the total
    ''' transaction amount value returned is a Credit value.
    ''' 
    ''' If the returned TransactionType value is a 2, then this indicates that the total
    ''' transaction amount value returned is a Debit value.
    '''</summary>
    Public Function GetTransactionTotalByFileID(ByVal FileID As String, ByRef TransactionType As Int16) As Double

        Dim GrandTotal As Double = 0

        Try

            Dim DB As New Utilities.DBManager

            Dim Param1 As New SqlParameter("FileID", SqlDbType.UniqueIdentifier)
            Param1.Value = New Guid(FileID)

            Dim Param2 As New SqlParameter("GrandTotal", SqlDbType.Money)
            Param2.Value = 0
            Dim Param3 As New SqlParameter("TotalType", SqlDbType.Int)
            Param3.Value = 0

            Param2.Direction = ParameterDirection.Output
            Param3.Direction = ParameterDirection.Output

            Dim TheCommand As New SqlCommand("GetTransactionTotalByFileID", DB.GetConnection)
            TheCommand.CommandType = CommandType.StoredProcedure
            TheCommand.Parameters.Add(Param1)
            TheCommand.Parameters.Add(Param2)
            TheCommand.Parameters.Add(Param3)
            TheCommand.ExecuteNonQuery()

            TransactionType = TheCommand.Parameters(2).Value
            GrandTotal = TheCommand.Parameters(1).Value

        Catch ex As Exception
            GrandTotal = 0
        End Try

        Return GrandTotal
    End Function

    Public Function RemoveAllTransaction() As Boolean
        ' Error indicator...
        Dim errIndicator As Int16 = 0
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)
        Dim strSQL As String

        Try

            ' Build delete query and execution type...
            strSQL = "delete transactions"

            ' Remove the transaction from the database...
            If dbMgr.TransactSql(strSQL, DELETE) > 0 Then
                ' Return true to caller...
                RemoveAllTransaction = True
            Else
                ' Return true to caller...
                RemoveAllTransaction = True
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            RemoveAllTransaction = False
            Throw (ex)
        Finally
            ' Clean-up...
        End Try

    End Function

End Class
