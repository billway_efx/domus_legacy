Public Class ReturnObject

#Region "Private class variables"

    'This ID points to a unique record in the ReturnObjects table
    Private cInternalReturnObjectID As Guid

    Private cODFIID As Guid
    Private cReturnStatus As Integer
    Private cReferenceNumber As String
    Private cODFITransactionID As String
    Private cMerchantID As String
    Private cBatchID As String
    Private cCollectionLevel As String
    Private cTransactionDate As Date
    Private cRoutingNumber As String
    Private cAccountNumber As String
    Private cCheckNumber As String
    Private cAmount As Double
    Private cACHDate As Date
    Private cReturnDate As Date
    Private cReturnReasonCode As String
    Private cReturnReasonDescription As String
    Private cReturnTypeCode As String
    Private cNOCData1 As String
    Private cNOCData2 As String
    Private cNOCData3 As String
    Private cCustomerID As String

    'If from a file, then there will be a ReturnFileID
    Private cReturnFileID As Guid

    Private cClearingJobID As Long
    Private cTransactionType As Integer
    Private cIndividualIdNumber As String
    Private cIndividualName As String
    Private cDiscretionaryData As String
    Private cTraceNumber As String
    Private cAddendaInfo As String

    Private cEffectiveDate As Date

#End Region

    'Reflects the ReturnStatuses table
    Public Enum ReturnStatuses As Integer
        Pending = 1
        InProcess = 2
        Processed = 3
    End Enum

#Region "Class attributes"

    Public Property InternalReturnObjectID() As Guid
        Get
            Return cInternalReturnObjectID
        End Get
        Set(ByVal value As Guid)
            cInternalReturnObjectID = value
        End Set
    End Property
    Public Property ODFIID() As Guid
        Get
            Return cODFIID
        End Get
        Set(ByVal value As Guid)
            cODFIID = value
        End Set
    End Property
    Public Property ReturnStatus() As Long
        Get
            Return cReturnStatus
        End Get
        Set(ByVal value As Long)
            cReturnStatus = value
        End Set
    End Property
    Public Property ReferenceNumber() As String
        Get
            Return cReferenceNumber
        End Get
        Set(ByVal value As String)
            cReferenceNumber = value
        End Set
    End Property
    Public Property ODFITransactionID() As String
        Get
            Return cODFITransactionID
        End Get
        Set(ByVal value As String)
            cODFITransactionID = value
        End Set
    End Property
    Public Property MerchantID() As String
        Get
            Return cMerchantID
        End Get
        Set(ByVal value As String)
            cMerchantID = value
        End Set
    End Property
    Public Property BatchID() As String
        Get
            Return cBatchID
        End Get
        Set(ByVal value As String)
            cBatchID = value
        End Set
    End Property
    Public Property CollectionLevel() As String
        Get
            Return cCollectionLevel
        End Get
        Set(ByVal value As String)
            cCollectionLevel = value
        End Set
    End Property
    Public Property TransactionDate() As Date
        Get
            Return cTransactionDate
        End Get
        Set(ByVal value As Date)
            cTransactionDate = value
        End Set
    End Property
    Public Property RoutingNumber() As String
        Get
            Return cRoutingNumber
        End Get
        Set(ByVal value As String)
            cRoutingNumber = value
        End Set
    End Property
    Public Property AccountNumber() As String
        Get
            Return cAccountNumber
        End Get
        Set(ByVal value As String)
            cAccountNumber = value
        End Set
    End Property
    Public Property CheckNumber() As String
        Get
            Return cCheckNumber
        End Get
        Set(ByVal value As String)
            cCheckNumber = value
        End Set
    End Property
    Public Property Amount() As Double
        Get
            Return cAmount
        End Get
        Set(ByVal value As Double)
            cAmount = value
        End Set
    End Property
    Public Property ACHDate() As Date
        Get
            Return cACHDate
        End Get
        Set(ByVal value As Date)
            cACHDate = value
        End Set
    End Property
    Public Property ReturnDate() As Date
        Get
            Return cReturnDate
        End Get
        Set(ByVal value As Date)
            cReturnDate = value
        End Set
    End Property
    Public Property ReturnReasonCode() As String
        Get
            Return cReturnReasonCode
        End Get
        Set(ByVal value As String)
            cReturnReasonCode = value
        End Set
    End Property
    Public Property ReturnReasonDescription() As String
        Get
            Return cReturnReasonDescription
        End Get
        Set(ByVal value As String)
            cReturnReasonDescription = value
        End Set
    End Property
    Public Property ReturnTypeCode() As String
        Get
            Return cReturnTypeCode
        End Get
        Set(ByVal value As String)
            cReturnTypeCode = value
        End Set
    End Property
    Public Property NOCData1() As String
        Get
            Return cNOCData1
        End Get
        Set(ByVal value As String)
            cNOCData1 = value
        End Set
    End Property
    Public Property NOCData2() As String
        Get
            Return cNOCData2
        End Get
        Set(ByVal value As String)
            cNOCData2 = value
        End Set
    End Property
    Public Property NOCData3() As String
        Get
            Return cNOCData3
        End Get
        Set(ByVal value As String)
            cNOCData3 = value
        End Set
    End Property
    Public Property CustomerID() As String
        Get
            Return cCustomerID
        End Get
        Set(ByVal value As String)
            cCustomerID = value
        End Set
    End Property
    Public Property ReturnFileID() As Guid
        Get
            Return cReturnFileID
        End Get
        Set(ByVal value As Guid)
            cReturnFileID = value
        End Set
    End Property

    Public Property ClearingJobId As Long
        Get
            Return cClearingJobID
        End Get
        Set(ByVal value As Long)
            cClearingJobID = value
        End Set
    End Property
    Public Property TransactionType As Integer
        Get
            Return cTransactionType
        End Get
        Set(ByVal value As Integer)
            cTransactionType = value
        End Set
    End Property
    Public Property IndividualIdNumber As String
        Get
            Return cIndividualIdNumber
        End Get
        Set(ByVal value As String)
            cIndividualIdNumber = value
        End Set
    End Property
    Public Property IndividualName As String
        Get
            Return cIndividualName
        End Get
        Set(ByVal value As String)
            cIndividualName = value
        End Set
    End Property
    Public Property DiscretionaryData As String
        Get
            Return cDiscretionaryData
        End Get
        Set(ByVal value As String)
            cDiscretionaryData = value
        End Set
    End Property
    Public Property TraceNumber As String
        Get
            Return cTraceNumber
        End Get
        Set(ByVal value As String)
            cTraceNumber = value
        End Set
    End Property
    Public Property AddendaInfo As String
        Get
            Return cAddendaInfo
        End Get
        Set(ByVal value As String)
            cAddendaInfo = value
        End Set
    End Property
    Public Property EffectiveDate() As Date
        Get
            Return cEffectiveDate
        End Get
        Set(ByVal value As Date)
            cEffectiveDate = value
        End Set
    End Property

#End Region

#Region "Public Class Methods"

    'Default constructor
    Public Sub New()
        cInternalReturnObjectID = Guid.Empty
        cODFIID = Guid.Empty
        cReturnStatus = 0
        cReferenceNumber = ""
        cODFITransactionID = ""
        cMerchantID = ""
        cBatchID = ""
        cCollectionLevel = ""
        cTransactionDate = New Date(1800, 1, 1)
        cRoutingNumber = ""
        cAccountNumber = ""
        cCheckNumber = ""
        cAmount = 0
        cACHDate = New Date(1800, 1, 1)
        cReturnDate = New Date(1800, 1, 1)
        cReturnReasonCode = ""
        cReturnReasonDescription = ""
        cReturnTypeCode = ""
        cNOCData1 = ""
        cNOCData2 = ""
        cNOCData3 = ""
        cCustomerID = ""
        cReturnFileID = Guid.Empty
        cEffectiveDate = New Date(1800, 1, 1)
    End Sub

    'If the ODFIID guid is known, gives empty ReturnObject object with populated ODFIID attribute
    Public Sub New(ByVal ODFIID As Guid)
        cInternalReturnObjectID = Guid.Empty
        cODFIID = ODFIID
        cReturnStatus = 0
        cReferenceNumber = ""
        cODFITransactionID = ""
        cMerchantID = ""
        cBatchID = ""
        cCollectionLevel = ""
        cTransactionDate = New Date(1800, 1, 1)
        cRoutingNumber = ""
        cAccountNumber = ""
        cCheckNumber = ""
        cAmount = 0
        cACHDate = New Date(1800, 1, 1)
        cReturnDate = New Date(1800, 1, 1)
        cReturnReasonCode = ""
        cReturnReasonDescription = ""
        cReturnTypeCode = ""
        cNOCData1 = ""
        cNOCData2 = ""
        cNOCData3 = ""
        cCustomerID = ""
        cReturnFileID = Guid.Empty
        cEffectiveDate = New Date(1800, 1, 1)
    End Sub

#End Region

End Class
