Public Class TransactionSearch

#Region "Private class variables"

    'This ID points to a unique record in the table
    Private cSearchID As Guid

    Private cMerchantID As Guid
    Private cSearchDate As DateTime
    Private cFromDate As DateTime
    Private cToDate As DateTime
    Private cTransactionStatus As Int16
    Private cTransactionAmount As Double
    Private cAmountFrom As Double
    Private cAmountTo As Double
    Private cFirstName As String
    Private cLastName As String
    Private cMerchantTransactionID As String
    Private cMerchantCustomerID As String

#End Region

#Region "Class attributes"

    Public Property SearchID() As Guid
        Get
            Return cSearchID
        End Get
        Set(ByVal value As Guid)
            cSearchID = value
        End Set
    End Property
    Public Property MerchantID() As Guid
        Get
            Return cMerchantID
        End Get
        Set(ByVal value As Guid)
            cMerchantID = value
        End Set
    End Property
    Public Property SearchDate() As DateTime
        Get
            Return cSearchDate
        End Get
        Set(ByVal value As DateTime)
            cSearchDate = value
        End Set
    End Property
    Public Property FromDate() As DateTime
        Get
            Return cFromDate
        End Get
        Set(ByVal value As DateTime)
            cFromDate = value
        End Set
    End Property
    Public Property ToDate() As DateTime
        Get
            Return cToDate
        End Get
        Set(ByVal value As DateTime)
            cToDate = value
        End Set
    End Property
    Public Property TransactionStatus() As Int16
        Get
            Return cTransactionStatus
        End Get
        Set(ByVal value As Int16)
            cTransactionStatus = value
        End Set
    End Property
    Public Property TransactionAmount() As Double
        Get
            Return cTransactionAmount
        End Get
        Set(ByVal value As Double)
            cTransactionAmount = value
        End Set
    End Property
    Public Property AmountFrom() As Double
        Get
            Return cAmountFrom
        End Get
        Set(ByVal value As Double)
            cAmountFrom = value
        End Set
    End Property
    Public Property AmountTo() As Double
        Get
            Return cAmountTo
        End Get
        Set(ByVal value As Double)
            cAmountTo = value
        End Set
    End Property
    Public Property FirstName() As String
        Get
            Return cFirstName
        End Get
        Set(ByVal value As String)
            cFirstName = value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return cLastName
        End Get
        Set(ByVal value As String)
            cLastName = value
        End Set
    End Property
    Public Property MerchantTransactionID() As String
        Get
            Return cMerchantTransactionID
        End Get
        Set(ByVal value As String)
            cMerchantTransactionID = value
        End Set
    End Property
    Public Property MerchantCustomerID() As String
        Get
            Return cMerchantCustomerID
        End Get
        Set(ByVal value As String)
            cMerchantCustomerID = value
        End Set
    End Property
#End Region

#Region "Public Class Methods"

    'Default constructor
    Public Sub New()
        cSearchID = Guid.Empty
        cMerchantID = Guid.Empty
        cSearchDate = New Date(1800, 1, 1)
        cFromDate = New Date(1800, 1, 1)
        cToDate = New Date(1800, 1, 1)
        cTransactionStatus = 0
        cTransactionAmount = 0
        cAmountFrom = 0
        cAmountTo = 0
        cFirstName = ""
        cLastName = ""
        cMerchantTransactionID = ""
        cMerchantCustomerID = ""
    End Sub


#End Region

End Class
