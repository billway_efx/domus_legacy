Module DbConstants
    ' SQL query execution methods...
    Public Const NON_QUERY = "EXECUTENONQUERY"
    Public Const SCALAR = "EXECUTESCALAR"
    Public Const READER = "EXECUTEREADER"
    Public Const XML_READER = "EXECUTEXMLREADER"
    Public Const FILL_DATASET = "FILL"

    ' Application types to access database...
    Public Const APP_TYPE = "ACH_ACQUIRE"

    ' SQL commands...
    Public Const INSERT = "INSERT"
    Public Const DELETE = "DELETE"
    Public Const UPDATE = "UPDATE"


    'Reflects the AccountTypes table
    Enum AccountType As Integer
        Checking = 1
        Savings = 2
    End Enum

    'Reflects the OriginTypes table
    Enum OriginType As Integer
        UploadedFile = 1
        WebService = 2
    End Enum


End Module
