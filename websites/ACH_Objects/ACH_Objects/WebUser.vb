Imports System.Web.Security

Public Class WebUser

#Region "Private class variables"
    Private cWebUserID As Guid
    Private cAdminFlag As Boolean
    Private cDefaultMerchantID As Guid
    Private cASPMembership As MembershipUser
    Private cContactInfo As ContactInfo
    Private cAllowPasswordChange As Boolean
    Private cAllowManualBatchEntry As Boolean
    Private cAllowRecurringItemEntry As Boolean
    Private cAllowUploadWebNachaFiles As Boolean
    Private cAllowMerchantCreate As Boolean
#End Region

#Region "Class attributes"
    Public Property WebUserID() As Guid
        Get
            Return cWebUserID
        End Get
        Set(ByVal value As Guid)
            cWebUserID = value
        End Set
    End Property
    Public Property AdminFlag() As Boolean
        Get
            Return cAdminFlag
        End Get
        Set(ByVal value As Boolean)
            cAdminFlag = value
        End Set
    End Property
    Public Property DefaultMerchantID() As Guid
        Get
            Return cDefaultMerchantID
        End Get
        Set(ByVal value As Guid)
            cDefaultMerchantID = value
        End Set
    End Property
    Public Property ASPMembership() As MembershipUser
        Get
            Return cASPMembership
        End Get
        Set(ByVal value As MembershipUser)
            cASPMembership = value
        End Set
    End Property
    Public Property ContactInfo() As ContactInfo
        Get
            Return cContactInfo
        End Get
        Set(ByVal value As ContactInfo)
            cContactInfo = value
        End Set
    End Property
    Public Property AllowPasswordChange() As Boolean
        Get
            Return cAllowPasswordChange
        End Get
        Set(ByVal value As Boolean)
            cAllowPasswordChange = value
        End Set
    End Property
    Public Property AllowManualBatchEntry() As Boolean
        Get
            Return cAllowManualBatchEntry
        End Get
        Set(ByVal value As Boolean)
            cAllowManualBatchEntry = value
        End Set
    End Property
    Public Property AllowRecurringItemEntry() As Boolean
        Get
            Return cAllowRecurringItemEntry
        End Get
        Set(ByVal value As Boolean)
            cAllowRecurringItemEntry = value
        End Set
    End Property
    Public Property AllowUploadWebNachaFiles() As Boolean
        Get
            Return cAllowUploadWebNachaFiles
        End Get
        Set(ByVal value As Boolean)
            cAllowUploadWebNachaFiles = value
        End Set
    End Property
    Public Property AllowMerchantCreate() As Boolean
        Get
            Return cAllowMerchantCreate
        End Get
        Set(ByVal value As Boolean)
            cAllowMerchantCreate = value
        End Set
    End Property
#End Region

#Region "Public Class Methods"

    'Default constructor
    Public Sub New()
        cWebUserID = Guid.Empty
        cAdminFlag = False
        cDefaultMerchantID = Guid.Empty
        cASPMembership = Nothing
        cAllowPasswordChange = False
        cAllowRecurringItemEntry = False
        cAllowUploadWebNachaFiles = False
        cAllowMerchantCreate = False
    End Sub

    'Creates an instance populated by the indicated LoginID, empty if the WebUserID does not exist in the WebUsers table
    Public Sub New(ByVal LoginID As String)
        Dim objWebUserList As New WebUserList
        Dim objWebUser As WebUser
        objWebUser = objWebUserList.GetWebUser(LoginID)

        Call FillWebUserObject(objWebUser)
    End Sub

    'Creates an instance populated by the indicated WebUserID, empty if the WebUserID does not exist in the WebUsers table
    Public Sub New(ByVal WebUserID As System.Guid)
        Dim objWebUserList As New WebUserList
        Dim objWebUser As WebUser
        objWebUser = objWebUserList.GetWebUser(WebUserID)

        Call FillWebUserObject(objWebUser)
    End Sub

    Private Sub FillWebUserObject(ByVal TheWebUser As WebUser)
        cWebUserID = TheWebUser.WebUserID
        cAdminFlag = TheWebUser.AdminFlag
        cDefaultMerchantID = TheWebUser.DefaultMerchantID
        cASPMembership = TheWebUser.ASPMembership
        cContactInfo = TheWebUser.ContactInfo
        cAllowPasswordChange = TheWebUser.AllowPasswordChange
        cAllowRecurringItemEntry = TheWebUser.AllowRecurringItemEntry
        cAllowUploadWebNachaFiles = TheWebUser.AllowUploadWebNachaFiles
        cAllowMerchantCreate = TheWebUser.AllowMerchantCreate
    End Sub
#End Region

End Class
