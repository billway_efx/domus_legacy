Imports System.Configuration
Imports ACH_Objects
Imports Utilities
Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports System
Imports System.Collections

Public Class NachaFileHistory

    Inherits CollectionBase

    Public Const APP_TYPE As String = "ACH_ACQUIRE"
    Public Const INSERT As String = "INSERT"

    Public dbMgr As Utilities.DBManager
    Dim cFileID As Guid

    Public Sub New(FileID As Guid)
        dbMgr = New Utilities.DBManager(APP_TYPE)
        cFileID = FileID
    End Sub

    Public Function SafeStr(ByVal Input As String) As String
        Return Replace(Replace(Input, "'", "''"), ";", "")
    End Function

    Public Function WriteHistory(ByVal WebUserID As Guid, ByVal Description As String) As Boolean

        Dim sql As String

        Try
            'The column holds 2000 characters, but let's keep it to 1990 just to allow a little
            'breathing room ...
            If Len(SafeStr(Description)) > 1990 Then
                Description = Left(SafeStr(Description), 1990)
            Else
                Description = SafeStr(Description)
            End If

            sql = "Insert into NachaFileHistory (WebUserID, FileID, Description) values ('" & _
                WebUserID.ToString() & "','" & cFileID.ToString & "', '" & _
                Description & "')"

            ' Execute statement ...return result to caller
            WriteHistory = dbMgr.TransactSql(sql, INSERT)

        Catch ex As Exception
            ' Log exception and propogate up to caller and ASP.Net app
            ExceptionManager.Publish(ex)
            Throw (ex)

        Finally
            
        End Try


    End Function

    Public Function WriteHistory(ByVal WebUserName As String, ByVal Description As String) As Boolean

         Dim sql As String

        Try

            'The column holds 2000 characters, but let's keep it to 1990 just to allow a little
            'breathing room ...
            If Len(SafeStr(Description)) > 1990 Then
                Description = Left(SafeStr(Description), 1990)
            Else
                Description = SafeStr(Description)
            End If

            Dim WUL As New WebUserList
            Dim WU As New WebUser
            WU = WUL.GetWebUser(WebUserName)

            sql = "Insert into NachaFileHistory (WebUserID, FileID, Description) values ('" & _
                WU.WebUserID.ToString() & "','" & cFileID.ToString & "', '" & _
                Description & "')"

            ' Execute statement ...return result to caller
            WriteHistory = dbMgr.TransactSql(sql, INSERT)

        Catch ex As Exception
            ' Log exception and propogate up to caller and ASP.Net app
            ExceptionManager.Publish(ex)
            Throw (ex)

        Finally
            
        End Try


    End Function

    Protected Overrides Sub Finalize()

        ' Clean-up...
        If Not (dbMgr Is Nothing) Then
            dbMgr = Nothing
        End If

        MyBase.Finalize()
    End Sub
End Class
