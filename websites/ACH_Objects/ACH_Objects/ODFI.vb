﻿Public Class ODFI

#Region "Private class variables"

    'This ID points to a unique record in the ODFIs table
    Private cODFIID As Guid

    Private cODFIName As String
    Private cSiteID As String
    Private cMerchantID As String
    Private cLastCounterValue As Long
    Private cLastACHGeneration As DateTime
    Private cOutgoingFileProcedure As String

#End Region

#Region "Class attributes"

    Public Property ODFIID() As Guid
        Get
            Return cODFIID
        End Get
        Set(ByVal value As Guid)
            cODFIID = value
        End Set
    End Property
    Public Property ODFIName() As String
        Get
            Return cODFIName
        End Get
        Set(ByVal value As String)
            cODFIName = value
        End Set
    End Property
    Public Property SiteID() As String
        Get
            Return cSiteID
        End Get
        Set(ByVal value As String)
            cSiteID = value
        End Set
    End Property
    Public Property MerchantID() As String
        Get
            Return cMerchantID
        End Get
        Set(ByVal value As String)
            cMerchantID = value
        End Set
    End Property
    Public Property LastCounterValue() As Long
        Get
            Return cLastCounterValue
        End Get
        Set(ByVal value As Long)
            cLastCounterValue = value
        End Set
    End Property
    Public Property LastACHGeneration() As DateTime
        Get
            Return cLastACHGeneration
        End Get
        Set(ByVal value As DateTime)
            cLastACHGeneration = value
        End Set
    End Property
    Public Property OutgoingFileProcedure() As String
        Get
            Return cOutgoingFileProcedure
        End Get
        Set(ByVal value As String)
            cOutgoingFileProcedure = value
        End Set
    End Property

#End Region

#Region "Public Class Methods"

    'Default constructor
    Public Sub New()
        cODFIID = Guid.Empty
        cODFIName = ""
        cSiteID = ""
        cMerchantID = ""
        cLastCounterValue = 0
        cLastACHGeneration = New Date(1800, 1, 1)
        cOutgoingFileProcedure = ""
    End Sub

#End Region
End Class
