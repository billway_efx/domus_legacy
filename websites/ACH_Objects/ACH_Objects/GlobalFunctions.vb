Imports System.Reflection
Imports System.Net.Mail
Imports System.Net
Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports System.Data.SqlClient

Public Module GlobalFunctions

    Public Function DataSetHasRows(ByRef oDS As DataSet) As Boolean
        Try
            'This function will check to see that there is at least 1 table in the dataset
            'and that the table has at least one row.
            If Not oDS Is Nothing Then
                If oDS.Tables.Count > 0 Then
                    If oDS.Tables(0).Rows.Count > 0 Then
                        DataSetHasRows = True
                    Else
                        DataSetHasRows = False
                    End If
                Else
                    DataSetHasRows = False
                End If
            End If

        Catch ex As Exception
            Debug.WriteLine(ex.Message)
            Throw (ex)
        Finally
            If Not IsNothing(oDS) Then
                oDS.Dispose()
            End If
        End Try
    End Function

    Public Function SafeDBStringField(ByVal TheRow As DataRow, ByVal TheField As String) As String

        Dim TheString As String
        If Not TheRow.IsNull(TheField) Then
            TheString = TheRow(TheField)
        Else
            TheString = ""
        End If

        Return TheString
    End Function

    Public Function SafeDBGuidField(ByVal TheRow As DataRow, ByVal TheField As String) As Guid

        Dim TheGuid As Guid
        If Not TheRow.IsNull(TheField) Then
            TheGuid = New Guid(TheRow(TheField).ToString)
        Else
            TheGuid = Guid.Empty
        End If

        Return TheGuid
    End Function

    Public Function SafeDBBitField(ByVal TheRow As DataRow, ByVal TheField As String) As Boolean

        Dim TheBit As Boolean
        If Not TheRow.IsNull(TheField) Then
            TheBit = CBool(TheRow(TheField))
        Else
            TheBit = False
        End If

        Return TheBit
    End Function

    Public Function SafeDBIntField(ByVal TheRow As DataRow, ByVal TheField As String) As Integer

        Dim TheInt As Integer
        If Not TheRow.IsNull(TheField) Then
            TheInt = CLng(TheRow(TheField))
        Else
            TheInt = 0
        End If

        Return TheInt
    End Function

    Public Function SafeDBSmallIntField(ByVal TheRow As DataRow, ByVal TheField As String) As Short

        Dim TheInt As Short
        If Not TheRow.IsNull(TheField) Then
            TheInt = CShort(TheRow(TheField))
        Else
            TheInt = 0
        End If

        Return TheInt
    End Function

    Public Function SafeDBMoneyField(ByVal TheRow As DataRow, ByVal TheField As String) As Double

        Dim TheDouble As Double
        If Not TheRow.IsNull(TheField) Then
            TheDouble = CDbl(TheRow(TheField))
        Else
            TheDouble = 0
        End If

        Return TheDouble
    End Function

    Public Function SafeDBFloatField(ByVal TheRow As DataRow, ByVal TheField As String) As Double

        Dim TheDouble As Double
        If Not TheRow.IsNull(TheField) Then
            TheDouble = CDbl(TheRow(TheField))
        Else
            TheDouble = 0
        End If

        Return TheDouble
    End Function

    Public Function SafeDBDateField(ByVal TheRow As DataRow, ByVal TheField As String) As Date

        Dim TheDate As Date
        If Not TheRow.IsNull(TheField) Then
            TheDate = CDate(TheRow(TheField))
        Else
            TheDate = New Date(1800, 1, 1)
        End If

        Return TheDate
    End Function

    Public Function SafeDBDateTimeField(ByVal TheRow As DataRow, ByVal TheField As String) As DateTime

        Dim TheDateTime As DateTime
        If Not TheRow.IsNull(TheField) Then
            TheDateTime = CDate(TheRow(TheField))
        Else
            TheDateTime = New DateTime(1800, 1, 1)
        End If

        Return TheDateTime
    End Function


    Public Function SafeDBBigIntField(ByVal TheRow As DataRow, ByVal TheField As String) As Long

        Dim TheLong As Long
        If Not TheRow.IsNull(TheField) Then
            TheLong = CLng(TheRow(TheField))
        Else
            TheLong = 0
        End If

        Return TheLong
    End Function

    Public Function IsEqualTo(ByVal one As Object, ByVal two As Object) As Boolean

        Dim isEqual As Boolean = True

        Dim p As PropertyInfo

        For Each p In one.GetType().GetProperties()
            If (Not p.GetValue(one, Nothing).Equals(p.GetValue(two, Nothing))) Then
                isEqual = False
                Exit For
            End If
        Next

        Return isEqual

    End Function

    Public Function SafeApos(ByVal Input As String) As String
        Dim Replacement As String = ""
        Replacement = Replace(Input, "'", "''")
        If Replacement Is Nothing Then Replacement = ""
        Return Replacement
    End Function

    Public Function SendEmail(ByVal EmailTo As String, ByVal EmailSubject As String, _
        ByVal EmailBody As String) As Boolean

        Dim bReturn As Boolean = False
        Try

            Dim GC As New GlobalConfig

            Dim sUser As String = GC.ContactEmailUser
            Dim sPwd As String = GC.ContactEmailPassword
            Dim sSMTPServer As String = GC.ContactSMTPServer
            Dim sFromEmail As String = GC.ContactEmailFrom

            'If an EmailTo address was supplied, use it, otherwise
            'send it to the globally configured email address
            Dim sToEmail As String = ""
            If EmailTo <> "" Then
                sToEmail = EmailTo
            Else
                sToEmail = GC.ContactEmailTo
            End If

            Dim Message As MailMessage = New MailMessage
            Message.From = New MailAddress(sFromEmail)

            If EmailSubject <> "" Then
                Message.Subject = EmailSubject
            Else
                Message.Subject = "[No Subject]"
            End If

            If EmailBody <> "" Then
                Message.Body = EmailBody
            Else
                Message.Body = "[No Body]"
            End If

            Dim sList() As String
            Dim sEmail As String
            sList = sToEmail.Split(";")
            For Each sEmail In sList
                Message.To.Add(New MailAddress(sEmail))
            Next

            Dim EmailClient As SmtpClient = New SmtpClient(sSMTPServer)
            EmailClient.Port = 80
            EmailClient.UseDefaultCredentials = False
            EmailClient.Credentials = New System.Net.NetworkCredential(sUser, sPwd)

            'Specify a timeout of 15 seconds
            EmailClient.Timeout = 15000

            EmailClient.Send(Message)

            EmailClient = Nothing

            bReturn = True

        Catch ex As SmtpException
            ExceptionManager.Publish(ex)

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)

        End Try

        Return bReturn

    End Function

    Public Function SendDatabaseMail(ByVal EmailTo As String, ByVal EmailSubject As String, _
        ByVal EmailBody As String, ByVal DatabaseMailProfileName As String) As Boolean

        Dim bReturn As Boolean = False

        Try

            Dim Param1 As New SqlParameter("recipients", SqlDbType.VarChar)
            Param1.Size = 1000
            Dim Param2 As New SqlParameter("subject", SqlDbType.NVarChar)
            Param2.Size = 255
            Dim Param3 As New SqlParameter("body", SqlDbType.VarChar)
            Param3.Size = 5000
            Dim Param4 As New SqlParameter("profile_name", SqlDbType.VarChar)
            Param4.Size = 255
            Dim DB As New Utilities.DBManager

            If EmailSubject = "" Then
                EmailSubject = "[No Subject]"
            End If

            If EmailBody = "" Then
                EmailBody = "[No Body]"
            End If

            If EmailTo.Length > 1000 Then
                EmailTo = EmailTo.Substring(0, 1000)
            End If
            Param1.Value = EmailTo
            Param2.Value = EmailSubject
            If EmailBody.Length > 5000 Then
                EmailBody = EmailBody.Substring(0, 5000)
            End If
            Param3.Value = EmailBody
            Param4.Value = DatabaseMailProfileName

            Dim sqlCmd As New SqlCommand()
            sqlCmd.Connection = DB.GetConnection
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.CommandText = "msdb.dbo.sp_send_dbmail"
            sqlCmd.Parameters.Add(Param1)
            sqlCmd.Parameters.Add(Param2)
            sqlCmd.Parameters.Add(Param3)
            sqlCmd.Parameters.Add(Param4)

            Call sqlCmd.ExecuteNonQuery()

            bReturn = True

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            bReturn = False
        End Try

        Return bReturn

    End Function
End Module
