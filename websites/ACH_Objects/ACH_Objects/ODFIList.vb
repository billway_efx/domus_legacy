﻿Imports Microsoft.ApplicationBlocks.ExceptionManagement

Public Class ODFIList

    Inherits CollectionBase

    Public Function GetODFI(ByVal ODFIID As Guid) As ODFI
        Dim strSQL As String
        Dim objODFI As New ODFI

        Try

            strSQL = _
               "SELECT ODFIID, ODFIName, SiteID, MerchantID, LastCounterValue, LastACHGeneration, OutgoingFileProcedure FROM ODFIs " & _
               "WHERE ODFIID = '" & ODFIID.ToString & "'"

            objODFI = GetODFIHelper(strSQL)
            Return objODFI

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Private Function GetODFIHelper(ByVal TheSql As String) As ODFI
        Dim dsODFI As DataSet
        Dim drODFI As DataRow
        Dim objODFI As New ODFI
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)

        Try

            dsODFI = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(dsODFI) Then

                drODFI = dsODFI.Tables(0).Rows(0)

                ' Build ODFI object
                objODFI.ODFIID = SafeDBGuidField(drODFI, "ODFIID")
                objODFI.ODFIName = SafeDBStringField(drODFI, "ODFIName")
                objODFI.SiteID = SafeDBStringField(drODFI, "SiteID")
                objODFI.MerchantID = SafeDBStringField(drODFI, "MerchantID")
                objODFI.LastCounterValue = SafeDBBigIntField(drODFI, "LastCounterValue")
                objODFI.LastACHGeneration = SafeDBDateTimeField(drODFI, "LastACHGeneration")
                objODFI.OutgoingFileProcedure = SafeDBStringField(drODFI, "OutgoingFileProcedure")
            End If

            Return objODFI

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Sub FillCollection()
        Dim dbMgr As Utilities.DBManager
        Dim drODFI As DataRow
        Dim myODFI As ODFI
        Dim strSQL As String
        Dim dsODFIs As DataSet = Nothing

        Try

            ' Build the select query...
            strSQL = "SELECT ODFIID FROM ODFIs"

            ' Retrieve the rows from the database...
            dbMgr = New Utilities.DBManager(APP_TYPE)
            dsODFIs = dbMgr.TransactSql(strSQL, FILL_DATASET)

            ' Insert each user into the user list collection...
            For Each drODFI In dsODFIs.Tables(0).Rows

                myODFI = GetODFI(drODFI("ODFIID"))

                ' Add file to list...
                Me.List.Add(myODFI)
            Next

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
            ' Release resources
            If Not (dsODFIs Is Nothing) Then
                dsODFIs.Dispose()
            End If
        End Try


    End Sub

    Public Function GetDataset() As DataSet

        Dim MyDataset As New DataSet
        Dim MyTable As New DataTable("ODFIs")

        MyTable.Columns.Add("ODFIID", GetType(Guid))
        MyTable.Columns.Add("ODFIName", GetType(String))
        MyTable.Columns.Add("SiteID", GetType(String))
        MyTable.Columns.Add("MerchantID", GetType(String))
        MyTable.Columns.Add("LastCounterValue", GetType(Long))
        MyTable.Columns.Add("LastACHGeneration", GetType(DateTime))
        MyTable.Columns.Add("OutgoingFileProcedure", GetType(String))

        Dim TheRow(6) As Object
        Dim TheODFI As ODFI
        For Each TheODFI In Me
            TheRow(0) = TheODFI.ODFIID
            TheRow(1) = TheODFI.ODFIName
            TheRow(2) = TheODFI.SiteID
            TheRow(3) = TheODFI.MerchantID
            TheRow(4) = TheODFI.LastCounterValue
            TheRow(5) = TheODFI.LastACHGeneration
            TheRow(6) = TheODFI.OutgoingFileProcedure
            MyTable.Rows.Add(TheRow)
        Next

        MyDataset.Tables.Add(MyTable)
        Return MyDataset

    End Function

    Public ReadOnly Property ItemByODFIID(ByVal ODFIID As Guid) As ODFI
        Get
            Dim MyODFI As New ODFI
            ItemByODFIID = Nothing

            For Each MyODFI In Me
                If ODFIID = MyODFI.ODFIID Then
                    ItemByODFIID = MyODFI
                    Exit For
                End If
            Next
        End Get
    End Property

End Class
