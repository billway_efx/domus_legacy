Imports Microsoft.ApplicationBlocks.ExceptionManagement

Public Class ClearingJobLogList

    Inherits CollectionBase


    Public Function GetClearingJobLog(ByVal JobId As Long) As ClearingJobLog
        Dim strSQL As String
        Dim objClearingJobLog As New ClearingJobLog

        Try

            strSQL = _
               "SELECT * FROM ClearingJobLog " & _
               "WHERE jobid = " & JobId

            objClearingJobLog = GetClearingJobLogHelper(strSQL)
            Return objClearingJobLog

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function

    Public Function GetLastClearingJobLog() As ClearingJobLog
        Dim strSQL As String
        Dim objClearingJobLog As New ClearingJobLog

        Try

            strSQL = _
               "SELECT top 1 * FROM ClearingJobLog " & _
               "order by jobid desc"

            objClearingJobLog = GetClearingJobLogHelper(strSQL)
            Return objClearingJobLog

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try

    End Function


    Private Function GetClearingJobLogHelper(ByVal TheSql As String) As ClearingJobLog
        Dim dsClearingJobLog As DataSet
        Dim drClearingJobLog As DataRow
        Dim objClearingJobLog As New ClearingJobLog
        Dim dbMgr As New Utilities.DBManager(APP_TYPE)

        Try

            dsClearingJobLog = dbMgr.TransactSql(TheSql, FILL_DATASET)

            If DataSetHasRows(dsClearingJobLog) Then

                drClearingJobLog = dsClearingJobLog.Tables(0).Rows(0)

                ' Build web user object
                objClearingJobLog.JobID = SafeDBBigIntField(drClearingJobLog, "JobID")
                objClearingJobLog.TimeStarted = SafeDBDateField(drClearingJobLog, "TimeStarted")
                objClearingJobLog.TimeEnded = SafeDBDateField(drClearingJobLog, "TimeEnded")
                objClearingJobLog.Status = SafeDBIntField(drClearingJobLog, "Status")
                objClearingJobLog.TotalReturnsRecords = SafeDBBigIntField(drClearingJobLog, "TotalReturnsRecords")
                objClearingJobLog.TotalReturnsRecordsInvalid = SafeDBBigIntField(drClearingJobLog, "TotalReturnsRecordsInvalid")
                objClearingJobLog.TotalReturnsProcessed = SafeDBBigIntField(drClearingJobLog, "TotalReturnsProcessed")
                objClearingJobLog.TotalTransactionsMarkedReturned = SafeDBBigIntField(drClearingJobLog, "TotalTransactionsMarkedReturned")
                objClearingJobLog.TotalTransactionsMarkedCleared = SafeDBBigIntField(drClearingJobLog, "TotalTransactionsMarkedCleared")

            End If

            Return objClearingJobLog

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        Finally
        End Try


    End Function


End Class
