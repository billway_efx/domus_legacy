Public Class SettlementStatementReport

#Region "Private class variables"

    Dim cReportDataID As Guid
    Dim cMerchantID As Guid
    Dim cUserID As String
    Dim cMerchantName As String
    Dim cDateFrom As Date
    Dim cDateTo As Date
    Dim cDrSubCount As Long
    Dim cDrSubAmount As Double
    Dim cDrSubFees As Double
    Dim cDrSubTotalFees As Double
    Dim cDrRejCount As Long
    Dim cDrRejAmount As Double
    Dim cDrRejFees As Double
    Dim cDrRejTotalFees As Double
    Dim cDrClrCount As Long
    Dim cDrClrAmount As Double
    Dim cDrClrFees As Double
    Dim cDrClrTotalFees As Double
    Dim cDrRetCount As Long
    Dim cDrRetAmount As Double
    Dim cDrRetFees As Double
    Dim cDrRetTotalFees As Double
    Dim cDrChgCount As Long
    Dim cDrChgAmount As Double
    Dim cDrChgFees As Double
    Dim cDrChgTotalFees As Double
    Dim cCrSubCount As Long
    Dim cCrSubAmount As Double
    Dim cCrSubFees As Double
    Dim cCrSubTotalFees As Double
    Dim cCrRejCount As Long
    Dim cCrRejAmount As Double
    Dim cCrRejFees As Double
    Dim cCrRejTotalFees As Double
    Dim cCrClrCount As Long
    Dim cCrClrAmount As Double
    Dim cCrClrFees As Double
    Dim cCrClrTotalFees As Double
    Dim cCrRetCount As Long
    Dim cCrRetAmount As Double
    Dim cCrRetFees As Double
    Dim cCrRetTotalFees As Double
    Dim cCrChgCount As Long
    Dim cCrChgAmount As Double
    Dim cCrChgFees As Double
    Dim cCrChgTotalFees As Double
    Dim cReserves As Double
    Dim cReservesReleased As Double
    Dim cNetSettlement As Double
    Dim cDateCreated As Date
    Dim cReservesRate As Double

#End Region

#Region "Class attributes"

    Public Property ReportDataID() As Guid
        Get
            Return cReportDataID
        End Get
        Set(ByVal value As Guid)
            cReportDataID = value
        End Set
    End Property
    Public Property MerchantID() As Guid
        Get
            Return cMerchantID
        End Get
        Set(ByVal value As Guid)
            cMerchantID = value
        End Set
    End Property
    Public Property UserID() As String
        Get
            Return cUserID
        End Get
        Set(ByVal value As String)
            cUserID = value
        End Set
    End Property
    Public Property MerchantName() As String
        Get
            Return cMerchantName
        End Get
        Set(ByVal value As String)
            cMerchantName = value
        End Set
    End Property
    Public Property DateFrom() As Date
        Get
            Return cDateFrom
        End Get
        Set(ByVal value As Date)
            cDateFrom = value
        End Set
    End Property
    Public Property DateTo() As Date
        Get
            Return cDateTo
        End Get
        Set(ByVal value As Date)
            cDateTo = value
        End Set
    End Property
    Public Property DrSubCount() As Long
        Get
            Return cDrSubCount
        End Get
        Set(ByVal value As Long)
            cDrSubCount = value
        End Set
    End Property
    Public Property DrSubAmount() As Double
        Get
            Return cDrSubAmount
        End Get
        Set(ByVal value As Double)
            cDrSubAmount = value
        End Set
    End Property
    Public Property DrSubFees() As Double
        Get
            Return cDrSubFees
        End Get
        Set(ByVal value As Double)
            cDrSubFees = value
        End Set
    End Property
    Public Property DrSubTotalFees() As Double
        Get
            Return cDrSubTotalFees
        End Get
        Set(ByVal value As Double)
            cDrSubTotalFees = value
        End Set
    End Property
    Public Property DrRejCount() As Long
        Get
            Return cDrRejCount
        End Get
        Set(ByVal value As Long)
            cDrRejCount = value
        End Set
    End Property
    Public Property DrRejAmount() As Double
        Get
            Return cDrRejAmount
        End Get
        Set(ByVal value As Double)
            cDrRejAmount = value
        End Set
    End Property
    Public Property DrRejFees() As Double
        Get
            Return cDrRejFees
        End Get
        Set(ByVal value As Double)
            cDrRejFees = value
        End Set
    End Property
    Public Property DrRejTotalFees() As Double
        Get
            Return cDrRejTotalFees
        End Get
        Set(ByVal value As Double)
            cDrRejTotalFees = value
        End Set
    End Property
    Public Property DrClrCount() As Long
        Get
            Return cDrClrCount
        End Get
        Set(ByVal value As Long)
            cDrClrCount = value
        End Set
    End Property
    Public Property DrClrAmount() As Double
        Get
            Return cDrClrAmount
        End Get
        Set(ByVal value As Double)
            cDrClrAmount = value
        End Set
    End Property
    Public Property DrClrFees() As Double
        Get
            Return cDrClrFees
        End Get
        Set(ByVal value As Double)
            cDrClrFees = value
        End Set
    End Property
    Public Property DrClrTotalFees() As Double
        Get
            Return cDrClrTotalFees
        End Get
        Set(ByVal value As Double)
            cDrClrTotalFees = value
        End Set
    End Property
    Public Property DrRetCount() As Long
        Get
            Return cDrRetCount
        End Get
        Set(ByVal value As Long)
            cDrRetCount = value
        End Set
    End Property
    Public Property DrRetAmount() As Double
        Get
            Return cDrRetAmount
        End Get
        Set(ByVal value As Double)
            cDrRetAmount = value
        End Set
    End Property
    Public Property DrRetFees() As Double
        Get
            Return cDrRetFees
        End Get
        Set(ByVal value As Double)
            cDrRetFees = value
        End Set
    End Property
    Public Property DrRetTotalFees() As Double
        Get
            Return cDrRetTotalFees
        End Get
        Set(ByVal value As Double)
            cDrRetTotalFees = value
        End Set
    End Property
    Public Property DrChgCount() As Long
        Get
            Return cDrChgCount
        End Get
        Set(ByVal value As Long)
            cDrChgCount = value
        End Set
    End Property
    Public Property DrChgAmount() As Double
        Get
            Return cDrChgAmount
        End Get
        Set(ByVal value As Double)
            cDrChgAmount = value
        End Set
    End Property
    Public Property DrChgFees() As Double
        Get
            Return cDrChgFees
        End Get
        Set(ByVal value As Double)
            cDrChgFees = value
        End Set
    End Property
    Public Property DrChgTotalFees() As Double
        Get
            Return cDrChgTotalFees
        End Get
        Set(ByVal value As Double)
            cDrChgTotalFees = value
        End Set
    End Property
    Public Property CrSubCount() As Long
        Get
            Return cCrSubCount
        End Get
        Set(ByVal value As Long)
            cCrSubCount = value
        End Set
    End Property
    Public Property CrSubAmount() As Double
        Get
            Return cCrSubAmount
        End Get
        Set(ByVal value As Double)
            cCrSubAmount = value
        End Set
    End Property
    Public Property CrSubFees() As Double
        Get
            Return cCrSubFees
        End Get
        Set(ByVal value As Double)
            cCrSubFees = value
        End Set
    End Property
    Public Property CrSubTotalFees() As Double
        Get
            Return cCrSubTotalFees
        End Get
        Set(ByVal value As Double)
            cCrSubTotalFees = value
        End Set
    End Property
    Public Property CrRejCount() As Long
        Get
            Return cCrRejCount
        End Get
        Set(ByVal value As Long)
            cCrRejCount = value
        End Set
    End Property
    Public Property CrRejAmount() As Double
        Get
            Return cCrRejAmount
        End Get
        Set(ByVal value As Double)
            cCrRejAmount = value
        End Set
    End Property
    Public Property CrRejFees() As Double
        Get
            Return cCrRejFees
        End Get
        Set(ByVal value As Double)
            cCrRejFees = value
        End Set
    End Property
    Public Property CrRejTotalFees() As Double
        Get
            Return cCrRejTotalFees
        End Get
        Set(ByVal value As Double)
            cCrRejTotalFees = value
        End Set
    End Property
    Public Property CrClrCount() As Long
        Get
            Return cCrClrCount
        End Get
        Set(ByVal value As Long)
            cCrClrCount = value
        End Set
    End Property
    Public Property CrClrAmount() As Double
        Get
            Return cCrClrAmount
        End Get
        Set(ByVal value As Double)
            cCrClrAmount = value
        End Set
    End Property
    Public Property CrClrFees() As Double
        Get
            Return cCrClrFees
        End Get
        Set(ByVal value As Double)
            cCrClrFees = value
        End Set
    End Property
    Public Property CrClrTotalFees() As Double
        Get
            Return cCrClrTotalFees
        End Get
        Set(ByVal value As Double)
            cCrClrTotalFees = value
        End Set
    End Property
    Public Property CrRetCount() As Long
        Get
            Return cCrRetCount
        End Get
        Set(ByVal value As Long)
            cCrRetCount = value
        End Set
    End Property
    Public Property CrRetAmount() As Double
        Get
            Return cCrRetAmount
        End Get
        Set(ByVal value As Double)
            cCrRetAmount = value
        End Set
    End Property
    Public Property CrRetFees() As Double
        Get
            Return cCrRetFees
        End Get
        Set(ByVal value As Double)
            cCrRetFees = value
        End Set
    End Property
    Public Property CrRetTotalFees() As Double
        Get
            Return cCrRetTotalFees
        End Get
        Set(ByVal value As Double)
            cCrRetTotalFees = value
        End Set
    End Property
    Public Property CrChgCount() As Long
        Get
            Return cCrChgCount
        End Get
        Set(ByVal value As Long)
            cCrChgCount = value
        End Set
    End Property
    Public Property CrChgAmount() As Double
        Get
            Return cCrChgAmount
        End Get
        Set(ByVal value As Double)
            cCrChgAmount = value
        End Set
    End Property
    Public Property CrChgFees() As Double
        Get
            Return cCrChgFees
        End Get
        Set(ByVal value As Double)
            cCrChgFees = value
        End Set
    End Property
    Public Property CrChgTotalFees() As Double
        Get
            Return cCrChgTotalFees
        End Get
        Set(ByVal value As Double)
            cCrChgTotalFees = value
        End Set
    End Property
    Public Property Reserves() As Double
        Get
            Return cReserves
        End Get
        Set(ByVal value As Double)
            cReserves = value
        End Set
    End Property
    Public Property ReservesReleased() As Double
        Get
            Return cReservesReleased
        End Get
        Set(ByVal value As Double)
            cReservesReleased = value
        End Set
    End Property
    Public Property NetSettlement() As Double
        Get
            Return cNetSettlement
        End Get
        Set(ByVal value As Double)
            cNetSettlement = value
        End Set
    End Property
    Public Property DateCreated() As Date
        Get
            Return cDateCreated
        End Get
        Set(ByVal value As Date)
            cDateCreated = value
        End Set
    End Property
    Public Property ReservesRate() As Double
        Get
            Return cReservesRate
        End Get
        Set(ByVal value As Double)
            cReservesRate = value
        End Set
    End Property
#End Region

#Region "Public Class Methods"

    'Default constructor
    Public Sub New()
        
    End Sub


#End Region

End Class
