Public Class Transaction

#Region "Private class variables"

    'This ID points to a unique record in the Transactions table
    Private cInternalTransactionID As Guid

    'This identifies the unique Merchant record in the Merchants table
    Private cInternalMerchantID As Guid

    'This is the Merchant's own unique identifier for the transaction
    Private cMerchantTransactionID As String

    'Transaction attributes
    Private cTransactionType As TransactionTypes
    Private cTransactionStatus As TransactionStatuses
    Private cTransactionTime As DateTime
    Private cTransactionAmount As Double

     'Transaction effective date
    Private cReceivedDate As Date

    'Indicates whether the transaction originated from an uploaded file, or from the web service
    Private cOriginType As OriginType

    'If from a file, then there will be a FileID
    Private cFileID As Guid

    Private cReferenceNumber As Long
    Private cClearingJobID As Long
    Private cStatusChangeDate As Date
    Private cClearDate As Date
    Private cReturnDate As Date
    Private cEffectiveDate As Date

    'This is the Merchant's own unique ID for their customer
    Private cMerchantCustomerID As String

    'Customer attributes
    Private cFirstName As String
    Private cLastName As String
    Private cAddress1 As String
    Private cAddress2 As String
    Private cCity As String
    Private cState As String
    Private cZip As String
    Private cSSNumber As String
    Private cDLNumber As String
    Private cDLState As String
    Private cBirthDate As Date

    'Optional data
    Private cEmailAddress As String
    Private cPhoneNumber As String

    'Customer Account attributes
    Private cRoutingNumber As String
    Private cAccountNumber As String
    Private cAccountType As AccountType

    Private cRefundTransactionID As Guid
    Private cReferenceTransactionID As Guid

    Private cReturnReasonCode As String

    Private cRecurringItemID As Guid
    Private cReturnID As Guid

    Private cSubmitOnOrAfter As DateTime
    Private cPrenoteFlag As Boolean
    Private cZeroDollarFlag As Boolean

#End Region

    'Reflects the TransactionTypes table
    Public Enum TransactionTypes As Integer
        Credit = 1
        Debit = 2
    End Enum

    'Reflects the TransactionStatuses table
    Public Enum TransactionStatuses As Integer
        Pending = 1
        InProcess = 2
        Cleared = 3
        Rejected = 4
        Returned = 5
    End Enum

#Region "Class attributes"

    Public Property InternalTransactionID() As Guid
        Get
            Return cInternalTransactionID
        End Get
        Set(ByVal value As Guid)
            cInternalTransactionID = value
        End Set
    End Property
    Public Property InternalMerchantID() As Guid
        Get
            Return cInternalMerchantID
        End Get
        Set(ByVal value As Guid)
            cInternalMerchantID = value
        End Set
    End Property
    Public Property MerchantTransactionID() As String
        Get
            Return cMerchantTransactionID
        End Get
        Set(ByVal value As String)
            cMerchantTransactionID = value
        End Set
    End Property

    Public Property TransactionType() As Int16
        Get
            Return cTransactionType
        End Get
        Set(ByVal value As Int16)
            cTransactionType = value
        End Set
    End Property
    Public Property TransactionStatus() As Int16
        Get
            Return cTransactionStatus
        End Get
        Set(ByVal value As Int16)
            cTransactionStatus = value
        End Set
    End Property
    Public Property TransactionTime() As DateTime
        Get
            Return cTransactionTime
        End Get
        Set(ByVal value As DateTime)
            cTransactionTime = value
        End Set
    End Property
    Public Property TransactionAmount() As Double
        Get
            Return cTransactionAmount
        End Get
        Set(ByVal value As Double)
            cTransactionAmount = value
        End Set
    End Property

    Public Property ReceivedDate() As Date
        Get
            Return cReceivedDate
        End Get
        Set(ByVal value As Date)
            cReceivedDate = value
        End Set
    End Property
    Public Property EffectiveDate() As Date
        Get
            Return cEffectiveDate
        End Get
        Set(ByVal value As Date)
            cEffectiveDate = value
        End Set
    End Property
    Public Property OriginType() As Int16
        Get
            Return cOriginType
        End Get
        Set(ByVal value As Int16)
            cOriginType = value
        End Set
    End Property
    Public Property FileID() As Guid
        Get
            Return cFileID
        End Get
        Set(ByVal value As Guid)
            cFileID = value
        End Set
    End Property

    Public Property ReferenceNumber() As Long
        Get
            Return cReferenceNumber
        End Get
        Set(ByVal value As Long)
            cReferenceNumber = value
        End Set
    End Property
    Public Property ClearingJobID() As Long
        Get
            Return cClearingJobID
        End Get
        Set(ByVal value As Long)
            cClearingJobID = value
        End Set
    End Property
    Public Property StatusChangeDate() As Date
        Get
            Return cStatusChangeDate
        End Get
        Set(ByVal value As Date)
            cStatusChangeDate = value
        End Set
    End Property
    Public Property ClearDate() As Date
        Get
            Return cClearDate
        End Get
        Set(ByVal value As Date)
            cClearDate = value
        End Set
    End Property
    Public Property ReturnDate() As Date
        Get
            Return cReturnDate
        End Get
        Set(ByVal value As Date)
            cReturnDate = value
        End Set
    End Property

    Public Property ReturnReasonCode() As String
        Get
            Return cReturnReasonCode
        End Get
        Set(ByVal value As String)
            cReturnReasonCode = value
        End Set
    End Property

    Public Property MerchantCustomerID() As String
        Get
            Return cMerchantCustomerID
        End Get
        Set(ByVal value As String)
            cMerchantCustomerID = value
        End Set
    End Property

    Public Property FirstName() As String
        Get
            Return cFirstName
        End Get
        Set(ByVal value As String)
            cFirstName = value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return cLastName
        End Get
        Set(ByVal value As String)
            cLastName = value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return cAddress1
        End Get
        Set(ByVal value As String)
            cAddress1 = value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return cAddress2
        End Get
        Set(ByVal value As String)
            cAddress2 = value
        End Set
    End Property
    Public Property City() As String
        Get
            Return cCity
        End Get
        Set(ByVal value As String)
            cCity = value
        End Set
    End Property
    Public Property State() As String
        Get
            Return cState
        End Get
        Set(ByVal value As String)
            cState = value
        End Set
    End Property
    Public Property Zip() As String
        Get
            Return cZip
        End Get
        Set(ByVal value As String)
            cZip = value
        End Set
    End Property

    Public Property SSNumber() As String
        Get
            Return cSSNumber
        End Get
        Set(ByVal value As String)
            cSSNumber = value
        End Set
    End Property
    Public Property DLNumber() As String
        Get
            Return cDLNumber
        End Get
        Set(ByVal value As String)
            cDLNumber = value
        End Set
    End Property
    Public Property DLState() As String
        Get
            Return cDLState
        End Get
        Set(ByVal value As String)
            cDLState = value
        End Set
    End Property
    Public Property BirthDate() As Date
        Get
            Return cBirthDate
        End Get
        Set(ByVal value As Date)
            cBirthDate = value
        End Set
    End Property

    Public Property EmailAddress() As String
        Get
            Return cEmailAddress
        End Get
        Set(ByVal value As String)
            cEmailAddress = value
        End Set
    End Property
    Public Property PhoneNumber() As String
        Get
            Return cPhoneNumber
        End Get
        Set(ByVal value As String)
            cPhoneNumber = value
        End Set
    End Property
    Public Property RoutingNumber() As String
        Get
            Return cRoutingNumber
        End Get
        Set(ByVal value As String)
            cRoutingNumber = value
        End Set
    End Property
    Public Property AccountNumber() As String
        Get
            Return cAccountNumber
        End Get
        Set(ByVal value As String)
            cAccountNumber = value
        End Set
    End Property
    Public Property AccountType() As Integer
        Get
            Return cAccountType
        End Get
        Set(ByVal value As Integer)
            cAccountType = value
        End Set
    End Property
    Public Property RefundTransactionID() As Guid
        Get
            Return cRefundTransactionID
        End Get
        Set(ByVal value As Guid)
            cRefundTransactionID = value
        End Set
    End Property
    Public Property ReferenceTransactionID() As Guid
        Get
            Return cReferenceTransactionID
        End Get
        Set(ByVal value As Guid)
            cReferenceTransactionID = value
        End Set
    End Property
    Public Property RecurringItemID() As Guid
        Get
            Return cRecurringItemID
        End Get
        Set(ByVal value As Guid)
            cRecurringItemID = value
        End Set
    End Property
    Public Property ReturnID() As Guid
        Get
            Return cReturnID
        End Get
        Set(ByVal value As Guid)
            cReturnID = value
        End Set
    End Property
    Public Property SubmitOnOrAfter As DateTime
        Get
            Return cSubmitOnOrAfter
        End Get
        Set(value As DateTime)
            cSubmitOnOrAfter = value
        End Set
    End Property
    Public Property PrenoteFlag As Boolean
        Get
            Return cPrenoteFlag
        End Get
        Set(value As Boolean)
            cPrenoteFlag = value
        End Set
    End Property
    Public Property ZeroDollarFlag As Boolean
        Get
            Return cZeroDollarFlag
        End Get
        Set(value As Boolean)
            cZeroDollarFlag = value
        End Set
    End Property

#End Region

#Region "Public Class Methods"

    'Default constructor
    Public Sub New()
        cInternalTransactionID = Guid.Empty
        cMerchantTransactionID = ""
        cInternalMerchantID = Guid.Empty
        cTransactionType = TransactionTypes.Credit
        cTransactionStatus = TransactionStatuses.Pending
        cTransactionTime = New Date(1800, 1, 1)
        cTransactionAmount = 0
        cReceivedDate = New Date(2099, 1, 1)
        cOriginType = DbConstants.OriginType.UploadedFile
        cFileID = Guid.Empty
        cReferenceNumber = 0
        cClearingJobID = 0
        cStatusChangeDate = New Date(1800, 1, 1)
        cClearDate = New Date(1800, 1, 1)
        cReturnDate = New Date(1800, 1, 1)
        cEffectiveDate = New Date(1800, 1, 1)

        cMerchantCustomerID = ""
        cFirstName = ""
        cLastName = ""
        cAddress1 = ""
        cAddress2 = ""
        cCity = ""
        cState = ""
        cZip = ""
        cSSNumber = ""
        cDLNumber = ""
        cDLState = ""
        cBirthDate = New Date(1800, 1, 1)
        cEmailAddress = ""
        cPhoneNumber = "(000) 000-0000"

        cRoutingNumber = ""
        cAccountNumber = ""
        cAccountType = 1

        cRefundTransactionID = Guid.Empty
        cReferenceTransactionID = Guid.Empty

        cReturnReasonCode = ""

        cRecurringItemID = Guid.Empty
        cReturnID = Guid.Empty

        cSubmitOnOrAfter = New Date(1800, 1, 1)
        cPrenoteFlag = 0
        cZeroDollarFlag = 0

    End Sub

 
    Public Sub New(ByVal MerchantID As Guid, ByVal MerchantCustomerID As String)
        cInternalTransactionID = Guid.Empty
        cMerchantTransactionID = ""

        cInternalMerchantID = MerchantID

        cTransactionType = TransactionTypes.Credit
        cTransactionStatus = TransactionStatuses.Pending
        cTransactionTime = New Date(1800, 1, 1)
        cTransactionAmount = 0
        cReceivedDate = New Date(2099, 1, 1)
        cOriginType = DbConstants.OriginType.UploadedFile
        cFileID = Guid.Empty

        cReferenceNumber = 0
        cClearingJobID = 0
        cStatusChangeDate = New Date(1800, 1, 1)
        cClearDate = New Date(1800, 1, 1)
        cReturnDate = New Date(1800, 1, 1)
        cEffectiveDate = New Date(1800, 1, 1)

        cMerchantCustomerID = MerchantCustomerID
        cFirstName = ""
        cLastName = ""
        cAddress1 = ""
        cAddress2 = ""
        cCity = ""
        cState = ""
        cZip = ""
        cSSNumber = ""
        cDLNumber = ""
        cDLState = ""
        cBirthDate = New Date(1800, 1, 1)
        cEmailAddress = ""
        cPhoneNumber = "(000) 000-0000"

        cRoutingNumber = ""
        cAccountNumber = ""
        cAccountType = 1

        cRefundTransactionID = Guid.Empty
        cReferenceTransactionID = Guid.Empty

        cReturnReasonCode = ""

        cRecurringItemID = Guid.Empty
        cReturnID = Guid.Empty
        cSubmitOnOrAfter = New Date(2199, 12, 31)
        cPrenoteFlag = 0
        cZeroDollarFlag = 0

    End Sub


#End Region

End Class
