<Serializable()> _
Public Class ClearingJobLog

#Region "Private class variables"
    Private cJobID As Long
    Private cTimeStarted As Date
    Private cTimeEnded As Date
    Private cStatus As Integer
    Private cTotalReturnsRecords As Long
    Private cTotalReturnsRecordsInvalid As Long
    Private cTotalReturnsProcessed As Long
    Private cTotalTransactionsMarkedReturned As Long
    Private cTotalTransactionsMarkedCleared As Long
#End Region

#Region "Class attributes"
    Public Property JobID() As Long
        Get
            Return cJobID
        End Get
        Set(ByVal value As Long)
            cJobID = value
        End Set
    End Property
    Public Property TimeStarted() As Date
        Get
            Return cTimeStarted
        End Get
        Set(ByVal value As Date)
            cTimeStarted = value
        End Set
    End Property
    Public Property TimeEnded() As Date
        Get
            Return cTimeEnded
        End Get
        Set(ByVal value As Date)
            cTimeEnded = value
        End Set
    End Property
    Public Property Status() As Integer
        Get
            Return cStatus
        End Get
        Set(ByVal value As Integer)
            cStatus = value
        End Set
    End Property
    Public Property TotalReturnsRecords() As Long
        Get
            Return cTotalReturnsRecords
        End Get
        Set(ByVal value As Long)
            cTotalReturnsRecords = value
        End Set
    End Property
    Public Property TotalReturnsRecordsInvalid() As Long
        Get
            Return cTotalReturnsRecordsInvalid
        End Get
        Set(ByVal value As Long)
            cTotalReturnsRecordsInvalid = value
        End Set
    End Property
    Public Property TotalReturnsProcessed() As Long
        Get
            Return cTotalReturnsProcessed
        End Get
        Set(ByVal value As Long)
            cTotalReturnsProcessed = value
        End Set
    End Property
    Public Property TotalTransactionsMarkedReturned() As Long
        Get
            Return cTotalTransactionsMarkedReturned
        End Get
        Set(ByVal value As Long)
            cTotalTransactionsMarkedReturned = value
        End Set
    End Property
    Public Property TotalTransactionsMarkedCleared() As Long
        Get
            Return cTotalTransactionsMarkedCleared
        End Get
        Set(ByVal value As Long)
            cTotalTransactionsMarkedCleared = value
        End Set
    End Property

#End Region

#Region "Public Class Methods"

    'Default constructor
    Public Sub New()
        cJobID = 0
        cTimeStarted = New Date(1800, 1, 1)
        cTimeEnded = New Date(1800, 1, 1)
        cStatus = 0
        cTotalReturnsRecords = 0
        cTotalReturnsRecordsInvalid = 0
        cTotalReturnsProcessed = 0
        cTotalTransactionsMarkedReturned = 0
        cTotalTransactionsMarkedCleared = 0

    End Sub

#End Region

End Class
