Public Class SettlementStatementFile

#Region "Private class variables"

    'This ID points to a unique record in the table
    Private cFileID As Guid

    'File attributes
    Private cMerchantID As Guid
    Private cTimeCreated As DateTime
    Private cFileSize As Long
    Private cStatus As Int16
    Private cPeriodStartDate As DateTime
    Private cPeriodEndDate As DateTime
    Private cDescription As String
    Private cReportDataID As Guid

#End Region

#Region "Class attributes"

    Public Property FileID() As Guid
        Get
            Return cFileID
        End Get
        Set(ByVal value As Guid)
            cFileID = value
        End Set
    End Property
    Public Property MerchantID() As Guid
        Get
            Return cMerchantID
        End Get
        Set(ByVal value As Guid)
            cMerchantID = value
        End Set
    End Property
    Public Property TimeCreated() As DateTime
        Get
            Return cTimeCreated
        End Get
        Set(ByVal value As DateTime)
            cTimeCreated = value
        End Set
    End Property
    Public Property FileSize() As Long
        Get
            Return cFileSize
        End Get
        Set(ByVal value As Long)
            cFileSize = value
        End Set
    End Property
    Public Property Status() As Integer
        Get
            Return cStatus
        End Get
        Set(ByVal value As Integer)
            cStatus = value
        End Set
    End Property
    Public Property PeriodStartDate() As DateTime
        Get
            Return cPeriodStartDate
        End Get
        Set(ByVal value As DateTime)
            cPeriodStartDate = value
        End Set
    End Property
    Public Property PeriodEndDate() As DateTime
        Get
            Return cPeriodEndDate
        End Get
        Set(ByVal value As DateTime)
            cPeriodEndDate = value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return cDescription
        End Get
        Set(ByVal value As String)
            cDescription = value
        End Set
    End Property
    Public Property ReportDataID() As Guid
        Get
            Return cReportDataID
        End Get
        Set(ByVal value As Guid)
            cReportDataID = value
        End Set
    End Property

#End Region

#Region "Public Class Methods"

    'Default constructor
    Public Sub New()
        cFileID = Guid.Empty
        cMerchantID = Guid.Empty
        cTimeCreated = New Date(1800, 1, 1)
        cFileSize = 0
        cStatus = 0
        cPeriodStartDate = New Date(2099, 1, 1)
        cPeriodEndDate = New Date(2099, 1, 1)
        cDescription = ""
        cReportDataID = Guid.Empty
    End Sub

 
#End Region

End Class
