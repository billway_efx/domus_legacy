Public Class TransactionFile

#Region "Private class variables"

    'This ID points to a unique record in the TransactionFiles table
    Private cFileID As Guid

    'File attributes
    Private cFileDate As DateTime
    Private cMerchantID As Guid
    Private cFileName As String
    Private cTimeReceived As DateTime
    Private cFileSize As Long
    Private cStatus As Int16
    Private cRejectReason As String
    Private cStatusChangeDate As DateTime
    Private cNACHAFileCreationDate As String
    Private cNACHAFileIDModifier As String

#End Region

#Region "Class attributes"

    Public Property FileID() As Guid
        Get
            Return cFileID
        End Get
        Set(ByVal value As Guid)
            cFileID = value
        End Set
    End Property
    Public Property FileDate() As DateTime
        Get
            Return cFileDate
        End Get
        Set(ByVal value As DateTime)
            cFileDate = value
        End Set
    End Property
    Public Property MerchantID() As Guid
        Get
            Return cMerchantID
        End Get
        Set(ByVal value As Guid)
            cMerchantID = value
        End Set
    End Property
    Public Property FileName() As String
        Get
            Return cFileName
        End Get
        Set(ByVal value As String)
            cFileName = value
        End Set
    End Property
    Public Property TimeReceived() As DateTime
        Get
            Return cTimeReceived
        End Get
        Set(ByVal value As DateTime)
            cTimeReceived = value
        End Set
    End Property
    Public Property FileSize() As Long
        Get
            Return cFileSize
        End Get
        Set(ByVal value As Long)
            cFileSize = value
        End Set
    End Property
    Public Property Status() As Integer
        Get
            Return cStatus
        End Get
        Set(ByVal value As Integer)
            cStatus = value
        End Set
    End Property
    Public Property RejectReason() As String
        Get
            Return cRejectReason
        End Get
        Set(ByVal value As String)
            cRejectReason = value
        End Set
    End Property
    Public Property StatusChangeDate() As DateTime
        Get
            Return cStatusChangeDate
        End Get
        Set(ByVal value As DateTime)
            cStatusChangeDate = value
        End Set
    End Property
    Public Property NACHAFileCreationDate() As String
        Get
            Return cNACHAFileCreationDate
        End Get
        Set(ByVal value As String)
            cNACHAFileCreationDate = value
        End Set
    End Property
    Public Property NACHAFileIdModifier() As String
        Get
            Return cNACHAFileIDModifier
        End Get
        Set(ByVal value As String)
            cNACHAFileIDModifier = value
        End Set
    End Property
#End Region

#Region "Public Class Methods"

    'Default constructor
    Public Sub New()
        cFileID = Guid.Empty
        cFileDate = New Date(1800, 1, 1)
        cMerchantID = Guid.Empty
        cFileName = ""
        cTimeReceived = New Date(2099, 1, 1)
        cFileSize = 0
        cStatus = 0
        cRejectReason = ""
        cStatusChangeDate = Now()
        cNACHAFileCreationDate = ""
        cNACHAFileIDModifier = ""
    End Sub

    'If the MerchantID guid is known, gives empty TransactionFile object with populated MerchantID
    Public Sub New(ByVal MerchantID As Guid)
        cFileID = Guid.Empty
        cFileDate = New Date(1800, 1, 1)
        cMerchantID = MerchantID
        cFileName = ""
        cTimeReceived = New Date(1800, 1, 1)
        cFileSize = 0
        cStatus = 0
        cRejectReason = ""
        cStatusChangeDate = Now()

    End Sub

#End Region

End Class
