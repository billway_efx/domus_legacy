﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Data.SqlClient;
using EfxFramework;
using System.Xml;
using System.Messaging;
using RPO;

namespace QueueProcessor
{
    public partial class Service1 : ServiceBase
    {
        private Timer timer1 = null;

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            timer1 = new Timer();

            // Fire the timer every quarter second, but in the timer tick handler, 
            // we wait up to 10 seconds for a message to arrive before reenabling 
            // the timer to fire again after 250ms. This gives the service a chance 
            // to respond to the Stop command.
            this.timer1.Interval = 250; 
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Tick);

            QueueProcessor.DeleteLog();
            QueueProcessor.WriteErrorLog("Service started", "OnStart");
            QueueProcessor.WriteErrorLog("This log file contains a record of activity since the QueueProcessor service was started.", "OnStart");
            QueueProcessor.WriteErrorLog("Older log data is stored in ArchiveLog table where UserName='QueueProcessor'", "OnStart");

            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, ElapsedEventArgs e)
        {
            timer1.Enabled = false;
            //QueueProcessor.WriteErrorLog("Timer ticked");

            try
            {
                string queueName = @".\private$\DomusQueue";

                // check if queue exists, if not create it
                MessageQueue msMq = null;
                if (!MessageQueue.Exists(queueName))
                {
                    msMq = MessageQueue.Create(queueName, (bool)true); //Create a transactional queue
                }
                else
                {
                    msMq = new MessageQueue(queueName);
                }
                MessageQueueTransaction transaction = new MessageQueueTransaction();

                bool KeepGoing = true;
                while (KeepGoing)
                {
                    try
                    {
                        transaction.Begin();

                        //Wait up to 10 seconds for a message to arrive before timing out
                        
                        Message TheMessage = msMq.Receive(new TimeSpan(0, 0, 10), transaction);
                        TheMessage.Formatter = new System.Messaging.XmlMessageFormatter(new Type[1] { typeof(string) });
                        //QueueProcessor.WriteErrorLog("Nessage read: " + TheMessage.Body);
                        bool Result = ProcessMessage((string)TheMessage.Body);

                        if (Result == true)
                        { 
                            transaction.Commit();
                        }
                        else
                        {
                            KeepGoing = false;
                            transaction.Abort();
                        }
                    }
                    catch (MessageQueueException exx)
                    {
                        // We timed out, or there was some other problem
                        KeepGoing = false;
                        transaction.Abort();
                        if (exx.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)
                        {
                            //Unknown problem occurred
                            QueueProcessor.WriteErrorLog("Exception: " + exx.ToString(),"timer1_Tick");
                            throw (exx);
                        }
                        else
                        {
                            //Normal behavior, we will timeout if there are no messages available to process.
                            //QueueProcessor.WriteErrorLog("Timeout occurred.");
                        }
                    }
                    catch (Exception ex)
                    {
                        KeepGoing = false;
                        transaction.Abort();
                        QueueProcessor.WriteErrorLog("Exception: " + ex.ToString(),"timer1_Tick");
                    }
                }
                transaction.Dispose();
                msMq.Close();

                // Salcedo - 7/19/2016 - changed logic to use MSMQ (above)
                ////The 'Queue_Get_Single_Record' procedure will wait 10 seconds for messages to arrive in the queue
                //SqlDataReader dr = EfxFramework.NewDataAccess.ExecuteProcedure(Settings.ConnectionString, "Queue_Get_Single_Record", (SqlParameter[])null);
                //if (dr.HasRows)
                //{
                //    while (dr.Read())
                //    {
                //        //Process each message
                //        string TheMessage = dr.GetString(dr.GetOrdinal("message_body"));
                //        if (TheMessage != "")
                //        {
                //            //QueueProcessor.WriteErrorLog("Message received: " + TheMessage);
                //            ProcessMessage(TheMessage);
                //        }
                //    }
                //}
                //dr.Close();

            }
            catch(Exception ex)
            {
                QueueProcessor.WriteErrorLog("Exception: " + ex.ToString(),"timer1_Tick");
            }

            timer1.Enabled = true;
        }
        protected override void OnStop()
        {
            timer1.Enabled = false;
            QueueProcessor.WriteErrorLog("Service stopped","OnStop");
        }

        private bool ProcessMessage(string TheMessage)
        {
            bool TheReturn = false;

            XmlDocument TheDoc = new XmlDocument();
            TheDoc.LoadXml(TheMessage);
            XmlNode root = TheDoc.FirstChild;

            string NodeName = root.Name;
            //QueueProcessor.WriteErrorLog("  NodeName: " + NodeName);

            string InnerXmlValue = root.InnerXml;
            //QueueProcessor.WriteErrorLog("  InnerXmlValue: " + InnerXmlValue);

            if (NodeName == "RenterLogin")
            { 
                ProcessRenterLogin(InnerXmlValue);
                TheReturn = true;
            }
            if (NodeName == "RenterPaymentProcessed")
            { 
                ProcessRenterPaymentProcessed(InnerXmlValue);
                TheReturn = true;
            }
            if (NodeName == "FinalizePostAuth")
            { 
                TheReturn = ProcessFinalizePostAuth(InnerXmlValue);
            }
            if (NodeName == "ProcessAchQueue")
            {
                TheReturn = ProcessAchQueue(TheMessage);
            }

            return TheReturn;
        }

        private void ProcessRenterLogin(string StringRenterId)
        {
            // When a user logs in, we want to retrieve the latest balances from the third party property management system.
            // After doing that, we also need to update the wallet and payables tables for that user.

            try
            {
                QueueProcessor.WriteErrorLog("Processing RenterLogin message for RenterId: " + StringRenterId, "ProcessRenterLogin");
                int RenterId = Convert.ToInt32(StringRenterId);

                int? PropertyId = EfxFramework.Renter.GetPropertyByRenterId(RenterId);
                if (PropertyId.HasValue)
                {
                    var Property = new EfxFramework.Property(PropertyId.Value);
                    if (Property.PmsTypeId.HasValue)
                    {
                        switch (Property.PmsTypeId)
                        {
                            case 1:
                                // Yardi
                                // EfxFramework.Pms.Yardi.ImportRequest.UpdateRenterBalanceDueRealTimeYardi(Resident);
                                var Resident = new EfxFramework.Renter(RenterId);
                                EfxFramework.Pms.Yardi.ImportRequest.UpdateRenterBalanceDueRealTimeYardi(Resident);
                                break;
                            case 2:
                                // AMSI
                                //EfxFramework.Pms.AMSI.AmsiRequest.AMSI_RequestXmlForRenterImport(2, _RenterID: Resident.RenterId);
                                EfxFramework.Pms.AMSI.AmsiRequest.AMSI_RequestXmlForRenterImport(2, _RenterID: RenterId);
                                break;
                            case 3:
                                // RealPage - not supported for real time updates
                                break;
                            case 4:
                                // MRI
                                // EfxFramework.Pms.MRI.MriRequest.MRI_ProcessSingleRenter(Renter.RenterId);
                                EfxFramework.Pms.MRI.MriRequest.MRI_ProcessSingleRenter(RenterId);
                                break;
                            default:
                                break;
                        }

                        // Update/synchronize wallet/payables tables
                        UpdateWalletAndPayables(RenterId);
                    }
                }
            }
            catch (Exception ex)
            {
                QueueProcessor.WriteErrorLog("Exception: " + ex.ToString(),"ProcessRenterLogin");
            }
            
        }

        private void ProcessRenterPaymentProcessed(string StringRenterId)
        {
            QueueProcessor.WriteErrorLog("Processing RenterPaymentProcessed message for RenterId: " + StringRenterId,"ProcessRenterPaymentProcessed");
            int RenterId = Convert.ToInt32(StringRenterId);

            // If a payment has been made, we need to update the wallet and the payables tables
            UpdateWalletAndPayables(RenterId);
        }

        private bool ProcessFinalizePostAuth(string TransactionId)
        {
            bool TheReturn = false;

            try
            {
                QueueProcessor.WriteErrorLog("Processing FinalizePostAuth message for TransactionId: " + TransactionId,"ProcessFinalizePostAuth");

                // Call stored procedure to finalize everything associated with the PostAuth Transaction Id
                // This includes adding/updating payment/transaction records, updating resident's balance, sending an email to resident, etc.
                // Information related to this TransactionId was persisted on the original call to PreAuth the credit card.
                // The information from the PreAuth was stored in the PreAuthTransactions table.
                try
                {
                    //int IntTransactionId = Convert.ToInt32(TransactionId);
                    EfxFramework.NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "usp_Payment_ProcessFinalizePostAuth",
                        new[] { new SqlParameter("@ExternalPreAuthTransactionId", TransactionId) });

                    //Returning true allows the MSMQ transaction receive operation to be committed. Otherwise, the message stays in the queue.
                    TheReturn = true;
                }
                catch (SqlException Sqlex)
                {
                    //This exception will be raised by the SP if the SQL transaction was unsuccessful.
                    //This will cause the MSMQ to remain in the queue, and the attempt will be made to process it again.
                    //From SP: RAISERROR('Transaction rolled back',16,1)
                    QueueProcessor.WriteErrorLog("Sql Exception: " + Sqlex.ToString(), "ProcessFinalizePostAuth");
                }
                catch (Exception ex)
                {
                    QueueProcessor.WriteErrorLog("Exception: " + ex.ToString(),"ProcessFinalizePostAuth");
                }
            }
            catch (Exception ex)
            {
                QueueProcessor.WriteErrorLog("Exception: " + ex.ToString(),"ProcessFinalizePostAuth");
            }

            return TheReturn;
        }

        private void UpdateWalletAndPayables(int RenterId)
        {
            try
            {
                EfxFramework.NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "Pay_PayableControllerWalletItemsBuild",
                    new[] { new SqlParameter("@RenterId", RenterId) });
            }
            catch (Exception ex)
            {
                QueueProcessor.WriteErrorLog("Exception: " + ex.ToString(),"UpdateWalletAndPayables");
            }
        }

        private bool ProcessAchQueue(string MessageXml)
        {
            bool TheReturn = false;

            try
            {
                XmlDocument TheDoc = new XmlDocument();
                TheDoc.LoadXml(MessageXml);
                XmlNode root = TheDoc.FirstChild;

                string TransactionId = root.Attributes.GetNamedItem("TransactionId").Value;
                string SplitPaymentGroupCompleteFlag = root.Attributes.GetNamedItem("SplitPaymentGroupCompleteFlag").Value;

                QueueProcessor.WriteErrorLog("Processing QueuedAchTransactionId: '" + TransactionId + 
                    "'. SplitPaymentGroupCompleteFlag: '" + SplitPaymentGroupCompleteFlag + "'.", "ProcessAchQueue");

                try
                {
                    // This SP creates an actual ACH transaction from the entry in the queue, 
                    // updates the resident balance, add records to payment and transaction tables,  
                    // processes against the SplitPaymentGroup table, and sends an email if all the 
                    // related Split payments have been completed.
                    int iSplitPaymentGroupCompleteFlag = 0;
                    if (Convert.ToBoolean(SplitPaymentGroupCompleteFlag)) iSplitPaymentGroupCompleteFlag = 1;
                    EfxFramework.NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "usp_QueuedAchTransactions_Finalize",
                        new[] { 
                            new SqlParameter("@TransactionId", TransactionId),
                            new SqlParameter("@SplitPaymentGroupCompleteFlag", iSplitPaymentGroupCompleteFlag)
                        });

                    //Returning true allows the MSMQ transaction receive operation to be committed. Otherwise, the message stays in the queue.
                    TheReturn = true;
                }
                catch (SqlException Sqlex)
                {
                    //This exception will be raised by the SP if the SQL transaction was unsuccessful.
                    //This will cause the MSMQ to remain in the queue, and the attempt will be made to process it again.
                    //From SP: RAISERROR('Transaction rolled back',16,1)
                    QueueProcessor.WriteErrorLog("Sql Exception: " + Sqlex.ToString(), "ProcessFinalizePostAuth");
                }
                catch (Exception ex)
                {
                    QueueProcessor.WriteErrorLog("Exception: " + ex.ToString(), "ProcessFinalizePostAuth");
                }

            }
            catch (Exception ex)
            {
                QueueProcessor.WriteErrorLog("Exception: " + ex.ToString(), "ProcessFinalizePostAuth");
            }

            return TheReturn;
        }
    }
}
