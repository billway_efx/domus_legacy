﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace QueueProcessor
{
    public static class QueueProcessor
    {
        public static void DeleteLog()
        {
            try
            { 
                File.Delete(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt");
            }
            catch
            { }
        }

        public static void WriteErrorLog(string Message, string FunctionName)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt",true);
                sw.WriteLine(DateTime.Now.ToString() + " " + FunctionName + ": " + Message);
                sw.Flush();
                sw.Close();

                var log = new RPO.ActivityLog();
                log.WriteLog("QueueProcessor", FunctionName + ": " + Message, 1, "", false);
            }
            catch (Exception ex)
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + "Exception occurred in WriteErrorLog: " + ex.ToString());
                sw.Flush();
                sw.Close();
            }
        }
    }
}
