﻿Instructions for installing the QueueProcessor service:
========================================================

1. Open a Command Prompt window as Administrator.

2. Change to the directory in which QueueProcessor.exe exists (use the CD command). Example:

	CD c:\rpo-service

3. Uninstall the old version of the service (if it was previously installed) using this command:

	C:\Windows\Microsoft.NET\Framework64\v4.0.30319\installutil /u "QueueProcessor.exe"

	Note:  installutil.exe might be located in a different location on your server

4. Install the new version of the service, using this command:

	C:\Windows\Microsoft.NET\Framework64\v4.0.30319\installutil "QueueProcessor.exe"

5. From the Services console, right-click the service (it is named "Queue Processor Service") and click "Start" to start the service.
