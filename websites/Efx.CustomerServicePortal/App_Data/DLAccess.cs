﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using RPO;
using EfxFramework;

public class DLAccess
{

    //cakel: BUGID0008
    public class GetRenterDetail
    {
        public string renterID;
        public string leaseID;
        //cakel: BUGID00085 added 
        public string rentAmount;
        //cakel: BUGID00085 added
        public DateTime paymentDueDate;
        public DateTime LeaseStart;
        public DateTime LeaseEnd;
        //cakel BUGID00085 Part 2 Added
        public int IsIntegrated;  //value = 1 for integrated or NO for non-integrated
        //cakel: BUGID000116
        public string AcceptedPaymentTypeId;
    }



    //New Gridview 
    public static SqlDataReader GetmonthlyFeeByID(int _Rentid)
    {
        SqlConnection con = new SqlConnection(ConnString);
        SqlCommand com = new SqlCommand("usp_MonthlyFee_GetMonthlyFeesByRenterId", con);
        com.CommandType = CommandType.StoredProcedure;
        com.Parameters.Add("@RenterId", SqlDbType.Int).Value = _Rentid;
        try
        {
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }
        catch (Exception ex)
        {
            con.Close();
            throw ex;
        }
    }


    //cakel: BUGID0008 - Needed to class to return values to set date picker on lease start and end date.
    //Returns renter details for the lease start and end date
    //cakel BUGID00085 - part 2 updated command string to SP
    //cakel: BUGID000116 updated SP to return more detail for the renter
    public static GetRenterDetail RenterDetails(string RenterID)
    {
        SqlConnection con = new SqlConnection(ConnString);
        //Sql command returns renter to lease information
        //cakel: BUGID00085 - updated SP string - added new fields
        SqlCommand com = new SqlCommand("usp_Renter_GetRenterDetails_V2", con);
        com.CommandType = CommandType.StoredProcedure;
        com.Parameters.Add("@RenterID", SqlDbType.Int).Value = RenterID;
        con.Open();
        SqlDataReader reader = com.ExecuteReader();
        reader.Read();

        GetRenterDetail CurrentRenter = new GetRenterDetail();

        if (reader.HasRows)
        {
            CurrentRenter.leaseID = reader["LeaseId"].ToString();
            CurrentRenter.renterID = reader["RenterID"].ToString();
            //cakel: BUGID00085 - added
            CurrentRenter.rentAmount = reader["RentAmount"].ToString();
            //cakel: BUGID00085 - added
            CurrentRenter.paymentDueDate = Convert.ToDateTime(reader["PaymentDueDate"]);
            CurrentRenter.LeaseStart = Convert.ToDateTime(reader["StartDate"]);
            CurrentRenter.LeaseEnd = Convert.ToDateTime(reader["EndDate"]);
            //cakel BUGID00085 - part 2 added
            CurrentRenter.IsIntegrated = (int)reader["IsIntegerated"];
            //cakel: BUGID000116
            CurrentRenter.AcceptedPaymentTypeId = reader["AcceptedPaymentTypeId"].ToString();
        }
        else
        {
            CurrentRenter.leaseID = "0";
            CurrentRenter.renterID = RenterID;
            CurrentRenter.rentAmount = "0.00";
            CurrentRenter.paymentDueDate = DateTime.Now;
            CurrentRenter.LeaseStart = DateTime.Now;
            CurrentRenter.LeaseEnd = DateTime.Now.AddYears(1);
            //cakel BUGID00085 Part 2 - Added
            CurrentRenter.IsIntegrated = 0;
            //cakel: BUGID000116
            CurrentRenter.AcceptedPaymentTypeId = "0";
        }

        con.Close();
        reader.Close();

        return CurrentRenter;

    }


    // Get Fees by Renter ID for Lease Tab
    public static SqlDataReader GetFeesList(string RenterID)
    {
        SqlConnection con = new SqlConnection(ConnString);
        SqlCommand com = new SqlCommand("SELECT MonthlyFeeId,FeeName,DefaultFeeAmount FROM MonthlyFee WHERE PropertyId in (SELECT PropertyId FROM PropertyRenter WHERE RENTERID =" + RenterID + ") AND IsActive = 1", con);
        com.CommandType = CommandType.Text;
        try
        {
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }
        catch (Exception ex)
        {
            con.Close();
            throw ex;
        }

    }

    //cakel: BUGID 00085 Part 3 - This will replace command in gridview
    public static void DeleteFeeByRenterID(String FeeID, int RenterID)
    {
        string Renter = RenterID.ToString();
        string MySqlString = "DELETE FROM LeaseFee WHERE RenterId = " + Renter + " and MonthlyFeeId = " + FeeID;
        SqlConnection con = new SqlConnection(ConnString);
        SqlCommand com = new SqlCommand(MySqlString, con);
        com.CommandType = CommandType.Text;
        try
        {
            con.Open();
            com.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            throw ex;

        }
        finally
        {
            con.Close();
        }


    }



    public static SqlDataReader GetFeeAmount(int FeeID)
    {
        SqlConnection con = new SqlConnection(ConnString);
        SqlCommand com = new SqlCommand("SELECT MonthlyFeeId, DefaultFeeAmount FROM MonthlyFee WHERE MonthlyFeeId =" + FeeID, con);
        com.CommandType = CommandType.Text;
        try
        {
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }
        catch (Exception ex)
        {
            con.Close();
            throw ex;
        }
    }



    //cakel: BugID00033
    //Updates Renter application status - Insert function is handled by "Set" function in ApplicantApplication.cs
    public static void SetApplicationStatus(int _ApplicationID, int _AppStatusID, int _AppSubmissionTypeID, string _UpdatedBy)
    {
        SqlConnection con = new SqlConnection(ConnString);
        SqlCommand com = new SqlCommand("usp_ApplicantApplication_UpdateSingleObject", con);
        com.CommandType = CommandType.StoredProcedure;
        com.Parameters.Add("@ApplicantApplicationId", SqlDbType.Int).Value = _ApplicationID;
        com.Parameters.Add("@ApplicationStatusId", SqlDbType.Int).Value = _AppStatusID;
        com.Parameters.Add("@ApplicationSubmissionTypeId", SqlDbType.Int).Value = _AppSubmissionTypeID;
        com.Parameters.Add("@StatusUpdatedBy", SqlDbType.NVarChar, 100).Value = _UpdatedBy;
        try
        {
            con.Open();
            com.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            con.Close();
        }

    }


    //cakel: BUGID00085
    public static int GetDay(DateTime _day)
    {
        DateTime NewDay = _day;
        int day = NewDay.Day;
        return day;
    }
    //cakel: BUGID00085
    public static int GetMonthNum(DateTime _month)
    {
        DateTime NewMonth = _month;
        int month = NewMonth.Month;
        return month;
    }
    //cakel: BUGID00085
    public static int GetYearNum(DateTime _year)
    {
        DateTime NewYear = _year;
        int year = NewYear.Year;
        return year;
    }

    public static int GetMaxMonthDay(int monthNum, int yearNum)
    {
        string _day = monthNum.ToString();
        string _Year = yearNum.ToString();
        DateTime TheDate = Convert.ToDateTime(_day + "/" + "1" + "/" + _Year);
        TheDate = TheDate.AddMonths(1);
        TheDate = TheDate.AddDays(-1);
        string TheLastDay = TheDate.Day.ToString();
        return Convert.ToInt32(TheLastDay);
    }


    public static SqlDataReader GetPropertyDetailsByRenter(int RenterID)
    {
        SqlConnection con = new SqlConnection(ConnString);
        SqlCommand com = new SqlCommand("usp_Property_GetDetailsByRenter", con);
        com.CommandType = CommandType.StoredProcedure;
        com.Parameters.Add("@RenterID", SqlDbType.Int).Value = RenterID;
        try
        {
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }
        catch (Exception ex)
        {
            con.Close();
            throw ex;
        }
    }

    //cakel: 



    public static int SetLeaseDetailsByRenter(int renterID, int leaseID, int propertyID, int propertyStaffID, string unitNumber,
        decimal rentAmount, int rentDueDayOfMonth, int numberOfTenants, DateTime startDate, DateTime endDate, decimal beginningBalance,
        DateTime beginningBalanceDate, DateTime paymentDueDate, int UsesUpdatedCurrentBalanceCalc)
    {

        SqlConnection con = new SqlConnection(ConnString);
        SqlCommand com = new SqlCommand("usp_Lease_SetSingleObject_v2", con);
        com.CommandType = CommandType.StoredProcedure;
        com.Parameters.Add("@RenterID", SqlDbType.Int).Value = renterID;
        com.Parameters.Add("@LeaseID", SqlDbType.Int).Value = leaseID;
        com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = propertyID;
        com.Parameters.Add("@PropertyStaffId", SqlDbType.Int).Value = propertyStaffID;
        com.Parameters.Add("@UnitNumber", SqlDbType.NVarChar, 20).Value = unitNumber;
        com.Parameters.Add("@RentAmount", SqlDbType.Money).Value = rentAmount;
        com.Parameters.Add("@RentDueDayOfMonth", SqlDbType.Int).Value = rentDueDayOfMonth;
        com.Parameters.Add("@NumberOfTenants", SqlDbType.Int).Value = numberOfTenants;
        com.Parameters.Add("@StartDate", SqlDbType.Date).Value = startDate;
        com.Parameters.Add("@EndDate", SqlDbType.Date).Value = endDate;
        com.Parameters.Add("@BeginningBalance", SqlDbType.Money).Value = beginningBalance;
        com.Parameters.Add("@BeginningBalanceDate", SqlDbType.Date).Value = beginningBalanceDate;
        com.Parameters.Add("@PaymentDueDate", SqlDbType.Date).Value = paymentDueDate;
        com.Parameters.Add("@UsesUpdatedCurrentBalanceCalc", SqlDbType.Int).Value = UsesUpdatedCurrentBalanceCalc;
        com.Parameters.Add("@LeaseID_out", SqlDbType.Int).Direction = ParameterDirection.Output;

        try
        {
            int _leaseIDOut = 0;
            con.Open();
            com.ExecuteNonQuery();
            _leaseIDOut = (int)com.Parameters["@LeaseID_out"].Value;
            con.Close();
            return _leaseIDOut;

        }
        catch (Exception ex)
        {
            con.Close();
            throw ex;

        }

    }

    public static void SetLeaseFeeToRenter(int monthlyfeeID, int leaseID, int renterID, decimal feeAmount)
    {
        SqlConnection con = new SqlConnection(ConnString);
        SqlCommand com = new SqlCommand("usp_LeaseFee_SetSingleObject", con);
        com.CommandType = CommandType.StoredProcedure;
        com.Parameters.Add("@MonthlyFeeId", SqlDbType.Int).Value = monthlyfeeID;
        com.Parameters.Add("@LeaseId", SqlDbType.Int).Value = leaseID;
        com.Parameters.Add("@RenterId", SqlDbType.Int).Value = renterID;
        com.Parameters.Add("@FeeAmount", SqlDbType.Money).Value = feeAmount;
        com.Parameters.Add("@IsActive", SqlDbType.Bit).Value = 1;
        try
        {
            con.Open();
            com.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            con.Close();
        }

    }

    public static int SetNewMonthlyFee(int monthlyFeeID, int propertyID, string FeeName, decimal defaultFeeAmount)
    {
        SqlConnection con = new SqlConnection(ConnString);
        SqlCommand com = new SqlCommand("usp_MonthlyFee_SetSingleObject_V2", con);
        com.CommandType = CommandType.StoredProcedure;
        com.Parameters.Add("@MonthlyFeeId", SqlDbType.Int).Value = monthlyFeeID;
        com.Parameters.Add("@PropertyId", SqlDbType.Int).Value = propertyID;
        com.Parameters.Add("@FeeName", SqlDbType.NVarChar, 100).Value = FeeName;
        com.Parameters.Add("@DefaultFeeAmount", SqlDbType.Money).Value = defaultFeeAmount;
        com.Parameters.Add("@IsActive", SqlDbType.Int).Value = 1;
        com.Parameters.Add("@ChargeCode", SqlDbType.NVarChar, 50).Value = "";
        com.Parameters.Add("@MonthFeeID_Out", SqlDbType.Int).Direction = ParameterDirection.Output;
        try
        {
            int Feeid_out = 0;
            con.Open();
            com.ExecuteNonQuery();
            Feeid_out = (int)com.Parameters["@MonthFeeID_Out"].Value;
            con.Close();
            return Feeid_out;

        }
        catch (Exception ex)
        {
            con.Close();
            throw ex;
        }
    }

    //cakel: BUGID00085 Part 2 - Update Current Balance For Renter
    public static void UpdateRenterCurrentBalance(DateTime PaymentDueDate, int LeaseID, Decimal RentAmount)
    {
        SqlConnection con = new SqlConnection(ConnString);
        SqlCommand com = new SqlCommand("usp_Lease_UpdateRentersBalance",con);
        com.CommandType = CommandType.StoredProcedure;
        com.Parameters.Add("@PaymentDate", SqlDbType.Date).Value = PaymentDueDate;
        com.Parameters.Add("@LeaseID", SqlDbType.Int).Value = LeaseID;
        com.Parameters.Add("@RentAmount", SqlDbType.Money).Value = RentAmount;
        try
        {
            con.Open();
            com.ExecuteNonQuery();
        }
        catch(Exception ex)
        {
            throw ex;
        }
        finally
        {
            con.Close();
        }

    }

    //cakel: BUGID00085 Part 3
    public static SqlDataReader GetTotalDueByRenterID(int renterid)
    {

        SqlConnection con = new SqlConnection(ConnString);
        SqlCommand com = new SqlCommand("usp_Lease_GetTotalDueByRenterID", con);
        com.CommandType = CommandType.StoredProcedure;
        com.Parameters.Add("@RenterID", SqlDbType.Int).Value = renterid;
        try
        {
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);

        }
        catch (Exception ex)
        {
            con.Close();
            throw ex;
        }

    }

    //cakel: BUGID00085 Part 3
    public static SqlDataReader GetFeeDetailByRenter(int renterid)
    {
        

        SqlConnection con = new SqlConnection(ConnString);
        SqlCommand com = new SqlCommand("usp_Lease_FeeDetailBYRenter", con);
        com.CommandType = CommandType.StoredProcedure;
        com.Parameters.Add("@RenterID", SqlDbType.Int).Value = renterid;
        try
        {
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);

        }
        catch (Exception ex)
        {
            con.Close();
            throw ex;
        }

    }



    public static void UpdateLeaseFee()
    {
        SqlConnection con = new SqlConnection(ConnString);
        SqlCommand com = new SqlCommand("usp_Lease_UpdateTotalFeesForNonIntegratedProperties", con);
        com.CommandType = CommandType.StoredProcedure;
        try
        {
            con.Open();
            com.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            con.Close();
        }

    }

    public static void UpdateNonIntegratedLeases()
    {
        SqlConnection con = new SqlConnection(ConnString);
        SqlCommand com = new SqlCommand("usp_Lease_AddCurrentBalanceDueForNonIntegratedProperties", con);
        com.CommandType = CommandType.StoredProcedure;


        try
        {
            con.Open();
            com.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            con.Close();
        }

    }


    //cakel: BUGID00213 - Added function to return property details
    public static SqlDataReader GetPropertyDetailsByPropertyID(string _PropertyID)
    {
        SqlConnection con = new SqlConnection(ConnString);
        SqlCommand com = new SqlCommand("usp_Property_GetSingleObject_v4", con);
        com.CommandType = CommandType.StoredProcedure;
        com.Parameters.Add("@PropertyId",SqlDbType.Int).Value = _PropertyID;

        try
        {
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }
        catch (Exception ex)
        {
            con.Close();
            throw ex;
        }

    }





    private static String ConnString
    {
        get
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }
    }

}