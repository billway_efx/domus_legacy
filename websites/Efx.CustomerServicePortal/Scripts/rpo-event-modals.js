﻿var calendarEventApp = angular.module('rpoEventModals', ['API', '$strap.directives'])
.config(function ($locationProvider) {
    var html5Ok = navigator.appVersion.indexOf('MSIE') == -1;
    $locationProvider.html5Mode(html5Ok).hashPrefix('');
});
calendarEventApp.value('$strapConfig', {
    datepicker: {
        language: 'en',
        format: 'mm/dd/yy'
    }
});
function CalendarEventController($scope, calendarEventService) {
    $scope.launchEventModal = function () {
        calendarEventService.createEvent();
        
    };
}

function EventDetailController($rootScope, $scope, calendarEventService) {
    $rootScope.$broadcast('Details');
    //$rootScope.$on('Event:Show', function () {
    console.log('getting event to show...');
    eventId = calendarEventService.getEventId();
    if (eventId) {
        calendarEventService.getEvent(eventId).then(function (event) {
            $scope.event = event;
        });
    }
    //});
}
angular.bootstrap($('#event-modals'), ["rpoEventModals"]);