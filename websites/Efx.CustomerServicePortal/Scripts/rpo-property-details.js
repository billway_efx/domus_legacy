﻿var propertyApp = angular.module('rpoPropertyDetails', ['$strap.directives', 'pagination', 'API', 'ngAnimate', 'ajoslin.promise-tracker']).config(function ($locationProvider) {
    var html5Ok = navigator.appVersion.indexOf('MSIE') == -1;
    $locationProvider.html5Mode(html5Ok).hashPrefix('');
});

function PropertyController($scope, $http, $location, $q, $rootScope, $filter, propertyService, promiseTracker) {
    $scope.urlPrefix = navigator.appVersion.indexOf('MSIE') == -1 ? "" : "/rpo#";
    $scope.viewTitle = 'Property Data';
    if (navigator.appVersion.indexOf('MSIE 8.0') == -1) {
    	$scope.loading = promiseTracker('property');
    }
    var propertyId = $.getUrlVar('propertyId');
    var propertyPromise = propertyService.getProperty(propertyId);
    if (navigator.appVersion.indexOf('MSIE 8.0') == -1) {
    	$scope.loading.addPromise(propertyPromise);
    }
    propertyPromise.then(function (property) {
        $scope.property = property;
    });
	
    $scope.hideTabs = function () {
        $('#NameTab').hide();
        $('#BankingTab').hide();
        $('#StaffTab').hide();
        $('#AnnouncementTab').hide();
        $('#RentAndFeesTab').hide();
        $('#ApplicationTab').hide();
    };
    $scope.showTab = function (tab) {
        $scope.hideTabs();
        $('#' + tab).show();
        $scope.activeTab = tab;
    };
    $scope.deleteProperty = function () {
    	propertyService.deleteProperty(propertyId).then(function (success) {
    		if (success) {
    			window.location.href = '/rpo/properties';
    		} else {
    		    alert('Failed to delete Property');
    		}
    	});
    };
    //$scope.toggleActive = function () {
    //    propertyService.toggleActive($scope.property).then(function (success) {
    //        if (success) {
    //            propertyService.getProperty(propertyId).then(function (renter) {
    //                angular.extend($scope.property, property);
    //            });
    //        } else {
    //            alert('Failed to toggle Property Active/InActive');
    //        }
    //    });


    //};

    $scope.hideTabs();

	//select the correct tab if specified
    var currentPath = $location.absUrl();
    if (currentPath.length > 0) {
    	if (currentPath.indexOf("NameTab") != -1) {
    		$scope.showTab('NameTab');
    	}
    	else if (currentPath.indexOf("BankingTab") != -1) {
    		$scope.showTab('BankingTab');
    	}
    	else if (currentPath.indexOf("StaffTab") != -1) {
    		$scope.showTab('StaffTab');
    	}
    	else if (currentPath.indexOf("AnnouncementTab") != -1) {
    		$scope.showTab('AnnouncementTab');
    	}
    	else if (currentPath.indexOf("RentAndFeesTab") != -1) {
    		$scope.showTab('RentAndFeesTab');
    	}
    	else if (currentPath.indexOf("ApplicationTab") != -1) {
    		$scope.showTab('ApplicationTab');
    	}
    	else {
    		$scope.showTab('NameTab');
    	}
    }
	else {
    	if ($location.absUrl().indexOf("ApplicantApplicationId") != -1) {
    		$location.path('ApplicationTab');
    		$scope.showTab('ApplicationTab');
    	}
    	else if ($location.absUrl().indexOf("tab=BankingTab") != -1) {
    		$location.path('BankingTab');
    		$scope.showTab('BankingTab');
    	}
    	else if ($location.absUrl().indexOf("tab=StaffTab") != -1) {
    		$location.path('StaffTab');
    		$scope.showTab('StaffTab');
    	}
    	else {
    		$scope.showTab('NameTab');
    	}
    }
}
angular.bootstrap($('#propertyManager'), ["rpoPropertyDetails"]);