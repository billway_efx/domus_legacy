﻿//app
var residentApp = angular.module('rpoResidentDetail', ['ngRoute', '$strap.directives', 'transactionServices', 'pagination', 'API', 'ngAnimate', 'ajoslin.promise-tracker']);
//directives for validation
residentApp.directive('decimal', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            //initialize to valid
            scope.$watch('decimal', function () {
                ctrl.$setViewValue(ctrl.$viewValue);
            });
            var decimalValidator = function (value) {
                var DECIMAL_REGEXP = /^\d+(\.\d{1,2})?$/;
                if (!isEmpty(value) && !DECIMAL_REGEXP.test(value)) {
                    // it is valid
                    ctrl.$setValidity('decimal', false);
                    return undefined;
                } else {
                    // it is invalid, return undefined (no model update)
                    ctrl.$setValidity('decimal', true);
                    return value;
                }
            };

            ctrl.$parsers.push(decimalValidator);
            ctrl.$formatters.push(decimalValidator);

            //ctrl.$parsers.unshift(function (viewValue) {
            //    var DECIMAL_REGEXP = /^\d+(\.\d{1,2})?$/;
            //    if (!DECIMAL_REGEXP.test(viewValue)) {
            //        // it is valid
            //        ctrl.$setValidity('decimal', false);
            //        return undefined;
            //    } else {
            //        // it is invalid, return undefined (no model update)
            //        ctrl.$setValidity('decimal', true);
            //        return viewValue;
            //    }
            //});

        }
    };
});
residentApp.directive('decimalPlaces', function () {
    return {
        link: function (scope, ele, attrs) {
            ele.bind('keypress', function (e) {
                var newVal = $(this).val() + (e.charCode !== 0 ? String.fromCharCode(e.charCode) : '');
                if ($(this).val().search(/(.*)\.[0-9][0-9]/) === 0 && newVal.length > $(this).val().length) {
                    e.preventDefault();
                }
            });
        }
    };
});
residentApp.directive('ngMin', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            scope.$watch(attr.ngMin, function () {
                ctrl.$setViewValue(ctrl.$viewValue);
            });
            var minValidator = function (value) {
                var min = scope.$eval(attr.ngMin) || 0;
                if (!isEmpty(value) && value < min) {
                    ctrl.$setValidity('ngMin', false);
                    return undefined;
                } else {
                    ctrl.$setValidity('ngMin', true);
                    return value;
                }
            };

            ctrl.$parsers.push(minValidator);
            ctrl.$formatters.push(minValidator);
        }
    };
});

residentApp.directive('ngMax', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            scope.$watch(attr.ngMax, function () {
                ctrl.$setViewValue(ctrl.$viewValue);
            });
            var maxValidator = function (value) {
                var max = scope.$eval(attr.ngMax) || Infinity;
                if (!isEmpty(value) && value > max) {
                    ctrl.$setValidity('ngMax', false);
                    return undefined;
                } else {
                    ctrl.$setValidity('ngMax', true);
                    return value;
                }
            };

            ctrl.$parsers.push(maxValidator);
            ctrl.$formatters.push(maxValidator);
        }
    };
});
//controllers
function ResidentController($scope, $http, $routeParams, $location, $q, $modal, $rootScope, $filter, Payment, renterService, promiseTracker) {
    $scope.urlPrefix = navigator.appVersion.indexOf('MSIE') == -1 ? "" : "/rpo/#!";
    $scope.params = $routeParams;
    $scope.payment = Payment;
    $scope.viewTitle = 'Resident Data';
    
    var renterId = $routeParams.residentId != null ? $routeParams.residentId : $.getUrlVar('renterId');
    var renterPromise = renterService.getRenter(renterId);
    if (navigator.appVersion.indexOf('MSIE 8.0') == -1) {
        $scope.loading = promiseTracker('renter');
        $scope.loading.addPromise(renterPromise);
    }
    renterPromise.then(function (renter) {
        $scope.renter = renter;
    });
    $scope.hideTabs = function () {
        $('#NameTab').hide();
        $('#PayersInfo').hide();
        $('#PaymentInformationTab').hide();
        $('#LeaseInformationTab').hide();
        $('#HistoryTab').hide();
        $('#NotesTab').hide();
    };
    $scope.hideTransactions = function () {
        $scope.showAllTransactions = false;
        angular.forEach($scope.renter.PaymentHistory, function (value) {
            value.showTransactions = false;
        });
    };
    $scope.refundTransaction = function (payment) {
        $scope.payment.refundPayment(payment);
    };
    $scope.showTab = function (tab) {
        $scope.hideTabs();
        $('#' + tab).show();
        $scope.activeTab = tab;
    };
    $scope.showTransactions = function () {
        $scope.showAllTransactions = true;
        angular.forEach($scope.renter.PaymentHistory, function (value) {
            value.showTransactions = true;
        });
    };
    $scope.toggleActive = function () {
        renterService.toggleActive($scope.renter).then(function (success) {
            if (success) {
                renterService.getRenter(renterId).then(function (renter) {
                    angular.extend($scope.renter, renter);
                });
            } else {
                alert('Failed to toggle Resident Active/InActive');
            }
        });


    };
    $scope.togglePaymentTransactions = function (payment) {
        payment.showTransactions = !payment.showTransactions;
    };
    $scope.voidTransaction = function (payment) {
        $scope.payment.voidPayment(payment);
    };

    $scope.hideTabs();
    $scope.showTab('NameTab');
}
function VoidTransactionController($scope, $http, $rootScope, Payment) {
    $scope.payment = Payment;

    $rootScope.$on('payment:void', function () {
        $scope.paymentToVoid = Payment.getPaymentObject();
    });
    $scope.submit = function () {
        Payment.processVoid().then(function (response) {
            Payment.getUpdatedPaymentObject($scope.paymentToVoid.PaymentId).then(function (payment) {
                angular.extend($scope.paymentToVoid, payment);
            });
        });
    };
}
function RefundController($scope, $http, $rootScope, Payment) {
    $scope.payment = Payment;
    $scope.refundButtonText = "Issue Refund";
    $rootScope.$on('payment:refund', function () {
        $scope.paymentToRefund = Payment.getPaymentObject();
        $scope.amount = $scope.paymentToRefund.Balance;
    });
    $scope.success = false;
    $scope.submit = function () {
        $scope.submitDisabled = true;
        $scope.refundButtonText = "Processing...";
        Payment.processRefund($scope.paymentToRefund.PaymentId, $scope.amount).then(function (response) {
            Payment.getUpdatedPaymentObject($scope.paymentToRefund.PaymentId).then(function (payment) {
                $scope.success = true;
                $scope.refundButtonText = "Processed";
                setTimeout(function () {
                    $scope.success = false;
                    $('#refundTransactionModal').modal('hide');
                    $scope.submitDisabled = false;
                    $scope.refundButtonText = "Issue Refund";
                }, 1000);
                angular.extend($scope.paymentToRefund, payment);
            });
        });
    };
    //validation
}
angular.bootstrap($('#renterManager'), ["rpoResidentDetail"]);