﻿angular.module('transactionServices', [])
    .factory('Payment', function ($rootScope, $http, $q) {
        var paymentEntity = null;

        return {
            voidPayment:
                function (payment) {
                    paymentEntity = payment;
                    $rootScope.$broadcast('payment:void');
                },
            refundPayment:
                function (payment) {
                    paymentEntity = payment;

                    $rootScope.$broadcast('payment:refund');
                },
            getPaymentObject:
                function () {
                    return paymentEntity;
                },
            getUpdatedPaymentObject:
                function (paymentId) {
                    var deferred = $q.defer();
                    $http.get('/api/achpayment/get/' + paymentId).success(function (data) {
                        paymentEntity = data;
                        deferred.resolve(data);
                        $rootScope.$broadcast('payment:updated');
                    });
                    return deferred.promise;
                },
            processRefund:
                function (paymentId, refundAmount) {
                    var deferred = $q.defer();
                    var params = { PaymentId: paymentId, Amount: refundAmount };
                    if (paymentEntity.CreditCardHolderName != null) {
                        $http.post('/api/creditcardpayment/refund', params).success(function (data) {
                            deferred.resolve(data);
                        });
                    } else {
                        $http.post('/api/achpayment/refund', params).success(function (data) {
                            deferred.resolve(data);
                        });
                    }
                    return deferred.promise;
                },
            processVoid:
                function () {
                    var deferred = $q.defer();

                    $http.post('/api/achpayment/void', paymentEntity).success(function (data) {
                        deferred.resolve(data);
                    });

                    return deferred.promise;
                }
        };
    });
