﻿// Client side validation helpers
$.fn.maskDateInput = function () { $(this).mask("99/99/9999"); };
$.fn.maskNumberInput = function () { $(this).numeric({ negative: false }); };
$.fn.maskRoutingNumber = function () { $(this).numeric({ negative: false }); };
$.fn.maskCVV = function () { $(this).mask("999?9"); };
$.fn.maskPhoneInput = function () { $(this).mask("999-999-9999"); };
$.fn.maskTimeInput = function () { $(this).mask("99:99"); };
$.fn.maskSSNInput = function () { $(this).mask("999-99-9999"); };
$.fn.maskPercentInput = function () { $(this).numeric({ negative: false }); };
$.fn.maskMoneyInput = function () { $(this).numeric({ negative: false }); };
$.fn.formatMoney = function () {
    var m = (this).val().replace('$', '').split('.');
    var d = '00';
    if (m.length > 1) {
        d = m[1].length == 1 ? m[1] + '0' : m[1].substring(0, 2);
    }
    $(this).val('$' + m[0] + '.' + d);
};
$.fn.formatPercent = function () {
    var m = (this).val().replace('%', '').split('.');
    var d = '00';
    if (m.length > 1) {
        d = m[1].length == 1 ? m[1] + '0' : m[1].substring(0, 2);
    }
    $(this).val(m[0] + '.' + d + '%');
};
$.fn.center = function (centerTop) {
    this.css("position", "fixed");
    if (centerTop == true) {
        this.css("top", Math.max(0, (($(window).height() - this.outerHeight()) / 2) +
            $(window).scrollTop()) + "px");
    }
    else {
        this.css("top", "0");
    }
    this.css("left", Math.max(0, (($(window).width() - this.outerWidth()) / 2) +
                                                $(window).scrollLeft()) + "px");
    return this;
}

// EFX Client contains client side functionality 
var EFXClient = {
    Init: function () {
        $('.Welcome').mouseover(function () {
            $(this).addClass('WelcomeHover');
        });
        $('.Welcome').mouseout(function () {
            $(this).removeClass('WelcomeHover');
        });
        $('.Welcome').toggle(function () {
            var w = $('.Welcome').width();
            $('#AccountSubNavigation').width(w).slideDown();
        },
        function () {
            $('#AccountSubNavigation').slideUp();
        });

        $('.datepicker').each(function () {
            $(this).datepicker();
        });
    },
    UpdateFee: function () {
        var averageRent = $('#AvgMonthlyPaymentTextbox').val();
        var program = $('#ProgramsDropdownList').val();
        if (typeof(program) == 'undefined')
            program = 0;
        $.ajax({
            url: "/Handlers/EFXAdminHandler.ashx?RequestType=6&averageRent=" + averageRent + "&program=" + program,
            success: function (s) {
                $('#AvgConvenienceFeeTextbox').val(s);
            }
        });
    },
    ShowEditorHTMLPreview: function () {
        var html = CKEDITOR.instances.CKEditor1.getData();
        
        var mywin = window.open("", "ckeditor_preview", "location=0,status=0,scrollbars=1,width=615");

        $(mywin.document.body).html(html);
        return false;
    },
    WireUpValidation: function () {
        // Wire up client side validators
        $('.money').each(function () {
            $(this).maskMoneyInput();
            $(this).blur(function () {
                $(this).formatMoney();
            });
        });
        $('.phone').each(function () {
            $(this).maskPhoneInput();
        });
        $('.number').each(function () {
            $(this).maskNumberInput();
        });
        $('.cvv').each(function () {
            $(this).maskCVV();
        });
        $('.percent').each(function () {
            $(this).maskPercentInput();
            $(this).blur(function () {
                $(this).formatPercent();
            });
        });
        $('.routing').each(function () {
            $(this).maskRoutingNumber();
        });
    },
    WireUpDatePickers: function () {
        $('.datepicker').each(function () {
            $(this).datepicker();
        });
    },
    SetSelectedNavigation: function (id) {
        $('#Navigation > li').each(function () {
            if ($(this).hasClass('selected')) $(this).removeClass('selected');
            if ($(this).attr('id') == id) $(this).addClass('selected');
        });
    },
    ShowStepOne: function () {
        $('#StepOne').show();
        $('#StepTwo').hide();
        $('#StepThree').hide();
        $('#StepFour').hide();

        $('#StepOneTitle').show();
        $('#StepTwoTitle').hide();
        $('#StepThreeTitle').hide();
        $('#StepFourTitle').hide();
    },
    ShowStepTwo: function () {
        $('#StepOne').hide();
        $('#StepTwo').show();
        $('#StepThree').hide();
        $('#StepFour').hide();

        $('#StepOneTitle').hide();
        $('#StepTwoTitle').show();
        $('#StepThreeTitle').hide();
        $('#StepFourTitle').hide();
    },
    ShowStepThree: function () {
        $('#StepOne').hide();
        $('#StepTwo').hide();
        $('#StepThree').show();
        $('#StepFour').hide();

        $('#StepOneTitle').hide();
        $('#StepTwoTitle').hide();
        $('#StepThreeTitle').show();
        $('#StepFourTitle').hide();
    },
    ShowStepFour: function () {
        $('#StepOne').hide();
        $('#StepTwo').hide();
        $('#StepThree').hide();
        $('#StepFour').show();

        $('#StepOneTitle').hide();
        $('#StepTwoTitle').hide();
        $('#StepThreeTitle').hide();
        $('#StepFourTitle').show();
    },
    HideMainSystemMessage: function () {
        $('#MainSystemMessage').slideUp();
    },
    ShowAddStaff: function () {
        $('#AddStaffWrap').slideDown();
        $('#addStaffTopBar').slideUp();

        return false;
    },
    HideAddStaff: function () {
        $('#AddStaffWrap').slideUp();
        $('#addStaffTopBar').slideDown();

        return false;
    },
    ToggleStaffCards: function (sender) {
        var staffCardWrapSelector = '.staff-card-wrap';
        if ($(sender).hasClass('collapsed')) {
            $(sender).html('See Less');
            $(sender).removeClass('collapsed');
            $(sender).addClass('expanded');

            $(staffCardWrapSelector).css({ height: '100%' });
        }
        else {
            $(sender).html('See More');
            $(sender).removeClass('expanded');
            $(sender).addClass('collapsed');

            $(staffCardWrapSelector).css({ height: '410px' });
        }
    },
    CheckAcceptTerms: function (selector) {
        if ($(selector).attr('checked'))
            return true;
        alert('Please accept the terms of agreement');
        return false;
    }
}
$(EFXClient.Init);