﻿angular.module('renters', ['ngRoute']).config(function ($routeProvider, $locationProvider) {
    var html5Ok = navigator.appVersion.indexOf('MSIE 10.0') == -1;
    $locationProvider.html5Mode(html5Ok).hashPrefix('!');
    $routeProvider.
      when('/', { controller: RentersController, templateUrl: '../../Templates/renters.html' }).
      when('/p/:page', { controller: RentersController, templateUrl: '../../Templates/renters.html' }).
      when('/property/:propertyId', { controller: RentersController, templateUrl: '../../Templates/renters.html' }).
      when('/property/:propertyId/p/:page', { controller: RentersController, templateUrl: '../../Templates/renters.html' }).
      when('/renter/:renterId/', { controller: RenterController, templateUrl: '../../Templates/properties.html' }).
      otherwise({ redirectTo: '/' });

});

