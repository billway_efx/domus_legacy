﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RentPaidOnline
{
    public partial class PaymentFailed : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ProcessQueryStrings();
        }

        //Salcedo - 1/18/2014 - added support for more specific error information
        private void ProcessQueryStrings()
        {
            //CMallory - Task 00648 - Commented out Charity logic
            //string CharityMsg = Request.QueryString["CharityFailureMessage"];
            string RentMsg = Request.QueryString["RentAndFeeFailureMessage"];
            //if (CharityMsg != null)
            //{
            //    if (CharityMsg.Length > 0)
            //    {
            //        lblCharityFailureMessage.Text = "Charity Transaction: " + CharityMsg;
            //        lblCharityFailureMessage.Visible = true;
            //    }

            //}
            if (RentMsg != null)
            {
                if (RentMsg.Length > 0)
                {
                    lblRentAndFeeFailureMessage.Text = "Rent & Fee Transaction: " + RentMsg;
                    lblRentAndFeeFailureMessage.Visible = true;
                }
            }
        }
    }
}