﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Efx.CustomerServicePortal;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Net.Mail;
using EfxFramework.Security.Authentication;
using EfxFramework.EmailTemplateService;
using EfxFramework;
using EfxFramework.Web.Facade;
using RPO;

namespace Efx.CustomerServicePortal
{
     
    public partial class CustomerServicePortal : System.Web.UI.Page
    {
        // On Page load, these labels should be invisible by default. If they are not, this code 
        //will force them to be that way. 
        //int RenterIdReturned = 0;
        EfxFramework.Renter residentInfoThatWillPopulatePage = new EfxFramework.Renter();
        string primaryEmail = string.Empty;
        protected void rblSearchCriteria_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void paymentMOdalClose_OnClick(object sender, EventArgs e)
        {
            mpePaymentHistory.Hide();

        }
        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ResidentSection.Visible = false;
                NoResidentsSelected.Visible = true;

                btnPaymentHistory.Enabled = false;
                btnUpdatePassword.Enabled = false;
                btnUpdateUserName.Enabled = false;
                btnEmailPW.Enabled = false;

                lblUsernameDidUpdate.Visible = false;
                lblPasswordUpdated.Visible = false;
                lblPasswordResetEmailSent.Visible = false;
                tbUpdateUserName.Text = "";
                tbUpdatePassword.Text = "";
            }

            if (!EfxFramework.Helpers.Helper.HasCsrAccess)
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

        protected void btnClearRenterInfo_Click(object sender, EventArgs e)
        {
            //refresh customer service portal, should clear renter info
            Response.Redirect(Request.RawUrl);
        }

        //CMallory - Task 00593 - Modified GetGridSql method by adding check for IsDeleted value to the Where clause of the queries.
        private String GetGridSql()
        {
            String ReturnSql = "";
 
            if (cbExactMatch.Checked)
            {
                if (rblSearchCriteria.SelectedItem.Value == "First Name")
                {
                    ReturnSql = "SELECT RENTERID, FirstName,LastName, c.CompanyName as Company, p.PropertyName, r.Unit, r.StreetAddress, r.PrimaryEmailAddress, r.MainPhoneNumber, r.IsActive "
                                            + " FROM Renter R"
                                            + " join Property p"
                                            + " on p.PropertyId = r.RenterPropertyId"
                                            + " join Company c"
                                            + " on c.CompanyId = p.CompanyId Where R.FirstName = '" + tbSearchResident.Text + "' and p.IsDeleted=0";
                }
                if (rblSearchCriteria.SelectedItem.Value == "Phone")
                {
                    var charsToRemove = new string[] { " ", "-", "(", ")" };
                    foreach (var c in charsToRemove)
                    {
                        tbSearchResident.Text = tbSearchResident.Text.Replace(c, string.Empty);
                    }
                    // Patrick Whittingham - 8/20/15 - task #457
                    ReturnSql = "SELECT RENTERID, FirstName,LastName, c.CompanyName as Company, p.PropertyName, r.Unit, r.StreetAddress, r.PrimaryEmailAddress, r.MainPhoneNumber, r.IsActive "
                                            + " FROM Renter R"
                                            + " join Property p"
                                            + " on p.PropertyId = r.RenterPropertyId"
                                            + " join Company c"
                                            + " on c.CompanyId = p.CompanyId Where P.MainPhoneNumber = '" + tbSearchResident.Text + "' and p.IsDeleted=0";
                }
                if (rblSearchCriteria.SelectedItem.Value == "Property")
                {
                    ReturnSql = "SELECT RENTERID, FirstName,LastName, c.CompanyName as Company, p.PropertyName, r.Unit, r.StreetAddress, r.PrimaryEmailAddress, r.MainPhoneNumber, r.IsActive "
                                            + " FROM Renter R"
                                            + " join Property p"
                                            + " on p.PropertyId = r.RenterPropertyId"
                                            + " join Company c"
                                            + " on c.CompanyId = p.CompanyId Where P.PropertyName = '" + tbSearchResident.Text + "' and p.IsDeleted=0";
                }
                if (rblSearchCriteria.SelectedItem.Value == "Email")
                {
                    ReturnSql = "SELECT RENTERID, FirstName,LastName, c.CompanyName as Company, p.PropertyName, r.Unit, r.StreetAddress, r.PrimaryEmailAddress, r.MainPhoneNumber, r.IsActive "
                                            + " FROM Renter R"
                                            + " join Property p"
                                            + " on p.PropertyId = r.RenterPropertyId"
                                            + " join Company c"
                                            + " on c.CompanyId = p.CompanyId Where R.PrimaryEmailAddress = '" + tbSearchResident.Text + "' and p.IsDeleted=0";
                }
                if (rblSearchCriteria.SelectedItem.Value == "Last Name")
                {
                    ReturnSql = "SELECT RENTERID, FirstName,LastName, c.CompanyName as Company, p.PropertyName, r.Unit, r.StreetAddress, r.PrimaryEmailAddress, r.MainPhoneNumber, r.IsActive "
                                            + " FROM Renter R"
                                            + " join Property p"
                                            + " on p.PropertyId = r.RenterPropertyId"
                                            + " join Company c"
                                            + " on c.CompanyId = p.CompanyId Where R.LastName = '" + tbSearchResident.Text + "' and p.IsDeleted=0";
                }
                if (rblSearchCriteria.SelectedItem.Value == "Company")
                {
                    ReturnSql = "SELECT RENTERID, FirstName,LastName, c.CompanyName as Company, p.PropertyName, r.Unit, r.StreetAddress, r.PrimaryEmailAddress, r.MainPhoneNumber, r.IsActive "
                                            + " FROM Renter R"
                                            + " join Property p"
                                            + " on p.PropertyId = r.RenterPropertyId"
                                            + " join Company c"
                                            + " on c.CompanyId = p.CompanyId Where c.CompanyName = '" + tbSearchResident.Text + "' and p.IsDeleted=0";
                }
            }
            else
            {
                if (rblSearchCriteria.SelectedItem.Value == "First Name")
                {
                    ReturnSql = "SELECT RENTERID, FirstName,LastName, c.CompanyName as Company, p.PropertyName, r.Unit, r.StreetAddress, r.PrimaryEmailAddress, r.MainPhoneNumber, r.IsActive "
                                            + " FROM Renter R"
                                            + " join Property p"
                                            + " on p.PropertyId = r.RenterPropertyId"
                                            + " join Company c"
                                            + " on c.CompanyId = p.CompanyId Where R.FirstName LIKE '%" + tbSearchResident.Text + "%' and p.IsDeleted=0";
                }
                if (rblSearchCriteria.SelectedItem.Value == "Phone")
                {
                    ReturnSql = "SELECT RENTERID, FirstName,LastName, c.CompanyName as Company, p.PropertyName, r.Unit, r.StreetAddress, r.PrimaryEmailAddress, r.MainPhoneNumbe, r.IsActiver "
                                            + " FROM Renter R"
                                            + " join Property p"
                                            + " on p.PropertyId = r.RenterPropertyId"
                                            + " join Company c"
                                            + " on c.CompanyId = p.CompanyId Where R.MainPHoneNumber LIKE '%" + tbSearchResident.Text + "%' and p.IsDeleted=0";
                }
                if (rblSearchCriteria.SelectedItem.Value == "Property")
                {
                    ReturnSql = "SELECT RENTERID, FirstName,LastName, c.CompanyName as Company, p.PropertyName, r.Unit, r.StreetAddress, r.PrimaryEmailAddress, r.MainPhoneNumber, r.IsActive "
                                            + " FROM Renter R"
                                            + " join Property p"
                                            + " on p.PropertyId = r.RenterPropertyId"
                                            + " join Company c"
                                            + " on c.CompanyId = p.CompanyId Where p.PropertyName LIKE '%" + tbSearchResident.Text + "%' and p.IsDeleted=0";
                }

                if (rblSearchCriteria.SelectedItem.Value == "Email")
                {
                    ReturnSql = "SELECT RENTERID, FirstName,LastName, c.CompanyName as Company, p.PropertyName, r.Unit, r.StreetAddress, r.PrimaryEmailAddress, r.MainPhoneNumbe, r.IsActiver "
                                            + " FROM Renter R"
                                            + " join Property p"
                                            + " on p.PropertyId = r.RenterPropertyId"
                                            + " join Company c"
                                            + " on c.CompanyId = p.CompanyId Where R.PrimaryEmailAddress LIKE '%" + tbSearchResident.Text + "%' and p.IsDeleted=0";
                }
                if (rblSearchCriteria.SelectedItem.Value == "Last Name")
                {
                    ReturnSql = "SELECT RENTERID, FirstName,LastName, c.CompanyName as Company, p.PropertyName, r.Unit, r.StreetAddress, r.PrimaryEmailAddress, r.MainPhoneNumber, r.IsActive "
                                            + " FROM Renter R"
                                            + " join Property p"
                                            + " on p.PropertyId = r.RenterPropertyId"
                                            + " join Company c"
                                            + " on c.CompanyId = p.CompanyId Where R.LastName LIKE '%" + tbSearchResident.Text + "%' and p.IsDeleted=0";
                }


                if (rblSearchCriteria.SelectedItem.Value == "Company")
                {
                    ReturnSql = "SELECT RENTERID, FirstName,LastName, c.CompanyName as Company, p.PropertyName, r.Unit, r.StreetAddress, r.PrimaryEmailAddress, r.MainPhoneNumber, r.IsActive "
                                            + " FROM Renter R"
                                            + " join Property p"
                                            + " on p.PropertyId = r.RenterPropertyId"
                                            + " join Company c"
                                            + " on c.CompanyId = p.CompanyId Where c.CompanyName LIKE '%" + tbSearchResident.Text + "%' and p.IsDeleted=0";
                }

            }
            return ReturnSql;
        }

        protected void btnSearchResident_Click(object sender, EventArgs e)
        {
            int LocalRenterId = 0;

            lblSearchError.Text = "";
            lblSearchError.Visible = false;

            if (tbSearchResident.Text == "" || tbSearchResident.Text == null)
            {
                lblSearchError.Text = "Please enter a search value.";
                lblSearchError.Visible = true;
            }
            else
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConnString;
                SqlCommand command = new SqlCommand();
                SqlDataReader dr;
                DataTable dt = new DataTable();
                command.CommandText = GetGridSql();

                try
                {
                    conn.Open();
                    command.Connection = conn;
                    dr = command.ExecuteReader();
                    dt.Load(dr);
                }
                //catch (Exception exex)
                catch
                {
                    //pop up saying it doesnt work
                }
                conn.Close();
                if (dt.Rows.Count == 0)
                {
                    lblSearchError.Text = "No records found.";
                    lblSearchError.Visible = true;

                    //Salcedo - task # 00321
                    btnPaymentHistory.Enabled = false;
                    btnUpdatePassword.Enabled = false;
                    btnUpdateUserName.Enabled = false;
                    btnEmailPW.Enabled = false;

                    ResidentSection.Visible = false;
                    NoResidentsSelected.Visible = true;

                    lblUsernameDidUpdate.Visible = false;
                    lblPasswordUpdated.Visible = false;
                    lblPasswordResetEmailSent.Visible = false;
                    tbUpdateUserName.Text = "";
                    tbUpdatePassword.Text = "";

                }

                if (dt.Rows.Count > 1)
                {
                    //gvRenters.DataSource = new SqlDataSource(ConnString, command.CommandText);
                    //gvRenters.DataBind();

                    dv = bindgrid();
                    gvRenters.DataSource = dv;
                    gvRenters.DataBind();

                    mpe.Show();
                }
                else
                {
                    LocalRenterId = (from DataRow row in dt.Rows select (int)row["RENTERID"]).FirstOrDefault();
                    if (dt.Rows.Count == 1)
                    {
                        PopulateRenterControls(LocalRenterId);
                        string propertyName = (from DataRow row in dt.Rows select (string)row["PropertyName"]).FirstOrDefault();
                        lblhiddenproperty.Text = propertyName;
                    }
                }
            }
        }

        protected void ibtnSelectUser_Click(object sender, EventArgs e)
        {
            GridViewRow gvRow = (GridViewRow)((Button)sender).NamingContainer;
            int RenterId = Convert.ToInt32(gvRenters.DataKeys[gvRow.RowIndex].Value);
            mpe.Hide();

            PopulateRenterControls(RenterId);
        }



        public void PopulateRenterControls(int RenterId)
        {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConnString;
                SqlCommand command = new SqlCommand();
                SqlDataReader dr;
                //SqlBulkCopy drProp;
            DataTable dt = new DataTable();
            command.CommandText = "SELECT PROPERTY.PROPERTYNAME"
                                     + " FROM PROPERTY"
                                     + " JOIN Renter"
                                     + " on property.PropertyId = Renter.RenterPropertyId"
                                    + " where renter.RenterId = " + RenterId.ToString();
                try
                {
                    conn.Open();
                    command.Connection = conn;
                    dr = command.ExecuteReader();
                    dt.Load(dr);
                    string propertyName = (from DataRow row in dt.Rows
                                           select (string)row["PropertyName"]).FirstOrDefault();
                    lblhiddenproperty.Text = propertyName;
                    conn.Close();
                }
                //catch (Exception exex)
            catch
                {
                    //pop up saying it doesnt work
                }
                residentInfoThatWillPopulatePage = new EfxFramework.Renter(RenterId);

                primaryEmail = residentInfoThatWillPopulatePage.PrimaryEmailAddress;
                EfxFramework.Lease myLease = EfxFramework.Lease.GetRenterLeaseByRenterId(residentInfoThatWillPopulatePage.RenterId);
                lblPropertyNameText.Visible = true;
                lblRName.Visible = true;
                lblRAddress.Visible = true;
                lblRUnit.Visible = true;
                lblRUserName.Visible = true;
                lblREmail.Visible = true;
                lblRRPOID.Visible = true;
                lblRTotalAmountDue.Visible = true;
                lblRStatus.Visible = true;
                lblRLeaseEndDate.Visible = true;
                lblRLeaseStartDate.Visible = true;
                lblRPayByText.Visible = true;
                lblRName.Visible = true;
                lblRAutoPay.Visible = true;
                lblRPhoneNumber.Visible = true;
                lblStatus.Visible = true;

                lblPropertyNameText.Text = lblhiddenproperty.Text;
                lblRName.Text = residentInfoThatWillPopulatePage.FirstName + " " + residentInfoThatWillPopulatePage.LastName;
                lblRealRPOIDHidden.Text = Convert.ToString(residentInfoThatWillPopulatePage.RenterId);
                switch (residentInfoThatWillPopulatePage.StateProvinceId)
                {
                    case 1:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "AK" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 2:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "AL" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 3:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "AR" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 4:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "AZ" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 5:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "CA" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 6:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "CO" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 7:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "CT" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 8:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "DE" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 9:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "FL" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 10:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "GA" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 11:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "HI" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 12:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "IA" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 13:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "ID" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 14:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "IL" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 15:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "IN" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 16:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "KS" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 17:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "KY" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 18:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "LA" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 19:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "MA" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 20:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "MD" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 21:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "ME" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 22:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "MI" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 23:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "MN" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 24:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "MO" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 25:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "MS" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 26:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "MT" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 27:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "NC" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 28:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "ND" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 29:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "NE" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 30:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "NH" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 31:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "NJ" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 32:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "NM" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 33:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "NV" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 34:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "NY" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 35:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "OH" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 36:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "OK" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 37:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "OR" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 38:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "PA" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 39:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "RI" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 40:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "SC" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 41:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "SD" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 42:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "TN" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 43:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "TX" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 44:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "UT" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 45:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "VA" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 46:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "VT" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 47:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "WA" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 48:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "WI" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 49:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "WV" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 50:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "WY" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;
                    case 51:
                        lblRAddress.Text = residentInfoThatWillPopulatePage.StreetAddress + ", " + residentInfoThatWillPopulatePage.City + ", " + "DC" + " " + residentInfoThatWillPopulatePage.PostalCode;
                        break;

                }
            
                lblRPhoneNumber.Text = residentInfoThatWillPopulatePage.MainPhoneNumber;
                lblRUnit.Text = residentInfoThatWillPopulatePage.Unit;
                lblRUserName.Text = Convert.ToString(residentInfoThatWillPopulatePage.PrimaryEmailAddress);
                lblREmail.Text = Convert.ToString(residentInfoThatWillPopulatePage.PrimaryEmailAddress);
                lblRRPOID.Text = Convert.ToString(residentInfoThatWillPopulatePage.PublicRenterId);
                double tam = Convert.ToDouble(myLease.CurrentBalanceDue + myLease.TotalFees);
                lblRTotalAmountDue.Text = "$" + Convert.ToString(tam) + ".00";
                // Patrick Whittingham - 8/20/15 - task #457
                if (residentInfoThatWillPopulatePage.IsActive == true) {
                    lblStatus.Text = "Active";
                } else {
                    lblStatus.Text = "Deactivated";
                }

                switch (residentInfoThatWillPopulatePage.AcceptedPaymentTypeId)
                {
                    case 1:
                        lblRStatus.Text = "Any";
                        break;
                    case 2:
                        lblRStatus.Text = "Check";
                        break;
                    case 3:
                        lblRStatus.Text = "Cash Equivalent";
                        break;
                    case 4:
                        lblRStatus.Text = "Do Not Accept Payment";
                        break;
                }

                
                
                lblRLeaseStartDate.Text = Convert.ToString(myLease.StartDate.ToShortDateString());
                lblRLeaseEndDate.Text = Convert.ToString(myLease.EndDate.ToShortDateString());

                SqlCommand getAutoPayInfo = new SqlCommand();
                getAutoPayInfo.CommandText = "Select PaymentDayOfMonth from AutoPayment where RenterId = " + residentInfoThatWillPopulatePage.RenterId;
                getAutoPayInfo.Connection = conn;
                conn.Open();
               

                dr = getAutoPayInfo.ExecuteReader();
                //Salcedo - 11/3/2015 - emergency production fix
                lblRAutoPay.Text = "InActive";
                while (dr.Read())
                {
                    string AutoPayDay = dr["PaymentDayOfMonth"].ToString();
                     if (AutoPayDay != null)
                     {
                    lblRAutoPay.Text = "Payment is taken out on the " + AutoPayDay + " day of the month.";
                     }
                    //
                    // string IsActive = dr["IsActive"].ToString();


                    // if (IsActive != null)
                    //{
                    //    lblRAutoPay.Text = IsActive;
                    //}
                     lblRAutoPay.Text = "Active";
                }
                

                conn.Close();
                if (residentInfoThatWillPopulatePage.SmsReminderDayOfMonth != null)
                {
                    lblRPayByText.Text = "SMS reminder is sent on the " + Convert.ToString(residentInfoThatWillPopulatePage.SmsReminderDayOfMonth) + " of the month";
                }
                btnUpdatePassword.Visible = true;
                btnPaymentHistory.Visible = true;
                btnEmailPW.Visible = true;
                btnUpdateUserName.Visible = true;

                ResidentSection.Visible = true;
                NoResidentsSelected.Visible = false;
                lblSearchError.Visible = false;
                lblSendTicketConfirmation.Visible = false;

                //Salcedo - task # 00321
                btnPaymentHistory.Enabled = true;
                btnUpdatePassword.Enabled = true;
                btnUpdateUserName.Enabled = true;
                btnEmailPW.Enabled = true;

                lblUsernameDidUpdate.Visible = false;
                lblPasswordUpdated.Visible = false;
                lblPasswordResetEmailSent.Visible = false;
                tbUpdateUserName.Text = "";
                tbUpdatePassword.Text = "";

        }

        public List<EfxFramework.PaymentHistory> PaymentList { set { gvPaymentHistory.DataSource = value; gvPaymentHistory.DataBind(); } }

        public void GetResidentPaymentHistory(int renterID)
        {
            PaymentList = EfxFramework.Payment.GetPaymentListByRenterId(renterID).OrderByDescending(p => p.TransactionDateTime)
                                 .Where(p => p.OtherDescription1 != "Waived Payments").Select(p => new PaymentHistory(p)).ToList(); 
           
        }

        public void UpdateTheUserNameForResident()
        {
            if (tbUpdateUserName.Text != null)
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConnString;

                SqlCommand command = new SqlCommand();
                command.CommandText = "UPDATE Renter SET PrimaryEmailAddress = '" + tbUpdateUserName.Text + "' Where PrimaryEmailAddress = '" +lblREmail.Text+"'";
                try
                {
                    conn.Open();
                    command.Connection = conn;
                    command.ExecuteNonQuery();
                    string editor = RPO.Helpers.Helper.GetCurrentUserName(true, RPO.Helpers.Helper.IsCsrUser);
                    string updatedUSerNameNote = "Username updated on " + Convert.ToString(System.DateTime.Now);

                    EfxFramework.Renter.AddNoteForRenter(-100, Convert.ToInt32(lblRealRPOIDHidden.Text), updatedUSerNameNote, editor, 2);
                    lblUsernameDidUpdate.Visible = true;
                }
                //catch (Exception ex)
                catch
                {
                    //pop up saying it doesnt work and suggest clearing the renter object
                    //or just run the clear renter object.
                    lblUsernameDidUpdate.Text = "User name did not update.";
                    lblUsernameDidUpdate.Visible = true;
                    //cakel: BUGID00280
                    conn.Close();
                }

               

                conn.Close();
            }
        }
        protected void btnUpdateUserName_Click(object sender, EventArgs e)
        {
            UpdateTheUserNameForResident();
        }
        private void SetNewPassword(int renterID, byte[] pw, byte[] salt)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Renter_SetNewPassword", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@id", SqlDbType.Int).Value = Convert.ToInt32(lblRealRPOIDHidden.Text);
            com.Parameters.Add("@pw", SqlDbType.Binary, 64).Value = pw;
            com.Parameters.Add("@salt", SqlDbType.Binary, 64).Value = salt;
            try
            {
                con.Open();
                com.ExecuteNonQuery();

                lblPasswordUpdated.Text = "Password Changed!";
                tbUpdatePassword.Text = string.Empty;
                lblPasswordUpdated.Visible = true;
            }
            catch
            {
                //con.Close();
                lblPasswordUpdated.Text = "Password Not Updated";
            }
            finally
            {
                con.Close();
            }
        }
        public void UpdateThePasswordForResident()
        {
             if (tbUpdatePassword != null)
            {
                byte[] PasswordHash;
                byte[] Salt;

                
              
                try
                {
                    string renterIdPasswordUpdate = Convert.ToString(lblRealRPOIDHidden.Text);
                    if (renterIdPasswordUpdate != null)
                    {
                        if (tbUpdatePassword.Text == null)
                        {
                            lblPasswordUpdated.Text = "Password Not Updated";
                            lblPasswordUpdated.Visible = true;
                        }
                        string NewPassword = tbUpdatePassword.Text;

                        //Generate a new salt and set it
                        Salt = AuthenticationTools.GenerateSalt();

                        //Hash the new password
                        PasswordHash = AuthenticationTools.HashPassword(NewPassword, Salt);

                        SetNewPassword(Convert.ToInt32(lblRealRPOIDHidden.Text), PasswordHash, Salt);

                    }
                    
                    lblPasswordUpdated.Visible = true;
                    string editor = RPO.Helpers.Helper.GetCurrentUserName(true, RPO.Helpers.Helper.IsCsrUser);
                    string updatedUSerNameNote = "Password updated on " + Convert.ToString(System.DateTime.Now);
                    EfxFramework.Renter.AddNoteForRenter(-100, Convert.ToInt32(lblRealRPOIDHidden.Text), updatedUSerNameNote, editor, 2);
                }
                //catch (Exception ex)
                catch
                {
                    //pop up saying it doesnt work and suggest clearing the renter object
                    //or just run the clear renter object.
                }

                
               
            }
        }

        protected void SendEmail(EfxFramework.Renter renter, string password)
        {
            if (String.IsNullOrEmpty(renter.PrimaryEmailAddress))
                return;

            //Salcedo - 3/12/2014 - replaced with new html email template
            var Message = string.Format(EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-password-reset.html").Replace("[[ROOT]]",
                EmailTemplateService.RootPath), renter.DisplayName, password);

            EfxFramework.BulkMail.BulkMail.SendMailMessage(
                null,
                "EFX",
                false,
                EfxFramework.EfxSettings.SendMailUsername,
                EfxFramework.EfxSettings.SendMailPassword,
                new MailAddress( EfxFramework.EfxSettings.SupportEmail),
                "Forgot Password",
                Message,
                new List<MailAddress> { new MailAddress(renter.PrimaryEmailAddress) });
        }

        public void SendTicketEmail()
        {
            var NewRenterInfo = new EfxFramework.Renter(Convert.ToInt32(lblRealRPOIDHidden.Text));
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConnString;
            SqlCommand command = new SqlCommand();
            SqlDataReader dr;
            DataTable dt = new DataTable();
            // Patrick Whittingham - 8/20/15 - task #457
            //Salcedo - task #457 - fixed syntax error
            command.CommandText = "SELECT PropertyRenter.PropertyId, Property.PropertyName,"
                                    + "Property.MainPhoneNumber "
                                    + " FROM PropertyRenter"
                                    + " join"
                                    + " Property"
                                    + " on Property.PropertyId = PropertyRenter.PropertyId"
                                     + " where PropertyRenter.RenterId = " + Convert.ToString(NewRenterInfo.RenterId);

            try
            {
                conn.Open();
                command.Connection = conn;
                dr = command.ExecuteReader();
                dt.Load(dr);
                //while (dr.Read())
                //{
                //    string RenterIDstring = dr["RenterId"].ToString();
                //    LocalRenterId = Convert.ToInt32(RenterIDstring);
                //}


                string PropertyName = (from DataRow row in dt.Rows
                                       select (string)row["PropertyName"]).FirstOrDefault();
                string PropertyPhone = (from DataRow row in dt.Rows
                                        select (string)row["MainPhoneNumber"]).FirstOrDefault();
                string ResidentPhone = Convert.ToString(NewRenterInfo.MainPhoneNumber);
                string ResidentEmail = Convert.ToString(NewRenterInfo.PrimaryEmailAddress);

                String Message = "<html>Property: " + PropertyName + "<br />"
                                   + "Property Phone: " + PropertyPhone + "<br />"
                                   + "Resident Phone: " + ResidentPhone + "<br />"
                                   + "Resident Email: " + ResidentEmail + "<br />"
                                   + "Issue: " + tbSendTicketEmail.Text + "</html>";

                EfxFramework.BulkMail.BulkMail.SendMailMessage(
                null,
                "EFX",
                false,
                EfxFramework.EfxSettings.SendMailUsername,
                EfxFramework.EfxSettings.SendMailPassword,
                new MailAddress(EfxFramework.EfxSettings.SupportEmail),
                "Support Ticket From " + NewRenterInfo.FirstName + " " + NewRenterInfo.LastName,
                Message,
                new List<MailAddress> { new MailAddress(EfxFramework.EfxSettings.SupportEmail) });

                lblSendTicketConfirmation.Text = "Ticket email has been sent successfully.";
                lblSendTicketConfirmation.Visible = true;

            }
            //catch (Exception exex)
            catch
            {
                //pop up saying it doesnt work
                lblSendTicketConfirmation.Text = "Ticket email was not sent. Please try again later.";
                lblSendTicketConfirmation.Visible = true;
            }

             //cakel: BUGID00280
            finally
            {
                conn.Close();
            }
            
        }
        public void TriggerPasswordResetEmail()
        {
            //EfxFramework.Renter newRenterForPasswordResetEmail = new EfxFramework.Renter(residentInfoThatWillPopulatePage.RenterId);
            //var Password = newRenterForPasswordResetEmail.ResetPassword();
            //newRenterForPasswordResetEmail.Set(residentInfoThatWillPopulatePage.RenterId,)


            try
            {
                var NewRenterInfo = new EfxFramework.Renter(Convert.ToInt32(lblRealRPOIDHidden.Text));
                var Password = NewRenterInfo.ResetPassword();
                EfxFramework.Renter.Set(NewRenterInfo, UIFacade.GetAuthenticatedUserString(UserType.EfxAdministrator));
                SendEmail(NewRenterInfo, Password);
                lblPasswordResetEmailSent.Text = "Reset Password Email Sent!";
                lblPasswordResetEmailSent.Visible = true;
                string editor = RPO.Helpers.Helper.GetCurrentUserName(true, RPO.Helpers.Helper.IsCsrUser);
                string updatedUSerNameNote = "Password reset email sent on " + Convert.ToString(System.DateTime.Now);
                EfxFramework.Renter.AddNoteForRenter(-100, Convert.ToInt32(lblRealRPOIDHidden.Text), updatedUSerNameNote, editor, 2);
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
                lblPasswordResetEmailSent.Text = "Password Email Not Sent, Please Try Again Later";
                lblPasswordResetEmailSent.Visible = true;
            }



        }

        protected void btnUpdatePassword_Click(object sender, EventArgs e)
        {
            UpdateThePasswordForResident();
        }

        //Salcedo - task # 00321 - not used
        //protected void btnTakePayment_Click(object sender, EventArgs e)
        //{
        //    int RenterId = Convert.ToInt32(lblRealRPOIDHidden.Text);
        //    irm1.Src = "PaymentV2.aspx?RenterId=" + RenterId.ToString();

        //    mpe2.Show();
        //}
       
        protected void OKButton2_Click(object sender, EventArgs e)
        {
            mpe2.Hide();
        }

        protected void btnPaymentHistory_Click(object sender, EventArgs e)
        {
            PaymentList = EfxFramework.Payment.GetPaymentListByRenterId(Convert.ToInt32(lblRealRPOIDHidden.Text)).OrderByDescending(p => p.TransactionDateTime)
                                 .Where(p => p.OtherDescription1 != "Waived Payments").Select(p => new PaymentHistory(p)).ToList(); 
            mpePaymentHistory.Show();
        }

        protected void btnKnowledgeBase_Click1(object sender, EventArgs e)
        {
            Response.Redirect("https://rentpaidonline.rhinosupport.com/helpdesk.htm#knowledgebase");
         
        }

        protected void btnEmailPW_Click(object sender, EventArgs e)
        {
            //TriggerPasswordResetEmail();
            mpePasswordEmailConfirmation.Show();
        }

        protected void btnSendEmailToHelpdesk_Click(object sender, EventArgs e)
        {

        }

        protected void btnSendTicketEmail_Click(object sender, EventArgs e)
        {
            if (tbSendTicketEmail.Text == "")
            {
                lblSendTicketConfirmation.Text = "Please enter support ticket information. Ticket has not been sent.";
                lblSendTicketConfirmation.Visible = true;
            }
            else
            {
                SendTicketEmail();
            }
                

        }

        protected void PasswordAreYouSureYes_Click(object sender, EventArgs e)
        {
            TriggerPasswordResetEmail();
            mpePasswordEmailConfirmation.Hide();

        }

        protected void PasswordAreYouSureNo_Click(object sender, EventArgs e)
        {
            mpePasswordEmailConfirmation.Hide();
        }

        protected void gvRenters_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvRenters.PageIndex = e.NewPageIndex;
            //gvRenters.DataSource = new SqlDataSource(ConnString, GetGridSql());
            //gvRenters.DataBind();

            dv = bindgrid();
            gvRenters.DataSource = dv;
            gvRenters.DataBind();

            mpe.Show();
        }

        protected void gvRenters_Sorting(object sender, GridViewSortEventArgs e)
        {
            //gvRenters.DataSource = new SqlDataSource(ConnString, GetGridSql());
            //gvRenters.DataBind();

            ViewState["sortExpr"] = e.SortExpression; 
            dv = bindgrid();
            gvRenters.DataSource = dv;
            gvRenters.DataBind();

            mpe.Show();
            
        }

        DataSet ds = new DataSet();
        DataView dv = new DataView();

        private DataView bindgrid()
        {
            String StrQuery = GetGridSql();
            SqlDataAdapter sadp = new SqlDataAdapter(StrQuery, ConnString);
            sadp.Fill(ds);

            if (ViewState["sortExpr"] != null)
            {
                dv = new DataView(ds.Tables[0]);
                dv.Sort = (string)ViewState["sortExpr"];
            }
            else
                dv = ds.Tables[0].DefaultView;
            return dv;

        }

        //Salcedo - task # 00321 - prepopulate the payment window with the selected renter
        protected void TakeAPayment_Click(object sender, EventArgs e)
        {
            int RenterId = 0;
            if (lblRealRPOIDHidden.Text != "")
            { 
                RenterId = Convert.ToInt32(lblRealRPOIDHidden.Text);
            }
            if (RenterId != 0)
            { 
                Page.ClientScript.RegisterStartupScript(this.GetType(), "openlink", "window.open('PaymentV2.aspx?RenterId=" + RenterId.ToString() + "');", true);
            }
            else
            {
                //CMallory - Task 00633 - Changed the code to use C# code instead of Javascript to open the PaymentV2.aspx page in order to keep the code in the Global.asax page.
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "openlink", "window.open('PaymentV2.aspx');", true);
                Response.Redirect("PaymentV2.aspx");
            }
        }
    }

  
    

}