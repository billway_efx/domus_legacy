﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSR.master" AutoEventWireup="true" CodeBehind="PaymentFailed.aspx.cs" Inherits="RentPaidOnline.PaymentFailed" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="main-container">
        <div class="section-body">

            <h2>Payment Status</h2>

            <div class="well">
                <div class="highlighted message red">Your Transaction has NOT been processed.</div>

                <br />
                <asp:Label ID="lblCharityFailureMessage" runat="server" Text="Label" Visible="false"></asp:Label><br />
                <asp:Label ID="lblRentAndFeeFailureMessage" runat="server" Text="Label" Visible ="false"></asp:Label>

                <h2>Please try again</h2>

                <!-- cakel: BUGID000194 - Commented out html below to prevent confusion.  This was a static message that did change for payments or declines
                <p>Rent and Fees Transaction Accepted</p>
                -->

            </div>
            <div class="formWrapper">
                <div class="formHalf">
                    <h3>RPO Support </h3>
                    <div class="lightPanel">
                        Support Line:
                            <span style="font-weight: bold">1-855-PMYRENT</span><br />

                        Email:
                            <span style="font-weight: bold">
                                <a href="mailto:support@rentpaidonline.com">support@rentpaidonline.com</a></span><br />

                    </div>
                </div>
            </div>

            <!-- <section class="paymentStatusInfoBlock">
            <img src="/Images/RentPaidOnlineLogo.png" class="smallLogo" />
        </section> -->

        </div>

        <div class="formWhole"></div>
         
        <div class="button-footer">
            <div class="button-action">
                <asp:Button ID="CancelButton" runat="server" Text="Process A New Payment" CssClass="btn btn-default" PostBackUrl="~/PaymentV2.aspx" />
            </div>
            <div class="main-button-action">
                <asp:LinkButton ID="PrintButton" runat="server" CssClass="btn btn-primary" OnClientClick="window.print(); return false;">Print</asp:LinkButton>
            </div>
            <div class="clear"></div>
        </div>

    </div>

</asp:Content>
