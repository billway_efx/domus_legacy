﻿using EfxFramework.Interfaces.Account;
using EfxFramework.Web;
using System;

namespace RentPaidOnline.Account
{
    public partial class LogOut : BasePage, ILogout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var Presenter = new EfxFramework.Presenters.Account.Logout(this);
        }
    }
}