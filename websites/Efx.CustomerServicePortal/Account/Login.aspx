﻿<%@ Page Title="Log in" Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="RentPaidOnline.Account.Login" MasterPageFile="~/CSR.master" %>

<asp:Content runat="server" ContentPlaceHolderID="head">
<style type="text/css">
.top-bar,.middle-bar .content-container,.side-nav,.main-actions,footer{display:none;}
html,body,form,.main{height:100%;}
.main{position:relative;z-index:1;}.main:after{content:"";display:block;position:absolute;top:0;right:0;bottom:0;left:0;background:#EBE9CF;opacity:0.3;z-index:2;}
.logo{float:none;position:relative;width:100%;display:block;text-align:center;padding:50px 3% 12px;z-index:3;}
.content{display:block;position:relative;width:100%;padding:0;z-index:3;}
.content .content-container,.old-ie .content .content-container{display: block;width:100%;padding:0;}
.modal-content{margin:10px auto 0;width:70%;max-width:640px;}
.modal-content p{margin-top:3px;}
</style>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="modal-content">
        <div class="modal-body">
            <asp:Panel DefaultButton="LoginButton" runat="server">
                <div id="LoginWrap" class="formWrapper">
                    <h1>Log In</h1>
                    <asp:ValidationSummary ID="ApplicationSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="Login" />
                    <div class="formWhole">
                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserNameTextbox" Text="Username"></asp:Label>
                        <!--CMallory - Task 00476 - The hidden input type box below disables the save password option for IE and partially works for Firefox by not allowing them to save a password -->
                        <input type="text" style="display:none" />  
                        <asp:TextBox CssClass="form-control" ID="UserNameTextbox" runat="server" ClientIDMode="Static"></asp:TextBox>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="ValUserName" runat="server" ControlToValidate="UserNameTextbox" ErrorMessage="* Required field" SetFocusOnError="true" CssClass="error-message"></asp:RequiredFieldValidator>
                    </div>
                    <div class="formWhole">
                        <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="PasswordTextbox" Text="Password"></asp:Label>
                        <!--CMallory - Task 00476 - The hidden input type box below disables the save password option for IE and partially works for Firefox by not allowing them to save a password -->
                        <input type="text" style="display:none" />  
                        <asp:TextBox CssClass="form-control" ID="PasswordTextbox" TextMode="Password" runat="server" ClientIDMode="Static" style="-webkit-text-security: square !important;"></asp:TextBox>
                        <input type="text" style="display:none" />  
                        <asp:RequiredFieldValidator Display="Dynamic" ID="ValPassword" runat="server" ControlToValidate="PasswordTextbox" ErrorMessage="* Required field" SetFocusOnError="true" CssClass="error-message"></asp:RequiredFieldValidator>
                        <p><a href="/Account/ForgotPassword">Forgot Password?</a></p>
                    </div>
                    <div class="formWhole">
                        <asp:Button ID="LoginButton" Text="Log On" runat="server" CssClass="btn btn-primary btn-large" OnClientClick="ChangeType()"/>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>

     <!--CMallory - Task 00476 - Added Javascript to work for users using Chrome -->
    <script>
        function ChangeType() {

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE");
            var chrome = navigator.userAgent.search("Chrome");
            var firefox = navigator.userAgent.search("Firefox");
            if (chrome > 0) {
                document.getElementById('PasswordTextbox').type = 'text';
            }
        }
    </script>
</asp:Content>
