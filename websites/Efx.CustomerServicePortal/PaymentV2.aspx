﻿<%@ Page Title="RPO Take A Payment" Language="C#" AutoEventWireup="true" CodeBehind="PaymentV2.aspx.cs" MasterPageFile="~/CSR.master" Inherits="Efx.CustomerServicePortal.Payment" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

		    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

            
            <div class="main-container" style="padding-top: 0px; padding-left:0px; ">
                <div class="formWrapper" style="padding-top:0px; margin-left: 0px;">
                    
                    <div style="margin-bottom: 36px; margin-left: 42px;">
                        <h2><span>Take A Payment</span></h2>
                    </div>
            <div style="margin-left:40px;">
                <div class="formFourth" style="width:41%;">
                    <asp:Label ID="PropertyLabel" AssociatedControlID="PropertyDropDown" runat="server" Text="Select a Property" />
                    <asp:DropDownList ID="PropertyDropDown" runat="server" AppendDataBoundItems="True" AutoPostBack="True" DataTextField="PropertyName" CssClass="form-control" DataValueField="PropertyId" OnSelectedIndexChanged="PropertyDropDown_SelectedIndexChanged">
                        <asp:ListItem Value="0" Text="-- Select a Property --"></asp:ListItem>
                    </asp:DropDownList>


                </div>
                <div class="formFourth" style="width:16%;">
                    <asp:Label ID="PayerTypeLabel" AssociatedControlID="PayerTypeRadioButtonList" runat="server" Text="Payer Type" />
                    <asp:RadioButtonList ID="PayerTypeRadioButtonList" runat="server" AutoPostBack="True" RepeatLayout="UnorderedList" CssClass="list-unstyled radio" >
                        <asp:ListItem Value="0" Text="Resident" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Applicant"></asp:ListItem>
                    </asp:RadioButtonList>
                    	        <asp:DropDownList ID="PaymentProcessingDateComboBox" runat="server" style="width:19%;float:left;margin-right:5px;padding-left:4px;padding-right:4px;" CssClass="form-control" Visible="false" >
			<asp:ListItem Value="1" Text="1"></asp:ListItem>
			<asp:ListItem Value="2" Text="2"></asp:ListItem>
			<asp:ListItem Value="3" Text="3"></asp:ListItem>
			<asp:ListItem Value="4" Text="4"></asp:ListItem>
			<asp:ListItem Value="5" Text="5"></asp:ListItem>
			<asp:ListItem Value="6" Text="6"></asp:ListItem>
			<asp:ListItem Value="7" Text="7"></asp:ListItem>
			<asp:ListItem Value="8" Text="8"></asp:ListItem>
			<asp:ListItem Value="9" Text="9"></asp:ListItem>
			<asp:ListItem Value="10" Text="10"></asp:ListItem>
			<asp:ListItem Value="11" Text="11"></asp:ListItem>
			<asp:ListItem Value="12" Text="12"></asp:ListItem>
			<asp:ListItem Value="13" Text="13"></asp:ListItem>
			<asp:ListItem Value="14" Text="14"></asp:ListItem>
			<asp:ListItem Value="15" Text="15"></asp:ListItem>
			<asp:ListItem Value="16" Text="16"></asp:ListItem>
			<asp:ListItem Value="17" Text="17"></asp:ListItem>
			<asp:ListItem Value="18" Text="18"></asp:ListItem>
			<asp:ListItem Value="19" Text="19"></asp:ListItem>
			<asp:ListItem Value="20" Text="20"></asp:ListItem>
			<asp:ListItem Value="21" Text="21"></asp:ListItem>
			<asp:ListItem Value="22" Text="22"></asp:ListItem>
			<asp:ListItem Value="23" Text="23"></asp:ListItem>
			<asp:ListItem Value="24" Text="24"></asp:ListItem>
			<asp:ListItem Value="25" Text="25"></asp:ListItem>
			<asp:ListItem Value="26" Text="26"></asp:ListItem>
			<asp:ListItem Value="27" Text="27"></asp:ListItem>
			<asp:ListItem Value="28" Text="28"></asp:ListItem>
			<asp:ListItem Value="29" Text="29"></asp:ListItem>
			<asp:ListItem Value="30" Text="30"></asp:ListItem>
			<asp:ListItem Value="31" Text="31"></asp:ListItem>
		</asp:DropDownList>
                </div>

                <%--Salcedo - added div to nest the other divs - 6/30/2014--%>
                <div class="formFourth" style="width:42%;">
                    <div class="formFourth" style="width:71%;"> <%--was 30%--%>
                        <asp:Label ID="PayerLabel" AssociatedControlID="PayerDropDown" runat="server" Text="Select a Payer" />
                        <asp:DropDownList ID="PayerDropDown" CssClass="form-control" runat="server"></asp:DropDownList>
                    
                    </div>
                    <div class="formFourth noLabel" style="width:29%;"> <%--was 12% --%>
                        <asp:Button ID="LoadPayerButton" runat="server" Text="Load" CssClass="btn btn-default" OnClick="LoadPayerButton_Click" />
                    </div>
                    
                    <%--Salcedo - fixed formatting/location of this link - 6/30/2014--%>
                    <div style="width:100%; float:left; padding-top: 0px; padding-left:14px; padding-bottom: 0px;">
                        <asp:HyperLink ID="ApplicantLink" runat="server" Text="Not listed, click here: Future Resident/Applicant Info" CssClass="generalLink" Visible="false" />
                    </div>

                </div>
            </div>


                    <!--cakel: BUGID000116 - Display Warning message when resident is cash only-->
                    <div style="padding: 10px; text-align: center;">
                            <asp:Label ID="WarningLabeltxt" runat="server" Visible="False"></asp:Label>
                    </div>
   
                    <div style="margin-top: 50px; margin-left:20px;">
			            <asp:ValidationSummary ID="ApplicationSummary" DisplayMode="List" CssClass="alert alert-danger"  runat="server" ValidationGroup="Payment" />
                    </div>

                    <div class="formHalf">
                        <h2>Payer's Information</h2>
                        <div class="formWhole">
                            <label class="checkbox">
                                <asp:CheckBox ID="DifferentPayerCheckBox" runat="server" AutoPostBack="True" />Different Payer</label>
                        </div>
                        <uc1:PersonalInformation ID="PersonalInformation" runat="server" />
                    </div>
                        <div class="clear"></div>
                        <uc1:ResidentBillingInformation ID="ResidentBillingInformation" runat="server" />
        
                    <div class="clear"></div>

                    <div class="formHalf">
                        <h2>Payment Information</h2><asp:Label runat="server" ID="uxPaymentInformationLabel" Text="This property (or client) is not configured to accept payments." Visible="false"></asp:Label>
                        <uc1:SelectPaymentType ID="SelectPaymentType" runat="server" />
                    </div>

                    <div class="formHalf">
                        <div style="min-height:305px;">

                
                        <asp:Panel ID="AutoPayPanel" runat="server">
                            <h3>Setup AutoPayments</h3>
                            <uc1:AutoPayTile ID="AutoPayTile" runat="server" />
                        </asp:Panel>
                            </div>
                    </div>

                    <div class="formHalf">
                        <!--cakel: BUGID00053 added link to redirect user to update fees -->
                        <%--<asp:HyperLink ID="UpdateResidentFeesLink" runat="server" CssClass="generalLink" Visible="False">Update Residents Fees</asp:HyperLink>--%>
				        <uc1:PaymentAmountsAlternate ID="PaymentAmounts" runat="server" />
			        </div>

			        <div class="formHalf">
				        <hr />
                        <!--cakel: BUGID 00053 - commented out link and added link to top of page -->
				        <%--<div class="formWhole">
					        <asp:HyperLink ID="ApplicantLink" runat="server" Text="Not listed, click here: Future Resident/Applicant Info" CssClass="generalLink" />
				        </div>--%>
				        <div class="formWhole">
                            <!--cakel: BUGID000133 - updated checkbox to autopostback -->
					        <label class="checkbox"><asp:CheckBox ID="TermsCheckbox" runat="server" AutoPostBack="True" OnCheckedChanged="TermsCheckbox_CheckedChanged" />Accept Terms of Agreement</label>
				        </div>
				        <div class="formWhole" title="For non-integrated properties only. By checking this box, the payment will be charged but will not apply to the current balance. The payment will instead be marked as a credit for the next billing cycle.">
					
                            <%--cakel: BUGID00053 - Updated label runat server and id to hide when applicant--%>
                            <label class="checkbox" id="NextMOnthLabel" runat="server"><asp:CheckBox ID="uxApplyPaymentToNextMonth" runat="server" />Apply this payment to next month's rent</label>
				        </div>
				        <hr style="margin-top:0;" />
			        </div>
            
            </div>
	        <div class="button-footer">
		        <div class="button-action">
			        <%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default" />--%>
		        </div>
		        <div class="main-button-action">
                    <!--CMallory - Task 00601 - Added OnClientClick Attribute to keep user from clicking the Pay Now button twice.-->
			        <asp:Button ID="PayNowButton" runat="server" Text="Pay Now" CssClass="btn btn-primary btn-large" OnClientClick="DisablePaymentButton()" UseSubmitBehavior="false" OnClick="PayNowButton_Click" />
		        </div>
		        <div class="clear"></div>
	        </div>

            <script>
                $(document).ready(function () {
                    /* Mask */
                    ApplySingleMask("#<%=ResidentBillingInformation.PhoneNumberClientId%>", "(000) 000-0000");
                        ApplySingleMask("#<%=ResidentBillingInformation.ZipCodeClientId%>", "00000");
                });

                //CMallory - Task 00633 - Took out inline javascript from the PayNowButton and added a call to the function below. 
                function DisablePaymentButton() {
                    var PayButton = document.getElementById("<%=PayNowButton.ClientID%>");
                    PayButton.disabled = true;
                    PayButton.value = "Payment Processing, please wait...";
                }
            </script>
            </div>
            
            <!-- Include all compiled plugins (below), or include individual files as needed -->
		    <script src="<%= ResolveClientUrl("~/Scripts/bootstrap.min.js") %>"></script>
    
		    <script src="<%= ResolveClientUrl("~/Scripts/jquery.design.js") %>"></script>
            <script src="<%= ResolveClientUrl("~/Scripts/jquery.urlvars.js") %>"></script>


</asp:Content>
