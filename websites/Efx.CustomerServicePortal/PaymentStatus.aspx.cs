﻿using System.Linq;
using EfxFramework;
using EfxFramework.PaymentMethods;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Web;
using RPO;

namespace RentPaidOnline
{
    public partial class PaymentStatus : BasePageV2, IPaymentStatus
    {
        private PaymentStatusPresenter _Presenter;

        public int UserId { get; private set; }

        public string TransactionId
        {
            get { return Request.QueryString["TransactionId"]; }
            set { TransactionIdLabel.Text = value; }
        }

        public int? ApplicantId { get { return HttpContext.Current.Request.QueryString["ApplicantId"].ToNullInt32(); } }

        public string RentAmount { set { RentAmountLabel.Text = value; } }
        public string ConvenienceFee { set { ConvenienceFeeLabel.Text = value; } }
        public string PaymentTotal { set { TotalPaymentAmountLabel.Text = value; } }
        public string PropertyName { set { PropertyNameLabel.Text = value; } }
        public string PropertyAddress { set { PropertyAddressLabel.Text = value; } }
        public string PropertyCityStateZip { set { PropertyCityStateZipLabel.Text = value; } }
        public string FirstName { set { FirstNameLabel.Text = value; } }
        public string MiddleName { set { MiddleNameLabel.Text = value; } }
        public string LastName { set { LastNameLabel.Text = value; } }
        public string Suffix { set { SuffixLabel.Text = value; } }
        public string BillingAddress { set { BillingAddressLabel.Text = value; } }
        public string BillingCityStateZip { set { BillingCityStateZipLabel.Text = value; } }
        public string PhoneNumber { set { PhoneNumberLabel.Text = value; } }
        public string PaymentMethod { set { PaymentMethodLabel.Text = value; } }
        public EventHandler PrintClicked { set { PrintButton.Click += value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            supportEmail.Text = EfxSettings.SupportEmail;
            supportPhone.Text = EfxSettings.SupportPhone;

            Page.Header.DataBind();

            if (!ApplicantId.HasValue)
            {
                _Presenter = new PaymentStatusPresenter(this);

                if (!Page.IsPostBack)
                    _Presenter.InitializeValues();

                UserId = _Presenter.GetPaymentForView().RenterId;

                string editor = RPO.Helpers.Helper.GetCurrentUserName(true, RPO.Helpers.Helper.IsCsrUser);
                string updatedUSerNameNote = "Payment processed on " + Convert.ToString(System.DateTime.Now);
                EfxFramework.Renter.AddNoteForRenter(-100, UserId, updatedUSerNameNote, editor, 2);
            }
            else
                PopulateForApplicant();
        }

        private void PopulateForApplicant()
        {
            var Transaction = EfxFramework.Transaction.GetTransactionByInternalTransactionId(TransactionId);
            if (Transaction.TransactionId < 1)
                return;

            var Payments = EfxFramework.FeePayment.GetAllFeePaymentsByTransactionId(Transaction.TransactionId).Where(f => f.FeePaid).ToList();
            if (!Payments.Any())
                return;

            TransactionId = Transaction.InternalTransactionId;
            RentAmount = Payments.Select(payment => new EfxFramework.ApplicationFee(payment.ApplicationFeeId)).Select(fee => fee.FeesPaid).Sum().ToString("C");
            PaymentTotal = RentAmountLabel.Text;

            var Property = new EfxFramework.Property(Transaction.PropertyId);
            if (Property.PropertyId.HasValue && Property.PropertyId.Value > 0)
            {
                PropertyName = Property.PropertyName;
                PropertyAddress = Property.DisplayAddress;
            }

            FirstName = Transaction.FirstName;
            LastName = Transaction.LastName;
            BillingAddress = String.Format("{0} {1}", Transaction.StreetAddress, Transaction.StreetAddress2);
            BillingCityStateZip = String.Format("{0}, {1} {2}", Transaction.City, ((EfxFramework.StateProvince)Transaction.StateProvinceId).ToString(), Transaction.PostalCode);
            PhoneNumber = Transaction.PhoneNumber;
            PaymentMethod = ((EfxFramework.PaymentMethods.PaymentType)Transaction.PaymentTypeId).ToString();
        }
    }
}