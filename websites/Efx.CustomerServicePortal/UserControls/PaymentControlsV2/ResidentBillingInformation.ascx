﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResidentBillingInformation.ascx.cs" Inherits="RentPaidOnline.UserControls.PaymentControlsV2.ResidentBillingInformation" %>

<div class="formWhole">
    <h2>Billing Address</h2>
    <label class="checkbox">
        <asp:CheckBox ID="SameAsMailingCheckBox" runat="server" CssClass="checkBoxText" AutoPostBack="True" />Same as Mailing</label>
</div>
<!--**Fifth Row (Billing Address)**-->
<div class="formHalf">
    <div class="formWhole">
        <label>Billing Address</label>
        <asp:TextBox ID="BillingAddressTextBox" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <!--**Sixth Row (City)**-->
    <div class="formWhole">
    <div class="formTwoThirds">
        <label>City</label>
        <asp:TextBox ID="CityTextBox" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
        </div>
    <!--**Seventh Row (State & Zip)**-->
    <div class="formWhole">
        <div class="formHalf">
            <label>State</label>
            <asp:DropDownList ID="StateDropDownList" runat="server" CssClass="form-control"></asp:DropDownList>
        </div>

        <div class="formHalf">
            <label>Zip code</label>
            <asp:TextBox ID="ZipCodeTextBox" CssClass="form-control" runat="server" Width="100px"></asp:TextBox>
        </div>
    </div>
</div>
<!--**Eigth Row (Phone Number)**-->
<div class="formHalf">
    <div class="formWhole">
        <label>Email Address</label>
        <asp:TextBox ID="EmailAddressTextBox" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="formWhole">
        <label>Phone Number</label>
        <asp:TextBox ID="PhoneNumberTextBox" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
</div>
<div class="clear"></div>

<asp:RequiredFieldValidator ID="BillingAddressRequiredValidation"
    ErrorMessage="Billing Address is Required"
    Display="None"
    runat="server"
    ControlToValidate="BillingAddressTextBox"
    ValidationGroup="Payment" />

<asp:RequiredFieldValidator ID="CityRequiredValidation"
    ErrorMessage="City is Required"
    Display="None"
    runat="server"
    ControlToValidate="CityTextBox"
    ValidationGroup="Payment" />

<asp:RequiredFieldValidator ID="ZipCodeRequiredValidation"
    ErrorMessage="Zip Code is Required"
    Display="None"
    runat="server"
    ControlToValidate="ZipCodeTextBox"
    ValidationGroup="Payment" />

<script>
	$(function () {
		/* Mask */
		ApplySingleMask("#<%=ZipCodeTextBox.ClientID%>", "00000");
	});
</script>