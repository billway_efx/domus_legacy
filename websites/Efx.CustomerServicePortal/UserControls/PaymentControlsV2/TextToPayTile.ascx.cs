﻿using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;
using EfxFramework.ViewPresentation.Presenters.Factories;
using EfxFramework.ViewPresentation.Presenters.UserControls.PaymentTiles;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RentPaidOnline.UserControls.PaymentControlsV2
{
    public partial class TextToPayTile : UserControl, ITextToPayTile
    {
        private PaymentTilePresenterBase _Presenter;

        public int RenterId { get; set; }
        public EventHandler OnButtonClick { set { TextToPayOnButton.Click += value; } }
        public EventHandler OffButtonClick { set { TextToPayOffButton.Click += value; } }
        public string OnButtonCssClass { get { return TextToPayOnButton.CssClass; } set { TextToPayOnButton.CssClass = value; } }
        public string OffButtonCssClass { get { return TextToPayOffButton.CssClass; } set { TextToPayOffButton.CssClass = value; } }
        public DropDownList DayOfTheMonthDropdown { get { return TextRemindersComboBox; } }
        public DropDownList PaymentTypeDropdown { get { return TextToPayAccountComboBox; } }
        public string MobileNumberText { get { return TextToPayMobileNumberTextBox.Text; } set { TextToPayMobileNumberTextBox.Text = value; } }
        public bool IsOn { get; set; }
        public ServerValidateEventHandler RequiredMobileNumberValidation { set { RequiredTextToPayMobileNumberValidation.ServerValidate += value; } }
        public string MobileNumberClientId { get { return TextToPayMobileNumberTextBox.ClientID; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = PaymentTilePresenterFactory.GetPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }

		public void ReInitializeValues()
		{
			_Presenter.InitializeValues();
		}
    }
}