﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AutoPayTile.ascx.cs" Inherits="RentPaidOnline.UserControls.PaymentControlsV2.AutoPayTile" %>
<div class="well well-sm">
    <div class="well-header">
        <div class="expandable-table-actions generic">
			<asp:LinkButton ID="AutoPayOnButton" runat="server" CssClass="btn btn-primary" CausesValidation="false">On</asp:LinkButton>
			<asp:LinkButton ID="AutoPayOffButton" runat="server" CssClass="btn btn-blank" CausesValidation="false">Off</asp:LinkButton>
		</div>
		<h5>AutoPay</h5>
    </div>

	<!--Payment Amount -- read only -->
	<label style="margin:0;">Payment Amount: <b>Full Balance Due</b></label>
	<asp:Label ID="PaymentAmountLabel" runat="server" />
	<%--<div class="autoPayText">Auto Payments will draft the current balance due each month. The amount you see reflected here is a representation of your current rent and monthly fees. This amount is subject to change and may not be the amount paid every month.</div>--%>

	<hr />
	<div class="formHalf">
		<label>AutoPay Account</label>
		<asp:DropDownList ID="AutoPayPaymentMethodComboBox" runat="server" CssClass="form-control" AutoPostBack="True">
			<asp:ListItem Value="1" Text="Credit Card" Selected="True"></asp:ListItem>
			<asp:ListItem Value="2" Text="E-Check"></asp:ListItem>
		</asp:DropDownList>
	</div>
	<div class="clear"></div>
	<!--Payment Processing Date -->
	<div class="formWhole">
		<label>Payment Processing Date</label>
		<asp:DropDownList ID="PaymentProcessingDateComboBox" runat="server" style="width:19%;float:left;margin-right:5px;padding-left:4px;padding-right:4px;" CssClass="form-control">
			<asp:ListItem Value="1" Text="1"></asp:ListItem>
			<asp:ListItem Value="2" Text="2"></asp:ListItem>
			<asp:ListItem Value="3" Text="3"></asp:ListItem>
			<asp:ListItem Value="4" Text="4"></asp:ListItem>
			<asp:ListItem Value="5" Text="5"></asp:ListItem>
			<asp:ListItem Value="6" Text="6"></asp:ListItem>
			<asp:ListItem Value="7" Text="7"></asp:ListItem>
			<asp:ListItem Value="8" Text="8"></asp:ListItem>
			<asp:ListItem Value="9" Text="9"></asp:ListItem>
			<asp:ListItem Value="10" Text="10"></asp:ListItem>
			<asp:ListItem Value="11" Text="11"></asp:ListItem>
			<asp:ListItem Value="12" Text="12"></asp:ListItem>
			<asp:ListItem Value="13" Text="13"></asp:ListItem>
			<asp:ListItem Value="14" Text="14"></asp:ListItem>
			<asp:ListItem Value="15" Text="15"></asp:ListItem>
			<asp:ListItem Value="16" Text="16"></asp:ListItem>
			<asp:ListItem Value="17" Text="17"></asp:ListItem>
			<asp:ListItem Value="18" Text="18"></asp:ListItem>
			<asp:ListItem Value="19" Text="19"></asp:ListItem>
			<asp:ListItem Value="20" Text="20"></asp:ListItem>
			<asp:ListItem Value="21" Text="21"></asp:ListItem>
			<asp:ListItem Value="22" Text="22"></asp:ListItem>
			<asp:ListItem Value="23" Text="23"></asp:ListItem>
			<asp:ListItem Value="24" Text="24"></asp:ListItem>
			<asp:ListItem Value="25" Text="25"></asp:ListItem>
			<asp:ListItem Value="26" Text="26"></asp:ListItem>
			<asp:ListItem Value="27" Text="27"></asp:ListItem>
			<asp:ListItem Value="28" Text="28"></asp:ListItem>
			<asp:ListItem Value="29" Text="29"></asp:ListItem>
			<asp:ListItem Value="30" Text="30"></asp:ListItem>
			<asp:ListItem Value="31" Text="31"></asp:ListItem>
		</asp:DropDownList>
		<span class="formAsideText">of each month</span>
		<div class="clear"></div>
	</div>
</div>