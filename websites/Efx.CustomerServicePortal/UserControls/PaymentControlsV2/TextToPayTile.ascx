﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TextToPayTile.ascx.cs" Inherits="RentPaidOnline.UserControls.PaymentControlsV2.TextToPayTile" %>
<div class="well well-sm">
	<div class="well-header">
		<div class="expandable-table-actions generic">
			<asp:LinkButton ID="TextToPayOnButton" runat="server" CssClass="btn btn-primary" CausesValidation="false">On</asp:LinkButton>
			<asp:LinkButton ID="TextToPayOffButton" runat="server" CssClass="btn btn-blank" CausesValidation="false">Off</asp:LinkButton>
		</div>
		<h5>PayByText&#0153</h5>
	</div>
	<label style="margin:0;">Instructions: <b>Text "PayMyRent" to 57682</b></label>
	<asp:Label ID="PaymentAmountLabel" runat="server" />
	<hr />

	<div class="formWhole">
		<div class="formHalf">
			<label>Mobile Number</label>
			<asp:TextBox CssClass="form-control" ID="TextToPayMobileNumberTextBox" runat="server" MaxLength="10" onkeypress="return isNumericKey(event);"></asp:TextBox>
		</div>
		<div class="formHalf">
			<label>PayByText&#0153 Account</label>
			<asp:DropDownList ID="TextToPayAccountComboBox" runat="server" CssClass="form-control">
				<asp:ListItem Value="1" Text="Credit Card" Selected="True"></asp:ListItem>
				<asp:ListItem Value="2" Text="E-Check"></asp:ListItem>
			</asp:DropDownList>
		</div>
	</div>
	<div class="formWhole">
		<label>Text Reminders</label>
		<asp:DropDownList ID="TextRemindersComboBox" runat="server" style="width:19%;float:left;margin-right:5px;padding-left:4px;padding-right:4px;" CssClass="form-control">
			<asp:ListItem Value="1" Text="1"></asp:ListItem>
			<asp:ListItem Value="2" Text="2"></asp:ListItem>
			<asp:ListItem Value="3" Text="3"></asp:ListItem>
			<asp:ListItem Value="4" Text="4"></asp:ListItem>
			<asp:ListItem Value="5" Text="5"></asp:ListItem>
			<asp:ListItem Value="6" Text="6"></asp:ListItem>
			<asp:ListItem Value="7" Text="7"></asp:ListItem>
			<asp:ListItem Value="8" Text="8"></asp:ListItem>
			<asp:ListItem Value="9" Text="9"></asp:ListItem>
			<asp:ListItem Value="10" Text="10"></asp:ListItem>
			<asp:ListItem Value="11" Text="11"></asp:ListItem>
			<asp:ListItem Value="12" Text="12"></asp:ListItem>
			<asp:ListItem Value="13" Text="13"></asp:ListItem>
			<asp:ListItem Value="14" Text="14"></asp:ListItem>
			<asp:ListItem Value="15" Text="15"></asp:ListItem>
			<asp:ListItem Value="16" Text="16"></asp:ListItem>
			<asp:ListItem Value="17" Text="17"></asp:ListItem>
			<asp:ListItem Value="18" Text="18"></asp:ListItem>
			<asp:ListItem Value="19" Text="19"></asp:ListItem>
			<asp:ListItem Value="20" Text="20"></asp:ListItem>
			<asp:ListItem Value="21" Text="21"></asp:ListItem>
			<asp:ListItem Value="22" Text="22"></asp:ListItem>
			<asp:ListItem Value="23" Text="23"></asp:ListItem>
			<asp:ListItem Value="24" Text="24"></asp:ListItem>
			<asp:ListItem Value="25" Text="25"></asp:ListItem>
			<asp:ListItem Value="26" Text="26"></asp:ListItem>
			<asp:ListItem Value="27" Text="27"></asp:ListItem>
			<asp:ListItem Value="28" Text="28"></asp:ListItem>
			<asp:ListItem Value="29" Text="29"></asp:ListItem>
			<asp:ListItem Value="30" Text="30"></asp:ListItem>
			<asp:ListItem Value="31" Text="31"></asp:ListItem>
		</asp:DropDownList>
		<span class="formAsideText">of each month</span>
		<div class="clear"></div>
	</div>

	<asp:CustomValidator ID="RequiredTextToPayMobileNumberValidation"
			runat="server"
			ErrorMessage="Please enter a Valid Mobile Number, no special characters included"
			Display="None"
		/>
</div>

<script>
    function isNumericKey(e) {

        var charInp = window.event.keyCode;

        if (charInp > 31 && (charInp < 48 || charInp > 57)) {

            return false;
        }
        return true;
    }
</script>