﻿using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;
using EfxFramework.ViewPresentation.Presenters.Factories;
using EfxFramework.ViewPresentation.Presenters.UserControls.PaymentTiles;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RentPaidOnline.UserControls.PaymentControlsV2
{
    public partial class AutoPayTile : UserControl, IAutoPayTile
    {
        private PaymentTilePresenterBase _Presenter;

        public int RenterId { get; set; }
        public EventHandler PaymentTypeChanged { set { PaymentTypeDropdown.SelectedIndexChanged += value; } }
        public EventHandler OnButtonClick { set { AutoPayOnButton.Click += value; } }
        public EventHandler OffButtonClick { set { AutoPayOffButton.Click += value; } }
        public string OnButtonCssClass { get { return AutoPayOnButton.CssClass; } set { AutoPayOnButton.CssClass = value; } }
        public string OffButtonCssClass { get { return AutoPayOffButton.CssClass; } set { AutoPayOffButton.CssClass = value; } }
        public DropDownList DayOfTheMonthDropdown { get { return PaymentProcessingDateComboBox; } }
        public DropDownList PaymentTypeDropdown { get { return AutoPayPaymentMethodComboBox; } }
        public string PaymentAmountText { set { PaymentAmountLabel.Text = value; } }
        public bool IsOn { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = PaymentTilePresenterFactory.GetPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();

			int currentPropertyId = 0;
			DropDownList propertyDropDown = (DropDownList)this.Parent.FindControl("PropertyDropDown");
			if(propertyDropDown != null && propertyDropDown.SelectedValue != "0" && Int32.TryParse(propertyDropDown.SelectedValue, out currentPropertyId))
			{
				//get the Property by the Renter
				EfxFramework.Property currentProperty = new EfxFramework.Property(currentPropertyId);
				if (currentProperty != null && !currentProperty.AcceptACHPayments)
				{
					ListItem achItem = PaymentTypeDropdown.Items.FindByText("E-Check");
					achItem.Enabled = false;
				}
				else
				{
					ListItem achItem = PaymentTypeDropdown.Items.FindByText("E-Check");
					achItem.Enabled = true;
				}
				if (currentProperty != null && !currentProperty.AcceptCCPayments)
				{
					ListItem ccItem = PaymentTypeDropdown.Items.FindByText("Credit Card");
					ccItem.Enabled = false;
				}
				else
				{
					ListItem ccItem = PaymentTypeDropdown.Items.FindByText("Credit Card");
					ccItem.Enabled = true;
				}
			}
        }

		public void ReInitializeValues()
		{
			_Presenter.InitializeValues();
		}
    }
}