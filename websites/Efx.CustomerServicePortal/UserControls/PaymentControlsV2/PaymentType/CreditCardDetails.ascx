﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreditCardDetails.ascx.cs" Inherits="RentPaidOnline.UserControls.PaymentControlsV2.PaymentType.CreditCardDetails" %>
<!--***Credit Card Option***-->

<!--**Second Row (Card Type)**-->
<div class="formHalf">
    <label>Name On Card</label>
    <asp:TextBox ID="NameOnCardTextBox" CssClass="form-control" runat="server"></asp:TextBox>
</div>
<!--**Third Row (Card number)**-->
<div class="formThird">
    <label>Card Number</label>
    <asp:TextBox ID="CreditCardNumberTextBox" CssClass="form-control" runat="server" MaxLength="16"></asp:TextBox>
</div>
<div class="clear"></div>
<!--**Fourth Row (Exiration Date)**-->
<asp:TextBox ID="HiddenCCtxt" runat="server" Text="" Visible="false" />

<div class="formWhole">
	<div class="formHalf">   
		<div class="formWhole">
			<label>Expiration Date</label>
			<div class="formHalf">
				<asp:DropDownList ID="ExpirationMonthDropDown" runat="server" CssClass="form-control">
					<asp:ListItem Value="1" Text="January"></asp:ListItem>
					<asp:ListItem Value="2" Text="February"></asp:ListItem>
					<asp:ListItem Value="3" Text="March"></asp:ListItem>
					<asp:ListItem Value="4" Text="April"></asp:ListItem>
					<asp:ListItem Value="5" Text="May"></asp:ListItem>
					<asp:ListItem Value="6" Text="June"></asp:ListItem>
					<asp:ListItem Value="7" Text="July"></asp:ListItem>
					<asp:ListItem Value="8" Text="August"></asp:ListItem>
					<asp:ListItem Value="9" Text="September"></asp:ListItem>
					<asp:ListItem Value="10" Text="October"></asp:ListItem>
					<asp:ListItem Value="11" Text="November"></asp:ListItem>
					<asp:ListItem Value="12" Text="December"></asp:ListItem>
				</asp:DropDownList>
			</div>
			<div class="formHalf">
				<asp:DropDownList ID="ExpirationYearDropDown" runat="server" CssClass="form-control"></asp:DropDownList>
			</div>
		</div>
	</div>
	<div class="formHalf">
		<div class="formWhole">
			<div class="formHalf">
				<label>CVV Code</label>
				<asp:TextBox ID="CvvTextBox" runat="server" CssClass="form-control" MaxLength="4"></asp:TextBox>
			</div>
		</div>
	</div>
</div>




<asp:RequiredFieldValidator ID="RequiredNameOnCardValidation"
    Display="None"
    ErrorMessage="Name On Card is Required"
    runat="server"
    ValidationGroup="Payment"
    ControlToValidate="NameOnCardTextBox" />

<asp:RequiredFieldValidator ID="RequiredCardNumberValidation"
    Display="None"
    ErrorMessage="Credit Card Number is Required"
    runat="server"
    ValidationGroup="Payment"
    ControlToValidate="CreditCardNumberTextBox" />

<asp:RequiredFieldValidator ID="RequiredCvvValidation"
    Display="None"
    ErrorMessage="Cvv is Required"
    runat="server"
    ValidationGroup="Payment"
    ControlToValidate="CvvTextBox" />

<!-- CMallory - Task 00648 - Added validation below.  Credit Card number allows for dashes and spaces as well but the field has a 16 digit restriction and the validation forces a user to enter 16 digits as well. -->
<asp:RegularExpressionValidator ID="CreditCardFormatValidation"
    Display="None"
    ErrorMessage="Please enter just the 16 numbers on the credit card. I.E 1234567891234567"
    runat="server"
    ValidationGroup="Payment"
    ControlToValidate="HiddenCCtxt"
    ValidationExpression="\b(?:3[47]\d{2}([\ \-]?)\d{6}\1\d|(?:(?:4\d|5[1-5]|65)\d{2}|6011)([\ \-]?)\d{4}\2\d{4}\2)\d{4}\b"/>

<!-- CMallory - Task 00648 - Forces a user to only use numbers -->
<asp:RegularExpressionValidator ID="CSVFormatValidation"
    Display="None"
    ErrorMessage="Please only use numbers for the CVV. I.E. 1234"
    runat="server"
    ValidationGroup="Payment"
    ControlToValidate="CvvTextBox"
    ValidationExpression="^[0-9]*$"/>