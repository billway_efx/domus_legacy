﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ElectronicCheckDetails.ascx.cs" Inherits="RentPaidOnline.UserControls.PaymentControlsV2.PaymentType.ElectronicCheckDetails" %>
<!--***E-Check Option***-->
<asp:TextBox ID="HiddenACHAccountNumbertxt" runat="server" Text="" Visible="false" />
<!--**Second Row (Account Number)**-->
<div class="formHalf">
	<label>Routing Number</label>
	<asp:TextBox ID="RoutingNumberTextBox" CssClass="form-control" runat="server" MaxLength="9"></asp:TextBox>
	<asp:RequiredFieldValidator ID="RequiredRoutingNumberValidation" 
		Display="None" 
		ErrorMessage="Routing Number is Required" 
		runat="server" 
		ValidationGroup="Payment" 
		ControlToValidate="RoutingNumberTextBox" />

    <!-- CMallory - Task 00648 - Forces a user to only use numbers -->
    <asp:RegularExpressionValidator ID="ACHRoutingNumberFormatValidation"
        Display="None"
        ErrorMessage="Please only use numbers for the Routing Number"
        runat="server"
        ValidationGroup="Payment"
        ControlToValidate="RoutingNumberTextBox"
        ValidationExpression="^[0-9]*$"/>
</div>
                        
<!--**Third Row (Routing Number)**-->
<div class="formHalf">
	<label>Checking Account Number</label>
    <!-- cakel: BUGID00072 - 08/04/2014 - Updated MaxLength to 17 from 15 -->
	<asp:TextBox ID="AccountNumberTextBox" CssClass="form-control" runat="server" MaxLength="17"></asp:TextBox>
	<asp:RequiredFieldValidator ID="RequiredAccountNumberValidation" 
		Display="None" 
		ErrorMessage="Checking Account Number is Required" 
		runat="server" 
		ValidationGroup="Payment" 
		ControlToValidate="AccountNumberTextBox" />

<!-- CMallory - Task 00648 - Forces a user to only use numbers -->
    <asp:RegularExpressionValidator ID="ACHAccountNumberFormatValidation"
        Display="None"
        ErrorMessage="Please only use numbers for the Account Number"
        runat="server"
        ValidationGroup="Payment"
        ControlToValidate="HiddenACHAccountNumbertxt"
        ValidationExpression="^[0-9]*$"/>
</div>