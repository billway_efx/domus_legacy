﻿using System.Web.UI.WebControls;
using EfxFramework.Interfaces.UserControls.PaymentControlsV2;
using EfxFramework.Presenters.UserControls.PaymentControlsV2;
using System;
using System.Web;
using System.Web.UI;

namespace RentPaidOnline.UserControls.PaymentControlsV2
{
    public partial class Refund : UserControl, IRefund
    {
        private RefundPresenter _Presenter;

        public Page ParentPage { get { return Page; } }
        public string TransactionId { get { return HttpContext.Current.Request.QueryString["TransactionId"]; } }
        public string ResidentNameText { set { ResidentNameLabel.Text = value; } }
        public string TransactionIdText { set { TransactionLabel.Text = value; } }
        public string RefundAmountText { get { return RefundAmountTextBox.Text; } set { RefundAmountTextBox.Text = value; } }
        public string ProcessingDateText { set { ProcessingDateLabel.Text = value; } }
        public string RefundDateText { set { RefundDateLabel.Text = value; } }
        public string PaymentMethodText { set { PaymentMethodLabel.Text = value; } }
        public Button SubmitRefundBtn { get { return SubmitRefundButton; } }
        public HiddenField AchProcessingConfirmedField { get { return AchProcessingConfirmed; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new RefundPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.IntializeValues();
        }
    }
}