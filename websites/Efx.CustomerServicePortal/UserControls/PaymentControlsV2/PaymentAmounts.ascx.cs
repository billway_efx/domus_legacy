﻿using System.Web;
using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Collections.Generic;
using System.Web.UI;

namespace RentPaidOnline.UserControls.PaymentControlsV2
{
    public partial class PaymentAmounts : UserControl, IPaymentAmounts
    {
        private PaymentAmountsPresenter _Presenter;

        public int RenterId { get; set; }
        public IPayment ParentPage { get { return (Page as IPayment); } }
        public string RentAmountText { set { RentAmountLabel.Text = value; } }
        public string ConvenienceFeeText { set { ConvenienceFeeLabel.Text = value; } }
        public string TotalAmountText { set { TotalAmountLabel.Text = value; } }
        public string PastDueHeaderText { set { PastDueHeaderLabel.Text = value; } }
        public string PastDueAmountText { set { PastDueAmountLabel.Text = value; } }
        public bool PastDueVisible { set { PastDuePanel.Visible = value; } }
        public ISelectPaymentType PaymentTypes { get; set; }
        public decimal ConvenienceFeeAmount { get; set; }
        public decimal RentAmount { get; set; }

        public List<LeaseFee> MonthlyFeesDataSource
        {
            set
            {
                MonthlyFeesList.DataSource = value;
                MonthlyFeesList.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new PaymentAmountsPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }

		public void ReInitializeValues()
		{
			_Presenter.InitializeValues();
		}
    }
}