﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentInfoTab.ascx.cs" Inherits="RentPaidOnline.UserControls.PaymentControlsV2.PaymentInfo.PaymentInfoTab" %>
<asp:ValidationSummary ID="PaymentInfoTabSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="Payment" />
<asp:PlaceHolder ID="paymentInfoSuccess" runat="server" Visible="false"><div class="alert alert-success"><b>Success!</b></div></asp:PlaceHolder>
<div class="formTint">
    <div class="formWrapper">
		<h2>Payment Information</h2>
        <uc1:SelectPaymentType ID="SelectPaymentType" runat="server" />
    </div>
</div>
<div class="formWrapper">
    <uc1:ResidentBillingInformation ID="ResidentBillingInformation" runat="server" />
</div>
<div class="formTint">
    <div class="formWrapper">
		<h2>Personal Information</h2>
        <uc1:PersonalInformation ID="PersonalInformation" runat="server" />
    </div>
</div>
<div class="formWrapper orWrapper">
    <!--cakel: BUGID00069 - Capitalized "P" in h2 tag -->
	<h2>Scheduled Payment Options</h2>
	<div class="formHalf">
		<uc1:AutoPayTile ID="AutoPayTile" runat="server" />
	</div>
	<div class="formHalf">
		<uc1:TextToPayTile ID="TextToPayTile" runat="server" />
	</div>
	<span class="or-text">Or</span>
</div>
<div class="formWrapper">
	<div class="formHalf">
        <!--cakel: BUGID00085 Part 3 - Added display:None to hide user control -->
        <div style="display:none">
            <uc1:PaymentAmounts ID="PaymentAmounts" runat="server" />
        </div>
		
	</div>
</div>
<div class="button-footer">
	<div class="button-action">
		<%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default" OnClick="btnCancel_Click" CausesValidation="false" />--%>
	</div>
	<div class="main-button-action">
		<asp:Button ID="SaveButton" CssClass="btn btn-primary btn-large" runat="server" Text="Save" CausesValidation="False" />
	</div>
	<div class="clear"></div>
</div>