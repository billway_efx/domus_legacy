﻿using System.Web;
using EfxFramework.Interfaces.UserControls.PaymentControlsV2;
using EfxFramework.Presenters.UserControls.PaymentControlsV2;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;
using System;
using System.Web.UI;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System.Web.UI.WebControls;

namespace RentPaidOnline.UserControls.PaymentControlsV2.PaymentInfo
{
    public partial class PaymentInfoTab : UserControl, IPaymentInfoTab
    {
        private PaymentInfoTabPresenter _Presenter;

        public Page ParentPage { get { return Page; } }
		public PlaceHolder PaymentSuccessPlaceHolder { get { return paymentInfoSuccess; } }
        public IPersonalInformation PersonalInformationControl { get { return PersonalInformation; } }
        public ISelectPaymentType SelectPaymentTypeControl { get { return SelectPaymentType; } }
        public IResidentBillingInformation ResidentBillingInformationControl { get { return ResidentBillingInformation; } }
        public IPaymentAmounts PaymentAmountsControl { get { return PaymentAmounts; } }
        public IAutoPayTile AutoPayTileControl { get { return AutoPayTile; } }
        public ITextToPayTile TextToPayTileControl { get { return TextToPayTile; } }
        public EventHandler SaveButtonClicked { set { SaveButton.Click += value; } }
        public bool IsPropertyManagerSite { get { return false; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeControls();
            _Presenter = new PaymentInfoTabPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }

        private void InitializeControls()
        {
            var Id = HttpContext.Current.Request.QueryString["renterId"].ToNullInt32();
            if (!Id.HasValue || Id.Value < 1)
            {
                BasePage.SafePageRedirect("~/Renters/Default.aspx");
                return;
            }

            //Salcedo - 5/23/2014 - Task # 0000007 - update CurrentBalanceDue from Yardi
            //cakel: BUGID00074 Function Name Update - “UpdateRenterBalanceDueForPrePay” to “UpdateRenterBalanceDueRealTimeYardi”.  
            var Resident = new EfxFramework.Renter(Id.Value);

            if (Resident.PmsTypeId == 1 && Resident.PmsId != "")
            {
                EfxFramework.Pms.Yardi.ImportRequest.UpdateRenterBalanceDueRealTimeYardi(Resident);
            }
            //cakel: BUGID00316 - AMSI Update Balance call
            if (Resident.PmsTypeId == 2 && Resident.PmsId != "")
            {
                //cakel: 03/23/2015 added code to call code to update balances
                EfxFramework.Pms.AMSI.AmsiRequest.AMSI_RequestXmlForRenterImport(2, _RenterID: Resident.RenterId);
            }

            PersonalInformation.RenterId = Id.Value;
            SelectPaymentType.RenterId = Id.Value;
            ResidentBillingInformation.RenterId = Id.Value;
            PaymentAmounts.RenterId = Id.Value;
            PaymentAmounts.PaymentTypes = SelectPaymentType;
            AutoPayTile.RenterId = Id.Value;
            TextToPayTile.RenterId = Id.Value;
        }

		protected void btnCancel_Click(object sender, EventArgs e)
		{
			SelectPaymentType.ReInitialize();
			ResidentBillingInformation.ReInitializeValues();
			PersonalInformation.ReInitializeValues();
			AutoPayTile.ReInitializeValues();
			TextToPayTile.ReInitializeValues();
			PaymentAmounts.ReInitializeValues();
		}
    }
}