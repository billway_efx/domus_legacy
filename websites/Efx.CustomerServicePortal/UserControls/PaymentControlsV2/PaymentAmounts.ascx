﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentAmounts.ascx.cs" Inherits="RentPaidOnline.UserControls.PaymentControlsV2.PaymentAmounts" %>

<div class="well well-sm">
    <h2>Total Payment Amount</h2>                                  
    <!--Rent Amount -->
    <div class="itemRow">
        <span class="itemTitle">Rent:</span>
        <span class="itemAmount"><asp:Label ID="RentAmountLabel" runat="server" /></span>
    </div>
    <!--Past Due Amount -->
    <asp:Panel ID="PastDuePanel" runat="server" Visible="False">
        <div class="itemRow">
            <span class="itemTitle"><asp:Label ID="PastDueHeaderLabel" runat="server" /></span>
            <span class="itemAmount"><asp:Label ID="PastDueAmountLabel" runat="server" /></span>
        </div>
    </asp:Panel>                                  
    <!--Convenience Fee Amount -->
    <div class="itemRow">
        <span class="itemTitle">Convenience Fee:</span>
        <span class="itemAmount"><asp:Label ID="ConvenienceFeeLabel" runat="server" /></span>
    </div>                            
    <asp:ListView ID="MonthlyFeesList" runat="server">
        <LayoutTemplate>
            <div id="itemPlaceholder" class="itemRow" runat="server"></div>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="itemRow">
                <span class="itemTitle"><asp:Label ID="ItemTitleLabel" runat="server" Text='<%# Bind("FeeName") %>' /></span>
                <span class="itemAmount"><asp:Label ID="ItemAmountLabel" runat="server" Text='<%# Bind("FeeAmountText") %>' /></span>
            </div>
        </ItemTemplate>
    </asp:ListView>                     
    <!--Total -->
    <div class="well-footer">
		<span class="pull-right">Total: <asp:Label ID="TotalAmountLabel" CssClass="text-success" runat="server" /></span>
		<div class="clear"></div>
        <p>*Note that convenience fees will be applied to partial and full payments</p>
    </div>
</div>
