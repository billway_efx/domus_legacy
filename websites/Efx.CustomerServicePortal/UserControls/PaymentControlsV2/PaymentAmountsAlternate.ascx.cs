﻿using EfxFramework.Interfaces.UserControls.PaymentControlsV2;
using EfxFramework.Presenters.UserControls.PaymentControlsV2;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mb2x.ExtensionMethods;

namespace RentPaidOnline.UserControls.PaymentControlsV2
{
    public partial class PaymentAmountsAlternate : UserControl, IPaymentAmountsAlternate
    {
        private PaymentAmountsAlternatePresenter _Presenter;

        public int RenterId { get; set; }
        public EfxFramework.Interfaces.IPayment ParentPage { get { return (Page as EfxFramework.Interfaces.IPayment); } }
        public MultiView PaymentAmountsView { get { return PaymentAmountsMultiView; } }
        public View RentAmountsView { get { return RentView; } }
        public View ApplicationAmountsView { get { return ApplicationFeeView; } }
        public string RentAmountText { get { return RentAmountLabel.Text; } set { RentAmountLabel.Text = value; } }
        public string ConvenienceFeeText { get { return ConvenienceFeeLabel.Text; } set { ConvenienceFeeLabel.Text = value; } }
        public string TotalAmountText { set { TotalAmountLabel.Text = value; } }
        public string PastDueHeaderText { set { PastDueHeaderLabel.Text = value; } }
        public string PastDueAmountText { get { return PastDueAmountLabel.Text; } set { PastDueAmountLabel.Text = value; } }
        public bool PastDueVisible { set { PastDuePanel.Visible = value; } }
        public ListView MonthlyFeesList { get { return MonthlyFeesListView; } }
        public ListView ApplicationFeesList { get { return ApplicationFeesListView; } }
        public ISelectPaymentType PaymentTypes { get; set; }
        public decimal ConvenienceFeeAmount { get; set; }
        public decimal RentAmount { get; set; }
        public CheckBox WaiveRent { get { return RentWaiveCheckBox; } }
        public CheckBox WaivePastDue { get { return PastDueWaiveCheckBox; } }
        public CheckBox WaiveConvenienceFee { get { return ConvenienceFeeWaiveCheckBox; } }
        public EventHandler UpdateTotalClicked { set { RefreshButton.Click += value; } }
        public TextBox OverrideText { get { return OverrideTextBox; } }
        public Label OverrideLabel { get { return OverrideDescription; } }
		public EventHandler<System.Web.UI.WebControls.ListViewItemEventArgs> FeeListViewItemDataBind { set { MonthlyFeesListView.ItemDataBound += value; } }
        public string AppConvenienceFeeText { get { return AppConvenienceFeeLabel.Text; } set { AppConvenienceFeeLabel.Text = value; } }
        public decimal AppConvenienceFeeAmount { get; set; }
         public CheckBox AppWaiveConvenienceFee { get { return AppConvenienceFeeWaiveCheckBox; } }

        protected void Page_Load(object sender, EventArgs e)
        {

            
            _Presenter = new PaymentAmountsAlternatePresenter(this);

			ConvenienceFeeWaiveCheckBox.Enabled = EfxFramework.Helpers.Helper.IsRpoAdmin;

            //cakel: BUGID00053 - Check for RPO Admin if not, hide waive buttons
            ConvenienceFeeWaiveCheckBox.Visible = EfxFramework.Helpers.Helper.IsRpoAdmin;

            RentWaiveCheckBox.Visible = false;
            ApplicationFeesListView.Visible = true;

            if (EfxFramework.Helpers.Helper.IsRpoAdmin == false)
            {
                WaiveFeeTitle.InnerText = "";
                Th1.InnerText = ""; 
            }


            //cakel: BUGID00053 - add link back to renter details to update fees

          if (RenterId != 0)
          {
              int PropertyID = 0;
              try
              {
                  PropertyID =  (int)EfxFramework.Renter.GetPropertyByRenterId(RenterId);
              }
              catch
              {
                  PropertyID = 0;
              }

              if (PropertyID != 0)
              {
                  //UpdateResidentFeesLink.Visible = true;
                  //string LinkString = "/MyProperties/PropertyDetails.aspx?propertyId=" + PropertyID + "#RentAndFeesTab";
                  //UpdateResidentFeesLink.NavigateUrl = LinkString;
              }

          }

          //Salcedo - 2/27/2015 - CSR portal users should not be allowed to waive convenience fee
          Page.LoadComplete += new EventHandler(Page_LoadComplete);

        }

        //Salcedo - 2/27/2015 - CSR portal users should not be allowed to waive fees
        void Page_LoadComplete(object sender, EventArgs e)
        {
            RentWaiveCheckBox.Enabled = false;
            RentWaiveCheckBox.Visible = false;
            ConvenienceFeeWaiveCheckBox.Enabled = false;
            ConvenienceFeeWaiveCheckBox.Visible = false;
            WaiveFeeTitle.InnerText = "";
            Th1.InnerText = "";

            // Salcedo - 7/2/2015 - only show the UpdateTotal button if we can override the amount
            RefreshButton.Visible = OverrideText.Enabled;
        }
    }
}