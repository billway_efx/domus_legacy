﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PersonalInformation.ascx.cs" Inherits="RentPaidOnline.UserControls.PaymentControlsV2.PersonalInformation" %>

<div class="formWhole">
    <table style="border: 0px lightgray  solid; width: 50%;" id="tblName" class="formHalf">
        <tr>
            <td style="width: 25%;">
                <label>First Name</label>            
            </td>
            <td style="width: 25%;">
                <label>Middle Name</label>
            </td>
            <td style="width: 25%;">
                <label>Last Name</label>
            </td>
            <td style="width: 25%;">
                <label>Suffix</label>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
                <asp:TextBox ID="FirstNameTextBox" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFirstNameValidation"
                    runat="server"
                    Display="Static"
                    ErrorMessage="First Name is Required"
                    ControlToValidate="FirstNameTextBox"
                    ValidationGroup="Payment" InitialValue="" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
            </td>
            <td style="width: 25%;">
                <asp:TextBox ID="MiddleNameTextBox" CssClass="form-control" runat="server"></asp:TextBox>
            </td>
            <td style="width: 25%;">
                <asp:TextBox ID="LastNameTextBox" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredLastNameValidation"
                    runat="server"
                    Display="Static"
                    ErrorMessage="Last Name is Required"
                    ControlToValidate="LastNameTextBox"
                    ValidationGroup="Payment" InitialValue="" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
            </td>
            <td style="width: 25%;">
                <asp:TextBox ID="SuffixTextBox" CssClass="form-control" runat="server" Width="100px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
                <label>Email Address</label>
            </td>
            <td style="width: 25%;">
                <label>Phone Number</label>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
                <asp:TextBox ID="EmailAddressTextBox" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredEmailValidator"
                                            runat="server"
                                            Display="Static"
                                            ErrorMessage="EMail Address is Required"
                                            ControlToValidate="LastNameTextBox"
                                            ValidationGroup="Payment" InitialValue="" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
            </td>
            <td style="width: 25%;">
                <asp:TextBox ID="PhoneNumberTextBox" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredPhoneValidator"
                                            runat="server"
                                            Display="Static"
                                            ErrorMessage="Phone Number is Required"
                                            ControlToValidate="LastNameTextBox"
                                            ValidationGroup="Payment" InitialValue="" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
</div>



