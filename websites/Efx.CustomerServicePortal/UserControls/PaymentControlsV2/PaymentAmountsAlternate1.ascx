﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PaymentAmountsAlternate.ascx.cs" Inherits="RentPaidOnline.UserControls.PaymentControlsV2.PaymentAmountsAlternate" %>
<div class="rentAndFees">
  
    <div>
   <!--cakel: BUGID00053 - moved to Payments.aspx -->
       <%-- <asp:HyperLink ID="UpdateResidentFeesLink" runat="server" CssClass="generalLink" Visible="False">Update Residents Fees</asp:HyperLink>--%>
       
    </div>

    <asp:MultiView ID="PaymentAmountsMultiView" runat="server">

        <asp:View ID="RentView" runat="server">

            <table cellpadding="0" cellspacing="0" class="table-striped centered-last" width="100%">
                <thead>
                    <tr>
                        <th width="310">Items</th>
                        <!--cakel: BUGID00053 - need to hide when not RPO admin -->
                        <th width="90" id="WaiveFeeTitle" runat="server">Waive Fee</th>
                    </tr>
                </thead>
                <tbody width="100%">
                    <tr>
                        <td>
                            <div class="itemRow">
                                <span class="itemTitle">Rent:</span>
                                <span class="itemAmount">
                                    <asp:Label ID="RentAmountLabel" runat="server" /></span>

                            </div>
                        </td>
                        <td>

                            <span class="feeCheckBox">
                                <asp:CheckBox ID="RentWaiveCheckBox" runat="server" Text="" /></span>

                        </td>
                    </tr>
                    <tr>
                        <asp:Panel ID="PastDuePanel" runat="server" Visible="False">
                            <td>
                                <span class="itemTitle">
                                    <asp:Label ID="PastDueHeaderLabel" runat="server" /></span>
                                <span class="itemAmount">
                                    <asp:Label ID="PastDueAmountLabel" runat="server" /></span>
                            </td>
                            <td>
                                <span class="feeCheckBox">
                                    <asp:CheckBox ID="PastDueWaiveCheckBox" runat="server" Text="" /></span>
                            </td>
                        </asp:Panel>
                    </tr>
                    <tr>
                        <td>
                            <span class="itemTitle">Convenience Fee:</span>
                            <span class="itemAmount">
                                <asp:Label ID="ConvenienceFeeLabel" runat="server" /></span>
                        </td>
                        <td>
                            <span class="feeCheckBox">
                                <asp:CheckBox ID="ConvenienceFeeWaiveCheckBox" runat="server" Text=""/></span>
                        </td>
                    </tr>
					<asp:ListView ID="MonthlyFeesListView" runat="server">
						<ItemTemplate>
							<tr>
								<td>
                                    <div class="itemRow">
									<asp:HiddenField ID="MonthlyFeeIdField" runat="server" Value='<%# Bind("MonthlyFeeId") %>' />
									<span class="itemTitle"><asp:Label ID="MonthlyFeeItemTitleLabel" runat="server" Text='<%# Bind("FeeName") %>' /></span>
									<span class="itemAmount"><asp:Label ID="MonthlyFeeItemAmountLabel" runat="server" Text='<%# Bind("FeeAmountText") %>' /></span>
                                        </div>
								</td>
								<td>
                                    <!--cakel: BugID00053 hide the fee box -->
									<span class="feeCheckBox"><asp:CheckBox ID="MonthlyFeeWaiveCheckBox" runat="server" Text="" Visible="False" /></span>
								</td>
							</tr>
						</ItemTemplate>
					</asp:ListView>
                </tbody>
            </table>
            <br />
            <br />
        </asp:View>


        <asp:View ID="ApplicationFeeView" runat="server">

            <%--Salcedo - 8/30/2014 - multiple changes to duplicate formatting of the RentView (above)--%>

            <table cellpadding="0" cellspacing="0" class="table-striped centered-last" width="100%">
                <thead>
                    <tr>
                        <th width="310">Items</th>
                        <!--cakel: BUGID00053 - need to hide when not RPO admin -->
                        <th width="90" id="Th1" runat="server">Waive Fee</th>
                    </tr>
                </thead>

                <tbody width="100%">
                    <asp:ListView ID="ApplicationFeesListView" runat="server">
                        <ItemTemplate>
                            <tr>
							    <td>
                                    <div class="itemRow">

                                        <asp:HiddenField ID="ApplicationFeeIdField" runat="server" Value='<%# Bind("ApplicationFeeId") %>' />
                  
                                        <span class="itemTitle">
                                            <asp:Label ID="ApplicationFeeFeeNameLabel" runat="server" Text='<%# Bind("FeeName") %>' />
                                        </span>
                                        <span class="itemAmount">
                                            <asp:Label ID="ApplicationFeeFeeAmountLabel" runat="server" Text='<%# Bind("FeeAmountText") %>' />
                                        </span>
                                    </div>
                                </td>        
                                <td>
                                    <span class="feeCheckBox">
                                        <asp:CheckBox ID="ApplicationFeeWaiveCheckBox" runat="server" Text="Waive" Visible="False" />
                                    </span>
                                </td>
						    </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </tbody>
            </table>

            <br />
            <br />
        </asp:View>

    </asp:MultiView>

    <!--Total -->

    <div class="formWhole">
        <div class="well well-sm">
            <div class="formWhole" style="padding-bottom: 0;">
                <asp:Label ID="OverrideDescription" runat="server" AssociatedControlID="OverrideTextBox" Text="Amount Being Paid Today:" />

                <div class="formHalf">
                    <asp:TextBox ID="OverrideTextBox" runat="server" CssClass="form-control"  />
                </div>

                <div class="formHalf">
                    <asp:Button ID="RefreshButton" runat="server" CssClass="btn btn-default" Text="UpdateTotal" />
                </div>
            </div>

            <div class="well-footer">
                <span class="pull-right">Total:
                <asp:Label ID="TotalAmountLabel" CssClass="text-success" runat="server" /></span>
                <div class="clear"></div>
            </div>
        </div>
    </div>


</div>