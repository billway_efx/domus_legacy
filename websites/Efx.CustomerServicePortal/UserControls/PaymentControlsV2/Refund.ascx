﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Refund.ascx.cs" Inherits="RentPaidOnline.UserControls.PaymentControlsV2.Refund" %>
<div class="generalContainer">
    <div class="formRow">
        <div class="formHalfWidth">
            <div class="formLabel bolded"><asp:Label ID="ResidentHeaderLabel" runat="server" Text="Resident: " /></div>
            <asp:Label ID="ResidentNameLabel" runat="server" />
        </div>
        <div class="formHalfWidth">
            <div class="formLabel bolded"><asp:Label ID="TransactionHeaderLabel" runat="server" Text="Transaction ID: " /></div>
            <asp:Label ID="TransactionLabel" runat="server" />
        </div>
    </div>
    
    <div class="formRow">
        <div class="formHalfWidth">
            <div class="formLabel bolded"><asp:Label ID="RefundAmountLabel" runat="server" Text="Amount to Refund: " /></div>
            <asp:TextBox ID="RefundAmountTextBox" runat="server" />
        </div>
        <div class="formHalfWidth">
            <div class="formLabel bolded"><asp:Label ID="ProcessingDateHeaderLabel" runat="server" Text="Original Processing Date: " /></div>
            <asp:Label ID="ProcessingDateLabel" runat="server" />
        </div>
    </div>
    
    <div class="formRow">
        <div class="formHalfWidth">
            <div class="formLabel bolded"><asp:Label ID="RefundDateHeaderLabel" runat="server" Text="Resident: " /></div>
            <asp:Label ID="RefundDateLabel" runat="server" />
        </div>
        <div class="formHalfWidth">
            <div class="formLabel bolded"><asp:Label ID="PaymentMethodHeaderLabel" runat="server" Text="Resident: " /></div>
            <asp:Label ID="PaymentMethodLabel" runat="server" />
        </div>
    </div>
    
    <div class="formRow">
        <div class="formFullWidth">
            <div class="formLabel"><asp:Button ID="SubmitRefundButton" runat="server" Text="Submit Refund" /></div>
        </div>
    </div>
    
    <asp:HiddenField ID="AchProcessingConfirmed" runat="server" Value="0" />
</div>