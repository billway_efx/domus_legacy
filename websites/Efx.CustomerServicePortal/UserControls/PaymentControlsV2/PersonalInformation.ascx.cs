﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;

namespace RentPaidOnline.UserControls.PaymentControlsV2
{
    public partial class PersonalInformation : System.Web.UI.UserControl, IPersonalInformation
    {
        private PersonalInformationPresenter _Presenter;

        public int RenterId { get; set; }
        public string FirstNameText { get { return FirstNameTextBox.Text; } set { FirstNameTextBox.Text = value; } }
        public string MiddleNameText { get { return ""; } set {  } }
        public string LastNameText { get { return LastNameTextBox.Text; } set { LastNameTextBox.Text = value; } }
        public string SuffixText { get { return ""; } set {  } }
        public string EmailAddressText { get { return EmailAddressTextBox.Text; } set { EmailAddressTextBox.Text = value; } }
        public string PhoneNumberText { get { return PhoneNumberTextBox.Text; } set { PhoneNumberTextBox.Text = value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new PersonalInformationPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }

		public void ReInitializeValues()
		{
			_Presenter.InitializeValues();
		}
    }
}