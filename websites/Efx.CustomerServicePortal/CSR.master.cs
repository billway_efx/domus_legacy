﻿using EfxFramework;
using System;
using System.Collections.Generic;

namespace Efx.CustomerServicePortal
{
    public partial class Csr : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (RPO.Helpers.Helper.GetCurrentUserName(true, RPO.Helpers.Helper.IsCsrUser) == "")
                AccountId.Visible = false;

        }
        protected bool IsRpoAdmin
        {
            get
            {
                var roleListAdmin = new List<RoleName> { RoleName.EfxPowerAdministrator };
                return RoleManager.IsUserInRole(EfxFramework.RoleType.EfxAdministrator, RPO.Helpers.Helper.CurrentUserId, roleListAdmin);
            }
        }
    }
}