﻿<%@ page title="RPO Customer Service Portal" language="C#" autoeventwireup="True" codebehind="Default.aspx.cs" masterpagefile="~/CSR.master" inherits="Efx.CustomerServicePortal.CustomerServicePortal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title><%--<ItemTemplate>--%></title>
    <script type="text/javascript">
        function scriptKB() {
            window.open("https://rentpaidonline.rhinosupport.com/helpdesk.htm#knowledgebase");
        }
    </script>

    <script type="text/javascript">
        var launch = false;
        function launchModal() {
            launch = true;
        }

        function pageLoad() {
            if (launch) {
                $find("mpe").show();
            }
        }
    </script>

    <style>
        .modalBackground {
            height: 100%;
            background-color: #EBEBEB;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }

        .modalPanel {
            background-color: white;
        }

        .inline-rb input[type="radio"] {
            width: auto;
            padding-right: 10px;
            padding-left: 5px;
            margin-left: 12px;
        }

        .inline-rb label {
            display: inline;
            padding-left: 5px;
            padding-right: 20px;
        }


        #TextArea1 {
            height: 111px;
            width: 350px;
        }

        #txtareaInfoForTicket {
            height: 121px;
            width: 389px;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="padding-top: 0px; padding-left: 0px;">
        <div class="formWrapper" style="margin-left: 0px; padding-top: 0px;">
            <h2><span>Let's Find Your Resident</span></h2>
            <div class="formHalf" style="margin-bottom: 12px; margin-left: 20px;">
                <div style="margin-top: 12px;">
                    <asp:Label ID="Label1" AssociatedControlID="rblSearchCriteria" runat="server" Text="Select resident search type"/>
                    <asp:RadioButtonList ID="rblSearchCriteria" runat="server"
                        OnSelectedIndexChanged="rblSearchCriteria_SelectedIndexChanged" RepeatDirection="Horizontal" CssClass="inline-rb" BorderStyle="Solid" BorderWidth="1px">
                        <asp:ListItem Selected="True">Email</asp:ListItem>
                        <asp:ListItem>Last Name</asp:ListItem>
                        <asp:ListItem>First Name</asp:ListItem>
                        <asp:ListItem>Phone</asp:ListItem>
                        <asp:ListItem>Property</asp:ListItem>
                        <asp:ListItem>Company</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div style="margin-bottom: 12px; margin-top: 12px;">
                    <asp:Label ID="Label2" AssociatedControlID="tbSearchResident" runat="server" Text="Enter search value"/>
                    <asp:TextBox ID="tbSearchResident" runat="server" Width="350px" CssClass="form-control"/>
                </div>
                <div style="text-align: left;">
                    <asp:Label ID="lblSearchError" runat="server" ValidateRequestMode="Disabled" Visible="False"/>
                    <br />
                    <asp:Button ID="btnSearchResident" runat="server" Text="Search Resident" Width="250px" OnClick="btnSearchResident_Click" CssClass="btn btn-primary" />
                    &nbsp;&nbsp;
                <asp:CheckBox ID="cbExactMatch" runat="server" Text="Exact Match" CssClass="inline-rb" />
                </div>
                <div class="formHalf" style="margin-bottom: 12px; margin-left: 20px; height: 220px; margin-top: 24px;" runat="server" id="NoResidentsSelected">
                    <h2><span>No Resident Is Currently Selected</span></h2>
                </div>
                <div class="formWhole" style="margin-bottom: 6px; margin-left: 20px; margin-top: 12px;" runat="server" id="ResidentSection">
                    <h2><span>Resident Overview<asp:Label ID="lblhiddenproperty" runat="server" Visible="False"/>
                    </span></h2>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblRPropertyName" runat="server" Text="Property Name: " Font-Bold="True"/>
                            </td>
                            <td>
                                <asp:Label ID="lblPropertyNameText" runat="server" Text="&quot;&quot;" Visible="False"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label4" runat="server" Text="Name: " Font-Bold="True"/></td>
                            <td>
                                <asp:Label ID="lblRName" runat="server" Visible="False"/></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label19" runat="server" Text="Phone Number: " Font-Bold="True"/></td>
                            <td>
                                <asp:Label ID="lblRPhoneNumber" runat="server" Visible="False"/></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label5" runat="server" Text="Address: " Font-Bold="True"/></td>
                            <td>
                                <asp:Label ID="lblRAddress" runat="server" Visible="False"/></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label6" runat="server" Text="Unit: " Font-Bold="True"/></td>
                            <td>
                                <asp:Label ID="lblRUnit" runat="server" Visible="False"/></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label7" runat="server" Text="UserName: " Font-Bold="True"/></td>
                            <td>
                                <asp:Label ID="lblRUserName" runat="server" Visible="False"/></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label8" runat="server" Text="Email: " Font-Bold="True"/></td>
                            <td>
                                <asp:Label ID="lblREmail" runat="server" Visible="False"/></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label9" runat="server" Text="Registration ID: " Font-Bold="True"/></td>
                            <td>
                                <asp:Label ID="lblRRPOID" runat="server" Visible="False"/></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label10" runat="server" Text="Total Amount Due:" Font-Bold="True"/>&nbsp;</td>
                            <td>
                                <asp:Label ID="lblRTotalAmountDue" runat="server" Visible="False"/>
                                <asp:Label ID="lblRealRPOIDHidden" runat="server" Visible="False"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label11" runat="server" Text="Accepted Payment Type:" Font-Bold="True"/>&nbsp;</td>
                            <td>
                                <asp:Label ID="lblRStatus" runat="server" Visible="False"/></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label12" runat="server" Text="Lease Start Date : " Font-Bold="True"/></td>
                            <td>
                                <asp:Label ID="lblRLeaseStartDate" runat="server" Visible="False"/></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label13" runat="server" Text="Lease End Date: " Font-Bold="True"/></td>
                            <td>
                                <asp:Label ID="lblRLeaseEndDate" runat="server" Visible="False"/></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label14" runat="server" Text="Auto Pay: " Font-Bold="True"/></td>
                            <td>
                                <asp:Label ID="lblRAutoPay" runat="server" Visible="False"/></td>
                        </tr>

                        <tr>
                            <td>
                                <asp:Label ID="Label17" runat="server" Text="Status: " Font-Bold="True"/></td>
                            <td>
                                <asp:Label ID="lblStatus" runat="server" Visible="False"/></td>
                        </tr>

                        <tr>
                            <td>
                                <asp:Label ID="Label15" runat="server" Text="Pay By Text: " Font-Bold="True"/></td>
                            <td>
                                <asp:Label ID="lblRPayByText" runat="server" Visible="False"/></td>
                        </tr>
                    </table>
                    <br />
                    <div class="formWhole">
                        <asp:Label ID="Label20" runat="server" Text="Enter Support Ticket Information For Selected Resident (Optional)"/><br />
                        <asp:TextBox ID="tbSendTicketEmail" runat="server" Height="79px" Width="746px" TextMode="MultiLine"/><br />
                        <br />
                        <asp:Button ID="btnSendTicketEmail" runat="server" Text="Create New Ticket" Width="191px" OnClick="btnSendTicketEmail_Click" CssClass="btn btn-default " />
                        &nbsp;<asp:Label ID="lblSendTicketConfirmation" runat="server" Visible="False"/>
                    </div>
                </div>
            </div>
            <div style="margin-left: 20px; padding-left: 20px; float: left;">
                <h2><span>Actions</span></h2>
                <br />
                <asp:Label ID="Label3" runat="server" Text="Enter new user name"/><br />
                <asp:TextBox ID="tbUpdateUserName" runat="server" CssClass="form-control-inline" Width="201px"/>
                <asp:Button ID="btnUpdateUserName" runat="server" Text="Update User Name" Width="150px" OnClick="btnUpdateUserName_Click" CssClass="btn btn-default " />
                &nbsp;
                <asp:Label ID="lblUsernameDidUpdate" runat="server" Text="User Name Updated!" Visible="False"/>
                <br />
                <br />
                <asp:Label ID="Label16" runat="server" Text="Enter new user password"/><br />
                <asp:TextBox ID="tbUpdatePassword" runat="server" CssClass="form-control-inline" Width="201px"/>
                <asp:Button ID="btnUpdatePassword" runat="server" Text="Update Password" Width="150px" OnClick="btnUpdatePassword_Click" CssClass="btn btn-default " />
                &nbsp;<asp:Label ID="lblPasswordUpdated" runat="server" Text="Password Updated!" Visible="False"/>
                <br />
                &nbsp;
                <br />
                &nbsp;
                <br />
                <asp:Button ID="btnEmailPW" runat="server" Text="Trigger Password Reset to User's Email" Width="354px" OnClick="btnEmailPW_Click" CssClass="btn btn-default" />
                &nbsp;<asp:Label ID="lblPasswordResetEmailSent" runat="server" Text="Password Reset Email Sent!" Visible="False"/>
                <br />
                &nbsp;
                <br />
                <asp:Button ID="btnPaymentHistory" runat="server" OnClick="btnPaymentHistory_Click" Text="Get Payment History" Width="354px" CssClass="btn btn-default" />
                <div class="formWhole" style="margin-bottom: 12px; margin-left: 70px;">
                    <%--<asp:Button ID="btnTakePayment" runat="server" Text="Take A Payment" Width="174px" OnClick="btnTakePayment_Click" Visible="false" CssClass="btn btn-primary"/>--%>
                    <br />
                    <br />
                    <div style="margin-left: auto; margin-right: auto; width: 100%;">
                        <%--<asp:Button ID="TakeAPayment" runat="server" Text="Take A Payment" Width="250px" OnClientClick="window.open('PaymentV2.aspx', '_blank')" CssClass="btn btn-primary"/>--%>
                        <asp:Button ID="TakeAPayment" runat="server" Text="Take A Payment" Width="250px" CssClass="btn btn-primary" OnClick="TakeAPayment_Click" />
                        <%--<ItemTemplate>--%>
                        <br />
                        <br />
                        <asp:Button ID="btnClearRenterInfo" runat="server" Text="Clear Renter Info" Width="250px" OnClick="btnClearRenterInfo_Click" CssClass="btn btn-primary" />
                        <br />
                        <br />
                        <asp:Button ID="btnKnowledgeBase" runat="server" Text="Knowledge Base" Width="250px"
                            OnClientClick="window.open('https://rentpaidonline.rhinosupport.com/helpdesk.htm#knowledgebase', '_blank')" CssClass="btn btn-primary" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="divOpenner" runat="server"></div>
    <div id="divOpenner2" runat="server"></div>
    <div id="divPaymentHistoryOpener" runat="server"></div>
    <div id="divOpenerPasswordConfirmation" runat="server"></div>
    <asp:Panel ID="ModalPanel" runat="server" Width="80%" CssClass="modalPanel" BorderStyle="Solid" BorderWidth="1px">
        <div style="margin-left: 12px; margin-right: 12px; margin-bottom: 12px; margin-top: 6px;">
            <h2><span>Select a Renter</span></h2>
            <div style="margin-top: 12px;">
                <asp:GridView ID="gvRenters" runat="server" Width="100%"
                    AutoGenerateColumns="False"
                    GridLines="Vertical"
                    PageSize="10" OnPageIndexChanging="gvRenters_PageIndexChanging" AllowSorting="true" OnSorting="gvRenters_Sorting"
                    DataKeyNames="RenterID" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" AllowPaging="True">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <Columns>
                        <asp:BoundField DataField="PrimaryEmailAddress" HeaderText="Email" SortExpression="PrimaryEmailAddress" />
                        <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                        <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                        <asp:BoundField DataField="MainPhoneNumber" HeaderText="Phone Number" SortExpression="MainPhoneNumber" />
                        <asp:BoundField DataField="PropertyName" HeaderText="Property Name" SortExpression="PropertyName" />
                        <asp:BoundField DataField="Company" HeaderText="Company" SortExpression="Company" />
                        <asp:BoundField DataField="StreetAddress" HeaderText="Address" SortExpression="StreetAddress" />
                        <asp:BoundField DataField="Unit" HeaderText="Unit" SortExpression="Unit" />
                        <%-- Patrick Whittingham - 8/20/15 - task #457  --%>
                        <asp:BoundField DataField="IsActive" HeaderText="Active" SortExpression="IsActive" />
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:Button ID="ibtnSelectUser" runat="server" Text="Select"
                                    ToolTip="Select"
                                    OnClick="ibtnSelectUser_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerSettings FirstPageText="First Page" LastPageText="Last Page" Mode="NextPreviousFirstLast" NextPageText="Next Page" PreviousPageText="Previous Page" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" CssClass="cssPager" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                </asp:GridView>
            </div>
            <br />
            <asp:Button ID="OKButton" runat="server" Text="Cancel" CssClass="btn btn-default" />
        </div>
    </asp:Panel>
    <asp:Panel ID="modalPanelPaymentHistory" runat="server" Width="80%" CssClass="modalPanel" BorderStyle="Solid" BorderWidth="1px">
        <div style="margin-left: 12px; margin-right: 12px; margin-bottom: 12px; margin-top: 6px;">
            <h2><span>Renter Payment History</span></h2>
            <asp:GridView ID="gvPaymentHistory" Width="100%"
                runat="server" AlternatingRowStyle-CssClass="gridAltRow"
                HeaderStyle-CssClass="tableHeader" AutoGenerateColumns="False"
                CssClass="responsive payment-history"
                GridLines="Vertical"
                PageSize="20" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" AllowPaging="True">
                <AlternatingRowStyle BackColor="#CCCCCC" CssClass="gridAltRow" />
                <Columns>
                    <asp:BoundField DataField="TransactionDateTime" HeaderText="Date" />
                    <asp:BoundField DataField="TransactionId" HeaderText="Transaction ID" />
                    <asp:BoundField DataField="PaymentAmount" HeaderText="Amount" />
                    <asp:BoundField DataField="Description" HeaderText="Description" />
                    <asp:BoundField DataField="PaymentMethod" HeaderText="Method" />
                    <asp:BoundField DataField="IsAutoPay" HeaderText="AutoPay" />
                    <asp:BoundField DataField="Status" HeaderText="Status" />
                </Columns>
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" CssClass="tableHeader" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#808080" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
            </asp:GridView>
            <asp:Button ID="btnOkayPaymentHistory" runat="server" Text="Close" />
        </div>
    </asp:Panel>
    <asp:Panel ID="ModalPanel2" runat="server" CssClass="modalPanel" Width="90%" Height="85%">
        <iframe style="width: 100%; height: 100%;" id="irm1" src="Empty.aspx" runat="server"/>
        <br />
        <div style="margin-top: 6px; margin-bottom: 12px;">
            <asp:Button ID="OKButton2" runat="server" Text="Close Payment Window" OnClick="OKButton2_Click" />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </div>
    </asp:Panel>

    <asp:Panel ID="PasswordAreYouSure" runat="server" CssClass="modalPanel" Width="350px" Height="140px" BorderStyle="Solid" BorderWidth="1px">
        <div style="margin-left: 12px; margin-right: 12px; margin-bottom: 6px; margin-top: 6px;">
            <h3>Are you sure you want to randomly generate and email a new password to this user?</h3>
            <br />
            <asp:Button ID="PasswordAreYouSureYes" runat="server" Text="Yes" OnClick="PasswordAreYouSureYes_Click" CssClass="btn btn-default " Width="60" />
            <asp:Button ID="PasswordAreYouSureNo" runat="server" Text="No" OnClick="PasswordAreYouSureNo_Click" CssClass="btn btn-default " Width="60" />
        </div>
    </asp:Panel>
    <ajaxtoolkit:modalpopupextender id="mpe" runat="server"
        targetcontrolid="divOpenner" popupcontrolid="ModalPanel" okcontrolid="OKButton" backgroundcssclass="modalBackground" />
    <ajaxtoolkit:modalpopupextender id="mpe2" runat="server"
        targetcontrolid="divOpenner2" popupcontrolid="ModalPanel2" okcontrolid="OKButton2" backgroundcssclass="modalBackground" />
    <ajaxtoolkit:modalpopupextender id="mpePaymentHistory" runat="server"
        targetcontrolid="divPaymentHistoryOpener" popupcontrolid="modalPanelPaymentHistory" okcontrolid="btnOkayPaymentHistory" backgroundcssclass="modalBackground" />
    <ajaxtoolkit:modalpopupextender id="mpePasswordEmailConfirmation" runat="server"
        targetcontrolid="divOpenerPasswordConfirmation" popupcontrolid="PasswordAreYouSure" okcontrolid="PasswordAreYouSureNo" backgroundcssclass="modalBackground" />
    <asp:scriptmanager id="ToolkitScriptManager1" runat="server"></asp:scriptmanager>    
</asp:Content>
