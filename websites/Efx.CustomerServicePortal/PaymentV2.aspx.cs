﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.Interfaces;
using EfxFramework.Interfaces.UserControls.PaymentControlsV2;
using EfxFramework.Presenters;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using RPO;

namespace Efx.CustomerServicePortal
{
    public partial class Payment : BasePage, IPayment
    {
        private PaymentPresenter _Presenter;

        public Page ParentPage { get { return this; } }
        public DropDownList PropertyList { get { return PropertyDropDown; } }
        public CheckBox ApplyToNextMonthCheckBox { get { return uxApplyPaymentToNextMonth; } }
        public EventHandler PayerTypeChanged { set { PayerTypeRadioButtonList.SelectedIndexChanged += value; } }
        public string SelectedPayerType { get { return PayerTypeRadioButtonList.SelectedValue; } }
        public string PayerLabelText { set { PayerLabel.Text = value; } }
        public DropDownList PayerList { get { return PayerDropDown; } }
        public bool IsDifferentPayerChecked { get { return DifferentPayerCheckBox.Checked; } }
        public EventHandler LoadButtonClicked { set { LoadPayerButton.Click += value; } }
        //public EventHandler CancelButtonClicked { set { } }//btnCancel.Click += value; } }
        public EventHandler DifferentPayerChecked { set { DifferentPayerCheckBox.CheckedChanged += value; } }
        public IPersonalInformation PersonalInformationControl { get { return PersonalInformation; } }
        public IResidentBillingInformation ResidentBillingInformationControl { get { return ResidentBillingInformation; } }
        public ISelectPaymentType SelectPaymentTypeControl { get { return SelectPaymentType; } }
        public IPaymentAmountsAlternate PaymentAmountsControl { get { return PaymentAmounts; } }
        public bool AutoPayVisible { set { AutoPayPanel.Visible = value; } }
        public IAutoPayTile AutoPayTileControl { get { return AutoPayTile; } }
        public bool TermsChecked { get { return TermsCheckbox.Checked; } set { TermsCheckbox.Checked = value; } }
        public bool PayNowButtonEnabled { set { PayNowButton.Enabled = value; } }
        public EventHandler PayNowClicked { set { PayNowButton.Click += value; } }
        public HyperLink RedirectApplicantLink { get { return ApplicantLink; } }
        //public bool IsPropertyManagerSite { get { return !EfxFramework.Helpers.Helper.IsRPOAdmin; } }
        public bool IsPropertyManagerSite { get { return false; } }
        public bool IsOverride { get { return PaymentAmounts.OverrideText.Enabled && PaymentAmounts.OverrideText.Text.Replace("$", "").ToNullDecimal().HasValue; } }
        public bool ApplyPaymentToNextMonth { get { return uxApplyPaymentToNextMonth.Checked; } set { uxApplyPaymentToNextMonth.Checked = value; } }
        public int PaymentDay { get { return PaymentProcessingDateComboBox.SelectedValue.ToInt32() ; } set { PaymentProcessingDateComboBox.SelectedValue = value.ToString(); } }
        public bool AddToAutoPay { get { return false; } }

        public int RenterId = 0;
        public string UnmaskedCCNumber { get { return CCNumber; } set { CCNumber = value; } }
        //Salcedo - 2/23/2015 - task # 00321
        public int PropertyId = 0;
        public bool IsCsrUser { get { return EfxFramework.Helpers.Helper.IsCsrUser; } }
        public string CCNumber;
        public string ACHAccountNumber;
        protected void Page_Load(object sender, EventArgs e)        
        {
            _Presenter = new PaymentPresenter(this);

            if (!EfxFramework.Helpers.Helper.HasCsrAccess)
            {
                Response.Redirect("~/Account/Login.aspx");
            }

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();

            InitializeControls();
			SetupPayNowButton();

            //cakel: BUGID000133 - Added "EfxFrameWork to Page to use ActivityLog
            if (!Page.IsPostBack)
            {
                string _user = Page.User.Identity.Name.ToString();
                ActivityLog AL = new ActivityLog();
                AL.WriteLog(_user, "Payments Page", (short)LogPriority.LogAlways);
            }
            //cakel: BUGID00053 hide if applicant is selected
            string PayeeType = PayerTypeRadioButtonList.SelectedItem.ToString();
            if (PayeeType == "Applicant")
            {
                uxApplyPaymentToNextMonth.Visible = false;
                NextMOnthLabel.Visible = false;
            }
            else
            {
                uxApplyPaymentToNextMonth.Visible = true;
                NextMOnthLabel.Visible = true;
            }

            //Salcedo - 2/22/2015 - task # 00321
            Page.LoadComplete += new EventHandler(Page_LoadComplete);
           
        }

        //Salcedo - 2/22/2015 - task # 00321 - add functionality to automatically load property and renter based on query string values
        void Page_LoadComplete(object sender, EventArgs e)
        {
            //Salcedo - 2/24/2015 - task # 00321 - QA testing revealed that we don't need to do this on a postback because everything has already been loaded
            if (!Page.IsPostBack)
            { 
                RenterId = 0;
                string TheParam = Request.QueryString["RenterId"];
                if (TheParam != null)
                {
                    Int32.TryParse(TheParam, out RenterId);
                }

                if (RenterId != 0)
                {
                    var Property = EfxFramework.Property.GetPropertyByRenterId(RenterId);
                    PropertyDropDown.SelectedValue = Property.PropertyId.ToString();
                    _Presenter.PropertyHasBeenSelected();

                    PayerList.SelectedValue = RenterId.ToString();

                    InitializeControls();
                    SetupPayNowButton();

                    _Presenter.PopulateFieldsOnLoadClick();
                }

                PropertyId = 0;
                TheParam = Request.QueryString["PropertyId"];
                if (TheParam != null)
                {
                    Int32.TryParse(TheParam, out PropertyId);
                }
                if (PropertyId != 0)
                {
                    PropertyDropDown.SelectedValue = PropertyId.ToString();
                    _Presenter.PropertyHasBeenSelected();
                    RenterId = 0;

                    InitializeControls();
                    SetupPayNowButton();
                }
            }
        }

        private void InitializeControls()
        {

            var Id = PayerList.SelectedValue.ToNullInt32();
            if (!Id.HasValue || Id.Value < 1)
                return;

            PersonalInformation.RenterId = Id.Value;
            ResidentBillingInformation.RenterId = Id.Value;
            SelectPaymentType.RenterId = Id.Value;
            PaymentAmounts.RenterId = Id.Value;
            PaymentAmounts.PaymentTypes = SelectPaymentType;
            AutoPayTile.RenterId = Id.Value;

            //cakel: BUGID000116
            WarningText();

        }

		private void SetupPayNowButton()
		{

            int selectedPropertyId = 0;

            if (Int32.TryParse(PropertyDropDown.SelectedValue, out selectedPropertyId) && selectedPropertyId > 0)
            if (selectedPropertyId > 0)
            {
                EfxFramework.Property selectedProperty = new EfxFramework.Property(selectedPropertyId);
                if (string.IsNullOrEmpty(selectedProperty.RentalDepositBankAccountNumber) || string.IsNullOrEmpty(selectedProperty.RentalDepositBankRoutingNumber))
                {
                    PayNowButton.Enabled = false;
                    PayNowButton.Visible = false;
                }
                else
                {
                    PayNowButton.Enabled = true;
                    PayNowButton.Visible = true;
                }
            }
            else
            {
                PayNowButton.Enabled = false;
                PayNowButton.Visible = false;
            }
		}

        //cakel: BUGID000133
        protected void TermsCheckbox_CheckedChanged(object sender, EventArgs e)
        {

            string _user = Page.User.Identity.Name.ToString();
            string CheckedStatus = "";
            if (TermsCheckbox.Checked == true)
            {
                CheckedStatus = "Accepted";
            }
            else
            {
                CheckedStatus = "Unaccepted";
            }

            ActivityLog AL = new ActivityLog();
            AL.WriteLog(_user, "Admin Site User Click Terms and Agreements - " + CheckedStatus, (short)LogPriority.LogAlways);
        }


        //cakel: BUGID000116
        public void WarningText()
        {
            string Id = PayerList.SelectedValue.ToString();
            DLAccess.GetRenterDetail _renter = new DLAccess.GetRenterDetail();
            _renter = DLAccess.RenterDetails(Id);
            string CashOnlyText = "";
            if (_renter.AcceptedPaymentTypeId == "3")
            {
                WarningLabeltxt.Visible = true;
                CashOnlyText = "The selected resident is currently on " + @"""Cash Equivalent""" + "status. eCheck payments are not permitted! <br />";
            }
            else
            {
                WarningLabeltxt.Visible = false;
                CashOnlyText = "";
            }

            WarningLabeltxt.Text = CashOnlyText;
        }

        //Salcedo - 2/22/2015 - task # 00321
        //If the user clicks the Load Payer button, we need to redirect to the same page, but specify the RenterId in the query string
        //New logic in the Page_LoadComplete() function will read the query string and populate the controls appropriately
        protected void LoadPayerButton_Click(object sender, EventArgs e)
        {
            var Id = PayerList.SelectedValue.ToNullInt32();
            if (Id.HasValue && Id.Value > 0)
            {
                Uri url = new Uri(Request.Url.AbsoluteUri);
                string path = String.Format("{0}{1}{2}{3}", url.Scheme, Uri.SchemeDelimiter, url.Authority, url.AbsolutePath);
                string newPath = path + "?RenterId=" + Id.ToString();
                Response.Redirect(newPath);
            }
        }

        //Salcedo - 2/22/2015 - task # 00321
        //If the user selects a new property, we need to redirect to the same page, but specify the PropertyId in the query string
        //New logic in the Page_LoadComplete() function will read the query string and populate the controls appropriately
        protected void PropertyDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            var Id = PropertyList.SelectedValue.ToNullInt32();
            if (Id.HasValue && Id.Value > 0)
            {
                Uri url = new Uri(Request.Url.AbsoluteUri);
                string path = String.Format("{0}{1}{2}{3}", url.Scheme, Uri.SchemeDelimiter, url.Authority, url.AbsolutePath);
                string newPath = path + "?PropertyId=" + Id.ToString();
                Response.Redirect(newPath);
            }
        }

        protected void PayNowButton_Click(object sender, EventArgs e)
        {
            var resident = new EfxFramework.Renter(PayerList.SelectedValue.ToInt32());
            if (resident.RenterId < 1)
                return;

            int payerId;

            if (!resident.PayerId.HasValue || resident.PayerId.Value < 1)
                payerId = EfxFramework.Payer.SetPayerFromRenter(resident).PayerId;
            else
                payerId = resident.PayerId.Value;

            if (SelectPaymentType.PaymentMethodDropDown.SelectedValue != "1" ||
                string.IsNullOrEmpty(SelectPaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText))
            {
                if (SelectPaymentType.PaymentMethodDropDown.SelectedValue != "2" ||
                    string.IsNullOrEmpty(SelectPaymentTypeControl.ECheckDetailsControl.AccountNumberText)) return;
                var ach = EfxFramework.PayerAch.GetPrimaryPayerAchByPayerId(payerId);
                if (ach.BankAccountTypeId < 1)
                    return;

                var lastFourAchTextBox =
                    SelectPaymentTypeControl.ECheckDetailsControl.AccountNumberText.Substring(Math.Max(0,
                        SelectPaymentTypeControl.ECheckDetailsControl.AccountNumberText.Length - 4));
                var lastFourAch = ach.BankAccountNumber.Substring(ach.BankAccountNumber.Length - 4);

                //Compare last 4 digits of credit card retrieved from database to last 4 digits of what's in the text field.
                if (lastFourAchTextBox == lastFourAch)
                {
                    ACHAccountNumber = ach.BankAccountNumber;
                    SelectPaymentType.ECheckDetailsControl.HiddenACHAccountNumber = ACHAccountNumber;
                }
                else
                {
                    SelectPaymentType.ECheckDetailsControl.HiddenACHAccountNumber =
                        SelectPaymentTypeControl.ECheckDetailsControl.AccountNumberText;
                }
            }
            else
            {
                var creditCard = EfxFramework.PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(payerId);
                if (creditCard.PayerCreditCardId < 1)
                    return;

                var lastFourCcTextBox =
                    SelectPaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText.Substring(Math.Max(0,
                        SelectPaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText.Length - 4));
                var lastFourCreditCard =
                    creditCard.CreditCardAccountNumber.Substring(creditCard.CreditCardAccountNumber.Length - 4);

                //Compare last 4 digits of credit card retrieved from database to last 4 digits of what's in the text field.
                if (lastFourCcTextBox == lastFourCreditCard)
                {
                    CCNumber = creditCard.CreditCardAccountNumber;
                    SelectPaymentType.CreditCardDetailsControl.HiddenCreditCard = CCNumber;
                }
                else
                {
                    SelectPaymentType.CreditCardDetailsControl.HiddenCreditCard =
                        SelectPaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText;
                }
            }
        }
    }
}