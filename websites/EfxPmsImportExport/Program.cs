﻿using System;
namespace EfxPmsImportExport
{
    internal class Program
    {
        /*
            This console application can Import, Export or Both. The following command line arguments will govern
            which processes are run:
            
            No arguments: Both Import and Export will execute
            
            Import: EfxPmsImportExport.exe /import 
            Export: EfxPmsImportExport.exe /export 
            Help:   EfxPmsImportExport.exe /help
        */

        private static void Main(string[] args)
        {
            //Salcedo - 3/17/2015 - added date/time to log
            Console.WriteLine(string.Concat("\nEFX PMS Import Export application started on ", DateTime.Now.ToShortDateString(), " at ", DateTime.Now.ToShortTimeString(), ".\n"));

            //If there are no parameters run both Import and Export
            if (args == null || args.Length == 0)
            {
                RunImportAndExport();                
            }

            for (var i = 0; i < args.Length; i++)
            {
                var argument = args[i];
                Console.WriteLine($"Argument Found: {argument}");

                if (argument == "/import")
                {
                    RunImport();
                }
                else if (argument == "/export")
                {
                    RunExport();
                }
                else if (argument == "/importsingleproperty")
                {
                    if (i + 1 > args.Length - 1)
                    {
                        Console.WriteLine(@"Missing property id");
                        Help();
                    }
                    else
                    {
                        var property = args[i + 1];
                        RunImportSingleProperty(property);
                        break;
                    }
                }
                else if (argument == "/exportsingleproperty")
                {
                    if (i + 1 > args.Length - 1)
                    {
                        Console.WriteLine(@"Missing property id");
                        Help();
                    }
                    else
                    {
                        var property = args[i + 1];
                        RunExportSingleProperty(property);
                        break;
                    }
                }
                else if (argument == "/help")
                {
                    Help();
                }
                else
                {
                    Console.WriteLine(@"Unknown Argument");
                    Help();
                }
            }

            //Salcedo - 3/17/2015 - added date/time to log
            Console.WriteLine("\nEFX PMS Import Export application completed on " + DateTime.Now.ToShortDateString() + " at " + DateTime.Now.ToShortTimeString() + ".\n");
        }
        private static void RunImport()
        {
            Console.WriteLine(@"Import Start");
            YardiImportExport.ImportYardiRenters();
            Console.WriteLine(@"Import Complete");

            //cakel: BUGID00316
            Console.WriteLine(@"AMSI Import Start");
            EfxFramework.Pms.AMSI.AmsiImportRequest.ProcessAmsiRentersByProperty();
            Console.WriteLine(@"AMSI Import Completed");

            //cakel: TASK 00382 MRI IMPORT
            Console.WriteLine(@"MRI Import Start");
            EfxFramework.Pms.MRI.MriRequest.MRI_ProcessRentersForAllProperties();
            Console.WriteLine(@"MRI Import Completed");

            //cakel: 00351 RealPage Import
            Console.WriteLine(@"RealPage Import Start");
            EfxFramework.Pms.RealPage.RealPageImport.ProcessAllRealPagePropertiesImport();
            Console.WriteLine(@"RealPage Import Completed");
        }

        private static void RunExport()
        {
            Console.WriteLine(@"Export Start");
            YardiImportExport.ExportYardiPayments();
            Console.WriteLine(@"Export Complete");

            //cakel: BUGID00316
            Console.WriteLine(@"AMSI Export Start");
            EfxFramework.Pms.AMSI.AmsiExportRequest.ProcessAmsiPaymentExport();
            Console.WriteLine(@"AMSI Export Completed");

            //cakel: TASK 00382 MRI
            Console.WriteLine(@"MRI Export Started");
            EfxFramework.Pms.MRI.MriExport.ProcessMriPaymentExport();
            Console.WriteLine(@"MRI Export Completed");

            //cakel: 00351 RealPage
            Console.WriteLine(@"MRI Export Started");
            EfxFramework.Pms.RealPage.RealPageExport.RealPageProcessExportAllProperties();
            Console.WriteLine(@"MRI Export Completed");
        }
        private static void RunExportSingleProperty(string PropertyId)
        {
            Console.WriteLine(@"Export Single Property Start");
            YardiImportExport.ExportPaymentsForProperty(Convert.ToInt32(PropertyId));
            Console.WriteLine(@"Export Single Property Complete");

            //cakel: BUGID00316
            Console.WriteLine(@"AMSI Export Single Property Start");
            EfxFramework.Pms.AMSI.AmsiExportRequest.ProcessAmsiPaymentExport(Convert.ToInt32(PropertyId));
            Console.WriteLine(@"AMSI Export Single Property End");

            //cakel: TASK 00382 MRI Single PropertyExport
            Console.WriteLine(@"MRI Export Single Property Started");
            EfxFramework.Pms.MRI.MriExport.ProcessMriPaymentExport(Convert.ToInt32(PropertyId));
            Console.WriteLine(@"MRI Export Single Property Completed");

            //cakel: 00351 RealPage
            Console.WriteLine(@"RealPage Export Single Property Started");
            EfxFramework.Pms.RealPage.RealPageExport.RealPageProcessExportSingleProperty(Convert.ToInt32(PropertyId));
            Console.WriteLine(@"RealPage Export Single Property Completed");
        }

        private static void RunImportSingleProperty(string PropertyId)
        {
            Console.WriteLine(@"Import Single Property Start: " + PropertyId);
            YardiImportExport.ImportSingleProperty(Convert.ToInt32(PropertyId));
            Console.WriteLine(@"Import Single Property Complete");

            //cakel: BUGID00316
            Console.WriteLine(@"AMSI Import Single Property Start");
            EfxFramework.Pms.AMSI.AmsiImportRequest.ProcessSingleAmsiPropertyImport(Convert.ToInt32(PropertyId));
            Console.WriteLine(@"AMSI Import Single Property End");

            //cakel: TASK 00382 MRI IMPORT - rev2
            Console.WriteLine(@"MRI Import Start");
            EfxFramework.Pms.MRI.MriRequest.MRI_ProcessRentersForSingleProperty(Convert.ToInt32(PropertyId));
            Console.WriteLine(@"MRI Import Completed");

            //cakel: 00351 RealPage
            Console.WriteLine(@"RealPage Import Start");
            EfxFramework.Pms.RealPage.RealPageImport.ProcessRealPageSingleProperty(Convert.ToInt32(PropertyId));
            Console.WriteLine(@"RealPage Import Completed");
        }

        private static void RunImportAndExport()
        {
            RunImport();
            RunExport();
        }

        private static void Help()
        {
            Console.WriteLine(@"The following command line arguments will govern which processes are run:");
            Console.WriteLine(@"No arguments: Both import and export will execute for all properties");
            Console.WriteLine(@"Import only for all properties: EfxPmsImportExport.exe /import");
            Console.WriteLine(@"Export only for all properties: EfxPmsImportExport.exe /export");
            Console.WriteLine(@"Import only for a single Property: EfxPmsImportExport.exe /importsingleproperty <PropertyId>");
            Console.WriteLine(@"Export only for a single Property: EfxPmsImportExport.exe /exportsingleproperty <PropertyId>");
            Console.WriteLine(@"Help:   EfxPmsImportExport.exe /help ");
        }
    }
}
