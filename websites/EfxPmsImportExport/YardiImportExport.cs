﻿using EfxFramework;
using EfxFramework.Pms.Yardi;

namespace EfxPmsImportExport
{
    public static class YardiImportExport
    {
        public static void ImportYardiRenters()
        {
            ImportRequest.ImportRenters();

        }

        public static void ExportYardiPayments()
        {
            if (!EfxSettings.EnableExports) 
                return;

            if (EfxSettings.UseYardiBatching)
            {
                ExportRequestV2.ExportPayments();
                //cakel: 00546
                ExportRequestV2.ExportAllPropertiesApplicationPayments();
            }
            else
            {
                ExportRequest.ExportPayments();
            }
                
        }

        public static void ImportSingleProperty(int PropertyId)
        {
            ImportRequest.ImportSingleProperty(PropertyId);
        }

        public static void ExportPaymentsForProperty(int PropertyId)
        {
            if (!EfxSettings.EnableExports)
                return;

            if (EfxSettings.UseYardiBatching)
                ExportRequestV2.ExportPaymentsForProperty(PropertyId);
        }
    }
}
