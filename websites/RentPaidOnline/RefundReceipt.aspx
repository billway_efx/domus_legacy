﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RefundReceipt.aspx.cs" Inherits="Domus.RefundReceipt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="pageContainer">
        <div class="pageTitle"><h2>Payments</h2></div>
        <!--Main Tabs Section -->
        <div class="singleItemSection">
            <div class="slimPadding">
                <div class="receiptHolder">
                    <header>
                        <h3>Payment Status</h3>
                        <p>Your Transaction was Processed</p>
                        <h4>Thank You</h4>
                        <p>Refund Request Accepted</p>
                    </header>
                    <div class="receipt">
                        <div class="recieptColumn">
                            
                            <!-- **First Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Transaction ID**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Transaction ID</span>
                                    <p><asp:Label ID="TransactionIdLabel" runat="server"/></p>
                                </div>
                            </div>

                            <!-- **Second Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Rent**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Refund Amount</span>
                                    <p><asp:Label ID="RefundAmountLabel" runat="server"/></p>
                                </div>                            
                            </div>

                            <!-- **Fifth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Property**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Property</span>
                                    <p><asp:Label ID="PropertyNameLabel" runat="server" /></p>
                                    <p><asp:Label ID="PropertyAddressLabel" runat="server" /></p>
                                </div>                                 
                            </div>
                            
                            <!-- **Sixth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**RPO Support**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">RPO Support</span>
                                    <p>Support Line: <asp:Label ID="supportPhone" runat="server" /></p>
                                    <p>Email: <asp:Label ID="supportEmail" runat="server" /></p>
                                </div>                                 
                            </div>

                        </div>
                        
                        <div class="recieptColumn">
                            
                            <!-- **First Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**First Name **-->
                                <div class="formFullWidth">
                                    <span class="formLabel">First Name</span>
                                    <p><asp:Label ID="FirstNameLabel" runat="server" /></p>
                                </div>
                            </div>
                            
                            <!-- **Second Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Middle Name **-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Middle Name</span>
                                    <p><asp:Label ID="MiddleNameLabel" runat="server" /></p>
                                </div>
                            </div>
                            
                            <!-- **Third Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Last Name**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Last Name</span>
                                    <p><asp:Label ID="LastNameLabel" runat="server" /></p>
                                </div>
                            </div>
                            
                            <!-- **Fourth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                 <!--**Suffix**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Suffix</span>
                                    <p><asp:Label ID="SuffixLabel" runat="server" /></p>
                                </div>
                            </div>
                            
                            <!-- **Fifth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                 <!--**Billing Address**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Billing Address</span>
                                    <p><asp:Label ID="BillingAddressLabel" runat="server" /></p>
                                    <p><asp:Label ID="BillingCityStateZipLabel" runat="server" /></p>
                                </div>
                            </div>
                            
                            <!-- **Sixth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                 <!--**Phone Number**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Phone Number</span>
                                    <p><asp:Label ID="PhoneNumberLabel" runat="server" /></p>
                                </div>
                            </div>
                            
                            <!-- **Seventh Row**-->
                            <div class="formRow rowSpacingAdjust">
                                 <!--**Payment Method**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Payment Method</span>
                                    <p><asp:Label ID="PaymentMethodLabel" runat="server" /></p>
                                </div>
                            </div>
                           
                        </div>

                     </div>
                 </div>
                <div class="printButton">
                    <div class="formFullWidth">
                        <asp:LinkButton ID="PrintButton" runat="server" CssClass="orangeButton" OnClientClick="window.print(); return false;">Print</asp:LinkButton>
                    </div>
                </div>
                <div class="clearFix"></div>
            </div>                            
        </div>
    </div>
</asp:Content>
