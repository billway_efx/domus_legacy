﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteNavigation.ascx.cs" Inherits="Domus.UserControls.SiteNavigation" %>
<asp:PlaceHolder runat="server" ID="RentersSiteNavigation" Visible="false">
    <a href="/Home"><h1><span>Rent Paid Online</span></h1></a>
	<ul id="Navigation">
        <li><img src="/Images/icon_home.png" alt="Home" /><a href="/RentPaid/Renters/" class="first">Residents Home</a></li>
        <li><span class="pipe">|</span></li>
		<li><a href="/Manager">Managers</a></li>
        <li><span class="pipe">|</span></li>
		<li><a href="/Renter">Residents</a></li>
        <li><span class="pipe">|</span></li>
		<li><a href="/Renter/RentersRoom">Resident's Room</a></li>
        <li><span class="pipe">|</span></li>
		<li><a href="/Pro/ProsCorner">Pro's Corner</a></li>
        <li><span class="pipe">|</span></li>
		<li><a href="/Home/About">About</a></li>
        <li><span class="pipe">|</span></li>
		<li><a href="/Home/Contact">Contact</a></li>
	</ul>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="PropertySiteNavigation" Visible="false">
    <a href="/Home"><h1><span>Rent Paid Online</span></h1></a>
	<ul id="Navigation">
        <li><img src="/Images/icon_home.png" alt="Home" /><a href="/RentPaid/Property/" class="first">Property Manager Home</a></li>
        <li><span class="pipe">|</span></li>
		<li><a href="/RentPaid/Property/Properties">Properties</a></li>
        <li><span class="pipe">|</span></li>
		<li><a href="/RentPaid/Property/Renters">Residents</a></li>
        <li><span class="pipe">|</span></li>
		<li><a href="/RentPaid/Property/Payment">Take a Payment</a></li>
        <li><span class="pipe">|</span></li>
		<li><a href="/RentPaid/Property/Marketing">eMail Marketing</a></li>
        <li><span class="pipe">|</span></li>
		<li><a href="/RentPaid/Property/Reports">Reports</a></li>
        <li><span class="pipe">|</span></li>
		<li><a href="/RentPaid/Property/ProsCorner">Pros Corner</a></li>
        <li><span class="pipe">|</span></li>
		<li><a href="/RentPaid/Property/MyAccount">My Account</a></li>
	</ul>
</asp:PlaceHolder>