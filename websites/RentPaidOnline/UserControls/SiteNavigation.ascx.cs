﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework;

namespace Domus.UserControls
{
    public partial class SiteNavigation : System.Web.UI.UserControl
    {
        public SiteNavigationGroup NavigationGroup
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            PropertySiteNavigation.Visible = this.NavigationGroup.Equals(SiteNavigationGroup.Property);
            RentersSiteNavigation.Visible = this.NavigationGroup.Equals(SiteNavigationGroup.Renters);

            base.OnPreRender(e);
        }
    }
}