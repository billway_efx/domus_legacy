﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AmsiAddEdit.ascx.cs" Inherits="Domus.UserControls.AmsiAddEdit" %>

<%-- cakel: BUGID00316 - AMSI Integration --%>
<div class="formWhole">
    <div class="formThird">
        <label>AMSI UserID</label>
        <asp:TextBox ID="AMSIUsernameText" runat="server" CssClass="form-control" />
    </div>
    <div class="formThird">
        <label>AMSI Password</label>
        <asp:TextBox ID="AMSIPasswordText" runat="server" CssClass="form-control" />
    </div>

</div>


<div class="formWhole">

    <div class="formThird">
        <label>AMSI Portfolio Name</label>
        <asp:TextBox ID="AmsiDatabaseNameText" runat="server" CssClass="form-control" />
    </div>

    <div class="formThird">
    <label>AMSI Property ID</label>
    <asp:TextBox ID="AmsiPropertyIdText" runat="server" CssClass="form-control" />
</div>



</div>
<!-- cakel: 00551 -->
<div class="formWhole">
    <div class="formThird">
        <label>Client Merchant ID - Credit</label>
    <asp:TextBox ID="AmsiClientMerchantIDText" runat="server" CssClass="form-control" ></asp:TextBox>
    </div>

    <div class="formThird">
        <label>Client Merchant ID - Ach</label>
    <asp:TextBox ID="AmsiAchClientMerchantIDText" runat="server" CssClass="form-control" ></asp:TextBox>
        </div>

    <div class="formThird">
        <label>Client Merchant ID - Cash</label>
    <asp:TextBox ID="AmsiCashClientMerchantIDText" runat="server" CssClass="form-control" ></asp:TextBox>
        </div>

</div>

<div class="formWhole">

        <label>AMSI Import End Point</label>
        <asp:TextBox ID="AmsiImportEndPointText" runat="server" CssClass="form-control" ></asp:TextBox>

</div>
<div class="formWhole">
     <label>Amsi Export End Point</label>
        <asp:TextBox ID="AmsiExportEndPointText" runat="server" CssClass="form-control" ></asp:TextBox>
</div>

