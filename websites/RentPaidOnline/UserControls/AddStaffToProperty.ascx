﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddStaffToProperty.ascx.cs" Inherits="Domus.UserControls.AddStaffToProperty" %>
<%@ Import Namespace="EfxFramework.ExtensionMethods" %>
<asp:UpdatePanel ID="StaffRepeaterUpdatePanel" runat="server">
    <ContentTemplate>
        <div class="formTint">
            <div class="formWrapper">
                <asp:ValidationSummary ID="AddStaffToPropertySummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="AddStaffToProperty" />
                <asp:PlaceHolder ID="successMsg" runat="server" Visible="false">
                    <div class="alert alert-success" id="Div1"><b>Success!</b></div>
                </asp:PlaceHolder>
                <h2>Staff</h2>
                <div class="formWhole">
                    <div class="formTwoThirds">
                        <div class="formHalf">
                            <asp:TextBox CssClass="form-control" ID="SearchTextbox" runat="server"></asp:TextBox>
                        </div>
                        <div class="formHalf">
                            <asp:Button ID="SearchButton" runat="server" CssClass="btn btn-default" Text="Search" OnClick="SearchButtonClick" Style="margin-right: 5px;" />
                            <asp:Button ID="ClearButton" runat="server" CssClass="btn btn-default" Text="Clear" OnClick="ClearButtonClick" Style="margin-right: 5px;" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <!-- Begin Staff Repeater -->
        <asp:Repeater ID="StaffRepeater" runat="server" OnItemDataBound="StaffRepeater_ItemDataBound">
            <HeaderTemplate>
                <h2>Current Staff</h2>
                <table id="MainContent_ResidentNotesControl_NotesGrid" class="table-striped centered-last" style="width: 100%;">
                    <tbody>
                        <tr>
                            <th scope="col" width="35%">Name</th>
                            <th scope="col" width="20%">Mobile</th>
                            <th scope="col" width="35%">Email</th>
                            <th scope="col" width="10%">Manage</th>
                        </tr>
            </HeaderTemplate>
            <FooterTemplate>
                </tbody>
                    </table>
            </FooterTemplate>

            <ItemTemplate>
                <tr>
                    <td>
                        <%# Eval("DisplayName") %>
                    </td>
                    <td>
                        <%# Eval("MobilePhoneNumber").ToString().FormatPhoneNumber() %>
                    </td>
                    <td>
                        <%# Eval("PrimaryEmailAddress") %>
                    </td>
                    <td>
                        <asp:LinkButton ID="EditStaffButton" CssClass="secondary-link" runat="server" CommandArgument='<%# Eval("PropertyStaffId") %>' OnCommand="EditStaffButtonCommand"><span data-icon="&#x24" aria-hidden="true"></span></asp:LinkButton>
                        <asp:LinkButton ID="RemoveStaffButton" CssClass="secondary-link" runat="server" OnClientClick="return confirm('Are you sure you want to delete this staff account?')" CommandArgument='<%# Eval("PropertyStaffId") %>' OnCommand="RemoveStaffButtonCommand">&nbsp;<span data-icon="&#x2b" aria-hidden="true"></span></asp:LinkButton>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        <asp:Panel ID="EmptyStaffRecords" runat="server" Visible="false" EnableViewState="false">
            <h4>No current staff members.</h4>
        </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>

<div id="addStaffTopBar">
    <asp:Button ID="DoneAddingPropertyButton" CssClass="green-button float-right margin-right" runat="server" OnClick="DoneAddingPropertyButtonClick" Text="Done Adding Property" />
</div>
<!--Is this needed-->

<div id="AddStaffWrap">
    <h2>
        <asp:Literal ID="AddEditStaffTitle" runat="server">Add Staff</asp:Literal></h2>
    <asp:UpdatePanel ID="UpdateStaff" runat="server">
        <ContentTemplate>
            <uc1:AddEditPropertyStaff ID="AddEditPropertyStaff" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
