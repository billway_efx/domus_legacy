﻿using EfxFramework.Interfaces.UserControls;
using System;
using System.Collections.Generic;

namespace Domus.UserControls
{
    public partial class DetailTile : System.Web.UI.UserControl, IDetailTile
    {
        private EfxFramework.Presenters.UserControls.DetailTile _presenter;
        private List<string> _dataSource;

        public string HeaderText { set { DetailTileHeaderLabel.Text = value; } }
        public string MainDetailHtml { get { return DetailLiteral.Text; } set { DetailLiteral.Text = value; } }
        public List<string> DataSource
        {
            get { return _dataSource ?? (_dataSource = new List<string>()); }
            set 
            { 
                _dataSource = value;

                if (_presenter != null) 
                    _presenter.DataBind();
            }
        }

        public string DetailsLinkUrl
        {
            set
            {
                DetailsLink.NavigateUrl = value;
                DetailsLinkText.NavigateUrl = value;
            }
        }

        public string AdditionalLinksHtml
        {
            set
            {
                AdditionalLinksLiteral.Text = value;
                AdditionalLinksLiteral.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter = new EfxFramework.Presenters.UserControls.DetailTile(this);
            _presenter.InitializeValues();
        }
    }
}