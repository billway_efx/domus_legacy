﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using EfxFramework.ViewPresentation.Interfaces.UserControls;


namespace Domus.UserControls.PaymentControlsV2
{
    public partial class ResidentWallet : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {  
            if (!IsPostBack)
            {
                  RenterId.Value = HttpContext.Current.Request.QueryString["renterId"].ToString();
            }
        }

        protected void CancelAutoPay_Click(object sender, EventArgs e)
        {
            UpdateAutoPaymentArea.Visible = false;
            //SWitherspoon Task 00737: Changed to use new gridview
            WalletGridTest.SelectedIndex = -1;
        }

        protected void SubmitAutoPay_Click(object sender, EventArgs e)
        {
            int PaymentTypeID = 0;
            string RenterIDstr = Request.QueryString["renterid"];

            int PayDayOfMonth = int.Parse(DayofMonthDDL.SelectedValue);

            int RenterID = int.Parse(RenterIDstr);

            //SWitherspoon Task 00737: Changed to use new gridview
            String PaymentMethod = WalletGridTest.SelectedDataKey[1].ToString();
            int _methodID = int.Parse(WalletGridTest.SelectedDataKey[2].ToString());
            int _payerID = int.Parse(WalletGridTest.SelectedDataKey[0].ToString());


            //SWitherspoon Task 00737: Changed IF to use String values instead of boolean to match Stored Procedure results
            if (PaymentMethod == "E-Check")
            {
                PaymentTypeID = 2;
            }
            if (PaymentMethod == "Credit Card")
            {
                PaymentTypeID = 1;
            }

            int PayerID = _payerID;

            try
            {
                DLAccess.AutoPayment_InsertUpdate(RenterID, PayerID, PaymentTypeID, _methodID, PayDayOfMonth);
                UpdateAutoPaymentArea.Visible = false;

                //SWitherspoon Task 00737: Changed to use new gridview
                WalletGridTest.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        protected void AddNewCCbtn_Click(object sender, EventArgs e)
        {
            ClearAddUpdateCreditCard();
            ClearAddUpdateACH();
            AddnewWalletItemSection.Visible = true;
            //SWitherspoon Task 00737: Changed to use new gridview
            WalletGridTest.SelectedIndex = -1;
            EcheckADD.Visible = false;
            CreditCardAdd.Visible = true;
            AddNewCCandPaybtn.Text = "Submit New Credit Card";
            CardNumlbl.Text = "Card Number";
        }

        protected void AddNewEcheckbtn_Click(object sender, EventArgs e)
        {
            ClearAddUpdateACH();
            ClearAddUpdateCreditCard();
            AddnewWalletItemSection.Visible = true;
            //SWitherspoon Task 00737: Changed to use new gridview
            WalletGridTest.SelectedIndex = -1;
            EcheckADD.Visible = true;
            CreditCardAdd.Visible = false;
            AddNewEcheckandPaybtn.Text = "Submit New Bank Account";
            AccountNumlbl.Text = "Account Number";
        }

        protected void AddEcheckCancel_Click(object sender, EventArgs e)
        {
            //SWitherspoon Task 00737: Changed to use new gridview
            WalletGridTest.SelectedIndex = -1;
            AddnewWalletItemSection.Visible = false;
            CreditCardAdd.Visible = false;
            EcheckADD.Visible = false;
            ClearAddUpdateACH();
        }

        protected void AddccCancel_Click(object sender, EventArgs e)
        {
            //SWitherspoon Task 00737: Changed to use new gridview
            WalletGridTest.SelectedIndex = -1;
            AddnewWalletItemSection.Visible = false;
            CreditCardAdd.Visible = false;
            EcheckADD.Visible = false;
            ClearAddUpdateCreditCard();
        }

        protected void AddNewEcheckandPaybtn_Click(object sender, EventArgs e)
        {
            //Checks to see if the ACH or Credit Card already exist.
            if(!CheckIfPaymentExist())
            {
                return;
            }

            //Checks to see if the resident's wallet already contains 30 wallet items.
            if(!GetRenterWalletCount())
            {
                return;
            }

            string AccountNumber = NewAccountnumbertxt.Text;
            string RoutingNumber = NewRoutingNumbertxt.Text;
            int AccountTypeID = BankAccountTypeDDL.SelectedIndex + 1; //1 for Checking Account
            int RenterID = int.Parse(Request.QueryString["renterid"]);

            //SWitherspoon Task 00737: Used to see when they are Updating pay methods
            bool UpdateACH = AddNewEcheckandPaybtn.Text.Contains("Update");

            //Edit Wallet item
            if (UpdateACH)
            {
                int MethodId = int.Parse(MethodID.Value);

                DLAccess.wallet_UpdateACHPaymentMethodForRenter(RenterID, MethodId, AccountTypeID, AccountNumber, RoutingNumber);

                //SWitherspoon Task 00737: Changed to use new gridview
                WalletGridTest.DataBind();
                ClearAddUpdateACH();
            }
            //Add New Wallet Item
            else if (!UpdateACH)
            {
                DLAccess.wallet_AddACHPaymentMethodForRenter(RenterID, AccountTypeID, AccountNumber, RoutingNumber);

                //SWitherspoon Task 00737: Changed to use new gridview
                WalletGridTest.DataBind();
                ClearAddUpdateACH();
            }

        }

        protected void AddNewCCandPaybtn_Click(object sender, EventArgs e)
        {
            //Checks to see if the ACH or Credit Card already exist.
            if(!CheckIfPaymentExist())
            {
                return;
            }

            //Checks to see if the resident's wallet already contains 30 wallet items.
            if(!GetRenterWalletCount())
            {
                return;
            }

            string FullName = FullNametxt.Text;
            string AccountNumber = CardAccountnumtxt.Text;
            int ExpMonth = int.Parse(ExpMonthDDL.SelectedValue);
            int ExpYear = int.Parse(ExpYearDDL.SelectedValue);
            int CSC = int.Parse(CVVtxt.Text);
            int RenterID = int.Parse(Request.QueryString["renterid"]);

            //SWitherspoon Task 00737: Used to see when they are Updating pay methods
            bool UpdateCC = AddNewCCandPaybtn.Text.Contains("Update");

            //Edit Wallet item
            if (UpdateCC)
            {
                int MethodId = int.Parse(MethodID.Value);

                DLAccess.wallet_UpdateCCPaymentMethodForRenter(RenterID, MethodId, FullName, AccountNumber, ExpMonth, ExpYear);

                //SWitherspoon Task 00737: Changed to use new gridview
                WalletGridTest.DataBind();
                ClearAddUpdateCreditCard();
            }
            //Add New Wallet Item
            else if (!UpdateCC)
            {
                DLAccess.wallet_AddCCPaymentMethodForRenter(RenterID, FullName, AccountNumber, ExpMonth, ExpYear, CSC);

                //SWitherspoon Task 00737: Changed to use new gridview
                WalletGridTest.DataBind();
                ClearAddUpdateCreditCard();

            }
        }

        private void ClearAddUpdateACH()
        {
            AddnewWalletItemSection.Visible = false;
            NewRoutingNumbertxt.Text = string.Empty;
            NewAccountnumbertxt.Text = string.Empty;
            //SWitherspoon Task 00737: Changed to use new gridview
            WalletGridTest.SelectedIndex = -1;
            AddNewCCbtn.Visible = true;
            AddNewEcheckbtn.Visible = true;
        }

        private void ClearAddUpdateCreditCard()
        {
            FullNametxt.Text = string.Empty;
            CardAccountnumtxt.Text = string.Empty;
            CVVtxt.Text = string.Empty;
            ExpMonthDDL.SelectedIndex = 0;
            ExpYearDDL.SelectedIndex = 0;
            AddnewWalletItemSection.Visible = false;
            //SWitherspoon Task 00737: Changed to use new gridview
            WalletGridTest.SelectedIndex = -1;
            AddNewCCbtn.Visible = true;
            AddNewEcheckbtn.Visible = true;
        }

        public bool CheckIfPaymentExist()
        {           
            string AccountNumber = CardAccountnumtxt.Text;
            string ExpMonth = ExpMonthDDL.SelectedValue;
            string ExpYear = ExpYearDDL.SelectedValue;
            string ACHAccountNumber = NewAccountnumbertxt.Text;
            string ACHRoutingNumber = NewRoutingNumbertxt.Text;
            int CreditCardCount = 0;
            int ACHCount = 0;
            string RenterID = HttpContext.Current.Request.QueryString["renterId"].ToString();

            try 
            {

                if(AccountNumber!=null && AccountNumber!="")
                {
                    CreditCardCount = EfxFramework.PayerCreditCard.CheckIfRenterCCExist(RenterID, AccountNumber, ExpMonth, ExpYear);

                    if (CreditCardCount > 0)
                    {
                        CustomValidator err = new CustomValidator();
                        err.ValidationGroup = "Payment";
                        err.IsValid = false;
                        err.ErrorMessage = String.Format("Credit card number {0} with the expiration date of {1}/{2} already exist.", AccountNumber, ExpMonth, ExpYear);
                        Page.Validators.Add(err);
                        return false;
                    }

                    else
                    {
                        return true;
                    }
                }
            
                else if (ACHAccountNumber!=null && ACHAccountNumber!="")
                {
                    ACHCount = EfxFramework.PayerAch.CheckIfRenterACHExist(RenterID, ACHAccountNumber, ACHRoutingNumber);

                    if (ACHCount>0)
                    {
                        CustomValidator err = new CustomValidator();
				        err.ValidationGroup = "Payment";
				        err.IsValid = false;
                        err.ErrorMessage = String.Format("ACH account number {0} with the routing number of {1} already exist.", ACHAccountNumber, ACHRoutingNumber);
                        Page.Validators.Add(err);
                        return false;
                    }

                    else
                    {
                        return true;
                    }
                } 

                return false;
            }

            catch(Exception ex)
            {
                return false;
            }
        }

        public bool GetRenterWalletCount()
        {
            string RenterID = HttpContext.Current.Request.QueryString["renterId"].ToString();
            int WalletCount = EfxFramework.Payer.GetWalletCount(RenterID);

            if (WalletCount>=30)
            {
                CustomValidator err = new CustomValidator();
				err.ValidationGroup = "Payment";
				err.IsValid = false;
                err.ErrorMessage = String.Format("The user has already reached their limit for ACH and Credit Cards in your wallet.  The limit is 30.");
                Page.Validators.Add(err);
                return false;
            }

            else
            {
                return true;
            }

        }

        protected void MakePrimarybtn_Click(object sender, EventArgs e)
        {
            
        }


        //SWitherspoon Task 00737: Copy of original gridview function altered to match new gridview
        protected void WalletGrid_RowDataBound_test(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label PayTypeLabel = (Label)e.Row.FindControl("PayTypelbl") as Label;
                Label ExpirationDateLabel = (Label)e.Row.FindControl("ExpDatelbl") as Label;
                Label CreditCardNumLabel = (Label)e.Row.FindControl("CCNum") as Label;
                Label IsPrimarylbl = (Label)e.Row.FindControl("IsPrimarylbl") as Label;
                Label IsAutoPaylbl = (Label)e.Row.FindControl("IsAutoPaylbl") as Label;
                Label AutoPaylbl = (Label)e.Row.FindControl("AutoPaylbl") as Label;

                Button AutoPaySet = (Button)e.Row.FindControl("AutoPaySetbtn") as Button;
                Button AutoPayCancel = (Button)e.Row.FindControl("CancelAutoPaybtn") as Button;
                Button SetPrimaryPayment = (Button)e.Row.FindControl("MakePrimarybtn") as Button;
                Button UpdatePayMethod = (Button)e.Row.FindControl("UpdatePayMethodbtn") as Button;
                Button DeletePayMethod = (Button)e.Row.FindControl("DeletePayMethodbtn") as Button;


                string IsPrimary = IsPrimarylbl.Text;
                string AutoLabel = IsAutoPaylbl.Text;

                if (AutoLabel == "1")
                {
                    AutoPaySet.Enabled = false;
                    AutoPaySet.Visible = false;
                    AutoPayCancel.Visible = true;
                    AutoPayCancel.Enabled = !EfxFramework.Helpers.Helper.IsCsrUser;
                    AutoPaylbl.Text = "Item currently used for autopayments";
                    DeletePayMethod.Visible = false;

                    e.Row.ForeColor = System.Drawing.Color.White;
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#662E6B");
                }
                else
                {
                    AutoPayCancel.Enabled = !EfxFramework.Helpers.Helper.IsCsrUser;
                    AutoPayCancel.Visible = false;
                }

                if (PayTypeLabel.Text == "Credit Card")
                {
                    SetPrimaryPayment.Text = "Make Primary Credit Card";
                    ExpirationDateLabel.Text = "Exp " + ExpirationDateLabel.Text;
                    UpdatePayMethod.Text = "Update Credit Card";
                    DeletePayMethod.Text = "Delete Credit Card";
                }

                else if (PayTypeLabel.Text == "E-Check")
                {
                    SetPrimaryPayment.Text = "Make Primary ACH";
                    ExpirationDateLabel.Text = "";
                    UpdatePayMethod.Text = "Update Bank Account";
                    DeletePayMethod.Text = "Delete Bank Account";
                }

                if (IsPrimary == "True")
                {
                    SetPrimaryPayment.Visible = false;
                    if (PayTypeLabel.Text == "Credit Card")
                    {
                        PayTypeLabel.Text = PayTypeLabel.Text + " (Is Primary Credit Card)";
                    }

                    else if (PayTypeLabel.Text == "E-Check")
                    {
                        PayTypeLabel.Text = PayTypeLabel.Text + " (Is Primary E-Check)";
                    }

                    PayTypeLabel.Font.Bold = true;
                }

            }
        }

        //SWitherspoon Task 00737: Copy of original gridview function altered to match new gridview
        protected void WalletGrid_RowCommand_test(object sender, GridViewCommandEventArgs e)
        {
            string RenterIDstr = Request.QueryString["renterid"];

            string[] arg = e.CommandArgument.ToString().Split(';');

            string _payerID = "";
            string CommandType = "";
            string _method = "";
            string _methodID = "";

            CommandType = arg[0];
            _method = arg[1];
            _payerID = arg[2];
            _methodID = arg[3]; //Payer%_%Id of the chosen payment type (CC/ECheck)


            if (CommandType == "SetAutoPayment")
            {
                UpdateAutoPaymentArea.Visible = true;
            }

            else if (CommandType == "CancelAutoPayment")
            {
                int RenterID = int.Parse(RenterIDstr);
                DLAccess.AutoPay_Cancel(RenterID);
                WalletGridTest.DataBind();
            }

            else if (CommandType == "MakePrimaryPayment")
            {
                int PaymentID = int.Parse(_methodID);
                int RenterID = int.Parse(RenterIDstr);

                if (_method == "Credit Card")
                {
                    EfxFramework.PayerCreditCard.MakePrimaryCreditCard(PaymentID);
                }

                else
                {
                    EfxFramework.PayerAch.MakePrimaryACH(PaymentID);
                }
                WalletGridTest.DataBind();
            }

            else if (CommandType == "UpdatePayMethod")
            {
                ClearAddUpdateACH();
                ClearAddUpdateCreditCard();
                AddNewCCbtn.Visible = false;
                AddNewEcheckbtn.Visible = false;

                if (_method == "Credit Card")
                {
                    CreditCardAdd.Visible = true;
                    EcheckADD.Visible = false;
                    AddNewCCandPaybtn.Text = "Update Credit Card";
                    CardNumlbl.Text = "New Card Number";
                }
                else
                {
                    CreditCardAdd.Visible = false;
                    EcheckADD.Visible = true;
                    AddNewEcheckandPaybtn.Text = "Update Bank Account";
                    AccountNumlbl.Text = "New Account Number";
                }

                MethodID.Value = _methodID;
                AddnewWalletItemSection.Visible = true;

            }

            else if (CommandType == "DeletePayMethod")
            {
                int PaymentID = int.Parse(_methodID);

                if (_method == "Credit Card")
                { DLAccess.wallet_RemovePaymentMethodForRenter(PaymentID, "CC"); }
                else
                { DLAccess.wallet_RemovePaymentMethodForRenter(PaymentID, "E-Check"); }

                WalletGridTest.DataBind();

            }

        }




    }
}