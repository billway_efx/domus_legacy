﻿using System.Web;
using EfxFramework.Interfaces.UserControls.PaymentControlsV2;
using EfxFramework.Presenters.UserControls.PaymentControlsV2;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;
using System;
using System.Web.UI;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace Domus.UserControls.PaymentControlsV2.PaymentInfo
{
    public partial class PaymentInfoTab : System.Web.UI.UserControl, IPaymentInfoTab
    {
        private PaymentInfoTabPresenter _Presenter;

        public Page ParentPage { get { return Page; } }
		public PlaceHolder PaymentSuccessPlaceHolder { get { return paymentInfoSuccess; } }
        public IPersonalInformation PersonalInformationControl { get { return PersonalInformation; } }
        public ISelectPaymentType SelectPaymentTypeControl { get { return SelectPaymentType; } }
        public IResidentBillingInformation ResidentBillingInformationControl { get { return ResidentBillingInformation; } }
        public IPaymentAmounts PaymentAmountsControl { get { return PaymentAmounts; } }
        public IAutoPayTile AutoPayTileControl { get { return AutoPayTile; } }
        public ITextToPayTile TextToPayTileControl { get { return TextToPayTile; } }
        public EventHandler SaveButtonClicked { set { SaveButton.Click += value; } }
        public bool IsPropertyManagerSite { get { return false; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeControls();
            _Presenter = new PaymentInfoTabPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }

        private void InitializeControls()
        {
            var Id = HttpContext.Current.Request.QueryString["renterId"].ToNullInt32();
            if (!Id.HasValue || Id.Value < 1)
            {
                BasePage.SafePageRedirect("~/Renters/Default.aspx");
                return;
            }

            //Salcedo - 5/23/2014 - Task # 0000007 - update CurrentBalanceDue from Yardi
            //cakel: BUGID00074 Function Name Update - “UpdateRenterBalanceDueForPrePay” to “UpdateRenterBalanceDueRealTimeYardi”.  
            var Resident = new EfxFramework.Renter(Id.Value);
            if (Resident.PmsTypeId == 1 && Resident.PmsId != "")
            {
                EfxFramework.Pms.Yardi.ImportRequest.UpdateRenterBalanceDueRealTimeYardi(Resident);
            }
            //cakel: BUGID00316
            if (Resident.PmsTypeId == 2 && Resident.PmsId != "")
            {
               
                EfxFramework.Pms.AMSI.AmsiRequest.AMSI_RequestXmlForRenterImport(2, _RenterID: Resident.RenterId);
                //TextToPayTile.DayOfTheMonthDropdown
            }

            PersonalInformation.RenterId = Id.Value;
            SelectPaymentType.RenterId = Id.Value;
            ResidentBillingInformation.RenterId = Id.Value;
            PaymentAmounts.RenterId = Id.Value;
            PaymentAmounts.PaymentTypes = SelectPaymentType;
            AutoPayTile.RenterId = Id.Value;
            TextToPayTile.RenterId = Id.Value;
        }

		protected void btnCancel_Click(object sender, EventArgs e)
		{
			SelectPaymentType.ReInitialize();
			ResidentBillingInformation.ReInitializeValues();
			PersonalInformation.ReInitializeValues();
			AutoPayTile.ReInitializeValues();
			TextToPayTile.ReInitializeValues();
			PaymentAmounts.ReInitializeValues();
		}

        //CMallory - Task 0090 - Added PayNowButton_click method that shows a Message Box with specific text based on the payment type.
        protected void SaveButton_Click(object sender, EventArgs e)
        {
            if (TextToPayTile.IsOn && !AutoPayTile.IsOn)
            {   
                MessageBoxText("PayByText Reminders have been set up, you will receieve a payment reminder by text on the " + TextToPayTile.DayOfTheMonthDropdown.SelectedValue +  " of each month.");
            }

            else if(AutoPayTile.IsOn && !TextToPayTile.IsOn)
            {
                if (AutoPayTile.PaymentTypeDropdown.SelectedValue == "1")
                {
                    MessageBoxText("AutoPay setup has been completed. You will receive a security validation email on the " + AutoPayTile.DayOfTheMonthDropdown.SelectedValue + " of each month, once confirmed your payment will process automatically.");
                }

                else if (AutoPayTile.PaymentTypeDropdown.SelectedValue == "2")
                {
                    MessageBoxText("AutoPay setup has been completed. Your payments will automatically process by E-Check on the " + AutoPayTile.DayOfTheMonthDropdown.SelectedValue + " of each month.");

                }
            }
            
            else if (AutoPayTile.IsOn && TextToPayTile.IsOn)
            {
                MessageBoxText("PayByText Reminders have been set up, you will receieve a payment reminder by text on the " + TextToPayTile.DayOfTheMonthDropdown.SelectedValue +  " of each month.");
                if (AutoPayTile.PaymentTypeDropdown.SelectedValue == "1")
                {
                    MessageBoxText("AutoPay setup has been completed. You will receive a security validation email on the " + AutoPayTile.DayOfTheMonthDropdown.SelectedValue + " of each month, once confirmed your payment will process automatically.");
                }

                else if (AutoPayTile.PaymentTypeDropdown.SelectedValue == "2")
                {
                    MessageBoxText("AutoPay setup has been completed. Your payments will automatically process by E-Check on the " + AutoPayTile.DayOfTheMonthDropdown.SelectedValue + " of each month.");
                }
            }
        }

        //CMallory - Task 0090 - Added utility method below.
        private void MessageBoxText(string text)
        {
            MessageBox.Show(text, "Important Note", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        //CMallory - Task 00173 - Added
        protected void DeleteIcon_Click(object sender, EventArgs e)
        {
            int Id = (int)HttpContext.Current.Request.QueryString["renterId"].ToNullInt32();

            //Credit Card
            if (SelectPaymentType.PaymentTypeSelectedValue=="1")
            {
                EfxFramework.PayerCreditCard.DeletePayerSavedCreditCard(Id);
                
                SelectPaymentType.CreditCardDetailsControl.CreditCardNumberText = null;
                SelectPaymentType.CreditCardDetailsControl.CvvText = null;
                SelectPaymentType.CreditCardDetailsControl.NameOnCardText = null;
                //Response.Redirect(Request.RawUrl);
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
            
            //Echeck/ACH
            else if (SelectPaymentType.PaymentTypeSelectedValue=="2")
            {
                EfxFramework.PayerAch.DeletePayerSavedACH(Id);
                SelectPaymentType.ECheckDetailsControl.RoutingNumberText = null;
                SelectPaymentType.ECheckDetailsControl.AccountNumberText = null;
            }

        }
    }
}