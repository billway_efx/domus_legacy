﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentInfoTab.ascx.cs" Inherits="Domus.UserControls.PaymentControlsV2.PaymentInfo.PaymentInfoTab" %>

<asp:ValidationSummary ID="PaymentInfoTabSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="Payment" />
<asp:PlaceHolder ID="paymentInfoSuccess" runat="server" Visible="false">
   <div class="alert alert-success"><b>Success!</b></div>
</asp:PlaceHolder>
<div class="formTint">
   <div class="formWrapper">
      <uc1:ResidentBillingInformation ID="ResidentBillingInformation" runat="server" />
   </div>
   <div class="formTint">
      <div class="formWrapper">
         <h2>Personal Information</h2>
         <uc1:PersonalInformation ID="PersonalInformation" runat="server" />
      </div>
   </div>
   <div class="formWrapper">
      <h2>Payment Information</h2>
      <uc1:ResidentWallet ID="ResidentWalletTabControl" runat="server" Visible="true" />
      <uc1:SelectPaymentType ID="SelectPaymentType" runat="server" Visible="false" />
   </div>
</div>
<div class="formWrapper orWrapper" runat="server" id="AutoPayTextToPay">
   <!--cakel: BUGID00069 - Capitalized "P" in h2 tag -->
   <h2>Text Payment Options</h2>
   <!--cakel: hide the autopayment option -->
   <div class="formHalf" id="AutopayArea" runat="server" visible="false">
      <uc1:AutoPayTile ID="AutoPayTile" runat="server" />
   </div>
   <div class="formHalf">
      <uc1:TextToPayTile ID="TextToPayTile" runat="server" />
   </div>

</div>
<div class="formWrapper">
   <div class="formHalf">
      <!--cakel: BUGID00085 Part 3 - Added display:None to hide user control -->
      <div style="display: none">
         <uc1:PaymentAmounts ID="PaymentAmounts" runat="server" />
      </div>

   </div>
</div>
<div class="button-footer">
   <div class="button-action">
      <%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default" OnClick="btnCancel_Click" CausesValidation="false" />--%>
   </div>
   <div class="main-button-action">
      <asp:Button ID="SaveButton" CssClass="btn btn-large footer-button" runat="server" Text="Save" CausesValidation="False" />
   </div>
   <div class="clear"></div>
</div>
