﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectPaymentType.ascx.cs" Inherits="Domus.UserControls.PaymentControlsV2.SelectPaymentType" %>

<!--**First Row (Payment Method) This drop down option determines which blcok is displayed (creditCard, eCheck, or textToPay**-->
<div class="formHalf">

   <label>Payment Method</label>
   <asp:DropDownList ID="PaymentMethodComboBox" runat="server" CssClass="form-control" AutoPostBack="True">
      <asp:ListItem Value="1" Text="Credit Card" Selected="True"></asp:ListItem>
      <asp:ListItem Value="2" Text="E-Check"></asp:ListItem>
      <asp:ListItem Value="3" Text="RentByCash"></asp:ListItem>
   </asp:DropDownList>
</div>

<div class="clear"></div>
<asp:MultiView ID="SelectedType" runat="server">
   <asp:View ID="CreditCardView" runat="server">
      <uc1:CreditCardDetails ID="CreditCardDetails" runat="server" />
   </asp:View>
   <asp:View ID="EcheckView" runat="server">
      <div class="formWhole">
         <uc1:ElectronicCheckDetails ID="ElectronicCheckDetails" runat="server" />
      </div>
   </asp:View>
   <asp:View ID="RentByCash" runat="server">
   </asp:View>
</asp:MultiView>