﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Web.UI;

namespace Domus.UserControls.PaymentControlsV2.PaymentType
{
    public partial class ElectronicCheckDetails : UserControl, IElectronicCheckDetails
    {
        private ElectronicCheckDetailsPresenter _Presenter;

        public string RoutingNumberText { get { return RoutingNumberTextBox.Text; } set { RoutingNumberTextBox.Text = value; } }
        public string AccountNumberText { get { return AccountNumberTextBox.Text; } set { AccountNumberTextBox.Text = value; } }
        public string HiddenACHAccountNumber { get { return HiddenACHAccountNumbertxt.Text; } set { HiddenACHAccountNumbertxt.Text = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new ElectronicCheckDetailsPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.IntializeValues();
        }
    }
}