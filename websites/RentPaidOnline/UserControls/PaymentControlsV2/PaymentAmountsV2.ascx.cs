﻿using EfxFramework;
using EfxFramework.Interfaces.UserControls.Account;
using EfxFramework.Presenters.UserControls.Account;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.Web;
using System;
using Mb2x.ExtensionMethods;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RentPaidOnline.UserControls.PaymentControlsV2
{
    public partial class PaymentAmountsV2 : System.Web.UI.UserControl
    {

        //Get Renter ID from Querystring
        public int RenterId
        {
            get
            {
                int renter = 0;
                string Id = HttpContext.Current.Request.QueryString["renterId"].ToString();
                renter = Convert.ToInt32(Id);
                return renter;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            PopulateLeaseAmountDue(RenterId);
            PopulateFeeDetail(RenterId);
        }


        public void PopulateLeaseAmountDue(int _RenterID)
        {
            SqlDataReader _Reader = DLAccess.GetTotalDueByRenterID(_RenterID);
            _Reader.Read();

            if (_Reader.HasRows)
            {
                RentAmountLabel.Text = String.Format("{0:c}",_Reader["RentAmount"]);
                TotalAmountLabel.Text = String.Format("{0:c}",_Reader["TotalRentDue"]);
            }

        }

        public void PopulateFeeDetail(int RenterID)
        {
            MonthlyFeesList.DataSource = DLAccess.GetFeeDetailByRenter(RenterId);
            MonthlyFeesList.DataBind();
        }


    }
}