﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PersonalInformation.ascx.cs" Inherits="Domus.UserControls.PaymentControlsV2.PersonalInformation" %>


    <table style="border: 0px lightgray solid;" id="tblName" cellspacing="2" cellpadding="2" width="100%">
        <tr valign="top">
            <td style="vert-align: top;">
                <label>First Name</label>            
            </td>
            <td style="vert-align: top;" >
                <label>Middle Name</label>
            </td>
            <td style="vert-align: top;" >
                <label>Last Name</label>
            </td>
            <td style="vert-align: top;" >
                <label>Suffix</label>
            </td>
        </tr>
        <tr valign="top">
            <td style="vert-align: top;">
                <asp:TextBox ID="FirstNameTextBox" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFirstNameValidation"
                    runat="server"
                    Display="Static"
                    ErrorMessage="First Name is Required"
                    ControlToValidate="FirstNameTextBox"
                    ValidationGroup="Payment" InitialValue="" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
            </td>
            <td style="vert-align: top;">
                <asp:TextBox ID="MiddleNameTextBox" CssClass="form-control" runat="server"></asp:TextBox>
            </td>
            <td style="vert-align: top;">
                <asp:TextBox ID="LastNameTextBox" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredLastNameValidation"
                    runat="server"
                    Display="Static"
                    ErrorMessage="Last Name is Required"
                    ControlToValidate="LastNameTextBox"
                    ValidationGroup="Payment" InitialValue="" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
            </td>
            <td style="vert-align: top;">
                <asp:TextBox ID="SuffixTextBox" CssClass="form-control" runat="server" Width="100px"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br/>
    <table style="border: 0px lightgray solid;" id="tblPersonal" cellspacing="2" cellpadding="2" width="100%"> 
        <tr>
            <td style="vert-align: top;">
                <label>Email Address</label>
            </td>
            <td style="vert-align: top;">
                <label>Phone Number</label>
            </td>
        </tr>
    
        <tr>
            <td style="vert-align: top;">
                <asp:TextBox ID="EmailAddressTextBox" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredEmailValidator"
                                            runat="server"
                                            Display="Static"
                                            ErrorMessage="EMail Address is Required"
                                            ControlToValidate="LastNameTextBox"
                                            ValidationGroup="Payment" InitialValue="" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
            </td>
            <td style="vert-align: top;">
                <asp:TextBox ID="PhoneNumberTextBox" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredPhoneValidator"
                                            runat="server"
                                            Display="Static"
                                            ErrorMessage="Phone Number is Required"
                                            ControlToValidate="LastNameTextBox"
                                            ValidationGroup="Payment" InitialValue="" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>

