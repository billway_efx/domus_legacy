﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domus.UserControls.PaymentControlsV2
{
    public partial class ResidentBillingInformation : UserControl, IResidentBillingInformation
    {
        private ResidentBillingInformationPresenter _Presenter;

        public int RenterId { get; set; }
        public EventHandler SameAsMailingChecked { set { SameAsMailingCheckBox.CheckedChanged += value; } }
        public bool IsSameAsMailingChecked { get { return SameAsMailingCheckBox.Checked; } set { SameAsMailingCheckBox.Checked = value; } }
        public string BillingAddressText { get { return BillingAddressTextBox.Text; } set { BillingAddressTextBox.Text = value; } }
        public string CityText { get { return CityTextBox.Text; } set { CityTextBox.Text = value; } }
        public DropDownList StateDropDown { get { return StateDropDownList; } }
        public string ZipText { get { return ZipCodeTextBox.Text; } set { ZipCodeTextBox.Text = value; } }
        public string BillingPhone { get { return PhoneNumberTextBox.Text; } set { PhoneNumberTextBox.Text = value; } }
        //public string EmailAddressText { get { return EmailAddressTextBox.Text; } set { EmailAddressTextBox.Text = value; } }
        public string ZipCodeClientId { get { return ZipCodeTextBox.ClientID; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new ResidentBillingInformationPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }

		public void ReInitializeValues()
		{
			_Presenter.InitializeValues();
		}
    }
}