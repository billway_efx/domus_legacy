﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentAmountsV2.ascx.cs" Inherits="RentPaidOnline.UserControls.PaymentControlsV2.PaymentAmountsV2" %>



<div class="well well-sm">
    <h2>Total Payment Amount</h2>                                  
    <!--Rent Amount -->
    <div class="itemRow">
        <span class="itemTitle">Rent:</span>
        <span class="itemAmount"><asp:Label ID="RentAmountLabel" runat="server" /></span>
    </div>
                           
    <asp:ListView ID="MonthlyFeesList" runat="server">
        <LayoutTemplate>
            <div id="itemPlaceholder" class="itemRow" runat="server"></div>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="itemRow">
                <span class="itemTitle"><asp:Label ID="ItemTitleLabel" runat="server" Text='<%# Bind("FeeName") %>' /></span>
                <span class="itemAmount"><asp:Label ID="ItemAmountLabel" runat="server" Text='<%# Bind("FeeAmount","{0:c}") %>' /></span>
            </div>
        </ItemTemplate>
    </asp:ListView>                     
    <!--Total -->
    <div class="well-footer">
		<span class="pull-right">Total: <asp:Label ID="TotalAmountLabel" CssClass="text-success" runat="server" /></span>
		<div class="clear"></div>
       
    </div>
</div>