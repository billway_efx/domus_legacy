﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResidentWallet.ascx.cs" Inherits="Domus.UserControls.PaymentControlsV2.ResidentWallet" %>
<%@ Import Namespace="EfxFramework.ExtensionMethods" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        
        <div class="formWhole">
            <asp:HiddenField ID="RenterId" runat="server" />
              <% if (!EfxFramework.Helpers.Helper.IsCsrUser)
		    { %>
            <div class="col-lg-10">
                <asp:ValidationSummary ID="ApplicationSummary" DisplayMode="List" CssClass="alert alert-danger"  runat="server" ValidationGroup="Payment" />
                <asp:Button ID="AddNewCCbtn" CssClass="btn btn-primary" runat="server" Text="Add New Credit Card" OnClick="AddNewCCbtn_Click" CausesValidation="False" />&nbsp;
                <asp:Button ID="AddNewEcheckbtn" CssClass="btn btn-primary" runat="server" Text="Add New Bank Account" OnClick="AddNewEcheckbtn_Click" CausesValidation="False" />
                
            </div>
            <%} %>
            <div class="col-lg-12" id="AddnewWalletItemSection" runat="server" visible="false">

                    <!--Add New ECheck -->
                    <div class="col-lg-8 text-left" id="EcheckADD" runat="server" visible="false">
                        <h4>
                            <asp:Label ID="EcheckHeaderlbl" runat="server" Text="New Bank Account"></asp:Label></h4>
                        <div class="col-lg-10">
                            <label>Account Type</label>
                            <asp:DropDownList ID="BankAccountTypeDDL" CssClass="form-control" runat="server">
                                <asp:ListItem Value="1">Checking Account</asp:ListItem>
                                <%-- SWitherspoon Task 00737: We do not accept savings account. Hiding --%>
                                <%-- <asp:ListItem Value="2">Savings Account</asp:ListItem>--%>
                            </asp:DropDownList>
                        </div>
                        <div class="col-lg-10">
                            <label>Routing Number</label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="echeck" ControlToValidate="NewRoutingNumbertxt" runat="server" ErrorMessage="RequiredFieldValidator"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ValidationGroup="echeck" ControlToValidate="NewRoutingNumbertxt" ValidationExpression="^[0-9]*$" runat="server" ErrorMessage="RegularExpressionValidator"><i class="fa fa-warning validationStyle"></i></asp:RegularExpressionValidator>
                            <asp:TextBox ID="NewRoutingNumbertxt" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <%-- SWitherspoon Task 00737: Removed Left-Margin to fix alignment issues with boxes --%>
                        <div class="col-lg-10">
                             <asp:Label ID="AccountNumlbl" runat="server" Text="Account Number"></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="echeck" ControlToValidate="NewAccountnumbertxt" ErrorMessage="RequiredFieldValidator"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                             <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ValidationGroup="echeck" ControlToValidate="NewAccountnumbertxt" ValidationExpression="^[0-9]*$" runat="server" ErrorMessage="RegularExpressionValidator"><i class="fa fa-warning validationStyle"></i></asp:RegularExpressionValidator>
                            <asp:TextBox ID="NewAccountnumbertxt" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-lg-10" style="margin-top:15px;">
                            <asp:Button ID="AddNewEcheckandPaybtn" CssClass="btn btn-primary" ValidationGroup="echeck" Text="Submit New Account" runat="server" OnClick="AddNewEcheckandPaybtn_Click" />
                            &nbsp;
                            <asp:Button ID="AddEcheckCancel" CssClass="btn btn-primary" CausesValidation="false" runat="server" Text="Cancel" OnClick="AddEcheckCancel_Click" />
                        </div>
                    </div>
                    <!--End Add New ECheck -->

                    <!--Add New CC -->
                    <div class="col-lg-8 text-left" id="CreditCardAdd" runat="server" visible="false">
                        
                        <h4>
                            <asp:Label ID="CCHeaderlbl" runat="server" Text="New Credit Card"></asp:Label>
                        </h4>
                        <div class="col-lg-12">
                            <label>Name on Card</label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="CreditCard" ControlToValidate="FullNametxt" runat="server" ErrorMessage="RequiredFieldValidator"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                            <asp:TextBox ID="FullNametxt" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-12">
                            <asp:Label ID = "CardNumlbl"  runat="server" Text = "Card Number" >Card Number</asp:Label>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationGroup="CreditCard" ControlToValidate="CardAccountnumtxt" ValidationExpression="^[0-9]*$" runat="server" ErrorMessage="RegularExpressionValidator"><i class="fa fa-warning validationStyle"></i></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="CreditCard" ControlToValidate="CardAccountnumtxt" runat="server" ErrorMessage="RequiredFieldValidator"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                            <asp:TextBox ID="CardAccountnumtxt" runat="server" CssClass="form-control" MaxLength="16"></asp:TextBox>
                        </div>

                        <%--SWitherspoon Task 00737: Removed outer div to fix an issue with boxes not aligning properly. Blame the CSS --%>
                        <%--<div class="col-lg-12">--%>
                            <div class="col-lg-4">
                                <label>Exp Month</label>
                                <asp:DropDownList ID="ExpMonthDDL" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="1">Jan</asp:ListItem>
                                    <asp:ListItem Value="2">Feb</asp:ListItem>
                                    <asp:ListItem Value="3">Mar</asp:ListItem>
                                    <asp:ListItem Value="4">Apr</asp:ListItem>
                                    <asp:ListItem Value="5">May</asp:ListItem>
                                    <asp:ListItem Value="6">Jun</asp:ListItem>
                                    <asp:ListItem Value="7">Jul</asp:ListItem>
                                    <asp:ListItem Value="8">Aug</asp:ListItem>
                                    <asp:ListItem Value="9">Sep</asp:ListItem>
                                    <asp:ListItem Value="10">Oct</asp:ListItem>
                                    <asp:ListItem Value="11">Nov</asp:ListItem>
                                    <asp:ListItem Value="12">Dec</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-lg-4">
                                <label>Exp Year</label>
                                <asp:DropDownList ID="ExpYearDDL" runat="server" CssClass="form-control">
                                    <asp:ListItem>2016</asp:ListItem>
                                    <asp:ListItem>2017</asp:ListItem>
                                    <asp:ListItem>2018</asp:ListItem>
                                    <asp:ListItem>2019</asp:ListItem>
                                    <asp:ListItem>2020</asp:ListItem>
                                    <asp:ListItem>2021</asp:ListItem>
                                    <asp:ListItem>2022</asp:ListItem>
                                    <asp:ListItem>2023</asp:ListItem>
                                    <asp:ListItem>2024</asp:ListItem>
                                    <asp:ListItem>2025</asp:ListItem>
                                    <asp:ListItem>2026</asp:ListItem>
                                    <asp:ListItem>2027</asp:ListItem>
                                    <asp:ListItem>2028</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-lg-4">
                                <label>CVV</label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="CVVtxt" ValidationGroup="CreditCard" runat="server" ErrorMessage="RequiredFieldValidator"><i class="fa fa-warning validationStyle"></i></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="CreditCard" runat="server" ValidationExpression="^[0-9]*$" ErrorMessage="RegularExpressionValidator" ControlToValidate="CVVtxt"><i class="fa fa-warning validationStyle"></i></asp:RegularExpressionValidator>
                                <asp:TextBox ID="CVVtxt" runat="server" CssClass="form-control" MaxLength="4"></asp:TextBox>
                            </div>
                        <%--</div>--%>

                        <div class="col-lg-8" style="margin-top:15px;">
                            <asp:Button ID="AddNewCCandPaybtn" CssClass="btn btn-primary" ValidationGroup="CreditCard" Text="Submit New Credit Card" runat="server" OnClick="AddNewCCandPaybtn_Click" />
                            &nbsp;
                            <asp:Button ID="AddccCancel" CssClass="btn btn-primary" runat="server" CausesValidation="false" Text="Cancel" OnClick="AddccCancel_Click" />
                            <br />
                            <%-- SWitherspoon Task 00737: Added hidden field to grab info on pay methods to populate fields when Updating pay methods --%>
                            <asp:HiddenField ID="MethodID" runat="server" />
                        </div>
                    </div>
                    <!--End Add New CC -->

                </div>
            
            <%-- SWitherspoon Task 00737: Copied and altered gridview to use new stored procedure information --%>
            <asp:GridView ID="WalletGridTest" CssClass="table table-responsive" runat="server" DataKeyNames="PayerId,PaymentMethodDesc,PayerCreditCardId" AutoGenerateColumns="False" OnRowDataBound="WalletGrid_RowDataBound_test" OnRowCommand="WalletGrid_RowCommand_test" DataSourceID="ResidentWalletTest">                
                <Columns>
                        <asp:TemplateField HeaderText="Wallet Item">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("PaymentMethodDesc") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="PayTypelbl" runat="server" Text='<%# Bind("PaymentMethodDesc") %>'></asp:Label><br />
                                <asp:Label ID="CCNum" runat="server" Text='<%# Bind("CreditCardNumber") %>'></asp:Label>&nbsp;
                                <asp:Label ID="ExpDatelbl" runat="server" Text='<%# Bind("Expiration") %>'></asp:Label><br />
                                <asp:Label ID="AutoPaylbl" runat="server" Font-Size="X-Small"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Exp Date" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("Expiration") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="EXP_Datelbl" runat="server" Text='<%# Bind("Expiration") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:BoundField DataField="IsPrimary" HeaderText="IsPrimary" Visible="False" />

                        <asp:TemplateField HeaderText="PayerId" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PayerId") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("PayerId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PayerMethodId" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("PayerCreditCardId") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("PayerCreditCardId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="IsPrimary" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="IsPrimarylbl" runat="server" Text='<%# Bind("IsPrimary") %>'></asp:Label>
                                <asp:Label ID="IsAutoPaylbl" runat="server" Text='<%# Bind("IsAutoPay") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
               <asp:TemplateField HeaderText="" >
                   <ItemTemplate>
                       <asp:Button ID="AutoPaySetbtn" runat="server" Text="Set AutoPayment" CssClass="btn btn-primary" CommandName="Select" CommandArgument='<%# "SetAutoPayment" + ";" + Eval("PaymentMethodDesc") + ";" + Eval("PayerId") + ";" + Eval("PayerCreditCardId") %>' Visible='<%#!EfxFramework.Helpers.Helper.IsCsrUser%>' />
                       <asp:Button ID="CancelAutoPaybtn" CssClass="btn btn-primary" runat="server" Text="Cancel AutoPayment" CommandArgument='<%# "CancelAutoPayment" + ";" + Eval("PaymentMethodDesc") + ";" + Eval("PayerId") + ";" + Eval("PayerCreditCardId") %>' OnClientClick="return confirm('Are you sure you want to cancel this automatic payment?');" />
                       <asp:Button ID="UpdatePayMethodbtn" CssClass="btn btn-primary" runat="server" Text="Update PayMethod" CommandArgument='<%# "UpdatePayMethod" + ";" + Eval("PaymentMethodDesc") + ";" + Eval("PayerId") + ";" + Eval("PayerCreditCardId") %>'/>
                       <asp:Button ID="DeletePayMethodbtn" CssClass="btn btn-primary" runat="server" Text="Delete PayMethod" CommandArgument='<%# "DeletePayMethod" + ";" + Eval("PaymentMethodDesc") + ";" + Eval("PayerId") + ";" + Eval("PayerCreditCardId") %>' OnClientClick="return confirm('Are you sure you want to delete this payment method?');"/>
                       <asp:Button ID="MakePrimarybtn" CssClass="btn btn-primary" runat="server" Text="Make Primary AutoPay" CommandArgument='<%# "MakePrimaryPayment" + ";" + Eval("PaymentMethodDesc") + ";" + Eval("PayerId") + ";" + Eval("PayerCreditCardId") %>'/>
                   </ItemTemplate>                
               </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <div class="col-lg-6">
                            <h3>No Wallet Items Found</h3>
                            <h4>
                                Use Add Wallet items buttons to add a payment method to your Wallet.
                            </h4> 
                        </div>
                    </EmptyDataTemplate>

                    <SelectedRowStyle BackColor="#CCCCCC" />
            </asp:GridView>
            <%-- SWitherspoon Task 00737: New Datasource to use with new stored procedure --%>
                        <asp:SqlDataSource ID="ResidentWalletTest" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="wallet_GetPaymentMethodsForRenter_v2" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:QueryStringParameter DbType="String" Name="RenterId" QueryStringField="renterId" />
                        </SelectParameters>
                    </asp:SqlDataSource>
            <br />
                
        </div>
                
<div class="formWhole">

                <!--Update AutoPayment -->
            <div class="col-lg-12" id="UpdateAutoPaymentArea" runat="server" visible="false">
                 <h4>Add Wallet Item For AutoPayment</h4>
                <div class="col-lg-10">
                <p>The above selected payment method will be used to process your payments automatically. </p>
                    <p>Please select the day of the month you would like us to make your payment</p>
                    <div class="col-lg-6">
                        <label>Select Day</label>
                         <asp:DropDownList ID="DayofMonthDDL" CssClass="form-control" runat="server">
                             <asp:ListItem>1</asp:ListItem>
                             <asp:ListItem>2</asp:ListItem>
                             <asp:ListItem>3</asp:ListItem>
                             <asp:ListItem>4</asp:ListItem>
                             <asp:ListItem>5</asp:ListItem>
                             <asp:ListItem>5</asp:ListItem>
                             <asp:ListItem>6</asp:ListItem>
                             <asp:ListItem>7</asp:ListItem>
                             <asp:ListItem>8</asp:ListItem>
                             <asp:ListItem>9</asp:ListItem>
                             <asp:ListItem>10</asp:ListItem>
                             <asp:ListItem>11</asp:ListItem>
                             <asp:ListItem>12</asp:ListItem>
                             <asp:ListItem>13</asp:ListItem>
                             <asp:ListItem>14</asp:ListItem>
                             <asp:ListItem>15</asp:ListItem>
                             <asp:ListItem>16</asp:ListItem>
                             <asp:ListItem>17</asp:ListItem>
                             <asp:ListItem>18</asp:ListItem>
                             <asp:ListItem>19</asp:ListItem>
                             <asp:ListItem>20</asp:ListItem>
                             <asp:ListItem>21</asp:ListItem>
                             <asp:ListItem>22</asp:ListItem>
                             <asp:ListItem>23</asp:ListItem>
                             <asp:ListItem>24</asp:ListItem>
                             <asp:ListItem>25</asp:ListItem>
                             <asp:ListItem>26</asp:ListItem>
                             <asp:ListItem>27</asp:ListItem>
                             <asp:ListItem>28</asp:ListItem>
                         </asp:DropDownList>

                        <br />
                        <br />
                        <asp:Button ID="SubmitAutoPay" CssClass="btn btn-primary" runat="server" Text="Add Autopayment" OnClick="SubmitAutoPay_Click" OnClientClick="return confirm('Are you sure you want to set this method to pay automatically?');" />&nbsp;&nbsp;
                        <asp:Button ID="CancelAutoPay" CssClass="btn btn-primary" runat="server" Text="Cancel" OnClick="CancelAutoPay_Click" />

                    </div>
                   

                </div>
                
            </div>
            <!--End Update AutoPayment -->

</div>
        
            <!--End Update AutoPayment -->

    </ContentTemplate>

</asp:UpdatePanel>
