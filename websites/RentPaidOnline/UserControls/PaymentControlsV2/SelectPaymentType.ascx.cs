﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mb2x.ExtensionMethods;

namespace Domus.UserControls.PaymentControlsV2
{
    public partial class SelectPaymentType : UserControl, ISelectPaymentType
    {
        private SelectPaymentTypePresenter _Presenter;

        public int RenterId { get; set; }
        public EventHandler PaymentTypeChanged { set { PaymentMethodComboBox.SelectedIndexChanged += value; } }
        public string PaymentTypeSelectedValue { get { return PaymentMethodComboBox.SelectedValue; } set { PaymentMethodComboBox.SelectedValue = value; } }
        public int PaymentTypeSelectedIndex { set { SelectedType.ActiveViewIndex = value; } }
        public ICreditCardDetails CreditCardDetailsControl { get { return CreditCardDetails; } }
        public IElectronicCheckDetails ECheckDetailsControl { get { return ElectronicCheckDetails; } }
        public bool IsApplicant { get; set; }
		public DropDownList PaymentMethodDropDown { get { return PaymentMethodComboBox; } }
		public bool AcceptCCPayments { get; set; }
		public bool AcceptACHPayments { get; set; }
		public bool PNMEnabled { get; set; }
		public int PropertyId { get; set; }
		private EfxFramework.Property currentProperty { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
			
            _Presenter = new SelectPaymentTypePresenter(this);
			AssignCurrentProperty();
			_Presenter.currentProperty = currentProperty;
			_Presenter.applyToNextMonthChk = (CheckBox)this.Parent.FindControl("uxApplyPaymentToNextMonth");
			_Presenter.PayNowButton = (Button)this.Parent.FindControl("PayNowButton");
            if (!IsPostBack)
                _Presenter.InitializeValues();

            InitializeCreditCardExpiration();
        }

		private void AssignCurrentProperty()
		{
			DropDownList selectedPropertyDropDown = (DropDownList)this.Parent.FindControl("PropertyDropDown");
			if (selectedPropertyDropDown != null)
			{
				PropertyId = selectedPropertyDropDown.SelectedValue.ToInt32();
			}
			if (currentProperty == null && PropertyId > 0)
			{
				currentProperty = new EfxFramework.Property(PropertyId);
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			try
			{
				base.OnPreRender(e);
				if (currentProperty != null)
				{
					AcceptACHPayments = currentProperty.AcceptACHPayments;
					AcceptCCPayments = currentProperty.AcceptCCPayments;
					PNMEnabled = currentProperty.PNMEnabled;
					setControlsBasedOnProperty();
				}
			}
			catch (Exception ex)
			{
				EfxFramework.Logging.ExceptionLogger.LogException(ex);
			}
		}

		private void setControlsBasedOnProperty()
		{
			ListItem acceptCCItem = PaymentMethodComboBox.Items.FindByValue("1");
			if (acceptCCItem != null && !AcceptCCPayments)
			{
				PaymentMethodComboBox.Items.Remove(acceptCCItem);
			}
			else if (acceptCCItem == null && AcceptCCPayments)
			{
				ListItem acceptCCLItem = new ListItem();
				acceptCCLItem.Text = "Credit Card";
				acceptCCLItem.Value = "1";
				PaymentMethodComboBox.Items.Insert(0, acceptCCLItem);
			}

			ListItem acceptACHItem = PaymentMethodComboBox.Items.FindByValue("2");
			if (acceptACHItem != null && !AcceptACHPayments)
			{
				PaymentMethodComboBox.Items.Remove(acceptACHItem);
			}
			else if (acceptACHItem == null && AcceptACHPayments)
			{
				ListItem acceptACHLItem = new ListItem();
				acceptACHLItem.Text = "E-Check";
				acceptACHLItem.Value = "2";
				PaymentMethodComboBox.Items.Add(acceptACHLItem);
			}

			ListItem acceptPayByCashItem = PaymentMethodComboBox.Items.FindByValue("3");
			if (acceptPayByCashItem != null && !PNMEnabled)
			{
				PaymentMethodComboBox.Items.Remove(acceptPayByCashItem);
			}
			else if (acceptPayByCashItem == null && PNMEnabled)
			{
				ListItem acceptPayByCashLItem = new ListItem();
				acceptPayByCashLItem.Text = "RentByCash&#8482;";
				acceptPayByCashLItem.Value = "3";
				PaymentMethodComboBox.Items.Add(acceptPayByCashLItem);
			}

			if (PaymentMethodComboBox.Items.Count > 0)
			{
				SelectedType.Visible = true;
				int selectedValue = 0;
				if (Int32.TryParse(PaymentMethodComboBox.SelectedValue, out selectedValue))
				{
					if (selectedValue == 3)
					{
						CheckBox applyPaymentToNextMonthChk = (CheckBox)this.Parent.FindControl("uxApplyPaymentToNextMonth");
						Button PayNowButton = (Button)this.Parent.FindControl("PayNowButton");
						if (applyPaymentToNextMonthChk != null)
						{
							applyPaymentToNextMonthChk.Enabled = false;
							applyPaymentToNextMonthChk.Checked = false;
						}
						if (PayNowButton != null)
						{
							PayNowButton.Text = "Create PayNearMe Ticket";
						}
					}
					else
					{
						CheckBox applyPaymentToNextMonthChk = (CheckBox)this.Parent.FindControl("uxApplyPaymentToNextMonth");
						Button PayNowButton = (Button)this.Parent.FindControl("PayNowButton");
						if (applyPaymentToNextMonthChk != null && !string.IsNullOrEmpty(currentProperty.PmsId) && !currentProperty.PmsTypeId.HasValue)
						{
							applyPaymentToNextMonthChk.Enabled = true;
							applyPaymentToNextMonthChk.Checked = true;
						}
						if (PayNowButton != null)
						{
							PayNowButton.Text = "Pay Now";
						}
					}
					SelectedType.ActiveViewIndex = selectedValue - 1;
				}
			}
			else
			{
				SelectedType.Visible = false;
			}

			if (!AcceptACHPayments && !AcceptCCPayments && !PNMEnabled)
			{
				this.Visible = false;
				Label payInfoLabel = (Label)this.Parent.FindControl("uxPaymentInformationLabel");
				if (payInfoLabel != null)
				{
					payInfoLabel.Visible = true;
				}

				Button payNow = (Button)this.Parent.FindControl("PayNowButton");
				if (payNow != null)
				{
					payNow.Enabled = false;
					payNow.Visible = false;
				}
			}
			else
			{
				this.Visible = true;
				Label payInfoLabel = (Label)this.Parent.FindControl("uxPaymentInformationLabel");
				if (payInfoLabel != null)
				{
					payInfoLabel.Visible = false;
				}

				Button payNow = (Button)this.Parent.FindControl("PayNowButton");
				if (payNow != null)
				{
					payNow.Enabled = true;
					payNow.Visible = true;
				}
			}
		}

        public void InitializeCreditCardExpiration()
        {
            _Presenter.InitializeExpirationYears();
        }

        public void ClearFields()
        {
            
        }

		public void ReInitialize()
		{
			_Presenter.InitializeValues();
			InitializeCreditCardExpiration();
		}
    }
}