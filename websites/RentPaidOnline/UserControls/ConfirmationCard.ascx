﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConfirmationCard.ascx.cs" Inherits="Domus.UserControls.ConfirmationCard" %>

<div class="confirmation-card">
    <h4>
        <asp:Label ID="HeaderLabel" runat="server" />
        <asp:Button ID="EditButton" runat="server" Text="Edit" CssClass="green-button" />
    </h4>
    <div class="content">
        <asp:Literal ID="ConfirmationInformation" runat="server" />
    </div>
</div>