﻿using System.Web.UI.WebControls;
using EfxFramework.Interfaces.UserControls.Property;
using System;
using System.Web.UI;
using EfxFramework;

namespace Domus.UserControls.Property
{
    public partial class PmsAddEdit : UserControl, IPmsAddEdit
    {
        private EfxFramework.Presenters.UserControls.Property.PmsAddEdit _presenter;

        public int PropertyId { get; set; }
        public DropDownList PmsDropdownList { get { return PMSDropdown; } }
        public MultiView PmsViews { get { return PmsMultiView; } }
        public View Blank { get { return BlankView; } }
        public View Yardi { get { return YardiView; } }
        public View Amsi { get { return AmsiView; } }
        public View RealPage { get { return RealPageView; } }
        public View MRI { get { return MRIView; } }
        public IYardiAddEdit YardiControl { get { return YardiAddEditControl; } }
        //public IMRIAddEdit MRIControl { get { return MRIAddEditControl; } }
        //cakel: 00382 MRI
        public IMRIAddEdit2 MRIControl2 { get { return MriAddEditNew; } }
        //cakel: BUGID00316
        public IAmsiAddEdit AmsiControl { get { return AmsiAddEdit; } }
        //cakel: 00351 RealPage integration
        public IRealPageAddEdit RealPageControl { get { return RealPageAddEdit; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter = new EfxFramework.Presenters.UserControls.Property.PmsAddEdit(this);

            if (!Page.IsPostBack)
                _presenter.InitializeValues();
        }

        //cakel: BUGID000179
        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}