﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SMSMarketing.ascx.cs" Inherits="RentPaidOnline.UserControls.Property.SMSMarketing" %>

<%@ Register Src="~/ReportingPages/UserControls/PropertyPicker.ascx" TagPrefix="uc1" TagName="PropertyPicker" %>

<asp:UpdatePanel ID="SMSSender" runat="server">
    <Triggers>
         <asp:PostBackTrigger ControlID="SendButton" />
    </Triggers>

    <ContentTemplate>
        <div class="main-container">
            <div class="section-body">
                <div id="MessageArea">
                    <div class="formTint">
                        <div class="formWrapper">
                            <h2>Compose SMS Message</h2>
                            <div class="formThird">
                                <uc1:PropertyPicker runat="server" ID="PropertyPicker" />
                                <asp:HiddenField ID="HiddenPropertyList" runat="server" />
                            </div>
                            <div class="formTwoThirds">
                                <asp:label runat="server" id="lblContent" Text="Content (Maximum Length 160 characters):" />
                                <asp:TextBox runat="server" ID="messagetxt" TextMode="MultiLine" Rows="4" Wrap="true" OnTextChanged="messagetxt_TextChanged" AutoPostBack="True" MaxLength="160" />
                                <asp:Label runat="server" ID="errorlbl" Text="" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="formWhole">
                    <asp:ListBox runat="server" ID="renterDetailslst" Visible="false" />
                    <asp:GridView runat="server" ID="resultsGrid" Visible="false" >
                     <Columns>
                        <asp:TemplateField HeaderText="" >
                            <ItemTemplate>
                                <asp:Label runat="server" ID="renterIDlbl" />
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="" >
                            <ItemTemplate>
                                <asp:Label runat="server" ID="firstNamelbl" />
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="" >
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lastNamelbl" />
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="" >
                            <ItemTemplate>
                                <asp:Label runat="server" ID="errorMessagelbl" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    </asp:GridView>
                </div>

                <div class="button-footer">
                    <div class="main-button-action"> 
                        <asp:Button ID="SendButton" OnClick="SendButton_Click" runat="server" CssClass="btn btn-default footer-button" Text="Send" />
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
