﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EfxSalesAddEdit.ascx.cs" Inherits="Domus.UserControls.Property.EfxSalesAddEdit" %>

<label for="FirstNameTextbox">Contact First Name</label>
<input runat="server" id="FirstNameTextbox" name="FirstNameTextbox" type="text" />
<label for="LastNameTextbox">Contact Last Name</label>
<input runat="server" id="LastNameTextbox" name="LastNameTextbox" type="text" />
<label for="TelephoneNumberTextbox">Contact Phone Number</label>
<input runat="server" id="TelephoneNumberTextbox" name="TelephoneNumberTextbox" type="text" />