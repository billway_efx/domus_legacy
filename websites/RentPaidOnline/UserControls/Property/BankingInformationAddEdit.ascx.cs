﻿using EfxFramework.Interfaces.UserControls.Property;
using System;
using System.Web;
using System.Web.UI;
using System.Data.SqlClient;
using System.Data;

namespace Domus.UserControls.Property
{
    public partial class BankingInformationAddEdit : UserControl, IBankingInformationAddEdit
    {
        private EfxFramework.Presenters.UserControls.Property.BankingInformationAddEdit _presenter;

        public int PropertyId { get; set; }
        public string RentalBankNameText { get { return RentalBankNameTextbox.Value; } set { RentalBankNameTextbox.Value = value; } }
        public string RentalBankNameAccountText { get { return RentalAccountNumberTextbox.Value; } set { RentalAccountNumberTextbox.Value = value; } }
        public string RentalBankNameRoutingText { get { return RentalRoutingNumberTextbox.Value; } set { RentalRoutingNumberTextbox.Value = value; } }
        public string SecurityBankNameText { get { return SecurityBankNameTextbox.Value; } set { SecurityBankNameTextbox.Value = value; } }
        public string SecurityBankNameAccountText { get { return SecurityAccountNumberTextbox.Value; } set { SecurityAccountNumberTextbox.Value = value; } }
        public string SecurityBankNameRoutingText { get { return SecurityRoutingNumberTextbox.Value; } set { SecurityRoutingNumberTextbox.Value = value; } }
        public string FeeBankNameText { get { return FeeBankNameTextbox.Value; } set { FeeBankNameTextbox.Value = value; } }
        public string FeeBankNameAccountText { get { return FeeAccountNumberTextbox.Value; } set { FeeAccountNumberTextbox.Value = value; } }
        public string FeeBankNameRoutingText { get { return FeeRoutingNumberTextbox.Value; } set { FeeRoutingNumberTextbox.Value = value; } }
        //cakel: BUGID00213 added new properties
        public string CCDepositBankNameText { get { return CCDepositBankNameTextbox.Value; } set { CCDepositBankNameTextbox.Value = value; } }
        public string CCDepositAccountNumberText { get { return CCDepositAccountNumberTextbox.Value; } set { CCDepositAccountNumberTextbox.Value = value; } }
        public string CCDepositRoutingNumberText { get { return CCDepositRoutingNumberTextbox.Value; } set { CCDepositRoutingNumberTextbox.Value = value; } }
        ////cakel: BUGID00213 added new properties
        public string CashDepositBankNameText { get { return RentalBankNameTextbox.Value; } set { RentalBankNameTextbox.Value = value; } }
        public string CashDepositAccountNumberText { get { return RentalAccountNumberTextbox.Value; } set { RentalAccountNumberTextbox.Value = value; } }
        public string CashDepositRoutingNumberText { get { return RentalRoutingNumberTextbox.Value; } set { RentalRoutingNumberTextbox.Value = value; } }



        protected void Page_Load(object sender, EventArgs e)
        {
            //cakel:BUGID00213
            if (!Page.IsPostBack)
            {
                
                string _PropID = Request.QueryString["propertyid"];
                if (_PropID != null)
                {
                    GroupSettlementDDL.SelectedValue = GroupSettlementFlagString(_PropID);
                }

               
            }

			//disabled account textboxes if not rpo admin or corpadmin
            //if (!EfxFramework.Helpers.Helper.IsRPOAdmin && !EfxFramework.Helpers.Helper.IsCorporateAdmin)
            //cakel: TASK 00531
			if (!EfxFramework.Helpers.Helper.IsRpoAdmin)
			{
                
                RentalBankNameTextbox.Disabled = true;
				RentalAccountNumberTextbox.Disabled = true;
				RentalRoutingNumberTextbox.Disabled = true;
				SecurityBankNameTextbox.Disabled = true;
				SecurityAccountNumberTextbox.Disabled = true;
				SecurityRoutingNumberTextbox.Disabled = true;
				FeeBankNameTextbox.Disabled = true;
				FeeAccountNumberTextbox.Disabled = true;
				FeeRoutingNumberTextbox.Disabled = true;

                //cakel: BUGID00213 added new fields
                CCDepositBankNameTextbox.Disabled = true;
                CCDepositAccountNumberTextbox.Disabled = true;
                CCDepositRoutingNumberTextbox.Disabled = true;
                GroupSettlementDDL.Enabled = false;
			}
			else
			{
                
				RentalBankNameTextbox.Disabled = false;
				RentalAccountNumberTextbox.Disabled = false;
				RentalRoutingNumberTextbox.Disabled = false;
				SecurityBankNameTextbox.Disabled = false;
				SecurityAccountNumberTextbox.Disabled = false;
				SecurityRoutingNumberTextbox.Disabled = false;
				FeeBankNameTextbox.Disabled = false;
				FeeAccountNumberTextbox.Disabled = false;
				FeeRoutingNumberTextbox.Disabled = false;

                //cakel: BUGID00213 added new fields
                CCDepositBankNameTextbox.Disabled = false;
                CCDepositAccountNumberTextbox.Disabled = false;
                CCDepositRoutingNumberTextbox.Disabled = false;
                GroupSettlementDDL.Enabled = true;
                
			}

            _presenter = new EfxFramework.Presenters.UserControls.Property.BankingInformationAddEdit(this);

            if (!Page.IsPostBack)
                _presenter.IntializeValues();
        }

        //cakel: BUGID00213
        public string GroupSettlementFlagString(string propID)
        {
            SqlDataReader reader = DLAccess.GetPropertyDetailsByPropertyID(propID);
            reader.Read();
            if (reader.HasRows)
            {

                string GetFlag = reader["GroupSettlementPaymentFlag"].ToString();
                if (GetFlag == "True")
                {
                    GetFlag = "1";
                }
                else
                {
                    GetFlag = "0";
                }

                return GetFlag;
            }
            else
            {
                return null;
            }

        }

        //cakel:BUGID00213
        protected void GroupSettlementDDL_SelectedIndexChanged(object sender, EventArgs e)
        {


            string _PropID = Request.QueryString["propertyid"];

            //cakel: BUGID000213 rev2
            if (_PropID != null)
            {

                string Selected = GroupSettlementDDL.SelectedValue.ToString();
                string SqlString = "Update Property Set GroupSettlementPaymentFlag = " + Selected + " Where propertyid = " + _PropID;

                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                SqlCommand com = new SqlCommand(SqlString, con);
                com.CommandType = CommandType.Text;
                try
                {
                    con.Open();
                    com.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //con.Close();
                    throw ex;
                }
                //cakel: BUGID00280
                finally
                {
                    con.Close();
                }
            }

        }

    }
}