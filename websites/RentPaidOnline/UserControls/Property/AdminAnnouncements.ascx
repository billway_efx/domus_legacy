﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminAnnouncements.ascx.cs" Inherits="Domus.UserControls.Property.AdminAnnouncements" %>
<%@ Register Src="~/ReportingPages/UserControls/PropertyPicker.ascx" TagPrefix="uc1" TagName="PropertyPicker" %>
<style>
    .hide {
        display: none;
    }
</style>
<asp:UpdatePanel ID="AnnouncementUpdate" runat="server">

    <ContentTemplate>
		<asp:ValidationSummary ID="uxAnnouncementSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="AnnouncementTab" />
        <div class="formWrapper">
                 
                <div>
                    <br />
                    <asp:HiddenField ID="HiddenPropertyList" runat="server" />
                </div>
            <h2>Announcements</h2>
            <div class="formHalf">
                <!-- Announcement Title -->

                <asp:Label ID="AnnouncementTitleLabel" Text="Announcement Title" runat="server" />
                <asp:TextBox ID="AnnouncementTitleTextBox" runat="server" CssClass="form-control" />
            </div>
            <!-- CMallory - Task 00462 - Added Company and Property pickers that a lot of the rest of the site already uses as well as a few other controls -->
            <td>
                   <uc1:PropertyPicker runat="server" ID="PropertyPicker" />
            </td>
            <div class="formHalf">
                <asp:Label Text="Click the icon to the right to select a date" runat="server"></asp:Label>
                <asp:ImageButton ID="CalendarPic" runat="server" Height="17px" Width="21px" OnClick="CalendarPic_Click" ImageUrl="~/Images/Calendar.png" />
                <asp:TextBox ID="ExpDate" runat="server" ></asp:TextBox>
                <asp:Calendar ID="Calendar" runat="server" OnSelectionChanged="Calendar_SelectionChanged" Visible="false"></asp:Calendar>
            </div>
            <div class="formWhole">
                <!-- Announcement Body -->
                <asp:Label ID="AnnouncementBodyLabel" Text="Announcement Body" runat="server" />
                <textarea id="AnnouncementBodyTextArea" runat="server" CssClass="form-control" style="height:400px;" />
            </div>

            <div class="formHalf">
                <asp:Label ID="AnnouncementUrlLabel" Text="URL or Hyperlink" runat="server" />
                <asp:TextBox ID="AnnouncementUrlTextBox" runat="server" CssClass="form-control" />
            </div>

            <div class="formHalf noLabel">
                <asp:CheckBox ID="PostAllPropertiesCheckBox" Text=" &nbsp; Post to All My Properties" runat="server" />
            </div>
        </div>
        

        <div class="formWrapper">
            <div class="formWhole">
                <h2><asp:Label ID="AllPostedGridLabel" Text="All Posted Announcements" runat="server" /></h2>


<%--                <asp:GridView ID="AllPostedAnnouncementsGrid" runat="server" AutoGenerateColumns="false" GridLines="None" Width="100%" CssClass="table-striped">
                    <Columns>
                        <asp:TemplateField HeaderStyle-CssClass="hide" ItemStyle-CssClass="hide">
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddenAnnouncementId" runat="server" Value='<%#Bind("AnnouncementId")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Title" DataField="AnnouncementTitle" ItemStyle-Width="80%" />
						<asp:TemplateField HeaderText="Manage" HeaderStyle-Width="20%">
							<ItemTemplate>
								<asp:LinkButton ID="editAnnouncementLink" runat="server" CausesValidation="False" CommandName="Select" Text="Edit"><span data-icon="&#x24" aria-hidden="true"></span></asp:LinkButton>
								<asp:LinkButton ID="deleteAnnouncementLink" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"><span data-icon="&#x2b" aria-hidden="true"></span></asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
                    </Columns>
                </asp:GridView>--%>
                <!--CMallory - Task  00462 - Commented out previous grid and added the one below.-->
                <div class="padding">
                 <!-- CMallory - Layout Change - Added btn-footer css tag to button -->
                <asp:Button ID="ViewAnnouncements" runat="server" Text="View Announcements" CssClass="btn btn-primary btn-footer" OnClick="ViewAnnouncements_Click"/>
                </div>
                    <asp:GridView ID="AllPostedAnnouncementsGrid" runat="server" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="True" PageSize="30" CellPadding="10" Width="98%" GridLines="Horizontal" DataKeyNames="PropertyID,AnnouncementTitle,AnnouncementText,AnnouncementDate">
                    <Columns>
                        <asp:BoundField DataField="PropertyName" HeaderText="Property Name" ReadOnly="True" SortExpression="PropertyName" />
                        <asp:BoundField DataField="AnnouncementTitle" HeaderText="Announcement Title" ReadOnly="True" SortExpression="AnnouncementTitle" />
                        <asp:BoundField DataField="AnnouncementText" HeaderText="Announcement Text" SortExpression="AnnouncementText" ReadOnly="True" />
                        <asp:BoundField DataField="AnnouncementDate" HeaderText="Announcement Date" ReadOnly="True" SortExpression="AnnouncementDate" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="ExpirationDate" HeaderText="Expiration Date" SortExpression="ExpirationDate" ReadOnly="True" DataFormatString="{0:d}"/>
                        <asp:TemplateField HeaderText="Active" SortExpression="Active">
                            <ItemTemplate><%# (Boolean.Parse(Eval("IsActive").ToString())) ? "Yes" : "No" %></ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="ReportTableHeader" />
                    <PagerSettings PageButtonCount="10" FirstPageText="First" LastPageText="Last" NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" Mode="NumericFirstLast" />
                    <PagerStyle Font-Size="16px" HorizontalAlign="Right" />
                    <RowStyle CssClass="ReportTableData" />
                    <SelectedRowStyle BackColor="#E6E6E6" />
                </asp:GridView>
                
                <asp:SqlDataSource ID="AnnouncementData" runat="server" ConnectionString="<%$ ConnectionStrings:ReportsConnection %>" SelectCommand="usp_Announcement_GetAllAnnouncementsByPropertyIDs" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenPropertyList" Name="PropertyList" PropertyName="Value" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>

            <!-- All Posted Announcements  -->
            <asp:HiddenField ID="CanEditGrid" runat="server" />
            <asp:HiddenField ID="IdValue" runat="server" />

        </div>
        <!-- Footer -->
        <div class="button-footer">
            <div class="main-button-action">
                <!-- CMallory - Layout Change - Added btn-footer css tag to button -->
                <asp:Button ID="SaveButton" OnClick="SaveButton_Click" Text="Save" runat="server" ValidationGroup="AnnouncementTab" CssClass="btn btn-primary btn-footer" />
            </div>
            <div class="clear"></div>
        </div>

    </ContentTemplate>

</asp:UpdatePanel>

<asp:RequiredFieldValidator ID="RequiredAnnouncementTitleValidation"
    ControlToValidate="AnnouncementTitleTextBox"
    Display="None"
    ErrorMessage="The Announcement Title is Required"
    runat="server"
    ValidationGroup="AnnouncementTab" />

<asp:RequiredFieldValidator ID="RequiredAnnouncementBodyValidation"
    ControlToValidate="AnnouncementBodyTextArea"
    Display="None"
    ErrorMessage="The Announcement Body is Required"
    runat="server"
    ValidationGroup="AnnouncementTab" />

<asp:RegularExpressionValidator ID="ValidURl"
    runat="server"
    ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"
    ErrorMessage="Please enter a valid hyperlink formatted as https://www.example.com or http://www.example.com"
    Display="None"
    ValidationGroup="AnnouncementTab"
    ControlToValidate="AnnouncementUrlTextBox" />
<!-- CMallory - Task 00462 - Added validation for Date textbox -->
<asp:RegularExpressionValidator ID="RegularExpressionValidator4" 
    runat="server" 
    ForeColor="Red"
    ControlToValidate="ExpDate" 
    ValidationGroup="AnnouncementTab" 
    ErrorMessage="Date format in DD/MM/YYYY HH:MM"
    ValidationExpression= "^(((((0[13578])|([13578])|(1[02]))[\-\/\s]?((0[1-9])|([1-9])|([1-2][0-9])|(3[01])))|((([469])|(11))[\-\/\s]?((0[1-9])|([1-9])|([1-2][0-9])|(30)))|((02|2)[\-\/\s]?((0[1-9])|([1-9])|([1-2][0-9]))))[\-\/\s]?\d{4})(\s(((0[1-9])|([1-9])|(1[0-2]))\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|am|pm]{2,2})))?$" />
   

<%--<asp:RangeValidator ID="RngAge" 
    ValidationGroup="FormVal" 
    runat="server" 
    ControlToValidate="tbx_DOB" 
    Display="Dynamic" 
    ErrorMessage="" 
    MinimumValue="1900/1/1" 
    MaximumValue = "2099/12/31" Type="Date">*</asp:RangeValidator>--%>