﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BankingInformationAddEdit.ascx.cs" Inherits="Domus.UserControls.Property.BankingInformationAddEdit" %>
<div class="formWrapper">
    <!--cakel: BUGID00213 - Updated H2 tag to echeck Deposits from Rental Deposits -->
    <h2>eCheck Deposits</h2>
    <div class="formThird">
        <label>Bank Name</label>
        <input runat="server" id="RentalBankNameTextbox" name="RentalBankNameTextbox" type="text" class="form-control" />
    </div>

    <div class="formThird">
        <label>Account Number</label>
        <!--cakel: BUGID00072 - Updated max length from 15 to 17 -->
        <input runat="server" id="RentalAccountNumberTextbox" name="RentalAccountNumberTextbox" type="text" class="form-control" maxlength="17" />
    </div>

    <div class="formThird">
        <label>Routing Number</label>
        <input runat="server" id="RentalRoutingNumberTextbox" name="RentalRoutingNumberTextbox" type="text" class="form-control" maxlength="9" />
    </div>
    <%-- cakel: BUGID00213 - Updated Name For Tag --%>
    <%--<h2>Lease Up</h2>--%>
    <h2>Billing Account</h2>
    <div class="formThird">
        <label>Bank Name</label>
        <input runat="server" id="SecurityBankNameTextbox" name="SecurityBankNameTextbox" type="text" class="form-control" />
    </div>

    <div class="formThird">
        <label>Account Number</label>
        <!--cakel: BUGID00072 - Updated max length from 15 to 17 -->
        <input runat="server" id="SecurityAccountNumberTextbox" name="SecurityAccountNumberTextbox" type="text" class="form-control" maxlength="17" />
    </div>
    
    <div class="formThird">
        <label>Routing Number</label>
        <input runat="server" id="SecurityRoutingNumberTextbox" name="SecurityRoutingNumberTextbox" type="text" class="form-control" maxlength="9" />
    </div>

     <%-- cakel: BUGID00213 - Updated Name For Tag --%>
    <%--<h2>Billing Account</h2>--%>
    <h2>Applicant Portal Account</h2>
    <div class="formThird">
        <label>Bank Name</label>
        <input runat="server" id="FeeBankNameTextbox" name="FeeBankNameTextbox" type="text" class="form-control" />
    </div>
    
    <div class="formThird">
        <label>Account Number</label>
        <!--cakel: BUGID00072 - Updated max length from 15 to 17 -->
        <input runat="server" id="FeeAccountNumberTextbox" name="FeeAccountNumberTextbox" type="text" class="form-control" maxlength="17" />
    </div>
    
    <div class="formThird">
        <label>Routing Number</label>
        <input runat="server" id="FeeRoutingNumberTextbox" name="FeeRoutingNumberTextbox" type="text" class="form-control" maxlength="9" />
    </div>

<!--cakel: BUGID00213 Added New section for Credit Card Deposits-->
<h2>Credit Card Deposits</h2>
<div class="formThird">  
<label>Bank Name</label>     
<input runat="server" id="CCDepositBankNameTextbox" name="CCDepositBankNameTextbox" type="text" class="form-control" />
</div>

<div class="formThird">
<label>Account Number</label>
<input runat="server" id="CCDepositAccountNumberTextbox" name="CCDepositAccountNumberTextbox" type="text" class="form-control" maxlength="17" />
</div>
 
<div class="formThird">
<label>Routing Number</label>     
<input runat="server" id="CCDepositRoutingNumberTextbox" name="CCDepositRoutingNumberTextbox" type="text" class="form-control" maxlength="9" />
</div>

<div class="formThird">
    <asp:Label ID="Label1" runat="server" Text="Group Settlement"></asp:Label>
    <asp:DropDownList ID="GroupSettlementDDL" runat="server" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="GroupSettlementDDL_SelectedIndexChanged">
        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
        <asp:ListItem Text="No" Value="0"></asp:ListItem>
    </asp:DropDownList>
</div>
<!--BUGID00213 - End ** Added New section for Credit Card Deposits -->



</div><!--End Form Wrapper -->