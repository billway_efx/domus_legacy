﻿using EfxFramework;
using EfxFramework.Interfaces.UserControls.Property;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web;

namespace Domus.UserControls.Property
{ 
    public partial class PropertyDetailsAddEdit : UserControl, IEfxAdministratorPropertyDetailsAddEdit
    {
        private EfxFramework.Presenters.UserControls.Property.PropertyDetailsAddEditBase _Presenter;

        public int PropertyId { get; set; }
        public string PmCompanySelectedValue { get { return PmCompanyDropdown.SelectedValue; } set { PmCompanyDropdown.SelectedValue = value; } }
        public string PropertyCodeText { get { return PropertyCodeTextbox.Value; } set { PropertyCodeTextbox.Value = value; } }
        public string PropertyNameText { get { return PropertyNameTextbox.Value; } set { PropertyNameTextbox.Value = value; } }
        //cakel: TASK 00498
        public string PropertyNameTwoText { get { return PropertyNameTwoTextbox.Value; } set { PropertyNameTwoTextbox.Value = value; } }
        public string NumberOfUnitsText { get { return NumberOfUnitsTextbox.Value; } set { NumberOfUnitsTextbox.Value = value; } }
        public string PropertyAddressText { get { return PropertyAddressTextbox.Value; } set { PropertyAddressTextbox.Value = value; } }
        public string PropertyAddress2Text { get { return PropertyAddress2Textbox.Value; } set { PropertyAddress2Textbox.Value = value; } }
        public string CityText { get { return CityTextbox.Value; } set { CityTextbox.Value = value; } }
        public DropDownList StateList { get { return StateDropdown; } }
        public DropDownList TimeZoneList { get { return uxTimeZoneDropDown; } }
        public string PostalCodeText { get { return PostalCodeTextbox.Value; } set { PostalCodeTextbox.Value = value; } }
        public string MainTelephoneText { get { return MainTelephoneTextbox.Value; } set { MainTelephoneTextbox.Value = value; } }
        public string AlternateTelephoneText { get { return AltTelephoneTextbox.Value; } set { AltTelephoneTextbox.Value = value; } }
        public string FaxNumberText { get { return FaxNumberTextbox.Value; } set { FaxNumberTextbox.Value = value; } }
        public string OfficeEmailText { get { return OfficeEmailTextbox.Value; } set { OfficeEmailTextbox.Value = value; } }
        // Patrick Whittingham - 9/2/15 - task #371 : add notification email address 
        public string NotificationEmailText { get { return NotificationEmailTextbox.Value; } set { NotificationEmailTextbox.Value = value; } }

        public string AverageRentText { get { return AverageRentTextbox.Value; } set { AverageRentTextbox.Value = value; } }
        public string ConvenienceFeeText { set { ConvenienceFeeTextbox.Value = value; } }
        public bool LineItemDetailChecked { get { return LineItemDetailCheckBox.Checked; } set { LineItemDetailCheckBox.Checked = value; } }
        public bool RentReportersVerifiedChecked { get { return RentReportersVerifiedCheckBox.Checked; } set { RentReportersVerifiedCheckBox.Checked = value; } }
        public bool AllowPartialPayments { get { return AllowPartialPaymentsCheckBox.Checked; } set { AllowPartialPaymentsCheckBox.Checked = value; } }

        //cakel: AddedBugID 007
        public string NumOfPrePayDays { get { return PrePayDaysDL.SelectedValue; } set { PrePayDaysDL.SelectedValue = value; } }
        public string PaymentDueDay { get { return PaymentDueDDL.SelectedValue; } set { PaymentDueDDL.SelectedValue = value; } }
        public bool AllowPrePayments { get { return AllowPrePaymentsCK.Checked; } set { AllowPrePaymentsCK.Checked = value; } }


		public bool AcceptACHPayments { get { return AcceptACHPaymentsCheckBox.Checked; } set { AcceptACHPaymentsCheckBox.Checked = value; } }
		public bool AcceptCCPayments { get { return AcceptCreditCardPaymentsCheckBox.Checked; } set { AcceptCreditCardPaymentsCheckBox.Checked = value; } }
		public bool PNMEnabled { get { return PNMEnabledCheckBox.Checked; } set { PNMEnabledCheckBox.Checked = value; } }

        //cakel: BUGID00019 - 
        public bool AllowPartialPaymentsResidentPortal { get { return AllowPartialPaymentsResidentPortalCkBx.Checked; } set { AllowPartialPaymentsResidentPortalCkBx.Checked = value; } }
        //CMallory - Task 00425
        public bool DisableAllPayments { get { return DisableAllPaymentsCheckBox.Checked; } set { DisableAllPaymentsCheckBox.Checked = value; } } 
         //CMallory - Task 00554
        public bool DisablePayByText { get { return DisablePayByTextCheckBox.Checked; } set { DisablePayByTextCheckBox.Checked = value; } }
        //CMallory - Task 00166
        public bool PropertyIsPublic { get { return PropertyIsPublicCheckBox.Checked; } set { PropertyIsPublicCheckBox.Checked = value; } }
        public List<Company> PmCompanyDataSource
        {
            set
            {
                PmCompanyDropdown.DataSource = value;
                PmCompanyDropdown.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Collier Mallory - Task 00508
            //Added logic to check a session variable to see if a user is an RPO Admin.  If so, they get access to change the Average Rent for a property.
            bool isRPOAdmin;

            if (HttpContext.Current.Session["IsRPOAdmin"] != null)
            {
                isRPOAdmin = (bool)HttpContext.Current.Session["IsRPOAdmin"];
            }

            else
            {
                isRPOAdmin = false;
            }

            //cakel:10/06/2016 UPdated code not use Session
            if (!EfxFramework.Helpers.Helper.IsRpoAdmin)
            {
                AverageRentTextbox.Disabled = true;
            }
          
            if (!Page.IsPostBack)
            {
                //cakel: BugID007 Populate Drop Down List for Prepay days when Page loads
                PopulatePrePayDays();
                //cakel: BugID007 Populate Payment due day list
                PopulatePaymentDueDays();
            }

			AllowPartialPaymentsCheckBox.Enabled = EfxFramework.Helpers.Helper.IsRpoAdmin;
            //cakel: BUGID00019
            AllowPartialPaymentsResidentPortalCkBx.Enabled = EfxFramework.Helpers.Helper.IsRpoAdmin;
			AcceptACHPaymentsCheckBox.Enabled = EfxFramework.Helpers.Helper.IsRpoAdmin;
			AcceptCreditCardPaymentsCheckBox.Enabled = EfxFramework.Helpers.Helper.IsRpoAdmin;
			PNMEnabledCheckBox.Enabled = EfxFramework.Helpers.Helper.IsRpoAdmin;

            //Salcedo - 2/23/2014 - only enable this drop down for RPO managers
            PmCompanyDropdown.Enabled = EfxFramework.Helpers.Helper.IsRpoAdmin;
            
            _Presenter = EfxFramework.Presenters.UserControls.Property.PropertyDetailsAddEditFactory.GetPropertyDetailsAddEdit(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();

            //Salcedo - 2/23/2014 - and if we're disabling the control, we also need to get rid of the items
            //in the control that are not selected. Otherwise, view source in the browser reveals our client list.
            if (!EfxFramework.Helpers.Helper.IsRpoAdmin)
            {
                ListItem SelectedItem = PmCompanyDropdown.SelectedItem;
                int OriginalCount = PmCompanyDropdown.Items.Count;
                for (int i = OriginalCount-1; i >= 0; i--)
                {
                    if (PmCompanyDropdown.Items[i] != SelectedItem)
                        PmCompanyDropdown.Items.RemoveAt(i);
                }
            }

            //cakel: BugID007Display Prepay day picker
            if (AllowPrePaymentsCK.Checked == true)
            {
                PrePayDaysDL.Visible = true;
                DayAllowedlbl.Visible = true;
            }
            else
            {
                PrePayDaysDL.Visible = false;
                DayAllowedlbl.Visible = false;
            }
            //CMallory - Task 0554 - Hide Pay By Text
            DisablePayByTextCheckBox.Visible = false;
        }

        //cakel: BugID007 Disable control if Allow pre payments check box is disabled
        public void EnableDayDL()
        {
            if (AllowPrePaymentsCK.Enabled == true)
            {
                PrePayDaysDL.Enabled = true;
                DayAllowedlbl.Visible = true;
            }
            else
            {
                PrePayDaysDL.Enabled = false;
                DayAllowedlbl.Visible = false;
            }
        }

        //cakel: BugID007 populate dropdown list
        public void PopulatePrePayDays()
        {
            int myint = 0;
            //Create loop for prepay days: 28 days before
            for (int i = 0; i < 28; i++)
            {
                myint += 1;
                //Add drop down items
                PrePayDaysDL.Items.Add(new ListItem(myint.ToString(), myint.ToString()));
            }
        }

        //cakel: BugID007 populate dropdown ist for payment due days
        public void PopulatePaymentDueDays()
        {
            PaymentDueDDL.Items.Add(new ListItem("1st", "1"));
            PaymentDueDDL.Items.Add(new ListItem("2nd", "2"));
            PaymentDueDDL.Items.Add(new ListItem("3rd", "3"));
            PaymentDueDDL.Items.Add(new ListItem("4th", "4"));
            PaymentDueDDL.Items.Add(new ListItem("5th", "5"));
            PaymentDueDDL.Items.Add(new ListItem("6th", "6"));
            PaymentDueDDL.Items.Add(new ListItem("7th", "7"));
            PaymentDueDDL.Items.Add(new ListItem("8th", "8"));
            PaymentDueDDL.Items.Add(new ListItem("9th", "9"));
            PaymentDueDDL.Items.Add(new ListItem("10th", "10"));
            PaymentDueDDL.Items.Add(new ListItem("11th", "11"));
            PaymentDueDDL.Items.Add(new ListItem("12th", "12"));
            PaymentDueDDL.Items.Add(new ListItem("13th", "13"));
            PaymentDueDDL.Items.Add(new ListItem("14th", "14"));
            PaymentDueDDL.Items.Add(new ListItem("15th", "15"));
            PaymentDueDDL.Items.Add(new ListItem("16th", "16"));
            PaymentDueDDL.Items.Add(new ListItem("17th", "17"));
            PaymentDueDDL.Items.Add(new ListItem("18th", "18"));
            PaymentDueDDL.Items.Add(new ListItem("19th", "19"));
            PaymentDueDDL.Items.Add(new ListItem("20th", "20"));
            PaymentDueDDL.Items.Add(new ListItem("21st", "21"));
            PaymentDueDDL.Items.Add(new ListItem("22th", "22"));
            PaymentDueDDL.Items.Add(new ListItem("23th", "23"));
            PaymentDueDDL.Items.Add(new ListItem("24th", "24"));
            PaymentDueDDL.Items.Add(new ListItem("25th", "25"));
        }
    }
}