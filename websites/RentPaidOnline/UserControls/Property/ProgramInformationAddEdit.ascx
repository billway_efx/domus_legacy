﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProgramInformationAddEdit.ascx.cs" Inherits="Domus.UserControls.Property.ProgramInformationAddEdit" %>

<div class="formWrapper">
    <div class="formThird">
         <!--cakel: 10/19/2016 - added new item -->
        <asp:DropDownList ID="ProgramDropdown" runat="server" AutoPostBack="True" CssClass="form-control">
            <asp:ListItem Text="-- Please Select a Program --" Value="-1" />
            <asp:ListItem Text="Air Program" Value="1" />
            <asp:ListItem Text="Air-2 Legacy" Value="4" />
            <asp:ListItem Text="Property Paid Program" Value="3" />
            <asp:ListItem Text="Spa Program" Value="2" />
            <asp:ListItem Text="Unassigned" Value="0" />
        </asp:DropDownList>
    </div>

    <!--This tag if a completion of one in the PmsAddEdit.ascx file-->
    <br /><br />
    <div class="formWhole">
        <!--cakel: BUGID00242 Hide fees table -->
        <div id="Feetbl" runat="server" visible ="false">

        
        <asp:MultiView ID="ProgramsMultiview" runat="server">
            <asp:View ID="NoneSelected" runat="server" />
            <asp:View ID="PropertyPaidProgramView" runat="server">
                <asp:GridView ID="PropertyPaidGrid" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" CssClass="table-striped">
                    <Columns>
                        <asp:BoundField DataField="SetupFee" HeaderText="Set Up Fee" ItemStyle-Width="20%" />
                        <asp:BoundField DataField="MonthlyFeeToPmc" HeaderText="Monthly Fee to PMC" ItemStyle-Width="20%" />
                        <asp:BoundField DataField="InterchangeCostCcToPmc" HeaderText="Interchange Cost CC to PMC" ItemStyle-Width="20%" />
                        <asp:BoundField DataField="AchCostToPmc" HeaderText="ACH Single and Recurring Cost to PMC" ItemStyle-Width="20%" />
                        <asp:BoundField DataField="AppAndDepositFee" HeaderText="App Fee and Deposit Fee" ItemStyle-Width="20%" />
                    </Columns>
                </asp:GridView>
            </asp:View>

            <asp:View ID="AirProgramView" runat="server">
                <asp:GridView ID="AirProgramGrid" runat="server" AutoGenerateColumns="false" GridLines="None" Width="100%" CssClass="table-striped">
                    <Columns>
                        <asp:BoundField DataField="SetupFee" HeaderText="Set Up Fee" ItemStyle-Width="20%" />
                        <asp:BoundField DataField="UnitCount" HeaderText="Unit Count" ItemStyle-Width="20%" />
                        <asp:BoundField DataField="AllInclusiveRate" HeaderText="All Inclusive Rate" ItemStyle-Width="20%" />
                        <asp:BoundField DataField="RentByCashFee" HeaderText="RentByCash Fee" ItemStyle-Width="20%" />
                        <asp:BoundField DataField="AppAndDepositFee" HeaderText="App Fee and Deposit Fee" ItemStyle-Width="20%" />
                    </Columns>
                </asp:GridView>
            </asp:View>

            <asp:View ID="SpaProgramView" runat="server">
                <asp:GridView ID="SpaProgramGrid" runat="server" AutoGenerateColumns="false" GridLines="None" Width="100%" CssClass="table-striped">
                    <Columns>
                        <asp:BoundField DataField="SetupFee" HeaderText="Set Up Fee" ItemStyle-Width="15%" />
                        <asp:BoundField DataField="MonthlyFeeToPmc" HeaderText="Monthly Fee to PMC" ItemStyle-Width="15%" />
                        <asp:BoundField DataField="CcFeeToResident" HeaderText="Convenience Fee to Resident CC" ItemStyle-Width="15%" />
                        <asp:BoundField DataField="AchFeeToResidentSingle" HeaderText="Convenience Fee to Resident ACH Single Use" ItemStyle-Width="15%" />
                        <asp:BoundField DataField="AchFeeToResidentAuto" HeaderText="Convenience Fee to Resident ACH Auto-Pay" ItemStyle-Width="15%" />
                        <asp:BoundField DataField="AppAndDepositFee" HeaderText="App Fee and Deposit Fee" ItemStyle-Width="25%" />
                    </Columns>
                </asp:GridView>
                <br />
                <p>* Credit Card Convenience fee based off of properties average rental amount</p>
            </asp:View>
        </asp:MultiView>
            </div>
    </div>
</div>
