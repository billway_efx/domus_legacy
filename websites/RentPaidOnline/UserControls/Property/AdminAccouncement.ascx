﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminAccouncement.ascx.cs" Inherits="RentPaidOnline.UserControls.Property.AdminAccouncement" %>

<style>
    .hide {
        display: none;
    }
</style>

<asp:UpdatePanel ID="AnnouncementUpdate1" runat="server">

    <ContentTemplate>
		<asp:ValidationSummary ID="uxAnnouncementSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="AnnouncementTab" />
        <div class="formWrapper">

            <h2>Announcements</h2>
            <!-- Patrick Whittingham - 8/14/15 - josewhishlist 2a -->
            <div class="formHalf">
                <label for="select">Property</label>
                <select name="selectedProperty" class="form-control" size="4" multiple ng-model="selectedProperty" ng-options="property.PropertyName for property in properties" required></select>
                <span class="errorText" ng-show="calendarEvent.selectedProperty.$error.required">Please select a property</span>
            </div>

            <div class="formHalf">
                <!-- Announcement Title -->
                <asp:Label ID="AnnouncementTitleLabel" Text="Announcement Title" runat="server" />
                <asp:TextBox ID="AnnouncementTitleTextBox" runat="server" CssClass="form-control" />
            </div>


            <div class="formWhole">
                <!-- Announcement Body -->
                <asp:Label ID="AnnouncementBodyLabel" Text="Announcement Body" runat="server" />
                <textarea id="AnnouncementBodyTextArea" runat="server" CssClass="form-control" style="height:400px;" />
            </div>

            <div class="formHalf">
                <asp:Label ID="AnnouncementUrlLabel" Text="URL or Hyperlink" runat="server" />
                <asp:TextBox ID="AnnouncementUrlTextBox" runat="server" CssClass="form-control" />
            </div>

            <div class="formHalf noLabel">
                <asp:CheckBox ID="PostAllPropertiesCheckBox" Text=" &nbsp; Post to All My Properties" runat="server" />
            </div>
        </div>
        

        <div class="formWrapper">
            <div class="formWhole">
                <h2><asp:Label ID="AllPostedGridLabel" Text="All Posted Announcements1" runat="server" /></h2>


                <asp:GridView ID="AllPostedAnnouncementsGrid" runat="server" AutoGenerateColumns="false" GridLines="None" Width="100%" CssClass="table-striped">
                    <Columns>
                        <asp:TemplateField HeaderStyle-CssClass="hide" ItemStyle-CssClass="hide">
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddenAnnouncementId" runat="server" Value='<%#Bind("AnnouncementId")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Title" DataField="AnnouncementTitle" ItemStyle-Width="80%" />
						<asp:TemplateField HeaderText="Manage" HeaderStyle-Width="20%">
							<ItemTemplate>
								<asp:LinkButton ID="editAnnouncementLink" runat="server" CausesValidation="False" CommandName="Select" Text="Edit"><span data-icon="&#x24" aria-hidden="true"></span></asp:LinkButton>
								<asp:LinkButton ID="deleteAnnouncementLink" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"><span data-icon="&#x2b" aria-hidden="true"></span></asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>

            <!-- All Posted Announcements  -->
            <asp:HiddenField ID="CanEditGrid" runat="server" />
            <asp:HiddenField ID="IdValue" runat="server" />

        </div>
        <!-- Footer -->
        <div class="button-footer">
            <div class="main-button-action">
                <asp:Button ID="SaveButton" Text="Save" runat="server" ValidationGroup="AnnouncementTab" CssClass="btn btn-primary" />
            </div>
            <div class="clear"></div>
        </div>

    </ContentTemplate>

</asp:UpdatePanel>

<asp:RequiredFieldValidator ID="RequiredAnnouncementTitleValidation"
    ControlToValidate="AnnouncementTitleTextBox"
    Display="None"
    ErrorMessage="The Announcement Title is Required"
    runat="server"
    ValidationGroup="AnnouncementTab" />

<asp:RequiredFieldValidator ID="RequiredAnnouncementBodyValidation"
    ControlToValidate="AnnouncementBodyTextArea"
    Display="None"
    ErrorMessage="The Announcement Body is Required"
    runat="server"
    ValidationGroup="AnnouncementTab" />

<asp:RegularExpressionValidator ID="ValidURl"
    runat="server"
    ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"
    ErrorMessage="Please enter a valid hyperlink formatted as https://www.example.com or http://www.example.com"
    Display="None"
    ValidationGroup="AnnouncementTab"
    ControlToValidate="AnnouncementUrlTextBox" />
