﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Collections;
using System.Diagnostics;
using EfxFramework;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System.Data.SqlTypes;
using EfxFramework.ExtensionMethods;
using EfxFramework.PaymentMethods.Ach;
using Mb2x.Data;
using EfxFramework.Interfaces;
using EfxFramework.Interfaces.UserControls.PaymentControlsV2;
using EfxFramework.Presenters;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;

using EfxFramework.Logging;
using RPO.Helpers;
using RPO;
using System.Data.SqlClient;



namespace Domus.UserControls.Property
{
    // UserControl System.Web.UI.UserControl
    public partial class PropertiesStaff : System.Web.UI.UserControl
    {

        private int? _CurrentPropertyId;
        public DropDownList StaffList { get { return PropertyStaffDropDown; } }
        public string PmCompanySelectedValue { get { return PropertyStaffDropDown.SelectedValue; } set { PropertyStaffDropDown.SelectedValue = value; } }


        // get-set current property id
        public int CurrentPropertyId
        {
            get
            {
                if (_CurrentPropertyId.HasValue)
                    return _CurrentPropertyId.Value;

                _CurrentPropertyId = Request.QueryString["propertyId"].ToNullInt32();

                if (!_CurrentPropertyId.HasValue)
                    _CurrentPropertyId = 0;

                return _CurrentPropertyId.Value;
            }

            set { _CurrentPropertyId = value; }
        }


        // page load task#: joselist-1a - Patrick Whittingham 7-20-15
        protected void Page_Load(object sender, EventArgs e)
        {
            var vUser = false;
            if (EfxFramework.Helpers.Helper.IsRpoAdmin || EfxFramework.Helpers.Helper.IsCorporateAdmin )
            {
                vUser = true;
            }
            PropertyStaffDropDown.Visible = false;
            var _PropertyId = Request.QueryString["propertyId"].ToNullInt32();
            var vCount = PropertyStaffDropDown.Items.Count;
            if (_PropertyId != null && vUser == true && vCount == 1)
            {
                var allStaff = EfxFramework.PropertyStaff.GetStaffListByProperty(_PropertyId.Value);
                PropertyStaffDropDown.DataSource = allStaff;
                if (allStaff.Count > 0)
                {
                    PropertyStaffDropDown.DataBind();
                    PropertyStaffDropDown.Visible = true;
                }
            }
        }


    }
}
