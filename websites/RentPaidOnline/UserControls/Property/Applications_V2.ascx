﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Applications_V2.ascx.cs" Inherits="Domus.UserControls.Property.Applications_V2" %>



<div class="formWrapper">
    <h2>Upload Property Application </h2>
      <p>Upload Your Application for potential residents to download, fill out and upload.</p>

     <div class="formWhole well">
        <div class="formThird">
 

            <h3>Upload New Property Application</h3>
            <label>Application Name</label>
            <asp:FileUpload ID="ApplicationUpload" runat="server" CssClass="btn btn-default" /><br />
             <asp:Button ID="UploadButton" runat="server" Text="Upload" CausesValidation="False" CssClass="btn btn-default" OnClick="UploadButton_Click" />

    </div>
    <div class="formThird">


          <h3>Current Application</h3>
       
        <asp:Button ID="Button2" runat="server" Text="View Property Application" CssClass="btn btn-sm" OnClick="Button2_Click" CausesValidation="False" />
        <br />
        <br />
        <hr />
            <asp:Button ID="Button1" runat="server" Text="Delete Application" CssClass="btn btn-sm" OnClick="Button1_Click" style="height: 26px" />
 
    </div>
    </div>
    <div class="formWhole">
        <div class="formWhole">
            <h3 class="newSection">New Applicant Details</h3>
    
        </div>

        <div class="formHalf">
           
            <asp:Label ID="Label2" runat="server" Text="Upload Application"></asp:Label>
            <asp:FileUpload ID="ApplicationFileUpload" runat="server" CssClass="form-control" /><br />
            <div class="formThird">
                  <asp:Button ID="Button3" runat="server" Text="Clear File" CssClass="btn btn-default" CausesValidation="False" OnClick="Button3_Click" />
            </div>
          <div class="formThird">
              <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                  <ContentTemplate>
                       <asp:Button ID="ViewAppbtn" runat="server" Text="View Application" CssClass="btn btn-default" Visible="False" CausesValidation="False" OnClick="ViewAppbtn_Click" />
                  </ContentTemplate>
                  <Triggers>
                      <asp:PostBackTrigger ControlID="ViewAppbtn" />
                  </Triggers>
              </asp:UpdatePanel>
             
          </div>



        </div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>


                 <asp:HiddenField ID="CurrentApplicantHidden" runat="server" />

                <div class="formWhole">
                   
                    <div class="formThird">
                        
                        <asp:Label ID="FirstNameLabel" runat="server" Text="First Name" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="FirstNameTextBox" ForeColor="#FF3300" Font-Size="16px" Font-Bold="True" ValidationGroup="AppGroup">*</asp:RequiredFieldValidator>
                        <asp:TextBox ID="FirstNameTextBox" runat="server" CssClass="form-control" />
                    </div>

                    <div class="formThird">
                        <asp:Label ID="LastNameLabel" runat="server" Text="Last Name" /><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="LastNameTextBox" ForeColor="#FF3300" Font-Size="16px" Font-Bold="True" ValidationGroup="AppGroup">*</asp:RequiredFieldValidator>
                        <asp:TextBox ID="LastNameTextBox" runat="server" CssClass="form-control" />
                    </div>

                    <div class="formThird" id="pmsIDArea" runat="server">
                        <asp:Label ID="Label1" runat="server" Text="PMS ID"></asp:Label>
                        <asp:TextBox ID="PmsIdtxt" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>


                </div>
                <div class="formWhole">
                    <div class="formHalf">
                        <asp:Label ID="EmailAddressLabel" runat="server" Text="Email Address" />
                        <asp:Label ID="Responselbl" runat="server" ForeColor="Red"></asp:Label>
                        <asp:TextBox ID="EmailAddressTextBox" runat="server" CssClass="form-control" />
                    </div>


                </div>


                <div class="formThird">
                    &nbsp;<asp:Label ID="ApplicationStatusLabel" runat="server" Text="Application Status" /><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="RequiredFieldValidator" Text="*" ControlToValidate="ApplicationStatusDropDown" InitialValue="0" ForeColor="#FF3300" Font-Size="16px" Font-Bold="True" ValidationGroup="AppGroup"></asp:RequiredFieldValidator>
                    <div class="formWhole">
                        <asp:DropDownList ID="ApplicationStatusDropDown" runat="server" CssClass="form-control" AppendDataBoundItems="True" />
                    </div>
                </div>

                <div class="formThird">
                    &nbsp;<asp:Label ID="ApplicationReceivedLabel" runat="server" Text="Application Received" /><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="ApplicationReceivedDropDown" InitialValue="0" ForeColor="#FF3300" Font-Size="16px" Font-Bold="True" ValidationGroup="AppGroup">*</asp:RequiredFieldValidator>
                    <div class="formWhole">
                        <asp:DropDownList ID="ApplicationReceivedDropDown" runat="server" CssClass="form-control" AppendDataBoundItems="True" />
                    </div>

                </div>
                
             <div class="formThird">
                        <h3>
                            <asp:Label ID="RenterStatuslbl" runat="server" ForeColor="#009933"></asp:Label>
                        </h3>
                    </div>


            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="formWhole">
            <div class="formThird">
                <asp:Button ID="AddApplicantButton" runat="server" Text="Submit" CssClass="btn btn-default" OnClick="AddApplicantButton_Click" ValidationGroup="AppGroup" />
                   </div>
            <div class="formThird">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="AppClearBtn" runat="server" Text="Clear" CssClass="btn btn-default" OnClick="AppClearBtn_Click" CausesValidation="False" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
            </div>

                            
         
        </div>
    </div>

    <div class="formWhole">
        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
            <ContentTemplate>

                   <div class="formThird" id="ImportApplicantBtn" runat="server">
                    <asp:Button ID="Button4" runat="server" Text="Import Latest Applicants" CausesValidation="False" CssClass="btn btn-default" OnClick="Button4_Click" />

                </div>
                <div class="formThird">
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="50">
                        <ProgressTemplate>
                            <div>
                                <img src="/Images/report_wait.gif" />
                            </div>

                        </ProgressTemplate>

                    </asp:UpdateProgress>
                </div>


            </ContentTemplate>
        </asp:UpdatePanel>


    </div>

    <div class="formWhole">

        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
             


                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="10" Width="98%" DataKeyNames="ApplicantId,Propertyid,HasFile,Renterid" DataSourceID="ApplicantListSQL" AllowPaging="True" AllowSorting="True" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" OnRowDataBound="GridView1_RowDataBound" PageSize="30" OnRowCommand="GridView1_RowCommand">
                    <Columns>

                        <asp:TemplateField HeaderText="Name" SortExpression="FullName">
                            <EditItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("FullName") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Select" Text='<%# Eval("FullName") %>' CausesValidation="False"></asp:LinkButton>
                                <br />
                                <asp:Label ID="Label3" runat="server" Font-Size="11px" ForeColor="#009933"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="ApplicantId" HeaderText="ApplicantId" InsertVisible="False" ReadOnly="True" SortExpression="ApplicantId" Visible="False" />
                        <asp:BoundField DataField="PropertyId" HeaderText="PropertyId" SortExpression="PropertyId" Visible="False" />
                        <asp:BoundField DataField="PrimaryEmailAddress" HeaderText="Email" SortExpression="PrimaryEmailAddress" />
                        <asp:TemplateField HeaderText="Status" SortExpression="ApplicationStatusName">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ApplicationStatusName") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("ApplicationStatusName") %>'></asp:Label>
                                <br />
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="GoToRenter" CommandArgument='<%# Eval("Renterid") %>'>Converted</asp:LinkButton>
                     
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ApplicationSubmissionTypeName" HeaderText="ApplicationSubmissionTypeName" SortExpression="ApplicationSubmissionTypeName" Visible="False" />
                        <asp:BoundField DataField="Payment" HeaderText="Payment" ReadOnly="True" SortExpression="Payment" DataFormatString="{0:c}" />
                        <asp:TemplateField HeaderText="App on File" SortExpression="HasFile">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("HasFile") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="HasFilelbl" runat="server" Text='<%# Bind("HasFile") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DateCreated" DataFormatString="{0:d}" HeaderText="Date Created" SortExpression="DateCreated" />
                        <asp:BoundField DataField="LastUpdated" DataFormatString="{0:d}" HeaderText="Last Updated" SortExpression="LastUpdated" />
                        <asp:TemplateField HeaderText="PmsIdValidated" SortExpression="PmsIdValidated" Visible="False">
                            <EditItemTemplate>
                                <asp:TextBox ID="Isvalidatedtxt" runat="server" Text='<%# Bind("PmsIdValidated") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Isvalidatedlbl" runat="server" Text='<%# Bind("PmsIdValidated") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="ReportTableHeader" />
                    <PagerStyle Font-Bold="True" />
                    <SelectedRowStyle BackColor="#CCCCCC" />
                </asp:GridView>

                <asp:SqlDataSource ID="ApplicantListSQL" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="usp_Applicants_GetListByPropertyID" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="PropertyID" QueryStringField="PropertyID" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>


            </ContentTemplate>
        </asp:UpdatePanel>
    </div>



</div>