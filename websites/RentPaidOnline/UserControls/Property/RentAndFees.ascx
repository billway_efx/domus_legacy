﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RentAndFees.ascx.cs" Inherits="Domus.UserControls.Property.RentAndFees" %>

<asp:UpdatePanel ID="UpdateApplicationFeeArea" runat="server">
    <ContentTemplate>
	<asp:PlaceHolder ID="successMsg" runat="server" Visible="false"><div class="alert alert-success" id="Div1"><b>Success!</b></div></asp:PlaceHolder>
        <div class="formWrapper">
            <h2>Application Fees</h2>

            <div class="formWhole">
                <div class="formThird">
                    <asp:Label ID="ApplicationFeeNameLabel" runat="server" Text="Fee Name" />
                    <asp:TextBox ID="ApplicationFeeNameTextBox" runat="server" CausesValidation="false" CssClass="form-control" />
                </div>
            </div>

            <div class="formWhole">
                <div class="formHalf">
                    <div class="formThird">
                        <asp:Label ID="ApplicationFeeAmountLabel" runat="server" Text="Amount" />
                        <asp:TextBox ID="ApplicationFeeAmountTextBox" runat="server" CausesValidation="false" CssClass="form-control" />
                    </div>
                    <div class="formThird noLabel">
                        <asp:Button ID="AddApplicationFeeButton" runat="server" Text="Add Application Fee" ValidationGroup="ApplicationFees" CssClass="btn btn-default" />
                    </div>
                </div>
            </div>


            <br />
            <div class="formWhole">
                <asp:GridView ID="ApplicationFeesGrid" runat="server" AutoGenerateColumns="false" GridLines="None" Width="100%" CssClass="table-striped" >
                    <Columns>
                        <asp:BoundField HeaderText="Fee Name" DataField="FeeName" ItemStyle-Width="80%" />
                        <asp:BoundField HeaderText="Amount" DataField="FeeAmountText" />
						<asp:TemplateField HeaderText="Manage" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <%--<asp:HyperLink ID="DeleteLinkApplication" CssClass="secondary-link" runat="server" NavigateUrl='<%# Bind("DeleteApplicationFeeUrl") %>'><span data-icon="&#x2b" aria-hidden="true"></span></asp:HyperLink>--%>
                                <asp:Linkbutton ID="DeleteLinkApplication" CssClass="secondary-link" runat="server" PostBackUrl='<%# Bind("DeleteApplicationFeeUrl") %>'><span data-icon="&#x2b" aria-hidden="true"></span></asp:Linkbutton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <p>There are no Application Fees currently setup for this Property</p>
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>

        <asp:RequiredFieldValidator ID="RequiredApplicationFeeName"
            ErrorMessage="The Fee Name is Required"
            Display="None"
            ControlToValidate="ApplicationFeeNameTextBox"
            runat="server"
            ValidationGroup="ApplicationFees" />

        <asp:RequiredFieldValidator ID="RequiredApplicationFeeAmount"
            ErrorMessage="The Fee Amount is Required"
            Display="None"
            ControlToValidate="ApplicationFeeAmountTextBox"
            runat="server"
            ValidationGroup="ApplicationFees" />

        <asp:RegularExpressionValidator ID="ValidApplicationFeeAmount"
            ErrorMessage="Please enter a valid amount."
            Display="None"
            ControlToValidate="ApplicationFeeAmountTextBox"
            runat="server"
            ValidationExpression="^(-)?\d+(\.\d\d)?$"
            ValidationGroup="ApplicationFees" />
        <asp:ValidationSummary ID="ApplicationSummary" DisplayMode="BulletList" runat="server" ValidationGroup="ApplicationFees" />
    </ContentTemplate>
</asp:UpdatePanel>



<asp:UpdatePanel ID="UpdateMonthlyFeeArea" runat="server">
    <ContentTemplate>
        <div class="formWrapper">
            <h2>Monthly Fees</h2>
            <div class="formWhole">
                <div class="formHalf">
                    <asp:Label ID="MonthlyFeeNameLabel" Text="Fee Name" runat="server" />
                    <asp:TextBox ID="MonthlyFeeNameTextBox" runat="server" CssClass="form-control" />
                </div>
                <div class="formHalf">
                <div class="formThird">
                    <asp:Label ID="MonthlyFeeAmountLabel" Text="Amount" runat="server" />
                    <asp:TextBox ID="MonthlyFeeAmountTextBox" runat="server" CssClass="form-control" />
                </div>
                    </div>
            </div>
            <div class="formWhole">
                <div class="formHalf">
                    <asp:CheckBox ID="ApplyToAllResidentsCheckBox" Text="&nbsp; Apply this Fee to all Residents" runat="server"/>
                </div>
                <div class="formHalf">
                    <asp:Button ID="AddMonthlyFeeButton" runat="server" Text="Add Fee" ValidationGroup="MonthlyFees" CssClass="btn btn-default" />
                </div>
            </div>
            <br />
            <div class="formWhole">
                <asp:GridView ID="MonthlyFeesGrid" runat="server" AutoGenerateColumns="false" GridLines="None" Width="100%" CssClass="table-striped">
                    <Columns>
                        <asp:BoundField HeaderText="Fee Name" DataField="FeeName" ItemStyle-Width="80%" />
                        <asp:BoundField HeaderText="Amount" DataField="FeeAmountText" />
						<asp:TemplateField HeaderText="Delete" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%--<asp:HyperLink ID="DeleteLinkMonthly" CssClass="secondary-link" runat="server" NavigateUrl='<%# Bind("DeleteMonthlyFeeUrl") %>'><span data-icon="&#x2b" aria-hidden="true"></span></asp:HyperLink>--%>
                                <asp:Linkbutton ID="DeleteLinkMonthly" CssClass="secondary-link" runat="server" PostBackUrl='<%# Bind("DeleteMonthlyFeeUrl") %>'><span data-icon="&#x2b" aria-hidden="true"></span></asp:Linkbutton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <p>There are no Monthly Fees currently setup for this Property</p>
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>


            <asp:RequiredFieldValidator ID="RequiredMonthlyFeeName"
                ErrorMessage="The Fee Name is Required"
                Display="None"
                ControlToValidate="MonthlyFeeNameTextBox"
                runat="server"
                ValidationGroup="MonthlyFees" />

            <asp:RequiredFieldValidator ID="RequiredMonthlyFeeAmount"
                ErrorMessage="The Fee Amount is Required"
                Display="None"
                ControlToValidate="MonthlyFeeAmountTextBox"
                runat="server"
                ValidationGroup="MonthlyFees" />

            <asp:RegularExpressionValidator ID="ValidMonthlyFeeAmount"
                ErrorMessage="Please enter a valid amount."
                Display="None"
                ControlToValidate="MonthlyFeeAmountTextBox"
                runat="server"
                ValidationExpression="^(-)?\d+(\.\d\d)?$"
                ValidationGroup="MonthlyFees" />
            <asp:ValidationSummary ID="MonthlySummary" DisplayMode="BulletList" runat="server" ValidationGroup="MonthlyFees" />

            <div class="formHalf">              
                    <asp:CheckBox ID="IntegratedFeesBreakdownCheckBox" runat="server" Text="&nbsp; Display Fee Breakdown to Residents" />
            </div>
        </div>


        <!-- Footer -->
        <div class="button-footer">
            <div class="main-button-action">
                <!-- CMallory - Layout Change - Added btn-footer css tag to button -->
                <asp:Button ID="SaveFeesButton" Text="Save" runat="server" CausesValidation="false" CssClass="btn btn-primary btn-footer" />
            </div>
            <div class="clear"></div>
        </div>


    </ContentTemplate>

</asp:UpdatePanel>
