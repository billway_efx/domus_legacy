﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertiesStaff.ascx.cs" Inherits="Domus.UserControls.Property.PropertiesStaff" %>
<%@ Import Namespace="EfxFramework.ExtensionMethods" %>

<style>
    .hide {
        display: none;
    }
    .AdminList{
        font-size:12px;
    }
</style>

<div id="divPS" style="color: white; ">

    <!-- task#: joselist-1a - patrick whittingham - 7/20/15 - list of property admins.    -->
    <asp:DropDownList ID="PropertyStaffDropDown" runat="server" AppendDataBoundItems="True" AutoPostBack="False" DataTextField="DisplayName" CssClass="form-control propertyStaff" DataValueField="PropertyStaffId" Font-Size="12px" Width="180px"> 
        <asp:ListItem Text="Property Admin" Value="" />
    </asp:DropDownList>
      
</div>

<script>

    // task#: joselist-1a - Patrick Whittingham - 7/20/15
    $(document).ready(function () {

         $(".propertyStaff").change(function (e) {

             if ($(this).val() != "") {
                 var selectedUrl = "/staff.aspx?staffId=" + $(this).val();
                 window.location.href = selectedUrl;
             }

         });

    });

</script>
