﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using EfxFramework.Web;
using EfxFramework.Web.Facade;
using Mb2x.ExtensionMethods;
using System.Net.Mail;
using System.Collections.Generic;
using EfxFramework.BulkMail;
using System.Linq;
using System.IO;
using System.Collections;
using System.Net;
using System.Text;

//CMallory - Task 00462 - Copied code from Email/Default.aspx page to this one.
namespace Domus.UserControls.Property
{
    public partial class EmailMarketing : UserControl// System.Web.UI.Page
    {
        //private EmailMarketingPresenter _Presenter;
        public int? EmailsSentCount = 0;
        List<MailAddress> recipients = new List<MailAddress>();

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            //PageMessage.DisplayMessage(SystemMessageType.Success, "Successfully saved email.");
            successDiv.Visible = true;
            var test = EfxFramework.Renter.GetRenterListByPropertyId(2);
        }

        public string GetPropertyString()
        {

            CheckBoxList ckbx = (CheckBoxList)this.PropertyPicker.FindControl("CheckBoxList1");
            List<String> PropertyID_list = new List<string>();
            List<String> PropertyName_list = new List<string>();

            foreach (ListItem item in ckbx.Items)
            {
                if (item.Selected)
                {
                    PropertyID_list.Add(item.Value);
                    PropertyName_list.Add(item.Text);
                }
            }
            string PropList = String.Join(",", PropertyName_list.ToArray());
            return PropList;
        }

        protected void EmailImageDropdownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Salcedo - 6/13/2014 - # 000140 - add selected file as an attachment instead of embedding within the text of the email
            if (EmailImageDropdownList.SelectedValue != "-1")
            {
                var BaseUrl = String.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, !String.IsNullOrEmpty(Request.ApplicationPath) ? Request.ApplicationPath.TrimEnd('/') : string.Empty);

                //CKEditor1.Text = String.Format("<p>{0}</p><img src={1} alt=There are images attached to this email.  Please make sure that your email settings allow images to be downloaded to view the content. />", 
                //    "There are images attached to this email. Please make sure that your email settings allow images to be downloaded to view the content.",
                //    String.Format("{0}{1}", BaseUrl, EmailImageDropdownList.SelectedValue));
                ////CKEditor1.Text = "There are one or more images attached to this email. Please make sure that your email settings allow images to be downloaded to view the content.";
                string path = HttpContext.Current.Request.Url.Host;
                String FilePath = path + @"\Images\EmailMarketing\";// Path.GetFullPath(Path.Combine(path, @"..\Images\EmailMarketing\")); // Server.MapPath("/Images/EmailMarketing/");
                String FileName = Path.GetFileName(EmailImageDropdownList.SelectedItem.Value.ToString());
                HideDownload.Visible = true;
                ButtonLink.NavigateUrl = Server.UrlDecode(@"EmailMarketing\\" + FileName).ToString().Trim();
            }
            else
            {
                HideDownload.Visible = false;
                ////CKEditor1.Text = "";
            }
        }

        //        protected void DownloadFile_Click(object sender, EventArgs e)
        //        {
        //            if (EmailImageDropdownList.SelectedValue != "-1")
        //            {
        //                byte[] bytes = File.ReadAllBytes(EmailImageDropdownList.SelectedItem.Value.ToString());
        //                String FileName = Path.GetFileName(EmailImageDropdownList.SelectedItem.Value.ToString());
        //                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;

        //                //response.Buffer = true;
        //                //response.Clear();
        //                //response.ClearContent();
        //                //response.ClearHeaders();

        //                //response.ContentType = "application/pdf";
        //                ////response.AppendHeader("content-disposition", "attachment; filename=" + FileName);
        //                ////response.AddHeader("content-disposition", "attachment; filename=" + FileName);
        //                //response.AddHeader("Content-Disposition", string.Format("attachment; filename= " + FileName));
        //                ////response.BinaryWrite(bytes);
        //                ////response.TransmitFile(Server.MapPath("/Images/EmailMarketing/").ToString() + FileName);
        //                //response.Flush();
        //            }
        //        }

        //CMallory - Task 00638 - Added PopulateDropDown and Page_Load methods retrieved from the Email\Default.aspx.cs file.
        //Salcedo - 6/13/2014 - # 000140 - populate control from directory instead of using static list of files
        private void PopulateDropDown()
        {
            String FilePath = EfxSettings.MarketingImageFolderLocation;

            string NewFilePath = Server.MapPath("/Images/EmailMarketing/");

            //cakel:08/23/2016
            String[] fileEntries = Directory.GetFiles(NewFilePath);
            //String[] fileEntries = Directory.GetFiles(FilePath);

            EmailImageDropdownList.Items.Clear();
            ListItem NewItem = new ListItem("-- Select Attachment --", "-1");
            EmailImageDropdownList.Items.Add(NewItem);

            //Add all the file names in the directory to the dropdown control
            foreach (String FileName in fileEntries)
            {
                //Remove the extension, and replace all hyphens and underscores with a blank space to make the file names more user-friendly
                String DisplayName = Path.GetFileNameWithoutExtension(FileName).Replace("-", " ").Replace("_", " ");
                NewItem = new ListItem(DisplayName, FileName);
                EmailImageDropdownList.Items.Add(NewItem);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            successDiv.Visible = false;
            var allProps = EfxFramework.Property.GetAllPropertyList();
            if (!IsPostBack)
            {
                //Salcedo - 6/13/2014 - # 000140 - populate control from directory instead of using static list of files
                PopulateDropDown();
                CKEditor1.config.filebrowserImageUploadUrl = "/Handlers/EFXAdminHandler.ashx?requestType=1";
            }
            //Need for FileUpload control to work properly.
            Page.Form.Attributes.Add("enctype", "multipart/form-data");
        }

        protected void SendButton_Click(object sender, EventArgs e)
        {
            //Salcedo = task # 413
            EmailsSentCount = 0;

            if (RenterGroupDropdownList.SelectedValue == "-1")
            {
                CustomValidator err = new CustomValidator();
                err.ValidationGroup = "Email";
                err.IsValid = false;
                err.ErrorMessage = "Please select a Resident group.";
                Page.Validators.Add(err);
                //PageMessage.DisplayMessage(SystemMessageType.Error, "Please select a renter group.");
                return;
            }

            // Flag to denote if we are sending to all renters or delinquent
            var sendAll = RenterGroupDropdownList.SelectedValue == "2";

            // Salcedo - 1/15/2014 - we're now pulling the "From"email address from the web page, instead of the logged in user's primaryemailaddress
            String FromEmail = FromTextbox.Text;
            if (FromEmail.Length == 0 || !FromEmail.IsValidEmailAddress())
            {
                CustomValidator err = new CustomValidator();
                err.ValidationGroup = "Email";
                err.IsValid = false;
                err.ErrorMessage = "Please enter a valid From email address.";
                Page.Validators.Add(err);
                return;
            }

            // joselist-4b - Patrick Whittingham - 7/17/2015 - Add new BCC form field
            String BCCEmail = BCCTextBox.Text;
            if (BCCEmail.Length > 0 && !BCCEmail.IsValidEmailAddress())
            {
                CustomValidator err = new CustomValidator();
                err.ValidationGroup = "Email";
                err.IsValid = false;
                err.ErrorMessage = "Please enter a valid BCC email address.";
                Page.Validators.Add(err);
                return;
            }

            //.Visible = false;
            //EmailSentPanel.Visible = true;

            //Collier Mallory - Task 00026
            //Changed logic to get property names from the GetPropertyString method instead of a text box.
            EmailToLabel.Text = GetPropertyString();
            EmailSubjectLabel.Text = SubjectTextbox.Text;

            PropertyStaff CurrentStaff = UIFacade.GetCurrentUser(UserType.PropertyStaff) as PropertyStaff;
            EmailSentByLabel.Text = CurrentStaff.PrimaryEmailAddress;
            // Build recipients list


            // First get properties
            var propsArray = EmailToLabel.Text.Split(',');
            var allProps = EfxFramework.Property.GetAllPropertyList();
            var propsToAdd = new List<EfxFramework.Property>();
            foreach (var prop in propsArray)
            {
                var toAdd = allProps.FirstOrDefault(p => p.PropertyName.ToLower().Equals(prop.ToLower()));
                if (toAdd != null)
                    propsToAdd.Add(toAdd);
            }

            if (propsToAdd.Count() == 0)
            {
                CustomValidator err = new CustomValidator();
                err.ValidationGroup = "Email";
                err.IsValid = false;
                err.ErrorMessage = "No properties have been selected.";
                Page.Validators.Add(err);
            }
            else
            {
                bool pastDueRentersExist = true;
                // Now renters
                foreach (var property in propsToAdd)
                {
                    //If All Residents is selected.
                    if (RenterGroupDropdownList.SelectedValue == "2")
                    {
                        var renters = EfxFramework.Renter.GetRenterListByPropertyId(property.PropertyId.Value);

                        foreach (var renter in renters)
                        {
                            AddEmailRecipient((EfxFramework.Renter)renter);
                            continue;
                        }
                    }

                    //If Active Residents is selected.
                    else if (RenterGroupDropdownList.SelectedValue == "3")
                    {
                        var renters = EfxFramework.Renter.GetRenterListByPropertyIdForEmailMarketing(property.PropertyId.Value, 3);

                        foreach (var renter in renters)
                        {
                            //// joselist-4b - Patrick Whittingham - 7/17/2015 - show bad email addresses
                            //try
                            //{
                            //    recipients.Add(new MailAddress(renter.PrimaryEmailAddress));
                            //    // Patrick Whittingham - 8/22/15 - task #413 : add email counter
                            //    EmailsSentCount++;
                            //}
                            //catch (FormatException ex)
                            //{
                            //    ExceptionMethod((EfxFramework.Renter)renter);
                            //}
                            AddEmailRecipient((EfxFramework.Renter)renter);
                            continue;
                        }
                    }

                    //If Late Residents is selected.
                    else if (RenterGroupDropdownList.SelectedValue == "4")
                    {
                        var renters = EfxFramework.Renter.GetRenterListByPropertyIdForEmailMarketing(property.PropertyId.Value, 4);

                        foreach (var renter in renters)
                        {
                            AddEmailRecipient((EfxFramework.Renter)renter);
                            continue;
                        }
                    }



                    //Collier Mallory - Commented out on 08/24/2016
                    //// Get past due information
                    //var pastDueData = EfxFramework.Renter.GetRenterPastDueByRenterId(renter.RenterId);
                    //// joselist-4b - Patrick Whittingham - 7/17/2015 - send to all residents and not just past due residents
                    ////if (pastDueData.Rows.Count > 0)
                    ////{
                    //    var amount = pastDueData.Rows[0][4].ToString().ToNullDecimal(); // Past Due Amount
                    //    //if (amount.HasValue && amount.Value > 0)
                    //    //{
                    //        // Add past due renter
                    //    // joselist-4b - Patrick Whittingham - 7/17/2015 - show bad email addresses
                    //    try
                    //    {
                    //        recipients.Add(new MailAddress(renter.PrimaryEmailAddress));
                    //        // Patrick Whittingham - 8/22/15 - task #413 : add email counter
                    //        EmailsSentCount++;
                    //    }
                    //    catch (FormatException ex)
                    //    {
                    //        CustomValidator err = new CustomValidator();
                    //        err.ValidationGroup = "Email";
                    //        err.IsValid = false;
                    //        //err.ErrorMessage = pastDueRentersExist ? "This is a bad email address format: " + renter.PrimaryEmailAddress : "No recipients have been added.";
                    //        // Patrick Whittingham - 8/20/15 - task #413 : remove error message
                    //        err.ErrorMessage = pastDueRentersExist ? "Resident '" + renter.FirstName + " " + renter.LastName + "' skipped due to invalid email address: '" + renter.PrimaryEmailAddress + "' " : "No recipients have been added.";
                    //        Page.Validators.Add(err);
                    //    }

                    //}
                    //}
                    //else
                    //pastDueRentersExist = false;
                }
            }

            //var Administrator = new EfxAdministrator(HttpContext.Current.User.Identity.Name.ToInt32());

            // Send it
            // joselist-4b - Patrick Whittingham - 7/17/2015 - change error message
            if (recipients.Count() == 0)
            {
                CustomValidator err = new CustomValidator();
                err.ValidationGroup = "Email";
                err.IsValid = false;
                //err.ErrorMessage = pastDueRentersExist ? "No email sent to recipients" : "No recipients have been added.";
                err.ErrorMessage = "No recipients have been added.";
                Page.Validators.Add(err);
            }
            else
            {
                // joselist-4b - Patrick Whittingham - 7/17/2015 - Add BCC email address
                if (BCCEmail.Length > 0)
                {
                    recipients.Add(new MailAddress(BCCEmail));
                }

                //Salcedo - 6/13/2014 - # 000140 - add selected file as an attachment instead of embedding within the text of the email
                //if (EmailImageDropdownList.SelectedItem.Value != "-1")
                if (UploadAttachment.HasFile)
                {
                    List<Attachment> AttachmentList = new List<Attachment>();
                    //String FullAttachmentPath = EmailImageDropdownList.SelectedItem.Value.ToString();
                    string fileName = Path.GetFileName(UploadAttachment.PostedFile.FileName);
                    
                    //Check if extension is allowed.
                    if(!IsExtensionAllowed(fileName))
                    {
                        return;
                    }
                    //Attachment MyAttachment = new Attachment(FullAttachmentPath);
                    Attachment MyAttachment = new Attachment(UploadAttachment.FileContent, fileName);
                    AttachmentList.Add(MyAttachment);

                    //Cmallory Task 00622 - Added CKEditor1.Text field to the Function call below.
                    BulkMail.SendMultiPropertyMailMessage(propsToAdd.Select(property => property.PropertyId).ToList(), RenterGroupDropdownList.SelectedItem.Text,
                        true, EfxSettings.SendMailUsername, EfxSettings.SendMailPassword,
                        new MailAddress(FromEmail), EmailSubjectLabel.Text, CKEditor1.Text, recipients, AttachmentList);
                }
                else
                {
                    //Cmallory Task 00622 - Added CKEditor1.Text field to the Function call below.
                    BulkMail.SendMultiPropertyMailMessage(propsToAdd.Select(property => property.PropertyId).ToList(), RenterGroupDropdownList.SelectedItem.Text,
                        true, EfxSettings.SendMailUsername, EfxSettings.SendMailPassword,
                        new MailAddress(FromEmail), EmailSubjectLabel.Text, CKEditor1.Text, recipients);
                }

                successDiv.Visible = true;
            }
        }
        //PageMessage.DisplayMessage(SystemMessageType.Success, "Email sent successfully.");
        protected void AddEmailRecipient(EfxFramework.Renter CurrentRenter)
        {
            try
            {
                recipients.Add(new MailAddress(CurrentRenter.PrimaryEmailAddress));
                // Patrick Whittingham - 8/22/15 - task #413 : add email counter
                EmailsSentCount++;
            }

            catch (FormatException ex)
            {
                CustomValidator err = new CustomValidator();
                err.ValidationGroup = "Email";
                err.IsValid = false;
                //err.ErrorMessage = pastDueRentersExist ? "This is a bad email address format: " + renter.PrimaryEmailAddress : "No recipients have been added.";
                // Patrick Whittingham - 8/22/15 - task #413 : remove error message
                err.ErrorMessage = "Resident '" + CurrentRenter.FirstName + " " + CurrentRenter.LastName + "' skipped due to invalid email address: '" + CurrentRenter.PrimaryEmailAddress + "' ";
            }

        }

        protected void DownloadFile_Click(object sender, EventArgs e)
        {
            //Redirect(ButtonLink.NavigateUrl , "_blank", "menubar=0,width=900,height=900");

        }

         public static void Redirect(string url, string target, string windowFeatures) {
        HttpContext context = HttpContext.Current;
 
        if ((String.IsNullOrEmpty(target) ||
            target.Equals("_self", StringComparison.OrdinalIgnoreCase)) &&
            String.IsNullOrEmpty(windowFeatures)) {
 
            context.Response.Redirect(url);
        }
        else {
            Page page = (Page)context.Handler;
            if (page == null) {
                throw new InvalidOperationException(
                    "Cannot redirect to new window outside Page context.");
            }
            url = page.ResolveClientUrl(url);
 
            string script;
            if (!String.IsNullOrEmpty(windowFeatures)) {
                script = @"window.open(""{0}"", ""{1}"", ""{2}"");";
            }
            else {
                script = @"window.open(""{0}"", ""{1}"");";
            }
 
            script = String.Format(script, url, target, windowFeatures);
            ScriptManager.RegisterStartupScript(page,
                typeof(Page),
                "Redirect",
                script,
                true);
        }
    }
        public bool IsExtensionAllowed(string FileName)
         {
            //If file has no extension
            if(!FileName.Contains("."))
            return false;
        
            List<string> ValidExtensions = new List<string>();
            string Extension = Path.GetExtension(FileName).ToLower(); //FileName.substring(FileName.lastIndexOf('.') + 1).toLowerCase();

            //File Extensions not allowed
            ValidExtensions.Add(".exe");
            ValidExtensions.Add(".mp3");
            ValidExtensions.Add(".mp4");
            ValidExtensions.Add(".wma");
            foreach(string Ext in ValidExtensions)
            {
                if(Extension == Ext)
                {
                    CustomValidator err = new CustomValidator();
				    err.ValidationGroup = "Email";
				    err.IsValid = false;
                    err.ErrorMessage = String.Format("A file with the extension {0} cannot be sent via Domus.", Ext);
                    Page.Validators.Add(err);
                    return false;
                    
                }      
            }
            return true;
         }
    }
}
