﻿using EfxFramework;
using EfxFramework.Interfaces.UserControls.Property;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domus.UserControls.Property
{
    public partial class ProgramInformationAddEdit : UserControl, IProgramInformationAddEdit
    {
        private EfxFramework.Presenters.UserControls.Property.ProgramInformationAddEdit _presenter;

        public int PropertyId { get; set; }
        public EventHandler ProgramSelected { set { ProgramDropdown.SelectedIndexChanged += value; } }
        public string ProgramSelectedValue { get { return ProgramDropdown.SelectedValue; } set { ProgramDropdown.SelectedValue = value; } }
        public MultiView ProgramMultiview { get { return ProgramsMultiview; } }
        public View NoneSelectedView { get { return NoneSelected; } }
        public View PropertyPaidView { get { return PropertyPaidProgramView; } }
        public View AirView { get { return AirProgramView; } }
        public View SpaView { get { return SpaProgramView; } }

        public List<PropertyPaidProgram> PropertyPaidProgramDataSource
        {
            set
            {
                PropertyPaidGrid.DataSource = value;
                PropertyPaidGrid.DataBind();
            }
        }

        public List<AirProgram> AirProgramDataSource
        {
            set
            {
                AirProgramGrid.DataSource = value;
                AirProgramGrid.DataBind();
            }
        }

        public List<SpaProgram> SpaProgramDataSource
        {
            set
            {
                SpaProgramGrid.DataSource = value;
                SpaProgramGrid.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter = new EfxFramework.Presenters.UserControls.Property.ProgramInformationAddEdit(this);

            if (!Page.IsPostBack)
                _presenter.InitializeValues();
        }
    }
}