﻿using EfxFramework.Interfaces.UserControls.Property;
using System;
using System.Web.UI;

namespace Domus.UserControls.Property
{
    public partial class EfxSalesAddEdit : UserControl, IEfxSalesAddEdit
    {
        private EfxFramework.Presenters.UserControls.Property.EfxSalesAddEdit _presenter;

        public int CompanyId { get; set; }
        public string FirstNameText { get { return FirstNameTextbox.Value; } set { FirstNameTextbox.Value = value; } }
        public string LastNameText { get { return LastNameTextbox.Value; } set { LastNameTextbox.Value = value; } }
        public string TelephoneNumberText { get { return TelephoneNumberTextbox.Value; } set { TelephoneNumberTextbox.Value = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter = new EfxFramework.Presenters.UserControls.Property.EfxSalesAddEdit(this);

            if (!Page.IsPostBack)
                _presenter.InitializeValues();
        }
    }
}