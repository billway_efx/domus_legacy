﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Applications.ascx.cs" Inherits="Domus.UserControls.Property.Applications" %>

<style>
    .hide {
        display: none;
    }
</style>
<asp:ValidationSummary ID="ApplicationSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="Payment" />

<!--cakel: BUGID00033 Added update panel and DIV to show save msg-->
<asp:UpdatePanel ID="SaveSuccessPanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div id="SaveStatusArea" runat="server" visible="false" style="padding: 10px; background-color: #dff0d8; height: 30px; color: #468847; margin-top: 10px;">
    <asp:Label ID="SaveStatusLabel" runat="server" Font-Bold="True"></asp:Label>
</div>
    </ContentTemplate>
</asp:UpdatePanel>
<!--END cakel: BUGID00033 Added update panel and DIV to show save msg-->



<h2>Upload Application</h2>
<div class="formWrapper">
    <p>Upload Your Application for potential residents to download, fill out and upload.</p>



    <div class="formTwoThirds">
        <div class="well">
            <asp:FileUpload ID="ApplicationUpload" runat="server" CssClass="btn btn-default" />
        </div>
    </div>
    <div class="formThird noLabelMore">
        <asp:Button ID="UploadButton" runat="server" Text="Upload" CausesValidation="False" CssClass="btn btn-default" />
    </div>



    <div class="formWhole">
        <h3>Current Application</h3>
        <asp:Label ID="ApplicationLabel" runat="server" />
        <br />
        <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" Text="Delete Application" />
        <hr />
    </div>


    <asp:UpdatePanel ID="UpdateNewApplicant" runat="server">
        <ContentTemplate>
            <div class="formWhole">
                <h3 class="newSection">New Applicant</h3>
            </div>
            <div class="formWhole">

                <div class="formThird">
                    <asp:Label ID="FirstNameLabel" runat="server" Text="First Name" />
                    <asp:TextBox ID="FirstNameTextBox" runat="server" CssClass="form-control" />
                </div>

                <div class="formThird">
                    <asp:Label ID="LastNameLabel" runat="server" Text="Last Name" />
                    <asp:TextBox ID="LastNameTextBox" runat="server" CssClass="form-control" />
                </div>
            </div>
            <div class="formWhole">
                <div class="formHalf">
                    <asp:Label ID="EmailAddressLabel" runat="server" Text="Email Address" />
                    <asp:TextBox ID="EmailAddressTextBox" runat="server" CssClass="form-control" />
                </div>
            </div>


            <div class="formFourth">
                &nbsp;<asp:Label ID="ApplicationStatusLabel" runat="server" Text="Application Status" />
                <div class="formWhole">
                    <asp:DropDownList ID="ApplicationStatusDropDown" runat="server" CssClass="form-control" />
                </div>
            </div>

            <div class="formFourth">
                &nbsp;<asp:Label ID="ApplicationReceivedLabel" runat="server" Text="Application Received" />
                <div class="formWhole">
                    <asp:DropDownList ID="ApplicationReceivedDropDown" runat="server" CssClass="form-control" />
                </div>

            </div>
            <div class="formFourth noLabel">
                <asp:Button ID="AddApplicantButton" runat="server" Text="Add Applicant" CausesValidation="False" CssClass="btn btn-default" />
            </div>


        </ContentTemplate>
    </asp:UpdatePanel>

</div>

<asp:UpdatePanel ID="UpdateSaveApplicant" runat="server">
    <ContentTemplate>
        <h3 class="newSection">Applicants</h3>
        <h4>Last 30 Days</h4>
        <div class="formWrapper">

            <div class="formWhole">
                <asp:Panel ID="ApplicantPanel" runat="server" Width="100%" ScrollBars="Auto">
                    <asp:GridView ID="ApplicantGridView" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" CssClass="table-striped">
                        <Columns>
                            <asp:TemplateField HeaderStyle-CssClass="hide" ItemStyle-CssClass="hide">
                                <ItemTemplate>
                                    <asp:HiddenField ID="ApplicantApplicationId" runat="server" Value='<%# Bind("ApplicantApplicationId") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField HeaderText="Name" DataField="ApplicantDisplayName" />

                            <asp:HyperLinkField HeaderText="Application" DataNavigateUrlFields="ApplicationUrl" DataTextField="DownloadLinkText" Target="_blank" />

                            <asp:TemplateField HeaderText="Received">
                                <ItemTemplate>
                                    <!--cakel: BUGID00033 updated drop down to autopostback and exec function on index change -->
                                    <asp:DropDownList ID="ReceivedDropDown" runat="server" OnSelectedIndexChanged="ReceivedDropDown_SelectedIndexChanged" AutoPostBack="True">
                                        <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Mail"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Email"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Fax"></asp:ListItem>
                                        <asp:ListItem Value="5" Text="In Person"></asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField HeaderText="Application Date" DataField="ApplicationDateString" />

                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <!--cakel: BUGID00033 updated drop down to autopostback and exec function on index change -->
                                    <asp:DropDownList ID="StatusDropDown" runat="server" AutoPostBack="True" OnSelectedIndexChanged="StatusDropDown_SelectedIndexChanged">
                                        <asp:ListItem Value="1" Text="Approved"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Denied"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Pending"></asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField HeaderText="Status Date" DataField="ApplicationStatusDateString" />

                            <asp:TemplateField HeaderText="Actions" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:HyperLink ID="TakeAPaymentText" CssClass="secondary-link" runat="server" Visible="false" NavigateUrl='<%# Bind("TakeAPaymentUrl") %>'><span data-icon="&#x27" aria-hidden="true"></span></asp:HyperLink>                                                           
                                   &nbsp; <asp:HyperLink ID="EmailApplicant" CssClass="secondary-link" runat="server" NavigateUrl='<%# Bind("EmailApplicantUrl") %>'><span data-icon="&#x46" aria-hidden="true"></span></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </div>

            <div class="formWhole">
                <hr />
            </div>
            <div class="formWhole float-right">
                <!--cakel: BUGID00033 -  Set visible to false for SAVE button -->
                <asp:Button ID="SaveApplicantsButton" runat="server" Text="Save" CausesValidation="False" CssClass="btn btn-primary" Visible="False" />
            </div>
            <div class="formWhole">
                <hr />
            </div>
        </div>


    </ContentTemplate>
</asp:UpdatePanel>
<asp:Panel runat="server" ID="FeesPanel" Visible="false">
<div class="formWrapper">
    <h2>Take Application Fees and Deposits</h2>

    <uc1:PersonalInformation ID="PersonalInformation" runat="server" />


    <asp:UpdatePanel ID="UpdateBillingInformation" runat="server">
        <ContentTemplate>

            <uc1:ResidentBillingInformation ID="BillingInformation" runat="server" />

        </ContentTemplate>
    </asp:UpdatePanel>


    <asp:UpdatePanel ID="UpdateSelectedPayment" runat="server">
        <ContentTemplate>

            <uc1:SelectPaymentType ID="SelectPaymentType" runat="server" />

        </ContentTemplate>
    </asp:UpdatePanel>


    <asp:UpdatePanel ID="UpdateFeesPayment" runat="server">
        <ContentTemplate>

                <uc1:PaymentAmountsAlternate ID="PaymentAmounts" runat="server" />
 
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

<div class="button-footer">
    <div class="main-button-action">
        <asp:Button ID="SubmitPaymentButton" runat="server" Text="Submit Payment" CausesValidation="False" CssClass="btn btn-primary" />
    </div>
    <div class="clear"></div>
</div>
</asp:Panel>