﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System.Web.UI.WebControls;

namespace Domus.UserControls.Property
{
    public partial class RentAndFees : UserControl, IRentAndFees
    {
        private RentAndFeesPresenter _Presenter;
        public Page ViewPage { get { return Page; } }
        public List<ApplicationFee> ListOfApplicationFees { set { ApplicationFeesGrid.DataSource = value; ApplicationFeesGrid.DataBind(); } }
        public EventHandler AddApplicationFeeClicked { set { AddApplicationFeeButton.Click += value; } }
        public string ApplicationFeeName { get { return ApplicationFeeNameTextBox.Text; } set { ApplicationFeeNameTextBox.Text = value; } }
        public string ApplicationFeeAmount { get { return ApplicationFeeAmountTextBox.Text; } set { ApplicationFeeAmountTextBox.Text = value; } }
        public List<MonthlyFee> ListOfMonthlyFees { set { MonthlyFeesGrid.DataSource = value; MonthlyFeesGrid.DataBind(); } }
        public EventHandler AddMonthlyFeeClicked { set {AddMonthlyFeeButton.Click += value;} }
        public string MonthlyFeeName { get { return MonthlyFeeNameTextBox.Text; } set { MonthlyFeeNameTextBox.Text = value; } }
        public string MonthlyFeeAmount { get { return MonthlyFeeAmountTextBox.Text; } set { MonthlyFeeAmountTextBox.Text = value; } }
        public bool IsFeeForAllResidents { get { return ApplyToAllResidentsCheckBox.Checked; } set { ApplyToAllResidentsCheckBox.Checked = value; } }
        public bool IsIntegratedProperty { get { return IntegratedFeesBreakdownCheckBox.Visible; } set { IntegratedFeesBreakdownCheckBox.Visible = value; } }
        public bool IsIntegratedChecked { get { return IntegratedFeesBreakdownCheckBox.Checked; } set { IntegratedFeesBreakdownCheckBox.Checked = value; } }
        public EventHandler SaveButtonClicked { set { SaveFeesButton.Click += value; } }
		public PlaceHolder SuccessMessagePlaceHolder { get { return successMsg; } }

        public int PropertyId
        {
            get
            {
                var Id = HttpContext.Current.Request.QueryString["propertyId"].ToNullInt32();
                if (!Id.HasValue || Id.Value < 1)
                {
                    BasePage.SafePageRedirect("~/MyProperties/Default.aspx");
                    return 0;
                }

                return Id.Value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new RentAndFeesPresenter(this);
			successMsg.Visible = false;
            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }

        protected void SaveFeesButton_Click(object sender, EventArgs e)
        {

        }
    }
}