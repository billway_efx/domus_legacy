﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PmsAddEdit.ascx.cs" Inherits="Domus.UserControls.Property.PmsAddEdit" %>
<%@ Register Src="~/UserControls/AmsiAddEdit.ascx" TagPrefix="uc1" TagName="AmsiAddEdit" %>
<%@ Register Src="~/UserControls/MriAddEditNew.ascx" TagPrefix="uc1" TagName="MriAddEditNew" %>
<%@ Register Src="~/UserControls/RealPageAddEdit.ascx" TagPrefix="uc1" TagName="RealPageAddEdit" %>



<div class="formWrapper">
    <h2>Property Management</h2>
    <div class="formWhole">
        <div class="formThird">
            <asp:DropDownList ID="PMSDropdown" runat="server" AutoPostBack="True" CssClass="form-control" AppendDataBoundItems="true">
                <asp:ListItem Text="Non-Integrated" Value="-1" />
            </asp:DropDownList>
            <!--tag ends in ProgramInformationAdEdit.ascx-->
        </div>

    </div>

    <asp:MultiView ID="PmsMultiView" runat="server">
        <asp:View ID="BlankView" runat="server" />
        <asp:View ID="YardiView" runat="server">
            <uc2:YardiAddEdit runat="server" ID="YardiAddEditControl" />
        </asp:View>

        <asp:View ID="AmsiView" runat="server">
            <!--cakel: BUGID00316 -->
            <uc1:AmsiAddEdit runat="server" id="AmsiAddEdit" />
        </asp:View>

        <!--cakel: 00351 RealPage -->
        <asp:View ID="RealPageView" runat="server">
            <uc1:RealPageAddEdit runat="server" id="RealPageAddEdit" />

            </asp:View>

        <asp:View ID="MRIView" runat="server">
           <%-- <uc2:MRIAddEdit runat="server" ID="MRIAddEditControl" />--%>
            <uc1:MriAddEditNew runat="server" id="MriAddEditNew" />
        </asp:View>
        


    </asp:MultiView>
</div>
