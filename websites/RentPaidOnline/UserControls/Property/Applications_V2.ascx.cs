﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using EfxFramework;

//cakel: 00546

namespace Domus.UserControls.Property
{
    public partial class Applications_V2 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            string PropertyIDstr = Request.QueryString["propertyId"];
            int PropIdint = int.Parse(PropertyIDstr);

            if(!Page.IsPostBack)
            {
                PopulateApplicationStatusList();
                PopulateSubmissionTypeList();


                Applicant_V2.GetPropertyApplicationDetail ad = Applicant_V2.GetPropertyAppDetail(PropIdint);
                byte[] CheckByte = ad.DocumentBinary;
                if(CheckByte.Length > 10 && CheckByte != null)
                {
                    Button2.Visible = true;
                    Button1.Visible = true;
                }
                else
                {
                    Button2.Visible = false;
                    Button1.Visible = false;
                }

                if(ad.IsIntegrated == 1)
                {
                    pmsIDArea.Visible = true;
                    ImportApplicantBtn.Visible = true;

                }
                else
                {
                    pmsIDArea.Visible = false;
                    ImportApplicantBtn.Visible = false;
                }

            }
            
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            string PropertyIDstr = Request.QueryString["propertyId"];
            int PropIDint = int.Parse(PropertyIDstr);

            if(ApplicationUpload.HasFile)
            {
                string FileExt = Path.GetExtension(ApplicationUpload.FileName);

                Stream fs = ApplicationUpload.PostedFile.InputStream;
                BinaryReader br = new BinaryReader(fs);                   
                Byte[] _bytes = br.ReadBytes((Int32)fs.Length);

                EfxFramework.Property.SetApplicationForResidency(PropIDint, _bytes);

                string NewUrl = "~/MyProperties/PropertyDetails.aspx?propertyId=" + PropIDint + "#ApplicationTab";
                Response.Redirect(NewUrl);

            }

        }

        protected void AddApplicantButton_Click(object sender, EventArgs e)
        {
            //Test: Future Resident t2034438
            bool IsValidated = false;
            string _user =  HttpContext.Current.User.Identity.Name.ToString();

            string PropertyIDstr = Request.QueryString["propertyId"];
            int PropIDint = int.Parse(PropertyIDstr);
            bool _result = Applicant_V2.CheckforExistingRenterEmail(EmailAddressTextBox.Text);

            int AppID = 0;

            if(GridView1.SelectedIndex > -1)
            {
                AppID = (int)GridView1.SelectedDataKey[0];
            }
            else
            {
                AppID = 0;
            }


            if(_result == false)
            {
                //insert new applicant
                string userid = Page.User.Identity.Name.ToString();
                string FirstName = FirstNameTextBox.Text;
                string LastName = LastNameTextBox.Text;
                string PmsID = PmsIdtxt.Text;
                string email = EmailAddressTextBox.Text;
                int AppStatus = int.Parse(ApplicationStatusDropDown.SelectedValue);
                int AppSubmission = int.Parse(ApplicationReceivedDropDown.SelectedValue);

                string IsIntegrated = EfxFramework.Property.GetIntegrationStatus(PropIDint);

                if(IsIntegrated != "Non-Integrated")
                {
                    IsValidated = EfxFramework.Pms.Yardi.YardiCommon.IsValidApplicant(PmsID, PropIDint);
                }
                else
                {
                    IsValidated = false;
                }

                Responselbl.Text = string.Empty;
                //Insert new Applicant or Update existing Applicant in Applicant Table
                AppID = Applicant_V2.Applicant_InsertUpdate(AppID, PropIDint, FirstName, LastName, PmsID, IsValidated, email);

                //Insert / Update ApplicantApplicantion 
                Applicant_V2.ApplicantApplication_InsertUpdate(AppID, PropIDint, AppStatus, false, AppSubmission, DateTime.Now, DateTime.Now, _user);


                //promote Applicant to Renter only if renter is approved and not already a renter
                if (IsIntegrated == "Non-Integrated")
                {
                    if (RenterStatuslbl.Text.Length < 1 && AppStatus == 1)
                    {
                        ApplicantRenter_PromoteApplicantToRenter_v2(AppID, PropIDint);

                    }
                }


                //Check if File Upload has file and if it does upload Application
                if (ApplicationFileUpload.HasFile)
                {
                    string FileExt = Path.GetExtension(ApplicationFileUpload.FileName);

                    Stream fs = ApplicationFileUpload.PostedFile.InputStream;
                    BinaryReader br = new BinaryReader(fs);
                    Byte[] _bytes = br.ReadBytes((Int32)fs.Length);
                    Applicant_V2.ApplicantApplication_InsertUploadedApplication(AppID, _bytes);
                }

                string NewUrl = "~/MyProperties/PropertyDetails.aspx?propertyId=" + PropIDint + "#ApplicationTab";
                Response.Redirect(NewUrl);



                


            }
            else
            {
                //return response of dupe email
                Responselbl.Text = "The following email is not available";
            }

      



        }


        private void PopulateApplicationStatusList()
        {
            ApplicationStatusDropDown.Items.Clear();
            ApplicationStatusDropDown.Items.Add(new ListItem("Select Status", "0"));
            ApplicationStatusDropDown.DataSource = Applicant_V2.Applicant_StatusGetList();
            ApplicationStatusDropDown.DataTextField = "ApplicationStatusName";
            ApplicationStatusDropDown.DataValueField = "ApplicationStatusId";
            ApplicationStatusDropDown.DataBind();

        }

        private void PopulateSubmissionTypeList()
        {
            ApplicationReceivedDropDown.Items.Clear();
            ApplicationReceivedDropDown.Items.Add(new ListItem("Select Received Type", "0"));
            ApplicationReceivedDropDown.DataSource = Applicant_V2.Applicant_SubmissionTypeGetList();
            ApplicationReceivedDropDown.DataTextField = "ApplicationSubmissionTypeName";
            ApplicationReceivedDropDown.DataValueField = "ApplicationSubmissionTypeId";
            ApplicationReceivedDropDown.DataBind();

        }

        private void PopulateApplicantDetails(int ApplicantID)
        {
            SqlDataReader Reader = Applicant_V2.Applicant_GetApplicantDetail(ApplicantID);
            Reader.Read();
            string HasFile = "";
            string IsRenter = "";
            if(Reader.HasRows)
            {
                CurrentApplicantHidden.Value = Reader["ApplicantId"].ToString();
                FirstNameTextBox.Text = Reader["FirstName"].ToString();
                LastNameTextBox.Text = Reader["LastName"].ToString();
                PmsIdtxt.Text = Reader["PmsID"].ToString();
                EmailAddressTextBox.Text = Reader["PrimaryEmailAddress"].ToString();
                ApplicationReceivedDropDown.SelectedValue = Reader["ApplicationSubmissionTypeId"].ToString();
                ApplicationStatusDropDown.SelectedValue = Reader["ApplicationStatusId"].ToString();
                HasFile = Reader["HasFile"].ToString();
                IsRenter = Reader["IsRenter"].ToString();


                if(HasFile == "1")
                {
                    ViewAppbtn.Visible = true;
                }
                else
                {
                    ViewAppbtn.Visible = false;
                }

                if(IsRenter == "Yes")
                {
                    RenterStatuslbl.Text = "Converted to Renter on " + Reader["ConvertedDate"].ToString();

                }
                else
                {
                    RenterStatuslbl.Text = string.Empty;
                }



            }
            Reader.Close();


        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string PropertyIDstr = Request.QueryString["propertyId"];
            int PropIdint = int.Parse(PropertyIDstr);

            int _status = Applicant_V2.DownLoadPropertyApplication(PropIdint);
           
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            
        }

        protected void AppClearBtn_Click(object sender, EventArgs e)
        {
            CurrentApplicantHidden.Value = string.Empty;
            GridView1.SelectedIndex = -1;
            FirstNameTextBox.Text = string.Empty;
            LastNameTextBox.Text = string.Empty;
            EmailAddressTextBox.Text = string.Empty;
            ApplicationReceivedDropDown.SelectedIndex = 0;
            ApplicationStatusDropDown.SelectedIndex = 0;
            
            


        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int ApplicantID = (int)GridView1.SelectedDataKey[0];
            CurrentApplicantHidden.Value = ApplicantID.ToString();

            PopulateApplicantDetails(ApplicantID);
            

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            ApplicationFileUpload.PostedFile.InputStream.Dispose();
            ApplicationFileUpload.Dispose();
        }

        protected void ViewAppbtn_Click(object sender, EventArgs e)
        {
            int ApplicantID = (int)GridView1.SelectedDataKey[0];
            int AppStatus = Applicant_V2.DownloadApplicantsApplication(ApplicantID);



        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if(e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                string RenterID = GridView1.DataKeys[e.Row.RowIndex].Values[3].ToString();


                Label Isvalid = (Label)e.Row.Cells[10].FindControl("Isvalidatedlbl");
                Label statuslbl = (Label)e.Row.Cells[0].FindControl("Label3");
                Label HasFile = (Label)e.Row.Cells[7].FindControl("HasFilelbl");
                LinkButton _RenterID = (LinkButton)e.Row.Cells[4].FindControl("LinkButton2");

                if(Isvalid.Text == "True")
                {
                    statuslbl.Text = "Validated";

                }

                if(HasFile.Text == "1")
                {
                    HasFile.Text = "Yes";
                }
                else
                {
                    HasFile.Text = "No";
                }

                if(RenterID.Length > 0)
                {
                    _RenterID.Visible = true;

                }
                else
                {
                    _RenterID.Visible = false;

                }

            }
            

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if(e.CommandName == "GoToRenter")
            {
                string index = e.CommandArgument.ToString();
                Response.Redirect("/Renters/RenterDetails.aspx?Renterid=" + index);
            }
        }


        private void ApplicantRenter_PromoteApplicantToRenter_v2(int Appid, int PropID)
        {
            string ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_ApplicantRenter_PromoteApplicantToRenter_v2", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ApplicantID", SqlDbType.Int).Value = Appid;
            com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = PropID;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }

        }

        protected void Button4_Click(object sender, EventArgs e)
        {

            int propID = int.Parse(Request.QueryString["propertyId"]);
            ImportApplicantBtn.Disabled = true;
            EfxFramework.Pms.Yardi.YardiCommon.ImportYardiApplicantsByProperty(propID);
            GridView1.DataBind();
            ImportApplicantBtn.Disabled = false;

        }



    }
}