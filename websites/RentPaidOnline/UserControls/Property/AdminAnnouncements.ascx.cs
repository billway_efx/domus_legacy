﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Domus.UserControls.Property
{
    public partial class AdminAnnouncements : UserControl, IPropertyAnnouncements
    {
        private PropertyAnnouncementsPresenter _Presenter;
        public string TitleText { get { return AnnouncementTitleTextBox.Text; } set { AnnouncementTitleTextBox.Text = value; } }
        public string BodyText { get { return AnnouncementBodyTextArea.InnerText; } set { AnnouncementBodyTextArea.InnerText = value; } }
        public string UrlText { get { return AnnouncementUrlTextBox.Text; } set { AnnouncementUrlTextBox.Text = value; } }
        public bool CheckBoxIsChecked { get { return PostAllPropertiesCheckBox.Checked; } set { PostAllPropertiesCheckBox.Checked = value; } }
        public bool CheckBoxIsVisible { get { return PostAllPropertiesCheckBox.Visible; } set { PostAllPropertiesCheckBox.Visible = value; } }
        public EventHandler SaveButtonClicked { set { SaveButton.Click += value; } }
        public GridView PostedAnnoucementGrid { get { return AllPostedAnnouncementsGrid; } set { AllPostedAnnouncementsGrid = value; } }
        public Page ViewPage { get { return Page; } }
        public GridViewSelectEventHandler EditRow { set { AllPostedAnnouncementsGrid.SelectedIndexChanging += value; } }
        public GridViewDeleteEventHandler DeleteRow { set { AllPostedAnnouncementsGrid.RowDeleting += value; } }
        public bool IsAdminSite { get { return EfxFramework.Helpers.Helper.IsRpoAdmin; } }
        public HiddenField EditEnabled { get { return CanEditGrid; } set { CanEditGrid = value; } }
        public HiddenField AnnouncementId { get { return IdValue; } set { IdValue = value; } }
        public List<int> PropertyIDs { get { return PropertyID_list; } set { PropertyIDs = PropertyID_list; } }
        //CMallory - Task 00462 - Added following 3 properties.
        private List<int> PropertyID_list = new List<int>();
        public DateTime ExpirationDate  { get { return DateTime.ParseExact(Calendar.SelectedDate.ToString("MM/dd/yyyy hh:mm tt"), "MM/dd/yyyy hh:mm tt", null); } set { ExpDate.Text = value.ToString(); } }//{ get { return DateTime.ParseExact("12/01/2015 02:22 PM", "MM/dd/yyyy hh:mm tt", null); } set { ExpDate.Text = value.ToString(); } }
        public EventHandler ViewAnnouncementsButtonClicked { set { ViewAnnouncements.Click += value; } }
        public int PropertyId
        { 
            get
            {
                int test2 = RPO.Helpers.Helper.CurrentUserId;
                var Id = HttpContext.Current.Request.QueryString["propertyId"].ToNullInt32();
                if (!Id.HasValue || Id.Value < 1)
                {
                    //CMallory - Task 00462 - Commented out so all users can view the Announcement page.
                    //BasePage.SafePageRedirect("~/MyProperties/Default.aspx");
                    return 0;
                }

                return Id.Value;
            }
        }

        public int CurrentStaff
        {
            get
            {
                var Id = HttpContext.Current.User.Identity.Name.ToNullInt32();
                if (!Id.HasValue || Id.Value < 1)
                {
                    BasePage.SafePageRedirect("~/Account/Login.aspx");
                    return 0;
                }
                return Id.Value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new PropertyAnnouncementsPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
            Calendar.Visible = false;
            ExpDate.Text = null;
        }
        //CMallory - Task 00462 - Populates a string of Property ID numbers to send to presenter.
        public String GetPropertyString()
        {

            CheckBoxList ckbx = (CheckBoxList)this.PropertyPicker.FindControl("CheckBoxList1");
            
            List<String> PropertyName_list = new List<string>();

            foreach (ListItem item in ckbx.Items)
            {
                if (item.Selected)
                {
                    PropertyID_list.Add(item.Value.ToInt32());
                    PropertyName_list.Add(item.Text);
                }
            }
            string PropList = String.Join(",", PropertyID_list.ToArray());
            return PropList;
        }
        //CMallory - Task 00462 - Added
        protected void SaveButton_Click(object sender, EventArgs e)
        {
            GetPropertyString();
        }
        //CMallory - Task 00462 - Added
        protected void CalendarPic_Click(object sender, ImageClickEventArgs e)
        {
            Calendar.Visible = true;
        }

        //CMallory - Task 00462 - Added
        protected void Calendar_SelectionChanged(object sender, EventArgs e)
        {
            //cakel: 00462 - Updated date string
            ExpDate.Text = Calendar.SelectedDate.ToString("M/dd/yyyy hh:mm tt");
        }

        //CMallory - Task 00462 - Added
        protected void ViewAnnouncements_Click(object sender, EventArgs e)
        {
            GetPropertyString();
        }
    }
}