﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyDetailsAddEdit.ascx.cs" Inherits="Domus.UserControls.Property.PropertyDetailsAddEdit" %>


<div class="formTint">
    <div class="formWrapper">
        <asp:ValidationSummary ID="PropertySummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="PropertySummary" />
        <h2>Property Information</h2>
        <div class="formWhole">
            <label>Property Management Company</label>
            <div class="formHalf no-left-padding">
                <asp:DropDownList ID="PmCompanyDropdown" runat="server" DataTextField="CompanyName" DataValueField="CompanyId" CssClass="form-control" AppendDataBoundItems="True">
                    <asp:ListItem Text="--Select a Property Management Company--" Value="-1" />
                </asp:DropDownList>
            </div>
            <div class="formHalf">
                <!-- CMallory - Task 00166 - Added Checkbox to make a property public. -->
                <asp:CheckBox ID="PropertyIsPublicCheckBox" runat="server" Text="&nbsp; Property Is Public" />
            </div>
        </div>



        <div class="formWhole">
            <div class="formHalf">
                <label>Property Name</label>
                <input runat="server" id="PropertyNameTextbox" name="PropertyNameTextbox" type="text" class="form-control" maxlength="80" />
            </div>
            <div class="formHalf">
                <label>Property Name 2</label>
                <input runat="server" id="PropertyNameTwoTextbox" name="PropertyNameTwoTextbox" type="text" class="form-control" maxlength="100" />
            </div>

        </div>
        <div class="formWhole">
            <div class="formHalf">
                <label>Property Code</label>
                <input runat="server" id="PropertyCodeTextbox" name="PropertyCodeTextbox" type="text" class="form-control" />
            </div>
            <div class="formHalf">
                <label>Number of Units</label>
                <input runat="server" id="NumberOfUnitsTextbox" name="NumberOfUnitsTextbox" type="text" class="form-control" />
            </div>
        </div>




    </div>
</div>
<br />
<div class="formWrapper">
    <h2>Property Address</h2>
    <div class="formWhole">
        <div class="formHalf">
            <label>Property Address</label>
            <input runat="server" id="PropertyAddressTextbox" name="PropertyAddressTextbox" type="text" class="form-control" />
        </div>

        <div class="formHalf">
            <label>Property Address 2</label>
            <input runat="server" id="PropertyAddress2Textbox" name="PropertyAddress2Textbox" type="text" class="form-control" />
        </div>
    </div>
    <div class="formWhole">
        <div class="formHalf">
            <label>City</label>
            <input runat="server" id="CityTextbox" name="CityTextbox" type="text" class="form-control" />
        </div>
        <div class="formHalf">
            <div class="formHalf">
                <label>State</label>
                <asp:DropDownList ID="StateDropdown" runat="server" CssClass="form-control" />
            </div>
            <div class="formHalf">
                <label>Postal Code</label>
                <input runat="server" id="PostalCodeTextbox" name="PostalCodeTextbox" type="text" class="form-control" />
            </div>
        </div>
    </div>
</div>
<br />
<div class="formTint">
    <br />
    <div class="formWrapper">
        <h2>Property Contact Information</h2>
        <div class="formWhole">
            <div class="formThird">
                <label>Main Phone Number</label>
                <input runat="server" id="MainTelephoneTextbox" name="MainTelephoneTextbox" type="text" class="form-control" maxlength="14" />
            </div>
            <div class="formThird">
                <label>Alternate Phone Number</label>
                <input runat="server" id="AltTelephoneTextbox" name="AltTelephoneTextbox" type="text" class="form-control" maxlength="14" />
            </div>
            <div class="formThird">
                <label>Fax Number</label>
                <input runat="server" id="FaxNumberTextbox" name="FaxNumberTextbox" type="text" class="form-control" maxlength="14" />
            </div>
        </div>
        <div class="formHalf">
            <label>Office Email</label>
            <input runat="server" id="OfficeEmailTextbox" name="OfficeEmailTextbox" type="text" class="form-control" />
        </div>
        <!-- Patrick Whittingham - 9/2/15 - task #371 : add notification email address  -->
        <div class="formHalf">
            <label>Notification Email</label>
            <input runat="server" id="NotificationEmailTextbox" name="NotificationEmailTextbox" type="text" class="form-control" />
        </div>
        <div class="formWhole">
            <hr />
        </div>

        <div class="formHalf">
            <label>Property Time Zone</label>
            <asp:DropDownList ID="uxTimeZoneDropDown" runat="server" CssClass="form-control">
                <asp:ListItem Text="--Select a time zone for this property--" Value="" />
                <asp:ListItem Text="UTC-04:00 Atlantic Standard Time" Value="Atlantic Standard Time" />
                <asp:ListItem Text="UTC-05:00 Eastern Standard Time" Value="Eastern Standard Time" />
                <asp:ListItem Text="UTC-06:00 Central Standard Time" Value="Central Standard Time" />
                <asp:ListItem Text="UTC-07:00 Mountain Standard Time" Value="Mountain Standard Time" />
                <asp:ListItem Text="UTC-08:00 Pacific Standard Time" Value="Pacific Standard Time" />
                <asp:ListItem Text="UTC-09:00 Alaskan Standard Time" Value="Alaskan Standard Time" />
                <asp:ListItem Text="UTC-10:00 Hawaiian Standard Time" Value="Hawaiian Standard Time" />
            </asp:DropDownList>
        </div>

        <div class="formWhole">
            <hr />
        </div>

        <div class="formWhole">
            <div class="formThird">
                <label>&nbsp; Average Monthly Rent</label>
                <div class="formWhole">
                    <input runat="server" id="AverageRentTextbox" name="AverageRentTextbox" type="text" class="form-control" onblur="CalculateConvenienceFee()" />
                </div>
            </div>
            <div class="formTwoThirds">
                <label>&nbsp; Resident's Credit Card Convenience Fee</label>
                <div class="formHalf">
                    <input runat="server" id="ConvenienceFeeTextbox" name="ConvenienceFeeTextbox" type="text" readonly="readonly" class="form-control" />
                </div>
            </div>
        </div>

        <!-- cakel: BugID0007 added-->
        <div class="formWhole">
            <label>&nbsp;Payment Due On:</label>
            <div class="formWhole">
                <asp:DropDownList ID="PaymentDueDDL" runat="server" CssClass="form-control" Width="125px">
                </asp:DropDownList>
            </div>
        </div>
        <!--END ****  cakel BugID0007     -->

        <div class="formWhole">
            <hr style="margin-top: 0px" />
        </div>
        <!--cakel: BUGID00019 - Updated order of checkboxes, added new checkbox (AllowPartialPaymentResidentPortalCkBx) -->
        <!-- CMallory - Task 00425 - Added Disable Property Payments Checkbox -->
        <div class="formWhole">
            <div class="formHalf">
                <asp:CheckBox ID="DisableAllPaymentsCheckBox" OnClick="DisableCheckBox(this.checked);" runat="server" Text="&nbsp; Disable Property Payments" />
            </div>
            <div class="formHalf">
                <asp:CheckBox ID="LineItemDetailCheckBox" runat="server" Text="&nbsp; Display line item fee detail to residents" />
            </div>
            <div class="clear"></div>
            <div class="formHalf no-left-padding">
                <asp:CheckBox ID="RentReportersVerifiedCheckBox" runat="server" Text="&nbsp; RentReporters Verified" />
            </div>

            <div class="formHalf">
                <asp:CheckBox ID="AllowPartialPaymentsCheckBox" runat="server" Text="&nbsp; Allow Partial Payments - Admin Portal" />&nbsp;<br />
                <div class="clear"></div>
            </div>
            <div class="formHalf no-left-padding">
                <asp:CheckBox ID="AllowPartialPaymentsResidentPortalCkBx" runat="server" Text="&nbsp; Allow Partial Payments - Resident Portal" />
            </div>

            <div class="formHalf">
                <asp:CheckBox ID="AcceptCreditCardPaymentsCheckBox" OnClick="OtherCheckBoxes(this.checked);" runat="server" Text="&nbsp; Accept Credit Card Payments" />
            </div>
            <div class="clear"></div>
            <div class="formHalf no-left-padding">
                <asp:CheckBox ID="PNMEnabledCheckBox" OnClick="OtherCheckBoxes(this.checked);" runat="server" Text="&nbsp; PNM Enabled" />
            </div>


            <!--cakel: BugID0007 added -->
            <div class="formHalf">
                <asp:CheckBox ID="AllowPrePaymentsCK" runat="server" Text="&nbsp; Allow PrePayments" AutoPostBack="True" />
            </div>
            <div class="clear"></div>
            <div class="formHalf no-left-padding">
                <asp:CheckBox ID="AcceptACHPaymentsCheckBox" OnClick="OtherCheckBoxes(this.checked);" runat="server" Text="&nbsp; Accept ACH Payments" />
            </div>
            <div class="clear"></div>
            <div class="formHalf no-left-padding">
                <!-- CMallory - Task 00554 - Added Disable Pay By Text Checkbox -->
                <asp:CheckBox ID="DisablePayByTextCheckBox" runat="server" Text="&nbsp; Disable Pay By Text" />
            </div>
            <div class="clear"></div>
            <div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <br />
                        <asp:Label ID="DayAllowedlbl" runat="server" Text="Days Before the 1st: "></asp:Label><br />
                        <asp:DropDownList ID="PrePayDaysDL" runat="server" CssClass="form-control" Width="100px">
                        </asp:DropDownList>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="clear"></div>
            <!--END *******cakel: BugID0007 added -->

            <div class="formHalf">
            </div>
            <div class="clear"></div>

        </div>




        <%--        </div>--%><%-- bottom --%>
    </div>
</div>
<br />

<script>
    $(function () {
        /* Mask */
        ApplySingleMask("#<%=PostalCodeTextbox.ClientID%>", "00000");
        ApplySingleMask("#<%=MainTelephoneTextbox.ClientID%>", "(000) 000-0000");
        ApplySingleMask("#<%=AltTelephoneTextbox.ClientID%>", "(000) 000-0000");
        ApplySingleMask("#<%=FaxNumberTextbox.ClientID%>", "(000) 000-0000");
    });

    //CMallory - Task 00425 - Javascript that handles unchecking the other payment checkboxes if the Disable All Payments checkbox is checked.
    function DisableCheckBox(checked) {

        var error = false, errorText = "";
        if (checked) {
            document.getElementById("<%=AcceptACHPaymentsCheckBox.ClientID%>").checked = false;
            document.getElementById("<%=AcceptCreditCardPaymentsCheckBox.ClientID%>").checked = false;
            document.getElementById("<%=PNMEnabledCheckBox.ClientID%>").checked = false;
            document.getElementById("<%=DisablePayByTextCheckBox.ClientID%>").checked = true;
        }

    }

    //CMallory - Task 00425 - Javascript that handles unchecking the Disable All Payments checkbox if another payment checkbox is checked.
    function OtherCheckBoxes(checked) {

        var error = false, errorText = "";
        if (checked) {
            document.getElementById("<%=DisableAllPaymentsCheckBox.ClientID%>").checked = false;
        }

    }

</script>
