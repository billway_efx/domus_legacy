﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Text;
using Mb2x.ExtensionMethods;

namespace RentPaidOnline.UserControls.Property
{
    public partial class SMSMarketing : System.Web.UI.UserControl
    {
       
        public class RenterSMSDetails
        {
            public int RenterId;
            public String PmsId, FirstName, LastName, MainPhoneNumber, MobilePhoneNumber, ErrorMessage;
            public bool sentCell, sentMain;

            

            public RenterSMSDetails(int RID, String PID,String FName, String LName, String MainPhone, String CellPhone)
            {
                this.RenterId = RID;
                this.PmsId = PID;
                this.FirstName = FName;
                this.LastName = LName;
                this.MainPhoneNumber = MainPhone;
                this.MobilePhoneNumber = CellPhone;
                this.sentCell = true;
                this.sentMain = true;
                this.ErrorMessage = "";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void SendButton_Click(object sender, EventArgs e)
        {
            String messageText = messagetxt.Text.Trim();
            String propList = GetPropertyIDs();

            if (messagetxt.Text.Length > ConfigurationManager.AppSettings["SMSMaxLength"].ToInt32())
            {
                errorlbl.Text = "Maximum Length is " + ConfigurationManager.AppSettings["SMSMaxLength"] + " chracters. \r\nYou have " + messagetxt.Text.Length.ToString() + " in your message.";
                return;
            }
            

            if (messageText.Length == 0)
            {
                //No text, do not send
                errorlbl.Text = "Can not send blank message.";
                return;
            }

            //messagetxt.Text = propList;
            List<RenterSMSDetails> renterDetails = GetSMSDetails_ForList(propList);


            using (var Client = new EfxFramework.EfxSms.MessageMediaService())
            {
                List<EfxFramework.EfxSms.RecipientType> recipientList = new List<EfxFramework.EfxSms.RecipientType>();

                foreach (RenterSMSDetails Renter in renterDetails)
                {
                    String phoneNumber = Renter.MobilePhoneNumber.Trim();
                    String id = "0";

                    if (phoneNumber == "")
                    {
                        Renter.sentCell = false;
                        Renter.ErrorMessage = "No number";
                        continue;
                    }

                    if (!phoneNumber.StartsWith("1"))
                    { phoneNumber = phoneNumber.Insert(0, "1"); }

                    Renter.ErrorMessage = "";
                    recipientList.Add(new EfxFramework.EfxSms.RecipientType { Value = phoneNumber, uid = UInt32.Parse(id ?? "0") });
                }

                //SWitherspoon 03/16/2018: Added "No reply" at request of Ashleigh
                messageText += "\r\n\r\nDo not reply. Contact leasing office for questions.";

                var Message = new EfxFramework.EfxSms.MessageType { content = messageText };
                var MessageList = new EfxFramework.EfxSms.MessageListType();
                var MessageBody = new EfxFramework.EfxSms.SendMessagesBodyType();
                var Auth = new EfxFramework.EfxSms.AuthenticationType { userId = EfxSettings.EfxSmsUserId, password = EfxSettings.EfxSmsPassword };

                Message.recipients = recipientList.ToArray();
                MessageList.message = new[] { Message };
                MessageBody.messages = MessageList;

                //MessageList.sendMode = EfxFramework.EfxSms.MessageSendModeType.dropAllWithErrors;

                var testObj = Client.sendMessages(Auth, MessageBody);

                //For each failed message, make note
                EfxFramework.EfxSms.MessageErrorType[] errors = testObj.errors;

                renterDetailslst.Items.Clear();

                if (errors != null)
                {
                    foreach (EfxFramework.EfxSms.MessageErrorType error in errors)
                    {
                        renterDetailslst.Items.Add(new ListItem("Error Code:" + error.code));
                        renterDetailslst.Items.Add(new ListItem("Error Content:" + error.content));
                        foreach (EfxFramework.EfxSms.RecipientType Recipient in error.recipients)
                        {
                            renterDetailslst.Items.Add(new ListItem("Error Recipients:" + Recipient.Value));
                            RenterSMSDetails r = renterDetails.FirstOrDefault(o => o.MobilePhoneNumber == Recipient.Value);
                            if (r != null)
                            {
                                r.sentCell = false;
                                r.ErrorMessage = error.code.ToString();
                            }
                        }
                        renterDetailslst.Items.Add(new ListItem("Error Seq Num:" + error.sequenceNumber));
                    }
                }


                //ROUND 2, try MainPhone for any with no Cell or that failed
                List<EfxFramework.EfxSms.RecipientType> recipientList2 = new List<EfxFramework.EfxSms.RecipientType>();

                foreach (RenterSMSDetails Renter in renterDetails)
                {
                    String phoneNumber = Renter.MainPhoneNumber.Trim();
                    String id = "0";

                    if (phoneNumber == "")
                    {
                        Renter.sentMain = false;
                        Renter.ErrorMessage = "No number";
                        continue;
                    }
                    else if (Renter.sentCell)
                    {
                        Renter.sentMain = false;
                        Renter.ErrorMessage = "";
                        continue;
                    }

                    if (!phoneNumber.StartsWith("1"))
                    { phoneNumber = phoneNumber.Insert(0, "1"); }

                    Renter.ErrorMessage = "";
                    recipientList2.Add(new EfxFramework.EfxSms.RecipientType { Value = phoneNumber, uid = UInt32.Parse(id ?? "0") });
                }


                Message = new EfxFramework.EfxSms.MessageType { content = messageText };
                MessageList = new EfxFramework.EfxSms.MessageListType();
                MessageBody = new EfxFramework.EfxSms.SendMessagesBodyType();
                Auth = new EfxFramework.EfxSms.AuthenticationType { userId = EfxSettings.EfxSmsUserId, password = EfxSettings.EfxSmsPassword };

                Message.recipients = recipientList2.ToArray();
                MessageList.message = new[] { Message };
                MessageBody.messages = MessageList;

                testObj = Client.sendMessages(Auth, MessageBody);

                //For each failed message, make note
                errors = testObj.errors;


                if (errors != null)
                {
                    foreach (EfxFramework.EfxSms.MessageErrorType error in errors)
                    {
                        renterDetailslst.Items.Add(new ListItem("Error Code:" + error.code));
                        renterDetailslst.Items.Add(new ListItem("Error Content:" + error.content));
                        foreach (EfxFramework.EfxSms.RecipientType Recipient in error.recipients)
                        {
                            renterDetailslst.Items.Add(new ListItem("Error Recipients:" + Recipient.Value));
                            RenterSMSDetails r = renterDetails.FirstOrDefault(o => o.MainPhoneNumber == Recipient.Value);
                            if (r != null)
                            {
                                r.sentCell = false;
                                r.ErrorMessage = error.code.ToString();
                            }
                        }
                        renterDetailslst.Items.Add(new ListItem("Error Seq Num:" + error.sequenceNumber));
                    }
                }

            }//End Using: End of sendMessages section
            errorlbl.Text = "Message Sent";

            //Populate results grid: Testing only
            populateResultsGrid(renterDetails);


            //Construct report email to send
            int userID = int.Parse(Page.User.Identity.Name);
            List<MailAddress> recipients = new List<MailAddress>();

            MailAddress fromAddress = new MailAddress("support@domusme.com");
            string subject = "SMS Marketing Report";
            StringBuilder messageBody = new StringBuilder();
            messageBody.Append("The following table lists all users to whom we were unable to send your SMS broadcast.<br /><br />");
            messageBody.Append("Message Sent: " + messageText + "<br /><br />");

            //Get currentUsers email
            if (EfxFramework.Helpers.Helper.IsRpoAdmin)
            {
                //This is an RPO Admin, pull from EfxAdmin table
                var Admin = new EfxAdministrator(userID);
                recipients.Add(new MailAddress(Admin.PrimaryEmailAddress));
            }
            else
            {
                //This is a normal user, pul from PropertyStaff table
                var Staff = new PropertyStaff(userID);
                recipients.Add(new MailAddress(Staff.PrimaryEmailAddress));
            }

            //Populate email with list of Renters that failed and why they failed
            messageBody.Append("<table><tr><th>Renter ID</th><th>Renter Name</th><th>Failure Reason</th></tr>");
            foreach (RenterSMSDetails renter in renterDetails)
            {
                if (!renter.sentCell && !renter.sentMain)
                {
                    messageBody.Append("<tr><td>");
                    messageBody.Append(renter.PmsId);
                    messageBody.Append("</td><td>");
                    messageBody.Append(renter.FirstName + " " + renter.LastName);
                    messageBody.Append("</td><td>");
                    messageBody.Append(renter.ErrorMessage);
                    messageBody.Append("</td></tr>");
                }
            }
            messageBody.Append("</table>");

            sendReportEmail(fromAddress, subject, messageBody.ToString(), recipients);

        }

        public void populateResultsGrid(List<RenterSMSDetails> renterDetails)
        {
            resultsGrid.Visible = false;
            DataTable dt = new DataTable();

            foreach (RenterSMSDetails renter in renterDetails)
            {
                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);
            }

            resultsGrid.DataSource = dt;
            resultsGrid.DataBind();

            int i = 0;
            foreach (GridViewRow row in resultsGrid.Rows)
            {
                Label RIDlbl = row.FindControl("renterIDlbl") as Label;
                Label FNamelbl = row.FindControl("firstNamelbl") as Label;
                Label LNamelbl = row.FindControl("lastNamelbl") as Label;
                Label ErrorMsglbl = row.FindControl("errorMessagelbl") as Label;

                RIDlbl.Text = renterDetails[i].RenterId.ToString();
                FNamelbl.Text = renterDetails[i].FirstName;
                LNamelbl.Text = renterDetails[i].LastName;
                ErrorMsglbl.Text = renterDetails[i].ErrorMessage;

                i++;
            }
        }

        public void sendReportEmail(MailAddress fromAddress, string subject, string messageBody, List<MailAddress> recipients)
        {
            MailMessage message = new MailMessage();
            message.From = fromAddress;
            string TextRecipientList = "";

            foreach (MailAddress ma in recipients)
            {
                message.To.Add(ma);
                TextRecipientList = TextRecipientList + "; " + ma.Address;
            }
            message.IsBodyHtml = true;
            message.Subject = subject;
            message.Body = messageBody;
            SmtpClient client = new SmtpClient();

            client.Host = EfxSettings.SmtpHost;

            if (message.Bcc.Count > 0 || message.CC.Count > 0 || message.To.Count > 0)
            {
                var log = new RPO.ActivityLog();
                try
                {
                    client.Send(message);
                    log.WriteLog("BulkMail", "Email Successfully Sent to: " + TextRecipientList + "; Subject: " + subject + "; Body: " + messageBody
                       , 1, "", false);
                }
                catch (Exception ex)
                {
                    log.WriteLog("BulkMail", "Unable to send email. Error: " + ex.ToString() + "; Recipients: " + TextRecipientList + "; Subject: " + subject + "; Body: " + messageBody
                        , 1, "", false);
                }
            }
        }

        public string GetPropertyIDs()
        {
            CheckBoxList ckbx = (CheckBoxList)this.PropertyPicker.FindControl("CheckBoxList1");
            List<String> PropertyID_list = new List<string>();

            foreach (ListItem item in ckbx.Items)
            {
                if (item.Selected)
                {
                    PropertyID_list.Add(item.Value);
                }
            }
            string PropList = String.Join(",", PropertyID_list.ToArray());
            return PropList;
        }

        public static List<RenterSMSDetails> GetSMSDetails_ForList(String propList)
        {
            List<RenterSMSDetails> SMSDetails = new List<RenterSMSDetails>();

            DataSet DS = new DataSet();
            SqlConnection con = new SqlConnection(EfxSettings.ConnectionString);
            SqlCommand com = new SqlCommand("Renter_GetSMSDetails_ForPropertyList", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@propList", SqlDbType.NVarChar, -1).Value = propList;

            try
            {
                con.Open();
                SqlDataAdapter SqlAdapt = new SqlDataAdapter(com);
                SqlAdapt.Fill(DS, "Data");
                con.Close();
            }
            catch (SqlException)
            {
                con.Close();
                throw;
            }

            foreach (DataRow row in DS.Tables[0].Rows)
            {
                SMSDetails.Add(new RenterSMSDetails(
                    int.Parse(row["RenterID"].ToString()),
                    row["PmsID"].ToString(),
                    row["FirstName"].ToString(),
                    row["LastName"].ToString(),
                    row["MainPhoneNumber"].ToString(),
                    row["MobilePhoneNumber"].ToString()
                    ));        
            }

            return SMSDetails;
        }

        protected void messagetxt_TextChanged(object sender, EventArgs e)
        {
            if (messagetxt.Text.Length > 160)
            {
                errorlbl.Text = "SMS Message can not exceed 160 characters";
                
            }
            else
            {
                errorlbl.Text = "";
            }
        }
    }
}