﻿using EfxFramework.Interfaces.UserControls.PaymentControlsV2;
using EfxFramework.Interfaces.UserControls.Property;
using EfxFramework.Presenters.UserControls.Property;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domus.UserControls.Property
{
    public partial class Applications : UserControl, IApplications
    {
        private ApplicationsPresenter _Presenter;

        public int? ApplicantApplicationId { get { return HttpContext.Current.Request.QueryString["ApplicantApplicationId"].ToNullInt32(); } }
        public Page ParentPage { get { return Page; } }
        public byte[] ApplicationBytes { get { return ApplicationUpload.HasFile ? ApplicationUpload.FileBytes : null; } }
        public string ApplicationLabelText { set { ApplicationLabel.Text = value; } }
        public EventHandler UploadButtonClicked { set { UploadButton.Click += value; } }
        public bool DeleteButtonVisible { set { DeleteButton.Visible = value; } }
        public EventHandler DeleteButtonClicked { set { DeleteButton.Click += value; } }
        public string FirstNameText { get { return FirstNameTextBox.Text; } }
        public string LastNameText { get { return LastNameTextBox.Text; } }
        public string EmailAddressText { get { return EmailAddressTextBox.Text; } }
        public DropDownList ApplicationStatusList { get { return ApplicationStatusDropDown; } }
        public DropDownList ApplicationReceivedList { get { return ApplicationReceivedDropDown; } }
        public EventHandler AddApplicantClicked { set { AddApplicantButton.Click += value; } }
        public GridView ApplicantsGrid { get { return ApplicantGridView; } }
        public EventHandler SaveApplicantsClicked { set { SaveApplicantsButton.Click += value; } }
        public IPersonalInformation PersonalInformationControl { get { return PersonalInformation; } }
        public IResidentBillingInformation BillingInformationControl { get { return BillingInformation; } }
        public ISelectPaymentType SelectPaymentControl { get { return SelectPaymentType; } }
        public IPaymentAmountsAlternate PaymentAmountsControl { get { return PaymentAmounts; } }
        public EventHandler SubmitPaymentClicked { set { SubmitPaymentButton.Click += value; } }
        public bool IsPropertyManagerSite { get { return true; } }
        public bool PaymentButtonEnabled { set { SubmitPaymentButton.Enabled = value; } }
        public GridViewRowEventHandler RowBound { set { ApplicantsGrid.RowDataBound += value; } }

        public int PropertyId
        {
            get
            {
                var Id = HttpContext.Current.Request.QueryString["propertyId"].ToNullInt32();
                if (!Id.HasValue || Id.Value < 1)
                {
                    BasePage.SafePageRedirect("~/MyProperties/Default.aspx");
                    return 0;
                }

                return Id.Value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new ApplicationsPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }


        //cakel: BUGID00033 saves new values when selected index changes
        protected void StatusDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Get index row from selected dropdown
            DropDownList ddl = (DropDownList)sender;
            GridViewRow row = (GridViewRow)ddl.Parent.Parent;

            //assign row index to variable
            int myrowIndex = row.RowIndex;

            //Get ApplicationID from Hidden Field
            HiddenField hf = (HiddenField)ApplicantGridView.Rows[myrowIndex].Cells[0].FindControl("ApplicantApplicationId");
            int _applicationID = hf.Value.ToInt32();

            //Get Application Recieved value from dropdown
            DropDownList dl = (DropDownList)ApplicantGridView.Rows[myrowIndex].Cells[3].FindControl("ReceivedDropDown");
            int _apprec = dl.SelectedValue.ToInt32();

            //Gets Status ID for update
            int _status = ddl.SelectedValue.ToInt32();

            string _updatedBY = "";

            DLAccess.SetApplicationStatus(_applicationID, _status, _apprec, _updatedBY);

            //Refreshes Gridview to new values
            _Presenter.InitializeValues();
            populateSaveMsg();
        }

        //cakel: BUGID00033
        protected void ReceivedDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Get index row from selected dropdown
            DropDownList ddl = (DropDownList)sender;
            GridViewRow row = (GridViewRow)ddl.Parent.Parent;

            //assign row index to variable
            int myrowIndex = row.RowIndex;

            //Get ApplicationID from Hidden Field
            HiddenField hf = (HiddenField)ApplicantGridView.Rows[myrowIndex].Cells[0].FindControl("ApplicantApplicationId");
            int _applicationID = hf.Value.ToInt32();


            //Get Application Status value from dropdown
            DropDownList dl = (DropDownList)ApplicantGridView.Rows[myrowIndex].Cells[5].FindControl("StatusDropDown");
            int _status = dl.SelectedValue.ToInt32();

            //Gets Application Recieved Type ID for update
            int _apprec = ddl.SelectedValue.ToInt32();

            string _updatedBY = "";

            DLAccess.SetApplicationStatus(_applicationID, _status, _apprec, _updatedBY);

            //Refreshes Gridview to new values
            _Presenter.InitializeValues();
            populateSaveMsg();
        }

        //cakel: BUGID00033
        private void populateSaveMsg()
        {
            SaveSuccessPanel.Update();
            SaveStatusArea.Visible = true;
            SaveStatusLabel.Text = "Success!";
        }


    }
}