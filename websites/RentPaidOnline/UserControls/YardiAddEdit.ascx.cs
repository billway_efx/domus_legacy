﻿using EfxFramework.Interfaces.UserControls.Property;
using System;

namespace Domus.UserControls
{
    public partial class YardiAddEdit : System.Web.UI.UserControl, IYardiAddEdit
    {
        public EfxFramework.Presenters.UserControls.Property.YardiAddEdit Presenter { get; set; }

        public int PropertyId { get; set; }
        public string UsernameText { get { return YardiUsernameText.Text; } set { YardiUsernameText.Text = value; } }
        public string PasswordText { get { return YardiPasswordText.Text; } set { YardiPasswordText.Text = value; } }
        public string ServerNameText { get { return YardiServerNameText.Text; } set { YardiServerNameText.Text = value; } }
        public string DatabaseNameText { get { return YardiDatabaseNameText.Text; } set { YardiDatabaseNameText.Text = value; } }
        public string PlatformText { get { return YardiPlatformText.Text; } set { YardiPlatformText.Text = value; } }
        public string EndpointText { get { return YardiEndpointText.Text; } set { YardiEndpointText.Text = value; } }
        public string PropertyIdText { get { return YardiPropertyIdText.Text; } set { YardiPropertyIdText.Text = value; } }

        //Salcedo - Issue # 0000179
        public bool YardiDepositDateCheckbox { get { return YardiDepositDate.Checked; } set { YardiDepositDate.Checked = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            Presenter = new EfxFramework.Presenters.UserControls.Property.YardiAddEdit(this);

            if (!Page.IsPostBack)
                Presenter.InitializeValues();
        }
    }
}