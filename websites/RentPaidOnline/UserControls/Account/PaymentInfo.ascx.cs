﻿using EfxFramework.Interfaces;
using EfxFramework.Interfaces.UserControls.Account;
using EfxFramework.Interfaces.UserControls.Payment;
using EfxFramework.Presenters.UserControls.Account;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domus.UserControls.Account
{
    public partial class PaymentInfo : UserControl, IEfxAdministratorPaymentInfo
    {
        private PaymentInfoBase<EfxFramework.EfxAdministrator> _presenter;

        public IBasePage ParentPage { get { return Parent.Page as IBasePage; } }
        public IPaymentAmount AmountControl { get { return PaymentAmountControl; } }
        public ICharityDonation CharityControl { get { return CharityDonationControl; } }
        public MultiView PaymentMultiView { get { return PaymentMethodMultiview; } }
        public View CreditCardPaymentView { get { return CreditCardView; } }
        public View EcheckPaymentView { get { return EcheckView; } }
        public View SmsPaymentView { get { return SmsView; } }
        public UpdatePanel PaymentInfoUpdatePanel { get { return PaymentInformationUpdatePanel; } }
        public DropDownList CarrierDropdownList { get { return CarrierDropdown; } }
        public EventHandler PaymentInfoPreRender { set { PaymentInformationUpdatePanel.PreRender += value; } }
        public EventHandler PaymentMethodSelectionChanged { set { PaymentMethodDropdown.SelectedIndexChanged += value; } }
        public EventHandler SaveButtonClicked { set { SavePaymentMethodsButton.Click += value; } }
        public EventHandler EcheckAutoPaymentChanged { set { EcheckAutoPaymentCheckbox.CheckedChanged += value; } }
        public EventHandler CreditCardAutoPaymentChanged { set { CreditCardAutoPaymentCheckbox.CheckedChanged += value; } }
        public EventHandler SmsScheduleChanged { set { SmsReminderCheckbox.CheckedChanged += value; } }
        public string PaymentMethodSelectedValue { get { return PaymentMethodDropdown.SelectedValue; } set { PaymentMethodDropdown.SelectedValue = value; } }
        public string PaymentMethodSelectedText { get { return PaymentMethodDropdown.SelectedItem.Text; } }
        public string DayOfEachMonthText { get { return DayOfEachMonthTextbox.Text; } set { DayOfEachMonthTextbox.Text = value; } }
        public string BankAccountNumberText { get { return BankAccountNumberTextbox.Text; } set { BankAccountNumberTextbox.Text = value; } }
        public string RoutingNumberText { get { return RoutingNumberTextbox.Text; } set { RoutingNumberTextbox.Text = value; } }
        public string NameOnCardText { get { return NameOnCardTextbox.Text; } set { NameOnCardTextbox.Text = value; } }
        public string CreditCardNumberText { get { return CreditCardNumberTextbox.Text; } set { CreditCardNumberTextbox.Text = value; } }
        public string ExpirationDateText { get { return ExpirationDateTextbox.Text; } set { ExpirationDateTextbox.Text = value; } }
        public string SmsCellNumberText { get { return TextPaymentCellNumberTextbox.Text; } set { TextPaymentCellNumberTextbox.Text = value; } }
        public string SmsPaymentMethodSelectedValue { get { return SmsPaymentMethodDropdown.SelectedValue; } set { SmsPaymentMethodDropdown.SelectedValue = value; } }
        public string SmsPaymentMethodSelectedText { get { return SmsPaymentMethodDropdown.SelectedItem.Text; } }
        public string SmsReminderDay { get { return SmsDayOfMonth.Text; } set { SmsDayOfMonth.Text = value; } }
        public bool IsPrimaryCheckingAccount { get { return MakeECheckPrimaryPayMethodCheckbox.Checked; } set { MakeECheckPrimaryPayMethodCheckbox.Checked = value; } }
        public bool IsPrimaryCreditCard { get { return MakeCreditPrimaryPayMethodCheckbox.Checked; } set { MakeCreditPrimaryPayMethodCheckbox.Checked = value; } }
        public bool AutoDebitPanelVisible { set { AutoDebitPanel.Visible = value; } }
        public bool EcheckAutoPaymentChecked { get { return EcheckAutoPaymentCheckbox.Checked; } set { EcheckAutoPaymentCheckbox.Checked = value; } }
        public bool CreditCardAutoPaymentChecked { get { return CreditCardAutoPaymentCheckbox.Checked; } set { CreditCardAutoPaymentCheckbox.Checked = value; } }
        public bool CreditCardAutoPaymentCheckboxVisible { get { return CreditCardAutoPaymentCheckbox.Visible; } set { CreditCardAutoPaymentCheckbox.Visible = value; } }
        public bool AutoPaymentAmountVisible { set { AutoPaymentAmountPanel.Visible = value; } }
        public bool SmsReminderPanelVisible { set { SmsReminderPanel.Visible = value; } }
        public bool SmsReminderChecked { get { return SmsReminderCheckbox.Checked; } set { SmsReminderCheckbox.Checked = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter = PaymentInfoFactory.GetPaymentInfo(this);

            if (!Page.IsPostBack)
                _presenter.IntializeValues();
        }
    }
}