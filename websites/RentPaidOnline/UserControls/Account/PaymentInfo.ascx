﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentInfo.ascx.cs" Inherits="Domus.UserControls.Account.PaymentInfo" %>

<asp:UpdatePanel ID="PaymentInformationUpdatePanel" runat="server">
    <ContentTemplate>
        <h3>Payment Methods</h3>
        <div class="payment-method-labels">
            <label>Payment Method:</label>
        </div>
        <div class="payment-method-values">
            <asp:DropDownList ID="PaymentMethodDropdown" runat="server" AutoPostBack="true">
                <asp:ListItem Text="eCheck" Value="echeck" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Credit" Value="credit"></asp:ListItem>
                <asp:ListItem Text="Text" Value="text"></asp:ListItem>
            </asp:DropDownList>
            <asp:Panel ID="AutoDebitPanel" runat="server">
                <div class="checkbox-wrap" style="width: 400px; padding-top: 5px;">
                    <asp:Label ID="AutoDebitLabel" runat="server" CssClass="paymentInfoLabel" Text="Auto Debit on the " />
                    <asp:TextBox ClientIDMode="Static" CssClass="textbox number" runat="server" ID="DayOfEachMonthTextbox" Width="30px" MaxLength="2" />
                    <asp:Label ID="DayOfTheMonthLabel" runat="server" CssClass="paymentInfoLabel" Text=" day of each month" />
                </div>
            </asp:Panel>
        </div>
        <asp:MultiView ID="PaymentMethodMultiview" runat="server">
            <asp:View ID="EcheckView" runat="server">
                <label class="clear padTop">Bank Account Number</label>
                  <!--cakel: BUGID00072 - Updated max length from 15 to 17 -->
                <asp:TextBox ClientIDMode="Static" CssClass="textbox w100 bank" runat="server" ID="BankAccountNumberTextbox" style="width: 200px;display: block;margin-top: 5px;" MaxLength="17" />
                <label>Routing Number</label>
                <asp:TextBox ClientIDMode="Static" CssClass="textbox w100 routing" runat="server" ID="RoutingNumberTextbox" style="width: 200px;display: block;margin-top: 5px;" MaxLength="9" />
                <div>
                    <asp:CheckBox ID="MakeECheckPrimaryPayMethodCheckbox" runat="server" Text="Make Primary eCheck Account" />
                </div>
                <div class="padding-top padding-bottom">
                    <asp:CheckBox ID="EcheckAutoPaymentCheckbox" runat="server" Text="Setup Auto Debit and use this account" AutoPostBack="true" />
                </div>
            </asp:View>
            <asp:View ID="CreditCardView" runat="server">
                <label class="padTop">Name On Card</label>
                <asp:TextBox ClientIDMode="Static" CssClass="textbox w100" runat="server" ID="NameOnCardTextbox" style="width: 200px;display: block;margin-top: 5px;" />
                <label>Credit Card Number</label>
                <asp:TextBox ClientIDMode="Static" CssClass="textbox w100 credit" runat="server" ID="CreditCardNumberTextbox" style="width: 200px;display: block;margin-top: 5px;" MaxLength="16" />
                <label style="width: 260px">Exp. Date</label>
                <asp:TextBox ClientIDMode="Static" CssClass="textbox datepicker" Width="250px" runat="server" ID="ExpirationDateTextbox" style="width: 200px;display: block;margin-top: 5px;" />
                <div>
                    <asp:CheckBox ID="MakeCreditPrimaryPayMethodCheckbox" runat="server" Text="Make Primary Credit Card Account" />
                </div>
                <div class="padding-top padding-bottom">
                    <asp:CheckBox ID="CreditCardAutoPaymentCheckbox" runat="server" Text="Setup Auto Debit and use this account" AutoPostBack="true" />
                </div>
            </asp:View>
            <asp:View ID="SmsView" runat="server">
                <asp:CheckBox ID="SmsReminderCheckbox" runat="server" Text="Send me a text message reminder when my rent is due." AutoPostBack="true" Width="100%" />
                <asp:Panel ID="SmsReminderPanel" runat="server">
                    <div class="checkbox-wrap" style="width: 400px; padding-top: 5px;">
                        <asp:Label ID="SmsScheduleLabelLeft" runat="server" CssClass="paymentInfoLabel" Text="Send me a text reminder on the " />
                        <asp:TextBox ClientIDMode="Static" CssClass="textbox number" runat="server" ID="SmsDayOfMonth" Width="30px" MaxLength="2" />
                        <asp:Label ID="SmsScheduleLabelRight" runat="server" CssClass="paymentInfoLabel" Text=" day of each month" />
                    </div>
                </asp:Panel>
                <label class="padTop">Cell Number</label>
                <asp:TextBox ClientIDMode="Static" CssClass="textbox w100 phone" runat="server" ID="TextPaymentCellNumberTextbox" style="width: 200px;display: block;margin-top: 5px;" />
                <div>
                    <label style="margin: 0 5px 0 0">Carrier</label>
                    <asp:DropDownList ID="CarrierDropdown" runat="server" AppendDataBoundItems="True" >
                        <asp:ListItem Value="-1">-- Select Carrier --</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <label style="width: 100%; padding: 5px 0 5px 0;">What account should the pay by text pull from?</label>
                <asp:DropDownList ID="SmsPaymentMethodDropdown" runat="server">
                    <asp:ListItem Text="-- Select Account Type --" Value="-1" />
                    <asp:ListItem Text="Primary Credit Card" Value="1" />
                    <asp:ListItem Text="Primary Bank Account" Value="2" />
                </asp:DropDownList>
            </asp:View>
        </asp:MultiView>
        <asp:Panel ID="AutoPaymentAmountPanel" runat="server">
            <div style="width: 100%">
                <uc1:PaymentAmount ID="PaymentAmountControl" runat="server" />
            </div>
            <div style="width: 100%">
                <uc1:CharityDonation ID="CharityDonationControl" runat="server" />
            </div>
        </asp:Panel>
        <div class="padding-top padding-bottom">
            <div class="float-wrap">
                <asp:HyperLink ID="PrivacyLink" runat="server" NavigateUrl="~/Privacy/Default.aspx" CssClass="float-right underline" Text="TERMS" />
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="clear padding float-wrap">
    <asp:Button ID="SavePaymentMethodsButton" runat="server" CssClass="green-button float-right" Text="Save" />
</div>