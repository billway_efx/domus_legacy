﻿using EfxFramework;
using EfxFramework.Interfaces;
using EfxFramework.Interfaces.UserControls.Account;
using EfxFramework.Presenters.UserControls.Account;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domus.UserControls.Account
{
    public partial class LeaseInfo : UserControl, IEfxAdministratorLeaseInfo
    {
        private LeaseInfoBase<EfxAdministrator> _presenter;

        public IBasePage ParentPage { get { return Parent.Page as IBasePage; } }
        public EventHandler PropertySelected { set { PropertyDropdown.SelectedIndexChanged += value; } }
        public EventHandler LeasingAgentSelected { set { LeasingAgentDropdown.SelectedIndexChanged += value; } }
        public EventHandler SaveButtonClicked { set { SaveLeaseInfoButton.Click += value; } }
        public DropDownList PropertyList { get { return PropertyDropdown; } }
        public DropDownList LeasingAgentList { get { return LeasingAgentDropdown; } }
        public string UnitNumberText { get { return UnitNumberTextbox.Text; } set { UnitNumberTextbox.Text = value; } }
        public string RentAmountText { get { return RentAmountTextbox.Text; } set { RentAmountTextbox.Text = value; } }
        public string DueDayOfMonthText { get { return DueDayOfTheMonthTextbox.Text; } set { DueDayOfTheMonthTextbox.Text = value; } }
        public string LeaseStartText { get { return LeaseStartTextbox.Text; } set { LeaseStartTextbox.Text = value; } }
        public string LeaseEndText { get { return LeaseEndTextbox.Text; } set { LeaseEndTextbox.Text = value; } }
        public string NumberOfPeopleOnLeaseText { get { return NumberOfPeopleOnLeaseTextbox.Text; } set { NumberOfPeopleOnLeaseTextbox.Text = value; } }
        public string SelectedPropertyName { set { SelectedPropertyNameText.Text = value; } }
        public string SelectedPropertyPhotoUrl { set { SelectedPropertyPhoto.ImageUrl = value; } }
        public string SelectedPropertyAddress { set { SelectedPropertyAddressText.Text = value; } }
        public string SelectedPropertyPhone { set { SelectedPropertyMainContactPhone.Text = value; } }
        public string SelectedLeasingAgentName { set { SelectedLeasingAgentFullNameText.Text = value; } }
        public string SelectedLeasingAgentPhotoUrl { set { SelectedLeasingAgentPhoto.ImageUrl = value; } }
        public string SelectedLeasingAgentMainPhone { set { SelectedLeasingAgentMainPhoneNumberText.Text = value; } }
        public string SelectedLeasingAgentCellPhone { set { SelectedLeasingAgentMobilePhoneNumberText.Text = value; } }
        public string SelectedLeasingAgentEmail { set { SelectedLeasingAgentEmailText.Text = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter = LeaseInfoFactory.GetLeaseInfo(this);

            if (!Page.IsPostBack)
                _presenter.InitializeValues();
        }
    }
}