﻿using EfxFramework;
using EfxFramework.Interfaces.UserControls.Account;
using EfxFramework.Presenters.UserControls.Account;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.Web;
using System;
using Mb2x.ExtensionMethods;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


//cakel: New User control Added to fix multiple bugs
//BUGID 00033
//BUGID 00085
//BUGID 00085 Part 2

namespace Domus.UserControls.Account
{
    public partial class LeaseInfoV3 : System.Web.UI.UserControl
    {
        //Get Renter ID from Querystring
        public int RenterId
        {
            get
            {
                int renter = 0;
                string Id = HttpContext.Current.Request.QueryString["renterId"].ToString();
                renter = Convert.ToInt32(Id);
                return renter;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (EfxFramework.Helpers.Helper.IsCsrUser)
            {
                RentAmountTextBox.Enabled = false;
                RentDueMonthDropDown.Enabled = false;
                RentDueDayDropDown.Enabled = false;
                RentDueYearDropDown.Enabled = false;
                Button1.Enabled = false;
                AddNewFeeBtn.Enabled = false;
                FeeDropDown.Enabled = false;
                FeeAmountTextBox.Enabled = false;
                AddFeeButton.Enabled = false;
                StartMonthDropDown.Enabled = false;
                StartDayDropDown.Enabled = false;
                StartYearDropDown.Enabled = false;
                EndMonthDropDown.Enabled = false;
                EndDayDropDown.Enabled = false;
                EndYearDropDown.Enabled = false;
                FeeGrid.Enabled = false;
                SaveButton.Enabled = false;
                //LinkButton1.Enabled = false;

            }

            if (!Page.IsPostBack)
            {
                PopulateFeeDetail(RenterId);
                PopulateLeaseAmountDue(RenterId);
                popGrid(RenterId);
                populateRenterDetails(RenterId);
                PopulateFeeList(RenterId.ToString());
                PopulateFeeAmount();
            }
        }




        //Adds Records to grid
        public void popGrid(int _renterID)
        {
            FeeGrid.DataSource = DLAccess.GetmonthlyFeeByID(_renterID);
            FeeGrid.DataBind();
        }

        //Populates Renter Details for Lease info tab
        public void populateRenterDetails(int _RenterID)
        {
            DLAccess.GetRenterDetail _renter = new DLAccess.GetRenterDetail();
            string id = _RenterID.ToString();
            _renter = DLAccess.RenterDetails(id);


            int StartMonth = DLAccess.GetMonthNum(_renter.LeaseStart);
            int StartYear = DLAccess.GetYearNum(_renter.LeaseStart);
            int StartDay = DLAccess.GetDay(_renter.LeaseStart);

            int EndMonth = DLAccess.GetMonthNum(_renter.LeaseEnd);
            int EndYear = DLAccess.GetYearNum(_renter.LeaseEnd);
            int EndDay = DLAccess.GetDay(_renter.LeaseEnd);

            int RentDueMonth = DLAccess.GetMonthNum(_renter.paymentDueDate);
            int RentDueYear = DLAccess.GetYearNum(_renter.paymentDueDate);
            int RentDueDay = DLAccess.GetDay(_renter.paymentDueDate);

            //cakel BUGID00085 part 2 added new function to hide Fee area for integrated properties
            HideFeeArea(_renter.IsIntegrated);

            populateYearList();

            //Adds value for Rent Amount
            double RentAmount = _renter.rentAmount.ToDouble();
            RentAmountTextBox.Text = String.Format("{0:0.00}", RentAmount);
            RentDueMonthDropDown.SelectedValue = RentDueMonth.ToString();
            RentDueYearDropDown.SelectedValue = RentDueYear.ToString();
            StartMonthDropDown.SelectedValue = StartMonth.ToString();
            StartYearDropDown.SelectedValue = StartYear.ToString();
            EndMonthDropDown.SelectedValue = EndMonth.ToString();
            EndYearDropDown.SelectedValue = EndYear.ToString();
            populateStartDays(StartMonth, StartYear);
            StartDayDropDown.SelectedValue = EndDay.ToString();
            populateEndDays(EndMonth, EndYear);
            EndDayDropDown.SelectedValue = EndDay.ToString();
            populateRentDueDays(RentDueMonth, RentDueYear);
            RentDueDayDropDown.SelectedValue = RentDueDay.ToString();

            if (_renter.leaseID == "0")
            {
                SaveRenterLeaseInfo();
            }

        }

        public void populateStartDays(int _monthNum, int _yearNum)
        {
            int MaxDay = DLAccess.GetMaxMonthDay(_monthNum, _yearNum);
            int dayInt = 0;
            for (int i = 0; i < MaxDay; i++)
            {
                dayInt += 1;

                StartDayDropDown.Items.Add(new ListItem(dayInt.ToString(), dayInt.ToString()));
            }
        }

        public void populateEndDays(int _monthNum, int _yearNum)
        {
            int MaxDay = DLAccess.GetMaxMonthDay(_monthNum, _yearNum);
            int dayInt = 0;
            for (int i = 0; i < MaxDay; i++)
            {
                dayInt += 1;

                EndDayDropDown.Items.Add(new ListItem(dayInt.ToString(), dayInt.ToString()));
            }
        }

        public void populateRentDueDays(int _monthNum, int _yearNum)
        {
            int MaxDay = DLAccess.GetMaxMonthDay(_monthNum, _yearNum);
            int dayInt = 0;
            for (int i = 0; i < MaxDay; i++)
            {
                dayInt += 1;

                RentDueDayDropDown.Items.Add(new ListItem(dayInt.ToString(), dayInt.ToString()));
            }
        }


        public void populateYearList()
        {
            DateTime _currYear = DateTime.Now;
            int _year = (_currYear.Year - 10);
            int myint = _year;
            for (int i = _year; i < (_year + 15); i++)
            {
                myint += 1;
                StartYearDropDown.Items.Add(new ListItem(myint.ToString(), myint.ToString()));
                EndYearDropDown.Items.Add(new ListItem(myint.ToString(), myint.ToString()));
                RentDueYearDropDown.Items.Add(new ListItem(myint.ToString(), myint.ToString()));
            }
        }

        protected void RentDueMonthDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            int monthNUm = Convert.ToInt32(RentDueMonthDropDown.SelectedValue);
            int yearNum = RentDueYearDropDown.SelectedValue.ToInt32();
            RentDueDayDropDown.Items.Clear();
            populateRentDueDays(monthNUm, yearNum);

            SaveRenterLeaseInfo();

        }

        protected void StartMonthDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            int monthNum = StartMonthDropDown.SelectedValue.ToInt32();
            int yearNum = StartYearDropDown.SelectedValue.ToInt32();
            StartDayDropDown.Items.Clear();
            populateStartDays(monthNum, yearNum);

            SaveRenterLeaseInfo();

        }

        protected void EndMonthDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            int monthNum = EndMonthDropDown.SelectedValue.ToInt32();
            int yearNum = EndYearDropDown.SelectedValue.ToInt32();
            EndDayDropDown.Items.Clear();
            populateEndDays(monthNum, yearNum);
            SaveRenterLeaseInfo();

        }

        public void PopulateFeeList(string renterID)
        {
            FeeDropDown.Items.Clear();
            FeeDropDown.DataSource = DLAccess.GetFeesList(renterID);
            FeeDropDown.DataTextField = "FeeName";
            FeeDropDown.DataValueField = "MonthlyFeeID";
            FeeDropDown.DataBind();

        }

        public void PopulateFeeAmount()
        {
            //Updated code when there are no fees for given property (DBNULL) - will return 0.00
            String _getFeeID = FeeDropDown.SelectedValue.ToString();
            int _FeeID = 0;
            if (_getFeeID != "")
            {
                _FeeID = Convert.ToInt32(FeeDropDown.SelectedValue);
            }

            SqlDataReader FeeReader = DLAccess.GetFeeAmount(_FeeID);
            FeeReader = DLAccess.GetFeeAmount(_FeeID);
            double FeeAmount;
            FeeReader.Read();
            if (FeeReader.HasRows)
            {
                FeeAmount = Convert.ToDouble(FeeReader["DefaultFeeAmount"]);
            }
            else
            {
                FeeAmount = 0.00;
            }

            FeeAmountTextBox.Text = String.Format("{0:0.00}", FeeAmount);
        }

        protected void FeeDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateFeeAmount();
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            SaveRenterLeaseInfo();
        }


        public void SaveRenterLeaseInfo()
        {
            int leaseOut = 0;

            int RenterID = Convert.ToInt32(HttpContext.Current.Request.QueryString["renterId"]);

            DLAccess.GetRenterDetail _renter = new DLAccess.GetRenterDetail();
            _renter = DLAccess.RenterDetails(RenterID.ToString());
            int leaseID = _renter.leaseID.ToInt32();

            SqlDataReader _PropertyReader = DLAccess.GetPropertyDetailsByRenter(RenterID);
            _PropertyReader.Read();
            int PropertyId = 0;
            string unitNumber = "";
            int PaymentDueDay = 1;
            int CurrentBalanceCalc = 0;
            if (_PropertyReader.HasRows)
            {
                PropertyId = Convert.ToInt32(_PropertyReader["PropertyId"]);
                unitNumber = _PropertyReader["Unit"].ToString();
                PaymentDueDay = Convert.ToInt32(_PropertyReader["PaymentDueDay"]);
                CurrentBalanceCalc = Convert.ToInt32(_PropertyReader["UsesUpdatedCurrentBalanceCalc"]);
            }

            int PropertyStaffID = Convert.ToInt32(Page.User.Identity.Name.ToString());
            decimal rentAmount = Convert.ToDecimal(RentAmountTextBox.Text);
            int NumTenants = 1;

            System.DateTime DummyDateValue;

            string RentDueonstring = (RentDueMonthDropDown.SelectedValue + "/" + RentDueDayDropDown.SelectedValue + "/" + RentDueYearDropDown.SelectedValue);
            string LeaseStartString = (StartMonthDropDown.SelectedValue + "/" + StartDayDropDown.SelectedValue + "/" + StartYearDropDown.SelectedValue);
            string LeaseEndString = (EndMonthDropDown.SelectedValue + "/" + EndDayDropDown.SelectedValue + "/" + EndYearDropDown.SelectedValue);

            DateTime RentDueOn;
            DateTime leaseStart;
            DateTime leaseEnd;

            //Rent Due on Day
            if (DateTime.TryParse(RentDueonstring, out DummyDateValue))
            {
                RentDueOn = Convert.ToDateTime(RentDueonstring);
            }
            else
            {
                RentDueOn = DateTime.Now;
            }

            //Lease Start Date
            if (DateTime.TryParse(LeaseStartString, out DummyDateValue))
            {
                leaseStart = Convert.ToDateTime(LeaseStartString);
            }
            else
            {
                leaseStart = DateTime.Now;
            }

            //Lease End Date
            if (DateTime.TryParse(LeaseEndString, out DummyDateValue))
            {
                leaseEnd = Convert.ToDateTime(LeaseEndString);
            }
            else
            {
                leaseEnd = DateTime.Now;
            }

            leaseOut = DLAccess.SetLeaseDetailsByRenter(RenterID, leaseID, PropertyId, PropertyStaffID, unitNumber, rentAmount, PaymentDueDay, NumTenants, leaseStart, leaseEnd, rentAmount, leaseStart, RentDueOn, CurrentBalanceCalc);


            DLAccess.UpdateLeaseFee();
            DLAccess.UpdateNonIntegratedLeases();

            DLAccess.UpdateRenterCurrentBalance(RentDueOn,leaseID,rentAmount);


        }

        protected void RentDueDayDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            SaveRenterLeaseInfo();
        }

        protected void RentDueYearDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            SaveRenterLeaseInfo();
        }

        protected void StartYearDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            SaveRenterLeaseInfo();
        }

        protected void StartDayDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            SaveRenterLeaseInfo();
        }

        protected void EndDayDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            SaveRenterLeaseInfo();
        }

        protected void EndYearDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            SaveRenterLeaseInfo();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            // 2/18/2019 - The ghost payment problem seems to occur when a new renter registers
            // then an admin sets a rent amount ONLY. This combo never creates a payer record for the renter.
            // So, create a payer record here when the rent amount is saved.

            SaveRenterLeaseInfo();

            // Now create the payer record.

            var Resident = new EfxFramework.Renter(RenterId);
            EfxFramework.Payer.SetPayerFromRenter(Resident, Resident.PayerId);


            popGrid(RenterId);
            PopulateLeaseAmountDue(RenterId);
            PopulateFeeDetail(RenterId);

        }


        //Adds Existing fee to Resident
        protected void AddFeeButton_Click(object sender, EventArgs e)
        {
            DLAccess.GetRenterDetail _renter = new DLAccess.GetRenterDetail();
            _renter = DLAccess.RenterDetails(RenterId.ToString());
            int leaseID = _renter.leaseID.ToInt32();

            int monthlyFeeID = Convert.ToInt32(FeeDropDown.SelectedValue);

            decimal FeeAmount = Convert.ToDecimal(FeeAmountTextBox.Text);


            DLAccess.SetLeaseFeeToRenter(monthlyFeeID, leaseID, RenterId, FeeAmount);
            //cakel: BUGID00085 Part 3
            SaveRenterLeaseInfo();
            popGrid(RenterId);
            PopulateLeaseAmountDue(RenterId);
            PopulateFeeDetail(RenterId);

        }


        //Switch to New Fee Area
        protected void Button3_Click(object sender, EventArgs e)
        {
            FeeArea.Visible = false;
            newfeeArea.Visible = true;
        }

        //Cancels new fee area and goes back to existing fee area
        protected void Button2_Click(object sender, EventArgs e)
        {
            newfeeArea.Visible = false;
            FeeArea.Visible = true;
        }



        //Validates Fee Name to prevent 0 characters
        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {

            if (newfeeArea.Visible == true)
            {
                int NewFeeLength = NewfeeAmounttxt.Text.Length;

                if (NewFeeLength < 1)
                {
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }

        }

        protected void CreateNewFeebtn_Click(object sender, EventArgs e)
        {
            //Need RenterID to get Property assigned
            int RenterID = Convert.ToInt32(HttpContext.Current.Request.QueryString["renterId"]);
            int PropertyID = 0;
            int MonthlyFeeID = 0;
            //Gets Renter Details, needed to get property ID
            DLAccess.GetRenterDetail _renter = new DLAccess.GetRenterDetail();
            _renter = DLAccess.RenterDetails(RenterId.ToString());
            SqlDataReader _PropertyReader = DLAccess.GetPropertyDetailsByRenter(RenterID);
            _PropertyReader.Read();
            if (_PropertyReader.HasRows)
            {
                PropertyID = Convert.ToInt32(_PropertyReader["PropertyId"]);
            }
            int LeaseID = Convert.ToInt32(_renter.leaseID);
            string NewfeeName = NewFeeNametxt.Text;
            decimal NewFeeAmount = Convert.ToDecimal(NewfeeAmounttxt.Text);
            MonthlyFeeID = DLAccess.SetNewMonthlyFee(0, PropertyID, NewfeeName, NewFeeAmount);

            DLAccess.SetLeaseFeeToRenter(MonthlyFeeID, LeaseID, RenterID, NewFeeAmount);
            popGrid(RenterID);
            PopulateFeeList(RenterID.ToString());

            //cakel: BUGID00085 Part 3
            SaveRenterLeaseInfo();
            PopulateFeeDetail(RenterId);
            PopulateLeaseAmountDue(RenterId);
        }

        public void HideFeeArea(int IsIntegrated)
        {
            int PropertyCheck = IsIntegrated;
            if (PropertyCheck == 1)
            {

                FeeAreaToHide.Visible = false;

            }
            else
            {
                FeeAreaToHide.Visible = true;
            }

        }
        //cakel: BUGID00085 Part 3
        public void PopulateLeaseAmountDue(int _RenterID)
        {
            SqlDataReader _Reader = DLAccess.GetTotalDueByRenterID(_RenterID);
            _Reader.Read();

            if (_Reader.HasRows)
            {
                decimal CreditDebit = (decimal)_Reader["Credit_Debit"];
                RentAmountLabel.Text = String.Format("{0:c}", _Reader["RentAmount"]);
                TotalAmountLabel.Text = String.Format("{0:c}", _Reader["TotalRentDue"]);

                if (CreditDebit != 0)
                {
                    CreditDebitAmount.Visible = true;
                    CreditDebitTitle.Visible = true;

                    if (CreditDebit > 0)
                    {
                        CreditDebitTitle.Text = "Credit Amount";
                        CreditDebitAmount.Text = String.Format("{0:c}", CreditDebit);
                    }
                    else
                    {
                        CreditDebitTitle.Text = "Past Due Amount";
                        CreditDebitAmount.Text = String.Format("{0:c}", CreditDebit);
                    }


                }
                else
                {
                    CreditDebitAmount.Visible = false;
                    CreditDebitTitle.Visible = false;
                }


            }

        }
        //cakel: BUGID00085 Part 3
        public void PopulateFeeDetail(int RenterID)
        {
            MonthlyFeesList.DataSource = DLAccess.GetFeeDetailByRenter(RenterId);
            MonthlyFeesList.DataBind();
        }

        //cakel: BUGID00085 Part 3
        protected void FeeGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "select")
            {
                LinkButton FeeLink = (LinkButton)e.CommandSource;
                GridViewRow FeeRow = (GridViewRow)FeeLink.Parent.Parent;
                string dk1 = FeeGrid.DataKeys[FeeRow.RowIndex].Value.ToString();

                DLAccess.DeleteFeeByRenterID(dk1, RenterId);
                SaveRenterLeaseInfo();
                popGrid(RenterId);

                PopulateFeeDetail(RenterId);
                PopulateLeaseAmountDue(RenterId);


            }
        }


    }



}