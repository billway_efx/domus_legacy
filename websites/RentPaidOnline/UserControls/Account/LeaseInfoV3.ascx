﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeaseInfoV3.ascx.cs" Inherits="Domus.UserControls.Account.LeaseInfoV3" %>
<%@ Register Src="~/UserControls/DatePickerStart.ascx" TagPrefix="uc1" TagName="DatePickerStart" %>
<%@ Register Src="~/UserControls/DatePickerEnd.ascx" TagPrefix="uc1" TagName="DatePickerEnd" %>




<div class="leasingInfoContainer">
	
    <asp:UpdatePanel ID="LeasePanel" runat="server">
        <ContentTemplate>

			<asp:ValidationSummary ID="LeaseInfoTabSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="LeaseInfo" />
			<asp:PlaceHolder ID="leaseSuccess" runat="server" Visible="false"><div class="alert alert-success"><b>Success!</b></div></asp:PlaceHolder>
            <div class="formTint">

                <div class="formWrapper">
                    <div class="formWhole">
                       
                        <h2>Amounts Due</h2>
                        <div class="formHalf">
                            <label>Rent Amount</label>
                            <asp:TextBox ID="RentAmountTextBox" runat="server" CssClass="form-control" ValidationGroup="LeaseInfo" />
                                                            <asp:RegularExpressionValidator ID="ValidRentAmount"
                                    ErrorMessage="Please enter a valid amount."
                                    Display="None"
                                    ControlToValidate="RentAmountTextBox"
                                    runat="server"
                                    ValidationExpression="^\$?\d+(\.\d\d)?$"
                                    ValidationGroup="LeaseInfo" />
                            <br />
                            <br />
                            <asp:Button ID="Button1" runat="server" Text="Save Rent Amount" CssClass="btn btn-default" OnClick="Button1_Click" ValidationGroup="LeaseInfo"  />
                        </div>

                        <div class="formHalf">
                         
                            <label>Rent Due On:</label>

                                           <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>


                                                <div class="formThird">
                                                    <asp:DropDownList ID="RentDueMonthDropDown" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="RentDueMonthDropDown_SelectedIndexChanged">
                                                        <asp:ListItem Value="1" Text="Jan"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="Feb"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="Mar"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="Apr"></asp:ListItem>
                                                        <asp:ListItem Value="5" Text="May"></asp:ListItem>
                                                        <asp:ListItem Value="6" Text="Jun"></asp:ListItem>
                                                        <asp:ListItem Value="7" Text="Jul"></asp:ListItem>
                                                        <asp:ListItem Value="8" Text="Aug"></asp:ListItem>
                                                        <asp:ListItem Value="9" Text="Sep"></asp:ListItem>
                                                        <asp:ListItem Value="10" Text="Oct"></asp:ListItem>
                                                        <asp:ListItem Value="11" Text="Nov"></asp:ListItem>
                                                        <asp:ListItem Value="12" Text="Dec"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="formThird">
 
                                                    <asp:DropDownList ID="RentDueDayDropDown" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="RentDueDayDropDown_SelectedIndexChanged">
                                                    </asp:DropDownList>

                                                </div>
                                                <div class="formThird">
                                                    <asp:DropDownList ID="RentDueYearDropDown" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="RentDueYearDropDown_SelectedIndexChanged"></asp:DropDownList>
                                                </div>


                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                        </div>
                    </div>
                </div>
            </div>

            <div class="formWrapper" id="FeesDiv" runat="server">

                <!--Fee Area - Must be hidden if property is integrated  -->
                <!--cakel: BUGID00085 Part 2 - Added div id="FeeAreaToHide" block to hide fee area-->
                <div id="FeeAreaToHide" runat="server">

                
                <div class="formHalf">
                    

                    <h2>Fees</h2>

                    <div id="FeeArea" runat="server">
                        <asp:Button ID="AddNewFeeBtn" runat="server" Text="Add A New Fee"  CssClass="btn btn-default"  OnClick="Button3_Click" CausesValidation="False" />
                    
                    <div class="well">
                        <div class="formWhole">
                           
                            <label>Fee Type</label>
                            <asp:DropDownList ID="FeeDropDown" runat="server" DataTextField="FeeName" DataValueField="MonthlyFeeId" AutoPostBack="True" CssClass="form-control" OnSelectedIndexChanged="FeeDropDown_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="formWhole">
                            <div class="formHalf">
                               
                                <label>Fee Amount</label>
                                <asp:TextBox ID="FeeAmountTextBox" runat="server" CssClass="form-control" ValidationGroup="MonthlyFees" />
                            </div>
                            <div class="formHalf noLabel">
                                <asp:Button ID="AddFeeButton" runat="server" Text="Apply Existing Fee" CssClass="btn btn-default" OnClick="AddFeeButton_Click" ValidationGroup="MonthlyFees" />
                                <!--cakel: Updated regular expression to accept negative amounts -->
                                <asp:RegularExpressionValidator ID="ValidFeeAmount"
                                    ErrorMessage="Please enter a valid amount."
                                    Display="None"
                                    ControlToValidate="FeeAmountTextBox"
                                    runat="server"
                                    ValidationExpression="^-?\$?\d+(\.\d\d)?$"
                                    ValidationGroup="MonthlyFees" />

                                <asp:ValidationSummary ID="ValidationSummary2"
                                    DisplayMode="BulletList"
                                    runat="server"
                                    ValidationGroup="MonthlyFees" />
                            </div>

                        </div>
                    </div>
                        </div>
                    <div id="newfeeArea" runat="server" visible="false">
                        <h2>Add New Fee</h2>
                        <asp:Label ID="Label1" runat="server" Text="New Fee Name"></asp:Label><br />
                        <asp:TextBox ID="NewFeeNametxt" runat="server" CssClass="form-control" ></asp:TextBox><br />
                        <asp:Label ID="Label2" runat="server" Text="New Fee Amount"></asp:Label><br />
                        <asp:TextBox ID="NewfeeAmounttxt" runat="server" CssClass="form-control" ></asp:TextBox><br />
                        <asp:Button ID="CreateNewFeebtn" runat="server" Text="Add" CssClass="btn btn-default" ValidationGroup="NewMonthlyFees" OnClick="CreateNewFeebtn_Click" />&nbsp;
                        <asp:Button ID="CancelNewFeeBtn" runat="server" Text="Cancel"  CssClass="btn btn-default"  OnClick="Button2_Click" CausesValidation="False" />
                    </div>

                    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Please enter a name for the new fee" Display="None" OnServerValidate="CustomValidator1_ServerValidate" ValidationGroup="NewMonthlyFees"></asp:CustomValidator>


                    <asp:RegularExpressionValidator ID="NewFeeValidate"
                        ErrorMessage="Please enter a valid amount."
                        Display="None"
                        ControlToValidate="FeeAmountTextBox"
                        runat="server"
                        ValidationExpression="^-?\$?\d+(\.\d\d)?$"
                        ValidationGroup="NewMonthlyFees" />

                    <asp:ValidationSummary ID="ValidationSummary1"
                        DisplayMode="BulletList"
                        runat="server"
                        ValidationGroup="NewMonthlyFees" />

                </div>
                <div class="formHalf">
                    <asp:GridView ID="FeeGrid" runat="server" AutoGenerateColumns="False" CssClass="table-striped centered-last" GridLines="None" Width="100%" DataKeyNames="MonthlyFeeId, LeaseId, RenterId" OnRowCommand="FeeGrid_RowCommand">
                        <Columns>
                            <asp:BoundField HeaderText="Fee Name" DataField="FeeName" />
                            <asp:BoundField HeaderText="Amount" DataField="FeeAmount" DataFormatString="{0:0.00}" />
							<asp:TemplateField HeaderText="Manage">                               
								<ItemTemplate>                               
                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="secondary-link" CommandName="select">
                                        <% if (!EfxFramework.Helpers.Helper.IsCsrUser)
					                      { %>
                                            <span data-icon="&#x2b" aria-hidden="true"></span>
                                           <%}
                                        %>
                                    </asp:LinkButton>
                                 <%-- <a href="<%# "/Handlers/GeneralHandler.ashx?RequestType=9&amp;MonthlyFeeId=" + Eval("MonthlyFeeId").ToString() + "&amp;LeaseId=" + Eval("LeaseId").ToString() + "&amp;RenterId=" + Eval("RenterId").ToString() + "#LeaseInformationTab" %>" target="_self" class="secondary-link"><span data-icon="&#x2b" aria-hidden="true"></span></a> --%>
                                </ItemTemplate>                       
							</asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>

             </div> <!--End Fee Area to Hide if Integrated -->

            </div>

            <div class="formTint">
                <div class="formWrapper">
                    <div class="formHalf">
                        <h2>Lease Dates</h2>

                                    <div class="formWhole">
                                        <label>Lease Start Date</label>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>


                                                <div class="formThird">
                                                    <asp:DropDownList ID="StartMonthDropDown" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="StartMonthDropDown_SelectedIndexChanged">
                                                        <asp:ListItem Value="1" Text="Jan"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="Feb"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="Mar"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="Apr"></asp:ListItem>
                                                        <asp:ListItem Value="5" Text="May"></asp:ListItem>
                                                        <asp:ListItem Value="6" Text="Jun"></asp:ListItem>
                                                        <asp:ListItem Value="7" Text="Jul"></asp:ListItem>
                                                        <asp:ListItem Value="8" Text="Aug"></asp:ListItem>
                                                        <asp:ListItem Value="9" Text="Sep"></asp:ListItem>
                                                        <asp:ListItem Value="10" Text="Oct"></asp:ListItem>
                                                        <asp:ListItem Value="11" Text="Nov"></asp:ListItem>
                                                        <asp:ListItem Value="12" Text="Dec"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="formThird">
                                                    
                                                    <asp:DropDownList ID="StartDayDropDown" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="StartDayDropDown_SelectedIndexChanged">
                                                    </asp:DropDownList>

                                                </div>
                                                <div class="formThird">
                                                    <asp:DropDownList ID="StartYearDropDown" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="StartYearDropDown_SelectedIndexChanged"></asp:DropDownList>
                                                </div>

                                               
                                            </ContentTemplate>
                                        </asp:UpdatePanel>



                                    </div>
                                    <div class="formWhole">
                                        <label>Lease End Date</label>

  
                                     
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>


                                                <div class="formThird">
                                                    <asp:DropDownList ID="EndMonthDropDown" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="EndMonthDropDown_SelectedIndexChanged">
                                                        <asp:ListItem Value="1" Text="Jan"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="Feb"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="Mar"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="Apr"></asp:ListItem>
                                                        <asp:ListItem Value="5" Text="May"></asp:ListItem>
                                                        <asp:ListItem Value="6" Text="Jun"></asp:ListItem>
                                                        <asp:ListItem Value="7" Text="Jul"></asp:ListItem>
                                                        <asp:ListItem Value="8" Text="Aug"></asp:ListItem>
                                                        <asp:ListItem Value="9" Text="Sep"></asp:ListItem>
                                                        <asp:ListItem Value="10" Text="Oct"></asp:ListItem>
                                                        <asp:ListItem Value="11" Text="Nov"></asp:ListItem>
                                                        <asp:ListItem Value="12" Text="Dec"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="formThird">
                                                 
                                                    <asp:DropDownList ID="EndDayDropDown" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="EndDayDropDown_SelectedIndexChanged">
                                                    </asp:DropDownList>

                                                </div>
                                                <div class="formThird">
                                                    <asp:DropDownList ID="EndYearDropDown" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="EndYearDropDown_SelectedIndexChanged"></asp:DropDownList>
                                                </div>

                                           
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </div>

                    </div>
                </div>
            </div>
            <div style="width: 50%">
                <!--cakel: BUGID00085 Part 3 - Added new  control for Payment Total -->
                <div class="well well-sm">
                    <!--cakel: BUGID00069 - Changed h2 tag to "Total Ledger Balance" from Total Balance Due" -->
                    <!--cakel: BUGID00069 - The Ledger Balance was moved in previous release -->
                    <h2>Total Ledger Balance</h2>
                   
                    <!--Rent Amount -->
                    
                    <div class="itemRow">
                        <span class="itemTitle">Rent:</span>
                        <span class="itemAmount">
                            <asp:Label ID="RentAmountLabel" runat="server" /></span><br />
                        <span class="ItemTitle">
                            <asp:Label ID="CreditDebitTitle" runat="server"></asp:Label>
                        </span>
                        <span>
                            <asp:Label ID="CreditDebitAmount" runat="server"></asp:Label></span>
                    </div>  
                    <!--cakel: BUGID00069 - Removed Convenience fee amount.  this was addressed in previous release -->
                    <asp:ListView ID="MonthlyFeesList" runat="server">
                        <LayoutTemplate>
                            <div id="itemPlaceholder" class="itemRow" runat="server"></div>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <div class="itemRow">
                                <span class="itemTitle">
                                    <asp:Label ID="ItemTitleLabel" runat="server" Text='<%# Bind("FeeName") %>' /></span>
                                <span class="itemAmount">
                                    <asp:Label ID="ItemAmountLabel" runat="server" Text='<%# Bind("FeeAmount","{0:c}") %>' /></span>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                        


                    <!--cakel:  -->

                    <!--Total -->
                    
                    <div class="well-footer">
                        <span class="pull-right">Total:
                            <asp:Label ID="TotalAmountLabel" CssClass="text-success" runat="server" /></span>
                        <div class="clear"></div>
                        <span>*Total Amount Due does not reflect convenience Fees</span>
                    </div>
                    
                </div>



            </div>

			<div class="button-footer">
				<div class="button-action">
					<%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default" OnClick="btnCancel_Click" CausesValidation="false" />--%>
				</div>
				<div class="main-button-action">
					<asp:Button ID="SaveButton" runat="server" Text="Save" CssClass="btn btn-primary btn-large" OnClick="SaveButton_Click" Visible="False" />
				</div>
				<div class="clear"></div>

			</div>

        </ContentTemplate>
    </asp:UpdatePanel>
</div>