﻿using EfxFramework;
using EfxFramework.Interfaces.UserControls.Account;
using EfxFramework.Presenters.UserControls.Account;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domus.UserControls.Account
{
    public partial class LeaseInfoV2 : UserControl, ILeaseInfoV2
    {
        private LeaseInfoPresenterV2 _Presenter;

        public Page ParentPage { get { return Page; } }
		public PlaceHolder SuccessMessagePlaceHolder { get { return leaseSuccess; } }
        public string RentAmountText { get { return RentAmountTextBox.Text; } set { RentAmountTextBox.Text = value; } }
        public string FirstMonthStartText { set { FirstMonthDueLabel.Text = value; } }
        public IDatePicker FirstMonthStartControl { get { return FirstMonthDatePicker; } }
        public DropDownList FeeList { get { return FeeDropDown; } }
        public string FeeAmountText { get { return FeeAmountTextBox.Text; } set { FeeAmountTextBox.Text = value; } }
        public EventHandler AddFeeClicked { set { AddFeeButton.Click += value; } }
        public DropDownList LeasingAgentList { get { return LeasingAgentDropDown; } }
        public string AgentNameText { set { AgentNameLabel.Text = value; } }
        public string AgentPhoneText { set { AgentPhoneLabel.Text = value; } }
        public string AgentEmailText { set { AgentEmailLabel.Text = value; } }
        public string AgentImageUrl { set { AgentImage.ImageUrl = value; } }
        public IDatePicker LeaseStartControl { get { return LeaseStartDatePicker; } }
        public IDatePicker LeaseEndControl { get { return LeaseEndDatePicker; } }
        public string UnitNumberText { get { return UnitNumberTextBox.Text; } set { UnitNumberTextBox.Text = value; } }
        public string NumberOfPeopleOnLeaseText { get { return NumberOfPeopleOnLeaseTextBox.Text; } set { NumberOfPeopleOnLeaseTextBox.Text = value; } }
        public DropDownList PetTypeList { get { return PetTypeDropDown; } }
        public string BreedText { get { return BreedTextBox.Text; } set { BreedTextBox.Text = value; } }
        public string PetNameText { get { return NameTextBox.Text; } set { NameTextBox.Text = value; } }
        public EventHandler AddPetClicked { set { AddPetButton.Click += value; } }
        public EventHandler SaveButtonClicked { set { SaveButton.Click += value; } }

        public List<LeaseFee> FeeGridDataSource
        {
            set
            {
                //03/28/2014 - Cakel: Commented out below, new databind procedure below.
                //FeeGrid.DataSource = value;
                //FeeGrid.DataBind();
            }
        }

        public List<Pet> PetGridDataSource
        {
            set
            {
                PetGrid.DataSource = value;
                PetGrid.DataBind();
            }
        }

        public int RenterId
        {
            get
            {
                var Id = HttpContext.Current.Request.QueryString["renterId"].ToNullInt32();
                if (!Id.HasValue || Id.Value < 1)
                {
                    BasePage.SafePageRedirect("~/Renters/Default.aspx");
                    return 0;
                }

                return Id.Value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new LeaseInfoPresenterV2(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
			FeesDiv.Visible = _Presenter.DisplayFees;

            //03/28/2014 Cakel: Added code at page load
            popGrid(RenterId); 

        }

		protected void btnCancel_Click(object sender, EventArgs e)
		{
			_Presenter.InitializeValues();
		}

        //03/28/2014 Cakel: Created new procedure for databind
        //BugID: 00033
        public void popGrid(int _renterID)
        {
            FeeGrid.DataSource = DLAccess.GetmonthlyFeeByID(_renterID);
            FeeGrid.DataBind();
        }

    }
}