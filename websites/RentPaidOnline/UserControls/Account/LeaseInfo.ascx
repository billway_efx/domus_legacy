﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeaseInfo.ascx.cs" Inherits="Domus.UserControls.Account.LeaseInfo" %>

<asp:UpdatePanel ID="LeaseInfoUpdatePanel" runat="server">
    <ContentTemplate>
        <h3>Lease Information</h3>
        <div class="float-wrap">
            <div class="float-left" style="width: 235px;">
                <label>Unit Number:</label>
                <asp:TextBox ID="UnitNumberTextbox" runat="server" CssClass="textbox" />
                <label>Rent Amount:</label>
                <asp:TextBox ID="RentAmountTextbox" runat="server" CssClass="textbox" />
                <label>Rent Due Day of the Month:</label>
                <asp:TextBox ID="DueDayOfTheMonthTextbox" runat="server" CssClass="textbox" />
                <label>Lease Start:</label>
                <asp:TextBox ID="LeaseStartTextbox" runat="server" CssClass="textbox datepicker" />
                <label>Lease End:</label>
                <asp:TextBox ID="LeaseEndTextbox" runat="server" CssClass="textbox datepicker" />
                <label>Number of People on Lease:</label>
                <asp:TextBox ID="NumberOfPeopleOnLeaseTextbox" runat="server" CssClass="textbox number" />
            </div>
            <div class="float-right" style="width: 308px;">
                <div class="float-right padding-bottom" style="margin-bottom: 10px; display: inline-block; width: 100%; text-align: right;">
                    <%--<label>Select:</label>--%>
                    <asp:DropDownList ID="PropertyDropdown" runat="server" AutoPostBack="true" DataTextField="PropertyName" DataValueField="PropertyId" />
                </div>
                <div class="clear white-gradient-wrap float-wrap" style=" display: inline-block; width: 100%; padding: 10px 20px 20px 20px; margin-bottom: 20px; width: 265px; height: auto;">
                    <h4><asp:Literal ID="SelectedPropertyNameText" runat="server" /></h4>
                    <asp:Image ID="SelectedPropertyPhoto" runat="server" CssClass="orange float-right" AlternateText="Property Photo" />
                    <p class="padding-top">
                        <asp:Literal ID="SelectedPropertyAddressText" runat="server" />
                    </p>
                    <p class="padding-top">
                        Phone: <asp:Literal ID="SelectedPropertyMainContactPhone" runat="server" />
                    </p>
                </div>
                <div class="float-right padding-bottom" style="margin-bottom: 10px; display: inline-block; width: 100%; text-align: right;">
                    <%--<label>Select:</label>--%>
                    <asp:DropDownList ID="LeasingAgentDropdown" runat="server" AutoPostBack="true" DataTextField="DisplayName" DataValueField="PropertyStaffId" />
                </div>
                <div class="clear white-gradient-wrap float-wrap" style=" display: inline-block; width: 100%; padding: 10px 20px 20px 20px; margin-bottom: 20px; width: 265px; height: auto;">
                    <h4><asp:Literal ID="SelectedLeasingAgentFullNameText" runat="server" /></h4>
                    <asp:Image ID="SelectedLeasingAgentPhoto" runat="server" CssClass="orange float-right" AlternateText="Leasing Agent Photo" />
                    <div class="padding-top">
                        <p class="padding-bottom phone">
                            <asp:Literal ID="SelectedLeasingAgentMainPhoneNumberText" runat="server" />
                        </p>
                        <p class="padding-bottom phone">
                            <asp:Literal ID="SelectedLeasingAgentMobilePhoneNumberText" runat="server" />
                        </p>
                        <p class="padding-bottom">
                            <asp:Literal ID="SelectedLeasingAgentEmailText" runat="server" />
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="clear padding float-wrap">
    <asp:Button ID="SaveLeaseInfoButton" runat="server" CssClass="green-button float-right" Text="Save" />
</div>