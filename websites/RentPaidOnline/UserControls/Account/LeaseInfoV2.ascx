﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeaseInfoV2.ascx.cs" Inherits="Domus.UserControls.Account.LeaseInfoV2" %>
<%@ Register Src="~/UserControls/DatePickerStart.ascx" TagPrefix="uc1" TagName="DatePickerStart" %>
<%@ Register Src="~/UserControls/DatePickerEnd.ascx" TagPrefix="uc1" TagName="DatePickerEnd" %>



<div class="leasingInfoContainer">
	
    <asp:UpdatePanel ID="LeasePanel" runat="server">
        <ContentTemplate>
			<asp:ValidationSummary ID="LeaseInfoTabSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="LeaseInfo" />
			<asp:PlaceHolder ID="leaseSuccess" runat="server" Visible="false"><div class="alert alert-success"><b>Success!</b></div></asp:PlaceHolder>
            <div class="formTint">
                <div class="formWrapper">
                    <div class="formWhole">

                        <h2>Amounts Due</h2>
                        <div class="formHalf">
                            <label>Rent Amount</label>
                            <asp:TextBox ID="RentAmountTextBox" runat="server" CssClass="form-control" />
                        </div>

                        <div class="formHalf">
                            <!--   <div class="formLabel"><asp:Label ID="FirstMonthDueLabel" runat="server" Text="First Month's Rent Due On:" /></div> -->
                            <label>First Month's Rent Due On:</label>
                            <uc1:DatePicker ID="FirstMonthDatePicker" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="formWrapper" id="FeesDiv" runat="server">
                <div class="formHalf">
                    <h2>Fees</h2>
                    <div class="well">
                        <div class="formWhole">
                            <!-- <div class="formLabel"><asp:Label ID="FeeLabel" runat="server" Text="Fee" /></div> -->
                            <label>Fee Type</label>
                            <asp:DropDownList ID="FeeDropDown" runat="server" DataTextField="FeeName" DataValueField="MonthlyFeeId" AutoPostBack="True" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <div class="formWhole">
                            <div class="formHalf">
                                <!-- <div class="formLabel"><asp:Label ID="FeeAmountLabel" runat="server" Text="Amount" /></div> -->
                                <label>Fee Amount</label>
                                <asp:TextBox ID="FeeAmountTextBox" runat="server" CssClass="form-control" />
                            </div>
                            <div class="formHalf noLabel">
                                <asp:Button ID="AddFeeButton" runat="server" Text="Add Fee" CssClass="btn btn-default" />

                                <asp:RegularExpressionValidator ID="ValidFeeAmount"
                                    ErrorMessage="Please enter a valid amount."
                                    Display="None"
                                    ControlToValidate="FeeAmountTextBox"
                                    runat="server"
                                    ValidationExpression="^\$?\d+(\.\d\d)?$"
                                    ValidationGroup="MonthlyFees" />

                                <asp:ValidationSummary ID="FeeSummary"
                                    DisplayMode="BulletList"
                                    runat="server"
                                    ValidationGroup="MonthlyFees" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="formHalf">
                    <asp:GridView ID="FeeGrid" runat="server" AutoGenerateColumns="False" CssClass="table-striped centered-last" GridLines="None" Width="100%">
                        <Columns>
                            <asp:BoundField HeaderText="Fee Name" DataField="FeeName" />
                            <asp:BoundField HeaderText="Amount" DataField="FeeAmount" />
							<asp:TemplateField HeaderText="Manage">
								<ItemTemplate>
                                    <%-- 03/28/2014 BugID: 00033  --%>
                                    <a href="<%# "/Handlers/GeneralHandler.ashx?RequestType=9&amp;MonthlyFeeId=" + Eval("MonthlyFeeId").ToString() + "&amp;LeaseId=" + Eval("LeaseId").ToString() + "&amp;RenterId=" + Eval("RenterId").ToString() + "#LeaseInformationTab" %>" 
                                        target="_self" class="secondary-link"><span data-icon="&#x2b" aria-hidden="true"></span></a>
                                </ItemTemplate>
							</asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>

            <!--     <h2>Leasing Agent</h2>
            <div class="formRow">
                <div class="formHalfWidth">
                    <div class="formLabel">
                        <asp:Label ID="LeasingAgentLabel" runat="server" Text="Leasing Agent" />
                    </div>
                    <asp:DropDownList ID="LeasingAgentDropDown" runat="server" AppendDataBoundItems="True" DataTextField="DisplayName" DataValueField="PropertyStaffId" AutoPostBack="True">
                        <asp:ListItem Value="0" Text="-- Select a Leasing Agent --"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="formHalfWidth">
                    <div class="formLabel">
                        <asp:Label ID="AgentNameHeadingLabel" runat="server" Text="Leasing Agent Name: " />
                    </div>
                    <asp:Label ID="AgentNameLabel" runat="server" />
                </div>
            </div>

            <div class="formRow">
                <div class="formHalfWidth">
                    <div class="formLabel">
                        <asp:Label ID="AgentPhoneHeadingLabel" runat="server" Text="Main Phone: " />
                    </div>
                    <asp:Label ID="AgentPhoneLabel" runat="server" />
                </div>
                <div class="formHalfWidth">
                    <div class="formLabel">
                        <asp:Label ID="AgentEmailHeadingLabel" runat="server" Text="Email:" />
                    </div>
                    <asp:Label ID="AgentEmailLabel" runat="server" />
                </div>
            </div>

            <div class="formRow">
                <div class="formFullWidth">
                    <asp:Image ID="AgentImage" runat="server" AlternateText="Leasing Agent" Width="150px" />
                </div>
            </div>
            -->
            <div class="formTint">
                <div class="formWrapper">
                    <div class="formHalf">
                        <h2>Lease Dates</h2>

                                    <div class="formWhole">
                                        <label>Lease Start Date</label>

                                        <!--cakel: BUGID0008 - Added New usercontrol for start date -->
                                        <uc1:DatePickerStart runat="server" id="LeaseStartDatePicker" CssClass="form-controls" />
                                    </div>
                                    <div class="formWhole">
                                        <label>Lease End Date</label>

                                        <!--cakel: BUGID0008 - Added New usercontrol for end date -->
                                        <uc1:DatePickerEnd runat="server" id="LeaseEndDatePicker" CssClass="form-controls" />



                                    </div>

                    </div>
                </div>
            </div>

            <!--
            <h4>Other Lease Information</h4>
            <div class="formRow">
                <div class="formHalfWidth">
                    <div class="formLabel">
                        <asp:Label ID="UnitNumberLabel" runat="server" Text="Unit Number" />
                    </div>
                    <asp:TextBox ID="UnitNumberTextBox" runat="server" />
                </div>
                <div class="formHalfWidth">
                    <div class="formLabel">
                        <asp:Label ID="NumberOfPeopleOnLeaseLabel" runat="server" Text="Number of People on Lease" />
                    </div>
                    <asp:TextBox ID="NumberOfPeopleOnLeaseTextBox" runat="server" />
                </div>
            </div>



            <h4>Pets</h4>
            <div class="formRow">
                <div class="formHalfWidth">
                    <div class="formLabel">
                        <asp:Label ID="PetTypeLabel" runat="server" Text="Pet" />
                    </div>
                    <asp:DropDownList ID="PetTypeDropDown" runat="server" />
                </div>
                <div class="formHalfWidth">
                    <div class="formLabel">
                        <asp:Label ID="BreedLabel" runat="server" Text="Breed" />
                    </div>
                    <asp:TextBox ID="BreedTextBox" runat="server" />
                </div>
            </div>

            <div class="formRow">
                <div class="formHalfWidth">
                    <div class="formLabel">
                        <asp:Label ID="NameLabel" runat="server" Text="Name" />
                    </div>
                    <asp:TextBox ID="NameTextBox" runat="server" />
                </div>
                <div class="formHalfWidth">
                    <asp:Button ID="AddPetButton" runat="server" Text="Add" />
                </div>
            </div>

            <div class="formRow">
                <div class="formFullWidth">
                    <asp:GridView ID="PetGrid" runat="server" AutoGenerateColumns="False" CssClass="grid">
                        <Columns>
                            <asp:BoundField HeaderText="Species" DataField="PetTypeName" />
                            <asp:BoundField HeaderText="Breed" DataField="PetBreed" />
                            <asp:BoundField HeaderText="Name" DataField="PetName" />
                            <asp:HyperLinkField HeaderText="Manage" DataNavigateUrlFields="DeletePetUrl" Text="Delete">
                                <ControlStyle ForeColor="Blue" />
                                <ItemStyle Font-Underline="True" ForeColor="Blue" />
                            </asp:HyperLinkField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
-->
			<div class="button-footer">
				<div class="button-action">
					<%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default" OnClick="btnCancel_Click" CausesValidation="false" />--%>
				</div>
				<div class="main-button-action">
					<asp:Button ID="SaveButton" runat="server" Text="Save" CssClass="btn btn-primary btn-large" />
				</div>
				<div class="clear"></div>
			</div>
            <asp:RequiredFieldValidator ID="RequiredBreedValidation"
                ErrorMessage="A Breed is Required"
                Display="None"
                ControlToValidate="BreedTextBox"
                runat="server"
                ValidationGroup="Pets" />

            <asp:RequiredFieldValidator ID="RequiredNameValidation"
                ErrorMessage="A Pet Name is Required"
                Display="None"
                ControlToValidate="NameTextBox"
                runat="server"
                ValidationGroup="Pets" />

            <asp:ValidationSummary ID="PetsSummary"
                DisplayMode="BulletList"
                runat="server"
                ValidationGroup="Pets" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>


