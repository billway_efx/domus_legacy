﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DetailTile.ascx.cs" Inherits="Domus.UserControls.DetailTile" %>

<div class="property-listitem">
    <div class="property-wrap" style="width: 280px;">
        <h4><asp:Label ID="DetailTileHeaderLabel" runat="server" /></h4>
        <div class="clear padding-top">
            <asp:Literal ID="DetailLiteral" runat="server" />
        </div>
        <div class="padding-bottom padding-top">
            <span style="float: left; padding: 0 0 0 0;"><asp:Literal ID="AdditionalLinksLiteral" runat="server" Visible="false" /></span>
            <div style="float: right; width: 56px;">
                <asp:HyperLink ID="DetailsLinkText" runat="server" Text="View" CssClass="magnifierText" />
                <asp:HyperLink ID="DetailsLink" runat="server" ImageUrl="/Images/magnifier.png" CssClass="magnifier" />
            </div>
        </div>
    </div>
</div>