﻿using EfxFramework;
using EfxFramework.Interfaces.UserControls.Users.Admin;
using EfxFramework.Presenters.UserControls.Users.Admin;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domus.UserControls.Users.Admin
{
    public partial class AddEditEfxAdministrator : UserControl, IAddEditEfxAdministrator
    {
        private AddEditEfxAdministratorPresenter _Presenter;

        public Page ParentPage { get { return Page; } }
        public int EfxAdministratorId { get; set; }
        public string FirstNameText { get { return FirstNameTextBox.Text; } set { FirstNameTextBox.Text = value; } }
        public string LastNameText { get { return LastNameTextBox.Text; } set { LastNameTextBox.Text = value; } }
        public string PrimaryEmailAddressText { get { return PrimaryEmailTextBox.Text; } set { PrimaryEmailTextBox.Text = value; } }
        public EventHandler PrimaryEmailAddressChanged { set { PrimaryEmailTextBox.TextChanged += value; } }
        public string UsernameText { get { return UsernameTextBox.Text; } set { UsernameTextBox.Text = value; } }
        public string PasswordText { get { return PasswordTextBox.Text; } }
        public string ConfirmPasswordText { get { return ConfirmPasswordTextBox.Text; } }
        public DropDownList RolesList { get { return RolesDropDown; } }
        public EventHandler RolesListChanged { set { RolesDropDown.SelectedIndexChanged += value; } }
        public EventHandler SaveButtonClicked { set { SaveButton.Click += value; } }
		public PlaceHolder SuccessMessagePlaceHolder { get { return successMsg; } }

        public List<Permission>PermissionsDataSource
        {
            set
            {
                PermissionsList.DataSource = value;
                PermissionsList.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new AddEditEfxAdministratorPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }

        public void PopulateData()
        {
            _Presenter.PopulateFields();
        }
    }
}