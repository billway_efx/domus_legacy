﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddEditEfxAdministrator.ascx.cs" Inherits="Domus.UserControls.Users.Admin.AddEditEfxAdministrator" %>

<asp:ValidationSummary ID="AddEfxAdministrator" runat="server" DisplayMode="List" CssClass="alert alert-danger" ValidationGroup="AddEfxAdmin" />
<asp:PlaceHolder ID="successMsg" runat="server" Visible="false"><div class="alert alert-success"><b>Success!</b></div></asp:PlaceHolder>
<asp:UpdatePanel ID="UpdatePanel" class="formWrapper" runat="server">
    <ContentTemplate>
        <div class="formHalf">
            <asp:Label AssociatedControlID="FirstNameTextBox" ID="FirstNameLabel" runat="server" Text="First Name"/>
            <asp:TextBox ID="FirstNameTextBox" CssClass="form-control" runat="server"/> 
			<asp:RequiredFieldValidator ID="RequiredFirstNameValidation"
                            runat="server"
                            Display="None"
                            ErrorMessage="First Name is Required"
                            ControlToValidate="FirstNameTextBox"
							ValidationGroup="AddEfxAdmin" />               
        </div>
        <div class="formHalf">
            <asp:Label AssociatedControlID="LastNameTextBox" ID="LastNameLabel" runat="server" Text="Last Name"/>
            <asp:TextBox ID="LastNameTextBox" CssClass="form-control" runat="server"/>
			<asp:RequiredFieldValidator ID="RequiredLastNameValidation"
                            runat="server"
                            Display="None"
                            ErrorMessage="Last Name is Required"
                            ControlToValidate="LastNameTextBox"
							ValidationGroup="AddEfxAdmin" />
        </div>
        <div class="formHalf">
            <asp:Label AssociatedControlID="PrimaryEmailTextBox" ID="PrimaryEmailLabel" runat="server" Text="Primary Email Address"/>
            <asp:TextBox ID="PrimaryEmailTextBox" CssClass="form-control" runat="server" AutoPostBack="True"/>
			<asp:RequiredFieldValidator ID="RequiredEmailAddressValidation"
                            runat="server"
                            Display="None"
                            ErrorMessage="Email Address is Required"
                            ControlToValidate="PrimaryEmailTextBox"
							ValidationGroup="AddEfxAdmin" />
        </div>
		<div class="clear"></div>
        <div class="formHalf">
            <asp:Label AssociatedControlID="UsernameTextBox" ID="UsernameLabel" runat="server" Text="Username"/>
            <asp:TextBox ID="UsernameTextBox" CssClass="form-control" runat="server" ReadOnly="True"/>
			<asp:RegularExpressionValidator ID="ValidEmailAddress"
                                runat="server"
                                Display="None"
                                ErrorMessage="Please enter a valid Email Address"
                                ControlToValidate="PrimaryEmailTextBox" 
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
								ValidationGroup="AddEfxAdmin" />
        </div>
		<div class="clear"></div>
        <div class="formHalf">
            <asp:Label AssociatedControlID="PasswordTextBox" ID="PasswordLabel" runat="server" Text="Password"/>
            <asp:TextBox ID="PasswordTextBox" CssClass="form-control" runat="server" TextMode="Password"/>
			<asp:RequiredFieldValidator ID="RequiredPasswordValidation"
                            runat="server"
                            ControlToValidate="PasswordTextBox"
                            Display="None"
                            ErrorMessage="Password is Required"
							ValidationGroup="AddEfxAdmin" />
			<asp:RegularExpressionValidator ID="PasswordFormatValidator"
                                ControlToValidate="PasswordTextBox"
                                ValidationExpression="^.*(?=.{6,})(?=.*[\d])(?=.*[a-zA-Z]).*$"
                                ErrorMessage="Invalid format. Password must be at least 6 digits and contain at least 1 letter and 1 number."
                                runat="server"
                                Display="None"
								ValidationGroup="AddEfxAdmin" />
        </div>
        <div class="formHalf">
            <asp:Label AssociatedControlID="ConfirmPasswordTextBox" ID="ConfirmPasswordLabel" runat="server" Text="Confirm Password"/>
            <asp:TextBox ID="ConfirmPasswordTextBox" CssClass="form-control" runat="server" TextMode="Password"/>
			<asp:CompareValidator   ID="PasswordComparison"
                        runat="server"
                        ControlToValidate="ConfirmPasswordTextBox"
                        ControlToCompare="PasswordTextBox"
                        Display="None"
                        ErrorMessage="Please make sure the Passwords Match"
						ValidationGroup="AddEfxAdmin" />  
			<asp:RequiredFieldValidator ID="RequiredConfirmPasswordValidation"
                            runat="server"
                            ControlToValidate="ConfirmPasswordTextBox"
                            Display="None"
                            ErrorMessage="Confirm Password is Required"
							ValidationGroup="AddEfxAdmin" />
        </div>
		<div class="clear"></div>
        <div class="well well-sm">
            <div class="formHalf">
                <asp:Label AssociatedControlID="RolesDropDown" ID="RolesLabel" runat="server" Text="Role"/>
                <asp:DropDownList ID="RolesDropDown" CssClass="form-control" runat="server" AutoPostBack="True" DataTextField="RoleName" DataValueField="RoleId"/>
            </div>
            <div class="formHalf">
                <asp:Label AssociatedControlID="PermissionsList" ID="PermissionsLabel" runat="server" Text="Permissions"/>
                <asp:ListView ID="PermissionsList" runat="server">
                    <LayoutTemplate>
                        <ul class="list-unstyled">
                            <li id="itemPlaceholder" runat="server"></li>
                        </ul>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <li><asp:Label ID="PermissionItemLabel" runat="server" Text='<%# Bind("PermissionName") %>' /></li>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <li><asp:Label ID="EmptyDataLabel" runat="server" Text="No Permissions Associated with this Role" /></li>
                    </EmptyDataTemplate>
                </asp:ListView>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="button-footer">
	<div class="main-button-action">
        <!-- CMallory - Layout Change - Changed CSS Class poperties -->
		<asp:Button ID="SaveButton" CssClass="btn footer-button" runat="server" Text="Save" CausesValidation="False" />
	</div>
	<div class="clear"></div>
</div>