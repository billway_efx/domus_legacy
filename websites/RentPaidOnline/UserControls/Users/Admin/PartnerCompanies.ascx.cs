﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System.Web.UI.WebControls;

namespace Domus.UserControls.Users
{
    public partial class PartnerCompanies : UserControl, IPartnerCompanies
    {
        private PartnerCompaniesPresenter _Presenter;
        public List<PartnerCompany> PartnerCompanyList { set { PartnershipGridView.DataSource = value; PartnershipGridView.DataBind(); } }
        public EventHandler AddButtonClick { set { AddButton.Click += value; } }
        public string PartnerNameText { get { return PartnerCompanyTextBox.Text; } }
        public Page ParentPage { get { return Page; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new PartnerCompaniesPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}