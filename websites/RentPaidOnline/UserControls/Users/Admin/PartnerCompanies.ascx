﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PartnerCompanies.ascx.cs" Inherits="Domus.UserControls.Users.PartnerCompanies" %>

<asp:UpdatePanel ID="UpdatePartnershipCompany" runat="server">
    <ContentTemplate>
        <h2>Add a New Partnership Company</h2>
        <div class="form-group">
            <asp:Label ID="PartnerCompanyNameLabel" Text="Name" runat="server" />
            <asp:TextBox ID="PartnerCompanyTextBox" class="form-control" runat="server" />
        </div>
        <div class="form-group">
            <asp:Button ID="AddButton" Text="Add New Company" runat="server" CssClass="btn btn-default" />
        </div>
        <asp:RequiredFieldValidator ID="RequiredPartnerNameValidation"
                                    runat="server"
                                    ControlToValidate="PartnerCompanyTextBox"
                                    Display="None"
                                    ErrorMessage="Please Enter a Partnership Company Name"
                                    ValidationGroup="Partners" />
        <asp:ValidationSummary  ID="PartnerSummary"
                                DisplayMode="BulletList"
                                runat="server"
                                ValidationGroup="Partners" />
                    
        <h2>Current Partnerships</h2>        
        <asp:GridView ID="PartnershipGridView" runat="server" AutoGenerateColumns="false" GridLines="None" Width="100%" CssClass="table-striped centered-last">
            <Columns>
                <asp:BoundField HeaderText="Partnership Companies" DataField="PartnerCompanyName" ItemStyle-Width="90%"/>
                
                
                <asp:TemplateField HeaderText="Manage" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <div class="listing-actions">
                                           
                                            <asp:HyperLink ID="DeleteLink" runat="server" Visible='<%# Bind("CanDelete") %>' NavigateUrl='<%# Bind("DeletePartnerUrl") %>'>&nbsp;<span data-icon="&#x2b" aria-hidden="true"></span></asp:HyperLink>
                                        </div>
                                        <!--/listing-actions-->
                                    </ItemTemplate>
                                </asp:TemplateField>

            </Columns>
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>