﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework;
using EfxFramework.PublicApi;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using Mb2x.ExtensionMethods;

namespace Domus.UserControls.Users
{
    public partial class APIUsers : UserControl, IAPIUsers
    {
        private APIUsersPresenter _Presenter;
        public Page ParentPage { get { return Page; } }
        public int SelectedView { set { ApiUserMultiView.ActiveViewIndex = value; } }
        public List<ApiUser> ApiUsersDataSource { set { ApiUsersGrid.DataSource = value; ApiUsersGrid.DataBind(); } }
        public CheckBox SelectAllProperties { get { return SelectAll; } }
        public GridView ApiUserData { get { return ApiUsersGrid; } }
        public GridViewSelectEventHandler EditRow { set { ApiUsersGrid.SelectedIndexChanging += value; } }
        public GridViewDeleteEventHandler DeleteRow { set { ApiUsersGrid.RowDeleting += value; } }
        public HiddenField EditEnabled { get { return CanEditGrid; } set { CanEditGrid = value; } }
        public HiddenField HiddenId { get { return HiddenIdValue; } set { HiddenIdValue = value; } }
        public EventHandler AddNewApiUser { set { NewApiUserButton.Click += value; } }
        public string UsernameText { get { return UsernameTextBox.Text; } set { UsernameTextBox.Text = value; } }
        public string PasswordText { get { return PasswordTextBox.Text; } set { PasswordTextBox.Text = value; } }
        public string ConfirmPasswordText { get { return ConfirmPasswordTextBox.Text; } set { ConfirmPasswordTextBox.Text = value; } }
        public List<EfxFramework.Property> PropertiesCheckListDataSource { set { PropertiesCheckBoxList.DataSource = value; PropertiesCheckBoxList.DataBind(); } }
        public CheckBoxList PropertiesCheckList { get { return PropertiesCheckBoxList; } }
        public List<PartnerCompany> PartnersCheckListDataSource { set { PartnershipCheckBoxList.DataSource = value; PartnershipCheckBoxList.DataBind(); } }
        public CheckBoxList PartnershipsCheckList { get { return PartnershipCheckBoxList; } }
        public EventHandler SaveButtonClick { set { SaveButton.Click += value; } }
        public EventHandler CancelButtonClick { set { CancelButton.Click += value; } }
        public ServerValidateEventHandler OnlyOnePartner { set { OnePartnerAllowed.ServerValidate += value; } }
		public bool SetApiUserTabAsActive { get; set; }

        public int ApiUserId
        {
            get
            {
                var Id = HttpContext.Current.Request.QueryString["ApiUserId"].ToNullInt32();
                return Id.HasValue && Id.Value > 0 ? Id.Value : 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new APIUsersPresenter(this);

			if (ApiUserId > 0)
			{
				SetApiUserTabAsActive = true;
			}
            
            //CMallory - Task 00495 - If user clicks on the Create New API User link on the UI, they'll be sent to the screen where they can click the Create New API User button. 
            string CreateAPIUser = Request.QueryString["ApiUserId"];
            if (CreateAPIUser.Contains("New"))
            {
                SetApiUserTabAsActive = true;
            }
            
            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}