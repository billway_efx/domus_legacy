﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="APIUsers.ascx.cs" Inherits="Domus.UserControls.Users.APIUsers" %>
<div class="section-body">

    <h2>API Users</h2>
    <asp:UpdatePanel ID="UpdateApiUser" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="ApiUserMultiView" runat="server" ActiveViewIndex="0">
                <asp:View ID="ApiGridView" runat="server">
                    <asp:GridView ID="ApiUsersGrid" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" CssClass="table-striped centered-last">
                        <Columns>
                            <asp:TemplateField HeaderStyle-CssClass="hide" ItemStyle-CssClass="hide">
                                <ItemTemplate>
                                    <asp:HiddenField ID="HiddenApiUserId" runat="server" Value='<%#Bind("ApiUserId")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="API User Username" DataField="Username" ItemStyle-Width="30%" />
                            <asp:TemplateField HeaderText="Associated Properties" ItemStyle-Width="30%">
                                <ItemTemplate>
                                    <asp:DropDownList ID="PropertiesDropDown" runat="server" DataTextField="PropertyName" DataValueField="PropertyId" CssClass="form-control" />
                                    <asp:Label ID="NoProperties" runat="server" Text="No Properties Associated with this User" Visible="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Partnership Company" DataField="PartnerCompanyName" ItemStyle-Width="30%" />
                            <asp:TemplateField HeaderText="Manage" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <div class="listing-actions">
                                        <asp:HyperLink ID="EditLink" runat="server" NavigateUrl='<%# Bind("ViewApiUserUrl") %>'><span data-icon="&#x24" aria-hidden="true"></span>&nbsp;</asp:HyperLink>
                                        <asp:HyperLink ID="DeleteLink" runat="server" NavigateUrl='<%# Bind("DeleteApiUserUrl") %>'>&nbsp;<span data-icon="&#x2b" aria-hidden="true"></span></asp:HyperLink>
                                    </div>
                                    <!--/listing-actions-->
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div class="formWhole">
                        <br />
                        <asp:Button ID="NewApiUserButton" runat="server" Text="Create new API User" CausesValidation="False" CssClass="btn btn-default" />
                    </div>
                </asp:View>

                <!-- Begin Add API User -->


                <asp:View ID="ApiEditView" runat="server">
                    <div class="formWrapper">
                        <div class="formWhole">
                            <div class="formHalf">
                                <asp:Label ID="UsernameLabel" runat="server" Text="Username" />
                                <asp:TextBox ID="UsernameTextBox" runat="server" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="formHalf">
                            <asp:Label ID="PasswordLabel" runat="server" Text="Password" />
                            <asp:TextBox ID="PasswordTextBox" runat="server" TextMode="Password" CssClass="form-control" />
                        </div>

                        <div class="formHalf">
                            <asp:Label ID="ConfirmPasswordLabel" runat="server" Text="Confirm Password" />
                            <asp:TextBox ID="ConfirmPasswordTextBox" runat="server" TextMode="Password" CssClass="form-control" />
                        </div>
                    </div>


                    <asp:Panel ID="PropertiesPanel" Height="200" ScrollBars="Auto" runat="server" CssClass="panelScrolling">
                        <h2>
                            <asp:Label ID="PropertiesLabel" runat="server" Text="Properties" /></h2>
                        <div>
                            <asp:CheckBox ID="SelectAll" runat="server" Text="Select All Properties" AutoPostBack="True" />
                            <asp:CheckBoxList ID="PropertiesCheckBoxList" runat="server" DataTextField="PropertyName" DataValueField="PropertyId" />
                        </div>
                    </asp:Panel>


                    <asp:Panel ID="PartnersPanel" Height="200" ScrollBars="Auto" runat="server" CssClass="panelScrolling">
                        <h2>
                            <asp:Label ID="PartnershipLabel" runat="server" Text="Partnership Companies" />
                        </h2>
                        <asp:CheckBoxList ID="PartnershipCheckBoxList" runat="server" DataTextField="PartnerCompanyName" DataValueField="PartnerCompanyId" />
                    </asp:Panel>

                    <!--CMallory - Task 00494 - Added to fix the UI. -->
                    <div class="tab-pane" id="PartnersTab" runat="server" ClientIDMode="Static">
                        <uc1:PartnerCompanies runat="server" id="PartnerCompanies" />
                    </div> 

                    <!--CMallory Layout Change - Changed CssClass values of Cancel and Save buttons below -->
                    <div class="button-footer">
                        <div class="button-action">
                            <asp:Button ID="CancelButton" runat="server" Text="Cancel" CausesValidation="false" CssClass="btn btn-default footer-button" />
                        </div>
                        <div class="main-button-action">
                            <asp:Button ID="SaveButton" runat="server" Text="Save" CssClass="btn btn-default footer-button" />
                        </div>
                        <div class="clear"></div>
                    </div>

                    <asp:HiddenField ID="CanEditGrid" runat="server" />
                    <asp:HiddenField ID="HiddenIdValue" runat="server" />

                    <asp:RequiredFieldValidator ID="RequiredUserNameValidation"
                        runat="server"
                        ControlToValidate="UsernameTextBox"
                        Display="None"
                        ErrorMessage="Username is Required"
                        ValidationGroup="ApiUserSave" />

                    <asp:RegularExpressionValidator ID="ValidUserName"
                        runat="server"
                        ControlToValidate="UsernameTextBox"
                        Display="None"
                        ErrorMessage="The Username must be a Valid Email Address"
                        ValidationGroup="ApiUserSave"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />

                    <asp:RequiredFieldValidator ID="RequiredPasswordValidation"
                        runat="server"
                        ControlToValidate="PasswordTextBox"
                        Display="None"
                        ErrorMessage="Password is Required"
                        ValidationGroup="ApiUserSave" />

                    <asp:CompareValidator ID="PasswordComparison"
                        runat="server"
                        ControlToValidate="ConfirmPasswordTextBox"
                        ControlToCompare="PasswordTextBox"
                        Display="None"
                        ErrorMessage="Please make sure the Passwords Match"
                        ValidationGroup="ApiUserSave" />

                    <asp:RegularExpressionValidator ID="PasswordFormatValidator"
                        ControlToValidate="PasswordTextBox"
                        ValidationExpression="^.*(?=.{6,})(?=.*[\d])(?=.*[a-zA-Z]).*$"
                        ErrorMessage="Invalid format. Password must be at least 6 digits and contain at least 1 letter and 1 number."
                        runat="server"
                        ValidationGroup="ApiUserSave"
                        Display="None" />

                    <asp:CustomValidator ID="OnePartnerAllowed"
                        runat="server"
                        Display="None"
                        ErrorMessage="You can only assign one Partner Company"
                        ValidationGroup="ApiUserSave" />

                    <asp:ValidationSummary ID="ApiUserSummary"
                        runat="server"
                        DisplayMode="BulletList"
						CssClass="alert alert-danger"
                        ValidationGroup="ApiUserSave" />

                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>

</div>
