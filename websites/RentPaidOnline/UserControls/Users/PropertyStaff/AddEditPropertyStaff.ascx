﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddEditPropertyStaff.ascx.cs" Inherits="Domus.UserControls.Users.PropertyStaff.AddEditPropertyStaff" %>

<asp:ValidationSummary ID="AddEditPropertyStaffSummary" runat="server" DisplayMode="List" CssClass="alert alert-danger" ValidationGroup="AddEditPS" />
<asp:PlaceHolder ID="successMsg" runat="server" Visible="false">
    <div class="alert alert-success"><b>Success!</b></div>
</asp:PlaceHolder>

    <asp:UpdatePanel ID="UpdatePanel" class="formWrapper" runat="server">
        <ContentTemplate>

            <div class="formHalf">
                <asp:DropDownList ID="StaffDropDown" runat="server" AppendDataBoundItems="True" AutoPostBack="True" DataTextField="DisplayName" DataValueField="PropertyStaffId" CssClass="form-control">
                    <asp:ListItem Value="0" Text="-- Add New Staff --"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="formWhole">
                <hr />
            </div>
            <div class="clear"></div>
            

            <h2>Staff Name</h2>
            <div class="formThird">
                <asp:Label ID="FirstNameLabel" AssociatedControlID="FirstNameTextBox" runat="server" Text="First Name" />
                <asp:TextBox ID="FirstNameTextBox" CssClass="form-control" runat="server" />
                <asp:RequiredFieldValidator ID="RequiredFirstNameValidation"
                    runat="server"
                    Display="None"
                    ErrorMessage="First Name is Required"
                    ControlToValidate="FirstNameTextBox"
                    ValidationGroup="AddEditPS" />
            </div>
            <div class="formThird">
                <asp:Label ID="MiddleNameLabel" AssociatedControlID="MiddleNameTextBox" runat="server" Text="Middle Name" />
                <asp:TextBox ID="MiddleNameTextBox" CssClass="form-control" runat="server" />
            </div>
            <div class="formThird">
                <asp:Label ID="LastNameLabel" AssociatedControlID="LastNameTextBox" runat="server" Text="Last Name" />
                <asp:TextBox ID="LastNameTextBox" runat="server" CssClass="form-control" />
                <asp:RequiredFieldValidator ID="RequiredLastNameValidation"
                    runat="server"
                    Display="None"
                    ErrorMessage="Last Name is Required"
                    ControlToValidate="LastNameTextBox"
                    ValidationGroup="AddEditPS" />
            </div>
   <div class="clear"></div>
            <h2>Staff Address</h2>
            <div class="formHalf">
                <asp:Label ID="Address1Label" AssociatedControlID="Address1TextBox" runat="server" Text="Street Address" />
                <asp:TextBox ID="Address1TextBox" CssClass="form-control" runat="server" />
            </div>
            <div class="formHalf">
                <asp:Label ID="Address2Label" AssociatedControlID="Address2TextBox" runat="server" Text="Street Address Line 2" />
                <asp:TextBox ID="Address2TextBox" CssClass="form-control" runat="server" />
            </div>
            <div class="formThird">
                <asp:Label ID="CityLabel" AssociatedControlID="CityTextBox" runat="server" Text="City" />
                <asp:TextBox ID="CityTextBox" CssClass="form-control" runat="server" />
                <asp:RequiredFieldValidator ID="RequiredCityValidation"
                    runat="server"
                    Display="None"
                    ErrorMessage="City is Required"
                    ControlToValidate="CityTextBox"
                    ValidationGroup="AddEditPS" />
            </div>
            <div class="formThird">
                <asp:Label ID="StateLabel" AssociatedControlID="StateDropDown" runat="server" Text="State" />
                <asp:DropDownList ID="StateDropDown" CssClass="form-control" runat="server" />
            </div>
            <div class="formThird">
                <div class="formHalf">
                    <asp:Label ID="ZipLabel" AssociatedControlID="ZipTextBox" runat="server" Text="Zip Code" />
                    <asp:TextBox ID="ZipTextBox" CssClass="form-control" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredZipCodeValidation"
                        runat="server"
                        Display="None"
                        ErrorMessage="Zip Code is Required"
                        ControlToValidate="ZipTextBox"
                        ValidationGroup="AddEditPS" />
                </div>
            </div>
            <h2>Staff Contact Info</h2>
            <div class="formWhole">
                <div class="formHalf">
                    <asp:Label ID="PrimaryEmailLabel" AssociatedControlID="PrimaryEmailTextBox" runat="server" Text="Primary Email Address" />
                    <asp:TextBox ID="PrimaryEmailTextBox" CssClass="form-control" runat="server" AutoPostBack="True" />
                    <asp:RequiredFieldValidator ID="RequiredEmailAddressValidation"
                        runat="server"
                        Display="None"
                        ErrorMessage="Email Address is Required"
                        ControlToValidate="PrimaryEmailTextBox"
                        ValidationGroup="AddEditPS" />
                    <asp:RegularExpressionValidator ID="ValidEmailAddress"
                        runat="server"
                        Display="None"
                        ErrorMessage="Please enter a valid Email Address"
                        ControlToValidate="PrimaryEmailTextBox"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ValidationGroup="AddEditPS" />
                </div>

            </div>
            <div class="formThird">
                <asp:Label ID="OfficePhoneLabel" AssociatedControlID="OfficePhoneTextBox" runat="server" Text="Office Phone" />
                <asp:TextBox ID="OfficePhoneTextBox" CssClass="form-control" runat="server" />
            </div>
            <div class="formThird">
                <asp:Label ID="MobilePhoneLabel" AssociatedControlID="MobilePhoneTextBox" runat="server" Text="Mobile Phone" />
                <asp:TextBox ID="MobilePhoneTextBox" CssClass="form-control" runat="server" />
            </div>
            <div class="formThird">
                <asp:Label ID="FaxLabel" AssociatedControlID="FaxTextBox" runat="server" Text="Fax Number" />
                <asp:TextBox ID="FaxTextBox" CssClass="form-control" runat="server" />
            </div>
            <div class="formWhole">
                <label class="checkbox">
                    <asp:CheckBox ID="IsMainContactCheckBox" runat="server" />Is Main Contact for Property</label>
                <label class="checkbox">
                    <asp:CheckBox ID="IsGeneralManagerCheckBox" runat="server" />Is General Manager for Property</label>
            </div>
            <div class="formHalf">
                <asp:Label ID="UsernameLabel" AssociatedControlID="UsernameTextBox" runat="server" Text="Username" />
                <asp:TextBox ID="UsernameTextBox" CssClass="form-control" runat="server" ReadOnly="True" />
            </div>
            <div class="clear"></div>

            <div class="formHalf">
                <asp:Label ID="PasswordLabel" AssociatedControlID="PasswordTextBox" runat="server" Text="Password" />
                <asp:TextBox ID="PasswordTextBox" CssClass="form-control" runat="server" TextMode="Password" />
                <asp:RequiredFieldValidator ID="RequiredPasswordValidation"
                    runat="server"
                    ControlToValidate="PasswordTextBox"
                    Display="None"
                    ErrorMessage="Password is Required"
                    ValidationGroup="AddEditPS" />
                <asp:RegularExpressionValidator ID="PasswordFormatValidator"
                    ControlToValidate="PasswordTextBox"
                    ValidationExpression="^.*(?=.{6,})(?=.*[\d])(?=.*[a-zA-Z]).*$"
                    ErrorMessage="Invalid format. Password must be at least 6 digits and contain at least 1 letter and 1 number."
                    runat="server"
                    Display="None"
                    ValidationGroup="AddEditPS" />
            </div>
            <div class="formHalf">
                <asp:Label ID="ConfirmPasswordLabel" AssociatedControlID="ConfirmPasswordTextBox" runat="server" Text="Confirm Password" />
                <asp:TextBox ID="ConfirmPasswordTextBox" CssClass="form-control" runat="server" TextMode="Password" />
                <asp:CompareValidator ID="PasswordComparison"
                    runat="server"
                    ControlToValidate="ConfirmPasswordTextBox"
                    ControlToCompare="PasswordTextBox"
                    Display="None"
                    ErrorMessage="Please make sure the Passwords Match"
                    ValidationGroup="AddEditPS" />
                <asp:RequiredFieldValidator ID="RequiredConfirmPasswordValidation"
                    runat="server"
                    ControlToValidate="ConfirmPasswordTextBox"
                    Display="None"
                    ErrorMessage="Confirm Password is Required"
                    ValidationGroup="AddEditPS" />
            </div>

            <div class="clear"></div>
            <div class="well">
                <div class="formHalf">
                    <asp:Label ID="RolesLabel" AssociatedControlID="RolesDropDown" runat="server" Text="Role" />
                    <asp:DropDownList ID="RolesDropDown" CssClass="form-control" runat="server" AutoPostBack="True" DataTextField="RoleName" DataValueField="RoleId" />
                </div>
                <div class="formHalf">
                    <asp:Label ID="PermissionsLabel" AssociatedControlID="PermissionsList" runat="server" Text="Permissions" />
                    <asp:ListView ID="PermissionsList" runat="server">
                        <LayoutTemplate>
                            <ul class="list-unstyled">
                                <li id="itemPlaceholder" runat="server"></li>
                            </ul>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <li>
                                <asp:Label ID="PermissionItemLabel" runat="server" Text='<%# Bind("PermissionName") %>' /></li>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <li>
                                <asp:Label ID="EmptyDataLabel" runat="server" Text="No Permissions Associated with this Role" /></li>
                        </EmptyDataTemplate>
                    </asp:ListView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

<div class="button-footer">
    <div class="main-button-action">
        <asp:Button ID="SaveButton" CssClass="btn btn-primary btn-large" runat="server" Text="Save" CausesValidation="False" ValidationGroup="AddStaff" />
    </div>
    <div class="clear"></div>
</div>

<script>
	$(function () {
		/* Mask */
		ApplySingleMask("#<%=ZipTextBox.ClientID%>", "00000");
		ApplySingleMask("#<%=OfficePhoneTextBox.ClientID%>", "(000) 000-0000");
		ApplySingleMask("#<%=MobilePhoneTextBox.ClientID%>", "(000) 000-0000");
		ApplySingleMask("#<%=FaxTextBox.ClientID%>", "(000) 000-0000");
	});
</script>