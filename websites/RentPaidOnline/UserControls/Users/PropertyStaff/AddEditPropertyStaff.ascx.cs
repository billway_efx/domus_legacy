﻿using EfxFramework;
using EfxFramework.Interfaces.UserControls.Users.PropertyStaff;
using EfxFramework.Presenters.UserControls.Users.PropertyStaff;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domus.UserControls.Users.PropertyStaff
{
    public partial class AddEditPropertyStaff : UserControl, IAddEditPropertyStaff
    {
        private AddEditPropertyStaffPresenter _Presenter;

        public Page ParentPage { get { return Page; } }
        public int PropertyId { get; set; }
        public int PropertyStaffId { get; set; }
        public DropDownList StaffList { get { return StaffDropDown; } }
        public EventHandler StaffListChanged { set { StaffDropDown.SelectedIndexChanged += value; } }
        public bool ShowStaffSelection { get { return StaffDropDown.Visible; } set { StaffDropDown.Visible = value; } }
        public string FirstNameText { get { return FirstNameTextBox.Text; } set { FirstNameTextBox.Text = value; } }
        public string MiddleNameText { get { return MiddleNameTextBox.Text; } set { MiddleNameTextBox.Text = value; } }
        public string LastNameText { get { return LastNameTextBox.Text; } set { LastNameTextBox.Text = value; } }
        public string Address1Text { get { return Address1TextBox.Text; } set { Address1TextBox.Text = value; } }
        public string Address2Text { get { return Address2TextBox.Text; } set { Address2TextBox.Text = value; } }
        public string CityText { get { return CityTextBox.Text; } set { CityTextBox.Text = value; } }
        public DropDownList StateList { get { return StateDropDown; } }
        public string ZipCodeText { get { return ZipTextBox.Text; } set { ZipTextBox.Text = value; } }
        public string PrimaryEmailAddressText { get { return PrimaryEmailTextBox.Text; } set { PrimaryEmailTextBox.Text = value; } }
        public EventHandler EmailAddressChanged { set { PrimaryEmailTextBox.TextChanged += value; } }
        public string OfficePhoneText { get { return OfficePhoneTextBox.Text; } set { OfficePhoneTextBox.Text = value; } }
        public string MobilePhoneText { get { return MobilePhoneTextBox.Text; } set { MobilePhoneTextBox.Text = value; } }
        public string FaxNumberText { get { return FaxTextBox.Text; } set { FaxTextBox.Text = value; } }
        public bool IsMainContactChecked { get { return IsMainContactCheckBox.Checked; } set { IsMainContactCheckBox.Checked = value; } }
        public bool IsGeneralManagerChecked { get { return IsGeneralManagerCheckBox.Checked; } set { IsGeneralManagerCheckBox.Checked = value; } }
        public string UsernameText { get { return UsernameTextBox.Text; } set { UsernameTextBox.Text = value; } }
        public string PasswordText { get { return PasswordTextBox.Text; } }
        public string ConfirmPasswordText { get { return ConfirmPasswordTextBox.Text; } }
        public DropDownList RolesList { get { return RolesDropDown; } }
        public EventHandler RolesListChanged { set { RolesDropDown.SelectedIndexChanged += value; } }
        public EventHandler SaveButtonClicked { set { SaveButton.Click += value; } }
        public string ZipCodeClientId { get { return ZipTextBox.ClientID; } }
        public string OfficePhoneClientId {get { return OfficePhoneTextBox.ClientID; } }
        public string MobilePhoneClientId { get { return MobilePhoneTextBox.ClientID; } }
        public string FaxPhoneClientId { get { return FaxTextBox.ClientID; } }
		public PlaceHolder SuccessMessagePlaceHolder { get { return successMsg; } }


        public List<Permission> PermissionsDataSource
        {
            set
            {
                PermissionsList.DataSource = value;
                PermissionsList.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new AddEditPropertyStaffPresenter(this);
			successMsg.Visible = false;
            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }

        public void BuildPropertyStaffList()
        {
            _Presenter.PopulateStaffList();
        }

        public void PopulateData()
        {
            _Presenter.PopulateFields();
        }
    }
}