﻿using EfxFramework.Interfaces.UserControls.Users.Resident;
using EfxFramework.Presenters.UserControls.Users.Resident;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domus.UserControls.Users.Resident
{
    public partial class AddEditResident : UserControl, IAddEditResident
    {
        private AddEditResidentPresenter _Presenter;

        public Page ParentPage { get { return Page; } }
        public int ResidentId { get; set; }
        public int PropertyId { get; set; }
        public string FirstNameText { get { return FirstNameTextBox.Text; } set { FirstNameTextBox.Text = value; } }
        public string MiddleNameText { get { return MiddleNameTextBox.Text; } set { MiddleNameTextBox.Text = value; } }
        public string LastNameText { get { return LastNameTextBox.Text; } set { LastNameTextBox.Text = value; } }
        public string Address1Text { get { return Address1TextBox.Text; } set { Address1TextBox.Text = value; } }
        public string Address2Text { get { return Address2TextBox.Text; } set { Address2TextBox.Text = value; } }
        public string CityText { get { return CityTextBox.Text; } set { CityTextBox.Text = value; } }
        public DropDownList StateList { get { return StateDropDown; } }
        public string ZipCodeText { get { return ZipCodeTextBox.Text; } set { ZipCodeTextBox.Text = value; } }
        public string MainPhoneText { get { return MainPhoneTextBox.Text; } set { MainPhoneTextBox.Text = value; } }
        public string MobilePhoneText { get { return MobilePhoneTextBox.Text; } set { MobilePhoneTextBox.Text = value; } }
        public string FaxNumberText { get { return FaxNumberTextBox.Text; } set { FaxNumberTextBox.Text = value; } }
        public string PrimaryEmailAddressText { get { return PrimaryEmailAddressTextBox.Text; } set { PrimaryEmailAddressTextBox.Text = value; } }
        public EventHandler PrimaryEmailAddressChanged { set { PrimaryEmailAddressTextBox.TextChanged += value; } }
        public bool PmsPanelVisible { set { PmsPanel.Visible = value; } }
        public string PmsIdText { get { return PmsIdTextBox.Text; } set { PmsIdTextBox.Text = value; } }
        public DropDownList PmsTypeList { get { return PmsTypeDropDown; } }
        public string UsernameText { get { return UsernameTextBox.Text; } set { UsernameTextBox.Text = value; } }
        public string PasswordText { get { return PasswordTextBox.Text; } }
        public string ConfirmPasswordText { get { return ConfirmatPasswordTextBox.Text; } }
        public EventHandler SaveButtonClicked { set { SaveButton.Click += value; } }
        public string ZipCodeClientId { get { return ZipCodeTextBox.ClientID; } }
        public string MainPhoneClientId { get { return MainPhoneTextBox.ClientID; } }
        public string MobilePhoneClientId { get { return MobilePhoneTextBox.ClientID; } }
        public string FaxPhoneClientId { get { return FaxNumberTextBox.ClientID; } }
		public PlaceHolder SuccessMessagePlaceHolder { get { return successMsg; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new AddEditResidentPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }

        public void PopulateData()
        {
            _Presenter.SetPmsVisibility();
            _Presenter.PopulateFields();
        }
    }
}