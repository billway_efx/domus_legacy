﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddEditResident.ascx.cs" Inherits="Domus.UserControls.Users.Resident.AddEditResident" %>

<div class="formWrapper">
	<asp:ValidationSummary ID="AddResidentSummary" runat="server" DisplayMode="List" CssClass="alert alert-danger" ValidationGroup="AddResident" />
	<asp:PlaceHolder ID="successMsg" runat="server" Visible="false"><div class="alert alert-success"><b>Success!</b></div></asp:PlaceHolder>

	<div class="formThird">
		<asp:Label ID="FirstNameLabel" AssociatedControlID="FirstNameTextBox" runat="server" Text="First Name" />
		<asp:TextBox ID="FirstNameTextBox" CssClass="form-control" runat="server" />
		<asp:RequiredFieldValidator ID="RequiredFirstNameValidation"
								runat="server"
								Display="None"
								ErrorMessage="First Name is Required"
								ControlToValidate="FirstNameTextBox" ValidationGroup="AddResident" />
	</div>
	<div class="formThird">
		<asp:Label ID="MiddleNameLabel" AssociatedControlID="MiddleNameTextBox" runat="server" Text="Middle Name" />
		<asp:TextBox ID="MiddleNameTextBox" CssClass="form-control" runat="server" />
	</div>
	<div class="formThird">
		<asp:Label ID="LastNameLabel" AssociatedControlID="LastNameTextBox" runat="server" Text="Last Name" />
		<asp:TextBox ID="LastNameTextBox" CssClass="form-control" runat="server" />
		<asp:RequiredFieldValidator ID="RequiredLastNameValidation"
								runat="server"
								Display="None"
								ErrorMessage="Last Name is Required"
								ControlToValidate="LastNameTextBox" ValidationGroup="AddResident" />
	</div>
	<div class="formTwoThirds">
		<asp:Label ID="Address1Label" AssociatedControlID="Address1TextBox" runat="server" Text="Street Address" />
		<asp:TextBox ID="Address1TextBox" CssClass="form-control" runat="server" />
	</div>
	<div class="formThird">
		<asp:Label ID="Address2Label" AssociatedControlID="Address2TextBox" runat="server" Text="Unit" />
		<asp:TextBox ID="Address2TextBox" CssClass="form-control" runat="server" />
	</div>
	<div class="formHalf">
		<asp:Label ID="CityLabel" AssociatedControlID="CityTextBox" runat="server" Text="City" />
		<asp:TextBox ID="CityTextBox" CssClass="form-control" runat="server" />
		<asp:RequiredFieldValidator ID="RequiredCityValidation"
								runat="server"
								Display="None"
								ErrorMessage="City is Required"
								ControlToValidate="CityTextBox" ValidationGroup="AddResident" />
	</div>
	<div class="formHalf">
		<asp:Label ID="StateLabel" AssociatedControlID="StateDropDown" runat="server" Text="State" />
		<asp:DropDownList ID="StateDropDown" CssClass="form-control" runat="server" />
	</div>
	<div class="formHalf">
		<asp:Label ID="ZipCodeLabel" AssociatedControlID="ZipCodeTextBox" runat="server" Text="Zip Code" />
		<asp:TextBox ID="ZipCodeTextBox" CssClass="form-control" runat="server" />
		<asp:RequiredFieldValidator ID="RequiredZipCodeValidation"
								runat="server"
								Display="None"
								ErrorMessage="Zip Code is Required"
								ControlToValidate="ZipCodeTextBox" ValidationGroup="AddResident" />
	</div>
	<div class="clear"></div>

	<div class="formHalf">
		<asp:Label ID="MainPhoneLabel" AssociatedControlID="MainPhoneTextBox" runat="server" Text="Main Phone Number" />
		<asp:TextBox ID="MainPhoneTextBox" CssClass="form-control" runat="server" />
	</div>
	<div class="formHalf">
		<asp:Label ID="MobilePhoneLabel" AssociatedControlID="MobilePhoneTextBox" runat="server" Text="Mobile Phone Number" />
		<asp:TextBox ID="MobilePhoneTextBox" CssClass="form-control" runat="server" />
	</div>
	<div class="formHalf">
		<asp:Label ID="FaxNumberLabel" AssociatedControlID="FaxNumberTextBox" runat="server" Text="Fax Number" />
		<asp:TextBox ID="FaxNumberTextBox" CssClass="form-control" runat="server" />
	</div>
	<div class="formWhole">
		<asp:Label ID="PrimaryEmailAddressLabel" AssociatedControlID="PrimaryEmailAddressTextBox" runat="server" Text="Email Address" />
		<asp:TextBox ID="PrimaryEmailAddressTextBox" CssClass="form-control" runat="server" AutoPostBack="True" />
		<asp:RequiredFieldValidator ID="RequiredEmailAddressValidation"
								runat="server"
								Display="None"
								ErrorMessage="Email Address is Required"
								ControlToValidate="PrimaryEmailAddressTextBox" ValidationGroup="AddResident" />
		<asp:RegularExpressionValidator ID="ValidEmailAddress"
									runat="server"
									Display="None"
									ErrorMessage="Please enter a valid Email Address"
									ControlToValidate="PrimaryEmailAddressTextBox" 
									ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="AddResident" />
	</div>


	<asp:Panel ID="PmsPanel" runat="server" Visible="False">
		<div class="formHalf">
			<asp:Label ID="PmsIdLabel" AssociatedControlID="PmsIdTextBox" runat="server" Text="Property Management System ID" />
			<asp:TextBox ID="PmsIdTextBox" CssClass="form-control" runat="server" />
		</div>
		<div class="formHalf">
			<asp:Label ID="PmsTypeLabel" AssociatedControlID="PmsTypeDropDown" runat="server" Text="Property Management System" />
			<asp:DropDownList ID="PmsTypeDropDown" CssClass="form-control" runat="server">
				<asp:ListItem Value="0" Text="-- Select Property Management System --"></asp:ListItem>
				<asp:ListItem Value="1" Text="Yardi"></asp:ListItem>
			</asp:DropDownList>
		</div>
	</asp:Panel>

	<div class="formHalf">
		<asp:Label ID="UsernameLabel" AssociatedControlID="UsernameTextBox" runat="server" Text="Username" />
		<asp:TextBox ID="UsernameTextBox" CssClass="form-control" runat="server" ReadOnly="True" />
	</div>
	<div class="clear"></div>
	<div class="formHalf">
		<asp:Label ID="PasswordLabel" AssociatedControlID="PasswordTextBox" runat="server" Text="Password" />
		<asp:TextBox ID="PasswordTextBox" CssClass="form-control" runat="server" TextMode="Password" />
		<asp:RequiredFieldValidator ID="RequiredPasswordValidation"
								runat="server"
								ControlToValidate="PasswordTextBox"
								Display="None"
								ErrorMessage="Password is Required" ValidationGroup="AddResident" />
		<asp:RegularExpressionValidator ID="PasswordFormatValidator"
									ControlToValidate="PasswordTextBox"
									ValidationExpression="^.*(?=.{6,})(?=.*[\d])(?=.*[a-zA-Z]).*$"
									ErrorMessage="Invalid format. Password must be at least 6 digits and contain at least 1 letter and 1 number."
									runat="server"
									Display="None" ValidationGroup="AddResident" />
	</div>
	<div class="formHalf">
		<asp:Label ID="ConfirmPasswordLabel" AssociatedControlID="ConfirmatPasswordTextBox" runat="server" Text="Confirm Password" />
		<asp:TextBox ID="ConfirmatPasswordTextBox" CssClass="form-control" runat="server" TextMode="Password" />
		<asp:CompareValidator   ID="PasswordComparison"
							runat="server"
							ControlToValidate="ConfirmatPasswordTextBox"
							ControlToCompare="PasswordTextBox"
							Display="None"
							ErrorMessage="Please make sure the Passwords Match" ValidationGroup="AddResident" /> 
		<asp:RequiredFieldValidator ID="RequiredConfirmPasswordValidation"
								runat="server"
								ControlToValidate="ConfirmatPasswordTextBox"
								Display="None"
								ErrorMessage="Confirm Password is Required" ValidationGroup="AddResident" />
	</div>
</div>
<div class="button-footer">
	<div class="main-button-action">
		<asp:Button ID="SaveButton" CssClass="btn btn-large footer-button" runat="server" Text="Save" CausesValidation="False" />
	</div>
	<div class="clear"></div>
</div>