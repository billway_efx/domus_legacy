﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DatePickerEnd.ascx.cs" Inherits="Domus.UserControls.DatePickerEnd" %>

<div class="formWhole">

    <!--cakel: BUGID00008 - Added Update panel Opens Update Panel-->
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>


    <div class="formThird">
        <asp:DropDownList ID="MonthDropDown" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="MonthDropDown_SelectedIndexChanged">
            <asp:ListItem Value="1" Text="Jan"></asp:ListItem>
            <asp:ListItem Value="2" Text="Feb"></asp:ListItem>
            <asp:ListItem Value="3" Text="Mar"></asp:ListItem>
            <asp:ListItem Value="4" Text="Apr"></asp:ListItem>
            <asp:ListItem Value="5" Text="May"></asp:ListItem>
            <asp:ListItem Value="6" Text="Jun"></asp:ListItem>
            <asp:ListItem Value="7" Text="Jul"></asp:ListItem>
            <asp:ListItem Value="8" Text="Aug"></asp:ListItem>
            <asp:ListItem Value="9" Text="Sep"></asp:ListItem>
            <asp:ListItem Value="10" Text="Oct"></asp:ListItem>
            <asp:ListItem Value="11" Text="Nov"></asp:ListItem>
            <asp:ListItem Value="12" Text="Dec"></asp:ListItem>
        </asp:DropDownList>
    </div>

    <div class="formThird">
        <!--cakel: BugID-008 Removed static list items and replaced with code -->
<asp:DropDownList ID="DayDropDown" runat="server" CssClass="form-control">
        </asp:DropDownList>
 
    </div>
    <div class="formThird">
        <asp:DropDownList ID="YearDropDown" runat="server" CssClass="form-control"></asp:DropDownList>
    </div>

        <!--cakel: BUGID00008 - Added Update panel Closes Update Panel-->
            </ContentTemplate>
</asp:UpdatePanel>

</div>
