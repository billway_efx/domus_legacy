﻿using EfxFramework;
using EfxFramework.Interfaces.UserControls.Property;
using EfxFramework.Interfaces.UserControls.Users.PropertyStaff;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domus.UserControls
{
    public partial class AddStaffToProperty : UserControl
    {
        public int CurrentPropertyId
        {
            get;
            set;
        }

        public IAddEditPropertyStaff AddEditPropertyStaffControl { get { return AddEditPropertyStaff; } }

        EfxFramework.Property _prop;
        public EfxFramework.Property CurrentProperty
        {
            get
            {
                if (_prop == null && CurrentPropertyId > 0)
                {
                    _prop = new EfxFramework.Property(CurrentPropertyId);
                }

                return _prop;
            }
        }

        public bool IsNewProperty
        {
            get;
            set;
        }

        protected override void LoadControlState(object savedState)
        {
            CurrentPropertyId = savedState.ToString().ToInt32();
        }

        protected override object SaveControlState()
        {
            base.SaveControlState();
            return CurrentPropertyId;
        }

        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                Page.RegisterRequiresControlState(this);
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
				successMsg.Visible = false;
                BindStaffRepeater();
	            DoneAddingPropertyButton.Visible = IsNewProperty;
                AddEditPropertyStaff.PropertyId = CurrentPropertyId;
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }

        private void BindStaffRepeater()
        {
            StaffRepeater.DataSource = PropertyStaff.GetPropertyStaffListByPropertyId(CurrentPropertyId);
            StaffRepeater.DataBind();
            StaffRepeater.Visible = StaffRepeater.Items.Count > 0;
            EmptyStaffRecords.Visible = StaffRepeater.Items.Count == 0;
            //SeeMoreButton.Visible = StaffRepeater.Items.Count > 4;
        }

        protected void SearchButtonClick(object sender, EventArgs e)
        {
            try
            {
                // Bind Staff
                var Data = PropertyStaff.GetPropertyStaffListByPropertyId(CurrentPropertyId);
                var Term = SearchTextbox.Text.Trim().ToLower();

                StaffRepeater.DataSource = Data.Where(p => p.DisplayName.IndexOf(Term, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                StaffRepeater.DataBind();
				
                StaffRepeater.Visible = StaffRepeater.Items.Count > 0;
                EmptyStaffRecords.Visible = StaffRepeater.Items.Count == 0;
                //SeeMoreButton.Visible = StaffRepeater.Items.Count > 4;
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }

        protected void ClearButtonClick(object sender, EventArgs e)
        {
            try
            {
                // Bind Staff
                var Data = PropertyStaff.GetPropertyStaffListByPropertyId(CurrentPropertyId);

                StaffRepeater.DataSource = Data;
                StaffRepeater.DataBind();
                StaffRepeater.Visible = StaffRepeater.Items.Count > 0;
                EmptyStaffRecords.Visible = StaffRepeater.Items.Count == 0;
                //SeeMoreButton.Visible = StaffRepeater.Items.Count > 4;

                SearchTextbox.Text = string.Empty;
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }

        protected void DeleteCommand(object sender, CommandEventArgs e)
        {
            try
            {
                var Id = e.CommandArgument.ToString().ToNullInt32();
                if (Id.HasValue)
                    EfxFramework.Property.RemoveStaffFromProperty(Id.Value, CurrentPropertyId);

                // Bind Staff
                BindStaffRepeater();
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }

        protected void RemoveStaffButtonCommand(object sender, CommandEventArgs e)
        {
            try
            {
                var Id = e.CommandArgument.ToString().ToInt32();

                if (Id <= 0) 
                    return;

                EfxFramework.Property.RemoveStaffFromProperty(Id, CurrentPropertyId);
				string test = Request.QueryString.AllKeys[0];
                // Bind Staff
                BindStaffRepeater();
				successMsg.Visible = true;
                //((BasePage)Page).PageMessage.DisplayMessage(SystemMessageType.Success, "Staff member was removed from property.");
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }

        protected void EditStaffButtonCommand(object sender, CommandEventArgs e)
        {
            try
            {
                var Id = e.CommandArgument.ToString().ToInt32();

                if (Id <= 0) 
                    return;

                AddEditPropertyStaff.PropertyStaffId = Id;
                AddEditPropertyStaff.StaffList.SelectedValue = Id.ToString(CultureInfo.InvariantCulture);
                AddEditPropertyStaff.PopulateData();
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }

        protected void DoneAddingPropertyButtonClick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("/MyProperties/Default.aspx");
            }
            catch (ThreadAbortException)
            {

            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }

		protected void StaffRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{

				LinkButton edit = (LinkButton)e.Item.FindControl("EditStaffButton");
				LinkButton remove = (LinkButton)e.Item.FindControl("RemoveStaffButton");
				edit.PostBackUrl = "~/MyProperties/PropertyDetails.aspx?propertyId=" + CurrentPropertyId + "&tab=StaffTab";
				remove.PostBackUrl = "~/MyProperties/PropertyDetails.aspx?propertyId=" + CurrentPropertyId + "&tab=StaffTab";
			}
		}

    }
}