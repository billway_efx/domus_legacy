﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RealPageAddEdit.ascx.cs" Inherits="Domus.UserControls.RealPageAddEdit" %>

<%-- cakel: 00351 RealPage Integration --%>
<div class="formWhole">
    <div class="formThird">
        <label>
            RealPage User Name
        </label>
        <asp:TextBox ID="RealPageUsernameText" runat="server" CssClass="form-control" />
    </div>

    <div class="formThird">
        <label>RealPage Password</label>
        <asp:TextBox ID="RealPagePasswordText" runat="server" CssClass="form-control"></asp:TextBox>
    </div>

        <div class="formThird">
        <label>
            RealPage PMC ID
        </label>
            <asp:TextBox ID="RealPagePmcIdText" runat="server" CssClass="form-control"></asp:TextBox>
        </div>

</div>

<div class="formWhole">
    <div class="formThird">
        <label>Internal User</label>
        <asp:TextBox ID="RealPageInternalUserText" runat="server" CssClass="form-control"></asp:TextBox>
    </div>

    <div class="formThird">
        <label>
            Site ID
        </label>
        <asp:TextBox ID="RealPageSiteIDText" runat="server" CssClass="form-control"></asp:TextBox>
    </div>

</div>


<div class="formWhole">


    <div class="formThird">
        <label>
            Import URL
        </label>
        <asp:TextBox ID="RealPageImportUrlText" runat="server" CssClass="form-control"></asp:TextBox>
        </div>

       <div class="formThird">
           <label>
               Export URL
           </label>
           <asp:TextBox ID="RealPageExportUrlText" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
 </div>
