﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.Interfaces.UserControls.Property;

namespace Domus.UserControls
{
    public partial class MriAddEditNew : System.Web.UI.UserControl, IMRIAddEdit2
    {
        public EfxFramework.Presenters.UserControls.Property.MRIAddEdit2 Presenter { get; set; }

        public int PropertyId { get; set; }
        public string MRIClientID { get { return MRIClientIDText.Text; } set { MRIClientIDText.Text = value; } }
        public string MRIDatabaseName { get { return MRIDatabseNameText.Text; } set { MRIDatabseNameText.Text = value; } }
        public string MRIWebServiceUserName { get { return MRIWebserviceUserNameText.Text; } set { MRIWebserviceUserNameText.Text = value; } }
        public string MRIPartnerKey { get { return MRIParnterKeyText.Text; } set { MRIParnterKeyText.Text = value; } }
        public string MRIImportEndPoint { get { return MRIImportEndPointText.Text; } set { MRIImportEndPointText.Text = value; } }
        public string MRIExportEndPoint { get { return MRIExportEndPointText.Text; } set { MRIExportEndPointText.Text = value; } }
        public string MRIWebServicePassword { get { return MRIWebservicePasswordText.Text; } set { MRIWebservicePasswordText.Text = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {

            Presenter = new EfxFramework.Presenters.UserControls.Property.MRIAddEdit2(this);

            if (!Page.IsPostBack)
            {
                Presenter.InitializeValues();
            }
                

        }
    }
}