﻿using EfxFramework.Interfaces.UserControls.Property;
using System;

//cakel: BUGID00316 - AMSI Integration
namespace Domus.UserControls
{
    public partial class AmsiAddEdit : System.Web.UI.UserControl, IAmsiAddEdit
    {

        public EfxFramework.Presenters.UserControls.Property.AmsiAddEdit Presenter { get; set; }

        public int PropertyId { get; set; }
        public string UsernameText { get { return AMSIUsernameText.Text; } set { AMSIUsernameText.Text = value; } }
        public string PasswordText { get { return AMSIPasswordText.Text; } set { AMSIPasswordText.Text = value; } }
        public string DatabaseNameText { get { return AmsiDatabaseNameText.Text; } set { AmsiDatabaseNameText.Text = value; } }
        public string PropertyIdText { get { return AmsiPropertyIdText.Text;} set {AmsiPropertyIdText.Text = value;}}
        public string ClientMerchantID { get { return AmsiClientMerchantIDText.Text; } set { AmsiClientMerchantIDText.Text = value; } }
        //cakel:00551
        public string AchClientMerchantID { get { return AmsiAchClientMerchantIDText.Text; } set { AmsiAchClientMerchantIDText.Text = value; } }
        public string CashClientMerchantID { get { return AmsiCashClientMerchantIDText.Text; } set { AmsiCashClientMerchantIDText.Text = value; } }
        public string AmsiImportEndPoint { get { return AmsiImportEndPointText.Text; } set { AmsiImportEndPointText.Text = value; } }
        public string AmsiExportEndPoint { get { return AmsiExportEndPointText.Text; } set { AmsiExportEndPointText.Text = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            Presenter = new EfxFramework.Presenters.UserControls.Property.AmsiAddEdit(this);

            if (!Page.IsPostBack)
                Presenter.InitializeValues();


        }
    }
}