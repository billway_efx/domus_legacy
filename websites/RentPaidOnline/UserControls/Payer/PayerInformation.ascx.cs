﻿using System;
using System.Web.UI.WebControls;
using EfxFramework.Interfaces.UserControls.Payer;

namespace Domus.UserControls.Payer
{
    public partial class PayerInformation : System.Web.UI.UserControl, IPayerInformation
    {
        private EfxFramework.Presenters.UserControls.Payer.PayerInformation _presenter;

        public EventHandler RenterInfoPayerInfoChecked { set { RentersInfoIsPayersInfoCheckbox.CheckedChanged += value; } }
        public string PayersFirstName { get { return PayersFirstNameTextbox.Text; } set { PayersFirstNameTextbox.Text = value; } }
        public string PayersMiddleName { get { return PayersMiddleNameTextbox.Text; } set { PayersMiddleNameTextbox.Text = value; } }
        public string PayersLastName { get { return PayersLastNameTextbox.Text; } set { PayersLastNameTextbox.Text = value; } }
        public string PayersSuffix { get { return PayersSuffixTextbox.Text; } set { PayersSuffixTextbox.Text = value; } }
        public string PayersEmail { get { return PayersEmailTextbox.Text; } set { PayersEmailTextbox.Text = value; } }
        public string PayersAddress { get { return PayersAddressTextbox.Text; } set { PayersAddressTextbox.Text = value; } }
        public string PayersAddress2 { get { return PayersAddressLine2Textbox.Text; } set { PayersAddressLine2Textbox.Text = value; } }
        public string PayersCity { get { return PayersCityTextbox.Text; } set { PayersCityTextbox.Text = value; } }
        public string PayersZipCode { get { return PayersZipTextbox.Text; } set { PayersZipTextbox.Text = value; } }
        public string PayersCellPhone { get { return PayersCellPhoneTextbox.Text; } set { PayersCellPhoneTextbox.Text = value; } }
        public string PayersHomePhone { get { return PayersHomePhoneTextbox.Text; } set { PayersHomePhoneTextbox.Text = value; } }
        public DropDownList StateDropdown { get { return PayerStateDropdown; } }
        public string CurrentRenterId { get { return HiddenRenterId.Value; } set { HiddenRenterId.Value = value; } }
        public bool RenterIsPayerChecked { get { return RentersInfoIsPayersInfoCheckbox.Checked; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter = new EfxFramework.Presenters.UserControls.Payer.PayerInformation(this);

            if (!Page.IsPostBack)
                _presenter.InitializeValues();
        }
    }
}