﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayerInformation.ascx.cs" Inherits="Domus.UserControls.Payer.PayerInformation" %>

<asp:UpdatePanel ID="PayerInformationUpdatePanel" runat="server">
    <ContentTemplate>
        <h2>
            Payer's Information 
            <asp:CheckBox ID="RentersInfoIsPayersInfoCheckbox" runat="server" Text="Same as Resident's Information." AutoPostBack="true" CssClass="float-right default-font padding-bottom block dark upTen" />
        </h2>   
        <div class="profile-information">                             
            <div class="full-name-labels">
                <label class="fn" style="width: 172px;">First Name</label>
                <label class="ln" style="width: 172px;">Middle Name</label>
                <label class="ln" style="width: 172px;">Last Name</label>
                <label class="ln" style="width: 100px;">Suffix</label>
            </div>
            <div class="full-name-textboxes">
                <asp:TextBox ClientIDMode="Static" ID="PayersFirstNameTextbox" runat="server" CssClass="textbox" Width="168px" />
                <asp:TextBox ClientIDMode="Static" ID="PayersMiddleNameTextbox" runat="server" CssClass="textbox" Width="168px" />
                <asp:TextBox ClientIDMode="Static" ID="PayersLastNameTextbox" runat="server" CssClass="textbox" Width="168px" />
                <asp:TextBox ClientIDMode="Static" ID="PayersSuffixTextbox" runat="server" CssClass="textbox" Width="100px" />
            </div>
            <label class="clear">Email</label>
            <asp:TextBox ClientIDMode="Static" id="PayersEmailTextbox" CssClass="textbox w531" runat="server"></asp:TextBox>
            <label class="clear">Address</label>
            <asp:TextBox ClientIDMode="Static" id="PayersAddressTextbox" CssClass="textbox w531" runat="server"></asp:TextBox>
            <label class="clear">Address2</label>
            <asp:TextBox ClientIDMode="Static" id="PayersAddressLine2Textbox" CssClass="textbox w531" runat="server"></asp:TextBox>
            <div class="renter-locale-labels">
                <label class="city" style="width: 238px;">City</label>
                <label class="state">State</label>
                <label class="zip">Zip</label>
            </div>
            <div class="renter-locale-textboxes">
                <asp:TextBox ClientIDMode="Static" ID="PayersCityTextbox" runat="server" CssClass="textbox" Width="233px" />
                <asp:DropDownList ID="PayerStateDropdown" ClientIDMode="Static" runat="server" CssClass="state-dropdown" />
                <asp:TextBox ClientIDMode="Static" ID="PayersZipTextbox" runat="server" CssClass="textbox" Width="120px" />
            </div>
            <div class="renter-phone-labels">
                <label style="width: 238px;">Cell Phone</label>
                <label style="width: 238px;">Home Phone</label>
            </div>
            <div class="renter-phone-textboxes">
                <asp:TextBox ClientIDMode="Static" TextMode="Phone" ID="PayersCellPhoneTextbox" runat="server" CssClass="textbox phone" Width="233px" />
                <asp:TextBox ClientIDMode="Static" TextMode="Phone" ID="PayersHomePhoneTextbox" runat="server" CssClass="textbox phone" Width="233px" />
            </div>
        </div>
        <asp:HiddenField ID="HiddenRenterId" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>