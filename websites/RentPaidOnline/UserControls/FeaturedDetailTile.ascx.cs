﻿using EfxFramework.Interfaces.UserControls;
using System;
using System.Collections.Generic;

namespace Domus.UserControls
{
    public partial class FeaturedDetailTile : System.Web.UI.UserControl, IDetailTile
    {
        private EfxFramework.Presenters.UserControls.DetailTile _presenter;

        public string HeaderText { set { DetailTileHeaderLabel.Text = value; } }
        public string MainDetailHtml { get { return DetailLiteral.Text; } set { DetailLiteral.Text = value; } }
        public List<string> DataSource { get; set; }

        public string DetailsLinkUrl
        {
            set
            {
                DetailsLink.NavigateUrl = value;
                DetailsLinkText.NavigateUrl = value;
            }
        }

        public string AdditionalLinksHtml
        {
            set
            {
                AdditionalLinksLiteral.Text = value;
                AdditionalLinksLiteral.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter = new EfxFramework.Presenters.UserControls.DetailTile(this);

            if (!Page.IsPostBack)
                _presenter.InitializeValues();
        }
    }
}