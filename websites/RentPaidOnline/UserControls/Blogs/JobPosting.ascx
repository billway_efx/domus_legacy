﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobPosting.ascx.cs" Inherits="Domus.UserControls.Blogs.JobPosting" %>
<asp:UpdatePanel ID="JobsUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:MultiView ID="JobPostingMultiView" runat="server" ActiveViewIndex="0">
            <asp:View ID="JobsOverviewView" runat="server">



                <!-- New Grid View -->
                <asp:GridView ID="JobsGrid" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" CssClass="table-striped centered-last">
                    <Columns>

                        <asp:BoundField HeaderText="Date Posted" ItemStyle-Width="20%" DataField="DatePosted" />
                        <asp:TemplateField HeaderText="Job Title" ItemStyle-Width="35%">
                            <ItemTemplate>
                                <asp:HyperLink ID="JobPostingLink" runat="server" NavigateUrl='<%# Eval("AdminJobPostingUrl") %>'><%# Eval("JobTitle") %></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:HyperLinkField ID="JobPostingLink" HeaderText="Job Title" ItemStyle-Width="35%" DataTextField="JobTitle" NavigateUrl='<%# Bind("AdminJobPostingUrl") %>'></asp:HyperLinkField>--%>
                        <asp:BoundField HeaderText="Status" ItemStyle-Width="15%" DataField="JobPostingStatus" />
                        <asp:BoundField HeaderText="Created By" ItemStyle-Width="20%" DataField="PostedBy" />
                        <asp:TemplateField HeaderText="Manage" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:HyperLink ID="EditLink" CssClass="secondary-link" runat="server" NavigateUrl='<%# Bind("AdminJobPostingUrl") %>'><span data-icon="$" aria-hidden="true">&nbsp; &nbsp;</span></asp:HyperLink>
                                <asp:HyperLink ID="DeleteLink" CssClass="secondary-link" runat="server" NavigateUrl='<%# Bind("DeleteJobPostingUrl") %>'><span data-icon="&#x2b" aria-hidden="true"></span></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

                <!-- End New Grid View -->


                <!-- Button -->
                <br />
                <br />
                <div class="float-right">
                    <asp:Button ID="AddNewJobButton" runat="server" Text="Add a New Job Post" CssClass="btn btn-primary" />
                </div>

            </asp:View>


            <!-- Add Job Form -->

            <asp:View ID="JobDetailsView" runat="server">
                <div class="formWrapper">
                    <div class="formHalf">
                        <label>Job Title</label>
                        <asp:TextBox ID="JobTitleTextBox" runat="server" CssClass="form-control" />
                    </div>
                </div>

                <label style="padding: 30px 0px 8px">Job Description</label>
                <CKEditor:CKEditorControl ID="JobDescriptionEditor" BasePath="~/ckeditor" runat="server" ResizeEnabled="False"></CKEditor:CKEditorControl>

                <label style="padding: 30px 0px 8px">Principal Duties & Responsibilities</label>
                <CKEditor:CKEditorControl ID="DutiesEditor" BasePath="~/ckeditor" runat="server" ResizeEnabled="False"></CKEditor:CKEditorControl>

                <label style="padding: 30px 0px 8px">Skills & Abilities Required</label>
                <CKEditor:CKEditorControl ID="SkillsEditor" BasePath="~/ckeditor" runat="server" ResizeEnabled="False"></CKEditor:CKEditorControl>

                <div class="formWrapper">
                    <div class="formThird">
                        <div class="well" style="margin-top: 30px">
                            <asp:CheckBox ID="PublishCheckBox" runat="server" Text=" &nbsp; Publish Job Posting?" TextAlign="Right" />
                        </div>
                    </div>
                </div>


                <div class="button-footer">
                    <div class="button-action">
                        <%--<asp:Button ID="CancelButton" runat="server" Text="Cancel" CssClass="btn btn-default" PostBackUrl="~/MyAccount/Default.aspx#JobPostsTab" />--%>
                    </div>
                    <div class="main-button-action">
                        <!--CMallory - Layout Change - Changed value of CssClass property --> 
                        <asp:Button ID="SaveButton" runat="server" Text="Save" CssClass="btn btn-default footer-button" />
                    </div>
                    <div class="clear"></div>
                </div>

            </asp:View>
        </asp:MultiView>
    </ContentTemplate>
</asp:UpdatePanel>
