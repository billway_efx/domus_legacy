﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProsCorner.ascx.cs" Inherits="Domus.UserControls.Blogs.ProsCorner" %>
<asp:MultiView ID="ProsMultiView" runat="server" ActiveViewIndex="0">
    <asp:View ID="BlogOverviewView" runat="server">

        <!-- Begin Table -->

        <asp:GridView ID="BlogGrid" runat="server" AutoGenerateColumns="False" GridLines="None" CssClass="table-striped centered-last" Width="100%">
            <Columns>
                <asp:BoundField HeaderText="Date Posted" ItemStyle-Width="20%" DataField="AnnouncementDate" />
                <asp:TemplateField HeaderText="Article Title" ItemStyle-Width="35%">
                    <ItemTemplate>
                        <asp:HyperLink ID="ArticleLink" runat="server" NavigateUrl='<%# Eval("EditUrl") %>'><%# Eval("AnnouncementTitle") %></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:HyperLinkField HeaderText="Article Title" ItemStyle-Width="35%" DataTextField="AnnouncementTitle"></asp:HyperLinkField>--%>
                <asp:BoundField HeaderText="Status" ItemStyle-Width="15%" DataField="AnnouncementStatus" />
                <asp:BoundField HeaderText="Created By" ItemStyle-Width="20%" DataField="PostedBy" />
                <asp:TemplateField HeaderText="Manage" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:HyperLink ID="EditLink" CssClass="secondary-link" runat="server" NavigateUrl='<%# Bind("EditUrl") %>'><span data-icon="$" aria-hidden="true">&nbsp; &nbsp;</span></asp:HyperLink>
                        <asp:HyperLink ID="DeleteLink" CssClass="secondary-link" runat="server" NavigateUrl='<%# Bind("DeleteUrl") %>'><span data-icon="&#x2b" aria-hidden="true"></span></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

        <!-- End Table -->
        <br />
        <br />
        <div class="float-right">
            <asp:Button ID="AddNewBlogButton" runat="server" Text="Add a New Blog Post" CssClass="btn btn-primary" />
        </div>

    </asp:View>



    <!-- Add Post Form -->
    <asp:View ID="BlogDetailsView" runat="server">
        <div class="formWrapper">
            <div class="formHalf">
                <label>Article Title</label>
                <asp:TextBox ID="BlogTitleTextBox" runat="server" CssClass="form-control" />
            </div>
        </div>
        <label style="padding: 30px 0px 8px">Content</label>
        <CKEditor:CKEditorControl ID="BlogContentEditor" AutoParagraph="false" BasePath="~/ckeditor" runat="server" ResizeEnabled="False" ClientIDMode="Static" ForcePasteAsPlainText="true"></CKEditor:CKEditorControl>


        <div class="formWrapper">
            <div class="formThird">
                <div class="well" style="margin-top: 30px">
                    <asp:CheckBox ID="PublishCheckBox" runat="server" Text="&nbsp; Publish Article?" TextAlign="Right" />
                </div>
            </div>
        </div>


        <div class="button-footer">
            <div class="button-action">
                <!--CMallory - Layout Change - Changed value of CssClass property for the buttons below. --> 
                <asp:Button ID="CancelButton" runat="server" Text="Cancel" CssClass="btn btn-default footer-button" />
            </div>
            <div class="main-button-action">
                <asp:Button ID="PreviewButton" runat="server" Text="Preview" CssClass="btn btn-default footer-button" />
                <asp:Button ID="SaveButton" runat="server" Text="Save" CssClass="btn btn-default footer-button" />
            </div>
            <div class="clear"></div>
        </div>

    </asp:View>
</asp:MultiView>
<script type="text/javascript">
    function ShowEditorHtmlPreview(data) {
        var mywin = window.open("", "ckeditor_preview", "location=0,status=0,scrollbars=1,width=615");
        $(mywin.document.body).html(data);
        return false;
    }
</script>
