﻿using EfxFramework;
using EfxFramework.Interfaces.UserControls.Blogs;
using EfxFramework.Presenters.UserControls.Blogs;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domus.UserControls.Blogs
{
    public partial class NewsAndEvents : UserControl, IBlog
    {
        private BlogPresenter _Presenter;

        public Page ParentPage { get { return Page; } }
        public AnnouncementType TypeOfAnnouncement { get; set; }
        public MultiView BlogsView { get { return NewsMultiView; } }
        public View BlogsOverview { get { return BlogOverviewView; } }
        public View BlogDetails { get { return BlogDetailsView; } }
        public EventHandler MultiViewChanged { set { NewsMultiView.ActiveViewChanged += value; } }
        public EventHandler AddNewBlogClicked { set { AddNewBlogButton.Click += value; } }
        public string BlogTitleText { get { return BlogTitleTextBox.Text; } set { BlogTitleTextBox.Text = value; } }
        public string BlogContentText { get { return BlogContentEditor.Text; } set { BlogContentEditor.Text = value; } }
        public EventHandler SaveClicked { set { SaveButton.Click += value; } }
        public EventHandler PreviewClicked { set { PreviewButton.Click += value; } }
        public EventHandler CancelClicked { set { CancelButton.Click += value; } }
        public bool PublishChecked { get { return PublishCheckBox.Checked; } set { PublishCheckBox.Checked = value; } }
        public string PreviewLocation { set { PreviewButton.PostBackUrl = value; } }

        public int AnnouncementId
        {
            get
            {
                var Id = HttpContext.Current.Request.QueryString["AnnouncementId"].ToNullInt32();
                return Id.HasValue && Id.Value > 0 ? Id.Value : 0;
            }
        }

        public int AnnouncementTypeId
        {
            get
            {
                var Id = HttpContext.Current.Request.QueryString["AnnouncementTypeId"].ToNullInt32();
                return Id.HasValue && Id.Value > 0 ? Id.Value : 0;
            }
        }

        public List<Announcement> BlogsGridDataSource
        {
            set
            {
                BlogGrid.DataSource = value;
                BlogGrid.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new BlogPresenter(this);

            if (!Page.IsPostBack)
            {
                _Presenter.InitializeValues();
                BlogContentEditor.config.filebrowserImageUploadUrl = "/Handlers/EFXAdminHandler.ashx?requestType=1";
            }
        }
    }
}