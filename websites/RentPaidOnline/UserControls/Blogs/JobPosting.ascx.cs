﻿using System.Web;
using EfxFramework.Interfaces.UserControls.Blogs;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.Presenters.UserControls.Blogs;
using Mb2x.ExtensionMethods;

namespace Domus.UserControls.Blogs
{
    public partial class JobPosting : UserControl, IJobPosting
    {
        private JobPostingPresenter _Presenter;

        public Page ParentPage { get { return Page; } }
        public MultiView JobPostingsView { get { return JobPostingMultiView; } }
        public View JobsOverview { get { return JobsOverviewView; } }
        public View JobDetails { get { return JobDetailsView; } }
        public EventHandler MultiViewChanged { set { JobPostingMultiView.ActiveViewChanged += value; } }
        public EventHandler AddNewJobClicked { set { AddNewJobButton.Click += value; } }
        public string JobTitleText { get { return JobTitleTextBox.Text; } set { JobTitleTextBox.Text = value; } }
        public string JobDescriptionText { get { return JobDescriptionEditor.Text; } set { JobDescriptionEditor.Text = value; } }
        public string DutiesText { get { return DutiesEditor.Text; } set { DutiesEditor.Text = value; } }
        public string SkillsText { get { return SkillsEditor.Text; } set { SkillsEditor.Text = value; } }
        public EventHandler SaveClicked { set { SaveButton.Click += value; } }
        public bool PublishChecked { get { return PublishCheckBox.Checked; } set { PublishCheckBox.Checked = value; } }

        public List<EfxFramework.JobPosting> JobsGridDataSource
        {
            set
            {
                JobsGrid.DataSource = value;
                JobsGrid.DataBind();
            }
        }

        public int JobPostingId
        {
            get
            {
                var Id = HttpContext.Current.Request.QueryString["JobId"].ToNullInt32();
                return Id.HasValue && Id.Value > 0 ? Id.Value : 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new JobPostingPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}