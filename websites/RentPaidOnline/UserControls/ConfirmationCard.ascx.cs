﻿using System;
using System.Collections.Generic;
using EfxFramework.Interfaces.UserControls;

namespace Domus.UserControls
{
    public partial class ConfirmationCard : System.Web.UI.UserControl, IConfirmationCard
    {
        private List<string> _dataSource;
        private EfxFramework.Presenters.UserControls.ConfirmationCard _presenter;

        public EventHandler EditButtonClick { set { EditButton.Click += value; } }
        public string HeaderText { set { HeaderLabel.Text = value; } }
        public string ConfirmationTextHtml { set { ConfirmationInformation.Text = value; } }

        public List<string> DataSource
        {
            get { return _dataSource ?? (_dataSource = new List<string>()); }
            set
            {
                _dataSource = value;
                _presenter.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter = new EfxFramework.Presenters.UserControls.ConfirmationCard(this);

            if (!Page.IsPostBack)
                _presenter.InitializeValues();
        }
    }
}