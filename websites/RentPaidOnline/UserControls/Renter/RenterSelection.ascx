﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RenterSelection.ascx.cs" Inherits="Domus.UserControls.Renter.RenterSelection" %>

<div class="float-left" style="width: 300px">
    <asp:UpdatePanel ID="PropertyRenterUpdatePanel" runat="server">
        <ContentTemplate>
            <h4>Select Resident</h4>
            <asp:DropDownList ID="PropertyDropdownList" AutoPostBack="True" ClientIDMode="Static" runat="server" Width="260px" AppendDataBoundItems="True" DataTextField="PropertyName" DataValueField="PropertyId">
                <asp:ListItem Selected="True" Value="-1">-- Select Property --</asp:ListItem>
            </asp:DropDownList>
            <h4>Select Resident</h4>
            <asp:DropDownList ID="RenterDropdownList" ClientIDMode="Static" runat="server" Width="260px" AppendDataBoundItems="True" DataTextField="DisplayName" DataValueField="RenterId" Enabled="false">
                <asp:ListItem Selected="True" Value="-1">-- Select Resident --</asp:ListItem>
            </asp:DropDownList>
            <div class="padding-top">
                <asp:Button ID="AddRenterButton" runat="server" CssClass="green-button" Text="Add Resident" />
            </div>
            <asp:Panel ID="RenterInfoPanel" runat="server" Visible="false">
                <div id="RenterSingleList">
                    <div class="renter-listitem" style="width: 275px">
                        <div class="renter-wrap">
                            <asp:Image ID="RenterImage" runat="server" AlternateText="Picture" CssClass="orange float-right" />
                            <h4><asp:Label ID="RenterDisplayNameLabel" runat="server" /></h4>
                            <div class="padding-top">
                                <p class="padding-bottom breakword"><asp:Label ID="RenterMainPhoneNumberLabel" runat="server" /></p>
                                <p class="padding-bottom breakword"><asp:Label ID="RenterPrimaryEmailAddress" runat="server" /></p>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>