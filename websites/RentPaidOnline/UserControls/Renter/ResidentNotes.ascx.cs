﻿using EfxFramework;
using EfxFramework.Interfaces.UserControls.Renter;
using EfxFramework.Presenters.UserControls.Renter;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;

namespace Domus.UserControls.Renter
{
    public partial class ResidentNotes : UserControl, IRenterNotes
    {
        private RenterNotesPresenter _Presenter;

        public Page ParentPage { get { return Page; } }
        public string NoteTextInput { get { return NoteTextBox.Text; } }
        public string NoteTextView { set { NoteTextLiteral.Text = value; } }
        public EventHandler SaveButtonClicked { set { SaveButton.Click += value; } }
        public bool IsPropertyManagerSite { get { return false; } }
        public bool ResidentNotesVisible { set { ResidentNotesPanel.Visible = value; } }

        public List<RenterNote> NotesGridDataSource
        {
            set
            {
                NotesGrid.DataSource = value;
                NotesGrid.DataBind();
            }
        }

        public int NoteId
        {
            get
            {
                var Id = HttpContext.Current.Request.QueryString["RenterNoteId"].ToNullInt32();
                return !Id.HasValue ? 0 : Id.Value;
            }
        }

        public int RenterId 
        {
            get 
            {
                var Id = HttpContext.Current.Request.QueryString["renterId"].ToNullInt32();
                if (!Id.HasValue || Id.Value < 1)
                {
                    BasePage.SafePageRedirect("~/Renters/Default.aspx");
                    return 0;
                }

                return Id.Value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new RenterNotesPresenter(this);

            if (EfxFramework.Helpers.Helper.IsCsrUser)
            {
                SaveButton.Enabled = false;
                NotesGrid.Enabled = false;
                NoteTextBox.Enabled = false;       
            }

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }

        protected void NotesGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (NotesGrid.SelectedIndex > -1)
            {//int PropID = (int)GridView1.DataKeys[GridView1.SelectedIndex].Values["PropertyId"];
                //int RenterNoteID = this.NoteId;// (int)NotesGrid.DataKeys[NotesGrid.SelectedIndex].Values["RenterNoteId"];
                int RenterNoteID = (int)NotesGrid.DataKeys[NotesGrid.SelectedIndex].Values["RenterNoteId"];
                //int NoteID = RenterNoteID.ToInt32();
                GridView2.DataSource = GetSubReportDetail(RenterNoteID);
                GridView2.DataBind();

                ModalPopupExtender1.Show();
            }
        }

        public SqlDataReader GetSubReportDetail(int NoteID)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_RenterNote_GetSingleObject", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@RenterNoteID", SqlDbType.Int).Value = NoteID;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                con.Close();
                throw e;
            }

        }

        private static String ConnString
        {
            get
            {
                return EfxFramework.EfxSettings.ReportConnectionString;
            }
        }
    }
}