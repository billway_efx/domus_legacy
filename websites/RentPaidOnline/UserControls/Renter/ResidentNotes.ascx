﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResidentNotes.ascx.cs" Inherits="Domus.UserControls.Renter.ResidentNotes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:UpdatePanel ID="NotesUpdatePanel" runat="server">
    <ContentTemplate>
        
        <div class="formTint">
			<div class="formWrapper">
			<h2>Internal Notes</h2>
			<div class="formWhole">
				<label>Please add details below for the selected resident</label>
				<asp:TextBox ID="NoteTextBox" runat="server" TextMode="MultiLine" CssClass="form-controls"  Height="250px" />
			</div>
        
			<asp:RequiredFieldValidator ID="RequiredNotesValidation"
				ControlToValidate="NoteTextBox"
				Display="None"
				ErrorMessage="Please provide some details for the resident."
				runat="server"
				ValidationGroup="Notes" />

			<asp:ValidationSummary ID="ApplicationSummary"
				DisplayMode="BulletList"
				runat="server"
				ValidationGroup="Notes" />
			</div>
		</div>
		<div class="formWrapper">
			<div class="formTwoThirds">
				<br />
				<asp:GridView ID="NotesGrid" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" CssClass="table-striped centered-last" OnSelectedIndexChanged="NotesGrid_SelectedIndexChanged" DataKeyNames="RenterNoteId">
					<Columns>
						<asp:BoundField HeaderText="Date" DataField="PostedDate">
						</asp:BoundField>
                        <asp:BoundField HeaderText="Note ID" DataField="RenterNoteId" Visible="false"/>
						<asp:BoundField HeaderText="Time" DataField="PostedTime" />
						<asp:BoundField HeaderText="RPO Administrator" DataField="LastEditedBy" />
						<asp:TemplateField HeaderText="Manage">

						<ItemTemplate>
                          <% if (!EfxFramework.Helpers.Helper.IsCsrUser){ %>
	                        <a class="secondary-link"  href="<%# Page.ResolveUrl(Eval("DeleteNoteUrl").ToString()) %>" target="_self" ><span data-icon="&#x2b" aria-hidden="true" ></span></a>
                         <%} %>

							</ItemTemplate>
    
						</asp:TemplateField>
                         <asp:CommandField ShowSelectButton="True" SelectText="Details" /> 
					</Columns>
				</asp:GridView>
			</div>
			<div class="clear"></div>
			<asp:Panel ID="ResidentNotesPanel" runat="server">
				<div class="formWhole residentsNotes">
					<asp:Literal ID="NoteTextLiteral" runat="server" />
				</div>
			</asp:Panel>
		</div>
<!------------------------------------------------->
                   <div>

                    <asp:Button ID="ModalShow" runat="server" Text="Button" Style="display: none;" />

                    <asp:Panel ID="PaymentDetailsPanel" runat="server" Width="85%" CssClass="subPanelBg">

                        <asp:Panel ID="Panel2" runat="server" Width="99%" ScrollBars="Vertical" CssClass="SubReportModal">
                            <div style="padding: 10px;">


                                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="5" GridLines="Horizontal" Width="98%">
                                    <Columns>
                                        <asp:BoundField DataField="Note" HeaderText="Note" />
                                    </Columns>
                                    <HeaderStyle CssClass="SubReportTable" />
                                    <RowStyle CssClass="SubReportTableData" />
                                </asp:GridView>

                            </div>

                        </asp:Panel>
                        <div style="padding: 5px 5px 10px 5px; text-align: center;">
                            <asp:Button ID="CloseModalbtn1" runat="server" Text="Close" CssClass="btn btn-primary" />
                        </div>

                    </asp:Panel>
                     <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="PaymentDetailsPanel" BackgroundCssClass="ReportmodalPop" CancelControlID="CloseModalbtn1" TargetControlID="ModalShow"></asp:ModalPopupExtender> 
                       </div> 
                            <!--------------------------------------------------->
		<div class="button-footer">
			<div class="button-action">
				<%--<button class="btn btn-default" onclick="document.getElementById('<%= NoteTextBox.ClientID %>').value =''">Cancel</button>--%>
			</div>
			<div class="main-button-action">
                <!--CMallory - Layout Change - Changed value of CssClass property -->
				<asp:Button ID="SaveButton" runat="server" Text="Save" CssClass="btn btn-large footer-button" />
			</div>
			<div class="clear"></div>

               
		</div>

    </ContentTemplate>
</asp:UpdatePanel>
