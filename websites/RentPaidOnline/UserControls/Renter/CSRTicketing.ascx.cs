﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mb2x.ExtensionMethods;
using EfxFramework.Web;
using System.Text.RegularExpressions;

namespace Domus.UserControls.Renter
{
    public partial class CSRTicketing : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SendButton_Click(object sender, EventArgs e)
        {
            SendTicketEmail();
        }




        public void SendTicketEmail()
        {
            if (!RunValidation())
            {
                return;
            }

            var RenterID = HttpContext.Current.Request.QueryString["renterId"].ToNullInt32();

            if (!RenterID.HasValue || RenterID.Value < 1)
            {
                BasePage.SafePageRedirect("~/Renters/Default.aspx");
                return;
            }

            var NewRenterInfo = new EfxFramework.Renter(Convert.ToInt32(RenterID));
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConnString;
            SqlCommand command = new SqlCommand();
            SqlDataReader dr;
            DataTable dt = new DataTable();
            // Patrick Whittingham - 8/20/15 - task #457
            //Salcedo - task #457 - fixed syntax error
            command.CommandText = "SELECT PropertyRenter.PropertyId, Property.PropertyName,"
                                    + "Property.MainPhoneNumber "
                                    + " FROM PropertyRenter"
                                    + " join"
                                    + " Property"
                                    + " on Property.PropertyId = PropertyRenter.PropertyId"
                                     + " where PropertyRenter.RenterId = " + Convert.ToString(NewRenterInfo.RenterId);

            try
            {
                conn.Open();
                command.Connection = conn;
                dr = command.ExecuteReader();
                dt.Load(dr);
  
                string PropertyName = (from DataRow row in dt.Rows
                                       select (string)row["PropertyName"]).FirstOrDefault();
                string PropertyPhone = (from DataRow row in dt.Rows
                                        select (string)row["MainPhoneNumber"]).FirstOrDefault();
                string ResidentPhone = Convert.ToString(NewRenterInfo.MainPhoneNumber);
                string ResidentEmail = Convert.ToString(NewRenterInfo.PrimaryEmailAddress);

                string EmailMessage = "<html>Property: " + PropertyName + "<br />"
                                   + "Property Phone: " + PropertyPhone + "<br />"
                                   + "Resident Phone: " + ResidentPhone + "<br />"
                                   + "Resident Email: " + ResidentEmail + "<br />"
                                   + "Customer Service Represenative: " + HttpContext.Current.Session["CurrentUserName"].ToString() + "<br />"
                                   + "Call Back Number: " + txtCallBackNumber.Text + "<br />"
                                   + "Language: " + ddLanguage.SelectedValue + "<br />"
                                   + "Issue: " + Explanationtxt.Text + "</html>";

                string RenterFullName = NewRenterInfo.FirstName + " " + NewRenterInfo.LastName;
                string _csr = HttpContext.Current.Session["CurrentUserName"].ToString();
                int _renterID = Convert.ToInt32(RenterID);

                //cakel: added new code to send email from DB
                SubmitTicket(_renterID, RenterFullName, _csr, EmailMessage);

                //EfxFramework.BulkMail.BulkMail.SendMailMessage(
                //null,
                //"EFX",
                //false,
                //EfxFramework.Settings.SendMailUsername,
                //EfxFramework.Settings.SendMailPassword,
                //new MailAddress(EfxFramework.Settings.SupportEmail),
                //"Support Ticket From " + NewRenterInfo.FirstName + " " + NewRenterInfo.LastName,
                //Message,
                //new List<MailAddress> { new MailAddress(EfxFramework.Settings.SupportEmail) });

                lblConfirmation.Text = "Ticket email has been sent successfully.";
                lblConfirmation.Visible = true;
                txtCallBackNumber.Text = null;
                Explanationtxt.Text = null;
                ddLanguage.SelectedValue = "";

            }
            //catch (Exception exex)
            catch
            {
                //pop up saying it doesnt work
                lblConfirmation.Text = "Ticket email was not sent. Please try again later.";
                lblConfirmation.Visible = true;
            }

             //cakel: BUGID00280
            finally
            {
                conn.Close();
            }

        }

        private bool RunValidation()
        {
            CustomValidator err = new CustomValidator();
            err.ValidationGroup = "CSRTicket";
            string tmpNumber = txtCallBackNumber.Text;
            tmpNumber = tmpNumber.Replace("(", "");
            tmpNumber = tmpNumber.Replace(")", "");
            tmpNumber = tmpNumber.Replace("-", "");
            tmpNumber = tmpNumber.Replace(" ", "");
            if (tmpNumber.Length < 10)
            {
                err.IsValid = false;
                err.ErrorMessage = "Resident's contact number must be 10 digits.";
                Page.Validators.Add(err);
                return false;
            }

            if (ddLanguage.SelectedValue == "" || ddLanguage.SelectedValue == null)
            {
                err.IsValid = false;
                err.ErrorMessage = "You must select a language type.";
                Page.Validators.Add(err);
                return false;
            }

            if (Explanationtxt.Text == null || Explanationtxt.Text == "")
            {
                err.IsValid = false;
                err.ErrorMessage = "You must provide an explanation.";
                Page.Validators.Add(err);
                return false;
            }
            return true;
        }



        private void SubmitTicket(int RenterID, string RenterName, string CSR, string Message)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("Email_SupportTicket", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@RenterID", SqlDbType.Int).Value = RenterID;
            com.Parameters.Add("@ResidentName", SqlDbType.NVarChar,250).Value = RenterName;
            com.Parameters.Add("@Csr", SqlDbType.NVarChar, 300).Value = CSR;
            com.Parameters.Add("@Message", SqlDbType.NVarChar, 2000).Value = Message;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }

        }



        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }


    }
}