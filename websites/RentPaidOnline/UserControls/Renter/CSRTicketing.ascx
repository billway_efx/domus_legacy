﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CSRTicketing.ascx.cs" Inherits="Domus.UserControls.Renter.CSRTicketing" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

        <script>
            //Sys.WebForms.PageRequestManager.getInstance().add_endRequest(doSomething);
            Sys.Application.add_load(function () {
                ApplySingleMask("#<%=txtCallBackNumber.ClientID%>", "(000) 000-0000");
                //ApplySingleMask("#txtCallBackNumber.Text", "(000) 000-0000");
                //"#txtCallBackNumber".mask("(000) 000-0000");

            });
        </script>
<asp:UpdatePanel ID="CSRTicketingUpdatePanel" runat="server">
    <ContentTemplate>
        
        <div class="formTint">
			<div class="formWrapper">
			<h2>Customer Service Tier 1 Agent to Tier 2 Ticket</h2>
            <asp:ValidationSummary ID="ValidationSummary1" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="CSRTicket" />
			<div class="formHalf">
				<label style="font-weight: bold">Resident Contact Number:</label>
				<asp:TextBox ID="txtCallBackNumber" runat="server"  CssClass="form-controls"  Columns="25" > </asp:TextBox>
                          <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="None"
     ControlToValidate="txtCallBackNumber"
     ErrorMessage="Only numerics are allowed for the Resident Contact Number field." ForeColor="Red"
     ValidationExpression="^[0-9]*$" ValidationGroup="CSRTicket">*
</asp:RegularExpressionValidator>
			</div>

            <div class="formThird">
                <label style="font-weight: bold">Language Type:</label>
				<asp:DropDownList runat="server" AutoPostBack="true" ID="ddLanguage" >
                    <asp:ListItem Text="English" Value="English"></asp:ListItem>
                    <asp:ListItem Text="Spanish" Value="Spanish"></asp:ListItem>
				</asp:DropDownList>
			</div>
            
            <div class="formWhole">
               <label style="font-weight: bold">Explanation of Escalation:</label>
				<asp:TextBox TextMode="MultiLine" ID="Explanationtxt" runat="server" Rows="16"></asp:TextBox>
			</div>

            <asp:label style="font-weight: bold" id="lblConfirmation" runat="server" visible="false"></asp:label>

			<asp:RequiredFieldValidator ID="RequiredContactNumber"
				ControlToValidate="txtCallBackNumber"
				Display="None"
				ErrorMessage="You must provide a call back number for the resident."
				runat="server"
				ValidationGroup="CSRTicket" />
            
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
				ControlToValidate="Explanationtxt"
				Display="None"
				ErrorMessage="You must provide an explanation for submitting this ticket."
				runat="server"
				ValidationGroup="CSRTicket" />

			<asp:ValidationSummary ID="ApplicationSummary"
				DisplayMode="BulletList"
				runat="server"
				ValidationGroup="CSRTicket" />
			</div>
		</div>
<!------------------------------------------------->


                   
		<div class="button-footer"> 
            <div class="button-action">
				<%--<button class="btn btn-default" onclick="document.getElementById('<%= NoteTextBox.ClientID %>').value =''">Cancel</button>--%>
			</div>
			<div class="main-button-action">
                <!--CMallory - Layout Change - Changed value of CssClass property -->
				<asp:Button ID="SendButton" runat="server" Text="Send" CssClass="btn btn-large footer-button"  OnClick="SendButton_Click"/>
			</div>
			<div class="clear"></div>

               
		</div>


    </ContentTemplate>


</asp:UpdatePanel>

