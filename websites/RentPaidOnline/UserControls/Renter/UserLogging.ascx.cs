﻿using EfxFramework;
using EfxFramework.Presenters.UserControls.Renter;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;

namespace Domus.UserControls.Renter
{
    public partial class UserLogging : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //cakel: Added for Testing
            int MyTest = RenterId;

            popGrid(RenterId);

        }


        //Adds Records to grid
        public void popGrid(int _renterID)
        {
            //FeeGrid.DataSource = DLAccess.GetmonthlyFeeByID(_renterID);
            //FeeGrid.DataBind();
            UserLoggingGrid.DataSource = DLAccess.GetActivityLog(_renterID.ToString());

            UserLoggingGrid.DataBind();

        }


        public int RenterId
        {
            get
            {
                var Id = HttpContext.Current.Request.QueryString["renterId"].ToNullInt32();
                if (!Id.HasValue || Id.Value < 1)
                {
                    BasePage.SafePageRedirect("~/Renters/Default.aspx");
                    return 0;
                }

                return Id.Value;
            }
        }
    }
}