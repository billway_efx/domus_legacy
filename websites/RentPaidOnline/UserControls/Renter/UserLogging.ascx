﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserLogging.ascx.cs" Inherits="Domus.UserControls.Renter.UserLogging" %>
<%--dwillis Task 0000076--%>
<asp:UpdatePanel ID="UserLoggingPanel" runat="server">
    <ContentTemplate>
		<div class="formWrapper" >
            <h3>User Logs</h3>
			
            <div class="formWhole">
			<br />
			<asp:GridView ID="UserLoggingGrid" runat="server" AutoGenerateColumns="False" GridLines="Both" Width="100%" CssClass="table-striped centered-last">
				<Columns>
					<asp:BoundField HeaderText="Date/Time" DataField="UserLogTime" />
					<asp:BoundField HeaderText="Description" DataField="DescriptionOfLog" />
                    <asp:BoundField HeaderText="Performed By" DataField="WhoDidLog" />
				</Columns>
				<EmptyDataTemplate>
                    No user logs available for this resident.
                </EmptyDataTemplate>
			</asp:GridView>
			</div>
			<div class="clear"></div>
		</div>

    </ContentTemplate>
</asp:UpdatePanel>

