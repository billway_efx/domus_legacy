﻿using System;
using EfxFramework.Interfaces.UserControls.Renter;
using EfxFramework.Presenters.UserControls.Renter;

namespace Domus.UserControls.Renter
{
    public partial class RenterSelection : System.Web.UI.UserControl, IEfxAdministratorSiteRenterSelection
    {
        private RenterSelectionBase<EfxFramework.EfxAdministrator> _presenter;

        public EventHandler PropertySelected { set { PropertyDropdownList.SelectedIndexChanged += value; } }
        public EventHandler AddRenterClicked { set { AddRenterButton.Click += value; } }
        public string PropertyDropdownSelectedValue { get { return PropertyDropdownList.SelectedValue; } set { PropertyDropdownList.SelectedValue = value; } }
        public string PropertyDropdownSelectedText { get { return PropertyDropdownList.SelectedItem.Text; } }
        public string RenterDropdownSelectedValue { get { return RenterDropdownList.SelectedValue; } set { RenterDropdownList.SelectedValue = value; } }
        public string RenterDropdownSelectedText { get { return RenterDropdownList.SelectedItem.Text; } }
        public string RenterImageUrl { set { RenterImage.ImageUrl = value; } }
        public string RenterDisplayNameText { set { RenterDisplayNameLabel.Text = value; } }
        public string RenterMainPhoneNumberText { set { RenterMainPhoneNumberLabel.Text = value; } }
        public string RenterPrimaryEmailAddressText { set { RenterPrimaryEmailAddress.Text = value; } }
        public bool RenterInformationPanelVisible { get { return RenterInfoPanel.Visible; } set { RenterInfoPanel.Visible = value; } }
        public bool RenterDropdownListEnabled { get { return RenterDropdownList.Enabled; } set { RenterDropdownList.Enabled = value; } }

        public object PropertyDropdownDataSource
        {
            set
            {
                PropertyDropdownList.DataSource = value;
                PropertyDropdownList.DataBind();
            }
        }

        public object RenterDropdownDataSource
        {
            set
            {
                RenterDropdownList.DataSource = value;
                RenterDropdownList.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter = RenterSelectionFactory.GetRenterSelection(this);

            if (!Page.IsPostBack)
                _presenter.InitializeValues();

            PropertyDropdownList.Focus();
        }
    }
}