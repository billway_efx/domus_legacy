﻿using EfxFramework.Interfaces.UserControls.Property;
using System;

namespace Domus.UserControls
{
    public partial class MRIAddEdit : System.Web.UI.UserControl, IMRIAddEdit
    {
        public EfxFramework.Presenters.UserControls.Property.MRIAddEdit Presenter { get; set; }

        public int PropertyId { get; set; }

        public bool EnableFtpUpload { get { return cbEnableFtpUpload.Checked; } set { cbEnableFtpUpload.Checked = value; } }
        public bool EnableFtpDownload { get { return cbEnableFtpDownload.Checked; } set { cbEnableFtpDownload.Checked = value; } }
        public string MRIPropertyIDText { get { return txtMRIPropertyId.Text; } set { txtMRIPropertyId.Text = value; } }
        public string FTPSiteAddress { get { return txtSiteAddress.Text; } set { txtSiteAddress.Text = value; } }
        public string FTPPort { get { return txtPort.Text; } set { txtPort.Text = value; } }
        public string FTPAccountUserName { get { return txtAccountUserName.Text; } set { txtAccountUserName.Text = value; } }
        public string FTPAccountPassword { get { return txtAccountPassword.Text; } set { txtAccountPassword.Text = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            Presenter = new EfxFramework.Presenters.UserControls.Property.MRIAddEdit(this);

            if (!Page.IsPostBack)
            {
                Presenter.InitializeValues();
                if (EfxFramework.Helpers.Helper.IsRpoAdmin)
                {
                    MRIEfxAdminDiv.Visible = true;
                }
                else
                {
                    MRIEfxAdminDiv.Visible = false;
                    txtSiteAddress.Visible = false;
                    txtPort.Visible = false;
                    txtAccountUserName.Visible = false;
                    txtAccountPassword.Visible = false;

                    cbEnableFtpDownload.Enabled = false;
                    cbEnableFtpUpload.Enabled = false;
                    txtMRIPropertyId.Enabled = false;
                }
            }
        }
    }
}