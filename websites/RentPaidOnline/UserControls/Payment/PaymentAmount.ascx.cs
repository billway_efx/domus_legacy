﻿using System;
using EfxFramework.Interfaces.UserControls.Payment;

namespace Domus.UserControls.Payment
{
    public partial class PaymentAmount : System.Web.UI.UserControl, IPaymentAmount
    {
        private EfxFramework.Presenters.UserControls.Payment.PaymentAmount _presenter;

        public string DisplayNameText { set { DisplayNameLabel.Text = value; } }
        public string RentAmountText { get { return RentAmountTextbox.Text; } set { RentAmountTextbox.Text = value; } }
        public string RentDescriptionText { get { return DescriptionTextbox.Text; } set { DescriptionTextbox.Text = value; } }
        public string OtherAmount1Text { get { return OtherAmountTextbox.Text; } set { OtherAmountTextbox.Text = value; } }
        public string OtherDescription1Text { get { return OtherAmountDescriptionTextbox.Text; } set { OtherAmountDescriptionTextbox.Text = value; } }
        public string OtherAmount2Text { get { return OtherAmount2Textbox.Text; } set { OtherAmount2Textbox.Text = value; } }
        public string OtherDescription2Text { get { return OtherAmountDescription2Textbox.Text; } set { OtherAmountDescription2Textbox.Text = value; } }
        public string OtherAmount3Text { get { return OtherAmount3Textbox.Text; } set { OtherAmount3Textbox.Text = value; } }
        public string OtherDescription3Text { get { return OtherAmountDescription3Textbox.Text; } set { OtherAmountDescription3Textbox.Text = value; } }
        public string OtherAmount4Text { get { return OtherAmount4Textbox.Text; } set { OtherAmount4Textbox.Text = value; } }
        public string OtherDescription4Text { get { return OtherAmountDescription4Textbox.Text; } set { OtherAmountDescription4Textbox.Text = value; } }
        public string OtherAmount5Text { get { return OtherAmount5Textbox.Text; } set { OtherAmount5Textbox.Text = value; } }
        public string OtherDescription5Text { get { return OtherAmountDescription5Textbox.Text; } set { OtherAmountDescription5Textbox.Text = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter = new EfxFramework.Presenters.UserControls.Payment.PaymentAmount(this);

            if (!Page.IsPostBack)
                _presenter.InitializeValues();
        }
    }
}