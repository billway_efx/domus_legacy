﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CharityDonation.ascx.cs" Inherits="Domus.UserControls.Payment.CharityDonation" %>

<label>Donate</label>
<div>
    <asp:TextBox ID="DonationAmountTextbox" CssClass="textbox money" ClientIDMode="Static" Width="40px" runat="server"></asp:TextBox>
    <asp:DropDownList ID="CharityDropdownList" runat="server" Width="200px" AppendDataBoundItems="True" DataTextField="CharityName" DataValueField="CharityId">
        <asp:ListItem Selected="True" Value="-1">-- Select Charity --</asp:ListItem>
    </asp:DropDownList>
</div>