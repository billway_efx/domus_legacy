﻿using EfxFramework.Interfaces.UserControls.Payment;
using System;
using System.Web.UI;

namespace Domus.UserControls.Payment
{
    public partial class PaymentMethods : System.Web.UI.UserControl, IEfxAdministratorSitePaymentMethods
    {
        private EfxFramework.Presenters.UserControls.Payment.PaymentMethodsBase _presenter;

        public EventHandler PaymentMethodsSelected { set { PaymentMethodsDropdownList.SelectedIndexChanged += value; } }
        public EventHandler UpdatePanelPreRender { set { PaymentMethodsUpdatePanel.PreRender += value; } }
        public UpdatePanel PaymentMethodsPanel { get { return PaymentMethodsUpdatePanel; } }
        public string PaymentMethodsSelectedValue { get { return PaymentMethodsDropdownList.SelectedValue; } set { PaymentMethodsDropdownList.SelectedValue = value; } }
        public string PaymentMethodsSelectedText { get { return PaymentMethodsDropdownList.SelectedItem.Text; } }
        public string BankAccountNumberText { get { return BankAccountNumberTextbox.Text; } set { BankAccountNumberTextbox.Text = value; } }
        public string RoutingNumberText { get { return RoutingNumberTextbox.Text; } set { RoutingNumberTextbox.Text = value; } }
        public string NameOnCardText { get { return NameOnCardTextbox.Text; } set { NameOnCardTextbox.Text = value; } }
        public string CreditCardNumberText { get { return CreditCardNumberTextbox.Text; } set { CreditCardNumberTextbox.Text = value; } }
        public string ExpirationDateText { get { return ExpirationDateTextbox.Text; } set { ExpirationDateTextbox.Text = value; } }
        public string CvvText { get { return CVVTextbox.Text; } set { CVVTextbox.Text = value; } }
        public bool EcheckPanelVisible { get { return ECheckPanel.Visible; } set { ECheckPanel.Visible = value; } }
        public bool CreditCardPanelVisible { get { return CreditPanel.Visible; } set { CreditPanel.Visible = value; } }
        public string CurrentRenterId { get { return HiddenRenterId.Value; } set { HiddenRenterId.Value = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter = EfxFramework.Presenters.UserControls.Payment.PaymentMethodsFactory.GetPaymentMethods(this);

            if (!Page.IsPostBack)
                _presenter.InitializeValues();
        }
    }
}