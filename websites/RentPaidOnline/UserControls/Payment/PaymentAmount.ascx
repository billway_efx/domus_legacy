﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentAmount.ascx.cs" Inherits="Domus.UserControls.Payment.PaymentAmount" %>

<h2>Payment Amount</h2>
<div class="float-wrap">
    <asp:UpdatePanel ID="PaymentAmountUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:Panel ID="RenterPanelWrap" runat="server">
                <div style="width: 300px; float: left; margin: 0 5px 5px 0;">
                    <h4><asp:Label ID="DisplayNameLabel" runat="server" /></h4>
                    <label>Rent Amount</label>
                    <asp:TextBox ClientIDMode="Static" CssClass="textbox w100 money" runat="server" ID="RentAmountTextbox" />
                    <label>Description</label>
                    <asp:TextBox ClientIDMode="Static" CssClass="textbox w100" runat="server" ID="DescriptionTextbox" />
                    <label>Additional Amount</label>
                    <asp:TextBox ClientIDMode="Static" CssClass="textbox w100 money" runat="server" ID="OtherAmountTextbox" />
                    <label>Description</label>
                    <asp:TextBox ClientIDMode="Static" CssClass="textbox w100" runat="server" ID="OtherAmountDescriptionTextbox" />
                    <label>Additional Amount 2</label>
                    <asp:TextBox ClientIDMode="Static" CssClass="textbox w100 money" runat="server" ID="OtherAmount2Textbox" />
                    <label>Description 2</label>
                    <asp:TextBox ClientIDMode="Static" CssClass="textbox w100" runat="server" ID="OtherAmountDescription2Textbox" />
                    <label>Additional Amount 3</label>
                    <asp:TextBox ClientIDMode="Static" CssClass="textbox w100 money" runat="server" ID="OtherAmount3Textbox" />
                    <label>Description 3</label>
                    <asp:TextBox ClientIDMode="Static" CssClass="textbox w100" runat="server" ID="OtherAmountDescription3Textbox" />
                    <label>Additional Amount 4</label>
                    <asp:TextBox ClientIDMode="Static" CssClass="textbox w100 money" runat="server" ID="OtherAmount4Textbox" />
                    <label>Description 4</label>
                    <asp:TextBox ClientIDMode="Static" CssClass="textbox w100" runat="server" ID="OtherAmountDescription4Textbox" />
                    <label>Additional Amount 5</label>
                    <asp:TextBox ClientIDMode="Static" CssClass="textbox w100 money" runat="server" ID="OtherAmount5Textbox" />
                    <label>Description 5</label>
                    <asp:TextBox ClientIDMode="Static" CssClass="textbox w100" runat="server" ID="OtherAmountDescription5Textbox" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>