﻿using System;
using EfxFramework.Interfaces.UserControls.Payment;

namespace Domus.UserControls.Payment
{
    public partial class CharityDonation : System.Web.UI.UserControl, ICharityDonation
    {
        private EfxFramework.Presenters.UserControls.Payment.CharityDonation _presenter;

        public string DonationAmountText { get { return DonationAmountTextbox.Text; } set { DonationAmountTextbox.Text = value; } }
        public string SelectedCharityValue { get { return CharityDropdownList.SelectedValue; } set { CharityDropdownList.SelectedValue = value; } }
        public string SelectedCharityText { get { return CharityDropdownList.SelectedItem.Text; } }
        public object CharityDropdownDataSource
        {
            set
            {
                CharityDropdownList.DataSource = value;
                CharityDropdownList.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter = new EfxFramework.Presenters.UserControls.Payment.CharityDonation(this);

            if (!Page.IsPostBack)
                _presenter.InitializeValues();
        }
    }
}