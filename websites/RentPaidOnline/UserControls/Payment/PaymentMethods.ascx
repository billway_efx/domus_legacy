﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentMethods.ascx.cs" Inherits="Domus.UserControls.Payment.PaymentMethods" %>

<h2 class="padding-top clear">Payment Information</h2>
<asp:UpdatePanel ID="PaymentMethodsUpdatePanel" runat="server">
    <ContentTemplate>
        <div class="payment-method-labels">
            <label>Payment Method:</label>
        </div>
        <div class="payment-method-values" style="width: 305px;">
            <asp:DropDownList ID="PaymentMethodsDropdownList" runat="server" AutoPostBack="true">
                <asp:ListItem Text="eCheck" Value="echeck" Selected="True" />
                <asp:ListItem Text="Credit" Value="credit" />
            </asp:DropDownList>
        </div>
        <asp:Panel ID="ECheckPanel" runat="server" Visible="true" Width="305px">                
            <label class="clear padTop">Bank Account Number</label>
            <!--cakel: BUGID00072 - Updated max length from 15 to 17 -->
            <asp:TextBox ClientIDMode="Static" CssClass="textbox block bank" Width="300px" runat="server" ID="BankAccountNumberTextbox" MaxLength="17" />
            <label>Routing Number</label>
            <asp:TextBox ClientIDMode="Static" CssClass="textbox block routing" Width="300px" runat="server" ID="RoutingNumberTextbox" MaxLength="9" />
        </asp:Panel>
        <asp:Panel ID="CreditPanel" runat="server" Visible="false" Width="305px">
            <label class="padTop">Name On Card</label>
            <asp:TextBox ClientIDMode="Static" CssClass="textbox block" Width="300px" runat="server" ID="NameOnCardTextbox" />
            <label>Credit Card Number</label>
            <asp:TextBox ClientIDMode="Static" CssClass="textbox block credit" Width="300px" runat="server" ID="CreditCardNumberTextbox" MaxLength="16" />
            <label style="width: 300px">Exp. Date</label>
            <asp:TextBox ClientIDMode="Static" CssClass="textbox block datepicker" Width="300px" runat="server" ID="ExpirationDateTextbox" />
            <label style="width: 300px;">CVV</label>
            <asp:TextBox ClientIDMode="Static" CssClass="textbox block cvv" Width="300px" runat="server" ID="CVVTextbox" />
        </asp:Panel>
        <asp:HiddenField ID="HiddenRenterId" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>