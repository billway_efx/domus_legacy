﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework;
using EfxFramework.Web;
using EfxFramework.ExtensionMethods;

namespace RentPaidOnline.UserControls
{
    public partial class UserEditorControl : System.Web.UI.UserControl
    {
        const string CACHE_KEY = "CURRENT_EDITING_USER";

        public bool EditMode
        {
            get;
            set;
        }

        public bool UseLoggedInUser
        {
            get;
            set;
        }

        SystemUser _user = null;
        public SystemUser CurrentUser
        {
            get
            {
                if (_user == null)
                {
                    var nullId = Request.QueryString["userId"].ToNullInt32();

                    // Set to query string
                    var id = nullId.HasValue ? nullId.Value : 0;
                    var cache = false;

                    // UseLoggedInUser short circuit
                    if (UseLoggedInUser)
                    {
                        id = ((BasePage)Page).Facade.CurrentUser.SystemUserId.Value;
                    }

                    // If not in query string, check cache
                    if (id <= 0 && (Cache[CACHE_KEY] != null))
                        id = (int)Cache[CACHE_KEY];

                    if (id > 0)
                    {
                        _user = new SystemUser(id);
                        cache = true;
                    }
                    if (cache && !UseLoggedInUser)
                    {
                        // Remove and add to cache
                        if (Cache[CACHE_KEY] != null) Cache.Remove(CACHE_KEY);
                        Cache.Add(CACHE_KEY, id, null, DateTime.Now.Add(new TimeSpan(0, 30, 0)), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.Low, null);
                    }
                }

                return _user;
            }
        }

        Renter _renter = null;
        Renter CurrentRenter
        {
            get
            {
                if (_renter == null && !string.IsNullOrEmpty(Request.QueryString["renterId"]))
                {
                    var id = Request.QueryString["renterId"].ToNullInt32();
                    if (id.HasValue)
                        _renter = new Renter(id.Value);
                }

                if (_renter == null)
                    _renter = Renter.GetRenterBySystemUserId(CurrentUser.SystemUserId.Value);

                return _renter;
            }
        }

        PropertyStaff _staff = null;
        PropertyStaff CurrentPropertyStaff
        {
            get
            {
                if (_staff == null && !string.IsNullOrEmpty(Request.QueryString["propertyStaffId"]))
                {
                    var id = Request.QueryString["propertyStaffId"].ToNullInt32();
                    _staff = new PropertyStaff(id);
                }

                if (_staff == null)
                {
                    _staff = PropertyStaff.GetAllPropertyStaffList().Where(ps => ps.SystemUserId == CurrentUser.SystemUserId).FirstOrDefault();
                }

                return _staff;
            }
        }

        Company _company = null;
        Company CurrentCompany
        {
            get
            {
                if (_company == null && !string.IsNullOrEmpty(Request.QueryString["companyId"]))
                {
                    var id = Request.QueryString["companyId"].ToNullInt32();
                    if (id.HasValue)
                        _company = new Company(id.Value);
                }

                return _company;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AccountTypeDropdownList.BindToEnum(typeof(SystemUser.SystemRole), true);
                AccountTypeDropdownList.Items.Insert(0, new ListItem("-- Select Account Type --", "-1"));

                var props = Property.GetAllPropertyList();
                PropertyDropdownList.DataSource = props;
                PropertyDropdownList.DataTextField = "PropertyName";
                PropertyDropdownList.DataValueField = "PropertyId";
                PropertyDropdownList.DataBind();
                PropertyDropdownList.Items.Insert(0, new ListItem("-- Select Property --", "-1"));

                StateDropdown.BindToEnum(typeof(StateProvince));

                // Check for query string hints
                if (!string.IsNullOrEmpty(Request.QueryString["t"]))
                {
                    var t = Request.QueryString["t"];
                    if (t.Equals("renter"))
                    {
                        AccountTypeDropdownList.SelectedValue = ((int)SystemUser.SystemRole.Renter).ToString();
                    }
                }
                if (!string.IsNullOrEmpty(Request.QueryString["pid"]))
                {
                    var pid = Request.QueryString["pid"].ToNullInt32();
                    if (pid.HasValue)
                    {
                        PropertyDropdownList.SelectedValue = pid.Value.ToString();
                        PropertySelectPanel.Visible = true;
                    }
                }

                // Focus!
                FirstNameTextbox.Focus();
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (EditMode && CurrentUser != null && CurrentUser.SystemUserId.HasValue)
            {
                var fn = string.Empty;
                var mn = string.Empty;
                var ln = string.Empty;
                var address = string.Empty;
                var address2 = string.Empty;
                var city = string.Empty;
                var zip = string.Empty;
                var homePhone = string.Empty;
                var cellPhone = string.Empty;
                var fax = string.Empty;
                var email = string.Empty;
                var username = CurrentUser.UserName;
                var pw = string.Empty;
                var imgUrl = string.Empty;
                int? stateProvinceId = null;

                if (this.CurrentRenter != null)
                {
                    fn = this.CurrentRenter.FirstName;
                    mn = this.CurrentRenter.MiddleName;
                    ln = this.CurrentRenter.LastName;
                    address = this.CurrentRenter.StreetAddress;
                    address2 = this.CurrentRenter.StreetAddress2;
                    city = this.CurrentRenter.City;
                    zip = this.CurrentRenter.PostalCode;
                    homePhone = this.CurrentRenter.MainPhoneNumber;
                    cellPhone = this.CurrentRenter.MobilePhoneNumber;
                    fax = this.CurrentRenter.FaxNumber;
                    email = this.CurrentRenter.PrimaryEmailAddress;
                    username = this.CurrentUser.UserName;
                    stateProvinceId = this.CurrentRenter.StateProvinceId;

                    imgUrl = string.Concat("/Handlers/EFXAdminHandler.ashx?RequestType=0&renterId=", CurrentRenter.RenterId);

                    AccountTypeDropdownList.SelectedValue = ((int)SystemUser.SystemRole.Renter).ToString();
                }
                else if (this.CurrentPropertyStaff != null)
                {
                    fn = this.CurrentPropertyStaff.FirstName;
                    mn = this.CurrentPropertyStaff.MiddleName;
                    ln = this.CurrentPropertyStaff.LastName;
                    address = this.CurrentPropertyStaff.StreetAddress;
                    address2 = this.CurrentPropertyStaff.StreetAddress2;
                    city = this.CurrentPropertyStaff.City;
                    zip = this.CurrentPropertyStaff.PostalCode;
                    homePhone = this.CurrentPropertyStaff.MainPhoneNumber;
                    cellPhone = this.CurrentPropertyStaff.MobilePhoneNumber;
                    fax = this.CurrentPropertyStaff.FaxNumber;
                    email = this.CurrentPropertyStaff.PrimaryEmailAddress;
                    username = this.CurrentUser.UserName;
                    stateProvinceId = this.CurrentPropertyStaff.StateProvinceId;

                    imgUrl = string.Concat("/Handlers/EFXAdminHandler.ashx?RequestType=0&propertyStaffId=", CurrentPropertyStaff.PropertyStaffId);

                    AccountTypeDropdownList.SelectedValue = ((int)SystemUser.SystemRole.Property).ToString();
                }
                else if (this.CurrentCompany != null)
                {
                    fn = this.CurrentCompany.ContactFirstName;
                    ln = this.CurrentCompany.ContactLastName;
                    address = this.CurrentCompany.Address;
                    address2 = this.CurrentCompany.Address2;
                    city = this.CurrentCompany.City;
                    zip = this.CurrentCompany.PostalCode;
                    homePhone = this.CurrentCompany.Phone;
                    cellPhone = this.CurrentCompany.Mobile;
                    fax = this.CurrentCompany.Fax;
                    email = this.CurrentCompany.EmailAddress;
                    stateProvinceId = (int)this.CurrentCompany.StateProvinceId;

                    imgUrl = string.Concat("/Handlers/EFXAdminHandler.ashx?RequestType=0&companyId=", CurrentCompany.CompanyId);
                }

                FirstNameTextbox.Text = fn;
                LastNameTextbox.Text = ln;
                MiddleNameTextbox.Text = mn;
                AddressTextbox.Text = address;
                AddressLine2Textbox.Text = address2;
                CityTextbox.Text = city;
                if (stateProvinceId.HasValue)
                    StateDropdown.SelectedValue = stateProvinceId.Value.ToString();
                ZipTextbox.Text = zip;
                HomePhoneTextbox.Text = homePhone;
                CellPhoneTextbox.Text = cellPhone;
                FaxTextbox.Text = fax;
                Email1Textbox.Text = email;

                UserNameTextbox.Text = username;
                PasswordTextbox.Text = string.Empty;
                ConfirmPasswordTextbox.Text = string.Empty;

                Image1.ImageUrl = imgUrl;
            }

            AddAnotherUserButton.Visible = UseLoggedInUser == false;
            AllUsersLink.Visible = UseLoggedInUser == false;
        }

        protected void AccountTypeDropdownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AccountTypeDropdownList.SelectedValue != "-1")
            {
                EfxFramework.SystemUser.SystemRole role = (EfxFramework.SystemUser.SystemRole)Enum.Parse(typeof(EfxFramework.SystemUser.SystemRole), AccountTypeDropdownList.SelectedValue);

                var showPropertyDropdown = false;
                if (role == SystemUser.SystemRole.Property ||
                    role == SystemUser.SystemRole.Renter)
                {
                    showPropertyDropdown = true;
                }

                PropertySelectPanel.Visible = showPropertyDropdown;
            }
        }

        protected void AddAnotherUserButton_Click(object sender, EventArgs e)
        {
            var id = SaveNewUser();

            if (id.HasValue)
            {
                // Clear out for new user
                FirstNameTextbox.Text = string.Empty;
                MiddleNameTextbox.Text = string.Empty;
                LastNameTextbox.Text = string.Empty;
                AddressTextbox.Text = string.Empty;
                AddressLine2Textbox.Text = string.Empty;
                CityTextbox.Text = string.Empty;
                ZipTextbox.Text = string.Empty;
                HomePhoneTextbox.Text = string.Empty;
                CellPhoneTextbox.Text = string.Empty;
                FaxTextbox.Text = string.Empty;
                Email1Textbox.Text = string.Empty;

                UserNameTextbox.Text = string.Empty;
                PasswordTextbox.Text = string.Empty;
                ConfirmPasswordTextbox.Text = string.Empty;
                StateDropdown.SelectedIndex = 0;

                // Focus!
                FirstNameTextbox.Focus();
            }
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            var id = SaveNewUser();
            if (id.HasValue)
            {
                Page.Response.Redirect(
                    string.Concat("/Users/UserDetails.aspx?userId=", id.Value));
            }
        }

        int? SaveNewUser()
        {
            if (string.IsNullOrEmpty(Email1Textbox.Text))
            {
                ((BasePage)Page).PageMessage.DisplayMessage(SystemMessageType.Error, "Email address is required.");
                return null;
            }
            if (AccountTypeDropdownList.SelectedValue == "-1")
            {
                ((BasePage)Page).PageMessage.DisplayMessage(SystemMessageType.Error, "Account type is required.");
                return null;
            }
            if (PropertySelectPanel.Visible && PropertyDropdownList.SelectedValue == "-1")
            {
                ((BasePage)Page).PageMessage.DisplayMessage(SystemMessageType.Error, "Property is required for this account type.");
                return null;
            }

            var username = UserNameTextbox.Text;
            var password = PasswordTextbox.Text;

            // Check for existing
            var existing = SystemUser.GetByUserName(username);
            if (existing != null && existing.SystemUserId.HasValue)
            {
                ((BasePage)Page).PageMessage.DisplayMessage(SystemMessageType.Error, string.Concat("The Username \"", username, "\" is already taken."));
                return null;
            }

            // Create user
            Membership.CreateUser(username, password);

            // Get new user & set role
            EfxFramework.SystemUser.SystemRole role = (EfxFramework.SystemUser.SystemRole)Enum.Parse(typeof(EfxFramework.SystemUser.SystemRole), AccountTypeDropdownList.SelectedValue);
            var user = SystemUser.GetByUserName(username);
            user.SystemRoleId = role;
            SystemUser.Set(user);

            // Set user security
            var secSetting = new SystemUserSecurity();
            secSetting.SystemUserId = user.SystemUserId.Value;
            secSetting.IsPropertyGeneralManager = InTheGenManagerCheckbox.Checked;
            secSetting.CanViewSalesStaffAccounts = CanViewSalesStaffAccountsCheckbox.Checked;
            secSetting.CanViewRenterDetails = CanViewRenterDetailedInformationCheckbox.Checked;
            secSetting.CanViewPropertyDetail = CanViewPropertyDetailCheckbox.Checked;
            secSetting.CanSendBulkMail = CanSendOutBulkEmailCheckbox.Checked;
            secSetting.CanCreateNewSalesStaffAccounts = CanCreateSalesAccountsCheckbox.Checked;
            secSetting.CanCreateNewRenterAccounts = CanCreateNewRenterAccountsCheckbox.Checked;
            SystemUserSecurity.Set(secSetting);

            // Create appropriate user for given account type
            if (role == SystemUser.SystemRole.Property)
            {
                var staff = new PropertyStaff();
                staff.FirstName = FirstNameTextbox.Text;
                staff.LastName = LastNameTextbox.Text;
                staff.MiddleName = MiddleNameTextbox.Text;
                staff.AlternateEmailAddress1 = Email1Textbox.Text;
                staff.City = CityTextbox.Text;
                staff.StateProvinceId = StateDropdown.SelectedValue.ToNullInt32();
                staff.PostalCode = ZipTextbox.Text;
                staff.MainPhoneNumber = HomePhoneTextbox.Text;
                staff.MobilePhoneNumber = CellPhoneTextbox.Text;
                staff.FaxNumber = FaxTextbox.Text;
                staff.PropertyStaffId = PropertyStaff.Set(staff);

                if (PhotoUpload1.HasFile)
                {
                    PropertyStaff.SetPhoto(staff.PropertyStaffId.Value, PhotoUpload1.FileBytes);
                }

                // Add property
                var propId = PropertyDropdownList.SelectedValue.ToInt32();
                Property.AssignStaffToProperty(staff.PropertyStaffId.Value, propId, false);
            }
            else if (role == SystemUser.SystemRole.Renter)
            {
                // Create Renter
                var renter = new Renter();
                renter.SystemUserId = user.SystemUserId;
                renter.FirstName = FirstNameTextbox.Text;
                renter.MiddleName = MiddleNameTextbox.Text;
                renter.LastName = LastNameTextbox.Text;
                renter.StreetAddress = AddressTextbox.Text;
                renter.StreetAddress2 = AddressLine2Textbox.Text;
                renter.City = CityTextbox.Text;
                renter.StateProvinceId = StateDropdown.SelectedValue.ToNullInt32();
                renter.PostalCode = ZipTextbox.Text;
                renter.MainPhoneNumber = HomePhoneTextbox.Text;
                renter.MobilePhoneNumber = CellPhoneTextbox.Text;
                renter.FaxNumber = FaxTextbox.Text;
                renter.PrimaryEmailAddress = Email1Textbox.Text;

                renter.RenterId = Renter.Set(renter);

                if (PhotoUpload1.HasFile)
                {
                    Renter.SetPhoto(renter.RenterId, PhotoUpload1.FileBytes);
                }

                // Add property
                var propId = PropertyDropdownList.SelectedValue.ToInt32();
                Renter.AssignRenterToProperty(propId, renter.RenterId);
            }
            else if (role == SystemUser.SystemRole.SalesManager)
            {
            }
            else if (role == SystemUser.SystemRole.SalesPerson)
            {
            }
            else if (role == SystemUser.SystemRole.SystemAdministrator)
            {
            }
            else if (role == SystemUser.SystemRole.Unassigned)
            {
            }

            ((BasePage)Page).PageMessage.DisplayMessage(EfxFramework.SystemMessageType.Success, "Successfully created new user.");

            return user.SystemUserId;
        }
    }
}