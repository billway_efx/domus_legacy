﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="YardiAddEdit.ascx.cs" Inherits="Domus.UserControls.YardiAddEdit" %>

<div class="formWhole">
    <div class="formThird">
        <label>Yardi Username</label>
        <asp:TextBox ID="YardiUsernameText" runat="server" CssClass="form-control" />
    </div>
    <div class="formThird">
        <label>Yardi Password</label>
        <asp:TextBox ID="YardiPasswordText" runat="server" CssClass="form-control" />
    </div>
    <div class="formThird" style="padding-top: 12px;">
        &nbsp;<br />
        <asp:CheckBox ID="YardiDepositDate" runat="server" Checked="false" Text="&nbsp;Use Deposit Date API" />
    </div>
</div>


<div class="formWhole">
    <div class="formThird">
        <label>Yardi Server Name</label>
        <asp:TextBox ID="YardiServerNameText" runat="server" CssClass="form-control" />
    </div>

    <div class="formThird">
        <label>Yardi Database Name</label>
        <asp:TextBox ID="YardiDatabaseNameText" runat="server" CssClass="form-control" />
    </div>

    <div class="formThird">
        <label>Yardi Platform</label>
        <asp:TextBox ID="YardiPlatformText" runat="server" CssClass="form-control" />
    </div>
</div>

<div class="formTwoThirds">
    <label>Yardi Url</label>
    <asp:TextBox ID="YardiEndpointText" runat="server" CssClass="form-control" />
</div>

<div class="formThird">
    <label>Yardi Property ID</label>
    <asp:TextBox ID="YardiPropertyIdText" runat="server" CssClass="form-control" />
</div>
