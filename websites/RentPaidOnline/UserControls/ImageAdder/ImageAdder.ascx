﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageAdder.ascx.cs" Inherits="Domus.UserControls.ImageAdder.ImageAdder" %>

<div class="formWrapper">
    <div class="formHalf">
        <div class="well">
			<asp:FileUpload ID="PhotoUpload" runat="server" CssClass="btn btn-default" />
        </div>
    </div>
    <div class="formHalf noLabelMore">
        <asp:LinkButton ID="AddPhotoButton" runat="server" CssClass="btn btn-default">Add Photo</asp:LinkButton>
    </div>
    <div class="formWhole">
        <asp:ListView ID="PhotoListView" runat="server">
            <LayoutTemplate>
                <ul class="list-unstyled">
                    <li id="itemPlaceholder" runat="server" />
                </ul>
            </LayoutTemplate>
            <ItemTemplate>
                <li class="imageContainer bgCornersAndShadows imageSection">
                    <asp:Image ID="Photo" runat="server" ImageUrl='<%# Bind("PhotoUrl") %>' AlternateText='<%# Bind("Description") %>' />
                    <asp:LinkButton ID="DeletePhotoButton" runat="server" CssClass="btn btn-default" Style="margin-top: 8px;">Delete</asp:LinkButton>
                </li>
            </ItemTemplate>
        </asp:ListView>
    </div>
</div>