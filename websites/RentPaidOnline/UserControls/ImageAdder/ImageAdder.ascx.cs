﻿using EfxFramework;
using EfxFramework.Interfaces.UserControls.ImageAdder;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domus.UserControls.ImageAdder
{
    public partial class ImageAdder : UserControl, IImageAdder
    {
        private EfxFramework.Presenters.UserControls.ImageAdder.ImageAdder _Presenter;

        public EventHandler AddPhotoClicked { set { AddPhotoButton.Click += value; } }
        public EventHandler<ListViewCommandEventArgs> ItemCommandClicked { set { PhotoListView.ItemCommand += value; } }
        public byte[] FileUploadBytes { get { return PhotoUpload.HasFile ? PhotoUpload.FileBytes : null; } }
        public int MaxNumberOfPhotos { get; set; }
        public List<byte[]> CurrentPhotos { get; set; }

        public List<Photo> PhotoDataSource
        {
            set
            {
                PhotoListView.DataSource = value;
                PhotoListView.DataBind();
            }
        }

        public bool CanAddMorePhotos
        {
            set
            {
                PhotoUpload.Enabled = value;
                AddPhotoButton.Enabled = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new EfxFramework.Presenters.UserControls.ImageAdder.ImageAdder(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }

        public void ClearPhotos()
        {
            _Presenter.ClearPhotos();
        }
    }
}