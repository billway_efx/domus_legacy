﻿using System;
using EfxFramework.Interfaces.UserControls.Payer;
using EfxFramework.Interfaces.UserControls.Payment;
using EfxFramework.Interfaces.UserControls.PaymentSteps;
using EfxFramework.Interfaces.UserControls.Renter;

namespace Domus.UserControls.PaymentSteps
{
    public partial class OneTimePaymentStepOne : System.Web.UI.UserControl, IEfxAdministratorOneTimePaymentStepOne
    {
        private EfxFramework.Presenters.UserControls.PaymentSteps.OneTimePaymentStepOneBase _presenter;

        public IRenterSelection RenterSelectionControl { get { return RenterSelectionUserControl; } }
        public IPayerInformation PayerInformationControl { get { return PayerInformationUserControl; } }
        public IPaymentMethods PaymentMethodsControl { get { return PaymentMethodsUserControl; } }
        public IPaymentAmount PaymentAmountControl { get { return PaymentAmountUserControl; } }
        public ICharityDonation CharityDonationControl { get { return CharityDonationUserControl; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter = EfxFramework.Presenters.UserControls.PaymentSteps.OneTimePaymentStepOneFactory.GetOneTimePaymentStepOne(this);
        }
    }
}