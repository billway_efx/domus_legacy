﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OneTimePaymentStepTwo.ascx.cs" Inherits="Domus.UserControls.PaymentSteps.OneTimePaymentStepTwo" %>

<div class="onetime-payment-info">
    <h2>Take A One Time Payment - Confirmation Page</h2>
    <div class="float-left float-wrap" style="padding: 0 20px 20px 60px; width: 390px;">
        <uc1:ConfirmationCard ID="RenterInformationConfirmationUserControl" HeaderText="Resident's Information" runat="server" />
        <uc1:ConfirmationCard ID="PayerInformationConfirmationUserControl" HeaderText="Payer's Information" runat="server" />
        <uc1:ConfirmationCard ID="LeasingAgentConfirmationUserControl" HeaderText="Leasing Agent" runat="server" />
    </div>
    <div class="float-right" style="padding: 0 60px 20px 20px; width: 390px">
        <uc1:ConfirmationCard ID="PropertyInformationConfirmationUserControl" HeaderText="Property Information" runat="server" />
        <uc1:ConfirmationCard ID="PaymentInformationConfirmationUserControl" HeaderText="Payment Information" runat="server" />
        <uc1:ConfirmationCard ID="CharityDonationConfirmationUserControl" HeaderText="Donation" runat="server" />
        <div class="accept-terms clear">
            <asp:CheckBox ClientIDMode="Static" ID="AcceptTermsCheckbox" runat="server" />
            <label for="AcceptTermsCheckbox"><a href="#" onclick='return showTerms();' style="text-decoration: underline;">Accept Terms of Agreement</a></label>
        </div>
    </div>
</div>
<div id="TermsDiv" style="display: none;">
    <asp:Literal ID="TermsAndConditions" runat="server" />
</div>    
<script>
    $(function () {
        $("#TermsDiv").dialog({  //create dialog, but keep it closed
            autoOpen: false,
            height: 800,
            width: 900,
            modal: true
        });
    });
    function showTerms() {
        $("#TermsDiv").dialog("open");
        return false;
    }
</script>