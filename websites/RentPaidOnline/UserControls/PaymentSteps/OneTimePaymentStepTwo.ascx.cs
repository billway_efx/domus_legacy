﻿using System;
using EfxFramework.Interfaces.UserControls;
using EfxFramework.Interfaces.UserControls.PaymentSteps;

namespace Domus.UserControls.PaymentSteps
{
    public partial class OneTimePaymentStepTwo : System.Web.UI.UserControl, IOneTimePaymentStepTwo
    {
        private EfxFramework.Presenters.UserControls.PaymentSteps.OneTimePaymentStepTwo _presenter;

        public bool TermsAccepted { get { return AcceptTermsCheckbox.Checked; } set { AcceptTermsCheckbox.Checked = value; } }
        public string TermsAndConditionsText { set { TermsAndConditions.Text = value; } }
        public IConfirmationCard RenterInformationConfirmation { get { return RenterInformationConfirmationUserControl; } }
        public IConfirmationCard PayerInformationConfirmation { get { return PayerInformationConfirmationUserControl; } }
        public IConfirmationCard LeasingAgentConfirmation { get { return LeasingAgentConfirmationUserControl; } }
        public IConfirmationCard PaymentInformationConfirmation { get { return PaymentInformationConfirmationUserControl; } }
        public IConfirmationCard PropertyInformationConfirmation { get { return PropertyInformationConfirmationUserControl; } }
        public IConfirmationCard CharityDonationConfirmation { get { return CharityDonationConfirmationUserControl; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter = new EfxFramework.Presenters.UserControls.PaymentSteps.OneTimePaymentStepTwo(this);

            if (!Page.IsPostBack)
                _presenter.InitializeValues();
        }
    }
}