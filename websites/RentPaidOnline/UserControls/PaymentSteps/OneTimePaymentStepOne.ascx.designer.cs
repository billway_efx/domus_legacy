﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Domus.UserControls.PaymentSteps {
    
    
    public partial class OneTimePaymentStepOne {
        
        /// <summary>
        /// RenterSelectionUserControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Domus.UserControls.Renter.RenterSelection RenterSelectionUserControl;
        
        /// <summary>
        /// PayerInformationUserControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Domus.UserControls.Payer.PayerInformation PayerInformationUserControl;
        
        /// <summary>
        /// PaymentMethodsUserControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Domus.UserControls.Payment.PaymentMethods PaymentMethodsUserControl;
        
        /// <summary>
        /// PaymentAmountUserControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Domus.UserControls.Payment.PaymentAmount PaymentAmountUserControl;
        
        /// <summary>
        /// CharityDonationUserControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Domus.UserControls.Payment.CharityDonation CharityDonationUserControl;
    }
}
