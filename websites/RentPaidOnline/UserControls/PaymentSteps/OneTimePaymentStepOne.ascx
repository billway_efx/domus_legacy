﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OneTimePaymentStepOne.ascx.cs" Inherits="Domus.UserControls.PaymentSteps.OneTimePaymentStepOne" %>

<uc1:RenterSelection ID="RenterSelectionUserControl" runat="server" />
<div class="float-right" style="width: 645px">
    <uc1:PayerInformation ID="PayerInformationUserControl" runat="server" />
    <uc1:PaymentMethods ID="PaymentMethodsUserControl" runat="server" />
    <uc1:PaymentAmount ID="PaymentAmountUserControl" runat="server" />
    <uc1:CharityDonation ID="CharityDonationUserControl" runat="server" />
</div>