﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mb2x.ExtensionMethods;
using System.Web;
using System.Collections.Generic;



namespace Domus.UserControls
{
    public partial class DatePickerStart : UserControl, IDatePicker
    {
        public int StartYear { get; set; }
        public int NumberOfYears { get; set; }
        public DropDownList MonthList { get { return MonthDropDown; } }
        public DropDownList DayList { get { return DayDropDown; } }
        public DropDownList YearList { get { return YearDropDown; } }

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!Page.IsPostBack)
            {

                //cakel: BUGID0008 added 
                int monthNum = MonthDropDown.SelectedValue.ToInt32();
                PopulateDayList(monthNum);
                string myrenter = Request.QueryString["renterId"].ToString();
                //cakel: BUGID0008 added 
                DLAccess.GetRenterDetail _renter = new DLAccess.GetRenterDetail();
                _renter = DLAccess.RenterDetails(myrenter);
                //cakel: BUGID0008 added 
                 DateTime RentStart = Convert.ToDateTime(_renter.LeaseStart);
                 int StartDay = RentStart.Day;
                 DayDropDown.SelectedValue = StartDay.ToString();
            }
        }


        public void SetYearList()
        {
            YearList.Items.Clear();

            var Year = DateTime.UtcNow.Year - 1;

            for (var I = 0; I < StartYear - Year; I++)
            {
                YearList.Items.Add(new ListItem((Year + I).ToString(CultureInfo.InvariantCulture), (Year + I).ToString(CultureInfo.InvariantCulture)));
            }

            for (var I = 0; I < NumberOfYears; I++)
            {
                YearList.Items.Add(new ListItem((StartYear + I).ToString(CultureInfo.InvariantCulture), (StartYear + I).ToString(CultureInfo.InvariantCulture)));
            }
        }

        public void SetDefaults()
        {
            MonthList.SelectedValue = DateTime.UtcNow.Month.ToString(CultureInfo.InvariantCulture);
            DayList.SelectedValue = DateTime.UtcNow.Day.ToString(CultureInfo.InvariantCulture);
            YearList.SelectedValue = DateTime.UtcNow.Year.ToString(CultureInfo.InvariantCulture);
        }

        public DateTime GetDate()
        {
            return YearList.Items.Count < 1 ? DateTime.UtcNow : new DateTime(YearList.SelectedValue.ToInt32(), MonthList.SelectedValue.ToInt32(), GetDay());
        }

        private int GetDay()
        {
            var Month = MonthList.SelectedValue.ToInt32();
            var Day = DayList.SelectedValue.ToInt32();

            switch (Month)
            {
                case 4:
                case 6:
                case 9:
                case 11:
                    if (Day > 30)
                        Day = 30;

                    break;
                case 2:
                    if (DateTime.IsLeapYear(YearList.SelectedValue.ToInt32()) && Day > 29)
                        Day = 29;
                    if (!DateTime.IsLeapYear(YearList.SelectedValue.ToInt32()) && Day > 28)
                        Day = 28;

                    break;
            }

            return Day;
        }

        //cakel: BUGID0008 added 
        // Gets the last day of month
        public int GetMaxMonthDay(int monthNum)
        {
            string _day = monthNum.ToString();
            string _Year = YearDropDown.SelectedValue;
            DateTime TheDate = Convert.ToDateTime(_day + "/" + "1" + "/" + _Year);
            TheDate = TheDate.AddMonths(1);
            TheDate = TheDate.AddDays(-1);
            string TheLastDay = TheDate.Day.ToString();
            return TheLastDay.ToInt32();
        }

        //cakel: BUGID0008 added 
        //Creates list of days based on month
        public void PopulateDayList(int MonthNumber)
        {
            int MaxDay = GetMaxMonthDay(MonthNumber);
            int dayint = 0;
            for (int i = 0; i < MaxDay; i++)
            {
                dayint += 1;

                DayDropDown.Items.Add(new ListItem(dayint.ToString(), dayint.ToString()));
            }

        }


        //cakel: BUGID0008 added 
        //Changes day list with new selection
        protected void MonthDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            int monthNum = MonthDropDown.SelectedValue.ToInt32();
            DayDropDown.Items.Clear();
            PopulateDayList(monthNum);
        }




    }
}