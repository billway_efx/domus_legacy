﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserEditor.ascx.cs" Inherits="Domus.UserControls.UserEditor" %>
<div class="main-container">
    <asp:PlaceHolder ID="successMsg" runat="server" Visible="false"><div class="alert alert-success" id="successDiv"><b>Success!</b> <asp:Literal runat="server" ID="message" /></div></asp:PlaceHolder>
    <div class="report-header open">
		<a href="#" class="report-nav"><span data-icon="&#x36" aria-hidden="true"></span><span class="default-nav">Select User Account Type</span></a>
		<ul class="report-list list-unstyled">
			<asp:PlaceHolder ID="RPOLink" runat="server" Visible="false"><li><a href="NewUser.aspx?type=0">RPO Administrator</a></li></asp:PlaceHolder>
			<li><a href="/staff.aspx">Property Staff</a></li>
			<li><a href="NewUser.aspx?type=2">Resident</a></li>
            <!--CMallory - Task 00495 - Added Link to Create New API User screen-->
            <li><a href="Default.aspx?ApiUserId=New">API User</a></li>
		</ul>
	</div>

    <asp:MultiView ID="AccountTypesView" runat="server">
        <asp:View ID="AdminView" runat="server">
			<uc1:AddEditEfxAdministrator ID="AddEditEfxAdministrator" runat="server" />
        </asp:View>
        <asp:View ID="PropertyStaffView" runat="server">
            <div class="formWrapper">
				<div class="formHalf">
					<asp:DropDownList CssClass="form-control" ID="StaffPropertyDropDown" runat="server" AutoPostBack="True" AppendDataBoundItems="True" DataTextField="PropertyName" DataValueField="PropertyId" OnSelectedIndexChanged="StaffPropertyDropDownSelectedIndexChanged">
						<asp:ListItem Value="0" Text="-- Select Property --"></asp:ListItem>
					</asp:DropDownList>
				</div>
			</div>
            <uc1:AddEditPropertyStaff ID="AddEditPropertyStaff" runat="server" ShowStaffSelection="False" Visible="False" />
        </asp:View>
        <asp:View ID="ResidentView" runat="server">
            <div class="formWrapper">
				<div class="formHalf">
					<asp:DropDownList ID="ResidentPropertyDropDown" CssClass="form-control" runat="server" AutoPostBack="True" AppendDataBoundItems="True" DataTextField="PropertyName" DataValueField="PropertyId" OnSelectedIndexChanged="ResidentPropertyDropDownSelectedIndexChanged">
						<asp:ListItem Value="0" Text="-- Select Property --"></asp:ListItem>
					</asp:DropDownList>
				</div>
				<div class="clear"></div>
			</div>
			<uc1:AddEditResident ID="AddEditResident" runat="server" Visible="False" />
        </asp:View>
    </asp:MultiView>
</div>

<script>
	$(function () {
		function getUrlVars() {
			var vars = [], hash;
			var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
			for (var i = 0; i < hashes.length; i++) {
				hash = hashes[i].split('=');
				vars.push(hash[0]);
				vars[hash[0]] = hash[1];
			}
			return vars;
		}
		var urlReport = 'Select User Account Type';
		switch (getUrlVars()['type']) {
			case '0':
				urlReport = 'RPO Administrator';
				break;
			case '1':
				urlReport = 'Property Staff';
				break;
			case '2':
				urlReport = 'Resident';
				break;
			default:
				urlReport = 'Select User Account Type';
		}

		var $reportHeader = $('.report-header'),
			$reportUl = $('ul.report-list'),
			$reportIcon = $('a.report-nav span').eq(0),
			$reportNav = $('a.report-nav span').eq(1);

		function selectReport(reportText) {
			var currentReport = reportText;

			if (!(reportText == 'Select User Account Type')) {
				$reportUl.hide();
				$reportHeader.addClass('selected').removeClass('open');
				$reportIcon.addClass('back');
				$reportNav.html(currentReport);
			} else {
				deselectReport();
			}
		}
		function deselectReport() {
			$reportHeader.removeClass('selected').addClass('open');
			$reportIcon.removeClass('back');
			$reportNav.html('Select User Account Type');
			$reportUl.show();
		}

		selectReport(urlReport);
		$('a.report-nav').click(function (e) {
			deselectReport();
			e.preventDefault();
		});

    	/* Mask */
    	ApplySingleMask("#<%=AddEditResident.MainPhoneClientId%>", "(000) 000-0000");
    	ApplySingleMask("#<%=AddEditResident.MobilePhoneClientId%>", "(000) 000-0000");
    	ApplySingleMask("#<%=AddEditResident.FaxPhoneClientId%>", "(000) 000-0000");
    	ApplySingleMask("#<%=AddEditResident.ZipCodeClientId%>", "00000");
    	ApplySingleMask("#<%=AddEditPropertyStaff.OfficePhoneClientId%>", "(000) 000-0000");
    	ApplySingleMask("#<%=AddEditPropertyStaff.MobilePhoneClientId%>", "(000) 000-0000");
    	ApplySingleMask("#<%=AddEditPropertyStaff.FaxPhoneClientId%>", "(000) 000-0000");
    	ApplySingleMask("#<%=AddEditPropertyStaff.ZipCodeClientId%>", "00000");

	});
</script>