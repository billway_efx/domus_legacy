﻿using System;
using System.Globalization;
using System.Web;
using System.Web.UI;
using Mb2x.ExtensionMethods;
using System.Collections.Generic;
using System.Linq;

namespace Domus.UserControls
{
   public partial class UserEditor : UserControl
   {
      public int AdminId
      {
         get
         {
            var Id = HttpContext.Current.Request.QueryString["efxAdministratorId"].ToNullInt32();

            if (Id.HasValue && Id.Value > 0)
               return Id.Value;

            return 0;
         }
      }

      public int ResidentId
      {
         get
         {
            var Id = HttpContext.Current.Request.QueryString["residentId"].ToNullInt32();

            if (Id.HasValue && Id.Value > 0)
               return Id.Value;

            return 0;
         }
      }

      public int PropertyStaffId
      {
         get
         {
            var Id = HttpContext.Current.Request.QueryString["propertyStaffId"].ToNullInt32();

            if (Id.HasValue && Id.Value > 0)
               return Id.Value;

            return 0;
         }
      }

      public int PropertyId
      {
         get
         {
            var Id = HttpContext.Current.Request.QueryString["propertyId"].ToNullInt32();

            if (Id.HasValue && Id.Value > 0)
               return Id.Value;

            return 0;
         }
      }

      public int NewType
      {
         get
         {
            var Id = HttpContext.Current.Request.QueryString["type"].ToNullInt32();

            if (Id.HasValue)
               return Id.Value;

            return -1;
         }
      }

      protected void Page_Load(object sender, EventArgs e)
      {
         if (EfxFramework.Helpers.Helper.IsRpoAdmin)
            RPOLink.Visible = true;

         SetUserControls();
         SetDropDowns();
      }

      private void SetUserControls()
      {
         if (PropertyId > 0)
         {
            AddEditPropertyStaff.PropertyId = PropertyId;
            AddEditResident.PropertyId = PropertyId;
         }
         else
         {
            AddEditPropertyStaff.PropertyId = StaffPropertyDropDown.SelectedValue.ToInt32();
            AddEditResident.PropertyId = ResidentPropertyDropDown.SelectedValue.ToInt32();
         }

         if (PropertyStaffId > 0)
         {
            //AccountTypeDropdownList.SelectedValue = "2";
            AddEditPropertyStaff.PropertyStaffId = PropertyStaffId;
            SetView(1);
         }
         else if (ResidentId > 0)
         {
            //AccountTypeDropdownList.SelectedValue = "3";
            AddEditResident.ResidentId = ResidentId;
            SetView(2);
         }
         else if (AdminId > 0)
         {
            //AccountTypeDropdownList.SelectedValue = "1";
            AddEditEfxAdministrator.EfxAdministratorId = AdminId;
            SetView(0);
         }
         else if (NewType >= 0)
         {
            AccountTypesView.ActiveViewIndex = NewType;
         }
      }

      private void SetDropDowns()
      {
         if (!Page.IsPostBack)
         {
            List<EfxFramework.Property> AllProperties = new List<EfxFramework.Property>();
            if (EfxFramework.Helpers.Helper.IsRpoAdmin || EfxFramework.Helpers.Helper.IsCsrUser)
               AllProperties = EfxFramework.Property.GetAllPropertyList();
            else
               AllProperties = EfxFramework.Property.GetPropertyListByPropertyStaffId(EfxFramework.Helpers.Helper.CurrentUserId);
            StaffPropertyDropDown.DataSource = AllProperties;
            StaffPropertyDropDown.DataBind();
            //cakel: BUGID000198 - Added Order by 
            //ResidentPropertyDropDown.DataSource = AllProperties.Where(p => !p.PmsTypeId.HasValue && string.IsNullOrEmpty(p.PmsId))
            //ResidentPropertyDropDown.DataSource = AllProperties.Where(p => !p.PmsTypeId.HasValue && string.IsNullOrEmpty(p.PmsId)).OrderBy(p => p.PropertyName);
            ResidentPropertyDropDown.DataSource = AllProperties;
            ResidentPropertyDropDown.DataBind();
         }

         if (PropertyId < 1)
            return;

         StaffPropertyDropDown.SelectedValue = PropertyId.ToString(CultureInfo.InvariantCulture);
         ResidentPropertyDropDown.SelectedValue = PropertyId.ToString(CultureInfo.InvariantCulture);
      }

      //protected void AccountTypeDropdownListSelectedIndexChanged(object sender, EventArgs e)
      //{
      //	AccountTypesView.ActiveViewIndex = AccountTypeDropdownList.SelectedValue.ToInt32() - 1;
      //}

      protected void SetView(int view)
      {
         AccountTypesView.ActiveViewIndex = view;
      }

      protected void StaffPropertyDropDownSelectedIndexChanged(object sender, EventArgs e)
      {
         if (StaffPropertyDropDown.SelectedValue.ToInt32() > 0)
         {
            AddEditPropertyStaff.Visible = true;
            AddEditPropertyStaff.PropertyId = StaffPropertyDropDown.SelectedValue.ToInt32();
            AddEditPropertyStaff.PopulateData();
         }
         else
         {
            AddEditPropertyStaff.Visible = false;
         }
      }

      protected void ResidentPropertyDropDownSelectedIndexChanged(object sender, EventArgs e)
      {
         if (ResidentPropertyDropDown.SelectedValue.ToInt32() > 0)
         {
            AddEditResident.Visible = true;
            AddEditResident.PropertyId = ResidentPropertyDropDown.SelectedValue.ToInt32();
            AddEditResident.PopulateData();
         }
         else
         {
            AddEditResident.Visible = false;
         }
      }

   }
}