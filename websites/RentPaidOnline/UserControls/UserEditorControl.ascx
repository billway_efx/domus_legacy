﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserEditorControl.ascx.cs" Inherits="RentPaidOnline.UserControls.UserEditorControl" %>
<div class="green-gradient-wrap padding">
        <div class="profile-information">
            <div class="labels">
                <label class="fn">First Name</label>
                <label class="ln">Last Name</label>
                <label class="mn">Middle Name</label>
            </div>
            <div class="full-name-textboxes">
                <asp:TextBox ClientIDMode="Static" ID="FirstNameTextbox" runat="server" CssClass="fn-textbox textbox focus" />
                <asp:TextBox ClientIDMode="Static" ID="LastNameTextbox" runat="server" CssClass="ln-textbox textbox" />
                <asp:TextBox ClientIDMode="Static" ID="MiddleNameTextbox" runat="server" CssClass="mn-textbox textbox" />
            </div>
            <div class="clear">
                <label class="address">Address</label><br />
                <asp:TextBox ClientIDMode="Static" ID="AddressTextbox" CssClass="textbox address-textbox" runat="server"></asp:TextBox>
                <asp:TextBox ClientIDMode="Static" ID="AddressLine2Textbox" CssClass="textbox address-textbox" runat="server"></asp:TextBox>
            </div>
            <div class="locale-labels">
                <label class="city">City</label>
                <label class="state">State</label>
                <label class="zip">Zip</label>
            </div>
            <div class="locale-textboxes clear">
                <asp:TextBox ClientIDMode="Static" ID="CityTextbox" runat="server" CssClass="textbox city-textbox" />
                <asp:DropDownList ID="StateDropdown" ClientIDMode="Static" runat="server" CssClass="state-dropdown"></asp:DropDownList>
                <asp:TextBox ClientIDMode="Static" ID="ZipTextbox" runat="server" CssClass="textbox zip-textbox" />
            </div>
            <div class="phone-labels">
                <label class="home-phone">Phone</label>
                <label class="mobile-phone">Mobile</label>
                <label class="fax-phone">Fax</label>
            </div>
            <div class="phone-textboxes padding-bottom">
                <asp:TextBox ClientIDMode="Static" ID="HomePhoneTextbox" runat="server" CssClass="home-phone-textbox textbox" />
                <asp:TextBox ClientIDMode="Static" ID="CellPhoneTextbox" runat="server" CssClass="mobile-phone-textbox textbox" />
                <asp:TextBox ClientIDMode="Static" ID="FaxTextbox" runat="server" CssClass="fax-phone-textbox textbox" />
            </div>
            <div class="email-labels">
                <label class="email1">Email Address</label>
            </div>
            <div class="email-textboxes padding-bottom">
                <asp:TextBox ClientIDMode="Static" ID="Email1Textbox" runat="server" CssClass="textbox" />
            </div>
            <div class="labels">
                <label class="photo">Photo</label>
            </div>
            <div class="textboxes padding-bottom">
                <asp:Image ID="Image1" runat="server" CssClass="float-left" />
                <asp:FileUpload ID="PhotoUpload1" runat="server" CssClass="photo-upload" />
            </div>
        </div>
        <hr class="clear" />
        <h2>Log In Credentials</h2>
        <div class="float-wrap">
            <div class="float-left" style="width: 300px;">
                <asp:UpdatePanel ID="AccountTypeUpdatePanel" runat="server">
                    <ContentTemplate>
                        <p>
                            <label>Account Type</label>
                        </p>
                        <p>
                            <asp:DropDownList ID="AccountTypeDropdownList" runat="server" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="AccountTypeDropdownList_SelectedIndexChanged">
                            </asp:DropDownList>
                        </p>
                        <asp:Panel ID="PropertySelectPanel" runat="server" Visible="false">
                            <p>
                                <label>Property</label>
                            </p>
                            <p>
                                <asp:DropDownList ID="PropertyDropdownList" runat="server" Width="200px"></asp:DropDownList>
                            </p>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <p>
                    <label class="un">Username</label>
                </p>
                <p>
                    <asp:TextBox ID="UserNameTextbox" runat="server" CssClass="username-textbox textbox" AutoCompleteType="None"></asp:TextBox>
                </p>
                <p>
                    <label class="pw">Password</label>
                </p>
                <p>
                    <asp:TextBox ID="PasswordTextbox" TextMode="Password" runat="server" CssClass="password-textbox textbox" AutoCompleteType="None"></asp:TextBox>
                </p>
                <p>
                    <label class="pw">Confirm Password</label>
                </p>
                <p>
                    <asp:TextBox ID="ConfirmPasswordTextbox" TextMode="Password" runat="server" CssClass="password-textbox textbox" AutoCompleteType="None"></asp:TextBox>
                </p>
            </div>
            <div class="float-left default-font" style="width: 295px; margin-left: 30px;">
                <asp:CheckBox ID="CanViewPropertyDetailCheckbox" runat="server" Text="Can View Property Detail" TextAlign="Left" />
                <asp:CheckBox ID="InTheGenManagerCheckbox" runat="server" Text="Is the General Manager of the Property" TextAlign="Left" />
                <asp:CheckBox ID="CanCreateSalesAccountsCheckbox" runat="server" Text="Can Create New Sales Staff Accounts" TextAlign="Left" />
                <asp:CheckBox ID="CanViewSalesStaffAccountsCheckbox" runat="server" Text="Can View Sales Staff Accounts" TextAlign="Left" />
                <asp:CheckBox ID="CanCreateNewRenterAccountsCheckbox" runat="server" Text="Can Create New Renter Accounts" TextAlign="Left" />
                <asp:CheckBox ID="CanViewRenterDetailedInformationCheckbox" runat="server" Text="Can View Renter Detailed Information" TextAlign="Left" />
                <asp:CheckBox ID="CanSendOutBulkEmailCheckbox" runat="server" Text="Can Send Out Bulk Email" TextAlign="Left" />
            </div>
        </div>
        <div class="buttons float-wrap padding">
            <a href="/Users" class="green-button float-right margin-right" id="AllUsersLink" runat="server">View All Users</a>
            <asp:Button ID="AddAnotherUserButton" runat="server" CssClass="green-button float-right margin-right" Text="Save and Add Another User" OnClick="AddAnotherUserButton_Click" />
            <asp:Button ID="SaveButton" runat="server" CssClass="green-button float-right margin-right" Text="Save" OnClick="SaveButton_Click" />
        </div>
    </div>
