﻿using EfxFramework.Interfaces.UserControls.Property;
using System;

namespace Domus.UserControls
{
    public partial class RealPageAddEdit : System.Web.UI.UserControl, IRealPageAddEdit
    {

        public EfxFramework.Presenters.UserControls.Property.RealPageAddEdit Presenter { get; set; }

        //cakel: 00351 RealPage Integration      
        public int PropertyId { get; set; }
        public string UsernameText { get { return RealPageUsernameText.Text; } set { RealPageUsernameText.Text = value; } }
        public string PasswordText { get { return RealPagePasswordText.Text; } set { RealPagePasswordText.Text = value; } }
        public string RealPagePMCID { get { return RealPagePmcIdText.Text; } set { RealPagePmcIdText.Text = value; } }
        public string RealPageImportEndPoint { get { return RealPageImportUrlText.Text; } set { RealPageImportUrlText.Text = value; } }
        public string RealPageExportEndPoint { get { return RealPageExportUrlText.Text; } set { RealPageExportUrlText.Text = value; } }
        public string RealPageInternalUser { get { return RealPageInternalUserText.Text; } set { RealPageInternalUserText.Text = value; } }
        public string RealPageSiteID { get { return RealPageSiteIDText.Text; } set { RealPageSiteIDText.Text = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            Presenter = new EfxFramework.Presenters.UserControls.Property.RealPageAddEdit(this);

            if (!Page.IsPostBack)
                Presenter.InitializeValues();

        }
    }
}