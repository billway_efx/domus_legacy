﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MRIAddEdit.ascx.cs" Inherits="Domus.UserControls.MRIAddEdit" %>

<div class="formWhole">
    <div class="formTwoThirds" style="margin-top:14px;">
        <div class="formTwoThirds">
            <asp:CheckBox ID="cbEnableFtpUpload" Text="&nbsp;&nbsp;Enable FTP Upload for property file import" runat="server" Style="margin-left:20px;" />
        </div>

        <div class="formTwoThirds" style="padding-left: 0px;">
            <asp:CheckBox ID="cbEnableFtpDownload" Text="&nbsp;&nbsp;Enable FTP Download for receipts file export" runat="server" Style="margin-left:20px;" />
        </div>
    </div>
    <div class="formThird" style="padding-top: 14px;">
        <label>MRI Property ID</label>
        <asp:TextBox ID="txtMRIPropertyId" runat="server" CssClass="form-control" />
    </div>
</div>

<div class="formWhole"  runat="server" id="MRIEfxAdminDiv" visible="false">
    <label>FTP Details:</label>
    
        <div class="formTwoThirds" style="padding-left: 3%; padding-top: 12px;">
            <div class="formThird">
                <label>Site Address</label>
                <asp:TextBox ID="txtSiteAddress" runat="server" CssClass="form-control" />
            </div>
            <div class="formThird">
                <label>Port</label>
                <asp:TextBox ID="txtPort" runat="server" CssClass="form-control" />
            </div>
        </div>

        <div class="formTwoThirds" style="padding-left: 3%;">
            <div class="formThird">
                <label>Account User Name</label>
                <asp:TextBox ID="txtAccountUserName" runat="server" CssClass="form-control" />
            </div>
            <div class="formThird">
                <label>Account Password</label>
                <asp:TextBox ID="txtAccountPassword" runat="server" CssClass="form-control" />
            </div>
        </div>
</div>

