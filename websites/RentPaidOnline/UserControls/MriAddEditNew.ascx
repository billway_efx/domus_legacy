﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MriAddEditNew.ascx.cs" Inherits="Domus.UserControls.MriAddEditNew" %>


<%-- cakel: 00382 - MRI Integration --%>
<div class="formWhole">
    <div class="formThird">
        <label>MRI ClientID</label>
        <asp:TextBox ID="MRIClientIDText" runat="server" CssClass="form-control" />
    </div>
    <div class="formThird">
        <label>MRI Database Name</label>
        <asp:TextBox ID="MRIDatabseNameText" runat="server" CssClass="form-control" />
    </div>

</div>


<div class="formWhole">

    <div class="formThird">
        <label>MRI Webservice UserName</label>
        <asp:TextBox ID="MRIWebserviceUserNameText" runat="server" CssClass="form-control" />

    </div>
    <div class="formThird">
        <label>MRI Webservice Password</label>
        <asp:TextBox ID="MRIWebservicePasswordText" runat="server" CssClass="form-control" />

    </div>
    <div class="formThird">
        <label>MRI Partner Key</label>
        <asp:TextBox ID="MRIParnterKeyText" runat="server" CssClass="form-control" />
    </div>



</div>
<div class="formWhole">

        <label>MRI Import End Point</label>
        <asp:TextBox ID="MRIImportEndPointText" runat="server" CssClass="form-control" ></asp:TextBox>

</div>
<div class="formWhole">
     <label>MRI Export End Point</label>
        <asp:TextBox ID="MRIExportEndPointText" runat="server" CssClass="form-control" ></asp:TextBox>
</div>