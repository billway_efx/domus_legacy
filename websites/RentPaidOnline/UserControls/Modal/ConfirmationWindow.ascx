﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConfirmationWindow.ascx.cs" Inherits="Domus.UserControls.Modal.ConfirmationWindow" %>
<div class="modalContentContainer confirmationContainer">
    <h4 class="confirmTitleText"><asp:Label ID="ConfirmationHeadingLabel" runat="server" /></h4>
    <p><asp:Label ID="ConfirmationLabel" runat="server" /></p>
    <div class="modalButtonHolderCentered">
        <asp:Button ID="OkButton" runat="server" Text="Okay" CssClass="orangeButton okayButton" />
    </div>
    <div class="clearFix"></div>
</div>
