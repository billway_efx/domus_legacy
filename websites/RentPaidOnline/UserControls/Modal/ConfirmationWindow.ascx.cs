﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Web.UI;

namespace Domus.UserControls.Modal
{
    public partial class ConfirmationWindow : UserControl, IConfirmationWindow
    {
        private ConfirmationWindowPresenter _Presenter;

        public string ConfirmationHeaderText { set { ConfirmationHeadingLabel.Text = value; } }
        public string ConfirmationLabelText { set { ConfirmationLabel.Text = value; } }
        public string LinkUrl { set { OkButton.PostBackUrl = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new ConfirmationWindowPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}