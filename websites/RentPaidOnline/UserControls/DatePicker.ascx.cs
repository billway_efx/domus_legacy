﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mb2x.ExtensionMethods;

namespace Domus.UserControls
{
    public partial class DatePicker : UserControl, IDatePicker
    {
        public int StartYear { get; set; }
        public int NumberOfYears { get; set; }
        public DropDownList MonthList { get { return MonthDropDown; } }
        public DropDownList DayList { get { return DayDropDown; } }
        public DropDownList YearList { get { return YearDropDown; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public void SetYearList()
        {
            YearList.Items.Clear();

            var Year = DateTime.UtcNow.Year - 1;

            for (var I = 0; I < StartYear - Year; I++)
            {
                YearList.Items.Add(new ListItem((Year + I).ToString(CultureInfo.InvariantCulture), (Year + I).ToString(CultureInfo.InvariantCulture)));
            }

            for (var I = 0; I < NumberOfYears; I++)
            {
                YearList.Items.Add(new ListItem((StartYear + I).ToString(CultureInfo.InvariantCulture), (StartYear + I).ToString(CultureInfo.InvariantCulture)));
            }
        }

        public void SetDefaults()
        {
            MonthList.SelectedValue = DateTime.UtcNow.Month.ToString(CultureInfo.InvariantCulture);
            DayList.SelectedValue = DateTime.UtcNow.Day.ToString(CultureInfo.InvariantCulture);
            YearList.SelectedValue = DateTime.UtcNow.Year.ToString(CultureInfo.InvariantCulture);
        }

        public DateTime GetDate()
        {
            return YearList.Items.Count < 1 ? DateTime.UtcNow : new DateTime(YearList.SelectedValue.ToInt32(), MonthList.SelectedValue.ToInt32(), GetDay());
        }

        private int GetDay()
        {
            var Month = MonthList.SelectedValue.ToInt32();
            var Day = DayList.SelectedValue.ToInt32();

            switch (Month)
            {
                case 4:
                case 6:
                case 9:
                case 11:
                    if (Day > 30)
                        Day = 30;

                    break;
                case 2:
                    if (DateTime.IsLeapYear(YearList.SelectedValue.ToInt32()) && Day > 29)
                        Day = 29;
                    if (!DateTime.IsLeapYear(YearList.SelectedValue.ToInt32()) && Day > 28)
                        Day = 28;

                    break;
            }

            return Day;
        }
    }
}