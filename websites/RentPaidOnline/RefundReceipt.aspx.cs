﻿using EfxFramework;
using EfxFramework.ExtensionMethods;
using EfxFramework.PaymentMethods;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Web;

namespace Domus
{
    public partial class RefundReceipt : System.Web.UI.Page
    {
        private int ReversalId
        {
            get
            {
                var Id = HttpContext.Current.Request["TransactionReversalId"].ToNullInt32();
                if (!Id.HasValue)
                {
                    BasePage.SafePageRedirect("~/Renters/Default.aspx");
                    return 0;
                }

                return Id.Value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var Reversal = new TransactionReversal(ReversalId);
            if (Reversal.TransactionReversalId < 1)
            {
                BasePage.SafePageRedirect("~/Renters/Default.aspx");
                return;
            }

            var Transaction = EfxFramework.Transaction.GetTransactionByInternalTransactionId(Reversal.InternalTransactionId);
            if (Transaction.TransactionId < 1)
            {
                BasePage.SafePageRedirect("~/Renters/Default.aspx");
                return;
            }

            var Payment = new Payment(Transaction.PaymentId);
            if (Payment.PaymentId < 1)
            {
                BasePage.SafePageRedirect("~/Renters/Default.aspx");
                return;
            }

            var Resident = new Renter(Payment.RenterId);
            if (Resident.RenterId < 1)
            {
                BasePage.SafePageRedirect("~/Renters/Default.aspx");
                return;
            }

            var Property = EfxFramework.Property.GetPropertyByRenterId(Resident.RenterId);
            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                BasePage.SafePageRedirect("~/Renters/Default.aspx");
                return;
            }

            BuildView(Reversal, Transaction, Payment, Resident, Property);
        }

        private void BuildView(TransactionReversal reversal, Transaction transaction, Payment payment, Renter resident, Property property)
        {
            var UseBillingAddress = false;

            var Address = EfxFramework.Address.GetAddressByRenterIdAndAddressType(resident.RenterId, (int) TypeOfAddress.Billing);
            if (Address.AddressId < 1)
                UseBillingAddress = true;

            TransactionIdLabel.Text = transaction.InternalTransactionId;
            RefundAmountLabel.Text = reversal.ReversalAmount.ToString("C");
            PropertyNameLabel.Text = property.PropertyName;
            PropertyAddressLabel.Text = property.DisplayAddress;
            FirstNameLabel.Text = resident.FirstName;
            MiddleNameLabel.Text = resident.MiddleName;
            LastNameLabel.Text = resident.LastName;
            SuffixLabel.Text = resident.Suffix;

            if (UseBillingAddress)
            {
                BillingAddressLabel.Text = String.Format("{0} {1}", Address.AddressLine1, Address.AddressLine2);
                BillingCityStateZipLabel.Text = String.Format("{0}, {1} {2}", Address.City, Address.GetStateAbbeviation(), Address.PostalCode);
            }
            else
            {
                var State = resident.StateProvinceId.HasValue ? ((StateProvince) resident.StateProvinceId.Value).ToString() : "N/A";
                BillingAddressLabel.Text = String.Format("{0} {1}", resident.StreetAddress, resident.Unit);
                BillingCityStateZipLabel.Text = String.Format("{0}, {1}, {2}", resident.City, State, resident.PostalCode);
            }

            PhoneNumberLabel.Text = resident.MainPhoneNumber;
            PaymentMethodLabel.Text = ((PaymentType) payment.PaymentTypeId).GetFriendlyName();
        }
    }
}