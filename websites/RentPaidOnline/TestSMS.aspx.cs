﻿using System;
using System.Net.Mail;
using EfxFramework;

namespace RentPaidOnline
{
    public partial class TestSms : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            const int renterId = 0;
            var renter = new EfxFramework.Renter(renterId);
            var request = new EfxFramework.Sms.PaymentRequest("6314046561", "PayMyRent", "1383", "Verizon");
            request.ProcessPayment();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            var user = Page.User.Identity.Name;
            userNamelbl.Text = user;
        }

        protected void Button3_Click(object sender, EventArgs e)
        {

            var msg = new MailMessage();
            msg.To.Add(new MailAddress("aorr@efxfs.com", "Ashleigh Nelson"));
            msg.From = new MailAddress("DomusSupport@domusme.com", "aorr@efxfs.com");
            msg.ReplyToList.Add(new MailAddress("aorr@efxfs.com"));
            msg.Subject = "This is a Test Mail";
            msg.Body = "This is a test message with new settings for the wonderful rent payment site.";
            msg.IsBodyHtml = true;

            var userName = EfxSettings.UserName365;
            var password = EfxSettings.password365;

            var client = new SmtpClient
            {
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(userName, password),
                Port = 587,
                Host = "smtp.office365.com",
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true
            };
            // You can use Port 25 if 587 is blocked (mine is!)
            try
            {
                client.Send(msg);
                lblText.Text = @"Message Sent Successfully";
            }
            catch (Exception ex)
            {
                lblText.Text = ex.ToString();
            }
        }
    }
}