﻿<%@ Page Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="rpo-search.aspx.cs" Inherits="Domus.rpo_search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TopHeadline" runat="server">
    <h1 id="H1" ng-controller="TitleController" ng-bind="PageTitle"></h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="main-container">
        <div ng-include="'/search-filters.html'" id="searchFilters"></div>
        <div ng-view></div>
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="Scripts" runat="server">
    <script src="<%= ResolveClientUrl("~/Scripts/promise-tracker.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/rpo-search.js") %>"></script>
</asp:Content>
