﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Text;
using EfxFramework.Scheduler;
using System.Net;
using System.Net.Mail;
using EfxFramework;
using EfxFramework.Pms.Yardi;
using Domus;
using EfxFramework.Pms.AMSI;



namespace Domus
{
    public class AmsiRenterDetails
    {
        public string FirstName;
        public string Lastname;
        public string MiddleName;
        public string MainPhoneNumber;
        public string MobilePhone;
        public string PrimaryEmailAddress;
        public string UnitNumber;
        public string AMSIOccuSeqno;
        public string PmsID;
    }

    public class AmsiRenterAddress
    {
        public string PmsID;
        public string UnitNumber;
        public string address;
        public string city;
        public string state;
        public string zip;
    }


    public partial class X_NewXmlTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // Response.Redirect("~/Default.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {


            //ImportAMSI_Test("Domus", "rentpaid", "Domus");

            string GetProperty = "GetPropertyList";
            string GetResidents = "GetPropertyResidents";

            string MyTestString = WebServiceCall(GetProperty);
            TextBox1.Text = MyTestString;

        }




        private static void ImportAMSI_Test(string UserName, string Password, string Portfolio)
        {
            string endpoint2 = "https://amsitest.infor.com/AmsiWeb/edexweb/esite/leasing.asmx?op=GetPropertyList";
            string endpoint = "https://amsitest.infor.com/AmsiWeb/edexweb/esite/leasing.asmx";

            var Sb = new StringBuilder();
            Sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            Sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope\">");
            Sb.Append("<soap:Body>");
            Sb.Append("<GetPropertyList xmlns=\"http://tempuri.org\">");
            Sb.AppendFormat("<UserName>{0}</UserName>", UserName);
            Sb.AppendFormat("<Password>{0}</Password>", Password);
            Sb.AppendFormat("<PortfolioName>{0}</PortfolioName>", Portfolio);
            Sb.Append("<XMLData></XMLData>");
            Sb.Append("</GetPropertyList>");
            Sb.Append("</soap:Body>");
            Sb.Append("</soap:Envelope>");

           string test =  Sb.ToString();

           //<EDEX><propertyid></propertyid><includemarketingsources>1</includemarketingsources><includeamenities>0</includeamenities><includeunittypes>0</includeunittypes><includememo></includememo></EDEX>
            //Create the WebClient
            using (var Client = new WebClient())
            {
                var InputRequest = Encoding.UTF8.GetBytes(test);

                //Set the content-type header for the request
                Client.Headers[HttpRequestHeader.Host] = "amsitest.infor.com";
                Client.Headers[HttpRequestHeader.ContentType] = "text/xml; charset=utf-8";
                
                Client.Headers.Add("SOAPAction", "\"http://tempuri.org/GetPropertyList\"");
                
              

                try
                {

                    var RawResponse = Client.UploadData(endpoint2, "POST", InputRequest);
                    string stringtest = RawResponse.ToString();
                   
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        } // End ImportAMSI_Test




        public static string MyXmlString()
        {
           // string _PropertyID = "001";
            
            string myString =
            @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"">
                <soap:Body>
                 <GetPropertyList xmlns=""http://tempuri.org/"">
                     <UserID>Domus</UserID>
                     <Password>rentpaid</Password>
                     <PortfolioName>Domus</PortfolioName>
                     <XMLData>
                    <![CDATA[<edex> 
                     <propertyid></propertyid> 
                        <includemarketingsources>0</includemarketingsources>
                        <includeamenities>0</includeamenities> 
                        <includeunittypes>0</includeunittypes>
                        <includememo></includememo>
                     </edex>]]> 
                    </XMLData>
                     </GetPropertyList>
                </soap:Body>
            </soap:Envelope>";

            //myString = myString.Replace("##ID", _PropertyID);

            return myString;

        }

        private static string WebServiceCall(string _MethodName)
        {
            string methodName = _MethodName;
            string _xml = MyXmlString();
            //string _xml = GetResidents();


            WebRequest webRequest = WebRequest.Create("https://amsitest.infor.com/AmsiWeb/edexweb/esite/leasing.asmx");
            HttpWebRequest httpRequest = (HttpWebRequest)webRequest;
            httpRequest.Method = "POST";
            httpRequest.ContentType = "text/xml; charset=utf-8";
            httpRequest.Headers.Add("SOAPAction: http://tempuri.org/" + methodName);
            Stream requestStream = httpRequest.GetRequestStream();
            //Create Stream and Complete Request             
            StreamWriter streamWriter = new StreamWriter(requestStream, Encoding.ASCII);

            streamWriter.Write(_xml);
            streamWriter.Close();
            //Get the Response    
            HttpWebResponse wr = (HttpWebResponse)httpRequest.GetResponse();
            string testresponse = wr.ToString();
            StreamReader srd = new StreamReader(wr.GetResponseStream());
            string resulXmlFromWebService = srd.ReadToEnd();
            //cakel: Testing - returning string as xml formated
            string decodedString = HttpUtility.HtmlDecode(resulXmlFromWebService);
            return decodedString;
            //return resulXmlFromWebService;
        }


        protected void Method2btn_Click(object sender, System.EventArgs e)
        {

            Domus.AMSIReference.LeasingSoapClient Test = new Domus.AMSIReference.LeasingSoapClient();
            string _new = Test.GetPropertyList("Domus", "rentpaid", "Domus", "").ToString();
            string _new2 = _new;
        }

        protected void Button2_Click(object sender, System.EventArgs e)
        {
            string GetProperty = "GetPropertyList";
            string GetResidents = "GetPropertyResidents";


            string _xml = WebServiceCall(GetProperty);

            TryToReadXMLForProperties(_xml);
            //TryToReadXMLForResidents(_xml);

        }


        public void TryToReadXMLForProperties(string _xml)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(_xml);
            using (var OutputStream = new MemoryStream(byteArray))
            {
                var Xml = XDocument.Load(OutputStream);
                var Element = YardiCommon.GetRootElement(Xml.Root, "Properties");




                foreach (XElement _Property in Xml.Elements())
                {


                }


            }

        }//End TryToReadXML


        //XMl for Residents
        public void TryToReadXMLForResidents(string _xml)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(_xml);
            using (var OutputStream = new MemoryStream(byteArray))
            {
                var Xml = XDocument.Load(OutputStream);
                var Element = YardiCommon.GetRootElement(Xml.Root, "PropertyResidents");

                //string _LeaseID = "";
                //string ResidentID = "";
                //string firstname = "";
                //string lastname = "";
                //string OccuSeqNo = "";
                //string _OccuAddress = "";
                //string _openItemTest = "";


                
                //Start Renter Detail
                foreach (XElement _Lease in Element.Elements())
                {

                    var _details = new AmsiRenterDetails();

                    var Node1 = _Lease.Name.LocalName;
                    foreach (XElement LeaseNodes in _Lease.Elements())
                    {
                        var NodeType = LeaseNodes.Name.LocalName;
                        
                            switch (NodeType)
                            {
                               
                                case "Occupant":
                                    string RespFlag = LeaseNodes.Attribute("ResponsibleFlag").Value.ToString();
                                    if (RespFlag == "Responsible")
                                    {
                                        

                                    }
                                    break;

                                    //Insert Renter Detail into table

                            }

                    }

                }

            } //End Using
            

        }//End TryToReadXML



        public static string GetResidents()
        {
            string _propertyID = "001";

            string myString =
            @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"">
                <soap:Body>
                 <GetPropertyResidents xmlns=""http://tempuri.org/"">
                     <UserID>Domus</UserID>
                     <Password>rentpaid</Password>
                     <PortfolioName>Domus</PortfolioName>
                     <XMLData>
                    <![CDATA[<EDEX>
                    <propertyid>##ID</propertyid>
                    <bldgid></bldgid>
                    <unitid></unitid>
                    <Resiid></Resiid>
                    <occuseqno></occuseqno>
                    <leasestatus></leasestatus>
                    <includeprimaryaddress>1</includeprimaryaddress> 
                    <includemailingaddress>0</includemailingaddress>
                    <includebillingaddress>0</includebillingaddress>
                    <includestatementaddress>0</includestatementaddress> 
                    <includeotheraddress>0</includeotheraddress> 
                    <includecontactdetails>1</includecontactdetails>
                    <includerecurringcharges>1</includerecurringcharges> 
                    <includememo></includememo> 
                    <includeoccupantmemo></includeoccupantmemo>
                    <includerenewals>0</includerenewals> 
                    <renewalcompletionstatus>1</renewalcompletionstatus>
                    <sodacompleteddatefrom></sodacompleteddatefrom> 
                    <sodacompleteddatethru></sodacompleteddatethru> 
                    <hidedob>0</hidedob> 
                    </EDEX>]]> 
                    </XMLData>
                     </GetPropertyResidents>
                </soap:Body>
            </soap:Envelope>";

            myString = myString.Replace("##ID", _propertyID);

            return myString;
        }



        protected void Button3_Click(object sender, System.EventArgs e)
        {
            //string amsiPropertyID = "003";
            string amsiPropertyID = "5054";

                AmsiRequest.AMSI_RequestXmlForRenterImport(1, amsiPropertyID);

            }

        protected void Button4_Click(object sender, System.EventArgs e)
        {
            int RenterID = 2137;
            AmsiRequest.AMSI_RequestXmlForRenterImport(2, _RenterID: RenterID);
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            string amsiPropertyID = "gseminole";
            AmsiProperty.AMSI_ImportSingleProperty(amsiPropertyID);
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            try
            {
                AmsiImportRequest.ProcessAmsiRentersByProperty();
                Label1.Text = "Import Completed";
            }
            catch(Exception ex)
            {
                
            }
            
        }



        protected void Button7_Click1(object sender, EventArgs e)
        {
            int ID = 347;
            AmsiExportRequest.ProcessAmsiPaymentExport();
        }


            
        }


 }

