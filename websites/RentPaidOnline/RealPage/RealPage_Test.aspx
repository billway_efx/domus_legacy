﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RealPage_Test.aspx.cs" Inherits="Domus.RealPage.RealPage_Test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div style="margin-top:20px; margin-bottom:20px;">
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx">Go to Main site</asp:HyperLink>
            </div>

    <h2>Real Page</h2>
        <div style="margin-top:20px; margin-bottom:20px;">
                    <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />

        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>

        </div>

        <div>
            <asp:Label ID="Label2" runat="server" Text="Test Xml Output String"></asp:Label><br />
            <asp:Button ID="Button2" runat="server" Text="Test Output" OnClick="Button2_Click" />

        </div>

        <div>
            <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label><br />
            <asp:Button ID="Button3" runat="server" Text="Test Export 2" OnClick="Button3_Click" />

        </div>


    </div>
    </form>
</body>
</html>
