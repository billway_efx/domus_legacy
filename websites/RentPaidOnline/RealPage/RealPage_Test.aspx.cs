﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Text;
using EfxFramework.Scheduler;
using System.Net;
using System.Net.Mail;
using EfxFramework;
using EfxFramework.Pms.RealPage;


namespace Domus.RealPage
{
    public partial class RealPage_Test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string NewID = System.Guid.NewGuid().ToString();
            Label1.Text = NewID + "@unknown.com";
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            



            string RealPagePropertyPmsID = "3822119";

            RealPageImport.ImportFileList(RealPagePropertyPmsID);

            RealPageFileIdByProperty RP_File = new RealPageFileIdByProperty();
            RP_File = RealPageDataAccess.GetFileID(RealPagePropertyPmsID);

            RealPageImport.ImportRealPageResidentDetail(RealPagePropertyPmsID, RP_File.RealPageFileID);

            RealPageDataAccess.RealPageUpdateLeaseFeeByProperty(RealPagePropertyPmsID);



           //string TestReturn = WebServiceCall("DownloadListGroup");

        }


        private static string WebServiceCall(string _MethodName, string _RealPagePmsID = "")
        {
            string methodName = _MethodName;
            string _xml = "";
            switch(methodName)
            {
                case "DownloadListGroup":
                    _xml = DownloadListGroup();
                    break;
                case "DownloadBin":
                    _xml = DownloadBin();
                    break;
                case "DownloadLen":
                    _xml = DownloadLen();
                    break;

            }


            //Sample String for Import
            //https://onesite.realpage.com/webservices/generalServices/file.asmx?op=DownloadListGroup


            WebRequest webRequest = WebRequest.Create("https://onesite.realpage.com/webservices/generalServices/file.asmx");
            HttpWebRequest httpRequest = (HttpWebRequest)webRequest;
            httpRequest.Method = "POST";
            httpRequest.ContentType = "text/xml; charset=utf-8";
            httpRequest.ContentLength = _xml.Length;
            httpRequest.Headers.Add("SOAPAction: http://realpage.com/webservices/" + methodName);
            Stream requestStream = httpRequest.GetRequestStream();
            //Create Stream and Complete Request             
            StreamWriter streamWriter = new StreamWriter(requestStream, Encoding.ASCII);

            streamWriter.Write(_xml);
            streamWriter.Close();
            //Get the Response    
            HttpWebResponse wr = (HttpWebResponse)httpRequest.GetResponse();
            string testresponse = wr.ToString();
            StreamReader srd = new StreamReader(wr.GetResponseStream());
            string resulXmlFromWebService = srd.ReadToEnd();
            //cakel: Testing - returning string as xml formated
            string decodedString = HttpUtility.HtmlDecode(resulXmlFromWebService);
            return decodedString;
            //return resulXmlFromWebService;
        }



//        public static string DownloadListGroup()
//        {
//            string myString =
//            @"<?xml version=""1.0"" encoding=""utf-8""?>
//            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
//              <soap:Header>
//                <UserAuthInfo xmlns=""http://realpage.com/webservices"">
//                  <UserName>C_35046166-828D</UserName>
//                  <Password>ST5y3p</Password>
//                  <SiteID>3790036</SiteID>
//                  <PmcID>3790035</PmcID>
//                  <InternalUser>EFXFS</InternalUser>
//                </UserAuthInfo>
//              </soap:Header>
//                 <soap:Body>
//                    <DownloadListGroup xmlns=""http://realpage.com/webservices"">
//                      <GroupName>EFXFS</GroupName>
//                      <Range>All</Range>
//                      <Allsites>true</Allsites>
//                      <xmlerrors>true</xmlerrors>
//                    </DownloadListGroup>
//                  </soap:Body>
//            </soap:Envelope>";
//            return myString;
//        }


        public static string DownloadListGroup()
        {
            string myString =
            @"<?xml version=""1.0"" encoding=""utf-8""?>
                    <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
                      <soap:Header>
                        <UserAuthInfo xmlns=""http://realpage.com/webservices"">
                          <UserName>DGentry6</UserName>
                          <Password>P@ssword1</Password>
                          <SiteID>2999901</SiteID>
                          <PmcID>2999899</PmcID>
                          <InternalUser>EFXFS</InternalUser>
                        </UserAuthInfo>
                      </soap:Header>
                         <soap:Body>
                            <DownloadListGroup xmlns=""http://realpage.com/webservices"">
                              <GroupName>EFXFS</GroupName>
                              <Range>New</Range>
                              <Allsites>false</Allsites>
                              <xmlerrors>true</xmlerrors>
                            </DownloadListGroup>
                          </soap:Body>
                    </soap:Envelope>";
            return myString;
        }




        public static string DownloadLen()
        {
            string myString =
            @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
              <soap:Header>
                <UserAuthInfo xmlns=""http://realpage.com/webservices"">
                  <UserName>C_35046166-828D</UserName>
                  <Password>ST5y3p</Password>
                  <SiteID>3790036</SiteID>
                  <PmcID>3790035</PmcID>
                  <InternalUser>EFXFS</InternalUser>
                </UserAuthInfo>
              </soap:Header>
               <soap:Body>
                <DownloadLen xmlns=""http://realpage.com/webservices"">
                  <Interface>1</Interface>
                  <Format>xml</Format>
                  <FileID>409232</FileID>
                </DownloadLen>
              </soap:Body>
            </soap:Envelope>";
            return myString;

        }

        public static string DownloadBin()
        {
            string myString =
            @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
              <soap:Header>
                <UserAuthInfo xmlns=""http://realpage.com/webservices"">
                  <UserName>C_35046166-828D</UserName>
                  <Password>ST5y3p</Password>
                  <SiteID>3790036</SiteID>
                  <PmcID>3790035</PmcID>
                  <InternalUser>C_35046166-828D</InternalUser>
                </UserAuthInfo>
              </soap:Header>
                  <soap:Body>
                    <DownloadBin xmlns=""http://realpage.com/webservices"">
                      <Interface></Interface>
                      <Format></Format>
                      <FileID>409619</FileID>
                      <markdownloaded>false</markdownloaded>
                    </DownloadBin>
                  </soap:Body>
            </soap:Envelope>";
            return myString;
        }
 


        public static string GetInterfaceTypes()
        {
            string myString =
            @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
              <soap:Header>
                <UserAuthInfo xmlns=""http://realpage.com/webservices"">
                  <UserName>C_08302741-6BD6</UserName>
                  <Password>ST5y3p</Password>
                  <SiteID>3790036</SiteID>
                  <PmcID>3790035</PmcID>
                  <InternalUser>C_08302741-6BD6</InternalUser>
                </UserAuthInfo>
              </soap:Header>
              <soap:Body>
                <GetInterfaceTypes xmlns=""http://realpage.com/webservices"">
                  <xmlerrors>true</xmlerrors>
                </GetInterfaceTypes>
              </soap:Body>
            </soap:Envelope>";
            return myString;

        }

        public static string DownLoad()
        {
            string myString =
            @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
              <soap:Header>
                <UserAuthInfo xmlns=""http://realpage.com/webservices"">
                  <UserName>C_35046166-828D</UserName>
                  <Password>ST5y3p</Password>
                  <SiteID>3790036</SiteID>
                  <PmcID>3790035</PmcID>
                  <InternalUser>C_35046166-828D</InternalUser>
                </UserAuthInfo>
              </soap:Header>
                <soap:Body>
                    <Download xmlns=""http://realpage.com/webservices"">
                      <Interface>2</Interface>
                      <Format>xml</Format>
                      <FileID>409232</FileID>
                      <xmlerrors>true</xmlerrors>
                      <markdownloaded>false</markdownloaded>
                    </Download>
                  </soap:Body>
            </soap:Envelope>";
            return myString;
        }


        //Find Root Element 
        public static XElement GetRootElement(XElement element, string localName)
        {
            XElement Element = null;

            if (element != null && element.Name.LocalName == localName)
                return element;

            if (element != null && element.HasElements)
            {
                foreach (var E in element.Elements())
                {
                    Element = GetRootElement(E, localName);
                }
            }

            return Element;
        }

        private static string GetAttributeValue(XElement _Node, string attributeName)
        {
            foreach (XAttribute _att in _Node.Attributes())
            {
                if (_att.Name.LocalName == attributeName)
                {
                    return _att.Value;
                }

            }
            return "";
        }

        private static object CleanAttribute(string _XmlValue)
        {
            if (_XmlValue == "")
            {
                return null;
            }
            else
            {
                return _XmlValue;
            }
        }


        public static void GetRealPageXmlFile()
        {
            string _xml = WebServiceCall("DownloadBin");
            string _xmlRoot = "DownloadBinResult";
            string _result = "";

            string _xmlRoot2 = "ConvergentBilling";

            byte[] byteArray = Encoding.ASCII.GetBytes(_xml);

            using (var OutputStream = new MemoryStream(byteArray))
            {
                var Xml = XDocument.Load(OutputStream);
                var Element = GetRootElement(Xml.Root, _xmlRoot);

                if (Element != null)
                {
                    var XmlResponseString = Element.Value;
                    _result = ConvertBinary64ToXml(XmlResponseString);
                }

            }

            byte[] RespByteArray = Encoding.ASCII.GetBytes(_result);
            using (var OutputStream = new MemoryStream(RespByteArray))
            {
                var XmlResp = XDocument.Load(OutputStream);

                var RespElement = GetRootElement(XmlResp.Root, _xmlRoot2);

                foreach (XElement ConvBill in RespElement.Elements())
                {
                    string _node1 = ConvBill.Name.LocalName;

                    foreach(XElement node2 in ConvBill.Elements())
                    {
                        string node3 = node2.Name.LocalName;

                        if (node3 == "Resident")
                        {
                            
                            int ResidentID = (int)node2.Attribute("ReshID");
                            int RP_PropertyID = (int)node2.Attribute("SiteID");
                            string RP_UnitID = node2.Attribute("UnitID").Value.ToString();
                            int RP_LeaseID = (int)node2.Attribute("LeaID");
                            int ResidentMemberID = (int)node2.Attribute("ResmID");
                            string UseUnitAddress = node2.Attribute("UseUnitAddress").Value.ToString();
                            string HouseHoldStatus = node2.Attribute("Status").Value.ToString();
                            string ResidentBillingName = node2.Attribute("BillName").Value.ToString();
                            string ResidentMailingName = node2.Attribute("MailName").Value.ToString();
                            string ResidentPmtLatCount = node2.Attribute("LatePmtCnt").Value.ToString();
                            int ResidentNSFCount = (int)node2.Attribute("NSFCnt");
                            string ResidentNoChk = node2.Attribute("NoChk").Value.ToString();
                            string ResidentInColl = node2.Attribute("InColl").Value.ToString();
                            string LeaseSigner = node2.Attribute("LeaseSigner").Value.ToString();
                            string FirstName = node2.Attribute("FirstName").Value.ToString();
                            string LastName = node2.Attribute("LastName").Value.ToString();

                            string ResidentKeyID = RP_PropertyID + "-" + ResidentID + "-" + RP_LeaseID;

                            string ResidentAddress1 = "";
                            string ResidentAddress2 = "";
                            string City = "";
                            string ResidentState = "";
                            string Zip = "";
                            string Country = "";
                            string Phone = "";
                            string Phone2 = "";
                            string PhoneExt = "";
                            string PhoneExt2 = "";
                            string EmailAddress = "";
                            string FaxNumber = "";
                            string cellPhone = "";


                            foreach(XElement _Address in node2.Elements())
                            {

                                if (_Address.Name.LocalName == "RegularAddress")
                                {

                                    ResidentAddress1 = _Address.Attribute("Adr1").Value.ToString();
                                    ResidentAddress2 = _Address.Attribute("Adr2").Value.ToString();
                                    City = _Address.Attribute("City").Value.ToString();
                                    ResidentState = _Address.Attribute("State").Value.ToString();
                                    Zip = _Address.Attribute("Zip").Value.ToString();
                                    Country = _Address.Attribute("Country").Value.ToString();
                                    Phone = _Address.Attribute("Phone1").Value.ToString();
                                    Phone2 = _Address.Attribute("Phone2").Value.ToString();
                                    PhoneExt = _Address.Attribute("Ext1").Value.ToString();
                                    PhoneExt2 = _Address.Attribute("Ext2").Value.ToString();
                                    EmailAddress = _Address.Attribute("Email").Value.ToString();
                                    cellPhone = _Address.Attribute("Adr1").Value.ToString();
                                    
                                }
                                        
                            }

                            //save the resident data to table here


                            string saveData = "Saving Data for " + ResidentID + " " + FirstName + " " + LastName;

                        }
                        

                    }


                    if(_node1 == "LeaseList")
                    {
                        string LeaseID = "";
                        foreach (XElement _lease in ConvBill.Elements())
                        {
                            LeaseID = _lease.Attribute("LeaID").Value.ToString();
                        }

                    }
                }

            }//end Using


        }
        //end

        public static string ConvertBinary64ToXml(string _Binary64)
        {
            try
            {
                System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
                System.Text.Decoder utf8Decode = encoder.GetDecoder();

                byte[] todecode_byte = Convert.FromBase64String(_Binary64);
                int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                char[] decoded_char = new char[charCount];
                utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                string result = new String(decoded_char);

                return result;

            }
            catch (Exception e)
            {
                throw new Exception("Error in base64Decode" + e.Message);
            }

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            //string Test = RealPageExport.RealPageGetXmlPaymentDataByProperty("3822119");
            RealPageImport.ProcessRealPageSingleProperty(20);

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            string Test = RealPageExport.RealPageGetXmlPaymentDataByProperty("3822119");
            string _endPoint = "http://APPPARTNERS.ONESITE.REALPage.com/SDESvc/External/File.asmx/Upload";





            RealPageExport.RealPageExportByProperty("3822119", Test, _endPoint);

            RealPageExport.RealPageExportSummary(Test);

            



        }


        private string TestError()
        {

            string Test = @"
                <root>
                    <Response>
                    <Errors>
                        <Error ID="""" ShortDesc=""SDE.Validate:  An error has occured while attempting to validate XML."" Severity=""Client""><LongDesc><![CDATA[The element 'Authorization' cannot contain child element 'Parameters' because the parent element's content model is empty.]]></LongDesc></Error>
                        <Error ID="" ShortDesc=""SDE.Validate:  An error has occured while attempting to validate XML."" Severity=""Client""><LongDesc><![CDATA[The element 'Request' has incomplete content. List of possible elements expected: 'Parameters'.]]></LongDesc></Error>
                    </Errors>
                </Response>
                </root>";

            return Test;
        }





    }
}