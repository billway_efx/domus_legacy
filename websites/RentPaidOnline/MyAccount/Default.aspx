﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Domus.MyAccount.Default" %>

<%@ Import Namespace="EfxFramework" %>
<%@ Import Namespace="EfxFramework.Web.Facade" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopHeadline" runat="server">
    <h1>My Account</h1>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="main-container">
        <div class="section-wrap">
            <div class="section-body">
                <!-- RPO Admin Level -->
                <div class="clear"></div>
                <div id="rpo" runat="server">
                    <h2>My Account</h2>
                    <asp:ValidationSummary ID="rpoAdminValSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="RPOMyAccount" />
                    <div class="alert alert-success" runat="server" id="rpoAdminDivSuccess" visible="false"><b>Success!</b></div>
                    <div class="formWrapper">
                        <div class="formHalf">
                            <label>First Name</label>
                            <asp:TextBox ID="FirstNameTextbox" runat="server" CssClass="form-control" />
                        </div>
                        <div class="formHalf">
                            <label>Last Name</label>
                            <asp:TextBox ClientIDMode="Static" ID="LastNameTextbox" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                    <h2>Log In Credentials</h2>
                    <div class="formWrapper">
                        <div class="formWhole">
                            <div class="formHalf">
                                <label>Username</label>
                                <asp:TextBox ID="UserNameTextbox" runat="server" CssClass="form-control" AutoCompleteType="None"></asp:TextBox>
                            </div>
                        </div>
                        <div class="formHalf">
                            <label>Password</label>
                            <asp:TextBox ID="PasswordTextbox" TextMode="Password" runat="server" CssClass="form-control" AutoCompleteType="None"></asp:TextBox>
                        </div>
                        <div class="formHalf">
                            <label>Confirm Password</label>
                            <asp:TextBox ID="ConfirmPasswordTextbox" TextMode="Password" runat="server" CssClass="form-control" AutoCompleteType="None"></asp:TextBox>
                        </div>
                    </div>
                    <div class="button-footer">
                        <div class="button-action">
                            <%--<button class="btn btn-default">Cancel</button>--%>
                        </div>
                        <div class="main-button-action">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-default btn-large footer-button" OnClick="SaveAdminButtonClick" />
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>

                <!-- Property Manager Level-->
                <div id="tabs" runat="server" clientidmode="static">
                    <script src="//code.jquery.com/jquery-1.9.1.js"></script>
                    <script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
                    <!-- Begin Tabs -->
                    <div class="details-panel no-details">
                        <ul id="tabLinks" class="nav nav-tabs no-detail">
                            <li id="firstTab" class="active"><a href="#NameTab"><%=Facade.CurrentStaff.DisplayName %></a></li>
                            <li><a href="#AccountInformationTab">Account Information</a></li>
                            <li><a href="#FeedbackTab">Leave Feedback</a></li>
                        </ul>
                    </div>
                    <asp:ValidationSummary ID="MyAccountSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="MyAccount" />
                    <div class="alert alert-success" runat="server" id="uxSuccessDiv" visible="false"><b>Success!</b></div>
                    <!-- Tab 1 -->
                    <div id="NameTab">
                        <div class="formWrapper">
                            <h2>Basic Information</h2>
                            <div class="formThird">
                                <label>First Name</label>
                                <asp:TextBox ClientIDMode="Static" ID="PMFirstNameTextbox" runat="server" CssClass="form-control" />
                            </div>
                            <div class="formThird">
                                <label>Last Name</label>
                                <asp:TextBox ClientIDMode="Static" ID="PMLastNameTextbox" runat="server" CssClass="form-control" />
                            </div>
                            <div class="formThird">
                                <label>Email Address</label>
                                <asp:TextBox ClientIDMode="Static" ID="EmailTextbox" runat="server" CssClass="form-control" />
                            </div>

                            <div class="formHalf">
                                <label>Address</label>
                                <asp:TextBox ClientIDMode="Static" ID="AddressTextbox" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="formHalf">
                                <label>Address2</label>
                                <asp:TextBox ClientIDMode="Static" ID="AddressLine2Textbox" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>

                            <div class="formHalf">
                                <label>City</label>
                                <asp:TextBox ClientIDMode="Static" ID="CityTextbox" runat="server" CssClass="form-control" />
                            </div>
                            <div class="formHalf">
                                <div class="formHalf">
                                    <label>State</label>
                                    <asp:DropDownList ID="StateDropdown" ClientIDMode="Static" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <div class="formFourth">
                                    <label>Zip</label>
                                    <asp:TextBox ClientIDMode="Static" ID="ZipTextbox" runat="server" CssClass="form-control" />
                                </div>
                            </div>
                        </div>

                        <h2>Contact Info</h2>
                        <div class="formWrapper">
                            <div class="formThird">
                                <label>Cell Phone</label>
                                <asp:TextBox ClientIDMode="Static" TextMode="Phone" ID="CellPhoneTextbox" runat="server" CssClass="form-control" />
                            </div>
                            <div class="formThird">
                                <label>Office Phone</label>
                                <asp:TextBox ClientIDMode="Static" TextMode="Phone" ID="OfficePhoneTextbox" runat="server" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="button-footer">
                            <div class="main-button-action">
                                <asp:Button ID="SaveButton" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="SaveButtonClick" />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>


                    <!-- Tab 2 -->
                    <div id="AccountInformationTab">
                        <div class="formWrapper">
                            <h2>Log In / Password Information</h2>
                            <div class="formWhole">
                                <div class="formThird">
                                    <label>Username:</label>
                                    <asp:TextBox ClientIDMode="Static" ID="AccountEmailAddress" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="formWhole">
                                <div class="formThird">
                                    <label>New Password:</label>
                                    <asp:TextBox ClientIDMode="Static" ID="AccountNewPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                            <div class="formWhole">
                                <div class="formThird">
                                    <label>Confirm Password:</label>
                                    <asp:TextBox ClientIDMode="Static" ID="AccountConfirmPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="button-footer">
                            <div class="main-button-action">
                                <asp:Button ID="SaveAccountInfoButton" ValidationGroup="ChangeUserPasswordValidationGroup" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="SaveAccountInfoButtonClick" />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- Tab 3 -->
                    <div id="FeedbackTab">
                        <div class="formWrapper">
                            <asp:UpdatePanel ID="FeedbackPanel" runat="server">
                                <ContentTemplate>
                                    <div class="formWhole">
                                        <h2>Leave Feedback for Domus /  EFX Staff</h2>
                                    </div>
                                    <div class="formWhole">
                                        <label>Please provide feedback in the form below:</label>
                                        <asp:TextBox ClientIDMode="Static" ID="FeedbackTextbox" runat="server" CssClass="form-control" TextMode="MultiLine" Height="400px"></asp:TextBox>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="button-footer">
                            <div class="main-button-action">
                                <asp:Button ID="SubmitFeedbackButton" runat="server" Text="Send" CssClass="btn btn-primary" OnClick="SubmitFeedbackButtonClick" />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            if ($('#tabs').length != 0) {
                $("#tabs").tabs();
            }
        });

        function showTab(tabName) {
            var tabToShow = $('a[href="' + tabName + '"]');
            var index = $('#tabs a[href="' + tabName + '"]').parent().index();
            $('#tabLinks li').removeClass('active');
            $(tabToShow).parent().addClass('active');
            $("#tabs").tabs({ active: index });
        }

        $('#tabLinks li a').click(function (e) {
            $('#tabLinks li').removeClass('active');
            $(this).parent().addClass('active');
            $('#<%= uxSuccessDiv.ClientID %>').fadeOut();
            $('#<%= MyAccountSummary.ClientID %>').fadeOut();
        });
    </script>

</asp:Content>
