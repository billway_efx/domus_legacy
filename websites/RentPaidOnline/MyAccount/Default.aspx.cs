﻿using EfxFramework;
using EfxFramework.ExtensionMethods;
using EfxFramework.Web;
using EfxFramework.Web.Facade;
using Mb2x.ExtensionMethods;
using RPO.Helpers;
using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domus.MyAccount
{
    public partial class Default : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			uxSuccessDiv.Visible = false;
			rpoAdminDivSuccess.Visible = false;
            if (!IsPostBack && !EfxFramework.Helpers.Helper.IsRpoAdmin && !EfxFramework.Helpers.Helper.IsCsrUser)
			{
				StateDropdown.BindToSortedEnum(typeof(StateProvince));
				AccountEmailAddress.Text = Facade.CurrentStaff.PrimaryEmailAddress;
			}

            if (EfxFramework.Helpers.Helper.IsRpoAdmin || EfxFramework.Helpers.Helper.IsCsrUser)
			{
				tabs.Visible = false;
				rpo.Visible = true;
			}
			else
			{
				tabs.Visible = true;
				rpo.Visible = false;
			}

            if (Facade.CurrentEfxUser != null && !IsPostBack)
            {
                FirstNameTextbox.Text = Facade.CurrentEfxUser.FirstName;
                LastNameTextbox.Text = Facade.CurrentEfxUser.LastName;
                UserNameTextbox.Text = Facade.CurrentEfxUser.PrimaryEmailAddress;
            }

            if (EfxFramework.Helpers.Helper.IsCsrUser)
            {
                FirstNameTextbox.Enabled = false;
                LastNameTextbox.Enabled = false;
                UserNameTextbox.Enabled = false;
                PasswordTextbox.Enabled = false;
                ConfirmPasswordTextbox.Enabled = false;
            }
            //ResidentsRoom.TypeOfAnnouncement = AnnouncementType.ResidentsRoom;
            //ProsCorner.TypeOfAnnouncement = AnnouncementType.ProsCorner;
            //NewsAndEvents.TypeOfAnnouncement = AnnouncementType.News;
        }

		protected override void OnPreRender(EventArgs e)
		{
			try
			{
				base.OnPreRender(e);

				if (Facade.CurrentStaff != null && Facade.CurrentStaff.PropertyStaffId.HasValue)
				{
					// Bind account information tab
					//ProfileImage.Src = string.Format("/Handlers/PropertyManagerHandler.ashx?requestType=0&propertyStaffId={0}", Facade.CurrentStaff.PropertyStaffId.Value);
					PMFirstNameTextbox.Text = Facade.CurrentStaff.FirstName;
					PMLastNameTextbox.Text = Facade.CurrentStaff.LastName;
					EmailTextbox.Text = Facade.CurrentStaff.PrimaryEmailAddress;
					AddressTextbox.Text = Facade.CurrentStaff.StreetAddress;
					AddressLine2Textbox.Text = Facade.CurrentStaff.StreetAddress2;
					CityTextbox.Text = Facade.CurrentStaff.City;
					if (Facade.CurrentStaff.StateProvinceId.HasValue) StateDropdown.SelectedValue = Facade.CurrentStaff.StateProvinceId.Value.ToString(CultureInfo.InvariantCulture);
					ZipTextbox.Text = Facade.CurrentStaff.PostalCode;
					CellPhoneTextbox.Text = Facade.CurrentStaff.MobilePhoneNumber;
					OfficePhoneTextbox.Text = Facade.CurrentStaff.MainPhoneNumber;
				}
			}
			catch
			{
				Response.Redirect("~/ErrorPage.html", false);
			}
		}

        protected void SaveAdminButtonClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(PasswordTextbox.Text))
            {
                // Validate 
                if (!string.Equals(PasswordTextbox.Text, ConfirmPasswordTextbox.Text))
                {
					uxSuccessDiv.Visible = false;
					CustomValidator err = new CustomValidator();
					err.ValidationGroup = "RPOMyAccount";
					err.IsValid = false;
					err.ErrorMessage = "Passwords do not match.";
					Page.Validators.Add(err);
                    return;
                }

                Facade.CurrentEfxUser.SetPassword(PasswordTextbox.Text);
            }

            Facade.CurrentEfxUser.FirstName = FirstNameTextbox.Text;
            Facade.CurrentEfxUser.LastName = LastNameTextbox.Text;
            Facade.CurrentEfxUser.PrimaryEmailAddress = UserNameTextbox.Text;

            EfxAdministrator.Set(Facade.CurrentEfxUser, UIFacade.GetAuthenticatedUserString(UserType.EfxAdministrator));
			rpoAdminDivSuccess.Visible = true;
        }

		protected void SaveButtonClick(object sender, EventArgs e)
		{
			try
			{
				ClientScriptManager cs = Page.ClientScript;
				PropertyStaff ps = new PropertyStaff(Facade.CurrentStaff.PropertyStaffId);

				ps.FirstName = PMFirstNameTextbox.Text;
				ps.LastName = PMLastNameTextbox.Text;
				ps.StreetAddress = AddressTextbox.Text;
				ps.StreetAddress2 = AddressLine2Textbox.Text;
				ps.City = CityTextbox.Text;
				ps.StateProvinceId = StateDropdown.SelectedValue.ToNullInt32();
				ps.PostalCode = ZipTextbox.Text;
				ps.MobilePhoneNumber = CellPhoneTextbox.Text;
				ps.MainPhoneNumber = OfficePhoneTextbox.Text;

				PropertyStaff.Set(ps, UIFacade.GetAuthenticatedUserString(UserType.PropertyStaff));

				// Set renters photo if provided
				//if (PhotoUploader.HasFile && Facade.CurrentStaff.PropertyStaffId.HasValue)
				//	PropertyStaff.SetPhoto(Facade.CurrentStaff.PropertyStaffId.Value, PhotoUploader.FileBytes, UIFacade.GetAuthenticatedUserString(UserType.PropertyStaff));
				uxSuccessDiv.Visible = true;
				if (!cs.IsStartupScriptRegistered(this.GetType(), "startUpName"))
				{
					String cstext1 = "showTab('#NameTab');";
					cs.RegisterStartupScript(this.GetType(), "startUpName", cstext1, true);
				}
			}
			catch
			{
				uxSuccessDiv.Visible = false;
				Response.Redirect("~/ErrorPage.html", false);
			}
		}

		protected void SaveAccountInfoButtonClick(object sender, EventArgs e)
		{
			try
			{
				ClientScriptManager cs = Page.ClientScript;
				if (string.IsNullOrEmpty(AccountEmailAddress.Text))
				{
					uxSuccessDiv.Visible = false;
					CustomValidator err = new CustomValidator();
					err.ValidationGroup = "MyAccount";
					err.IsValid = false;
					err.ErrorMessage = "Email address is required to update password information.";
					Page.Validators.Add(err);

					if (!cs.IsStartupScriptRegistered(this.GetType(), "startUpAccount"))
					{
						String cstext1 = "showTab('#AccountInformationTab');";
						cs.RegisterStartupScript(this.GetType(), "startUpAccount", cstext1, true);
					}
					return;
				}
				if (string.IsNullOrEmpty(AccountNewPassword.Text) ||
					string.IsNullOrEmpty(AccountConfirmPassword.Text))
				{
					uxSuccessDiv.Visible = false;
					CustomValidator err = new CustomValidator();
					err.ValidationGroup = "MyAccount";
					err.IsValid = false;
					err.ErrorMessage = "New password and password confirmation are required to update password information.";
					Page.Validators.Add(err);

					if (!cs.IsStartupScriptRegistered(this.GetType(), "startUpAccount"))
					{
						String cstext1 = "showTab('#AccountInformationTab');";
						cs.RegisterStartupScript(this.GetType(), "startUpAccount", cstext1, true);
					}
					return;
				}
				if (!AccountNewPassword.Text.Equals(AccountConfirmPassword.Text))
				{
					uxSuccessDiv.Visible = false;
					CustomValidator err = new CustomValidator();
					err.ValidationGroup = "MyAccount";
					err.IsValid = false;
					err.ErrorMessage = "New password and password confirmation do not match.";
					Page.Validators.Add(err);
					if (!cs.IsStartupScriptRegistered(this.GetType(), "startUpAccount"))
					{
						String cstext1 = "showTab('#AccountInformationTab');";
						cs.RegisterStartupScript(this.GetType(), "startUpAccount", cstext1, true);
					}
					return;
				}
				PropertyStaff ps = new PropertyStaff(Facade.CurrentStaff.PropertyStaffId);
				ps.PrimaryEmailAddress = AccountEmailAddress.Text;

				// Membership needed here
				ps.SetPassword(AccountNewPassword.Text);

				PropertyStaff.Set(ps, UIFacade.GetAuthenticatedUserString(UserType.PropertyStaff));
				uxSuccessDiv.Visible = true;
				
				if (!cs.IsStartupScriptRegistered(this.GetType(), "startUpAccount"))
				{
					String cstext1 = "showTab('#AccountInformationTab');";
					cs.RegisterStartupScript(this.GetType(), "startUpAccount", cstext1, true);
				}
			}
			//catch(Exception ex)
            catch
			{
				uxSuccessDiv.Visible = false;
				Response.Redirect("~/ErrorPage.html", false);
			}
		}

		protected void SubmitFeedbackButtonClick(object sender, EventArgs e)
		{
			try
			{
				if (!string.IsNullOrEmpty(FeedbackTextbox.Text))
				{
					ClientScriptManager cs = Page.ClientScript;
					Facade.SendFeedbackEmail(null, "PropertyManager", false, Facade.CurrentStaff.PrimaryEmailAddress, FeedbackTextbox.Text);
					uxSuccessDiv.Visible = true;
					if (!cs.IsStartupScriptRegistered(this.GetType(), "startUpFeedback"))
					{
						String cstext1 = "showTab('#FeedbackTab');";
						cs.RegisterStartupScript(this.GetType(), "startUpFeedback", cstext1, true);
					}
				}
			}
			catch
			{
				uxSuccessDiv.Visible = false;
				Response.Redirect("~/ErrorPage.html", false);
			}
		}
    }
}