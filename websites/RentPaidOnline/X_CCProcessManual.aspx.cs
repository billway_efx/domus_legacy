﻿using System;

namespace RentPaidOnline
{
    public partial class X_CCProcessManual : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label1.Text = @"Process Complete";
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            EfxFramework.Scheduler.CreditCardAchSettlements.ProcessGroupCcAch();
            Label2.Text = @"Credit Card Complete";
        }
    }
}