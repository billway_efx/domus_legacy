﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="AccessDenied.aspx.cs" Inherits="Domus.Pages.AccessDenied" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!--cakel: BUGID00059 - Updated Master Page file from Site.Master to frontend.master -->
    <div class="SystemMessage-Error">
        <div class="SystemMessageBody">
            <h2>
                You are not authorized to view this page.
            </h2>
            
        </div>
    </div>
</asp:Content>
