﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Domus.Programs.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h3 class="icon padding-top"><img src="/Images/leaf.png" alt="All Programs" />All Programs</h3>
    <div class="float-wrap padding" style="width: 800px">
    <div class="float-left green-gradient-wrap" style="margin-right: 20px;padding: 10px 20px 20px 20px">
        <h4>
            Air Program
        </h4>
        <p>Credit Card Fee: <asp:Literal ID="AirCreditCardFee" runat="server"></asp:Literal></p>
        <p>eCheck Fee: <asp:Literal ID="AirECheckFee" runat="server"></asp:Literal></p>
        <p>App Deposit Fee: <asp:Literal ID="AirAppDepositFee" runat="server"></asp:Literal></p>
    </div>
    <div class="float-left green-gradient-wrap" style="margin-right: 20px;padding: 10px 20px 20px 20px">
    <h4>
        Spa Program
    </h4>
        <p>Credit Card Fee: <asp:Literal ID="SpaCreditCardFee" runat="server"></asp:Literal></p>
        <p>eCheck Fee: <asp:Literal ID="SpaECheckFee" runat="server"></asp:Literal></p>
        <p>App Deposit Fee: <asp:Literal ID="SpaAppDepositFee" runat="server"></asp:Literal></p>
    </div>
    <div class="float-left green-gradient-wrap" style="padding: 10px 20px 20px 20px">
    <h4>
        Property Paid Program
    </h4>
        <p>Credit Card Fee: <asp:Literal ID="PropertyCreditCardFee" runat="server"></asp:Literal></p>
        <p>eCheck Fee: <asp:Literal ID="PropertyECheckFee" runat="server"></asp:Literal></p>
        <p>App Deposit Fee: <asp:Literal ID="PropertyAppDepositFee" runat="server"></asp:Literal></p>
    </div>
    </div>
</asp:Content>
