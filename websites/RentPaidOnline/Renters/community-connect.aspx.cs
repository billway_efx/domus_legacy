﻿using EfxFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domus.Renters
{
    public partial class community_connect : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!EfxFramework.Helpers.Helper.IsRPOAdmin)
            //    Response.Redirect("~/");
            //cakel: 08/23/2016 commented out as per Sean we are not using these controls
            //ResidentsRoom.TypeOfAnnouncement = AnnouncementType.ResidentsRoom;
            //ProsCorner.TypeOfAnnouncement = AnnouncementType.ProsCorner;
            //NewsAndEvents.TypeOfAnnouncement = AnnouncementType.News;

            //CMallory - Task 00462 - Added
            if (!EfxFramework.Helpers.Helper.IsRpoAdmin)
            {
                JobPostbtn.Visible = false;
                ResidentRoombtn.Visible = false;
                ProCornerbtn.Visible = false;

                if(!Page.IsPostBack)
                {
                    MultiView1.ActiveViewIndex = 4;

                }

            }

        }

        protected void JobPostbtn_Click(object sender, EventArgs e)
        {
            MultiView1.SetActiveView(JobPostingView);
        }

        protected void ResidentRoombtn_Click(object sender, EventArgs e)
        {
            MultiView1.SetActiveView(ResidentsRoomView);
        }

        protected void ProCornerbtn_Click(object sender, EventArgs e)
        {
            MultiView1.SetActiveView(ProsCornerView);
        }

        protected void NewsEventbtn_Click(object sender, EventArgs e)
        {
            MultiView1.SetActiveView(NewsAndEventsView);
        }

        protected void Announcementsbtn_Click(object sender, EventArgs e)
        {
            MultiView1.SetActiveView(AnnouncementView);
        }

        protected void Marketingbtn_Click(object sender, EventArgs e)
        {
            MultiView1.SetActiveView(EmailMarketingView);
        }

        protected void SMSbtn_Click(object sender, EventArgs e)
        {
            MultiView1.SetActiveView(SMSMarketingView);
        }
    }
}