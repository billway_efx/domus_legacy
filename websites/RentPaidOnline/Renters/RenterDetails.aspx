﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="RenterDetails.aspx.cs" Inherits="RentPaidOnline.Renters.RenterDetails" %>

<%@ Import Namespace="EfxFramework.ExtensionMethods" %>
<%@ Register Src="~/UserControls/Account/LeaseInfoV3.ascx" TagPrefix="uc1" TagName="LeaseInfoV3" %>
<%@ Register Src="~/UserControls/PasswordUpdate.ascx" TagPrefix="uc1" TagName="PasswordUpdate" %>
<%@ Register Src="~/UserControls/Renter/UserLogging.ascx" TagPrefix="uc1" TagName="UserLogging" %>
<%@ Register Src="~/UserControls/Renter/CSRTicketing.ascx" TagPrefix="uc1" TagName="CSRTicketing" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TopHeadline" runat="server">
   <style>
      /* Task 00232 added dillon Hide class so that "we can hide buttons and show them for voiding or refunding transactions"*/
      dillonHide {
         display: none;
      }
   </style>

   <%-- Patrick Whittingham - 03/25/2015 - Task # 000388 : Fix error which shows a system error on the page to show a validation error.  --%>
   <asp:ValidationSummary ID="ApplicationSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="RenterDtl" />
   <h1>Renter Details</h1>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <div id="renterManager" class="main-container" ng-controller="ResidentController">
      <div class="details-panel">
         <div class="inner" style="position: relative;">
            <a href="{{urlPrefix}}/rpo/property/{{renter.RenterPropertyId}}/residents" class="back-to-listings" target="_self"><span data-icon="&#x5f" aria-hidden="true"></span>Back to Listings</a>
            <div class="details-panel-content">
               <div class="col-third">
                  <loading></loading>
                  <p>
                     <asp:Label ID="Label1" runat="server" Text="Renter: "></asp:Label><asp:Label ID="RenterNamelbl" class="value" runat="server"></asp:Label>
                  </p>
                  <p>
                     <asp:Label ID="Label2" runat="server" Text="Property: "></asp:Label><asp:Label ID="PropertyNamelbl" class="value" runat="server"></asp:Label>
                  </p>
                  <p>
                     <asp:Label ID="Label4" runat="server" Text="Unit: "></asp:Label><asp:Label ID="UnitNumberlbl" class="value" runat="server"></asp:Label>
                  </p>
                  <p>
                     <asp:Label ID="Label6" runat="server" Text="Rent Amount: "></asp:Label><asp:Label ID="RentAmountlbl" class="value" runat="server"></asp:Label>
                  </p>
                  <p>
                     <asp:Label ID="Label8" runat="server" Text="Accepted Payment Type: "></asp:Label><asp:Label ID="AcceptedPaymentlbl" class="value" runat="server"></asp:Label>
                  </p>
                  <%-- cakel: BUGID00226 rev2 commented out old html to replace with new code --%>
                  <%--                    <p ng-bind-template="Renter: {{renter.FirstName}} {{renter.LastName}}"></p>
                        <p class="ng-cloak" ng-cloak>Property: <span class="value" ng-bind="renter.PropertyName"></span></p>
                        <p>Unit: <span class="value" ng-bind="renter.Unit"></span></p>
                        <p>Rent: <span class="value" ng-bind="renter.RentAmount | currency"></span></p>--%>
               </div>

               <div class="col-third">
                  <p>
                     <asp:Label ID="Label3" runat="server" Text="Public Resident ID: "></asp:Label><asp:Label ID="PublicResidentIDlbl" class="value" runat="server"></asp:Label>
                  </p>
                  <p>
                     <asp:Label ID="Label7" runat="server" Text="PMS ID: "></asp:Label><asp:Label ID="PMSIDlbl" class="value" runat="server"></asp:Label>
                  </p>
                  <p>
                     <asp:Label ID="Label9" runat="server" Text="Property Manager: "></asp:Label><asp:Label ID="PropertyManagerNamelbl" class="value" runat="server"></asp:Label>
                  </p>
                  <p>
                     <asp:Label ID="Label11" runat="server" Text="Property Phone: "></asp:Label><asp:Label ID="PropertyPhonelbl" class="value" runat="server"></asp:Label>
                  </p>
                  <p>
                     <asp:Label ID="Label5" runat="server" Text="Has Registered on Site: "></asp:Label><asp:Label ID="HasRegisteredlbl" class="value" runat="server"></asp:Label>
                  </p>

                  <%-- cakel: BUGID00226 rev2 commented out old html to replace with new code --%>
                  <%--                    <p>Public Resident ID: <span class="value" ng-bind="renter.PublicRenterId"></span></p>
                        <p>PMS ID: <span class="value" ng-bind="renter.PmsId"></span></p>
                        <p>IVR Pin: <span class="value" ng-bind="renter.AccountCodePin"></span></p>
                        <!--cakel:BUGID00226 Added phone number for property -->
                        <p>Property Phone:<span class="value" ng-bind="renter.MainPhoneNumber"></span></p>--%>
               </div>

               <%--<div class="col-third emphasized balance-col">
									<p>Balance Due: <span class="balance-text">$844.73</span></p>
									<p><span class="value">as of September 9, 2013</span></p>
								</div>--%>
            </div>

            <% if (!EfxFramework.Helpers.Helper.IsCsrUser)
               { %>
            <div class="details-panel-actions">
               <%--<a href="#"><span data-icon="&#x24" aria-hidden="true"></span>Edit Account</a>--%>
               <%--<a href="#"><span data-icon="&#x50" aria-hidden="true" ng-click="toggleActive()"></span>Disable Account</a>--%>
               <asp:LinkButton ID="DeleteRenter" runat="server" OnClick="DeleteButtonClick" OnClientClick="return confirm('Are you sure you want to delete this resident account?')"><span data-icon="&#x2b" aria-hidden="true"></span>Remove Account</asp:LinkButton>
            </div>
            <%} %>
         </div>

      </div>
      <div id="tabs">
         <ul id="tabLinks" class="nav nav-tabs">
            <li ng-class="{active: activeTab == 'NameTab'}"><a href="#" ng-click="showTab('NameTab')">
               <asp:Literal ID="NameTab" runat="server"></asp:Literal></a></li>
            <asp:PlaceHolder ID="PayersTabPanel" runat="server">
               <li ng-class="{active: activeTab == 'PayersInfo'}"><a href="#" ng-click="showTab('PayersInfo')">Payers Info</a></li>
            </asp:PlaceHolder>
            <li ng-class="{active: activeTab == 'PaymentInformationTab'}"><a href="#" ng-click="showTab('PaymentInformationTab')">Payment Info</a></li>
            <li ng-class="{active: activeTab == 'LeaseInformationTab'}"><a href="#" ng-click="showTab('LeaseInformationTab')">Lease Info</a></li>
            <li ng-class="{active: activeTab == 'HistoryTab'}"><a href="#" ng-click="showTab('HistoryTab')">History</a></li>
            <li ng-class="{active: activeTab == 'NotesTab'}"><a href="#" ng-click="showTab('NotesTab')">Notes</a></li>
            <%--dwillis Task 00076--%>
            <% if (!EfxFramework.Helpers.Helper.IsCsrUser)
               { %>
            <li ng-class="{active: activeTab == 'LogTab'}"><a href="#" ng-click="showTab('LogTab')">User Logs</a></li>
            <%} %>
            <% if (EfxFramework.Helpers.Helper.IsCsrUser || EfxFramework.Helpers.Helper.IsRpoAdmin)
               { %>
            <li ng-class="{active: activeTab == 'TicketsTab'}"><a href="#" ng-click="showTab('TicketsTab')">Tier 2 Ticket</a></li>
            <%} %>
         </ul>

         <!-- Begin Tab 1 -->

         <div id="NameTab">
            <div class="formTint">
               <div class="formWrapper">

                  <!--  <h4>Public Resident ID: <!-- <%=CurrentRenter.PublicRenterId %> -->
                  <!-- /h4>  -->
                  <div class="formWhole">
                     <div class="formThird">
                        <h2>Account Information</h2>
                        <label>Email Address/Username</label>
                        <asp:TextBox ClientIDMode="Static" ID="EmailTextbox" runat="server" CssClass="form-control" Width="240px" OnTextChanged="EmailTextbox_TextChanged" />
                        <asp:Label runat="server" ID="EmailDupeLabel" ForeColor="#FF3300" Visible="False" Font-Bold="True" Font-Size="Smaller"></asp:Label>
                     </div>
                     <div class="formThird">
                        <label>Notification Options</label>
                        <asp:CheckBox ID="TextNotificationsCheckbox" ClientIDMode="Static" runat="server" CssClass="CheckBoxText" />
                        Text Notifications<br />
                        <asp:CheckBox ID="EmailNotificationsCheckbox" ClientIDMode="Static" runat="server" CssClass="CheckBoxText" />
                        Email Notifications           
                     </div>
                     <div class="formThird">
                        <%-- cakel: BUGID00200 commented out reset button to replace with new controls --%>

                        <h2>Update Password</h2>
                        <uc1:PasswordUpdate runat="server" ID="PasswordUpdate" />
                        <%--<asp:Button OnClientClick="return confirm('Are you sure you want to reset the password?');" ID="PayerResetRenterPassword" runat="server" CssClass="btn btn-default" Text="Reset Password" OnClick="PayerResetRenterPassword_Click" />--%>
                     </div>

                  </div>
                  <div class="formWhole" style="padding-top: 0px;">
                  </div>
               </div>
            </div>
            <div class="formWrapper">
               <h2>Resident's Information</h2>
               <div class="formHalf">
                  <label>First Name</label>
                  <asp:TextBox ClientIDMode="Static" ID="FirstNameTextbox" runat="server" CssClass="form-control" />
               </div>
               <div class="formHalf">
                  <label>Last Name</label>
                  <asp:TextBox ClientIDMode="Static" ID="LastNameTextbox" runat="server" CssClass="form-control" />
               </div>
               <div class="formHalf">
                  <label>Address</label>
                  <asp:TextBox ClientIDMode="Static" ID="AddressTextbox" CssClass="form-control" runat="server"></asp:TextBox>
               </div>
               <div class="formFourth">
                  <label>Unit</label>
                  <asp:TextBox ClientIDMode="Static" ID="AddressLine2Textbox" CssClass="form-control" Width="100px" runat="server"></asp:TextBox>
               </div>

               <div class="formHalf">
                  <label>City</label>
                  <asp:TextBox ClientIDMode="Static" ID="CityTextbox" runat="server" CssClass="form-control" />
               </div>
               <div class="formFourth">
                  <label>State</label>
                  <asp:DropDownList ID="StateDropdown" ClientIDMode="Static" runat="server" CssClass="form-control"></asp:DropDownList>
               </div>
               <div class="formFourth">
                  <label>Zip</label>
                  <asp:TextBox ClientIDMode="Static" ID="ZipTextbox" runat="server" CssClass="form-control" Width="100px" />
               </div>
            </div>

            <div class="formTint">
               <div class="formWrapper">
                  <h2>Contact Information</h2>
                  <div class="formThird">
                     <!--cakel: BUGID00069 Updated Cell Phone to Home Phone -->
                     <label>Home Phone</label>
                     <asp:TextBox ClientIDMode="Static" TextMode="Phone" ID="CellPhoneTextbox" runat="server" CssClass="form-control" />
                  </div>
                  <div class="formThird">
                     <!--cakel: BUGID00069 Updated Office Phone to Cell Phone -->
                     <label>Cell Phone</label>
                     <asp:TextBox ClientIDMode="Static" TextMode="Phone" ID="OfficePhoneTextbox" runat="server" CssClass="form-control" />
                  </div>
               </div>
            </div>

            <div class="formWrapper">
               <h2>PMS Information</h2>
               <div class="formThird">
                  <label>PMS ID</label>
                  <asp:TextBox ID="PMSIDTextbox" runat="server" CssClass="form-control"></asp:TextBox>
               </div>
               <div class="formThird">
                  <label>PMS Type</label>
                  <asp:DropDownList ID="PMSDropdown" runat="server" CssClass="form-control"></asp:DropDownList>
               </div>
            </div>

            <div class="button-footer">
               <div class="button-action">
                  <%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default" OnClick="btnCancel_Click" />--%>
               </div>
               <div class="main-button-action">
                  <asp:Button ID="SaveButton" runat="server" Text="Save" CssClass="btn btn-large footer-button" OnClick="SaveButtonClick" />
               </div>
               <div class="clear"></div>
            </div>
         </div>
         <!-- End Name Tab -->



         <asp:PlaceHolder ID="PayersContentPanel" runat="server">
            <div id="PayersInfo">
               <asp:UpdatePanel ID="PayersUpdatePanel" runat="server">
                  <ContentTemplate>
                     <h3>Account Information</h3>
                     <div class="profile-information">
                        <div class="full-name-labels">
                           <label class="title">Email Address/Username</label>
                        </div>
                        <div class="full-name-textboxes">
                           <asp:Button OnClientClick="return confirm('Are you sure you want to reset the password?');" ID="ResetRenterPassword" runat="server" CssClass="green-button float-right margin-left" Text="Reset" OnClick="ResetRenterPasswordClick" />
                           <asp:CheckBox ID="PayerTextNotificationsCheckbox" ClientIDMode="Static" runat="server" Text="Text Notifications" CssClass="float-right" />
                           <asp:CheckBox ID="PayerEmailNotificationsCheckbox" ClientIDMode="Static" runat="server" Text="Email Notifications" CssClass="float-right" />
                           <asp:TextBox ClientIDMode="Static" ID="PayersEmailTextbox" runat="server" CssClass="textbox" Width="240px" />
                        </div>
                     </div>
                     <h3 class="clear padding-top">Payers Information</h3>
                     <div class="profile-information">
                        <div class="full-name-labels">
                           <label class="fn" style="width: 174px;">First Name</label>
                           <label class="ln" style="width: 211px;">Last Name</label>
                        </div>
                        <div class="full-name-textboxes">
                           <asp:TextBox ClientIDMode="Static" ID="PayersFirstNameTextbox" runat="server" CssClass="textbox" Width="168px" />
                           <asp:TextBox ClientIDMode="Static" ID="PayersLastNameTextbox" runat="server" CssClass="textbox" Width="207px" />
                        </div>
                        <label class="clear">Address</label>
                        <asp:TextBox ClientIDMode="Static" ID="PayersAddressTextbox" CssClass="textbox w531" runat="server"></asp:TextBox>
                        <label class="clear">Unit</label>
                        <asp:TextBox ClientIDMode="Static" ID="PayersAddressLine2Textbox" CssClass="textbox w531" runat="server"></asp:TextBox>
                        <div class="renter-locale-labels">
                           <label class="city" style="width: 238px;">City</label>
                           <label class="state">State</label>
                           <label class="zip">Zip</label>
                        </div>
                        <div class="renter-locale-textboxes">
                           <asp:TextBox ClientIDMode="Static" ID="PayersCityTextbox" runat="server" CssClass="textbox" Width="233px" />
                           <asp:DropDownList ID="PayersStateDropdown" ClientIDMode="Static" runat="server" CssClass="state-dropdown"></asp:DropDownList>
                           <asp:TextBox ClientIDMode="Static" ID="PayersZipTextbox" runat="server" CssClass="textbox" Width="120px" />
                        </div>
                        <div class="renter-phone-labels">
                           <label class="cell-phone" style="width: 238px;">Cell Phone</label>
                           <label class="office-phone" style="width: 238px;">Office Phone</label>
                        </div>
                        <div class="renter-phone-textboxes">
                           <asp:TextBox ClientIDMode="Static" TextMode="Phone" ID="PayersCellPhoneTextbox" runat="server" CssClass="textbox phone" Width="233px" />
                           <asp:TextBox ClientIDMode="Static" TextMode="Phone" ID="PayersOfficePhoneTextbox" runat="server" CssClass="textbox phone" Width="233px" />
                        </div>
                     </div>
                     <div class="button-right">
                     </div>
                     <div class="button-footer">
                        <div class="button-action">
                           <button class="btn btn-default">Cancel</button>
                        </div>
                        <div class="main-button-action">
                           <asp:Button ID="SavePayerButton" runat="server" Text="Save" CssClass="btn btn-primary btn-large" OnClick="SavePayerButtonClick" />
                        </div>
                        <div class="clear"></div>
                     </div>
                  </ContentTemplate>
               </asp:UpdatePanel>
            </div>
         </asp:PlaceHolder>

         <!-- Begin Tab 2 -->

         <div id="PaymentInformationTab">
            <uc1:PaymentInfoTab ID="PaymentInfoTabControl" runat="server" />

         </div>

         <!-- Begin Tab 3 -->

         <div id="LeaseInformationTab">
            <%-- cakel: BUGID00085 - New User control added --%>
            <uc1:LeaseInfoV3 runat="server" ID="LeaseInfoV3" />
            <%--<uc1:LeaseInfo ID="LeaseInfoControl" runat="server" />--%>
         </div>

         <!-- Begin Tab 4 -->

         <div id="HistoryTab">
            <h3>History</h3>
            <div class="formTint" runat="server" id="AdminVoidECheckDiv" visible="false">
               <h3>
                  <asp:Label runat="server" ID="EChecklbl" Text="Void E-Check Payment" ForeColor="#662E6B"></asp:Label></h3>
               <br />
               <asp:Label runat="server" ID="Codelbl" Text="Enter Transaction Code:" Font-Bold="true"></asp:Label>
               <asp:TextBox runat="server" ID="VoidEChecktxt"></asp:TextBox>
               <asp:Button runat="server" ID="VoidECheckbtn" Text="Void E-Check" OnClick="VoidECheckbtn_Click" />
            </div>
            <div ng-include="'../resident.html'"></div>
         </div>

         <!-- Begin Tab 5 -->

         <div id="NotesTab">
            <uc1:ResidentNotes ID="ResidentNotesControl" runat="server" />
         </div>

         <!--dwillis Task 00076 -->
         <!--Begin Logging Tab-->
         <div id="LogTab">
            <uc1:UserLogging runat="server" ID="UserLogging" />
            <%-- <uc1:UserLogging ID="UserLoggingControl" runat="server" />--%>
         </div>

         <div id="TicketsTab">
            <uc1:CSRTicketing runat="server" ID="CSRTicketing" />
         </div>
      </div>
   </div>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="Scripts">
   <script src="<%= ResolveClientUrl("~/Scripts/promise-tracker.min.js") %>"></script>
   <script src="<%= ResolveClientUrl("~/Scripts/rpo-transactions.js") %>"></script>
   <script src="<%= ResolveClientUrl("~/Scripts/rpo-resident-payment-history.js") %>"></script>
   <script>
      $(function () {
         /* Mask */
         ApplySingleMask("#<%=ZipTextbox.ClientID%>", "00000");
         ApplySingleMask("#<%=CellPhoneTextbox.ClientID%>", "(000) 000-0000");
         ApplySingleMask("#<%=OfficePhoneTextbox.ClientID%>", "(000) 000-0000");

         ApplySingleMask("#<%=PayersZipTextbox.ClientID%>", "00000");
         ApplySingleMask("#<%=PayersCellPhoneTextbox.ClientID%>", "(000) 000-0000");
         ApplySingleMask("#<%=PayersOfficePhoneTextbox.ClientID%>", "(000) 000-0000");
      });
   </script>
</asp:Content>
