﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/frontend.master" CodeBehind="community-connect.aspx.cs" Inherits="Domus.Renters.community_connect" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TopHeadline" runat="server">
    <h1>Community Connect</h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div class="main-container">
        <div class="section-body">


            <%-- cakel: 00462 - fixed issues with visisble when not admin - Added new multiview --%>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                    <!--cakel:08/23/2016 made buttons invisible as per Sean -->
                    <asp:Button ID="JobPostbtn" runat="server" CssClass="btn btn-small" Text="Job Posting" OnClick="JobPostbtn_Click" Visible="false" />
                    <asp:Button ID="ResidentRoombtn" runat="server" CssClass="btn btn-small" Text="Resident's Room" OnClick="ResidentRoombtn_Click" Visible="false" />
                    <asp:Button ID="ProCornerbtn" runat="server" CssClass="btn btn-small" Text="Pro's Corner" OnClick="ProCornerbtn_Click" Visible="false" />
                    <asp:Button ID="NewsEventbtn" runat="server" CssClass="btn btn-small" Text="News & Events" OnClick="NewsEventbtn_Click" Visible="false" />
                    <asp:Button ID="Announcementsbtn" runat="server" CssClass="btn btn-small" Text="Announcements" OnClick="Announcementsbtn_Click" />
                    <asp:Button ID="Marketingbtn" runat="server" CssClass="btn btn-small" Text="Email Marketing" OnClick="Marketingbtn_Click" />
                    <asp:Button ID="SMSbtn" runat="server" CssClass="btn btn-small" Text="Text Marketing" OnClick="SMSbtn_Click" />

                    <a href="/rpo/events" class="btn btn-default"><span class="ctaIcon" data-icon="&#x64" aria-hidden="true"></span>Community Calendar</a>

                    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="4">
                        <asp:View ID="JobPostingView" runat="server">
                            <h2>Job Posts</h2>
                            <%-- <uc1:JobPosting ID="JobPosting" runat="server" />--%>
                        </asp:View>

                        <asp:View ID="ResidentsRoomView" runat="server">
                            <h2>Resident's Room</h2>
                            <%--  <uc1:ResidentsRoom ID="ResidentsRoom" runat="server" />--%>
                        </asp:View>


                        <asp:View ID="ProsCornerView" runat="server">
                            <h2>Pro's Corner</h2>
                            <%--  <uc1:ProsCorner ID="ProsCorner" runat="server" />--%>
                        </asp:View>

                        <asp:View ID="NewsAndEventsView" runat="server">
                            <h2>News & Events</h2>
                            <%--   <uc1:NewsAndEvents ID="NewsAndEvents" runat="server" />--%>
                        </asp:View>

                        <asp:View ID="AnnouncementView" runat="server">
                            <h2>Announcement</h2>
                            <uc1:AdminAnnouncements runat="server" ID="AdminAnnouncements" />
                        </asp:View>

                        <asp:View ID="EmailMarketingView" runat="server">
                            <uc1:EmailMarketing runat="server" ID="Marketing" />
                        </asp:View>

                        <asp:View ID="SMSMarketingView" runat="server">
                            <uc1:SMSMarketing runat="server" ID="SMSMarketing" />
                        </asp:View>

                    </asp:MultiView>

                </ContentTemplate>
            </asp:UpdatePanel>



        </div>
    </div>

</asp:Content>
