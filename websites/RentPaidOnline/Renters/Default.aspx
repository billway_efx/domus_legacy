﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Domus.Renters.Default" %>
<%@ Import Namespace="EfxFramework.ExtensionMethods" %>
<%@ Import Namespace="Mb2x.ExtensionMethods" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script> $(function () { EFXClient.SetSelectedNavigation('renters-link'); }); </script>
    <div id="PropertyDetails">
        
        <div class="float-wrap green-gradient-wrap add-property-wrap">
            <div class="float-right">
                <div class="float-right" style="margin-left: 10px;">
                    <asp:Button ID="ClearButton" runat="server" CssClass="green-button float-right" OnClick="ClearButtonClick" Text="Clear" style="position: relative;left: 5px;" />
                    <asp:Button ID="SearchButton" runat="server" CssClass="search-button float-right" Text="Search" AutoPostBack="true"/>
                    <asp:TextBox CssClass="float-right" ID="SearchTextbox" runat="server" Width="200px"></asp:TextBox>
                </div>
                <label>Sort by:</label>
                <asp:DropDownList ID="SortByDropdown" runat="server" >
                    <asp:ListItem Text="-- No Sort --" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="A-Z" Value="ASC"></asp:ListItem>
                    <asp:ListItem Text="Z-A" Value="DESC"></asp:ListItem>
                </asp:DropDownList>
                <label>Filter by:</label>
                <asp:DropDownList ID="FilterByDropdown" runat="server" >
                    <asp:ListItem Text="-- No Filter --" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="Active" Value="Active"></asp:ListItem>
                    <asp:ListItem Text="Not Active" Value="InActive"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        

        <div class="float-left" style="width: 255px;">
            <h3 class="icon padding-top"><img src="/Images/leaf.png" alt="Selected Property" />Selected Property</h3>
            <asp:Panel ID="FeaturedProperyPanel" runat="server">
                <div class="featured-property-listitem" style="margin-top: 10px;">
                    <div class="property-wrap green-gradient-wrap" style="width: 285px;">
                        <h4><%=CurrentProperty.PropertyName %></h4>
                        <div class="clear">
                            <p class="padding-bottom"><%= EfxFramework.Property.GetIntegrationStatus(CurrentProperty.PropertyId) %></p>
                            <p class="padding-bottom">Participating Residents: <%=EfxFramework.Renter.GetPercentageOfParticipatingRentersByPropertyId(CurrentProperty.PropertyId.HasValue
                                                                             ? CurrentProperty.PropertyId.Value
                                                                             : 1).ToString("P") %></p>
                            <p class="padding-bottom">Amount Collected this Month: <%= EfxFramework.Property.GetRentCollectedByMonthByPropertyId(DateTime.UtcNow.Month, CurrentProperty.PropertyId.HasValue ? CurrentProperty.PropertyId.Value : 0).ToString("C") %></p>
                            <p class="padding-bottom">Outstanding Rent: <%= EfxFramework.Property.GetOutstandingRentByPropertyId(CurrentProperty.PropertyId.HasValue ? CurrentProperty.PropertyId.Value : 0).ToString("C") %></p>
                            <p class="padding-bottom">Leasing Agents: <%= EfxFramework.Property.GetNumberOfLeasingAgentsByPropertyId(CurrentProperty.PropertyId.HasValue ? CurrentProperty.PropertyId.Value : 0) %></p>
                            <p class="padding-bottom">Number of Units: <%=CurrentProperty.NumberOfUnits %></p>
                            <p class="padding-bottom padding-top">
                                <a style="text-decoration: underline" href="/Reporting/Default.aspx?reportPath=RentersPastDueReportPath">Residents Past Due</a>
                                <a class="magnifier" href='<%= String.Concat("/MyProperties/PropertyDetails.aspx?propertyId=", CurrentProperty.PropertyId) %>'><img src="/Images/magnifier.png" alt="View Details" /></a>
                            </p>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:UpdatePanel ID="LeftSideUpdatePanel" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="SearchPropertyButton" EventName="Click" />
                </Triggers>
                <ContentTemplate>
                    <div class="clear padding-top" style="margin-top: 10px;">
                        <a href="/MyProperties/NewProperty.aspx" class="green-button float-right">Add New<span class="button-icon">+</span></a>
                        <h3>Other Properties</h3>
                        <div class="clear padding-top padding-bottom" style="width: 300px;">
                        <asp:Button ID="ClearPropertySearchButton" runat="server" CssClass="green-button float-right" Text="Clear" style="position: relative;left: 5px;" OnClick="ClearPropertySearchButtonClick" />
                        <asp:Button ID="SearchPropertyButton" runat="server" CssClass="search-button float-right" Text="Search" OnClick="SearchPropertyButtonClick" />
                        <asp:TextBox CssClass="float-right" ID="SearchPropertyTextbox" runat="server" Width="150px"></asp:TextBox>
                        </div>
                    </div>
                    <asp:Repeater ID="PropertiesRepeater" runat="server">
                        <HeaderTemplate>
                            <div id="PropertySingleList">
                        </HeaderTemplate>
                        <FooterTemplate></div></FooterTemplate>
                        <ItemTemplate>
                            <div class="property-listitem" style="width: 300px">
                                <div class="property-wrap">
                                    <h4><%# Eval("PropertyName") %></h4>
                                    <div class="clear padding-top">
                                        <p class="padding-bottom"><%# EfxFramework.Property.GetIntegrationStatus(Eval("PropertyId").ToString().ToInt32()) %></p>
                                        <p class="padding-bottom">Participating Residents: <%# EfxFramework.Renter.GetPercentageOfParticipatingRentersByPropertyId(Eval("PropertyId").ToString().ToInt32()).ToString("P") %></p>
                                        <p class="padding-bottom">Amount Collected this Month: <%# EfxFramework.Property.GetRentCollectedByMonthByPropertyId(DateTime.UtcNow.Month, Eval("PropertyId").ToString().ToInt32()).ToString("C") %></p>
                                        <p class="padding-bottom">Outstanding Rent: <%# EfxFramework.Property.GetOutstandingRentByPropertyId(Eval("PropertyId").ToString().ToInt32()).ToString("C") %></p>
                                        <p class="padding-bottom">Leasing Agents: <%# EfxFramework.Property.GetNumberOfLeasingAgentsByPropertyId(Eval("PropertyId").ToString().ToInt32()) %></p>
                                        <p class="padding-bottom">Number of Units: <%# Eval("NumberOfUnits") %></p>
                                        <p class="padding-bottom padding-top">
                                            <a style="text-decoration: underline" href="/Reporting/Default.aspx?reportPath=RentersPastDueReportPath">Residents Past Due</a>
                                            <a class="magnifier" href="<%# string.Concat("/Renters/Default.aspx?propertyId=", Eval("PropertyId")) %>"><span>View</span><img src="/Images/magnifier.png" alt="View Details" /></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="float-right" style="width: 720px">         
            <div class="green-gradient-wrap padding float-wrap padding" style="margin-top: 25px; padding-top: 10px;">
                <h2><%=CurrentProperty.PropertyName %></h2>
                <div class="float-wrap">
                <asp:Panel ID="RentersPanel" runat="server" Height="1200" Width="668" ScrollBars="Auto">
                <asp:Repeater ID="RentersRepeater" runat="server">
                    <HeaderTemplate>
                        <div id="RenterSingleList">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="renter-listitem" style="width: 290px;">
                            <div class="renter-wrap">
                                <img src="<%# string.Concat("/Handlers/EFXAdminHandler.ashx?RequestType=0&renterId=", Eval("RenterId")) %>" alt="Picture" class="orange float-right" />
                                <h4><%# Eval("DisplayName") %></h4>
                                <div class="padding-top">
                                    <p class="padding-bottom breakword">ID: <%# Eval("PublicRenterId") %></p>
                                    <p class="padding-bottom breakword"><%# Eval("MainPhoneNumber").ToString().FormatPhoneNumber() %></p>
                                    <p class="padding-bottom breakword"><%# Eval("PrimaryEmailAddress")  %></p>
                                    <p class="padding-bottom padding-top">
                                        <a class="magnifier" href="<%# string.Concat("/Renters/RenterDetails.aspx?renterId=", Eval("RenterId")) %>"><span>View</span><img src="/Images/magnifier.png" alt="View Details" /></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        <p class="padding-bottom text-center padding-top margin-top clear">
                            <a href="/Users/NewUser.aspx?t=renter&pid=<%=CurrentProperty.PropertyId.Value %>" class="green-button">Add Resident to <%=CurrentProperty.PropertyName%><span class="button-icon">+</span></a>
                        </p>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
                </asp:Panel>
                </div>
                <div class="buttons float-wrap padding" style="display: none;">
                    <a href="#" class="green-button float-right">View All</a>
                    <a href="#" class="green-button float-right">Next</a>
                </div>
                <asp:Panel ID="NoItems" runat="server" Visible="false">
                    <div class="empty-records">No renters have been added.
                        <p class="padding-top padding-bottom">
                            <a href="/Users/NewUser.aspx?t=renter&pid=<%=CurrentProperty.PropertyId.Value %>" class="green-button">Add Resident to <%=CurrentProperty.PropertyName%><span class="button-icon">+</span></a>
                        </p>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
