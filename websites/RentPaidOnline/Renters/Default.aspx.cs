﻿using EfxFramework;
using Mb2x.ExtensionMethods;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Domus.Renters
{
    public partial class Default : System.Web.UI.Page
    {
        const string CacheKey = "CURRENT_VIEWINGHOME_PROPERTY";
        const string CacheSearchKey = "CURRENT_VIEWINGHOME_SEARCH";

        List<Property> _ListOfProperties;
        List<Property> CurrentProperties
        {
            get { return _ListOfProperties ?? (_ListOfProperties = Property.GetAllPropertyList()); }
        }

        Property _Property;
        public Property CurrentProperty
        {
            get
            {
                if (_Property == null && ViewState[CacheKey] != null)
                {
                    var Id = ViewState[CacheKey].ToString().ToInt32();
                    _Property = new Property(Id);
                }
                else if (_Property == null && !String.IsNullOrEmpty(Request.QueryString["propertyId"]))
                {
                    // Just grab first one
                    var Pid = Request.QueryString["propertyId"].ToNullInt32();
                    if (Pid.HasValue)
                    {
                        _Property = new Property(Pid.Value);
                        ViewState[CacheKey] = _Property.PropertyId;
                    }
                }
                else if (_Property == null)
                {
                    // Just grab first one
                    _Property = CurrentProperties.First();
                }

                return _Property;
            }
            set
            {
                ViewState[CacheKey] = value.PropertyId;
            }
        }

        string _Search = string.Empty;
        string CurrentSearch
        {
            get
            {
                if (_Search.Equals(string.Empty) && ViewState[CacheSearchKey] != null)
                {
                    _Search = ViewState[CacheSearchKey].ToString();
                }

                return _Search;
            }
            set
            {
                ViewState[CacheSearchKey] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Salcedo - 4/12/2014 - we shouldn't be using this page anymore
            //Evidently we still have some code paths that bring us here. 
            //We need to find all those paths and send them to the correct location.
            Response.Redirect("/Default.aspx", false);
        }

        protected override void OnPreRender(EventArgs e)
        {
            var RenterList = new List<Renter>();

            try
            {
                base.OnPreRender(e);

                if (CurrentProperty.PropertyId.HasValue)
                {
                    foreach (var R in Renter.GetRenterListByPropertyId(CurrentProperty.PropertyId.Value))
                    {
                        if (R.PrimaryEmailAddress.ToLower().Contains("unknown.com"))
                            R.PrimaryEmailAddress = "Email Unavailable";

                        RenterList.Add(R);
                    }

                    RentersRepeater.DataSource = SortAndFilterRenterList(RenterList);
                    RentersRepeater.DataBind();


                    RentersRepeater.Visible = RenterList.Count > 0;
                    NoItems.Visible = RenterList.Count == 0;

                    // Search filter
                    var Properties = CurrentProperties;
                    if (!String.IsNullOrEmpty(CurrentSearch))
                    {
                        Properties = Properties.Where(p => p.PropertyName.IndexOf(CurrentSearch.ToLower(), StringComparison.OrdinalIgnoreCase) >= 0).ToList();
                    }
                    PropertiesRepeater.DataSource = Properties.Where(p => p.PropertyId.Value != CurrentProperty.PropertyId.Value).OrderBy(p => p.PropertyName).Take(EfxSettings.DisplayedItemsCount).ToList();
                    PropertiesRepeater.DataBind();
                }
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }


        protected void ClearPropertySearchButtonClick(object sender, EventArgs e)
        {
            try
            {
                CurrentSearch = string.Empty;
                SearchPropertyTextbox.Text = string.Empty;
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }

        protected void SearchPropertyButtonClick(object sender, EventArgs e)
        {
            try
            {
                var Term = SearchPropertyTextbox.Text.Trim().ToLower();

                CurrentSearch = Term;
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }

        protected void ClearButtonClick(object sender, EventArgs e)
        {
            //Reset Dropdowns
            SortByDropdown.SelectedIndex = 0;
            FilterByDropdown.SelectedIndex = 0;

            //Clear textbox
            SearchTextbox.Text = string.Empty;
        }

        protected IEnumerable SortAndFilterRenterList(List<Renter> renterList)
        {
            var ModifiedRenterList = new List<Renter>();

            //Filter list by search string
            if (!string.IsNullOrEmpty(SearchTextbox.Text))
            {
                var SearchStrings = SearchTextbox.Text.Split(' ');

                if (SearchStrings.Length == 1)
                {
                    ModifiedRenterList = (from R in renterList
                                          where R.FirstName.StartsWith(SearchStrings[0], StringComparison.OrdinalIgnoreCase) ||
                                                R.LastName.StartsWith(SearchStrings[0], StringComparison.OrdinalIgnoreCase)
                                          select R).ToList();
                }
                else if (SearchStrings.Length >= 2)
                {
                    if (SearchStrings[0].EndsWith(","))
                    {
                        ModifiedRenterList = (from R in renterList
                                              where R.LastName.StartsWith(SearchStrings[0].Trim(','), StringComparison.OrdinalIgnoreCase) &&
                                                    R.FirstName.StartsWith(SearchStrings[1], StringComparison.OrdinalIgnoreCase)
                                              select R).ToList();
                    }
                    else
                    {
                        ModifiedRenterList = (from R in renterList
                                              where R.FirstName.StartsWith(SearchStrings[0], StringComparison.OrdinalIgnoreCase) &&
                                                    R.LastName.StartsWith(SearchStrings[1], StringComparison.OrdinalIgnoreCase)
                                              select R).ToList();
                    }
                }
            }
            else
            {
                ModifiedRenterList = renterList.ToList();
            }

            //Filter active/inactive users
            if (string.Equals(FilterByDropdown.SelectedValue, "Active", StringComparison.OrdinalIgnoreCase))
                ModifiedRenterList.RemoveAll(r => r.IsActive = false);
            else if (string.Equals(FilterByDropdown.SelectedValue, "InActive", StringComparison.OrdinalIgnoreCase))
                ModifiedRenterList.RemoveAll(r => r.IsActive = true);

            //Sort list alphebetically
            if (string.Equals(SortByDropdown.SelectedValue, "ASC", StringComparison.OrdinalIgnoreCase) && ModifiedRenterList.Any())
                return ModifiedRenterList.OrderBy(r => r.LastName);
            if (string.Equals(SortByDropdown.SelectedValue, "DESC", StringComparison.OrdinalIgnoreCase) && ModifiedRenterList.Any())
                return ModifiedRenterList.OrderByDescending(r => r.LastName);

            return ModifiedRenterList;
        }
    }
}