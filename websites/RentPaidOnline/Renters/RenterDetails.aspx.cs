﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework;
using EfxFramework.EmailTemplateService;
using EfxFramework.ExtensionMethods;
using EfxFramework.Web;
using EfxFramework.Web.Facade;
using Mb2x.ExtensionMethods;

namespace RentPaidOnline.Renters
{
    public partial class RenterDetails : BasePage
    {
        private const string CacheKey = "CURRENT_EDITING_RENTER";
        private const string CacheSearchKey = "CURRENT_RENTERDETAILS_SEARCH";


        public bool IsRenterPayer => false;

        private Payer CurrentPayer => CurrentRenter?.PayerId != null ? new Payer(CurrentRenter.PayerId.Value) : new Payer();

        public Renter CurrentRenter
        {
            get
            {
                var id = HttpContext.Current.Request.QueryString["renterId"].ToNullInt32();
                return !id.HasValue ? new Renter() : new Renter(id.Value);
            }
            set => ViewState[CacheKey] = value.RenterId;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
				var manager = ScriptManager.GetCurrent(Page);
                manager?.RegisterPostBackControl(SaveButton);

                EmailDupeLabel.Visible = false; // To accomodate ExecuteScalar Error for duplicate email entries.  04.24.2020 - BW

                // Return home if no renter id to view
                if (CurrentRenter == null || CurrentRenter.RenterId <= 0)
                {
                    BasePage.SafePageRedirect("/Default.aspx");
                }

                if (EfxFramework.Helpers.Helper.IsCsrUser)
                {
                    FirstNameTextbox.Enabled = false;
                    LastNameTextbox.Enabled = false;
                    AddressTextbox.Enabled = false;
                    AddressLine2Textbox.Enabled = false;
                    CityTextbox.Enabled = false;
                    StateDropdown.Enabled = false;
                    ZipTextbox.Enabled = false;
                    CellPhoneTextbox.Enabled = false;
                    OfficePhoneTextbox.Enabled = false;
                    PMSIDTextbox.Enabled = false;
                    PMSDropdown.Enabled = false;
                }

                var isAdmin = EfxFramework.Helpers.Helper.IsRpoAdmin;
                var adminName = HttpContext.Current.Session["CurrentUserNameEmail"].ToString().ToLower();

                if(isAdmin)
                {
                    // var rpoAdmin =  entity.EfxAdministrators.FirstOrDefault(admin => admin.EfxAdministratorId == CurrentUserID);
                    if (adminName == "aorr@efxfs.com" || adminName == "dbelford@efxfs.com" ||
                        adminName == "bway@efxfs.com" || adminName == "mbermudez@efxfs.com" || 
                        adminName == "support@rentpaidonline.com")
                    {
                        AdminVoidECheckDiv.Visible = true;
                    }
                }

                if (!IsPostBack)
                {
                    StateDropdown.BindToSortedEnum(typeof(StateProvince));
                    PayersStateDropdown.BindToSortedEnum(typeof(StateProvince));
                    PMSDropdown.BindToSortedEnum(typeof(PmsType));
                    PMSDropdown.Items.Insert(0, new ListItem("-- Select PMS --", "-1"));
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
	            Response.Redirect("~/ErrorPage.html", false);
            }

            //cakel: BUGID00226 rev2
            var id = Request.QueryString["renterId"];
            if (id != null)
            {
                PopulateRenterDetailsHeader(id);
            }
        }
        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                base.OnPreRender(e);

                if (IsPostBack) return;

                LoadData();
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }

		private void LoadData()
		{
			NameTab.Text = CurrentRenter.DisplayName;

			PMSIDTextbox.Text = !String.IsNullOrEmpty(CurrentRenter.PmsId) ? CurrentRenter.PmsId : string.Empty;

			// Bind PMS info
			if (CurrentRenter.PmsTypeId.HasValue)
			{
				PMSDropdown.SelectedValue = CurrentRenter.PmsTypeId.Value.ToString(CultureInfo.InvariantCulture);
			}
            
            BindAccountInformationTab();
			BindPayerInformationTab();

			PayersContentPanel.Visible = IsRenterPayer;
			PayersTabPanel.Visible = IsRenterPayer;

            //cakel: 00364 - Added this to make sure the gridview refreshes when this is called after a save
            UserLogging.popGrid(CurrentRenter.RenterId);
		}
        // Patrick Whittingham - 03/25/2015 - Task # 000388 : Fix error which shows a system error on the page to show a validation error.
        private void ShowErrorMessage(string message)
        {
            var err = new CustomValidator
            {
                ValidationGroup = "RenterDtl"
                , IsValid = false
                , ErrorMessage = message
            };
            Page.Validators.Add(err);
        }
        private void BindAccountInformationTab()
        {
            FirstNameTextbox.Text = CurrentRenter.FirstName;
            LastNameTextbox.Text = CurrentRenter.LastName;
            EmailTextbox.Text = CurrentRenter.PrimaryEmailAddress;
            AddressTextbox.Text = CurrentRenter.StreetAddress;
            AddressLine2Textbox.Text = CurrentRenter.Unit;
            CityTextbox.Text = CurrentRenter.City;

            if (CurrentRenter.StateProvinceId.HasValue)
                StateDropdown.SelectedValue = CurrentRenter.StateProvinceId.Value.ToString(CultureInfo.InvariantCulture);

            ZipTextbox.Text = CurrentRenter.PostalCode;
            CellPhoneTextbox.Text = CurrentRenter.MobilePhoneNumber;
            OfficePhoneTextbox.Text = CurrentRenter.MainPhoneNumber;
            EmailNotificationsCheckbox.Checked = CurrentRenter.EmailNotifications.HasValue && CurrentRenter.EmailNotifications.Value;
            TextNotificationsCheckbox.Checked = CurrentRenter.TextNotifications.HasValue && CurrentRenter.TextNotifications.Value;
        }

        private void BindPayerInformationTab()
        {
            if (CurrentPayer != null)
            {
                PayersFirstNameTextbox.Text = CurrentPayer.FirstName;
                PayersLastNameTextbox.Text = CurrentPayer.LastName;
                PayersAddressTextbox.Text = CurrentPayer.StreetAddress;
                PayersAddressLine2Textbox.Text = CurrentPayer.StreetAddress2;
                PayersCityTextbox.Text = CurrentPayer.City;

                if (CurrentPayer.StateProvinceId.HasValue)
                    PayersStateDropdown.SelectedValue = CurrentPayer.StateProvinceId.Value.ToString(CultureInfo.InvariantCulture);

                PayersZipTextbox.Text = CurrentPayer.PostalCode;
                PayersCellPhoneTextbox.Text = CurrentPayer.MobilePhoneNumber;
                PayersOfficePhoneTextbox.Text = CurrentPayer.MainPhoneNumber;
                PayersEmailTextbox.Text = CurrentPayer.PrimaryEmailAddress;
            }
            else
            {
                // Patrick Whittingham - 03/25/2015 - Task # 000388 : Fix error which shows a system error on the page to show a validation error.
                ShowErrorMessage("Payer is not set for this renter.");
            }
        }

        protected void SaveButtonClick(object sender, EventArgs e)
        {
            var Unit = string.Empty;
            try
            {
                var property = Property.GetPropertyByRenterId(CurrentRenter.RenterId);

                //CMallory - Task 00501 - Added If Statement
                Unit = AddressLine2Textbox.Text.Contains("&") ? AddressLine2Textbox.Text.Replace("&", "") : AddressLine2Textbox.Text;

                var newRenterInfo = new Renter(CurrentRenter.RenterId)
                {
                    FirstName = FirstNameTextbox.Text,
                    LastName = LastNameTextbox.Text,
                    StreetAddress = AddressTextbox.Text,
                    Unit = Unit,
                    City = CityTextbox.Text,
                    StateProvinceId = StateDropdown.SelectedValue.ToNullInt32(),
                    PostalCode = ZipTextbox.Text,
                    MobilePhoneNumber = CellPhoneTextbox.Text,
                    MainPhoneNumber = OfficePhoneTextbox.Text,
                    TextNotifications = TextNotificationsCheckbox.Checked,
                    EmailNotifications = EmailNotificationsCheckbox.Checked,
                    PrimaryEmailAddress = EmailTextbox.Text,
                    PmsId = PMSIDTextbox.Text
                };

                if (PMSDropdown.SelectedValue != "-1")
                {
                    newRenterInfo.PmsTypeId = PMSDropdown.SelectedValue.ToNullInt32();

                    if (property.PropertyId.HasValue && property.PropertyId.Value > 0)
                        newRenterInfo.PmsPropertyId = property.PmsId;
                }

                Renter.Set(newRenterInfo, UIFacade.GetAuthenticatedUserString(UserType.PropertyStaff));
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }
        protected void ResetRenterPasswordClick(object sender, EventArgs e)
        {
            try
            {
                PageMessage.DisplayMessage(SystemMessageType.Success, "Password has been reset.");
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }

        protected void SendEmail(Renter renter, string password)
        {
            if (string.IsNullOrEmpty(renter.PrimaryEmailAddress))
                return;

            //Salcedo - 3/12/2014 - replaced with new html email template
            var message = string.Format(EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-password-reset.html").Replace("[[ROOT]]",
                EmailTemplateService.RootPath), renter.DisplayName, password);

            EfxFramework.BulkMail.BulkMail.SendMailMessage(
                null,
                "EFX",
                false,
                EfxSettings.SendMailUsername,
                EfxSettings.SendMailPassword,
                new MailAddress(EfxSettings.SupportEmail),
                "Forgot Password",
                message,
                new List<MailAddress> { new MailAddress(renter.PrimaryEmailAddress) });
        }
        protected void SavePayerButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(PayersEmailTextbox.Text))
                {
                    // Patrick Whittingham - 03/25/2015 - Task # 000388 : Fix error which shows a system error on the page to show a validation error.
                    ShowErrorMessage("Email Address is required.");
                    return;
                }

                if (CurrentPayer != null)
                {
                    var payer = new Payer(CurrentPayer.PayerId) 
                    {
                        FirstName = PayersFirstNameTextbox.Text, 
                        LastName = LastNameTextbox.Text, 
                        StreetAddress = PayersAddressTextbox.Text, 
                        StreetAddress2 = PayersAddressLine2Textbox.Text, 
                        City = PayersCityTextbox.Text, 
                        StateProvinceId = PayersStateDropdown.SelectedValue.ToNullInt32(), 
                        PostalCode = PayersZipTextbox.Text, 
                        MobilePhoneNumber = PayersCellPhoneTextbox.Text, 
                        MainPhoneNumber = PayersOfficePhoneTextbox.Text, 
                        EmailNotifications = PayerEmailNotificationsCheckbox.Checked, 
                        TextNotifications = PayerTextNotificationsCheckbox.Checked, 
                        PrimaryEmailAddress = PayersEmailTextbox.Text
                    };

                    Payer.Set(payer);

                    PageMessage.DisplayMessage(SystemMessageType.Success, "Successfully updated payer information.");
                }
                else
                {
                    // Patrick Whittingham - 03/25/2015 - Task # 000388 : Fix error which shows a system error on the page to show a validation error.
                    ShowErrorMessage("Payer is not set for this renter.");
                }
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }
        protected void DeleteButtonClick(object sender, EventArgs e)
        {
            try
            {
                Renter.DeleteRenterByRenterId(CurrentRenter.RenterId, UIFacade.GetAuthenticatedUserString(UserType.EfxAdministrator));
                SafeRedirect("Default.aspx");
            }
            catch (SqlException ex)
            {
                if (ex.Number == 547) {
                    // Patrick Whittingham - 03/25/2015 - Task # 000388 : Fix error which shows a system error on the page to show a validation error.
                    ShowErrorMessage("You cannot delete renters that have transactions associated with them..");
                }
                else
                {
                    throw;
                }
            }
            catch
            {
                SafeRedirect("~/ErrorPage.html");
            }
        }

        public void PopulateRenterDetailsHeader(string renterId)
        {
            var reader = GetRenterDetailsReader(renterId);

          var readReader = reader.Read();
          if (!reader.HasRows) return;
          var fullName = reader["FirstName"] + " " + reader["LastName"];
            RenterNamelbl.Text = fullName;
            PropertyNamelbl.Text = reader["PropertyName"].ToString();
            UnitNumberlbl.Text = reader["Unit"].ToString();
            RentAmountlbl.Text = $@"{reader["RentAmount"]:c}";
                
            PublicResidentIDlbl.Text = reader["PublicRenterId"].ToString();
            PMSIDlbl.Text = reader["PmsId"].ToString();
            PropertyManagerNamelbl.Text = reader["PropertManagerName"].ToString();
            PropertyPhonelbl.Text = reader["PropertyPhone"].ToString();
            HasRegisteredlbl.Text = reader["HasRegistered"].ToString();
            AcceptedPaymentlbl.Text = reader["AcceptedPaymentType"].ToString();
        }

        //cakel: BUGID00226 rev2
        public SqlDataReader GetRenterDetailsReader(string _RenterID)
        {
            var id = int.Parse(_RenterID);
            var con = new SqlConnection(ConnString);
            var com = new SqlCommand("usp_Renter_GetRenterDetails_V3", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            com.Parameters.Add("@RenterID", SqlDbType.Int).Value = id;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
                
            //catch (Exception ex)
            catch
            {
              con.Close();
              return null;
            }
        }

        private static string ConnString => System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        protected void VoidECheckbtn_Click(object sender, EventArgs e)
        {
            Payment.AdminVoidEcheck(VoidEChecktxt.Text);
            VoidEChecktxt.Text = null;
        }

        protected void EmailTextbox_TextChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable("primaryemailaddresses");
            Renter renter = CurrentRenter;
            
            cmd.CommandText = "select primaryemailaddress from renter where primaryemailaddress = '" +
                              EmailTextbox.Text + "'";
            cmd.Connection = con;

            con.Open();
            da.SelectCommand = cmd;
            da.Fill(dt);
            con.Close();

            string currentEmailAddress = renter.PrimaryEmailAddress;

            if (dt.Rows.Count == 1)
            {
                if (dt.Rows[0].ItemArray[0].ToString() == EmailTextbox.Text)
                {
                    string sMsg = "Email address already exists, reverting to previous entry.";
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), Guid.NewGuid().ToString(),
                    //    "alert('" + sMsg + "')", true);
                    EmailDupeLabel.Text = sMsg;
                    EmailDupeLabel.Visible = true;
                    EmailTextbox.Text = currentEmailAddress;
                }
            }
            
            
        }
    }
}