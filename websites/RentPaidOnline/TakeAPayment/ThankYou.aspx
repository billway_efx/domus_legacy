﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ThankYou.aspx.cs" Inherits="Domus.TakeAPayment.ThankYou" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="thank-you">
	     <div class="pageMessage">
	        <h3>Payment Status:</h3>
        </div>
	    <div class="transMessage">
	        <h4><%= TransactionMessage %></h4>
        </div>
		<asp:MultiView ID="RentMultiView" runat="server">
			<asp:View ID="RentSuccessView" runat="server">
			    <div class="RentSuccessDisplay">
			        <h5>Thank You!</h5>
                    <p class="paymentText">
                        <span class="transRec">Rent and Fees transaction accepted</span><br />
                        <span class="subTitle">Transaction ID:</span><br />
                        <%= RentAndFeeTransactionId %><br /><br />
                        <span class="subTitle">Convenience Fee:</span><br />
                        <%= ConvenienceFee.HasValue ? ConvenienceFee.Value.ToString("C") : "$0.00" %><br /><br />
                        An email has been sent to the resident and the property management company.
                    </p>
			    </div>
			</asp:View>
			<asp:View ID="RentFailureView" runat="server">
            <div class="RentFailureDisplay">
                    <h5>Unsuccessful Payment</h5>
                    <p class="paymentText">
                        <span class="failed">Rent and Fees transaction failed</span><br />
                        <span class="subTitle">Transaction ID:</span><br />
                        <%= RentAndFeeTransactionId %><br /><br />
                    </p>
			    </div>
            </asp:View>
            <asp:View ID="RentNoPayment" runat="server"></asp:View>
		</asp:MultiView>
		<asp:MultiView ID="CharityMultiView" runat="server">
		    <asp:View ID="CharitySuccessView" runat="server">
		        <div class="CharitySuccessDisplay">
		            <h5>Thank You!</h5>
                     <p class="paymentText">
                        <span class="transRec">Charity transaction accepted</span><br />
                        <span class="subTitle">Transaction ID:</span><br />
                        <%= CharityTransactionId %><br /><br />
                    </p>
			    </div>
		    </asp:View>
			<asp:View ID="CharityFailureView" runat="server">
			        <div class="CharityFailureDisplay">
			        <h5>Unsuccessful Payment</h5>
                    <p class="paymentText">
                        <span class="failed">Charity transaction failed</span><br />
                        <span class="subTitle">Transaction ID:</span><br />
                        <%= CharityTransactionId %>
                    </p>
			    </div>
			</asp:View>
            <asp:View ID="CharityNoPayment" runat="server"></asp:View>
		</asp:MultiView>
        <div class="OTPlogoHolder"></div>
	</div>        
</asp:Content>