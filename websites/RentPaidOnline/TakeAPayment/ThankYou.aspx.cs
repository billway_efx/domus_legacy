﻿using EfxFramework.PaymentMethods;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Web.UI.WebControls;

namespace Domus.TakeAPayment
{
    public partial class ThankYou : BasePage
    {
        //Properties
        public string TransactionMessage
        {
            get
            {
                var RentStateId = Request.QueryString["RentStateId"].ToNullInt32();
                var CharityStateId = Request.QueryString["CharityStateId"].ToNullInt32();

                if ((RentStateId.HasValue && RentStateId.Value > 0) && (CharityStateId.HasValue && CharityStateId > 0))
                    return "Two transactions were processed.";

                return "Your transaction was processed.";
            }
        }
        public string RentAndFeeResult
        {
            get
            {
                var Result = (GeneralResponseResult)Enum.Parse(typeof(GeneralResponseResult), Request.QueryString["RentAndFeeResult"]);
                return Result == GeneralResponseResult.Success ? "Your Rent paymet has been successfully processed." : "Your Rent payment failed to process.";
            }
        }
        public string RentAndFeeResponseMessage
        {
            get { return Request.QueryString["RentAndFeeResponseMessage"]; }
        }
        public string RentAndFeeTransactionId
        {
            get { return Request.QueryString["RentAndFeeTransactionId"]; }
        }
        public decimal? ConvenienceFee
        {
            get { return Request.QueryString["ConvenienceFee"].ToNullDecimal(); }
        }
        public string CharityResult
        {
            get
            {
                var Result = (GeneralResponseResult)Enum.Parse(typeof(GeneralResponseResult), Request.QueryString["CharityResult"]);
                return Result == GeneralResponseResult.Success ? "Your Charity paymet has been successfully processed." : "Your Charity payment failed to process.";
            }
        }
        public string CharityResponseMessage
        {
            get { return Request.QueryString["CharityResponseResult"]; }
        }
        public string CharityTransactionId
        {
            get { return Request.QueryString["CharityTransactionId"]; }
        }
        public View RentViewToDisplay
        {
            get
            {
                //Determine the state of the transaction
                var StateId = Request.QueryString["RentStateId"].ToNullInt32();

                switch (StateId)
                {
                    case 1:
                        return RentSuccessView;
                    case 2:
                        return RentFailureView;
                    default:
                        return RentNoPayment;
                }
            }
        }

        public View CharityViewToDisplay
        {
            get
            {
                //Determine the state of the transaction
                var StateId = Request.QueryString["CharityStateId"].ToNullInt32();

                switch (StateId)
                {
                    case 1:
                        return CharitySuccessView;
                    case 2:
                        return CharityFailureView;
                    default:
                        return CharityNoPayment;
                }
            }
        }
        //Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            //Set the appropriate view
            RentMultiView.SetActiveView(RentViewToDisplay);
            CharityMultiView.SetActiveView(CharityViewToDisplay);
        }
    }
}