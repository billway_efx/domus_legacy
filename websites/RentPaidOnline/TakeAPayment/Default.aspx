﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Domus.TakeAPayment.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script> $(function () { EFXClient.SetSelectedNavigation('onetimepayment-link'); }); </script>
    <div id="OneTimePaymentWrap">
        <h3 class="icon"><img src="/images/leaf.png" alt="Take a Payment" />Take a Payment</h3>
            <asp:MultiView ID="PaymentMultiView" runat="server">
                <asp:View ID="StepOneView" runat="server">
                    <div class="float-wrap">
                        <div id="StepOne">
                            <uc1:OneTimePaymentStep1 ID="OneTimePaymentStepOne" runat="server" />
                            <div class="float-right" style="width: 645px">
                                <div class="buttons float-wrap padding">
                                    <asp:Button ID="NextButton" runat="server" CssClass="green-button float-right" Text="Next" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>
                <asp:View ID="StepTwoView" runat="server">
                    <div class="float-wrap">
                        <div id="StepTwo" style="padding-bottom: 63px;">
                            <uc2:OneTimePaymentStep2 ID="OneTimePaymentStepTwo" runat="server" />
                            <div class="buttons float-right padding-top padding-bottom">
                                <asp:Button ID="ConfirmButton" runat="server" CssClass="green-button float-right margin-right" Text="Confirm" />
                                <asp:Button ID="BackButton" runat="server" CssClass="green-button float-right margin-right" Text="Back" />
                            </div>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>
</asp:Content>
