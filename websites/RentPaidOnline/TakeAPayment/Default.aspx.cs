﻿using System;
using System.Web.UI.WebControls;
using EfxFramework.Interfaces;
using EfxFramework.Interfaces.UserControls.PaymentSteps;
using EfxFramework.Web;

namespace Domus.TakeAPayment
{
    public partial class Default : BasePage, IEfxAdministratorSinglePayment
    {
        public MultiView PaymentView { get { return PaymentMultiView; } }
        public View StepOne { get { return StepOneView; } }
        public View StepTwo { get { return StepTwoView; } }
        public EventHandler ConfirmButtonClicked { set { ConfirmButton.Click += value; } }
        public EventHandler NextButtonClicked { set { NextButton.Click += value; } }
        public EventHandler BackButtonClicked { set { BackButton.Click += value; } }
        public IOneTimePaymentStepOne StepOneControl { get { return OneTimePaymentStepOne; } }
        public IOneTimePaymentStepTwo StepTwoControl { get { return OneTimePaymentStepTwo; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            SafeRedirect("~/PaymentsV2/Payment.aspx");
        }
    }
}