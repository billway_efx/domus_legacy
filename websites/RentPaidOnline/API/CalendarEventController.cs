﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RPO;
using System.Web;
using EfxFramework.Helpers;
using EfxFramework.BulkMail;
using System.Net.Mail;
using EfxFramework;

namespace Domus.API
{
	public class CalendarEventController : ApiController
	{
		[HttpGet]
		[ActionName("all")]
		public object GetAll()
		{
			using (var entity = new RPOEntities())
			{
				CalendarEventViewModel returnModel = new CalendarEventViewModel();
                var events = entity.CalendarEvents.Where(e => !e.Deleted);
                //var properties = entity.Properties.ToList();
                int? companyId = null;
                //if the user is not an RPO Admin, filter the list of events to show them for that company or the user's properties only -  && !Helper.IsCorporateAdmin
                if (!Helper.IsRpoAdmin)
                {
                    //cakel: BUGID00309 p2
                    if (Helper.IsCorporateAdmin)
                    {
                        var userProperties = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyStaffId == Helper.CurrentUserId).Select(p => p.PropertyId).ToList();
                        //get companyid of current user for events that display to all properties
                        companyId = entity.PropertyStaffs.First(ps => ps.PropertyStaffId == Helper.CurrentUserId).CompanyId;
                        events =
                            from ce in entity.CalendarEvents
                            //cakel: BUGID00309 - p2 - removed "all Properties"
                            //where (ce.AllProperties == true
                            //&& (!ce.CompanyId.HasValue || ce.CompanyId == companyId))
                            where  (!ce.CompanyId.HasValue || ce.CompanyId == companyId) && (!ce.Deleted )
                            select ce;
                    }
                    //cakel: BUGID00309 p2
                    if (Helper.IsPropertyManager)
                    {
                        var userProperties = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyStaffId == Helper.CurrentUserId).Select(p => p.PropertyId).ToList();
                        var propID = entity.PropertyPropertyStaffs.First(ps => ps.PropertyStaffId == Helper.CurrentUserId).PropertyId;
                        //get companyid of current user for events that display to all properties
                        companyId = entity.PropertyStaffs.First(ps => ps.PropertyStaffId == Helper.CurrentUserId).CompanyId;
                        events =
                            from ce in entity.CalendarEvents
                            where
                            //cakel: BUGID00309 p2 - removed "all Properties" Filter
                            //(ce.AllProperties == true &&
                            (!ce.CompanyId.HasValue || ce.CompanyId == companyId)
                            && (!ce.PropertyId.HasValue || ce.PropertyId == propID)
                            select ce;
                    }
                    
                }

                // Patrick Whittingham - 7/29/15 - josewhishlist 3a 
				var eventList = events.ToList();
                var isCorpAdmin = EfxFramework.Helpers.Helper.IsCorporateAdmin;
                var calendarEvents = eventList.Select(e => new
                    {
                        id = e.CalendarEventId,
                        title = e.Title,
                        start = e.StartDate,
                        end = e.EndDate,
                        url = "details/" + e.CalendarEventId.ToString(),
                        contact = e.Contact,
                        location = e.Location,
                        property     = e.AllProperties ? "All Properties" : e.Property != null ? e.Property.PropertyName : "",
                        active = e.Active,
                        canEdit = Helper.IsRpoAdmin || Helper.IsCorporateAdmin || (e.PropertyStaffId.HasValue && e.PropertyStaffId.Value == Helper.CurrentUserId)
                    }).ToList();
                return new {
                    events = calendarEvents, 
                    color = "", 
                    textColor = "" };
			}
		}

        // get property id
		[HttpGet]
		[ActionName("getbypropertyid")]
		public List<RPO.CalendarEvent> GetByPropertyId(int id)
		{
			using (var entity = new RPOEntities())
			{
				if (Helper.IsRpoAdmin || Helper.IsCorporateAdmin)
				{
					var eventList = entity.CalendarEvents.Include("CalendarEventType").Where(e => ((e.PropertyId.HasValue && e.PropertyId.Value == id) || e.AllProperties) && !e.Deleted).ToList();
					eventList.ForEach(e => e.IncludedCalendarEventType = e.CalendarEventType);
					return eventList;
				}
				else
				{
                    var companyId = entity.PropertyStaffs.First(ps => ps.PropertyStaffId == Helper.CurrentUserId).CompanyId;
                    var userProperties = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyStaffId == Helper.CurrentUserId).ToList();
					if(userProperties.Any(p => p.PropertyId == id))
					{
						var eventList = entity.CalendarEvents.Include("CalendarEventType").Where(e => (e.PropertyId.HasValue && e.PropertyId.Value == id) || ((e.AllProperties && e.CompanyId == companyId) || !e.CompanyId.HasValue)).ToList();
						eventList.ForEach(e => e.IncludedCalendarEventType = e.CalendarEventType);
						return eventList;
					}
				}
			}

            return new List<RPO.CalendarEvent>();
		}
		
        // get event id
		[HttpGet]
		[ActionName("getbyeventid")]
        public RPO.CalendarEvent GetByEventId(int id)
		{
			using (var entity = new RPOEntities())
			{

                var calEvent = entity.CalendarEvents.Where(e => e.CalendarEventId == id).FirstOrDefault();
                calEvent.PropertyName = calEvent.Property != null ? calEvent.Property.PropertyName : "All Properties";
				if (Helper.IsRpoAdmin)
				{
					return calEvent;
				}
				else if (calEvent != null)
				{
                    var companyId = entity.PropertyStaffs.First(ps => ps.PropertyStaffId == Helper.CurrentUserId).CompanyId;
                    var userProperties = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyStaffId == Helper.CurrentUserId).ToList();
                    bool canAccessEvent = (calEvent.PropertyId.HasValue && userProperties.Any(p => p.PropertyId == calEvent.PropertyId.Value) || ((calEvent.AllProperties && calEvent.CompanyId == companyId) || !calEvent.CompanyId.HasValue));
					if (canAccessEvent)
						return calEvent;
				}
			}

			return null;
		}

        // edit event
        [HttpGet]
        [ActionName("editevent")]
        public RPO.CalendarEvent EditEventById(int id)
        {
            using (var entity = new RPOEntities())
            {
                
                var calEvent = entity.CalendarEvents.Where(e => e.CalendarEventId == id).FirstOrDefault();
                calEvent.PropertyName = calEvent.Property != null ? calEvent.Property.PropertyName : "All Properties";

                if (Helper.IsRpoAdmin || Helper.IsCorporateAdmin )
                {
                    return calEvent;
                }
                else if (calEvent != null && !calEvent.EfxAdministratorId.HasValue)
                {
                    var userProperties = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyStaffId == Helper.CurrentUserId).ToList();
                    if (CanManageEvent(calEvent, userProperties))
                        return calEvent;
                }
            }

            return null;
        }

        // get event types
        [HttpGet]
        [ActionName("geteventtypes")]
        public List<CalendarEventType> GetCalendarEventTypes()
        {
            using (var entity = new RPOEntities())
            {
                return entity.CalendarEventTypes.ToList(); 
            }
        }

        // add event
		[HttpPost]
		[ActionName("addevent")]
        public bool AddEvent(RPO.CalendarEvent newEvent)
		{
			bool resultsValue = false;
			using (var entity = new RPOEntities())
			{
                newEvent.DateCreated = DateTime.UtcNow;
                newEvent.DateLastModified = DateTime.UtcNow;
                newEvent.StartDate = newEvent.StartDate;
                newEvent.EndDate = newEvent.EndDate;

                newEvent.Recurring = newEvent.Recurring;
                newEvent.PropertyId = newEvent.PropertyId;

                // Patrick Whittingham -8/19/15 - 3b josewhishlist
                if (newEvent.Location == null)
                {
                    newEvent.Location = " ";
                }
                //cakel: Test code
                if (Helper.IsRpoAdmin  )
				{
                    newEvent.EfxAdministratorId = Helper.CurrentUserId;
                    entity.CalendarEvents.Add(newEvent);
                    
                    //CMallory - 0079 - Use stored procedure to add recurring event instead of Entity Frameworks.  Entity Framework will still be used for normal events.
                    if (newEvent.Recurring)
                    {
                       int AssociatedCalendarEventId = DLAccess.CreateCalendarEvents(newEvent.Title, newEvent.Location, newEvent.Details, newEvent.StartDate, newEvent.EndDate, newEvent.Contact, newEvent.EfxAdministratorId, newEvent.PropertyStaffId, DateTime.Now, newEvent.Active, newEvent.PropertyId, newEvent.AllProperties, newEvent.Recurring, 0);
                        for(int i=1; i<=59; i++)
                        {
                            DLAccess.CreateCalendarEvents(newEvent.Title, newEvent.Location, newEvent.Details, newEvent.StartDate.AddMonths(i), newEvent.EndDate.AddMonths(i), newEvent.Contact, newEvent.EfxAdministratorId, newEvent.PropertyStaffId, DateTime.Now, newEvent.Active,newEvent.PropertyId, newEvent.AllProperties, newEvent.Recurring, AssociatedCalendarEventId);
                        }
                    }

                    else
                    {
                        entity.SaveChanges();
                    }
					
                    if (newEvent.ResidentsNotified)
                    {
                        EfxFramework.EfxAdministrator EfxAdmin = new EfxFramework.EfxAdministrator(newEvent.EfxAdministratorId);
                        EfxFramework.PropertyStaff PropStaff = new EfxFramework.PropertyStaff(newEvent.PropertyStaffId);
                        if (EfxAdmin!=null)
                        {
                            SendEmail(newEvent.Title, newEvent.Details, EfxAdmin.PrimaryEmailAddress, newEvent.PropertyId);
                        }

                        else if (PropStaff!=null)
                        {
                            SendEmail(newEvent.Title, newEvent.Details, PropStaff.PrimaryEmailAddress, newEvent.PropertyId);
                        }
                        
                    }
					resultsValue = true;
				}
				else
				{
                    newEvent.PropertyStaffId = Helper.CurrentUserId;
                    if (newEvent.AllProperties)
                    {
                        var userProperty = entity.PropertyPropertyStaffs.FirstOrDefault(pps => pps.PropertyStaffId == Helper.CurrentUserId);
                        //if (userProperty != null)
                        if (userProperty == null)
                        {
                            //var companyId = entity.Properties.First(p => p.PropertyId == userProperty.PropertyId).CompanyId;
                            //cakel: BUGID00309 p2
                            var companyId = entity.PropertyStaffs.First(p => p.PropertyStaffId == Helper.CurrentUserId).CompanyId;
                            //add company to new event
                            newEvent.CompanyId = companyId;
                            newEvent.PropertyId = null;
                        }
                    }
                    //cakel: BUGID00309 - Added code to include companyid in inserting of new record
                    else
                    {

                        var companyId = entity.PropertyStaffs.First(p => p.PropertyStaffId == Helper.CurrentUserId).CompanyId;
                        //add company to new event
                        newEvent.CompanyId = companyId;
                        //cakel: BUGID00309 - add propertyid to insert if property is not null
                        //cakel BUGID00309 p2 - 
                        newEvent.PropertyId = newEvent.PropertyId;
                        //cakel: BUGID00309 -  commented out code below
                        //newEvent.PropertyId = null;
                    }
                    entity.CalendarEvents.Add(newEvent);

                    //CMallory - 0079
                    if (newEvent.Recurring)
                    {
                         //CMallory - 0079 - Use stored procedure to add recurring event instead of Entity Frameworks.  Entity Framework will still be used for normal events.
                        int AssociatedCalendarEventId =  DLAccess.CreateCalendarEvents(newEvent.Title, newEvent.Location, newEvent.Details, newEvent.StartDate, newEvent.EndDate, newEvent.Contact, newEvent.EfxAdministratorId, newEvent.PropertyStaffId, DateTime.Now, newEvent.Active, newEvent.PropertyId, newEvent.AllProperties, newEvent.Recurring, 0);
                        for(int i=1; i<=59; i++)
                        {
                            DLAccess.CreateCalendarEvents(newEvent.Title, newEvent.Location, newEvent.Details, newEvent.StartDate.AddMonths(i), newEvent.EndDate, newEvent.Contact, newEvent.EfxAdministratorId, newEvent.PropertyStaffId, DateTime.Now, newEvent.Active,newEvent.PropertyId, newEvent.AllProperties, newEvent.Recurring, AssociatedCalendarEventId);
                        }
                    }

                    else
                    {
                        entity.SaveChanges();
                    }

                    if (newEvent.ResidentsNotified)
                    {
                        EfxFramework.EfxAdministrator EfxAdmin = new EfxFramework.EfxAdministrator(newEvent.EfxAdministratorId);
                        EfxFramework.PropertyStaff PropStaff = new EfxFramework.PropertyStaff(newEvent.PropertyStaffId);
                        //cakel: 0079
                        if (Helper.IsRpoAdmin)
                        {
                            SendEmail(newEvent.Title, newEvent.Details, EfxAdmin.PrimaryEmailAddress, newEvent.PropertyId);
                        }

                        else if (PropStaff!=null)
                        {
                            SendEmail(newEvent.Title, newEvent.Details, PropStaff.PrimaryEmailAddress, newEvent.PropertyId);
                        }
                    }
                    resultsValue = true;
				}
			}
			return resultsValue;
		}

        // update event
		[HttpPost]
		[ActionName("updateevent")]
        public bool UpdateEvent(RPO.CalendarEvent updatedEvent)
		{
			bool resultsValue = false;
            
			using (var entity = new RPOEntities())
			{
                var eventToUpdate = entity.CalendarEvents.First(ce => ce.CalendarEventId == updatedEvent.CalendarEventId);
				eventToUpdate.Active = updatedEvent.Active;
				eventToUpdate.Contact = updatedEvent.Contact;
				eventToUpdate.DateLastModified = DateTime.UtcNow;
				eventToUpdate.Deleted = updatedEvent.Deleted;
				eventToUpdate.Details = updatedEvent.Details;
                eventToUpdate.EndDate = updatedEvent.EndDate;
				eventToUpdate.Location = updatedEvent.Location;
				eventToUpdate.PropertyId = updatedEvent.PropertyId;
                eventToUpdate.AllProperties = updatedEvent.AllProperties;
                eventToUpdate.StartDate = updatedEvent.StartDate;
				eventToUpdate.Title = updatedEvent.Title;

                //CMallory - Task 0079 
                eventToUpdate.PropertyStaffId = updatedEvent.PropertyStaffId;
                eventToUpdate.EfxAdministratorId = updatedEvent.EfxAdministratorId;
                eventToUpdate.CompanyId = updatedEvent.CompanyId;
                eventToUpdate.AssociatedCalendarEventId = updatedEvent.AssociatedCalendarEventId;
                eventToUpdate.Recurring = updatedEvent.Recurring;
                // Patrick Whittingham - 7/30/15 - josewhishlist 3a 
                if (Helper.IsRpoAdmin || Helper.IsCorporateAdmin )
				{
                     //CMallory - 0079 - Use stored procedure to Updatte events.
                    DLAccess.UpdateCalendarEvents(eventToUpdate.CalendarEventId, eventToUpdate.Title, eventToUpdate.Location, eventToUpdate.Details, eventToUpdate.StartDate, eventToUpdate.EndDate, eventToUpdate.Contact, eventToUpdate.EfxAdministratorId,eventToUpdate.PropertyStaffId, eventToUpdate.DateCreated,
                        eventToUpdate.Active, eventToUpdate.Deleted, eventToUpdate.PropertyId, eventToUpdate.AllProperties, eventToUpdate.CompanyId, eventToUpdate.AssociatedCalendarEventId);
					//entity.SaveChanges();
					resultsValue = true;
				}
				else if (updatedEvent.PropertyStaffId.HasValue)
				{
					var userProperties = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyStaffId == Helper.CurrentUserId).ToList();
                    if (eventToUpdate.AllProperties)
                    {
                        var userProperty = entity.PropertyPropertyStaffs.FirstOrDefault(pps => pps.PropertyStaffId == Helper.CurrentUserId);
                        if (userProperty != null)
                        {
                            var companyId = entity.Properties.First(p => p.PropertyId == userProperty.PropertyId).CompanyId;
                            //add company to new event
                            eventToUpdate.CompanyId = companyId;
                            eventToUpdate.PropertyId = null;
                        }
                    }
                    if (CanManageEvent(updatedEvent, userProperties))
					{
						//CMallory - 0079 - Use stored procedure to Updatte events.
                        DLAccess.UpdateCalendarEvents(eventToUpdate.CalendarEventId, eventToUpdate.Title, eventToUpdate.Location, eventToUpdate.Details, eventToUpdate.StartDate, eventToUpdate.EndDate, eventToUpdate.Contact, eventToUpdate.EfxAdministratorId,eventToUpdate.PropertyStaffId, eventToUpdate.DateCreated,
                        eventToUpdate.Active, eventToUpdate.Deleted, eventToUpdate.PropertyId, eventToUpdate.AllProperties, eventToUpdate.CompanyId, eventToUpdate.AssociatedCalendarEventId);
					    //entity.SaveChanges();
						resultsValue = true;
					}
				}
			}
			return resultsValue;
		}

        // toggle active
		[HttpPost]
		[ActionName("toggleactive")]
		public bool ToggleActive(int id)
		{
			bool resultsValue = false;
			using (var entity = new RPOEntities())
			{
				var calEvent = entity.CalendarEvents.First(c => c.CalendarEventId == id);
				if (Helper.IsRpoAdmin)
				{
					calEvent.Active = !calEvent.Active;
					entity.SaveChanges();
					resultsValue = true;
				}
				else if(calEvent.PropertyStaffId.HasValue)
				{
					var userProperties = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyStaffId == Helper.CurrentUserId).ToList();
                    if (CanManageEvent(calEvent, userProperties))
					{
						calEvent.Active = !calEvent.Active;
						entity.SaveChanges();
						resultsValue = true;
					}
				}
			}

			return resultsValue;
		}

        // dekete event
		[HttpPost]
		[ActionName("deleteevent")]
		public bool Delete(int id)
		{
			bool resultsValue = false;
			using (var entity = new RPOEntities())
			{
                var calEvent = entity.CalendarEvents.First(c => c.CalendarEventId == id);
                // Patrick Whittingham - 7/30/15 - josewhishlist 3a 
                if (Helper.IsRpoAdmin || Helper.IsCorporateAdmin )
				{
					calEvent.Deleted = true;
                    //CMallory - 0079 - Used Stored Procedure to delete an event and it's recurring events instead of Entity Framework
                    DLAccess.DeleteCalendarEvents(id, calEvent.AssociatedCalendarEventId);
					//entity.SaveChanges();
					resultsValue = true;
				}
				else if (calEvent.PropertyStaffId.HasValue)
				{
					var userProperties = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyStaffId == Helper.CurrentUserId).ToList();
                    if (CanManageEvent(calEvent, userProperties))
					{
                        calEvent.Deleted = true;
                        //CMallory - 0079 - Used Stored Procedure to delete an event and it's recurring events instead of Entity Framework
                        DLAccess.DeleteCalendarEvents(id, calEvent.AssociatedCalendarEventId);
						//entity.SaveChanges();
						resultsValue = true;
					}
				}
			}
			return resultsValue;
		}

        private bool CanManageEvent(RPO.CalendarEvent calEvent, List<PropertyPropertyStaff> pps)
		{
            bool canManageEvent = pps.Any(ps => (calEvent.PropertyId.HasValue && ps.PropertyId == calEvent.PropertyId.Value) || (calEvent.AllProperties && calEvent.PropertyStaffId.HasValue && calEvent.PropertyStaffId.Value == Helper.CurrentUserId));
            return canManageEvent;
		}

        //CMallory - Task 0079 - Added
        public void SendEmail(string Subject, string Body, string FromEmailAddress, int? PropertyID = 0)
        {
            

            List<EfxFramework.Renter> Residents = EfxFramework.Renter.GetRenterListByPropertyId_v2(PropertyID);
            List<string> MailAddress= new List<string>();
            foreach(EfxFramework.Renter m in Residents)
            {
                MailAddress.Add(m.PrimaryEmailAddress);
            }
            BulkMail.SendSimpleMailMessage(EfxSettings.SendMailUsername, EfxSettings.SendMailPassword, FromEmailAddress, Subject, Body, MailAddress);
        }
	}
}