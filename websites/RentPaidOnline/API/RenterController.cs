﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EfxFramework.PaymentMethods;
using EfxFramework.PaymentMethods.Ach;
using RPO;
//using WebAPI.OutputCache;
using WebApi.OutputCache.Core;
using EfxFramework.Helpers;
using System.Data.SqlClient;
using System.Data;
using EfxFramework;
//using EfxFramework;

namespace Domus.API
{
    public class RenterController : ApiController
    {
        [HttpGet]
        [ActionName("id")]
        public int GetCurrentUserId()
        {
            return RPO.Helpers.Helper.CurrentUserId;
        }

        [HttpGet]
        [ActionName("isadmin")]
        public bool IsAdmin()
        {
            List<RoleName> roleListAdmin = new List<RoleName> { RoleName.EfxPowerAdministrator };
            return RoleManager.IsUserInRole(EfxFramework.RoleType.EfxAdministrator, GetCurrentUserId(), roleListAdmin);
        }

        [HttpGet]
        [ActionName("iscsr")]
        public bool IsCsrUser()
        {
                List<RoleName> roleListAdmin = new List<RoleName> { RoleName.EfxCustomerService };
                return RoleManager.IsUserInRole(EfxFramework.RoleType.CustomerServiceStaff, GetCurrentUserId(), roleListAdmin);
        }

        [HttpGet]
        [ActionName("all")]
        public object Get()
        {
            using (var entity = new RPOEntities())
            {
                int page = 1;
                bool isCorpAdmin = Helper.IsCorporateAdmin;
                bool isRPOAdmin = Helper.IsRpoAdmin;
                bool isRPOCSR = Helper.IsCsrUser;
                int userId = Helper.CurrentUserId;
                var currentUserProperties =
                        (from pps in entity.PropertyPropertyStaffs
                         where pps.PropertyStaffId == userId
                         select pps.PropertyId).ToList();

                //Salcedo - 2/16/2015 - task # 000138 - for corporate admins, let them see all properties in their company, regardless of the PropertyPropertyStaff table
                if (isCorpAdmin)
                {
                    currentUserProperties =
                        (from ps in entity.PropertyStaffs 
                         join p in entity.Properties on ps.CompanyId equals p.CompanyId 
                         where ps.PropertyStaffId == userId 
                         select p.PropertyId).ToList();
                }

                var query = Request.RequestUri.ParseQueryString();
                IQueryable<RPO.Renter> renters = entity.Renters;

                //Salcedo - for testing: from r in renters.Take(100) 
                //Salcedo - 3/21/2014 - changed LeaseRenters and Leases to outer joins, so that we include non-integrated renters without leases in the list
                var renterList =
                    (from r in renters
                     join lr in entity.LeaseRenters on r.RenterId equals lr.RenterId into outerlr from sublr in outerlr.DefaultIfEmpty()
                     join l in entity.Leases on sublr.LeaseId equals l.LeaseId into outerl from subl in outerl.DefaultIfEmpty()   
                     join p in entity.Properties on r.RenterPropertyId equals p.PropertyId
                     select new
                     {
                         r.RenterId,
                         r.FirstName,
                         r.LastName,
                         RentAmount = (subl == null ? 0 : subl.RentAmount),
                         p.PropertyName,
                         p.PropertyId,
                         r.PrimaryEmailAddress,
                         r.Unit,
                         r.PublicRenterId,
                         r.PmsId,
                         r.AccountCodePin,
                         r.IsActive,
                         //cakel: BUGID00226 - Added Phone number for property
                         p.MainPhoneNumber
                     });

                if (!isRPOAdmin && !isRPOCSR)
                {
                    //Salcedo - 3/11/2014 - the Linq was nested too deeply for one more join, so I changed to use "Contains"
                    //renterList = from r in renterList
                    //             join p in currentUserProperties on r.PropertyId equals p
                    //             select r;
                    renterList = renterList.Where(r => currentUserProperties.Contains(r.PropertyId));
                }
                if (!string.IsNullOrEmpty(query["pid"]))
                {
                    int propertyId = Convert.ToInt32(query["pid"]);
                    renterList = renterList.Where(r => r.PropertyId == propertyId);
                }
                if (!string.IsNullOrEmpty(query["q"]))
                {
                    var q = query["q"].ToString();

                    renterList = renterList.Where(r =>
                        r.FirstName.Contains(q) ||
                        r.LastName.Contains(q) ||
                        (r.FirstName + " " + r.LastName).Contains(q) ||
                        r.PrimaryEmailAddress.Contains(q) ||
                        r.PmsId.Contains(q) ||
                        r.Unit.Contains(q));
                }
                if (!string.IsNullOrEmpty(query["active"]))
                {
                    var active = Convert.ToBoolean(query["active"]);
                    renterList = renterList.Where(r => r.IsActive == active);
                }

                //if (!string.IsNullOrEmpty(query["o"]) && !string.IsNullOrEmpty(query["d"]))
                //{
                //    if (query["o"].ToString() == "LastName")
                //    {
                //        if (query["d"].ToString() == "desc")
                //            renterList = renterList.OrderByDescending(r => r.LastName).ThenBy(r => r.FirstName);
                //        else
                //            renterList = renterList.OrderBy(r => r.LastName).ThenBy(r => r.FirstName);
                //    }
                //    else if (query["o"].ToString() == "FirstName")
                //    {
                //        if (query["d"].ToString() == "desc")
                //            renterList = renterList.OrderByDescending(r => r.FirstName).ThenBy(r => r.LastName);
                //        else
                //            renterList = renterList.OrderBy(r => r.FirstName).ThenBy(r => r.LastName);
                //    }
                //}
                //else
                //    renterList = renterList.OrderBy(r => r.LastName).ThenBy(r => r.FirstName);

                //Salcedo - 2/4/2014 - calling Count() was causing an exception regarding Sql being too deeply nested
                //There's likely another problem at work here. But, this work around resolves the problem for now.
                //Also, same problem was happening with the Skip.Take logic. I broked it up and forced execution early
                //by calling ToList(). Otherwise, execution is deferred, and consequently was too complex for SQL for situations where
                //we have a LOT of renters. The test case that bombed had almost 12,000 renters on 2/4/2014. (CompanyID = 7).
                //Need a better solution, like caching this list of renters.

                //int count = renterList.Count();
                int count = 0;

                foreach (var Renter in renterList)
                {
                    count++;
                }

                if (int.TryParse(query["p"], out page))
                {
                    int skip = (page - 1) * 10;
                    //renterList = renterList.Skip(skip).Take(10);
                    var temp = renterList.ToList();
                    var temp2 = temp.OrderByDescending(r => r.LastName);

                    if (!string.IsNullOrEmpty(query["o"]) && !string.IsNullOrEmpty(query["d"]))
                    {
                        if (query["o"].ToString() == "LastName")
                        {
                            if (query["d"].ToString() == "desc")
                                temp2 = temp.OrderByDescending(r => r.LastName).ThenBy(r => r.FirstName);
                            else
                                temp2 = temp.OrderBy(r => r.LastName).ThenBy(r => r.FirstName);
                        }
                        else if (query["o"].ToString() == "FirstName")
                        {
                            if (query["d"].ToString() == "desc")
                                temp2 = temp.OrderByDescending(r => r.FirstName).ThenBy(r => r.LastName);
                            else
                                temp2 = temp.OrderBy(r => r.FirstName).ThenBy(r => r.LastName);
                        }
                    }
                    else
                        temp2 = temp.OrderBy(r => r.LastName).ThenBy(r => r.FirstName);

                    var temp3 = temp2.Skip(skip).ToList();
                    var temp4 = temp3.Take(10).ToList();
                    return new { data = temp4, itemCount = count };
                }
                else
                {
                    renterList = renterList.Take(10);

                    var temp = renterList.ToList();
                    var temp2 = temp.OrderByDescending(r => r.LastName);

                    if (!string.IsNullOrEmpty(query["o"]) && !string.IsNullOrEmpty(query["d"]))
                    {
                        if (query["o"].ToString() == "LastName")
                        {
                            if (query["d"].ToString() == "desc")
                                temp2 = temp.OrderByDescending(r => r.LastName).ThenBy(r => r.FirstName);
                            else
                                temp2 = temp.OrderBy(r => r.LastName).ThenBy(r => r.FirstName);
                        }
                        else if (query["o"].ToString() == "FirstName")
                        {
                            if (query["d"].ToString() == "desc")
                                temp2 = temp.OrderByDescending(r => r.FirstName).ThenBy(r => r.LastName);
                            else
                                temp2 = temp.OrderBy(r => r.FirstName).ThenBy(r => r.LastName);
                        }
                    }
                    else
                        temp2 = temp.OrderBy(r => r.LastName).ThenBy(r => r.FirstName);

                    return new { data = temp2.ToList(), itemCount = count };
                }
                //return new { data = renterList.ToList(), itemCount = count };
            }
        }

        //Salcedo - 2/17/2015 - changes made for task # 000138
        private static int[] GetRenters(string text, string filter)
        {
            List<int> RenterIds = new List<int>();
            RenterIds.Add(0);
            string Sql = "";
            int UserId = EfxFramework.Helpers.Helper.CurrentUserId;
            Sql = "select r.RenterId from Renter r where RenterId = 0";

            if (EfxFramework.Helpers.Helper.IsCorporateAdmin)
            {
                // For Corporate administrators, show all for their company
                switch (filter)
                {
                    case "FirstName":
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "join PropertyStaff ps on p.CompanyId = ps.CompanyId and ps.PropertyStaffId = " + UserId.ToString() + " " +
                            "where UPPER(r.Firstname) like '%" + text.ToUpper() + "%'";
                        break;
                    case "LastName":
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "join PropertyStaff ps on p.CompanyId = ps.CompanyId and ps.PropertyStaffId = " + UserId.ToString() + " " +
                            "where UPPER(r.Lastname) like '%" + text.ToUpper() + "%'";
                        break;
                    case "RenterID":
                        decimal renterId = 0;
                        decimal.TryParse(text, out renterId);
                        if (renterId == 0) renterId = -9999;
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "join PropertyStaff ps on p.CompanyId = ps.CompanyId and ps.PropertyStaffId = " + UserId.ToString() + " " +
                            "where r.PublicRenterId = " + renterId.ToString() + " " +
                            "or UPPER(r.PmsId) like '%" + text.ToUpper() + "%'";
                        break;
                    //Salcedo - 2/24/2015 - added Mobile Phone and Fax Number to the phone number search
                    //Also, I added code to strip formatting that the user might have entered, because phone numbers are stored in the database without formatting.
                    case "MainPhoneNumber":
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "join PropertyStaff ps on p.CompanyId = ps.CompanyId and ps.PropertyStaffId = " + UserId.ToString() + " " +
                            "where UPPER(r.MainPhoneNumber) like '%" + text.ToUpper().Replace("-","").Replace("(","").Replace(")","") + "%' " + 
                                "OR UPPER(r.MobilePhoneNumber) like '%" + text.ToUpper().Replace("-","").Replace("(","").Replace(")","") + "%'" +
                                "OR UPPER(r.FaxNumber) like '%" + text.ToUpper().Replace("-", "").Replace("(", "").Replace(")", "") + "%'";
                        break;
                    case "Email":
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "join PropertyStaff ps on p.CompanyId = ps.CompanyId and ps.PropertyStaffId = " + UserId.ToString() + " " +
                            "where UPPER(r.PrimaryEmailAddress) like '%" + text.ToUpper() + "%'";
                        break;
                    case "PropertyName":
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "join PropertyStaff ps on p.CompanyId = ps.CompanyId and ps.PropertyStaffId = " + UserId.ToString() + " " +
                            "where UPPER(p.PropertyName) like '%" + text.ToUpper() + "%'";
                        break;
                    case "RentAmount":
                        decimal rentAmount = -9999;
                        decimal.TryParse(text, out rentAmount);
                        if (rentAmount == 0 && text != "0") rentAmount = -9999;
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join LeaseRenter lr on r.RenterId = lr.RenterId " +
                            "join Lease l on lr.LeaseId = l.LeaseId " + 
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "join PropertyStaff ps on p.CompanyId = ps.CompanyId and ps.PropertyStaffId = " + UserId.ToString() + " " +
                            "where l.RentAmount = " + rentAmount.ToString();
                        break;
                    default:
                        decimal searchAsDecimal = -9999;
                        decimal.TryParse(text, out searchAsDecimal);
                        if (searchAsDecimal == 0) searchAsDecimal = -9999;
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join LeaseRenter lr on r.RenterId = lr.RenterId " +
                            "join Lease l on lr.LeaseId = l.LeaseId " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "join PropertyStaff ps on p.CompanyId = ps.CompanyId and ps.PropertyStaffId = " + UserId.ToString() + " " +
                            "where (UPPER(r.Firstname) like '%" + text.ToUpper() + "%') " +
                            "OR (UPPER(r.Lastname) like '%" + text.ToUpper() + "%') " +
                            "OR (UPPER(r.PmsId) like '%" + text.ToUpper() + "%') " +
                            "OR (r.PublicRenterId = " + searchAsDecimal.ToString() + ") " +
                            "OR (UPPER(r.MainPhoneNumber) like '%" + text.ToUpper() + "%') " +
                            "OR (UPPER(r.PrimaryEmailAddress) like '%" + text.ToUpper() + "%') " +
                            "OR (UPPER(p.PropertyName) like '%" + text.ToUpper() + "%') " +
                            "OR (l.RentAmount = " + searchAsDecimal.ToString() + ") "; 
                        break;
                }
            }
            else if (!EfxFramework.Helpers.Helper.IsRpoAdmin && !EfxFramework.Helpers.Helper.IsCsrUser)
            {
                // For property managers, show only their properties ...
                switch (filter)
                {
                    case "FirstName":
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "join PropertyPropertyStaff pps on p.PropertyId = pps.PropertyId and pps.PropertyStaffId = " + UserId.ToString() + " " +
                            "where UPPER(r.Firstname) like '%" + text.ToUpper() + "%'";
                        break;
                    case "LastName":
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "join PropertyPropertyStaff pps on p.PropertyId = pps.PropertyId and pps.PropertyStaffId = " + UserId.ToString() + " " +
                            "where UPPER(r.Lastname) like '%" + text.ToUpper() + "%'";
                        break;
                    case "RenterID":
                        decimal renterId = 0;
                        decimal.TryParse(text, out renterId);
                        if (renterId == 0) renterId = -9999;
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "join PropertyPropertyStaff pps on p.PropertyId = pps.PropertyId and pps.PropertyStaffId = " + UserId.ToString() + " " +
                            "where r.PublicRenterId = " + renterId.ToString() + " " +
                            "or UPPER(r.PmsId) like '%" + text.ToUpper() + "%'";
                        break;
                    //Salcedo - 2/24/2015 - added Mobile Phone and Fax Number to the phone number search
                    //Also, I added code to strip formatting that the user might have entered, because phone numbers are stored in the database without formatting.
                    case "MainPhoneNumber":
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "join PropertyPropertyStaff pps on p.PropertyId = pps.PropertyId and pps.PropertyStaffId = " + UserId.ToString() + " " +
                            "where UPPER(r.MainPhoneNumber) like '%" + text.ToUpper().Replace("-", "").Replace("(", "").Replace(")", "") + "%' " + 
                                "OR UPPER(r.MobilePhoneNumber) like '%" + text.ToUpper().Replace("-", "").Replace("(", "").Replace(")", "") + "%' " +
                                "OR UPPER(r.FaxNumber) like '%" + text.ToUpper().Replace("-", "").Replace("(", "").Replace(")", "") + "%'";
                        break;
                    case "Email":
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "join PropertyPropertyStaff pps on p.PropertyId = pps.PropertyId and pps.PropertyStaffId = " + UserId.ToString() + " " +
                            "where UPPER(r.PrimaryEmailAddress) like '%" + text.ToUpper() + "%'";
                        break;
                    case "PropertyName":
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "join PropertyPropertyStaff pps on p.PropertyId = pps.PropertyId and pps.PropertyStaffId = " + UserId.ToString() + " " +
                            "where UPPER(p.PropertyName) like '%" + text.ToUpper() + "%'";
                        break;
                    case "RentAmount":
                        decimal rentAmount = -9999;
                        decimal.TryParse(text, out rentAmount);
                        if (rentAmount == 0 && text != "0") rentAmount = -9999;
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join LeaseRenter lr on r.RenterId = lr.RenterId " +
                            "join Lease l on lr.LeaseId = l.LeaseId " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "join PropertyPropertyStaff pps on p.PropertyId = pps.PropertyId and pps.PropertyStaffId = " + UserId.ToString() + " " +
                            "where l.RentAmount = " + rentAmount.ToString();
                        break;
                    default:
                        decimal searchAsDecimal = -9999;
                        decimal.TryParse(text, out searchAsDecimal);
                        if (searchAsDecimal == 0) searchAsDecimal = -9999;
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join LeaseRenter lr on r.RenterId = lr.RenterId " +
                            "join Lease l on lr.LeaseId = l.LeaseId " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "join PropertyPropertyStaff pps on p.PropertyId = pps.PropertyId and pps.PropertyStaffId = " + UserId.ToString() + " " +
                            "where (UPPER(r.Firstname) like '%" + text.ToUpper() + "%') " +
                            "OR (UPPER(r.Lastname) like '%" + text.ToUpper() + "%') " +
                            "OR (UPPER(r.PmsId) like '%" + text.ToUpper() + "%') " +
                            "OR (r.PublicRenterId = " + searchAsDecimal.ToString() + ") " +
                            "OR (UPPER(r.MainPhoneNumber) like '%" + text.ToUpper() + "%') " +
                            "OR (UPPER(r.PrimaryEmailAddress) like '%" + text.ToUpper() + "%') " +
                            "OR (UPPER(p.PropertyName) like '%" + text.ToUpper() + "%') " +
                            "OR (l.RentAmount = " + searchAsDecimal.ToString() + ") ";
                        break;
                }
            }
            else
            {
                // For EFX Administrators, show everything ...
                switch (filter)
                {
                    case "FirstName":
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "where UPPER(r.Firstname) like '%" + text.ToUpper() + "%'";
                        break;
                    case "LastName":
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "where UPPER(r.Lastname) like '%" + text.ToUpper() + "%'";
                        break;
                    case "RenterID":
                        decimal renterId = 0;
                        decimal.TryParse(text, out renterId);
                        if (renterId == 0) renterId = -9999;
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "where r.PublicRenterId = " + renterId.ToString() + " " +
                            "or UPPER(r.PmsId) like '%" + text.ToUpper() + "%'";
                        break;
                    //Salcedo - 2/24/2015 - added Mobile Phone and Fax Number to the phone number search
                    //Also, I added code to strip formatting that the user might have entered, because phone numbers are stored in the database without formatting.
                    case "MainPhoneNumber":
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "where UPPER(r.MainPhoneNumber) like '%" + text.ToUpper().Replace("-", "").Replace("(", "").Replace(")", "") + "%' " +
                                "OR UPPER(r.MobilePhoneNumber) like '%" + text.ToUpper().Replace("-", "").Replace("(", "").Replace(")", "") + "%' " +
                                "OR UPPER(r.FaxNumber) like '%" + text.ToUpper().Replace("-", "").Replace("(", "").Replace(")", "") + "%'";
                        break;
                    case "Email":
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "where UPPER(r.PrimaryEmailAddress) like '%" + text.ToUpper() + "%'";
                        break;
                    case "PropertyName":
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "where UPPER(p.PropertyName) like '%" + text.ToUpper() + "%'";
                        break;
                    case "RentAmount":
                        decimal rentAmount = -9999;
                        decimal.TryParse(text, out rentAmount);
                        if (rentAmount == 0 && text != "0") rentAmount = -9999;
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join LeaseRenter lr on r.RenterId = lr.RenterId " +
                            "join Lease l on lr.LeaseId = l.LeaseId " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "where l.RentAmount = " + rentAmount.ToString();
                        break;
                    default:
                        decimal searchAsDecimal = -9999;
                        decimal.TryParse(text, out searchAsDecimal);
                        if (searchAsDecimal == 0) searchAsDecimal = -9999;
                        Sql = "select top 100 r.RenterId from Renter r " +
                            "join LeaseRenter lr on r.RenterId = lr.RenterId " +
                            "join Lease l on lr.LeaseId = l.LeaseId " +
                            "join Property p on r.renterpropertyid = p.propertyid " +
                            "where (UPPER(r.Firstname) like '%" + text.ToUpper() + "%') " +
                            "OR (UPPER(r.Lastname) like '%" + text.ToUpper() + "%') " +
                            "OR (UPPER(r.PmsId) like '%" + text.ToUpper() + "%') " +
                            "OR (r.PublicRenterId = " + searchAsDecimal.ToString() + ") " +
                            "OR (UPPER(r.MainPhoneNumber) like '%" + text.ToUpper() + "%') " +
                            "OR (UPPER(r.PrimaryEmailAddress) like '%" + text.ToUpper() + "%') " +
                            "OR (UPPER(p.PropertyName) like '%" + text.ToUpper() + "%') " +
                            "OR (l.RentAmount = " + searchAsDecimal.ToString() + ") ";
                        break;

                }
            }

            SqlDataReader reader = EfxFramework.MiscDatabase.GetReader(Sql);
            while (reader.Read())
            {
                RenterIds.Add((int)reader["RenterId"]);
            }
            reader.Close();

            int[] RenterIdList = RenterIds.ToArray();
            return RenterIdList;
        }

        //Salcedo - 2/17/2015 - changes made for task # 000138
        private static void GetDetailsForRenter(int RenterId, out string PropertyName, out string CompanyName, out decimal RentAmount)
        {
            string ThePropertyName = "";
            string TheCompanyName = "";
            decimal TheRentAmount = 0;

            string Sql;

            Sql = "select p.PropertyName, c.CompanyName, isnull(l.RentAmount,0) as RentAmount " +
                "from renter r " +
                "join property p on r.renterpropertyid = p.propertyid " +
                "join company c on p.companyid = c.companyid " +
                "left outer join leaserenter lr on r.renterid = lr.renterid " +
                "left outer join lease l on lr.leaseid = l.leaseid " +
                "where r.renterid = " + RenterId.ToString();
            SqlDataReader reader = EfxFramework.MiscDatabase.GetReader(Sql);
            while (reader.Read())
            {
                ThePropertyName = reader["PropertyName"].ToString();
                TheCompanyName = reader["CompanyName"].ToString();
                TheRentAmount = (decimal)reader["RentAmount"];
            }
            reader.Close();

            PropertyName = ThePropertyName;
            CompanyName = TheCompanyName;
            RentAmount = TheRentAmount;

            return;
        }

        [HttpGet]
        [ActionName("search")]
        public List<RPO.Renter> Search()
        {
            var query = Request.RequestUri.ParseQueryString();
            string text = query["text"].ToString().ToLower();
            string filter = query["filter"].ToString();
            if (string.IsNullOrEmpty(text))
                return null;

            //Salcedo - 2/17/2015 - added code to improve performance so I could test changes made for task # 000138
            int[] RenterIdList = GetRenters(text, filter);
            using (var entity = new RPOEntities())
            {
                var rentersList = entity.Renters.Where(r => RenterIdList.Contains(r.RenterId)).ToList();
                foreach (RPO.Renter TheRenter in rentersList)
                {
                    string CompanyName = "";
                    string PropertyName = "";
                    decimal RentAmount = 0;
                    GetDetailsForRenter(TheRenter.RenterId, out PropertyName, out CompanyName, out RentAmount);
                    TheRenter.PropertyName = PropertyName;
                    TheRenter.CompanyName = CompanyName;
                    TheRenter.RentAmount = RentAmount;
                }
                return rentersList;
            }

            //Salcedo - 2/17/2015 - below code removed and replaced with above
            //using (var entity = new RPOEntities())
            //{
            //    var rentersList = entity.Renters.ToList();

            //    if (EfxFramework.Helpers.Helper.IsRPOAdmin)
            //    {
            //        // If we're an EFX admin, get everything, wait ... go have some coffee ...
            //        rentersList = entity.Renters.ToList();
            //    }
            //    else
            //    {
            //        if (EfxFramework.Helpers.Helper.IsCorporateAdmin)
            //        {
            //            // If we're a corporate admin, then only worry about renters for all properties for your company ...
            //            var userProperties = entity.PropertyStaffs
            //                .Join(entity.Properties,
            //                    ps => ps.CompanyId,
            //                    p => p.CompanyId,
            //                    (ps, p) => new { PropertyStaffs = ps, Properties = p })
            //                .Where(TheJoin => TheJoin.PropertyStaffs.PropertyStaffId == EfxFramework.Helpers.Helper.CurrentUserId).ToList();
            //            rentersList = entity.Renters.Where(r => userProperties.Exists(up => up.Properties.PropertyId == r.RenterPropertyId.Value)).ToList();
            //        }
            //        else
            //        {
            //            //If we're a property manager, then only worry about properties you have specific access to ..
            //            var userProperties = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyStaffId == EfxFramework.Helpers.Helper.CurrentUserId).ToList();
            //            rentersList = entity.Renters.Where(r => userProperties.Exists(up => up.PropertyId == r.RenterPropertyId)).ToList();
            //        }
            //    }

            //    var propertyList = entity.Properties.Select(p => new PropertyListItem() { PropertyName = p.PropertyName, PropertyId = p.PropertyId, CompanyId = p.CompanyId }).ToList();
            //    var leaseList = entity.Leases.ToList();
            //    var leaseRenterList = entity.LeaseRenters.ToList();
            //    var companies = entity.Companies.ToList();
				
            //    List<int> propertyIdList;
            //    List<int> leaseIdList;
            //    List<int> leaseRenterIdList;
            //    switch (filter)
            //    {
            //        case "FirstName":
            //            rentersList = rentersList.Where(r => r.FirstName.ToLower().Contains(text)).ToList();
            //            break;
            //        case "LastName":
            //            rentersList = rentersList.Where(r => r.LastName.ToLower().Contains(text)).ToList();
            //            break;
            //        case "RenterID":
            //            decimal renterId = 0;
            //            decimal.TryParse(text, out renterId);
            //            rentersList = rentersList.Where(r => !string.IsNullOrEmpty(r.PmsId) && r.PmsId.Contains(text) || r.PublicRenterId == renterId).ToList();
            //            break;
            //        case "MainPhoneNumber":
            //            rentersList = rentersList.Where(r => !string.IsNullOrEmpty(r.MainPhoneNumber) && r.MainPhoneNumber.Contains(text)).ToList();
            //            break;
            //        case "Email":
            //            rentersList = rentersList.Where(r => r.PrimaryEmailAddress.ToLower().Contains(text)).ToList();
            //            break;
            //        case "PropertyName":
            //            var properties = entity.Properties.Where(p => p.PropertyName.ToLower().Contains(text)).Select(p => p.PropertyId).ToList();
            //            rentersList = rentersList.Where(r => r.RenterPropertyId.HasValue && properties.Contains(r.RenterPropertyId.Value)).ToList();
            //            break;
            //        case "RentAmount":
            //            decimal rentAmount = 0;
            //            decimal.TryParse(text, out rentAmount);
            //            leaseIdList = leaseList
            //                .Where(l => l.RentAmount == rentAmount)
            //                .Select(l => l.LeaseId)
            //                .ToList();
            //            var leaseRenters = entity.LeaseRenters
            //                .ToList()
            //                .Where(lr => leaseIdList.Contains(lr.LeaseId))
            //                .Select(lr => lr.RenterId)
            //                .ToList();
            //            rentersList = rentersList
            //                .Where(r => leaseRenters.Contains(r.RenterId)).ToList();
            //            break;
            //        default:
            //            decimal searchAsDecimal = 0;
            //            decimal.TryParse(text, out searchAsDecimal);
            //            propertyIdList = propertyList
            //                .Where(p => p.PropertyName.ToLower().Contains(text))
            //                .Select(p => p.PropertyId)
            //                .ToList();
            //            leaseIdList = leaseList
            //                .Where(l => l.RentAmount == searchAsDecimal)
            //                .Select(l => l.LeaseId)
            //                .ToList();
            //            leaseRenterIdList = leaseRenterList
            //                .ToList()
            //                .Where(lr => leaseIdList.Contains(lr.LeaseId))
            //                .Select(lr => lr.RenterId)
            //                .ToList();
            //            rentersList = rentersList.Where(r =>
            //                r.FirstName.ToLower().Contains(text)
            //                    || r.LastName.ToLower().Contains(text) 
            //                    || (!string.IsNullOrEmpty(r.PmsId) && r.PmsId.Contains(text))
            //                    || r.PublicRenterId == searchAsDecimal
            //                    || (!string.IsNullOrEmpty(r.MainPhoneNumber) && r.MainPhoneNumber.Contains(text))
            //                    || r.PrimaryEmailAddress.ToLower().Contains(text)
            //                    || leaseRenterIdList.Contains(r.RenterId)
            //                    || (r.RenterPropertyId.HasValue && propertyIdList.Contains(r.RenterPropertyId.Value))).ToList();
            //            break;
            //    }
            //    rentersList.ForEach(r =>
            //        {
            //            r.PropertyName = propertyList.Where(p => r.RenterPropertyId.HasValue && p.PropertyId == r.RenterPropertyId.Value).ToList().DefaultIfEmpty(new PropertyListItem() { PropertyName = "" }).First().PropertyName;
            //            r.RentAmount = leaseList.Where(l => leaseRenterList.Any(lr => lr.RenterId == r.RenterId && lr.LeaseId == l.LeaseId)).DefaultIfEmpty(new Lease(){RentAmount = 0}).First().RentAmount;
            //            int companyId = propertyList.Where(prop => r.RenterPropertyId.HasValue && prop.PropertyId == r.RenterPropertyId.Value).DefaultIfEmpty(new PropertyListItem() { CompanyId = 0, PropertyName = "" }).First().CompanyId;
            //            r.CompanyName = companyId > 0 ? companies.First(c => c.CompanyId == companyId).CompanyName : "";
            //        });

            //    //filter the list based on PM access level
            //    if (EfxFramework.Helpers.Helper.IsCorporateAdmin)
            //    {
            //        //var userProperties =
            //        //    (from ps in entity.PropertyStaffs
            //        //     join p in entity.Properties on ps.CompanyId equals p.CompanyId
            //        //     where ps.PropertyStaffId == EfxFramework.Helpers.Helper.CurrentUserId
            //        //     select p.PropertyId).ToList();
                    
            //        var userProperties = entity.PropertyStaffs
            //            .Join(entity.Properties, 
            //                ps => ps.CompanyId,
            //                p => p.CompanyId,
            //                (ps, p) => new {PropertyStaffs = ps, Properties = p})
            //            .Where(TheJoin => TheJoin.PropertyStaffs.PropertyStaffId == EfxFramework.Helpers.Helper.CurrentUserId).ToList();

            //        //return rentersList.Where(r => userProperties.Contains(r.RenterPropertyId)).ToList();
            //        //return rentersList.Where(r => r.RenterPropertyId =
            //        //return rentersList.Where(r => userProperties.Exists(up => r. .PropertyId == r.RenterPropertyId)).ToList();
            //        return rentersList.Where(r => userProperties.Exists(up => up.Properties.PropertyId == r.RenterPropertyId.Value)).ToList();
            //    }

            //    if (!EfxFramework.Helpers.Helper.IsRPOAdmin)
            //    {
            //        var userProperties = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyStaffId == EfxFramework.Helpers.Helper.CurrentUserId).ToList();
            //        return rentersList.Where(r => userProperties.Exists(up => up.PropertyId == r.RenterPropertyId)).ToList();
            //    }
            //    return rentersList;
            //}
        }
        // GET api/<controller>/5
        [HttpGet]
        [ActionName("byproperty")]
        public List<RPO.Renter> GetByPropertyId(int id)
        {
            using (var entity = new RPOEntities())
            {
                int page = 1;
                var query = Request.RequestUri.ParseQueryString();
                IEnumerable<RPO.Renter> renters = entity.Renters.Where(r => r.RenterPropertyId == id);
                if (!string.IsNullOrEmpty(query["q"]))
                    renters = renters.Where(r => r.FirstName.Contains(query["q"]) || r.LastName.Contains(query["q"]));
                if (int.TryParse(query["p"], out page))
                {
                    int skip = (page - 1) * 10;
                    renters = renters.Skip(skip).Take(10);
                }
                //else
                //{
                //    renters = renters.Take(10);
                //}
                return renters.ToList();
            }
        }
        [HttpGet]
        [ActionName("one")]
        public RPO.Renter Get(int id)
        {
            using (var entity = new RPOEntities())
            {
                var renter = entity.Renters.First(p => p.RenterId == id);
                renter.PaymentHistory = entity.Payments.Include("Transactions").Where(p => p.RenterId == renter.RenterId).OrderByDescending(p => p.TransactionDateTime).ToList();
                var paymentStatuses = entity.PaymentStatus.ToList();
                var property = entity.Properties.FirstOrDefault(p => p.PropertyId == renter.RenterPropertyId);
                var leaseRenter = entity.LeaseRenters.FirstOrDefault(lr => lr.RenterId == renter.RenterId);
				if (leaseRenter != null)
				{
					var leaseInfo = entity.Leases.FirstOrDefault(l => l.LeaseId == leaseRenter.LeaseId);
					renter.RentAmount = leaseInfo.RentAmount;
				}
                
                renter.PaymentHistory.ForEach(p =>
                {
                    p.Balance = p.RentAmount;
                    
                    p.PaymentTransactions = p.Transactions.OrderByDescending(t => t.TransactionDate).ToList();
                    if (property != null)
                    {
                        var lastTransaction = p.PaymentTransactions.LastOrDefault(t => t.PaymentId == p.PaymentId);
                        if (lastTransaction != null)
                        {
                            AchStatusResponse status = AchService.GetAchStatus(property.RentalAccountAchClientId,
                            lastTransaction.ExternalTransactionId);
                            if (status.Result == GeneralResponseResult.Success)
                            {
                                //cakel: BUGID00232 (really not related to 232 but we will fix it anyway)
                                if (p.PaymentStatusId != 7)
                                {
                                    p.PaymentStatusId =
                                        Convert.ToInt32(
                                            RPO.Transaction.GetDescription((RPO.Transaction.AchStatus)status.StatusCode.Value));
                                }
                            }
                        }
                        
                    }
                    p.PaymentTransactions.ForEach(t =>
                    {
                        if (t.PaymentId == p.PaymentId && t.InternalTransactionId == p.TransactionId)
                        {
                            t.PaymentStatusId = p.PaymentStatusId.Value;
                        }
                        t.TransactionStatus = paymentStatuses.First(s => s.PaymentStatusId == t.PaymentStatusId).PaymentStatusName;
                        if (t.PaymentAmount < 0)
                            p.Balance += t.PaymentAmount;
                    });
                    p.PaymentStatus = paymentStatuses.First(s => s.PaymentStatusId == p.PaymentStatusId).PaymentStatusName;
                });
                
                entity.SaveChanges();
                //update time for timezone
                if (property != null)
                {
                    renter.PropertyName = property.PropertyName;
                    if (!String.IsNullOrEmpty(property.TimeZone))
                        renter.PaymentHistory.ForEach(p =>
                        {
                            //Salcedo - 2/13/2014 - Also need to update this value
                            p.TransactionDateTime = EfxFramework.Helpers.DateTimeHelper.setDateTimeZone(p.TransactionDateTime,
                                property.TimeZone);

                            if (!p.LastUpdated.HasValue)
                                p.LastUpdated = p.TransactionDateTime;
                            p.LastUpdated = EfxFramework.Helpers.DateTimeHelper.setDateTimeZone(p.LastUpdated.Value,
                                property.TimeZone);
                            p.PaymentTransactions.ForEach(t =>
                            {
                                t.TransactionDate = EfxFramework.Helpers.DateTimeHelper.setDateTimeZone(t.TransactionDate,
                                property.TimeZone);
                            });
                        });
                    else
                        renter.PaymentHistory.ForEach(p =>
                        {
                            p.TransactionDateTime =
                                EfxFramework.Helpers.DateTimeHelper.setDateTimeZone(p.TransactionDateTime,
                                    "Eastern Standard Time");
                            p.PaymentTransactions.ForEach(t =>
                            {
                                t.TransactionDate = EfxFramework.Helpers.DateTimeHelper.setDateTimeZone(t.TransactionDate,
                                "Eastern Standard Time");
                            });
                        });
                }
                return renter;
            }
        }

        // POST api/<controller>
        [HttpPost]
        [ActionName("toggleactive")]
        public bool ToggleActive(RPO.Renter renter)
        {
            using (var entity = new RPOEntities())
            {
                var renterEntity = entity.Renters.First(p => p.RenterId == renter.RenterId);
                renterEntity.IsActive = !renterEntity.IsActive;
                entity.SaveChanges();

                // Salcedo - 2/12/2013 - task 364 - add logging for status change - Renter.IsActive column
                ActivityLog AL = new ActivityLog();
                string TheStatus = "";
                if (renterEntity.IsActive)
                {
                    TheStatus = "Active";
                }
                else
                {
                    TheStatus = "Deactivated";
                }
                AL.WriteLog(RPO.Helpers.Helper.CurrentUserId.ToString(), "Renter status was changed to: " + TheStatus,
                    (short)LogPriority.LogAlways, renter.RenterId.ToString(), true);

                return true;
            }
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}