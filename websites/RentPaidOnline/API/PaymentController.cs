﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EfxFramework.PaymentMethods;
using EfxFramework.PaymentMethods.Ach;
using RPO;
using Transaction = EfxFramework.Transaction;

namespace Domus.API
{
    public class PaymentController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/payment/search?text=<text>&filter=<filter>
        [HttpGet]
        [ActionName("search")]
        public List<Payment> Search()
        {
            var query = Request.RequestUri.ParseQueryString();
            string text = query["text"].ToString().ToLower();
            string filter = query["filter"].ToString();
            
            DateTime? startDate = null;
            if(!string.IsNullOrEmpty(query["startDate"].ToString())) 
            {
                DateTime temp;
                DateTime.TryParse(query["startDate"].ToString(), out temp);
                startDate = temp;
            }
            DateTime? endDate = null;
            if (!string.IsNullOrEmpty(query["endDate"].ToString()))
            {
                DateTime temp;
                DateTime.TryParse(query["endDate"].ToString(), out temp);
                endDate = temp;
            }
            if (string.IsNullOrEmpty(text))
                return null;
            using (var entity = new RPOEntities())
            {
                
                IQueryable<Payment> paymentQuery = entity.Payments;
                if (startDate != null)
                {
                    paymentQuery = paymentQuery.Where(p => p.TransactionDateTime >= startDate);
                }
                if (endDate != null)
                {
                    paymentQuery = paymentQuery.Where(p => p.TransactionDateTime <= endDate);
                }
                var payments = paymentQuery.ToList();
                var payers = entity.Payers.ToList();
                var renters = entity.Renters.ToList();
                var properties = entity.Properties.Select(p => new PropertyListItem() { PropertyName = p.PropertyName, PropertyId = p.PropertyId, CompanyId = p.CompanyId }).ToList();
                var companies = entity.Companies.ToList();
                var paymentChannels = entity.PaymentChannels.ToList();
                var paymentStatuses = entity.PaymentStatus.ToList();
                switch (filter)
                {
                    case "TransactionID":
                        payments = payments.Where(p => p.TransactionId.ToLower().Contains(text)).ToList();
                        break;
                    case "FirstName":
                        payments = payments.Where(p => payers.Any(payer => payer.FirstName.ToLower().Contains(text) && payer.PayerId == p.PayerId)).ToList();
                        break;
                    case "LastName":
                        payments = payments.Where(p => payers.Any(payer => payer.LastName.ToLower().Contains(text) && payer.PayerId == p.PayerId)).ToList();
                        break;
                    case "RenterID":
						decimal renterId = 0;
						bool parsed = Decimal.TryParse(text, out renterId);
                        renters = renters.Where(r => !string.IsNullOrEmpty(r.PmsId) && r.PmsId.Contains(text) || (parsed && r.PublicRenterId == renterId)).ToList();
                        payments = payments.Where(p => renters.Any(r => r.RenterId == p.RenterId)).ToList();
                        break;
                    case "PropertyName":
                        properties = properties.Where(p => p.PropertyName.ToLower().Contains(text)).ToList();
                        payments = payments.Where(p => renters.Any(r => r.RenterId == p.RenterId)).ToList();
                        break;
                    case "CompanyName":
                        companies = companies.Where(c => c.CompanyName.ToLower().Contains(text)).ToList();
                        properties = properties.Where(p => companies.Any(c => c.CompanyId == p.CompanyId)).ToList();
                        renters = renters.Where(r => r.RenterPropertyId.HasValue && properties.Any(p => p.PropertyId == r.RenterPropertyId.Value)).ToList();
                        payments = payments.Where(p => renters.Any(r => r.RenterId == p.RenterId)).ToList();
                        break;
                    case "RentAmount":
                        decimal rentAmount = Convert.ToDecimal(text);
                        payments = payments.Where(p => p.RentAmount.HasValue && p.RentAmount.Value == rentAmount).ToList();
                        break;
                    case "Portal":
                        payments = payments.Where(p => paymentChannels.Any(pc => pc.PaymentChannelName.ToLower().Equals(text, StringComparison.OrdinalIgnoreCase) && pc.PaymentChannelId == p.PaymentChannelId)).ToList();
                        break;
                    case "Status":
                        payments = payments.Where(p => paymentStatuses.Any(s => s.PaymentStatusName.Equals(text, StringComparison.OrdinalIgnoreCase) && s.PaymentStatusId == p.PaymentStatusId)).ToList();
                        break;
                    default:
                        decimal searchAsDecimal = 0;
                        decimal.TryParse(text, out searchAsDecimal);
                        companies = companies.Where(c => c.CompanyName.ToLower().Contains(text)).ToList();
                        properties = properties.Where(p => companies.Any(c => c.CompanyId == p.CompanyId)).ToList();
                        renters = renters.Where(r => 
                            (r.RenterPropertyId.HasValue && properties.Any(p => p.PropertyId == r.RenterPropertyId.Value))
                            || (!string.IsNullOrEmpty(r.PmsId) && r.PmsId.Contains(text))
                            || r.PublicRenterId == searchAsDecimal)
                            .ToList();
                        payments = payments.Where(p =>
                                    (!string.IsNullOrEmpty(p.TransactionId) && p.TransactionId.Contains(text))
                                || (payers.Any(payer => payer.FirstName.ToLower().Contains(text) && payer.PayerId == p.PayerId))
                                || (payers.Any(payer => payer.LastName.ToLower().Contains(text) && payer.PayerId == p.PayerId))
                                ||  renters.Any(r => r.RenterId == p.RenterId)
                                || (p.RentAmount.HasValue && p.RentAmount.Value == searchAsDecimal)
                                || paymentStatuses.Any(s => s.PaymentStatusName.Equals(text, StringComparison.OrdinalIgnoreCase) && s.PaymentStatusId == p.PaymentStatusId))
                            .ToList();
                        break;
                }
                
                payments.ForEach(p =>
                {
                    p.PaymentChannelName = paymentChannels.First(pc => pc.PaymentChannelId == p.PaymentChannelId).PaymentChannelName;
                    p.PropertyId = renters.Where(r => r.RenterId == p.RenterId).DefaultIfEmpty(new Renter(){ RenterPropertyId = 0}).First().RenterPropertyId;
					p.Unit = renters.Where(r => r.RenterId == p.RenterId).DefaultIfEmpty(new Renter() { Unit = "" }).First().Unit;
                    p.CompanyId = properties.Where(prop => p.PropertyId.HasValue && p.PropertyId.Value == prop.PropertyId).DefaultIfEmpty(new PropertyListItem() { CompanyId = 0, PropertyName = "" }).First().CompanyId;
                    p.PropertyName = p.PropertyId.HasValue ? properties.Where(prop => prop.PropertyId == p.PropertyId.Value).DefaultIfEmpty(new PropertyListItem(){PropertyName = ""}).First().PropertyName : "";
                    p.CompanyName = p.CompanyId.HasValue && p.CompanyId.Value > 0 ? companies.First(c => c.CompanyId == p.CompanyId.Value).CompanyName : "";
                    p.PaymentStatus = paymentStatuses.First(s => s.PaymentStatusId == p.PaymentStatusId).PaymentStatusName;
					p.PayerFirstName = payers.Where(payer => p.PayerId == payer.PayerId).FirstOrDefault().FirstName;
					p.PayerLastName = payers.Where(payer => p.PayerId == payer.PayerId).FirstOrDefault().LastName;
                });

				//filter the list based on PM access level
				if (!EfxFramework.Helpers.Helper.IsRpoAdmin)
				{
					var userProperties = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyStaffId == EfxFramework.Helpers.Helper.CurrentUserId).ToList();
					return payments.Where(p => p.PropertyId.HasValue && p.PropertyId.Value > 0 && userProperties.Exists(up => up.PropertyId == p.PropertyId)).ToList();
				}
                return payments.OrderByDescending(p => p.TransactionDateTime).ToList();
            }
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
        [HttpPost]
        [ActionName("voidpayment")]
        public bool Void(Payment paymentEntity)
        {
            using (var entity = new RPOEntities())
            {
                //get payment object
                var payment = entity.Payments.FirstOrDefault(pmt => pmt.PaymentId == paymentEntity.PaymentId);
                if (payment == null)
                    return false;
                //get renter
                var renter = entity.Renters.FirstOrDefault(r => r.RenterId == payment.RenterId);
                if (renter == null)
                    return false;
                //get property
                var property = entity.Properties.FirstOrDefault(p => p.PropertyId == renter.RenterPropertyId);
                if (property == null)
                    return false;
                //get transaction
                var transaction = entity.Transactions.FirstOrDefault(t => t.PaymentId == payment.PaymentId);
                if (transaction == null)
                    return false;
                //get current status of transaction
                AchStatusResponse status = AchService.GetAchStatus(property.RentalAccountAchClientId, transaction.ExternalTransactionId);
                if (status.Result == GeneralResponseResult.Success && (status.StatusCode.HasValue && status.StatusCode.Value == 1))
                {
                    var cancelResult = AchService.CancelPendingAchTransaction(property.RentalAccountAchClientId, transaction.ExternalTransactionId);
                    //could not get status of transaction
                    if (cancelResult.Result != GeneralResponseResult.Success)
                        return false;
                    {
                        //create a new void record
                        var subTransaction = new Transaction
                        {
                            InternalTransactionId = Transaction.GetNewInternalTransactionId(),
                            ExternalTransactionId = cancelResult.TransactionId,
                            PaymentTypeId = (int)payment.PaymentTypeId,
                            FirstName = transaction.FirstName,
                            LastName = transaction.LastName,
                            EmailAddress = transaction.EmailAddress,
                            StreetAddress = transaction.StreetAddress,
                            StreetAddress2 = transaction.StreetAddress2,
                            City = transaction.City,
                            StateProvinceId = transaction.StateProvinceId,
                            PostalCode = transaction.PostalCode,
                            PhoneNumber = transaction.PhoneNumber,
                            BankAccountNumber = transaction.BankAccountNumber.ToString(),
                            BankRoutingNumber = transaction.BankRoutingNumber.ToString(),
                            BankAccountTypeId = transaction.BankAccountTypeId,
                            PaymentAmount = 0,
                            PaymentId = null,
                            PaymentStatusId = (int)EfxFramework.PublicApi.Payment.PaymentStatus.Voided,
                            ResponseResult = (int)cancelResult.Result,
                            ResponseMessage = "VOID",
                            TransactionDate = DateTime.UtcNow,
                            PropertyId = property.PropertyId
                        };
                        int newTransactionId = Transaction.SetRPOTransaction(subTransaction, transaction.InternalTransactionId);
                    }
                    payment.PaymentStatusId = 8;
                    entity.SaveChanges();
                    return true;
                }

            }
            return false;
        }
    }
}