﻿using EfxFramework;
using RPO;
using RPO.Reports;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;

namespace Domus.API
{
    public class ReportController : ApiController
    {
        [HttpPost]
        [ActionName("applicantpayment")]
        public List<ApplicantPortalPaymentReport> GetApplicantPortalPaymentReport(ReportQuery query)
        {
            RPOReport report = new RPOReport();
            return report.ApplicantPortalPaymentReport(query);
        }

        [HttpPost]
        [ActionName("autopayment")]
        public List<AutoPaymentReport> GetAutoPaymentReport(ReportQuery query)
        {
            RPOReport report = new RPOReport();
            return report.AutoPaymentReport(query);
        }

        [HttpPost]
        [ActionName("billing")]
        public List<BillingReport> GetBillingReport(ReportQuery query)
        {
            RPOReport report = new RPOReport();
            return report.BillingReport(query);
        }

        [HttpPost]
        [ActionName("charity")]
        public List<CharityReport> GetCharityReport(ReportQuery query)
        {
            RPOReport report = new RPOReport();
            return report.CharityReport(query);
        }

        [HttpPost]
        [ActionName("detaileddeposit")]
        public List<DetailedDepositReport> GetDetailedDepositReport(ReportQuery query)
        {
            RPOReport report = new RPOReport();
            return report.DetailedDepositReport(query);
        }

        [HttpGet]
        [ActionName("allpaymenttypes")]
        public List<PaymentType> GetAllPaymentTypes()
        {
            RPOReport report = new RPOReport();
            return report.AllPaymentTypes();
        }

        [HttpGet]
        [ActionName("allproperties")]
        public List<PropertiesListing> GetAllProperties()
        {
            RPOReport report = new RPOReport();
            return report.AllProperties();
        }

        [HttpGet]
        [ActionName("allrenters")]
        public List<RentersListing> GetAllRenters()
        {
            RPOReport report = new RPOReport();
            return report.AllRenters();
        }

        [HttpPost]
        [ActionName("propertiesbystaffid")]
        public List<PropertiesListing> GetPropertiesByStaffId(ReportQuery query)
        {
            RPOReport report = new RPOReport();
            int Id;
            Int32.TryParse(HttpContext.Current.User.Identity.Name, out Id);
            List<RoleName> roleListAdmin = new List<RoleName> { RoleName.EfxPowerAdministrator };
            var HasAdminPermission = RoleManager.IsUserInRole(EfxFramework.RoleType.EfxAdministrator, Id, roleListAdmin);
            if (HasAdminPermission)
                query.PropertyStaffId = -1;
            else
                query.PropertyStaffId = Id;
            return report.PropertiesByStaffId(query);
        }

        [HttpPost]
        [ActionName("rentersbypropertyid")]
        public List<RentersListing> GetRentersByPropertyId(ReportQuery query)
        {
            RPOReport report = new RPOReport();
            return report.RentersByPropertyId(query);
        }

        [HttpPost]
        [ActionName("lead")]
        public List<LeadReport> GetLeadReport(ReportQuery query)
        {
            RPOReport report = new RPOReport();
            return report.LeadReport(query);
        }

        [HttpPost]
        [ActionName("paymentexportsummary")]
        public List<PaymentExportSummaryReport> GetPaymentExportSummaryReport(ReportQuery query)
        {
            RPOReport report = new RPOReport();
            return report.PaymentExportSummaryReport(query);
        }

        [HttpPost]
        [ActionName("paymentreceipt")]
        public List<PaymentReceiptReport> GetPaymentReceiptReport(ReportQuery query)
        {
            RPOReport report = new RPOReport();
            return report.PaymentReceiptReport(query);
        }

        [HttpPost]
        [ActionName("paymentsummary")]
        public List<PaymentSummaryReport> GetPaymentSummaryReport(ReportQuery query)
        {
            RPOReport report = new RPOReport();
            return report.PaymentSummaryReport(query);
        }

        [HttpPost]
        [ActionName("residentdataimportsummary")]
        public List<ResidentDataImportSummaryReport> GetResidentDataImportSummaryReport(ReportQuery query)
        {
            RPOReport report = new RPOReport();
            return report.ResidentDataImportSummaryReport(query);
        }

        [HttpPost]
        [ActionName("renterspastdue")]
        public List<RentersPastDueReport> GetRentersPastDueReport(ReportQuery query)
        {
            RPOReport report = new RPOReport();
            return report.RentersPastDueReport(query);
        }

        [HttpPost]
        [ActionName("transactionsummary")]
        public List<TransactionSummaryReport> GetTransactionSummaryReport(ReportQuery query)
        {
            RPOReport report = new RPOReport();
            return report.TransactionSummaryReport(query);
        }
    }
}