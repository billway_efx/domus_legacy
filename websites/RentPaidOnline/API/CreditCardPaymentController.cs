﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web.Http;
using EfxFramework;
using EfxFramework.PaymentMethods;
using RPO;
using Transaction = EfxFramework.Transaction;

namespace RentPaidOnline.API
{
    public class CreditCardPaymentController : ApiController
    {
        private static string AchConnString =>
            System.Configuration.ConfigurationManager.ConnectionStrings["ACHAcquireConnectionString"]
                .ConnectionString;

        //Task 00232dWillis Added Void Action to allow for void credit card transaction
        [HttpPost]
        [ActionName("void")]
        public bool VoidPayment(RPO.Payment voidedPayment)
        {
            var sixPm = new DateTime(Convert.ToInt16(System.DateTime.Now.Year),
                Convert.ToInt16(System.DateTime.Now.Month), Convert.ToInt16(System.DateTime.Now.Day));
            var sixPmYesterday = new DateTime(Convert.ToInt16(System.DateTime.Now.Year),
                Convert.ToInt16(System.DateTime.Now.Month), Convert.ToInt16(System.DateTime.Now.Day));
            DateTime rsy = sixPmYesterday.AddDays(-1);
            var todayHoursAdjusted = sixPm.AddHours(18);
            DateTime yesterdayHOursAdjusted = rsy.AddHours(18);
            using (var entity = new RPOEntities())
            {
                //Getting Payment Object Here
                var payment = entity.Payments.FirstOrDefault(pmt => pmt.PaymentId == voidedPayment.PaymentId);
                var paymentBalance = (decimal) payment.Balance; // +(decimal)payment.OtherAmount2;
                var transactionDate = payment.TransactionDateTime.ToLocalTime();
                if (payment == null || Convert.ToString(payment.PaymentId) == "") return false;

                //Getting EFXPayment
                var efxPayment = new EfxFramework.Payment(payment.PaymentId);
                if (efxPayment == null || Convert.ToString(efxPayment.PaymentId) == "") return false;

                //Getting Transaction Object
                //int Tid = Convert.ToInt32(payment.TransactionId.FirstOrDefault());
                var transaction =
                    entity.Transactions.FirstOrDefault(t => t.InternalTransactionId == payment.TransactionId);
                if (transaction == null || Convert.ToString(transaction.TransactionId) == "") return false;

                //Getting Renter Object
                var renter = entity.Renters.FirstOrDefault(r => r.RenterId == payment.RenterId);
                if (renter == null || Convert.ToString(renter.RenterId) == "") return false;

                //Getting Property Object
                //var property = entity.Properties.FirstOrDefault(p => p.PropertyId == transaction.PropertyId);
                var property = EfxFramework.Property.GetPropertyByRenterId(renter.RenterId);
                if (property == null || Convert.ToString(property) == "") return false;

                if (payment.TransactionDateTime.ToLocalTime() <= todayHoursAdjusted &&
                    payment.TransactionDateTime >= yesterdayHOursAdjusted)
                {
                    SqlCommand commACH = new SqlCommand();
                    SqlCommand Achuserid = new SqlCommand();
                    if (Convert.ToInt16(property.GroupSettlementPaymentFlag) == 0)
                    {
                        var result = EfxFramework.PaymentMethods.CreditCard.CreditCardPayment.VoidCreditCardPayment(
                            EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword,
                            transaction.ExternalTransactionId);
                        if (result.Result != GeneralResponseResult.Success)
                        {
                            EmailNotice("Fail", (renter.FirstName + " " + renter.LastName), property.PropertyName,
                                Convert.ToString(payment.RentAmount + payment.OtherAmount2),
                                Convert.ToString(transaction.TransactionDate), transaction.InternalTransactionId,
                                EfxFramework.EfxSettings.SupportEmail);

                            return false;
                        }

                        if (result.Result != GeneralResponseResult.Success) return true;
                        payment.Balance += transaction.PaymentAmount;
                        payment.PaymentStatusId = 8;
                        payment.LastUpdated = DateTime.UtcNow;
                        payment.PaymentStatusId = (int) EfxFramework.PublicApi.Payment.PaymentStatus.Voided;
                        entity.SaveChanges();

                        //cakel: TASK 00506
                        if ((int) payment.PaymentStatusId == 8)
                        {
                            MiscDatabase.UpdateVoidStatusForPayment(payment.PaymentId, (int) payment.PaymentStatusId);
                            var leases = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);
                            var LeaseID = leases.LeaseId;
                            DLAccess.UpdateRenterCurrentBalancev3(transactionDate, LeaseID, paymentBalance);
                        }

                        //cakel: BUGID00232
                        EmailNotice("Void", (renter.FirstName + " " + renter.LastName), property.PropertyName,
                            Convert.ToString(payment.RentAmount + payment.OtherAmount2),
                            Convert.ToString(transaction.TransactionDate), transaction.InternalTransactionId,
                            renter.PrimaryEmailAddress);

                        //Salcedo - Task 00076
                        var log = new RPO.ActivityLog();
                        log.WriteLog(RPO.Helpers.Helper.CurrentUserId.ToString(),
                            "Transaction payment " + transaction.ExternalTransactionId + " has been voided.", 0,
                            renter.RenterId.ToString(), true);

                        if (payment.PaymentStatusId != 8) return true;
                        var voidProperty = EfxFramework.Property.GetPropertyByRenterId(renter.RenterId);

                        //change status of payment here, do not create payment
                        //Grab the external ID to compare to the merchantTransactionID

                        var realExternalId = transaction.ExternalTransactionId;

                        transaction.TransactionStatus = Convert.ToString(8);
                        if (transaction.TransactionStatus != Convert.ToString(8)) return true;
                        var connACH = new SqlConnection(AchConnString);
                        commACH.CommandText = "UPDATE [ACHAcquire].[dbo].[Transactions] " +
                                              "SET TRANSACTIONSTATUS = 8 " +
                                              "WHERE MERCHANTTRANSACTIONID = '" + Convert.ToString(realExternalId) +
                                              "'";
                        connACH.Open();
                        commACH.Connection = connACH;
                        commACH.ExecuteNonQuery();
                        connACH.Close();
                        //Get achTransaction ID
                        var achTransactionIdFromTransactionObject = transaction.ExternalTransactionId;
                        var commAchId = new SqlCommand();
                        var achTransactionIDFromTransactionObject = transaction.ExternalTransactionId;
                        commAchId.CommandText =
                            "SELECT TRANSACTIONID FROM [ACHAcquire].[dbo].[Transactions] WHERE MERCHANTTRANSACTIONID = '" +
                            realExternalId + "'";

                        var connAchId = new SqlConnection(AchConnString);
                        commAchId.Connection = connAchId;
                        connAchId.Open();

                        var achTransactionId = commAchId.ExecuteNonQuery();
                        Achuserid.CommandText =
                            "Select MERCHANTID from [ACHAcquire].[dbo].[Transactions] where MerchantTransactionID = '" +
                            achTransactionIdFromTransactionObject + "'";
                        Achuserid.Connection = connAchId;

                        var merchantIdForAchUserId = Achuserid.ExecuteNonQuery();
                        connAchId.Close();

                        var achValidate =
                            EfxFramework.PaymentMethods.Ach.AchService.CancelPendingAchTransaction(
                                Convert.ToString(merchantIdForAchUserId), Convert.ToString(achTransactionId));


                        var achDBResult = achValidate.Result;
                        if (achDBResult == 0) return true;

                        var tranStatus =
                            string.Concat(
                                "Unable to cancel the pending ACH Transaction while attempting a credit card transaction void. ",
                                achValidate.Result, ": ", achValidate.StatusDescription);
                        SendAchErrorEmail(tranStatus, property.PropertyName, transaction.FirstName,
                            transaction.LastName, transaction.TransactionDate.ToString(),
                            transaction.InternalTransactionId);
                    }
                    else
                    {
                        var result = EfxFramework.PaymentMethods.CreditCard.CreditCardPayment.VoidCreditCardPayment(
                            EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword,
                            transaction.ExternalTransactionId);
                        if (result.Result != GeneralResponseResult.Success)
                        {
                            EmailNotice("Fail", (renter.FirstName + " " + renter.LastName), property.PropertyName,
                                Convert.ToString(payment.RentAmount + payment.OtherAmount2),
                                Convert.ToString(transaction.TransactionDate), transaction.InternalTransactionId,
                                EfxSettings.SupportEmail);

                            return false;
                        }

                        if (result.Result != GeneralResponseResult.Success) return true;

                        payment.Balance += transaction.PaymentAmount;
                        payment.PaymentStatusId = 8;
                        payment.LastUpdated = DateTime.UtcNow;
                        payment.PaymentStatusId = (int) EfxFramework.PublicApi.Payment.PaymentStatus.Voided;
                        entity.SaveChanges();

                        if ((int) payment.PaymentStatusId == 8)
                        {
                            MiscDatabase.UpdateVoidStatusForPayment(payment.PaymentId, (int) payment.PaymentStatusId);

                            var leases = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);
                            var LeaseID = leases.LeaseId;
                            DLAccess.UpdateRenterCurrentBalancev3(transactionDate, LeaseID, paymentBalance);
                        }

                        EmailNotice("Void", (renter.FirstName + " " + renter.LastName), property.PropertyName,
                            Convert.ToString(payment.RentAmount + payment.OtherAmount2),
                            Convert.ToString(transaction.TransactionDate), transaction.InternalTransactionId,
                            renter.PrimaryEmailAddress);
                    }
                }
                else
                    return false;
            }

            return true;
        }

        public void EmailNotice(string EmailType, string ResidentName, string PropertyName, string Amount,
            string TransDate, string TransID, string EmailAddress)
        {
            var templateId = "";
            var emailString = "";

            const string sqlString = "SELECT TEMPLATETEXT FROM EMAILTEMPLATES WHERE TEMPLATEID =";
            var emailType = EmailType;
            templateId = emailType == "Void" ? "33" : "34";

            var conn = new SqlConnection(EfxFramework.EfxSettings.ConnectionString);
            var com = new SqlCommand(sqlString + templateId, conn)
            {
                CommandType = CommandType.Text
            };
            conn.Open();
            var reader = com.ExecuteReader(CommandBehavior.CloseConnection);
            reader.Read();
            if (reader.HasRows)
            {
                emailString = reader["TEMPLATETEXT"].ToString();
            }

            emailString = emailString.Replace("##RESIDENTNAME##", ResidentName)
                .Replace("##PROPERTYNAME##", PropertyName)
                .Replace("##AMOUNT##", Convert.ToString(Amount))
                .Replace("##TRANSACTIONDATE##", Convert.ToString(TransDate))
                .Replace("##VOIDIDNUMBER## ", TransID);

            const string subjectVoid = "Your Domus Payment Has Been Voided";
            EfxFramework.BulkMail.BulkMail.SendMailMessage(
                null,
                "EFX",
                false,
                EfxFramework.EfxSettings.SendMailUsername,
                EfxFramework.EfxSettings.SendMailPassword,
                new MailAddress("payments@Domus.com"),
                subjectVoid,
                emailString,
                new List<MailAddress> {new MailAddress(EmailAddress)});
        }

        [HttpPost]
        [ActionName("refund")]
        public bool RefundPayment(RPO.Refund refund)
        {
            using (var entity = new RPOEntities())
            {
                //get payment object
                var payment = entity.Payments.FirstOrDefault(pmt => pmt.PaymentId == refund.PaymentId);
                if (payment == null) return false;
                var efxPayment = new EfxFramework.Payment(payment.PaymentId);

                //get transaction
                var transaction = entity.Transactions.FirstOrDefault(t => t.PaymentId == payment.PaymentId);
                if (transaction == null) return false;

                //get renter
                var renter = entity.Renters.FirstOrDefault(r => r.RenterId == payment.RenterId);
                if (renter == null) return false;

                //get property
                var property = entity.Properties.FirstOrDefault(p => p.PropertyId == transaction.PropertyId);
                if (property == null) return false;
                var result = EfxFramework.PaymentMethods.CreditCard.CreditCardPayment.ReverseCreditCardPayment(
                    EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword,
                    transaction.ExternalTransactionId, refund.Amount);
                if (result.Result != GeneralResponseResult.Success) return false;

                int newPaymentStatusId;
                if (refund.Amount != transaction.PaymentAmount)
                {
                    newPaymentStatusId =
                        result.Result == GeneralResponseResult.Success
                            ? (int) EfxFramework.PublicApi.Payment.PaymentStatus.PartiallyRefunded
                            : (int) EfxFramework.PublicApi.Payment.PaymentStatus.Declined;
                }
                else
                {
                    newPaymentStatusId =
                        result.Result == GeneralResponseResult.Success
                            ? (int) EfxFramework.PublicApi.Payment.PaymentStatus.Approved
                            : (int) EfxFramework.PublicApi.Payment.PaymentStatus.Declined;
                }

                var subTransaction = new Transaction
                {
                    InternalTransactionId = Transaction.GetNewInternalTransactionId(),
                    ExternalTransactionId = result.TransactionId,
                    PaymentTypeId = (int) payment.PaymentTypeId,
                    FirstName = transaction.FirstName,
                    LastName = transaction.LastName,
                    EmailAddress = transaction.EmailAddress,
                    StreetAddress = transaction.StreetAddress,
                    StreetAddress2 = transaction.StreetAddress2,
                    City = transaction.City,
                    StateProvinceId = transaction.StateProvinceId,
                    PostalCode = transaction.PostalCode,
                    PhoneNumber = transaction.PhoneNumber,
                    CreditCardAccountNumber = efxPayment.CreditCardAccountNumber,
                    CreditCardExpirationMonth = efxPayment.CreditCardExpirationMonth,
                    CreditCardExpirationYear = efxPayment.CreditCardExpirationYear,
                    CreditCardHolderName = efxPayment.CreditCardHolderName,
                    PaymentAmount = refund.Amount * -1,
                    PaymentId = payment.PaymentId,
                    PaymentStatusId = newPaymentStatusId,
                    ResponseResult = (int) result.Result,
                    ResponseMessage = result.ResponseMessage,
                    TransactionDate = DateTime.UtcNow,
                    PropertyId = property.PropertyId,
                    CCAchSettledFlag = true
                };


                var ccResponseResult = subTransaction.ResponseResult;
                var log = new ActivityLog();
                var amountRefunded = refund.Amount;

                if (payment.PaymentStatusId == 1 && ccResponseResult == 0)
                {
                    var refundProperty = EfxFramework.Property.GetPropertyByRenterId(renter.RenterId);
                    var achValidate =
                        EfxFramework.PaymentMethods.Ach.AchService.ProcessAchPayment(EfxSettings.AchUserId,
                            EfxSettings.AchPassword, EfxSettings.CcSettlementAchId, amountRefunded,
                            refundProperty.CCDepositRoutingNumber, refundProperty.CCDepositAccountNumber, "Checking",
                            subTransaction.FirstName, subTransaction.LastName, property.StreetAddress, "",
                            property.City,
                            refundProperty.DisplayStateProvince, property.PostalCode, "");


                    //cakel: BUGID00061 Added code for response from ACH DB - This will send email to manager about the ACH insert failure.
                    var achdbResult = achValidate.Result;
                    if (achdbResult != 0)
                    {
                        var _result = achValidate.Result.ToString();

                        //CMallory - Task 00607 - Put logging here to capture if a failure.
                        log.WriteLog(RPO.Helpers.Helper.CurrentUserId.ToString(),
                            string.Concat("Error occured while processing refund for External Transaction: ",
                                subTransaction.ExternalTransactionId,
                                " for the refunded the amount of ", refund.Amount), 0, renter.RenterId.ToString());
                        SendAchErrorEmail(_result, property.PropertyName, subTransaction.FirstName,
                            subTransaction.LastName, subTransaction.TransactionDate.ToString(),
                            subTransaction.InternalTransactionId);
                    }

                    UpdateCcSettledFlagByTransaction(subTransaction.InternalTransactionId);
                }

                //update payment object to reflect most recent transaction
                payment.PaymentStatusId = (int) EfxFramework.PublicApi.Payment.PaymentStatus.Refunded;
                if (result.Result == GeneralResponseResult.Success)
                    payment.Balance += subTransaction.PaymentAmount;
                payment.PaymentStatusId = payment.Balance == 0
                    ? (int) EfxFramework.PublicApi.Payment.PaymentStatus.Refunded
                    : (int) EfxFramework.PublicApi.Payment.PaymentStatus.PartiallyRefunded;
                payment.LastUpdated = DateTime.UtcNow;
                entity.SaveChanges();
                Transaction.SetRPOTransaction(subTransaction, transaction.InternalTransactionId);

                try
                {
                    log.WriteLog(RPO.Helpers.Helper.CurrentUserId.ToString(), string.Concat("Transaction payment ",
                        transaction.ExternalTransactionId, " has been refunded the amount of ",
                        refund.Amount.ToString()), 0, renter.RenterId.ToString(), true);
                    //cakel: BUGID00300
                    SendRefundEmail(property.PropertyId, renter.RenterId, refund.Amount, payment.PaymentId);
                }
                catch
                {
                    return true;
                }
            }

            return true;
        }

        //cakel: BUGID00061 Updated Transaction table with CCSettledFlag to True
        private static void UpdateCcSettledFlagByTransaction(string internalTransactionId)
        {
            var conString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"]
                .ConnectionString;
            var con = new SqlConnection(conString);
            SqlCommand com = new SqlCommand("usp_Payment_UpdateCCSettledFlagByTransaction", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            com.Parameters.Add("@ID", System.Data.SqlDbType.NVarChar, 10).Value = internalTransactionId;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }

        private static void SendAchErrorEmail(string tranStatus, string propertyName, string firstName, string lastName,
            string transDate, string transId)
        {
            var mailBody =
                "<p>An error occured when trying to insert record into ACH DB for submission.  Please check the properties Credit Card Account on file.</p><br />";
            mailBody += "Status: " + tranStatus + "<br />";
            mailBody += "Property Name: " + propertyName + "<br />";
            mailBody += "Payer First Name: " + firstName + "<br />";
            mailBody += "Payer Last Name: " + lastName + "<br />";
            mailBody += "Transaction Date: " + transDate + "<br />";
            mailBody += "Transaction ID: " + transId + "<br />";

            var achErrorMail = new MailMessage
            {
                Subject = "ACH DB Insert Error",
                Body = mailBody,
                From = new MailAddress("support@Domusme.com", "RPO Support")
            };
            achErrorMail.To.Add(new MailAddress(EfxSettings.SupportEmail, "RPO Support Admin"));
            achErrorMail.IsBodyHtml = true;
            var rpoSmtp = new SmtpClient
            {
                Host = EfxSettings.SmtpHost
            };

            rpoSmtp.Send(achErrorMail);
        }

        public static void SendRefundEmail(int propertyId, int renterId, decimal refundAmount, int paymentId)
        {
            using (var con = new SqlConnection(EfxSettings.ConnectionString))
            {
                con.Open();
                using (var com = new SqlCommand("Email_SendRefundNotification_DomusMe", con))
                {
                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 360;

                    com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = propertyId;
                    com.Parameters.Add("@RenterID", SqlDbType.Int).Value = renterId;
                    com.Parameters.Add("@RefundAmount", SqlDbType.Decimal).Value = refundAmount;
                    com.Parameters.Add("@PaymentID", SqlDbType.Int).Value = paymentId;

                    com.ExecuteNonQuery();
                }
            }
        }
    }
}