﻿using EfxFramework;
using EfxFramework.PaymentMethods;
using EfxFramework.PaymentMethods.Ach;
using RPO;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using Payment = RPO.Payment;
using StateProvince = EfxFramework.StateProvince;
using Transaction = EfxFramework.Transaction;

namespace RentPaidOnline.API
{
    public class AchPaymentController : ApiController
    {
        [HttpPost]
        [ActionName("void")]
        public bool VoidPayment(Payment paymentEntity)
        {
            using (var entity = new RPOEntities())
            {
                //get payment object
                var payment = entity.Payments.FirstOrDefault(pmt => pmt.PaymentId == paymentEntity.PaymentId);
                if (payment == null) return false;

                var paymentBalance = (decimal)payment.Balance;
                var transactionDate = payment.TransactionDateTime.ToLocalTime();

                //get transaction
                var transaction = entity.Transactions.FirstOrDefault(t => t.PaymentId == payment.PaymentId);
                if (transaction == null) return false;
                
                //get renter
                var renter = entity.Renters.FirstOrDefault(r => r.RenterId == payment.RenterId);
                if (renter == null) return false;
                
                //get property
                var property = entity.Properties.FirstOrDefault(p => p.PropertyId == transaction.PropertyId);
                if (property == null) return false;
                
                //get current status of transaction
                var status = AchService.GetAchStatus(property.RentalAccountAchClientId, transaction.ExternalTransactionId);
                if (status.Result != GeneralResponseResult.Success) return true;
                
                //CMallory - Task 00404 - Reset CurrentBalanceDue in Lease table with void amount.
                var leases = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);
                var leaseId = leases.LeaseId;
                DLAccess.UpdateRenterCurrentBalancev3(transactionDate,leaseId,paymentBalance);

                //Salcedo - Task 00076
                var log = new RPO.ActivityLog();
                log.WriteLog(RPO.Helpers.Helper.CurrentUserId.ToString(), "Transaction payment " + transaction.ExternalTransactionId + " has been voided.", 0, renter.RenterId.ToString(), true);

                if (status.StatusCode.HasValue && status.StatusCode.Value == 1)
                {
                    var cancelResult = AchService.CancelPendingAchTransaction(property.RentalAccountAchClientId,
                        transaction.ExternalTransactionId);
                    
                    //could not get status of transaction
                    if (cancelResult.Result != GeneralResponseResult.Success) return false;
                    
                    //create a new void record
                    var subTransaction = new Transaction
                    {
                        InternalTransactionId = Transaction.GetNewInternalTransactionId(),
                        ExternalTransactionId = cancelResult.TransactionId,
                        PaymentTypeId = (int)payment.PaymentTypeId,
                        FirstName = transaction.FirstName,
                        LastName = transaction.LastName,
                        EmailAddress = transaction.EmailAddress,
                        StreetAddress = transaction.StreetAddress,
                        StreetAddress2 = transaction.StreetAddress2,
                        City = transaction.City,
                        StateProvinceId = transaction.StateProvinceId,
                        PostalCode = transaction.PostalCode,
                        PhoneNumber = transaction.PhoneNumber,
                        BankAccountNumber = transaction.BankAccountNumber.ToString(),
                        BankRoutingNumber = transaction.BankRoutingNumber.ToString(),
                        BankAccountTypeId = transaction.BankAccountTypeId,
                        PaymentAmount = 0,
                        PaymentId = payment.PaymentId,
                        PaymentStatusId = (int)EfxFramework.PublicApi.Payment.PaymentStatus.Voided,
                        ResponseResult = (int)cancelResult.Result,
                        ResponseMessage = "VOID",
                        TransactionDate = DateTime.UtcNow,
                        PropertyId = property.PropertyId
                    };
                    var newTransactionId = Transaction.SetRPOTransaction(subTransaction,
                        transaction.InternalTransactionId);
                       
                    payment.PaymentStatusId = (int)EfxFramework.PublicApi.Payment.PaymentStatus.Voided;
                    payment.LastUpdated = DateTime.UtcNow;
                    payment.Balance = 0;
                    entity.SaveChanges();
                }
                else
                {
                    //update transaction status
                    transaction.PaymentStatusId = Convert.ToInt32(RPO.Transaction.GetDescription((RPO.Transaction.AchStatus)status.StatusCode.Value));
                    payment.PaymentStatusId = transaction.PaymentStatusId;
                    payment.LastUpdated = DateTime.UtcNow;
                    entity.SaveChanges();
                    return false;
                }
            }
            return true;
        }

        [HttpPost]
        [ActionName("refund")]
        public bool RefundPayment(RPO.Refund refund)
        {
            using (var entity = new RPOEntities())
            {
                //ensure amount is negative
                if (refund.Amount > 0)
                    refund.Amount *= -1;
                
                //get payment object
                var payment = entity.Payments.FirstOrDefault(pmt => pmt.PaymentId == refund.PaymentId);
                if (payment == null) return false;

                var efxPayment = new EfxFramework.Payment(payment.PaymentId);
                
                //get transaction
                var transaction = entity.Transactions.FirstOrDefault(t => t.PaymentId == payment.PaymentId);
                if (transaction == null) return false;
                
                //get renter
                var renter = entity.Renters.FirstOrDefault(r => r.RenterId == payment.RenterId);
                if (renter == null) return false;
                
                //get property
                var property = entity.Properties.FirstOrDefault(p => p.PropertyId == transaction.PropertyId);
                if (property == null) return false;
                
                //get current status of transaction
                var status = AchService.GetAchStatus(property.RentalAccountAchClientId,
                    transaction.ExternalTransactionId);
                //get current transaction's status
                var state = renter.StateProvinceId.HasValue ? ((StateProvince)renter.StateProvinceId.Value).ToString() : "AK";

                if (status.Result != GeneralResponseResult.Success) return true;
                if (status.StatusCode.Value == (int)RPO.Transaction.AchStatus.Cleared || status.StatusCode.Value == (int)RPO.Transaction.AchStatus.InProcess)

                {
                    //attempt refund
                    var achResponse = AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword,
                        property.RentalAccountAchClientId, refund.Amount, efxPayment.BankRoutingNumber,
                        efxPayment.BankAccountNumber, "CHECKING", renter.FirstName, renter.LastName,
                        renter.StreetAddress, renter.Unit, renter.City, state, renter.PostalCode,
                        renter.MainPhoneNumber);

                    //var transactionReversalId = EfxFramework.TransactionReversal.Set(reversal);
                    var subTransaction = new Transaction
                    {
                        InternalTransactionId = Transaction.GetNewInternalTransactionId(),
                        ExternalTransactionId = achResponse.TransactionId,
                        PaymentTypeId = (int) payment.PaymentTypeId,
                        FirstName = transaction.FirstName,
                        LastName = transaction.LastName,
                        EmailAddress = transaction.EmailAddress,
                        StreetAddress = transaction.StreetAddress,
                        StreetAddress2 = transaction.StreetAddress2,
                        City = transaction.City,
                        StateProvinceId = transaction.StateProvinceId,
                        PostalCode = transaction.PostalCode,
                        PhoneNumber = transaction.PhoneNumber,
                        BankAccountNumber = efxPayment.BankAccountNumber,
                        BankRoutingNumber = efxPayment.BankRoutingNumber,
                        BankAccountTypeId = transaction.BankAccountTypeId,
                        PaymentAmount = refund.Amount,
                        PaymentId = payment.PaymentId,
                        PaymentStatusId =
                            achResponse.Result == GeneralResponseResult.Success
                                ? (int) EfxFramework.PublicApi.Payment.PaymentStatus.Processing
                                : (int) EfxFramework.PublicApi.Payment.PaymentStatus.Declined,
                        ResponseResult = (int) achResponse.Result,
                        ResponseMessage = achResponse.ResponseDescription,
                        TransactionDate = DateTime.UtcNow,
                        PropertyId = property.PropertyId
                    };
                        
                    //update payment object to reflect most recent transaction
                    payment.PaymentStatusId = (int)EfxFramework.PublicApi.Payment.PaymentStatus.Refunded;
                    if (achResponse.Result != GeneralResponseResult.Success) return true;
                    payment.Balance += refund.Amount;

                    payment.PaymentStatusId = payment.Balance == 0 ? (int)EfxFramework.PublicApi.Payment.PaymentStatus.Refunded : 
                        (int)EfxFramework.PublicApi.Payment.PaymentStatus.PartiallyRefunded;
                    payment.LastUpdated = DateTime.UtcNow;
                    entity.SaveChanges();
                    Transaction.SetRPOTransaction(subTransaction, transaction.InternalTransactionId);

                    //Salcedo - Task 00076
                    var log = new RPO.ActivityLog();
                    log.WriteLog(RPO.Helpers.Helper.CurrentUserId.ToString(), "Transaction payment " + transaction.ExternalTransactionId + 
                                                                              " has been refunded the amount of " + refund.Amount.ToString(), 0, renter.RenterId.ToString(), true);
                    //cakel: BUGID00300
                    SendRefundEmail(property.PropertyId, renter.RenterId, refund.Amount, payment.PaymentId);
                }
                else
                {
                    //update transaction status
                    transaction.PaymentStatusId = Convert.ToInt32(RPO.Transaction.GetDescription((RPO.Transaction.AchStatus)status.StatusCode.Value));
                    payment.PaymentStatusId = transaction.PaymentStatusId;
                    payment.LastUpdated = DateTime.UtcNow;

                    //CMallory - Task 00611 - Put Try/Catch block around the database change due to me not finding evidence the record in the database was updated.  Maybe the error happened on the database end and caused the issue?
                    try
                    {
                        entity.SaveChanges();
                        return false;
                    }

                    catch (Exception ex)
                    {
                        var log = new RPO.ActivityLog();
                        log.WriteLog(RPO.Helpers.Helper.CurrentUserId.ToString(), "Error occured while processintg refund for External Transaction: " + transaction.ExternalTransactionId + 
                                                                                  " for the refunded the amount of " + refund.Amount.ToString(), 0, renter.RenterId.ToString() + " with an error message of " + ex.Message, true);
                        return false;

                    } 
                }
            }
            return true;
        }
        [HttpGet]
        public Payment Get(int id)
        {
            using (var entity = new RPOEntities())
            {
                var payment = entity.Payments.Include("Transactions").First(c => c.PaymentId == id);
                var paymentStatuses = entity.PaymentStatus.ToList();
                payment.PaymentStatus = paymentStatuses.First(s => s.PaymentStatusId == payment.PaymentStatusId).PaymentStatusName;
                payment.PaymentTransactions = payment.Transactions.OrderByDescending(t => t.TransactionDate).ToList();
                payment.PaymentTransactions.ForEach(t =>
                {
                    t.TransactionStatus =
                        paymentStatuses.First(s => s.PaymentStatusId == t.PaymentStatusId).PaymentStatusName;
                });
                return payment;
            }
        }

        //cakel: BUGID00300 - Send Refund email when payment is refunded
        public static void SendRefundEmail(int propertyId, int renterId, decimal refundAmount, int paymentId)
        {
            var con = new SqlConnection(EfxFramework.EfxSettings.ConnectionString);
            var com = new SqlCommand("Email_SendRefundNotification_DomusMe", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = propertyId;
            com.Parameters.Add("@RenterID", SqlDbType.Int).Value = renterId;
            com.Parameters.Add("@RefundAmount", SqlDbType.Decimal).Value = refundAmount;
            com.Parameters.Add("@PaymentID", SqlDbType.Int).Value = paymentId;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
    }
}