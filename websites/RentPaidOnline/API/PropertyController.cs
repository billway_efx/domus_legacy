﻿using EfxFramework.Logging;
using RPO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
//using WebAPI.OutputCache.Core;
using WebApi.OutputCache.Core;

namespace Domus.API
{
    public class PropertyController : ApiController
    {
        // GET api/<controller>
        [HttpGet]
        [ActionName("all")]
        public List<PropertyListItem> Get()
        {

            using (var entity = new RPOEntities())
            {                
                var query = Request.RequestUri.ParseQueryString();
                bool forDropDown = Convert.ToBoolean(query["dropdown"]);
                List<PropertyListItem> properties = null;
                if (forDropDown)
                {
                    properties = entity.Properties
                        .Where(p => (p.IsDeleted.HasValue && !p.IsDeleted.Value) || !p.IsDeleted.HasValue)
                        .Select(p => new PropertyListItem { 
                            PropertyId      = p.PropertyId, 
                            PropertyName    = p.PropertyName,
                            CompanyId       = p.CompanyId,
                            PropertyName2 = p.PropertyName2 //CMallory - Task 00521 - Added to Display 2nd Property Name Field
                        })
                        .ToList();
                    properties.Insert(0, new PropertyListItem() { PropertyId = 0, PropertyName = "All Properties" });
                }
                else
                {
                    properties = entity.Properties
						.Where(p => (p.IsDeleted.HasValue && !p.IsDeleted.Value) || !p.IsDeleted.HasValue)
                        .Select(p => new PropertyListItem { 
                            City            = p.City, 
                            CompanyId       = p.CompanyId,
                            IsDeleted       = p.IsDeleted,
                            MainPhoneNumber = p.MainPhoneNumber, 
                            NumberOfUnits   = p.NumberOfUnits,                             
                            PostalCode      = p.PostalCode,
                            ProgramId       = p.ProgramId,
                            PropertyId      = p.PropertyId,
                            PropertyName    = p.PropertyName,                                                        
                            StateProvinceId = p.StateProvinceId,                                                        
                            StreetAddress   = p.StreetAddress,
                            PmsTypeId       = p.PmsTypeId,
                            PropertyName2 = p.PropertyName2 //CMallory - Task 00521 - Added to Display 2nd Property Name Field
                        })
                        .ToList();

                    var companies = entity.Companies
                        .Select(c => new { 
                            CompanyId   = c.CompanyId, 
                            CompanyName = c.CompanyName 
                        })
                        .ToList();

                    var pmsTypes = entity.PmsTypes.ToList();
                    var propertyStaffers = entity.PropertyPropertyStaffs.ToList();
                    var staff = entity.PropertyStaffs.Select(ps => new {
                            FirstName = ps.FirstName, 
                            LastName = ps.LastName,
                            PropertyStaffId = ps.PropertyStaffId                                                      
                        })
                        .ToList();


                    properties.ForEach(p =>
                    {
                        p.State = ((EfxFramework.StateProvince)p.StateProvinceId.Value).ToString();

                        var propertyMainContact = propertyStaffers.FirstOrDefault(pps => pps.PropertyId == p.PropertyId && pps.IsMainContact);
                        if (propertyMainContact != null)
                        {
                            var mainContact = staff.FirstOrDefault(ps => ps.PropertyStaffId == propertyMainContact.PropertyStaffId);
                            if (mainContact != null)
                            {
                                p.MainContactName = mainContact.FirstName + " " + mainContact.LastName;
                            }
                        }

                        var company = companies.FirstOrDefault(c => c.CompanyId == p.CompanyId);
                        if (company != null)
                        {
                            p.CompanyName = company.CompanyName;
                        }

                        if (p.PmsTypeId.HasValue)
                        {
                            p.PmsTypeName = pmsTypes.First(pt => pt.PmsTypeId == p.PmsTypeId.Value).PmsTypeName;
                        }
                    });
                }

                return properties.OrderBy(p => p.PropertyName).ToList();
            }
           
            //Stuff in the way
            //int page = 1;
            //Include("Company").Include("PmsType").Select(p => new Property() {PropertyId = p.PropertyId, PropertyName = p.PropertyName,NumberOfUnits = p.NumberOfUnits, StreetAddress = p.StreetAddress, City = p.City,StateProvinceId=p.StateProvinceId,CompanyName = p.Company.CompanyName,PmsTypeName = p.PmsType.PmsTypeName, MainContactName = entity.PropertyStaffs.Where(ps => ps.PropertyStaffId == propertyMainContact.PropertyStaffId).DefaultIfEmpty(new PropertyStaff(){ FirstName = "", LastName = ""}).Select(ps => ps.FirstName + " " + ps.LastName).First()}).ToList();// Where(p => !p.PropertyName.Contains("OLD")).ToList();
            //if (int.TryParse(query["p"], out page))
            //{
            //    int skip = (page - 1) * 10;
            //    properties = properties.Skip(skip).Take(10);
            //}
            //else
            //{
            //    properties = properties.Take(10);
            //}
        }

        // GET api/<controller>/5
        [HttpGet]
        [ActionName("bycompany")]
        public List<PropertyListItem> GetByCompanyId(int id)
        {
            using (var entity = new RPOEntities())
            {
                int page = 1;
                var query = Request.RequestUri.ParseQueryString();
                var properties = Get().Where(p => p.CompanyId == id);
                if (!string.IsNullOrEmpty(query["q"]))
                    properties = properties.Where(p => p.PropertyName.Contains(query["q"]));
                if (int.TryParse(query["p"], out page))
                {
                    int skip = (page - 1) * 10;
                    properties = properties.Skip(skip).Take(10);
                }
                else
                {
                    properties = properties.Take(10);
                }
                return properties.ToList();
            }
        }
        [HttpGet]
        [ActionName("byuser")]
        public List<PropertyListItem> GetByUserId(int id)
        {
            using (var entity = new RPOEntities())
            {
                //int page = 1;
                var query = Request.RequestUri.ParseQueryString();
                var isCorpAdmin = EfxFramework.Helpers.Helper.IsCorporateAdmin;
                var properties = Get();
                if (isCorpAdmin)
                {
                    var companyId = entity.PropertyStaffs.First(s => s.PropertyStaffId == EfxFramework.Helpers.Helper.CurrentUserId).CompanyId;
                    properties = properties.Where(p => p.CompanyId == companyId).ToList();
                }
                else
                {
                    var userProperties = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyStaffId == id).ToList();
                    properties = properties.Where(p => userProperties.Exists(up => up.PropertyId == p.PropertyId)).ToList();
                }
                
                
                //if (!string.IsNullOrEmpty(query["q"]))
                //    properties = properties.Where(p => p.PropertyName.Contains(query["q"])).ToList();
                //if (int.TryParse(query["p"], out page))
                //{
                //    int skip = (page - 1) * 10;
                //    properties = properties.Skip(skip).Take(10).ToList();
                //}
                //else
                //{
                //    properties = properties.Take(10);
                //}
                if (Convert.ToBoolean(query["dropdown"]) == true)
                    properties.Insert(0, new PropertyListItem() { PropertyId = 0, PropertyName = "All Properties" });
                return properties;
            }
        }
        [HttpGet]
        [ActionName("one")]
        public Property Get(int id)
        {
            using (var entity = new RPOEntities())
            {
				var property = entity.Properties.Include("ProgramEntity").FirstOrDefault(p => p.PropertyId == id && ((p.IsDeleted.HasValue && !p.IsDeleted.Value) || !p.IsDeleted.HasValue));
                if (property != null)
                {
                    var company = entity.Companies.FirstOrDefault(c => c.CompanyId == property.CompanyId && !c.IsDeleted);
                    if (company != null)
                        property.CompanyName = company.CompanyName;
                    var propertyStaffs = entity.PropertyPropertyStaffs.Where(s => s.PropertyId == property.PropertyId).ToList();
                    property.NumberOfLeasingAgents = propertyStaffs.Count();
					property.BillingTypes = property.ProgramEntity != null ? property.ProgramEntity.ProgramDescription : "";//!string.IsNullOrEmpty(property.RentalAccountAchClientId) ? "ACH," : "";
                    //property.BillingTypes += !string.IsNullOrEmpty(property.RentalAccountAchClientId) ? " Credit Card" : "";
                    //property.BillingTypes.Trim().TrimEnd(',');
                    property.State = ((EfxFramework.StateProvince)property.StateProvinceId.Value).ToString();

                    var propertyMainContact = propertyStaffs.FirstOrDefault(pps => pps.PropertyId == property.PropertyId && pps.IsMainContact);
                    if (propertyMainContact != null)
                    {
                        var mainContact = entity.PropertyStaffs.FirstOrDefault(ps => ps.PropertyStaffId == propertyMainContact.PropertyStaffId);
                        if (mainContact != null)
                        {
                            property.MainContactName = mainContact.FirstName + " " + mainContact.LastName;
                        }
                    }
                    var pmsTypes = entity.PmsTypes.ToList();
                    if (property.PmsTypeId.HasValue)
                    {
                        property.PmsTypeName = pmsTypes.First(pt => pt.PmsTypeId == property.PmsTypeId.Value).PmsTypeName;
                    }
                }
                return property;
            }
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
		[HttpGet]
		[ActionName("delete")]
        public bool DeleteProperty(int id)
        {
            using (var entity = new RPOEntities())
            {
				if (EfxFramework.Helpers.Helper.IsRpoAdmin)
				{
					var property = entity.Properties.First(p => p.PropertyId == id);
					property.IsDeleted = true;
					entity.SaveChanges();
					return true;
				}
				else
				{
					var userProperties = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyStaffId == EfxFramework.Helpers.Helper.CurrentUserId).ToList();
					if (userProperties.Any(p => p.PropertyId == id))
					{
						var property = entity.Properties.First(p => p.PropertyId == id);
						property.IsDeleted = true;
						entity.SaveChanges();
						return true;
					}
				}
				return false;
            }
        }
    }
}