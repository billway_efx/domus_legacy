﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using EfxFramework.Helpers;
using EfxFramework.Web.Facade;
using RPO;

namespace RentPaidOnline.API
{
    public class PropertyStaffController : ApiController
    {
		[HttpGet]
		[ActionName("all")]
		public object Get()
		{
			var isCorpAdmin = Helper.IsCorporateAdmin;
			if (!Helper.IsRpoAdmin && !isCorpAdmin)
                return new { data = "", itemCount = 0 };

			using (var entity = new RPOEntities())
			{
                var query = Request.RequestUri.ParseQueryString();
				var propertyStaffs = entity.PropertyStaffs.AsQueryable();

				//if user is type PMC, only show users they can manage
				if (isCorpAdmin)
				{
					var userId = Helper.CurrentUserId;
                    var corpAdminCompanyId = entity.PropertyStaffs.First(s => s.PropertyStaffId == userId).CompanyId;
                    propertyStaffs = propertyStaffs.Where(s => s.CompanyId == corpAdminCompanyId);
				}

				//get only what we need and then page the results
				var propertyStaffList = 
                    (from ps in propertyStaffs 
                     select new 
                     { 
                         ps.PropertyStaffId, 
                         ps.FirstName, 
                         ps.LastName,
                         ps.Company.CompanyName,
                         ps.MobilePhoneNumber, 
                         ps.MainPhoneNumber,
                         ps.PrimaryEmailAddress
                     });
                if (!string.IsNullOrEmpty(query["q"]))
                {
                    var q = query["q"];
                    propertyStaffList = propertyStaffList.Where(ps =>
                        ps.FirstName.Contains(q) ||
                        ps.LastName.Contains(q) ||
                        ps.PrimaryEmailAddress.Contains(q) ||
                        ps.CompanyName.Contains(q) ||
                        ps.MainPhoneNumber.Contains(q));
                }
                if (!string.IsNullOrEmpty(query["o"]) && !string.IsNullOrEmpty(query["d"]))
                {
                    if (query["o"] != "LastName")
                    {
                        if (query["o"] == "FirstName")
                        {
                            if (query["d"] == "desc")
                                propertyStaffList = propertyStaffList.OrderByDescending(ps => ps.FirstName)
                                    .ThenBy(ps => ps.LastName);
                            else
                                propertyStaffList = propertyStaffList.OrderBy(ps => ps.FirstName)
                                    .ThenBy(ps => ps.LastName);
                        }
                    }
                    else
                    {
                        if (query["d"] == "desc")
                            propertyStaffList = propertyStaffList.OrderByDescending(ps => ps.LastName)
                                .ThenBy(ps => ps.FirstName);
                        else
                            propertyStaffList = propertyStaffList.OrderBy(ps => ps.LastName).ThenBy(ps => ps.FirstName);
                    }
                }
                else
                    propertyStaffList = propertyStaffList.OrderBy(ps => ps.LastName).ThenBy(ps => ps.FirstName);
                var count = propertyStaffList.Count();
				if (int.TryParse(query["p"], out var page))
				{
					var skip = (page - 1) * 10;
					propertyStaffList = propertyStaffList.Skip(skip).Take(10);
				}
				else
				{
					propertyStaffList = propertyStaffList.Take(10);
				}
                return new { data = propertyStaffList.ToList(), itemCount = count };
			}
		}

        [HttpPost]
        [ActionName("addpropertystaff")]
        public string AddStaff(PropertyStaff rpoPropertyStaff)
        {
            var efxPropertyStaff = GetEfxPropertyStaffObject(rpoPropertyStaff);
            var currentUserType = EfxFramework.UserType.EfxAdministrator;
            try
			{
                //hash the password
				if (!string.IsNullOrEmpty(rpoPropertyStaff.Password))
					efxPropertyStaff.SetPassword(rpoPropertyStaff.Password);

                if (!Helper.IsRpoAdmin)
					currentUserType = EfxFramework.UserType.PropertyStaff;

                var testStaffId = new EfxFramework.PropertyStaff(rpoPropertyStaff.PrimaryEmailAddress);
                if (testStaffId.PropertyStaffId.HasValue && testStaffId.PropertyStaffId.Value != 0)
                {
                    return "That email address is already in use. Please try another address.";
                }

                var adminDupTest = new EfxFramework.EfxAdministrator(rpoPropertyStaff.PrimaryEmailAddress);
                if (adminDupTest.EfxAdministratorId != 0)
                {
                    return "Email Address already used (by RPO Administrator) - please try again.";
                }

				var staffId = EfxFramework.PropertyStaff.Set(efxPropertyStaff, UIFacade.GetAuthenticatedUserString(currentUserType));
                using (var entity = new RPOEntities())
                {
                    var staff = entity.PropertyStaffs.FirstOrDefault(s => s.PropertyStaffId == staffId);
                    if (staff != null)
                    {
                        staff.CompanyId = rpoPropertyStaff.CompanyId;
                        if (!Helper.IsRpoAdmin)
                        {
                            var currentUser = entity.PropertyStaffs.First(sp =>
                                sp.PropertyStaffId == RPO.Helpers.Helper.CurrentUserId);
                            staff.CompanyId = currentUser.CompanyId;
                        }
                    }

                    entity.SaveChanges();
                }
                //assign PMs to properties
                rpoPropertyStaff.PropertyIds.ForEach(p => EfxFramework.Property.AssignStaffToProperty(staffId, p, rpoPropertyStaff.IsMainContact));

				//add to propertystaffrole
				AddStaffToRole(staffId, rpoPropertyStaff.RoleId);
				return staffId > 0 ? "success" : "Staff Not Created";
			}
			catch (SqlException ex)
            {
                return ex.Number == 2601 ? "That email address is already in use." : ex.Message;
            }
			catch (Exception ex)
			{
				return ex.Message;
			}
        }
        [HttpGet]
        [ActionName("getstaff")]
        public PropertyStaff GetStaff(int id)
        {
            using (var entity = new RPOEntities())
            {
                var staff = entity.PropertyStaffs.FirstOrDefault(s => s.PropertyStaffId == id);
                if (staff == null) return null;

                var staffproperties = entity.PropertyPropertyStaffs.Where(sp => sp.PropertyStaffId == id).ToList();
                staff.PropertyIds = staffproperties.Select(p => p.PropertyId).ToList();
                
                //check if user is corp admin. if so, check if they can manage this user's account
                var roleListCorporateAdmin = new List<EfxFramework.RoleName> { EfxFramework.RoleName.CorporateAdmin };
                if (!Helper.IsRpoAdmin)
                {
                    var currentUser = entity.PropertyStaffs.First(sp => sp.PropertyStaffId == RPO.Helpers.Helper.CurrentUserId);
                    var canEditStaff =
                        (EfxFramework.RoleManager.IsUserInRole(EfxFramework.RoleType.PropertyStaff,
                             RPO.Helpers.Helper.CurrentUserId, roleListCorporateAdmin) &&
                         currentUser.CompanyId == staff.CompanyId) || Helper.IsRpoAdmin;
                    
                    if (!canEditStaff) return null; 
                }

                var isCorpAdmin = EfxFramework.RoleManager.IsUserInRole(EfxFramework.RoleType.PropertyStaff, staff.PropertyStaffId, roleListCorporateAdmin);
                if (!isCorpAdmin)
                {
                    staff.IsMainContact = staffproperties.Count > 0 ? staffproperties.First().IsMainContact : false;

                }
                var staffRole = EfxFramework.Role.GetRolesByStaffId(staff.PropertyStaffId).FirstOrDefault();
                if (staffRole != null)
                {
                    staff.RoleId = staffRole.RoleId;
                }
                return staff;
            }
        }

        [HttpPost]
        [ActionName("updatepropertystaff")]
        public bool UpdateStaff(PropertyStaff rpoPropertyStaff)
        {
            using (var entity = new RPOEntities())
            {
                var staff = entity.PropertyStaffs.FirstOrDefault(s => s.PropertyStaffId == rpoPropertyStaff.PropertyStaffId);
                if (staff == null) return false;

                var roleListCorporateAdmin = new List<EfxFramework.RoleName> { EfxFramework.RoleName.CorporateAdmin };
                if (!Helper.IsRpoAdmin)
                {
                    var currentUser = entity.PropertyStaffs.First(sp => sp.PropertyStaffId == RPO.Helpers.Helper.CurrentUserId);
                    var canEditStaff =
                        (EfxFramework.RoleManager.IsUserInRole(EfxFramework.RoleType.PropertyStaff,
                             RPO.Helpers.Helper.CurrentUserId, roleListCorporateAdmin) &&
                         currentUser.CompanyId == staff.CompanyId) || Helper.IsRpoAdmin;
                    
                    if (!canEditStaff) return false; 
                }

                var role = EfxFramework.Role.GetRolesByStaffId(rpoPropertyStaff.PropertyStaffId).FirstOrDefault();
                staff.FirstName = rpoPropertyStaff.FirstName;
                staff.MiddleName = rpoPropertyStaff.MiddleName;
                staff.LastName = rpoPropertyStaff.LastName;

                if (role == null || role.RoleId != rpoPropertyStaff.RoleId)
                {
                    AddStaffToRole(rpoPropertyStaff.PropertyStaffId, rpoPropertyStaff.RoleId);
                }
                var staffProperties = entity.PropertyPropertyStaffs.Where(sp => sp.PropertyStaffId == rpoPropertyStaff.PropertyStaffId).ToList();
                staffProperties.ForEach(sp => sp.IsMainContact = rpoPropertyStaff.IsMainContact);
                staff.IsPropertyGeneralManager = rpoPropertyStaff.IsPropertyGeneralManager;
                staff.MainPhoneNumber = rpoPropertyStaff.MainPhoneNumber;
                staff.MobilePhoneNumber = rpoPropertyStaff.MobilePhoneNumber;
                staff.StreetAddress = rpoPropertyStaff.StreetAddress;
                staff.StreetAddress2 = rpoPropertyStaff.StreetAddress2;
                staff.City = rpoPropertyStaff.City;
                staff.StateProvinceId = rpoPropertyStaff.StateProvinceId;
                staff.PostalCode = rpoPropertyStaff.PostalCode;
                staff.CompanyId = rpoPropertyStaff.CompanyId;

                //add new properties
                rpoPropertyStaff.PropertyIds.ForEach(p =>
                {
                    if (staffProperties.Any(sp => sp.PropertyId == p)) return;

                    var pps = new PropertyPropertyStaff
                    {
                        PropertyId = p,
                        PropertyStaffId = rpoPropertyStaff.PropertyStaffId,
                        IsMainContact = rpoPropertyStaff.IsMainContact
                    };
                    entity.PropertyPropertyStaffs.Add(pps);
                });
                //delete ones that this staff can no longer access
                staffProperties.ForEach(sp =>
                {
                    if (rpoPropertyStaff.PropertyIds.All(p => p != sp.PropertyId))
                    {
                        entity.PropertyPropertyStaffs.Remove(sp);
                    }
                });
                if (!string.IsNullOrEmpty(rpoPropertyStaff.Password))
                {
                    staff.PasswordHash = EfxFramework.Security.Authentication.AuthenticationTools.HashPassword(rpoPropertyStaff.Password, staff.Salt);
                }
                entity.SaveChanges();
            }
            return true;
        }

        [HttpPost]
        [ActionName("deletestaff")]
        public int DeletePropertyStaff(PropertyStaff rpoPropertyStaff)
        {
            var al = new ActivityLog();
            var userId = string.Empty;
            try
            {
                userId = Helper.IsRpoAdmin
                    ? UIFacade.GetAuthenticatedUserString(EfxFramework.UserType.EfxAdministrator)
                    : UIFacade.GetAuthenticatedUserString(EfxFramework.UserType.PropertyStaff);
                if (userId != null)
                {
                    EfxFramework.PropertyStaff.DeletePropertyStaffByPropertyStaffId(rpoPropertyStaff.PropertyStaffId, userId);
                    return 1;
                }

                al.WriteLog(UserName: null, "An error occured when trying to delete Property Staff ID: " + rpoPropertyStaff.PropertyStaffId + ".", 2);
                return 2;
            }
            catch (Exception ex)
            {
                al.WriteLog(userId,
                    "An error occured when trying to delete Property Staff ID: " + rpoPropertyStaff.PropertyStaffId +
                    ".  Error Message: " + ex.Message, 2);
                return 3;
            }
        }
		[HttpGet]
		[ActionName("getstatelist")]
		public List<StateProvince> GetStateList()
		{
			using (var entity = new RPOEntities())
			{
				return entity.StateProvinces.ToList();
			}
		}

        [HttpPost]
        [ActionName("addstafftocompanyproperties")]
        public bool AddStaffToCompanyProperties(PropertyStaff propertyStaff)
        {
            return false;
        }
		private EfxFramework.PropertyStaff GetEfxPropertyStaffObject(PropertyStaff rpoPropertyStaff)
		{
            var efxPropertyStaff = new EfxFramework.PropertyStaff
            {
                AlternateEmailAddress1 = rpoPropertyStaff.AlternateEmailAddress1,
                AlternateEmailAddress2 = rpoPropertyStaff.AlternateEmailAddress2,
                BirthDate = rpoPropertyStaff.BirthDate,
                CanAddProperty = rpoPropertyStaff.CanAddProperty,
                CanChangeLeaseInfo = rpoPropertyStaff.CanChangeLeaseInfo,
                CanChangePictures = rpoPropertyStaff.CanChangePictures,
                CanCreateNewRenterAccounts = rpoPropertyStaff.CanCreateNewRenterAccounts,
                CanCreateNewSalesStaffAccounts = rpoPropertyStaff.CanCreateNewSalesStaffAccounts,
                CanSendBulkMail = rpoPropertyStaff.CanSendBulkMail,
                CanTakePaymentsForEfx = rpoPropertyStaff.CanTakePaymentsForEfx,
                CanTakePaymentsForProperties = rpoPropertyStaff.CanTakePaymentsForProperties,
                CanViewPropertyDetail = rpoPropertyStaff.CanViewPropertyDetail,
                CanViewRenterDetails = rpoPropertyStaff.CanViewRenterDetails,
                CanViewReports = rpoPropertyStaff.CanViewReports,
                CanViewSalesStaffAccounts = rpoPropertyStaff.CanViewSalesStaffAccounts,
                City = rpoPropertyStaff.City,
                CommissionRate = rpoPropertyStaff.CommissionRate,
                FaxNumber = rpoPropertyStaff.FaxNumber,
                FirstName = rpoPropertyStaff.FirstName,
                IsPropertyGeneralManager = rpoPropertyStaff.IsPropertyGeneralManager,
                LastName = rpoPropertyStaff.LastName,
                MainPhoneNumber = rpoPropertyStaff.MainPhoneNumber,
                MiddleName = rpoPropertyStaff.MiddleName,
                MobilePhoneNumber = rpoPropertyStaff.MobilePhoneNumber,
                PostalCode = rpoPropertyStaff.PostalCode,
                PrimaryEmailAddress = rpoPropertyStaff.PrimaryEmailAddress,
                PropertyStaffId = rpoPropertyStaff.PropertyStaffId,
                Salt = rpoPropertyStaff.Salt,
                StateProvinceId = rpoPropertyStaff.StateProvinceId,
                StreetAddress = rpoPropertyStaff.StreetAddress,
                StreetAddress2 = rpoPropertyStaff.StreetAddress2
            };
            return efxPropertyStaff;
		}

        private static void AddStaffToRole(int propertyStaffId, int roleId)
		{
			//Delete any existing roles to add new ones.
			var roles = EfxFramework.Role.GetRolesByStaffId(propertyStaffId);

			foreach (var role in roles)
			{
				EfxFramework.Role.DeletePropertyStaffRole(propertyStaffId, role.RoleId);
			}

			var propertyStaffRole = new EfxFramework.PropertyStaffRole
			{
				PropertyStaffId = propertyStaffId,
				RoleId = roleId
			};

			EfxFramework.PropertyStaffRole.Set(propertyStaffRole);
		}
    }
}