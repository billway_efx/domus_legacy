﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using RPO;
//using WebAPI.OutputCache.Core;
using WebApi.OutputCache.Core;

namespace Domus.API
{
    public class CompanyController : ApiController
    {
        
        // GET api/<controller>
        [HttpGet]
        [ActionName("all")]
        public List<Company> Get()
        {
            using (var entity = new RPOEntities())
            {
                int page = 1;
                var query = Request.RequestUri.ParseQueryString();
                var companies = entity.Companies.Where(c => !c.IsDeleted).OrderBy(c => c.CompanyName).ToList();
                if (EfxFramework.Helpers.Helper.IsCorporateAdmin)
                {
                    var corpAdminCompanyId = entity.PropertyStaffs.First(s => s.PropertyStaffId == EfxFramework.Helpers.Helper.CurrentUserId).CompanyId;
                    companies = companies.Where(c => c.CompanyId == corpAdminCompanyId.Value).ToList();
                }
				foreach (var company in companies)
				{
					if (company.StateProvinceId.HasValue)
					{
						EfxFramework.StateProvince currentState = (EfxFramework.StateProvince)company.StateProvinceId.Value;
						string state = currentState.ToString();
						company.State = state;
					}
				}
                if (int.TryParse(query["p"], out page))
                {
                    int skip = (page - 1)*10;
                    companies = companies.Skip(skip).Take(10).ToList();
                }
                return companies;
            }
        }

        // GET api/<controller>/5
        [HttpGet]
        [ActionName("one")]
        public Company Get(int id)
        {
            using (var entity = new RPOEntities())
            {
                return entity.Companies.First(c => c.CompanyId == id && !c.IsDeleted);
            }
        }

        
        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

		[HttpGet]
		[ActionName("delete")]
        public bool DeleteCompany(int id)
        {
			using (var entity = new RPOEntities())
			{
				if (EfxFramework.Helpers.Helper.IsRpoAdmin)
				{
					var company = entity.Companies.First(c => c.CompanyId == id);
					company.IsDeleted = true;
					entity.SaveChanges();
					return true;
				}
				return false;
			}
        }
    }
}