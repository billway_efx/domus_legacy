﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using RPO;
using EfxFramework;
using EfxFramework.Helpers;
using System.Configuration;
using System.Net.Http;
using System.Data.SqlClient;
//using WebAPI.OutputCache;
using WebApi.OutputCache.Core;
using EfxFramework.Web.Facade;

namespace Domus.API
{
    public class UserController : ApiController
    {
        // GET api/<controller>/5
        [HttpGet]
        [ActionName("id")]
        public int GetCurrentUserId()
        {
            return RPO.Helpers.Helper.CurrentUserId;
        }
        [HttpGet]
        [ActionName("isadmin")]
        public bool IsAdmin()
        {
            List<RoleName> roleListAdmin = new List<RoleName> { RoleName.EfxPowerAdministrator };
            //bool Test = false;
              //  Test = RoleManager.IsUserInRole(EfxFramework.RoleType.EfxAdministrator, GetCurrentUserId(), roleListAdmin);
            return RoleManager.IsUserInRole(EfxFramework.RoleType.EfxAdministrator, GetCurrentUserId(), roleListAdmin);
        }
		[HttpGet]
		[ActionName("iscorporateadmin")]
		public bool IsCorporateAdmin()
		{
			List<RoleName> roleListCorporateAdmin = new List<RoleName> { RoleName.CorporateAdmin };
            //bool Test = RoleManager.IsUserInRole(EfxFramework.RoleType.PropertyStaff, GetCurrentUserId(), roleListCorporateAdmin);
			return RoleManager.IsUserInRole(EfxFramework.RoleType.PropertyStaff, GetCurrentUserId(), roleListCorporateAdmin);
		}

        [HttpGet]
        [ActionName("iscsr")]
        public bool IsCsrUser()
        {
                List<RoleName> roleListAdmin = new List<RoleName> { RoleName.EfxCustomerService };
               // bool test = false; 
                 //   test = RoleManager.IsUserInRole(EfxFramework.RoleType.CustomerServiceStaff, GetCurrentUserId(), roleListAdmin);
                return RoleManager.IsUserInRole(EfxFramework.RoleType.CustomerServiceStaff, GetCurrentUserId(), roleListAdmin);
        }

		[HttpGet]
		[ActionName("allapi")]
		public object Get()
		{
			if (!Helper.IsRpoAdmin)
				return new object();

			using (var entity = new RPOEntities())
			{
				int page = 1;
				var query = Request.RequestUri.ParseQueryString();
				var apiusers = entity.ApiUsers.Include("PartnerCompany").AsQueryable();
				var apiUserList = 
                    (from api in apiusers 
                     select new 
                     { 
                         api.ApiUserId, 
                         api.Username, 
                         api.PartnerCompany.PartnerCompanyName,
                         api.IsActive
                     });
                if (!string.IsNullOrEmpty(query["q"]))
                {
                    var q = query["q"].ToString();
                    apiUserList = apiUserList.Where(u =>
                        u.Username.Contains(q) ||
                        u.PartnerCompanyName.Contains(q));
                }
				apiUserList = apiUserList.OrderBy(api => api.Username);
                int count = apiUserList.Count();
				if (int.TryParse(query["p"], out page))
				{
					int skip = (page - 1) * 10;
					apiUserList = apiUserList.Skip(skip).Take(10);
				}
				else
				{
					apiUserList = apiUserList.Take(10);
				}
                return new { data = apiUserList.ToList(), itemCount = count };
			}
		}

		[HttpGet]
		[ActionName("alladmin")]
		public object GetAllAdmin()
		{
			if (!Helper.IsRpoAdmin)
				return new object();

			using (var entity = new RPOEntities())
			{
				int page = 1;
				var query = Request.RequestUri.ParseQueryString();
				var admins = entity.EfxAdministrators.AsQueryable();
				var adminsList = 
                    (from a in admins 
                     select new 
                     { 
                         a.EfxAdministratorId, 
                         a.FirstName, 
                         a.LastName, 
                         a.PrimaryEmailAddress 
                     });
                if (!string.IsNullOrEmpty(query["q"]))
                {
                    var q = query["q"].ToString();
                    adminsList = adminsList.Where(a =>
                        a.FirstName.Contains(q) ||
                        a.LastName.Contains(q) ||
                        a.PrimaryEmailAddress.Contains(q));
                }
                if (!string.IsNullOrEmpty(query["o"]) && !string.IsNullOrEmpty(query["d"]))
                {
                    if (query["o"].ToString() == "LastName")
                    {
                        if (query["d"].ToString() == "desc")
                            adminsList = adminsList.OrderByDescending(a => a.LastName).ThenBy(a => a.FirstName);
                        else
                            adminsList = adminsList.OrderBy(a => a.LastName).ThenBy(a => a.FirstName);
                    }
                    else if (query["o"].ToString() == "FirstName")
                    {
                        if (query["d"].ToString() == "desc")
                            adminsList = adminsList.OrderByDescending(a => a.FirstName).ThenBy(a => a.LastName);
                        else
                            adminsList = adminsList.OrderBy(a => a.FirstName).ThenBy(a => a.LastName);
                    }
                }
                else
                    adminsList = adminsList.OrderBy(a => a.LastName).ThenBy(a => a.FirstName);
                int count = adminsList.Count();
				if (int.TryParse(query["p"], out page))
				{
					int skip = (page - 1) * 10;
					adminsList = adminsList.Skip(skip).Take(10);
				}
				else
				{
					adminsList = adminsList.Take(10);
				}
                return new { data = adminsList.ToList(), itemCount = count };
			}
		}

        [HttpPost]
        [ActionName("deleteadmin")]
        public bool DeleteAdmin(RPO.EfxAdministrator rpoAdmin)
        {
            try
            {
                EfxFramework.EfxAdministrator.DeleteEfxAdministratorByEfxAdministratorId(rpoAdmin.EfxAdministratorId, EfxFramework.Helpers.Helper.IsRpoAdmin ? UIFacade.GetAuthenticatedUserString(EfxFramework.UserType.EfxAdministrator) : UIFacade.GetAuthenticatedUserString(EfxFramework.UserType.PropertyStaff));
                return true;
            }
            //catch (Exception e)
            catch
            {
                return false;
            }
            //return false;
        }
        [HttpPost]
        [ActionName("deleteapiuser")]
        public bool DeleteApiUser(RPO.ApiUser apiUser)
        {
            using (var entity = new RPOEntities())
            {
                var user = entity.ApiUsers.FirstOrDefault(u => u.ApiUserId == apiUser.ApiUserId);
                if (user == null)
                    return true;
                entity.ApiUsers.Remove(user);
                entity.SaveChanges();
                return true;
            }
        }
        [HttpPost]
        [ActionName("toggleapiuser")]
        public bool ToggleApiUser(RPO.ApiUser apiUser)
        {
            using (var entity = new RPOEntities())
            {
                var user = entity.ApiUsers.FirstOrDefault(u => u.ApiUserId == apiUser.ApiUserId);
                if (user == null)
                    return false;
                user.IsActive = !user.IsActive;
                entity.SaveChanges();
                return true;
            }
        }
    }
}