﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Domus.Email.Default" %>
<%@ Register Src="~/ReportingPages/UserControls/PropertyPicker.ascx" TagPrefix="uc1" TagName="PropertyPicker" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopHeadline" runat="server">
    <h1>Email Manager</h1>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script> $(function () { EFXClient.SetSelectedNavigation('email-link'); }); </script>

    <div class="main-container">
        <div class="section-body">
            <asp:ValidationSummary ID="ApplicationSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="Email" />
            <div class="alert alert-success" runat="server" visible="false" id="successDiv">
                <b><span>Email was sent successfully to </span>
                    <span id="emailCount"><%= EmailsSentCount %></span>
                    <span> residents</span>
                </b></div>
            <!-- Begin Email Selecters -->
            <div id="EmailMarketing">
                <div class="formTint">
                <div class="formWrapper">
                    <h2>Email Marketing</h2>

                    <!--CMallory - Task 0026 - Added Company/Property Picker to front-end -->
                    <div class="formThird">
                        <uc1:PropertyPicker runat="server" ID="PropertyPicker" />
                        <asp:HiddenField ID="HiddenPropertyList" runat="server" />
                        <%--<label style="margin-top:41px;">Select Property <span style="color: red; ">*</span></label>
                        <asp:TextBox ID="PropertiesTextbox" runat="server" Width="100%" ClientIDMode="Static" CssClass="form-control"  ></asp:TextBox>--%>
                      
                    </div>
                    <div class="formTwoThirds">
                        <div class="well">
                            <div class="formHalf">
                                <label>Resident Group</label>
                                <asp:DropDownList ID="RenterGroupDropdownList" ClientIDMode="Static" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="-- Select Resident Group --" Value="-1"></asp:ListItem>
                                    <%--<asp:ListItem Text="All Delinquent Residents" Value="1"></asp:ListItem>--%>
                                    <asp:ListItem Text="All Residents" Value="2" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="formHalf">
                                <label>Select Marketing Attachment</label>
                                <asp:DropDownList ID="EmailImageDropdownList" ClientIDMode="Static" runat="server" AutoPostBack="True" OnSelectedIndexChanged="EmailImageDropdownList_SelectedIndexChanged" CssClass="form-control">
                                    <asp:ListItem Text="-- Select Attachment --" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <div id="HideDownload" runat="server" visible="false" style="margin-top:12px;margin-left: 2px;">
                                    <asp:Button ID="DownloadFile" runat="server" Text="Preview Marketing Attachment"  ToolTip="Download and preview selected marketing attachment file" 
                                        OnClick="DownloadFile_Click" class="btn btn-primary" />
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
                </div>
            </div>
            <!-- End Email Selecters -->

            <!-- Begin Compose Email -->
            <div class="formWrapper">
                <asp:Panel ID="SendEmailPanel" runat="server">

                    <div class="formWhole">
                        <h2>Compose Email</h2>
                    </div>

                    <div class="formHalf">
                        <label>From <span style="color: red; ">*</span>:</label>
                        <asp:TextBox ID="FromTextbox" runat="server" CssClass="form-control"  ></asp:TextBox>
                    </div>
                    <!-- joselist-4b - Patrick Whittingham - 7/17/2015 - Add BCC email address   -->
                    <div class="formHalf">
                        <label>BCC:</label>
                        <asp:TextBox ID="BCCTextBox" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="formHalf">
                        <label>Subject <span style="color: red; ">*</span>:</label>
                        <asp:TextBox ID="SubjectTextbox" runat="server" CssClass="form-control"  ></asp:TextBox>
                    </div>
                    <div class="formWhole">
                        <label>Content:</label>
                        <CKEditor:CKEditorControl ID="CKEditor1" BasePath="~/ckeditor" runat="server" ClientIDMode="Static"></CKEditor:CKEditorControl>
                    </div>
                </asp:Panel>
            </div>

            <div class="button-footer">
                <div class="main-button-action">
                    <asp:Button ID="PreviewButton" Style="margin-right: 5px;" runat="server" CssClass="btn btn-default" Text="Preview" OnClientClick="return EFXClient.ShowEditorHTMLPreview();" />
                    <asp:Button ID="SaveButton" Visible="false" OnClick="SaveButton_Click" runat="server" CssClass="btn btn-primary" Text="Save" /> 
                    <asp:Button ID="SendButton" OnClick="SendButton_Click" runat="server" CssClass="btn btn-primary" Text="Send" Style="margin-right: 5px;" OnClientClick="return _field_val()  " />
                </div>
                <div class="clear"></div>
            </div>

            <!-- End Compose Email -->


            <asp:Panel ID="EmailSentPanel" runat="server" Visible="false">
                <h2>Your Email Has Been Sent!</h2>
                <p class="padding-top">
                    <label>To:</label><br />
                    <asp:Label ID="EmailToLabel" runat="server"></asp:Label>
                </p>
                <p class="padding-top">
                    <label>Subject:</label><br />
                    <asp:Label ID="EmailSubjectLabel" runat="server"></asp:Label>
                </p>
                <p class="padding-top">
                    <label>Sent By:</label><br />
                    <asp:Label ID="EmailSentByLabel" runat="server"></asp:Label>
                </p>
            </asp:Panel>
        </div>
    </div>


    <%--<script src="http://code.jquery.com/jquery-1.9.1.js"></script>--%>
    <script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="/Scripts/efx.js" type="text/javascript"></script>
    <script>

            // field validation
            function _field_val() {

                var error = false, errorText = "";
                //if ($("#PropertiesTextbox").val().length == 0 ) {
                //    $("#PropertiesTextbox").css('border', '1px solid red');
                //    error = true;
                //    errorText += "Please enter at least one Property. <br />";
                //}
                if ($("#MainContent_FromTextbox").val().length == 0 ) {
                    $("#MainContent_FromTextbox").css('border', '1px solid red');
                    error = true;
                    errorText += "Please enter a From email address. <br />";
                }
                if ($("#MainContent_SubjectTextbox").val().length == 0) {
                    $("#MainContent_SubjectTextbox").css('border', '1px solid red');
                    error = true;
                    errorText += "Please enter a Subject. <br />";
                }

                if (error == false) {
                    $("#MainContent_ApplicationSummary").css("display", "none");
                    confirm('Send email now?');
                    return true;
                } else {
                    $("#MainContent_ApplicationSummary").css("display", "block");
                   $("#MainContent_ApplicationSummary").html(errorText);
                    return false;
                }

            } // end of field validation....


        $(function () {


            //
            function split(val) {
                return val.split(/,\s*/);
            }
            function extractLast(term) {
                return split(term).pop();
            }

            $("#PropertiesTextbox")
            .bind("keydown", function (event) {
                if (event.keyCode === $.ui.keyCode.TAB &&
                        $(this).data("autocomplete").menu.active) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                minLength: 0,
                source: function (request, response) {
                    $.getJSON("/WebServices/TypeAheadService.svc/GetPropertiesBySearchString/" + extractLast(request.term), {
                        term: extractLast(request.term)
                    }, response);
                },
                focus: function () {
                    // prevent value inserted on focus
                    return false;
                },
                select: function (event, ui) {
                    var terms = split(this.value);
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push(ui.item.value);
                    // add placeholder to get the comma-and-space at the end
                    terms.push("");
                    this.value = terms.join(", ");
                    return false;
                }
            });
        });
    </script>
</asp:Content>
