﻿using System.Web;
using EfxFramework;
using EfxFramework.BulkMail;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.UI.WebControls;
using System.IO;
using System.Collections;

namespace Domus.Email
{
    public partial class Default : BasePage
    {
        //Collier Mallory - Task 00026
        //Added GetPropertyString function to get the property names and IDs for the ones with their checkbox checked.
        public string GetPropertyString()
        {

            CheckBoxList ckbx = (CheckBoxList)this.PropertyPicker.FindControl("CheckBoxList1");
            List<String> PropertyID_list = new List<string>();
            List<String> PropertyName_list = new List<string>();

            foreach (ListItem item in ckbx.Items)
            {
                if (item.Selected)
                {
                    PropertyID_list.Add(item.Value);
                    PropertyName_list.Add(item.Text);
                }
            }
            string PropList = String.Join(",", PropertyName_list.ToArray());
            return PropList;
        }

        public int? EmailsSentCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            successDiv.Visible = false;
            var allProps = Property.GetAllPropertyList();
            if (!IsPostBack)
            {
                //Salcedo - 6/13/2014 - # 000140 - populate control from directory instead of using static list of files
                PopulateDropDown();
                CKEditor1.config.filebrowserImageUploadUrl = "/Handlers/EFXAdminHandler.ashx?requestType=1";
            }

        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            //PageMessage.DisplayMessage(SystemMessageType.Success, "Successfully saved email.");
			successDiv.Visible = true;
        }

        //Salcedo - 6/13/2014 - # 000140 - populate control from directory instead of using static list of files
        private void PopulateDropDown()
        {
            String FilePath = EfxSettings.MarketingImageFolderLocation;

            String[] fileEntries = Directory.GetFiles(FilePath);

            EmailImageDropdownList.Items.Clear();
            ListItem NewItem = new ListItem("-- Select Attachment --", "-1");
            EmailImageDropdownList.Items.Add(NewItem);

            //Add all the file names in the directory to the dropdown control
            foreach (String FileName in fileEntries)
            {
                //Remove the extension, and replace all hyphens and underscores with a blank space to make the file names more user-friendly
                String DisplayName = Path.GetFileNameWithoutExtension(FileName).Replace("-"," ").Replace("_"," ");
                NewItem = new ListItem(DisplayName,FileName);
                EmailImageDropdownList.Items.Add(NewItem);
            }
        }

        protected void SendButton_Click(object sender, EventArgs e)
        {
            //Salcedo = task # 413
            EmailsSentCount = 0;

            if (RenterGroupDropdownList.SelectedValue == "-1")
            {
				CustomValidator err = new CustomValidator();
				err.ValidationGroup = "Email";
				err.IsValid = false;
				err.ErrorMessage = "Please select a Resident group.";
				Page.Validators.Add(err);
                //PageMessage.DisplayMessage(SystemMessageType.Error, "Please select a renter group.");
                return;
            }

            // Flag to denote if we are sending to all renters or delinquent
            var sendAll = RenterGroupDropdownList.SelectedValue == "2";

            // Salcedo - 1/15/2014 - we're now pulling the "From"email address from the web page, instead of the logged in user's primaryemailaddress
            String FromEmail = FromTextbox.Text;
            if (FromEmail.Length == 0 || !FromEmail.IsValidEmailAddress())
            {
                CustomValidator err = new CustomValidator();
                err.ValidationGroup = "Email";
                err.IsValid = false;
                err.ErrorMessage = "Please enter a valid From email address.";
                Page.Validators.Add(err);
                return;
            }

            // joselist-4b - Patrick Whittingham - 7/17/2015 - Add new BCC form field
            String BCCEmail = BCCTextBox.Text;
            if (BCCEmail.Length > 0 && !BCCEmail.IsValidEmailAddress())
            {
                CustomValidator err = new CustomValidator();
                err.ValidationGroup = "Email";
                err.IsValid = false;
                err.ErrorMessage = "Please enter a valid BCC email address.";
                Page.Validators.Add(err);
                return;
            }

            //.Visible = false;
            //EmailSentPanel.Visible = true;

            //Collier Mallory - Task 00026
            //Changed logic to get property names from the GetPropertyString method instead of a text box.
            EmailToLabel.Text = GetPropertyString();
            EmailSubjectLabel.Text = SubjectTextbox.Text;
            EmailSentByLabel.Text = Facade.CurrentStaff.PrimaryEmailAddress;

            // Build recipients list
            var recipients = new List<MailAddress>();

            // First get properties
            var propsArray = EmailToLabel.Text.Split(',');
            var allProps = Property.GetAllPropertyList();
            var propsToAdd = new List<Property>();
            foreach (var prop in propsArray)
            {
                var toAdd = allProps.FirstOrDefault(p => p.PropertyName.ToLower().Equals(prop.ToLower()));
                if (toAdd != null)
                    propsToAdd.Add(toAdd);
            }

            if (propsToAdd.Count() == 0)
            {
                CustomValidator err = new CustomValidator();
                err.ValidationGroup = "Email";
                err.IsValid = false;
                err.ErrorMessage = "No properties have been selected.";
                Page.Validators.Add(err);
            }
			else
			{
				bool pastDueRentersExist = true;
				// Now renters
				foreach (var property in propsToAdd)
				{
					var renters = Renter.GetRenterListByPropertyId(property.PropertyId.Value);

					foreach (var renter in renters)
					{
						if (sendAll) // Jump out
						{
                            // joselist-4b - Patrick Whittingham - 7/17/2015 - show bad email addresses
                            try
                            {
							recipients.Add(new MailAddress(renter.PrimaryEmailAddress));
                            // Patrick Whittingham - 8/22/15 - task #413 : add email counter
                            EmailsSentCount++;
                            }
                            catch (FormatException ex)
                            {
                                CustomValidator err = new CustomValidator();
                                err.ValidationGroup = "Email";
                                err.IsValid = false;
                                //err.ErrorMessage = pastDueRentersExist ? "This is a bad email address format: " + renter.PrimaryEmailAddress : "No recipients have been added.";
                                // Patrick Whittingham - 8/22/15 - task #413 : remove error message
                                err.ErrorMessage = pastDueRentersExist ? "Resident '" + renter.FirstName + " " + renter.LastName + "' skipped due to invalid email address: '" + renter.PrimaryEmailAddress + "' " : "No recipients have been added.";
                                Page.Validators.Add(err);
                            }
							continue;
						}

						// Get past due information
						var pastDueData = Renter.GetRenterPastDueByRenterId(renter.RenterId);
                        // joselist-4b - Patrick Whittingham - 7/17/2015 - send to all residents and not just past due residents
						//if (pastDueData.Rows.Count > 0)
						//{
							var amount = pastDueData.Rows[0][4].ToString().ToNullDecimal(); // Past Due Amount
							//if (amount.HasValue && amount.Value > 0)
							//{
								// Add past due renter
                            // joselist-4b - Patrick Whittingham - 7/17/2015 - show bad email addresses
                            try
                            {
								recipients.Add(new MailAddress(renter.PrimaryEmailAddress));
                                // Patrick Whittingham - 8/22/15 - task #413 : add email counter
                                EmailsSentCount++;
                            }
                            catch (FormatException ex)
                            {
                                CustomValidator err = new CustomValidator();
                                err.ValidationGroup = "Email";
                                err.IsValid = false;
                                //err.ErrorMessage = pastDueRentersExist ? "This is a bad email address format: " + renter.PrimaryEmailAddress : "No recipients have been added.";
                                // Patrick Whittingham - 8/20/15 - task #413 : remove error message
                                err.ErrorMessage = pastDueRentersExist ? "Resident '" + renter.FirstName + " " + renter.LastName + "' skipped due to invalid email address: '" + renter.PrimaryEmailAddress + "' " : "No recipients have been added.";
                                Page.Validators.Add(err);
                            }

							//}
						//}
						//else
							//pastDueRentersExist = false;
					}
				}

				//var Administrator = new EfxAdministrator(HttpContext.Current.User.Identity.Name.ToInt32());

				// Send it
                // joselist-4b - Patrick Whittingham - 7/17/2015 - change error message
				if (recipients.Count() == 0)
				{
					CustomValidator err = new CustomValidator();
					err.ValidationGroup = "Email";
					err.IsValid = false;
					err.ErrorMessage = pastDueRentersExist ? "No email sent to recipients" : "No recipients have been added.";
					Page.Validators.Add(err);
				}
				else
				{
                    // joselist-4b - Patrick Whittingham - 7/17/2015 - Add BCC email address
                    if ( BCCEmail.Length > 0 )
                    {
                        recipients.Add(new MailAddress(BCCEmail));                    
                    }

                    //Salcedo - 6/13/2014 - # 000140 - add selected file as an attachment instead of embedding within the text of the email
                    if (EmailImageDropdownList.SelectedItem.Value != "-1")
                    {
                        List<Attachment> AttachmentList = new List<Attachment>();
                        String FullAttachmentPath = EmailImageDropdownList.SelectedItem.Value.ToString();
                        Attachment MyAttachment = new Attachment(FullAttachmentPath);
                        AttachmentList.Add(MyAttachment);

                        BulkMail.SendMultiPropertyMailMessage(propsToAdd.Select(property => property.PropertyId).ToList(), RenterGroupDropdownList.SelectedItem.Text,
                            true, EfxSettings.SendMailUsername, EfxSettings.SendMailPassword,
                            new MailAddress(FromEmail), SubjectTextbox.Text, CKEditor1.Text, recipients, AttachmentList);
                    }
                    else
                    {
                        BulkMail.SendMultiPropertyMailMessage(propsToAdd.Select(property => property.PropertyId).ToList(), RenterGroupDropdownList.SelectedItem.Text,
                            true, EfxSettings.SendMailUsername, EfxSettings.SendMailPassword,
                            new MailAddress(FromEmail), SubjectTextbox.Text, CKEditor1.Text, recipients);
                    }

					successDiv.Visible = true;
				}
			}
            //PageMessage.DisplayMessage(SystemMessageType.Success, "Email sent successfully.");
        }


        protected void EmailImageDropdownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Salcedo - 6/13/2014 - # 000140 - add selected file as an attachment instead of embedding within the text of the email
            if (EmailImageDropdownList.SelectedValue != "-1")
            {
                var BaseUrl = String.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, !String.IsNullOrEmpty(Request.ApplicationPath) ? Request.ApplicationPath.TrimEnd('/') : string.Empty);

                //CKEditor1.Text = String.Format("<p>{0}</p><img src={1} alt=There are images attached to this email.  Please make sure that your email settings allow images to be downloaded to view the content. />", 
                //    "There are images attached to this email. Please make sure that your email settings allow images to be downloaded to view the content.",
                //    String.Format("{0}{1}", BaseUrl, EmailImageDropdownList.SelectedValue));
                CKEditor1.Text = "There are one or more images attached to this email. Please make sure that your email settings allow images to be downloaded to view the content.";

                HideDownload.Visible = true;
            }
            else
            {
                HideDownload.Visible = false;
                CKEditor1.Text = "";
            }
        }

        //Salcedo - 6/13/2014 - # 000140 - add ability to preview selected file
        protected void DownloadFile_Click(object sender, EventArgs e)
        {
            if (EmailImageDropdownList.SelectedValue != "-1")
            {
                byte[] bytes = File.ReadAllBytes(EmailImageDropdownList.SelectedItem.Value.ToString());
                String FileName = Path.GetFileName(EmailImageDropdownList.SelectedItem.Value.ToString());
                Response.Buffer = true;
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; filename=" + FileName);
                Response.BinaryWrite(bytes);
                Response.Flush();
            }
        }
    }
}