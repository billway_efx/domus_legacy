﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace RentPaidOnline.WebServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITypeAheadService" in both code and config file together.
    [ServiceContract]
    public interface ITypeAheadService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPropertiesBySearchString/{searchString}", ResponseFormat = WebMessageFormat.Json)]
        List<string> GetPropertiesBySearchString(string searchString);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetAverageConvenienceFeeForProperty/{programId}/{averageRent}", ResponseFormat = WebMessageFormat.Json)]
        decimal GetAverageConvenienceFeeForProperty(string programId, string averageRent);
    }
}
