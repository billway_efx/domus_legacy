﻿using EfxFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using EfxFramework.Services;
using RPO.Helpers;
using System.Web;
using Mb2x.ExtensionMethods;

namespace RentPaidOnline.WebServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TypeAheadService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select TypeAheadService.svc or TypeAheadService.svc.cs at the Solution Explorer and start debugging.
    [DefaultServiceBehavior]
    public class TypeAheadService : ITypeAheadService
    {
        public List<string> GetPropertiesBySearchString(string searchString)
        {
			List<Property> allPropertiesBySearchString = Property.GetPropertyListBySearchString(searchString);
			if (Helper.IsRPOAdmin)
				return allPropertiesBySearchString.Select(p => p.PropertyName).ToList();
			else
			{
				var PropertyStaffId = HttpContext.Current.User.Identity.Name.ToInt32();
				return (from P in allPropertiesBySearchString from Staff in PropertyStaff.GetPropertyStaffListByPropertyId(P.PropertyId.HasValue ? P.PropertyId.Value : 0) where Staff.PropertyStaffId.HasValue && Staff.PropertyStaffId.Value == PropertyStaffId select P.PropertyName).ToList();
			}
        }

        public decimal GetAverageConvenienceFeeForProperty(string programId, string averageRent)
        {
            return Property.GetAverageConvenienceFeeForProperty(programId, averageRent);
        }
    }
}
