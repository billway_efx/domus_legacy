﻿using System;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using System.Web.Optimization;
using System.Web.Routing;
using Domus;
using EfxFramework;
using SqlServerTypes;

namespace RentPaidOnline
{
    public class Global : HttpApplication
	{
        private void Application_Start(object sender, EventArgs e)
		{
            RegisterRoutes(RouteTable.Routes);		
			BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Utilities.LoadNativeAssemblies(Server.MapPath("~/bin"));
        }

        private static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapHttpRoute(
                name: "Default",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new
                {
                    id = RouteParameter.Optional
                });

            GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
            routes.MapPageRoute("reports", "rpo/reports/{*pathInfo}", "~/rpo-reports.aspx");
            routes.MapPageRoute("search", "rpo/search/{*pathInfo}", "~/rpo-search.aspx");
            routes.MapPageRoute("event", "rpo/event/{*pathInfo}", "~/rpo-event.aspx");
            routes.MapPageRoute("events", "rpo/events/{*pathInfo}", "~/rpo-event.aspx");
            routes.MapPageRoute("listing", "rpo/{*pathInfo}", "~/rpo-admin.aspx");
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            if (HttpContext.Current.Server.GetLastError() == null) return;

            var emailList = EfxSettings.ErrorEmailList;
            var myException = HttpContext.Current.Server.GetLastError().GetBaseException();
            
            var message = string.Empty;
            message += "<strong>Message</strong><br />" + myException.Message + "<br />";
            message += "<strong>StackTrace</strong><br />" + myException.StackTrace + "<br />";
            message += "<strong>Query String</strong><br />" + Request.QueryString + "<br />";

            var myMessage =
                new MailMessage("support@rentpaidonline.com", emailList, "RPO Admin Site Error", message)
                {
                    IsBodyHtml = true
                };

            var mySmtpClient = new SmtpClient
            {
                Host = EfxSettings.SmtpHost
            };

            mySmtpClient.Send(myMessage);
        }
	}
}
