﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="rpo-admin.aspx.cs" Inherits="Domus.rpo_admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TopHeadline" runat="server">
<h1 id="H1" ng-controller="TitleController" ng-bind="PageTitle"></h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

        
        <div class="main-container">
            <div ng-view></div>
        </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="Scripts" runat="server">
    <script src="<%= ResolveClientUrl("~/Scripts/rpo-transactions.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/rpo.js") %>"></script>
    
</asp:Content>
