﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="CompanyDetails.aspx.cs" Inherits="Domus.Companies.CompanyDetails" %>

<%@ Import Namespace="EfxFramework.ExtensionMethods" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TopHeadline" runat="server">
    <h1>Company Details</h1>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <div id="CompanyDetails" class="main-container">
        <div class="details-panel">
            <div class="inner" style="position: relative;">
                <a href="/rpo" class="back-to-listings"><span data-icon="&#x5f" aria-hidden="true"></span>Back to Listings</a>
                <div class="details-panel-content">
                    <!-- CMallory - Task 00593 - Added div -->
                    <div runat="server" visible="false" id="DeactivatedHeader">
                        <h1>This company has been deactivated</h1>
                    </div>
                    <div class="col-third">
						<p><asp:Literal ID="CompanyName" runat="server"></asp:Literal></p>
                        <p><asp:Literal ID="Address1" runat="server"></asp:Literal></p>
                        <p><asp:Literal ID="Address2" runat="server"></asp:Literal></p>
                    </div>

                    <div class="col-third">
                        <p>Main Contact: <asp:Literal ID="MainContact" runat="server"></asp:Literal></p>
                        <p>Phone: <asp:Literal ID="Phone" runat="server"></asp:Literal></p>
                    </div>
                </div>

                <div class="details-panel-actions">
                    <%--<a href="#"><span data-icon="&#x24" aria-hidden="true"></span>Edit Account</a>--%>
                    <%--<a href="#"><span data-icon="&#x50" aria-hidden="true" ng-click="toggleActive()"></span>Disable Account</a>--%>
                    <%--<a href="#"><span data-icon="&#x2b" aria-hidden="true"></span></a>--%>
					<asp:LinkButton runat="server" ID="DeleteIcon" OnClientClick="return confirm('Are you sure you want to delete this company?')" OnClick="DeleteIcon_Click"><span data-icon="&#x2b" aria-hidden="true"></span></asp:LinkButton>
                </div>
            </div>
        </div>

        <div id="tabs">
            <ul id="tabLinks" class="nav nav-tabs">
                <li class="active"><a href="#NameTab" data-toggle="tab">
                    Company Details</a></li>
                <li><a href="#PropertiesTab" data-toggle="tab">Properties</a></li>
                <li><a href="#StaffTab" data-toggle="tab">Staff</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="NameTab">
                    <div class="formWrapper">
						<asp:PlaceHolder ID="companySuccess" runat="server" Visible="false"><div class="alert alert-success"><b>Success!</b></div></asp:PlaceHolder>
                        <h2>Property Details</h2>

                        <div class="formWhole">
                            <div class="formHalf">
                                <label>Property Management Company</label>
                                <asp:TextBox ID="PropertyManagementCompanyNameTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="formWhole">
                            <hr />
                        </div>

                        <div class="formWhole">
                            <div class="formThird">
                                <label>Contact First Name</label>
                                <asp:TextBox ID="ContactFirstNameTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="formThird">
                                <label>Contact Last Name</label>
                                <asp:TextBox ID="ContactLastNameTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
						<div class="formWhole">
                            <div class="formThird">
                                <label>PNM Site Id</label>
                                <asp:TextBox ID="PNMSiteId" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="formThird">
                                <label>PNM Secret Key</label>
                                <asp:TextBox ID="PNMSecretKey" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <!--CMallory - Task 00581 - Edited some of the HTML to correct formatting issues. -->
                    <div class="formTint">
                        <div class="formWrapper">
                            <!--CMallory - Task00499 - Added checkbox to set Apply Connect -->
                            <div class="formHalf">
                                <div>
                                <asp:CheckBox ID="chkboxApplyConnect" runat="server" Font-Bold="true" />
                                <asp:Label Text="Apply Connect Enabled" runat="server" Font-Bold="true"></asp:Label>
                                </div>
                            </div>
                            </div>                          
                        <div class="formWrapper" style="margin-top:-40px">                          
                            <h2>Company Address</h2>
                            <div class="formHalf">
                                <label>Address</label>
                                <asp:TextBox ClientIDMode="Static" ID="AddressTextbox" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="formHalf">
                                <label>Address 2</label>
                                <asp:TextBox ClientIDMode="Static" ID="AddressLine2Textbox" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="formTwoThirds">
                                <div class="formHalf">
                                    <label>City</label>
                                    <asp:TextBox ClientIDMode="Static" ID="CityTextbox" runat="server" CssClass="form-control" />
                                </div>

                                <div class="formHalf">
                                    <label>State</label>
                                    <asp:DropDownList ID="StateDropdown" ClientIDMode="Static" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="formThird">
                                <div class="formHalf">
                                    <label>Zip</label>
                                    <asp:TextBox ClientIDMode="Static" ID="ZipTextbox" runat="server" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="formWrapper">
                        <h2>Company Contact Info</h2>

                        <div class="formThird">
                            <label>Phone</label>
                            <asp:TextBox ID="CompanyPhoneTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="formThird">
                            <label>Mobile</label>
                            <asp:TextBox ID="CompanyMobileTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="formThird">
                            <label>Fax</label>
                            <asp:TextBox ID="CompanyFaxTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="formHalf">
                            <label>Email</label>
                            <asp:TextBox ID="CompanyEmailTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    
                    <div class="formTint">
                        <div class="formWrapper">
                            <h2>Company Logo</h2>

                            <div class="formHalf">
                                <div class="well">
                                    <asp:FileUpload ID="CompanyLogoUpload" runat="server" CssClass="btn btn-default" />
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="formWhole">
                                <asp:Image ID="CompanyImage" runat="server" CssClass="orange padding-top padding-bottom" Width="200px" />
                            </div>
                        </div>
                    </div>

                    <div class="formWrapper">
                        <h2>EFX Sales</h2>

                        <div class="formThird">
                            <label>Contact First Name</label>
                            <asp:TextBox ID="CompanyContactFirstNameTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="formThird">
                            <label>Contact Last Name</label>
                            <asp:TextBox ID="CompanyContactLastNameTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="formThird">
                            <label>Contact Phone</label>
                            <asp:TextBox ID="CompanyContactPhoneTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
 
                    <div class="button-footer">
                        <div class="button-action">
                            <%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default" OnClick="btnCancel_Click" />--%>
                        </div>
                        <div class="main-button-action">
                            <asp:Button ID="SaveButtonStep1" runat="server" CssClass="btn btn-primary btn-footer" Text="Save" OnClick="SaveButtonStep1_Click" />
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>

                <!-- Begin Properties Tab -->

                <div class="tab-pane" id="PropertiesTab">
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="PropertiesUpdatePanel">
                        <ProgressTemplate>
                            <div class="loading">
                                <p>Loading, please wait...</p>
                                <img src="/Images/loading2.gif" alt="Loading, please wait..." />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="PropertiesUpdatePanel" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="top-buttons">
                                <a href="/MyProperties/NewProperty.aspx?companyId=<%=CurrentCompany.CompanyId %>" class="btn btn-secondary pull-right"><span data-icon="&#x2c" aria-hidden="true"></span>New Property</a>
                            </div>
							<asp:PlaceHolder ID="successMsg" runat="server" Visible="false"><div class="alert alert-success"><b>Success!</b></div></asp:PlaceHolder>

                            <asp:Repeater ID="PropertiesRepeater" runat="server">
                                <HeaderTemplate>
                                    <div class="listing-group" id="PropertyList">
                                </HeaderTemplate>
                                <FooterTemplate>
                                    </div>
                                    <!--/listing-group-->
                                </FooterTemplate>
                                <ItemTemplate>
                                    <div class="listing-row">
                                        <div class="inner">
                                            <div class="listing-content">
                                                <div class="summary-col">
                                                    <h3><%# Eval("PropertyName") %></h3>
                                                    <p ng-cloak><%# Eval("DisplayAddress") %><br/>
                                                        Number of Units: <strong><%# Eval("NumberOfUnits") %></strong><br/>
                                                        Participating: <strong><%# Eval("IsParticipating") != null && Eval("IsParticipating").ToString() == "true" ? "Yes" : "No"  %></strong></p>
                                                </div>
                                                <div class="details-col">
                                                    <p>Main Contact: <strong><%# Eval("MainContactName") %></strong><br/>
                                                        Phone: <strong><%# Eval("MainPhoneNumber").ToString().FormatPhoneNumber() %></strong></p>
                                                </div>
                                            </div>
                                            <div class="listing-actions">
												<asp:LinkButton OnClientClick="return confirm('Are you sure you want to remove this property?');"  AlternateText="Remove Property" CommandArgument='<%# Eval("PropertyId") %>' CommandName="RemoveProperty" OnCommand="RemovePropertyFromCompany_Command" runat="server" ID="deleteLink" ><span aria-hidden="true" data-icon="&#x2b"></span></asp:LinkButton>
                                            </div>
                                            <!--/listing-actions-->
                                            <a class="btn" href="/MyProperties/PropertyDetails.aspx?propertyId=<%# Eval("PropertyId") %>"><span data-icon="&#x60" aria-hidden="true"></span>Property Details</a>
                                        </div>
                                        <!--/inner-->
                                    </div>
                                    <!--/listing-row-->
                                </ItemTemplate>
                            </asp:Repeater>



                            <asp:Panel ID="NoRecords" runat="server" Visible="false">
                                <div class="empty-records">
                                    <h2>There are no Properties.</h2>
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

                    <div class="tab-pane" id="StaffTab">
                        <div ng-include="'../staff.html'"></div>
                    </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="Scripts">
	<script>
		$(function () {
			/* Mask */
			ApplySingleMask("#<%=ZipTextbox.ClientID%>", "00000");
			ApplySingleMask("#<%=CompanyPhoneTextbox.ClientID%>", "(000) 000-0000");
			ApplySingleMask("#<%=CompanyMobileTextbox.ClientID%>", "(000) 000-0000");
			ApplySingleMask("#<%=CompanyFaxTextbox.ClientID%>", "(000) 000-0000");
			ApplySingleMask("#<%=CompanyContactPhoneTextbox.ClientID%>", "(000) 000-0000");
		});
	</script>
    <script src="<%= ResolveClientUrl("~/Scripts/rpo-staff.js") %>"></script>
</asp:Content>