﻿using EfxFramework;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Domus.Companies
{
    public partial class Default : BasePage
    {
        const string CACHE_SORT_KEY = "CURRENT_PROPERYHOME_SORT";
        const string CACHE_FILTER_KEY = "CURRENT_PROPERYHOME_FILTER";
        const string CACHE_SEARCH_KEY = "CURRENT_PROPERYHOME_SEARCH";

        string _sort = string.Empty;
        string CurrentSort
        {
            get
            {
                if (ViewState[CACHE_SORT_KEY] != null)
                {
                    _sort = ViewState[CACHE_SORT_KEY].ToString();
                }

                return _sort;
            }
            set
            {
                ViewState[CACHE_SORT_KEY] = value;
            }
        }

        string _filter = string.Empty;
        string CurrentFilter
        {
            get
            {
                if (ViewState[CACHE_FILTER_KEY] != null)
                {
                    _filter = ViewState[CACHE_FILTER_KEY].ToString();
                }

                return _filter;
            }
            set
            {
                ViewState[CACHE_FILTER_KEY] = value;
            }
        }

        string _search = string.Empty;
        string CurrentSearch
        {
            get
            {
                if (ViewState[CACHE_SEARCH_KEY] != null)
                {
                    _search = ViewState[CACHE_SEARCH_KEY].ToString();
                }

                return _search;
            }
            set
            {
                ViewState[CACHE_SEARCH_KEY] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var data = Company.GetAllCompanyList();
                CompaniesRepeater.DataSource = data;
                CompaniesRepeater.DataBind();
                CompaniesRepeater.Visible = data.Count > 0;
                EmptyRecordsSection.Visible = data.Count == 0;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (IsPostBack)
            {
                // Get data
                List<Company> data = null;
                if (String.IsNullOrEmpty(CurrentSearch))
                {
                    data  = Company.GetAllCompanyList();
                }
                else
                {
                    data = Company.GetAllCompanyList().Where(c => c.CompanyName.ToLower().IndexOf(CurrentSearch.ToLower()) >= 0).ToList();
                }

                // Filter
                if (!string.IsNullOrEmpty(CurrentFilter))
                {
                    if (CurrentFilter.Equals("Active"))
                    {
                        data = data.Where(d => !d.IsDeleted).ToList();
                    }
                    else if (CurrentFilter.Equals("InActive"))
                    {
                        data = data.Where(d => d.IsDeleted).ToList();
                    }
                }

                // Sort
                if (!string.IsNullOrEmpty(CurrentSort) && CurrentSort.Equals("ASC"))
                {
                    data = data.OrderBy(p => p.CompanyName).ToList();
                }
                else if (!string.IsNullOrEmpty(CurrentSort) && CurrentSort.Equals("DESC"))
                {
                    data = data.OrderByDescending(p => p.CompanyName).ToList();
                }

                CompaniesRepeater.DataSource = data;
                CompaniesRepeater.DataBind();

                CompaniesRepeater.Visible = data.Count > 0;
                EmptyRecordsSection.Visible = data.Count == 0;
            }
        }

        protected void SortByDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SortByDropdown.SelectedValue != "-1")
            {
                CurrentSort = SortByDropdown.SelectedValue;
            }
        }

        protected void FilterByDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (FilterByDropdown.SelectedValue != "-1")
            {
                CurrentFilter = FilterByDropdown.SelectedValue;
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            var data = Company.GetAllCompanyList();
            var term = SearchTextbox.Text.Trim().ToLower();

            CurrentSearch = term;
        }

        protected void ClearButton_Click(object sender, EventArgs e)
        {
            CurrentSearch = string.Empty;
            SearchTextbox.Text = string.Empty;
        }

        protected void DeleteCompany_Command(object sender, CommandEventArgs e)
        {
            var id = e.CommandArgument.ToString().ToNullInt32();
            if (id.HasValue)
            {
                var company = new Company(id.Value);

                if (e.CommandName == "Delete")
                {
                    company.IsDeleted = true;
                }
                else if (e.CommandName == "UnDelete")
                {
                    company.IsDeleted = false;
                }
                Company.Set(company);
            }
        }
    }
}