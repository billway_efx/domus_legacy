﻿using EfxFramework;
using EfxFramework.ExtensionMethods;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Domus.Companies
{
    public partial class CompanyDetails : BasePage
    {
        const string CACHE_KEY = "CURRENT_EDITING_COMPANY";

        Company _company = null;
        public Company CurrentCompany
        {
            get
            {
                if (_company == null)
                {
                    var nullId = Request.QueryString["companyId"].ToNullInt32();
                    var id = nullId.HasValue ? nullId.Value : 0;
                    _company = new Company(id);
                }

                return _company;
            }
        }

        List<Property> _props = null;
        List<Property> CurrentProperties
        {
            get
            {
                if (_props == null)
                {
                    _props = Property.GetPropertyListByCompanyId(CurrentCompany.CompanyId);
                }

                return _props;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
			successMsg.Visible = false;
			companySuccess.Visible = false;
			if (!EfxFramework.Helpers.Helper.IsRpoAdmin)
                Response.Redirect("/");
            // Return home if no company id to view
            if (CurrentCompany == null) Response.Redirect("/");

            if (!IsPostBack)
            {
                StateDropdown.BindToSortedEnum(typeof(StateProvince), true);

                // Bind companies
                var data = Company.GetAllCompanyList().Where(c => c.CompanyId != CurrentCompany.CompanyId).ToArray();
            }

            //CMallory - Task 00593 - Added if statement for the DeactivatedHeader div.
            if (CurrentCompany.IsDeleted==true)
            {
               DeactivatedHeader.Visible = true;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                base.OnPreRender(e);
				StateProvince currentState = (StateProvince)CurrentCompany.StateProvinceId;
				string state = currentState.ToString();
                CompanyName.Text = CurrentCompany.CompanyName;
				Address1.Text = CurrentCompany.Address;
				Address2.Text = CurrentCompany.City + ", " + state + " " + CurrentCompany.PostalCode;
				MainContact.Text = CurrentCompany.ContactFullName;
				Phone.Text = CurrentCompany.Phone;

                // Bind Name Tab
                PropertyManagementCompanyNameTextbox.Text = CurrentCompany.CompanyName;
                ContactFirstNameTextbox.Text = CurrentCompany.ContactFirstName;
                ContactLastNameTextbox.Text = CurrentCompany.ContactLastName;
				PNMSiteId.Text = CurrentCompany.PNMSiteId;
				PNMSecretKey.Text = CurrentCompany.PNMSecretKey;
                AddressTextbox.Text = CurrentCompany.Address;
                AddressLine2Textbox.Text = CurrentCompany.Address2;
                CityTextbox.Text = CurrentCompany.City;
                if (CurrentCompany.StateProvinceId.HasValue)
                {
                    StateDropdown.SelectedValue = ((int)CurrentCompany.StateProvinceId.Value).ToString();
                }
                ZipTextbox.Text = CurrentCompany.PostalCode;
                CompanyPhoneTextbox.Text = CurrentCompany.Phone;
                CompanyMobileTextbox.Text = CurrentCompany.Mobile;
                CompanyFaxTextbox.Text = CurrentCompany.Fax;
                CompanyContactFirstNameTextbox.Text = CurrentCompany.SalesFirstName;
                CompanyContactLastNameTextbox.Text = CurrentCompany.SalesLastName;
                CompanyContactPhoneTextbox.Text = CurrentCompany.SalesPhone;
                CompanyEmailTextbox.Text = CurrentCompany.EmailAddress;
                CompanyImage.ImageUrl = string.Concat("/Handlers/EFXAdminHandler.ashx?RequestType=0&companyId=", CurrentCompany.CompanyId);
                chkboxApplyConnect.Checked = CurrentCompany.UsesApplyConnect;

                // Bind properties tab
                using (var entity = new RPO.RPOEntities())
                {
                    var properties = entity.Properties.Include("StateProvince").Where(p => p.CompanyId == CurrentCompany.CompanyId).OrderBy(p => p.PropertyName).ToList();
                    var propertyIds = properties.Select(p => p.PropertyId).ToList();

                    //Salcedo - 1/20/2014 - broke this out because it was crashing sometimes
                    //var propertyStaffers = entity.PropertyPropertyStaffs.Where(s => propertyIds.Any(p => p == s.PropertyId) && s.IsMainContact).ToList();
                    var propertyStaffers1 = entity.PropertyPropertyStaffs.Where(s => s.IsMainContact).ToList();
                    var propertyStaffers = propertyStaffers1.Where(s => propertyIds.Any(p => p == s.PropertyId)).ToList();

                    var staffIds = propertyStaffers.Select(s => s.PropertyStaffId).ToList();

                    //Salcedo - 1/20/2014 - broke this out because it was crashing sometimes
                    //var staff = entity.PropertyStaffs.Where(s => staffIds.Any(ps => ps == s.PropertyStaffId)).ToList();
                    var staff1 = entity.PropertyStaffs.Where(s => (s.PropertyStaffId != -9999)).ToList();
                    var staff = staff1.Where(s => staffIds.Any(ps => ps == s.PropertyStaffId)).ToList();

                    properties.ForEach(p =>
                        {
                            var mainContact = staff.FirstOrDefault(s => propertyStaffers.Exists(ps => ps.PropertyStaffId == s.PropertyStaffId && ps.PropertyId == p.PropertyId));
                            p.MainContactName = mainContact != null ? (mainContact.FirstName + " " + mainContact.LastName) : "";
                        });
                    PropertiesRepeater.DataSource = properties;
                    PropertiesRepeater.DataBind();
                    PropertiesRepeater.Visible = PropertiesRepeater.Items.Count > 0;
                    NoRecords.Visible = PropertiesRepeater.Items.Count == 0;
                }
                
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }

        protected void DeleteCompany_Command(object sender, CommandEventArgs e)
        {
            try
            {
                var id = e.CommandArgument.ToString().ToNullInt32();
                if (id.HasValue)
                {
                    var company = new Company(id.Value);

                    if (e.CommandName == "Delete")
                    {
                        company.IsDeleted = true;
                    }
                    else if (e.CommandName == "UnDelete")
                    {
                        company.IsDeleted = false;
                    }
                    Company.Set(company);
                }
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }

        protected void SaveButtonStep1_Click(object sender, EventArgs e)
        {
            try
            {
                var company = new Company();
                CurrentCompany.CompanyName = PropertyManagementCompanyNameTextbox.Text;
                CurrentCompany.EmailAddress = CompanyEmailTextbox.Text;
                CurrentCompany.Address = AddressTextbox.Text;
                CurrentCompany.Address2 = AddressLine2Textbox.Text;
                CurrentCompany.City = CityTextbox.Text;
                CurrentCompany.PostalCode = ZipTextbox.Text;
                CurrentCompany.StateProvinceId = (StateProvince)Enum.Parse(typeof(StateProvince), StateDropdown.SelectedValue);
                CurrentCompany.Phone = CompanyPhoneTextbox.Text;
                CurrentCompany.Mobile = CompanyMobileTextbox.Text;
                CurrentCompany.ContactFirstName = ContactFirstNameTextbox.Text;
                CurrentCompany.ContactLastName = ContactLastNameTextbox.Text;
				CurrentCompany.PNMSiteId = PNMSiteId.Text;
				CurrentCompany.PNMSecretKey = PNMSecretKey.Text;
                CurrentCompany.SalesFirstName = CompanyContactFirstNameTextbox.Text;
                CurrentCompany.SalesLastName = CompanyContactLastNameTextbox.Text;
                CurrentCompany.SalesPhone = CompanyContactPhoneTextbox.Text;
                CurrentCompany.Fax = CompanyFaxTextbox.Text;
                CurrentCompany.UsesApplyConnect = chkboxApplyConnect.Checked;
                
                Company.Set(CurrentCompany);

                if (chkboxApplyConnect.Checked)
                {
                    SqlConnection con = new SqlConnection(ConnString);
                    {
				        SqlCommand com = new SqlCommand("usp_Property_SetApplyConnect", con);
				        com.CommandType = CommandType.StoredProcedure;
				        com.Parameters.Add("@CompanyID", SqlDbType.Int, -1).Value = CurrentCompany.CompanyId;
                        com.Parameters.Add("@Checked", SqlDbType.Bit, -1).Value = 1;
				        com.CommandTimeout = 360;
				        con.Open();
				        SqlDataAdapter SqlAdapt = new SqlDataAdapter(com);
                        com.ExecuteReader(CommandBehavior.CloseConnection);
				        con.Close();
                    }
                }

                else
                {
                    SqlConnection con = new SqlConnection(ConnString);
                    {
				        SqlCommand com = new SqlCommand("usp_Property_SetApplyConnect", con);
				        com.CommandType = CommandType.StoredProcedure;
				        com.Parameters.Add("@CompanyID", SqlDbType.Int, -1).Value = CurrentCompany.CompanyId;
                        com.Parameters.Add("@Checked", SqlDbType.Bit, -1).Value = 0;
				        com.CommandTimeout = 360;
				        con.Open();
				        SqlDataAdapter SqlAdapt = new SqlDataAdapter(com);
                        com.ExecuteReader(CommandBehavior.CloseConnection);
				        con.Close();
                    }
                }

                if (CompanyLogoUpload.PostedFile != null && CompanyLogoUpload.FileBytes.Length > 0)
                {
                    Company.SetLogo(CurrentCompany.CompanyId, CompanyLogoUpload.FileBytes);
                }
				companySuccess.Visible = true;
                //PageMessage.DisplayMessage(SystemMessageType.Success, "Successfully saved property management information.");
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }

        protected void RemovePropertyFromCompany_Command(object sender, CommandEventArgs e)
        {
            try
            {
                var id = e.CommandArgument.ToString().ToNullInt32();
                if (id.HasValue)
                {
                    Company.RemovePropertyFromCompany(id.Value, CurrentCompany.CompanyId);
					successMsg.Visible = true;
                    //PageMessage.DisplayMessage(SystemMessageType.Success, "Removed property from company.");
                }
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
        }

		protected void DeleteIcon_Click(object sender, EventArgs e)
		{
			using (var entity = new RPO.RPOEntities())
			{
				if (EfxFramework.Helpers.Helper.IsRpoAdmin)
				{
					var company = entity.Companies.First(c => c.CompanyId == CurrentCompany.CompanyId);
					company.IsDeleted = true;
					entity.SaveChanges();

                    //CMallory - Task 00593 - Added call to stored procedure that sets the IsDeleted value in the Property table in the database table to true when a company is deleted.
                    try
                    {
                        Company.DeleteCompanyProperties(CurrentCompany.CompanyId);
                    }
                
                    catch
                    {
                        Response.Redirect("~/ErrorPage.html", false);
                    }
			    }
				Response.Redirect("/rpo");
			}
		}
        private static String ConnString
        {
            get
            {
                return EfxFramework.EfxSettings.ReportConnectionString;
            }
        }
    }
}