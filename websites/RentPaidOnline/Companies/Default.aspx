﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Domus.Companies.Default" %>
<%@ Import Namespace="EfxFramework.ExtensionMethods" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script> $(function () { EFXClient.SetSelectedNavigation('property-management-link'); }); </script>
    <div id="CompanyHome">
        <div class="add-property-wrap float-wrap green-gradient-wrap">
            <a href="NewCompany.aspx" class="green-button">Add Company <span class="button-icon">+</span></a>
            <div class="float-right">
                <div class="float-right" style="margin-left: 10px;">
                <asp:Button ID="ClearButton" runat="server" CssClass="green-button float-right" OnClick="ClearButton_Click" Text="Clear" style="position: relative;left: 5px;" />
                <asp:Button ID="SearchButton" runat="server" CssClass="search-button float-right" OnClick="SearchButton_Click" Text="Search" />
                <asp:TextBox CssClass="float-right" ID="SearchTextbox" runat="server" Width="200px"></asp:TextBox>
                </div>
                <label>Sort by:</label>
                <asp:DropDownList ID="SortByDropdown" runat="server" AutoPostBack="true" OnSelectedIndexChanged="SortByDropdown_SelectedIndexChanged">
                    <asp:ListItem Text="-- No Sort --" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="A-Z" Value="ASC"></asp:ListItem>
                    <asp:ListItem Text="Z-A" Value="DESC"></asp:ListItem>
                </asp:DropDownList>
                <label>Filter by:</label>
                <asp:DropDownList ID="FilterByDropdown" runat="server" AutoPostBack="true" OnSelectedIndexChanged="FilterByDropdown_SelectedIndexChanged">
                    <asp:ListItem Text="-- No Filter --" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="Active" Value="Active"></asp:ListItem>
                    <asp:ListItem Text="Not Active" Value="InActive"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <asp:Repeater ID="CompaniesRepeater" runat="server">
            <HeaderTemplate><div id="CompanyList" class="green-gradient-wrap float-wrap padding"></HeaderTemplate>
            <FooterTemplate></div></FooterTemplate>
            <ItemTemplate>
                <div class="property-listitem" style="width: 292px;">
                    <div class="property-wrap" style="height: 220px;">
                        <h4><%# Eval("CompanyName") %></h4>
                        <div class="propert-right">
                            <p><%# Eval("DisplayAddress") %></p>
                            <p>Number of Units: <%# Eval("NumberOfUnits") %></p>
                            <p>Participating Units: <%# Eval("NumberOfParticipatingUnits") %></p>
                        </div>
                        <div class="propert-left">
                            <img src="<%# string.Concat("/Handlers/EFXAdminHandler.ashx?RequestType=0&companyId=", Eval("CompanyId")) %>" alt="Picture" class="orange" style="width: 90px;" />
                        </div>
                        <div class="clear padding-top">
                            <p class="padding-bottom">Main Contact: <%# Eval("ContactFullName") %></p>
                            <p>Phone: <%# Eval("Phone").ToString().FormatPhoneNumber() %></p>
                        </div>
                        <div class="float-wrap" style="position: absolute;bottom: 10px;width: 270px;">
                            <div style="position: relative; left: 0">
                                <asp:ImageButton OnClientClick="return confirm('Are you sure you want to delete this company?');" ImageUrl="/Images/Trash.png" runat="server" Visible='<%# (bool)Eval("IsDeleted") == false %>' CssClass="float-left" ID="DeleteCompany" CommandArgument='<%# Eval("CompanyId") %>' CommandName="Delete" OnCommand="DeleteCompany_Command" />
                                <asp:ImageButton ImageUrl="/Images/UndoTrash.png" runat="server" Visible='<%# Eval("IsDeleted") %>' CssClass="float-left" ID="UnDeleteCompany" CommandArgument='<%# Eval("CompanyId") %>' CommandName="UnDelete" OnCommand="DeleteCompany_Command" />
                            </div>
                            <div style="position: relative; right: 10px">
                                <a class="float-right" href="CompanyDetails.aspx?companyId=<%# Eval("CompanyId") %>"><img src="/Images/magnifier.png" alt="View Details" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:PlaceHolder ID="EmptyRecordsSection" runat="server" Visible="false">
            <div class="empty-records">
                There are no property management companies.
            </div>
        </asp:PlaceHolder>
        <div class="add-property-wrap float-wrap green-gradient-wrap">
            <a href="NewCompany.aspx" class="green-button">Add Company <span class="button-icon"">+</span></a>
        </div>
    </div>
</asp:Content>
