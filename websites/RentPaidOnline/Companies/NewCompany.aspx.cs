﻿using EfxFramework;
using EfxFramework.ExtensionMethods;
using EfxFramework.Web;
using System;
using System.Web.UI.WebControls;
using RPO.Helpers;

namespace Domus.Companies
{
    public partial class NewCompany : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			if (!EfxFramework.Helpers.Helper.IsRpoAdmin)
				Response.Redirect("/");
            if (!IsPostBack)
            {
                StateDropdown.BindToSortedEnum(typeof(StateProvince));
                PropertyManagementCompanyNameTextbox.Focus();
            }
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(PropertyManagementCompanyNameTextbox.Text))
            {
				CustomValidator err = new CustomValidator();
				err.ValidationGroup = "NewCompany";
				err.IsValid = false;
				err.ErrorMessage = "Property management company name is required.";
				Page.Validators.Add(err);
                //PageMessage.DisplayMessage(SystemMessageType.Error, "Property management company name is required.");
                return;
            }
            if (string.IsNullOrEmpty(CompanyEmailTextbox.Text))
            {
				CustomValidator err = new CustomValidator();
				err.ValidationGroup = "NewCompany";
				err.IsValid = false;
				err.ErrorMessage = "Email address is required.";
				Page.Validators.Add(err);
                //PageMessage.DisplayMessage(SystemMessageType.Error, "Email address is required.");
                return;
            }

            var company = new Company();
            company.CompanyName = PropertyManagementCompanyNameTextbox.Text;
            company.EmailAddress = CompanyEmailTextbox.Text;
            company.Address = AddressTextbox.Text;
            company.Address2 = AddressLine2Textbox.Text;
            company.City = CityTextbox.Text;
            company.PostalCode = ZipTextbox.Text;
            company.StateProvinceId = (StateProvince)Enum.Parse(typeof(StateProvince), StateDropdown.SelectedValue);
            company.Phone = CompanyPhoneTextbox.Text;
            company.Mobile = CompanyMobileTextbox.Text;
            company.SalesFirstName = CompanyContactFirstNameTextbox.Text;
            company.SalesLastName = CompanyContactLastNameTextbox.Text;
            company.ContactFirstName = ContactFirstNameTextbox.Text;
            company.ContactLastName = ContactLastNameTextbox.Text;
            company.SalesPhone = CompanyContactPhoneTextbox.Text;
            company.Fax = CompanyFaxTextbox.Text;
            company.IsDeleted = false;

            var id = Company.Set(company);

            if (CompanyLogoUpload.PostedFile != null)
            {
                Company.SetLogo(id, CompanyLogoUpload.FileBytes);
            }

            // Redirect to company details
            SafeRedirect(string.Format("CompanyDetails.aspx?companyId={0}", id));
        }
    }
}