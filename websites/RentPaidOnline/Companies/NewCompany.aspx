﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="NewCompany.aspx.cs" Inherits="Domus.Companies.NewCompany" %>

<%@ Import Namespace="EfxFramework.ExtensionMethods" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="main-container">
        <div class="section-body">
            <div id="CompaniesWrap">
                <div class="formWrapper">
                    <h2>Add Property Management Company</h2>
					<asp:ValidationSummary ID="newCompanyValidationSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="NewCompany" />
                    <div class="formWhole">
                        <div class="formHalf">
                            <div class="well">
                                <label>Property Management Company Name</label>
                                <asp:TextBox ID="PropertyManagementCompanyNameTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>


                    <div class="formHalf">
                        <label>Address</label>
                        <asp:TextBox ClientIDMode="Static" ID="AddressTextbox" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="formHalf">
                        <label>Address 2</label>
                        <asp:TextBox ClientIDMode="Static" ID="AddressLine2Textbox" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>

                    <div class="formWhole">
                        <div class="formHalf">
                            <label>City</label>
                            <asp:TextBox ClientIDMode="Static" ID="CityTextbox" runat="server" CssClass="form-control" />
                        </div>
                        <div class="formHalf">
                            <div class="formHalf">
                                <label>State</label>
                                <asp:DropDownList ID="StateDropdown" ClientIDMode="Static" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <div class="formFourth">
                                <label>Zip</label>
                                <asp:TextBox ClientIDMode="Static" ID="ZipTextbox" runat="server" CssClass="form-control" />
                            </div>
                        </div>
                    </div>

                    <h2>Contact Information</h2>

                    <div class="formHalf">
                        <label>Contact First Name</label>
                        <asp:TextBox ID="ContactFirstNameTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="formHalf">
                        <label>Contact Last Name</label>
                        <asp:TextBox ID="ContactLastNameTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>


                    <div class="formWhole">
                        <div class="formThird">
                            <label>Phone</label>
                            <asp:TextBox ID="CompanyPhoneTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="formThird">
                            <label>Mobile</label>
                            <asp:TextBox ID="CompanyMobileTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="formThird">
                            <label>Fax</label>
                            <asp:TextBox ID="CompanyFaxTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>


                    <div class="formThird">
                        <label>Email</label>
                        <asp:TextBox ID="CompanyEmailTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="formWhole">
                        <hr />
                    </div>


                    <div class="formWhole">
                        <div class="formHalf">
                            <h2>Company Logo</h2>
                            <div class="well">

                                <asp:FileUpload ID="CompanyLogoUpload" runat="server" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>

                    <div class="formWhole">
                        <hr />
                    </div>

                    <h2>EFX Sales</h2>
                    <div class="formHalf">
                        <label>Contact First Name</label>
                        <asp:TextBox ID="CompanyContactFirstNameTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="formHalf">
                        <label>Contact Last Name</label>
                        <asp:TextBox ID="CompanyContactLastNameTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="formThird">
                        <label>Contact Phone</label>
                        <asp:TextBox ID="CompanyContactPhoneTextbox" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>


                <div class="button-footer">
                    <div class="main-button-action">
                         <!--CMallory - Layout Change - Added btn-footer CSS class to tag -->
                        <asp:Button ID="SaveButtonStep1" runat="server" CssClass="btn btn-primary btn-footer" Text="Save" OnClick="SaveButton_Click" />
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="Scripts">
	<script>
		$(function () {
			/* Mask */
			ApplySingleMask("#<%=ZipTextbox.ClientID%>", "00000");
			ApplySingleMask("#<%=CompanyPhoneTextbox.ClientID%>", "(000) 000-0000");
			ApplySingleMask("#<%=CompanyMobileTextbox.ClientID%>", "(000) 000-0000");
			ApplySingleMask("#<%=CompanyFaxTextbox.ClientID%>", "(000) 000-0000");
			ApplySingleMask("#<%=CompanyContactPhoneTextbox.ClientID%>", "(000) 000-0000");
		});
	</script>
</asp:Content>
