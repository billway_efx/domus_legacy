﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestXML.aspx.cs" Inherits="Domus.TestXML" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div>
            <h2>Test New SqlDatareader</h2>
            <%--<asp:Button ID="Button10" runat="server" Text="Button" OnClick="Button10_Click" />--%>

        </div>

        <div>
           <h2>
               test output
           </h2>
            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="Button11" runat="server" Text="Test Output" OnClick="Button11_Click" /><br />
            <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label>

        </div>

    <div>
    <h2>Testing XML stuff</h2>

        <asp:Button ID="Button1" runat="server" Text="Test Xml Export" OnClick="Button1_Click" />&nbsp;
        <asp:Button ID="Button2" runat="server" Text="Loop for Ach" OnClick="Button2_Click" />
        <br />
        <asp:Label ID="TestLbl" runat="server" Text="Label"></asp:Label><br />
        <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Height="133px" Width="991px"></asp:TextBox>
    </div>

        <div>

            <h2>
                Testing Scheduler
            </h2>
            <asp:Button ID="Button3" runat="server" Text="Run Scheduler" OnClick="Button3_Click" />

            <br />
            <br />
            <asp:Button ID="Button4" runat="server" Text="Send Test Email" OnClick="Button4_Click" />
            <br />
            <br />
            <asp:Label ID="Label1" runat="server" Text="Payment ID"></asp:Label><asp:TextBox ID="TextBox2" runat="server"></asp:TextBox><br />
            <asp:Label ID="Label4" runat="server" Text="External ID"></asp:Label><asp:TextBox ID="ExtTranstxt" runat="server"></asp:TextBox><br />
            <asp:Button ID="Button5" runat="server" Text="Run Transaction Update" OnClick="Button5_Click" /><br />
            <asp:Label ID="statslbl" runat="server"></asp:Label>
        </div>

        <div>
            <h2> Manual insert into ACH</h2>
            <asp:Button ID="Button9" runat="server" Text="Insert Record to ACH" OnClick="Button9_Click" /><br /><br />

            <asp:Label ID="AchTransIDtxt" runat="server" ></asp:Label>

        </div>


        <div>
            <br />
            <br />

            CheckBox COntrol
                        <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                            <asp:ListItem Value="1">Property 1</asp:ListItem>
                             <asp:ListItem Value="2">Property 2</asp:ListItem>
                             <asp:ListItem Value="3">Property 3</asp:ListItem>
                             <asp:ListItem Value="4">Property 4</asp:ListItem>
            </asp:CheckBoxList>

        </div>

        <hr />
        <div>
            <h1>Test Yardi Resident Import</h1>
            <asp:Label ID="Label2" runat="server" Text="Renter ID: "></asp:Label><asp:TextBox ID="YardiRenterIDtxt" runat="server"></asp:TextBox><br />
            <asp:Button ID="Button6" runat="server" Text="Test Import" OnClick="Button6_Click" />

        </div>

        <div>
            <h2>Test Local time</h2>
            <asp:Label ID="Label3" runat="server" Text="Time"></asp:Label><br />
            <asp:TextBox ID="timetxt" runat="server"></asp:TextBox><br />
            <asp:Button ID="Button7" runat="server" Text="Test Time" OnClick="Button7_Click" />
        </div>
        <div>
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1030px">
                <LocalReport ReportPath="ReportingPages\SSRSreports\AutoPaymentReport.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="AutoPaymentData" />
                    </DataSources>
                </LocalReport>
            </rsweb:ReportViewer>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="Domus.ReportingPages.SSRSreports.DataSet1TableAdapters.usp_Report_RPO_AutoPaymentTableAdapter">
                <SelectParameters>
                    <asp:Parameter DefaultValue="10" Name="PropertyIdList" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>



        </div>

        <div>
            <asp:Button ID="Button8" runat="server" Text="GetXmL" OnClick="Button8_Click" />

        </div>


    </form>
</body>
</html>
