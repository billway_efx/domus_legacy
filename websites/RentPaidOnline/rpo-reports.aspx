﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="rpo-reports.aspx.cs" Inherits="Domus.rpo_reports" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TopHeadline" runat="server">
	<h1>Reports</h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<div id="reportManager" class="main-container success-top">
		<div class="report-header {{ selected ? 'selected' : 'open'}}" ng-controller="MenuController">
			<a href="#" class="report-nav"><span data-icon="&#x32" aria-hidden="true" ng-class="{'back': selected}"></span><span class="default-nav">{{title}}</span></a>
			<ul class="report-list list-unstyled">
				<li><a href="{{urlPrefix}}auto-payment" data-report="">AutoPayment</a></li>
				<li><a href="{{urlPrefix}}applicant-portal-payment">Applicant Portal Payment</a></li>
				<li ng-show="isRPOAdmin"><a href="{{urlPrefix}}billing">Billing</a></li>
				<li><a href="{{urlPrefix}}detail-deposit">Detailed Deposit</a></li>
				<%--<li ng-show="isRPOAdmin"><a href="{{urlPrefix}}lead">Lead</a></li>--%>
				<li><a href="{{urlPrefix}}payment-export-summary">Payment Export Summary</a></li>
                <li><a href="{{urlPrefix}}payment-summary">Payment Summary</a></li>
                <li><a href="{{urlPrefix}}resident-data-import-summary">Resident Data Import Summary</a></li>
                <%--<li><a href="{{urlPrefix}}renters-past-due">Residents Past Due</a></li>--%>
                <li><a href="{{urlPrefix}}transaction-summary">Transaction Summary</a></li>
			</ul>
		</div>
        <div ng-controller="FilterController">
            <div class="well no-padding has-dividers">
			    <div class="divider-fluid" ng-show="showPropertyDropdown">
				    <div class="padding">
						<select ng-model="propertySelection" required ng-options="p.PropertyName for p in properties" class="form-control multiselect" data-placeholder="Select Properties" multiple="multiple" multiselect-dropdown >
						</select>
					</div>
			    </div>
			    <div class="divider-fluid date-select" ng-if="startDateVisibility">
				    <div class="padding">
						<div>
							<label>Start:</label>
							<input type="text" class="text form-control" ng-model="startDate.date" bs-datepicker />
							<a class="btn-calendar" href="" data-toggle="datepicker"><span class="ctaIcon" data-icon="&#x65" aria-hidden="true"></span></a>
						</div>
                        <div>
							<label class="range-to">End:</label>
							<input type="text" class="text form-control" ng-model="endDate.date" bs-datepicker />
							<a class="btn-calendar" href="" data-toggle="datepicker"><span class="ctaIcon" data-icon="&#x65" aria-hidden="true"></span></a>
                        </div>
				    </div>
			    </div>
			    <div class="divider-fluid">
				    <div class="padding">
					    <a href="" class="btn btn-primary" ng-disabled="!filtersSet" ng-click="filterReport(properties)">View Report</a>
				    </div>
			    </div>
		    </div>
		    <!--end expandable-table-actions-->
		    <h3 ng-show="filtered">{{fullReportName}} Report results</h3>
		    <div class="expandable-table-actions">
			    <a href="" class="btn btn-{{showAllDetails ? 'primary' : 'blank'}}" ng-click="expandList()">Expand All</a>
			    <a href="" class="btn btn-{{!showAllDetails ? 'primary' : 'blank'}}" ng-click="collapseList()">Collapse All</a>
		    </div>
		    <div class="export-select">
			    <select class="form-control" ng-model="fileType">
                    <option value="CSV" selected="selected">CSV</option>
                    <option value="PDF">PDF</option>
			    </select>
			    <a href="/reporting/export.ashx?f={{fileType}}&r={{reportName}}&s={{startDate.date}}&e={{endDate.date}}&p={{currentSelectedProperties}}" target="_self" class="btn btn-default"><span class="ctaIcon" data-icon="&#x48" aria-hidden="true"></span> Export</a>
		    </div>
        </div>
		<div class="clear"></div>

        <!--Added div for Report Loader called from .js file *********** -->
        <div id="ReportLoader" style="clear: both; text-align: center;"></div>
        <!--************************************************************ -->

		<div class="table-holder">
			<div ng-view></div>
            <!--end styled-table-->
		</div>
		<!--end table-holder-->
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="Scripts">
    <script src="<%= ResolveClientUrl("~/Scripts/rpo-reports.js") %>"></script>
    
	<script>
		$(function () {
			var $reportHeader = $('.report-header'),
				$reportUl = $('ul.report-list'),
				$reportIcon = $('a.report-nav span').eq(0),
				$reportNav = $('a.report-nav span').eq(1);

			function selectReport ($report) {
				var currentReport = $report.html();

				$reportUl.hide();
				$reportHeader.addClass('selected').removeClass('open');
				$reportIcon.addClass('back');
				$reportNav.html(currentReport);
			}
			function deselectReport () {
				$reportHeader.removeClass('selected').addClass('open');
				$reportIcon.removeClass('back');
				$reportNav.html('Select a Report');
				$reportUl.show();
			}
			$('ul.report-list a').click(function () {
				selectReport($(this));
			});
			$('a.report-nav').click(function (e) {
				deselectReport();
				e.preventDefault();
			});
		});
	</script>
</asp:Content>