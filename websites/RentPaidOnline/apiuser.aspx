﻿<%@ Page Title="API User Maintenance" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" 
    CodeBehind="apiuser.aspx.cs" Inherits="Domus.apiuser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <strong>Add new API user:</strong><br /><br />
    <asp:Table ID="Table1" runat="server" Width="672px">
        <asp:TableRow>
            <asp:TableCell>
                User name:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtUserName"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Password:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtPassword"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Partner Company Id:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtPartnerCompanyId" value="1"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center">
                &nbsp;<br />
                &nbsp;<br />
                <asp:Button runat="server" Text="Add New User" id="cmdAdd" OnClick="cmdAdd_Click" />
            </asp:TableCell>
            <asp:TableCell>
                &nbsp;
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center">
                &nbsp;<br />
                <asp:Button runat="server" Text="Maintain API Users" id="cmdMaintain" OnClick="cmdMaintain_Click" />
            </asp:TableCell>
            <asp:TableCell>
                &nbsp;
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <br />
    <br />
    <br />
    <br />
    <br />
    
</asp:Content>
