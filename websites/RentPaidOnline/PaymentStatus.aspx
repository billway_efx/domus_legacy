﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="PaymentStatus.aspx.cs" Inherits="Domus.PaymentStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopHeadline" runat="server">
    <h1>Payment Status</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div class="main-container">
        <div class="section-body">


            <div class="receiptHolder">
                <header>
                    <div class="well">
                        <div class="highlighted message">Your Transaction was Processed</div>
                        <h2>Thank You</h2>
                        <p>Rent and Fees Transaction Accepted</p>
                    </div>
                </header>

                <!--Left Column -->
                <div class="recieptColumn Left">


                    <!--**Transaction ID**-->

                    <div class="formWhole">
                        <span class="formLabel">Transaction ID: </span>
                        <div class="highlighted">
                            <asp:Label ID="TransactionIdLabel" runat="server" />
                        </div>
                    </div>

                    <!--**Rent**-->
                    <div class="formWhole">
                        <span class="formLabel">Rent: </span>
                        <div class="highlighted">
                            <asp:Label ID="RentAmountLabel" runat="server" />
                        </div>

                    </div>
                    <!--**Convenience Fee**-->
                    <div class="formWhole">
                        <span class="formLabel">Convenience Fee: </span>
                        <div class="highlighted">
                            <asp:Label ID="ConvenienceFeeLabel" runat="server" />
                        </div>

                    </div>
                    <!--**Total Payment Amount**-->
                    <div class="formWhole">
                        <span class="formLabel">Total Payment Amount: </span>
                        <div class="highlighted">
                            <asp:Label ID="TotalPaymentAmountLabel" runat="server" />
                        </div>
                    </div>


                    <!--**Property**-->
                    <div class="formWhole">
                        <h3>Property </h3>

                        <div class="lightPanel">
                            <span style="font-weight: bold">
                                <asp:Label ID="PropertyNameLabel" runat="server" /></span><br />

                            <span style="font-weight: bold">
                                <asp:Label ID="PropertyAddressLabel" runat="server" /></span><br />

                            <span style="font-weight: bold">
                                <asp:Label ID="PropertyCityStateZipLabel" runat="server" /></span>
                        </div>
                    </div>


                    <!--**RPO Support**-->
                    <div class="formWhole">
                        <h3>RPO Support </h3>
                        <div class="lightPanel">
                            Support Line:
                            <span style="font-weight: bold">
                                <asp:Label ID="supportPhone" runat="server" /></span><br />

                            Email:
                            <span style="font-weight: bold">
                                <asp:Label ID="supportEmail" runat="server" /></span><br />

                        </div>
                    </div>
                </div>
            </div>


            <!--Right Column-->
            <div class="recieptColumn right">


                <!--**First Name **-->
                <div class="formWhole">
                    <span class="formLabel">First Name: </span>
                    <span style="font-weight: bold">
                        <asp:Label ID="FirstNameLabel" runat="server" />
                    </span>
                </div>


                <!--**Middle Name **-->
                <div class="formWhole">
                    <span class="formLabel">Middle Name: </span>
                    <span style="font-weight: bold">
                        <asp:Label ID="MiddleNameLabel" runat="server" />
                    </span>
                </div>


                <!--**Last Name**-->
                <div class="formWhole">
                    <span class="formLabel">Last Name: </span>
                    <span style="font-weight: bold">
                        <asp:Label ID="LastNameLabel" runat="server" />
                    </span>
                </div>


                <!--**Suffix**-->
                <div class="formWhole">
                    <span class="formLabel">Suffix: </span>
                    <span style="font-weight: bold">
                        <asp:Label ID="SuffixLabel" runat="server" />
                    </span>
                </div>


                <!--**Billing Address**-->
                <div class="formWhole">
                    <span class="formLabel">Billing Address: </span>
                    <span style="font-weight: bold">
                        <asp:Label ID="BillingAddressLabel" runat="server" />,
                          
                        &nbsp;<asp:Label ID="BillingCityStateZipLabel" runat="server" />
                    </span>

                </div>


                <!--**Phone Number**-->
                <div class="formWhole">
                    <span class="formLabel">Phone Number: </span>
                    <span style="font-weight: bold">
                        <asp:Label ID="PhoneNumberLabel" runat="server" />
                    </span>
                </div>


                <!--**Payment Method**-->
                <div class="formWhole">
                    <span class="formLabel">Payment Method: </span>
                    <span style="font-weight: bold">
                        <asp:Label ID="PaymentMethodLabel" runat="server" />
                    </span>
                </div>

            </div>
        </div>

        <div class="formWhole"></div>
         
        <div class="button-footer">
            <div class="button-action">
                <%--<asp:Button ID="CancelButton" runat="server" Text="Cancel" CssClass="btn btn-default" PostBackUrl="~/MyAccount/Default.aspx#JobPostsTab" />--%>
            </div>
            <div class="main-button-action">
                <asp:LinkButton ID="PrintButton" runat="server" CssClass="btn btn-primary" OnClientClick="window.print(); return false;">Print</asp:LinkButton>
            </div>
            <div class="clear"></div>
        </div>

    </div>

</asp:Content>
