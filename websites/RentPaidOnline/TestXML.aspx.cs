﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Text;
using EfxFramework.Scheduler;
using System.Net;
using System.Net.Mail;
using EfxFramework;
//using RPO;
using EfxFramework.PublicApi;

namespace Domus
{
    public partial class TestXML : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Salcedo - Don't uncomment the redirect unless you're testing, as this page is a security risk otherwise.
            //Response.Redirect("/default.aspx");


            //TestPNMRequest();

            //AddTransactionRecordFromPaymentRecord(80363, "Transaction completed successfully");

            //Salcedo - 9/4/2015
            //We currently have no way to add an API user, so here's a temporary workaround
            //First, create a new PartnerCompany record and get a PartnerCompanyId
            //Then, add the user to the database, supplying a username, password, and PartnerCompanyId
           // SetNewApiUser("TestApiUser", "993Hvw6AA", 2);

        }

        private void SetNewApiUser(string NewUserName, string NewPassword, int ParterCompanyId)
        {
            var NewApiUser = new ApiUser
            {
                Username = NewUserName, //_View.UsernameText,
                IsActive = true
            };

            NewApiUser.SetPassword(NewPassword); //_View.PasswordText);
            //if (_View.PartnershipsCheckList.Items.Cast<ListItem>().Any(item => item.Selected))
            //    NewApiUser.PartnerCompanyId = _View.PartnershipsCheckList.SelectedValue.ToInt32();
            NewApiUser.PartnerCompanyId = ParterCompanyId;

            var ApiUserId = ApiUser.Set(NewApiUser);

            var User = new ApiUser(ApiUserId);

            //if (_View.PropertiesCheckList.Items.Cast<ListItem>().Any(item => item.Selected))
            //    foreach (var Item in _View.PropertiesCheckList.Items.Cast<ListItem>().Where(item => item.Selected))
            //    {
            //        User.AddPropertyToApiUser(Item.Value.ToInt32());
            //    }
        }

        private static void TestPNMRequest()
        {
            EfxFramework.Renter Resident = new EfxFramework.Renter(31); //Steven Foster ~ Test

            var Address = EfxFramework.Address.GetAddressByRenterIdAndAddressType(Resident.RenterId, (int)TypeOfAddress.Billing);
            Address.AddressTypeId = (int)TypeOfAddress.Billing;
            Address.AddressLine1 = "123 Test St";
            Address.City = "Test City";
            Address.StateProvinceId = 1;
            Address.PostalCode = "12345";
            Address.PrimaryEmailAddress = "mtest@test.com";
            Address.PrimaryPhoneNumber = "1231231234";



            RPO.PayNearMeRequest pnmRequest = new RPO.PayNearMeRequest();
            pnmRequest.Amount = 1;
            pnmRequest.CreatorIdentifier = "SalcedoTest";
            pnmRequest.Currency = "USD";
            pnmRequest.CustomerCity = Address.City;
            pnmRequest.CustomerEmail = Address.PrimaryEmailAddress;
            pnmRequest.CustomerIdentifier = Resident.PublicRenterId.ToString();
            pnmRequest.CustomerName = Resident.FirstName + " " + Resident.LastName;
            pnmRequest.CustomerPhone = Address.PrimaryPhoneNumber;
            pnmRequest.CustomerPostalCode = Address.PostalCode;
            pnmRequest.CustomerState = "FL";
            pnmRequest.CustomerStreet = Address.AddressLine1;
            pnmRequest.MinimumPaymentAmount = 1;
            pnmRequest.MinimumPaymentCurrency = "USD";
            pnmRequest.OrderDescription = "Resident Payment";
            pnmRequest.OrderDuration = 1;
            pnmRequest.OrderIdentifier = Guid.NewGuid().ToString();
            pnmRequest.OrderType = RPO.PayNearMeRequest.PNMOrderType.Exact;
            pnmRequest.RenterId = Resident.RenterId;
            pnmRequest.PropertyId = 1;
            pnmRequest.CompanyId = 19;
            pnmRequest.SiteIdentifier = "S4306765371"; //borrwed from AGPM
            pnmRequest.SecretKey = "b15e2f63fe3de0dc"; //borrwed from AGPM
            //Salcedo - task 00337 - 1/12/2015 - moved appSettings to database
            //pnmRequest.Version = ConfigurationManager.AppSettings["PNMVersion"].ToString();
            pnmRequest.Version = Settings.PNMVersion;

            //Update the associated address
            var AddressId = EfxFramework.Address.Set(Address);
            //EfxFramework.Address.AssignAddressToRenter(Resident.RenterId, AddressId);

            //Salcedo Task 00076
            var Log = new RPO.ActivityLog();
            //Log.WriteLog(_View.ViewPage.Page.User.Identity.Name, "Address has been updated (VPNM)", 0, Resident.RenterId.ToString(), true);

            RPO.RentByCash rentByCash = new RPO.RentByCash();
            //Salcedo - task 00337 - 1/12/2015 - moved appSettings to database
            //rentByCash.SandBoxMode = ConfigurationManager.AppSettings["PNMUseSandboxMode"].ToBool();
            rentByCash.SandBoxMode = true; // Settings.PNMUseSandboxMode.ToBool();

            string response = rentByCash.CreatePayNearMeRequest(pnmRequest, rentByCash.SandBoxMode);
        }

        private static void FixPNMPayment(int RenterId, decimal netPaymentAmount)
        {
            RecordPayment(netPaymentAmount, new EfxFramework.Renter(RenterId), "Rent By Cash successfully submitted");
            EfxFramework.Lease lease = EfxFramework.Lease.GetRenterLeaseByRenterId(RenterId);
            AdjustCurrentBalanceDue(netPaymentAmount, lease);
        }

        private static void RecordPayment(decimal amount, EfxFramework.Renter renter, string responseMessage)
        {
            SetPayment(renter, EfxFramework.PaymentMethods.PaymentAmount.GetPaymentAmount(amount, renter, EfxFramework.PaymentMethods.PaymentType.RentByCash, false), "Rent By Cash", EfxFramework.PaymentMethods.PaymentApplication.Rent, responseMessage);
        }

        private static void SetPayment(EfxFramework.Renter renter, EfxFramework.PaymentMethods.PaymentAmount amount, string description, EfxFramework.PaymentMethods.PaymentApplication paymentApplication, string responseMessage)
        {
            if (!renter.PayerId.HasValue)
                renter.PayerId = 0;

            int PaymentId;

            PaymentId = EfxFramework.Payment.Set(EfxFramework.Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.RentByCash, null, null, null, null,
                null, amount, description, null, null, null, null, null, false, null,
                "0", responseMessage, null, EfxFramework.PaymentChannel.Web, EfxFramework.PublicApi.Payment.PaymentStatus.Cleared));
            EfxFramework.Transaction.SetTransactionFromPayment(new EfxFramework.Payment(PaymentId), 0, responseMessage);

            //cakel: BUGID00248
            //Add Email Response Here
            //SendPNMConfirmEmail(PaymentId);

        }

        private static void AddTransactionRecordFromPaymentRecord(int PaymentId, string responseMessage)
        {
            EfxFramework.Transaction.SetTransactionFromPayment(new EfxFramework.Payment(PaymentId), 0, responseMessage);
        }

        private static void AdjustCurrentBalanceDue(decimal amount, EfxFramework.Lease lease)
        {
            lease.CurrentBalanceDue -= amount;
            lease.CurrentBalanceDueLastUpdated = DateTime.UtcNow;

            if (lease.CurrentBalanceDue <= 0.00M)
                lease.PaymentDueDate = lease.PaymentDueDate.HasValue ? lease.PaymentDueDate.Value.AddMonths(1) : DateTime.UtcNow.AddMonths(1);

            EfxFramework.Lease.Set(lease);
        }
        public string mysqlstring()
        {
            string sqlstring = "Select 'Detail', RenterID, RentAmount from [dbo].[Payment] Where IsPmsProcessed = 0 and TransactionDateTime  > '01/04/2014'";
           // string sqlstring2 = "SELECT (Select RenterID, RentAmount from Payment as a Where IsPmsProcessed = 0 and a.PaymentId = b.PaymentId and a.RenterId = b.RenterId FOR XML AUTO, TYPE) as RTServiceTransactions, [TransactionDateTime] FROM [dbo].[Payment] b Where [TransactionDateTime] > '07/31/2014' and IsPmsProcessed = 0 and RenterID = 52738 FOR XML AUTO, TYPE";
            return sqlstring;
        }


        public DataSet GetDataSet()
        {
            SqlConnection con = new SqlConnection(ConnString);
            string _sql = mysqlstring();
            

            con.Open();
            SqlDataAdapter SqlAdapt = new SqlDataAdapter(_sql, con);
            DataSet ds = new DataSet();
            SqlAdapt.Fill(ds, "MyData");

            return  ds;
        }

        private string GenerateXML(DataSet ds)
        {
	        StringWriter obj = new StringWriter();
	        string xmlstring = null;
           
	       ds.WriteXml(obj);

            xmlstring = obj.ToString();

           return xmlstring;

        }




        protected void Button1_Click(object sender, EventArgs e)
        {
                //string test = GenerateXML(GetDataSet());
                //    TestLbl.Text = test;

            //PrintData(GetDataSet());
            string test = CreateXmlData();
            
            //TestLbl.Text = test;
            //TextBox1.Text = test;
        }

        //XML data set
        public void PrintData(DataSet MyData)
        {
            
            string Mystring = "";
            
            foreach (DataTable MyTable in MyData.Tables)
            {
                
                foreach (DataRow row in MyTable.Rows)
                {
                    foreach (DataColumn column in MyTable.Columns)
                    {  
                        Mystring += (row[0]).ToString() + " " + (row[1]).ToString();
                        Mystring += "<br />";
                    }
                }
            }
            string test = Mystring.ToString();
            //TestLbl.Text = Mystring.ToString();
            
        }


        public SqlDataReader GetSqlReader()
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Payment_GetAllUnprocessYARDIPaymentsWithDepositDate",con);
            com.CommandType = CommandType.StoredProcedure;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                ex.ToString();
                con.Close();
                return null;
            }

        }

        public SqlDataReader GetSqlReader2()
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Payment_GetPaymentsForGroupAchbyProperty",con);
            com.CommandType = CommandType.StoredProcedure;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                ex.ToString();
                con.Close();
                return null;
            }
        }

        public DataSet MyNewDataSet()
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                SqlCommand sqlComm = new SqlCommand("usp_Payment_GetPaymentsForGroupAchbyProperty", conn);
                sqlComm.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;
                da.Fill(ds);
            }
            return ds;
        }


        public string CreateXmlData()
        {
            StringBuilder  output = new StringBuilder();
            XmlWriterSettings xmlsettings = new XmlWriterSettings();
            //xmlsettings.IndentChars = "\t";
            xmlsettings.Indent = true;
            xmlsettings.OmitXmlDeclaration = true;
            XmlWriter writer = XmlWriter.Create(output,xmlsettings);

            SqlDataReader reader = GetSqlReader();
            
            writer.WriteStartDocument();
            writer.WriteStartElement("Property");
            writer.WriteStartElement("RT_Customer");
            writer.WriteStartElement("RTServiceTransactions");

            while (reader.Read())
            {
                
                writer.WriteStartElement("Transactions");
                
                writer.WriteStartElement("Payment");
                writer.WriteAttributeString("Type", reader[0].ToString());

                writer.WriteStartElement("Details");

                //Start items in Details node
                writer.WriteStartElement("DocumentNumber");
                writer.WriteString(reader[1].ToString());
                writer.WriteEndElement();//DocumentNumber

                writer.WriteStartElement("TransactionDate");
                writer.WriteAttributeString("TheDate", "12/01/2015");
                writer.WriteAttributeString("Test2", "My Test");
                writer.WriteString(reader[2].ToString());
                
                writer.WriteEndElement();//TransactionDate

                writer.WriteStartElement("CustomerID");
                writer.WriteString(reader[3].ToString());
                writer.WriteEndElement();//CustomerID

                writer.WriteStartElement("PaidBy");
                writer.WriteString(reader[4].ToString());
                writer.WriteEndElement();//PaidBy

                writer.WriteStartElement("Amount");
                writer.WriteString(reader[5].ToString());
                writer.WriteEndElement();//Amount

                writer.WriteStartElement("Comment");
                writer.WriteString(reader[6].ToString());
                writer.WriteEndElement();//Comment

                writer.WriteStartElement("PropertyPrimaryID");
                writer.WriteString(reader[7].ToString());
                writer.WriteEndElement();//PropertyPrimaryID

                //End items in Details node

                //close
                writer.WriteEndElement();//Details
                writer.WriteEndElement();//Payment
                writer.WriteEndElement();//Transactions
            }

            writer.WriteEndElement();//RTServiceTransactions
            writer.WriteEndElement();//RT_Customer
            writer.WriteEndElement();//Property

            writer.WriteEndDocument();
            writer.Flush();

            string TestString = output.ToString();

            return output.ToString();
        }

        public string PrintAchData()
        {
            string mytestString = "";
            DataSet mySet = MyNewDataSet();
            foreach (DataRow dr in mySet.Tables[0].Rows)
            {
                mytestString += dr["PropertyName"].ToString() + "<br />";
                mytestString += dr["RentAmount"].ToString()  + " " +  dr["CCDepositAccountAchClientID"].ToString() + "<br />";
            }
            return mytestString;
            
        }





        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            
            //TestLbl.Text = PrintAchData();
            
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            //CreditCardAchSettlements.ProcessGroupCCAch();
            EcheckAutoPayment.ProcessEcheckAutoPayments();
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            try
            {
                SendAchErrorEmail("Error", "Test Property", "Johnny", "Cash", "08/25/2014", "j3hrt");
            }
            //catch (Exception ex)
            catch
            {
                //TestLbl.Text = "Failed!!!!!!!";
            }
        }

        private static void SendAchErrorEmail(string Transtatus, string propertyName, string Firstname, string Lastname, string transDate, string transID)
        {
            string MailBody = "<p>An error occured when trying to insert record into ACH DB for submission.  Please check the properties Credit Card Account on file.</p><br />";
            MailBody += "Status: " + Transtatus + "<br />";
            MailBody += "Property Name: " + propertyName + "<br />";
            MailBody += "Payer First Name: " + Firstname + "<br />";
            MailBody += "Payer Last Name: " + Lastname  + "<br />";
            MailBody += "Transaction Date: " + transDate + "<br />";
            MailBody += "Transaction ID: " + transID + "<br />";

            MailMessage ACHErrorMail = new MailMessage();
            ACHErrorMail.Subject = "ACH DB Insert Error";
            ACHErrorMail.Body = MailBody;
            //ACHErrorMail.From = MailAddress"support@Domus.com";
            ACHErrorMail.To.Add(new MailAddress("chris.akel@efxfs.com", "RPO Support Admin"));
            ACHErrorMail.IsBodyHtml = true;
            SmtpClient RpoSmtp = new SmtpClient();

            //Salcedo - 4/3/2015 - add support for dynamic host (mainly for testing)
            RpoSmtp.Host = Settings.SmtpHost;

            RpoSmtp.Send(ACHErrorMail);

        }

        protected void Button5_Click(object sender, EventArgs e)
        {

            try
            {
                int _PaymentID = int.Parse(TextBox2.Text);
                string ExtTransID = ExtTranstxt.Text;
                
                Transaction.SetTransactionFromPayment(new Payment(_PaymentID), 0, "Successfully Submitted");
                //Transaction.SetTransactionFromPayment(new Payment(32013), 0, "Successfully Submitted");

                InsertMerchantTransactionID(_PaymentID, ExtTransID);
                
                statslbl.Text = "Updated " + TextBox2.Text;
                TextBox2.Text = "";
                
            }
            catch (Exception ex)
            {
                
                string _ex = ex.InnerException.ToString();
                statslbl.Text = "ERROR!!!!!!!!" + _ex;
            }


        }

        public void InsertMerchantTransactionID(int PaymentID, string ExtID)
        {
            //usp_Transaction_UpdateExternalMerchantTransID
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Transaction_UpdateExternalMerchantTransID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PaymentId", SqlDbType.Int).Value = PaymentID;
            com.Parameters.Add("@ExternalTransactionId", SqlDbType.NVarChar, 50).Value = ExtID;
            try
            {
                con.Open();
                com.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }



        protected void Button6_Click(object sender, EventArgs e)
        {
            //int _id = int.Parse(YardiRenterIDtxt.Text);
            //var Resident = new Renter(_id);
            //string test = "";
            //EfxFramework.Pms.Yardi.ImportRequest.UpdateRenterBalanceDueRealTimeYardi(Resident);
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            //string _time = timetxt.Text;
            //DateTime _newDate = DateTime.Parse(_time);
            //_newDate = _newDate.ToLocalTime();
            //Label3.Text = _newDate.ToString();


        }

        protected void Button8_Click(object sender, EventArgs e)
        {



            HttpWebRequest r = (HttpWebRequest)WebRequest.Create(ImportRequest("Domus","rentpaid","Domus"));
            r.Method = "Get";
            HttpWebResponse res = (HttpWebResponse)r.GetResponse();
            Stream sr = res.GetResponseStream();
            StreamReader sre = new StreamReader(sr);

            string s = sre.ReadToEnd();
            Response.Write(s);
        }

        public string ImportRequest(string UserName, string Password, string portfolio)
        {
            string method = "<EDEX><propertyid>0</propertyid><includemarketingsources>0</includemarketingsources><includeamenities>1</includeamenities><includeunittypes>1</includeunittypes></EDEX>";
            {
                var Sb = new StringBuilder();
                Sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                Sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
                Sb.Append("<soap:Body>");
                Sb.AppendFormat("<{0} xmlns=\"https://amsitest.infor.com/AmsiWeb/edexweb/esite/leasing.asmx\">");
                Sb.AppendFormat("<UserName>{0}</UserName>", UserName);
                Sb.AppendFormat("<Password>{0}</Password>", Password);
                Sb.AppendFormat("<PortfolioName>{0}</PortfolioName>",portfolio);
                Sb.AppendFormat("<XMLData>{0}</XMLData>", method);
                Sb.AppendFormat("   </GetPropertyList></soap:Body></soap:Envelope>");

                return Sb.ToString();
            }
        }

        protected void Button9_Click(object sender, EventArgs e)
        {

            //Remember to use negative numbers to pay the property
            decimal Amount = -764.00m;
            string CCDepositRoutingNumber = "063114289";
            string CCDepositAccountNumber = "3055563";
            string firstName = "Crystal";
            string lastName = "Hollingsworth";
            string streetAddress = "2701 Logan Circle";
            string city = "Nacogdoches";
            string stateId = "TX";
            string postalCode = "36052";
            string phone = "";


            var ACH_Validate =
            EfxFramework.PaymentMethods.Ach.AchService.ProcessAchPayment(Settings.AchUserId, Settings.AchPassword, Settings.CCSettlementAchId, Amount, CCDepositRoutingNumber, CCDepositAccountNumber, "Checking",
                             firstName, lastName, streetAddress, "", city, stateId, postalCode, phone);


            var ACHTransID = ACH_Validate.TransactionId;

            AchTransIDtxt.Text = ACHTransID.ToString();

        }

        //protected void Button10_Click(object sender, EventArgs e)
        //{
        //    int PropID = -10;
        //    var _testDataReader = RPO_DataAccess.ReturnOneRow(Settings.ConnectionString, "dbo.usp_Property_GetSingleObject_v133", 
        //        new[] 
        //        {
        //            new SqlParameter("@PropertyId", PropID) 
        //        }
        //        );

        //    if (_testDataReader != null)
        //    {
        //        _testDataReader.Read();

        //        if (_testDataReader.HasRows)
        //        {
        //            string Test = _testDataReader["PropertyName"].ToString();
        //        }


        //    }
            
        //}


       public int GetLastPaymentForRenter(int renterID)
        {
            //usp_Payment_GetRenterLastPaymentDetail
          
            
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Payment_GetRenterLastPaymentDetail", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@RenterID", SqlDbType.Int).Value = renterID;
            com.Parameters.Add("@Days", SqlDbType.Int).Direction = ParameterDirection.Output;

           
           int _days = 0;
           try
           {
               con.Open();
               com.ExecuteNonQuery();
               _days = (int)com.Parameters["@Days"].Value;
               return _days;

           }
           catch(Exception ex)
           {
               con.Close();
               throw ex;
           }

        }

       protected void Button11_Click(object sender, EventArgs e)
       {
           string _t = TextBox3.Text;

           int t = int.Parse(_t);

           int _i = GetLastPaymentForRenter(t);

           Label5.Text = _i.ToString();

       }



        }
    }
