﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domus.Terms
{
    public partial class Default : EfxFramework.Web.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TermsContent.Text = Facade.HtmlTemplateContent["TermsAndConditions"].ToString();
        }
    }
}