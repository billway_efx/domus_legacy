﻿
var app = angular.module('rpo', ['ngRoute', '$strap.directives', 'transactionServices', 'pagination','paginator', 'API'])
    .config(function ($routeProvider, $locationProvider, $httpProvider) {
    var html5Ok = navigator.appVersion.indexOf('MSIE') == -1;
    $locationProvider.html5Mode(html5Ok).hashPrefix('');
    $routeProvider.
        when('/rpo', { controller: CompaniesController, templateUrl: '../../companies.html', reloadOnSearch: false }).
        when('/rpo/p/:page', { controller: CompaniesController, templateUrl: '../../companies.html', reloadOnSearch: false }).
        when('/rpo/company/:companyId', { controller: CompanyController, templateUrl: '../../company.html' }).
        //when('/rpo/company/:companyId/properties', { controller: PropertiesController, templateUrl: '../../../properties.html', reloadOnSearch: false }).
        //when('/rpo/company/:companyId/properties/p/:page', { controller: PropertiesController, templateUrl: '../../../properties.html' }).
        when('/rpo/admins', { controller: AdminsController, templateUrl: '../../rpo-admins.html', reloadOnSearch: false }).
        when('/rpo/apiusers', { controller: ApiUsersController, templateUrl: '../../rpo-api-users.html', reloadOnSearch: false }).
        when('/rpo/properties', { controller: PropertiesController, templateUrl: '../../properties.html', reloadOnSearch: false }).
        when('/rpo/properties/p/:page', { controller: PropertiesController, templateUrl: '../../properties.html', reloadOnSearch: false }).
        when('/rpo/property/:propertyId', { controller: PropertyController, templateUrl: '../../property.html' }).
        when('/rpo/property/:propertyId/residents', { controller: ResidentsController, templateUrl: '../../../residents.html', reloadOnSearch: false }).
        when('/rpo/property/:propertyId/residents/p/:page', { controller: ResidentsController, templateUrl: '../../residents.html', reloadOnSearch: false }).
        when('/rpo/residents', { controller: ResidentsController, templateUrl: '../../residents.html', reloadOnSearch: false}).
        when('/rpo/staff', { controller: StaffController, templateUrl: '../../rpo-staff.html', reloadOnSearch: false }).
        when('/residents/p/:page', { controller: ResidentsController, templateUrl: '../residents.html', reloadOnSearch: false }).
        //when('/rpo/resident/:residentId', { controller: ResidentController, templateUrl: '../../resident.html' }).
        otherwise({ redirectTo: '/rpo' }); 
});
function CompaniesController($scope, $rootScope, companyService, userService,$location) {
    userService.userIsAdmin().then(function (isAdmin) {
        if (isAdmin == 'false') {
            window.location = '/';
        }
        else {
            $scope.companies = companyService.getAllCompanies();
            $scope.emptyText = "No Companies to display.";
            $rootScope.$broadcast('Companies');
        }
    });

    $scope.$watchCollection('[predicate,reverse]', function (sorting) {
        if (sorting[0] == 'CompanyName' && sorting[1] == false) {
            $scope.sortLabel = 'Company Name (A-Z)';
            $location.search({ orderby: 'CompanyName', direction: 'asc' });
        }
        else if (sorting[0] == 'CompanyName' && sorting[1] == true) {
            $scope.sortLabel = 'Company Name (Z-A)';
            $location.search({ orderby: 'CompanyName', direction: 'desc' });
        }
        else {
            $scope.sortLabel = 'Company Name (A-Z)';
        }
    });
    
}
function CompanyController($scope, $http, $routeParams) {
    $http.get('/api/company/one/' + $routeParams.companyId).success(function (data) {
        $scope.company = data;
    });
}
function PropertiesController($scope, $rootScope, $http, $routeParams, propertyService, companyService, userService, $location, $filter) {
    $scope.params = $routeParams;
    $rootScope.$broadcast('Properties');
    $scope.showProperties = false;
    $scope.companyFromUrl = "";
    $scope.emptyText = "No properties to display.";
    userService.userIsAdmin().then(function (isAdmin) {
        $scope.isRPOAdmin = isAdmin;
        $scope.pageLoaded = false;
        if (isAdmin == 'true') {
             companyService.getAllCompanies().then(function (companies) {
                 $scope.companies = companies;
                 if (isEmpty($location.search().companyid)) {
                 	propertyService.getAllProperties().then(function (properties) {
                 		$scope.properties = properties;
                 	});

                 } else {
                     angular.forEach($scope.companies, function (company, key) {
                         if (company.CompanyId == $location.search().companyid) {
                             $scope.selectedCompany = company;
                         }
                     });
                     propertyService.getCompanyProperties($location.search().companyid).then(function (properties) {
                         $scope.properties = properties;
                         if (properties.length == 0) {
                         	$scope.emptyText = "No Properties have been set up for this Company.";
                         }
                     });
                     
                 }
            });//for dropdown
            
        } else {
            userService.getUserId().then(function (userId) {
                $scope.properties = propertyService.getUserProperties(userId);
                if ($scope.properties.length == 0) {
                    $scope.emptyText = "You currently manage no properties.";
                }
            });
        }
    });
    $scope.filterPropertiesByCompany = function (company) {
        $scope.urlPrefix = navigator.appVersion.indexOf('MSIE') == -1 ? "" : "/rpo#";

        if (!isEmpty(company)) {
            $location.path($scope.urlPrefix + '/rpo/properties').search({ companyid: company.CompanyId });
            propertyService.getCompanyProperties($location.search().companyid).then(function (properties) {
                $scope.properties = properties;
                if (properties.length == 0) {
                    $scope.emptyText = "No Properties exist for this company.";
                }
            });
        }
    };
    $scope.$watchCollection('[predicate,reverse]', function (sorting) {
        if (sorting[0] == 'PropertyName' && sorting[1] == false) {
            $scope.sortLabel = 'Property Name (A-Z)';
            $location.search({ orderby: 'PropertyName', direction: 'asc' });
        }
        else if (sorting[0] == 'PropertyName' && sorting[1] == true) {
            $scope.sortLabel = 'Property Name (Z-A)';
            $location.search({ orderby: 'PropertyName', direction: 'desc' });
        }
        else {
            $scope.sortLabel = 'Property Name (A-Z)';
        }
    });
    $scope.$watch('properties', function () {
        if ($scope.properties) {
            $scope.showProperties = $scope.properties.length > 0;
        }
    });
}
function PropertyController($scope, $http, $routeParams) {
    $http.get('/api/property/one/' + $routeParams.propertyId).success(function (data) {
        $scope.property = data;
    });
}
function ResidentsController($scope, $rootScope, $http, $routeParams, renterService, $location, Paging, userService) {
    $rootScope.$broadcast('Residents');
    $scope.params = $routeParams;
    $scope.q = $location.search().q;
    $scope.p = $location.search().p;
    $scope.orderby = $location.search().orderby;
    $scope.direction = $location.search().direction;
    $scope.filterActive = $location.search().active;
    $scope.isRPOCSR = false;
    $scope.isRPOAdmin = false;

    $scope.$watchCollection('[predicate,reverse]', function (sorting) {
        if (sorting[0] == 'LastName' && sorting[1] == false) {
            $scope.sortLabel = 'Last Name (A-Z)';
            $location.search({
                q: !isEmpty($scope.search) ? $scope.search : '',
                active: !isEmpty($scope.filterActive) ? $scope.filterActive : '',
                orderby: 'LastName',
                direction: 'asc'
            });
        }
        else if (sorting[0] == 'LastName' && sorting[1] == true) {
            $scope.sortLabel = 'Last Name (Z-A)';
            $location.search({
                q: !isEmpty($scope.search) ? $scope.search : '',
                active: !isEmpty($scope.filterActive) ? $scope.filterActive : '',
                orderby: 'LastName',
                direction: 'desc'
            });
        }
        else if (sorting[0] == 'FirstName' && sorting[1] == false) {
            $scope.sortLabel = 'First Name (A-Z)';
            $location.search({
                q: !isEmpty($scope.search) ? $scope.search : '',
                active: !isEmpty($scope.filterActive) ? $scope.filterActive : '',
                orderby: 'FirstName',
                direction: 'asc'
            });
        }
        else if (sorting[0] == 'FirstName' && sorting[1] == true) {
            $scope.sortLabel = 'First Name (Z-A)';
            $location.search({
                q: !isEmpty($scope.search) ? $scope.search : '',
                active: !isEmpty($scope.filterActive) ? $scope.filterActive : '',
                orderby: 'FirstName',
                direction: 'desc'
            });
        }
        else {
            $scope.sortLabel = 'Last Name (A-Z)';
        }
        $scope.orderby = $location.search().orderby;
        $scope.direction = $location.search().direction;
    });
    $scope.$watch('search', function () {
        if (!isEmpty($scope.search)) {
            if (!isEmpty($scope.filterActive)) {
                $location.search({
                    q: $scope.search,
                    active: $scope.filterActive,
                    orderby: !isEmpty($scope.orderby) ? $scope.orderby : '',
                    direction: !isEmpty($scope.direction) ? $scope.direction : ''
                });
            }
            else {
                $location.search({
                    q: $scope.search,
                    orderby: !isEmpty($scope.orderby) ? $scope.orderby : '',
                    direction: !isEmpty($scope.direction) ? $scope.direction : ''
                });
            }
        }
        else {
            if (!isEmpty($scope.filterActive)) {
                $location.search({
                    active: $scope.filterActive,
                    orderby: !isEmpty($scope.orderby) ? $scope.orderby : '',
                    direction: !isEmpty($scope.direction) ? $scope.direction : ''
                });
            }
            else if (isEmpty($scope.orderby) &&
                isEmpty($scope.direction) &&
                isEmpty($scope.search) &&
                isEmpty($scope.filterActive)) {
                $location.search('');
            }
        }
    });
    $scope.$watch('filterActive', function () {
        if (!isEmpty($scope.filterActive)) {
            if (!isEmpty($scope.search)) {
                $location.search({
                    q: $scope.search,
                    active: $scope.filterActive,
                    orderby: !isEmpty($scope.orderby) ? $scope.orderby : '',
                    direction: !isEmpty($scope.direction) ? $scope.direction : ''
                });
            }
            else {
                $location.search({
                    active: $scope.filterActive,
                    orderby: !isEmpty($scope.orderby) ? $scope.orderby : '',
                    direction: !isEmpty($scope.direction) ? $scope.direction : ''
                });
            }

        }
        else {

            if (!isEmpty($scope.search)) {
                $location.search({
                    q: $scope.search,
                    orderby: !isEmpty($scope.orderby) ? $scope.orderby : '',
                    direction: !isEmpty($scope.direction) ? $scope.direction : ''
                });
            }
            else  if(isEmpty($scope.orderby) && 
                isEmpty($scope.direction) &&
                isEmpty($scope.search) &&
                isEmpty($scope.filterActive)) {
                $location.search('');
            }
        }
    });
    

    userService.userIsCSR().then(function (isCSR) {
        $scope.isRPOCSR = isCSR;

        if ($scope.isRPOCSR=='true')
        {
            $scope.isRPOAdmin = 'false';
        }
    });

    userService.userIsAdmin().then(function (isAdmin) {
        $scope.isRPOAdmin = isAdmin;

        if ($scope.isRPOAdmin=='true')
        {
            $scope.isRPOCSR = 'false';
        }
    });


    $scope.getRenters = function () {
        var search = { q: $location.search().q, p: $location.search().p, o: $location.search().orderby, d: $location.search().direction, pid: $routeParams.propertyId, active: $location.search().active };
        renterService.getAllRenters(search).then(
            function (renters) {
                $scope.renters = renters.data;
                Paging.setItemCount(renters.itemCount);
                Paging.setRowsPerPage(10);
                $scope.emptyText = "No Residents to display.";
            });
    }
    $scope.getRenters();
    $scope.$on('$routeUpdate', function () {
        $scope.q = $location.search().q;
        $scope.p = $location.search().p;
        $scope.filterActive = $location.search().active;
        $scope.getRenters();
    });

    $scope.toggleActive = function (renter) {
        renterService.toggleActive(renter).then(
            function (response) {
                if (response == 'true') {
                    alert(renter.FirstName + ' ' + renter.LastName + ' is now ' + (renter.IsActive ? 'deactivated.' : 'active.'));
                    $scope.getRenters();
                } else {
                    alert('Error toggling ' + renter.FirstName + ' ' + renter.LastName + '. Please try again or contact an RPO Admin if this persists.');
                }
            });
    };
}
function StaffController($scope, $rootScope, $http, $routeParams, userService, staffService, $location, Paging) {
    $rootScope.$broadcast('Staff');
    $scope.params = $routeParams;
    $scope.q = $location.search().q;
    $scope.p = $location.search().p;
    $scope.orderby = $location.search().orderby;
    $scope.direction = $location.search().direction;
    $scope.$watchCollection('[predicate,reverse]', function (sorting) {
        if (sorting[0] == 'LastName' && sorting[1] == false) {
            $scope.sortLabel = 'Last Name (A-Z)';
            $location.search({ orderby: 'LastName', direction: 'asc' });
        }
        else if (sorting[0] == 'LastName' && sorting[1] == true) {
            $scope.sortLabel = 'Last Name (Z-A)';
            $location.search({ orderby: 'LastName', direction: 'desc' });
        }
        else if (sorting[0] == 'FirstName' && sorting[1] == false) {
            $scope.sortLabel = 'First Name (A-Z)';
            $location.search({ orderby: 'FirstName', direction: 'asc' });
        }
        else if (sorting[0] == 'FirstName' && sorting[1] == true) {
            $scope.sortLabel = 'First Name (Z-A)';
            $location.search({ orderby: 'FirstName', direction: 'desc' });
        }
        else {
            $scope.sortLabel = 'Last Name (A-Z)';
        }
        $scope.orderby = $location.search().orderby;
        $scope.direction = $location.search().direction;
    });
    $scope.$watch('search', function () {
        if (!isEmpty($scope.search)) {
            $location.search({ q: $scope.search });
        }
        else {
            $location.search('');
        }
    });
    $scope.getStaff = function () {
        var search = { q: $scope.q, p: $scope.p, o: $scope.orderby, d: $scope.direction };
        userService.getStaff(search).then(
            function (staff) {
                $scope.users = staff.data;
                Paging.setItemCount(staff.itemCount);
                Paging.setRowsPerPage(10);
                $scope.emptyText = "No Property Managers to display.";
            });
    };
    $scope.getStaff();

    $scope.$on('$routeUpdate', function () {
        $scope.q = $location.search().q;
        $scope.p = $location.search().p;
        $scope.getStaff();
    });
    $scope.deleteStaff = function (staffToDelete) {
        if (confirm('Are you sure you want to delete this Property Manager?')) {
            var index = $scope.users.indexOf(staffToDelete);
            staffService.deleteStaff(staffToDelete).then(
               function (deleted) {
                   //CMallory - Task 00543 - Changed If/Else logic to display different error messages based on the error.
                   if (deleted==1) {
                       alert(staffToDelete.FirstName + ' ' + staffToDelete.LastName + ' was successfully deleted.');
                       $scope.getStaff();
                   }
                   else if(deleted==2){
                       alert('Your sessions has expired.  Try logging out and repeating the procedure.');
                       $scope.getStaff();
                   }
                   else {
                        alert('Error deleting ' + staffToDelete.FirstName + ' ' + staffToDelete.LastName + '. Please try again or contact an RPO Admin if this persists.');
                    } 
                //function (deleted) {
                //    if (deleted == 'true') {
                //        alert(staffToDelete.FirstName + ' ' + staffToDelete.LastName + ' was successfully deleted.');
                //        $scope.getStaff();
                //    } else {
                //        alert('Error deleting ' + staffToDelete.FirstName + ' ' + staffToDelete.LastName + '. Please try again or contact an RPO Admin if this persists.');
                //    }
                });

        }
    };

}
function AdminsController($scope, $rootScope, $http, $routeParams, userService, $location, Paging) {
    $rootScope.$broadcast('Admins');
    $scope.params = $routeParams;
    $scope.q = $location.search().q;
    $scope.p = $location.search().p;
    $scope.orderby = $location.search().orderby;
    $scope.direction = $location.search().direction;
    $scope.$watchCollection('[predicate,reverse]', function (sorting) {
        if (sorting[0] == 'LastName' && sorting[1] == false) {
            $scope.sortLabel = 'Last Name (A-Z)';
            $location.search({ orderby: 'LastName', direction: 'asc' });
        }
        else if (sorting[0] == 'LastName' && sorting[1] == true) {
            $scope.sortLabel = 'Last Name (Z-A)';
            $location.search({ orderby: 'LastName', direction: 'desc' });
        }
        else if (sorting[0] == 'FirstName' && sorting[1] == false) {
            $scope.sortLabel = 'First Name (A-Z)';
            $location.search({ orderby: 'FirstName', direction: 'asc' });
        }
        else if (sorting[0] == 'FirstName' && sorting[1] == true) {
            $scope.sortLabel = 'First Name (Z-A)';
            $location.search({ orderby: 'FirstName', direction: 'desc' });
        }
        else {
            $scope.sortLabel = 'Last Name (A-Z)';
        }
        $scope.orderby = $location.search().orderby;
        $scope.direction = $location.search().direction;
    });
    $scope.$watch('search', function () {
        if (!isEmpty($scope.search)) {
            $location.search({ q: $scope.search });
        }
        else {
            $location.search('');
        }
    });
    $scope.getAdmins = function () {
        var search = { q: $scope.q, p: $scope.p, o: $scope.orderby, d: $scope.direction };
        userService.getAdmins(search).then(
        function (admins) {
            $scope.users = admins.data;
            Paging.setItemCount(admins.itemCount);
            Paging.setRowsPerPage(10);
            $scope.emptyText = "No RPO Admins to display.";

        });
    };
    $scope.getAdmins();

    $scope.$on('$routeUpdate', function () {
        $scope.q = $location.search().q;
        $scope.p = $location.search().p;
        $scope.getAdmins();
    });

    $scope.deleteAdmin = function (adminToDelete) {
        if (confirm('Are you sure you want to delete this RPO Admin?')) {
            var index = $scope.users.indexOf(adminToDelete);
            userService.deleteAdmin(adminToDelete).then(
                function (deleted) {
                    if (deleted == 'true') {
                        alert(adminToDelete.FirstName + ' ' + adminToDelete.LastName + ' was successfully deleted.');
                        $scope.getAdmins();
                    }
                    else {
                        alert('Error deleting ' + adminToDelete.FirstName + ' ' + adminToDelete.LastName + '. Please try again or contact an RPO Admin if this persists.');
                    }
                });

        }
    };
}
function ApiUsersController($scope, $rootScope, $http, $routeParams, userService, $location, Paging) {
    $rootScope.$broadcast('APIUsers');
    $scope.params = $routeParams;
    $scope.q = $location.search().q;
    $scope.p = $location.search().p;
    $scope.orderby = $location.search().orderby;
    $scope.direction = $location.search().direction;

    $scope.getApiUsers = function () {
        var search = { q: $scope.q, p: $scope.p, o: $scope.orderby, d: $scope.direction };
        userService.getApiUsers(search).then(
        function (apiUsers) {
            $scope.users = apiUsers.data;
            Paging.setItemCount(apiUsers.itemCount);
            Paging.setRowsPerPage(10);
            $scope.emptyText = "No API Users to display.";
        });
    };
    $scope.getApiUsers();

    $scope.$on('$routeUpdate', function () {
        $scope.q = $location.search().q;
        $scope.p = $location.search().p;
        $scope.getApiUsers();
    });

    $scope.deleteApiUser = function (apiUserToDelete) {
        if (confirm('Are you sure you want to delete this API User?')) {
            userService.deleteAdmin(apiUserToDelete).then(
                function (deleted) {
                    if (deleted == 'true') {
                        alert(apiUserToDelete.FirstName + ' ' + apiUserToDelete.LastName + ' was successfully deleted.');
                        $scope.getApiUsers();
                    } else {
                        alert('Error deleting ' + apiUserToDelete.FirstName + ' ' + apiUserToDelete.LastName + '. Please try again or contact an RPO Admin if this persists.');
                    }
                });
        }
    };

    $scope.toggleActive = function (apiUser) {
        var index = $scope.users.indexOf(apiUser);
        userService.toggleApiUser(apiUser).then(
            function (response) {
                if (response == 'true') {
                    alert(apiUser.Username + ' is now ' + (apiUser.IsActive ? 'deactivated.' : 'active.'));
                    $scope.getApiUsers();
                } else {
                    alert('Error toggling ' + apiUser.Username + '. Please try again or contact an RPO Admin if this persists.');
                }
            });
    };
}
function TitleController($scope, $rootScope) {
    $rootScope.$on('Residents', function () {
        $scope.PageTitle = 'Residents';
    });
    $rootScope.$on('Users', function () {
        $scope.PageTitle = 'Users';
    });
    $rootScope.$on('Properties', function () {
        $scope.PageTitle = 'Properties';
    });
    $rootScope.$on('Companies', function () {
        $scope.PageTitle = 'Companies';
    });
    $rootScope.$on('Admins', function () {
        $scope.PageTitle = 'RPO Administrators';
    });
    $rootScope.$on('Staff', function () {
        $scope.PageTitle = 'Property Managers';
    });
    $rootScope.$on('APIUsers', function () {
        $scope.PageTitle = 'API Users';
    });
}

angular.bootstrap($('#app'), ["rpo"]);

