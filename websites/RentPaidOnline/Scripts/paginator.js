﻿angular.module('pagination', [])
    .filter('paginate', function (Paginator,$location,$filter) {
        return function(input, rowsPerPage) {
            if (!input) {
                return input;
            }

            if (rowsPerPage) {
                Paginator.rowsPerPage = rowsPerPage;
            } 

            Paginator.setItemCount(input.length);
            //Paginator.setItemCount(500);
            //handle sorting here
             var orderBy = ($location.search().direction == 'desc' ? '-' : '+') + $location.search().orderby;
             if($location.search().orderby) {
                console.log('order by: ' + orderBy);
                input = $filter('orderBy')(input, orderBy);
             }
            //Salcedo - 4/30/2014 - fix problem with 11 rows per page, instead of 10
             //return input.slice(parseInt(Paginator.page * Paginator.rowsPerPage), parseInt((Paginator.page + 1) * Paginator.rowsPerPage + 1));
             return input.slice(parseInt(Paginator.page * Paginator.rowsPerPage), parseInt((Paginator.page + 1) * Paginator.rowsPerPage));
        };
    })

    .filter('forLoop', function () {
        return function(input, start, end) {
            input = new Array(end - start);
            for (var i = 0; start < end; start++, i++) {
                input[i] = start;
            }
            return input;
        };
    })

    .service('Paginator', function ($rootScope, $location) {
        this.page = ($location.search()).p == null ? 0 : ($location.search()).p-1;
        //cakel Updated rowsperpage from 5 to 10
        //BugID 00018
        this.rowsPerPage = 10;
        this.itemCount = 0;
        this.startPageIndex = 0;
        this.endPageIndex = 5;
        this.setPage = function (page) {
            if (page > this.pageCount()) {
                return;
            }

            this.page = page;
            this.updateUrl();
            this.updatePageRange();
        };

        this.nextPage = function () {
            if (this.isLastPage()) {
                return;
            }

            this.page++;
            this.updateUrl();
            this.updatePageRange();
        };

        this.previousPage = function () {
            if (this.isFirstPage()) {
                return;
            }

            this.page--;
            this.updateUrl();
            this.updatePageRange();
        };

        this.firstPage = function () {
            this.page = 0;
            this.updateUrl();
            this.updatePageRange();
        };

        this.lastPage = function () {
            this.page = this.pageCount() - 1;
            this.updateUrl();
            this.updatePageRange();
        };

        this.isFirstPage = function () {
            return this.page == 0;
        };

        this.isLastPage = function () {
            return this.page == this.pageCount() - 1;
        };

        this.pageCount = function () {
            return Math.ceil(parseInt(this.itemCount) / parseInt(this.rowsPerPage));
        };
        this.startPage = function () {
            return this.startPageIndex;
        };
        this.endPage = function() {
            return this.endPageIndex;
        };
        this.updatePageRange = function () {
            if (this.page > 0) {
                //simplify page calculation
                if (this.page <= this.pageCount()) {
                    this.startPageIndex = this.page - 2;
                    this.endPageIndex = this.page + 3;

                    if (this.startPageIndex < 0) {
                        this.endPageIndex = this.endPageIndex + (0 - this.startPageIndex);
                        this.startPageIndex = 0;
                    }
                    if (this.endPageIndex > this.pageCount()) {
                        this.startPageIndex = this.startPageIndex - (this.endPageIndex - this.pageCount());
                        if (this.startPageIndex < 0) {
                            this.startPageIndex = 0;
                        }
                        this.endPageIndex = this.pageCount();
                    }
                }
            } else {
                this.startPageIndex = 0;
                this.endPageIndex = this.pageCount() > 5 ? 5 : this.pageCount();
            }

        };


        // cakel: BugID00018 - Updated code to accept query into url
        //Code affects Search residents page when changing pages
        this.updateUrl = function () {
            if ($location.search().orderby) {
                $location.search({ orderby: $location.search().orderby, direction: $location.search().direction, p: this.page + 1 });
            }
            else {
                $location.search({ p: this.page + 1 });
            }
        };

 
        this.setItemCount = function (itemCount) {
            this.itemCount = itemCount;
            this.updatePageRange();
        };
    })
    .directive('paginator', function factory() {
        return {
            restrict: 'E',
            controller: function ($scope, Paginator) {
                $scope.paginator = Paginator;
            },
            templateUrl: '/paginator.html'
        };
    });