﻿var calendarApp = angular.module('rpoCalendar', ['ngRoute', 'pagination', 'API', '$strap.directives', 'ui.calendar', 'ui.tinymce', 'ngSanitize']).config(function ($routeProvider, $locationProvider, $httpProvider) {
    var html5Ok = navigator.appVersion.indexOf('MSIE') == -1;
    $locationProvider.html5Mode(html5Ok).hashPrefix('');
    $routeProvider.
        when('/rpo/events/calendar', { controller: CalendarController, templateUrl: '../../event-calendar.html', reloadOnSearch: false }).
        when('/rpo/events', { controller: EventListController, templateUrl: '../../event-list.html', reloadOnSearch: false }).
        when('/rpo/event/details/:id', { controller: EventDetailsController, templateUrl: '../../../event-details.html', reloadOnSearch: false }).
        when('/rpo/event/create', { controller: EventCreateController, templateUrl: '../../../event-create.html', reloadOnSearch: false }).
        when('/rpo/event/edit/:id', { controller: EventEditController, templateUrl: '../../../event-edit.html', reloadOnSearch: false }).
        otherwise({ redirectTo: '/rpo/events' });
});

// calendar app
calendarApp.value('$strapConfig', {
    datepicker: {
        language: 'en',
        format: 'mm/dd/yy'
    }
});

// filter
calendarApp.filter('show', function () {
    return function (input, mode) {
        var out = [];
        
        if (!isEmpty(input)) {
            
            for (var i = 0; i < input.length; i++) {
                var start = new Date(Date.parse(input[i].start));
                if ( start >= new Date() && mode == 'future') {
                    out.push(input[i]);
                }
                else if (start < new Date() && mode == 'archived') {
                    out.push(input[i]);
                }
            }
        }
        return out;
    };
});

// calendar controller
function CalendarController($rootScope, $scope, calendarEventService, userService) {
    $scope.urlPrefix = navigator.appVersion.indexOf('MSIE') == -1 ? "" : "/rpo/events#";
    $rootScope.$broadcast('Calendar');
    $scope.uiConfig = {
        calendar: {
            height: 500,
            header: {
                left: 'month agendaWeek agendaDay',
                center: 'title',
                right: 'today prev,next'
            },
            dayClick: function () {
            },
            eventClick: function(event) {
                if (event.id) {
                    calendarEventService.showEvent(event.id);
                    $rootScope.$broadcast('Event:Show');
                    return false;
                }
            },
            editable: false,
            allDayDefault: false
        }
    };
    var events = calendarEventService.getEvents();
    $scope.calendarEvents = {events: events};
    userService.userIsAdmin().then(function (isAdmin) {
        $scope.isRPOAdmin = isAdmin;

        userService.getUserId().then(function (userId) {
            $scope.userId = userId;
        });
    });
}

// event create controller
function EventCreateController($scope, $http, $rootScope, $filter, $modal, $q, $timeout, calendarEventService, propertyService, userService) {
    var ie = navigator.appVersion.indexOf('MSIE') != -1;
    $scope.urlPrefix = ie ? "/rpo/events#" : "";
    $rootScope.$broadcast('Create');
    $scope.submitButtonText = 'Create Event';
    //$rootScope.$on('event:create', function () {
    $scope.startDate = { date: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss Z') };
    $scope.endDate = { date: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss Z') };
    $scope.startTime = { time: "" };
    $scope.endTime = { time: "" };
    $scope.active = true;
    //CMallory - Task 0079 - Added recurring and residentsNofieid
    $scope.recurring = false;
    $scope.residentsNotified = false;
    userService.userIsAdmin().then(function (isAdmin) {

        $scope.isRPOAdmin = isAdmin;
        userService.getUserId().then(function (userId) {

            $scope.userId = userId;
            // Patrick Whittingham - 7/29/15 - josewhishlist 3a
            if (isAdmin == 'true' ) {
                $scope.properties = propertyService.getAllPropertiesForDropdown();
            }
            else {
                propertyService.getUserPropertiesForDropdown(userId).then(
                function (properties) {
                    $scope.properties = properties;
                    userService.userIsCorporateAdmin().then(function (isCorporateAdmin) {
                        $scope.isCorporateAdmin = isCorporateAdmin;
                        if (isCorporateAdmin != 'true') {
                            $scope.properties = $scope.properties.splice(1, 1);
                        }
                    });
                });
            }
        });
    });

    //
    $scope.submit = function () {
        $scope.submitDisabled = true;
        $scope.submitButtonText = 'Creating Event...';
        var start = Date.parse($scope.startDate.date) == null ? $scope.startDate.date : Date.parse($scope.startDate.date);
        var end = Date.parse($scope.endDate.date) == null ? $scope.endDate.date : Date.parse($scope.endDate.date);
        var localOffset = start.getTimezoneOffset();
        start.setMinutes(start.getMinutes() + localOffset);
        end.setMinutes(end.getMinutes() + localOffset);
        var startDate = $filter('date')(start, 'yyyy-MM-dd') + ' ' + $scope.startTime.time + ' Z';
        var endDate = $filter('date')(end, 'yyyy-MM-dd') + ' ' + $scope.endTime.time + ' Z';
        var allProperties = $scope.selectedProperty[0].PropertyName == "All Properties";
        //CMallory - Task 0079 - Added recurring and residentsNofieid
        var recurring = $scope.recurring;
        var residentsNotified = $scope.ResidentsNotified;

        // Patrick Whittingham - 7/30/15 - josewhishlist 3a 
        $scope.propertyArray = [];
        //console.log("e==" + $scope.properties );
        if (allProperties == false) {
            $scope.propertyArray = $scope.selectedProperty;
            //console.log("e1===" + $scope.propertyArray + "====e2===" + $scope.selectedProperty );
        }
        var vLen = 1;
        if ( $scope.propertyArray.length > 0 ) {
            vLen = $scope.propertyArray.length;
        }
        for (var i = 0; i < vLen; i++) {
            //CMallory - Task 0079 - Added recurring and residentsNofieid
            var calendarEvent = { Title: $scope.title, Location: $scope.location, Details: $scope.details, CalendarEventTypeId: null, StartDate: startDate, EndDate: endDate, Contact: $scope.contact, Active: $scope.active, Deleted: false, PropertyId: allProperties == false ? $scope.propertyArray[i].PropertyId : null, AllProperties: allProperties, allProperties: allProperties, Recurring: recurring, ResidentsNotified: residentsNotified };

            calendarEventService.saveNewEvent(calendarEvent).then(function (response) {
                //show success msg and close modal
                if (response == 'true') {
                    $scope.success = true;
                    $scope.submitButtonText = "Event Created";
                    $timeout(function () {
                        $scope.success = false;
                        $scope.title = '';
                        $scope.location = '';
                        $scope.details = '';
                        $scope.contact = '';
                        $scope.selectedProperty = null;
                        $scope.submitButtonText = "Create Event";
                        $scope.submitDisabled = false;
                    }, 2000);

                }
            });
        } // for_i
    };
}

// event details
function EventDetailsController($rootScope, $scope, $sanitize, calendarEventService) {
    $scope.urlPrefix = navigator.appVersion.indexOf('MSIE') == -1 ? "" : "/rpo/events#";
    $rootScope.$on('Event:Show', function () {
        eventId = calendarEventService.getEventId();
        if (eventId) {
            calendarEventService.getEvent(eventId).then(function (event) {
                $scope.event = event;
                $scope.eventDetails = $sanitize(event.Details);
                $('#eventDetails').modal('show');
            });
        }
    });
}

// event EDIT event
function EventEditController($rootScope, $scope, $routeParams, $filter, $timeout, calendarEventService, userService, propertyService) {
    var ie = navigator.appVersion.indexOf('MSIE') != -1;
    $scope.urlPrefix = ie ? "/rpo/events#" : "";
    $rootScope.$broadcast('Edit');
    $scope.submitButtonText = 'Save Event';
    //setup TineMCE
    $scope.tinymceOptions = {
        handle_event_callback: function (e) {
            // put logic here for keypress
        },
        plugins: "code,fullscreen,hr,link,anchor,insertdatetime,table",
    };

    //get EDIT event object
    calendarEventService.editEvent($routeParams.id).then(function (event) {
        $scope.event = event;
        var startDate = new Date(Date.parse($scope.event.StartDate));
        var endDate = new Date(Date.parse($scope.event.EndDate));
        $('#startTime').timepicker('setTime', $filter('date')(startDate, 'h:mm a'));
        $('#endTime').timepicker('setTime', $filter('date')(endDate, 'h:mm a'));
        var localOffset = startDate.getTimezoneOffset();
        startDate.setMinutes(startDate.getMinutes() - localOffset);
        endDate.setMinutes(endDate.getMinutes() - localOffset);
        $scope.startDate = { date: startDate };
        $scope.endDate = { date: endDate };

        //
        userService.userIsAdmin().then(function (isAdmin) {
            // Patrick Whittingham - 7/30/15 - josewhishlist 3a 
            $scope.isRPOAdmin = isAdmin;
            userService.getUserId().then(function (userId) {
                $scope.userId = userId;
                var propertiesPromise = isAdmin == 'true' ? propertyService.getAllPropertiesForDropdown() : propertyService.getUserPropertiesForDropdown(userId);
                propertiesPromise.then(function (properties) {
                    $scope.properties = properties;
                    angular.forEach($scope.properties, function (property, key) {
                        //console.log("each==prop=="+ property + "===key==" + key );
                        if (property.PropertyId == $scope.event.PropertyId) {
                           //console.log("e51===" + property.PropertyId + "===e52==" + $scope.event.PropertyId + "==obj==" + property );
                            $scope.selectedProperty = property;
                        }
                        else if ($scope.event.AllProperties == true && property.PropertyId == 0) {
                            $scope.selectedProperty = property;
                        }
                    });
                });

            });
        });

    });

    $scope.submit = function () {
        $scope.submitDisabled = true;
        $scope.submitButtonText = 'Saving Event...';

        var localOffset = $scope.startDate.date.getTimezoneOffset();
        $scope.startDate.date.setMinutes($scope.startDate.date.getMinutes() + localOffset);
        $scope.endDate.date.setMinutes($scope.endDate.date.getMinutes() + localOffset);
        //console.log("len==" + $scope.selectedProperty.length );
        $scope.event.StartDate = $filter('date')($scope.startDate.date, 'yyyy-MM-dd') + ' ' + $scope.startTime.time + ' Z';
        $scope.event.EndDate = $filter('date')($scope.endDate.date, 'yyyy-MM-dd') + ' ' + $scope.endTime.time + ' Z';
        $scope.event.AllProperties = $scope.selectedProperty.PropertyName == "All Properties"; 
        if ($scope.event.AllProperties == false) {
            // Patrick Whittingham - 7/30/15 - josewhishlist 3a 
            $scope.propertyArray = [];
            //console.log("e==" + $scope.properties);
            $scope.event.propertyArray = $scope.selectedProperty;
            $scope.event.PropertyId = $scope.selectedProperty.PropertyId;
            //console.log("e1===" + $scope.propertyArray + "====e2===" + $scope.event.PropertyId );
        }
        else {
            $scope.event.PropertyId = null;
        }
        calendarEventService.saveExistingEvent($scope.event).then(function (response) {
            //show success msg and close modal
            if (response == 'true') {
                $scope.success = true;
                $scope.submitDisabled = false;
                $scope.submitButtonText = "Event Saved";
                $timeout(function () {
                    $scope.success = false;
                    $scope.submitButtonText = "Save Event";
                }, 3000);
            }
        });
    };
}

// event list
function EventListController($rootScope, $scope, $filter, calendarEventService, userService) {
    $scope.urlPrefix = navigator.appVersion.indexOf('MSIE') == -1 ? "" : "/rpo/events#";
    $rootScope.$broadcast('List');
    $scope.mode = 'future';
    $scope.order = false;
    calendarEventService.getEvents().then(function (events) {
        $scope.allEvents = events.events;
        $scope.events = $filter('show')(events.events, $scope.mode);
    });
    $scope.showEvent = function (eventId) {
        calendarEventService.showEvent(eventId);
        $rootScope.$broadcast('Event:Show');
    };
    $scope.createEvent = function () {
        calendarEventService.createEvent();
        $('#eventCreate').modal('show');
    };
    $scope.deleteEvent = function (eventToDelete) {
        if (confirm('Are you sure you want to delete this event?')) {
            var index = $scope.allEvents.indexOf(eventToDelete);
            calendarEventService.deleteEvent(eventToDelete.id).then(
                function (deleted) {
                    if (deleted == 'true') {
                        // Collier $scope.allEvents.splice(index, 1);
                        calendarEventService.getEvents().then(function (events) {
                            $scope.allEvents = events.events;
                            $scope.events = $filter('show')(events.events, $scope.mode);
                        });
                    }
                });
            
        }
    };
    $scope.filterEvents = function (mode) {
        $scope.mode = mode;
        $scope.events = $filter('show')($scope.allEvents, $scope.mode);
        $scope.order = mode == 'archived';
    }
    userService.userIsAdmin().then(function (isAdmin) {
        $scope.isRPOAdmin = isAdmin;
        userService.getUserId().then(function (userId) {
            $scope.userId = userId;
        });
    });
}

// title controller
function TitleController($scope, $rootScope) {
    $rootScope.$on('Calendar', function () {
        $scope.PageTitle = 'Event Calendar';
    });
    $rootScope.$on('List', function () {
        $scope.PageTitle = 'Event List';
    });
    $rootScope.$on('Create', function () {
        $scope.PageTitle = 'Create Calendar Event';
    });
    $rootScope.$on('Edit', function () {
        $scope.PageTitle = 'Edit Calendar Event';
    });
    $rootScope.$on('Details', function () {
        $scope.PageTitle = 'Event Details';
    });
}
angular.bootstrap($('#app'), ["rpoCalendar"]);