﻿//API to make service calls to WebAPI
angular.module('ReportAPI', [])
    .factory('reportFactory', function ($rootScope, $http, $q, $filter) {
        var currentReportName = null;

        //Salcedo - 4/19/2014 - added pageNumber to function
        var autoPaymentData = function (startDate,endDate,propertyIds,pageNumber) {
            var deferred = $q.defer();

            //Salcedo - 4/19/2014 - added pageNumber to structure
            var propertyIdList = { StartDate: startDate, EndDate: endDate, PropertyIdList: propertyIds, PropertyStaffId: null, TransactionId: null, FlattenData: 'false', PageNumber: pageNumber };

            //Adds a report loader gif to tell user report is being prepared
            document.getElementById('ReportLoader').innerHTML = "<img src='/Images/report_wait.gif' style='margin-top: 60px'/>";

            $http.post('/api/report/autopayment', propertyIdList).success(function (data) {
                deferred.resolve(data);

                //Hides report loader after report displays on screen
                document.getElementById('ReportLoader').innerHTML = "";

            });
            return deferred.promise;
        };

        var applicantPortalPaymentData = function (startDate, endDate, propertyIds) {
            var deferred = $q.defer();
            var propertyIdList = { StartDate: startDate, EndDate: endDate, PropertyIdList: propertyIds, PropertyStaffId: null, TransactionId: null, FlattenData: 'false' };

            //Adds a report loader gif to tell user report is being prepared
            document.getElementById('ReportLoader').innerHTML = "<img src='/Images/report_wait.gif' style='margin-top: 60px'/>";

            $http.post('/api/report/applicantpayment', propertyIdList).success(function (data) {
                deferred.resolve(data);

                //Hides report loader after report displays on screen
                document.getElementById('ReportLoader').innerHTML = "";

            });
            return deferred.promise;
        };

        var billingData = function (propertyIds) {
            var deferred = $q.defer();
            var propertyIdList = { StartDate: null, EndDate: null, PropertyIdList: propertyIds, PropertyStaffId: null, TransactionId: null, FlattenData: 'false' };

            //Adds a report loader gif to tell user report is being prepared
            document.getElementById('ReportLoader').innerHTML = "<img src='/Images/report_wait.gif' style='margin-top: 60px'/>";

            $http.post('/api/report/billing', propertyIdList).success(function (data) {
                deferred.resolve(data);

                //Hides report loader after report displays on screen
                document.getElementById('ReportLoader').innerHTML = "";

            });
            return deferred.promise;
        };

        var charityData = function (startDate, endDate, propertyIds) {
            var deferred = $q.defer();
            var propertyIdList = { StartDate: startDate, EndDate: endDate, PropertyIdList: propertyIds, PropertyStaffId: null, TransactionId: null, FlattenData: 'false' };

            //Adds a report loader gif to tell user report is being prepared
            document.getElementById('ReportLoader').innerHTML = "<img src='/Images/report_wait.gif' style='margin-top: 60px'/>";

            $http.post('/api/report/charity', propertyIdList).success(function (data) {
                deferred.resolve(data);

                //Hides report loader after report displays on screen
                document.getElementById('ReportLoader').innerHTML = "";

            });
            return deferred.promise;
        };

        var detailDepositData = function (startDate, endDate, propertyIds) {
            var deferred = $q.defer();
            var propertyIdList = { StartDate: startDate, EndDate: endDate, PropertyIdList: propertyIds, PropertyStaffId: null, TransactionId: null, FlattenData: 'false' };

            //Adds a report loader gif to tell user report is being prepared
            document.getElementById('ReportLoader').innerHTML = "<img src='/Images/report_wait.gif' style='margin-top: 60px'/>";

            $http.post('/api/report/detaileddeposit', propertyIdList).success(function (data) {
                deferred.resolve(data);

                //Hides report loader after report displays on screen
                document.getElementById('ReportLoader').innerHTML = "";

            });
            return deferred.promise;
        };

        var allPaymentTypesData = function () {
            var deferred = $q.defer();
            $http.get('/api/report/allpaymenttypes').success(function (data) {
                deferred.resolve(data);
            });
            return deferred.promise;
        };

        var allPropertiesData = function () {
            var deferred = $q.defer();
            $http.get('/api/report/allproperties').success(function (data) {
                deferred.resolve(data);
            });
            return deferred.promise;
        };

        var allRentersData = function () {
            var deferred = $q.defer();
            $http.get('/api/report/allrenters').success(function (data) {
                deferred.resolve(data);
            });
            return deferred.promise;
        };

        var propertiesByStaffIdData = function (userId) {
            var deferred = $q.defer();
            var propertyIdList = { StartDate: null, EndDate: null, PropertyIdList: null, PropertyStaffId: userId, TransactionId: null, FlattenData: 'false' };

            //Adds a report loader gif to tell user report is being prepared
            document.getElementById('ReportLoader').innerHTML = "<img src='/Images/report_wait.gif' style='margin-top: 60px'/>";

            $http.post('/api/report/propertiesbystaffid', propertyIdList).success(function (data) {
                deferred.resolve(data);

                //Hides report loader after report displays on screen
                document.getElementById('ReportLoader').innerHTML = "";

            });
            return deferred.promise;
        };

        var rentersByPropertyIdData = function (propertyId) {
            var deferred = $q.defer();
            var propertyIdList = { StartDate: null, EndDate: null, PropertyIdList: propertyIds, PropertyStaffId: null, TransactionId: null, FlattenData: 'false' };

            //Adds a report loader gif to tell user report is being prepared
            document.getElementById('ReportLoader').innerHTML = "<img src='/Images/report_wait.gif' style='margin-top: 60px'/>";

            $http.post('/api/report/rentersbypropertyid', propertyIdList).success(function (data) {
                deferred.resolve(data);

                //Hides report loader after report displays on screen
                document.getElementById('ReportLoader').innerHTML = "";

            });
            return deferred.promise;
        };

        var leadData = function (startDate, endDate) {
            var deferred = $q.defer();
            var propertyIdList = { StartDate: startDate, EndDate: endDate, PropertyIdList: null, PropertyStaffId: null, TransactionId: null, FlattenData: 'false' };

            //Adds a report loader gif to tell user report is being prepared
            document.getElementById('ReportLoader').innerHTML = "<img src='/Images/report_wait.gif' style='margin-top: 60px'/>";

            $http.post('/api/report/lead', propertyIdList).success(function (data) {
                deferred.resolve(data);

                //Hides report loader after report displays on screen
                document.getElementById('ReportLoader').innerHTML = "";

            });
            return deferred.promise;
        };

        var paymentExportSummaryData = function (startDate, endDate, propertyIds) {
            var deferred = $q.defer();
            var propertyIdList = { StartDate: startDate, EndDate: endDate, PropertyIdList: propertyIds, PropertyStaffId: null, TransactionId: null, FlattenData: 'false' };

            //Adds a report loader gif to tell user report is being prepared
            document.getElementById('ReportLoader').innerHTML = "<img src='/Images/report_wait.gif' style='margin-top: 60px'/>";

            $http.post('/api/report/paymentexportsummary', propertyIdList).success(function (data) {
                deferred.resolve(data);

                //Hides report loader after report displays on screen
                document.getElementById('ReportLoader').innerHTML = "";

            });
            return deferred.promise;
        };

        var paymentReceiptData = function (transactionId) {
            var deferred = $q.defer();
            var propertyIdList = { StartDate: null, EndDate: null, PropertyIdList: null, PropertyStaffId: null, TransactionId: transactionId, FlattenData: 'false' };

            //Adds a report loader gif to tell user report is being prepared
            document.getElementById('ReportLoader').innerHTML = "<img src='/Images/report_wait.gif' style='margin-top: 60px'/>";

            $http.post('/api/report/paymentreceipt', propertyIdList).success(function (data) {
                deferred.resolve(data);

                //Hides report loader after report displays on screen
                document.getElementById('ReportLoader').innerHTML = "";

            });
            return deferred.promise;
        };

        var paymentSummaryData = function (startDate, endDate, propertyIds) {
            var deferred = $q.defer();
            var propertyidlist = { startdate: startDate, enddate: endDate, propertyidlist: propertyIds, propertystaffid: null, transactionid: null, FlattenData: 'false' };

            //Adds a report loader gif to tell user report is being prepared
            document.getElementById('ReportLoader').innerHTML = "<img src='/Images/report_wait.gif' style='margin-top: 60px'/>";

            $http.post('/api/report/paymentsummary', propertyidlist).success(function (data) {
                deferred.resolve(data);

                //Hides report loader after report displays on screen
                document.getElementById('ReportLoader').innerHTML = "";

            });
            return deferred.promise;
        };

        var residentDataImportSummaryData = function (startDate, endDate, propertyIds) {
            var deferred = $q.defer();
            var propertyIdList = { StartDate: startDate, EndDate: endDate, PropertyIdList: propertyIds, PropertyStaffId: null, TransactionId: null, FlattenData: 'false' };

            //Adds a report loader gif to tell user report is being prepared
            document.getElementById('ReportLoader').innerHTML = "<img src='/Images/report_wait.gif' style='margin-top: 60px'/>";

            $http.post('/api/report/residentdataimportsummary', propertyIdList).success(function (data) {
                deferred.resolve(data);

                //Hides report loader after report displays on screen
                document.getElementById('ReportLoader').innerHTML = "";

            });
            return deferred.promise;
        };

        var rentersPastDueData = function (propertyId) {
            var deferred = $q.defer();
            var propertyIdList = { StartDate: null, EndDate: null, PropertyIdList: propertyIds, PropertyStaffId: null, TransactionId: null, FlattenData: 'false' };

            //Adds a report loader gif to tell user report is being prepared
            document.getElementById('ReportLoader').innerHTML = "<img src='/Images/report_wait.gif' style='margin-top: 60px'/>";

            $http.post('/api/report/renterspastdue', propertyIdList).success(function (data) {
                deferred.resolve(data);

                //Hides report loader after report displays on screen
                document.getElementById('ReportLoader').innerHTML = "";

            });
            return deferred.promise;
        };

        var transactionSummaryData = function (startDate, endDate, propertyIds) {
            var deferred = $q.defer();
            var propertyIdList = { StartDate: startDate, EndDate: endDate, PropertyIdList: propertyIds, PropertyStaffId: null, TransactionId: null, FlattenData: 'false' };

            //Adds a report loader gif to tell user report is being prepared
            document.getElementById('ReportLoader').innerHTML = "<img src='/Images/report_wait.gif' style='margin-top: 60px'/>";

            $http.post('/api/report/transactionsummary', propertyIdList).success(function (data) {
                deferred.resolve(data);

                //Hides report loader after report displays on screen
                document.getElementById('ReportLoader').innerHTML = "";

            });
            return deferred.promise;
        };
        var setReportName = function (report) {
            currentReportName = report;
            $rootScope.$broadcast('report:nameSet');
        };
        var getReportName = function () {
            return currentReportName;
        };
        return {            
            getAutoPaymentReport: autoPaymentData,
            getApplicantPortalPaymentReport: applicantPortalPaymentData,
            getBillingReport: billingData,
            getCharityReport: charityData,
            getDetailDepositReport: detailDepositData,
            getAllPaymentTypes: allPaymentTypesData,
            getAllProperties: allPropertiesData,
            getAllRenters: allRentersData,
            getPropertiesByStaffIdReport: propertiesByStaffIdData,
            getRentersByPropertyIdReport: rentersByPropertyIdData,
            getLeadReport: leadData,
            getPaymentExportSummaryReport: paymentExportSummaryData,
            getPaymentReceiptReport: paymentReceiptData,
            getPaymentSummaryReport: paymentSummaryData,
            getResidentDataImportSummaryReport: residentDataImportSummaryData,
            getRentersPastDueReport: rentersPastDueData,
            getTransactionSummaryReport: transactionSummaryData,
            getCurrentReportName: getReportName,
            setCurrentReportName: setReportName
        };
    })
.factory('filterFactory', function ($rootScope, $filter) {
    var currentController = null;
    var filterPropertyIds = null;
    var filterStartDate = null;
    var filterEndDate = null;
    var displayStartDate = null;
    var displayEndDate = null;
    var displayPropertyDropdown = null;
    var displayRenterDropdown = null;
    return {
        expandAll:
            function () {
                $rootScope.$broadcast('list:expand');
            },
        collapseAll:
            function () {
                $rootScope.$broadcast('list:collapse');
            },
        setCurrentController:
            function (controllerName) {
                currentController = controllerName;
                $rootScope.$broadcast('controller:nameSet');
            },
        getCurrentControllerName:
            function () {
                return currentController;
            },
        filterReport:
            function (propertyIds, startDate, endDate) {
                filterPropertyIds = propertyIds;
                filterStartDate = startDate;
                filterEndDate = endDate;
                $rootScope.$broadcast('report:filter:' + currentController);
            },
        getStartDate:
            function () {
                var startDate = new Date(filterStartDate);
                if (startDate == 'Invalid Date')//IE fix for when no date is selected. Only occurs if initialized date is used.
                    startDate = new Date();
                startDate.setDate(startDate.getDate() + 1);
                if ($filter('date')(startDate, 'MM/dd/yyyy') > $filter('date')(new Date(), 'MM/dd/yyyy'))
                    startDate.setDate(startDate.getDate() - 1);
                return $filter('date')(startDate, 'MM/dd/yyyy');
            },
        getEndDate:
            function () {
                var endDate = new Date(Date.parse(filterEndDate));
                if (endDate == 'Invalid Date')
                    endDate = new Date();
                endDate.setDate(endDate.getDate() + 1);
                if ($filter('date')(endDate, 'MM/dd/yyyy') > $filter('date')(new Date(), 'MM/dd/yyyy'))
                    endDate.setDate(endDate.getDate() - 1);
                return $filter('date')(endDate, 'MM/dd/yyyy');
            },
        getPropertyIds:
            function () { return filterPropertyIds; },
        hideStartDate:
            function () {
                displayStartDate = false;
                $rootScope.$broadcast('report:setFilterVisibility');
            },
        hideEndDate:
            function () {
                displayEndDate = false;
                $rootScope.$broadcast('report:setFilterVisibility');
            },
        hidePropertyDropdown:
            function () {
                displayPropertyDropdown = false;
                $rootScope.$broadcast('report:setFilterVisibility');
            },
        showStartDate:
            function () {
                displayStartDate = true;
                $rootScope.$broadcast('report:setFilterVisibility');
            },
        showEndDate:
            function () {
                displayEndDate = true;
                $rootScope.$broadcast('report:setFilterVisibility');
            },
        showPropertyDropdown:
            function () {
                displayPropertyDropdown = true;
                $rootScope.$broadcast('report:setFilterVisibility');
            },
        getStartDateVisibility:
            function () { return displayStartDate; },
        getEndDateVisibility:
            function () { return displayEndDate; },
        getPropertyDropdownVisibility:
            function () { return displayPropertyDropdown; }
    };
});
//Angular APP
var reportApp = angular.module('reportManager', ['ngRoute', 'pagination', 'ReportAPI', 'API', '$strap.directives']).config(function ($routeProvider, $locationProvider) {
    var html5Ok = navigator.appVersion.indexOf('MSIE') == -1;
    $locationProvider.html5Mode(html5Ok).hashPrefix('');
    
    $routeProvider.
        when('/rpo/reports/', { controller: ReportsController, templateUrl: '../../reports.html' }).
        when('/rpo/reports/auto-payment', { controller: AutoPaymentController, templateUrl: '../../report-auto-payment.html' }).
        when('/rpo/reports/applicant-portal-payment', { controller: ApplicantPortalPaymentController, templateUrl: '../../report-applicant-portal-payment.html' }).
        when('/rpo/reports/billing', { controller: BillingController, templateUrl: '../../report-billing.html' }).
        when('/rpo/reports/charity', { controller: CharityController, templateUrl: '../../report-charity.html' }).
        when('/rpo/reports/detail-deposit', { controller: DetailDepositController, templateUrl: '../../report-detail-deposit.html' }).
        when('/rpo/reports/properties-by-staff', { controller: PropertiesByStaffIdController, templateUrl: '../../report-properties-by-staff-id.html' }).
        when('/rpo/reports/renters-by-property', { controller: RentersByPropertyIdController, templateUrl: '../../report-renters-by-property-id.html' }).
        when('/rpo/reports/lead', { controller: LeadController, templateUrl: '../../report-lead.html' }).
        when('/rpo/reports/payment-export-summary', { controller: PaymentExportSummaryController, templateUrl: '../../report-payment-export-summary.html' }).
        when('/rpo/reports/payment-receipt', { controller: PaymentReceiptController, templateUrl: '../../report-payment-receipt.html' }).
        when('/rpo/reports/payment-summary', { controller: PaymentSummaryController, templateUrl: '../../report-payment-summary.html' }).
        when('/rpo/reports/resident-data-import-summary', { controller: ResidentDataImportSummaryController, templateUrl: '../../report-resident-data-import-summary.html' }).
        when('/rpo/reports/renters-past-due', { controller: RentersPastDueController, templateUrl: '../../report-renters-past-due.html' }).
        when('/rpo/reports/transaction-summary', { controller: TransactionSummaryController, templateUrl: '../../report-transaction-summary.html' }).
        otherwise({ redirectTo: '/rpo/reports/' });
});
reportApp.value('$strapConfig', {
    datepicker: {
        language: 'en',
        format: 'mm/dd/yy'
    }
});
reportApp.directive('multiselectDropdown', [function () {
    return function(scope, element, attributes) {

        element = $(element[0]); // Get the element as a jQuery element

        // Below setup the dropdown:

        element.multiselect({
            buttonClass: 'btn btn-small',
            buttonWidth: '270px',
            buttonContainer: '<div class="btn-group" />',
            maxHeight: 200,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            includeSelectAllOption: false,
            buttonText: function(options) {
                if (options.length == 0) {
                    return element.data()['placeholder'] + ' <b class="caret"></b>';
                } else if (options.length > 1) {
                    return (options.length)
                        + ' selected <b class="caret"></b>';
                } else {
                    return _.first(options).text
                        + ' <b class="caret"></b>';
                }
            },
            // Replicate the native functionality on the elements so
            // that angular can handle the changes for us.
            onChange: function(optionElement, checked) {
                optionElement.prop('selected'), false;
                if (checked) {
                    optionElement.prop('selected', true);
                }
                element.change();
            }
        });
        // Watch for any changes to the length of our select element
        scope.$watch(function() {
            return element[0].length;
        }, function() {
            element.multiselect('rebuild');
        });

        // Watch for any changes from outside the directive and refresh
        scope.$watch(attributes.ngModel, function() {
            element.multiselect('refresh');
        });

        // Below maybe some additional setup
    };
}]);
//Landing Page Controller
function ReportsController($scope, $http, $rootScope,$location) {
}
//Auto Payment Controller
function AutoPaymentController($scope, $http, $rootScope, reportFactory, filterFactory,$location) {
    $scope.emptyText = '';
    $scope.showDetails = false;
    filterFactory.showPropertyDropdown();
    $scope.toggleReport = function (row) {
        row.showDetails = !row.showDetails;
    };
    filterFactory.setCurrentController('AutoPayment');
    filterFactory.hideStartDate();
    filterFactory.hideEndDate();
    if (filterFactory.getCurrentControllerName() == 'AutoPayment') {
        reportFactory.setCurrentReportName('auto-payment');
        $rootScope.$on('list:collapse', function () {
            angular.forEach($scope.report, function (value) {
                value.showDetails = false;
            });
        });
        $rootScope.$on('list:expand', function () {
            angular.forEach($scope.report, function (value) {
                value.showDetails = true;
            });
        });
        $rootScope.$on('report:filter:AutoPayment', function () {
            reportFactory.getAutoPaymentReport(filterFactory.getStartDate(),filterFactory.getEndDate(),filterFactory.getPropertyIds()).then(function (reportData) {
                $scope.report = reportData;
                $scope.emptyText = 'No data available for the selected properties.';
                $scope.noData = reportData.length == 0;
                console.log($scope.noData);
            });
        });

        //Salcedo - 4/19/2014 - added code to handle the 'p' querystring, as in '?p=2', '?p=3', etc. 
        var queryfoundloc = $location.$$url.indexOf("?p=");
        if (queryfoundloc != -1) {
            var pageNumber = parseInt($location.$$url.substring(queryfoundloc+3));
            reportFactory.getAutoPaymentReport(filterFactory.getStartDate(), filterFactory.getEndDate(), filterFactory.getPropertyIds(), pageNumber).then(function (reportData) {
                $scope.report = reportData;
                $scope.emptyText = 'No data available for the selected properties.';
                $scope.noData = reportData.length == 0;
                console.log($scope.noData);
            });
        }
    }
}

function ApplicantPortalPaymentController($scope, $http, $rootScope, reportFactory, filterFactory) {
    filterFactory.setCurrentController('Applicant Portal Payment');
    filterFactory.showStartDate();
    filterFactory.showEndDate();
    filterFactory.showPropertyDropdown();
    $scope.showDetails = false;
    $scope.toggleReport = function (row) {
        row.showDetails = !row.showDetails;
    };
    if (filterFactory.getCurrentControllerName() == 'Applicant Portal Payment') {
        reportFactory.setCurrentReportName('applicant-portal-payment');
        $rootScope.$on('list:collapse', function () {
            angular.forEach($scope.report, function (value) {
                value.showDetails = false;
            });
        });
        $rootScope.$on('list:expand', function () {
            angular.forEach($scope.report, function (value) {
                value.showDetails = true;
            });
        });
        $rootScope.$on('report:filter:Applicant Portal Payment', function () {
            reportFactory.getApplicantPortalPaymentReport(filterFactory.getStartDate(), filterFactory.getEndDate(), filterFactory.getPropertyIds()).then(function (reportData) {
                $scope.report = reportData;
                $scope.emptyText = 'No data available for the selected properties for the date range: ' + filterFactory.getStartDate() + ' - ' + filterFactory.getEndDate() + '.';
            });
        });
    }
}

function BillingController($scope, $http, $rootScope, reportFactory, filterFactory, userService, $location) {
	userService.userIsAdmin().then(function (isAdmin) {
		if (isAdmin == 'false') {
			$location.path('/rpo/reports');
		}
		else {
			filterFactory.setCurrentController('Billing');
			filterFactory.hideStartDate();
			filterFactory.hideEndDate();
			filterFactory.showPropertyDropdown();
			$scope.showDetails = false;
			$scope.toggleReport = function (row) {
				row.showDetails = !row.showDetails;
			};
			if (filterFactory.getCurrentControllerName() == 'Billing') {
				reportFactory.setCurrentReportName('billing');
				$rootScope.$on('list:collapse', function () {
					angular.forEach($scope.report, function (value) {
						value.showDetails = false;
					});
				});
				$rootScope.$on('list:expand', function () {
					angular.forEach($scope.report, function (value) {
						value.showDetails = true;
					});
				});
				$rootScope.$on('report:filter:Billing', function () {
					reportFactory.getBillingReport(filterFactory.getPropertyIds()).then(function (reportData) {
						$scope.report = reportData;
						$scope.emptyText = 'No data available for the selected properties.';
					});
				});
			}
		}
	});
}

function CharityController($scope, $http, $rootScope, reportFactory) {
    $scope.data = reportFactory.getCharityReport();
    $scope.emptyText = 'No data available for this report.';
}

function DetailDepositController($scope, $http, $rootScope, reportFactory, filterFactory) {
    filterFactory.setCurrentController('Detailed Deposit');
    filterFactory.showStartDate();
    filterFactory.showEndDate();
    filterFactory.showPropertyDropdown();
    $scope.showDetails = false;
    $scope.toggleReport = function (row) {
        row.showDetails = !row.showDetails;
    };
    if (filterFactory.getCurrentControllerName() == 'Detailed Deposit') {
        reportFactory.setCurrentReportName('detail-deposit');
        $rootScope.$on('list:collapse', function () {
            angular.forEach($scope.report, function (value) {
                value.showDetails = false;
            });
        });
        $rootScope.$on('list:expand', function () {
            angular.forEach($scope.report, function (value) {
                value.showDetails = true;
            });
        });
        $rootScope.$on('report:filter:Detailed Deposit', function () {
            reportFactory.getDetailDepositReport(filterFactory.getStartDate(), filterFactory.getEndDate(), filterFactory.getPropertyIds()).then(function (reportData) {
                $scope.report = reportData;
                $scope.emptyText = 'No data available for the selected properties for the date range: ' + filterFactory.getStartDate() + ' - ' + filterFactory.getEndDate() + '.';
            });
        });
    }
}

function AllPaymentTypesController($scope, $http, $rootScope, reportFactory) {
    $scope.data = reportFactory.getAllPaymentTypes();
}

function AllPropertiesController($scope, $http, $rootScope, reportFactory) {
    $scope.data = reportFactory.getAllProperties();
}

function AllRentersController($scope, $http, $rootScope, reportFactory) {
    $scope.data = reportFactory.getAllRenters();
}

//This is the one to use to populate the property dropdown
//You'll need to pass in a PropertyStaffId for the PM site or 
//a -1 for the admin site
function PropertiesByStaffIdController($scope, $http, $rootScope, reportFactory) {
    $scope.data = reportFactory.getPropertiesByStaffIdReport();
}

function RentersByPropertyIdController($scope, $http, $rootScope, reportFactory) {
    $scope.data = reportFactory.getRentersByPropertyIdReport();
}

function LeadController($scope, $http, $rootScope, reportFactory, filterFactory, userService, $location) {
	userService.userIsAdmin().then(function (isAdmin) {
		if (isAdmin == 'false') {
			$location.path('/rpo/reports');
		}
		else{
			filterFactory.setCurrentController('Lead');
			filterFactory.showStartDate();
			filterFactory.showEndDate();
			filterFactory.hidePropertyDropdown();
			$scope.showDetails = false;
			$scope.toggleReport = function (row) {
				row.showDetails = !row.showDetails;
			};
			if (filterFactory.getCurrentControllerName() == 'Lead') {
				reportFactory.setCurrentReportName('lead');
				$rootScope.$on('list:collapse', function () {
					angular.forEach($scope.report, function (value) {
						value.showDetails = false;
					});
				});
				$rootScope.$on('list:expand', function () {
					angular.forEach($scope.report, function (value) {
						value.showDetails = true;
					});
				});
				$rootScope.$on('report:filter:Lead', function () {
					reportFactory.getLeadReport(filterFactory.getStartDate(), filterFactory.getEndDate()).then(function (reportData) {
						$scope.report = reportData;
						$scope.emptyText = 'No data available for the date range: ' + filterFactory.getStartDate() + ' - ' + filterFactory.getEndDate() + '.';
					});
				});
			}
		}
	});
}

function PaymentExportSummaryController($scope, $http, $rootScope, reportFactory, filterFactory) {
    filterFactory.setCurrentController('Payment Export Summary');
    filterFactory.showStartDate();
    filterFactory.showEndDate();
    filterFactory.showPropertyDropdown();
    $scope.showDetails = false;
    $scope.toggleReport = function (row) {
        row.showDetails = !row.showDetails;
    };
    if (filterFactory.getCurrentControllerName() == 'Payment Export Summary') {
        reportFactory.setCurrentReportName('payment-export-summary');
        $rootScope.$on('list:collapse', function () {
            angular.forEach($scope.report, function (value) {
                value.showDetails = false;
            });
        });
        $rootScope.$on('list:expand', function () {
            angular.forEach($scope.report, function (value) {
                value.showDetails = true;
            });
        });
        $rootScope.$on('report:filter:Payment Export Summary', function () {
            reportFactory.getPaymentExportSummaryReport(filterFactory.getStartDate(), filterFactory.getEndDate(), filterFactory.getPropertyIds()).then(function (reportData) {
                $scope.report = reportData;
                $scope.emptyText = 'No data available for the selected properties for the date range: ' + filterFactory.getStartDate() + ' - ' + filterFactory.getEndDate() + '.';
            });
        });
        $scope.setModal = function (row) {
        	$scope.modal = {RequestXml: row.RequestXml, ResponseXml: row.ResponseXml};
        };
    }
}

function PaymentReceiptController($scope, $http, $rootScope, reportFactory) {
    $scope.data = reportFactory.getPaymentReceiptReport();
}

function PaymentSummaryController($scope, $http, $rootScope, reportFactory, filterFactory,$filter) {
    filterFactory.setCurrentController('Payment Summary');
    filterFactory.showStartDate();
    filterFactory.showEndDate();
    filterFactory.showPropertyDropdown();
    $scope.showDetails = false;
    $scope.toggleReport = function (row) {
        row.showDetails = !row.showDetails;
    };
    if (filterFactory.getCurrentControllerName() == 'Payment Summary') {
        reportFactory.setCurrentReportName('payment-summary');
        $rootScope.$on('list:collapse', function () {
            angular.forEach($scope.report, function (value) {
                value.showDetails = false;
            });
        });
        $rootScope.$on('list:expand', function () {
            angular.forEach($scope.report, function (value) {
                value.showDetails = true;
            });
        });
        $rootScope.$on('report:filter:Payment Summary', function () {
            reportFactory.getPaymentSummaryReport(filterFactory.getStartDate(), filterFactory.getEndDate(), filterFactory.getPropertyIds()).then(function (reportData) {
                $scope.report = reportData;
                $scope.emptyText = 'No data available for the selected properties for the date range: ' + filterFactory.getStartDate() + ' - ' + filterFactory.getEndDate() + '.';
            });
        });
    }
}

function ResidentDataImportSummaryController($scope, $http, $rootScope, reportFactory, filterFactory) {
    filterFactory.setCurrentController('Resident Data Import Summary');
    filterFactory.showStartDate();
    filterFactory.showEndDate();
    filterFactory.showPropertyDropdown();
    $scope.showDetails = false;
    $scope.toggleReport = function (row) {
        row.showDetails = !row.showDetails;
    };
    if (filterFactory.getCurrentControllerName() == 'Resident Data Import Summary') {
        reportFactory.setCurrentReportName('resident-data-import-summary');
        $rootScope.$on('list:collapse', function () {
            angular.forEach($scope.report, function (value) {
                value.showDetails = false;
            });
        });
        $rootScope.$on('list:expand', function () {
            angular.forEach($scope.report, function (value) {
                value.showDetails = true;
            });
        });
        $rootScope.$on('report:filter:Resident Data Import Summary', function () {
            reportFactory.getResidentDataImportSummaryReport(filterFactory.getStartDate(), filterFactory.getEndDate(), filterFactory.getPropertyIds()).then(function (reportData) {
                $scope.report = reportData;
                $scope.emptyText = 'No data available for the selected properties for the date range: ' + filterFactory.getStartDate() + ' - ' + filterFactory.getEndDate() + '.';
            });
        });
    }
}

function RentersPastDueController($scope, $http, $rootScope, reportFactory, filterFactory) {
    filterFactory.setCurrentController('Residents Past Due');
    filterFactory.hideStartDate();
    filterFactory.hideEndDate();
    filterFactory.showPropertyDropdown();
    reportFactory.getRentersPastDueReport().then(function (reportData) {
        $scope.report = reportData;
    });
    $scope.showDetails = false;
    $scope.toggleReport = function (row) {
        row.showDetails = !row.showDetails;
    };
    if (filterFactory.getCurrentControllerName() == 'Residents Past Due') {
        reportFactory.setCurrentReportName('renters-past-due');
        $rootScope.$on('list:collapse', function () {
            angular.forEach($scope.report, function (value) {
                value.showDetails = false;
            });
        });
        $rootScope.$on('list:expand', function () {
            angular.forEach($scope.report, function (value) {
                value.showDetails = true;
            });
        });
        $rootScope.$on('report:filter:Residents Past Due', function () {
            reportFactory.getRentersPastDueReport(filterFactory.getPropertyIds()).then(function (reportData) {
                $scope.report = reportData;
                $scope.emptyText = 'No data available for the selected properties.';
            });
        });
    }
}

function TransactionSummaryController($scope, $http, $rootScope, reportFactory, filterFactory) {
    filterFactory.setCurrentController('Transaction Summary');
    filterFactory.showStartDate();
    filterFactory.showEndDate();
    filterFactory.showPropertyDropdown();
    $scope.showDetails = false;
    $scope.toggleReport = function (row) {
        row.showDetails = !row.showDetails;
    };
    if (filterFactory.getCurrentControllerName() == 'Transaction Summary') {
        reportFactory.setCurrentReportName('transaction-summary');
        $rootScope.$on('list:collapse', function () {
            angular.forEach($scope.report, function (value) {
                value.showDetails = false;
            });
        });
        $rootScope.$on('list:expand', function () {
            angular.forEach($scope.report, function (value) {
                value.showDetails = true;
            });
        });
        $rootScope.$on('report:filter:Transaction Summary', function () {
            reportFactory.getTransactionSummaryReport(filterFactory.getStartDate(), filterFactory.getEndDate(), filterFactory.getPropertyIds()).then(function (reportData) {
                $scope.report = reportData;
                $scope.emptyText = 'No data available for the selected properties for the date range: ' + filterFactory.getStartDate() + ' - ' + filterFactory.getEndDate() + '.';
            });
        });
    }
}
function FilterController($scope, $rootScope, filterFactory, propertyService, renterService, reportFactory, userService, $filter) {
    $scope.expandList = function () {
        $scope.showAllDetails = true;
        filterFactory.expandAll();
    };
    $scope.fileType = 'CSV';
    $scope.collapseList = function() {
        $scope.showAllDetails = false;
        filterFactory.collapseAll();
    };
    $scope.startDate = { date: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss Z') };
    $scope.endDate = { date: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss Z') };
    userService.userIsAdmin().then(function (isAdmin) {
    	$scope.isRPOAdmin = isAdmin;
    	userService.getUserId().then(function (userId) {
    		$scope.userId = userId;
    		$scope.properties = isAdmin == 'true' ? propertyService.getAllPropertiesForDropdown() : propertyService.getUserPropertiesForDropdown(userId);
    	});
    });
    
    $scope.filterReport = function (properties) {
    	$scope.filtered = true;
    	$scope.collapseList();
    	var selectedPropertyIds = [];
    	if ($scope.propertySelection != null && $scope.propertySelection.length > 0 && $scope.propertySelection[0].PropertyId == 0) {
    		//select all
    		for (var i = 1; i < properties.length; i++) {
    			selectedPropertyIds.push(properties[i].PropertyId);
    		}
    	}
    	else {
    	    for (var i = 0; i < $scope.propertySelection.length; i++) {
    	        selectedPropertyIds.push($scope.propertySelection[i].PropertyId);
    	    }
    	}
        filterFactory.filterReport(selectedPropertyIds, $scope.startDate.date, $scope.endDate.date);
        $scope.selectedProperties = filterFactory.getPropertyIds();
    };

	//this is a slightly varied version of the filterReport function above and is used for the export button
	//it is run by the propertySelection watch
    $scope.getCurrentSelectedProperties = function () {
    	var selectedPropertyIds = [];
    	if ($scope.propertySelection != null && $scope.propertySelection.length > 0 && $scope.propertySelection[0].PropertyId == 0) {
    		for (var i = 1; i < $scope.properties.$$v.length; i++) {
    			selectedPropertyIds.push($scope.properties.$$v[i].PropertyId);
    		}
    	}
    	else {
    		if ($scope.propertySelection != null) {
    			for (var i = 0; i < $scope.propertySelection.length; i++) {
    				selectedPropertyIds.push($scope.propertySelection[i].PropertyId);
    			}
    		}
    	}
    	return selectedPropertyIds;
    }

    $rootScope.$on('report:nameSet', function () {
        $scope.reportName = reportFactory.getCurrentReportName();
        $scope.fullReportName = filterFactory.getCurrentControllerName();
    });
    $rootScope.$on('report:setFilterVisibility', function () {
        $scope.startDateVisibility = filterFactory.getStartDateVisibility();
        $scope.endDateVisibility = filterFactory.getEndDateVisibility();
        $scope.showPropertyDropdown = filterFactory.getPropertyDropdownVisibility();

        $scope.$watch('propertySelection', function () {
        	$scope.currentSelectedProperties = $scope.getCurrentSelectedProperties();
            $scope.filtersSet = !$scope.showPropertyDropdown || ($scope.propertySelection != null && $scope.propertySelection.length > 0);
        });
    });
}
function MenuController($scope,$location,filterFactory,$rootScope,userService) {
    $scope.urlPrefix = navigator.appVersion.indexOf('MSIE') == -1 ? "" : "/rpo/reports#/rpo/reports/";
    $scope.selected = !location.href.match("/rpo/reports/$");
    $scope.title = 'Select A Report';
    $rootScope.$on('controller:nameSet', function () {
        $scope.title = filterFactory.getCurrentControllerName();
    });
    userService.userIsAdmin().then(function (isAdmin) {
    	$scope.isRPOAdmin = isAdmin;
    });
}
//Initialize App (do not move. must be at end to work)
angular.bootstrap($('#reportManager'), ["reportManager"]);