﻿var staffApp = angular.module('rpoStaffManager', ['$strap.directives', 'API', 'ngAnimate']);
staffApp.directive('multiselectDropdown', [function () {
    return function (scope, element, attributes) {

        element = $(element[0]); // Get the element as a jQuery element
        rebuilt = false;
        // Below setup the dropdown:

        element.multiselect({
            buttonClass: 'btn btn-small',
            buttonWidth: '270px',
            buttonContainer: '<div class="btn-group" />',
            maxHeight: 200,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            includeSelectAllOption: false,
            buttonText: function (options) {
                if (options.length == 0) {
                    return element.data()['placeholder'] + ' <b class="caret"></b>';
                } else if (options.length > 1) {
                    return (options.length)
                        + ' selected <b class="caret"></b>';
                } else {
                    return _.first(options).text
                        + ' <b class="caret"></b>';
                }
            },
            // Replicate the native functionality on the elements so
            // that angular can handle the changes for us.
            onChange: function (optionElement, checked) {
                optionElement.prop('selected'), false;
                if (checked) {
                    optionElement.prop('selected', true);
                }
                element.change();
            }
        });
        // Watch for any changes to the length of our select element
        scope.$watch(function () {
            return element[0].length;
        }, function () {
            rebuilt = true;
            element.multiselect('rebuild');
            
        });

        // Watch for any changes from outside the directive and refresh
        scope.$watch(attributes.ngModel, function () {
                element.multiselect('refresh');
        });
        // Below maybe some additional setup
    };
}]);
staffApp.directive("passwordVerify", function () {
    return {
        require: "ngModel",
        scope: {
            passwordVerify: '='
        },
        link: function (scope, element, attrs, ctrl) {
            scope.$watch(function () {
                var combined;

                if (scope.passwordVerify || ctrl.$viewValue) {
                    combined = scope.passwordVerify + '_' + ctrl.$viewValue;
                }
                return combined;
            }, function (value) {
                if (value) {
                    ctrl.$parsers.unshift(function (viewValue) {
                        var origin = scope.passwordVerify;
                        if (origin !== viewValue) {
                            ctrl.$setValidity("passwordVerify", false);
                            return undefined;
                        } else {
                            ctrl.$setValidity("passwordVerify", true);
                            return viewValue;
                        }
                    });
                }
            });
        }
    };
});
function StaffController($scope, $rootScope, $http, $timeout, $q, propertyService, companyService, staffService, userService, $location, $filter) {
    $scope.setupCompanies = function () {
        var deferred = $q.defer();
        companyService.getAllCompanies().then(function (companies) {
            $scope.companies = companies;
            deferred.resolve(companies);
        });
        return deferred.promise;
    }
    $scope.setupProperties = function (isAdmin, editMode) {
        var deferred = $q.defer();
        if (!isEmpty($scope.CompanyId)) {
            propertyService.getCompanyPropertiesForDropdown($scope.CompanyId).then(function (properties) {
                $scope.properties = properties;
                deferred.resolve(properties);
            });
        }
        else if (isAdmin == 'true') {
            propertyService.getAllPropertiesForDropdown().then(function (properties) {
                $scope.properties = properties;
                deferred.resolve(properties);
            });
        }
        else {
            
            userService.getUserId().then(function (userId) {
                propertyService.getUserPropertiesForDropdown(userId).then(function (properties) {
                    $scope.properties = properties;
                    deferred.resolve(properties);
                });
            });
        }
        return deferred.promise;
    }
    $scope.setupRoles = function (isAdmin) {
        $scope.roles =
        [
            { name: 'Property Manager', value: 1 },
            { name: 'Leasing Agent', value: 2 },
            { name: 'Corporate Admin', value: 3 },
            { name: 'Customer Support Represenative', value: 4 }
        ];
        if (isAdmin == 'true') {
            
        }
    }
    $scope.setupStates = function () {
        staffService.getStates().then(function (states) {
            $scope.states = states;
        });
    }
    $scope.initializePage = function () {
        userService.userIsAdmin().then(function (isAdmin) {
            $scope.isRPOAdmin = isAdmin;
            if (isAdmin == 'false') {
                userService.userIsCorporateAdmin().then(function (isCorporateAdmin) {
                    $scope.isCorporateAdmin = isCorporateAdmin;
                    if ($scope.isCorporateAdmin === 'false') {
                        window.location.href = '/';
                    }
                });
            }
            if ($.getUrlVar('companyId')) {
                $scope.CompanyId = $.getUrlVar('companyId');

            }
            if ($.getUrlVar('propertyId')) {
                $scope.PropertyId = $.getUrlVar('propertyId');
            }
            //setup states
            $scope.setupStates();
            //setup roles
            $scope.setupRoles(isAdmin);
            var staffId = $.getUrlVar('staffId');
            if (staffId) {
                $rootScope.$broadcast('Edit');
                $scope.submitButtonText = 'Save Staff';
                $scope.mode = 'Edit';
                $scope.editMode = true;
                $scope.loadData(staffId);
            } else {
                $rootScope.$broadcast('Create');
                $scope.submitButtonText = 'Create Staff';
                $scope.mode = 'Create';
                $scope.setupCompanies().then(
                    function () {
                        if (!isEmpty($scope.CompanyId)) {
                            angular.forEach($scope.companies, function (company, key) {
                                if (company.CompanyId == $scope.CompanyId) {
                                    $scope.selectedCompany = company;
                                }
                            });
                        }
                    }
                    );
                $scope.setupProperties(isAdmin, false);
                if (!isEmpty($scope.CompanyId) && !isEmpty($scope.PropertyId)) {
                    angular.forEach($scope.roles, function (role, key) {
                        if (role.value == 1) {
                            $scope.selectedRole = role;
                        }
                    });
                }
                //if (isAdmin != 'true') {
                    
                    
                //    //$scope.showCompanies = true;
                //}
                //else {
                    
                //    //$scope.showProperties = true;
                //}
            }
        });
    }
    $scope.selectProperties = function () {
        var propertySelection = [];
        angular.forEach($scope.properties, function (property, key) {
            if (_.contains($scope.staff.PropertyIds, property.PropertyId)) {
                propertySelection.push(property);
            }
        });
        $scope.propertySelection = propertySelection;
    }
    $scope.loadData = function (staffId) {
        staffService.getStaff(staffId).then(function (staff) {
            if (staff == 'null') {
                window.location.href = '/staff.aspx';
            }
            $scope.staff = staff;
            $scope.FirstName = staff.FirstName;
            $scope.MiddleName = staff.MiddleName;
            $scope.LastName = staff.LastName;
            $scope.Email = staff.PrimaryEmailAddress;
            //$scope.selectedRole = staff.;
            $scope.MainPhone = staff.MainPhoneNumber;
            $scope.MobilePhone = staff.MobilePhoneNumber;
            $scope.StreetAddress = staff.StreetAddress;
            $scope.StreetAddress2 = staff.StreetAddress2;
            $scope.City = staff.City;
            //$scope.selectedState = staff.;
            $scope.PostalCode = staff.PostalCode;
            $scope.IsPropertyGeneralManager = staff.IsPropertyGeneralManager;
            $scope.IsMainContact = staff.IsMainContact;
            $scope.CompanyId = staff.CompanyId;
            $scope.setupCompanies().then(
                    function () {
                        $scope.selectCompany();
                        $scope.showCompanies = true;
                    });
            //if ($scope.isRPOAdmin == 'true') {
                
            //}
            $scope.selectRole();
            //setup properties if user is not PMC
            if ($scope.staff.RoleId != 3) {
                $scope.setupProperties($scope.isRPOAdmin, true).then(function () {
                    $scope.selectProperties();
                });
            }
            $scope.selectState();
            
        });
    }
    $scope.selectRole = function () {
        angular.forEach($scope.roles, function (role, key) {
            if ($scope.staff.RoleId == role.value) {
                $scope.selectedRole = role;
            }
        });
    }
    $scope.selectState = function () {
        angular.forEach($scope.states, function (state, key) {
            if ($scope.staff.StateProvinceId == state.StateProvinceId) {
                $scope.selectedState = state;
            }
        });
    }
    $scope.selectCompany = function () {
        angular.forEach($scope.companies, function (company, key) {
            if ($scope.staff.CompanyId == company.CompanyId) {
                $scope.selectedCompany = company;
            }
        });
    }
    //start
    $scope.initializePage();
    $scope.$watch('selectedRole', function () {
        if ($scope.selectedRole) {
            $scope.showProperties = ($scope.isCorporateAdmin == 'true' || $scope.selectedCompany) && $scope.selectedRole.value == 1 || $scope.selectedRole.value == 2;
            $scope.showCompanies = true;
        }
        else {
            $scope.showProperties = false;
            $scope.showCompanies = false;
        }
    });
    $scope.$watch('selectedCompany', function () {
        //if ($scope.editMode != true 
        //    && $scope.selectedRole 
        //    && ($scope.selectedRole.value == 1 || $scope.selectedRole.value == 2)
        //    && $scope.isRPOAdmin == 'true') {
        if($scope.selectedCompany) {
            //filter properties by company then show properties
            $scope.CompanyId = $scope.selectedCompany.CompanyId;
            $scope.setupProperties().then(
                    function () {
                        if($scope.selectedRole && ($scope.selectedRole.value == 1 || $scope.selectedRole.value == 2)) {
                            $scope.showProperties = true;
                        }
                        if (!isEmpty($scope.PropertyId)) {
                            $scope.propertySelection = [];
                            angular.forEach($scope.properties, function (property, key) {
                                if (property.PropertyId == $scope.PropertyId) {
                                    $scope.propertySelection.push(property);
                                }
                            });
                        }
                    });
        }
    });
    $scope.submit = function () {
        $scope.submitDisabled = true;
        $scope.submitButtonText = $scope.mode == 'Edit' ? 'Saving...' : 'Creating Staff...';
        var selectedPropertyIds = [];
        if (!isEmpty($scope.propertySelection)) {
            if ($scope.propertySelection != null && $scope.propertySelection.length > 0 && $scope.propertySelection[0].PropertyId == 0) {
                //select all
                for (var i = 1; i < properties.length; i++) {
                    selectedPropertyIds.push(properties[i].PropertyId);
                }
            }
            else {
                for (var i = 0; i < $scope.propertySelection.length; i++) {
                    selectedPropertyIds.push($scope.propertySelection[i].PropertyId);
                }
            }
        }
        var staff = {
            FirstName: $scope.FirstName,
            MiddleName: $scope.MiddleName,
            LastName: $scope.LastName,
            PrimaryEmailAddress: $scope.Email,
            Password: $scope.Password,
            RoleId: $scope.selectedRole.value,
            MainPhoneNumber: $scope.MainPhone,
            MobilePhoneNumber: $scope.MobilePhone,

            //cakel: BUGID00014 added Company Name
            CompanyName: $scope.CompanyName,

            StreetAddress: $scope.StreetAddress,
            StreetAddress2: $scope.StreetAddress2,
            City: $scope.City,
            StateProvinceId: $scope.selectedState != null ? $scope.selectedState.StateProvinceId : null,
            PostalCode: $scope.PostalCode,
            IsPropertyGeneralManager: $scope.IsPropertyGeneralManager,
            IsMainContact: $scope.IsMainContact,
            PropertyIds: selectedPropertyIds,
            CompanyId: $scope.CompanyId
        };
        //create staff object
        if ($scope.mode == 'Create') {
            //if ($scope.selectedCompany) {
            //    staff.CompanyId = $scope.selectedCompany.CompanyId;
            //}
            staffService.createStaff(staff).then(function (response) {
                if (response.indexOf('success') != -1) {
                    $scope.successMsg = 'Staff successfully created!';
                    $scope.success = true;
                    $scope.submitButtonText = "Staff Created";
                    $timeout(function () {
                        $scope.FirstName = '';
                        $scope.MiddleName = '';
                        $scope.LastName = '';
                        $scope.Email = '';
                        $scope.Password = '';
                        $scope.ConfirmPassword = '';
                        $scope.selectedRole = null;
                        $scope.MainPhone = '';
                        $scope.MobilePhone = '';
                        $scope.StreetAddress = '';
                        $scope.StreetAddress2 = '';
                        $scope.City = '';
                        $scope.selectedState = null;
                        $scope.PostalCode = '';
                        $scope.IsPropertyGeneralManager = false;
                        $scope.IsMainContact = false;
                        $scope.propertySelection = null;
                        $scope.submitButtonText = "Create Staff";
                        $scope.submitDisabled = false;
                        $scope.success = false;
                    }, 3000);
                }
                else {
                    $scope.failureMsg = 'Error creating Staff: ' + response;
                    $scope.failure = true;
                    $scope.submitButtonText = "Create Staff";
                    $scope.submitDisabled = false;
                    $timeout(function () {
                        $scope.failure = false;
                    }, 5000);
                }
            });
        }
        else if ($scope.mode == 'Edit') {
            staff.PropertyStaffId = $scope.staff.PropertyStaffId;
            staffService.editStaff(staff).then(function (response) {
                if (response == 'true') {
                    $scope.success = true;
                    $scope.successMsg = 'Staff successfully updated!';
                    $scope.submitButtonText = "Staff Saved";
                    $timeout(function () {
                        $scope.submitButtonText = "Save Staff";
                        $scope.submitDisabled = false;
                        $scope.success = false;
                    }, 3000);
                }
                else {
                    $scope.failureMsg = 'Error updating Staff';
                    $scope.failure = true;
                    $scope.submitButtonText = "Save Staff";
                    $scope.submitDisabled = false;
                    $timeout(function () {
                        $scope.failure = false;
                    }, 5000);
                }
            });
        }
    }
}
function TitleController($scope, $rootScope) {
    $rootScope.$on('Edit', function () {
        $scope.PageTitle = 'Edit Property Staff';
    });
    $rootScope.$on('Create', function () {
        $scope.PageTitle = 'Add New Property Staff';
    });
}
angular.bootstrap($('#app'), ["rpoStaffManager"]);