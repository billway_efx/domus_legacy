﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PaymentIvr.aspx.cs" Inherits="Domus.PaymentIvr" %>
<%@ Import Namespace="EfxFramework" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="pageContainer">
        <div class="pageTitle"><h2>Payments</h2></div>
        <!--Main Tabs Section -->
        <div class="singleItemSection">
            <div class="slimPadding">
                <div class="receiptHolder">
                    <header>
                        <h3>Payment Status</h3>
                        <p>Your Payment has been pre-processed.</p>
                        <p>To complete your payment, please call:</p> 
                        <p>1-855-PMY-RENT (1-855-769-7368)</p>
                        <p>At the prompt, please enter this code:</p>
                        <p><asp:Label ID="PaymentQueueNumberLabel" runat="server"></asp:Label></p>
                    </header>
                    <div class="receipt">
                        <div class="recieptColumn">
                            <!-- **Second Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Rent**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Rent</span>
                                    <p><asp:Label ID="RentAmountLabel" runat="server"/></p>
                                </div>                            
                            </div>
                            <!-- **Third Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Convenience Fee**-->
                                 <div class="formFullWidth">
                                     <span class="formLabel">Convenience Fee</span>
                                     <p><asp:Label ID="ConvenienceFeeLabel" runat="server"/></p>
                                 </div>
                            </div>
                            <!-- **Fourth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Total Payment Amount**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Total Payment Amount</span>
                                    <p><asp:Label ID="TotalPaymentAmountLabel" runat="server" /></p>
                                </div>                                 
                            </div>
                            <!-- **Fifth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Property**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Property</span>
                                    <p><asp:Label ID="PropertyNameLabel" runat="server" /></p>
                                    <p><asp:Label ID="PropertyAddressLabel" runat="server" /></p>
                                    <p><asp:Label ID="PropertyCityStateZipLabel" runat="server" /></p>
                                </div>                                 
                            </div>
                            <!-- **Sixth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**RPO Support**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">RPO Support</span>
                                    <p>Support Line: <%=Settings.SupportPhone %></p>
                                    <p>Email: <%=Settings.SupportEmail %></p>
                                </div>                                 
                            </div>
                        </div>
                        <div class="recieptColumn">
                            <!-- **First Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**First Name **-->
                                <div class="formFullWidth">
                                    <span class="formLabel">First Name</span>
                                    <p><asp:Label ID="FirstNameLabel" runat="server" /></p>
                                </div>
                            </div>
                            <!-- **Second Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Middle Name **-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Middle Name</span>
                                    <p><asp:Label ID="MiddleNameLabel" runat="server" /></p>
                                </div>
                            </div>
                            <!-- **Third Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Last Name**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Last Name</span>
                                    <p><asp:Label ID="LastNameLabel" runat="server" /></p>
                                </div>
                            </div>
                            <!-- **Fourth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                 <!--**Suffix**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Suffix</span>
                                    <p><asp:Label ID="SuffixLabel" runat="server" /></p>
                                </div>
                            </div>
                            <!-- **Fifth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                 <!--**Billing Address**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Billing Address</span>
                                    <p><asp:Label ID="BillingAddressLabel" runat="server" /></p>
                                    <p><asp:Label ID="BillingCityStateZipLabel" runat="server" /></p>
                                </div>
                            </div>
                            <!-- **Sixth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                 <!--**Phone Number**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Phone Number</span>
                                    <p><asp:Label ID="PhoneNumberLabel" runat="server" /></p>
                                </div>
                            </div>
                        </div>
                     </div>
                 </div>
                <div class="clearFix"></div>
            </div>                            
        </div>
    </div>
</asp:Content>
