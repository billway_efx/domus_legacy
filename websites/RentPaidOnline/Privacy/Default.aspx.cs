﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.Web;

namespace Domus.Privacy
{
    public partial class Default : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PrivacyContent.Text = Facade.HtmlTemplateContent["PrivacyPolicy"].ToString();
        }
    }
}