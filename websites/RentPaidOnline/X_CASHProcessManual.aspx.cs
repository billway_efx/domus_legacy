﻿using EfxFramework.PaymentMethods.Ach;
using EfxFramework.PublicApi.Payment;
using Mb2x.ExtensionMethods;
using EfxFramework.ExtensionMethods;
using EfxFramework.PaymentMethods;
using EfxFramework.PaymentMethods.CreditCard;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Text;
using System;
using System.Net;
using System.Net.Mail;
using EfxFramework;

namespace Domus
{
    public partial class X_CASHProcessManual : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ProcessGroupCASHAch();

            Label1.Text = "Process Complete";
        }


        //cakel: BUGID00266 - File for Scheduler -- Added class file for Payment Scheduler to run Batch Cash(PayNearMe) payments for properties who want batch payments
        public static void ProcessGroupCASHAch()
        {

            DataSet mySet = GroupPaymentACHDataSet();
            //test to make sure we have a dataset
            if (mySet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in mySet.Tables[0].Rows)
                {
                    string UserID = EfxSettings.AchUserId;
                    string Pw = EfxSettings.AchPassword;
                    //string AchClientID = Settings.CCSettlementAchID;

                    string rent = dr["RentAmount"].ToString();

                    //cakel: Update ACH DB with grouped payments by property
                    //RentAmount is returned by SP as a Negative amount - This will add a credit to the property
                    //cakel: Updated Cash Settlement to RPOCash
                    var ACH_Validate =
                    EfxFramework.PaymentMethods.Ach.AchService.ProcessAchPayment(
                        EfxSettings.AchUserId,
                        EfxSettings.AchPassword,
                        "4FFF451A-6B33-4EFE-AFD4-E68035CC5347",
                        (decimal)dr["RentAmount"],
                        dr["BankRoutingNumber"].ToString(),
                        dr["BankAccountNumber"].ToString(),
                        "Checking",
                        dr["PropertyName"].ToString(),
                        dr["PropertyName"].ToString(),
                        dr["StreetAddress"].ToString(),
                        "",
                        dr["City"].ToString(),
                        dr["PropertyState"].ToString(),
                        dr["PostalCode"].ToString(),
                        ""
                        );

                    //Execute SP to update records in payment table
                    int PropID = (int)dr["PropertyId"];
                    //cakel: BUGID00367
                    DateTime TransTime = (DateTime)dr["TransTimeEnd"];
                    UpdatedCCSettledFlag(PropID, TransTime);


                    //cakel: Updated //dr["CashDepositAccountACHClientID"].ToString(),
                }
            }
        }


        //cakel: BUGID00367 - Updated SP
        public static DataSet GroupPaymentACHDataSet()
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                SqlCommand sqlComm = new SqlCommand("usp_Payment_GetCashPaymentsForGroupAchbyProperty_0512", conn);
                sqlComm.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;
                da.Fill(ds);
            }
            return ds;
        }

        //cakel: BUGID00367 - Updated SP
        //Updates records by property in Payment table by Property ID
        public static void UpdatedCCSettledFlag(int id, DateTime TransTime)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Payment_UpdateGroupCashSettledFlagByPropertyID_0512", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ID", SqlDbType.Int).Value = id;
            com.Parameters.Add("@TransTimeEnd", SqlDbType.DateTime).Value = TransTime;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //con.Close();
                throw ex;
            }
            //cakelBUGID00280
            finally
            {
                con.Close();
            }

        }

        //cakel - Connection string to EFX DB
        private static string ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }



    }
}