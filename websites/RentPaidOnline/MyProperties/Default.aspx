﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Domus.MyProperties.Default" EnableEventValidation="False" %>
<%@ Import Namespace="EfxFramework.ExtensionMethods" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script> $(function () { EFXClient.SetSelectedNavigation('properties-link'); }); </script>
    <div id="PropertyHome">
        <div class="add-property-wrap float-wrap green-gradient-wrap">
            <a href="NewProperty.aspx" class="green-button">New Property <span class="button-icon">+</span></a>
            <div class="float-right">
                <div class="float-right" style="margin-left: 10px;">
                    <asp:Button ID="ClearButton" runat="server" CssClass="green-button float-right" OnClick="ClearButtonClick" Text="Clear" style="position: relative;left: 5px;" />
                    <asp:Button ID="SearchButton" runat="server" CssClass="search-button float-right" OnClick="SearchButtonClick" Text="Search" />
                    <asp:TextBox CssClass="float-right" ID="SearchTextbox" runat="server" Width="200px"></asp:TextBox>
                </div>
                <label>Sort by:</label>
                <asp:DropDownList ID="SortByDropdown" runat="server" AutoPostBack="true" OnSelectedIndexChanged="SortByDropdownSelectedIndexChanged">
                    <asp:ListItem Text="-- No Sort --" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="A-Z" Value="ASC"></asp:ListItem>
                    <asp:ListItem Text="Z-A" Value="DESC"></asp:ListItem>
                </asp:DropDownList>
                <label>Filter by:</label>
                <asp:DropDownList ID="FilterByDropdown" runat="server" AutoPostBack="true" OnSelectedIndexChanged="FilterByDropdownSelectedIndexChanged">
                    <asp:ListItem Text="-- No Filter --" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="Active" Value="Active"></asp:ListItem>
                    <asp:ListItem Text="Not Active" Value="InActive"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <asp:Repeater ID="PropertiesRepeater" runat="server">
            <HeaderTemplate><div id="PropertyList" class="green-gradient-wrap"></HeaderTemplate>
            <FooterTemplate></div></FooterTemplate>
            <ItemTemplate>
                <div class="property-listitem" style="width: 292px;">
                    <div class="property-wrap" style="min-height: 220px;">
                        <h4><%# Eval("PropertyName") %></h4>
                        <div class="propert-right">
                            <p><%# Eval("DisplayAddress") %></p>
                        </div>
                        <div class="propert-left">
                            <img src="<%# string.Concat("/Handlers/EFXAdminHandler.ashx?RequestType=0&photoIndex=1&propertyId=", Eval("PropertyId")) %>" alt="Picture" class="orange" />
                        </div>
                        <div class="clear padding-top">
                            <p class="padding-bottom"><%# EfxFramework.Property.GetIntegrationStatus((Eval("PropertyId") as int?)) %></p>
                            <p class="padding-bottom">Main Contact: <%# Eval("MainContactDisplayName") %></p>
                            <p>Phone: <%# Eval("MainPhoneNumber").ToString().FormatPhoneNumber() %></p>
                        </div>
                        <div class="float-wrap" style="position: absolute;bottom: 10px;width: 260px;">
                            <asp:ImageButton OnClientClick="return confirm('Are you sure you want to delete this property?');" ImageUrl="/Images/Trash.png" runat="server" CssClass="float-left" ID="DeleteProperty" CommandArgument='<%# Eval("PropertyId") %>' CommandName="Delete" OnCommand="PropertyCommand" />
                            <a href="PropertyDetails.aspx?propertyId=<%# Eval("PropertyId") %>" class="float-right"><img src="/Images/magnifier.png" alt="View Details" /></a>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:PlaceHolder ID="NoPropertiesSection" runat="server" Visible="false">
            <div class="empty-records">
                There are no properties.
            </div>
        </asp:PlaceHolder>
        <div class="add-property-wrap float-wrap green-gradient-wrap">
            <a href="NewProperty.aspx" class="green-button">New Property <span class="button-icon">+</span></a>
            <asp:Panel ID="ViewMorePanel" runat="server" Visible="false">
            <div class="float-right">
                <a href="#" class="green-button">View More <span style="color: #fff; font-weight: bold;">&gt;</span></a>
            </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
