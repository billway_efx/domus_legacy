﻿using EfxFramework;
using EfxFramework.Interfaces;
using EfxFramework.Interfaces.UserControls;
using EfxFramework.Interfaces.UserControls.ImageAdder;
using EfxFramework.Interfaces.UserControls.Property;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace Domus.MyProperties
{
    public partial class PropertyDetails : BasePage, IPropertyEdit, ICallbackEventHandler
    {
        // Patrick Whittingham - task #411 - 8/24/15 : change clientclick message
        public string ClientClickMsg = "";

        private EfxFramework.Presenters.PropertyEdit _Presenter;
        private int? _CurrentPropertyId;
        protected string ConvenienceFee { get; set; }

        public EventHandler SavePropertyDetailsClicked { set { SavePropertyDetailsButton.Click += value; } }
        public EventHandler SaveBankingInfoClicked { set { SaveBankingInfoButton.Click += value; } }
        public IPropertyDetailsAddEdit PropertyDetailInfo { get { return PropertyDetailsControl; } }
        public IImageAdder ImageAdder { get { return ImageAdderControl; } }
        public IPmsAddEdit PmsSelection { get { return PmsControl; } }
        public IProgramInformationAddEdit ProgramInformation { get { return ProgramInformationControl; } }
        public IBankingInformationAddEdit BankingInformation { get { return BankingInformationControl; } }
        public string NameTabText { set { NameTabLabel.Text = value; } }
        public bool IsPropertyManagerSite { get { return false; } }
        public Page ParentPage { get { return this; } }
        public PlaceHolder SuccessMessagePlaceHolder { get { return bankingSuccessDiv; } }

        public int CurrentPropertyId
        {
            get
            {
                if (_CurrentPropertyId.HasValue)
                    return _CurrentPropertyId.Value;

                _CurrentPropertyId = Request.QueryString["propertyId"].ToNullInt32();

                if (!_CurrentPropertyId.HasValue)
                    _CurrentPropertyId = 0;

                return _CurrentPropertyId.Value;
            }

            set { _CurrentPropertyId = value; }
        }

        public PropertyStaff CurrentStaff { get { return new PropertyStaff(HttpContext.Current.User.Identity.Name.ToInt32()); } }

        protected void Page_Load(object sender, EventArgs e)
        {

            SaveBankingInfoButton.PostBackUrl = "~/MyProperties/PropertyDetails.aspx?propertyId=" + CurrentPropertyId + "&tab=BankingTab";
            SuccessMessagePlaceHolder.Visible = false;

            //CMallory - Task 00593 - Changed how the property variable gets instantiated and added if statement for the DeactivatedHeader div.
            Property property = new Property(CurrentPropertyId);
            if (property.IsDeleted == true)
            {
                DeactivatedHeader.Visible = true;
            }

            if (!EfxFramework.Helpers.Helper.IsRpoAdmin)
            {

                using (var entity = new RPO.RPOEntities())
                {
                    //var property = entity.Properties.First(p => p.PropertyId == CurrentPropertyId);
                    var staff = entity.PropertyStaffs.First(s => s.PropertyStaffId == EfxFramework.Helpers.Helper.CurrentUserId);
                    if (EfxFramework.Helpers.Helper.IsCorporateAdmin)
                    {
                        if (!staff.CompanyId.HasValue || property.CompanyID != staff.CompanyId.Value)
                        {
                            Response.Redirect("/");
                        }
                    }
                    else if (!entity.PropertyPropertyStaffs.Any(ps => ps.PropertyStaffId == EfxFramework.Helpers.Helper.CurrentUserId && ps.PropertyId == CurrentPropertyId))
                    {
                        Response.Redirect("/");
                    }
                }
                //cakel: TASK 00522
                RpoAdminOnlyUpdateArea.Visible = false;
            }

            // patrick whittingham - task #411 - 8/24/15 : change clientclick message
            // salcedo - task #411 - restrict functionality to Power Admins only
            //if (EfxFramework.Helpers.Helper.IsRPOAdmin || EfxFramework.Helpers.Helper.IsCorporateAdmin)
            if (EfxFramework.Helpers.Helper.IsRpoAdmin)
            {
                DeleteIcon.OnClientClick = "return confirm('You are about to Deactivate and Delete This Property, are you sure you want to proceed?')";
            }
            else
            {
                DeleteIcon.OnClientClick = "return alert('Please Contact Domus Customer Service')";
            }
            var CsManager = Page.ClientScript;
            var CbReference = CsManager.GetCallbackEventReference(this, "arg", "ReceiveServerData", "context");
            var CbScript = String.Format("function CallServer(arg, context) {0} {1}; {2}", "{", CbReference, "}");

            CsManager.RegisterClientScriptBlock(GetType(), "CallServer", CbScript, true);
            _Presenter = new EfxFramework.Presenters.PropertyEdit(this);

            if (!Page.IsPostBack)
            {
                _Presenter.IntitializeValues();

                //Salcedo - 2/13/2015 - task # 00366 add support for Custom Property Maintenance System
                LoadPropertyMaintenanceSystemData();
            }
        }

        public void RaiseCallbackEvent(string eventArgument)
        {
            ConvenienceFee = _Presenter.CalculateConvenienceFee(eventArgument.Split('|'));
        }

        public string GetCallbackResult()
        {
            return ConvenienceFee;
        }

        //Salcedo - 2/12/2015 - added new local handler so we don't have to dive into lower level user controls, classes, etc. to add something simple
        protected void SavePropertyDetailsButton_Click(object sender, EventArgs e)
        {
            //Salcedo - 2/13/2015 - task # 00366 add support for Custom Property Maintenance System
            SavePropertyMaintenanceSystemData();

            //Note: When this function is complete, program logic will automatically flow to the SavePropertyDetails(object sender, EventArgs e) function
            //in \websites\EfxFramework\Presenters\PropertyEdit.cs. 
        }

        //Salcedo - 2/13/2015 - task # 00366 add support for Custom Property Maintenance System
        private void SavePropertyMaintenanceSystemData()
        {
            if (_CurrentPropertyId.HasValue)
            {
                MiscDatabase.PropertyMaintenanceSystem_SetSingleObject(_CurrentPropertyId.Value, UseNativeMaintenanceSystem.Checked,
                    UseExternalMaintenanceSystem.Checked, ExternalMaintenanceSiteUrl.Value);

                //Necessary to load the data again, to set the correct state for the related controls
                LoadPropertyMaintenanceSystemData();
            }
        }

        //Salcedo - 2/13/2015 - task # 00366 add support for Custom Property Maintenance System
        private void LoadPropertyMaintenanceSystemData()
        {
            if (_CurrentPropertyId.HasValue)
            {
                SqlDataReader reader = MiscDatabase.GetPropertyMaintenanceSystemForProperty(_CurrentPropertyId.Value);
                reader.Read();
                if (reader.HasRows)
                {
                    UseNativeMaintenanceSystem.Checked = (bool)reader["UseNativeMaintenanceSystem"];
                    UseExternalMaintenanceSystem.Checked = (bool)reader["UseExternalMaintenanceSystem"];
                    ExternalMaintenanceSiteUrl.Value = reader["ExternalMaintenanceSiteUrl"].ToString();

                    UseExternalMaintenanceSystem.Enabled = !UseNativeMaintenanceSystem.Checked;
                    ExternalMaintenanceSiteUrl.Disabled = !UseExternalMaintenanceSystem.Checked;

                    reader.Close();
                }
                else
                {
                    UseNativeMaintenanceSystem.Checked = true;
                    UseExternalMaintenanceSystem.Checked = false;
                    UseExternalMaintenanceSystem.Enabled = false;
                    ExternalMaintenanceSiteUrl.Disabled = true;
                }
            }
        }

        // Patrick Whittingham - 3/30/2015 - task# 000197 : Show delete property dialog box and delete property
        protected void DeleteIcon_Click(object sender, EventArgs e)
        {
            using (var entity = new RPO.RPOEntities())
            {
                if (EfxFramework.Helpers.Helper.IsRpoAdmin)
                {
                    var property = entity.Properties.First(p => p.PropertyId == CurrentPropertyId);
                    property.IsDeleted = true;
                    entity.SaveChanges();
                    Response.Redirect("/rpo/properties");
                }
                else
                {
                    var userProperties = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyStaffId == EfxFramework.Helpers.Helper.CurrentUserId).ToList();
                    if (userProperties.Any(p => p.PropertyId == CurrentPropertyId))
                    {
                        var property = entity.Properties.First(p => p.PropertyId == CurrentPropertyId);
                        property.IsDeleted = true;
                        Response.Redirect("/rpo/properties");
                    }
                }
            }
        }
    }
}