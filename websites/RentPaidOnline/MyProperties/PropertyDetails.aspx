﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="PropertyDetails.aspx.cs" Inherits="Domus.MyProperties.PropertyDetails" EnableEventValidation="False" %>

<%@ Register Src="~/UserControls/Property/Applications_V2.ascx" TagPrefix="uc1" TagName="Applications_V2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <title>Property Details</title>
    <link href="/Content/UserControls.css" rel="stylesheet" type="text/css" />
    <link href="/Content/PropertyDetailSpecific.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function CalculateConvenienceFee() {
            var avgRent = document.getElementById("MainContent_PropertyDetailsControl_AverageRentTextbox").value;
            var e = document.getElementById("MainContent_ProgramInformationControl_ProgramDropdown");
            var programId = e.options[e.selectedIndex].value;

            CallServer(avgRent + "|" + programId, "");
        }

        function ReceiveServerData(convenienceFee) {
            document.getElementById("MainContent_PropertyDetailsControl_ConvenienceFeeTextbox").value = convenienceFee;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div id="propertyManager" class="main-container" ng-controller="PropertyController">

        <!-- Begin Details Panel -->
        <div class="details-panel">
            <div class="inner" style="position: relative;">
                <a href="{{urlPrefix}}/rpo/properties" class="back-to-listings" target="_self"><span data-icon="&#x5f" aria-hidden="true"></span>Back to Listings</a>
                <div class="details-panel-content">
                    <loading></loading>
                    <!-- CMallory - Task 00593 - Added div -->
                    <div runat="server" visible="false" id="DeactivatedHeader">
                    <h1>This property has been deactivated</h1>
                    </div>
                    <div class="col-third">
                        <p ng-cloak>Managing Company: <span class="value" ng-bind="property.CompanyName"></span></p>
                        <!--cakel: BUGID00069 Changed from Participating Units: to Units: -->
                        <p ng-cloak>Units: <span class="value" ng-bind="property.NumberOfUnits"></span><p/>
                        <p ng-cloak>Type(s) of Billing: <span class="value" ng-bind="property.BillingTypes"></span><p/>
                    </div>

                    <div class="col-third">
                        <p ng-cloak>Integration Type: <span class="value" ng-bind="property.PmsTypeName"></span></p>
                        <p ng-cloak>Main Contact: <span class="value" ng-bind="property.MainContactName"></span></p>
                        <p ng-cloak>Phone: <span class="value" ng-bind="property.MainPhoneNumber"></span></p>
                    </div>

                    <div class="col-third emphasized balance-col">
                        <!-- cakel: BUGID00069 Commented out html as per Nick
                        <p ng-cloak>Participating Renters: <span class="value"></span></p>
                            -->
                        <!--cakel: BUGID00069 Updated Leasing AgentS: to Property Admin(s): -->
                        <p ng-cloak>Property Admin(s): <span class="value" ng-bind="property.NumberOfLeasingAgents"></span></p>
                        <!-- task#: joselist-1a - Patrick Whittingham: 7/20/15: add property staff list  -->
                        <div id="PropertyStaffTab">
                             <uc1:PropertiesStaff runat="server" ID="PropertiesStaff" />
                        </div>
                        <p ng-if="property.RentersPastDue">Renters Past Due</p>
					</div>
                </div>

                <div class="details-panel-actions">
                    <%--<a href="#"><span data-icon="&#x24" aria-hidden="true"></span>Edit Account</a>
                    <a href="#"><span data-icon="&#x50" aria-hidden="true" ng-click="toggleActive()"></span>Disable Account</a>--%>
                    <%-- <a href="#"><span data-icon="&#x2b" aria-hidden="true" ng-click="deleteProperty()"></span>Remove Account</a> DeleteIcon_Click   --%>
                    <%-- Patrick Whittingham - 3/30/2015 - task# 000197 : Show delete property dialog box and delete property   --%>
                    <%--  Patrick Whittingham - task #411 - 8/24/15 : change clientclick message    --%>
                    <%--   OnClientClick="return confirm('You are about to Deactivate and Delete This Property, are you sure you want to proceed?')" --%>
                    <asp:LinkButton runat="server" ID="DeleteIcon" OnClientClick="" OnClick="DeleteIcon_Click"><span data-icon="&#x2b" aria-hidden="true"></span></asp:LinkButton>
                </div>
            </div>

        </div>

        <!-- Begin Tabs  -->
        <div id="tabs">
            <ul id="tabLinks" class="nav nav-tabs">
                <li ng-class="{active: activeTab == 'NameTab'}"><a href="#NameTab" ng-click="showTab('NameTab')">
                    <asp:Literal ID="NameTabLabel" runat="server" /></a></li>
                <li ng-class="{active: activeTab == 'BankingTab'}"><a href="#BankingTab" ng-click="showTab('BankingTab')">Banking</a></li>
                <!--cakel: Updated Code to hide tab -->
                <li ng-class="{active: activeTab == 'AnnouncementTab'}" ng-show="hideme"><a href="#AnnouncementTab" ng-click="showTab('AnnouncementTab')">Announcements</a></li>
                <li ng-class="{active: activeTab == 'RentAndFeesTab'}"><a href="#RentAndFeesTab" ng-click="showTab('RentAndFeesTab')">Rent & Fees</a></li>
                <li ng-class="{active: activeTab == 'ApplicationTab'}"><a href="#ApplicationTab" ng-click="showTab('ApplicationTab')">Applications</a></li>
            </ul>

            <div id="NameTab">
                <uc1:PropertyDetailsAddEdit ID="PropertyDetailsControl" runat="server" />

                <h2>Property Photo</h2>
                <uc1:ImageAdder ID="ImageAdderControl" runat="server" MaxNumberOfPhotos="4" />

                <!--cakel: TASK 00522 -->
                <div id="RpoAdminOnlyUpdateArea" runat="server">

                <div class="formTint">
                    <uc1:PmsAddEdit ID="PmsControl" runat="server" />
                </div>

                <h2>Programs</h2>
                <uc1:ProgramInformationAddEdit ID="ProgramInformationControl" runat="server" />

                <!-- Salcedo - 2/13/2015 - task # 00366 - added support for external property maintenance systems -->
                <div class="formTint" style="padding-top:16px; padding-bottom: 30px;">
                    <h2>Property Maintenance</h2>
                    <div class="formWhole" style="margin-bottom: 10px; margin-top: 20px;">
                        <div style="float:left; width: 50%;">
                            <asp:CheckBox ID="UseNativeMaintenanceSystem" runat="server" Text="&nbsp; Use default maintenance system" OnChange="javascript:enableCheckBox();"/>
                        </div>
                        <div style="float:left; width: 50%;">
                            <asp:CheckBox ID="UseExternalMaintenanceSystem" runat="server" Text="&nbsp; Enable support for custom maintenance system" OnChange="javascript:enableTextBox();"/>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="formWhole">
                        <label>Custom Maintenance Site Address</label>
                        <input runat="server" id="ExternalMaintenanceSiteUrl" name="ExternalMaintenanceSiteUrlTextbox" type="text" class="form-control" placeholder="http://www.yoursite.com" />
                    </div>
                </div>

                </div><!--END RpoAdminOnlyUpdateArea -->
                
                <div class="button-footer">
                    <div class="main-button-action">
                        <!-- CMallory - Layout Change - Added btn-footer css tag to button -->
                        <asp:LinkButton ID="SavePropertyDetailsButton" runat="server" CssClass="btn btn-primary btn-footer" OnClick="SavePropertyDetailsButton_Click">Save</asp:LinkButton>
                    </div>
                    <div class="clearFix"></div>
                </div>
            </div>
            <div id="BankingTab">
				<asp:ValidationSummary ID="BankingInfoSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="Banking" />
				<asp:PlaceHolder ID="bankingSuccessDiv" runat="server" Visible="false"><div class="alert alert-success" id="successDiv"><b>Success!</b></div></asp:PlaceHolder>
                <uc1:BankingInformationAddEdit ID="BankingInformationControl" runat="server" />
                <div class="button-footer">
                    <div class="main-button-action">
                        <!-- CMallory - Layout Change - Added btn-footer css tag to button -->
                        <asp:LinkButton ID="SaveBankingInfoButton" runat="server" CssClass="btn btn-primary btn-footer">Save</asp:LinkButton>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div id="AnnouncementTab">
                <uc1:AdminAnnouncements runat="server" ID="AdminAnnouncements" />
            </div>


            <div id="ApplicationTab">
                <uc1:Applications_V2 runat="server" ID="Applications_V2" />
                <%-- cakel: 00546 hiding old user control --%>
               <%-- <uc1:Applications ID="Applications" runat="server" />--%>
            </div>


            <div id="RentAndFeesTab">
                <uc1:RentAndFees runat="server" ID="RentAndFees" />
            </div>


        </div>
    </div>

	<script src="/Scripts/efx.js" type="text/javascript"></script>
	<script src="/Scripts/jquery.mask.js" type="text/javascript"></script>
    <%-- cakel: 00546 --%>
<%--    <script>
        $(document).ready(function () {
            /* Mask */
            ApplySingleMask("#<%=Applications.BillingInformationControl.PhoneNumberClientId%>", "(000) 000-0000");
            ApplySingleMask("#<%=Applications.BillingInformationControl.ZipCodeClientId%>", "00000"); 
        });

    </script>--%>
    
    <!-- Salcedo - 2/13/2015 - task # 00366 - added support for external property maintenance systems -->
    <script type="text/javascript">
        function enableTextBox() {
            var textBoxID = "<%= ExternalMaintenanceSiteUrl.ClientID %>";
        if (document.getElementById("<%= UseExternalMaintenanceSystem.ClientID %>").checked == true)
            document.getElementById(textBoxID).disabled = false;
        else
            document.getElementById(textBoxID).disabled = true;
    }
    </script>
    <script type="text/javascript">
        function enableCheckBox() {
            var checkBoxID = "<%= UseExternalMaintenanceSystem.ClientID %>";
            var textBoxID = "<%= ExternalMaintenanceSiteUrl.ClientID %>";
            if (document.getElementById("<%= UseNativeMaintenanceSystem.ClientID %>").checked == true) {
                document.getElementById(checkBoxID).disabled = true;
                document.getElementById(checkBoxID).checked = false;
                document.getElementById(textBoxID).disabled = true;
            }
            else {
                document.getElementById(checkBoxID).disabled = false;
            }
        }

        // Patrick Whittingham - 4/14/2015 - task # 000172 - validate account numbers
        $("#MainContent_BankingInfoSummary").css("display", "none");
        var gv_error = "", gv_err1 = "", gv_err2 = "", gv_err3 = "", gv_err4 = "";

        $("#MainContent_BankingInformationControl_RentalAccountNumberTextbox").change(function () {
            var v_value = $("#MainContent_BankingInformationControl_RentalAccountNumberTextbox").val();
            if ( isNaN(v_value) == true ) {
                $("#MainContent_BankingInfoSummary").css("display", "block");
                gv_err1 = "Please enter a number for the eCheck Deposits Account Number without any asterisks or characters.<br /> ";
            } else {
                gv_err1 = "";
            }
            gv_error = gv_err1 + gv_err2 + gv_err3 + gv_err4;
            $("#MainContent_BankingInfoSummary").html(gv_error);
            if (gv_error == "") { $("#MainContent_BankingInfoSummary").css("display", "none");  }
        });

        $("#MainContent_BankingInformationControl_SecurityAccountNumberTextbox").change(function () {
            var v_value = $("#MainContent_BankingInformationControl_SecurityAccountNumberTextbox").val();
            if (isNaN(v_value) == true) {
                $("#MainContent_BankingInfoSummary").css("display", "block");
                gv_err2 = "Please enter a number for the Billing Account Account Number without any asterisks or characters.<br /> ";
            } else {
                gv_err2 = "";
            }
            gv_error = gv_err1 + gv_err2 + gv_err3 + gv_err4;
            $("#MainContent_BankingInfoSummary").html(gv_error);
            if (gv_error == "") { $("#MainContent_BankingInfoSummary").css("display", "none"); }
        });

        $("#MainContent_BankingInformationControl_FeeAccountNumberTextbox").change(function () {
            var v_value = $("#MainContent_BankingInformationControl_FeeAccountNumberTextbox").val();
            if (isNaN(v_value) == true) {
                $("#MainContent_BankingInfoSummary").css("display", "block");
                gv_err3 = "Please enter a number for the Applicant Portal Account Account Number without any asterisks or characters.<br /> ";
            } else {
                gv_err3 = "";
            }
            gv_error = gv_err1 + gv_err2 + gv_err3 + gv_err4;
            $("#MainContent_BankingInfoSummary").html(gv_error);
            if (gv_error == "") { $("#MainContent_BankingInfoSummary").css("display", "none"); }
        });

        $("#MainContent_BankingInformationControl_CCDepositAccountNumberTextbox").change(function () {
            var v_value = $("#MainContent_BankingInformationControl_CCDepositAccountNumberTextbox").val();
            if (isNaN(v_value) == true) {
                $("#MainContent_BankingInfoSummary").css("display", "block");
                gv_err4 = "Please enter a number for the Credit Card Deposits Account Number without any asterisks or characters.<br /> ";
            } else {
                gv_err4 = "";
            }
            gv_error = gv_err1 + gv_err2 + gv_err3 + gv_err4;
            $("#MainContent_BankingInfoSummary").html(gv_error);
            if (gv_error == "") { $("#MainContent_BankingInfoSummary").css("display", "none"); }
        });

    </script>

</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="Scripts">
<script src="<%= ResolveClientUrl("~/Scripts/promise-tracker.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/rpo-property-details.js") %>"></script>
    
</asp:Content>
