﻿using EfxFramework;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;

namespace Domus.MyProperties
{
	public partial class Default : BasePage
	{
		const string CacheSortKey = "CURRENT_PROPERYHOME_SORT";
		const string CacheFilterKey = "CURRENT_PROPERYHOME_FILTER";
		const string CacheSearchKey = "CURRENT_PROPERYHOME_SEARCH";

		string _Sort = string.Empty;
		string CurrentSort
		{
			get
			{
				if (_Sort.Equals(string.Empty) && ViewState[CacheSortKey] != null)
				{
					_Sort = ViewState[CacheSortKey].ToString();
				}

				return _Sort;
			}
			set
			{
				ViewState[CacheSortKey] = value;
			}
		}

		string _Filter = string.Empty;
		string CurrentFilter
		{
			get
			{
				if (_Filter.Equals(string.Empty) && ViewState[CacheFilterKey] != null)
				{
					_Filter = ViewState[CacheFilterKey].ToString();
				}

				return _Filter;
			}
			set
			{
				ViewState[CacheFilterKey] = value;
			}
		}

		string _Search = string.Empty;
		string CurrentSearch
		{
			get
			{
				if (_Search.Equals(string.Empty) && ViewState[CacheSearchKey] != null)
				{
					_Search = ViewState[CacheSearchKey].ToString();
				}

				return _Search;
			}
			set
			{
				ViewState[CacheSearchKey] = value;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			// Get data            
			var Data = String.IsNullOrEmpty(CurrentSearch)
							? Property.GetAllPropertyList()
							: Property.GetAllPropertyList().Where(c => c.PropertyName.ToLower().IndexOf(CurrentSearch.ToLower()) >= 0).ToList();

			// Filter
			if (!string.IsNullOrEmpty(CurrentFilter))
			{
				if (CurrentFilter.Equals("Active"))
				{
					Data = Data.Where(d => !d.IsDeleted.Value).ToList();
				}
				else if (CurrentFilter.Equals("InActive"))
				{
					Data = Data.Where(d => d.IsDeleted.Value).ToList();
				}
			}

			// SORTING
			if (!string.IsNullOrEmpty(CurrentSort) && CurrentSort.Equals("ASC"))
			{
				Data = Data.OrderBy(p => p.PropertyName).ToList();
			}
			else if (!string.IsNullOrEmpty(CurrentSort) && CurrentSort.Equals("DESC"))
			{
				Data = Data.OrderByDescending(p => p.PropertyName).ToList();
			}

			PropertiesRepeater.DataSource = Data;
			PropertiesRepeater.DataBind();
			PropertiesRepeater.Visible = Data.Count > 0;
			NoPropertiesSection.Visible = Data.Count == 0;
		}

		protected void SortByDropdownSelectedIndexChanged(object sender, EventArgs e)
		{
			CurrentSort = SortByDropdown.SelectedValue != "-1"
				? SortByDropdown.SelectedValue
				: string.Empty;
		}

		protected void FilterByDropdownSelectedIndexChanged(object sender, EventArgs e)
		{
			CurrentFilter = FilterByDropdown.SelectedValue != "-1"
				? FilterByDropdown.SelectedValue
				: string.Empty;
		}

		protected void SearchButtonClick(object sender, EventArgs e)
		{
			Property.GetAllPropertyList();
			var Term = SearchTextbox.Text.Trim().ToLower();

			CurrentSearch = Term;
		}

		protected void ClearButtonClick(object sender, EventArgs e)
		{
			CurrentSearch = string.Empty;
			SearchTextbox.Text = string.Empty;
		}

		protected void PropertyCommand(object sender, CommandEventArgs e)
		{
            var CurrentPropertyId = e.CommandArgument.ToString().ToNullInt32();

            if (!CurrentPropertyId.HasValue || CurrentPropertyId.Value <= 0)
            {
                PageMessage.DisplayMessage(SystemMessageType.Error, "Property not valid for deletion.");
                return;
            }

            if (!String.Equals(e.CommandName, "delete", StringComparison.OrdinalIgnoreCase))
            {
                PageMessage.DisplayMessage(SystemMessageType.Error, "Unable to perform the specified action.");
                return;
            }

            try
            {
                Property.DeletePropertyByPropertyId(CurrentPropertyId.Value);
            }
            catch (SqlException Ex)
            {
                if (Ex.Number == 547)
                    PageMessage.DisplayMessage(SystemMessageType.Error, "You cannot delete a property that has transactions associated with it.");
                else
                    throw;
            }
		}
	}
}