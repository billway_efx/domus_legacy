﻿using System.Web.UI;
using EfxFramework.Interfaces;
using EfxFramework.Interfaces.UserControls.ImageAdder;
using EfxFramework.Interfaces.UserControls.Property;
using EfxFramework.Web;
using System;
using System.Web.UI.WebControls;
using System.Web;
using Mb2x.ExtensionMethods;
using EfxFramework.Helpers;
using EfxFramework;
using System.Collections.Generic;

namespace Domus.MyProperties
{
    public partial class NewProperty : BasePage, IPropertyAdd, ICallbackEventHandler
    {
        private EfxFramework.Presenters.PropertyAdd _presenter;
        protected string ConvenienceFee { get; set; }

        public EventHandler SaveButtonClick { set { SaveButton.Click += value; } }
        public MultiView NewPropertySteps { get { return NewPropertyMultiView; } }
        public View StepOne { get { return StepOneView; } }
        public View StepTwo { get { return StepTwoView; } }
        public IPropertyDetailsAddEdit PropertyDetailInfo { get { return PropertyDetailsControl; } }
        public IImageAdder ImageAdder { get { return ImageAdderControl; } }
        public IBankingInformationAddEdit BankingInformation { get { return BankingInformationAddEditControl; } }
        public IPmsAddEdit PmsSelection { get { return PmsAddEditControl; } }
        public IProgramInformationAddEdit ProgramInformation { get { return ProgramInformationControl; } }
        //public IAddStaffToProperty AddStaffToProperty { get { return AddStaffToPropertyControl; } }
        public int CurrentPropertyId { get; set; }
		public Page ParentPage { get { return this; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            var CsManager = Page.ClientScript;
            var CbReference = CsManager.GetCallbackEventReference(this, "arg", "ReceiveServerData", "context");
            var CbScript = String.Format("function CallServer(arg, context) {0} {1}; {2}", "{", CbReference, "}");

            CsManager.RegisterClientScriptBlock(GetType(), "CallServer", CbScript, true);
            _presenter = new EfxFramework.Presenters.PropertyAdd(this);

            if (!Page.IsPostBack)
            {
                _presenter.InitializeValues();

                //Salcedo - 2/22/2014 - verify access
                if (!EfxFramework.Helpers.Helper.IsRpoAdmin)
                {
                    SafeRedirect("~/Account/Login.aspx");
                }
            }
        }

        public void RaiseCallbackEvent(string eventArgument)
        {
            ConvenienceFee = _presenter.CalculateConvenienceFee(eventArgument.Split('|'));
        }

        public string GetCallbackResult()
        {
            return ConvenienceFee;
        }
    }
}