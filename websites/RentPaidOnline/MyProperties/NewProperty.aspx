﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="NewProperty.aspx.cs" Inherits="Domus.MyProperties.NewProperty" EnableEventValidation="False" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <title>Add a New Property</title>
    <link href="/Content/UserControls.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function CalculateConvenienceFee() {
            var avgRent = document.getElementById("MainContent_PropertyDetailsControl_AverageRentTextbox").value;
            var e = document.getElementById("MainContent_ProgramInformationControl_ProgramDropdown");
            var programId = e.options[e.selectedIndex].value;

            CallServer(avgRent + "|" + programId, "");
        }

        function ReceiveServerData(convenienceFee) {
            document.getElementById("MainContent_PropertyDetailsControl_ConvenienceFeeTextbox").value = convenienceFee;
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="main-container">
        <h1>Add New Property</h1>
		<asp:ValidationSummary ID="bankingSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="Banking" />
		<asp:PlaceHolder ID="successMsg" runat="server" Visible="false"><div class="alert alert-success" id="successDiv"><b>Success!</b> An email has been sent with your new temporary password.</div></asp:PlaceHolder>
        <asp:MultiView ID="NewPropertyMultiView" runat="server">
            <asp:View ID="StepOneView" runat="server">
                

                <uc1:PropertyDetailsAddEdit ID="PropertyDetailsControl" runat="server" />

                <uc1:ImageAdder ID="ImageAdderControl" runat="server" MaxNumberOfPhotos="4" />

                <uc1:BankingInformationAddEdit ID="BankingInformationAddEditControl" runat="server" />
                
                <uc1:PmsAddEdit ID="PmsAddEditControl" runat="server" />

                <uc1:ProgramInformationAddEdit ID="ProgramInformationControl" runat="server" />

                <div class="button-footer">
                    <div class="main-button-action">
                        <!--CMallory - Layout Change - Added btn-footer CSS class to tag -->
                        <asp:LinkButton ID="SaveButton" runat="server" CssClass="btn btn-primary btn-footer">Save</asp:LinkButton>
                    </div>
                    <div class="clear"></div>
                </div>
            </asp:View>

            <asp:View ID="StepTwoView" runat="server">
                <div class="step-progress two-step">
                    <div class="step complete">
                        <h2>Step 1</h2>
                        <h3>Property Information</h3>
                    </div>
                    <div class="step active">
                        <h2>Step 2</h2>
                        <h3>Add Staff</h3>
                    </div>
                </div>
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>