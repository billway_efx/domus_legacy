﻿using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Text;
using System;
using System.Net;
using System.Net.Mail;

using EfxFramework;

namespace Domus
{
    public partial class X_SettlementTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }


        public static void ProcessGroupCCAch()
        {

            DataSet mySet = GroupPaymentACHDataSet();
            //test to make sure we have a dataset
            if (mySet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in mySet.Tables[0].Rows)
                {
                    string UserID = EfxSettings.AchUserId;
                    string Pw = EfxSettings.AchPassword;
                    //string AchClientID = Settings.CCSettlementAchID;

                    string rent = dr["RentAmount"].ToString();

                    //Salcedo - 4/11/2015 - we should not hard code this value
                    string PNMACHClientId = EfxSettings.PnmachClientId;



                    //Get the descriptor from the merchant table for this property
                    string CashDepositAccountACHClientID = dr["CashDepositAccountACHClientID"].ToString();

                    string FirstName = "RBC Set";
                    string LastName = "Domus";
             


                    decimal RentAmount = (decimal)dr["RentAmount"];
                    //var ACH_Validate =
                    //EfxFramework.PaymentMethods.Ach.AchService.ProcessAchPayment(
                    //    Settings.AchUserId,
                    //    Settings.AchPassword,

                    //    PNMACHClientId,
                    //    RentAmount,
                    //    dr["BankRoutingNumber"].ToString(),
                    //    dr["BankAccountNumber"].ToString(),
                    //    "Checking",
                    //    FirstName, 
                    //    LastName,
                    //    dr["StreetAddress"].ToString(),
                    //    "",
                    //    dr["City"].ToString(),
                    //    dr["PropertyState"].ToString(),
                    //    dr["PostalCode"].ToString(),
                    //    ""
                    //    );



                    //Salcedo - 5/12/2015 - add error checking
                    if (rent != "0")
                    {
                        Console.WriteLine("Error creating ACH transaction. PNMACHClientId=" + PNMACHClientId + " RentAmount=" + RentAmount.ToString() +
                            " FirstName=" + FirstName + " LastName=" + LastName + " RoutingNumber=" + dr["BankRoutingNumber"].ToString() + " Error=" 
                            );
                    }
                    else
                    {
                        //cakel: BUGID00394
                        var ACH_ValResult = "";;
                        //Execute SP to update records in payment table
                        int PropID = (int)dr["PropertyId"];
                        //cakel: BUGID00367
                        DateTime TransTime = (DateTime)dr["TransTimeEnd"];
                        //cakel: BUGID00394
                        //UpdatedCCSettledFlag(PropID, TransTime, ACH_ValResult.ToString());
                        string Test = "";
                    }

                   
                }
            }
        }

        //cakel: BUGID00367 - Updated SP
        public static DataSet GroupPaymentACHDataSet()
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConnString))
            {   //cakel: BUGID0394
                //SqlCommand sqlComm = new SqlCommand("usp_Payment_GetCashPaymentsForGroupAchbyProperty_v2", conn);
                SqlCommand sqlComm = new SqlCommand("usp_Payment_GetCashPaymentsForGroupAchbyProperty_V2", conn);
                sqlComm.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;
                da.Fill(ds);
            }
            return ds;
        }

        //cakel - Connection string to EFX DB
        private static string ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ProcessGroupCCAch();
        }






    }
}