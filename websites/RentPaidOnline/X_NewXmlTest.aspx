﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="X_NewXmlTest.aspx.cs" Inherits="Domus.X_NewXmlTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <div>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx">Go to Main Site</asp:HyperLink>
                </div>
    <div>
        <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />&nbsp;
        <asp:Button ID="Method2btn" runat="server" Text="method2" OnClick="Method2btn_Click" style="height: 26px" /><br />
        <asp:Button ID="Button2" runat="server" Text="Get All Properties" OnClick="Button2_Click" />
    </div>
        <div>

            <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Height="81px" Width="718px"></asp:TextBox>


        </div>
        <div>
            <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Get Renters By Property - Import" />
            <asp:Button ID="Button4" runat="server" OnClick="Button4_Click" Text="Get Single Renter" />
            <asp:Button ID="Button5" runat="server" OnClick="Button5_Click" Text="Get Property Details" />
            <asp:Button ID="Button7" runat="server" OnClick="Button7_Click1" Text="ExportPaymentTest" />
            <br />
    


        </div>

        <div>
            <br />
            <asp:Button ID="Button6" runat="server" Text="Process Renters From All Properties - Import" OnClick="Button6_Click" />


        </div>

                <div>
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="50">
                        <ProgressTemplate>
                            <div>
                                <img src="Images/report_wait.gif" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
             </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Label ID="Label1" runat="server"></asp:Label>
    </form>
</body>
</html>

