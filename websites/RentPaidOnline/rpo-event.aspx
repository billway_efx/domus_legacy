﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="rpo-event.aspx.cs" Inherits="Domus.rpo_event" %>
<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
	<link href="<%= ResolveClientUrl("~/Static/css/fullcalendar.css")%>" rel="stylesheet" />
	<link href="<%= ResolveClientUrl("~/Static/css/fullcalendar.print.css")%>" media="print" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopHeadline" runat="server">
	<h1 id="H1" ng-controller="TitleController" ng-bind="PageTitle"></h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="main-container">
            <div ng-view></div>
        </div>
    <div class="modal fade" ng-include="'/event-details.html'" id="eventDetails"></div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="Scripts">
    
    <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/angular-ui-tinymce.js") %>"></script>
	<script src="<%= ResolveClientUrl("~/Scripts/fullcalendar.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/calendar.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/date.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/rpo-event.js") %>"></script>
</asp:Content>