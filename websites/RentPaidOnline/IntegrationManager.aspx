﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="IntegrationManager.aspx.cs" Inherits="Domus.IntegrationManager" %>

<%@ Register Assembly="Infragistics45.Web.v14.1, Version=14.1.20141.2283, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.Web.UI.GridControls" TagPrefix="ig" %>

<%--<%@ Register Assembly="Infragistics4.Web.v14.1, Version=14.1.20141.2283, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.Web.UI.GridControls" TagPrefix="ig" %>--%>

<%--<%@ Register Assembly="Infragistics4.Web.jQuery.v12.1, Version=12.1.20121.2137, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>

<%@ Register Assembly="Infragistics4.Web.v12.1, Version=12.1.20121.2137, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>--%>

<%@ Register Assembly="Infragistics4.Web.v12.1, Version=12.1.20121.2137, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.Web.UI.GridControls" TagPrefix="ig" %>
<%@ Import Namespace="EfxFramework.ExtensionMethods" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
    .hidden
    {
    display:none;
    }
        .mytext {
            display: inline;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="TopHeadline" runat="server">
    <h1>Integration Manager</h1>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="main-container">
        <div class="section-body">

            <div class="formTint" style="float: left; padding-top: 20px;">
            
                <div style="width: 40%;" runat="server" id="CompanyDiv"> <%--display: none;--%>
                    <div style="margin-bottom: 10px;">
                        <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                            <asp:ListItem>-- Select Company --</asp:ListItem>
                            <asp:ListItem>Company One</asp:ListItem>
                            <asp:ListItem>Company Two</asp:ListItem>
                            <asp:ListItem>Company Three</asp:ListItem>
                            <asp:ListItem>Company Four</asp:ListItem>
                            <asp:ListItem>Company Five</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div >
                    <div style="float: left; margin-top: 12px; width: 60%; padding-right: 12px;">
                        <label>Click the Browse button to select a new MRI file in CSV format for uploading
                        and importing into your Rent Paid Online account.</label>
                    </div>

                    <div style="float: left; width: 40%; text-align: center; padding-top: 12px; ">
                        <%--<asp:Button ID="Button1" runat="server" Text="Upload CSV File" />--%>
                        <div style="margin: auto; width: 300px;">
                        <asp:FileUpload ID="BrowseFileControl" runat="server" onchange="if (confirm('Upload ' + this.value + '?')) this.form.submit();" />
                            <asp:Label ID="lblResult" runat="server" Text="File has been uploaded" Visible="False"></asp:Label>
                            </div>
                    </div>
                    <ig:WebDataGrid ID="WebDataGrid2" runat="server" Height="350px" Width="400px">
                    </ig:WebDataGrid>
                </div>

                <div style="float: left; width: 100%; margin-top: 24px; padding-bottom: 22px;">
                    <label>Receipt File Date Range</label>
                    <div style="margin-top: 2px; border: solid; border-width: 1px; width: 66.66%; border-radius:6px; border-color:lightgrey;" >
                        
                        <div style="padding-left:12px; padding-top: 6px; padding-bottom: 6px; margin: 12px;">
                            <label>Start:</label> <asp:TextBox ID="DateTextboxStart" runat="server" CssClass="m-wrap span12 date form_datetime text form-control date-select mytext" Width="26%" ></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
                            <label>End:</label> <asp:TextBox ID="DateTextboxEnd" runat="server" CssClass="m-wrap span12 date form_datetime text form-control mytext" Width="26%"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="btn btn-primary" OnClick="Button1_Click" Title="Click to regenerate receipt files list"  />
                        </div>
                    </div>
                </div>

            </div>

            <div style="float: left; width: 100%; margin-top: 12px;">
                <label>Receipt Files Available for Download</label>
                <ig:webdatagrid ID="WebDataGrid1" runat="server" Width="100%" DataSourceID="SqlDataSource1" AutoGenerateColumns="False" BorderStyle="None" StyleSetName="Trendy" 
                    DataKeyFields="FileID" EnableDataViewState="True" >
                    <Columns>
                        <ig:BoundDataField DataFieldName="FileID" Key="FileID"  Width="70" CssClass="hidden">
                                <Header Text="FileId" CssClass="hidden" />
                                <Footer ColSpan="1" />
                        </ig:BoundDataField>
                        <ig:BoundDataField DataFieldName="DateGenerated" Key="DateGenerated" Width="19%" DataFormatString="{0:G}">
                                <Header Text="Created" />
                                <Footer ColSpan="1" />
                        </ig:BoundDataField>
                        <ig:BoundDataField DataFieldName="CompanyId" Key="CompanyId" Width="19%" CssClass="hidden">
                                <Header Text="Company" CssClass="hidden" />
                                <Footer ColSpan="1" />
                        </ig:BoundDataField>
                        <ig:BoundDataField DataFieldName="CompanyName" Key="CompanyName" Width="19%">
                                <Header Text="Company Name" />
                                <Footer ColSpan="1" />
                        </ig:BoundDataField>
                        <ig:BoundDataField DataFieldName="TotalPaymentAmount" Key="TotalPaymentAmount" Width="19%" DataFormatString="{0:C}">
                                <Header Text="Totals" />
                                <Footer ColSpan="1" />
                        </ig:BoundDataField>
                        <ig:BoundDataField DataFieldName="DateDownloaded" Key="DateDownloaded" Width="19%" DataFormatString="{0:G}">
                                <Header Text="Downloaded" />
                                <Footer ColSpan="1" />
                        </ig:BoundDataField>
                        <ig:TemplateDataField Key="TemplateField1">
                            <ItemTemplate>
                                <asp:Button ID="GridButton1" runat="server" Text="Download" CommandName="Download" CssClass="btn-link"/>
                                <%--<asp:HyperLink ID="HyperLink1" runat="server">Download</asp:HyperLink>--%>
                            </ItemTemplate>
                            <Header Text="Action" />
                        </ig:TemplateDataField>
                    </Columns>
                    <Behaviors>
                        <ig:Activation></ig:Activation>
                        <ig:Selection CellClickAction="Row" CellSelectType="Single" />
                    </Behaviors>
                    <EmptyRowsTemplate>
                        <div style="text-align: center;">
                            <br />
                            No receipt files found for Company in selected date range.
                        </div>
                    </EmptyRowsTemplate>
                </ig:webdatagrid>
            </div>
        
        </div>
    </div>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" 
        
        SelectCommand="usp_Get_MRIReceiptFiles" SelectCommandType="StoredProcedure">    
        
        <SelectParameters>
            <asp:ControlParameter ControlID="CompanyIdFilter" DefaultValue="0" Name="CompanyId" PropertyName="Text" Type="Int32" />
            <asp:ControlParameter ControlID="DateTextboxStart" DefaultValue="7/1/2014" Name="StartDate" PropertyName="Text" Type="DateTime" />
            <asp:ControlParameter ControlID="DateTextboxEnd" DefaultValue="7/2/2014" Name="EndDate" PropertyName="Text" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:TextBox ID="CompanyIdFilter" runat="server" Width="1" Visible="false" AutoPostBack="True" Text="0"></asp:TextBox>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            var dp = $('#<%=DateTextboxStart.ClientID%>');
        dp.datepicker({
            changeMonth: true,
            changeYear: true,
            format: "mm/dd/yyyy",
            language: "tr"
        }).on('changeDate', function (ev) {
            $(this).blur();
            $(this).datepicker('hide');
        });
    });
</script>
        <script type="text/javascript">
            $(document).ready(function () {
                var dp = $('#<%=DateTextboxEnd.ClientID%>');
            dp.datepicker({
                changeMonth: true,
                changeYear: true,
                format: "mm/dd/yyyy",
                language: "tr"
            }).on('changeDate', function (ev) {
                $(this).blur();
                $(this).datepicker('hide');
            });
        });
</script>

</asp:Content>