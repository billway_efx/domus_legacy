﻿using EfxFramework;
using System;
using System.Linq;
using RPO;

namespace Domus
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string _user = Page.User.Identity.Name.ToString();
                ActivityLog AL = new ActivityLog();
                AL.WriteLog(_user, "User Logged in - Admin Home Page", (short)LogPriority.LogAlways);
            }

        }
    }
}