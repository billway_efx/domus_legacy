﻿using EfxFramework;
using EfxFramework.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using static System.String;

namespace RentPaidOnline.ReportingPages.UserControls
{
    public partial class PropertyPicker : System.Web.UI.UserControl
    {
        private int _propertyCount;
        private int _companyCount;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Helper.IsRpoAdmin)
                {
                    IsAdminCompanyButtonArea.Visible = true;
                    CompanyListList.Visible = true;
                    BindCompanyDropDown();
                    StartDatetxt.Text = DateTime.Now.Date.Subtract(TimeSpan.FromDays(7)).ToString("M/dd/yyyy");
                    EndDatetxt.Text = DateTime.Now.Date.ToString("M/dd/yyyy");
                }
                else
                {
                    IsAdminCompanyButtonArea.Visible = false;
                    CompanyListList.Visible = false;
                    ModalPopupExtender2.Enabled = false;
                    BindDropDownData();
                }
            }

            var PageName = System.IO.Path.GetFileName(Request.Url.AbsolutePath).ToString();
            if (PageName != "AutoPaymentReport.aspx" && PageName != "RPOBillingReport.aspx" &&
                PageName != "RPOAdoptionReport.aspx" && PageName != "default.aspx" && PageName != "Default.aspx" &&
                PageName != "community-connect.aspx" && PageName != "ItemFeesWalletSummaryReport.aspx" &&
                PageName != "DetailedDepositAnalysisReport.aspx") return;

            StartDateArea.Visible = false;
            EndDateArea.Visible = false;
            Label1.Visible = false;
            Label2.Visible = false;
            StartDatetxt.Visible = false;
            EndDatetxt.Visible = false;
        }

        public void BindDropDownData()
        {
            CheckBoxList1.DataSource = GetPropertiesForDropDownList();
            CheckBoxList1.DataTextField = "PropertyName";
            CheckBoxList1.DataValueField = "PropertyID";
            CheckBoxList1.DataBind();
        }

        public void BindCompanyDropDown()
        {
            CompanyChecklist.DataSource = GetCompaniesForDropDown();
            CompanyChecklist.DataTextField = "CompanyName";
            CompanyChecklist.DataValueField = "CompanyId";
            CompanyChecklist.DataBind();
        }
        public string GetCompanyString()
        {
            var propertyIdList = new List<string>();
            var propertyNameList = new List<string>();

            foreach (ListItem item in CompanyChecklist.Items)
            {
                if (!item.Selected) continue;

                propertyIdList.Add(item.Value);
                propertyNameList.Add(item.Text);
            }
            var propList = Join(",", propertyIdList.ToArray());
            return propList;
        }

        private SqlDataReader GetPropertiesForDropDownList()
        {   
            var companyString = "";
            var adminCheck = Helper.IsRpoAdmin;
            //Get Current Userid
            var currentUser = Helper.CurrentUserId;

            companyString = adminCheck ? GetCompanyString() : "0";

            var con = new SqlConnection(EfxSettings.ReportConnectionString);
            var com = new SqlCommand("usp_PropertyGetListForReports", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            com.Parameters.Add("@IsRpoAdmin", SqlDbType.Bit).Value = adminCheck;
            com.Parameters.Add("@CompanyList", SqlDbType.NVarChar, -1).Value = companyString;
            com.Parameters.Add("@PropertyStaffID", SqlDbType.Int).Value = currentUser;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch(Exception e)
            {
                con.Close();
                throw e;
            }
        }
        private SqlDataReader GetCompaniesForDropDown()
        {
            var con = new SqlConnection(EfxSettings.ReportConnectionString);
            var com = new SqlCommand("usp_Company_GetCompanyListForReports", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                con.Close();
                throw e;
            }
        }

        protected void CheckBoxList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ModalPopupExtender1.Show();
            foreach (ListItem item in CheckBoxList1.Items)
            {
                if (item.Selected)
                {
                    _propertyCount += 1;
                }

                if (_propertyCount > 0)
                {
                    SelectPropertyBtn.Text = _propertyCount + " Properties Selected";
                }
                else
                {
                    SelectPropertyBtn.Text = "Select Property";
                }
            }
        }
        protected void Button3_Click(object sender, EventArgs e)
        {
            var buttonString = Button3.Text;

            if (buttonString == "Select All")
            {
                foreach (ListItem item in CheckBoxList1.Items)
                {
                    item.Selected = true;
                    _propertyCount += 1;

                    if (_propertyCount > 0)
                    {
                        SelectPropertyBtn.Text = _propertyCount.ToString() + " Properties Selected";
                    }
                    else
                    {
                        SelectPropertyBtn.Text = "Select Property";
                    }
                }
                Button3.Text = "Deselect All";
            }
            else
            {
                foreach (ListItem item in CheckBoxList1.Items)
                {
                    item.Selected = false;
                    _propertyCount -= 1;

                    if (_propertyCount > 0)
                    {
                        SelectPropertyBtn.Text = _propertyCount + " Properties Selected";
                    }
                    else
                    {
                        SelectPropertyBtn.Text = "Select Property";
                    }
                }
                Button3.Text = "Select All";
            }

            ModalPopupExtender1.Show();
        }

        protected void CloseModalBtn_Click(object sender, EventArgs e)
        {
            var PropertyCount = 0;
            foreach (ListItem ckItem in CheckBoxList1.Items)
            {
                if (ckItem.Selected)
                {
                    PropertyCount += 1;
                }
            }

            SelectPropertyBtn.Text = PropertyCount.ToString() + " Properties Selected";

            ModalPopupExtender1.Hide();
        }

        protected void CloseCompanyModalBtn_Click(object sender, EventArgs e)
        {

            var CompanyCount = 0;
            foreach (ListItem ckItem in CompanyChecklist.Items)
            {
                if (ckItem.Selected)
                {
                    CompanyCount += 1;
                }
            }

            SelectCompanybtn.Text = CompanyCount.ToString() + " Companies Selected";

            BindDropDownData();
            
            Button3.Text = "Select All";
            SelectPropertyBtn.Text = "Select Property";
            ModalPopupExtender2.Hide();
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            StartDatetxt.Text = Calendar1.SelectedDate.ToShortDateString();
        }

        protected void Calendar2_SelectionChanged(object sender, EventArgs e)
        {
            EndDatetxt.Text = Calendar2.SelectedDate.ToShortDateString();
        }

        protected void SelectAllCompanybtn_Click(object sender, EventArgs e)
        {
            var buttonString = SelectAllCompanybtn.Text;

            if (buttonString == "Select All")
            {
                foreach (ListItem item in CompanyChecklist.Items)
                {
                    item.Selected = true;
                    _companyCount += 1;

                    if (_companyCount > 0)
                    {
                        SelectCompanybtn.Text = _companyCount.ToString() + " Companies Selected";
                    }
                    else
                    {
                        SelectCompanybtn.Text = "Select Company";
                    }
                }
                SelectAllCompanybtn.Text = "Deselect All";
            }
            else
            {
                foreach (ListItem item in CompanyChecklist.Items)
                {
                    item.Selected = false;
                    _companyCount -= 1;

                    if (_companyCount > 0)
                    {
                        SelectCompanybtn.Text = _companyCount.ToString() + " Comapanies Selected";
                    }
                    else
                    {
                        SelectCompanybtn.Text = "Select Company";
                    }
                }
                SelectAllCompanybtn.Text = "Select All";
            }

            ModalPopupExtender2.Show();
        }

        protected void CompanyChecklist_SelectedIndexChanged(object sender, EventArgs e)
        {
            ModalPopupExtender2.Show();
            foreach (ListItem item in CompanyChecklist.Items)
            {

                if (item.Selected)
                {
                    _companyCount += 1;
                }

                if (_companyCount > 0)
                {
                    SelectCompanybtn.Text = _companyCount.ToString() + " Companies Selected";
                }
                else
                {
                    SelectCompanybtn.Text = "Select Company";
                }
            }
        }
    }
}