﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyPicker.ascx.cs" Inherits="RentPaidOnline.ReportingPages.UserControls.PropertyPicker" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div>
        <!--cakel: BUGID00278 Added New User Control -->
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>



                <div class="divider-fluid">
                    <div class="padding">
                         <!-- CMallory - Layout Change - Added btn-primary btn-footer css tag to buttons and removed btn-small -->
                        <div id="IsAdminCompanyButtonArea" runat="server" style="padding-bottom: 10px;">
                            <asp:Button ID="SelectCompanybtn" runat="server" Text="Select Company" Width="170" CssClass="btn btn-primary footer-btn" /><br />
                        </div>
                        <asp:Button ID="SelectPropertyBtn" runat="server" Text="Select Property" Width="170" CssClass="btn btn-primary footer-btn" />
                    </div>
                </div>
                <div id="StartDateArea" class="divider-fluid" runat="server">
                    <div class="padding">
                        <asp:Label ID="Label1" runat="server" Text="Start"></asp:Label>
                        <asp:TextBox ID="StartDatetxt" runat="server" CssClass="text form-control"></asp:TextBox>
                        <asp:PopupControlExtender ID="StartDatetxt_PopupControlExtender" runat="server" Enabled="True" DynamicServicePath="" ExtenderControlID="" PopupControlID="Panel2" TargetControlID="StartDatetxt" Position="Bottom">
                        </asp:PopupControlExtender>
                    </div>
                </div>
                <div class="divider-fluid" id="EndDateArea" runat="server">
                    <div class="padding">
                        <asp:Label ID="Label2" runat="server" Text="End"></asp:Label>
                        <asp:TextBox ID="EndDatetxt" runat="server" CssClass="text form-control"></asp:TextBox>
                        <asp:PopupControlExtender ID="EndDatetxt_PopupControlExtender" runat="server" Enabled="True" DynamicServicePath="" ExtenderControlID="" TargetControlID="EndDatetxt" PopupControlID="Panel3" Position="Bottom">
                        </asp:PopupControlExtender>
                    </div>
                </div>


                <!--Property Selection Panel -->
                <!--Added Style to Panel Display None prevent flicker on Page load -->
                <asp:Panel ID="PropertyList" runat="server" Width="460px" Height="500px" Style="display: none;">
                    <div style="padding: 5px; background-color: #ffffff">
                        <div style="padding: 5px;">
                            <h2>Select Property</h2>
                            <asp:Button ID="Button3" runat="server" Text="Select All" OnClick="Button3_Click" CssClass="btn btn-small" />
                            <asp:Label ID="ModalSelectCountlbl" runat="server"></asp:Label>
                            <asp:Button ID="CloseModalBtn" runat="server" CssClass="btn btn-small" Text="Close" OnClick="CloseModalBtn_Click" />
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                            <ContentTemplate>

                                <asp:Panel ID="Panel1" runat="server" Width="455" Height="480" ScrollBars="Vertical">
                                    <asp:CheckBoxList ID="CheckBoxList1" runat="server" CssClass="MyCheck" OnSelectedIndexChanged="CheckBoxList1_SelectedIndexChanged"></asp:CheckBoxList>

                                </asp:Panel>

                            </ContentTemplate>

                        </asp:UpdatePanel>
                        
                    </div>
                </asp:Panel>
                <!--Added Style to Panel Display None prevent flicker on Page load -->
                <asp:Panel ID="Panel2" runat="server" style="display: none;">
                    <asp:Calendar ID="Calendar1" runat="server" OnSelectionChanged="Calendar1_SelectionChanged" BackColor="#F7F7F7" ForeColor="#333333" Font-Size="18px" Height="220px" NextMonthText="Next" PrevMonthText="Previous" Width="310px">
                        <DayHeaderStyle BackColor="#badc02" />
                        <NextPrevStyle Font-Bold="True" Font-Size="14px" ForeColor="White" />
                        <TitleStyle BackColor="#662E6B" ForeColor="White" />
                    </asp:Calendar>
                </asp:Panel>
                <!--Added Style to Panel Display None prevent flicker on Page load -->
                <asp:Panel ID="Panel3" runat="server" Style="display: none;">
                    <asp:Calendar ID="Calendar2" runat="server" OnSelectionChanged="Calendar2_SelectionChanged" BackColor="#F7F7F7" ForeColor="#333333" Font-Size="18px" Height="220px" NextMonthText="Next" PrevMonthText="Previous" Width="310px">
                        <DayHeaderStyle BackColor="#badc02" />
                        <NextPrevStyle Font-Bold="True" Font-Size="14px" ForeColor="White" />
                        <TitleStyle BackColor="#662E6B" ForeColor="White" />
                    </asp:Calendar>
                </asp:Panel>

                <!--Company Selection Panel -->
                <asp:Panel ID="CompanyListList" runat="server" Width="460px" Height="400px" Style="display: none;">
                    <div style="padding: 5px; background-color: #ffffff">
                        <div style="padding: 5px;">
                            <h2>Select Company</h2>
                            <asp:Button ID="SelectAllCompanybtn" runat="server" Text="Select All" CssClass="btn btn-small" OnClick="SelectAllCompanybtn_Click" />
                            <asp:Label ID="Label3" runat="server"></asp:Label>


                            <asp:Button ID="CloseCompanyModalBtn" runat="server" CssClass="btn btn-small" Text="Close" OnClick="CloseCompanyModalBtn_Click" />
                        </div>
                        <asp:UpdatePanel ID="CompanyUpdatePanel" runat="server" UpdateMode="Always">
                            <ContentTemplate>

                                <asp:Panel ID="CompanySubUpdatePanel" runat="server" Width="455" Height="398" ScrollBars="Vertical">
                                    <asp:CheckBoxList ID="CompanyChecklist" runat="server" CssClass="MyCheck" OnSelectedIndexChanged="CompanyChecklist_SelectedIndexChanged"></asp:CheckBoxList>

                                </asp:Panel>

                            </ContentTemplate>

                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
                <!--CMallory - Task 0026 - Added Div around Modal Popups so the close button could be seen when reducing the size of the window -->
                <div id="myModalPopup">
                <asp:ModalPopupExtender ID="ModalPopupExtender1" PopupDragHandleControlID="myModalPopup" runat="server" TargetControlID="SelectPropertyBtn" PopupControlID="PropertyList" BackgroundCssClass="ReportmodalPop" X="0" Y="0"></asp:ModalPopupExtender>
                <asp:ModalPopupExtender ID="ModalPopupExtender2" PopupDragHandleControlID="myModalPopup" runat="server" TargetControlID="SelectCompanybtn" PopupControlID="CompanyListList" BackgroundCssClass="ReportmodalPop" X="0" Y="0"></asp:ModalPopupExtender>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</div>
