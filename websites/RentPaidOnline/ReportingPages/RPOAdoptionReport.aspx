﻿<%@ Page Language="C#" MasterPageFile="~/frontend.master"  AutoEventWireup="true" CodeBehind="RPOAdoptionReport.aspx.cs" Inherits="RentPaidOnline.ReportingPages.RpoAdoptionReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/ReportingPages/UserControls/PropertyPicker.ascx" TagPrefix="uc1" TagName="PropertyPicker" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <!--cmallory: Task 00535 Added New Page and Controls to generate Adoption Report -->
    <div>
        <h1>Reports</h1>

    </div>

    <div class="main-container">

        <div class="report-header selected" style="width:40%;">
            <a class="report-nav" href="/ReportingPages/ReportSelection.aspx">
                <span class="back" aria-hidden="true" data-icon="2"></span>
                <!--cakel: Updated name of Report header -->
                <span class="default-nav">Adoption Renter Report</span></a>
        </div>

        <div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="well no-padding">
                    <table>
                        <tr>
                            <td>
                                <uc1:PropertyPicker runat="server" ID="PropertyPicker" />

                            </td>
                            <td style="width:90%; text-align:right;">
                                <div style="width:90%">
                                    <div class="padding">
                                        <asp:Button ID="ViewReportbtn" runat="server" Text="View Report" CssClass="btn btn-primary" OnClick="ViewReportbtn_Click" />
                                    </div>
                                </div>

                            </td>

                        </tr>
                    </table>
                </div>


            </ContentTemplate>
        </asp:UpdatePanel>

        <div style="padding-top: 2px; padding-bottom: 10px">
            <div class="export-select">
                <asp:DropDownList ID="ExportTypeDDL" runat="server" CssClass="form-control" Width="150px">
                    <asp:ListItem Value="application/excel">Excel</asp:ListItem>
                    <asp:ListItem Value="application/pdf">PDF</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="Exportbtn" runat="server" Text="Export" CssClass="btn btn-small" data-icon="H" OnClick="Exportbtn_Click" />
            </div>
        </div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div>
                    <br />
                    <asp:HiddenField ID="HiddenStart" runat="server" />
                    <asp:HiddenField ID="HiddenEnd" runat="server" />
                    <asp:HiddenField ID="HiddenPropertyList" runat="server" />
                </div>
                <div style="text-align: center; margin-top: 10px;">
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <img src="../Images/report_wait.gif" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:Label ID="Gridresultlbl" runat="server"></asp:Label>
                </div>
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" PageSize="100" CellPadding="10" Width="98%" GridLines="Horizontal" DataKeyNames="PropertyId" DataSourceID="RPOAdoptionReportSQL">
                    <Columns>
                        <asp:BoundField DataField="CompanyName" HeaderText="Company Name" InsertVisible="False" ReadOnly="True" Visible="False" />
                        <asp:BoundField DataField="PropertyName" HeaderText="Property Name" SortExpression="PropertyName" />
                        <asp:BoundField DataField="FirstName" HeaderText="First Name" ReadOnly="True" SortExpression="DateAdded" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="LastName" HeaderText="Last Name" ReadOnly="True" SortExpression="ProgramDescription" />
                        <asp:BoundField DataField="Using_RPO_YESNo" HeaderText="Using RPO YES/No" SortExpression="NumberOfUnits" ReadOnly="True" />
                        <asp:BoundField DataField="PMSId" HeaderText="PMS ID" ReadOnly="True" SortExpression="RentalDepositBankAccountNumber" />
                        <asp:BoundField DataField="MainPhoneNumber" HeaderText="Main Phone Number" ReadOnly="True" SortExpression="RentalDepositBankRoutingNumber" />
                        <asp:BoundField DataField="PrimaryEmailAddress" HeaderText="Primary Email Address" ReadOnly="True" SortExpression="Last4Account" Visible="False" />
                        <asp:BoundField DataField="StreetAddress" HeaderText="StreetAddress" ReadOnly="True" SortExpression="Last4Routing" Visible="False" />
                        <asp:BoundField DataField="City" HeaderText="City" ReadOnly="True" SortExpression="Last4Routing" Visible="False" />
                        <asp:BoundField DataField="State" HeaderText="State" ReadOnly="True" SortExpression="Last4Routing" Visible="False" />
                        <asp:BoundField DataField="Zip" HeaderText="Zip Code" ReadOnly="True" SortExpression="Last4Routing" Visible="False" />
                    </Columns>
                    <HeaderStyle CssClass="ReportTableHeader" />
                    <PagerSettings PageButtonCount="10" FirstPageText="First" LastPageText="Last" NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" Mode="NumericFirstLast" />
                    <PagerStyle Font-Size="16px" HorizontalAlign="Right" />
                    <RowStyle CssClass="ReportTableData" />
                    <SelectedRowStyle BackColor="#E6E6E6" />
                </asp:GridView>

                <asp:SqlDataSource ID="RPOAdoptionReportSQL" runat="server" ConnectionString="<%$ ConnectionStrings:ReportsConnection %>" SelectCommand="usp_Report_RPO_AdoptionReport" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenPropertyList" Name="PropertyIdList" PropertyName="Value" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>


            </ContentTemplate>
        </asp:UpdatePanel>

    </div>


</asp:Content>
