﻿<%@ Page Language="C#"  MasterPageFile="~/frontend.master"  AutoEventWireup="true" CodeBehind="ReportSelection.aspx.cs" Inherits="Domus.ReportingPages.ReportSelection" %>



<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <!--cakel: BUGID00278 Added New Page and Controls -->

    <h1>Select a Report</h1>

    <div class="main-container">
        <!--CMallory - Layout Change - Changed the background color of table and forecolor of links -->
        <table style="width: 98%; background-color: #F8F7FA; color: #badc02;">
            <tr>
                <td style="padding: 5px;">
                    <h1>Reconciliation</h1>
                    <h2>
                        <asp:HyperLink ID="HyperLink2" CssClass="ReportSelectLink" runat="server" NavigateUrl="~/ReportingPages/PaymentSummaryReport.aspx" ForeColor="#662E6B">Payment Summary</asp:HyperLink>
                    </h2>
                    <h2>
                        <asp:HyperLink ID="HyperLink8" CssClass="ReportSelectLink"  runat="server" NavigateUrl="~/ReportingPages/RefundReport.aspx" ForeColor="#662E6B">Refund/Return</asp:HyperLink>
                    </h2>
                    <h2>
                        <asp:HyperLink ID="HyperLink1" CssClass="ReportSelectLink" runat="server" NavigateUrl="~/ReportingPages/DetailedDepositReport.aspx" ForeColor="#662E6B">Detail Deposit</asp:HyperLink>
                    </h2>
                    <%-- SWitherspoon Task 00662: Commented link out as per CAkels instruction --%>
                    <%--<h2 id="AnalysisReportArea" runat="server">
                        <asp:HyperLink ID="DetailedDepositAnalysisReportlink" CssClass="ReportSelectLink" runat="server" NavigateUrl="~/ReportingPages/DetailedDepositAnalysisReport.aspx" ForeColor="#662E6B">Detail Deposit Analysis– To Payments Transactions and ACH Rollup</asp:HyperLink>
                    </h2>--%>
                    <h2>
                        <asp:HyperLink ID="HyperLink7" CssClass="ReportSelectLink" runat="server" NavigateUrl="~/ReportingPages/TransactionSummaryReport.aspx" ForeColor="#662E6B">Transaction Summary</asp:HyperLink>
                    </h2>
                    <h2>
                        <asp:HyperLink ID="HyperLink5" CssClass="ReportSelectLink"  runat="server" NavigateUrl="~/ReportingPages/ApplicantPortalPaymentReport.aspx" ForeColor="#662E6B">Applicant Portal Payments</asp:HyperLink>
                    </h2>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                 <h1>Resident Setup</h1>
                   <h2>
                        <asp:HyperLink ID="HyperLink3" CssClass="ReportSelectLink" runat="server" NavigateUrl="~/ReportingPages/AutoPaymentReport.aspx" ForeColor="#662E6B">AutoPayment</asp:HyperLink>
                  </h2>
                  <h2>
                        <asp:HyperLink ID="HyperLink9" CssClass="ReportSelectLink" runat="server" NavigateUrl="~/ReportingPages/ItemFeesWalletSummaryReport.aspx" ForeColor="#662E6B" Visible="false">Item Fees Wallet Summary Report</asp:HyperLink>
                  </h2>
                </td>
            </tr>
            <% if (EfxFramework.Helpers.Helper.IsRpoAdmin)
					{ %>
            <tr>
                <td style="padding: 5px">
                    <h1>Adoption</h1>
                    <h2>
                        <!--cmallory: Task 00535 Added link to run the Adoption Report -->
                        <!--cakel: TASK 00535  Updated Report Link Name-->
                        <asp:HyperLink ID="AdoptionReportLink" CssClass="ReportSelectLink"  runat="server" NavigateUrl="~/ReportingPages/RPOAdoptionReport.aspx" ForeColor="#662E6B">Renters Adoption Report</asp:HyperLink>
                    </h2>
                </td>
            </tr>
             <%} %>
            <tr>
                <td style="padding: 5px">
                    <h1>Integrated Imports and Exports</h1>
                    <h2>
                        <asp:HyperLink ID="HyperLink4" CssClass="ReportSelectLink"  runat="server" NavigateUrl="~/ReportingPages/PaymentExportSummary.aspx" ForeColor="#662E6B">Payment Export Summary</asp:HyperLink>
                    </h2>
                    <h2>
                        <asp:HyperLink ID="HyperLink6" CssClass="ReportSelectLink" runat="server" NavigateUrl="~/ReportingPages/ResidentDataImportSummary.aspx" ForeColor="#662E6B">Resident Data Import Summary</asp:HyperLink>
                    </h2>
                    
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <h1 id="billingarea" runat="server">Billing – DOMUS Admin</h1>
                    <h2>
                        <!--cmallory: Task 00553 Added link to run the Daily Recon Report -->
                        <asp:HyperLink ID="DailyReconReportLink" CssClass="ReportSelectLink"  runat="server" NavigateUrl="~/ReportingPages/RPODailyReconReport.aspx" ForeColor="#662E6B">Daily Recon Report - DOMUS Admin</asp:HyperLink>
                    </h2>  
                    <h2>
                        <asp:HyperLink ID="BillingReportLink" CssClass="ReportSelectLink"  runat="server" NavigateUrl="~/ReportingPages/RPOBillingReport.aspx" ForeColor="#662E6B">Billing - DOMUS Admin</asp:HyperLink>
                    </h2>       
                </td>
            </tr>
        </table>




    </div>

    </asp:Content>