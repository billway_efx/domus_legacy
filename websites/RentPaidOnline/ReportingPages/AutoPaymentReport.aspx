﻿<%@ Page Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="AutoPaymentReport.aspx.cs" Inherits="Domus.ReportingPages.AutoPaymentReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/ReportingPages/UserControls/PropertyPicker.ascx" TagPrefix="uc1" TagName="PropertyPicker" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <!--cakel: BUGID00278 Added New Page and Controls -->
    <div>
        <h1>Reports</h1>

    </div>


    <div class="main-container">
        <div class="report-header selected" style="width:40%;">
            <a class="report-nav" href="/ReportingPages/ReportSelection.aspx">
                <span class="back" aria-hidden="true" data-icon="2"></span>
                <span class="default-nav">AutoPayment</span></a>
        </div>
        <div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="well no-padding">
                    <table>
                        <tr>
                            <td>
                              
                                <uc1:PropertyPicker runat="server" id="PropertyPicker" />
                            </td>
                            <td style="width:90%; text-align:right;">
                                <div style="width:90%">
                                    <div class="padding">
                                        <asp:Button ID="ViewReportbtn" runat="server" Text="View Report" CssClass="btn btn-primary" OnClick="ViewReportbtn_Click" />
                                    </div>
                                </div>

                            </td>

                        </tr>
                    </table>
                </div>


            </ContentTemplate>
        </asp:UpdatePanel>

        <div style="padding-top: 2px; padding-bottom: 10px">
            <div class="export-select">
                <asp:DropDownList ID="ExportTypeDDL" runat="server" CssClass="form-control" Width="150px">
                    <asp:ListItem Value="application/excel">Excel</asp:ListItem>
                    <asp:ListItem Value="application/pdf">PDF</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="Exportbtn" runat="server" Text="Export" CssClass="btn btn-small" data-icon="H" OnClick="Exportbtn_Click" />
            </div>
        </div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                <div>

 
                    <br />
                    <asp:HiddenField ID="HiddenStart" runat="server" />
                    <asp:HiddenField ID="HiddenEnd" runat="server" />
                    <asp:HiddenField ID="HiddenPropertyList" runat="server" />
                    <div style="text-align: center; margin-top: 10px;">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <img src="../Images/report_wait.gif" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:Label ID="Gridresultlbl" runat="server"></asp:Label>
                    </div>
                    </div>

            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="AutoPaymentSQL" AllowPaging="True" AllowSorting="True" PageSize="30" CellPadding="5" Width="100%" GridLines="Horizontal" OnRowDataBound="GridView1_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="AutoPaymentId" HeaderText="AutoPaymentId" InsertVisible="False" ReadOnly="True" SortExpression="AutoPaymentId" Visible="False" />
                    <asp:BoundField DataField="PropertyId" HeaderText="PropertyId" SortExpression="PropertyId" Visible="False" />
                    <asp:BoundField DataField="PropertyName" HeaderText="Property" SortExpression="PropertyName" />
                    <asp:BoundField DataField="RenterId" HeaderText="RenterId" InsertVisible="False" ReadOnly="True" SortExpression="RenterId" Visible="False" />
                    <asp:TemplateField HeaderText="Renter Name" SortExpression="RenterName">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("RenterName") %>'></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "~/Renters/RenterDetails.aspx?renterId=" + Eval("RPORenterID") %>' Text='<%# Eval("RenterName") %>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PayerId" HeaderText="PayerId" InsertVisible="False" ReadOnly="True" SortExpression="PayerId" Visible="False" />
                    <asp:BoundField DataField="AutoPayDayOfMonth" HeaderText="Draft Date" SortExpression="AutoPayDayOfMonth" />
                    <asp:BoundField DataField="PaymentMethod" HeaderText="Method" ReadOnly="True" SortExpression="PaymentMethod" >
                    <ItemStyle Font-Size="12px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CurrentBalanceDue" DataFormatString="{0:c}" HeaderText="Current Balance" SortExpression="CurrentBalanceDue" />
                    <asp:BoundField DataField="LeaseRentAmount" HeaderText="Rent" ReadOnly="True" SortExpression="LeaseRentAmount" DataFormatString="{0:c}" />
                    <asp:BoundField DataField="WillAutoPayProcess" HeaderText="Will Process" SortExpression="WillAutoPayProcess" />
                    <asp:BoundField DataField="StatusDescription" HeaderText="Description" SortExpression="StatusDescription">
                    <ItemStyle Font-Size="12px" />
                    </asp:BoundField>
                </Columns>
                <HeaderStyle CssClass="ReportTableHeader" />
                <PagerSettings PageButtonCount="10" FirstPageText="First" LastPageText="Last" NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" Mode="NumericFirstLast" />
                    <PagerStyle Font-Size="16px" HorizontalAlign="Right" />
                <RowStyle CssClass="ReportTableData" />
            </asp:GridView>


                <asp:SqlDataSource ID="AutoPaymentSQL" runat="server" ConnectionString="<%$ ConnectionStrings:ReportsConnection %>" SelectCommand="usp_Report_RPO_AutoPayment_v2" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenPropertyList" Name="PropertyIdList" PropertyName="Value" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>






            </ContentTemplate></asp:UpdatePanel>
    </div>



</asp:Content>
