﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using EfxFramework;
using EfxFramework.Helpers;

namespace Domus.ReportingPages
{
    //cmallory: Task 00535 Added New Page and Controls to generate Daily Recon Report 
    public partial class RPODailyReconReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string GetPropertyString()
        {

            CheckBoxList ckbx = (CheckBoxList)this.PropertyPicker.FindControl("CheckBoxList1");
            List<String> PropertyID_list = new List<string>();
            List<String> PropertyName_list = new List<string>();

            foreach (ListItem item in ckbx.Items)
            {
                if (item.Selected)
                {
                    PropertyID_list.Add(item.Value);
                    PropertyName_list.Add(item.Text);
                }
            }
            string PropList = String.Join(",", PropertyID_list.ToArray());
            return PropList;
        }


        private void GenerateReport()
        {

            string ExportTypeValue = ExportTypeDDL.SelectedValue.ToString();
            string ExportTypeText = ExportTypeDDL.SelectedItem.Text;

            // Setup DataSet
            DataSet ds = GetReportDataSet();

            // Create Report DataSource
            ReportDataSource rds = new ReportDataSource("RPODailyReconReport", ds.Tables[0]);

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = ExportTypeValue;
            string encoding = "UTF-8";
            string extension = string.Empty;

            // Setup the report viewer object and get the array of bytes
            ReportViewer viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.ReportPath = "ReportingPages/SSRSreports/RPODailyReconReport.rdlc";
            viewer.LocalReport.DataSources.Add(rds); // Add datasource here


            byte[] bytes = viewer.LocalReport.Render(ExportTypeText, null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            string fileName = "RPO Daily Recon Report";

            // Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName + "." + extension);
            Response.BinaryWrite(bytes); // create the file
            Response.End();
            Response.Flush(); // send it to the client to download
            Response.Close();
        }


        private DataSet GetReportDataSet()
        {
            string ReportStartDate = HiddenStart.Value.ToString();
            string ReportEndDate = HiddenEnd.Value.ToString();
            string propertyID = HiddenPropertyList.Value.ToString();

            SqlConnection con = new SqlConnection(ConnString);
            {
                SqlCommand com = new SqlCommand("usp_Report_RPO_DailyReconReport", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("@StartDate", SqlDbType.Date).Value = ReportStartDate;
                com.Parameters.Add("@EndDate", SqlDbType.Date).Value = ReportEndDate;
                com.Parameters.Add("@PropertyIdList", SqlDbType.NVarChar, -1).Value = propertyID;
                com.CommandTimeout = 360;

                con.Open();
                SqlDataAdapter SqlAdapt = new SqlDataAdapter(com);
                //SqlAdapt.SelectCommand = com;
                DataSet DS = new DataSet();
                SqlAdapt.Fill(DS, "RPODailyReconReportData");
                con.Close();
                return DS;
            }

        }

        //PaymentDate,PropertyID,TransactionType,PaymentStatusDescription
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GridView1.SelectedIndex > -1)
            {
                DateTime DateString = (DateTime)GridView1.DataKeys[GridView1.SelectedIndex].Values["PaymentDate"];
                string TransactionType = GridView1.DataKeys[GridView1.SelectedIndex].Values["TransactionType"].ToString();
                string PaymentStatusDescription = GridView1.DataKeys[GridView1.SelectedIndex].Values["PaymentStatusDescription"].ToString();
                string PropID = GridView1.DataKeys[GridView1.SelectedIndex].Values["PropertyID"].ToString();

                GridView2.DataSource = GetSubReportDetail(DateString, PropID, TransactionType, PaymentStatusDescription);
                GridView2.DataBind();

                ModalPopupExtender1.Show();
            }
        }


        public SqlDataReader GetSubReportDetail(DateTime PaymentDate, string PropertyID, string TransactionType, string PaymentStatusDescription)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Report_RPO_RPODailyReconReport", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = PropertyID;
            com.Parameters.Add("@TransactionType", SqlDbType.NVarChar, 30).Value = TransactionType;
            com.Parameters.Add("@PaymentStatusDescription", SqlDbType.NVarChar, 30).Value = PaymentStatusDescription;
            com.Parameters.Add("@PaymentDate", SqlDbType.Date).Value = PaymentDate;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                con.Close();
                throw e;
            }

        }

        private static String ConnString
        {
            get
            {
                return EfxFramework.EfxSettings.ReportConnectionString;
            }
        }

        protected void ViewReportbtn_Click(object sender, EventArgs e)
        {
            TextBox StartDate = (TextBox)this.PropertyPicker.FindControl("StartDatetxt");
            TextBox EndDate = (TextBox)this.PropertyPicker.FindControl("EndDatetxt");
            RPODailyReconReportSQL.DataBind();
            HiddenStart.Value = StartDate.Text;
            HiddenEnd.Value = EndDate.Text;
            HiddenPropertyList.Value = GetPropertyString();

            UpdatePanel1.DataBind();
            if (GridView1.Rows.Count < 1)
            {
                Gridresultlbl.Text = "Your selection produced no results";
            }
            else
            {
                Gridresultlbl.Text = "";
            }

        }

        protected void Exportbtn_Click(object sender, EventArgs e)
        {
            if (GridView1.Rows.Count > 0)
            {
                GenerateReport();
            }
        }


    }
}