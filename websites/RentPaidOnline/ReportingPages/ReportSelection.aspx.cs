﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using EfxFramework;
using EfxFramework.Helpers;

namespace Domus.ReportingPages
{
    public partial class ReportSelection : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //check to see if user is admin
            bool AdminCheck = Helper.IsRpoAdmin;

            if (AdminCheck == false)
            {
                BillingReportLink.Visible = false;
                //Cmallory task 00535 - Added to hide the Adoption Report link if a user isn't an admin.
                AdoptionReportLink.Visible = false;
                //Cmallory task 00553 - Added to hide the Daily Recon Report link if a user isn't an admin.
                DailyReconReportLink.Visible = false;
                //SWitherspoon Task 00662: Commented out. Was causing an error after hiding "DetailedDepositAnalysisReport" link
                //AnalysisReportArea.Visible = false;
                billingarea.Visible = false;
            }


        }
    }
}