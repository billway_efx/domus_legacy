﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using EfxFramework;
using EfxFramework.Helpers;

namespace Domus.ReportingPages
{
    public partial class PaymentSummaryReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string GetPropertyString()
        {

            CheckBoxList ckbx = (CheckBoxList)this.PropertyPicker.FindControl("CheckBoxList1");
            List<String> PropertyID_list = new List<string>();
            List<String> PropertyName_list = new List<string>();

            foreach (ListItem item in ckbx.Items)
            {
                if (item.Selected)
                {
                    PropertyID_list.Add(item.Value);
                    PropertyName_list.Add(item.Text);
                }
            }
            string PropList = String.Join(",", PropertyID_list.ToArray());
            return PropList;
        }

        protected void ViewReportbtn_Click(object sender, EventArgs e)
        {
            TextBox StartDate = (TextBox)this.PropertyPicker.FindControl("StartDatetxt");
            TextBox EndDate = (TextBox)this.PropertyPicker.FindControl("EndDatetxt");
            PaymentSummarySQL.DataBind();
            HiddenStart.Value = StartDate.Text;
            HiddenEnd.Value = EndDate.Text;
            HiddenPropertyList.Value = GetPropertyString();

            UpdatePanel1.DataBind();
            if (GridView1.Rows.Count < 1)
            {
                Gridresultlbl.Text = "Your selection produced no results";
            }
            else
            {
                Gridresultlbl.Text = "";
            }



        }


        protected void Exportbtn_Click(object sender, EventArgs e)
        {
            if (GridView1.Rows.Count > 0)
            {
                GenerateReport();
            }
        }


        private void GenerateReport()
        {

            string ExportTypeValue = ExportTypeDDL.SelectedValue.ToString();
            string ExportTypeText = ExportTypeDDL.SelectedItem.Text;

            // Setup DataSet
            DataSet ds = GetReportDataSet();

            // Create Report DataSource
            ReportDataSource rds = new ReportDataSource("PaymentSummary", ds.Tables[0]);

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = ExportTypeValue;
            string encoding = "UTF-8";
            string extension = string.Empty;

            // Setup the report viewer object and get the array of bytes
            ReportViewer viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;
            // Patrick Whittingham - 8/25/15 - task #412 : add report parms
            TextBox StartDate = (TextBox)this.PropertyPicker.FindControl("StartDatetxt");
            TextBox EndDate = (TextBox)this.PropertyPicker.FindControl("EndDatetxt");

            ReportParameter[] parameters = new ReportParameter[2];
            parameters[0] = new ReportParameter("pStartDate", StartDate.Text, false);
            parameters[1] = new ReportParameter("pEndDate", EndDate.Text, false);
            viewer.LocalReport.ReportPath = "ReportingPages/SSRSreports/PaymentSummaryReport.rdlc";
            viewer.LocalReport.SetParameters(parameters);
            viewer.LocalReport.DataSources.Add(rds); // Add datasource here

            byte[] bytes = viewer.LocalReport.Render(ExportTypeText, null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            string fileName = "Payment Summary Report";

            // Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName + "." + extension);
            Response.BinaryWrite(bytes); // create the file
            Response.End();
            Response.Flush(); // send it to the client to download
            Response.Close();
        }


        private DataSet GetReportDataSet()
        {
            string ReportStartDate = HiddenStart.Value.ToString();
            string ReportEndDate = HiddenEnd.Value.ToString();
            string propertyID = HiddenPropertyList.Value.ToString();

            SqlConnection con = new SqlConnection(ConnString);
            {
                SqlCommand com = new SqlCommand("usp_Report_RPO_PaymentSummary_Export", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("@StartDate", SqlDbType.Date).Value = ReportStartDate;
                com.Parameters.Add("@EndDate", SqlDbType.Date).Value = ReportEndDate;
                com.Parameters.Add("@PropertyList", SqlDbType.NVarChar, -1).Value = propertyID;
                com.CommandTimeout = 360;

                con.Open();
                SqlDataAdapter SqlAdapt = new SqlDataAdapter(com);
                //SqlAdapt.SelectCommand = com;
                DataSet DS = new DataSet();
                SqlAdapt.Fill(DS, "PaymentSummary");
                con.Close();
                return DS;
            }

        }

        public SqlDataReader GetSubReportDetail(int PropertyID, string Datepaid, string TransType, string PayStatus, string DepositDate)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Report_RPO_PaymentSummaryDetail_V2", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = PropertyID;
            com.Parameters.Add("@PaymentDate", SqlDbType.Date).Value = Datepaid;
            com.Parameters.Add("@TransactionType", SqlDbType.NVarChar, 20).Value = TransType;
            com.Parameters.Add("@PaymentStatusDescription", SqlDbType.NVarChar, 20).Value = PayStatus;
            com.Parameters.Add("@DepositDate", SqlDbType.Date).Value = DepositDate;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                con.Close();
                throw e;
            }

        }

        //PropertyID,DatePaidGroup,PaymentMethod,RentPaidStatus
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GridView1.SelectedIndex > -1)
            {
                String DateString = GridView1.DataKeys[GridView1.SelectedIndex].Values["PaymentDate"].ToString();
                int PropID = (int)GridView1.DataKeys[GridView1.SelectedIndex].Values["PropertyID"];
                string PayMethod = GridView1.DataKeys[GridView1.SelectedIndex].Values["TransactionType"].ToString();
                string PayStatus = GridView1.DataKeys[GridView1.SelectedIndex].Values["PaymentStatusDescription"].ToString();
                string DepositDate = GridView1.DataKeys[GridView1.SelectedIndex].Values["DepositDate"].ToString();

                GridView2.DataSource = GetSubReportDetail(PropID, DateString, PayMethod, PayStatus, DepositDate);
                GridView2.DataBind();

                ModalPopupExtender1.Show();
            }
        }



        private static String ConnString
        {
            get
            {
                return EfxFramework.EfxSettings.ReportConnectionString;
            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[5].Text == "Returned")
                {
                    e.Row.Cells[7].ForeColor = System.Drawing.Color.Red;
                }
            }
        }

        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[5].Text == "Returned")
                {
                    e.Row.Cells[9].ForeColor = System.Drawing.Color.Red;
                }
            }
        }





    }
}