﻿<%@ Page Language="C#" MasterPageFile="~/frontend.master"  AutoEventWireup="true" CodeBehind="TransactionSummaryReport.aspx.cs" Inherits="Domus.ReportingPages.TransactionSummaryReport" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/ReportingPages/UserControls/PropertyPicker.ascx" TagPrefix="uc1" TagName="PropertyPicker" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <!--cakel: BUGID00278 Added New Page and Controls -->
    <div>
        <h1>Reports</h1>

    </div>


    <div class="main-container">

        <div class="report-header selected" style="width: 40%;">
            <a class="report-nav" href="/ReportingPages/ReportSelection.aspx">
                <span class="back" aria-hidden="true" data-icon="2"></span>
                <span class="default-nav">Transaction Summary</span></a>
        </div>


        <div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="well no-padding">
                    <table>
                        <tr>
                            <td>
                                <uc1:PropertyPicker runat="server" ID="PropertyPicker" />

                            </td>
                            <td style="width: 106px">
                                <div class="divider-fluid">
                                    <div class="padding">
                                        <asp:Button ID="ViewReportbtn" runat="server" Text="View Report" CssClass="btn btn-primary" OnClick="ViewReportbtn_Click" />
 
                                    </div>
                                </div>

                            </td>

                        </tr>
                    </table>
                </div>


            </ContentTemplate>
        </asp:UpdatePanel>

        <div style="padding-top: 2px; padding-bottom: 10px">
        <div class="export-select">
            <asp:DropDownList ID="ExportTypeDDL" runat="server" CssClass="form-control" Width="150px">
                <asp:ListItem Value="application/excel">Excel</asp:ListItem>
                <asp:ListItem Value="application/pdf">PDF</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="Exportbtn" runat="server" Text="Export" CssClass="btn btn-small" data-icon="H" OnClick="Exportbtn_Click" />
        </div>
        </div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <br />
                <div>
                    
                                                           

                    <br />
                    <asp:HiddenField ID="HiddenStart" runat="server" />
                    <asp:HiddenField ID="HiddenEnd" runat="server" />
                    <asp:HiddenField ID="HiddenPropertyList" runat="server" />



                    <div style="text-align: center; margin-top: 10px;">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <img src="../Images/report_wait.gif" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:Label ID="Gridresultlbl" runat="server"></asp:Label>
                    </div>

                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" PageSize="20" CellPadding="10" Width="98%" GridLines="Horizontal" DataSourceID="TransactionSummarySQL">
                        <Columns>
                            <asp:BoundField DataField="PaymentMethod" HeaderText="Payment Method" SortExpression="PaymentMethod" ReadOnly="True" />
                            <asp:BoundField DataField="PaymentCount" HeaderText="Payment Count" SortExpression="PaymentCount" ReadOnly="True"></asp:BoundField>
                            <asp:BoundField DataField="Payment" HeaderText="Payment" SortExpression="Payment" ReadOnly="True" DataFormatString="{0:c}" />
                        </Columns>
                        <HeaderStyle CssClass="ReportTableHeader" Wrap="False" />
                        <PagerSettings PageButtonCount="30" />
                        <PagerStyle Font-Size="18px" />
                        <RowStyle CssClass="ReportTableData" />
                        <SelectedRowStyle BackColor="#E6E6E6" />
                    </asp:GridView>

                    <asp:SqlDataSource ID="TransactionSummarySQL" runat="server" ConnectionString="<%$ ConnectionStrings:ReportsConnection %>" SelectCommand="usp_Report_RPO_TransactionSummary" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="HiddenStart" DbType="Date" Name="StartDate" PropertyName="Value" />
                            <asp:ControlParameter ControlID="HiddenEnd" DbType="Date" Name="EndDate" PropertyName="Value" />
                            <asp:ControlParameter ControlID="HiddenPropertyList" Name="PropertyIdList" PropertyName="Value" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>

                    

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>


    </div>

</asp:Content>