﻿<%@ Page Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="ApplicantPortalPaymentReport.aspx.cs" Inherits="Domus.ReportingPages.ApplicantPortalPaymentReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/ReportingPages/UserControls/PropertyPicker.ascx" TagPrefix="uc1" TagName="PropertyPicker" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!--cakel: BUGID00278 Added New Page and Controls -->
    <div>
        <h1>Reports</h1>

    </div>
    

    <div class="main-container">

        <div class="report-header selected" style="width: 45%;">
            <a class="report-nav" href="/ReportingPages/ReportSelection.aspx">
                <span class="back" aria-hidden="true" data-icon="2"></span>
                <span class="default-nav">Applicant Portal Payments</span></a>
        </div>

        <div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="well no-padding">
                    <table>
                        <tr>
                            <td>
                                <uc1:PropertyPicker runat="server" ID="PropertyPicker" />

                            </td>
                            <td style="width: 106px">
                                <div class="divider-fluid">
                                    <div class="padding">
                                        <asp:Button ID="ViewReportbtn" runat="server" Text="View Report" CssClass="btn btn-primary" OnClick="ViewReportbtn_Click" />
                                    </div>
                                </div>

                            </td>

                        </tr>
                    </table>
                </div>


            </ContentTemplate>
        </asp:UpdatePanel>

        <div style="padding-top: 2px; padding-bottom: 10px">
        <div class="export-select">
            <asp:DropDownList ID="ExportTypeDDL" runat="server" CssClass="form-control" Width="150px">
                <asp:ListItem Value="application/excel">Excel</asp:ListItem>
                <asp:ListItem Value="application/pdf">PDF</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="Exportbtn" runat="server" Text="Export" CssClass="btn btn-small" data-icon="H" OnClick="Exportbtn_Click" />
        </div>
            </div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div>
                    <br />
                    <asp:HiddenField ID="HiddenStart" runat="server" />
                    <asp:HiddenField ID="HiddenEnd" runat="server" />
                    <asp:HiddenField ID="HiddenPropertyList" runat="server" />

                    <div style="text-align: center; margin-top: 10px;">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <img src="../Images/report_wait.gif" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:Label ID="Gridresultlbl" runat="server"></asp:Label>
                    </div>


                </div>

                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" PageSize="30" CellPadding="10" Width="98%" GridLines="Horizontal" DataKeyNames="PropertyId,ApplicantId,InternalTransactionId,TransactionDate" DataSourceID="ApplicantPortalPaymentSQL" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                    <Columns>
                        <asp:BoundField DataField="PropertyId" HeaderText="Property Id" InsertVisible="False" ReadOnly="True" SortExpression="PropertyId" Visible="False" />
                        <asp:BoundField DataField="PropertyName" HeaderText="Property Name" SortExpression="PropertyName" />
                        <asp:BoundField DataField="ApplicantId" HeaderText="ApplicantId" InsertVisible="False" ReadOnly="True" SortExpression="ApplicantId" Visible="False" />
                        <asp:BoundField DataField="ApplicantName" HeaderText="Applicant Name" ReadOnly="True" SortExpression="ApplicantName" />
                        <asp:BoundField DataField="InternalTransactionId" HeaderText="Transaction ID" SortExpression="InternalTransactionId" />
                        <asp:BoundField DataField="TransactionDate" DataFormatString="{0:d}" HeaderText="Date" ReadOnly="True" SortExpression="TransactionDate" />
                        <asp:BoundField DataField="FeeCount" HeaderText="Fee Count" ReadOnly="True" SortExpression="FeeCount" />
                        <asp:BoundField DataField="Total" DataFormatString="{0:c}" HeaderText="Total" ReadOnly="True" SortExpression="Total" />
                        <asp:CommandField ShowSelectButton="True" />
                    </Columns>
                    <HeaderStyle CssClass="ReportTableHeader" />
                    <PagerSettings PageButtonCount="10" FirstPageText="First" LastPageText="Last" NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" Mode="NumericFirstLast" />
                    <PagerStyle Font-Size="16px" HorizontalAlign="Right" />
                    <RowStyle CssClass="ReportTableData" />
                    <SelectedRowStyle BackColor="#E6E6E6" />
                </asp:GridView>






                <asp:SqlDataSource ID="ApplicantPortalPaymentSQL" runat="server" ConnectionString="<%$ ConnectionStrings:ReportsConnection %>" SelectCommand="usp_Report_RPO_ApplicantPortalPaymentSummary" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenStart" DbType="Date" Name="StartDate" PropertyName="Value" />
                        <asp:ControlParameter ControlID="HiddenEnd" DbType="Date" Name="EndDate" PropertyName="Value" />
                        <asp:ControlParameter ControlID="HiddenPropertyList" Name="PropertyList" PropertyName="Value" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

                <div>
                    
                    <asp:Button ID="ModalShow" runat="server" Text="Button" Style="display: none;" />

                    <asp:Panel ID="PaymentDetailsPanel" runat="server" Width="60%" CssClass="subPanelBg">

                        <asp:Panel ID="Panel2" runat="server" Width="99%" ScrollBars="Vertical" CssClass="SubReportModal">
                            <div style="padding: 10px;">


                                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="5" GridLines="Horizontal" Width="98%" >
                                    <Columns>
                                        <asp:BoundField DataField="PaymentTypeName" HeaderText="Payment Type" />
                                        <asp:BoundField DataField="FeeName" HeaderText="Decription" />
                                        <asp:BoundField DataField="Amount" DataFormatString="{0:c}" HeaderText="Amount" />
                                    </Columns>
                                    <HeaderStyle CssClass="SubReportTable" />
                                    <RowStyle CssClass="SubReportTableData" />
                                </asp:GridView>

                            </div>

                        </asp:Panel>
                        <div style="padding: 5px 5px 10px 5px; text-align: center;">
                            <asp:Button ID="CloseModalbtn1" runat="server" Text="Close" CssClass="btn btn-primary" />
                        </div>

                    </asp:Panel>

                    <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="PaymentDetailsPanel" BackgroundCssClass="ReportmodalPop" CancelControlID="CloseModalbtn1" TargetControlID="ModalShow"></asp:ModalPopupExtender>

                       
                </div>




            </ContentTemplate>
        </asp:UpdatePanel>





    </div>




</asp:Content>
