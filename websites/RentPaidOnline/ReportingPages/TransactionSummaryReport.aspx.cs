﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using EfxFramework;
using EfxFramework.Helpers;

namespace Domus.ReportingPages
{
    public partial class TransactionSummaryReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string GetPropertyString()
        {

            CheckBoxList ckbx = (CheckBoxList)this.PropertyPicker.FindControl("CheckBoxList1");
            List<String> PropertyID_list = new List<string>();
            List<String> PropertyName_list = new List<string>();

            foreach (ListItem item in ckbx.Items)
            {
                if (item.Selected)
                {
                    PropertyID_list.Add(item.Value);
                    PropertyName_list.Add(item.Text);
                }
            }
            string PropList = String.Join(",", PropertyID_list.ToArray());
            return PropList;
        }

        protected void ViewReportbtn_Click(object sender, EventArgs e)
        {

            //check to see if user is admin
            bool AdminCheck = Helper.IsRpoAdmin;
            //Get Current Userid
            int CurrentUser = EfxFramework.Helpers.Helper.CurrentUserId;

            TextBox StartDate = (TextBox)this.PropertyPicker.FindControl("StartDatetxt");
            TextBox EndDate = (TextBox)this.PropertyPicker.FindControl("EndDatetxt");
            
            HiddenStart.Value = StartDate.Text;
            HiddenEnd.Value = EndDate.Text;
            HiddenPropertyList.Value = GetPropertyString();

            if (AdminCheck == false)
            {
                TransactionSummarySQL.DataBind();
            }
            else
            {
                TransactionSummarySQL.SelectCommand = "usp_Report_RPO_TransactionSummaryWithFees";
                TransactionSummarySQL.DataBind();
            }

            UpdatePanel1.DataBind();
            if (GridView1.Rows.Count < 1)
            {
                Gridresultlbl.Text = "Your selection produced no results";
            }
            else
            {
                Gridresultlbl.Text = "";
            }

            
        }


        protected void Exportbtn_Click(object sender, EventArgs e)
        {
            if (GridView1.Rows.Count > 0)
            {
                GenerateReport();
            }
        }


        private void GenerateReport()
        {

            string ExportTypeValue = ExportTypeDDL.SelectedValue.ToString();
            string ExportTypeText = ExportTypeDDL.SelectedItem.Text;

            // Setup DataSet
            DataSet ds = GetReportDataSet();

            // Create Report DataSource
            ReportDataSource rds = new ReportDataSource("TransSummary", ds.Tables[0]);

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = ExportTypeValue;
            string encoding = "UTF-8";
            string extension = string.Empty;

            // Setup the report viewer object and get the array of bytes
            ReportViewer viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.ReportPath = "ReportingPages/SSRSreports/TransactionSummaryReport.rdlc";
            viewer.LocalReport.DataSources.Add(rds); // Add datasource here


            byte[] bytes = viewer.LocalReport.Render(ExportTypeText, null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            string fileName = "Transaction Summary Report";

            // Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName + "." + extension);
            Response.BinaryWrite(bytes); // create the file
            Response.End();
            Response.Flush(); // send it to the client to download
            Response.Close();
        }


        private DataSet GetReportDataSet()
        {
            bool AdminCheck = Helper.IsRpoAdmin;
            string ComStr = "";

            if (AdminCheck == true)
            {
                ComStr = "usp_Report_RPO_TransactionSummaryWithFees";
            }
            else
            {
                ComStr = "usp_Report_RPO_TransactionSummary";
            }

            string ReportStartDate = HiddenStart.Value.ToString();
            string ReportEndDate = HiddenEnd.Value.ToString();
            string propertyID = HiddenPropertyList.Value.ToString();

            SqlConnection con = new SqlConnection(ConnString);
            {
                SqlCommand com = new SqlCommand(ComStr, con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("@StartDate", SqlDbType.Date).Value = ReportStartDate;
                com.Parameters.Add("@EndDate", SqlDbType.Date).Value = ReportEndDate;
                com.Parameters.Add("@PropertyIdList", SqlDbType.NVarChar, -1).Value = propertyID;
                com.CommandTimeout = 360;

                con.Open();
                SqlDataAdapter SqlAdapt = new SqlDataAdapter(com);
                //SqlAdapt.SelectCommand = com;
                DataSet DS = new DataSet();
                SqlAdapt.Fill(DS, "TransSummary");
                con.Close();
                return DS;
            }

        }



        private static String ConnString
        {
            get
            {
                return EfxFramework.EfxSettings.ReportConnectionString;
            }
        }


    }
}