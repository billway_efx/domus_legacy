﻿<%@ Page Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="PaymentExportSummary.aspx.cs" Inherits="Domus.ReportingPages.PaymentExportSummary" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/ReportingPages/UserControls/PropertyPicker.ascx" TagPrefix="uc1" TagName="PropertyPicker" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <!--cakel: BUGID00278 Added New Page and Controls -->
    <div>
        <h1>Reports</h1>

    </div>


    <div class="main-container">

        <div class="report-header selected" style="width: 45%;">
            <a class="report-nav" href="/ReportingPages/ReportSelection.aspx">
                <span class="back" aria-hidden="true" data-icon="2"></span>
                <span class="default-nav">Payment Export Summary</span></a>
        </div>


        <div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="well no-padding">
                    <table>
                        <tr>
                            <td>
                                <uc1:PropertyPicker runat="server" ID="PropertyPicker" />

                            </td>
                            <td style="width: 106px">
                                <div class="divider-fluid">
                                    <div class="padding">
                                        <asp:Button ID="ViewReportbtn" runat="server" Text="View Report" CssClass="btn btn-primary" OnClick="ViewReportbtn_Click" />
 
                                    </div>
                                </div>

                            </td>

                        </tr>
                    </table>
                </div>


            </ContentTemplate>
        </asp:UpdatePanel>

        <div style="padding-top: 2px; padding-bottom: 10px">
        <div class="export-select">
            <asp:DropDownList ID="ExportTypeDDL" runat="server" CssClass="form-control" Width="150px">
                <asp:ListItem Value="application/excel">Excel</asp:ListItem>
                <asp:ListItem Value="application/pdf">PDF</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="Exportbtn" runat="server" Text="Export" CssClass="btn btn-small" data-icon="H" OnClick="Exportbtn_Click" />
        </div>
        </div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ContentTemplate>
                <br />
                <div>
                                                 

                    <br />
                    <asp:HiddenField ID="HiddenStart" runat="server" />
                    <asp:HiddenField ID="HiddenEnd" runat="server" />
                    <asp:HiddenField ID="HiddenPropertyList" runat="server" />
                    <div style="text-align: center; margin-top: 10px;">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <img src="../Images/report_wait.gif" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:Label ID="Gridresultlbl" runat="server"></asp:Label>
                    </div>

                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" PageSize="100" CellPadding="10" Width="98%" GridLines="Horizontal" DataSourceID="PaymentSummarySQL" DataKeyNames="PropertyID,BatchId,PaymentExportSummaryId" OnRowDataBound="GridView1_RowDataBound" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="ExportDate" HeaderText="Date Sent" SortExpression="ExportDate" DataFormatString="{0:d}" ReadOnly="True" />
                            <asp:BoundField DataField="PropertyId" HeaderText="PropertyId" SortExpression="PropertyId" Visible="False"></asp:BoundField>
                            <asp:BoundField DataField="PropertyName" HeaderText="Property Name" SortExpression="PropertyName" />
                            <asp:BoundField DataField="Amount" HeaderText="Amount" SortExpression="Amount" DataFormatString="{0:c}" />
                            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" ReadOnly="True" />
                            <asp:BoundField DataField="PaymentType" HeaderText="Type" SortExpression="PaymentType" />
                            <asp:BoundField DataField="BatchId" HeaderText="Batch ID" SortExpression="BatchId" />
                            <asp:BoundField DataField="BatchCount" HeaderText="Batch Count" SortExpression="BatchCount" ReadOnly="True" />

                            <asp:BoundField DataField="PaymentExportSummaryId" HeaderText="PaymentExportSummaryId" InsertVisible="False" ReadOnly="True" SortExpression="PaymentExportSummaryId" Visible="False" />

                            <asp:TemplateField ShowHeader="False" HeaderText="View Result">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Select" Text="Success"></asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Select" Text="Failure"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="ReportTableHeader" Wrap="False" />
                        <PagerSettings PageButtonCount="10" FirstPageText="First" LastPageText="Last" NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" Mode="NumericFirstLast" />
                    <PagerStyle Font-Size="16px" HorizontalAlign="Right" />
                        <RowStyle CssClass="ReportTableData" />
                        <SelectedRowStyle BackColor="#E6E6E6" />
                    </asp:GridView>

                    <asp:SqlDataSource ID="PaymentSummarySQL" runat="server" ConnectionString="<%$ ConnectionStrings:ReportsConnection %>" SelectCommand="usp_Report_RPO_PaymentExportSummary_V2" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="HiddenStart" DbType="Date" Name="StartDate" PropertyName="Value" />
                            <asp:ControlParameter ControlID="HiddenEnd" DbType="Date" Name="EndDate" PropertyName="Value" />
                            <asp:ControlParameter ControlID="HiddenPropertyList" Name="PropertyIdList" PropertyName="Value" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>

                    <div>

                        <asp:Button ID="ModalShow" runat="server" Text="Button" Style="display: none;" />
                        <asp:Button ID="ModalShow2" runat="server" Text="Button" Style="display: none;" />

                        <asp:Panel ID="PaymentDetailsPanel" runat="server" Width="60%" CssClass="subPanelBg">

                            <asp:Panel ID="Panel2" runat="server" Width="99%" ScrollBars="Vertical" CssClass="SubReportModal">
                                <div style="padding: 10px;">


                                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="5" GridLines="Horizontal" Width="99%">
                                        <Columns>
                                            <asp:BoundField DataField="TransactionDateTime" HeaderText="Payment Date" DataFormatString="{0:D}" />
                                            <asp:BoundField DataField="BatchId" HeaderText="BatchId" />
                                            <asp:BoundField DataField="PmsId" HeaderText="Renter ID" />
                                            <asp:BoundField DataField="RenterName" HeaderText="RenterName" />
                                            <asp:BoundField DataField="PaymentType" HeaderText="Payment Type" />
                                            <asp:BoundField DataField="Amount" DataFormatString="{0:c}" HeaderText="Amount" />
                                        </Columns>
                                        <HeaderStyle CssClass="SubReportTable" />
                                        <RowStyle CssClass="SubReportTableData" />
                                    </asp:GridView>

                                </div>

                            </asp:Panel>
                            <div style="padding: 5px 5px 10px 5px; text-align: center;">
                                <asp:Button ID="CloseModalbtn1" runat="server" Text="Close" CssClass="btn btn-primary" />
                            </div>

                        </asp:Panel>

                        <asp:Panel ID="PaymentDetailsPanelFailure" runat="server" Width="50%" CssClass="subPanelBg">

                            <asp:Panel ID="Panel2Failure" runat="server" Width="99%" ScrollBars="Vertical" CssClass="SubReportModal">
                                <div style="padding: 10px;">
                                <h3>Export Faliure Details</h3>
                                                                        
                                    <br />
                                      <h4>
                                          Response XML
                                      </h4>
                                     <p style="-ms-word-wrap: break-word;">
                                    <asp:Label ID="RespXMLlbl" runat="server" Text="Label"></asp:Label>
                                         </p>

                                    <h4>
                                        Request XML
                                    </h4>
                                    <asp:GridView ID="ErrorDetailListGrid" runat="server" AutoGenerateColumns="False" CellPadding="5" GridLines="Horizontal" Width="99%">
                                        <Columns>
                                            <asp:BoundField DataField="RecordID" HeaderText="Record ID" />
                                            <asp:BoundField DataField="PaymentID" HeaderText="Payment ID" />
                                            <asp:BoundField DataField="RenterID" HeaderText="Renter ID" Visible="False" />
                                            <asp:BoundField DataField="CustomerNumber" HeaderText="Tennant ID" />
                                            <asp:BoundField DataField="PaidBy" HeaderText="Name" />
                                            <asp:BoundField DataField="Method" HeaderText="Method" />
                                        </Columns>
                                        <HeaderStyle CssClass="SubReportTable" />
                                        <RowStyle CssClass="SubReportTableData" />
                                    </asp:GridView>

                                    <br />
                                    <h4>Raw Request XML</h4>
                                    <p style="-ms-word-wrap: break-word;">
                                        <asp:Label ID="RawReqXMLlbl" runat="server" Text="Label"></asp:Label>

                                        </p>

                                </div>
                            </asp:Panel>
                            <div style="padding: 5px 5px 10px 5px; text-align: center;">
                                <asp:Button ID="CloseModalbtn2" runat="server" Text="Close" CssClass="btn btn-primary" />
                            </div>

                        </asp:Panel>


                        <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="PaymentDetailsPanel" BackgroundCssClass="ReportmodalPop" CancelControlID="CloseModalbtn1" TargetControlID="ModalShow"></asp:ModalPopupExtender>
                        <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="PaymentDetailsPanelFailure" BackgroundCssClass="ReportmodalPop" CancelControlID="CloseModalbtn2" TargetControlID="ModalShow2"></asp:ModalPopupExtender>

                    </div>

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>


    </div>

</asp:Content>
