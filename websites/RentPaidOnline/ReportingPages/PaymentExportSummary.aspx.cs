﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using EfxFramework;
using EfxFramework.Helpers;

namespace Domus.ReportingPages
{
    public partial class PaymentExportSummary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string GetPropertyString()
        {

            CheckBoxList ckbx = (CheckBoxList)this.PropertyPicker.FindControl("CheckBoxList1");
            List<String> PropertyID_list = new List<string>();
            List<String> PropertyName_list = new List<string>();

            foreach (ListItem item in ckbx.Items)
            {
                if (item.Selected)
                {
                    PropertyID_list.Add(item.Value);
                    PropertyName_list.Add(item.Text);
                }
            }
            string PropList = String.Join(",", PropertyID_list.ToArray());
            return PropList;
        }

        protected void ViewReportbtn_Click(object sender, EventArgs e)
        {
            TextBox StartDate = (TextBox)this.PropertyPicker.FindControl("StartDatetxt");
            TextBox EndDate = (TextBox)this.PropertyPicker.FindControl("EndDatetxt");
            PaymentSummarySQL.DataBind();
            HiddenStart.Value = StartDate.Text;
            HiddenEnd.Value = EndDate.Text;
            HiddenPropertyList.Value = GetPropertyString();

            UpdatePanel1.DataBind();
            if (GridView1.Rows.Count < 1)
            {
                Gridresultlbl.Text = "Your selection produced no results";
            }
            else
            {
                Gridresultlbl.Text = "";
            }

            
        }


        protected void Exportbtn_Click(object sender, EventArgs e)
        {
            if (GridView1.Rows.Count > 0)
            {
                GenerateReport();
            }
        }


        private void GenerateReport()
        {

            string ExportTypeValue = ExportTypeDDL.SelectedValue.ToString();
            string ExportTypeText = ExportTypeDDL.SelectedItem.Text;

            // Setup DataSet
            DataSet ds = GetReportDataSet();

            // Create Report DataSource
            ReportDataSource rds = new ReportDataSource("PaymentExportSummary", ds.Tables[0]);

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = ExportTypeValue;
            string encoding = "UTF-8";
            string extension = string.Empty;

            // Setup the report viewer object and get the array of bytes
            ReportViewer viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.ReportPath = "ReportingPages/SSRSreports/PaymentExportSummaryReport.rdlc";
            viewer.LocalReport.DataSources.Add(rds); // Add datasource here


            byte[] bytes = viewer.LocalReport.Render(ExportTypeText, null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            string fileName = "Payment Export Summary Report";

            // Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName + "." + extension);
            Response.BinaryWrite(bytes); // create the file
            Response.End();
            Response.Flush(); // send it to the client to download
            Response.Close();
        }

        //Data for Report Export
        private DataSet GetReportDataSet()
        {
            string ReportStartDate = HiddenStart.Value.ToString();
            string ReportEndDate = HiddenEnd.Value.ToString();
            string propertyID = HiddenPropertyList.Value.ToString();

            SqlConnection con = new SqlConnection(ConnString);
            {
                SqlCommand com = new SqlCommand("usp_Report_RPO_PaymentExportSummary_Export", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("@StartDate", SqlDbType.Date).Value = ReportStartDate;
                com.Parameters.Add("@EndDate", SqlDbType.Date).Value = ReportEndDate;
                com.Parameters.Add("@PropertyList", SqlDbType.NVarChar, -1).Value = propertyID;
                com.CommandTimeout = 360;

                con.Open();
                SqlDataAdapter SqlAdapt = new SqlDataAdapter(com);
                //SqlAdapt.SelectCommand = com;
                DataSet DS = new DataSet();
                SqlAdapt.Fill(DS, "PaymentExportSummary");
                con.Close();
                return DS;
            }

        }

        public SqlDataReader GetSubReportDetail(int PropertyID, string BatchID)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Report_RPO_PaymentExportSummaryDetail", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = PropertyID;
            com.Parameters.Add("@BatchID", SqlDbType.NVarChar,30).Value = BatchID;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                con.Close();
                throw e;
            }

        }

        public SqlDataReader GetErrorDetails(int PaymentExportID)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Report_RPO_PaymentExportError", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PaymentExportSummaryId", SqlDbType.Int).Value = PaymentExportID;
      
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                con.Close();
                throw e;
            }
        }

        public SqlDataReader GetErrorDetailsList(int PaymentExportID)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Report_RPO_PaymentExportErrorDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PaymentExportSummaryId", SqlDbType.Int).Value = PaymentExportID;
      
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                con.Close();
                throw e;
            }
        }


        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GridView1.SelectedIndex > -1)
            {
                int PropID = (int)GridView1.DataKeys[GridView1.SelectedIndex].Values["PropertyID"];
                string BatchID = GridView1.DataKeys[GridView1.SelectedIndex].Values["BatchId"].ToString();

                string Stat = GridView1.SelectedRow.Cells[4].Text;
                if (Stat == "Export Failed")
                {
                    string Res = "";
                    string RawReq = "";
                    int PaymentErrorID = (int)GridView1.DataKeys[GridView1.SelectedIndex].Values["PaymentExportSummaryId"];

                    SqlDataReader Reader = GetErrorDetails(PaymentErrorID);
                    Reader.Read();
                    if (Reader.HasRows)
                    {
 
                        Res = Reader["ResponseXml"].ToString();
                        RawReq = Reader["RawRequestXML"].ToString();
                        RespXMLlbl.Text = Server.HtmlEncode(Res);
                        RawReqXMLlbl.Text = Server.HtmlEncode(RawReq);

                        ErrorDetailListGrid.DataSource = GetErrorDetailsList(PaymentErrorID);
                        ErrorDetailListGrid.DataBind();


                    }
                    ModalPopupExtender2.Show();
                }
                else
                {

                    GridView2.DataSource = GetSubReportDetail(PropID, BatchID);
                    GridView2.DataBind();
                    ModalPopupExtender1.Show();
                }
            }
        }



        private static String ConnString
        {
            get
            {
                return EfxFramework.EfxSettings.ReportConnectionString;
            }
        }



        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lb1 = (LinkButton)e.Row.FindControl("LinkButton1");
                LinkButton lb2 = (LinkButton)e.Row.FindControl("LinkButton2");
                string Stat = e.Row.Cells[4].Text;
                if (Stat == "Export Failed")
                {
                    lb1.Visible = false;
                    lb2.Visible = true;
                    e.Row.Cells[4].ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    lb1.Visible = true;
                    lb2.Visible = false;
                }

            }
        }




    }
}