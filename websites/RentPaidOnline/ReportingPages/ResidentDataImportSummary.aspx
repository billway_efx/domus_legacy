﻿<%@ Page Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="ResidentDataImportSummary.aspx.cs" Inherits="Domus.ReportingPages.ResidentDataImportSummary" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/ReportingPages/UserControls/PropertyPicker.ascx" TagPrefix="uc1" TagName="PropertyPicker" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <!--cakel: BUGID00278 Added New Page and Controls -->
    <div>
        <h1>Reports</h1>

    </div>
    

    <div class="main-container">

        <div class="report-header selected" style="width: 48%;">
            <a class="report-nav" href="/ReportingPages/ReportSelection.aspx">
                <span class="back" aria-hidden="true" data-icon="2"></span>
                <span class="default-nav">Resident Data Import Summary</span></a>
        </div>


        <div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="well no-padding">
                    <table>
                        <tr>
                            <td>
                                <uc1:PropertyPicker runat="server" ID="PropertyPicker" />

                            </td>
                            <td style="width: 106px">
                                <div class="divider-fluid">
                                    <div class="padding">
                                        <asp:Button ID="ViewReportbtn" runat="server" Text="View Report" CssClass="btn btn-primary" OnClick="ViewReportbtn_Click" />
                                    </div>
                                </div>

                            </td>

                        </tr>
                    </table>
                </div>


            </ContentTemplate>
        </asp:UpdatePanel>

        <div style="padding-top: 2px; padding-bottom: 10px">
            <div class="export-select">
                <asp:DropDownList ID="ExportTypeDDL" runat="server" CssClass="form-control" Width="150px">
                    <asp:ListItem Value="application/excel">Excel</asp:ListItem>
                    <asp:ListItem Value="application/pdf">PDF</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="Exportbtn" runat="server" Text="Export" CssClass="btn btn-small" data-icon="H" OnClick="Exportbtn_Click" />

            </div>
            <br />
        </div>

                <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                <div>
     
 
                    <br />
                    <asp:HiddenField ID="HiddenStart" runat="server" />
                    <asp:HiddenField ID="HiddenEnd" runat="server" />
                    <asp:HiddenField ID="HiddenPropertyList" runat="server" />
                    <div style="text-align: center; margin-top: 10px;">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <img src="../Images/report_wait.gif" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:Label ID="Gridresultlbl" runat="server"></asp:Label>
                    </div>
                    
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" PageSize="20" CellPadding="10" Width="98%" GridLines="Horizontal" DataSourceID="ResidentDateImportSQL">
                        <Columns>
                            <asp:BoundField DataField="ResidentImportSummaryId" HeaderText="ResidentImportSummaryId" SortExpression="ResidentImportSummaryId" Visible="False" />
                            <asp:BoundField DataField="ImportDatetime" HeaderText="Import Date" SortExpression="ImportDatetime" >
                            <ItemStyle Font-Size="12px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PropertyId" HeaderText="PropertyId" SortExpression="PropertyId" />
                            <asp:BoundField DataField="PropertyName" HeaderText="Property" SortExpression="PropertyName" />
                            <asp:BoundField DataField="SuccessfulRecordCount" HeaderText="Successful Records" SortExpression="SuccessfulRecordCount" />
                            <asp:BoundField DataField="FailedRecordCount" HeaderText="Failed Records" SortExpression="FailedRecordCount" />
                            <asp:BoundField DataField="LastUpdate" HeaderText="Last Full Import" SortExpression="LastUpdate" />
                            <asp:BoundField DataField="TimeZone" HeaderText="TimeZone" ReadOnly="True" SortExpression="TimeZone" />
                        </Columns>
                        <HeaderStyle CssClass="ReportTableHeader" Wrap="False" />
                        <PagerSettings PageButtonCount="10" FirstPageText="First" LastPageText="Last" NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" Mode="NumericFirstLast" />
                    <PagerStyle Font-Size="16px" HorizontalAlign="Right" />
                        <RowStyle CssClass="ReportTableData" />
                        <SelectedRowStyle BackColor="#E6E6E6" />
                    </asp:GridView>

                    <asp:SqlDataSource ID="ResidentDateImportSQL" runat="server" ConnectionString="<%$ ConnectionStrings:ReportsConnection %>" SelectCommand="usp_Report_ResidentDataImportSummaryReport" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="HiddenStart" DbType="Date" Name="StartDate" PropertyName="Value" />
                            <asp:ControlParameter ControlID="HiddenEnd" DbType="Date" Name="EndDate" PropertyName="Value" />
                            <asp:ControlParameter ControlID="HiddenPropertyList" Name="PropertyIdList" PropertyName="Value" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>

                                   

                    </div>
                    </ContentTemplate></asp:UpdatePanel>


    </div>

</asp:Content>
