﻿using EfxFramework;
using Microsoft.Reporting.WebForms;
using RPO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace RentPaidOnline.ReportingPages
{
    //cmallory: Task 00535 Added New Page and Controls to generate Adoption Report 
    public partial class RpoAdoptionReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string GetPropertyString()
        {
            var checkBoxList = (CheckBoxList)this.PropertyPicker.FindControl("CheckBoxList1");
            var propertyIdList = new List<string>();
            var propertyNameList = new List<string>();

            foreach (ListItem item in checkBoxList.Items)
            {
                if (!item.Selected) continue;

                propertyIdList.Add(item.Value);
                propertyNameList.Add(item.Text);
            }
            var propList = string.Join(",", propertyIdList.ToArray());
            return propList;
        }

        protected void ViewReportbtn_Click(object sender, EventArgs e)
        {
            HiddenPropertyList.Value = GetPropertyString();
            RPOAdoptionReportSQL.DataBind();

            UpdatePanel1.DataBind();
            Gridresultlbl.Text = GridView1.Rows.Count < 1 ? "Your selection produced no results" : "";
        }


        protected void Exportbtn_Click(object sender, EventArgs e)
        {
            if (GridView1.Rows.Count > 0)
            {
                GenerateReport();
            }
        }


        private void GenerateReport()
        {
            var al = new ActivityLog();
            var exportTypeValue = ExportTypeDDL.SelectedValue;
            var exportTypeText = ExportTypeDDL.SelectedItem.Text;

            // Setup DataSet
            var ds = GetReportDataSet();
            var rds = new ReportDataSource("RPOAdoptionReport", ds.Tables[0]);
            var viewer = new ReportViewer();

            try
            {
                viewer.ProcessingMode = ProcessingMode.Local;
                viewer.LocalReport.ReportPath = "ReportingPages/SSRSreports/RPOAdoptionReport.rdlc";
                viewer.LocalReport.DataSources.Add(rds); 

                var bytes = viewer.LocalReport.Render(exportTypeText, null, out var mimeType, out _, out var extension, out _, out _);

                Response.Buffer = true;
                Response.Clear();
                Response.ContentType = mimeType;
                Response.AddHeader("content-disposition", "attachment; filename=RPO Adoption Report " + DateTime.Now.ToShortDateString() + "." + extension);
                Response.BinaryWrite(bytes); 
                Response.End();
                Response.Flush(); 
                Response.Close();
            }
            catch (Exception e)
            {
                al.WriteLog(RPO.Helpers.Helper.CurrentUserId.ToString(), e.Message, (short)LogPriority.LogAlways);
            }
        }

        //Data for Report Export`
        private DataSet GetReportDataSet()
        {
            var propertyId = HiddenPropertyList.Value;

            using (var con = new SqlConnection(EfxSettings.ReportConnectionString))
            {
                con.Open();

                using (var com = new SqlCommand("usp_Report_RPO_AdoptionReport", con))
                {
                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 360;

                    com.Parameters.Add("@PropertyIDList", SqlDbType.NVarChar, -1).Value = propertyId;

                    using (var sqlAdapt = new SqlDataAdapter(com))
                    {
                        var ds = new DataSet();
                        sqlAdapt.Fill(ds, "Data");

                        return ds;
                    }
                }
            }

        }
    }
}