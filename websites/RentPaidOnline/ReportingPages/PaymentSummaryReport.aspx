﻿<%@ Page Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="PaymentSummaryReport.aspx.cs" Inherits="Domus.ReportingPages.PaymentSummaryReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/ReportingPages/UserControls/PropertyPicker.ascx" TagPrefix="uc1" TagName="PropertyPicker" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <!--cakel: BUGID00278 Added New Page and Controls -->
    <div>
        <h1>Reports</h1>

    </div>


    <div class="main-container">

        <div class="report-header selected" style="width:40%;">
            <a class="report-nav" href="/ReportingPages/ReportSelection.aspx">
                <span class="back" aria-hidden="true" data-icon="2"></span>
                <span class="default-nav">Payment Summary</span></a>
        </div>


        <div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="well no-padding">
                    <table>
                        <tr>
                            <td>
                                <uc1:PropertyPicker runat="server" ID="PropertyPicker" />

                            </td>
                            <td style="width: 106px">
                                <div class="divider-fluid">
                                    <div class="padding">
                                        <asp:Button ID="ViewReportbtn" runat="server" Text="View Report" CssClass="btn btn-primary" OnClick="ViewReportbtn_Click" />
                                    </div>
                                </div>

                            </td>

                        </tr>
                    </table>
                </div>


            </ContentTemplate>
        </asp:UpdatePanel>

        <div style="padding-top: 2px; padding-bottom: 10px">
            <div class="export-select">
                <asp:DropDownList ID="ExportTypeDDL" runat="server" CssClass="form-control" Width="150px">
                    <asp:ListItem Value="application/excel">Excel</asp:ListItem>
                    <asp:ListItem Value="application/pdf">PDF</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="Exportbtn" runat="server" Text="Export" CssClass="btn btn-small" data-icon="H" OnClick="Exportbtn_Click" />

            </div>
        </div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div>
  

                    <br />
                    <asp:HiddenField ID="HiddenStart" runat="server" />
                    <asp:HiddenField ID="HiddenEnd" runat="server" />
                    <asp:HiddenField ID="HiddenPropertyList" runat="server" />
                    <div style="text-align: center; margin-top: 10px;">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <img src="../Images/report_wait.gif" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:Label ID="Gridresultlbl" runat="server"></asp:Label>
                    </div>

                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" PageSize="20" CellPadding="10" Width="98%" GridLines="Horizontal" DataSourceID="PaymentSummarySQL" DataKeyNames="PropertyID,PaymentDate,DepositDate,TransactionType,PaymentStatusDescription" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" OnRowDataBound="GridView1_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="PaymentDate" HeaderText="Payment Date" SortExpression="PaymentDate" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="DepositDate" HeaderText="Deposit Date" SortExpression="DepositDate" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="PropertyID" HeaderText="PropertyID" SortExpression="PropertyID" Visible="False" />
                            <asp:BoundField DataField="PropertyName" HeaderText="Property Name" SortExpression="PropertyName" />
                            <asp:BoundField DataField="TransactionType" HeaderText="Type" SortExpression="TransactionType" />
                            <asp:BoundField DataField="PaymentStatusDescription" HeaderText="Status" SortExpression="PaymentStatusDescription" />
                            <asp:BoundField DataField="TransCount" HeaderText="Count" SortExpression="TransCount" />
                            <asp:BoundField DataField="Total" HeaderText="Total" SortExpression="Total" DataFormatString="{0:c}" />
                            <asp:CommandField SelectText="View" ShowSelectButton="True" />
                        </Columns>
                        <HeaderStyle CssClass="ReportTableHeader" Wrap="False" />
                        <PagerSettings PageButtonCount="10" FirstPageText="First" LastPageText="Last" NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" Mode="NumericFirstLast" />
                    <PagerStyle Font-Size="16px" HorizontalAlign="Right" />
                        <RowStyle CssClass="ReportTableData" />
                        <SelectedRowStyle BackColor="#E6E6E6" />
                    </asp:GridView>

                    <asp:SqlDataSource ID="PaymentSummarySQL" runat="server" ConnectionString="<%$ ConnectionStrings:ReportsConnection %>" SelectCommand="usp_Report_RPO_PaymentSummary_V2" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="HiddenStart" DbType="Date" Name="StartDate" PropertyName="Value" />
                            <asp:ControlParameter ControlID="HiddenEnd" DbType="Date" Name="EndDate" PropertyName="Value" />
                            <asp:ControlParameter ControlID="HiddenPropertyList" Name="PropertyList" PropertyName="Value" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>

                    <div>

                        <asp:Button ID="ModalShow" runat="server" Text="Button" Style="display: none;" />

                        <asp:Panel ID="PaymentDetailsPanel" runat="server" Width="85%" CssClass="subPanelBg">

                            <asp:Panel ID="Panel2" runat="server" Width="99%" ScrollBars="Vertical" CssClass="SubReportModal">
                                <div style="padding: 10px;">


                                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="5" GridLines="Horizontal" Width="99%" OnRowDataBound="GridView2_RowDataBound">
                                        <Columns>
                                            <asp:BoundField DataField="RenterId" HeaderText="RenterId" Visible="False" />
                                            <asp:BoundField DataField="PaymentDateTime" HeaderText="Payment Date (EST)" />
                                            <asp:BoundField DataField="PaymentDate" HeaderText="Date Paid" Visible="False" />
                                            <asp:BoundField DataField="DepositDate" DataFormatString="{0:d}" HeaderText="Deposit Date" />
                                            <asp:BoundField DataField="BatchID" HeaderText="Batch ID" />
                                            <asp:BoundField DataField="PaymentStatusDescription" HeaderText="Status" />
                                            <asp:TemplateField HeaderText="Renter">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("RenterName") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="HyperLink3" runat="server" Text='<%# Eval("Resident") %>' NavigateUrl='<%# "~/Renters/RenterDetails.aspx?renterId=" + Eval("RPORenterID") %>' ForeColor="#badc02"></asp:HyperLink>
                                                    <br />
                                                    <asp:Label ID="Label3" runat="server" Text='<%# "Resident ID: " + Eval("RenterID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="PayerName" HeaderText="Payer" />
                                            <asp:TemplateField HeaderText="Method">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("PaymentMethod") %>'></asp:Label>
                                                    <br />
                                                    <asp:Label ID="Label2" runat="server" Font-Size="12px" Text='<%# Bind("AccountNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Total" DataFormatString="{0:c}" HeaderText="Total Paid" />
                                        </Columns>
                                        <HeaderStyle CssClass="SubReportTable" />
                                        <RowStyle CssClass="SubReportTableData" />
                                    </asp:GridView>

                                </div>

                            </asp:Panel>
                            <div style="padding: 5px 5px 10px 5px; text-align: center;">
                                <asp:Button ID="CloseModalbtn1" runat="server" Text="Close" CssClass="btn btn-primary" />
                            </div>

                        </asp:Panel>

                        <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="PaymentDetailsPanel" BackgroundCssClass="ReportmodalPop" CancelControlID="CloseModalbtn1" TargetControlID="ModalShow"></asp:ModalPopupExtender>


                    </div>



                </div>
            </ContentTemplate>
        </asp:UpdatePanel>


    </div>

</asp:Content>
