﻿<%@ Page Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="DetailedDepositAnalysisReport.aspx.cs" Inherits="RentPaidOnline.ReportingPages.DetailedDepositAnalysisReport" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/ReportingPages/UserControls/PropertyPicker.ascx" TagPrefix="uc1" TagName="PropertyPicker" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content28" ContentPlaceHolderID="MainContent" runat="server">
        <!--cakel: BUGID00278 Added New Page and Controls -->
    <div>
        <h1>Reports</h1>

    </div>


    <div class="main-container">

               <div class="report-header selected" style="width: 40%;">
            <a class="report-nav" href="/ReportingPages/ReportSelection.aspx">
                <span class="back" aria-hidden="true" data-icon="2"></span>
                <span class="default-nav">Detailed  Deposits Analysis</span></a>
        </div>
        <div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="well no-padding">
                    <table>
                        <tr>
                            <td>
                                <uc1:PropertyPicker runat="server" ID="PropertyPicker" />

                            </td>
                            <td style="width: 106px">
                                <div class="divider-fluid">
                                    <div class="padding">
                                        <asp:Button ID="Button1" runat="server" Text="View Report" CssClass="btn btn-primary" OnClick="Button1_Click" />
                                    </div>
                                </div>

                            </td>

                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

   

   



       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                             <div style="padding-top: 2px; padding-bottom: 10px">
                    <div class="export-select">
                        <asp:DropDownList ID="ExportTypeDDL" runat="server" CssClass="form-control" Width="150px">
                            <asp:ListItem Value="application/excel">Excel</asp:ListItem>
                            <asp:ListItem Value="application/pdf">PDF</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="Button2" runat="server" Text="Export" CssClass="btn btn-small" data-icon="H" OnClick="Button2_Click" />
                        <asp:Label ID="ExportlblWait" runat="server" Text=""></asp:Label>
                    </div>
                </div>




                <div>
                    <br />
                    <asp:HiddenField ID="HiddenPropertyList" runat="server" />

                    <div style="text-align: center; margin-top: 10px;">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="200">
                            <ProgressTemplate>
                                <img src="../Images/report_wait.gif" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>

                        <asp:Label ID="Gridresultlbl" runat="server"></asp:Label>
                    </div>


                </div>

                <div>
                   
                </div>
                <asp:GridView ID="GridView1" runat="server" DataSourceID="DepositAnalysisSummarySQL" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" PageSize="100" CellPadding="4" Width="98%" GridLines="Horizontal" DataKeyNames="PID" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                    <Columns>
                        <asp:BoundField DataField="PID" HeaderText="Property ID" ReadOnly="True" SortExpression="PID"/>
                        <asp:BoundField DataField="Source" HeaderText="Source" ReadOnly="True" SortExpression="Source" />
                        <asp:BoundField DataField="RollupDate" HeaderText="Rollup Date" ReadOnly="True" SortExpression="RollupDate" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="Type" HeaderText="Type" ReadOnly="True" SortExpression="Type" />
                        <asp:BoundField DataField="Payment" HeaderText="Payment" ReadOnly="True" SortExpression="Payment" DataFormatString="{0:c}" />
                        <asp:BoundField DataField="Transaction" HeaderText="Deposit Total" ReadOnly="True" SortExpression="Total" DataFormatString="{0:c}" />
                        <asp:BoundField DataField="Credit2ACH" HeaderText="Credit 2 ACH" ReadOnly="True" SortExpression="Credit2ACH" DataFormatString="{0:c}" />
                        <asp:BoundField DataField="DebitbyACH" HeaderText="Debit by ACH" ReadOnly="True" SortExpression="DebitbyACH" DataFormatString="{0:c}" />
                        <asp:BoundField DataField="TransactionDate" HeaderText="Transaction Date" ReadOnly="True" SortExpression="TransactionDate" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="EffectivePaymentDate" HeaderText="Effective Payment Date" ReadOnly="True" SortExpression="EffectivePaymentDate" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="ExternalTransactionId" HeaderText="External Transaction ID" ReadOnly="True" SortExpression="ExternalTransactionId" />
                        <asp:BoundField DataField="InternalTransactionId" HeaderText="Internal Transaction ID" ReadOnly="True" SortExpression="InternalTransactionId" />
                        <asp:BoundField DataField="ClearingJobId" HeaderText="Clearing Job ID" ReadOnly="True" SortExpression="ClearingJobId" />
                        <asp:BoundField DataField="originTypeId" HeaderText="Origin Type ID" ReadOnly="True" SortExpression="originTypeId" />
                    </Columns>
                    <HeaderStyle CssClass="ReportTableHeader" />
                    <PagerSettings PageButtonCount="10" FirstPageText="First" LastPageText="Last" NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" Mode="NumericFirstLast" />
                    <PagerStyle Font-Size="16px" HorizontalAlign="Right" />
                    <RowStyle CssClass="ReportTableData" />
                    <SelectedRowStyle BackColor="#E6E6E6" />
                </asp:GridView>

                <asp:SqlDataSource ID="DepositAnalysisSummarySQL" runat="server" ConnectionString="<%$ ConnectionStrings:ReportsConnection %>" SelectCommand="usp_Report_RPO_DepositsSettlementDailyAnalysis" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenPropertyList" Name="PropertyIDList" PropertyName="Value" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
 

                <div>

                    <asp:Button ID="ModalShow" runat="server" Text="Button" Style="display: none;" />

                    <asp:Panel ID="PaymentDetailsPanel" runat="server" Width="85%" CssClass="subPanelBg">

                        <%--<asp:Panel ID="Panel2" runat="server" Width="99%" ScrollBars="Vertical" CssClass="SubReportModal">
                            <div style="padding: 10px;">


                                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="5" GridLines="Horizontal" Width="98%" OnRowDataBound="GridView2_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="PaymentStatusDescription" HeaderText="Description" />
                                        <asp:BoundField DataField="IsIntegrated" HeaderText="IsIntegrated" Visible="False" />
                                        <asp:BoundField DataField="BatchID" HeaderText="Batch ID" />
                                        <asp:BoundField DataField="PaymentDate" DataFormatString="{0:d}" HeaderText="Payment Date" />
                                        <asp:BoundField DataField="DepositDate" DataFormatString="{0:d}" HeaderText="Deposit Date" />
                                        <asp:BoundField DataField="Portal" HeaderText="Portal" />
                                        <asp:TemplateField HeaderText="Resident">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Resident") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "~/Renters/RenterDetails.aspx?renterId=" + Eval("RPORenterID") %>' Text='<%# Eval("Resident") %>'></asp:HyperLink>
                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Resident") %>'></asp:Label>
                                                <br />
                                                <asp:Label ID="Label4" runat="server" Text='<%# "Resident ID: " + Eval("RenterID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="InternalTransactionID" HeaderText="Payment ID" />
                                        <asp:BoundField DataField="PaymentMethod" HeaderText="Method" />
                                        <asp:BoundField DataField="Total" HeaderText="Amount Paid" DataFormatString="{0:c}" />
                                    </Columns>
                                    <HeaderStyle CssClass="SubReportTable" />
                                    <RowStyle CssClass="SubReportTableData" />
                                </asp:GridView>

                            </div>

                        </asp:Panel>--%>
                        <div style="padding: 5px 5px 10px 5px; text-align: center;">
                            <asp:Button ID="CloseModalbtn1" runat="server" Text="Close" CssClass="btn btn-primary" />
                        </div>

                    </asp:Panel>

                    <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="PaymentDetailsPanel" BackgroundCssClass="ReportmodalPop" CancelControlID="CloseModalbtn1" TargetControlID="ModalShow"></asp:ModalPopupExtender>


                </div>
                
            </ContentTemplate>
           <Triggers>
               <asp:PostBackTrigger ControlID="Button2" />

           </Triggers>
               </asp:UpdatePanel>





</div>

</asp:Content>