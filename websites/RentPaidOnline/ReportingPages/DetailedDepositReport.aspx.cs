﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using EfxFramework;
using EfxFramework.Helpers;

namespace Domus.ReportingPages
{
    public partial class DetailedDepositReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public string GetPropertyString()
        {
            CheckBoxList ckbx = (CheckBoxList)this.PropertyPicker.FindControl("CheckBoxList1");
            List<String> PropertyID_list = new List<string>();
            List<String> PropertyName_list = new List<string>();

            foreach (ListItem item in ckbx.Items)
            {
                if (item.Selected)
                {
                    PropertyID_list.Add(item.Value);
                    PropertyName_list.Add(item.Text);
                }
            }
            string PropList = String.Join(",", PropertyID_list.ToArray());
            return PropList;
        }

        //view report
        protected void Button1_Click(object sender, EventArgs e)
        {
            TextBox StartDate = (TextBox)this.PropertyPicker.FindControl("StartDatetxt");
            TextBox EndDate = (TextBox)this.PropertyPicker.FindControl("EndDatetxt");
            DepositSummarySQL.DataBind();
            HiddenStart.Value = StartDate.Text;
            HiddenEnd.Value = EndDate.Text;
            HiddenPropertyList.Value = GetPropertyString();

            UpdatePanel1.DataBind();
            if (GridView1.Rows.Count < 1)
            {
                Gridresultlbl.Text = "Your selection produced no results";
            }
            else
            {
                Gridresultlbl.Text = "";
            }

        }

        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink ResidentLink = (HyperLink)e.Row.FindControl("HyperLink1");
                Label ResidentLabel = (Label)e.Row.FindControl("Label3");

                if (e.Row.Cells[0].Text == "Returned")
                {
                    e.Row.Cells[9].ForeColor = System.Drawing.Color.Red;
                }
                //cakel: BUGID00320 - Added code to display link only for residents and not applicants
                if (e.Row.Cells[5].Text == "Applicant")
                {
                    ResidentLink.Visible = false;
                    ResidentLabel.Visible = true;
                }
                else
                {
                    ResidentLink.Visible = true;
                    ResidentLabel.Visible = false;
                } 

            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label Paystatdesc = (Label)e.Row.FindControl("PayStatusDesclbl");
                

                //if (Paystatdesc.Text == "Returned")
                //{
                //    e.Row.Cells[7].ForeColor = System.Drawing.Color.Red;
                //    Paystatdesc.Visible = true;
                //}
                //else
                //{
                //    Paystatdesc.Visible = false;
                //}

            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GridView1.SelectedIndex > -1)
            {
                DateTime DateString = (DateTime)GridView1.DataKeys[GridView1.SelectedIndex].Values["DepositDate"];
                int mystring = (int)GridView1.DataKeys[GridView1.SelectedIndex].Values["PropertyID"];
                string TransType = GridView1.DataKeys[GridView1.SelectedIndex].Values["TransactionType"].ToString();
                string PaymentStatDesc = GridView1.DataKeys[GridView1.SelectedIndex].Values["PaymentStatusDescription"].ToString();

                GridView2.DataSource = GetSubReportDetail(DateString, mystring, TransType, PaymentStatDesc);
                GridView2.DataBind();

                ModalPopupExtender1.Show();
            }

        }

        public SqlDataReader GetSubReportDetail(DateTime depositDate, int PropertyID, string TransType, string PaymentDesc)
        {
            SqlConnection con = new SqlConnection(ConnString);
            //SWitherspoon: Changed to new procedure using proper DespositDates
            SqlCommand com = new SqlCommand("usp_Report_RPO_DepositDetail_v3", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@DepositDate", SqlDbType.Date).Value = depositDate;
            com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = PropertyID;
            com.Parameters.Add("@TransactionType", SqlDbType.NVarChar, 50).Value = TransType;
            com.Parameters.Add("@PaymentStatusDescription", SqlDbType.NVarChar, 50).Value = PaymentDesc;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                con.Close();
                throw e;
            }

        }

        private void GenerateReport()
        {
            string ExportTypeValue = ExportTypeDDL.SelectedValue.ToString();
            string ExportTypeText = ExportTypeDDL.SelectedItem.Text;

            // Setup DataSet
            DataSet ds = GetReportDataSet();

            // Create Report DataSource
            ReportDataSource rds = new ReportDataSource("DetailDepositData", ds.Tables[0]);

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = ExportTypeValue;
            string encoding = "UTF-8";
            string extension = string.Empty;

            // Setup the report viewer object and get the array of bytes
            ReportViewer viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.ReportPath = "ReportingPages/SSRSreports/DetailDepositReport.rdlc";
            viewer.LocalReport.DataSources.Add(rds); // Add datasource here

            byte[] bytes = viewer.LocalReport.Render(ExportTypeText, null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            string fileName = "Detail Deposit Report";

            // Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName + "." + extension);
            Response.BinaryWrite(bytes); // create the file
            Response.End();
            Response.Flush(); // send it to the client to download
            Response.Close();

        }

        private DataSet GetReportDataSet()
        {
            string ReportStartDate = HiddenStart.Value.ToString();
            string ReportEndDate = HiddenEnd.Value.ToString();
            string propertyID = HiddenPropertyList.Value.ToString();

            SqlConnection con = new SqlConnection(ConnString);
            {
                //Switherspoon Task 00662: Changed SqlCommand to use newly added Stored Procedure
                SqlCommand com = new SqlCommand("usp_Report_RPO_DepositDetail_Export_v4", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("@StartDate", SqlDbType.Date).Value = ReportStartDate;
                com.Parameters.Add("@EndDate", SqlDbType.Date).Value = ReportEndDate;
                com.Parameters.Add("@PropertyList", SqlDbType.NVarChar, -1).Value = propertyID;
                com.CommandTimeout = 360;

                con.Open();
                SqlDataAdapter SqlAdapt = new SqlDataAdapter(com);
                //SqlAdapt.SelectCommand = com;
                DataSet DS = new DataSet();
                SqlAdapt.Fill(DS, "DetailDepositData");
                con.Close();
                return DS;
            }

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (GridView1.Rows.Count > 0)
            {
                GenerateReport();
            }
        }

        private static String ConnString
        {
            get
            {
                return EfxFramework.EfxSettings.ReportConnectionString;
            }
        }


    }
}