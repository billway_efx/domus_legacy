﻿using System;
using System.Web.UI;
using EfxFramework;
using EfxFramework.Interfaces.Account;
using EfxFramework.Web;

namespace RentPaidOnline.Account
{
	public partial class Login : BasePage, ILogin
	{
        public string UsernameText => UserNameTextbox.Text;
        public string PasswordText => PasswordTextbox.Text;
        public EventHandler LoginButtonClick { set { LoginButton.Click += value; } }
        public bool IsFirstVisit => Request.QueryString["f"] == null;
        public Page ParentPage => this;

        //Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            UserNameTextbox.Focus();
            var presenter = new EfxFramework.Presenters.Account.Login<EfxAdministrator>(this);
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {

        }
    }
}