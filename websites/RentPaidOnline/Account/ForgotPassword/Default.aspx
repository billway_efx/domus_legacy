﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Domus.Account.ForgotPassword.Default" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="HeadContent">
<style type="text/css">
.top-bar,.middle-bar .content-container,.side-nav,.main-actions,footer{display:none;}
html,body,form,.main{height:100%;}
.main{position:relative;z-index:1;}.main:after{content:"";display:block;position:absolute;top:0;right:0;bottom:0;left:0;background:#EBE9CF;opacity:0.3;z-index:2;}
.logo{float:none;position:relative;width:100%;display:block;text-align:center;padding:50px 3% 12px;z-index:3;}
.content{display:block;position:relative;width:100%;padding:0;z-index:3;}
.content .content-container{display: block;width:100%;padding:0;}
.modal-content{margin:10px auto 0;width:70%;max-width:640px;}
.modal-content p{margin-top:3px;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="modal-content">
		<div class="modal-body">
			<div class="formWrapper">
				<h1>Forgot Password</h1>
				<asp:ValidationSummary ID="ApplicationSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" ValidationGroup="Password" />
				<asp:PlaceHolder ID="successMsg" runat="server" Visible="false"><div class="alert alert-success" id="successDiv"><b>Success!</b> An email has been sent with your new temporary password.</div></asp:PlaceHolder>
				<div class="formWhole">
					<p>Enter your email address below.</p>
					<label>Email Address:</label>
					<asp:TextBox ID="EmailAddressTextbox" runat="server" CssClass="form-control"></asp:TextBox>
				</div>
				<div class="formWhole">
					<asp:Button ID="SubmitButton" runat="server" CssClass="btn btn-primary btn-large" Text="Reset Password" />
					<a href="../Login.aspx" class="btn btn-default">Cancel</a>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
