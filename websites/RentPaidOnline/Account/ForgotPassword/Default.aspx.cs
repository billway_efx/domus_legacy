﻿using EfxFramework;
using EfxFramework.Interfaces.Account;
using EfxFramework.Web;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Domus.Account.ForgotPassword
{
    public partial class Default : BasePage, IForgotPassword
    {
        public string EmailAddressText { get { return EmailAddressTextbox.Text; } }
        public EventHandler SubmitButtonClick { set { SubmitButton.Click += value; } }
		public Page ParentPage { get { return this; } }
		public PlaceHolder SuccessMessagePlaceHolder { get { return successMsg; } }

        public Default()
            : base(true)
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var Presenter = new EfxFramework.Presenters.Account.ForgotPassword<EfxAdministrator>(this);
        }
    }
}