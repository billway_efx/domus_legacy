﻿using System;
using System.Collections.Generic;
using EfxFramework;

namespace RentPaidOnline
{
    public partial class Frontend : System.Web.UI.MasterPage
	{
        protected bool IsRpoAdmin
        {
            get
            {
                var roleListAdmin = new List<RoleName>
                {
                    RoleName.EfxPowerAdministrator
                };
                return RoleManager.IsUserInRole(RoleType.EfxAdministrator, RPO.Helpers.Helper.CurrentUserId, roleListAdmin);
            }
        }

        protected bool IsCsrUser
        {
            get
            {
                var roleListCustomerService = new List<RoleName>
                {
                    RoleName.EfxCustomerService
                };
                return RoleManager.IsUserInRole(RoleType.CustomerServiceStaff, RPO.Helpers.Helper.CurrentUserId, roleListCustomerService);
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
		{
			SetCurrentPage();
            if(IsCsrUser)
            {
                SupportMap.Visible = false;
            }
		}

		private void SetCurrentPage()
		{
			if(Request.Url.AbsolutePath.Equals("/default.aspx", StringComparison.InvariantCultureIgnoreCase))
				DashboardLink.Attributes["class"] = "current";
			if (Request.Url.ToString().ToLower().Contains("myaccount/"))
				MyAccountLink.Attributes["class"] = "current last";
		}
	}
}