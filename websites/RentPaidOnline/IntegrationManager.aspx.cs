﻿using EfxFramework;
using System;
using System.Linq;
using RPO;
using Infragistics.Web.UI;
using Infragistics.Web.UI.GridControls;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web;
using System.IO;

namespace Domus
{
    public partial class IntegrationManager : System.Web.UI.Page
    {
        int SelectedCompany = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string _user = Page.User.Identity.Name.ToString();
            
            if (!Page.IsPostBack)
            {
                
                ActivityLog AL = new ActivityLog();
                AL.WriteLog(_user, "User accessed Integration Manager Page", (short)LogPriority.LogAlways);

                if (!EfxFramework.Helpers.Helper.HasIntegrationManagerAccess)
                {
                    //You shouldn't be here. Please leave ...
                    Response.Redirect("/Default.aspx");
                }

                //Only show the company list to EFX Administrators ...
                if (EfxFramework.Helpers.Helper.IsRpoAdmin)
                {
                    //Retrieve Company List, filter out inactive Companies, sort the list
                    List<EfxFramework.Company> data = null;
                    data = EfxFramework.Company.GetAllMRICompanyList();
                    data = data.Where(d => !d.IsDeleted).ToList();
                    data = data.OrderBy(p => p.CompanyName).ToList();

                    //Clear out the control, add/select default values
                    DropDownList1.Items.Clear();
                    ListItem TheItem = new ListItem("-- Select MRI Integrated Company --");
                    TheItem.Value = "0";
                    DropDownList1.Items.Add(TheItem);

                    TheItem = new ListItem("All MRI Integrated Companies");
                    //Special value -99 used by usp_Get_MRIReceiptFiles stored procedure to get receipt files for all companies
                    TheItem.Value = "-99"; 
                    DropDownList1.Items.Add(TheItem);

                    DropDownList1.SelectedIndex = 0;
                    SelectedCompany = 0;

                    //Populate the control from the list of companies ...
                    foreach (EfxFramework.Company item in data)
                    {
                        TheItem = new ListItem(item.CompanyName);
                        TheItem.Value = item.CompanyId.ToString();
                        DropDownList1.Items.Add(TheItem);
                    }

                    // Since this Administrator user can see multiple (all) companies, disabled upload until they select a single Company to work with
                    BrowseFileControl.Enabled = false;
                    PopulateReceiptFileGrid();
                }
                else
                {
                    //Only EFX Administrators can see the company control. Hide it for everyone else ...
                    CompanyDiv.Style.Add("display", "none");
                }

                //Default the date range textboxes to one month prior ...
                DateTextboxStart.Text = DateTime.Now.AddMonths(-1).Month.ToString("D2") + "/" +
                    DateTime.Now.AddMonths(-1).Day.ToString("D2") + "/" +
                    DateTime.Now.AddMonths(-1).Year.ToString();
                DateTextboxEnd.Text = DateTime.Now.Month.ToString("D2") + "/" +
                    DateTime.Now.Day.ToString("D2") + "/" + 
                    DateTime.Now.Year.ToString();
            }

            //Register the handler for buttons in the Infragistics web grid Action column
            WebDataGrid1.ItemCommand +=
                new Infragistics.Web.UI.GridControls.ItemCommandEventHandler(WebDataGrid1_ItemCommand);

            if (!EfxFramework.Helpers.Helper.IsRpoAdmin)
            {
                //We're not an EFX Administrator, so set the CompanyId for this user
                SelectedCompany = EfxFramework.Company.GetCompanyIdForStaffId(Convert.ToInt32(_user));

                //And populate the list of receipt files for this Company ...
                PopulateReceiptFileGrid();
            }

            if (BrowseFileControl.PostedFiles.Count > 0)
            {
                if (BrowseFileControl.PostedFiles[0].ContentLength > 0)
                    UploadNewImportFile();
            }
        }

        void PopulateReceiptFileGrid()
        {
            if (SelectedCompany != 0)
            {
                CompanyIdFilter.Text = SelectedCompany.ToString();
                WebDataGrid1.DataBind();

                //Enable file upload if a single company has been selected
                if (SelectedCompany != -99)
                {
                    BrowseFileControl.Enabled = true;
                }
            }
        }

        void UploadNewImportFile()
        {
            string _user = Page.User.Identity.Name.ToString();
            ActivityLog AL = new ActivityLog();
            AL.WriteLog(_user, "User uploading file.", (short)LogPriority.LogAlways);

            int FileLength = BrowseFileControl.PostedFiles[0].ContentLength;
            byte[] FileData = new byte[FileLength - 1];
            FileData = BrowseFileControl.FileBytes;

            string FileName = Path.GetFileName(BrowseFileControl.PostedFiles[0].FileName);
            DateTime CurrentTime = DateTime.Now;
            int UserId = Convert.ToInt32(Page.User.Identity.Name.ToString());
            int CompanyId = Convert.ToInt32(DropDownList1.SelectedItem.Value);

            int NewId = UploadMRIImportFile(FileName, CurrentTime, UserId, CompanyId, FileData, FileLength);

            AL.WriteLog(_user, "File upload complete for '" + FileName + "' FileId " + NewId.ToString(), (short)LogPriority.LogAlways);

            lblResult.Text = FileName + " was successfully uploaded.";
            lblResult.Visible = true;
        }

        protected void WebDataGrid1_ItemCommand(object sender, Infragistics.Web.UI.GridControls.HandleCommandEventArgs e)
        {
            if (e.CommandName == "Download")
            {
                GridRecordItem TheItem = (GridRecordItem)((TemplateContainer)((Button)sender).Parent).Item;
                String TheDataKey = TheItem.Row.DataKey[0].ToString();

                string _user = Page.User.Identity.Name.ToString();
                ActivityLog AL = new ActivityLog();
                AL.WriteLog(_user, "User requested download receipt file ID " + TheDataKey, (short)LogPriority.LogAlways);

                Int64 FileId = Convert.ToInt64(TheDataKey);
                string FileName = "";
                byte[] ByteArray = DownloadMRIReceiptFile(FileId, ref FileName);

                int i = 0;
                for (i = 0; i <= 50; i++)
                {
                    if (ByteArray[i] < 250)
                        break; 
                }
                byte[] ByteArrayCopy = new byte[ByteArray.Length - i];

                Array.ConstrainedCopy(ByteArray, i, ByteArrayCopy, 0, ByteArray.Length - i);

                System.Text.Encoding Encoding = System.Text.Encoding.UTF8;
                string TheFile = Encoding.GetString(ByteArrayCopy);

                //To Do: Update the database with status that this user has downloaded the file

                AL.WriteLog(_user, "Download receipt file complete. Flushing response object to user. File ID " + TheDataKey, (short)LogPriority.LogAlways);

                Response.ClearContent();
                Response.ContentType = "application/vnd.ms-excel";
                Response.AppendHeader("content-disposition", "attachment; filename=" + FileName);
                Response.ContentEncoding = System.Text.Encoding.UTF8;
                Response.Write(TheFile);
                Response.End();

            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedCompany = Convert.ToInt32(DropDownList1.SelectedItem.Value);
            PopulateReceiptFileGrid();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            PopulateReceiptFileGrid();
        }

        private byte[] DownloadMRIReceiptFile(Int64 kFile, ref string FileName)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlParameter FileID = null;
            SqlParameter fileName = null;
            SqlDataReader dr = null;
            byte[] result = null;

            try
            {
                conn = new SqlConnection(EfxSettings.ConnectionString);
                cmd = new SqlCommand("DownloadMRIReceiptFile", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                FileID = new SqlParameter("@FileID", System.Data.SqlDbType.BigInt);
                FileID.Value = kFile;
                fileName = new SqlParameter("@FileName", SqlDbType.VarChar, 250);
                fileName.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(FileID);
                cmd.Parameters.Add(fileName);

                conn.Open();
                dr = cmd.ExecuteReader();
                dr.Read();

                // We are casting the value returned by the datareader to the byte[] data type.
                result = (byte[])dr.GetValue(0);

                FileName = (string)dr.GetValue(1);

                dr.Close();
                conn.Close();

                conn.Dispose();
                cmd.Dispose();

            }
            //catch (Exception e)
            catch
            {
                //Console.WriteLine(e.Message + " - " + e.StackTrace);
                result = null;
            }
            return result;
        }

        private int UploadMRIImportFile(string FileName, DateTime DateUploaded, int UploadedByUserId, int CompanyId, byte[] FileData, long FileSize)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlParameter ParamFileName = null;
            SqlParameter ParamDateUploaded = null;
            SqlParameter ParamUploadedByUserId = null;
            SqlParameter ParamCompanyId = null;
            SqlParameter ParamFileData = null;
            SqlParameter ParamFileSize = null;
            SqlParameter ParamFileID = null;

            int NewId = 0;

            // Note: When we insert the new MRI file into the MRIImportFiles table, the database fires the insert trigger 'MRIImportFiles_Insert', 
            // which runs the job 'Run_MRI_Import_File_Parser', which in turn parses the file and sends out notification email as necessary.
            // So, there's no need to do anything else here, as far as processing is concerned, except to get the file into the database, and
            // of course mark it as "archived" so we don't process it again, the next time this program runs.

            try
            {
                conn = new SqlConnection(EfxSettings.ConnectionString);
                cmd = new SqlCommand("UploadMRIImportFile", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                ParamFileName = new SqlParameter("@FileName", SqlDbType.VarChar, 250);
                ParamFileName.Value = FileName;

                ParamDateUploaded = new SqlParameter("@DateUploaded", SqlDbType.DateTime);
                ParamDateUploaded.Value = DateUploaded;

                ParamUploadedByUserId = new SqlParameter("@UploadedByUserId", SqlDbType.VarChar, 250);
                ParamUploadedByUserId.Value = UploadedByUserId;

                ParamCompanyId = new SqlParameter("@CompanyId", SqlDbType.VarChar, 250);
                ParamCompanyId.Value = CompanyId;
                
                ParamFileData = new SqlParameter("@FileData", SqlDbType.Image);
                ParamFileData.Value = FileData;

                ParamFileSize = new SqlParameter("@FileSize", SqlDbType.BigInt);
                ParamFileSize.Value = FileSize;

                ParamFileID = new SqlParameter("@FileID", SqlDbType.BigInt);
                ParamFileID.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(ParamFileName);
                cmd.Parameters.Add(ParamDateUploaded);
                cmd.Parameters.Add(ParamUploadedByUserId);
                cmd.Parameters.Add(ParamCompanyId);
                cmd.Parameters.Add(ParamFileData);
                cmd.Parameters.Add(ParamFileSize);
                cmd.Parameters.Add(ParamFileID);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                NewId = Convert.ToInt32(ParamFileID.Value);

                conn.Dispose();
                cmd.Dispose();

            }
            //catch (Exception e)
            catch
            {
                NewId = 0;
            }

            return NewId;
        }
       
    }

    //public sealed class Alert
    //{
    //    private Alert()
    //    {
    //    }

    //    /// <summary> 
    //    /// Shows a client-side JavaScript alert in the browser. 
    //    /// </summary> 
    //    /// <param name="message">The message to appear in the alert.</param> 
    //    public static void Show(string message)
    //    {
    //        // Cleans the message to allow single quotation marks 
    //        string cleanMessage = message.Replace("'", "\\'");

    //        string script = "<script type=\"text/javascript\">alert('" + cleanMessage + "');</script>";
    //        String scriptMessage = "<script language=\"'javascript'\">function window.onload(){alert('" + cleanMessage + "');}</script>";

    //        // Gets the executing web page 
    //        Page page = HttpContext.Current.CurrentHandler as Page;

    //        // Checks if the handler is a Page and that the script isn't allready on the Page 
    //        if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
    //        {
    //            page.ClientScript.RegisterClientScriptBlock(typeof(Alert), "alert", scriptMessage);
    //        }
    //    }

    //    public static void Show(string message, string RedirectUrl)
    //    {
    //        // Cleans the message to allow single quotation marks 
    //        string cleanMessage = message.Replace("'", "\\'");
    //        string script = "<script type=\"text/javascript\">alert('" + cleanMessage + "');window.location.href = '" + RedirectUrl + "'</script>";

    //        // Gets the executing web page 
    //        Page page = HttpContext.Current.CurrentHandler as Page;

    //        // Checks if the handler is a Page and that the script isn't allready on the Page 
    //        if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
    //        {
    //            page.ClientScript.RegisterClientScriptBlock(typeof(Alert), "alert", script);
    //        }
    //    }

    //}

}