﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="PaymentFailed.aspx.cs" Inherits="Domus.PaymentFailed" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopHeadline" runat="server">
    <h1>Payment Status</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div class="main-container">
        <div class="section-body">


            <div class="well">
                <div class="highlighted message red">Your Transaction has NOT been processed.</div>

                <br />
                <asp:Label ID="lblCharityFailureMessage" runat="server" Text="Label" Visible="false"></asp:Label><br />
                <asp:Label ID="lblRentAndFeeFailureMessage" runat="server" Text="Label" Visible ="false"></asp:Label>

                <h2>Please try again</h2>

                <!-- cakel: BUGID000194 - Commented out html below to prevent confusion.  This was a static message that did change for payments or declines
                <p>Rent and Fees Transaction Accepted</p>
                -->

            </div>
            <div class="formWrapper">
                <div class="formHalf">
                    <h3>RPO Support </h3>
                    <div class="lightPanel">
                        Support Line:
                            <span style="font-weight: bold">1-855-PMYRENT</span><br />

                        Email:
                            <span style="font-weight: bold">
                                <a href="mailto:support@Domusme.com">support@Domusme.com</a></span><br />

                    </div>
                </div>
            </div>

            <!-- <section class="paymentStatusInfoBlock">
            <img src="/Images/DomusLogo.png" class="smallLogo" />
        </section> -->

        </div>


    </div>

</asp:Content>
