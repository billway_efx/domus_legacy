﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="staff.aspx.cs" Inherits="Domus.staff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TopHeadline" runat="server">
<h1 id="title" ng-controller="TitleController" ng-bind="PageTitle"></h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
        <div class="main-container" id="StaffTab">
            <div ng-include="'../staff.html'"></div>
        </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Scripts" runat="server">
    <script src="<%= ResolveClientUrl("~/Scripts/rpo-staff.js") %>"></script>
</asp:Content>
