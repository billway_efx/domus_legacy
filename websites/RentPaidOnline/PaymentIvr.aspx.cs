﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;

namespace Domus
{
    public partial class PaymentIvr : BasePageV2, IPaymentIvr
    {
        private PaymentIvrPresenter _Presenter;

        public string PaymentQueueNumber
        {
            get { return Request.QueryString["PaymentQueueNumber"]; }
            set { PaymentQueueNumberLabel.Text = value; }
        }
        public string RentAmount { set { RentAmountLabel.Text = value; } }
        public string FeeAmount { set { ConvenienceFeeLabel.Text = value; } }
        public string TotalPayment { set { TotalPaymentAmountLabel.Text = value; } }
        public string PropertyName { set { PropertyNameLabel.Text = value; } }
        public string PropertyAddress { set { PropertyAddressLabel.Text = value; } }
        public string PropertyCityStateZip { set { PropertyCityStateZipLabel.Text = value; } }
        public string FirstName { set { FirstNameLabel.Text = value; } }
        public string MiddleName { set { MiddleNameLabel.Text = value; } }
        public string LastName { set { LastNameLabel.Text = value; } }
        public string Suffix { set { SuffixLabel.Text = value; } }
        public string BillingAddress { set { BillingAddressLabel.Text = value; } }
        public string BillingCityStateZip { set { BillingCityStateZipLabel.Text = value; } }
        public string Phone { set { PhoneNumberLabel.Text = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            PaymentQueueNumberLabel.Text = PaymentQueueNumber; 
            
            _Presenter = new PaymentIvrPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}