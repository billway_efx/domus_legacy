﻿using System;
using System.IO;
using System.Net;
using System.Xml.Linq;

namespace RentPaidOnline.MRI_Test
{
    public partial class MriRestTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }

        private string TryVersionX()
        {
            const string apiString = "https://Mri45pc.saas.mrisoftware.com/mriapiservices/api.asp?$api=MRI_S-PMRM_ResidentLeaseDetailsByPropertyID&RMPROPID=109-0140&$format=xml";

            const string strDatabase = "RENTPAIDONL";
            const string apiUserName = "L537999/" + strDatabase + "/HUBBELLAPI/160B4FDD99256A0860D6A7248E5F89735A5295F7423776DA5A13B436A247B2A1";
            const string apiPassword = "ypP3CJkG";

            // Create the web request  
            var request = WebRequest.Create(apiString) as HttpWebRequest;

            // Add authentication to request  
            if (request != null)
            {
                request.Credentials = new NetworkCredential(apiUserName, apiPassword);

                // Get response  
                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    // Get the response stream  
                    var reader = new StreamReader(response.GetResponseStream());
                    var respString = reader.ReadToEnd();
                    // Console application output  
                    return respString;
                }
            }
            return null;
        }

        //Find Root Element 
        public static XElement GetRootElement(XElement element, string localName)
        {
            XElement xElement= null;

            if (element != null && element.Name.LocalName == localName)
                return element;

            if (element == null || !element.HasElements) return null;
            foreach (var E in element.Elements())
            {
                xElement = GetRootElement(E, localName);
            }

            return xElement;
        }

        protected void verXbtn_Click(object sender, EventArgs e)
        {
            Label1.Text = TryVersionX();
        }
    }
}