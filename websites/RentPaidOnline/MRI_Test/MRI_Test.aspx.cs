﻿using System;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using EfxFramework.Pms.MRI;

namespace Domus.MRI_Test
{
    public partial class MRI_Test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Redirect("~/Default.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string residentID = "0000000021";

            string MriOpenCharges = "MRI_S-PMRM_OpenCharges";
            string MriResidentRentRoll = "mri_s-pmrm_residentialrentroll";
            string APi_call = "MRI_S-PMRM_ResidentLeaseDetailsByPropertyID";

            string PropertyID = "501";

            //string WebResponse = MriRequest.MriWebServiceCall(MriOpenCharges, PropertyID, residentID);



            //MriRequest.MRI_GetLeaseFeeDetails(PropertyID);

            //MriRequest.GetRentRollByPropertyID(WebResponse);

            MriRequest.MRI_GetLeaseDetailsByPropID("W8P");


            //MriRequest.MRI_ProcessRentersForAllProperties();




        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            string PropPmsID = "109-014";

            string ReturnedXMl = MriExport.GetXMLMriPaymentExportData(PropPmsID);



            string test = "";

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            string MriPropID = "109-0140";

            //string t =  MriExport.MRIWebServicePost(MriPropID);


            string _xml = ReturnTestXml();

           
            byte[] byteArray = Encoding.ASCII.GetBytes(_xml);
               using (var OutputStream = new MemoryStream(byteArray))
               {
                   var Xml = XDocument.Load(OutputStream);

                   var Element = GetRootElement(Xml.Root, "mri_s-pmrm_paymentdetailsbypropertyid");

                   string _Error = "";
                   string TransactionID = "";
                   string ResidentNameID = "";
                   string PropertyID = "";
                   string SiteID = "";
                   string ChargeCode = "";
                   string PaymentInitiationDatetime = "";
                   string PaymentAmount = "";
                   string PaymentType = "";
                   string ExternalTransactionNumber = ""; //this is the paymentID from RPO
                   string BatchID = "";

                   foreach (XElement _ReturnResp in Element.Elements())
                   {

                       _Error = TryGetElementValue(_ReturnResp,"Error");
                       TransactionID = TryGetElementValue(_ReturnResp, "TransactionID");
                       ResidentNameID = TryGetElementValue(_ReturnResp, "ResidentNameID");
                       PropertyID = TryGetElementValue(_ReturnResp, "PropertyID");
                       SiteID = TryGetElementValue(_ReturnResp, "SiteID");
                       ChargeCode = TryGetElementValue(_ReturnResp, "ChargeCode");
                       PaymentInitiationDatetime = TryGetElementValue(_ReturnResp, "PaymentInitiationDatetime");
                       PaymentAmount = TryGetElementValue(_ReturnResp, "PaymentAmount");

                       BatchID = _ReturnResp.Element("ExternalBatchID").Value.ToString();


                       if(_Error.Length < 1)
                       {
                           //Process the sucessful Export
                           string _Success = "";




                       }
                       else
                       {
                           //Handle the Failed export
                           string Err = _Error;

                       }

                   }//ENd foreach (XElement

                   string stopString = "";

               }//end using 




        }



        //Find Root Element 
        public static XElement GetRootElement(XElement element, string localName)
        {
            XElement Element = null;

            if (element != null && element.Name.LocalName == localName)
                return element;

            if (element != null && element.HasElements)
            {
                foreach (var E in element.Elements())
                {
                    Element = GetRootElement(E, localName);
                }
            }

            return Element;
        }

        //Checks to make sure the element exists and if not return empty string 
        public static string TryGetElementValue(XElement parentEl, string elementName, string defaultValue = "")
        {
            var foundEl = parentEl.Element(elementName);

            if (foundEl != null)
            {
                return foundEl.Value;
            }

            return defaultValue;
        }



        public string ReturnTestXml()
        {
            string _s = @"<?xml version=""1.0"" encoding=""utf-8""?>
                <mri_s-pmrm_paymentdetailsbypropertyid>
                  <entry>
                    <Error>
                      <Message>Property Id (RMLEDG.RMPROPID): 109-014 Not found in table RMPROP.</Message>
                    </Error>
                    <TransactionID />
                    <ResidentNameID>RI00010481</ResidentNameID>
                    <PropertyID>109-014</PropertyID>
                    <SiteID />
                    <ChargeCode>RNT</ChargeCode>
                    <PaymentInitiationDatetime>2015-08-31T00:00:00.0000000</PaymentInitiationDatetime>
                    <PaymentAmount>200.0000</PaymentAmount>
                    <CheckNumber />
                    <PaymentType>K</PaymentType>
                    <PartnerName>Domus</PartnerName>
                    <ExternalTransactionNumber>2701</ExternalTransactionNumber>
                    <ExternalBatchID />
                  </entry>
                  <entry>
                    <TransactionID>S100002258</TransactionID>
                    <ResidentNameID>HO00007282</ResidentNameID>
                    <PropertyID>109-014</PropertyID>
                    <SiteID />
                    <ChargeCode>RNT</ChargeCode>
                    <PaymentInitiationDatetime>2015-08-31T00:00:00.0000000</PaymentInitiationDatetime>
                    <PaymentAmount>755.0000</PaymentAmount>
                    <CheckNumber />
                    <PaymentType>K</PaymentType>
                    <PartnerName>Domus</PartnerName>
                    <ExternalTransactionNumber>2700</ExternalTransactionNumber>
                    <ExternalBatchID>00000TestBatch</ExternalBatchID>
                  </entry>
                </mri_s-pmrm_paymentdetailsbypropertyid>";


            return _s;
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            string MriPropID = "109-0140";

            MriExport.ProcessMriPaymentExport();


        }




    }
}