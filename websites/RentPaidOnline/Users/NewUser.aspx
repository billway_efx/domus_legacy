﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="NewUser.aspx.cs" Inherits="Domus.Users.NewUser" EnableEventValidation="False" %>
<%@ Register Src="~/UserControls/UserEditor.ascx" TagName="UserEditor" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopHeadline" runat="server">
	<h1>Add New User</h1>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:UserEditor ID="UserEditor1" runat="server" />
</asp:Content>