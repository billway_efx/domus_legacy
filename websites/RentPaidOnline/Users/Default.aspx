﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="RentPaidOnline.Users.Default" EnableEventValidation="False" %>
<%@ Import Namespace="EfxFramework.ExtensionMethods" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopHeadline" runat="server">
    <h1>Users</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="main-container">
    <div class="filterbar">
        <div class="filter pull-left">
            <button type="button" class="btn btn-filter dropdown-toggle" data-toggle="dropdown">
                Sort: <span data-icon="&#x61" aria-hidden="true"></span><span><asp:Label runat="server" ID="sortText" Text="A-Z"></asp:Label></span>
            </button>
            <ul class="dropdown-menu">
				<li><asp:LinkButton ID="AscendingLink" runat="server" CausesValidation="false" OnClick="AscendingLink_Click">Sort A-Z</asp:LinkButton></li>
				<li><asp:LinkButton ID="DescendingLink" runat="server" CausesValidation="false" OnClick="DescendingLink_Click">Sort Z-A</asp:LinkButton></li>
            </ul>
        </div>
        <a href="NewUser.aspx" class="btn btn-default pull-right"><span class="ctaIcon" data-icon="&#x2c" aria-hidden="true"></span>New User</a>
        <div class="filter pull-right search">
            <asp:TextBox CssClass="form-control" ID="SearchTextbox" runat="server" Placeholder="Search..."></asp:TextBox>
            <asp:Button ID="SearchButton" runat="server" CssClass="btn btn-default" OnClick="SearchButton_Click" Text="Search" />
        </div>
    </div>
        <div class="tab-pane" id="ApiUsersTab" runat="server" ClientIDMode="Static">
            <uc1:APIUsers runat="server" id="APIUsers" />
        </div>
    </div>
</asp:Content>