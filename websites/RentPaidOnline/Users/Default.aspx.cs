﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;
using EfxFramework;
using EfxFramework.Web;
using EfxFramework.Web.Facade;
using Mb2x.ExtensionMethods;

namespace RentPaidOnline.Users
{
    //CMallory - Task 00495 - Commented out lines of code since the UI equivalent parts were commented.
    //cakel: This page has code commented out related to task 00495.  
    //This is only a temp fix until this control is rewriten. The current control returns more data than needed. 

    public partial class Default : BasePage
    {
        private const string CacheSortKey = "CURRENT_USERHOME_SORT";
        private const string CacheFilterKey = "CURRENT_USERHOME_FILTER";
        private const string CacheSearchKey = "CURRENT_USERHOME_SEARCH";

        private string _search = string.Empty;

        private string CurrentSearch
        {
            get
            {
                if (_search.Equals(string.Empty) && ViewState[CacheSearchKey] != null)
                {
                    _search = ViewState[CacheSearchKey].ToString();
                }

                return _search;
            }
            set
            {
                ViewState[CacheSearchKey] = value;
            }
        }

        private string _sort = string.Empty;

        private string CurrentSort
        {
            get
            {
                if (_sort.Equals(string.Empty) && ViewState[CacheSortKey] != null)
                {
                    _sort = ViewState[CacheSortKey].ToString();
                }

                return _sort;
            }
            set
            {
                ViewState[CacheSortKey] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(EfxFramework.Helpers.Helper.IsRpoAdmin || EfxFramework.Helpers.Helper.IsCorporateAdmin))
				Response.Redirect("/");
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //CMallory - Task 00494 - If editing an API User, none of that binding function in the If statement need to be called.  This allows someone to edit an API user as well as speeds up the loading of the page.
            var apiQueryString = Request.QueryString["ApiUserId"];
            if (apiQueryString == null)
            {
                if (EfxFramework.Helpers.Helper.IsRpoAdmin)
                {
                    BindEfxAdmins();
                    BindRenters();
                }
                BindStaff();
            }
      
            if (EfxFramework.Helpers.Helper.IsCorporateAdmin)
            {
                ApiUsersTab.Visible = false;
            }

			if (APIUsers.SetApiUserTabAsActive) 
			{
				ApiUsersTab.Attributes["class"] = "tab-pane active";
			}
        }

        private void BindEfxAdmins()
        {
            // Get data            
            List<EfxAdministrator> data = null;
            data = string.IsNullOrEmpty(CurrentSearch)
                ? EfxAdministrator.GetAllEfxAdministrators()
                : EfxAdministrator.GetAllEfxAdministrators()
                    .Where(c => c.DisplayName.ToLower().IndexOf(CurrentSearch.ToLower()) >= 0).ToList();

            // SORTING
            if (!string.IsNullOrEmpty(CurrentSort) && CurrentSort.Equals("ASC"))
            {
                data = data.OrderBy(p => p.DisplayName).ToList();
            }
            else if (!string.IsNullOrEmpty(CurrentSort) && CurrentSort.Equals("DESC"))
            {
                data = data.OrderByDescending(p => p.DisplayName).ToList();
            }
        }

        private void BindRenters()
        {
            // Get data            
            List<Renter> renters = null;
            renters = string.IsNullOrEmpty(CurrentSearch)
                ? Renter.GetAllRenterList()
                : Renter.GetAllRenterList().Where(c => c.DisplayName.ToLower().IndexOf(CurrentSearch.ToLower()) >= 0)
                    .ToList();

            // SORTING
            if (!string.IsNullOrEmpty(CurrentSort) && CurrentSort.Equals("ASC"))
            {
                renters = renters.OrderBy(p => p.DisplayName).ToList();
            }
            else if (!string.IsNullOrEmpty(CurrentSort) && CurrentSort.Equals("DESC"))
            {
                renters = renters.OrderByDescending(p => p.DisplayName).ToList();
            }

            var rentersTable = new DataTable();
            rentersTable.Columns.Add("RenterId");
            rentersTable.Columns.Add("Unit");
            rentersTable.Columns.Add("DisplayName");
            rentersTable.Columns.Add("PublicRenterId");
            rentersTable.Columns.Add("MainPhoneNumber");
            rentersTable.Columns.Add("PrimaryEmailAddress");
            rentersTable.Columns.Add("PaysRent");
            rentersTable.Columns.Add("CurrentMonth");
            rentersTable.Columns.Add("IsPaid");

            foreach (var renter in renters)
            {
                var row = rentersTable.NewRow();
                row["RenterId"] = renter.RenterId;
                row["DisplayName"] = renter.DisplayName;
                row["PublicRenterId"] = renter.PublicRenterId;
                row["MainPhoneNumber"] = renter.MainPhoneNumber;
                row["PrimaryEmailAddress"] = renter.PrimaryEmailAddress.ToLower().Contains("unknown.com") ? "Email Unavailable" : renter.PrimaryEmailAddress;
                row["Unit"] = renter.Unit;

                var auto = AutoPayment.GetAutoPaymentByRenterId(renter.RenterId);
                if (auto != null)
                {
                    var paysRent = "N/A";
                    if (auto.PaymentTypeId > 0)
                        paysRent = ((RenterPaymentType)auto.PaymentTypeId).ToString();
                    row["PaysRent"] = paysRent;
                }

                var now = DateTime.Now;

                row["CurrentMonth"] = now.ToString("MMM");

                var paymentsMaid = Payment.GetPaymentListByRenterId(renter.RenterId);
                if (paymentsMaid.Any(pay => pay.TransactionDateTime.Year.Equals(now.Year) && pay.TransactionDateTime.Month.Equals(now)))
                {
                    row["IsPaid"] = "Paid";
                }
                else
                {
                    row["IsPaid"] = "Not Yet Paid";
                }

                rentersTable.Rows.Add(row);
            }
        }

        private void BindStaff()
        {
            List<PropertyStaff> data = null;
            if (EfxFramework.Helpers.Helper.IsCorporateAdmin)
            {
                using (var entity = new RPO.RPOEntities())
                {
                    var propertyStaffs = entity.PropertyStaffs.ToList();
                    var propertyPropertyStaffs = entity.PropertyPropertyStaffs.ToList();
                    var corpAdminPropertyIds = propertyPropertyStaffs.Where(sp => sp.PropertyStaffId == RPO.Helpers.Helper.CurrentUserId).Select(p => p.PropertyId).ToList();
                    var propertyStaffIds = propertyPropertyStaffs.Where(p => corpAdminPropertyIds.Contains(p.PropertyId)).Select(ps => ps.PropertyStaffId).Distinct();
                    propertyStaffs = propertyStaffs.Where(s => propertyStaffIds.Contains(s.PropertyStaffId)).ToList();

                    if (!string.IsNullOrEmpty(CurrentSearch))
                    {
                        propertyStaffs = propertyStaffs.Where(c => c.DisplayName.ToLower().IndexOf(CurrentSearch.ToLower()) >= 0).ToList();
                    }
                    // SORTING
                    if (!string.IsNullOrEmpty(CurrentSort) && CurrentSort.Equals("ASC"))
                    {
                        propertyStaffs.OrderBy(p => p.DisplayName).ToList();
                    }
                    else if (!string.IsNullOrEmpty(CurrentSort) && CurrentSort.Equals("DESC"))
                    {
                        propertyStaffs.OrderByDescending(p => p.DisplayName).ToList();
                    }
                }
            }
            else
            {
                // Get data            
                data = string.IsNullOrEmpty(CurrentSearch)
                    ? PropertyStaff.GetAllPropertyStaffList()
                    : PropertyStaff.GetAllPropertyStaffList()
                        .Where(c => c.DisplayName.ToLower().IndexOf(CurrentSearch.ToLower()) >= 0).ToList();
                // SORTING
                if (!string.IsNullOrEmpty(CurrentSort) && CurrentSort.Equals("ASC"))
                {
                    data.OrderBy(p => p.DisplayName).ToList();
                }
                else if (!string.IsNullOrEmpty(CurrentSort) && CurrentSort.Equals("DESC"))
                {
                    data.OrderByDescending(p => p.DisplayName).ToList();
                }
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            var data = Property.GetAllPropertyList();
            var term = SearchTextbox.Text.Trim().ToLower();

            CurrentSearch = term;
        }

        protected void AscendingLink_Click(object sender, EventArgs e)
		{
			CurrentSort = "ASC";
			sortText.Text = "A-Z";
		}

		protected void DescendingLink_Click(object sender, EventArgs e)
		{
			CurrentSort = "DESC";
			sortText.Text = "Z-A";
		}

		protected void DeleteStaffButton_Click(object sender, EventArgs e)
		{
            if (!(sender is LinkButton sendingControl)) return;

            var propertyStaffId = sendingControl.CommandArgument.ToNullInt32();
            if (!propertyStaffId.HasValue) return;
            try
            {
                PropertyStaff.DeletePropertyStaffByPropertyStaffId(propertyStaffId.Value, UIFacade.GetAuthenticatedUserString(UserType.EfxAdministrator));
            }
            catch (SqlException Ex)
            {
                var err = new CustomValidator();
                if (Ex.Number == 547)
                {
                    err.ValidationGroup = "PSSummaryValGroup";
                    err.IsValid = false;
                    err.ErrorMessage = "You cannot delete property staff that has transactions associated with it.";
                    Page.Validators.Add(err);
                }
                else
                {
                    throw;
                }
            }
        }
    }
}