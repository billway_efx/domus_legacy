﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Frontend.master" AutoEventWireup="true" CodeBehind="EditStaff.aspx.cs" Inherits="Domus.Users.EditStaff" EnableEventValidation="False" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="TopHeadline" runat="server">
    <h1>Edit Staff</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="main-container">
	<div class="formWrapper">
		<div class="formHalf">
    		<asp:DropDownList ID="PropertyDropDown" CssClass="form-control" runat="server" AutoPostBack="True" AppendDataBoundItems="True" DataTextField="PropertyName" DataValueField="PropertyId" OnSelectedIndexChanged="PropertyDropDownSelectedIndexChanged" >
		        <asp:ListItem Value="0" Text="-- Select a Property --" />
		    </asp:DropDownList>
		</div>
	</div>
	<!--end formWrapper-->
    <div class="editStaffContainer">
        <uc1:AddEditPropertyStaff ID="AddEditPropertyStaff" runat="server" ShowStaffSelection="False" Visible="False" />
    </div>
</div>
</asp:Content>
