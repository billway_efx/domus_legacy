﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="UserDetails.aspx.cs" Inherits="Domus.Users.UserDetails" EnableEventValidation="False" %>
<%@ Register Src="~/UserControls/UserEditor.ascx" TagName="UserEditor" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopHeadline" runat="server">
    <h1>User Details</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<div class="hide-account-bar">
    <uc1:UserEditor ID="UserEditor1" runat="server" EditMode="true" />
</div>
</asp:Content>
