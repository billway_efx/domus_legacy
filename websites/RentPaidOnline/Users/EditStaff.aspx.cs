﻿using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Web;

namespace Domus.Users
{
    public partial class EditStaff : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var StaffId = HttpContext.Current.Request.QueryString["propertyStaffId"].ToNullInt32();
            if (!StaffId.HasValue || StaffId.Value < 1)
            {
                SafeRedirect("~/Default.aspx");
                return;
            }

            if (!Page.IsPostBack)
            {
                PropertyDropDown.DataSource = EfxFramework.Property.GetPropertyListByPropertyStaffId(StaffId.Value);
                PropertyDropDown.DataBind();
            }

            SetPropertyStaffDefaults(StaffId.Value);
        }

        private void SetPropertyStaffDefaults(int staffId)
        {
            AddEditPropertyStaff.PropertyStaffId = staffId;
            AddEditPropertyStaff.PropertyId = PropertyDropDown.SelectedValue.ToInt32();
        }

        protected void PropertyDropDownSelectedIndexChanged(object sender, EventArgs e)
        {
            var PropertyId = PropertyDropDown.SelectedValue.ToInt32();

            if (PropertyId > 0)
                AddEditPropertyStaff.Visible = true;
            else
                AddEditPropertyStaff.Visible = false;

            AddEditPropertyStaff.PropertyId = PropertyId;
            AddEditPropertyStaff.PopulateData();
        }
    }
}