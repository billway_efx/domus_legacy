﻿using EfxFramework;
using EfxFramework.Web;

namespace Domus.Handlers
{
    /// <summary>
    /// Summary description for GeneralHandler
    /// </summary>
    public class GeneralHandler : BaseHandler<Company>
    {
        public override void ProcessRequest(System.Web.HttpContext context)
        {
            base.ProcessRequest(context);
        }
    }
}