﻿using EfxFramework;
using Mb2x.ExtensionMethods;
using System.Web;

namespace Domus.Handlers
{
    /// <summary>
    /// Summary description for ApplicationDownloadHandler
    /// </summary>
    public class ApplicationDownloadHandler : IHttpHandler
    {
        public bool IsReusable { get { return false; } }

        public int ApplicantApplicationId { get { return !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["ApplicantApplicationId"]) ? HttpContext.Current.Request.QueryString["ApplicantApplicationId"].ToInt32() : 0; } }

        public void ProcessRequest(HttpContext context)
        {
            var ResidencyApp = ApplicantApplication.GetCompletedApplication(ApplicantApplicationId);

            if (ResidencyApp == null)
                return;

            context.Response.ContentType = "application/pdf";
            context.Response.OutputStream.Write(ResidencyApp, 0, ResidencyApp.Length);
        }
    }
}