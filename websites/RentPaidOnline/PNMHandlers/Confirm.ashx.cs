﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using RPO.PayNearMe;
using EfxFramework;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Domus.PNMHandlers
{
	/// <summary>
	/// Summary description for Confirm
	/// </summary>
	public class Confirm : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			RPO.PayNearMe.ConfirmationCallBackContainer container = new ConfirmationCallBackContainer();
			container.pnm_order_identifier = !string.IsNullOrEmpty(context.Request.QueryString["pnm_order_identifier"]) ? context.Request.QueryString["pnm_order_identifier"].ToString() : "";
			container.pnm_payment_identifier = !string.IsNullOrEmpty(context.Request.QueryString["pnm_payment_identifier"]) ? context.Request.QueryString["pnm_payment_identifier"].ToString() : "";
			container.site_payment_identifier = !string.IsNullOrEmpty(context.Request.QueryString["site_payment_identifier"]) ? context.Request.QueryString["site_payment_identifier"].ToString() : "";
			container.site_order_identifier = !string.IsNullOrEmpty(context.Request.QueryString["site_order_identifier"]) ? context.Request.QueryString["site_order_identifier"].ToString() : "";
			container.site_customer_identifier = !string.IsNullOrEmpty(context.Request.QueryString["site_customer_identifier"]) ? context.Request.QueryString["site_customer_identifier"].ToString() : "";
			container.site_order_annotation = !string.IsNullOrEmpty(context.Request.QueryString["site_order_annotation"]) ? context.Request.QueryString["site_order_annotation"].ToString() : "";
			container.payment_amount = !string.IsNullOrEmpty(context.Request.QueryString["payment_amount"]) ? context.Request.QueryString["payment_amount"].ToString() : "";
			container.payment_currency = !string.IsNullOrEmpty(context.Request.QueryString["payment_currency"]) ? context.Request.QueryString["payment_currency"].ToString() : "";
			container.net_payment_amount = !string.IsNullOrEmpty(context.Request.QueryString["net_payment_amount"]) ? context.Request.QueryString["net_payment_amount"].ToString() : "";
			container.net_payment_currency = !string.IsNullOrEmpty(context.Request.QueryString["net_payment_currency"]) ? context.Request.QueryString["net_payment_currency"].ToString() : "";
			container.pnm_withheld_amount = !string.IsNullOrEmpty(context.Request.QueryString["pnm_withheld_amount"]) ? context.Request.QueryString["pnm_withheld_amount"].ToString() : "";
			container.pnm_withheld_currency = !string.IsNullOrEmpty(context.Request.QueryString["pnm_withheld_currency"]) ? context.Request.QueryString["pnm_withheld_currency"].ToString() : "";
			container.due_to_site_amount = !string.IsNullOrEmpty(context.Request.QueryString["due_to_site_amount"]) ? context.Request.QueryString["due_to_site_amount"].ToString() : "";
			container.due_to_site_currency = !string.IsNullOrEmpty(context.Request.QueryString["due_to_site_currency"]) ? context.Request.QueryString["due_to_site_currency"].ToString() : "";
			container.site_processing_fee = !string.IsNullOrEmpty(context.Request.QueryString["site_processing_fee"]) ? context.Request.QueryString["site_processing_fee"].ToString() : "";
			container.site_processing_currency = !string.IsNullOrEmpty(context.Request.QueryString["site_processing_currency"]) ? context.Request.QueryString["site_processing_currency"].ToString() : "";
			container.pnm_processing_fee = !string.IsNullOrEmpty(context.Request.QueryString["pnm_processing_fee"]) ? context.Request.QueryString["pnm_processing_fee"].ToString() : "";
			container.pnm_processing_currency = !string.IsNullOrEmpty(context.Request.QueryString["pnm_processing_currency"]) ? context.Request.QueryString["pnm_processing_currency"].ToString() : "";
			container.status = !string.IsNullOrEmpty(context.Request.QueryString["status"]) ? context.Request.QueryString["status"].ToString() : "";

            //Salcedo - log all requests, for troubleshooting/debugging purposes
            //Note: in web.config, set LogPriority to 3 or higher to log these messages
            RPO.ActivityLog al = new RPO.ActivityLog();
            string IncomingRequest = context.Request.RawUrl;
            al.WriteLog("PNMHandlers.Confirm.ProcessRequest", "RawUrl: " + IncomingRequest, 3, "System", false);

			string order_identifier = container.pnm_order_identifier;

			if (container != null || !string.IsNullOrEmpty(container.pnm_order_identifier))
			{
				//update and retrieve the PayNearMeOrder record
				RPO.PayNearMeOrder pnmOrder = RPO.PayNearMeOrder.UpdatePNMOrderForConfirmationCallBack(container);
				decimal netPaymentAmount = 0;
                //cakel: BUGID00395
                string PNMpaymentIdentifier = container.pnm_payment_identifier;

				if (pnmOrder != null && pnmOrder.Status == "payment" && Decimal.TryParse(container.net_payment_amount, out netPaymentAmount))
				{
                    //cakel: BUGID00395
                    
					RecordPayment(netPaymentAmount, new EfxFramework.Renter(pnmOrder.RenterId), "Rent By Cash successfully submitted", PNMpaymentIdentifier);
                    

					EfxFramework.Lease lease = EfxFramework.Lease.GetRenterLeaseByRenterId(pnmOrder.RenterId);
					AdjustCurrentBalanceDue(netPaymentAmount, lease);

                    DLAccess.GetRenterDetail _renter = new DLAccess.GetRenterDetail();
                    _renter = DLAccess.RenterDetails(pnmOrder.RenterId.ToString());

					//get the property
					EfxFramework.Property currentProperty = new EfxFramework.Property(pnmOrder.PropertyId);
					//EfxFramework.Logging.ExceptionLogger.LogException(new Exception("currentProperty is null = " + (currentProperty == null).ToString()));
					//EfxFramework.Logging.ExceptionLogger.LogException(new Exception("currentProperty id = " + (currentProperty.PropertyId)));
					if (currentProperty != null)
					{
                        //Salcedo - task 00337 - 1/12/2015 - moved appSettings to database
						//string PNMAchClientId = ConfigurationManager.AppSettings["PNMACHClientId"].ToString();
                        string PNMAchClientId = EfxSettings.PnmachClientId;

                        //EfxFramework.Logging.ExceptionLogger.LogException(new Exception(String.Format("values sent: {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}", 
                        //    Settings.AchUserId, 
                        //    Settings.AchPassword, 
                        //    PNMAchClientId, 
                        //    netPaymentAmount * -1,
                        //    currentProperty.RentalDepositBankAccountNumber,
                        //    "CHECKING",
                        //    "RPO",
                        //    "CASH",
                        //    "601 Cleveland Street",
                        //    "Suite 950",
                        //    "Clearwater",
                        //    "FL", "33755",
                        //    "855-769-7368"
                        //    )));

                        //cakel: BUGID00266 - Check if Property wants to settle by group or individual
                        if (currentProperty.GroupSettlementCashPaymentFlag == false)
                        {
                            //cakel: BUGID00266 - Updated code to use properties "Cash" account
                            
                            //Salcedo - 4/11/2015 - we should not be hitting the property with the debit for this credit, but 
                            //instead, we should debit the EFX account for the credit like was originally done before the change from Task # 0266.
                            //var RentResponse = EfxFramework.PaymentMethods.Ach.AchService.ProcessAchPayment(Settings.AchUserId, Settings.AchPassword, currentProperty.CashDepositAccountAchClientId, netPaymentAmount * -1, currentProperty.RentalDepositBankRoutingNumber,
                            //    currentProperty.RentalDepositBankAccountNumber, "CHECKING",_renter.FirstName, _renter.LastName, _renter.RenterStreetAddress,"", _renter.RenterCity, _renter.RenterState, "", "");

                            var RentResponse = EfxFramework.PaymentMethods.Ach.AchService.ProcessAchPayment(
                                EfxSettings.AchUserId, EfxSettings.AchPassword, 
                                PNMAchClientId, netPaymentAmount * -1, currentProperty.RentalDepositBankRoutingNumber,
                                currentProperty.RentalDepositBankAccountNumber, "CHECKING", _renter.FirstName, _renter.LastName, 
                                _renter.RenterStreetAddress, "", _renter.RenterCity, _renter.RenterState, "", "");

                            //cakel: BUGID00266 - commented out below var code and replace with var code above
                            //var RentResponse = EfxFramework.PaymentMethods.Ach.AchService.ProcessAchPayment(Settings.AchUserId, Settings.AchPassword, PNMAchClientId, netPaymentAmount * -1, currentProperty.RentalDepositBankRoutingNumber,
                            //    currentProperty.RentalDepositBankAccountNumber, "CHECKING", "RPO", "CASH", "601 Cleveland Street", "Suite 950", "Clearwater", "FL", "33755", "855-769-7368");

                            //EfxFramework.Logging.ExceptionLogger.LogException(new Exception(String.Format("RentResponse values: responseCode = {0}, responseDesc = {1}, result = {2}", RentResponse.ResponseCode, 
                            //    RentResponse.ResponseDescription, RentResponse.Result)));
                        }
					}
				}
				else if(pnmOrder != null && pnmOrder.Status == "decline")
				{
                    //cakel: BUGID00395
                    RecordPayment(netPaymentAmount, new EfxFramework.Renter(pnmOrder.RenterId), "Rent By Cash declined", PNMpaymentIdentifier);
				}
			}

			//send the xml response
			string source = "<t:payment_confirmation_response xsi:schemaLocation='http://www.paynearme.com/api/pnm_xmlschema_v2_0 pnm_xmlschema_v2_0.xsd' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' version='2.0' xmlns:t='http://www.paynearme.com/api/pnm_xmlschema_v2_0'></t:payment_confirmation_response>  ";
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(source);

			XmlElement paymentConfirmationElement = doc.CreateElement("t:confirmation", doc.DocumentElement.NamespaceURI);
			doc.DocumentElement.AppendChild(paymentConfirmationElement);

			XmlElement orderIdentifierElement = doc.CreateElement("t:pnm_order_identifier", doc.DocumentElement.NamespaceURI);
			orderIdentifierElement.AppendChild(doc.CreateTextNode(order_identifier));
			paymentConfirmationElement.AppendChild(orderIdentifierElement);

			context.Response.Clear();
			context.Response.ContentType = "text/xml";
			context.Response.ContentEncoding = System.Text.Encoding.UTF8;
			doc.Save(context.Response.Output);
			context.Response.End();
		}

		private static void RecordPayment(decimal amount, EfxFramework.Renter renter, string responseMessage, string _PNMPaymentID)
		{
            SetPayment(renter, EfxFramework.PaymentMethods.PaymentAmount.GetPaymentAmount(amount, renter, EfxFramework.PaymentMethods.PaymentType.RentByCash, false), "Rent By Cash", EfxFramework.PaymentMethods.PaymentApplication.Rent, responseMessage, _PNMPaymentID);
		}

        //cakel: BUGID00395 - added variable to include PayNearMe Order ID
		private static void SetPayment(EfxFramework.Renter renter, EfxFramework.PaymentMethods.PaymentAmount amount, string description, EfxFramework.PaymentMethods.PaymentApplication paymentApplication, string responseMessage, string PNMPaymentID)
		{
			if (!renter.PayerId.HasValue)
				renter.PayerId = 0;

			int PaymentId;

			PaymentId = EfxFramework.Payment.Set(EfxFramework.Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.RentByCash, null, null, null, null,
				null, amount, description, null, null, null, null, null, false, null,
				"0", responseMessage, null, EfxFramework.PaymentChannel.Web, EfxFramework.PublicApi.Payment.PaymentStatus.Cleared));
			EfxFramework.Transaction.SetTransactionFromPayment(new EfxFramework.Payment(PaymentId), 0, responseMessage);

            //cakel: BUGID00395 - this will update the transaction record with the PNM payment transaction ID
            InsertMerchantTransactionID(PaymentId, PNMPaymentID);

            SendPNMConfirmEmail(PaymentId);

		}

		private static void AdjustCurrentBalanceDue(decimal amount, EfxFramework.Lease lease)
		{
			lease.CurrentBalanceDue -= amount;
			lease.CurrentBalanceDueLastUpdated = DateTime.UtcNow;

			if (lease.CurrentBalanceDue <= 0.00M)
			    lease.PaymentDueDate = lease.PaymentDueDate.HasValue ? lease.PaymentDueDate.Value.AddMonths(1) : DateTime.UtcNow.AddMonths(1);

			EfxFramework.Lease.Set(lease);
		}


        //cakel: BUGID00248
        private static void SendPNMConfirmEmail(int PaymentID)
        {
            SqlDataReader reader = DLAccess.GetResidentTransactionDetail(PaymentID);
            reader.Read();
            if (reader.HasRows)
            {
                SqlConnection con = new SqlConnection(ConnString);
                SqlCommand com = new SqlCommand("EmailPayNearMeConfirmation_DomusMe", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = (int)reader["PropertyId"];
                com.Parameters.Add("@PropertyName", SqlDbType.NVarChar, 120).Value = reader["PropertyName"].ToString();
                com.Parameters.Add("@Name", SqlDbType.NVarChar, 120).Value = reader["FullName"].ToString();
                com.Parameters.Add("@RenterEmail", SqlDbType.NVarChar, 100).Value = reader["EmailAddress"].ToString();
                com.Parameters.Add("@Payment", SqlDbType.Money).Value = reader["PaymentAmount"].ToString();
                com.Parameters.Add("@ConvFee", SqlDbType.Money).Value = reader["ConvenienceFeeAmount"].ToString();
                com.Parameters.Add("@TransactionDate", SqlDbType.Date).Value = reader["TransactionDate"].ToString();
                com.Parameters.Add("@TransactionID", SqlDbType.NVarChar, 100).Value = reader["InternalTransactionId"].ToString();
                com.Parameters.Add("@TotalAmount", SqlDbType.Money).Value = reader["TotalAmount"].ToString();

                try
                {
                    con.Open();
                    com.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
                reader.Close();

            }


        }

        //cakel: BUGID00395
        public static void InsertMerchantTransactionID(int PaymentID, string ExtID)
        {
            //usp_Transaction_UpdateExternalMerchantTransID
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Transaction_UpdateExternalMerchantTransID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PaymentId", SqlDbType.Int).Value = PaymentID;
            com.Parameters.Add("@ExternalTransactionId", SqlDbType.NVarChar, 50).Value = ExtID;
            try
            {
                con.Open();
                com.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }



		public bool IsReusable
		{
			get
			{
				return false;
			}
		}

        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }



	}
}