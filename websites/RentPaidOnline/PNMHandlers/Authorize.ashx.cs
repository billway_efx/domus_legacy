﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Schema;
using RPO.PayNearMe;
using System.Globalization;

namespace Domus.PNMHandlers
{
	/// <summary>
	/// Authorizes a payment for PayNearMe.  Allows a resident to continue making the payment at POS.
	/// </summary>
	public class Authorize : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			AuthorizationCallBackContainer container = new AuthorizationCallBackContainer();
			container.pnm_order_identifier = !string.IsNullOrEmpty(context.Request.QueryString["pnm_order_identifier"]) ? context.Request.QueryString["pnm_order_identifier"].ToString() : "";
			container.pnm_payment_identifier = !string.IsNullOrEmpty(context.Request.QueryString["pnm_payment_identifier"]) ? context.Request.QueryString["pnm_payment_identifier"].ToString() : "";
			container.site_order_identifier = !string.IsNullOrEmpty(context.Request.QueryString["site_order_identifier"]) ? context.Request.QueryString["site_order_identifier"].ToString() : "";
			container.site_customer_identifier = !string.IsNullOrEmpty(context.Request.QueryString["site_customer_identifier"]) ? context.Request.QueryString["site_customer_identifier"].ToString() : "";
			container.site_order_annotation = !string.IsNullOrEmpty(context.Request.QueryString["site_order_annotation"]) ? context.Request.QueryString["site_order_annotation"].ToString() : "";
			container.payment_amount = !string.IsNullOrEmpty(context.Request.QueryString["payment_amount"]) ? context.Request.QueryString["payment_amount"].ToString() : "";
			container.payment_currency = !string.IsNullOrEmpty(context.Request.QueryString["payment_currency"]) ? context.Request.QueryString["payment_currency"].ToString() : "";
			container.net_payment_amount = !string.IsNullOrEmpty(context.Request.QueryString["net_payment_amount"]) ? context.Request.QueryString["net_payment_amount"].ToString() : "";
			container.net_payment_currency = !string.IsNullOrEmpty(context.Request.QueryString["net_payment_currency"]) ? context.Request.QueryString["net_payment_currency"].ToString() : "";
			container.pnm_withheld_amount = !string.IsNullOrEmpty(context.Request.QueryString["pnm_withheld_amount"]) ? context.Request.QueryString["pnm_withheld_amount"].ToString() : "";
			container.pnm_withheld_currency = !string.IsNullOrEmpty(context.Request.QueryString["pnm_withheld_currency"]) ? context.Request.QueryString["pnm_withheld_currency"].ToString() : "";
			container.due_to_site_amount = !string.IsNullOrEmpty(context.Request.QueryString["due_to_site_amount"]) ? context.Request.QueryString["due_to_site_amount"].ToString() : "";
			container.due_to_site_currency = !string.IsNullOrEmpty(context.Request.QueryString["due_to_site_currency"]) ? context.Request.QueryString["due_to_site_currency"].ToString() : "";
			container.site_processing_fee = !string.IsNullOrEmpty(context.Request.QueryString["site_processing_fee"]) ? context.Request.QueryString["site_processing_fee"].ToString() : "";
			container.site_processing_currency = !string.IsNullOrEmpty(context.Request.QueryString["site_processing_currency"]) ? context.Request.QueryString["site_processing_currency"].ToString() : "";
			container.pnm_processing_fee = !string.IsNullOrEmpty(context.Request.QueryString["pnm_processing_fee"]) ? context.Request.QueryString["pnm_processing_fee"].ToString() : "";
			container.pnm_processing_currency = !string.IsNullOrEmpty(context.Request.QueryString["pnm_processing_currency"]) ? context.Request.QueryString["pnm_processing_currency"].ToString() : "";
			container.payment_date = !string.IsNullOrEmpty(context.Request.QueryString["payment_date"]) ? context.Request.QueryString["payment_date"].ToString() : "";

            //Salcedo - log all requests, for troubleshooting/debugging purposes
            //Note: in web.config, set LogPriority to 3 or higher to log these messages
            RPO.ActivityLog al = new RPO.ActivityLog();
            string IncomingRequest = context.Request.RawUrl;
            al.WriteLog("PNMHandlers.Authorize.ProcessRequest", "RawUrl: " + IncomingRequest, 3, "System", false);

			bool createPayment = true;
			int currentRenterId = 0;
			AuthorizationHandler handler = new AuthorizationHandler();
			XmlDocument doc = handler.GetAuthorizationResponse(container, out currentRenterId, out createPayment);

			context.Response.Clear();
			context.Response.ContentType = "text/xml";
			context.Response.ContentEncoding = System.Text.Encoding.UTF8;
			doc.Save(context.Response.Output);
			context.Response.End();
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}