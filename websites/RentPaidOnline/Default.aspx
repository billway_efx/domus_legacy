﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Domus.Default" %>
<%@ Import Namespace="EfxFramework.ExtensionMethods" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="main-container">
   <div ng-include="'/search-filters.html'" id="searchFilters"></div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="server">
    <script src="<%= ResolveClientUrl("~/Scripts/promise-tracker.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/rpo-search.js") %>"></script>
</asp:Content>