﻿using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.Interfaces;
using EfxFramework.Interfaces.UserControls.PaymentControlsV2;
using EfxFramework.Presenters;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;
using EfxFramework.Web;
using EfxFramework;
using System;
using Mb2x.ExtensionMethods;
using System.Diagnostics;
using EfxFramework.Logging;
using RPO.Helpers;
using RPO;
using System.Windows.Forms;
using EfxFramework.ExtensionMethods;

namespace Domus.PaymentsV2
{
   public partial class Payment : BasePage, IPayment
   {
      private PaymentPresenter _Presenter;

      public Page ParentPage { get { return this; } }
      public DropDownList PropertyList { get { return PropertyDropDown; } }
      public System.Web.UI.WebControls.CheckBox ApplyToNextMonthCheckBox { get { return uxApplyPaymentToNextMonth; } }
      public EventHandler PayerTypeChanged { set { PayerTypeRadioButtonList.SelectedIndexChanged += value; } }
      public string SelectedPayerType { get { return PayerTypeRadioButtonList.SelectedValue; } }
      public string PayerLabelText { set { PayerLabel.Text = value; } }
      public DropDownList PayerList { get { return PayerDropDown; } }
      public bool IsDifferentPayerChecked { get { return DifferentPayerCheckBox.Checked; } }
      public EventHandler LoadButtonClicked { set { LoadPayerButton.Click += value; } }
      //public EventHandler CancelButtonClicked { set { } }//btnCancel.Click += value; } }
      public EventHandler DifferentPayerChecked { set { DifferentPayerCheckBox.CheckedChanged += value; } }
      public IPersonalInformation PersonalInformationControl { get { return PersonalInformation; } }
      public IResidentBillingInformation ResidentBillingInformationControl { get { return ResidentBillingInformation; } }
      public ISelectPaymentType SelectPaymentTypeControl { get { return SelectPaymentType; } }
      public IPaymentAmountsAlternate PaymentAmountsControl { get { return PaymentAmounts; } }
      //public bool AutoPayVisible { set {  false; } }
      public IAutoPayTile AutoPayTileControl { get { return AutoPayTile; } }
      public bool TermsChecked { get { return TermsCheckbox.Checked; } set { TermsCheckbox.Checked = value; } }
      public bool PayNowButtonEnabled { set { PayNowButton.Enabled = value; } }
      public EventHandler PayNowClicked { set { PayNowButton.Click += value; } }
      public HyperLink RedirectApplicantLink { get { return ApplicantLink; } }
      public bool IsPropertyManagerSite { get { return !EfxFramework.Helpers.Helper.IsRpoAdmin; } }

      public bool IsCsrUser { get { return EfxFramework.Helpers.Helper.IsCsrUser; } }
      public bool IsOverride { get { return PaymentAmounts.OverrideText.Enabled && PaymentAmounts.OverrideText.Text.Replace("$", "").ToNullDecimal().HasValue; } }
      public bool ApplyPaymentToNextMonth { get { return uxApplyPaymentToNextMonth.Checked; } set { uxApplyPaymentToNextMonth.Checked = value; } }
      public bool AddToAutoPay { get { return chkAddtoAutoPay.Checked; } set { chkAddtoAutoPay.Checked = value; } }
      public int PaymentDay { get { return PaymentProcessingDateComboBox.SelectedValue.ToInt32(); } set { PaymentProcessingDateComboBox.SelectedValue = value.ToString(); } }
      public string UnmaskedCCNumber { get { return CCNumber; } set { CCNumber = value; } }
      public string UnmaskedACHAccountNumber { get { return ACHAccountNumber; } set { ACHAccountNumber = value; } }
      public string CCNumber;
      public string ACHAccountNumber;
      private static bool bScheduledPayment;

      protected void Page_Load(object sender, EventArgs e)
      {
         var stopWatch = new Stopwatch();
         stopWatch.Start();

         _Presenter = new PaymentPresenter(this);



         if (!Page.IsPostBack)
            _Presenter.InitializeValues();

         InitializeControls();
         SetupPayNowButton();

         TimeSpan ts = stopWatch.Elapsed;
         string elapsedTime = String.Format(" Payment.Page_Load() RunTime: {0:00}:{1:00}:{2:00}:{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
         //ExceptionLogger.LogMessage(elapsedTime);

         //cakel: BUGID000133 - Added "EfxFrameWork to Page to use ActivityLog
         if (!Page.IsPostBack)
         {
            string _user = Page.User.Identity.Name.ToString();
            ActivityLog AL = new ActivityLog();
            AL.WriteLog(_user, "Payments Page", (short)LogPriority.LogAlways);
         }
         //cakel: BUGID00053 hide if applicant is selected
         string PayeeType = PayerTypeRadioButtonList.SelectedItem.ToString();
         if (PayeeType == "Applicant")
         {
            uxApplyPaymentToNextMonth.Visible = false;
            NextMOnthLabel.Visible = false;
         }
         else
         {
            uxApplyPaymentToNextMonth.Visible = true;
            NextMOnthLabel.Visible = true;
         }

         //cakel: BUGID00053 Hides link if property is integrated
         string PropID = PropertyDropDown.SelectedValue;
         string Stat = EfxFramework.Property.GetIntegrationStatus(PropID.ToInt32());
         if (Stat == "Non-Integrated")
         {
            UpdateResidentFeesLink.Visible = true;
            string LinkString = "/MyProperties/PropertyDetails.aspx?propertyId=" + PropID + "#RentAndFeesTab";
            UpdateResidentFeesLink.NavigateUrl = LinkString;
         }
         else
         {
            UpdateResidentFeesLink.Visible = false;
         }

         if (SelectPaymentTypeControl.PaymentTypeSelectedValue == "3")
         {
            chkAddtoAutoPay.Visible = false;
         }

         else
         {
            chkAddtoAutoPay.Visible = true;
         }

         if (EfxFramework.Helpers.Helper.IsCsrUser)
         {
            PayerTypeRadioButtonList.Enabled = false;
            chkAddtoAutoPay.Enabled = false;
            NextMOnthLabel.Disabled = true;
         }
      }

      private void InitializeControls()
      {
         var Id = PayerList.SelectedValue.ToNullInt32();
         if (!Id.HasValue || Id.Value < 1)
            return;

         PersonalInformation.RenterId = Id.Value;
         ResidentBillingInformation.RenterId = Id.Value;
         SelectPaymentType.RenterId = Id.Value;
         PaymentAmounts.RenterId = Id.Value;
         PaymentAmounts.PaymentTypes = SelectPaymentType;
         AutoPayTile.RenterId = Id.Value;

         //cakel: BUGID000116
         WarningText();

      }

      private void SetupPayNowButton()
      {
         int selectedPropertyId = 0;
         if (Int32.TryParse(PropertyDropDown.SelectedValue, out selectedPropertyId) && selectedPropertyId > 0)
         {
            EfxFramework.Property selectedProperty = new EfxFramework.Property(selectedPropertyId);
            if (string.IsNullOrEmpty(selectedProperty.RentalDepositBankAccountNumber) || string.IsNullOrEmpty(selectedProperty.RentalDepositBankRoutingNumber))
            {
               PayNowButton.Enabled = false;
               PayNowButton.Visible = false;
            }
            else
            {
               PayNowButton.Enabled = true;
               PayNowButton.Visible = true;
            }
         }
         else
         {
            PayNowButton.Enabled = false;
            PayNowButton.Visible = false;
         }
      }

      //cakel: BUGID000133
      protected void TermsCheckbox_CheckedChanged(object sender, EventArgs e)
      {

         string _user = Page.User.Identity.Name.ToString();
         string CheckedStatus = "";
         if (TermsCheckbox.Checked == true)
         {
            CheckedStatus = "Accepted";
         }
         else
         {
            CheckedStatus = "Unaccepted";
         }

         ActivityLog AL = new ActivityLog();
         AL.WriteLog(_user, "Admin Site User Click Terms and Agreements - " + CheckedStatus, (short)LogPriority.LogAlways);
      }


      //cakel: BUGID000116
      public void WarningText()
      {
         string Id = PayerList.SelectedValue.ToString();
         DLAccess.GetRenterDetail _renter = new DLAccess.GetRenterDetail();
         _renter = DLAccess.RenterDetails(Id);
         string CashOnlyText = "";
         if (_renter.AcceptedPaymentTypeId == "3")
         {
            WarningLabeltxt.Visible = true;
            CashOnlyText = "The selected resident is currently on " + @"""Cash Equivalent""" + "status. eCheck payments are not permitted! <br />If this is an error, please update the residents status in your property management software and try again!";
         }
         else
         {
            WarningLabeltxt.Visible = false;
            CashOnlyText = "";
         }

         WarningLabeltxt.Text = CashOnlyText;
      }

      //CMallory - Task 0090 - Added PayNowButton_click method that shows a Message Box with specific text based on the payment type.
      protected void PayNowButton_Click(object sender, EventArgs e)
      {

         //if (AutoPayTile.IsOn)
         //{
         //    //Credit Card
         //    if (AutoPayTile.PaymentTypeDropdown.SelectedValue == "1")
         //    {
         //        MessageBoxText("AutoPay setup has been completed. You will receive a security validation email on the " + AutoPayTile.DayOfTheMonthDropdown.SelectedValue + " of each month, once confirmed your payment will process automatically.");
         //    }

         //    //ACH
         //    else if (AutoPayTile.PaymentTypeDropdown.SelectedValue == "2")
         //    {
         //        MessageBoxText("AutoPay setup has been completed. Your payments will automatically process by E-Check on the " + AutoPayTile.DayOfTheMonthDropdown.SelectedValue + " of each month.");

         //    }
         //}
      }

      //CMallory - Task 0090 - Added utility method below.
      private void MessageBoxText(string text)
      {
         MessageBox.Show(text, "Important Note", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
      }

      protected void PayNowButton_Click1(object sender, EventArgs e)
      {
         var Resident = new EfxFramework.Renter(PayerList.SelectedValue.ToInt32());
         if (Resident.RenterId < 1)
            return;

         int PayerId;

         if (!Resident.PayerId.HasValue || Resident.PayerId.Value < 1)
            PayerId = EfxFramework.Payer.SetPayerFromRenter(Resident).PayerId;
         else
            PayerId = Resident.PayerId.Value;

         if (SelectPaymentType.PaymentMethodDropDown.SelectedValue == "1" &&
             !string.IsNullOrEmpty(SelectPaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText))
         {
            string LastFourCreditCard = string.Empty;

            var CreditCard = EfxFramework.PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(PayerId);
            if (CreditCard.PayerCreditCardId < 1)
               return;

            string LastFourCCTextBox = SelectPaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText.Substring(Math.Max(0, SelectPaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText.Length - 4));

            if (!string.IsNullOrEmpty(CreditCard.CreditCardAccountNumber))
            {
               LastFourCreditCard = CreditCard.CreditCardAccountNumber.Substring(CreditCard.CreditCardAccountNumber.Length - 4);
            }

            //Compare last 4 digits of credit card retrieved from database to last 4 digits of what's in the text field.
            if (LastFourCCTextBox == LastFourCreditCard)
            {
               CCNumber = CreditCard.CreditCardAccountNumber;
               SelectPaymentType.CreditCardDetailsControl.HiddenCreditCard = CCNumber;
            }

            else
            {
               SelectPaymentType.CreditCardDetailsControl.HiddenCreditCard = SelectPaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText;
            }
         }

         else if (SelectPaymentType.PaymentMethodDropDown.SelectedValue == "2" &&
                  !string.IsNullOrEmpty(SelectPaymentTypeControl.ECheckDetailsControl.AccountNumberText))
               {
                  string LastFourACH = string.Empty;

                  var ACH = EfxFramework.PayerAch.GetPrimaryPayerAchByPayerId(PayerId);
                  if (ACH.BankAccountTypeId < 1)
                     return;

                  string LastFourACHTextBox = SelectPaymentTypeControl.ECheckDetailsControl.AccountNumberText.Substring(Math.Max(0, SelectPaymentTypeControl.ECheckDetailsControl.AccountNumberText.Length - 4));


                  if (!string.IsNullOrEmpty(ACH.BankAccountNumber))
                  {
                     LastFourACH = ACH.BankAccountNumber.Substring(ACH.BankAccountNumber.Length - 4);
                  }

                  //Compare last 4 digits of credit card retrieved from database to last 4 digits of what's in the text field.
                  if (LastFourACHTextBox == LastFourACH)
                  {
                     ACHAccountNumber = ACH.BankAccountNumber;
                     SelectPaymentType.ECheckDetailsControl.HiddenACHAccountNumber = ACHAccountNumber;
                  }

                  else
                  {
                     SelectPaymentType.ECheckDetailsControl.HiddenACHAccountNumber = SelectPaymentTypeControl.ECheckDetailsControl.AccountNumberText;
                  }
               }
      }

      protected void chkAddtoAutoPay_CheckedChanged(object sender, EventArgs e)
      {
         if (chkAddtoAutoPay.Checked)
         {
            PaymentProcessingDateComboBox.Visible = true;
            lblPaymentDate.Visible = true;
         }

         else
         {
            PaymentProcessingDateComboBox.Visible = false;
            lblPaymentDate.Visible = false;
         }
      }

      private static EfxFramework.PaymentChannel GetPaymentChannel()
      {
         var paymentChannel = EfxFramework.PaymentChannel.Web;

         //Salcedo - 2/27/2014 - if the payment was scheduled, we don't have a CurrentUserID, so just set the channel to AutoPayment
         if (bScheduledPayment)
         {
            paymentChannel = EfxFramework.PaymentChannel.AutoPayment;
         }
         else
         {
            var renter = new EfxFramework.Renter(EfxFramework.Helpers.Helper.CurrentUserId);
            if (renter.RenterId != 0 && EfxSettings.CurrentSite.ToLower().Equals("resident"))
            {
               paymentChannel = EfxFramework.PaymentChannel.ResidentSite;
            }
            else if (EfxFramework.Helpers.Helper.IsRpoAdmin && EfxSettings.CurrentSite.ToLower().Equals("admin"))
            {
               paymentChannel = EfxFramework.PaymentChannel.AdminSite;
            }
            else if ((EfxFramework.Helpers.Helper.IsCorporateAdmin || EfxFramework.Helpers.Helper.IsPropertyManager) && EfxSettings.CurrentSite.ToLower().Equals("admin"))
            {
               paymentChannel = EfxFramework.PaymentChannel.PropertyManagerSite;
            }
         }

         return paymentChannel;
      }
   }
}