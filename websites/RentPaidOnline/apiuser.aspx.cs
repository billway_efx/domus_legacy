﻿using EfxFramework;
using System;
using System.Linq;
using Infragistics.Web.UI;
using Infragistics.Web.UI.GridControls;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web;
using System.IO;

using System.Xml;
using System.Text;
using EfxFramework.Scheduler;
using System.Net;
using System.Net.Mail;
using EfxFramework.PublicApi;

namespace Domus
{
    public partial class apiuser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!EfxFramework.Helpers.Helper.IsRpoAdmin )
            {
                //You shouldn't be here. Please leave ...
                Response.Redirect("/Default.aspx");
            }
        }

        protected void cmdAdd_Click(object sender, EventArgs e)
        {
            SetNewApiUser(txtUserName.Text, txtPassword.Text, Convert.ToInt16((txtPartnerCompanyId.Text)) );
        }

        private void SetNewApiUser(string NewUserName, string NewPassword, int PartnerCompanyId)
        {
            var NewApiUser = new ApiUser
            {
                Username = NewUserName, 
                IsActive = true
            };

            NewApiUser.SetPassword(NewPassword);
            NewApiUser.PartnerCompanyId = PartnerCompanyId;

            var ApiUserId = ApiUser.Set(NewApiUser);

            var User = new ApiUser(ApiUserId);

        }

        protected void cmdMaintain_Click(object sender, EventArgs e)
        {
            Response.Redirect("/rpo/apiusers");
        }


    }
}