﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework;
using RPO;

namespace RentPaidOnline
{
    public partial class X_Test_RentByCash : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {


            string resp = CreatePaynearMeTicket("31", "200.00");


        }


        public string CreatePaynearMeTicket(string RenterID, string AmountBeingPaid)
        {
            int _RenterID = int.Parse(RenterID);

            EfxFramework.Renter Resident = new EfxFramework.Renter(_RenterID);

            EfxFramework.Company currentCompany = EfxFramework.Company.GetCompanyByRenterId(Resident.RenterId);
            var Property = EfxFramework.Property.GetPropertyByRenterId(Resident.RenterId);

            var Address = EfxFramework.Address.GetAddressByRenterIdAndAddressType(Resident.RenterId, (int)TypeOfAddress.Billing);
            Address.AddressTypeId = (int)TypeOfAddress.Billing;
            Address.AddressLine1 = Resident.StreetAddress;
            Address.City = Resident.City;
            Address.StateProvinceId = (int)Resident.StateProvinceId;
            Address.PostalCode = Resident.PostalCode;
            Address.PrimaryEmailAddress = Resident.PrimaryEmailAddress;
            Address.PrimaryPhoneNumber = Resident.MainPhoneNumber;

            RPO.PayNearMeRequest pnmRequest = new RPO.PayNearMeRequest();
            pnmRequest.Amount = decimal.Parse(AmountBeingPaid);
            pnmRequest.CreatorIdentifier = Resident.PublicRenterId.ToString();
            pnmRequest.Currency = "USD";
            pnmRequest.CustomerCity = Address.City;
            pnmRequest.CustomerEmail = Address.PrimaryEmailAddress;
            pnmRequest.CustomerIdentifier = Resident.PublicRenterId.ToString();
            pnmRequest.CustomerName = Resident.FirstName + " " + Resident.LastName;
            pnmRequest.CustomerPhone = Address.PrimaryPhoneNumber;
            pnmRequest.CustomerPostalCode = Address.PostalCode;
            pnmRequest.CustomerState = Address.GetStateName();
            pnmRequest.CustomerStreet = Address.AddressLine1;
            pnmRequest.MinimumPaymentAmount = 1;
            pnmRequest.MinimumPaymentCurrency = "USD";
            pnmRequest.OrderDescription = "Resident Payment";
            pnmRequest.OrderDuration = 1;
            pnmRequest.OrderIdentifier = Guid.NewGuid().ToString();
            pnmRequest.OrderType = RPO.PayNearMeRequest.PNMOrderType.Exact;
            pnmRequest.RenterId = Resident.RenterId;
            pnmRequest.PropertyId = (int)Property.PropertyId;
            pnmRequest.CompanyId = currentCompany.CompanyId;
            pnmRequest.SiteIdentifier = currentCompany.PNMSiteId;
            pnmRequest.SecretKey = currentCompany.PNMSecretKey;
            pnmRequest.Version = Settings.PNMVersion;

            var Log = new RPO.ActivityLog();
            Log.WriteLog(Resident.PrimaryEmailAddress, "PNM Create New Ticket", 0, Resident.RenterId.ToString(), false);

            RPO.RentByCash rentByCash = new RPO.RentByCash();
            rentByCash.SandBoxMode = bool.Parse(Settings.PNMUseSandboxMode);

            string response = rentByCash.CreatePayNearMeRequest(pnmRequest, rentByCash.SandBoxMode);

            return response;

        }




    }
}