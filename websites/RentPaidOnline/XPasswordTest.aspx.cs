﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.Security.Authentication;
using System.Data;
using System.Data.SqlClient;

namespace Domus
{
    public partial class XPasswordTest : System.Web.UI.Page
    {

        public byte[] PasswordHash { get; set; }
        public byte[] Salt { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Redirect("~/Default.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {   
            int _renterID = int.Parse(TextBox1.Text);
            string NewPassword = TextBox2.Text;

            //Generate a new salt and set it
            Salt = AuthenticationTools.GenerateSalt();

            //Hash the new password and set it
            PasswordHash = AuthenticationTools.HashPassword(NewPassword, Salt);

           SetNewPassword(_renterID, PasswordHash,Salt);

        }


        private void SetNewPassword(int renterID, byte[] pw, byte[] salt)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_EfxAdmin_SetNewPassword", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@id", SqlDbType.Int).Value = renterID;
            com.Parameters.Add("@pw", SqlDbType.Binary, 64).Value = pw;
            com.Parameters.Add("@salt", SqlDbType.Binary, 64).Value = salt;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                checkPasswordlbl.Text = "Password Changed!";
            }
            catch(Exception ex)
            {
                con.Close();
                checkPasswordlbl.Text = "Something is wrong";
                throw ex;
            }
        }



        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }




    }
}