﻿using System;

namespace RentPaidOnline
{
    public partial class XYardiTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {}

        protected void Button1_Click(object sender, EventArgs e)
        {
            EfxFramework.Pms.Yardi.ExportRequestV2.ExportPaymentsForProperty(1213);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            const int propId = 1659;
            EfxFramework.Pms.Yardi.ExportRequestV2.ExportApplicantPaymentByPropertyId(propId);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            EfxFramework.Pms.Yardi.ExportRequestV2.ExportAllPropertiesApplicationPayments();
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            EfxFramework.Pms.Yardi.ImportRequest.ImportSingleProperty(1187);
        }
    }
}