﻿using System.Web;
using EfxFramework.Web;
using Microsoft.Reporting.WebForms;
using System;
using System.Configuration;

namespace Domus.Reports
{
    public partial class Default : BasePage
    {
        //Page Events
        protected override void OnInit(EventArgs e)
        {
            ////Verify permissions
            //if (!HttpContext.Current.User.Identity.IsAuthenticated)
            //    Server.Transfer("/Pages/AccessDenied.aspx");

            ////Call the base method
            //base.OnInit(e);
            Response.Redirect("/rpo/reports");
        }
        protected override void OnLoad(EventArgs e)
        {
            //Bind the report on load
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["reportPath"]))
                    BindReport(Request.QueryString["reportPath"]);
                else
                    BindReport();
            }

            //Call the base method
            base.OnLoad(e);
        }
        protected void ReportDropdownSelectedIndexChanged(object sender, EventArgs e)
        {
            //Rebind when the report selection changes
            BindReport();
        }

        //Private Methods
        private void BindReport(string reportOverride = null)
        {
            var reportPath = !string.IsNullOrEmpty(reportOverride) ? reportOverride : ReportDropdown.SelectedValue;

            if (reportPath == "MailDeliveryReportPath")
            {
                // Use local mode for emebedded report
                EfxReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                EfxReportViewer.LocalReport.ReportEmbeddedResource = "Domus.Reporting.MailDeliveryRateReport.rdlc";
                EfxReportViewer.ShowParameterPrompts = false;
                EfxReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsMail", EfxFramework.BulkMail.BulkMail.GetMessageDeliveryRate()));
                EfxReportViewer.LocalReport.Refresh();
            }
            else
            {
                // Use remote mode for reports hosted by SSRS
                EfxReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                EfxReportViewer.ShowParameterPrompts = true;
                EfxReportViewer.ServerReport.ReportServerCredentials = new EfxFramework.Reporting.ReportServerConnection();
                EfxReportViewer.ServerReport.ReportPath = ConfigurationManager.AppSettings[reportPath];

                // If PropertyStaffId parameter exists, set it to -1 to show all available properties since this is the admin site.
                var reportParameterInfoCollection = EfxReportViewer.ServerReport.GetParameters();
                foreach (ReportParameterInfo info in reportParameterInfoCollection)
                {
                    if (info.Name == "PropertyStaffId")
                    {
                        ReportParameter propertyStaffIdParm = new Microsoft.Reporting.WebForms.ReportParameter("PropertyStaffId", "-1");
                        EfxReportViewer.ServerReport.SetParameters(propertyStaffIdParm);
                    }
                }                

                EfxReportViewer.ServerReport.Refresh();
            }
            ReportDropdown.SelectedValue = reportPath;
        }
    }
}