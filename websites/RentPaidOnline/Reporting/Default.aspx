﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Domus.Reports.Default" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<script> $(function () { EFXClient.SetSelectedNavigation('reports-link'); });
	</script>
	<!-- IE 9 throws an error when this is called, so we need to override it and catch that error -->
	<!--[if IE 9]>
    <script language="javascript" type="text/javascript">
        $(window).ready(function () {
            window.getComputedStyle = function (el, pseudo) {
                this.el = el;
                this.getPropertyValue = function (prop) {
                    var re = /(\-([a-z]){1})/g;
                    if (prop == 'float') prop = 'styleFloat';
                    if (re.test(prop)) {
                        prop = prop.replace(re, function () {
                            return arguments[2].toUpperCase();
                        });
                    }
                    //error occures on the currentStyle
                    //return el.currentStyle[prop] ? el.currentStyle[prop] : null;
                    if (typeof(el.currentStyle) == 'undefined') {
                        return null;
                    } else {
                        return el.currentStyle[prop] ? el.currentStyle[prop] : null;
                    }
                }
                return this;
            }
        })        
    </script>
    <![endif]-->
	<div>
		<img src="/images/reportsheader.png" alt="Reports" style="float: left;" />
	</div>
	<div id="PropertyDetails">
		<div class="float-right" style="width: 100%">
			<div class="green-gradient-wrap padding float-wrap">
				<h3>Select Report
				</h3>
				<div>
					<asp:DropDownList ID="ReportDropdown" runat="server" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="ReportDropdownSelectedIndexChanged">
                        <asp:ListItem Text="Applicant Portal Payment Report" Value="ApplicantPortalPaymentReportPath"></asp:ListItem>
                        <asp:ListItem Text="AutoPayment Report" Value="AutoPaymentReportPath"></asp:ListItem>
                        <asp:ListItem Text="Billing Report" Value="BillingReportPath"></asp:ListItem>
                        <asp:ListItem Text="Detailed Deposit Report" Value="DetailedDepositReportPath"></asp:ListItem>
						<%--<asp:ListItem Text="Lead Report" Value="LeadReportPath"></asp:ListItem>--%>
                        <asp:ListItem Text="Mail Delivery Report" Value="MailDeliveryReportPath"></asp:ListItem>
                        <asp:ListItem Text="Payment Export Summary Report" Value="PaymentExportSummaryReportPath"></asp:ListItem>
                        <asp:ListItem Text="Payment Summary Report" Value="PaymentSummaryReportPath"></asp:ListItem>
                        <asp:ListItem Text="Resident Data Import Summary Report" Value="ResidentDataImportSummaryReportPath" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Resident Report" Value="RenterReportPath" Selected="True"></asp:ListItem>						
						<asp:ListItem Text="Residents Past Due Report" Value="RentersPastDueReportPath"></asp:ListItem>
						<%--<asp:ListItem Text="Transaction Summary Report" Value="TransactionSummaryReportPath"></asp:ListItem>--%>
					</asp:DropDownList>
				</div>
				<br />
				<div class="ReportWrap">
					<rsweb:ReportViewer ID="EfxReportViewer" runat="server" ProcessingMode="Remote" Width="975px" ShowParameterPrompts="true" ShowBackButton="False" ShowDocumentMapButton="False" ShowFindControls="False" ShowPageNavigationControls="True" Height="500px" BackColor="#f0f5de">
					</rsweb:ReportViewer>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
