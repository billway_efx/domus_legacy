﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using System.Text;
using EfxFramework;
using EfxFramework.Web.Facade;
using RPO.Reports;
using RPO;
using System.Data.SqlClient;
using RPO.Helpers;
using System.Web.UI.HtmlControls;
using System.Reflection;
using System.IO;
using System.Web.UI;
using WebSupergoo.ABCpdf8;
using System.Web.UI.WebControls;
using RPO.Reports.Formatters;

namespace Domus.Reporting
{
    public class Export : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //get all the parameters
            string fileType = "";
            string report = "";
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();
            string propertyList = "";

            if (context.Request.QueryString["f"] != null)
                fileType = context.Request.QueryString["f"];
            else
                throw new Exception("QueryString f must have a value");

            if (context.Request.QueryString["r"] != null)
                report = context.Request.QueryString["r"];
            else
                throw new Exception("QueryString r must have a value");

            if (context.Request.QueryString["s"] != null)
            {
                string startD = HttpUtility.UrlDecode(context.Request.QueryString["s"]).TrimStart('"').TrimEnd('"');
                DateTime.TryParse(startD,  out startDate);
                startDate = startDate.ToUniversalTime();
            }

            if (context.Request.QueryString["e"] != null)
            {
                string endD = HttpUtility.UrlDecode(context.Request.QueryString["e"]).TrimStart('"').TrimEnd('"');
                DateTime.TryParse(endD, out endDate);
                endDate = endDate.ToUniversalTime();
            }

            if (context.Request.QueryString["p"] != null)
                propertyList = context.Request.QueryString["p"];

            propertyList = propertyList.TrimStart('[').TrimEnd(']');;
            //verify this user has the appropriate access
            int Id;
            Int32.TryParse(HttpContext.Current.User.Identity.Name, out Id);
            List<RoleName> roleListStaff = new List<RoleName> { RoleName.PropertyManager, RoleName.LeasingAgent, RoleName.CorporateAdmin };
            List<RoleName> roleListAdmin = new List<RoleName> { RoleName.EfxPowerAdministrator };
            var HasPermission = RoleManager.IsUserInRole(EfxFramework.RoleType.PropertyStaff, Id, roleListStaff);
            var HasAdminPermission = RoleManager.IsUserInRole(EfxFramework.RoleType.EfxAdministrator, Id, roleListAdmin);

            UIFacade Facade = new UIFacade();
            if (!HasAdminPermission && !HasPermission && !Facade.CurrentStaff.CanViewReports)
                SafeRedirect("/Pages/AccessDenied.aspx");
            
            //make sure the current user has access to the passed in property list
            RPOReport rpoReport = new RPOReport();
            if (HasPermission)
            {
                List<PropertiesListing> returnList = null;
                ReportQuery rq = new ReportQuery();
                rq.PropertyStaffId = Id;
                returnList = rpoReport.PropertiesByStaffId(rq);

                if (returnList.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    string[] propArray = propertyList.Split(',');
                    returnList.ForEach(p => sb.Append(GetVerifiedPropertyString(propArray, p.PropertyId)));
                    propertyList = sb.ToString();
                    propertyList.TrimEnd(',');
                }
                else
                {
                    SafeRedirect("/Pages/AccessDenied.aspx");
                }
            }

            //make the call to get the data
            ReportQuery query = new ReportQuery();
            query.EndDate = endDate;
            query.FlattenData = true;
            query.PropertyIdList = propertyList.Split(',');
            query.StartDate = startDate;

            switch (report)
            {
                case "applicant-portal-payment":
                    if (fileType == "CSV")
                        CSVWriteHelper.WriteCSVToResponse(rpoReport.ApplicantPortalPaymentReport(query), true, context.Response);
                    else if (fileType == "PDF")
                        WritePdfToResponse(rpoReport.ApplicantPortalPaymentReport(query), context.Response, "Applicant Payment", query.StartDate, query.EndDate);
                    break;
                case "auto-payment":
                    if (fileType == "CSV")
                        CSVWriteHelper.WriteCSVToResponse(rpoReport.AutoPaymentReportExport(query), true, context.Response);
                    else if (fileType == "PDF")
                        WritePdfToResponse(rpoReport.AutoPaymentReportExport(query), context.Response, "AutoPayment", query.StartDate, query.EndDate);
                    break;
                case "billing":
                    if (fileType == "CSV")
                        CSVWriteHelper.WriteCSVToResponse(rpoReport.BillingReport(query), true, context.Response);
                    else if (fileType == "PDF")
						WritePdfToResponse(rpoReport.BillingReport(query), context.Response, "Billing", query.StartDate, query.EndDate);
                    break;
                case "charity":
                    if (fileType == "CSV")
                        CSVWriteHelper.WriteCSVToResponse(rpoReport.CharityReport(query), true, context.Response);
                    else if (fileType == "PDF")
						WritePdfToResponse(rpoReport.CharityReport(query), context.Response, "Charity", query.StartDate, query.EndDate);
                    break;
                case "detail-deposit":
                    if (fileType == "CSV")
                        CSVWriteHelper.WriteCSVToResponse(rpoReport.DetailedDepositReport(query), true, context.Response);
					else if (fileType == "PDF")
						WritePdfToResponse(rpoReport.DetailedDepositReport(query), context.Response, "Detailed Deposit", query.StartDate, query.EndDate, new DetailedDepositReportFormatter());
                    break;
                case "lead":
                    if (fileType == "CSV")
                        CSVWriteHelper.WriteCSVToResponse(rpoReport.LeadReport(query), true, context.Response);
                    else if (fileType == "PDF")
						WritePdfToResponse(rpoReport.LeadReport(query), context.Response, "Lead", query.StartDate, query.EndDate);
                    break;
                case "payment-export-summary":
                    if (fileType == "CSV")
                        CSVWriteHelper.WriteCSVToResponse(rpoReport.PaymentExportSummaryReport(query), true, context.Response);
                    else if (fileType == "PDF")
						WritePdfToResponse(rpoReport.PaymentExportSummaryReport(query), context.Response, "Payment Export Summary", query.StartDate, query.EndDate);
                    break;
                case "paymentreceipt":
                    if (fileType == "CSV")
                        CSVWriteHelper.WriteCSVToResponse(rpoReport.PaymentReceiptReport(query), true, context.Response);
                    else if (fileType == "PDF")
						WritePdfToResponse(rpoReport.PaymentReceiptReport(query), context.Response, "Payment Receipt", query.StartDate, query.EndDate);
                    break;
                case "payment-summary":
                    if (fileType == "CSV")
                        CSVWriteHelper.WriteCSVToResponse(rpoReport.PaymentSummaryReport(query), true, context.Response);
                    else if (fileType == "PDF")
						WritePdfToResponse(rpoReport.PaymentSummaryReport(query), context.Response, "Payment Summary", query.StartDate, query.EndDate);
                    break;
                case "resident-data-import-summary":
                    if (fileType == "CSV")
                        CSVWriteHelper.WriteCSVToResponse(rpoReport.ResidentDataImportSummaryReport(query), true, context.Response);
                    else if (fileType == "PDF")
						WritePdfToResponse(rpoReport.ResidentDataImportSummaryReport(query), context.Response, "Resident Data Import Summary", query.StartDate, query.EndDate);
                    break;
                case "renters-past-due":
                    if (fileType == "CSV")
                        CSVWriteHelper.WriteCSVToResponse(rpoReport.RentersPastDueReport(query), true, context.Response);
                    else if (fileType == "PDF")
						WritePdfToResponse(rpoReport.RentersPastDueReport(query), context.Response, "Renters Past Due", query.StartDate, query.EndDate);
                    break;
                case "transaction-summary":
                    if (fileType == "CSV")
                        CSVWriteHelper.WriteCSVToResponse(rpoReport.TransactionSummaryReport(query), true, context.Response);
                    else if (fileType == "PDF")
						WritePdfToResponse(rpoReport.TransactionSummaryReport(query), context.Response, "Transaction Summary", query.StartDate, query.EndDate);
                    break;
            }
        }

		private void WritePdfToResponse<T>(List<T> rows, HttpResponse Response, string ReportName, DateTime StartDate, DateTime EndDate, IReportFormatter formatter = null)
        {
            Table table = BuildTable(rows, formatter);
            string htmlTable = GetHtmlTableString(table, ReportName, StartDate, EndDate);
            writePDF(htmlTable, Response); 
        }

        private void writePDF(string html, HttpResponse Response)
        {
            MemoryStream output = new MemoryStream();
            Doc theDoc = new Doc();
			theDoc.HtmlOptions.Engine = EngineType.Gecko;

			// apply a rotation transform
			double w = theDoc.MediaBox.Width;
			double h = theDoc.MediaBox.Height;
			double l = theDoc.MediaBox.Left;
			double b = theDoc.MediaBox.Bottom;
			theDoc.Transform.Rotate(90, l, b);
			theDoc.Transform.Translate(w, 0);

			// rotate our rectangle
			theDoc.Rect.Width = h;
			theDoc.Rect.Height = w;
			theDoc.Rect.Inset(10, 10);

			//theDoc.HtmlOptions.BrowserWidth = 0;
			int theID = theDoc.GetInfoInt(theDoc.Root, "Pages");
			theDoc.SetInfo(theID, "/Rotate", "90");

			theID = theDoc.AddImageHtml(html);

			//this block adds a page when needed
			while (true)
			{
				//theDoc.FrameRect(); // add a black border
				if (!theDoc.Chainable(theID))
					break;
				theDoc.Page = theDoc.AddPage();
				theID = theDoc.AddImageToChain(theID);
			}

			//after adding pages, we have to flatten the document
			for (int i = 1; i <= theDoc.PageCount; i++)
			{
				theDoc.PageNumber = i;
				theDoc.Flatten();
			}

            theDoc.Save(output);
            theDoc.Clear();

            output.Position = 0;
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment; filename=data.pdf");
            output.WriteTo(Response.OutputStream);
            Response.Flush();
            Response.Close();
            Response.End();
        }
    
        public Table BuildTable<T>(List<T> Data, IReportFormatter formatter = null)
        {
            //Salcedo - 2/11/2014 - not all of the reports have yet been changed to use IReportFormatter
            if (formatter == null)
                return BuildTable(Data);

            Table ht = new Table();
            ht.Attributes["class"] = "export-table";
            ht.Attributes["cellpadding"] = "0";
            ht.Attributes["cellspacing"] = "0";
            ht.Attributes["width"] = "100%";

            //Get the columns
            TableRow htColumnsRow = new TableHeaderRow();
            htColumnsRow.TableSection = TableRowSection.TableHeader;
            PropertyInfo[] allProperties = typeof(T).GetProperties();
            PropertyInfo[] trimmedPropertiesByFormatter = allProperties.Where(ap => formatter.ColumnFormats.Any(cf => cf.ColumnName == ap.Name)).ToArray();

            trimmedPropertiesByFormatter.Select(prop =>
            {
                ColumnFormat cf = formatter.ColumnFormats.Where(c => c.ColumnName == prop.Name).FirstOrDefault();
                TableHeaderCell htCell = new TableHeaderCell();
                string renamedColumn = String.IsNullOrEmpty(cf.ColumnRename) ? cf.ColumnName : cf.ColumnRename;
                htCell.Text = renamedColumn;
                return htCell;
            }).ToList().ForEach(cell => htColumnsRow.Cells.Add(cell));
            ht.Rows.Add(htColumnsRow);
            //Get the remaining rows
            Data.ForEach(delegate(T obj)
            {
                TableRow htRow = new TableRow();
                obj.GetType().GetProperties().Where(ap => formatter.ColumnFormats.Any(cf => cf.ColumnName == ap.Name)).ToList().ForEach(delegate(PropertyInfo prop)
                {
                    ColumnFormat cf = formatter.ColumnFormats.Where(c => c.ColumnName == prop.Name).FirstOrDefault();
                    object returnedValue = prop.GetValue(obj, null);

                    TableCell htCell = new TableCell();
                    if (returnedValue != null)
                    {
                        string cellText = returnedValue.ToString();
                        if (returnedValue.GetType() == typeof(System.DateTime))
                        {
                            DateTime dt = (DateTime)returnedValue;
                            cellText = dt.ToString(cf.FormatString);
                        }
                        else if (returnedValue.GetType() == typeof(System.Decimal))
                        {
                            System.Decimal d = (System.Decimal)returnedValue;
                            cellText = d.ToString(cf.FormatString);
                        }
                        htCell.Text = HttpUtility.HtmlEncode(cellText);
                        htRow.Cells.Add(htCell);
                    }
                    else
                    {
                        htCell.Text = "";
                        htRow.Cells.Add(htCell);
                    }
                });
                ht.Rows.Add(htRow);
            });
            return ht;
        }

        //Salcedo - 2/11/2014 - old function, to use because not all reports have been modified to use IReportFormatter
        public Table BuildTable<T>(List<T> Data)
        {
            Table ht = new Table();
            ht.Attributes["class"] = "export-table";
            ht.Attributes["cellpadding"] = "0";
            ht.Attributes["cellspacing"] = "0";
            ht.Attributes["width"] = "100%";

            //Get the columns

            TableRow htColumnsRow = new TableHeaderRow();
            htColumnsRow.TableSection = TableRowSection.TableHeader;
            typeof(T).GetProperties().Select(prop =>
            {
                TableHeaderCell htCell = new TableHeaderCell();
                htCell.Text = prop.Name;
                return htCell;
            }).ToList().ForEach(cell => htColumnsRow.Cells.Add(cell));
            ht.Rows.Add(htColumnsRow);
            //Get the remaining rows
            Data.ForEach(delegate(T obj)
            {
                TableRow htRow = new TableRow();
                obj.GetType().GetProperties().ToList().ForEach(delegate(PropertyInfo prop)
                {
                    object returnedValue = prop.GetValue(obj, null);
                    returnedValue = HttpUtility.HtmlEncode(returnedValue);
                    TableCell htCell = new TableCell();
                    if (returnedValue != null)
                    {
                        htCell.Text = returnedValue.ToString();
                        htRow.Cells.Add(htCell);
                    }
                    else
                    {
                        htCell.Text = "";
                        htRow.Cells.Add(htCell);
                    }
                });
                ht.Rows.Add(htRow);
            });
            return ht;
        }

        private string GetHtmlTableString(Table table, string ReportName, DateTime StartDate, DateTime EndDate)
        {
            StringBuilder sb = new StringBuilder();
			sb.Append("<html>");
			sb.Append("<head><meta charset=\"utf-8\" /><title>Report Export Table</title></head>");
			sb.Append("<style type=\"text/css\">");
			sb.Append("h2 {color: #7F4824;font-size:20px;font-weight:bold;}");
			sb.Append("h5 {font-size:14px;font-weight:bold;}.export-table {width: 100%;}");
			sb.Append("td {border-top: 4px solid #FFF;padding: 8px;}");
			sb.Append("th {padding: 8px;}");
			sb.Append("p {margin: 0;}");
			sb.Append("thead {background-color: #77ac42;padding-top: 8px;padding-bottom: 8px;color: #fff;font-weight: normal;text-align:left;}");
			sb.Append("tbody {background-color: #F8F7FA;}");
			sb.Append("s.table-total {background-color: #4E535A;color: #fff;font-size: 18px;font-weight: bold;padding-top: 12px;padding-bottom: 12px;text-align: right;}");
			sb.Append(".centered-last td:last-child, .centered-last th:last-child {text-align: center;}");
			sb.Append(".print-wrapper {width: 100%;}");
			sb.Append(".print-content {margin: 0;position: relative;}");
			sb.Append("html {margin:0;padding:0;}");
			sb.Append("body {font-size: 12pt;color: #000;font-family:sans-serif;margin:0;padding:0;}");
			sb.Append(".print-logo {display: block;float: right;width: 10%;padding-bottom: 10px;}");
			sb.Append(".print-logo img{max-width: 100%;}");
			sb.Append("</style>");
			sb.Append("</head>");
			sb.Append("<body>");
			sb.Append("<div class=\"print-wrapper\"><div class=\"print-content\">");
			sb.Append("<h2>" + ReportName + "</h2>");
			sb.Append("<h5>" + StartDate.ToShortDateString() + " to " + EndDate.ToShortDateString() + "</h5>");
            using (StringWriter sw = new StringWriter())
            {
                table.RenderControl(new HtmlTextWriter(sw));
                sb.Append(sw.ToString());
            }
			sb.Append("</div></div>");
			sb.Append("</body>");
			sb.Append("</html>");
            return sb.ToString();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public void SafeRedirect(string redirectTo)
        {
            SafePageRedirect(redirectTo);
        }

        public static void SafePageRedirect(string redirectTo)
        {
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            HttpContext.Current.Response.Redirect(redirectTo, false);
        }

        private static string GetVerifiedPropertyString(string[] propArray, int PropertyId)
        {
            if (Array.IndexOf(propArray, PropertyId.ToString()) > -1)
                return PropertyId.ToString() + ",";
            else
                return "";
        }
    }
}