﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MissingRPOPayments.aspx.cs" Inherits="Domus.MissingPayments.MissingRPOPayments" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .TitleLabel {
            padding-right: 10px;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>


    <div>
        <h1>Missing Payments</h1>
    </div>

        <div style="width: 400px; text-align: right; float: left;">
            <h2 style="text-align: left">
                <asp:Label ID="RenterNamelbl" runat="server"></asp:Label><br />
                <asp:Label ID="RenterIDlbl" runat="server"></asp:Label>
                <br />
            </h2>
           
            <asp:Label ID="Label5" runat="server" Text="PayerID" CssClass="TitleLabel"></asp:Label><asp:Label ID="PayerIDlbl" runat="server" CssClass="TitleLabel" Width="200px"></asp:Label><br />
           
        <asp:Label ID="Label1" runat="server" Text="Payment Type" CssClass="TitleLabel"></asp:Label>
        <asp:DropDownList ID="PaymentTypeDDL" runat="server" Width="200px">
            <asp:ListItem Value="0">Select</asp:ListItem>
            <asp:ListItem Value="1">Credit Card</asp:ListItem>
            <asp:ListItem Value="2">e-Check</asp:ListItem>
            <asp:ListItem Value="3">RBC</asp:ListItem>
            </asp:DropDownList>
        <br />

                <asp:Label ID="Label2" runat="server" Text="Amount" CssClass="TitleLabel"></asp:Label>
        <asp:TextBox ID="TransAmounttxt" runat="server" Width="200px"></asp:TextBox><br />
                <asp:Label ID="Label3" runat="server" Text="Other Amount" ToolTip="This is for Convenience Fees" CssClass="TitleLabel"></asp:Label>
        <asp:TextBox ID="OtherAmounttxt" runat="server" Width="200px">0.00</asp:TextBox><br />
                <asp:Label ID="Label4" runat="server" Text="Transaction Date" CssClass="TitleLabel"></asp:Label>
        <asp:TextBox ID="TransDatetxt" runat="server" Width="200px"></asp:TextBox><br />
            <asp:Label ID="Label6" runat="server" Text="Merchant TransactionID" CssClass="TitleLabel"></asp:Label><asp:TextBox ID="MerchantTranIDtxt" runat="server" Width="200px"></asp:TextBox><br />
            <asp:Button ID="AddPaymentbtn" runat="server" Text="Add Payment" CausesValidation="False" OnClick="AddPaymentbtn_Click1" /><br />
        
            </div>
        <div style="width: 700px; text-align: right; float: left; margin-left: 20px;">
             <h2 style="text-align: left">
            <asp:Label ID="Label12" runat="server">Renter&#39;s Previous Transactions</asp:Label><br />
        </h2>

            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataSourceID="RenterTransactionsSQL" Font-Size="14px">
                <Columns>
                    <asp:BoundField DataField="RenterId" HeaderText="RenterId" SortExpression="RenterId" Visible="False" />
                    <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" Visible="False" />
                    <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" Visible="False" />
                    <asp:BoundField DataField="TransactionDate" HeaderText="TransactionDate" SortExpression="TransactionDate" />
                    <asp:BoundField DataField="PaymentAmount" HeaderText="Payment" SortExpression="PaymentAmount" />
                    <asp:BoundField DataField="ConvenienceFeeAmount" HeaderText="ConvenienceFee" SortExpression="ConvenienceFeeAmount" />
                    <asp:BoundField DataField="ExternalTransactionId" HeaderText="ExternalTransactionId" SortExpression="ExternalTransactionId" />
                    <asp:BoundField DataField="PaymentStatusId" HeaderText="PaymentStatusId" SortExpression="PaymentStatusId" Visible="False" />
                    <asp:BoundField DataField="PaymentStatusName" HeaderText="PaymentStatusName" SortExpression="PaymentStatusName" />
                </Columns>
             </asp:GridView>


             <asp:SqlDataSource ID="RenterTransactionsSQL" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT        Payment.RenterId, [Transaction].FirstName, [Transaction].LastName, [Transaction].TransactionDate, [Transaction].PaymentAmount, [Transaction].ConvenienceFeeAmount, [Transaction].ExternalTransactionId, 
                         [Transaction].PaymentStatusId, PaymentStatus.PaymentStatusName
FROM            Payment INNER JOIN
                         [Transaction] ON Payment.PaymentId = [Transaction].PaymentId INNER JOIN
                         PaymentStatus ON [Transaction].PaymentStatusId = PaymentStatus.PaymentStatusId
WHERE        (Payment.RenterId = @RenterID) and  [Transaction].TransactionDate &gt; DATEADD(Month, -4, GETDATE())
ORDER BY  [Transaction].TransactionDate">
                 <SelectParameters>
                     <asp:ControlParameter ControlID="GridView1" Name="RenterID" PropertyName="SelectedValue" />
                 </SelectParameters>
             </asp:SqlDataSource>


        </div>
        <div style="clear:both;">

            <asp:Label ID="Statlbl" runat="server"></asp:Label>

        </div>
                <hr />
                
        <div style="width: 1100px; margin-top: 20px;">
            <h2>Missing Payment List</h2>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="RenterID,PropertyID,PayerID" DataSourceID="MissingTransSQL" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" Font-Size="14px">
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="TransactionDate" HeaderText="TransactionDate" SortExpression="TransactionDate" />
                    <asp:BoundField DataField="TransactionAmount" HeaderText="TransactionAmount" SortExpression="TransactionAmount" />
                    <asp:BoundField DataField="RenterPayType" HeaderText="RenterPayType" SortExpression="RenterPayType" />
                    <asp:BoundField DataField="TransactionStatus" HeaderText="TransactionStatus" SortExpression="TransactionStatus" />
                    <asp:BoundField DataField="MerchantTransactionID" HeaderText="MerchantTransactionID" SortExpression="MerchantTransactionID" />
                    <asp:BoundField DataField="PropertyID" HeaderText="PropertyID" ReadOnly="True" SortExpression="PropertyID" Visible="False" />
                    <asp:TemplateField HeaderText="PropertyName" SortExpression="PropertyName">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("PropertyName") %>'></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("PropertyName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
                    <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
                    <asp:BoundField DataField="RenterID" HeaderText="RenterID" SortExpression="RenterID" Visible="False" />
                    <asp:BoundField DataField="PayerID" HeaderText="PayerID" SortExpression="PayerID" Visible="False" />
                    <asp:BoundField DataField="AccountNumber" HeaderText="AccountNumber" ReadOnly="True" SortExpression="AccountNumber" />
                </Columns>
                <RowStyle Height="35px" />
                <SelectedRowStyle BackColor="#FF9900" />
            </asp:GridView>
            <asp:SqlDataSource ID="MissingTransSQL" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="usp_GetMissingCCandACHTransactions" SelectCommandType="StoredProcedure"></asp:SqlDataSource>

        </div>
                            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
