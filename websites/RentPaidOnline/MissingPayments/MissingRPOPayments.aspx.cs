﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework;
using EfxFramework.Helpers;
using System.Data;
using System.Data.SqlClient;

namespace Domus.MissingPayments
{
    public partial class MissingRPOPayments : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Helper.IsRpoAdmin)
                {
                    int userId = EfxFramework.Helpers.Helper.CurrentUserId;
                    bool isUserValidated = false;
                    switch (userId)
                    {
                        case 42:
                            isUserValidated = true;
                            break;
                        //case 12:
                        //    isUserValidated = true;
                        //    break;
                        case 14:
                            isUserValidated = true;
                            break;
                    }


                    if (isUserValidated == false)
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string RenterFirstName = GridView1.SelectedRow.Cells[8].Text.ToString();
            string RenterLastName = GridView1.SelectedRow.Cells[9].Text.ToString();
            string RenterPayType = GridView1.SelectedRow.Cells[3].Text.ToString();
            string TransDate = GridView1.SelectedRow.Cells[1].Text.ToString();
            string TransAmount = GridView1.SelectedRow.Cells[2].Text.ToString();
            string RenterID = GridView1.SelectedDataKey[0].ToString();
            string PayerID = GridView1.SelectedDataKey[2].ToString();
            string MerchTransID = GridView1.SelectedRow.Cells[5].Text.ToString();

            if (RenterPayType == "ACH")
            {
                PaymentTypeDDL.SelectedValue = "2";
            }
            else if (RenterPayType == "CC")
            {
                PaymentTypeDDL.SelectedValue = "1";
            }

            RenterNamelbl.Text = RenterFirstName + " " + RenterLastName;
            RenterIDlbl.Text = RenterID;
            PayerIDlbl.Text = PayerID;
            TransAmounttxt.Text = TransAmount;
            TransDatetxt.Text = TransDate;
            MerchantTranIDtxt.Text = MerchTransID;


        }



        public int InsertMissingPayment(
            int renterID, 
            int payerID, 
            int PaymentTypeID,
            double rentAmount,
            string RentDescription,
            double otherAmount2, 
            string OtherAmount2Description, 
            DateTime TransDate,
            string ResponseMessage)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Payment_SetSingleObject_V2", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PaymentId", SqlDbType.Int).Value = 0;
            com.Parameters.Add("@RenterID", SqlDbType.Int).Value = renterID;
            com.Parameters.Add("@PayerID", SqlDbType.Int).Value = payerID;
            com.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = PaymentTypeID;
            com.Parameters.Add("@BankAccountNumber", SqlDbType.NVarChar, 20).Value = DBNull.Value;
            com.Parameters.Add("@BankRoutingNumber", SqlDbType.NVarChar, 20).Value = DBNull.Value;
            com.Parameters.Add("@CreditCardHolderName", SqlDbType.NVarChar, 200).Value = DBNull.Value;
            com.Parameters.Add("@CreditCardAccountNumber", SqlDbType.NVarChar, 20).Value = DBNull.Value;
            com.Parameters.Add("@CreditCardExpirationMonth", SqlDbType.Int).Value = DBNull.Value;
            com.Parameters.Add("@CreditCardExpirationYear", SqlDbType.Int).Value = DBNull.Value;
            com.Parameters.Add("@RentAmount", SqlDbType.Money).Value = rentAmount;
            com.Parameters.Add("@RentAmountDescription", SqlDbType.NVarChar, 200).Value = RentDescription;
            com.Parameters.Add("@CharityAmount", SqlDbType.Money).Value = 0.00;
            com.Parameters.Add("@CharityName", SqlDbType.NVarChar, 20).Value = DBNull.Value;
            com.Parameters.Add("@OtherAmount1", SqlDbType.Money).Value = 0.00;
            com.Parameters.Add("@OtherDescription1", SqlDbType.NVarChar, 20).Value = DBNull.Value;
            com.Parameters.Add("@OtherAmount2", SqlDbType.Money).Value = otherAmount2;
            com.Parameters.Add("@OtherDescription2", SqlDbType.NVarChar, 200).Value = DBNull.Value;
            com.Parameters.Add("@OtherAmount3", SqlDbType.Money).Value = 0.00;
            com.Parameters.Add("@OtherDescription3", SqlDbType.NVarChar, 20).Value = DBNull.Value;
            com.Parameters.Add("@OtherAmount4", SqlDbType.Money).Value = 0.00;
            com.Parameters.Add("@OtherDescription4", SqlDbType.NVarChar, 20).Value = DBNull.Value;
            com.Parameters.Add("@OtherAmount5", SqlDbType.Money).Value = 0.00;
            com.Parameters.Add("@OtherDescription5", SqlDbType.NVarChar, 20).Value = DBNull.Value;
            com.Parameters.Add("@IsTextMessagePayment", SqlDbType.Bit).Value = 0;
            com.Parameters.Add("@TextMessagePhoneNumber", SqlDbType.NVarChar, 20).Value = DBNull.Value;
            com.Parameters.Add("@CarrierId", SqlDbType.Int).Value = DBNull.Value;
            com.Parameters.Add("@TransactionDateTime", SqlDbType.DateTime).Value = TransDate;
            com.Parameters.Add("@TransactionId", SqlDbType.NVarChar, 20).Value = DBNull.Value;
            com.Parameters.Add("@ResponseCode", SqlDbType.NVarChar, 20).Value = "0";
            com.Parameters.Add("@ResponseMessage", SqlDbType.NVarChar, 200).Value = ResponseMessage;
            com.Parameters.Add("@IsPmsProcessed", SqlDbType.Bit).Value = 0;
            com.Parameters.Add("@PmsBatchId", SqlDbType.NVarChar, 20).Value = "";
            com.Parameters.Add("@PaymentChannelId", SqlDbType.Int).Value = 1;
            com.Parameters.Add("@PaymentStatusId", SqlDbType.Int).Value = 4;
            com.Parameters.Add("@CCAchSettledFlag", SqlDbType.Bit).Value = 1;

            try
            {
                con.Open();
                var _paymentID = com.ExecuteScalar();
                return (int)_paymentID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public void InsertMerchantTransactionID(int PaymentID, string ExtID)
        {
            //usp_Transaction_UpdateExternalMerchantTransID
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Transaction_UpdateExternalMerchantTransID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PaymentId", SqlDbType.Int).Value = PaymentID;
            com.Parameters.Add("@ExternalTransactionId", SqlDbType.NVarChar, 50).Value = ExtID;
            try
            {
                con.Open();
                com.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }



        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }


        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    DateTime DateTest = DateTime.Parse("12/31/2014 09:30:00");
        //    int _paymentIDReturn = InsertMissingPayment(1388, 1228, 2, 333.99, "Rent and Fees", 0.00, null, DateTest, "Successfully Submitted");
        //    Transaction.SetTransactionFromPayment(new Payment(_paymentIDReturn), 0, "Successfully Submitted");
        //}

        protected void AddPaymentbtn_Click1(object sender, EventArgs e)
        {

            if (GridView1.SelectedIndex > -1)
            {
                Statlbl.Text = "";

                //int paymentID = 0;
                int RenterID = int.Parse(RenterIDlbl.Text);
                int PayerID = int.Parse(PayerIDlbl.Text);
                int PaymentType = int.Parse(PaymentTypeDDL.SelectedValue);
                double rentAmount = double.Parse(TransAmounttxt.Text);
                double OtherAmount = double.Parse(OtherAmounttxt.Text);
                string OtherAmountDesc = "";
                string ExtID = MerchantTranIDtxt.Text;
                if (OtherAmount > 0.00)
                {
                    OtherAmountDesc = "Convenience Fee";
                }
                else
                {
                    OtherAmountDesc = "";
                }
                DateTime PayDate = DateTime.Parse(TransDatetxt.Text);

                int _paymentIDReturn = InsertMissingPayment(RenterID, PayerID, 2, rentAmount, "Rent and Fees", OtherAmount, OtherAmountDesc, PayDate, "Successfully Submitted");

                Transaction.SetTransactionFromPayment(new Payment(_paymentIDReturn), 0, "Successfully Submitted");

                InsertMerchantTransactionID(_paymentIDReturn, ExtID);

                GridView1.DataBind();

                Statlbl.Text = "Payment Inserted";
            }
            else
            {
                Statlbl.Text = "Must Select from List";
            }
        }



    }
}