﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework;
using EfxFramework.Helpers;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Domus.PerfCounters
{
    public partial class PerfCounters : System.Web.UI.Page
    {
        PerformanceCounter[] PerfCountersArray = new PerformanceCounter[10];
        SqlConnection connection = new SqlConnection();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Helper.IsRpoAdmin)
                {
                    int userId = EfxFramework.Helpers.Helper.CurrentUserId;
                    bool isUserValidated = false;
                    switch (userId)
                    {
                        case 42:
                            isUserValidated = true;
                            break;
                        //case 12:
                        //    isUserValidated = true;
                        //    break;
                        case 14:
                            isUserValidated = true;
                            break;
                    }


                    if (isUserValidated == false)
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }
            }

            DoMain();
        }

        private void DoMain()
        {
            connection.ConnectionString =
               GetIntegratedSecurityConnectionString();
            SetUpPerformanceCounters();
            //WriteText("Available Performance Counters:");
            

            // Create the connections and display the results.
            CreateConnections();
            //WriteText("Press Enter to finish.");

        }

        private void WriteText(string TheText)
        {
            TextBox1.Text = TextBox1.Text + TheText + "\n";

        }
        private void CreateConnections()
        {
            // List the Performance counters.
            //WritePerformanceCounters();

            // Create 4 connections and display counter information.
            SqlConnection connection1 = new SqlConnection(GetSqlConnectionString());
            connection1.Open();
            //WriteText("Opened the 1st Connection:");
            WritePerformanceCounters();

            //SqlConnection connection2 = new SqlConnection(
            //      GetSqlConnectionStringDifferent());
            //connection2.Open();
            //WriteText("Opened the 2nd Connection:");
            //WritePerformanceCounters();

            //SqlConnection connection3 = new SqlConnection(
            //      GetSqlConnectionString());
            //connection3.Open();
            //WriteText("Opened the 3rd Connection:");
            //WritePerformanceCounters();

            //SqlConnection connection4 = new SqlConnection(
            //      GetSqlConnectionString());
            //connection4.Open();
            //WriteText("Opened the 4th Connection:");
            //WritePerformanceCounters();

            connection1.Close();
            //WriteText("Closed the 1st Connection:");
            //WritePerformanceCounters();

            //connection2.Close();
            //WriteText("Closed the 2nd Connection:");
            //WritePerformanceCounters();

            //connection3.Close();
            //WriteText("Closed the 3rd Connection:");
            //WritePerformanceCounters();

            //connection4.Close();
            //WriteText("Closed the 4th Connection:");
            //WritePerformanceCounters();
        }

        private enum ADO_Net_Performance_Counters
        {
            NumberOfActiveConnectionPools,
            NumberOfReclaimedConnections,
            HardConnectsPerSecond,
            HardDisconnectsPerSecond,
            NumberOfActiveConnectionPoolGroups,
            NumberOfInactiveConnectionPoolGroups,
            NumberOfInactiveConnectionPools,
            NumberOfNonPooledConnections,
            NumberOfPooledConnections,
            NumberOfStasisConnections
            // The following performance counters are more expensive to track.
            // Enable ConnectionPoolPerformanceCounterDetail in your config file.
            //     SoftConnectsPerSecond
            //     SoftDisconnectsPerSecond
            //     NumberOfActiveConnections
            //     NumberOfFreeConnections
        }

        private void SetUpPerformanceCounters()
        {
            connection.Close();
            this.PerfCountersArray = new PerformanceCounter[10];
            string instanceName = GetInstanceName();
            Type apc = typeof(ADO_Net_Performance_Counters);
            int i = 0;
            foreach (string s in Enum.GetNames(apc))
            {
                this.PerfCountersArray[i] = new PerformanceCounter();
                this.PerfCountersArray[i].CategoryName = ".NET Data Provider for SqlServer";
                this.PerfCountersArray[i].CounterName = s;
                this.PerfCountersArray[i].InstanceName = instanceName;
                i++;
            }
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern int GetCurrentProcessId();

        private string GetInstanceName()
        {
            //This works for Winforms apps.
            //string instanceName =
            //    System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
            string instanceName = AppDomain.CurrentDomain.FriendlyName.ToString().Replace('(','[')
             .Replace(')',']').Replace('#','_').Replace('/','_').Replace('\\','_');

            // Must replace special characters like (, ), #, /, \\
            string instanceName2 =
                AppDomain.CurrentDomain.FriendlyName.ToString().Replace('(', '[')
                .Replace(')', ']').Replace('#', '_').Replace('/', '_').Replace('\\', '_');

            // For ASP.NET applications your instanceName will be your CurrentDomain's 
            // FriendlyName. Replace the line above that sets the instanceName with this:
            // instanceName = AppDomain.CurrentDomain.FriendlyName.ToString().Replace('(','[')
            // .Replace(')',']').Replace('#','_').Replace('/','_').Replace('\\','_');

            string pid = GetCurrentProcessId().ToString();
            instanceName = instanceName + "[" + pid + "]";
            WriteText("Instance Name: " + instanceName);
            WriteText("---------------------------");
            return instanceName;
        }

        private void WritePerformanceCounters()
        {
            WriteText(DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());

            WriteText("---------------------------");
            foreach (PerformanceCounter p in this.PerfCountersArray)
            {
                WriteText( p.CounterName + " = " + p.NextValue());
            }
            WriteText("---------------------------");
        }

        private static string GetIntegratedSecurityConnectionString()
        {
            // To avoid storing the connection string in your code,
            // you can retrive it from a configuration file.
            //return @"Data Source=.\SqlExpress;Integrated Security=True;" +
            //  "Initial Catalog=AdventureWorks";
            return @"Server=10.10.50.45,1433;Database=EFX;User ID=RPOUser;Password=2AC8E094DEBD";
        }
        private static string GetSqlConnectionString()
        {
            // To avoid storing the connection string in your code,
            // you can retrive it from a configuration file.
            //return @"Data Source=.\SqlExpress;User Id=LowPriv;Password=Data!05;" +
            //  "Initial Catalog=AdventureWorks";
            //return @"Server=10.10.50.45,1433;Database=EFX;User ID=RPOUser;Password=2AC8E094DEBD";
            return EfxSettings.ConnectionString;
        }

        private static string GetSqlConnectionStringDifferent()
        {
            // To avoid storing the connection string in your code,
            // you can retrive it from a configuration file.
            return @"Server=10.10.50.45,1433;Database=EFX;User ID=RPOUser;Password=2AC8E094DEBD;MultipleActiveResultSets=True;App=EntityFramework";
        }

    }
}