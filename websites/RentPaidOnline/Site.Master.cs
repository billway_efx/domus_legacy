﻿using EfxFramework;
using EfxFramework.Web.Facade;
using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace Domus
{
	public partial class SiteMaster : MasterPage
	{
		private const string AntiXsrfTokenKey = "__AntiXsrfToken";
		private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
		private string _AntiXsrfTokenValue;

		protected void Page_Init(object sender, EventArgs e)
		{
            try
            {
                // The code below helps to protect against XSRF attacks
                var RequestCookie = Request.Cookies[AntiXsrfTokenKey];
                Guid RequestCookieGuidValue;
                if (RequestCookie != null && Guid.TryParse(RequestCookie.Value, out RequestCookieGuidValue))
                {
                    // Use the Anti-XSRF token from the cookie
                    _AntiXsrfTokenValue = RequestCookie.Value;
                    Page.ViewStateUserKey = _AntiXsrfTokenValue;
                }
                else
                {
                    // Generate a new Anti-XSRF token and save to the cookie
                    _AntiXsrfTokenValue = Guid.NewGuid().ToString("N");
                    Page.ViewStateUserKey = _AntiXsrfTokenValue;

                    var ResponseCookie = new HttpCookie(AntiXsrfTokenKey)
                    {
                        HttpOnly = true,
                        Value = _AntiXsrfTokenValue
                    };
                    if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                    {
                        ResponseCookie.Secure = true;
                    }
                    Response.Cookies.Set(ResponseCookie);
                }

                Page.PreLoad += MasterPagePreLoad;
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
		}

		protected void MasterPagePreLoad(object sender, EventArgs e)
		{
            try
            {
                if (!IsPostBack)
                {
                    // Set Anti-XSRF token
                    ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                    ViewState[AntiXsrfUserNameKey] = UIFacade.GetCurrentUser(UserType.EfxAdministrator) != null ? UIFacade.GetCurrentUser(UserType.EfxAdministrator).UserName : String.Empty;
                }
                else
                {
                    // Validate the Anti-XSRF token
                    if ((string)ViewState[AntiXsrfTokenKey] != _AntiXsrfTokenValue
                        || (string)ViewState[AntiXsrfUserNameKey] != (UIFacade.GetCurrentUser(UserType.EfxAdministrator) != null ? UIFacade.GetCurrentUser(UserType.EfxAdministrator).UserName : string.Empty))
                    {
                        throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                    }
                }
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
            try
			{
				//Hide the navigation if the user is viewing the login screen
				if (string.Equals(Request.Url.AbsolutePath, "/Account/Login.aspx", StringComparison.OrdinalIgnoreCase))
				{
					Navigation.Visible = false;
					Spacer.Visible = true;
				}
				else
				{
					Navigation.Visible = true;
					Spacer.Visible = false;
				}
            }
            catch
            {
                Response.Redirect("~/ErrorPage.html", false);
            }
		}
	}
}