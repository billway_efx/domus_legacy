﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frontend.master" AutoEventWireup="true" CodeBehind="bootstrap.aspx.cs" Inherits="Domus.bootstrap" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ContentPlaceHolderID="TopHeadline" runat="server">
	<h1>Bootstrap Styles</h1>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="main-container">
	<h1>Style all this stuff
		<small>(including this h1)</small>
	</h1>
	<p><strong>Strong pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Emphasis aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>code commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Link donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>
	<hr /><br />
	<ul class="nav nav-tabs">
		<li class="active"><a href="#typography" data-toggle="tab">Typography</a></li>
		<li><a href="#forms" data-toggle="tab">Forms</a></li>
		<li><a href="#components" data-toggle="tab">Components</a></li>
		<li class="disabled"><a href="#disabled" data-toggle="tab">Disabled</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="typography">
			<h2>Header 2 Tag</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget<sub>Sub</sub> erat risus, vel accumsan<sup>Sup</sup> augue. Mauris odio nulla, eleifend sit amet <ins>insert placerat</ins> non, mattis a turpis. Proin <del>deleted lacus</del> urna, pulvinar quis molestie nec, elementum in tortor. Sed consectetur orci neque, ac hendrerit purus. Maecenas fringilla hendrerit nisi. Sed tortor lorem, pellentesque sed suscipit at, suscipit in mauris. Donec rutrum pharetra hendrerit. Ut ultrices, sapien nec facilisis imperdiet, arcu neque suscipit dui, non posuere ipsum nulla in ante. Ut vitae erat lectus.</p>
			<h3>Header 3 Tag</h3>
			<ol>
				<li>Ordered list lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            <ul>
				<li>Nested sunt nemo magni ratione sed natus sit.</li>
				<li>Sit aspernatur laudantium ratione magni sunt consequuntur.</li>
				<li>Odit qui quia laudantium ratione magni.</li>
			</ul>
				</li>
				<li>Odit qui quia laudantium ratione magni.
            <ol>
				<li>Nested odit qui quia laudantium ratione magni.</li>
				<li>Erro architecto laudantium ratione omnis sed quasi inventore inventore, doloremque.</li>
				<li>Magni sit et quia sit dolores vitae vitae, totam ratione.</li>
			</ol>
				</li>
				<li>Aliquam tincidunt mauris eu risus.</li>
				<li>Sit aspernatur laudantium ratione magni sunt consequuntur.</li>
				<li>Magni sit et quia sit dolores vitae vitae, totam ratione.</li>
			</ol>
			<blockquote>
				<p>Blockquote lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p>
			</blockquote>
			<h4>Header 4 Tag</h4>
			<ul>
				<li>Unrdered list lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            <ol>
				<li>Nested sunt nemo magni ratione sed natus sit.</li>
				<li>Sit aspernatur laudantium ratione magni sunt consequuntur.</li>
				<li>Odit qui quia laudantium ratione magni.</li>
			</ol>
				</li>
				<li>Odit qui quia laudantium ratione magni.
            <ul>
				<li>Nested odit qui quia laudantium ratione magni.</li>
				<li>Erro architecto laudantium ratione omnis sed quasi inventore inventore, doloremque.</li>
				<li>Magni sit et quia sit dolores vitae vitae, totam ratione.</li>
			</ul>
				</li>
				<li>Aliquam tincidunt mauris eu risus.</li>
				<li>Sit aspernatur laudantium ratione magni sunt consequuntur.</li>
				<li>Magni sit et quia sit dolores vitae vitae, totam ratione.</li>
			</ul>
			<pre><code>#example pre code { 
	display: block; 
	width: 300px; 
	height: 80px; 
}</code></pre>
			<h5>Header 5 Tag</h5>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p>
			<h6>Header 6 Tag</h6>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p>
		</div>
		<div class="tab-pane" id="forms">
			<br />
			<ul class="nav nav-pills">
				<li class="active"><a href="#default" data-toggle="tab">Default</a></li>
				<li><a href="#fieldset" data-toggle="tab">Fieldset</a></li>
				<li><a href="#error" data-toggle="tab">Error</a></li>
				<li><a href="#success" data-toggle="tab">Success</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="default">
					<div class="alert alert-info">Here are the typical form elements for styling.</div>
					<div class="formWrapper">
						<div class="formHalf">
							<label for="first">First Name</label>
							<input type="text" id="first" class="form-control">
						</div>
						<div class="formHalf">
							<label for="last">Last Name</label>
							<input type="text" id="last" class="form-control">
						</div>
						<div class="formHalf">
							<label for="select">Select Box</label>
							<select id="select" class="form-control">
								<option>First option</option>
								<optgroup label="Optgroup">
									<option>Nested option</option>
									<option>Nested option</option>
									<option>Nested option</option>
								</optgroup>
								<option>Second option</option>
								<option>Third option</option>
								<option>Fourth option</option>
							</select>
						</div>
						<div class="clear"></div>
						<div class="formWhole">
							<label for="long">Long field</label>
							<input type="text" id="long" class="form-control">
						</div>
						<div class="formHalf">
							<label>Some Checkboxes</label>
							<ul class="list-unstyled checkbox">
								<li>
									<input type="checkbox" name="checkboxes" id="this">
									<label for="this">This</label>
								</li>
								<li>
									<input type="checkbox" name="checkboxes" id="that">
									<label for="that">That</label>
								</li>
								<li>
									<input type="checkbox" name="checkboxes" id="other">
									<label for="other">The Other</label>
								</li>
							</ul>
						</div>
						<div class="formHalf">
							<label>Some Radios</label>
							<ul class="list-unstyled radio">
								<li>
									<input type="radio" name="radios" id="this1">
									<label for="this1">This</label>
								</li>
								<li>
									<input type="radio" name="radios" id="that1">
									<label for="that1">That</label>
								</li>
								<li>
									<input type="radio" name="radios" id="other1">
									<label for="other1">The Other</label>
								</li>
							</ul>
						</div>
						<div class="formWhole">
							<label for="textarea">Textarea</label>
							<textarea id="textarea" class="form-control" cols="150" rows="4"></textarea>
						</div>
						<a href="#" class="btn btn-primary">Submit</a>
						<a href="#" class="btn">Cancel</a>
					</div>
				</div>
				<div class="tab-pane" id="fieldset">
					<div class="alert alert-warning">
						<a class="close" data-dismiss="alert">&times;</a>
						<strong>Warning!</strong> Fieldset styles are not fully supported in Corestrap.
					</div>
					<div class="formWrapper">
						<fieldset>
							<legend>Legend Here</legend>
							<div class="formHalf">
								<label for="first2">First Name</label>
								<input type="text" id="first2" class="form-control">
							</div>
							<div class="formHalf">
								<label for="last2">Last Name</label>
								<input type="text" id="last2" class="form-control">
							</div>
							<div class="formHalf">
								<label for="select2">Select Box</label>
								<select id="select2" class="form-control">
									<option>First option</option>
									<optgroup label="Optgroup">
										<option>Nested option</option>
										<option>Nested option</option>
										<option>Nested option</option>
									</optgroup>
									<option>Second option</option>
									<option>Third option</option>
									<option>Fourth option</option>
								</select>
							</div>
							<div class="clear"></div>
							<div class="formWhole">
								<label for="long2">Long field</label>
								<input type="text" id="long2" class="form-control">
							</div>
							<div class="formHalf">
								<label>Some Checkboxes</label>
								<ul class="list-unstyled checkbox">
									<li>
										<input type="checkbox" name="checkboxes" id="this2">
										<label for="this2">This</label>
									</li>
									<li>
										<input type="checkbox" name="checkboxes" id="that2">
										<label for="that2">That</label>
									</li>
									<li>
										<input type="checkbox" name="checkboxes" id="other2">
										<label for="other2">The Other</label>
									</li>
								</ul>
							</div>
							<div class="formHalf">
								<label>Some Radios</label>
								<ul class="list-unstyled radio">
									<li>
										<input type="radio" name="radios" id="this21">
										<label for="this21">This</label>
									</li>
									<li>
										<input type="radio" name="radios" id="that21">
										<label for="that21">That</label>
									</li>
									<li>
										<input type="radio" name="radios" id="other21">
										<label for="other21">The Other</label>
									</li>
								</ul>
							</div>
							<div class="formWhole">
								<label for="textarea2">Textarea</label>
								<textarea id="textarea2" class="form-control" cols="150" rows="4"></textarea>
							</div>
							<a href="#" class="btn btn-primary">Submit</a>
							<a href="#" class="btn">Cancel</a>
						</fieldset>
					</div>
				</div>
				<div class="tab-pane" id="error">
					<div class="alert alert-danger"><strong>Error!</strong> The following errors should be styled.</div>
					<div class="formWrapper">
						<div class="formHalf errorWrapper has-error">
							<label for="first3">First Name<span class="asterisk">*</span></label>
							<input type="text" id="first3" class="form-control error">
							<span style="display: inline;" class="validator requiredValidator">
								<span class="errorMessage">
									<span class="asterisk">*</span><span class="errorText">First Name is required.</span>
								</span>
							</span>
						</div>
						<div class="formHalf errorWrapper has-error">
							<label for="last3">Last Name<span class="asterisk">*</span></label>
							<input type="text" id="last3" class="form-control error">
							<span style="display: inline;" class="validator requiredValidator">
								<span class="errorMessage">
									<span class="asterisk">*</span><span class="errorText">First Name is required.</span>
								</span>
							</span>
						</div>
						<div class="formHalf errorWrapper has-error">
							<label for="select3">Select Box</label>
							<select id="select3" class="form-control error">
								<option>First option</option>
								<optgroup label="Optgroup">
									<option>Nested option</option>
									<option>Nested option</option>
									<option>Nested option</option>
								</optgroup>
								<option>Second option</option>
								<option>Third option</option>
								<option>Fourth option</option>
							</select>
							<span style="display: inline;" class="validator requiredValidator">
								<span class="errorMessage">
									<span class="asterisk">*</span><span class="errorText">Why didn't you pick me?</span>
								</span>
							</span>
						</div>
						<div class="clear"></div>
						<div class="formWhole errorWrapper has-error">
							<label for="long3">Long field</label>
							<input type="text" id="long3" class="form-control error">
							<span style="display: inline;" class="validator requiredValidator">
								<span class="errorMessage">
									<span class="asterisk">*</span><span class="errorText">You didn't make use of the character limit.</span>
								</span>
							</span>
						</div>
						<div class="formHalf errorWrapper has-error">
							<label>Some Checkboxes</label>
							<ul class="list-unstyled checkbox">
								<li>
									<input type="checkbox" name="checkboxes" id="this3">
									<label for="this3">This</label>
								</li>
								<li>
									<input type="checkbox" name="checkboxes" id="that3">
									<label for="that3">That</label>
								</li>
								<li>
									<input type="checkbox" name="checkboxes" id="other3">
									<label for="other3">The Other</label>
								</li>
							</ul>
							<span style="display: inline;" class="validator requiredValidator">
								<span class="errorMessage">
									<span class="asterisk">*</span><span class="errorText">This will never work out.</span>
								</span>
							</span>
						</div>
						<div class="formHalf errorWrapper has-error">
							<label>Some Radios</label>
							<ul class="list-unstyled radio">
								<li>
									<input type="radio" name="radios" id="this31">
									<label for="this31">This</label>
								</li>
								<li>
									<input type="radio" name="radios" id="that31">
									<label for="that31">That</label>
								</li>
								<li>
									<input type="radio" name="radios" id="other31">
									<label for="other31">The Other</label>
								</li>
							</ul>
							<span style="display: inline;" class="validator requiredValidator">
								<span class="errorMessage">
									<span class="asterisk">*</span><span class="errorText">Why can't you have all three?</span>
								</span>
							</span>
						</div>
						<div class="formWhole errorWrapper has-error">
							<label for="textarea3">Textarea</label>
							<textarea class="form-control error" id="textarea3" cols="150" rows="4"></textarea>
							<span style="display: inline;" class="validator requiredValidator">
								<span class="errorMessage">
									<span class="asterisk">*</span><span class="errorText">Your free response was too free.</span>
								</span>
							</span>
						</div>
						<a href="#" class="btn btn-primary">Submit</a>
						<a href="#" class="btn">Cancel</a>
					</div>
				</div>
				<div class="tab-pane" id="success">
					<div class="alert alert-success"><strong>Success!</strong> You've finished your form styling!</div>
					<p>There aren't any fields here because you already succeeded!</p>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="components">
			<div class="well">
				I'm in a well! ... Help?
			</div>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Text colors</th>
						<th>Labels</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><span class="muted">Some muted text: Wes anderson ugh pickled, try-hard iphone</span></td>
						<td><span class="label">Default</span></td>
					</tr>
					<tr>
						<td><span class="text-success">Some success text: ethical street art mustache jean shorts fanny pack</span></td>
						<td><span class="label label-success">Success</span></td>
					</tr>
					<tr>
						<td><span class="text-danger">Some warning text: dreamcatcher carles Austin before they sold out</span></td>
						<td><span class="label label-danger">Warning</span></td>
					</tr>
					<tr>
						<td><span class="text-error">Some error text: synth fap skateboard semiotics, keffiyeh meh mustache</span></td>
						<td><span class="label label-important">Important</span></td>
					</tr>
					<tr>
						<td><span class="text-info">Some info text: whatever banjo pour-over, shoreditch flannel</span></td>
						<td><span class="label label-info">Info</span></td>
					</tr>
					<tr class="success">
						<td colspan="2">Checkout</td>
					</tr>
					<tr class="error">
						<td colspan="2">These</td>
					</tr>
					<tr class="warning">
						<td colspan="2">Pretty</td>
					</tr>
					<tr class="info">
						<td colspan="2">Colors</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="tab-pane" id="disabled">
			<h3>I'm not sure how you got here...</h3>
			<p>You win the prize!</p>
		</div>
	</div>
</div>
</asp:Content>