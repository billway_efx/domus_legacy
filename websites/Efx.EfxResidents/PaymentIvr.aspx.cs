﻿using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Presenters.Pages;
using Mb2x.ExtensionMethods;
using System;
using System.Web;

namespace Efx.EfxResidents
{
    public partial class PaymentIvr : System.Web.UI.Page, IPaymentIvr
    {
        private PaymentIvrPresenter _Presenter;

        public int UserId
        {
            get { return !string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? HttpContext.Current.User.Identity.Name.ToInt32() : 0; }
        }
        public string PaymentQueueNumber
        {
            get { return !string.IsNullOrEmpty(Request.QueryString["PaymentQueueNumber"]) ? Request.QueryString["PaymentQueueNumber"] : string.Empty; }
            set { PaymentQueueNumberLabel.Text = value; }
        }
        public string RentAmount { set { RentAmountLabel.Text = value; } }
        public string FeeAmount { set { ConvenienceFeeLabel.Text = value; } }
        public string TotalPayment { set { TotalPaymentAmountLabel.Text = value; } }
        public string PropertyName { set { PropertyNameLabel.Text = value; } }
        public string PropertyAddress { set { PropertyAddressLabel.Text = value; } }
        public string PropertyCityStateZip { set { PropertyCityStateZipLabel.Text = value; } }
        public string FirstName { set { FirstNameLabel.Text = value; } }
        public string MiddleName { set { MiddleNameLabel.Text = value; } }
        public string LastName { set { LastNameLabel.Text = value; } }
        public string Suffix { set { SuffixLabel.Text = value; } }
        public string BillingAddress { set { BillingAddressLabel.Text = value; } }
        public string BillingCityStateZip { set { BillingCityStateZipLabel.Text = value; } }
        public string Phone { set { PhoneNumberLabel.Text = value; } }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new PaymentIvrPresenter(this);

            if (!Page.IsPostBack)
            {
                _Presenter.InitializeValues();

                //Salcedo - 11/25/2013 - sorry for breaking the abstraction layering, but I didn't have time to mess with it
                //Salcedo - 11/25/2013 - add the PIN number to the confirmation page
                var CurrentPaymentQueue = EfxFramework.PaymentQueue.GetPaymentQueueByPaymentQueueNumber(PaymentQueueNumber);
                var CurrentRenter = new EfxFramework.Renter(CurrentPaymentQueue.RenterId);
                var CurrentPin = CurrentRenter.AccountCodePin;
                lblPin.Text = CurrentPin;

            }
        }
    }
}