﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConfirmScheduledPayment.aspx.cs" 
    Inherits="Efx.EfxResidents.ConfirmScheduledPayment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pageContainer scheduledPaymentPage">
        <div class="pageTitle"><h2>Recurring Credit Card Authorization</h2></div>
        <div class="singleItemSection">

<%--            <div class="standardPadding">
                <h3>Payment Information</h3>
            </div>--%>

            <div style="padding: 50px;">

                <h4>Please verify your payment information and enter your card's CVV code</h4>
                <br />

                <div class="formRow rowSpacingAdjust">
                    <div class="formFullWidth">
                        <span class="formInlineLable">First Name</span>
                        <p><asp:Label ID="FirstName" runat="server" Text="FirstName"/></p>
                    </div>                            
                </div>

                <div class="formRow rowSpacingAdjust">
                    <div class="formFullWidth">
                        <span class="formInlineLable">Last Name</span>
                        <p><asp:Label ID="LastName" runat="server" Text="LastName"/></p>
                    </div>                            
                </div>

                <div class="formRow rowSpacingAdjust">
                    <div class="formFullWidth">
                        <span class="formInlineLable">Billing Address</span>
                        <p><asp:Label ID="BillingAddress" runat="server" Text="Billing Address"/></p>
                    </div>                            
                </div>

                <div class="formRow rowSpacingAdjust">
                    <div class="formFullWidth">
                        <span class="formInlineLable">City</span>
                        <p><asp:Label ID="City" runat="server" Text="City"/></p>
                    </div>                            
                </div>

                <div class="formRow">
                    <div class="formQuarterWidth">
                    <div class="formHalfWidth">
                        <span class="formInlineLable">State</span>
                        <p><asp:Label ID="State" runat="server" Text="State"/></p>
                    </div>                            
                    <div class="formHalfWidth">
                        <span class="formInlineLable">Zip</span>
                        <p><asp:Label ID="Zip" runat="server" Text="Zip code"/></p>
                    </div>                     
                    </div>       
                </div>

                <div class="formRow rowSpacingAdjust">
                    <div class="formFullWidth">
                        <span class="formInlineLable">Property</span>
                        <p><asp:Label ID="PropertyName" runat="server" Text="Property Name"/></p>
                    </div>                            
                </div>

                <div class="formRow rowSpacingAdjust">
                    <div class="formFullWidth">
                        <span class="formInlineLable">Name On Card</span>
                        <p><asp:Label ID="NameOnCard" runat="server" Text="Name On Card"/></p>
                    </div>                            
                </div>

                <div class="formRow rowSpacingAdjust">
                    <div class="formFullWidth">
                        <span class="formInlineLable">Card Number</span>
                        <p><asp:Label ID="CardNumber" runat="server" Text="************1111"/></p>
                    </div>                            
                </div>

                <div class="formRow rowSpacingAdjust">
                    <div class="formFullWidth">
                        <span class="formInlineLable">Expiration Date</span>
                        <p><asp:Label ID="ExpirationDate" runat="server" Text="99/99"/></p>
                    </div>                            
                </div>

                <div class="formRow rowSpacingAdjust">
                    <div class="formFullWidth">
                        <span class="formInlineLable">Total Payment Amount</span>
                        <p><asp:Label ID="TotalPaymentAmount" runat="server" Text="$1,1234.25"/></p>
                    </div>                            
                </div>

                <div class="formRow rowSpacingAdjust">
                    <div class="formQuarterWidth">
                        <span class="formInlineLable">CVV Code</span>
                        <p><asp:TextBox ID="CVVCode" runat="server" ReadOnly="False" Text="123"></asp:TextBox></p>
                    </div>                            
                </div>

                <div class="bottomElements">
                    <div class="formFullWidth">
                        <div class="bottomButtonHolder">
                            <asp:Button ID="Cancel" runat="server" Text="Cancel" CssClass="orangeButton saveButton cancel-btn" OnClick="Cancel_Click" />
                            &nbsp;&nbsp;
                            <asp:Button ID="AuthorizeButton" runat="server" Text="Authorize" CssClass="orangeButton saveButton save-btn" OnClick="AuthorizeButton_Click" />
                        </div>
                    </div>
                </div>       

            </div>
        </div>
    </div>
    
<asp:CustomValidator        ID="RequiredCvvValidation" 
                            runat="server" 
                            Display="None" 
                            ErrorMessage="Your CVV Code is Required" />

        
</asp:Content>


