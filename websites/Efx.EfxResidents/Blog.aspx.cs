﻿using System;
using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.Pages.BlogPages;
using EfxFramework.ViewPresentation.Interfaces.UserControls.Blogs;
using EfxFramework.ViewPresentation.Presenters.Factories;
using EfxFramework.ViewPresentation.Presenters.Pages.BlogPresenters;
using EfxFramework.Web;

namespace Efx.EfxResidents
{
    public partial class Blog : BasePageV2, IPublicResidentsRoom
    {
        private BlogPresenterBase _Presenter;
        public IBlogView BlogViewUserControl { get { return BlogView; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = BlogPresenterFactory.GetBlogPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}