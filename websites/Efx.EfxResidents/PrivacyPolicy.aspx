﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrivacyPolicy.aspx.cs" Inherits="Efx.EfxResidents.PrivacyPolicy" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="pageContainer privacyPolicyPage">
        <div class="termsAndConditionsContainer">
        <h2 class="termsAndConditionsTitle">Terms of Use and Conditions</h2>
        <article class="fullrow">
                <p>RentPaidOnline a company of EFX Financial Services Inc. respects the privacy of every individual who visits our web site. In general, 
                    you can visit RentPaidOnline without telling us who you are or revealing any information about yourself. RentPaidOnline will not collect 
                    any personal information about you by means of this site unless you have provided such personal data to us. If you do not want your 
                    personal data collected, please don’t submit it. When you engage in certain activities on RentPaidOnline.com, such as requesting information, 
                    electronic payments, and marketing we will ask you to provide certain information about yourself by filling out and submitting an online form. 
                    It is completely optional for you to engage in these activities. If you elect to engage in these activities, however, RentPaidOnline will 
                    require that you provide your name, mailing address, e-mail address, and other personal information. RentPaidOnline also uses information that 
                    you provide as part of our effort to keep you informed about product upgrades, special offers, and other RentPaidOnline services. 
                    RentPaidOnline does not share any information with third-party marketers. The information we collect is for RentPaidOnline internal use only.</p>
            </article>
        </div>
    </div>
</asp:Content>
