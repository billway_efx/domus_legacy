﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Payments.aspx.cs" Inherits="Efx.EfxResidents.Payments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <div id="pageCenterContainer" class="pageContainer paymentPage residentsPage"> 
        <div class="pageTitle" style="border-bottom: 0px;">
            <h2 style="color:#0462c2  !important; ">Payments</h2>
        </div>
        <!--Main Tabs Section -->
        <div class="mainTabSection fr2BorderColor">
            <div class="tabs fr2BorderColor">
                <span class="" ><i class="fa fa-dollar fr2Color fr2BorderColor"></i>Payments</span>
                <span class="" style="border: 1px solid #06cef9;"><i class="fa fa-history fr2Color"></i>Payment History</span>
            </div>
            <!--this is the main container for all the panels on this page, a panel is each man tabbing section -->
            <div id="idPayment" class="panel_container fr2BorderColor" style="height: 991px; ">
                <!--this is the start of the multiple panels -->
                <div id="paymentPanels" class="panels" style="width: 1944px; left: 0px;">
                    <!--this is the first panel (Payments)-->
                    <div id="paymentPanel1" class="panel" style="width: 972px; left: 0px;">
                        <div class="panel_content">

                            <asp:Literal runat="server" ID="uxNoPaymentAcceptedLiteral" Text="<h3 class='standardPadding'>Please contact your property manager.</h3>"></asp:Literal>
                            <asp:Panel runat="server" ID="uxPaymentPanel">
                                <div class="standardPadding">
                                    <asp:ValidationSummary ID="PaymentSummary" DisplayMode="List" CssClass="alert alert-danger" runat="server" />
                                    <h3 class="fr1Color">Payment Information</h3>
                                    <div class="mobileTotalDue">
                                        <span class="totalTitle " style="color:black !important;">Total:</span>
                                        <span class="totalAmount">
                                            <asp:Label ID="TotalAmountLabel" CssClass="fr1Color" runat="server" />
                                        </span>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="mobileRequiredText">
                                        <p class="frBlack">* is required</p>
                                    </div>
                                    <div class="paymentRow" id="accordion">
                                        <div class="paymentFormColumn accordion-panel">
                                            <h4 data-toggle="collapse" data-target="#myInfo" data-parent="#accordion" class="required mobile-only fr2Color">
                                                <div class="stepsCircle">1</div>
                                                My Info <i class="pull-right fa fa-plus"></i>
                                                <h4></h4>
                                                <div id="myInfo" class="collapse">
                                                    <uc1:PersonalInformation ID="PersonalInformation" runat="server" />
                                                </div>
                                                <h4 class="required mobile-only fr2Color" data-parent="#accordion" data-target="#paymentMethod" data-toggle="collapse">
                                                    <div class="stepsCircle">
                                                        2</div>
                                                    Payment Method <i class="pull-right fa fa-plus"></i>
                                                    <h4></h4>
                                                    <!--Payment Method -->
                                                    <div id="paymentMethod" class="paymentBlockSection collapse">
                                                        <asp:Literal ID="uxPaymentInformationLiteral" runat="server" Visible="false"></asp:Literal>
                                                        <uc1:SelectPaymentType ID="SelectPaymentType" runat="server" IsApplicant="False" />
                                                    </div>
                                                    <h4></h4>
                                                    <h4></h4>
                                                    <h4></h4>
                                                    <h4></h4>
                                                    <h4></h4>
                                                    <h4></h4>
                                                    <h4></h4>
                                                    <h4></h4>
                                                </h4>
                                            </h4>
                                        </div>

                                        <div class="paymentFormColumn accordion-panel">
                                            <h4 data-toggle="collapse" data-target="#billingAdd" data-parent="#accordion" class="required mobile-only fr2Color">
                                                <div class="stepsCircle">3</div>
                                                Billing Address <i class="pull-right fa fa-plus"></i>
                                                <h4></h4>
                                                <!--Billing Address-->
                                                <div id="billingAdd" class="paymentBlockSection collapse">
                                                    <uc1:ResidentBillingInformation ID="ResidentBillingInformation" runat="server" />
                                                </div>
                                                <h4 class="required mobile-only fr2Color" data-parent="#accordion" data-target="#totalPay" data-toggle="collapse">
                                                    <div class="stepsCircle">
                                                        4</div>
                                                    Total Payment Amount <i class="pull-right fa fa-plus"></i>
                                                    <h4></h4>
                                                    <!--Payment Totals-->
                                                    <div id="totalPay" class="totalPaymentAmount collapse">
                                                        <uc1:PaymentAmounts ID="PaymentAmounts" runat="server" />
                                                    </div>
                                                    <h4></h4>
                                                    <h4></h4>
                                                    <h4></h4>
                                                    <h4></h4>
                                                    <h4></h4>
                                                    <h4></h4>
                                                    <h4></h4>
                                                    <h4></h4>
                                                </h4>
                                            </h4>
                                        </div>
                                        <div class="paymentFormColumn accordion-panel">
                                            <h4 data-toggle="collapse" data-target="#payOpt" data-parent="#accordion" class="mobile-only fr2Color">
                                                <div class="stepsCircle">5</div>
                                                Payment Options <span class="smallText">(optional)</span> <i class="pull-right fa fa-plus"></i>
                                                <h4></h4>
                                                <div id="payOpt" class="paymentFormColumn collapse">
                                                    <h4 class="fr2Color">Additional Payment Options</h4>
                                                    <div class="autoPayContainer">
                                                        <uc1:AutoPayTile ID="AutoPayTile" runat="server" />
                                                    </div>
                                                    <!--CMallory - Task 00554 - Added ID to OrBlock to hide it dependin gon Pay By Text value -->
                                                    <div class="orBlock" id="OrBlock" runat="server">
                                                        <span class="fr1Color">Or</span>
                                                    </div>
                                                    <!-- CMallory - Added margin-bottom to following div to prevent overlapping text when Pay Now Button is clicked -->
                                                    <div class="textToPayContainer" id="TextToPayContainer" runat="server" style="margin-bottom:17%;">
                                                        <uc1:TextToPayTile ID="TextToPayTile" runat="server" />
                                                    </div>
                                                </div>
                                                <h4></h4>
                                                <h4></h4>
                                                <h4></h4>
                                                <h4></h4>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                <div id="pay-now-box" class="bottomElements pay-now-box" style="bottom: 2px;">
                                    <div class="formFullWidth">
                                        <div>
                                        <%-- cakel: added Upanel --%>
                                         <asp:UpdatePanel ID="UpdatePanel1" class="acceptTOC" runat="server">
                                            <ContentTemplate>

                                         
                                        <asp:CheckBox ID="TermsCheckBox" runat="server" CssClass="checkBoxText checkboxTerms" OnCheckedChanged="TermsCheckBox_CheckedChanged" AutoPostBack="true" />
                                        <!--cakel: BUGID00045 commented out below control for modal popup -->
                                        <asp:HyperLink ID="HyperLink1" CssClass="frBlueLink" runat="server" Target="_blank" NavigateUrl="~/TermsAndConditions_RentPaidOnline.html" ToolTip="view terms and conditions of RentPaidOnline">Accept Terms and Conditions</asp:HyperLink>
                                        <!--    <asp:LinkButton ID="TermsAndConditionsButton" Text="Accept Terms and Conditions" runat="server" OnClientClick="ShowModal('#TermsAndCC')"/> -->

                                       </ContentTemplate>
                                      </asp:UpdatePanel>
                                        </div>
                                        <div class="bottomButtonHolder action-buttons">
                                            <asp:LinkButton ID="PaymentCancelButton" runat="server" CssClass="orangeButton buttonInline cancel-btn fr2BColor" CausesValidation="false">Cancel</asp:LinkButton>
                                            <asp:LinkButton ID="PaymentSaveButton" runat="server" CssClass="orangeButton buttonInline save-btn fr2BColor" CausesValidation="false">Save</asp:LinkButton>
                                            <!-- CMallory Task 0090 - Added OnClick event to PaymentPayNowButton -->
                                            <asp:LinkButton ID="PaymentPayNowButton" runat="server" CssClass="orangeButton buttonInline pay-now-btn fr2BColor" CausesValidation="false" OnClientClick="DisableButton();" OnClick="PaymentPayNowButton_Click">Pay Now</asp:LinkButton>
                                        </div>
                                    
                                    </div>

                                    <div class="formFullWidth disclaimer frBlack" style="font-size: smaller;">
                                        <span style="font-weight: bold; color: red;">IMPORTANT!&nbsp;</span>
                                        <!-- CMallory - Task 00481 - Added dynamic Warning Span to be changed depending on the payment method -->
                                        <span id="Warning">Click "Pay Now" only once. 
                                        Clicking more than once or hitting "BACK" on your browser could result in multiple and/or missing payments.</span>
                                    </div>
                                </div>
                                <!-- cakel: BUGID00045 - Hide user control due to postback issues
                                <div class="termsAndConditionsModalHolder">
                                    <div class="hide" id="TermsAndCC">
                                        <div class="modalContentContainer">
                                            <uc1:TermsAndConditions runat="server" ID="TermsAndConditions" />
                                            <div class="clearFix"></div>
                                        </div>

                                    </div>
                                </div>
                                -->
                                <div id="ConfirmationWindowRequest" class="hide">
                                    <uc1:ConfirmationWindow runat="server" ID="ConfirmationWindow" />
                                </div>
                                <div id="CancellationWindowRequest" class="hide">
                                    <uc1:CancellationWindow runat="server" ID="CancellationWindow" />
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                    <!--this is the second panel (Payment History screen) -->
                    <div id="paymentPanel2" class="panel" style="width: 972px; left: 972px; ">
                        <div class="panel_content">
                            <div class="standardPadding">
                                <h3 class="fr1Color">Payment History</h3>
                                <uc1:PaymentHistory ID="PaymentHistory" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:CustomValidator ID="RequiredCvvValidation"
        runat="server"
        Display="None"
        ErrorMessage="Your CVV Code is Required" />

    <script>

        var vOrient = 'p';
        // Listen for orientation changes
        window.addEventListener("orientationchange", function () {

            calc_container();

        }, false);

        // CMallory - Task 00481 - Added dynamic Warning Span to be changed depending on the payment method.
        function DisableButton()
        {
            document.getElementById('<% = PaymentPayNowButton.ClientID %>').disabled = true;
            document.getElementById('<% = PaymentPayNowButton.ClientID %>').textContent = "Payment Processing, please wait...";
        }

        // portrait OR landscape
        function _portrait_landspace() {

            //
            if (window.matchMedia("(orientation: portrait)").matches) {
                vOrient = 'p';
            }

            if (window.matchMedia("(orientation: landscape)").matches) {
                vOrient = 'l';
            }
            //

            var vDevice = navigator.userAgent;
            var w_width = $(window).width();
            if (vOrient == 'p') {
                if ($(window).width() >= 768 && $(window).width() < 1024) {
                    var vML = (w_width - 768) / 2;
                    $('#pageCenterContainer').attr('id', 'temp');
                }
                else if ($(window).width() < 768) {
                    $('#headerImg').hide();
                }
            }
            if (vOrient == 'l') {
                if ($(window).width() >= 768 && $(window).width() < 1024) {
                    var vML = (w_width - 768) / 2;
                    $('#pageCenterContainer').attr('id', 'temp');
                }
                else if ($(window).width() < 768) {
                    $('#headerImg').hide();
                }
            }

        } // end of p_l....


        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();

            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
               // $("#pageCenterContainer").css("margin-left", vML);
                //$("#pageCenterContainer").css("margin-right", vML);
               // $("#temp").css("margin-left", vML);
               // $("#temp").css("margin-right", vML);
            }
            else if ($(window).width() >= 768 && $(window).width() < 1024) {
                var vML = (w_width - 768) / 2;
                $('#pageCenterContainer').attr('id', 'temp');

            }
            if ($(window).width() == 1016) {
                $("#pageCenterContainer").css("width", "980px");
               // $("#pageCenterContainer").css("margin-left", "0px");
                //$("#pageCenterContainer").css("margin-right", "0px");
               // $("#temp").css("margin-left", "0px");
               // $("#temp").css("margin-right", "0px");
            }
            else if ($(window).width() < 768) {
                //$('#headerImg').hide();
            }

            //_portrait_landspace();

        } // end of calc....

        function slow() {

        }

        $(document).ready(function () {

            /* */
            var vObj = document.getElementById("header");
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "130px");
            if ($(window).width() <= 767) {
                $(".residentsPage").css("top", "100px");
            }
            //$('.wrapper').attr('style', 'height: 1500px !important');

            setTimeout("slow", 500);

            calc_container();


            //$("footer").css("position", "absolute");
            //$("footer").css("top", "1100px");
            if ($(window).width() <= 767) {
                $("footer").css("display", "none");
            }

            //CMallory - Task 00481 - Added  if statement to set the Warning Span if the payment method is Rent By Cash.
            if (<%=SelectPaymentType.PaymentTypeSelectedValue%> == 3)
            {
                Warning.textContent = "Disable pop-up blocker to Create a PayNearMe Ticket";
            }
            
           /* Mask */
            ApplySingleMask("#<%=TextToPayTile.MobileNumberClientId%>", "(000) 000-0000");
            ApplySingleMask("#<%=ResidentBillingInformation.PhoneNumberClientId%>", "(000) 000-0000");
            ApplySingleMask("#<%=ResidentBillingInformation.ZipCodeClientId%>", "00000");

            paybox = $(".pay-now-box"),
            payboxOrig = paybox.html();

            enquire.register("screen and (min-width: 300px) and (max-width: 640px)", {
                match: function () {
                    /* Duplicate total due for mobile */
                    var orig = $("#ContentPlaceHolder1_PaymentAmounts_TotalAmountLabel").text();
                    $("#ContentPlaceHolder1_TotalAmountLabel").text(orig);

                    /* Rearranging for mobile */
                    var ab = $(".action-buttons"),
                        disc = $(".bottomElements .disclaimer"),
                        paybtn = $(".pay-now-btn");

                    paybox.prepend(disc).prepend(ab).append(paybtn);
                    changePanels($(".mainTabSection .tabs .selected").index());

                    // Store section to open after postback when certain inputs/selects are changed
                    $("#ContentPlaceHolder1_SelectPaymentType_PaymentMethodComboBox").on("change", function (e) {
                        localStorage.setItem("sectionToOpen", "#paymentMethod");
                        
                    });
                    $("#ContentPlaceHolder1_PaymentAmounts_OverrideTextBox").on("change", function (e) {
                        localStorage.setItem("sectionToOpen", "#totalPay");
                    });

                    $("#payOpt select").on("change", function (e) {
                        localStorage.setItem("sectionToOpen", "#payOpt");
                    });
                    
                    $("#payOpt a").on("click", function (e) {
                        localStorage.setItem("sectionToOpen", "#payOpt");
                    });

                    $("#ContentPlaceHolder1_ResidentBillingInformation_SameAsMailingCheckBox").on("change", function (e) {
                        localStorage.setItem("sectionToOpen", "#billingAdd");
                    });

                    // Function to open accordion to specific section.
                    if (localStorage.getItem("sectionToOpen")) {
                        window.location.hash = localStorage.getItem("sectionToOpen");
                        if (window.location.hash) {
                            setTimeout(function () {
                                $('h4[data-target="' + window.location.hash + '"]').click();
                            }, 1600);
                            changePanels(0);
                        }
                        localStorage.removeItem("sectionToOpen");
                    }
                    //if (localStorage.getItem("sectionToOpen").toString() = "RentByCash")
                    //{

                    //}

                },

                unmatch: function () {
                    paybox.html(payboxOrig);
                    changePanels($(".mainTabSection .tabs .selected").index());
                },

            });

            // Handle accordion expand/collapse icons
            $('#accordion').on('show.bs.collapse', function (e) {
                $('h4[data-target="#' + e.target.id + '"] i').removeClass("fa-plus").addClass("fa-minus");
            });

            $('#accordion').on('hide.bs.collapse', function (e) {
                $('h4[data-target="#' + e.target.id + '"] i').removeClass("fa-minus").addClass("fa-plus");
            });

            // Update panel sizes as accordion expands/collapses
            $('#accordion').on('shown.bs.collapse', function (e) {
                changePanels(0);
            });

            $('#accordion').on('hidden.bs.collapse', function (e) {
                changePanels(0);
            });

            // Open autopay section if querystring is set
            if (window.location.search.indexOf("AutoPay=1") != -1) {
                setTimeout(function () {
                    $('h4[data-target="#payOpt"]').click();
                }, 1000);
            }

        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);

    </script>
    <script>


        $(document).ready(function () {

            $(window).bind("orientationchange", function () {

                // Resize panels when device orientation shifts
                setTimeout(function () {
                window.panelWidth = $('.mainTabSection').width();
                $('.panel_container .panel').each(function (index) {
                    $(this).css({ 'width': window.panelWidth + 'px', 'left': (index * window.panelWidth) + 'px' });
                    $('.mainTabSection .panels').css('width', (index + 1) * window.panelWidth + 'px');
                    changePanels($(".mainTabSection .tabs .selected").index());
                });
                }, 250);
            });
        });

        var rtime = new Date(1, 1, 2000, 12, 0, 0);
        var timeout = false;
        var delta = 100;
        // Listen for resize changes
        $(window).resize(function () {

            rtime = new Date();
            if (timeout === false) {
                timeout = true;
                setTimeout(resizeend, delta);
            }
        });

        function resizeend() {

            if (new Date() - rtime < delta) {
                setTimeout(resizeend, delta);
            } else {
                 timeout = false;
               window.panelWidth = $('.mainTabSection').width();
                $('.panel_container .panel').each(function (index) {
                    $(this).css({ 'width': window.panelWidth + 'px', 'left': (index * window.panelWidth) + 'px' });
                    $('.mainTabSection .panels').css('width', (index + 1) * window.panelWidth + 'px');
                    changePanels($(".mainTabSection .tabs .selected").index());
                });
            }
        } // end of resizeend....


    </script>
</asp:Content>
