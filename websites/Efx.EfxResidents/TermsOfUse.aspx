﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TermsOfUse.aspx.cs" Inherits="Efx.EfxResidents.TermsOfUse" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <div id="pageCenterContainer" class="legalText residentsPage">
        <uc1:TermsAndConditions runat="server" id="TermsAndConditions" />
    </div>

    <script>

        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
            }
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);
        } // end of calc....


        $(document).ready(function () {
            /* */
            var vObj = document.getElementById("header");
            //vObj.style.top = "-40px";
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "150px");
            $('.wrapper').attr('style', 'height: 3100px !important');

            calc_container();
        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);


        // set menu to go back to default page
        $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

            evt.preventDefault();

            var vHash = $(this).attr('href');
            window.location.href = "/Default.aspx" + vHash;

        });
    </script>    


</asp:Content>
