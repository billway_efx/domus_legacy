﻿using EfxFramework.Logging;
using System;
using System.Web.Http;
using System.Web.Routing;
using System.Web.UI;
using System.Web;
using System.Net;
using System.Net.Mail;
using EfxFramework;

namespace Efx.EfxResidents
{
    public class Global : System.Web.HttpApplication
    {
        private void RegisterRoutes(RouteCollection routes)
        {
            routes.MapHttpRoute(
                name: "Default",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = System.Web.Http.RouteParameter.Optional });

            GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started 
            var SessionId = Session.SessionID;
        }

        //CMallory - Task 00477 - Added to prevent Clicking Jacking Vulnerability.
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpContext.Current.Response.AddHeader("x-frame-options", "DENY");
        }
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        void Application_PreRequestHandlerExecute(object src, EventArgs e)
        {
            // hook up the PreInit page handler
            Page p = this.Context.Handler as Page;
            if (p != null)
            {
                p.PreInit += new EventHandler(page_PreInit);
            }
        }
        void page_PreInit(object sender, EventArgs e)
        {
            var current = HttpContext.Current;
            Page p = this.Context.Handler as Page;
            if (p != null && !String.IsNullOrWhiteSpace(current.Request.QueryString["view"]) && current.Request.QueryString["view"].ToLowerInvariant() == "mobile")
            {
                // set the theme and master page
                p.MasterPageFile = "~/Mobile.master";
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            
            //cakel: TaskID00430 - Resident Site
            if (HttpContext.Current.Server.GetLastError() != null)
            {
                

                string strIPaddress = null;
                strIPaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (strIPaddress == null)
                {
                    strIPaddress = Request.ServerVariables["REMOTE_ADDR"];
                }

                string EmailList = Settings.ErrorEmailList;

                Exception myException = HttpContext.Current.Server.GetLastError().GetBaseException();
                string mailsubject = "Error in Page for RPO Renter Site " + Request.Url.ToString();
                string message = string.Empty;
                //message += "<strong>User: <strong/>" + _user + "<br />";
                message += "<strong>Message</strong><br />" + myException.Message + "<br />";
                message += "<strong>StackTrace</strong><br />" + myException.StackTrace + "<br />";
                message += "<strong>Query String</strong><br />" + Request.QueryString.ToString() + "<br />";
                message += "<strong>IP Address</strong><br />" + strIPaddress.ToString() + "<br />";
                MailMessage myMessage = new MailMessage("support@efxach.com", EmailList, "RPO Renter Site Error", message);
                myMessage.IsBodyHtml = true;
                SmtpClient mySmtpClient = new SmtpClient();
                mySmtpClient.Host = Settings.SmtpHost;
                mySmtpClient.Send(myMessage);

            }
        }

    }
}