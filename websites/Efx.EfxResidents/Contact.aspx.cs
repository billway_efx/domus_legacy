﻿using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using System;
using System.Web;
using System.Web.UI.WebControls;
using Mb2x.ExtensionMethods;

namespace Efx.EfxResidents
{
    public partial class Contact : BasePageV2, IContactResident
    {
        private ContactResidentPresenter _Presenter;

        public int RenterId { get { return !string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? HttpContext.Current.User.Identity.Name.ToInt32() : 0; } }
        public CheckBoxList ContactRentPaidOnlineList { get { return ContactRentPaidOnline; } }
        public IContactForm ContactUserControl { get { return ContactForm; } }
        public ServerValidateEventHandler IsCheckBoxChecked { set { AtLeastOneCheckBoxIsCheckedValidation.ServerValidate += value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new ContactResidentPresenter(this);
            AddValidationSummaryToPage();
            //EmailLink.NavigateUrl = String.Format("mailto:{0}", Settings.ContactUsEmail);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}