﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RPO;
using System.Web;
using EfxFramework.Helpers;

namespace RentPaidOnline.API
{
	public class CalendarEventController : ApiController
	{
		[HttpGet]
		[ActionName("getbyresidentid")]
		public object GetByResidentId()
		{
			int id = 0;
			if (Int32.TryParse(HttpContext.Current.User.Identity.Name, out id))
			{
				using (var entity = new RPOEntities())
				{
					var renter = entity.Renters.Where(r => r.RenterId == id).FirstOrDefault();
					if (renter != null)
					{
						var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);
						string propertyIdString = Lease.PropertyId.ToString();
						var eventList = entity.CalendarEvents.Where(e => !e.Deleted && e.Active && ((e.PropertyId.HasValue && e.PropertyId.Value == renter.RenterPropertyId) || e.AllProperties)).ToList();
						eventList.AddRange(BuildRentDueDateEvent(Lease));
                        var property = entity.Properties.FirstOrDefault(p => p.PropertyId == Lease.PropertyId);
                        string propertyName = property != null ? property.PropertyName : "";
                        var eventsList = eventList.Select(e => new
                        {
                            id = e.CalendarEventId,
                            title = e.Title,
                            start = e.StartDate,
                            end = e.EndDate,
                            url = e.CalendarEventId > 0 ? "details/" + e.CalendarEventId : "",
                            contact = e.Contact,
                            location = e.Location,
                            property = propertyName,
                            active = e.Active

                        }).ToList();
                        return new { events = eventsList, color = "", textColor = "" };
					}
				}
			}
            return new { events = "", color = "", textColor = "" };
		}
        [HttpGet]
        [ActionName("getbyeventid")]
        public CalendarEvent GetByEventId(int id)
        {
            int renterId = 0;
            if (Int32.TryParse(HttpContext.Current.User.Identity.Name, out renterId))
            {
                using (var entity = new RPOEntities())
                {
                    var renter = entity.Renters.Where(r => r.RenterId == renterId).FirstOrDefault();
                    if (renter != null)
                    {
                        var calEvent = entity.CalendarEvents.Where(e => e.CalendarEventId == id && e.Active && !e.Deleted).FirstOrDefault();
                        if (calEvent != null)
                        {
                            bool canAccessEvent = (calEvent.Active && ((calEvent.PropertyId.HasValue && renter.RenterPropertyId == calEvent.PropertyId) || calEvent.AllProperties));
                            if (canAccessEvent)
                                return calEvent;
                        }
                    }
                }
            }

            return null;
        }
		private List<CalendarEvent> BuildRentDueDateEvent(EfxFramework.Lease lease)
		{
			List<CalendarEvent> returnList = new List<CalendarEvent>();
			
			int numberOfMonths = ((lease.EndDate.Year - DateTime.Now.Year) * 12) + lease.EndDate.Month - DateTime.Now.Month;
			DateTime dueDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, lease.RentDueDayOfMonth);

			for (int i = 0; i < numberOfMonths; i++)
			{
				CalendarEvent calEvent = new CalendarEvent();
				calEvent.Active = true;
				calEvent.CalendarEventTypeId = 0;
				calEvent.Deleted = false;
				calEvent.Details = "Rent Due";
				calEvent.EfxAdministratorId = 1;
				calEvent.PropertyId = lease.PropertyId;
				calEvent.Title = "Rent Due";
				calEvent.StartDate = dueDate.AddMonths(i);
				calEvent.EndDate = dueDate.AddMonths(i);
                calEvent.Contact = "";
                calEvent.CalendarEventType = new CalendarEventType() { CalendarEventTypeId = 0, Name = "Reminder" };
                calEvent.Location = "Leasing Office";
				returnList.Add(calEvent);
				System.Diagnostics.Debug.WriteLine(dueDate.ToShortDateString());
			}

			return returnList;
		}
	}
}