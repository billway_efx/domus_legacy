﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Efx.EfxResidents.Default" %>

<%@ Register Src="~/UserControls/AccountBalanceSection.ascx" TagPrefix="uc1" TagName="AccountBalanceSection" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $("#ContentPlaceHolder1_AccountBalanceSection_SetupAutopayButton").on("click", function (e) {
            localStorage.setItem("sectionToOpen", "#payOpt");
            return true
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <div id="pageCenterContainer" class="homePageContainer homePageDashboard residentsPage"  >
    <!-- <div class="homePageContainer homePageDashboard"> -->
        <!-- First Row (Account Balance, Account Summary, Maintenance) -->
        <div class="formRow borderBottom">         
            <!-- Account Balance column -->
            <section class="even3Column account-balance">
                <uc1:AccountBalanceSection runat="server" id="AccountBalanceSection" />
            </section>

            <!-- Account Summary column -->
            <section class="even3Column account-summary">
                <uc1:AccountSummarySection runat="server" id="AccountSummarySection" />
            </section>

            <!-- Maintenance column -->
            <section id="" class="even3Column maint-summary">
                <!-- Salcedo - 2/10/2015 - task # 00366 - added ability to hide/disable/change maintenance panel -->
                <div runat="server" id="MaintenanceVisibility" visible="false">
                    <div class="homepageMaintenanceHolder">
                        <uc1:MaintenanceSection runat="server" id="MaintenanceSection" />
                    </div>
                </div>
            </section>
        </div>
            
        <!-- Second Row (Announcements, latest News) -->
        <div class="formRow borderBottom">
            <section class="announcementsColumn even3Column">
                <h4 class="fr2Color">Announcements</h4>
                <div class="borderBottom">
                    <h3 class=" fr1Color">From Your Property Manager</h3>
                    <asp:ListView ID="PropertyManagerAnnouncements" runat="server">
                        <LayoutTemplate>
                            <ul>
                                <li id="itemPlaceHolder" runat="server"></li>
                            </ul>
                        </LayoutTemplate>
                        <ItemTemplate>                            
                            <li><asp:Label ID="AnnouncementLabel" runat="server" Text='<%# Bind("AnnouncementText") %>' /></li>                                    
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <p class="nothingToShow frBlack">There are no Property Announcements to Show at this time.</p>
                        </EmptyDataTemplate>
                    </asp:ListView>

                </div>
                <div>
                    <h3 class="fr1Color">From RentPaidOnline</h3>
                    <asp:ListView ID="RPOAnnouncements" runat="server">
                        <LayoutTemplate>
                            <ul>
                                <li id="itemPlaceHolder" runat="server"></li>
                            </ul>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <li><asp:Label ID="AnnouncementLabel" runat="server" Text='<%# Bind("AnnouncementText") %>' /></li>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <p class="nothingToShow frBlack">There are no RPO Administrative Announcements to Show at this time.</p>
                        </EmptyDataTemplate>
                    </asp:ListView>
                </div>
                <!-- Bottom links -->
                <div class="formRow rowSpacingAdjust alignRight">
                    <a href="Announcements.aspx" class="greenLink fr1Color">View All Announcements</a>
                </div>
            </section>
            <section class="latestNewsColumn even3Column">
                <h4 class="fr2Color">Latest News</h4>
                <uc1:NewsSection runat="server" ID="NewsSection" />                    
            </section>
			<section class="calendarColumn even3Column">
				<h4 class="fr2Color">Calendar of Events</h4>
				<a class="calendarCallout fr2Color" style="border-style: solid; border-color: #0462c2; border-width: 1px;" href="/calendar.aspx">View Calendar</a>
                <a class="calendarCalloutMobile orangeButton fr2BColor" href="/calendar.aspx">View Your Calendar</a>
			</section>
        </div>  
        
        <!-- Second Row (Local News & Weather) -->
        <div class="fullrow">
            <div class="newsAndWeatherHolder">
                <h4 class="fr2Color">Local News & Weather</h4>
                <uc1:NewsAndWeatherSection ID="NewsAndWeatherSectionControl" runat="server"/>
            </div>                
        </div>        
    </div>

    <script>

        var vOrient = 'p';
        // Listen for orientation changes
        window.addEventListener("orientationchange", function () {

            calc_container();
        }, false);

        // portrait OR landscape
        function _portrait_landspace() {

            //
            if (window.matchMedia("(orientation: portrait)").matches) {
                vOrient = 'p';
                change_elements();
            }

            if (window.matchMedia("(orientation: landscape)").matches) {
                vOrient = 'l';
                change_elements();
            }
            //
            var vDevice = navigator.userAgent;
            var w_width = $(window).width();
            if (vOrient == 'p') {
                if ($(window).width() >= 768 && $(window).width() < 1024) {
                    var vML = (w_width - 768) / 2;
                    //$('#pageCenterContainer').attr('id', 'temp');
                }
                else if ($(window).width() < 768) {
                    //$('#headerImg').hide();
                }
            }
            if (vOrient == 'l') {
                if ($(window).width() >= 768 && $(window).width() < 1024) {
                    var vML = (w_width - 768) / 2;
                    //$('#pageCenterContainer').attr('id', 'temp');
                }
                else if ($(window).width() < 768) {
                    //$('#headerImg').hide();
                }

            }

        } // end of p_l....

        function change_elements() {

        }


        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
                $("#pageCenterContainer").css("margin-left", vML);
                $("#pageCenterContainer").css("margin-right", vML);
            }
            _portrait_landspace();

        } // end of calc....

        $(document).ready(function () {

            /* */
            var w_width = $(window).width();
            var vObj = document.getElementById("header");

            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "170px");
            if ($(window).width() <= 767) {
                $(".residentsPage").css("top", "100px");
            }
            //$('.wrapper').attr('style', 'height: 1500px !important');

            calc_container();

        });



        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);


    </script>  
     
</asp:Content>