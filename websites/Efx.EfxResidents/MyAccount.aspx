﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyAccount.aspx.cs" Inherits="Efx.EfxResidents.MyAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden   -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <div id="pageCenterContainer" class="pageContainer accountPage residentsPage"> 
        <div class="pageTitle" style="border-bottom: 0px;">
            <h2 style="color:#0462c2  !important; ">Account</h2>
        </div>
        <div class="singleItemSection" style="  border: 1px solid #06cef9; " >
            <div class="standardPadding">
                <h3 class="fr1Color">Account Information</h3>
                <div id="accordion">
                    <!--** First Column (General Info)**-->
                    <div id="accountd1" class="even3Column accountPageColumn accordion-panel">
                        <section>
                            <h4 data-toggle="collapse" data-target="#genInfo" data-parent="#accordion" class="fr2Color">General Info <i class="pull-right fa fa-plus mobile-only" style="display:none;"></i></h4>
                        </section>
                        <section id="genInfo" class="collapse">
                            <p><span class="itemLabel frBlack">Resident ID&nbsp</span><asp:Label ID="ResidentIDLabel" CssClass="frBlack" runat="server" /></p>
                            <!-- RentReporters ID-->
                            <div class="formRow">
                                <asp:Panel ID="RenterReporterIDVisible" runat="server" Visible="false">
                                    <div class="formFullWidth">
                                        <span class="formLabel fr1Color">RentReporters ID</span>
                                        <asp:TextBox ID="RentReportersIdTextBox" runat="server"></asp:TextBox>
                                    </div>
                                </asp:Panel>
                            </div>
                            <!-- Email / Username-->
                            <div class="formRow">
                                <div class="formFullWidth">
                                    <span class="formLabel fr1Color">Email / Username</span>
                                    <asp:TextBox ID="EmailUsernameTextBox" type="email" runat="server"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="EmailValidator"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ControlToValidate="EmailUsernameTextBox"
                                        ErrorMessage="Invalid email address."
                                        runat="server"
                                        Display="None" />
                                    <asp:RequiredFieldValidator ID="EmailRequiredValidator"
                                        runat="server"
                                        ControlToValidate="EmailUsernameTextBox"
                                        ErrorMessage="Email Address is required."
                                        Display="None" />
                                </div>
                            </div>
                            <!-- First name-->
                            <div class="formRow">
                                <div class="formFullWidth">
                                    <span class="formLabel fr1Color">First Name</span>
                                    <asp:TextBox ID="FirstNameTextBox" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="FirstNameRequiredValidator"
                                        runat="server"
                                        ControlToValidate="FirstNameTextBox"
                                        ErrorMessage="First Name is required."
                                        Display="None" />
                                </div>
                            </div>
                            <!-- Middle name-->
                            <div class="formRow">
                                <div class="formFullWidth">
                                    <span class="formLabel fr1Color">Middle Name</span>
                                    <asp:TextBox ID="MiddleNameTextBox" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <!-- Last name-->
                            <div class="formRow">
                                <div class="formFullWidth">
                                    <span class="formLabel fr1Color">Last Name</span>
                                    <asp:TextBox ID="LastNameTextBox" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="LastNameRequiredValidator"
                                        runat="server"
                                        ControlToValidate="LastNameTextBox"
                                        ErrorMessage="Last Name is required."
                                        Display="None" />
                                </div>
                            </div>
                            <!-- Suffix-->
                            <div class="formRow">
                                <div class="formFullWidth">
                                    <span class="formLabel fr1Color">Suffix</span>
                                    <asp:TextBox ID="SuffixTextBox" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <asp:MultiView ID="Address" runat="server" ActiveViewIndex="0">
                                <asp:View ID="EditAddress" runat="server">
                                    <!-- Mailing Address-->
                                    <div class="formRow">
                                        <div class="formFullWidth">
                                            <span class="formLabel fr1Color">Mailing Address</span>
                                            <asp:TextBox ID="MailingAddressTextBox" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="MailingAddressRequiredValidator"
                                                runat="server"
                                                ControlToValidate="MailingAddressTextBox"
                                                ErrorMessage="Mailing Address is required."
                                                Display="None" />
                                        </div>
                                    </div>
                                    <!-- City-->
                                    <div class="formRow">
                                        <div class="formFullWidth">
                                            <span class="formLabel fr1Color">City</span>
                                            <asp:TextBox ID="CityTextBox" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="CityRequiredValidator"
                                                runat="server"
                                                ControlToValidate="CityTextBox"
                                                ErrorMessage="City, State, and Zip are required."
                                                Display="None" />
                                        </div>
                                    </div>
                                    <!-- State-->
                                    <div class="formRow">
                                        <div class="formFullWidth">
                                            <span class="formLabel fr1Color">State</span>
                                            <asp:DropDownList ID="StateDropDownList" runat="server" CssClass="chzn-select">
                                                <asp:ListItem Value="0" Text="Select a State"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <!-- Zip-->
                                    <div class="formRow">
                                        <div class="formFullWidth">
                                            <span class="formLabel fr1Color">Zip</span>
                                            <asp:TextBox ID="ZipTextBox" runat="server"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="ZipValidator"
                                                ControlToValidate="ZipTextBox"
                                                ValidationExpression="[0-9][0-9][0-9][0-9][0-9]"
                                                ErrorMessage="Invalid format. Pin must be 4 numeric digits."
                                                runat="server"
                                                Display="None" />
                                        </div>
                                    </div>
                                </asp:View>

                                <asp:View ID="ViewAddress" runat="server">
                                    <!-- Mailing Address-->
                                    <div class="fullrow rowSpacingAdjust">
                                        <h5 class="fr1Color">Mailing Address</h5>
                                        <p>
                                            <asp:Label ID="Address1Label" runat="server" />
                                        </p>
                                        <p>
                                            <asp:Label ID="CityStateZipLabel" runat="server" />
                                        </p>
                                    </div>
                                </asp:View>
                            </asp:MultiView>
                            <!-- Main Phone-->
                            <div class="formRow">
                                <div class="formFullWidth">
                                    <!--cakel: BUGID00069 Updated Main Phone to Cell Phone -->
                                    <span class="formLabel fr1Color">Cell Phone</span>
                                    <asp:TextBox ID="MainPhoneTextBox" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="PhoneValidator"
                                        runat="server"
                                        ControlToValidate="MainPhoneTextBox"
                                        ErrorMessage="Main Phone is required."
                                        Display="None" />
                                </div>
                            </div>
                            <!-- Alternative Phone-->
                            <div class="formRow">
                                <div class="formFullWidth">
                                    <!--cakel: BUGID00069 updated Alternative Phone to Home Phone -->
                                    <span class="formLabel fr1Color">Home Phone</span>
                                    <asp:TextBox ID="AlternativePhoneTextBox" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!--** Second Column (Pay-By-Phone, Password)**-->
                    <div id="accountd2" class="even3Column accountPageColumn accordion-panel">
                        <!--Pay-By-Phone section -->
                        <%-- cakel: Masked pay by phone  --%>
                      <%--  <section class="borderBottom dividedSection">
                            <h4 data-toggle="collapse" data-target="#payPhone" data-parent="#accordion" class="fr2Color">Pay-By-Phone <i class="pull-right fa fa-plus mobile-only" style="display:none;"></i></h4>
                        </section>--%>
                        <div id="PaybyPhoneSection" runat="server" visible ="false">
                        <section id="payPhone" class="collapse">
                            <p><span class="itemLabel fr1Color">Current ID&nbsp;</span><asp:Label ID="CurrentPinLabel" runat="server" /></p>
                            <div class="formRow">
                                <div class="formSeventyFiveWidth">
                                    <span class="formLabel fr1Color">New Pin</span>
                                    <asp:TextBox ID="NewPinTextBox" runat="server" MaxLength="4" autocomplete="off"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="PinFormatValidator"
                                        ControlToValidate="NewPinTextBox"
                                        ValidationExpression="[0-9][0-9][0-9][0-9]"
                                        ErrorMessage="Invalid format. Pin must be 4 numeric digits."
                                        runat="server"
                                        Display="None" />
                                </div>
                            </div>
                            <div class="formRow">
                                <div class="formSeventyFiveWidth">
                                    <span class="formLabel fr1Color">Confirm Pin</span>
                                    <asp:TextBox ID="ConfirmPinTextBox" runat="server" MaxLength="4" autocomplete="off"></asp:TextBox>
                                    <asp:CustomValidator ID="PinValidator"
                                        runat="server"
                                        ErrorMessage="Pin numbers must match."
                                        Display="None" />

                                </div>
                            </div>
                        </section>
                            </div>
                        <!--Password section -->
                        <section>
                            <h4 data-toggle="collapse" data-target="#pass" data-parent="#accordion" class="fr2Color">Password <i class="pull-right fa fa-plus mobile-only" style="display:none;"></i></h4>
                        </section>
                        <section id="pass" class="collapse">
                            <div class="formRow">
                                <div class="formSeventyFiveWidth">
                                    <span class="formLabel fr1Color">New Password</span>
                                    <asp:TextBox ID="NewPasswordTextBox" runat="server" TextMode="Password" autocomplete="off"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="PasswordFormatValidator"
                                        ControlToValidate="NewPasswordTextBox"
                                        ValidationExpression="^.*(?=.{6,})(?=.*[\d])(?=.*[a-zA-Z]).*$"
                                        ErrorMessage="Invalid format. Password must be at least 6 digits and contain at least 1 letter and 1 number."
                                        runat="server"
                                        Display="None" />
                                </div>
                            </div>
                            <div class="formRow">
                                <div class="formSeventyFiveWidth">
                                    <span class="formLabel fr1Color">Confirm Password</span>
                                    <asp:TextBox ID="ConfirmPasswordTextBox" runat="server" TextMode="Password" AutoCompleteType="Disabled" autocomplete="off"></asp:TextBox>
                                    <asp:CustomValidator ID="PasswordValidator"
                                        runat="server"
                                        ErrorMessage="Passwords must match."
                                        Display="None" />
                                </div>
                            </div>
                        </section>
                    </div>

                    <!--**Third Column (Photo)**-->
                    <div id="accountd3" class="even3Column accountPageColumn accordion-panel">
                        <section>
                            <h4 data-toggle="collapse" data-target="#photo" data-parent="#accordion" class="fr2Color">Your Photo <i class="pull-right fa fa-plus mobile-only" style="display:none;"></i></h4>
                        </section>
                        <section id="photo" class="collapse">
                            <div class="iconHolder">
                                <asp:Image ID="RenterPhoto" CssClass="resizedImage" runat="server" />
                            </div>
                            <div class="formRow rowSpacingAdjust">
                                <div class="formFullWidth">
                                    <uc1:ImageAdder ID="ImageAdder" runat="server" MaxNumberOfPhotos="1" />
                                </div>

                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <div id="accountBottom" class="bottomElements">
                <div class="formFullWidth">
                    <div class="bottomButtonHolder">
                        <asp:Button ID="SaveButton" runat="server" Text="Save" CssClass="orangeButton saveButton fr2BColor" CausesValidation="false" />
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>

        var vOrient = 'p';
        // Listen for orientation changes
        window.addEventListener("orientationchange", function () {

            calc_container(vOrient);

        }, false);

        // portrait OR landscape
        function _portrait_landspace() {

            //
            if (window.matchMedia("(orientation: portrait)").matches) {
                vOrient = 'p';
            }

            if (window.matchMedia("(orientation: landscape)").matches) {
                vOrient = 'l';
            }
            //

            var vDevice = navigator.userAgent;
            var w_width = $(window).width();
            if (vOrient == 'p') {
                if ($(window).width() < 768) {
                    $('#headerImg').hide();
                }
            }
            if (vOrient == 'l') {
                if ($(window).width() < 768) {
                    $('#headerImg').hide();
                }
            }

        } // end of p_l....


        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
                $("#pageCenterContainer").css("margin-left", vML);
                $("#pageCenterContainer").css("margin-right", vML);
            }
            else if ($(window).width() >= 768 && $(window).width() < 1024) {
                var vML = (w_width - 768) / 2;
                $('#pageCenterContainer').attr('id', 'temp');
            }
            if ($(window).width() == 1016) {
                $("#pageCenterContainer").css("width", "980px");
                $("#pageCenterContainer").css("margin-left", "0px");
                $("#pageCenterContainer").css("margin-right", "0px");
            }
            else if ($(window).width() < 768) {
                //$('#headerImg').hide();
            }


        } // end of calc....


        $(document).ready(function () {

            /* */
            var vObj = document.getElementById("header");

            $(".residentsPage").css("position", "absolute");
            //$(".residentsPage").css("top", "180px");
            if ($(window).width() <= 767) {
                $(".residentsPage").css("top", "100px");
                $(".residentsPage").css("background-color", "transparent");
            }
            //$('.wrapper').attr('style', 'height: 1500px !important');

            calc_container();

            $("footer").css("position", "absolute");
            $("footer").css("top", "1200px");
            if ($(window).width() <= 767) {
                $("footer").css("display", "none");
            } else {
                var vHeight = "height: " + ($(".pageContainer.accountPage.residentsPage").height() + 200) + "px !important";
                //$('.wrapper').attr('style', vHeight);
                vHeight = "position: absolute; top: " + ($(".wrapper").height() + 200) + "px";
                //$('footer').attr('style', vHeight);
             }

            /* Mask */
            ApplySingleMask("#<%=MainPhoneTextBox.ClientID%>", "(000) 000-0000");
            ApplySingleMask("#<%=AlternativePhoneTextBox.ClientID%>", "(000) 000-0000");

            // Handle accordion expand/collapse icons
            $('#accordion').on('show.bs.collapse', function (e) {
                $('h4[data-target="#' + e.target.id + '"] i').removeClass("fa-plus").addClass("fa-minus");
            });

            $('#accordion').on('hide.bs.collapse', function (e) {
                $('h4[data-target="#' + e.target.id + '"] i').removeClass("fa-minus").addClass("fa-plus");
            });

            // Store section to open after postback when certain inputs/selects are changed
            $("#photo a").on("click", function (e) {
                localStorage.setItem("sectionToOpen", "#photo");
            });

            // Function to open accordion to specific section.
            if (localStorage.getItem("sectionToOpen")) {
                window.location.hash = localStorage.getItem("sectionToOpen");
                if (window.location.hash) {
                    setTimeout(function () {
                        $('h4[data-target="' + window.location.hash + '"]').click();
                    }, 1000);
                }
                localStorage.removeItem("sectionToOpen");
            }
        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);

</script>
</asp:Content>