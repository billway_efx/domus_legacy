﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Efx.EfxResidents.Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden   -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <div id="pageCenterContainer" class="pageContainer contactPage residentsPage" > 
        <div class="pageTitle" style="border-width: 0;" ><h2 style="color: #0462c2 !important; ">Contact</h2></div>
        <div class="contactContent">
            
            <!-- **First Column (Contact form)** -->  
            <div class="contactColumn1">
                <h4 class="fr2Color">Contact RentPaidOnline</h4>
                <asp:CheckBoxList ID="ContactRentPaidOnline" runat="server">
                    <asp:ListItem Value="1" Text="Your Property Management Company" class="frBlack"></asp:ListItem>
                    <asp:ListItem Value="2" Text="RentPaidOnline Support" class="frBlack"></asp:ListItem>
                </asp:CheckBoxList>
                <uc1:ContactForm runat="server" ID="ContactForm" />
            </div>  
            
            <!-- **Second Column (Contact information)** -->         
            <div class="contactColumn2">
                <h4 class="fr2Color">Contact Info</h4>
                <article>
                    <h5 class="fr1Color">Address</h5>
                    <p class="frBlack">RentPaidOnline</p>
                    <p class="frBlack">c/o EFX Financial Services</p>
                    <p class="frBlack">601 Cleveland Street Suite 950</p>
                    <p class="frBlack">Clearwater, FL 33755</p>
                </article>
                <article>
                   <h5 class="fr1Color">Phone Number</h5>
                    <p class="frBlack">Toll Free: <a class="telephone" href="tel:855-769-7368">855-PMY-RENT</a></p> 
                </article>
                <a class="fr1Color" target="_blank" href='https://rentpaidonline.rhinosupport.com/'>Contact Support</a>
                
                <div>
                    <h5 class="fr1Color">Location</h5>
                    <div class="mapHolder">
                        <img src="/Images/LocationMap.png" style="width: 100%; height: auto;"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

<asp:CustomValidator    
    ID="AtLeastOneCheckBoxIsCheckedValidation"
    runat="server"
    ErrorMessage="You Must Select One of The Contacts"
    Display="None" />
    
<script>

    var vOrient = 'p';
    // Listen for orientation changes
    window.addEventListener("orientationchange", function () {

        calc_container(vOrient);

    }, false);

    // portrait OR landscape
    function _portrait_landspace() {

        //
        if (window.matchMedia("(orientation: portrait)").matches) {
            vOrient = 'p';
        }

        if (window.matchMedia("(orientation: landscape)").matches) {
            vOrient = 'l';
        }
        //

        var vDevice = navigator.userAgent;
        var w_width = $(window).width();
        if (vOrient == 'p') {
            if ($(window).width() < 768) {
                 $('#headerImg').hide();
            }
        }
        if (vOrient == 'l') {
            if ($(window).width() < 768) {
                $('#headerImg').hide();
            }
        }

    } // end of p_l....



    // calc margin left to center container
    function calc_container() {

        var w_width = $(window).width();
        if ($(window).width() >= 1024) {
            var vML = (w_width - 1024) / 2;
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);
        }
        else if ($(window).width() >= 768 && $(window).width() < 1024) {
            var vML = (w_width - 768) / 2;
            $('#pageCenterContainer').attr('id', 'temp');
        }
        if ($(window).width() == 1016) {
            $("#pageCenterContainer").css("width", "980px");
            $("#pageCenterContainer").css("margin-left", "0px");
            $("#pageCenterContainer").css("margin-right", "0px");
        }
        else if ($(window).width() < 768) {
            //$('#headerImg').hide();
        }


    } // end of calc....

    $(document).ready(function () {

        /* */
        var vObj = document.getElementById("header");
        //vObj.style.top = "-40px";
        $(".residentsPage").css("position", "absolute");
        $(".residentsPage").css("top", "180px");
        if ($(window).width() <= 767) {
            $(".residentsPage").css("top", "120px");
            $(".residentsPage").css("background-color", "transparent");
        }
        //$('.wrapper').attr('style', 'height: 1500px !important');

        calc_container();

        //$("footer").css("position", "absolute");
        //$("footer").css("top", "1100px");
        if ($(window).width() <= 767) {
            $("footer").css("display", "none");
        }

        /* Mask */
        ApplySingleMask("#<%=ContactForm.PhoneNumberClientId%>", "(000) 000-0000");
    });

    // Listen for resize changes
    window.addEventListener("resize", function () {

        calc_container();
    }, false);

</script>
</asp:Content>
