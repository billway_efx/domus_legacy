﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Blog.aspx.cs" Inherits="Efx.EfxResidents.Blog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>
    
    <div id="pageCenterContainer" class="pageContainer blogPage residentsPage" > 
        <div class="pageTitle" style="border-width: 0;" ><h2 style="color: #0462c2 !important; ">Blog</h2></div>
        <div><img class="residentsRoomImage" src="/Images/residentsRoomBlogImage.png" /></div>
        <div class="blogHolder">
            <uc1:BlogView runat="server" ID="BlogView" />
        </div>
    </div>

    <script>

        var vOrient = 'p';
        // Listen for orientation changes
        window.addEventListener("orientationchange", function () {

            calc_container();

        }, false);

        // portrait OR landscape
        function _portrait_landspace() {

            //
            if (window.matchMedia("(orientation: portrait)").matches) {
                vOrient = 'p';
            }

            if (window.matchMedia("(orientation: landscape)").matches) {
                vOrient = 'l';
            }
            //

            var vDevice = navigator.userAgent;
            var w_width = $(window).width();
            if (vOrient == 'p') {
                if ($(window).width() < 768) {
                    $('#headerImg').hide();
                }
            }
            if (vOrient == 'l') {
                if ($(window).width() < 768) {
                    $('#headerImg').hide();
                }
            }

        } // end of p_l....
        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
                $("#pageCenterContainer").css("margin-left", vML);
                $("#pageCenterContainer").css("margin-right", vML);
            }
            else if ($(window).width() >= 760 && $(window).width() < 1024) {
                var vML = (w_width - 760) / 2
                //$("#pageCenterContainer").css("margin-left", vML);
                //$("#pageCenterContainer").css("margin-right", vML);
                $('#pageCenterContainer').attr('id', 'temp');
            }
            //$("footer").css("position", "absolute");
            //$("footer").css("top", "1500px");
            if ($(window).width() <= 767) {
                $("footer").css("display", "none");
            } else {
                var vHeight = "height: " + ($(".pageContainer.blogPage.residentsPage").height() + 200) + "px !important";
                //$('.wrapper').attr('style', vHeight);
                vHeight = "position: absolute; top: " + ($(".pageContainer.blogPage.residentsPage").height() + 500) + "px";
                //$('footer').attr('style', vHeight);

            }



        } // end of calc....


        $(document).ready(function () {

            /* */
            var vObj = document.getElementById("header");
            //vObj.style.top = "-40px";
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "180px");
            //$('.wrapper').attr('style', 'height: 1500px !important');

            calc_container();

            $(window).bind("orientationchange", function () {
                setTimeout(function () {
                    window.panelWidth = $('.mainTabSection').width();
                    $('.panel_container .panel').each(function (index) {
                        $(this).css({ 'width': window.panelWidth + 'px', 'left': (index * window.panelWidth) + 'px' });
                        $('.mainTabSection .panels').css('width', (index + 1) * window.panelWidth + 'px');
                        changePanels($(".mainTabSection .tabs .selected").index());
                    });
                }, 500);
            });

        });

        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);

    </script>
</asp:Content>
