﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PaymentFailed.aspx.cs" Inherits="Efx.EfxResidents.PaymentFailed" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <div id="pageCenterContainer" class="pageContainer payFailedPage residentsPage">
      
        <div class="singleItemSection paymentFailed">
            <div class="standardPadding">
                <section class="paymentStatusInfoBlock">
                    <h3 class="fr1Color">Payment Status</h3>
                    <p class="redText">Your Transaction has NOT been processed.</p>
                    <br />
                    <asp:Label ID="lblCharityFailureMessage" runat="server" CssClass="fr1Color" Text="Label" Visible="false"></asp:Label><br />
                    <asp:Label ID="lblRentAndFeeFailureMessage" runat="server" CssClass="fr1Color" Text="Label" Visible ="false"></asp:Label><br /><br />

                    <p class="fr1Color">Please try again</p>
                </section> 
                <section class="paymentStatusInfoBlock">
                    <h5 class="fr1Color">RPO Support</h5>
                    <p class="fr1Color"><a class="telephone" href="tel:855-769-7368">Support Line: 1-855-PMY-RENT</a></p>
                    <p class="fr1Color">Email:<a class="fr2Color" href="mailto:support@rentpaidonline.com"> support@rentpaidonline.com</a></p>
                </section>
                <section class="paymentStatusInfoBlock">
                    <!--  <img src="/Images/RentPaidOnlineLogo.png" class="smallLogo" /> -->
                </section> 
                
           </div>    
        </div>
    </div>

    <script>

        var vOrient = 'p';
        // Listen for orientation changes
        window.addEventListener("orientationchange", function () {

            calc_container(vOrient);

        }, false);

        // portrait OR landscape
        function _portrait_landspace() {

            //
            if (window.matchMedia("(orientation: portrait)").matches) {
                vOrient = 'p';
            }

            if (window.matchMedia("(orientation: landscape)").matches) {
                vOrient = 'l';
            }
            //

            var vDevice = navigator.userAgent;
            var w_width = $(window).width();
            if (vOrient == 'p') {
                if ($(window).width() < 768) {
                    $('#headerImg').hide();
                }
            }
            if (vOrient == 'l') {
                if ($(window).width() < 768) {
                    $('#headerImg').hide();
                }
            }

        } // end of p_l....



        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
                $("#pageCenterContainer").css("margin-left", vML);
                $("#pageCenterContainer").css("margin-right", vML);
            }
            else if ($(window).width() >= 768 && $(window).width() < 1024) {
                var vML = (w_width - 768) / 2;
                $('#pageCenterContainer').attr('id', 'temp');
            }
            if ($(window).width() == 1016) {
                $("#pageCenterContainer").css("width", "980px");
                $("#pageCenterContainer").css("margin-left", "0px");
                $("#pageCenterContainer").css("margin-right", "0px");
            }
            else if ($(window).width() < 768) {
                $('#headerImg').hide();
            }


        } // end of calc....

        $(document).ready(function () {

            / /
            var vObj = document.getElementById("header");
            //vObj.style.top = "-40px";
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "180px");
            if ($(window).width() <= 767) {
                $(".residentsPage").css("top", "120px");
                $(".residentsPage").css("background-color", "white");
            }
            //$('.wrapper').attr('style', 'height: 1500px !important');

            calc_container();

            $("footer").css("position", "absolute");
            $("footer").css("top", "1100px");
            if ($(window).width() <= 767) {
                $("footer").css("display", "none");
            }


    });

    // Listen for resize changes
    window.addEventListener("resize", function () {

        calc_container();
    }, false);

</script>

</asp:Content>
