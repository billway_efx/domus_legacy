﻿using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using System;
using Mb2x.ExtensionMethods;
using EfxFramework;
using RPO;
using System.Windows.Forms;
namespace Efx.EfxResidents
{
    public partial class Payments : BasePageV2, IPayment
    {
        private PaymentPresenter _Presenter;

        public Page ViewPage { get { return this; } }
        public IPersonalInformation PersonalInformationControl { get { return PersonalInformation; } }
        public ISelectPaymentType SelectPaymentTypeControl { get { return SelectPaymentType; } }
        public IResidentBillingInformation ResidentBillingInformationControl { get { return ResidentBillingInformation; } }
        public IPaymentAmounts PaymentAmountsControl { get { return PaymentAmounts; } }
        public IAutoPayTile AutoPayTileControl { get { return AutoPayTile; } }
        public ITextToPayTile TextToPayTileControl { get { return TextToPayTile; } }
        public IPaymentHistory PaymentHistoryControl { get { return PaymentHistory; } }
        public IConfirmationWindow ConfirmationWindowControl { get { return ConfirmationWindow; } }
        public bool TermsAccepted { get { return TermsCheckBox.Checked; } }
        public EventHandler PaymentCancelButtonClick { set { PaymentCancelButton.Click += value; } }
        public EventHandler PaymentSaveButtonClick { set { PaymentSaveButton.Click += value; } }
        public EventHandler PaymentPayNowButtonClick { set { PaymentPayNowButton.Click += value; } }
        public ServerValidateEventHandler RequiredCvv { set { RequiredCvvValidation.ServerValidate += value; } }
        public bool EnablePayNow { set { PaymentPayNowButton.Enabled = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            AddValidationSummaryToPage();
            InitializeControls();
			SetupPaymentPayNowButton();

            if (Request.QueryString.Count > 0 && !String.IsNullOrEmpty(Request.QueryString["Tab"]) && Request.QueryString["Tab"].ToInt32() == 2)
            {
                var myScript = "$(document).ready(function() {changePanels(1);});";
                Page.ClientScript.RegisterStartupScript(GetType(), "myKey", myScript, true);
            }

            _Presenter = new PaymentPresenter(this);

            //cakel: BUGID000133
            if (!Page.IsPostBack)
            {
                string _user = Page.User.Identity.Name.ToString();
                RPO.ActivityLog AL = new RPO.ActivityLog();
                AL.WriteLog(_user, "Payments Page", (short)LogPriority.LogAlways);
            }

        }

        private void InitializeControls()
        {
            var Id = HttpContext.Current.User.Identity.Name.ToNullInt32();
            if (!Id.HasValue || Id.Value < 1)
            {
                SafeRedirect("~/Account/Login.aspx");
                return;
            }

            var Resident = new EfxFramework.Renter(Id);

			if (Resident.AcceptedPaymentTypeId == 4)
			{
				uxNoPaymentAcceptedLiteral.Visible = true;
				uxPaymentPanel.Visible = false;
				PersonalInformation.Visible = false;
				SelectPaymentType.Visible = false;
				ResidentBillingInformation.Visible = false;
				PaymentAmounts.Visible = false;
				PaymentAmounts.Visible = false;
				AutoPayTile.Visible = false;
				TextToPayTile.Visible = false;
			}
			else
			{
				uxNoPaymentAcceptedLiteral.Visible = false;
				uxPaymentPanel.Visible = true;
				PersonalInformation.RenterId = Id.Value;
				SelectPaymentType.RenterId = Id.Value;
				ResidentBillingInformation.RenterId = Id.Value;
				PaymentAmounts.RenterId = Id.Value;
				PaymentAmounts.PaymentTypes = SelectPaymentType;
				AutoPayTile.RenterId = Id.Value;
				TextToPayTile.RenterId = Id.Value;
			}

            //CMallory - Task 00554 - Added to prevent signing a renter up for Pay By Text from the Admin Portal.
            var Property = EfxFramework.Property.GetPropertyByRenterId(Id.Value);
            if (Property.DisablePayByText)
            {
                TextToPayContainer.Visible = false;
                OrBlock.Visible = false;
            }
        }

        private void SetupPaymentPayNowButton()
        {
            int? RenterId = HttpContext.Current.User.Identity.Name.ToNullInt32();

            if (!RenterId.HasValue || RenterId.Value < 1)
                BasePageV2.SafePageRedirect("~/Account/Login.aspx");

            EfxFramework.Property selectedProperty = EfxFramework.Property.GetPropertyByRenterId(RenterId.Value);
            if (string.IsNullOrEmpty(selectedProperty.RentalDepositBankAccountNumber) || string.IsNullOrEmpty(selectedProperty.RentalDepositBankRoutingNumber))
            {
                PaymentSaveButton.Enabled = false;
                PaymentSaveButton.Visible = false;
                PaymentPayNowButton.Enabled = false;
                PaymentPayNowButton.Visible = false;
            }
            else
            {
                PaymentSaveButton.Enabled = true;
                PaymentSaveButton.Visible = true;
                PaymentPayNowButton.Enabled = true;
                PaymentPayNowButton.Visible = true;
            }
        }
        //cakel: BUGID000133 - Adds to database when checked or unchecked
        protected void TermsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            string _user = Page.User.Identity.Name.ToString();
            string CheckedStatus = "";
            if (TermsCheckBox.Checked == true)
            {
                CheckedStatus = "Accepted";
            }
            else
            {
                CheckedStatus = "Unaccepted";
            }

            RPO.ActivityLog AL = new RPO.ActivityLog();
            AL.WriteLog(_user, "User Click Terms and Agreements - " + CheckedStatus, (short)LogPriority.LogAlways);
        }

        //CMallory - Task 00601 - Added temporary text to button.
        protected void PaymentPayNowButton_Click(object sender, EventArgs e)
        {
            PaymentPayNowButton.Text = "Payment Processing, please wait...";

            //CMallory - Task 0090  - Added If/Else If logic below.
            if (TextToPayTile.IsOn && !AutoPayTile.IsOn)
            {
                MessageBoxText("PayByText Reminders have been set up, you will receieve a payment reminder by text on the " + TextToPayTile.DayOfTheMonthDropdown.SelectedValue + " of each month.");
            }

            else if (AutoPayTile.IsOn && !TextToPayTile.IsOn)
            {
                if (AutoPayTile.PaymentTypeDropdown.SelectedValue == "1")
                {
                    MessageBoxText("AutoPay setup has been completed. You will receive a security validation email on the " + AutoPayTile.DayOfTheMonthDropdown.SelectedValue + " of each month, once confirmed your payment will process automatically.");
                }

                else if (AutoPayTile.PaymentTypeDropdown.SelectedValue == "2")
                {
                    MessageBoxText("AutoPay setup has been completed. Your payments will automatically process by E-Check on the " + AutoPayTile.DayOfTheMonthDropdown.SelectedValue + " of each month.");

                }
            }

            else if (AutoPayTile.IsOn && TextToPayTile.IsOn)
            {
                MessageBoxText("PayByText Reminders have been set up, you will receieve a payment reminder by text on the " + TextToPayTile.DayOfTheMonthDropdown.SelectedValue + " of each month.");
                if (AutoPayTile.PaymentTypeDropdown.SelectedValue == "1")
                {
                    MessageBoxText("AutoPay setup has been completed. You will receive a security validation email on the " + AutoPayTile.DayOfTheMonthDropdown.SelectedValue + " of each month, once confirmed your payment will process automatically.");
                }

                else if (AutoPayTile.PaymentTypeDropdown.SelectedValue == "2")
                {
                    MessageBoxText("AutoPay setup has been completed. Your payments will automatically process by E-Check on the " + AutoPayTile.DayOfTheMonthDropdown.SelectedValue + " of each month.");
                }
            }

            PaymentPayNowButton.Text = "Pay Now";
        }

        //CMallory - Task 0090 - Added utility method to assist with the PaymentPayNowButton_Click method.
        private void MessageBoxText(string text)
        {
            MessageBox.Show(text, "Important Note", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }
    }
}