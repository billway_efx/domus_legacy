﻿using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using System;
using System.Collections.Generic;
using RPO;
using System.Data.SqlClient;
using System.Data;

namespace Efx.EfxResidents
{
    public partial class Default : BasePageV2, IAnnouncementsHome
    {
        private AnnouncementsHomePresenter _Presenter;

        public List<EfxFramework.Announcement> PropertyManagerAnnouncementDataSource { set { PropertyManagerAnnouncements.DataSource = value; PropertyManagerAnnouncements.DataBind(); } }
        public List<EfxFramework.Announcement> RpoAnnouncementDataSource { set { RPOAnnouncements.DataSource = value; RPOAnnouncements.DataBind(); } }
        public string ZipCode { get; set; }
        public string CityState { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new AnnouncementsHomePresenter(this);

            if (!Page.IsPostBack)
            {
                _Presenter.InitializeValues();

                //Salcedo - 2/11/2015 - task # 00366 - added ability to use/not use native maintenance request system
                int RenterId = 0;
                if (User.Identity.IsAuthenticated)
                {
                    if (User.Identity.Name != "0")
                    {
                        RenterId = Convert.ToInt32(User.Identity.Name);
                        SqlDataReader reader = MiscDatabase.GetPropertyMaintenanceSystemForRenter(RenterId);
                        reader.Read();
                        if (reader.HasRows)
                        {
                            if ((bool)reader["UseNativeMaintenanceSystem"])
                            {
                                MaintenanceVisibility.Visible = true;
                            }
                            else
                            {
                                if ((bool)reader["UseExternalMaintenanceSystem"] && reader["ExternalMaintenanceSiteUrl"].ToString() != "")
                                {
                                    MaintenanceVisibility.Visible = true;
                                }
                            }
                            reader.Close();
                        }
                        else
                        {
                            MaintenanceVisibility.Visible = true;
                        }
                    }
                }
            }

            //cakel: BUGID000133
            string _user = Page.User.Identity.Name.ToString();
            RPO.ActivityLog AL = new RPO.ActivityLog();
            AL.WriteLog(_user, "Resident Site Home Page", (short)LogPriority.LogAlways);


        }
    }
}