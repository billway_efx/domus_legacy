﻿using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Web;
using System.Web.UI.WebControls;

namespace Efx.EfxResidents
{

    public partial class PaymentStatus : BasePageV2, IPaymentStatus
    {
        private PaymentStatusPresenter _Presenter;

        //cakel: 00407
        public decimal FeeTotal;
        public decimal _rent;

        public int UserId
        {
            get { return !string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? HttpContext.Current.User.Identity.Name.ToInt32() : 0; }
        }

        public string TransactionId
        {
            get { return Request.QueryString["TransactionId"]; }
            set { TransactionIdLabel.Text = value; }
        }

        public string RentAmount { set { RentAmountLabel.Text = value; } }
        public string ConvenienceFee { set { ConvenienceFeeLabel.Text = value; } }
        public string PaymentTotal { set { TotalPaymentAmountLabel.Text = value; } }
        public string PropertyName { set { PropertyNameLabel.Text = value; } }
        public string PropertyAddress { set { PropertyAddressLabel.Text = value; } }
        public string PropertyCityStateZip { set { PropertyCityStateZipLabel.Text = value; } }
        public string FirstName { set { FirstNameLabel.Text = value; } }
        public string MiddleName { set { MiddleNameLabel.Text = value; } }
        public string LastName { set { LastNameLabel.Text = value; } }
        public string Suffix { set { SuffixLabel.Text = value; } }
        public string BillingAddress { set { BillingAddressLabel.Text = value; } }
        public string BillingCityStateZip { set { BillingCityStateZipLabel.Text = value; } }
        public string PhoneNumber { set { PhoneNumberLabel.Text = value; } }
        public string PaymentMethod { set { PaymentMethodLabel.Text = value; } }
        public EventHandler PrintClicked { set { PrintButton.Click += value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new PaymentStatusPresenter(this);

            if (!Page.IsPostBack)
            {
                _Presenter.InitializeValues();
                PopulatePaymentDetails();
            }
                
        }

        //cakel: TASK 00407
        public void PopulatePaymentDetails()
        {
           

            var p = _Presenter.GetPaymentForView();
            var r = new EfxFramework.Renter(p.RenterId);

            var RenterLease = EfxFramework.Lease.GetRenterLeaseByRenterId(r.RenterId); 
            //Get the list for Lease if Any 
            GridView1.DataSource = EfxFramework.LeaseFee.GetFeesByLeaseId(RenterLease.LeaseId);
            GridView1.DataBind();

            var L = new EfxFramework.Lease(RenterLease.LeaseId);

            RentAmountLabel.Text = L.RentAmount.ToString("C");


        }

        //cakel: TASK 00407
        protected void GridView1_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                string FeeString = e.Row.Cells[1].Text;
                FeeString = FeeString.Replace("$", "");

                decimal Fees = Decimal.Parse(FeeString);

                    FeeTotal += Fees;
                
            }

        }


    }



}