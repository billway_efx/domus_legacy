﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageAdder.ascx.cs" Inherits="Efx.EfxResidents.UserControls.ImageAdder" %>
<div class="formGroup">
    <asp:FileUpload ID="PhotoUpload" runat="server" AllowMultiple="False" CssClass="buttonInline" />
    <asp:LinkButton ID="AddPhotoButton" runat="server" CssClass="orangeButton imagePickerButton fr2BColor addButton" CausesValidation="False">Add</asp:LinkButton>
    <asp:ListView ID="PhotoListView" runat="server">
        <LayoutTemplate>
            <ul style="list-style-type: none;">
                <li id="itemPlaceholder" runat="server" />
            </ul>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="imageContainer bgCornersAndShadows">
                <li class="imageSection">
                    <div class="mobileImageHolder">
                        <asp:Image ID="Photo" runat="server" CssClass="resizedImage" ImageUrl='<%# Bind("PhotoUrl") %>' AlternateText='<%# Bind("Description") %>'/>
                    </div>
                    <asp:LinkButton ID="DeletePhotoButton" runat="server" CssClass="orangeButton buttonsCornersAndShadow imageDelete fr2BColor" CausesValidation="False">Delete</asp:LinkButton>
                </li>
            </div>
        </ItemTemplate>
    </asp:ListView>
</div>