﻿using System;
using System.Web;
using System.Web.UI;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using EfxFramework;

namespace Efx.EfxResidents.UserControls
{
    public partial class AccountBalanceSection : UserControl, IAccountBalanceSection
    {
        private AccountBalanceSectionPresenter _Presenter;

        public string RentLabel { set { AccountBalanceRentLabel.Text = value; } }
        public string WillAutoPayLabel { set { AccountBalanceWillAutoPayLabel.Text = value; } }
        public bool WillAutoPayVisible { set { AccountBalanceWillAutoPayLabel.Visible = value; } }
        public string PayLabel { set { AccountBalancePayDateLabel.Text = value; } }
        public string SetUpAutoPay { set { SetupAutopayButton.PostBackUrl = value; } }
        public string SetPayment { set { AccountBalancePayRentButton.PostBackUrl = value; } }

        public int RenterId
        {
            get
            {
                var Id = HttpContext.Current.User.Identity.Name.ToNullInt32();
                if (!Id.HasValue || Id.Value < 1)
                {
                    BasePageV2.SafePageRedirect("~/Account/Login.aspx");
                    return 0;
                }
                return Id.Value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new AccountBalanceSectionPresenter(this);
			SetPayRentButtonVisibility();
            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }

		/// <summary>
		/// We can't allow payments if the Resident.AcceptedPaymentTypeId is equal to 4
		/// </summary>
		private void SetPayRentButtonVisibility()
		{
			var Resident = new Renter(RenterId);

			if (Resident.AcceptedPaymentTypeId == 4)
			{
				AccountBalancePayRentButton.Visible = false;
				SetupAutopayButton.Visible = false;
			}
			else
			{
				AccountBalancePayRentButton.Visible = true;
				SetupAutopayButton.Visible = true;
			}
		}
    }
}