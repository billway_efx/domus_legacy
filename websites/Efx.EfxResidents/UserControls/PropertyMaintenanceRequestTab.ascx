﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyMaintenanceRequestTab.ascx.cs" Inherits="Efx.EfxResidents.UserControls.PropertyMaintenanceRequestTab" %>

<div class="panel maintenanceTab" id="Tab2" style="width: 972px; left: 1944px; " >
    <div class="panel_content">
        <div class="standardPadding">
            <h3 class="fr1Color">Maintenance Requests</h3>
            <!--**(Maintenance Type, Maintenance Priority)**-->
            <div class="formRow">
                <div class="maintenanceType hideSearch">
                    <span class="formInlineLable fr1Color">Type:</span>
                    <asp:DropDownList ID="MaintenanceTypeDropDown" runat="server" CssClass="chzn-select maintenanceTypeBox" />
                </div>

                <div class="maintenancePriority hideSearch">
                    <span class="formInlineLable fr1Color">Priority:</span>
                    <asp:DropDownList ID="MaintenancePriorityDropDown" runat="server" CssClass="chzn-select maintenancePriorityBox" />
                </div>

                <div class="maintenancePriority hideSearch">
                    <span class="formInlineLable fr1Color">Permission To Enter</span>

                    <!--cakel: BUGID00087 added drop down-->
                    <asp:DropDownList ID="MaintenancePermissionDropDown" runat="server" CssClass="chzn-select maintenancePriorityBox">
                        <asp:ListItem>Call First</asp:ListItem>
                        <asp:ListItem>Yes - Anytime</asp:ListItem>
                        <asp:ListItem>Yes - Morning</asp:ListItem>
                        <asp:ListItem>Yes - Afternoon</asp:ListItem>
                        <asp:ListItem>Yes - Evening</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>

            <!--**(Check Boxing it)**-->
            <div class="formRow checkBoxBlock">
                <div class="formFullWidth">
                    <asp:CheckBox ID="CcMeCheckBox" runat="server" CssClass="checkBoxText fr1Color" Text="CC Me" />
                </div>
            </div>

            <div class="formRow">
                <div class="formFullWidth">
                    <span class="formLabel fr1Color">Please Describe the Problem Below and Include Contact Information</span>
                    <asp:TextBox ID="ProblemDescriptionTextBox" TextMode="MultiLine" class="maintenanceMessage messageBox" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="DescriptionRequiredValidator" CssClass="frBlack"
                        runat="server"
                        ErrorMessage="You must enter details for the problem you're experiencing before sending the request."
                        Display="None"
                        ControlToValidate="ProblemDescriptionTextBox" />
                </div>
                <!--cakel: BUGID000168 - added div with text for disclaimer -->
                <!--cakel: BUGID000243 - Updated div with text for disclaimer -->
                <div class="formFullWidth">
                    <span id="propertyNotes" class="formLabel fr1Color">**Please Note - <span style="color: #CC3300; font-weight: bold;">DO NOT USE FOR EMERGENCIES! </span> - If this is an after-hours maintenance emergency, please call the property office immediately. If this is a life threatening or health related emergency, please dial 911**
                    </span>



                </div>
            </div>
            <div class="bottomButtonHolder alignRight">
                <asp:LinkButton ID="SendMaintenanceMessage" runat="server" CssClass="orangeButton buttonInline fr2BColor" CausesValidation="false">Send</asp:LinkButton>
            </div>
        </div>
    </div>
</div>
