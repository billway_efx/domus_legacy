﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using Mb2x.ExtensionMethods;
using System;
using System.Web;
using System.Web.UI;

namespace Efx.EfxResidents.UserControls
{
    public partial class LeasingInfoTab : UserControl, IPropertyLeasingInfoTab
    {
        private PropertyLeasingInfoTabPresenter _Presenter;

        public int UserId
        {
            get { return !string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? HttpContext.Current.User.Identity.Name.ToInt32() : 0; }
        }
        public string PropertyNameLabelText { set { PropertyNameLabel.Text = value; } }
        public string PropertyAddressLabelText { set { PropertyAddressLabel.Text = value; } }
        public string PropertyCityStateZipLabelText { get { return PropertyCityStateZipLabel.Text; } set { PropertyCityStateZipLabel.Text = value; } }
        public string LeaseStartDateLabelText { set { LeaseStartDateLabel.Text = value; } }
        public string LeaseEndDateLabelText { set { LeaseEndDateLabel.Text = value; } }
        public string DueDateMonthLabelText { set { DueDateMonthLabel.Text = value; } }
        public string RentAmountLabelText { set { RentAmountLabel.Text = value; } }


        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new PropertyLeasingInfoTabPresenter(this);

            if (!Page.IsPostBack)
            _Presenter.InializeValues();            
        }
    }
}