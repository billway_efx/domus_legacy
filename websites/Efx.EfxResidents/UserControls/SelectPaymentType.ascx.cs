﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mb2x.ExtensionMethods;
using EfxFramework;

namespace Efx.EfxResidents.UserControls
{
    public partial class SelectPaymentType : UserControl, ISelectPaymentType
    {
        private SelectPaymentTypePresenter _Presenter;

        public int RenterId { get; set; }
        public EventHandler PaymentTypeChanged { set { PaymentMethodComboBox.SelectedIndexChanged += value; } }
        public string PaymentTypeSelectedValue { get { return PaymentMethodComboBox.SelectedValue; } set { PaymentMethodComboBox.SelectedValue = value; } }
        public int PaymentTypeSelectedIndex { set { SelectedType.ActiveViewIndex = value; } }
        public ICreditCardDetails CreditCardDetailsControl { get { return CreditCardDetails; } }
        public IElectronicCheckDetails ECheckDetailsControl { get { return ElectronicCheckDetails; } }
        public bool IsApplicant { get; set; }
		public bool AcceptCCPayments { get; set; }
		public bool AcceptACHPayments { get; set; }
		public bool PNMEnabled { get; set; }
		public int PropertyId { get; set; }

		public EfxFramework.Property currentProperty
		{
			get { return EfxFramework.Property.GetPropertyByRenterId(RenterId); }
		}

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new SelectPaymentTypePresenter(this);
			_Presenter.LinkPayNowButton = (LinkButton)this.Parent.FindControl("PaymentPayNowButton");
			_Presenter.LinkSaveButton = (LinkButton)this.Parent.FindControl("PaymentSaveButton");

            if (!IsPostBack)
                _Presenter.InitializeValues();
        }

        public void InitializeCreditCardExpiration()
        {
            _Presenter.InitializeExpirationYears();
        }

		protected override void OnPreRender(EventArgs e)
		{
			try
			{
				base.OnPreRender(e);

				if (currentProperty != null)
				{
					AcceptACHPayments = currentProperty.AcceptACHPayments;
					AcceptCCPayments = currentProperty.AcceptCCPayments;
					PNMEnabled = currentProperty.PNMEnabled;
					setControlsBasedOnProperty();
					setControlsBasedOnRenter();
				}
			}
			catch (Exception ex)
			{
				EfxFramework.Logging.ExceptionLogger.LogException(ex);
			}
		}

		private void setControlsBasedOnRenter()
		{
			//get the renter
			var Resident = new Renter(RenterId);

			//remove the E-Check option if Accepted Payment type is cash equivalent
			if (Resident.AcceptedPaymentTypeId == 3)
			{
				ListItem acceptACHItem = PaymentMethodComboBox.Items.FindByValue("2");
				if (acceptACHItem != null)
				{
					PaymentMethodComboBox.Items.Remove(acceptACHItem);
				}
			}
		}

		private void setControlsBasedOnProperty()
		{
			ListItem acceptCCItem = PaymentMethodComboBox.Items.FindByValue("1");
			if (acceptCCItem != null && !AcceptCCPayments)
			{
				PaymentMethodComboBox.Items.Remove(acceptCCItem);
			}
			else if (acceptCCItem == null && AcceptCCPayments)
			{
				ListItem acceptCCLItem = new ListItem();
				acceptCCLItem.Text = "Credit Card";
				acceptCCLItem.Value = "1";
				PaymentMethodComboBox.Items.Insert(0, acceptCCLItem);
			}

			ListItem acceptACHItem = PaymentMethodComboBox.Items.FindByValue("2");
			if (acceptACHItem != null && !AcceptACHPayments)
			{
				PaymentMethodComboBox.Items.Remove(acceptACHItem);
			}
			else if (acceptACHItem == null && AcceptACHPayments)
			{
				ListItem acceptACHLItem = new ListItem();
				acceptACHLItem.Text = "E-Check";
				acceptACHLItem.Value = "2";
				PaymentMethodComboBox.Items.Add(acceptACHLItem);
			}

			ListItem acceptPayByCashItem = PaymentMethodComboBox.Items.FindByValue("3");
			if (acceptPayByCashItem != null && !PNMEnabled)
			{
				PaymentMethodComboBox.Items.Remove(acceptPayByCashItem);
			}
			else if (acceptPayByCashItem == null && PNMEnabled)
			{
				ListItem acceptPayByCashLItem = new ListItem();
				acceptPayByCashLItem.Text = "RentByCash&#8482;";
				acceptPayByCashLItem.Value = "3";
				PaymentMethodComboBox.Items.Add(acceptPayByCashLItem);
			}

			if (PaymentMethodComboBox.Items.Count > 0)
			{
				SelectedType.Visible = true;
				int selectedValue = 0;
				if (Int32.TryParse(PaymentMethodComboBox.SelectedValue, out selectedValue))
				{
					if (selectedValue == 3)
					{
						LinkButton PayNowButton = (LinkButton)this.Parent.FindControl("PaymentPayNowButton");
						if (PayNowButton != null)
						{
							PayNowButton.Text = "Create PayNearMe Ticket";
						}
					}
					else
					{
						LinkButton PayNowButton = (LinkButton)this.Parent.FindControl("PaymentPayNowButton");
						if (PayNowButton != null)
						{

                            if (PayNowButton.Text != "Payment Processing, please wait...")
                            {
                                PayNowButton.Text = "Pay Now";
                            }
							
						}
					}
					SelectedType.ActiveViewIndex = selectedValue - 1;
				}
			}
			else
			{
				SelectedType.Visible = false;
			}

			if (!AcceptACHPayments && !AcceptCCPayments && !PNMEnabled)
			{
				this.Visible = false;
				Literal payInfoLiteral = (Literal)this.Parent.FindControl("uxPaymentInformationLiteral");
				if (payInfoLiteral != null)
				{
					payInfoLiteral.Text = "<div class=\"bottomMessage\"><p>*This property is not configured to accept payments.</p></div>";
					payInfoLiteral.Visible = true;
				}
				
				LinkButton payNowButton = (LinkButton)this.Parent.FindControl("PaymentPayNowButton");
				if (payNowButton != null)
				{
					payNowButton.Enabled = false;
					payNowButton.Visible = false;
				}

				LinkButton paySaveButton = (LinkButton)this.Parent.FindControl("PaymentSaveButton");
				if (paySaveButton != null)
				{
					paySaveButton.Enabled = false;
					paySaveButton.Visible = false;
				}
				
			}
			else
			{
				this.Visible = true;
				Literal payInfoLiteral = (Literal)this.Parent.FindControl("uxPaymentInformationLiteral");
				if (payInfoLiteral != null)
				{
					payInfoLiteral.Visible = false;
				}

				LinkButton payNowButton = (LinkButton)this.Parent.FindControl("PaymentPayNowButton");
				if (payNowButton != null)
				{
                    //CMallory - Task 00601 - Added if statement to keep the button from re-enabling after it's clicked.
                    if (payNowButton.Text != "Payment Processing, please wait...")
                    {
                        payNowButton.Enabled = true;
                    }	
					payNowButton.Visible = true;
				}

				LinkButton paySaveButton = (LinkButton)this.Parent.FindControl("PaymentSaveButton");
				if (paySaveButton != null)
				{
					if (PaymentMethodComboBox.SelectedValue != "3")
					{
						paySaveButton.Enabled = true;
						paySaveButton.Visible = true;
					}
					else
					{
						paySaveButton.Enabled = false;
						paySaveButton.Visible = false;
					}
				}
			}
		}
    }
}