﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Web.UI;

namespace Efx.EfxResidents.UserControls
{
    public partial class PersonalInformation : UserControl, IPersonalInformation
    {
        private PersonalInformationPresenter _Presenter;

        public int RenterId { get; set; }
        public string FirstNameText { get { return FirstNameTextBox.Text; } set { FirstNameTextBox.Text = value; } }
        public string MiddleNameText { get { return MiddleNameTextBox.Text; } set { MiddleNameTextBox.Text = value; } }
        public string LastNameText { get { return LastNameTextBox.Text; } set { LastNameTextBox.Text = value; } }
        public string SuffixText { get { return SuffixTextBox.Text; } set { SuffixTextBox.Text = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new PersonalInformationPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}