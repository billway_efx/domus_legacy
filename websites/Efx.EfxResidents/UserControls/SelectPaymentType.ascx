﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectPaymentType.ascx.cs" Inherits="Efx.EfxResidents.UserControls.SelectPaymentType" %>
<h4 class="fr2Color">Payment Method</h4>
<!--**First Row (Payment Method) This drop down option determines which blcok is displayed (creditCard, eCheck, or textToPay**--> 
<div class="formRow">
    <div class="formFullWidth hideSearch">
        <span class="formLabel fr1Color">Payment Method</span>
        <asp:DropDownList ID="PaymentMethodComboBox" runat="server" CssClass="chzn-select" AutoPostBack="True">
            <asp:ListItem Value="1" Text="Credit Card" Selected="True"></asp:ListItem>
            <asp:ListItem Value="2" Text="E-Check"></asp:ListItem>
			<asp:ListItem Value="3" Text="RentByCash&#8482;"></asp:ListItem>
        </asp:DropDownList>
    </div>
</div>
<asp:MultiView ID="SelectedType" runat="server">
    <asp:View ID="CreditCardView" runat="server">
        <uc1:CreditCardDetails ID="CreditCardDetails" runat="server" />
    </asp:View>
    <asp:View ID="EcheckView" runat="server">
        <uc1:ElectronicCheckDetails ID="ElectronicCheckDetails" runat="server" />
    </asp:View>
	<asp:View ID="RentByCash" runat="server">
    </asp:View>
</asp:MultiView>