﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountBalanceSection.ascx.cs" Inherits="Efx.EfxResidents.UserControls.AccountBalanceSection" %>
    <h3 class="orangeText fr2Color">Account Balance</h3>
    <!-- Rent column -->
    <div class="even2Column textCenter">
        <h4 class="greenText fr1Color">Amount Due</h4>
        <asp:Label ID="AccountBalanceRentLabel" runat="server" CssClass="displayFont frBlack" />
        <div class="homepageButton payRentButton" style="padding-top: 10px; ">
            <asp:LinkButton ID="AccountBalancePayRentButton" runat="server" CssClass="orangeButton fr2BColor" PostBackUrl="~/Payments.aspx">Pay Rent</asp:LinkButton>
        </div>
    </div>

    <!-- Due Date column -->
    <div class="even2Column textCenter">
        <h4 class="greenText fr1Color">Due Date</h4>
        <asp:Label ID="AccountBalanceWillAutoPayLabel" runat="server" CssClass="subText" Text="Will Auto Pay On" Visible="false"/>
        <asp:Label ID="AccountBalancePayDateLabel" runat="server" CssClass="displayFont frBlack"/>
        <div class="homepageButton setupAutoPay" style="padding-top: 10px; ">
            <asp:LinkButton ID="SetupAutopayButton" runat="server" CssClass="orangeButton fs12a fr2BColor" PostBackUrl="~/Payments.aspx">Setup AutoPay</asp:LinkButton>
        </div>
    </div>