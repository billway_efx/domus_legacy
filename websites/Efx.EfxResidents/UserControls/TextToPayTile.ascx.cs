﻿using System.Web;
using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;
using EfxFramework.ViewPresentation.Presenters.Factories;
using EfxFramework.ViewPresentation.Presenters.UserControls.PaymentTiles;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using EfxFramework;

namespace Efx.EfxResidents.UserControls
{
    public partial class TextToPayTile : UserControl, ITextToPayTile
    {
        private PaymentTilePresenterBase _Presenter;

        public int RenterId { get; set; }
        public EventHandler OnButtonClick { set { TextToPayOnButton.Click += value; } }
        public EventHandler OffButtonClick { set { TextToPayOffButton.Click += value; } }
        public string OnButtonCssClass { get { return TextToPayOnButton.CssClass; } set { TextToPayOnButton.CssClass = value; } }
        public string OffButtonCssClass { get { return TextToPayOffButton.CssClass; } set { TextToPayOffButton.CssClass = value; } }
        public DropDownList DayOfTheMonthDropdown { get { return TextRemindersComboBox; } }
        public DropDownList PaymentTypeDropdown { get { return TextToPayAccountComboBox; } }
        public string MobileNumberText { get { return TextToPayMobileNumberTextBox.Text; } set { TextToPayMobileNumberTextBox.Text = value; } }
        public bool IsOn { get; set; }
        public ServerValidateEventHandler RequiredMobileNumberValidation { set { RequiredTextToPayMobileNumberValidation.ServerValidate += value; } }
        public string MobileNumberClientId { get { return TextToPayMobileNumberTextBox.ClientID; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = PaymentTilePresenterFactory.GetPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();

			EfxFramework.Property currentProperty = EfxFramework.Property.GetPropertyByRenterId(RenterId);

            

			if (currentProperty != null && !currentProperty.AcceptACHPayments)
			{
				ListItem achItem = PaymentTypeDropdown.Items.FindByText("E-Check");
				achItem.Enabled = false;
			}
			else
			{
				ListItem achItem = PaymentTypeDropdown.Items.FindByText("E-Check");
                //cakel: BUGID000191 - Code was generating an error due to function looking for a control that did not exist.
                if (achItem != null)
                {
                    achItem.Enabled = true;
                }
			}
			if (currentProperty != null && !currentProperty.AcceptCCPayments)
			{
				ListItem ccItem = PaymentTypeDropdown.Items.FindByText("Credit Card");
				ccItem.Enabled = false;
			}
			else
			{
				ListItem ccItem = PaymentTypeDropdown.Items.FindByText("Credit Card");
				ccItem.Enabled = true;
			}

			setControlsBasedOnRenter();
        }

		private void setControlsBasedOnRenter()
		{
			//get the renter
			var Resident = new Renter(RenterId);

			//remove the E-Check option if Accepted Payment type is cash equivalent
			if (Resident.AcceptedPaymentTypeId == 3)
			{
				ListItem acceptACHItem = TextToPayAccountComboBox.Items.FindByValue("2");
				if (acceptACHItem != null)
				{
					TextToPayAccountComboBox.Items.Remove(acceptACHItem);
				}
			}
            //cakel: BUGID000191
            if (Resident.AcceptedPaymentTypeId == 4)
            {
                TextToPayAccountComboBox.Items.Clear();
            }

		}
    }
}