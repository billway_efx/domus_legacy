﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TextToPayTile.ascx.cs" Inherits="Efx.EfxResidents.UserControls.TextToPayTile" %>
<h5 class="fr2Color">PayByText&#0153</h5>
<!--Buttons -- inactive state is displayed with the class gratOffButton, active state is orangeButton -->
<asp:LinkButton ID="TextToPayOnButton" runat="server" CssClass="grayOffButton buttonInline" CausesValidation="false">On</asp:LinkButton>
<asp:LinkButton ID="TextToPayOffButton" runat="server" CssClass="grayOffButton buttonInline fr2BColor" CausesValidation="false">Off</asp:LinkButton>
<!--Payment Processing Date -->
<div class="formRow rowSpacingAdjust">
    <div class="formFullWidth">
        <span class="formLabel formLabelTextAdjust fr1Color">Mobile Number</span>
        <div class="formFullWidth">
            <asp:TextBox ID="TextToPayMobileNumberTextBox" runat="server" MaxLength="10"></asp:TextBox>
        </div>
    </div>
</div>
<!--Instructions -- read only -->
<div class="formRow">
    <div class="formFullWidth">
        <span class="formLabel formLabelTextAdjust fr1Color">Instructions</span>
        <p class="frBlack">Text "PayMyRent" to 57682</p>
    </div>
</div>
<div class="formRow paymentAmountMargin hideSearch">
    <div class="formFullWidth hideSearch">
        <span class="formLabel formLabelTextAdjust fr1Color">PayByText&#0153 Account</span>
        <asp:DropDownList ID="TextToPayAccountComboBox" runat="server" CssClass="chzn-select">
            <asp:ListItem Value="1" Text="Credit Card" Selected="True"></asp:ListItem>
            <asp:ListItem Value="2" Text="E-Check"></asp:ListItem>
        </asp:DropDownList>
    </div>
</div>
<div class="formRow rowSpacingAdjust">
    <div class="formFullWidth">
        <span class="formLabel formLabelTextAdjust fr1Color">Text Reminders</span>
        <div class="formAutoPayDate">
            <asp:DropDownList ID="TextRemindersComboBox" runat="server" CssClass="chzn-select">
                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                <asp:ListItem Value="8" Text="8"></asp:ListItem>
                <asp:ListItem Value="9" Text="9"></asp:ListItem>
                <asp:ListItem Value="10" Text="10"></asp:ListItem>
                <asp:ListItem Value="11" Text="11"></asp:ListItem>
                <asp:ListItem Value="12" Text="12"></asp:ListItem>
                <asp:ListItem Value="13" Text="13"></asp:ListItem>
                <asp:ListItem Value="14" Text="14"></asp:ListItem>
                <asp:ListItem Value="15" Text="15"></asp:ListItem>
                <asp:ListItem Value="16" Text="16"></asp:ListItem>
                <asp:ListItem Value="17" Text="17"></asp:ListItem>
                <asp:ListItem Value="18" Text="18"></asp:ListItem>
                <asp:ListItem Value="19" Text="19"></asp:ListItem>
                <asp:ListItem Value="20" Text="20"></asp:ListItem>
                <asp:ListItem Value="21" Text="21"></asp:ListItem>
                <asp:ListItem Value="22" Text="22"></asp:ListItem>
                <asp:ListItem Value="23" Text="23"></asp:ListItem>
                <asp:ListItem Value="24" Text="24"></asp:ListItem>
                <asp:ListItem Value="25" Text="25"></asp:ListItem>
                <asp:ListItem Value="26" Text="26"></asp:ListItem>
                <asp:ListItem Value="27" Text="27"></asp:ListItem>
                <asp:ListItem Value="28" Text="28"></asp:ListItem>
                <asp:ListItem Value="29" Text="29"></asp:ListItem>
                <asp:ListItem Value="30" Text="30"></asp:ListItem>
                <asp:ListItem Value="31" Text="31"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <span class="formAsideText frBlack">of each month</span>
    </div>
</div>

<asp:CustomValidator ID="RequiredTextToPayMobileNumberValidation"
                         runat="server"
                         ErrorMessage="Please enter a Valid Mobile Number, no special characters included"
                         Display="None"
                        />


