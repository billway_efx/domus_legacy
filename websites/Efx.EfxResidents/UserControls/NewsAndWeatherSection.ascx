﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsAndWeatherSection.ascx.cs" Inherits="Efx.EfxResidents.UserControls.NewsAndWeatherSection" %>


<div class="newsHolder">
    <h5 class="fr1Color">News</h5>
    <!-- News -->
        <asp:ListView ID="News" runat="server">
            <LayoutTemplate>
                <article>
                    <div id="itemPlaceHolder" runat="server"/>
                </article>
            </LayoutTemplate>
            <ItemTemplate>
                <article>
                    <div>
                        <asp:Label ID="ArticleTitle" CssClass="fr1Color" runat="server" Text='<%# Bind("Title") %>'/><br />
                        <asp:Label ID="ArticlePostDate" CssClass="fr2Color" runat="server" Text='<%# Bind("FormattedDate") %>'/><br />
                        <asp:Label ID="ArticleDescription" CssClass="frBlack" runat="server" Text='<%# Bind("Description") %>'/><br />
                        <asp:HyperLink ID="ArticleHyperLink" CssClass="fr1Color" runat="server" Target="_blank" Text="Read More..." NavigateUrl='<%# Bind("Url") %>'/><br />
                    </div>
                </article>
            </ItemTemplate>
        </asp:ListView>
    </div>
    <div class="weatherHolder">
        <h5 class="fr1Color">Weather</h5>
            <!-- Weather -->
        
                <section class="weatherContent">
    <div>
        <%-- cakel: Added weather location --%>
        <span class="weatherlabel fr1Color">Location:</span>
        <span class="weatherResult fr2Color"><asp:Label ID="LocationLabel" runat="server" /></span>
        <span class="weatherLabel fr1Color">Temperature:</span>
        <span class="weatherResult fr2Color"><asp:Label ID="Temperature" runat="server" /></span>
    </div>
            <div>
                <span class="weatherLabel fr1Color">Humidity:</span> 
                <span class="weatherResult fr2Color"><asp:Label ID="Humidity" runat="server" /></span>  
            </div>
            <div class="mainWeatherSection">
                <span class="MainweatherResult">
                    <span class="weatherIcon">
                        <asp:Image ID="WeatherImage" runat="server"/>
                    </span>
                    <span class="mainweathertext frBlack">
                        <asp:Label ID="Weather" runat="server" />
                    </span>
                </span>  
            </div>
            <div>
                <span class="weatherLabel fr1Color">Wind Speed (Low):</span> 
                <span class="weatherResult"><asp:Label ID="Wind" runat="server" /></span>  
            </div>
            <div>
                <span class="weatherLabel fr1Color">Wind Speed (High):</span> 
                <span class="weatherResult"><asp:Label ID="WindGusting" runat="server"/></span>  
            </div>
            <div></div>
        </section>      
            <!-- Earth, wind, fire, water, heart! Captain Planet! -->
    </div>


