﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogPostListing.ascx.cs" Inherits="Efx.EfxResidents.UserControls.Blog.BlogPostListing" %>
<asp:ListView ID="BlogPostListView" runat="server">
    <LayoutTemplate>
        <ul style="list-style-type: none;">
            <li id="itemPlaceholder" runat="server"></li>
        </ul>
    </LayoutTemplate>
    <ItemTemplate>
        <li>
            <%--Inline styles below.... look out!--%>
            <a id="PostTitleLink" runat="server" style="cursor: pointer;">
            <asp:Label ID="PostLinkLabel" CssClass="fr1Color fs12" runat="server" Text='<%# Bind("BlogTitle") %>' /></a>
            <asp:Literal ID="AnnouncementText"  runat="server" Text='<%# Bind("BlogText") %>' Visible="false" />
            <asp:Literal ID="AnnouncementDateShortDate" runat="server" Text='<%# Bind("AnnouncementDateShortDate") %>' Visible="false" />
            <asp:Literal ID="AnnouncementTitle" runat="server" Text='<%# Bind("BlogTitle") %>' Visible="false" />
        </li>
    </ItemTemplate>
    <EmptyItemTemplate>
        <asp:Label ID="EmptyItemLabel" CssClass="fr1Color" runat="server" Text="No Items to Display" />
    </EmptyItemTemplate>
</asp:ListView>