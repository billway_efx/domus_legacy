﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Efx.EfxResidents.UserControls
{
    public partial class PayerBillingInformation : UserControl, IResidentBillingInformation
    {
        private ResidentBillingInformationPresenter _Presenter;

        public int RenterId { get; set; }
        public EventHandler SameAsMailingChecked { set { SameAsMailingCheckBox.CheckedChanged += value; } }
        public bool IsSameAsMailingChecked { get { return SameAsMailingCheckBox.Checked; } set { SameAsMailingCheckBox.Checked = value; } }
        public string BillingAddressText { get { return BillingAddressTextBox.Text; } set { BillingAddressTextBox.Text = value; } }
        public string CityText { get { return CityTextBox.Text; } set { CityTextBox.Text = value; } }
        public DropDownList StateDropDown { get { return StateDropDownList; } }
        public string ZipText { get { return ZipCodeTextBox.Text; } set { ZipCodeTextBox.Text = value; } }
        public string BillingPhone { get { return PhoneNumberTextBox.Text; } set { PhoneNumberTextBox.Text = value; } }
        public string EmailAddressText { get; set; }
        public string ZipCodeClientId { get { return ZipCodeTextBox.ClientID; } }
        public string PhoneNumberClientId { get { return PhoneNumberTextBox.ClientID; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new ResidentBillingInformationPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}