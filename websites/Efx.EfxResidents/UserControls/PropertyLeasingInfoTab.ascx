﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyLeasingInfoTab.ascx.cs" Inherits="Efx.EfxResidents.UserControls.LeasingInfoTab" %>

<div class="panel leasingTab" style="width: 972px; left: 972px; ">
    <div class="panel_content">
        <div class="standardPadding">
            <h3 class="fr1Color">Lease Info</h3>
                                
            <!-- First column (Lease info and image) -->
            <div class="leaseMainInfoColumn">
                <hgroup>
                    <h4 class="fr2Color">Property</h4>
                    <p><asp:Label ID="PropertyNameLabel" class="frBlack" runat="server" /></p>
                </hgroup>
                <div class="formRow">
                    <div class="formFullWidth rowSpacingAdjust">
                        <span class="formLabel fr2Color">Address</span>
                        <p><asp:Label ID="PropertyAddressLabel" class="frBlack" runat="server" /></p>
                        <p><asp:Label ID="PropertyCityStateZipLabel" class="frBlack" runat="server" /></p>
                        <!--Property Image -->
                        <div>
                            <img src="/Images/Temp/PropertyPlaceholder.png" />
                        </div>
                    </div>
                </div>
            </div>
                                        
            <!-- Second column (General information -- Occupants, Pets, Important Dates, Amounts Due) -->
            <div class="leaseGeneralInfoColumn">
                <!--First Row-->
                <div class="propertyRow">
                    <!--Occupants-->
                    <div class="propertyInteriorColumn occupantsColumn">
                        <h4 class="fr2Color">Occupants</h4>                                           
                        <uc1:OccupantsList runat="server" id="OccupantsList" />
                    </div>
                    <!-- Pets-->
                    <div class="propertyInteriorColumn">
                        <h4 class="fr2Color">Pets</h4>
                        <uc1:PetsList runat="server" id="PetsList" />               
                    </div>
                </div>
                <div class="propertyRow">
                    <!-- Important Dates -->
                    <div class="propertyInteriorColumn">
                        <h4 class="fr2Color">Important Dates</h4>                                           
                        <ul>
                            <li><span class="itemLabel fr1Color">Lease Start Date:</span><asp:Label ID="LeaseStartDateLabel" class="frBlack" runat="server" /></li>
                            <li><span class="itemLabel fr1Color">Lease End Date:</span><asp:Label ID="LeaseEndDateLabel" class="frBlack" runat="server" /></li>
                            <li><span class="itemLabel fr1Color">Due Date of the Month:</span><asp:Label ID="DueDateMonthLabel" class="frBlack" runat="server" /></li>
                        </ul>
                    </div>
                    <div class="propertyInteriorColumn">
                        <h4 class="fr2Color">Amounts Due</h4>                                           
                        <ul class="amountDue fr2Color">
                            <li><span class="itemLabel fr1Color">Rent Amount:</span><asp:Label ID="RentAmountLabel" class="frBlack" runat="server" /></li>
                        </ul>
                                            
                        <h5 class="orangeText fr2Color">Fees</h5>     
                        <uc1:FeesList runat="server" ID="FeesList" />                       
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>