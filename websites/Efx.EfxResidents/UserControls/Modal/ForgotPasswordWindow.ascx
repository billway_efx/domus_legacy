﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ForgotPasswordWindow.ascx.cs" Inherits="Efx.EfxResidents.UserControls.Modal.ForgotPasswordWindow" %>

<div class="modalContentContainer">
    <h3 class="fr1Color">Forgot Your Password?</h3>
    <p class="frBlack">Enter your email address below.</p>
    <div class="modalRow ">
        <span class="formLabel fr1Color">Email Address</span>
        <asp:TextBox ID="EmailTextBox" runat="server" CssClass="ninetyFullWidth"></asp:TextBox>
        <asp:RegularExpressionValidator ID="EmailFormatValidator"
                                        runat="server"
                                        ControlToValidate="EmailTextBox"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ErrorMessage="Invalid Format"
                                        Display="Dynamic" />
    </div>
    <div class="modalButtonHolder">
        <asp:Button ID="SubmitButton" runat="server" CssClass="orangeButton fr2BColor" Text="Submit" CausesValidation="True"></asp:Button>       
    </div>
    <div class="clearFix"></div>
</div>