﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CancellationWindow.ascx.cs" Inherits="Efx.EfxResidents.UserControls.Modal.CancellationWindow" %>
<div class="modalContentContainer confirmationContainer">
    <h4 class="confirmTitleText fr1Color"><asp:Label ID="CancellationHeadingLabel" CssClass="fr1Color" runat="server" Text="Cancellation" /></h4>
    <p><asp:Label ID="CancellationLabel" CssClass="fr1Color" runat="server" Text="Autopay setup has been cancelled" /></p>
    <div class="modalButtonHolderCentered">
        <asp:Button ID="OkButton" runat="server" Text="Okay" CssClass="orangeButton okayButton fr2BColor" />
    </div>
    <div class="clearFix"></div>
</div>