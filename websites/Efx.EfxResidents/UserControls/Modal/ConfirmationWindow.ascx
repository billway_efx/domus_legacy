﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConfirmationWindow.ascx.cs" Inherits="Efx.EfxResidents.UserControls.Modal.ConfirmationWindow" %>
<div class="modalContentContainer confirmationContainer">
    <h4 class="confirmTitleText fr1Color"><asp:Label ID="ConfirmationHeadingLabel" runat="server" /></h4>
    <p><asp:Label ID="ConfirmationLabel" CssClass="fr1Color" runat="server" /></p>
    <div class="modalButtonHolderCentered">
        <asp:Button ID="OkButton" runat="server" Text="Okay" CssClass="orangeButton okayButton fr2BColor" />
    </div>
    <div class="clearFix"></div>
</div>
