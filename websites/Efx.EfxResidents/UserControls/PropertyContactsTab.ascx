﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyContactsTab.ascx.cs" Inherits="Efx.EfxResidents.UserControls.PropertyContactsTab" %>

<div class="panel contactTab" style="width: 972px; left: 0px;">
    <div class="panel_content">
        <div class="standardPadding">
            <h3 class="fr1Color">Contacts</h3>
            <div class="propertyRow borderBottom">
                <div class="propertyColumn">
                    <article class="informationHolder">
                        <hgroup>
                            <h4 class="fr2Color">Property</h4>
                            <h5>
                                <asp:Label ID="PropertyNameLabel" CssClass="fr1Color" runat="server" />
                            </h5>
                        </hgroup>
                                            
                        <!--**First Row (Address)**-->
                        <div class="formRow">
                            <span class="formLabel fr1Color">Address</span>
                            <p><asp:Label ID="PropertyAddressLabel" class="frBlack" runat="server"/></p>
                            <p><asp:Label ID="PropertyCityStateZipLabel" class="frBlack" runat="server" /></p>
                        </div>
                                            
                        <!--**Second Row (Phone)**-->
                        <div class="formRow">
                            <span class="formLabel fr1Color">Phone</span>
                            <p><asp:Label ID="PropertyPhoneLabel" class="frBlack" runat="server" /></p>
                        </div>
                                            
                        <!--**Third Row (Fax)**-->
                        <div class="formRow">
                            <span class="formLabel fr1Color">Fax</span>
                            <p><asp:Label ID="PropertyFaxLabel" class="frBlack" runat="server"/></p>
                        </div>
                                            
                        <!--**Fourth Row (Email)**-->
                        <div class="formRow">
                            <span class="formLabel fr1Color">Email</span>
                            <p><asp:Label ID="PropertyEmailLabel" class="frBlack" runat="server" /></p>
                        </div>
                        
                    </article>

                    <aside class="iconHolder">
                        <asp:Image ID="PropertyImageHolder" runat="server" CssClass="resizedImage" />                        
                    </aside>
                </div>
                <div class="propertyColumn">
                    <article class="informationHolder">
                        <hgroup>
                        <h4 class="fr2Color">Property Manager</h4>
                            <h5><asp:Label ID="PropertyManagerNameLabel" class="fr1Color" runat="server"/></h5>
                        </hgroup>
                                            
                        <!--**First Row (Phone)**-->
                        <div class="formRow">
                            <span class="formLabel fr2Color">Phone</span>
                            <p><asp:Label ID="PropertyManagerPhoneLabel"  class="frBlack" runat="server" /></p>
                        </div>
                                            
                        <!--**Second Row (Email)**-->
                        <div class="formRow">
                            <span class="formLabel fr2Color">Email</span>
                            <p><asp:Label ID="PropertyManagerEmailLabel" class="frBlack" runat="server" /></p>
                        </div>                                                                                      

                    </article>

                    <aside class="iconHolder">
                        <asp:Image ID="PropertyManagerImageHolder" runat="server" CssClass="resizedImage" />
                    </aside>
                </div>
            </div>
                                
            <div class="propertyRow">
                <div class="propertyColumn">
                    <article class="informationHolder">
                        <hgroup>
                        <h4 class="fr2Color">Leasing Agent</h4>
                        <h5><asp:Label ID="LeasingAgentNameLabel" runat="server" /></h5>
                        </hgroup>
                                            
                        <!--**First Row (Phone)**-->
                        <div class="formRow">
                            <span class="formLabel fr1Color">Phone</span>
                            <p><asp:Label ID="LeasingAgentPhoneLabel" CssClass="frBlack" runat="server" /></p>
                        </div>
                                            
                        <!--**Second Row (Email)**-->
                        <div class="formRow">
                            <span class="formLabel fr1Color">Email</span>
                            <p><asp:Label ID="LeasingAgentEmailLabel" CssClass="frBlack" runat="server" /></p>
                        </div>                                                                                      

                    </article>

                    <aside class="iconHolder">
                        <asp:Image ID="LeasingAgentImageHolder" runat="server" CssClass="resizedImage" />
                    </aside>
                </div>
                <div class="propertyColumn">
                    <article class="informationHolder">
                        <h4 class="fr2Color">RPO Support</h4>
                                            
                        <!--**First Row (Support Line)**-->
                        <div class="formRow">
                            <span class="formLabel fr1Color">Support Line</span>
                            <p style="color: black !important;" ><asp:Label ID="RpoSupportPhoneLabel" runat="server" /></p>  
                        </div>
                                            
                        <!--**Second Row (Email)**-->
                        <div class="formRow">
                            <span class="formLabel fr1Color">Email</span>
                            <p style="color: black !important;" ><asp:Label ID="RpoSupportEmailLabel" runat="server"/></p>
                        </div>
                                            
                        <!--**Third Row (Website)**-->
                        <div class="formRow">
                            <span class="formLabel fr1Color">Website</span>
                            <!--Salcedo - 1/15/2015 - task # 00349 - make link dynamic -->
                            <p style="color: #06cef9 !important;"><a runat="server" id="MainSiteUrl" class="anchorColor" href="www.rentpaidonlinexx.com">www.RentPaidOnline.com</a></p>
                        </div>                                                                                      

                    </article>
                    <!--
                    <aside class="iconHolder">
                        <asp:Image ID="RpoImageHolder" runat="server" />
                    </aside> -->
                </div>
            </div>
        </div>                           
    </div>
</div>