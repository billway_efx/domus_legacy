﻿using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace Efx.EfxResidents.UserControls
{
    public partial class PaymentAmounts : UserControl, IPaymentAmounts
    {
        private PaymentAmountsPresenter _Presenter;

        public int RenterId { get; set; }
        public IPayment ParentPage { get { return (Page as IPayment); } }
        public string RentAmountText { set { RentAmountLabel.Text = value; } }
        public string ConvenienceFeeText { set { ConvenienceFeeLabel.Text = value; } }
        public string TotalAmountText { set { TotalAmountLabel.Text = value; } }
        public string PastDueHeaderText { set { PastDueHeaderLabel.Text = value; } }
        public string PastDueAmountText { set { PastDueAmountLabel.Text = value; } }
        public bool PastDueVisible { set { PastDuePanel.Visible = value; } }
        public ISelectPaymentType PaymentTypes { get; set; }
        public decimal ConvenienceFeeAmount { get; set; }
        public decimal RentAmount { get; set; }

        //cakel: 00407
        public decimal PaymentTotal { get; set; }

        public List<LeaseFee> MonthlyFeesDataSource
        {
            set
            {
                MonthlyFeesList.DataSource = value;
                MonthlyFeesList.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new PaymentAmountsPresenter(this);
			
            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
            //cakel: BUGID00019 - Commented out old code and replace with new code to check if property will residents to make partial payments
			//partialPaymentsPlaceHolder.Visible = _Presenter.Property.AllowPartialPayments;
            partialPaymentsPlaceHolder.Visible = _Presenter.Property.AllowPartialPaymentsResidentPortal;
			SetRentAmount();

            //3/30/2014 - Salcedo - Issue #1
            HackECheckFees();
        }

        //3/30/2014 - Salcedo - Issue #1 - per Nick, we need to hack the convenience fee for 
        //residents that have a balance and are on the AIR model and paying with an eCheck
        private void HackECheckFees()
        {
            decimal TotalAmount;
            CultureInfo ci = new CultureInfo("en-US");
            Decimal.TryParse(TotalAmountLabel.Text,  NumberStyles.Currency, ci, out TotalAmount);
            
            if (_Presenter.PaymentTypeSelectedValue == "2" && 
                _Presenter.Property.ProgramId == EfxFramework.Property.Program.AirProgram && 
                ConvenienceFeeLabel.Text == "$0.00" &&
                TotalAmount > 0)
            {
                //Do the hack ...
                ConvenienceFeeLabel.Text = "$1.00";
                eCheckPromotionCredit.Text = "($1.00)";
                ECheckPromotion.Visible = true;
            }
            else
            {
                //Hide the hack ...
                ECheckPromotion.Visible = false;
            }
        }

		protected void RefreshButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
				return;
			SetRentAmount();
		}

		private void SetRentAmount()
		{

           
			decimal rentOverrideValue;


            //CMallory - Task 00597 - Changed some of the logic in the If/Else statement. 
			if (!string.IsNullOrEmpty(OverrideTextBox.Text) && decimal.TryParse(OverrideTextBox.Text, out rentOverrideValue) && rentOverrideValue > 0)
			{
				RentAmount = rentOverrideValue;
                //If Payment method is Credit Card Or Ach
				if (ParentPage.SelectPaymentTypeControl.PaymentTypeSelectedValue != "3")
				{
					TotalAmountText = (RentAmount + _Presenter.ConvenienceFee).ToString("C");
				}
				else
				{
					TotalAmountText = (RentAmount).ToString("C");
				}
			}
                //cakel: TEST 00407
            else
            {

            }
		}
    }
}