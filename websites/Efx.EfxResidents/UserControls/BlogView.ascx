﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogView.ascx.cs" Inherits="Efx.EfxResidents.UserControls.BlogView" %>
<div class="blogContainer">

    <article class="blogPost">
        <div id="PostTitle" class="postTitle fr1Color"></div>
        <div id="PostDate" class="postDate fr2Color"></div>
        <div id="PostText" class="postText frBlack"></div>
    </article>
    
    <div class="blogNavigation">
        <section class="recentPostsSection">
            <h4 class="fr2Color">Recent Posts</h4>
<uc1:BlogPostListing ID="RecentBlogPosts" runat="server"/>
        </section>

        <section class="archivesSection">
            <h4 class="fr2Color">Archives</h4>
<asp:ListView ID="ArchiveListView" runat="server">
    <LayoutTemplate>
        <ul style="list-style-type: none;">
            <li id="itemPlaceholder" runat="server"></li>
        </ul>
    </LayoutTemplate>
    
    <ItemTemplate>
        <li>
            <%--inline styles below.... look out!--%>
            <a id="ArchiveYearLink" runat="server"><asp:Label ID="ArchiveYear" CssClass="fr2Color" runat="server" Text=<%# Bind("Year", "{0:MMM yyyy}") %> /></a>
            <asp:Label ID="Label1" runat="server" CssClass="fr1Color" Text=<%# Bind("Count","({0})") %> />
            <div id="HidePost" runat="server" style="display: none;">
                <uc1:BlogPostListing ID="ArchiveBlogPosts" runat="server"/>
            </div>
            <asp:HiddenField ID="PostState" runat="server" Value="false" />
        </li>
    </ItemTemplate>
    <EmptyItemTemplate>
        <asp:Label ID="EmptyItemLabel" runat="server" Text="No Items to Display" />
    </EmptyItemTemplate>
</asp:ListView>
        </section>        
    </div>
    

<script type="text/javascript">
    function ShowPostData(text, date, title) {
        var postText = document.getElementById("PostText");
        postText.innerHTML = text;
        var postDate = document.getElementById("PostDate");
        postDate.innerHTML = date;
        var postTitle = document.getElementById("PostTitle");
        postTitle.innerHTML = title;
    }
</script>
</div>