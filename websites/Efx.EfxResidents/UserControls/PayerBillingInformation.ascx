﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayerBillingInformation.ascx.cs" Inherits="Efx.EfxResidents.UserControls.PayerBillingInformation" %>

<h4 class="fr2Color" style="color: #0462c2; ">Billing Address</h4>
<div class="formRow checkBoxBlock">
    <div class="formFullWidth">
        <asp:CheckBox ID="SameAsMailingCheckBox" runat="server" CssClass="checkBoxText fr1Color" Text="Same as Mailing" AutoPostBack="True" />
    </div>
</div>                                                                 
<!--**Fifth Row (Billing Address)**-->
<div class="formRow">
    <div class="formFullWidth">
        <span class="formLabel fr1Color">Billing Address</span>
        <asp:TextBox ID="BillingAddressTextBox" runat="server"></asp:TextBox>
    </div>
</div>                               
<!--**Sixth Row (City)**-->
<div class="formRow">
    <div class="formFullWidth">
        <span class="formLabel fr1Color">City</span>
        <asp:TextBox ID="CityTextBox" runat="server"></asp:TextBox>
    </div>
</div>                                 
<!--**Seventh Row (State & Zip)**-->
<div class="formRow">
    <!--State -->
    <div class="formHalfWidth">
        <span class="formLabel fr1Color">State</span>
        <asp:DropDownList ID="StateDropDownList" runat="server" CssClass="chzn-select"></asp:DropDownList>
    </div>                                 
    <div class="formHalfWidth">
        <span class="formLabel fr1Color">Zip code</span>
        <asp:TextBox ID="ZipCodeTextBox" runat="server"></asp:TextBox>
    </div>
</div>                                
<!--**Eigth Row (Phone Number)**-->
<div class="formRow">
    <div class="formFullWidth">
        <span class="formLabel fr1Color">Phone Number</span>
        <asp:TextBox ID="PhoneNumberTextBox" runat="server" MaxLength="10"></asp:TextBox>
    </div>
</div>

<asp:RequiredFieldValidator ID="RequiredBillingAddressValidation" 
                            ControlToValidate="BillingAddressTextBox" 
                            runat="server" 
                            Display="None" 
                            ErrorMessage="Your Billing Address is Required" />

<asp:RequiredFieldValidator ID="RequiredCityValidation" 
                            ControlToValidate="CityTextBox" 
                            runat="server" 
                            Display="None" 
                            ErrorMessage="Your City is Required" />

<asp:RequiredFieldValidator ID="RequiredZipCodeValidation" 
                            ControlToValidate="ZipCodeTextBox" 
                            runat="server" 
                            Display="None" 
                            ErrorMessage="Your Zip Code is Required" />