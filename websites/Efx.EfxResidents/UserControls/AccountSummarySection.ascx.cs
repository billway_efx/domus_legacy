﻿using System;
using System.Web.UI;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;

namespace Efx.EfxResidents.UserControls
{
    public partial class AccountSummarySection : System.Web.UI.UserControl, IAccountSummarySection
    {
        private AccountSummarySectionPresenter _Presenter;
        public string NameLabel { set { AccountSummaryNameLabel.Text = value; } }
        public string PropertyLabel { set { AccountSummaryPropertyLabel.Text = value; } }
        public string LastPaymentLabel { set { AccountSummaryLastPaymentLabel.Text = value; } }
        public string NextPaymentLabel { set { AccountSummaryNextPaymentLabel.Text = value; } }
        public string AutoPayLabel { set { AccountSummaryAutoPayLabel.Text = value; } }
        public string RentLabel { set { AccountSummaryRentLabel.Text = value; } }        

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new AccountSummarySectionPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}