﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using Mb2x.ExtensionMethods;
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Efx.EfxResidents.UserControls
{
    public partial class PropertyMaintenanceRequestTab : UserControl, IPropertyMaintenanceRequest
    {
        private PropertyMaintenanceRequestPresenter _Presenter;

        public bool CarbonCopy { get { return CcMeCheckBox.Checked; } set { CcMeCheckBox.Checked = value; } }
        public Page ParentPage { get { return Page; } }
        public string ProblemDescription { get { return ProblemDescriptionTextBox.Text; } set { ProblemDescriptionTextBox.Text = value; } }
        public DropDownList RequestType { get { return MaintenanceTypeDropDown; } }
        public DropDownList RequestPriority { get { return MaintenancePriorityDropDown; } }
        //cakel: BUGID00087
        public DropDownList RequestPermission { get { return MaintenancePermissionDropDown; } }
        public int UserId { get { return !string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? HttpContext.Current.User.Identity.Name.ToInt32() : 0; } }
        public EventHandler SubmitClick { set { SendMaintenanceMessage.Click += value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new PropertyMaintenanceRequestPresenter(this);
            
            if(!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}