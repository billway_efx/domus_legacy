﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using EfxFramework.NewsApi.ResponseStructure;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using Mb2x.ExtensionMethods;

namespace Efx.EfxResidents.UserControls
{
    public partial class NewsAndWeatherSection : UserControl, INewsAndWeatherSection
    {
        private NewsAndWeatherSectionPresenter _Presenter;

        public int UserId 
        {
            get { return !string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? HttpContext.Current.User.Identity.Name.ToInt32() : 0; }
        }
        public IEnumerable<NewsResult> NewsResultDataSource { set { News.DataSource = value; News.DataBind(); } }
        public string Temp { set { Temperature.Text = value; } }
        public string RelativeHumidity { set { Humidity.Text = value; } }
        public string WeatherString { set { Weather.Text = value; } }
        public string WindSpeed { set { Wind.Text = value; } }
        public string GustingWindSpeed { set { WindGusting.Text = value; } }
        public string WeatherImageUrl { set { WeatherImage.ImageUrl = value; } }
        //cakel: Weather
        public string LocationCity { set { LocationLabel.Text = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new NewsAndWeatherSectionPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}