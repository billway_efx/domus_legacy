﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using Mb2x.ExtensionMethods;
using System;
using System.Web;

namespace Efx.EfxResidents.UserControls
{
    public partial class PropertyContactsTab : System.Web.UI.UserControl, IPropertyContacts
    {
        private PropertyContactsPresenter _Presenter;

        public int UserId
        {
            get { return !string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? HttpContext.Current.User.Identity.Name.ToInt32() : 0; }
        }
        public string PropertyName { set { PropertyNameLabel.Text = value; } }
        public string PropertyAddress { set {PropertyAddressLabel.Text = value;} }
        public string PropertyCityStateZip { set { PropertyCityStateZipLabel.Text = value;} }
        public string PropertyPhone { set { PropertyPhoneLabel.Text = value; } }
        public string PropertyFax { set { PropertyFaxLabel.Text = value; } }
        public string PropertyEmail { set { PropertyEmailLabel.Text = value; } }
        public string PropertyImageUrl { set { PropertyImageHolder.ImageUrl = value; } }

        //Property manager properties
        public string PropertyManagerName { set { PropertyManagerNameLabel.Text = value; } }
        public string PropertyManagerPhone { set { PropertyManagerPhoneLabel.Text = value; } }
        public string PropertyManagerEmail { set { PropertyManagerEmailLabel.Text = value; } }
        public string PropertyManagerImageUrl { set { PropertyManagerImageHolder.ImageUrl = value; } }
         
        //Leasing agent properties
        public string LeasingAgentName { set { LeasingAgentNameLabel.Text = value; } }
        public string LeasingAgentPhone { set { LeasingAgentPhoneLabel.Text = value; } }
        public string LeasingAgentEmail { set { LeasingAgentEmailLabel.Text = value; } }
        public string LeasingAgentImageUrl { set { LeasingAgentImageHolder.ImageUrl = value; } }

        //RPO properties
        public string RpoSupportPhone { set { RpoSupportPhoneLabel.Text = value; } }
        public string RpoSupportEmail { set { RpoSupportEmailLabel.Text = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new PropertyContactsPresenter(this);

            //Salcedo - 1/15/2015 - task 349
            MainSiteUrl.HRef = EfxFramework.Settings.PublicSiteUrl;
        }
    }
}