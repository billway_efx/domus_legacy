﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls.Blogs;

namespace Efx.EfxResidents.UserControls
{
    public partial class BlogView : UserControl, IBlogView
    {
        public IBlogPostListing RecentBlogPostListUserControl { get { return RecentBlogPosts; } }
        public ListView ArchivedBlogPostList { get { return ArchiveListView; } set { ArchiveListView.DataSource = value; ArchiveListView.DataBind(); } }
        public EventHandler<ListViewItemEventArgs> ArchiveItemDataBound { set { ArchiveListView.ItemDataBound += value; } }
        public Page ParentPage { get { return Page; } }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}