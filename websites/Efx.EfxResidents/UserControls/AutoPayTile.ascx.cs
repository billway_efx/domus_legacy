﻿using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;
using EfxFramework.ViewPresentation.Presenters.Factories;
using EfxFramework.ViewPresentation.Presenters.UserControls.PaymentTiles;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Efx.EfxResidents.UserControls
{
    public partial class AutoPayTile : UserControl, IAutoPayTile
    {
        private PaymentTilePresenterBase _Presenter;

        public int RenterId { get; set; }
        public EventHandler PaymentTypeChanged { set { PaymentTypeDropdown.SelectedIndexChanged += value; } }
        public EventHandler OnButtonClick { set { AutoPayOnButton.Click += value; } }
        public EventHandler OffButtonClick { set { AutoPayOffButton.Click += value; } }
        public string OnButtonCssClass { get { return AutoPayOnButton.CssClass; } set { AutoPayOnButton.CssClass = value; } }
        public string OffButtonCssClass { get { return AutoPayOffButton.CssClass; } set { AutoPayOffButton.CssClass = value; } }
        public DropDownList DayOfTheMonthDropdown { get { return PaymentProcessingDateComboBox; } }
        public DropDownList PaymentTypeDropdown { get { return AutoPayPaymentMethodComboBox; } }
        public string PaymentAmountText { set { PaymentAmountLabel.Text = value; } }
        public bool IsOn { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = PaymentTilePresenterFactory.GetPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();

			EfxFramework.Property currentProperty = EfxFramework.Property.GetPropertyByRenterId(RenterId);

            //Salcedo - 5/30/2014 - added this next line in an attempt to avoid the "Object reference not set to an instance of an object" error message
            if (PaymentTypeDropdown != null)
            {
                if (currentProperty != null && !currentProperty.AcceptACHPayments)
                {
                    ListItem achItem = PaymentTypeDropdown.Items.FindByText("E-Check");
                    if (achItem != null)
                        achItem.Enabled = false;
                }
                else
                {
                    ListItem achItem = PaymentTypeDropdown.Items.FindByText("E-Check");
                        if (achItem != null)
                            achItem.Enabled = true;
                }
                if (currentProperty != null && !currentProperty.AcceptCCPayments)
                {
                    ListItem ccItem = PaymentTypeDropdown.Items.FindByText("Credit Card");
                    if (ccItem != null)
                        ccItem.Enabled = false;
                }
                else
                {
                    ListItem ccItem = PaymentTypeDropdown.Items.FindByText("Credit Card");
                    if (ccItem != null)
                        ccItem.Enabled = true;
                }
            }
			setControlsBasedOnRenter();
        }

		private void setControlsBasedOnRenter()
		{
			//get the renter
			var Resident = new Renter(RenterId);

			//remove the E-Check option if Accepted Payment type is cash equivalent
			if (Resident.AcceptedPaymentTypeId == 3)
			{
				ListItem acceptACHItem = AutoPayPaymentMethodComboBox.Items.FindByValue("2");
				if (acceptACHItem != null)
				{
					AutoPayPaymentMethodComboBox.Items.Remove(acceptACHItem);
				}
			}
		}
    }
}