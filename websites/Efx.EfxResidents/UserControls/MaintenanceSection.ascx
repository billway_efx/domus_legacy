﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MaintenanceSection.ascx.cs" Inherits="Efx.EfxResidents.UserControls.MaintenanceSection" %>
<h3 class="orangeText fr2Color">Maintenance</h3>

<!-- Salcedo - 2/10/2015 - task # 00366 - added ability to hide/disable/change maintenance request grid -->
<div runat="server" id="MaintenanceGridVisibility" visible="false">

    <!--cakel: BUGID00078 - Added Padding to fit grid when more than 7 rows -->
    <div class="formRow rowSpacingAdjustTop" style="padding: 5px;">
        <h5 class="fr1Color">Submitted Requests</h5>
        <uc1:MaintenanceRequestGrid runat="server" ID="MaintenanceRequestGrid" />
        <div id="ViewMoreRequest" class="hide modalGridMarginRightAdjust">
            <div class="modalContentContainer">
                <uc1:MaintenanceRequestGrid runat="server" ID="ModalRequestGrid" />
            </div>
        </div>
    </div>

    <div class="viewMoreLink alignRight">
        <asp:LinkButton ID="ViewMoreRequestButton" Text="View More" CausesValidation="false" runat="server" CssClass="greenLink fr1Color" />
    </div>

</div>


<div class="homepageButton submitMaintenanceRequestoPay">

    <!-- Salcedo - 2/10/2015 - task # 00366 - added ability to hide/disable/change maintenance request grid -->
    <p runat="server" id ="MaintenanceDescriptionText" visible="false" style="text-align:left; margin-bottom: 30px;"></p>

    <asp:LinkButton ID="SubmitMaintenanceRequestButton" runat="server" CssClass="orangeButton fr2BColor" PostBackUrl="~/Property.aspx/?Tab=3" Text="Submit Maintenance Request" />
</div>
