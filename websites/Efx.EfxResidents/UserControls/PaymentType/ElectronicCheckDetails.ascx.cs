﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Web.UI;

namespace Efx.EfxResidents.UserControls.PaymentType
{
    public partial class ElectronicCheckDetails : UserControl, IElectronicCheckDetails
    {
        private ElectronicCheckDetailsPresenter _Presenter;

        public string RoutingNumberText { get { return RoutingNumberTextBox.Text; } set { RoutingNumberTextBox.Text = value; } }
        public string AccountNumberText { get { return AccountNumberTextBox.Text; } set { AccountNumberTextBox.Text = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new ElectronicCheckDetailsPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.IntializeValues();
        }
    }
}