﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Efx.EfxResidents.UserControls.PaymentType
{
    public partial class CreditCardDetails : UserControl, ICreditCardDetails
    {
        private CreditCardDetailsPresenter _Presenter;

        public string NameOnCardText { get { return NameOnCardTextBox.Text; } set { NameOnCardTextBox.Text = value; } }
        public string CreditCardNumberText { get { return CreditCardNumberTextBox.Text; } set { CreditCardNumberTextBox.Text = value; } }
        public string ExpirationMonthSelectedValue { get { return ExpirationMonthDropDown.SelectedValue; } set { ExpirationMonthDropDown.SelectedValue = value; } }
        public DropDownList ExpirationYearList { get { return ExpirationYearDropDown; } }
        public string CvvText { get { return CvvTextBox.Text; } set { CvvTextBox.Text = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new CreditCardDetailsPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}