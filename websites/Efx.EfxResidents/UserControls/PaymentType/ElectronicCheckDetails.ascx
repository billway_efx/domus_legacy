﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ElectronicCheckDetails.ascx.cs" Inherits="Efx.EfxResidents.UserControls.PaymentType.ElectronicCheckDetails" %>
<!--***E-Check Option***-->
<div>
    <!--**Second Row (Account Number)**-->
    <div class="formRow">
        <div class="formFullWidth">
            <span class="formLabel tipItem fr1Color">Checking Account Number</span><span class="toolTipIcon checkNumbersTip"></span>
            <!--cakel: BUGID00072 - Updated max length from 15 to 17 -->
            <asp:TextBox ID="AccountNumberTextBox" runat="server" MaxLength="17"></asp:TextBox>
        </div>
    </div>                     
    <!--**Third Row (Routing Number)**-->
    <div class="formRow">
        <div class="formFullWidth">
            <span class="formLabel tipItem fr1Color">Routing Number</span><span class="toolTipIcon checkNumbersTip"></span>
            <asp:TextBox ID="RoutingNumberTextBox" runat="server" MaxLength="9"></asp:TextBox>
        </div>
    </div>                                                                                                                          
</div>

<asp:RequiredFieldValidator ID="RequiredAccountNumberValidation" 
                            ControlToValidate="AccountNumberTextBox" 
                            runat="server" 
                            Display="None" 
                            ErrorMessage="Your Checking Account Number is Required" />

<asp:RequiredFieldValidator ID="RequiredRoutingNumberValidation" 
                            ControlToValidate="RoutingNumberTextBox" 
                            runat="server" 
                            Display="None" 
                            ErrorMessage="Your Routing Number is Required" />