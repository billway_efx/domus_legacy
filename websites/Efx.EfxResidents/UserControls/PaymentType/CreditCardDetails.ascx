﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreditCardDetails.ascx.cs" Inherits="Efx.EfxResidents.UserControls.PaymentType.CreditCardDetails" %>
<!--***Credit Card Option***-->
<div>
    <!--**Second Row (Card Type)**-->
    <div class="formRow">
        <div class="formFullWidth">
            <span class="formLabel fr1Color">Name On Card</span>
            <asp:TextBox ID="NameOnCardTextBox" runat="server"></asp:TextBox>
        </div>
    </div>                            
    <!--**Third Row (Card number)**-->
    <div class="formRow">
        <div class="formFullWidth">
            <span class="formLabel fr1Color">Card Number</span>
            <asp:TextBox ID="CreditCardNumberTextBox" runat="server" MaxLength="16" TextMode="Number"></asp:TextBox>
        </div>
    </div>                       
    <!--**Fourth Row (Exiration Date)**-->
    <div class="formRow">
        <div class="formFullWidth"><span class="formLabel fr1Color">Expiration Date</span></div>
        <div class="formHalfWidth">                                                  
            <asp:DropDownList ID="ExpirationMonthDropDown" runat="server" CssClass="chzn-select">
                <asp:ListItem Value="1" Text="January"></asp:ListItem>
                <asp:ListItem Value="2" Text="February"></asp:ListItem>
                <asp:ListItem Value="3" Text="March"></asp:ListItem>
                <asp:ListItem Value="4" Text="April"></asp:ListItem>
                <asp:ListItem Value="5" Text="May"></asp:ListItem>
                <asp:ListItem Value="6" Text="June"></asp:ListItem>
                <asp:ListItem Value="7" Text="July"></asp:ListItem>
                <asp:ListItem Value="8" Text="August"></asp:ListItem>
                <asp:ListItem Value="9" Text="September"></asp:ListItem>
                <asp:ListItem Value="10" Text="October"></asp:ListItem>
                <asp:ListItem Value="11" Text="November"></asp:ListItem>
                <asp:ListItem Value="12" Text="December"></asp:ListItem>
            </asp:DropDownList>
        </div>                                  
        <div class="formHalfWidth">                                                  
            <asp:DropDownList ID="ExpirationYearDropDown" runat="server" CssClass="chzn-select"></asp:DropDownList>
        </div>
    </div>                                  
    <!--**Fifth Row (CVV Code)**-->
    <div class="formRow">
        <div class="formFullWidth">
            <span class="formLabel tipItem fr1Color">CVV Code</span><span class="toolTipIcon cvvTip fr1Color"></span>
            <asp:TextBox ID="CvvTextBox" runat="server" MaxLength="4" TextMode="Number"></asp:TextBox>
        </div>
    </div>
</div>

<asp:RequiredFieldValidator ID="RequiredNameOnCardValidation" 
                            ControlToValidate="NameOnCardTextBox" 
                            runat="server" 
                            Display="None" 
                            ErrorMessage="Name On Card is Required" />

<asp:RequiredFieldValidator ID="RequiredCardNumberValidation" 
                            ControlToValidate="CreditCardNumberTextBox" 
                            runat="server" 
                            Display="None" 
                            ErrorMessage="Your Card Number is Required" />
