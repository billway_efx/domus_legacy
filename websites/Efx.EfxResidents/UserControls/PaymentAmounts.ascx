﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentAmounts.ascx.cs" Inherits="Efx.EfxResidents.UserControls.PaymentAmounts" %>
<h4 class="fr2Color">Total Payment Amount</h4>
<!--Rent Amount -->
<div class="itemRow">
    <span class="itemTitle frBlack">Rent:</span>
    <span class="itemAmount">
        <asp:Label ID="RentAmountLabel" CssClass="frBlack" runat="server" /></span>
</div>
<!--Past Due Amount -->
<asp:Panel ID="PastDuePanel" runat="server" Visible="False">
    <div class="itemRow">
        <span class="itemTitle">
            <asp:Label ID="PastDueHeaderLabel" runat="server" /></span>
        <span class="itemAmount">
            <asp:Label ID="PastDueAmountLabel" runat="server" /></span>
    </div>
</asp:Panel>
<!--Convenience Fee Amount -->
<div class="itemRow">
    <span class="itemTitle frBlack">Convenience Fee:</span>
    <span class="itemAmount">
        <asp:Label ID="ConvenienceFeeLabel" CssClass="frBlack" runat="server" /></span>
</div>
<!-- 3/30/2014 - Salcedo - Issue #1 -->
<div class="itemRow" id="ECheckPromotion" runat="server">
    <span class="itemTitle">eCheck Promotion:</span>
    <span class="itemAmount"><asp:Label ID="eCheckPromotionCredit" runat="server" /></span>
</div>
<asp:ListView ID="MonthlyFeesList" runat="server">
    <LayoutTemplate>
        <div id="itemPlaceholder" class="itemRow" runat="server"></div>
    </LayoutTemplate>
    <ItemTemplate>
        <div class="itemRow">
            <span class="itemTitle">
                <asp:Label ID="ItemTitleLabel" runat="server" Text='<%# Bind("FeeName") %>' /></span>
            <span class="itemAmount">
                <asp:Label ID="ItemAmountLabel" runat="server" Text='<%# Bind("FeeAmountText") %>' /></span>
        </div>
    </ItemTemplate>
</asp:ListView>
<!--Total -->
<div class="itemRow borderTop">
    <asp:PlaceHolder ID="partialPaymentsPlaceHolder" runat="server">
        <br />
        <p><span class="formLabel fr2Color">Amount Being Paid Today:</span></p>
        <div class="formFullWidth">
            <!--CMallory - Task 00575 - Removed Type style from OverrideTextBox controll and added Error Message text to RangeValidator1 control-->
            <asp:TextBox ID="OverrideTextBox" runat="server" CssClass="form-control" />
            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="OverrideTextBox" ErrorMessage="The Amount Being Paid needs to be a dollar amount between $.01 and $10000.00" MaximumValue="10000.00" MinimumValue=".01" SetFocusOnError="True" Type="Double"></asp:RangeValidator>
        </div>
        <div class="formHalf">
            <asp:Button ID="RefreshButton" runat="server" CssClass="orangeButton buttonInline fr2BColor" Text="UpdateTotal" OnClick="RefreshButton_Click" />
        </div>
    </asp:PlaceHolder>
    <span class="totalTitle " style="background-color: #0462c2 !important; color: white  !important;">Total:</span>
    <span class="totalAmount">
        <asp:Label ID="TotalAmountLabel" runat="server" />
    </span>
</div>
<div class="itemRow">
    <div class="bottomMessage">
        <p class="frBlack">*Note that convenience fees will be applied to partial and full payments</p>
    </div>
</div>
