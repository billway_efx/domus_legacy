﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsSection.ascx.cs" Inherits="Efx.EfxResidents.UserControls.NewsSection" %>

<asp:ListView ID="NewsArticles" runat="server">
    <LayoutTemplate>
            <span id="itemPlaceHolder" runat="server"></span>  
    </LayoutTemplate>
    <ItemTemplate>
        <article class="newsArticle">        
            <h3 class="fr1Color"><asp:Label ID="NewsTitleLabel" CssClass="fr1Color" runat="server" Text='<%# Bind("AnnouncementTitle") %>' /></h3>  
            <span class="formLabel fr1Color"><asp:Label ID="NewsDate" Text= '<%# Bind("AnnouncementDate", "Posted on {0: MMMM dd, yyyy} at {0: hh:mm tt}")  %>' runat="server" ></asp:Label></span>       
            <p><asp:Label ID="NewsText" CssClass="frBlack" Text='<%# Bind("TruncateNews") %>' runat="server"/></p>      
            <p><asp:HyperLink ID="ReadMoreLink" CssClass="fr1Color" runat="server" Text="Read More" NavigateUrl='<%# Bind("RedirectNews") %>' Target="_blank" /></p>  
        </article>
    </ItemTemplate>
    <EmptyDataTemplate>
        <p class="nothingToShow frBlack">There are no News Articles to Show at this time.</p>
    </EmptyDataTemplate>
</asp:ListView>
<div class="clearFix"></div>