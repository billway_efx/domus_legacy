﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountSummarySection.ascx.cs" Inherits="Efx.EfxResidents.UserControls.AccountSummarySection" %>
    <h3 class="orangeText fr2Color">Account Summary</h3>
    <div class="formRow rowSpacingAdjust fr1Color">
        <h5 class="fr1Color">Account Info</h5>
        <!--Resident -->
        <div class="itemRow">
            <span class="itemTitle frBlack">Resident:</span>
            <asp:Label ID="AccountSummaryNameLabel" runat="server" CssClass="itemAmount frBlack" />
        </div>
                    
        <!--Property -->
        <div class="itemRow">
            <span class="itemTitle frBlack">Property:</span>
            <asp:Label ID="AccountSummaryPropertyLabel" runat="server" CssClass="itemAmount frBlack" />
        </div>
                    
        <!--Last Payment Method -->
        <div class="itemRow">
            <span class="itemTitle frBlack">Last Payment Made:</span>
            <asp:Label ID="AccountSummaryLastPaymentLabel" runat="server" CssClass="itemAmount frBlack" />
        </div>
                    
        <!--Next Payment Method -->
        <div class="itemRow">
            <span class="itemTitle frBlack">Next Payment Due:</span>
            <asp:Label ID="AccountSummaryNextPaymentLabel" runat="server" CssClass="itemAmount frBlack" />
        </div>
                    
        <!--AutoPay Method -->
        <div class="itemRow">
            <span class="itemTitle frBlack">AutoPay:</span>
            <asp:Label ID="AccountSummaryAutoPayLabel" runat="server" CssClass="itemAmount frBlack" Text="On" />
        </div>
    </div>
                
    <div class="formRow rowSpacingAdjust">
        <h5 class="fr1Color">Rent</h5>
        <!--Rent Amount -->
        <div class="itemRow">
            <span class="itemTitle frBlack">Rent Amount:</span>
            <asp:Label ID="AccountSummaryRentLabel" runat="server" CssClass="itemAmount frBlack" />
        </div>
    </div>
                
    <div class="formRow rowSpacingAdjust">
        <h5 class="fr1Color">Fees</h5>
        <uc1:FeesList runat="server" ID="FeesList" />
    </div>
                
        <!-- Bottom links -->
        <div class="formRow rowSpacingAdjust alignRight">
            <asp:LinkButton ID="MyAccountButton" CssClass="greenLink btn-primary-sm btn-half  fr2Color" PostBackUrl="~/MyAccount.aspx" Text="My Account" runat="server" />
            <asp:LinkButton ID="ViewHistoryButton" CssClass="greenLink btn-primary-sm btn-half fr2Color" PostBackUrl="~/Payments.aspx/?Tab=2" Text="View History" runat="server" />
        </div>