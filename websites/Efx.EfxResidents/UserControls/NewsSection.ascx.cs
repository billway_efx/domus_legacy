﻿using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Collections.Generic;

namespace Efx.EfxResidents.UserControls
{
    public partial class NewsSection : System.Web.UI.UserControl, INewsSection
    {
        private NewsSectionPresenter _Presenter;
        public List<Announcement> NewsListView { set { NewsArticles.DataSource = value; NewsArticles.DataBind(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new NewsSectionPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}