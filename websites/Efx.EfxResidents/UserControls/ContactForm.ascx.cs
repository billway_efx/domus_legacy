﻿using System;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System.Web.UI.WebControls;
using Mb2x.ExtensionMethods;
using System.Web;
using EfxFramework;
//using EfxFramework.Resources;

namespace Efx.EfxResidents.UserControls
{
    public partial class ContactForm : System.Web.UI.UserControl, IContactForm
    {
        private ContactFormPresenter _Presenter;

        public EventHandler SendButtonClick { set { SendButton.Click += value; } }
        public string NameText { get { return NameTextBox.Text; } set { NameTextBox.Text = value; } }
        public string EmailAddressText { get { return EmailAddressTextBox.Text; } set { EmailAddressTextBox.Text = value; } }
        public string PhoneNumberText { get { return PhoneNumberTextBox.Text; } set { PhoneNumberTextBox.Text = value; } }
        public string PropertyNameText { get { return PropertyNameTextBox.Text; } set { PropertyNameTextBox.Text = value; } }
        public string MessageText { get { return MessageTextBox.InnerText; } set { MessageTextBox.InnerText = value; } }
        public bool IsConditionalPhoneVisible { set { ConditionalPhone.Visible = value; } }
        public bool IsConditionalPropertyVisible { get { return ConditionalProperty.Visible; } set { ConditionalProperty.Visible = value; } }
        public int SetContactMultiView { set { ContactMultiView.ActiveViewIndex = value; } }
        public string PhoneNumberClientId { get { return PhoneNumberTextBox.ClientID; } }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new ContactFormPresenter(this);
            //CMallory - Task 00539 - Added to hide the Property textbox and to automatically populate the Name and Email fields.
            ConditionalProperty.Visible = false;
            int RenterId = HttpContext.Current.User.Identity.Name.ToInt32();
            var User = new Renter(RenterId);
            NameTextBox.Text = User.DisplayName;
            EmailAddressText = User.UserName;
            
        }
    }
}