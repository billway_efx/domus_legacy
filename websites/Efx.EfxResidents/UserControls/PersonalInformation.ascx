﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PersonalInformation.ascx.cs" Inherits="Efx.EfxResidents.UserControls.PersonalInformation" %>
<h4 class="fr2Color">My Info</h4>
<!--**First Row (first name)**-->
<div class="formRow">
    <div class="formFullWidth">
        <span class="formLabel fr1Color">First Name</span>
        <asp:TextBox ID="FirstNameTextBox" runat="server" ReadOnly="True"></asp:TextBox>
    </div>
</div>                               
<!--**Second Row (Middle name)**-->
<div class="formRow">
    <div class="formFullWidth">
        <span class="formLabel fr1Color">Middle Name</span>
        <asp:TextBox ID="MiddleNameTextBox" runat="server" ReadOnly="True"></asp:TextBox>
    </div>
</div>                              
<!--**Third Row (Last name)**-->
<div class="formRow">
    <div class="formFullWidth">
        <span class="formLabel fr1Color">Last Name</span>
        <asp:TextBox ID="LastNameTextBox" runat="server" ReadOnly="True"></asp:TextBox>
    </div>
</div>                              
<!--**Fourth Row (Suffix)**-->
<div class="formRow">
    <div class="formFullWidth">
        <span class="formLabel fr1Color">Suffix</span>
        <asp:TextBox ID="SuffixTextBox" runat="server" ReadOnly="True"></asp:TextBox>
    </div>
</div>

<asp:RequiredFieldValidator ID="RequiredFirstNameValidation" 
                            ControlToValidate="FirstNameTextBox" 
                            runat="server" 
                            Display="None" 
                            ErrorMessage="Your First Name is Required" />

<asp:RequiredFieldValidator ID="RequiredLastNameValidation" 
                            ControlToValidate="LastNameTextBox" 
                            runat="server" 
                            Display="None" 
                            ErrorMessage="Your Last Name is Required" />