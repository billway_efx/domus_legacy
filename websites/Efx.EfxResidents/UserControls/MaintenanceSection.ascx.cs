﻿using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Data.SqlClient;
using System.Data;

namespace Efx.EfxResidents.UserControls
{
    public partial class MaintenanceSection : System.Web.UI.UserControl, IMaintenanceSection
    {
        private MaintenanceSectionPresenter _Presenter;

        public Page PageView { get { return Page; } }
        public EventHandler ViewMoreRequestButtonClick { set { ViewMoreRequestButton.Click += value; } }
        public EventHandler SubmitMaintenanceRequestButtonClick { set { SubmitMaintenanceRequestButton.Click += value; } }
        public IMaintenanceRequestGrid GridForMaintenance { get { return MaintenanceRequestGrid; } }
        public List<MaintenanceRequest> ModalGrid { set { ModalRequestGrid.RequestGrid = value; } }
        public int? RenterId { get { return HttpContext.Current.User.Identity.Name.ToNullInt32(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new MaintenanceSectionPresenter(this);
            ViewMoreRequestButtonClick = MoreRequestButtonClick;

            if (!Page.IsPostBack)
            {
                _Presenter.InitializeValues();
                MaintenanceGridVisibility.Visible = true;

                //Salcedo - 2/11/2015 - task # 00366 - added ability to use/not use native maintenance request system
                int RenterId = 0;
                if (PageView.User.Identity.IsAuthenticated)
                {
                    if (PageView.User.Identity.Name != "0")
                    {
                        RenterId = Convert.ToInt32(PageView.User.Identity.Name);
                        SqlDataReader reader = MiscDatabase.GetPropertyMaintenanceSystemForRenter(RenterId);
                        reader.Read();
                        if (reader.HasRows)
                        {
                            if (!(bool)reader["UseNativeMaintenanceSystem"])
                            {
                                MaintenanceGridVisibility.Visible = false;
                                if ((bool)reader["UseExternalMaintenanceSystem"] && reader["ExternalMaintenanceSiteUrl"].ToString() != "")
                                {
                                    MaintenanceDescriptionText.Visible = true;

                                    SubmitMaintenanceRequestButton.PostBackUrl = "";
                                    SubmitMaintenanceRequestButton.OnClientClick = "window.open('" + reader["ExternalMaintenanceSiteUrl"].ToString() + "','_blank')";
                                    MaintenanceDescriptionText.InnerText = reader["ExternalMaintenanceDescriptionText"].ToString();
                                    SubmitMaintenanceRequestButton.Text = reader["ExternalMaintenanceButtonText"].ToString();
                                }
                            }
                            reader.Close();
                        }
                    }
                }

            }
        }

        private void MoreRequestButtonClick(object sender, EventArgs e)
        {
            var ScriptPage = this.Page;
            ScriptManager.RegisterStartupScript(ScriptPage, GetType(), "MaintenanceRequestModal", "<script type='text/javascript'>ShowModal(\"#ViewMoreRequest\");</script>", false);
        }
    }
}