﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;

namespace Efx.EfxResidents.UserControls.Grid
{
    public partial class PaymentHistory : UserControl, IPaymentHistory
    {
        private PaymentHistoryPresenter _Presenter;

        public int RenterId
        {
            get 
            { 
                var Id = HttpContext.Current.User.Identity.Name.ToNullInt32();
                return Id.HasValue && Id.Value  > 0 ? Id.Value : 0;
            }
        }

        public List<EfxFramework.PaymentHistory> PaymentList { set { PaymentHistoryGridView.DataSource = value; PaymentHistoryGridView.DataBind(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new PaymentHistoryPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }   
    }
}