﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentHistory.ascx.cs" Inherits="Efx.EfxResidents.UserControls.Grid.PaymentHistory" %>
<!--  payment-history   -->
<div class="formRow gridHolder">
    <asp:GridView ID="PaymentHistoryGridView" runat="server" AlternatingRowStyle-CssClass="gridAltRow" HeaderStyle-CssClass="tableHeader" AutoGenerateColumns="false" CssClass="responsive">
        <Columns>
            <asp:BoundField  HeaderText="Date" DataField="TransactionDateTime" />
            <asp:BoundField HeaderText="Transaction ID" DataField="TransactionId" />
            <asp:BoundField HeaderText="Amount" DataField="PaymentAmount" />
            <asp:BoundField HeaderText="Description" DataField="Description" />
            <asp:BoundField HeaderText="Method" DataField="PaymentMethod" />
            <asp:BoundField HeaderText="AutoPay" DataField="IsAutoPay" />
            <asp:BoundField HeaderText="Status" DataField="Status" />
        </Columns>
    </asp:GridView>
</div>