﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MaintenanceRequestGrid.ascx.cs" Inherits="Efx.EfxResidents.UserControls.MaintenanceRequestGrid" %>
<div class="formRow gridHolder">
    <asp:GridView ID="SubmittedRequestGrid" runat="server" AlternatingRowStyle-CssClass="gridAltRow" HeaderStyle-CssClass="tableHeader" AutoGenerateColumns="false" CssClass="responsive" >
        <Columns>
            <asp:BoundField HeaderText="Date" DataField="MaintenanceRequestDate" DataFormatString="{0:d}" />
            <asp:BoundField HeaderText="Request ID" DataField="MaintenanceRequestId" />
            <asp:BoundField HeaderText="Description" DataField="MaintenanceRequestDescription" />
        </Columns>
    </asp:GridView>
</div>