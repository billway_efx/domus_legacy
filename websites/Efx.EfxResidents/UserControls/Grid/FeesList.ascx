﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeesList.ascx.cs" Inherits="Efx.EfxResidents.UserControls.Grid.FeesList" %>

<asp:ListView ID="ListOfFees" runat="server">
    <LayoutTemplate>            
            <div id="itemPlaceHolder" runat="server"></div>        
    </LayoutTemplate>
    <ItemTemplate>            
            <div class="itemRow"><asp:Label ID="FeesNameLabel" runat="server" Text='<%# Bind("FeeName") %>' CssClass="itemTitle frBlack" /> <asp:Label ID="FeesAmountLabel" runat="server" Text='<%# Bind("FeeAmountText") %>' CssClass="itemAmount frBlack" /></div>
    </ItemTemplate>
    <EmptyDataTemplate>
            <p class=" frBlack">There are no fees</p>
    </EmptyDataTemplate>

</asp:ListView>