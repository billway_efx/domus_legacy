﻿using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Collections.Generic;

namespace Efx.EfxResidents.UserControls.Grid
{
    public partial class PetsList : System.Web.UI.UserControl, IPetsList
    {
        private PetsListPresenter _Presenter;
        public List<Pet> RenterPets { set { ListOfPets.DataSource = value; ListOfPets.DataBind(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new PetsListPresenter(this);

            if(!Page.IsPostBack)
            _Presenter.InitializeValues();
        }
    }
}