﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Collections.Generic;

namespace Efx.EfxResidents.UserControls.Grid
{
    public partial class OccupantsList : System.Web.UI.UserControl, IOccupantList
    {
        private OccupantListPresenter _Presenter;        
        public List<Occupant> ListOfPeople { set { ListOfOccupant.DataSource = value; ListOfOccupant.DataBind(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new OccupantListPresenter(this);

            if (!Page.IsPostBack)            
                _Presenter.InitializeValues();            
        }        
    }

    
}