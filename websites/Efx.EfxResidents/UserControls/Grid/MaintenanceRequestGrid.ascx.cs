﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using Mb2x.ExtensionMethods;

namespace Efx.EfxResidents.UserControls
{
    public partial class MaintenanceRequestGrid : System.Web.UI.UserControl, IMaintenanceRequestGrid
    {
        public List<MaintenanceRequest> RequestGrid { set { SubmittedRequestGrid.DataSource = value; SubmittedRequestGrid.DataBind(); } }
        public int RecordsToDisplay { get { return Settings.MaintenanceRequestView.ToInt32(); } }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
    }
}