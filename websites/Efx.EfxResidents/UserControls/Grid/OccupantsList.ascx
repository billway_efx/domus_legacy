﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OccupantsList.ascx.cs" Inherits="Efx.EfxResidents.UserControls.Grid.OccupantsList" %>

<asp:ListView ID="ListOfOccupant" runat="server">
    <LayoutTemplate>            
            <div id="itemPlaceHolder" runat="server"></div>        
    </LayoutTemplate>
    <ItemTemplate>            
            
            <div>
                <asp:Label ID="OccupantType" runat="server" Text='<%# Bind("OccupantType") %>' CssClass="itemLabel frBlack" /> 
                <asp:Label ID="OccupantName" runat="server" Text='<%# Bind("OccupantName") %>' class=" frBlack" />
            </div>
    </ItemTemplate>
    <EmptyDataTemplate>
            <p class="frBlack">There are no fees</p>
    </EmptyDataTemplate>

</asp:ListView>