﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PetsList.ascx.cs" Inherits="Efx.EfxResidents.UserControls.Grid.PetsList" %>

<asp:ListView ID="ListOfPets" runat="server">
    <LayoutTemplate>            
            <div id="itemPlaceHolder" runat="server"></div>        
    </LayoutTemplate>
    <ItemTemplate>            
            <div class="itemRow">
                <asp:Label ID="PetsNameLabel"  runat="server" Text='<%# Bind("PetName") %>' CssClass="itemTitle frBlack" /> 
                <asp:Label ID="PetsBreedLabel" runat="server" Text='<%# Bind("PetBreed") %>' CssClass="itemAmount frBlack" />
            </div>
    </ItemTemplate>
    <EmptyDataTemplate>
            <p class="frBlack">There are no pets</p>
    </EmptyDataTemplate>

</asp:ListView>