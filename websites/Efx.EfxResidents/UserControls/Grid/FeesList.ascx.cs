﻿using System.Web;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System;
using System.Collections.Generic;
using System.Web.UI;
using EfxFramework;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;

namespace Efx.EfxResidents.UserControls.Grid
{
    public partial class FeesList : UserControl, IFeesList
    {
        private FeesListPresenter _Presenter;
        public List<LeaseFee> RenterMonthlyFees { set { ListOfFees.DataSource = value; ListOfFees.DataBind(); } }

        public int RenterId
        {
            get
            {
                var Id = HttpContext.Current.User.Identity.Name.ToNullInt32();
                if (!Id.HasValue || Id.Value < 1)
                {
                    BasePageV2.SafePageRedirect("~/Account/Login.aspx");
                    return 0;
                }
                return Id.Value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new FeesListPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}