﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PaymentIvr.aspx.cs" Inherits="Efx.EfxResidents.PaymentIvr" %>
<%@ Import Namespace="EfxFramework" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

        <div id="pageCenterContainer" class="pageContainer paySuccessPage residentsPage">
        <div class="pageTitle" style="border-bottom: 0px;">
            <h2 style="color:#0462c2  !important; ">Payments</h2></div>
        <!--Main Tabs Section -->
        <div class="singleItemSection" style="width: 80% !important; margin-left: 10% !important; margin-right: 10% !important; ">
            <div class="slimPadding">
                <div class="receiptHolder">
                    <header>
                        <h3 class="fr1Color">Payment Status</h3>
                        <p class="fr1Color">Your Payment has been pre-processed.</p>
                        <p class="fr1Color">To complete your payment, please call:</p> 
                        <p class="fr1Color"> <a class="telephone" href="tel:855-769-7368">1-855-PMY-RENT (1-855-769-7368)</a></p>
                        <p class="fr1Color">At the prompt, please enter this code:</p>
                        <p><asp:Label ID="PaymentQueueNumberLabel" runat="server"></asp:Label></p>
                        <p class="fr1Color">You will then need to enter your pin: <asp:Label ID="lblPin" runat="server"></asp:Label></p>
                    </header>
                    <div class="receipt">
                        <div class="recieptColumn">
                            <!-- **Second Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Rent**-->
                                <div class="formFullWidth">
                                    <span class="formLabel fr1Color">Rent</span>
                                    <p><asp:Label ID="RentAmountLabel" runat="server"/></p>
                                </div>                            
                            </div>
                            <!-- **Third Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Convenience Fee**-->
                                 <div class="formFullWidth">
                                     <span class="formLabel fr1Color">Convenience Fee</span>
                                     <p><asp:Label ID="ConvenienceFeeLabel" runat="server"/></p>
                                 </div>
                            </div>
                            <!-- **Fourth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Total Payment Amount**-->
                                <div class="formFullWidth">
                                    <span class="formLabel fr1Color">Total Payment Amount</span>
                                    <p><asp:Label ID="TotalPaymentAmountLabel" runat="server" /></p>
                                </div>                                 
                            </div>
                            <!-- **Fifth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Property**-->
                                <div class="formFullWidth">
                                    <span class="formLabel fr1Color">Property</span>
                                    <p><asp:Label ID="PropertyNameLabel" runat="server" /></p>
                                    <p><asp:Label ID="PropertyAddressLabel" runat="server" /></p>
                                    <p><asp:Label ID="PropertyCityStateZipLabel" runat="server" /></p>
                                </div>                                 
                            </div>
                            <!-- **Sixth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**RPO Support**-->
                                <div class="formFullWidth">
                                    <span class="formLabel fr1Color">RPO Support</span>
                                    <p class="fr1Color"><a class="telephone" href="tel:855-769-7368">Support Line: s<%=Settings.SupportPhone %></a></p>
                                    <p class="fr1Color">Email:<a class="fr2Color" href="mailto:support@rentpaidonline.com"> <%=Settings.SupportEmail %></a></p>
                                </div>                                 
                            </div>
                        </div>
                        <div class="recieptColumn">
                            <!-- **First Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**First Name **-->
                                <div class="formFullWidth">
                                    <span class="formLabel fr1Color">First Name</span>
                                    <p><asp:Label ID="FirstNameLabel" runat="server" /></p>
                                </div>
                            </div>
                            <!-- **Second Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Middle Name **-->
                                <div class="formFullWidth">
                                    <span class="formLabel fr1Color">Middle Name</span>
                                    <p><asp:Label ID="MiddleNameLabel" runat="server" /></p>
                                </div>
                            </div>
                            <!-- **Third Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Last Name**-->
                                <div class="formFullWidth">
                                    <span class="formLabel fr1Color">Last Name</span>
                                    <p><asp:Label ID="LastNameLabel" runat="server" /></p>
                                </div>
                            </div>
                            <!-- **Fourth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                 <!--**Suffix**-->
                                <div class="formFullWidth">
                                    <span class="formLabel fr1Color">Suffix</span>
                                    <p><asp:Label ID="SuffixLabel" runat="server" /></p>
                                </div>
                            </div>
                            <!-- **Fifth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                 <!--**Billing Address**-->
                                <div class="formFullWidth">
                                    <span class="formLabel fr1Color">Billing Address</span>
                                    <p><asp:Label ID="BillingAddressLabel" runat="server" /></p>
                                    <p><asp:Label ID="BillingCityStateZipLabel" runat="server" /></p>
                                </div>
                            </div>
                            <!-- **Sixth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                 <!--**Phone Number**-->
                                <div class="formFullWidth">
                                    <span class="formLabel fr1Color">Phone Number</span>
                                    <p><asp:Label ID="PhoneNumberLabel" runat="server" /></p>
                                </div>
                            </div>
                        </div>
                     </div>
                 </div>
                <div class="clearFix"></div>
            </div>                            
        </div>
    </div>

    <script>

        var vOrient = 'p';
        // Listen for orientation changes
        window.addEventListener("orientationchange", function () {

            calc_container(vOrient);
        }, false);

        // portrait OR landscape
        function _portrait_landspace() {

            //
            if (window.matchMedia("(orientation: portrait)").matches) {
                vOrient = 'p';
            }

            if (window.matchMedia("(orientation: landscape)").matches) {
                vOrient = 'l';
            }

            var vDevice = navigator.userAgent;
            var w_width = $(window).width();
            if (vOrient == 'p') {
                if ($(window).width() < 768) {
                    $('#headerImg').hide();
                }
            }
            if (vOrient == 'l') {
                if ($(window).width() < 768) {
                    $('#headerImg').hide();
                }
            }

        } // end of p_l....



        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
                $("#pageCenterContainer").css("margin-left", vML);
                $("#pageCenterContainer").css("margin-right", vML);
            }
            else if ($(window).width() >= 768 && $(window).width() < 1024) {
                var vML = (w_width - 768) / 2;
                $("#pageCenterContainer").css("width", "90%");
                $("#pageCenterContainer").css("margin-left", "5%");
                $("#pageCenterContainer").css("margin-right", "5%");
                //$('#pageCenterContainer').attr('id', 'temp');
            }
            if ($(window).width() == 1016) {
                //$("#pageCenterContainer").css("width", "980px");
                //$("#pageCenterContainer").css("margin-left", vML );
                //$("#pageCenterContainer").css("margin-right", vML );
            }
            else if ($(window).width() < 760 ) {
                $('#headerImg').hide();
            }


        } // end of calc....

        $(document).ready(function () {

            / /
            var vObj = document.getElementById("header");
            //vObj.style.top = "-40px";
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "180px");
            if ($(window).width() <= 767) {
                $(".residentsPage").css("top", "120px");
                $(".residentsPage").css("background-color", "white");
            }
            //$('.wrapper').attr('style', 'height: 1500px !important');

            calc_container();

            $("footer").css("position", "absolute");
            $("footer").css("top", "1100px");
            if ($(window).width() <= 767) {
                $("footer").css("display", "none");
            }


    });

    // Listen for resize changes
    window.addEventListener("resize", function () {

        calc_container();
    }, false);

</script>

</asp:Content>
