﻿using EfxFramework.Interfaces.UserControls.ImageAdder;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Web;
using System.Web.UI.WebControls;

namespace Efx.EfxResidents
{
    public partial class MyAccount : BasePageV2, IMyAccount
    {
        private MyAccountPresenter _Presenter;
        private bool _RefreshState;

        public bool IsRefresh { get; private set; }
        public int AddressView { get { return Address.ActiveViewIndex; } set { Address.ActiveViewIndex = value; } }
        public int UserId { get { return !string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? HttpContext.Current.User.Identity.Name.ToInt32() : 0; } }
        public bool IsRentReportersIdVisible { set { RenterReporterIDVisible.Visible = value; } }
        public string RentReportersId { get { return RentReportersIdTextBox.Text; } set { RentReportersIdTextBox.Text = value; } }
        public string ResidentIdText { get { return ResidentIDLabel.Text; } set { ResidentIDLabel.Text = value; } }
        public string Email { get { return EmailUsernameTextBox.Text; } set { EmailUsernameTextBox.Text = value; } }
        public string FirstName { get { return FirstNameTextBox.Text; } set { FirstNameTextBox.Text = value; } }
        public string MiddleName { get { return MiddleNameTextBox.Text; } set { MiddleNameTextBox.Text = value; } }
        public string LastName { get { return LastNameTextBox.Text; } set { LastNameTextBox.Text = value; } }
        public string Suffix { get { return SuffixTextBox.Text; } set { SuffixTextBox.Text = value; } }
        public string MailingAddress { get { return MailingAddressTextBox.Text; } set { MailingAddressTextBox.Text = value; } }
        public string MailingCity { get { return CityTextBox.Text; } set { CityTextBox.Text = value; } }
        public string MailingZip { get { return ZipTextBox.Text; } set { ZipTextBox.Text = value; } }
        public string IntegratedMailingAddress { set { Address1Label.Text = value; } }
        public string IntegratedCityStateZip { set { CityStateZipLabel.Text = value; } }
        public string MainPhone { get { return MainPhoneTextBox.Text; } set { MainPhoneTextBox.Text = value; } }
        public string AltPhone { get { return AlternativePhoneTextBox.Text; } set { AlternativePhoneTextBox.Text = value; } }
        public string CurrentPin { set { CurrentPinLabel.Text = value; } }
        public string NewPin { get { return NewPinTextBox.Text; } }
        public string NewPinConfirm { get { return ConfirmPinTextBox.Text; } }
        public string Password { get { return NewPasswordTextBox.Text; } }
        public string PasswordConfirm { get { return ConfirmPasswordTextBox.Text; } }
        public string PhotoUrl { set { RenterPhoto.ImageUrl = value; } }
        public EventHandler SaveButtonClicked { set { SaveButton.Click += value; } }
        public IImageAdder ImageAdderControl { get { return ImageAdder; } }
        public ServerValidateEventHandler PinValidation { set { PinValidator.ServerValidate += value; } }
        public ServerValidateEventHandler PasswordValidation { set { PasswordValidator.ServerValidate += value; } }
        public DropDownList StateDropDown { get { return StateDropDownList; } }

        protected override object SaveViewState()
        {
            Session["__ISREFRESH"] = _RefreshState;
            var AllStates = new object[2];
            AllStates[0] = base.SaveViewState();
            AllStates[1] = !(_RefreshState);

            return AllStates;
        }

        protected override void LoadViewState(object savedState)
        {
            var AllStates = savedState as object[];

            if (AllStates == null || AllStates.Length < 1)
            {
                base.LoadViewState(savedState);
                return;
            }

            base.LoadViewState(AllStates[0]);
            bool Refresh;

            if (Session["__ISREFRESH"] == null)
            {
                base.LoadViewState(savedState);
                return;
            }

            Boolean.TryParse(AllStates[1].ToString(), out _RefreshState);
            Boolean.TryParse(Session["__ISREFRESH"].ToString(), out Refresh);
            IsRefresh = Refresh == _RefreshState;
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            AddValidationSummaryToPage();
            _Presenter = new MyAccountPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}