﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;

namespace Efx.EfxResidents
{
    public partial class Print : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            litYear.Text = DateTime.Now.Year.ToString(System.Globalization.CultureInfo.InvariantCulture);

            var RenterId = HttpContext.Current.User.Identity.Name.ToNullInt32();

            if (!RenterId.HasValue || RenterId.Value < 1)
            {
                LogoutLink.Visible = false;
                return;
            }

            if (RenterId > 1)
                LogInButton.Visible = false;

            var Renter = new Renter(RenterId);

            if (Renter.RenterId < 1)
            {
                LogoutLink.Visible = false;
                LogInButton.Visible = true;
                BasePageV2.SafePageRedirect("~/Account/LogOut.aspx");
            }

            WelcomeMessageText.Text = String.Format("Welcome, {0} ", Renter.FirstName);
        }
    }
}