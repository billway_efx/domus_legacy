﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Property.aspx.cs" Inherits="Efx.EfxResidents.Property" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <div id="pageCenterContainer" class="pageContainer propertyPage residentsPage" > 
        <div class="pageTitle" style="border-width: 0;"><h2 style="color: #0462c2 !important; ">Property</h2></div>
        <!--Main Tabs Section -->
        <div class="mainTabSection fr2BorderColor">
            <div class="tabs fr2BorderColor">
                <span class=""  ><i class="fa fa-book fr2Color fr2BorderColor"></i>Contacts</span>
                <span class="" style="border: 1px solid #06cef9;"><i class="fa fa-info-circle fr2Color"></i>Leasing Info</span>
                <span class="" style="border: 1px solid #06cef9;"><i class="fa fa-wrench fr2Color"></i>Maintenance <del style="text-decoration: none;">Requests</del></span>
            </div>
            <!--"panel_container" is the main container for all the panels on this page, a "panel" is each man tabbing section -->
            <div id="idProperty" class="panel_container fr2BorderColor" style="height: 857px; ">
                <!--this is the start of the multiple panels -->
                <div id="divPanels" class="panels" style="width: 2916px; left: 0px;" >
                    <!--this is the first panel (Contacts) -->
                    <uc1:PropertyContactsTab runat="server" id="PropertyContactsTab" />

                    <!--this is the Second panel (Lease Info) -->
                    <uc1:PropertyLeasingInfoTab runat="server" id="PropertyLeasingInfoTab" />                                
                                
                    <!--this is the Third panel (Maintenance Requests) -->
                    <uc1:PropertyMaintenanceRequestTab runat="server" id="PropertyMaintenanceRequestTab" />
                </div>
            </div>
        </div>
    </div>

    <script>

        var vOrient = 'p';
        // Listen for orientation changes
        window.addEventListener("orientationchange", function () {

            calc_container(vOrient);
        }, false);

        // portrait OR landscape
        function _portrait_landspace() {

            //
            if (window.matchMedia("(orientation: portrait)").matches) {
                vOrient = 'p';
            }

            if (window.matchMedia("(orientation: landscape)").matches) {
                vOrient = 'l';
            }

            var vDevice = navigator.userAgent;
            var w_width = $(window).width();
            if (vOrient == 'p') {
                if ($(window).width() < 768) {
                    $('#headerImg').hide();
                }
            }
            if (vOrient == 'l') {
                if ($(window).width() < 768) {
                    $('#headerImg').hide();
                }
            }

        } // end of p_l....


        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
                //$("#pageCenterContainer").css("margin-left", vML);
                //$("#pageCenterContainer").css("margin-right", vML);
            }
            else if ($(window).width() >= 768 && $(window).width() < 1024) {
                var vML = (w_width - 768) / 2;
                $('#pageCenterContainer').attr('id', 'temp');
            }
            if ($(window).width() == 1016) {
                $("#pageCenterContainer").css("width", "980px");
                //$("#pageCenterContainer").css("margin-left", "0px");
                //$("#pageCenterContainer").css("margin-right", "0px");
            }
            else if ($(window).width() < 768) {
                //$('#headerImg').hide();
            }

        } // end of calc....


        $(document).ready(function () {

            /* */
            var vObj = document.getElementById("header");
            //vObj.style.top = "-40px";
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "130px");
            if ($(window).width() <= 767) {
                $(".residentsPage").css("top", "100px");
                $(".residentsPage").css("background-color", "transparent");
            }
            //$('.wrapper').attr('style', 'height: 1500px !important');

            calc_container();

            //$("footer").css("position", "absolute");
            //$("footer").css("top", "1200px");
            if ($(window).width() <= 767) {
                $("footer").css("display", "none");
            }


        });

        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);

    </script>

    <script>


        $(window).bind("orientationchange", function () {
            setTimeout(function () {
                window.panelWidth = $('.mainTabSection').width();
                $('.panel_container .panel').each(function (index) {
                    $(this).css({ 'width': window.panelWidth + 'px', 'left': (index * window.panelWidth) + 'px' });
                    $('.mainTabSection .panels').css('width', (index + 1) * window.panelWidth + 'px');
                    changePanels($(".mainTabSection .tabs .selected").index());
                });
            }, 500);
        });

        var rtime = new Date(1, 1, 2000, 12, 0, 0);
        var timeout = false;
        var delta = 100;
        // Listen for resize changes
        $(window).resize(function () {

            rtime = new Date();
            if (timeout === false) {
                timeout = true;
                setTimeout(resizeend, delta);
            }
        });

        function resizeend() {

            if (new Date() - rtime < delta) {
                setTimeout(resizeend, delta);
            } else {
                timeout = false;
                window.panelWidth = $('.mainTabSection').width();
                $('.panel_container .panel').each(function (index) {
                    $(this).css({ 'width': window.panelWidth + 'px', 'left': (index * window.panelWidth) + 'px' });
                    $('.mainTabSection .panels').css('width', (index + 1) * window.panelWidth + 'px');
                    changePanels($(".mainTabSection .tabs .selected").index());
                });
            }
        } // end of resizeend....

    </script>
</asp:Content>
