﻿using EfxFramework;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Web;
using System.Web.Security;

namespace Efx.EfxResidents
{
    public partial class Site : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //cakel: 06/04/2015
            //CMallory - Task 00579 - Changed how the FAQ URL is set from a setting in the database to an actual file in the project.
            FAQLink.NavigateUrl = "/Faq.aspx";// Settings.RenterSiteFaqUrl;
            LogoLink.NavigateUrl = "/Default.aspx";
            TermsOfUseLink.NavigateUrl = Settings.TermsOfUse;
            PrivacyPolicyLink.NavigateUrl = Settings.PrivacyPolicy;

            string VersionNumber = "?version=" + EfxFramework.Settings.versionNumber.ToString();

            // Patrick Whittingham - 5/1/15 : DYNAMIC CSS
            chosenCSS.Href = EfxFramework.Settings.sharedCssUrl + "/chosen.css" + VersionNumber;
            chosenCSS.Href = "/styles/chosen.css" + VersionNumber;
            modalCSS.Href = EfxFramework.Settings.sharedCssUrl + "/Modal.css" + VersionNumber;

            //cakel: 06/22/2015 - Added version number to correct css issues
            DefaultCSS.Href = "/Styles/Default.css" + VersionNumber;
            ResponsiveCSS.Href = "/Styles/Responsive.css" + VersionNumber;
            InteriorSpecific.Href = "/Styles/InteriorSpecific.css" + VersionNumber;
            FontAwesomeCSS.Href = "/Styles/font-awesome.min.css" + VersionNumber;
            RenterFavicon.Href = "/RPO-favicon-Blue.ico" + VersionNumber;


            litYear.Text = DateTime.Now.Year.ToString(System.Globalization.CultureInfo.InvariantCulture);

            var RenterId = HttpContext.Current.User.Identity.Name.ToNullInt32();

            if (!RenterId.HasValue || RenterId.Value < 1)
            {
               LogoutLink.Visible = false;
                return;
            }

            if (RenterId > 1)
            {
                //LogInButton.Visible = false;
            }

            var Renter = new Renter(RenterId);

            if (Renter.RenterId < 1)
            {
                LogoutLink.Visible = false;
                //LogInButton.Visible = true;
                BasePageV2.SafePageRedirect("~/Account/LogOut.aspx");
            }

            WelcomeMessageText.Text = String.Format("Welcome, {0} ", Renter.FirstName);

            //Salcedo - 1/15/2015
            //FaqUrl.HRef = Settings.RenterSiteFaqUrl;


        }

        //cakel: 06/09/2015
        public void LogUserOut()
        {
            FormsAuthentication.SignOut();
          
        }



    }
}