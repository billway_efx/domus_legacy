﻿using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using System;
using Mb2x.ExtensionMethods;
using EfxFramework;
using EfxFramework.ExtensionMethods;
using EfxFramework.WebPayments;
using EfxFramework.PaymentMethods;
using RPO;

namespace Efx.EfxResidents
{
    public partial class ConfirmScheduledPayment : BasePageV2
    {
        private RPO.ActivityLog AL = new RPO.ActivityLog();

        public Page ViewPage { get { return this; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                AddValidationSummaryToPage();
                InitializeControls();
            }

            //Attempt to prevent double-click of Authorize button
            AuthorizeButton.Attributes.Add("onclick", " this.disabled = true; " + ClientScript.GetPostBackEventReference(AuthorizeButton, null) + ";");
        }

        private void InitializeControls()
        {
            var Id = HttpContext.Current.User.Identity.Name.ToNullInt32();
            if (!Id.HasValue || Id.Value < 1)
            {
                SafeRedirect("~/Account/Login.aspx");
                return;
            }

            AL.WriteLog(HttpContext.Current.User.Identity.Name, "User entered ConfirmScheduledPayment page.", (short)LogPriority.LogAlways);

            var Resident = new EfxFramework.Renter(Id);

            //Get queued payment for this renter ...
            var PaymentQueue = EfxFramework.PaymentQueue.GetPaymentQueueByRenterId(Resident.RenterId.ToString());

            //If there is a queued payment, display the queued payment information ...
            if (PaymentQueue.PaymentQueueId != 0)
            {
                AL.WriteLog(HttpContext.Current.User.Identity.Name, "Displaying payment queue information for PaymentQueueID " + PaymentQueue.PaymentQueueId.ToString(), (short)LogPriority.LogAlways);

                var Request = new EfxFramework.Ivr.PaymentRequestV2(
                    EfxFramework.PaymentChannel.AutoPayment, 
                    EfxFramework.Property.GetPropertyByRenterId(PaymentQueue.RenterId).PropertyId.HasValue
                        ? EfxFramework.Property.GetPropertyByRenterId(PaymentQueue.RenterId).PropertyId.Value : 0, 
                    PaymentQueue.RenterId);

                var Payer = EfxFramework.Payer.GetPayerByRenterId(PaymentQueue.RenterId);
                var CC = EfxFramework.PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(Payer.PayerId);

                FirstName.Text = Request.Resident.FirstName;
                LastName.Text = Request.Resident.LastName;

                EfxFramework.Address TheAddress = GetResidentAddress(Resident);
                BillingAddress.Text = TheAddress.AddressLine1;
                City.Text = TheAddress.City;
                State.Text = TheAddress.GetStateName();
                Zip.Text = TheAddress.PostalCode;

                PropertyName.Text = Request.Property.PropertyName;
                //NameOnCard.Text = PaymentQueue.CreditCardHolderName;
                NameOnCard.Text = CC.CreditCardHolderName;
                //CardNumber.Text = PaymentQueue.CreditCardAccountNumber.MaskAccountNumber();
                CardNumber.Text = CC.CreditCardAccountNumber.MaskAccountNumber();
                //ExpirationDate.Text = PaymentQueue.CreditCardExpirationMonth.ToString() + "/" + PaymentQueue.CreditCardExpirationYear.ToString();
                ExpirationDate.Text = CC.CreditCardExpirationMonth + "/" + CC.CreditCardExpirationYear;

                decimal TotalPayment = PaymentQueue.RentAmount.HasValue ? PaymentQueue.RentAmount.Value : 0;
                TotalPayment = TotalPayment + (PaymentQueue.CharityAmount.HasValue ? PaymentQueue.CharityAmount.Value : 0);
                TotalPayment = TotalPayment + (PaymentQueue.OtherAmount1.HasValue ? PaymentQueue.OtherAmount1.Value : 0);
                TotalPayment = TotalPayment + (PaymentQueue.OtherAmount2.HasValue ? PaymentQueue.OtherAmount2.Value : 0);
                TotalPayment = TotalPayment + (PaymentQueue.OtherAmount3.HasValue ? PaymentQueue.OtherAmount3.Value : 0);
                TotalPayment = TotalPayment + (PaymentQueue.OtherAmount4.HasValue ? PaymentQueue.OtherAmount4.Value : 0);
                TotalPayment = TotalPayment + (PaymentQueue.OtherAmount5.HasValue ? PaymentQueue.OtherAmount5.Value : 0);
                TotalPaymentAmount.Text = TotalPayment.ToString("C");

                CVVCode.Text = "";

            }
            else //If there is no queued payment available, simply redirect to the default page
            {
                SafeRedirect("~/Default.aspx");
                return;
            }

        }

        protected void AuthorizeButton_Click(object sender, EventArgs e)
        {
            
            //Attempt to authorize the transaction
            var Id = HttpContext.Current.User.Identity.Name.ToNullInt32();
            if (!Id.HasValue || Id.Value < 1)
            {
                SafeRedirect("~/Account/Login.aspx");
                return;
            }

            if (CVVCode.Text.Length == 0)
            {
                Type cstype = this.GetType();
                ClientScript.RegisterStartupScript(cstype, "startupScript", "<script language=JavaScript>alert('Invalid CVV Code.');</script>");
                return;
            }

            AL.WriteLog(HttpContext.Current.User.Identity.Name, "User clicked Authorize button.", (short)LogPriority.LogAlways);

            var Resident = new EfxFramework.Renter(Id);

            //Get queued payment for this renter ...
            var PaymentQueue = EfxFramework.PaymentQueue.GetPaymentQueueByRenterId(Resident.RenterId.ToString());

            if (PaymentQueue.PaymentQueueId != 0)
            {
                AL.WriteLog(HttpContext.Current.User.Identity.Name, "Authorizing payment for PaymentQueueID " + PaymentQueue.PaymentQueueId.ToString(), (short)LogPriority.LogAlways);

                var Request = new EfxFramework.Ivr.PaymentRequestV2(
                    EfxFramework.PaymentChannel.AutoPayment,
                    EfxFramework.Property.GetPropertyByRenterId(PaymentQueue.RenterId).PropertyId.HasValue
                        ? EfxFramework.Property.GetPropertyByRenterId(PaymentQueue.RenterId).PropertyId.Value : 0,
                    PaymentQueue.RenterId);

                var Payer = EfxFramework.Payer.GetPayerByRenterId(Resident.RenterId);
                if (Payer.PayerId == 0)
                {
                    Type cstype = this.GetType();
                    ClientScript.RegisterStartupScript(cstype, "startupScript", "<script language=JavaScript>alert('Unable to resolve Credit Card information. Please contact system administration.');</script>");
                    return;
                }

                var PayerCreditCard = EfxFramework.PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(Payer.PayerId);

                //var Month = ("0" + PayerCreditCard.CreditCardExpirationMonth);
                //Month = Month.Substring(Month.Length - 2);
                //var Year = PayerCreditCard.CreditCardExpirationYear.ToString();
                //Year = Year.Substring(Year.Length - 2);

                RenterPaymentInfo RenterPayment = new RenterPaymentInfo
                {
                    RenterId = PaymentQueue.RenterId,
                    PaymentType = (EfxFramework.PaymentMethods.PaymentType)PaymentQueue.PaymentTypeId,
                    BankAccountNumber = PaymentQueue.BankAccountNumber,
                    BankRoutingNumber = PaymentQueue.BankRoutingNumber,
                    CreditCardNumber = PayerCreditCard.CreditCardAccountNumber,
                    CreditCardHolderName = PayerCreditCard.CreditCardHolderName,
                    Cvv = CVVCode.Text,
                    //ExpirationDate = EfxFramework.Scheduler.SchedulerBase.GetDateTime(PayerCreditCard.CreditCardExpirationDate),
                    //ExpirationDate = EfxFramework.Scheduler.SchedulerBase.GetDateTime(Month + Year),
                    ExpirationDate = new DateTime(PayerCreditCard.CreditCardExpirationYear, PayerCreditCard.CreditCardExpirationMonth, 1),
                    CharityAmount = PaymentQueue.CharityAmount,
                    CharityName = PaymentQueue.CharityName,
                    RentAmount = PaymentQueue.RentAmount,
                    RentAmountDescription = PaymentQueue.RentAmountDescription,
                    OtherAmount = PaymentQueue.OtherAmount1,
                    OtherAmountDescription = PaymentQueue.OtherDescription1,
                    OtherAmount2 = PaymentQueue.OtherAmount2,
                    OtherAmountDescription2 = PaymentQueue.OtherDescription2,
                    OtherAmount3 = PaymentQueue.OtherAmount3,
                    OtherAmountDescription3 = PaymentQueue.OtherDescription3,
                    OtherAmount4 = PaymentQueue.OtherAmount4,
                    OtherAmountDescription4 = PaymentQueue.OtherDescription4,
                    OtherAmount5 = PaymentQueue.OtherAmount5,
                    OtherAmountDescription5 = PaymentQueue.OtherDescription5,
                    DisplayName = Request.Resident.DisplayName,
                    PayerId = Request.Resident.PayerId.HasValue ? Request.Resident.PayerId.Value : 0,
                    PropertyId = Request.PropertyId
                };

                var Result = PaymentRequest.ProcessPayment(RenterPayment, false);
                
                //Regardless of the outcome, mark the queue record as processed and go to the confirmation/error page
                PaymentQueue.IsProcessed = true;
                EfxFramework.PaymentQueue.Set(PaymentQueue);

                if (Result.RentAndFeeResult == GeneralResponseResult.Success)
                {
                    AL.WriteLog(HttpContext.Current.User.Identity.Name, "Payment success. Redirecting to " +
                        String.Format("~/PaymentStatus.aspx?TransactionId={0}", Result.RentAndFeeTransactionId), (short)LogPriority.LogAlways);
                    BasePage.SafePageRedirect(String.Format("~/PaymentStatus.aspx?TransactionId={0}", Result.RentAndFeeTransactionId));
                }
                else //Otherwise, display the unsuccessful transaction
                {
                    string CharityFailureMessage = "";
                    string RentAndFeeFailureMessage = "";
                    if (Result.CharityResult != GeneralResponseResult.Success)
                    {
                        CharityFailureMessage = Result.CharityResponseMessage;
                    }
                    else
                    {
                        if (Result.CharityTransactionId != null)
                            CharityFailureMessage = "Success";
                    }
                    if (Result.RentAndFeeResult != GeneralResponseResult.Success)
                    {
                        RentAndFeeFailureMessage = Result.RentAndFeeResponseMessage;
                    }
                    AL.WriteLog(HttpContext.Current.User.Identity.Name, "Payment failure. Redirecting to " +
                        String.Format("~/PaymentFailed.aspx?CharityFailureMessage={0}&RentAndFeeFailureMessage={1}", CharityFailureMessage, RentAndFeeFailureMessage),
                        (short)LogPriority.LogAlways);
                    BasePage.SafePageRedirect(String.Format("~/PaymentFailed.aspx?CharityFailureMessage={0}&RentAndFeeFailureMessage={1}", CharityFailureMessage, RentAndFeeFailureMessage));
                }

            }
            else
            {
                Type cstype = this.GetType();
                ClientScript.RegisterStartupScript(cstype, "startupScript", "<script language=JavaScript>alert('No scheduled payment information found. Please contact system administration.');</script>");
                return;
            }

        }

        protected EfxFramework.Address GetResidentAddress(EfxFramework.Renter Resident)
        {
            var BillingAddress = EfxFramework.Address.GetAddressByRenterIdAndAddressType(Resident.RenterId, (int)TypeOfAddress.Billing);
            if (BillingAddress.AddressId > 0)
                return BillingAddress;

            return new EfxFramework.Address
            {
                AddressId = BillingAddress.AddressId,
                AddressTypeId = BillingAddress.AddressId < 1 ? (int)TypeOfAddress.Primary : (int)TypeOfAddress.Billing,
                AddressLine1 = BillingAddress.AddressId < 1 ? Resident.StreetAddress : BillingAddress.AddressLine1,
                AddressLine2 = BillingAddress.AddressId < 1 ? Resident.Unit : BillingAddress.AddressLine2,
                City = BillingAddress.AddressId < 1 ? Resident.City : BillingAddress.City,
                StateProvinceId = BillingAddress.AddressId < 1 ? Resident.StateProvinceId.HasValue ? Resident.StateProvinceId.Value : 1 : BillingAddress.StateProvinceId,
                PostalCode = BillingAddress.AddressId < 1 ? Resident.PostalCode : BillingAddress.PostalCode,
                PrimaryEmailAddress = BillingAddress.AddressId < 1 || String.IsNullOrEmpty(BillingAddress.PrimaryEmailAddress) ? Resident.PrimaryEmailAddress : BillingAddress.PrimaryEmailAddress,
                PrimaryPhoneNumber = BillingAddress.AddressId < 1 || String.IsNullOrEmpty(BillingAddress.PrimaryPhoneNumber) ? Resident.MainPhoneNumber : BillingAddress.PrimaryPhoneNumber
            };
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            SafeRedirect("~/Default.aspx");
            return;
        }


    }

}