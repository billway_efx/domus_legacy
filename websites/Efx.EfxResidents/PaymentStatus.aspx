﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PaymentStatus.aspx.cs" Inherits="Efx.EfxResidents.PaymentStatus" %>
<%@ Import Namespace="EfxFramework" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

        <div id="pageCenterContainer" class="pageContainer payStatusPage residentsPage">
        <!--Main Tabs Section -->
        <div class="singleItemSection" style="width: 80% !important; margin-left: 10% !important; margin-right: 10% !important; ">
            <div class="slimPadding">
                <div class="receiptHolder">
                    <header>
                        <div class="printLogo"><img src="/Images/RentPaidOnlineLogo.png" alt ="rentPaidOnlineLogo" /></div>
                        <h3 class="fr1Color">Payment Status</h3>
                        <p class="fr1Color">Your Transaction was Processed</p>
                        <h4 class="fr2Color">Thank You</h4>
                        <p class="fr1Color">Rent and Fees Transaction Accepted</p>
                    </header>
                    <div class="receipt">
                        <div class="recieptColumn">
                            
                            <!-- **First Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Transaction ID**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Transaction ID</span>
                                    <p><asp:Label ID="TransactionIdLabel" runat="server"/></p>
                                </div>
                            </div>

                            <!-- **Second Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Rent**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Rent and Fees</span>
                                    <p>
                                        <asp:Label ID="Label2" runat="server" Text="Rent: "></asp:Label><asp:Label ID="RentAmountLabel" runat="server"/></p>
                                </div>                            
                            </div>
                            
                          <!--CAKEL: TASK 00407 -->
                            <div class="formRow rowSpacingAdjust">
                                <div class="formFullWidth">
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" ShowHeader="False" OnRowDataBound="GridView1_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Name">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FeeName") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("FeeName") + ": " %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="FeeAmount" HeaderText="Amount" DataFormatString="{0:c}" />
                                        </Columns>
                                    </asp:GridView>
                                    </div>

                            </div>


                            <!-- **Third Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Convenience Fee**-->
                                 <div class="formFullWidth">
                                     <span class="formLabel">Convenience Fee</span>
                                     <p><asp:Label ID="ConvenienceFeeLabel" runat="server"/></p>
                                 </div>
                            </div>
                            
                            <!-- **Fourth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Total Payment Amount**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Total Payment Amount</span>
                                    <p><asp:Label ID="TotalPaymentAmountLabel" runat="server" /></p>
                                </div>                                 
                            </div>

                            <!-- **Fifth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Property**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Property</span>
                                    <p><asp:Label ID="PropertyNameLabel" runat="server" /></p>
                                    <p><asp:Label ID="PropertyAddressLabel" runat="server" /></p>
                                    <p><asp:Label ID="PropertyCityStateZipLabel" runat="server" /></p>
                                </div>                                 
                            </div>


                            
                            <!-- **Sixth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**RPO Support**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">RPO Support</span>
                                    <p class="fr1Color"><a class="telephone" href="tel:855-769-7368">Support Line: <%=Settings.SupportPhone %></a></p>
                                    <p class="fr1Color">Email:<a class="fr2Color" href="mailto:support@rentpaidonline.com"> <%=Settings.SupportEmail %></a></p>
                                </div>                                 
                            </div>

                        </div>
                        
                        <div class="recieptColumn">
                            
                            <!-- **First Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**First Name **-->
                                <div class="formFullWidth">
                                    <span class="formLabel">First Name</span>
                                    <p><asp:Label ID="FirstNameLabel" runat="server" /></p>
                                </div>
                            </div>
                            
                            <!-- **Second Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Middle Name **-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Middle Name</span>
                                    <p><asp:Label ID="MiddleNameLabel" runat="server" /></p>
                                </div>
                            </div>
                            
                            <!-- **Third Row**-->
                            <div class="formRow rowSpacingAdjust">
                                <!--**Last Name**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Last Name</span>
                                    <p><asp:Label ID="LastNameLabel" runat="server" /></p>
                                </div>
                            </div>
                            
                            <!-- **Fourth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                 <!--**Suffix**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Suffix</span>
                                    <p><asp:Label ID="SuffixLabel" runat="server" /></p>
                                </div>
                            </div>
                            
                            <!-- **Fifth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                 <!--**Billing Address**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Billing Address</span>
                                    <p><asp:Label ID="BillingAddressLabel" runat="server" /></p>
                                    <p><asp:Label ID="BillingCityStateZipLabel" runat="server" /></p>
                                </div>
                            </div>
                            
                            <!-- **Sixth Row**-->
                            <div class="formRow rowSpacingAdjust">
                                 <!--**Phone Number**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Phone Number</span>
                                    <p><asp:Label ID="PhoneNumberLabel" runat="server" /></p>
                                </div>
                            </div>
                            
                            <!-- **Seventh Row**-->
                            <div class="formRow rowSpacingAdjust">
                                 <!--**Payment Method**-->
                                <div class="formFullWidth">
                                    <span class="formLabel">Payment Method</span>
                                    <p><asp:Label ID="PaymentMethodLabel" runat="server" /></p>
                                </div>
                            </div>
                           
                        </div>

                     </div>
                 </div>
                <div class="printButton" style="padding: 22px; " >
                    <div class="formFullWidth" >
                        <asp:LinkButton ID="PrintButton" runat="server" CssClass="orangeButton" OnClientClick="window.print(); return false;">Print</asp:LinkButton>
                    </div>
                </div>
                <div class="clearFix"></div>
            </div>                            
        </div>
    </div>

    <script>

        var vOrient = 'p';
        // Listen for orientation changes
        window.addEventListener("orientationchange", function () {

            calc_container(vOrient);

        }, false);

        // portrait OR landscape
        function _portrait_landspace() {

            //
            if (window.matchMedia("(orientation: portrait)").matches) {
                vOrient = 'p';
            }

            if (window.matchMedia("(orientation: landscape)").matches) {
                vOrient = 'l';
            }

            var vDevice = navigator.userAgent;
            var w_width = $(window).width();
            if (vOrient == 'p') {
                if ($(window).width() < 768) {
                    $('#headerImg').hide();
                }
            }
            if (vOrient == 'l') {
                if ($(window).width() < 768) {
                    $('#headerImg').hide();
                }
            }

        } // end of p_l....



        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
                $("#pageCenterContainer").css("margin-left", vML);
                $("#pageCenterContainer").css("margin-right", vML);
            }
            else if ($(window).width() >= 768 && $(window).width() < 1024) {
                var vML = (w_width - 768) / 2;
                $("#pageCenterContainer").css("width", "90%");
                $("#pageCenterContainer").css("margin-left", "5%");
                $("#pageCenterContainer").css("margin-right", "5%");
                //$('#pageCenterContainer').attr('id', 'temp');
            }
            if ($(window).width() == 1016) {
                //$("#pageCenterContainer").css("width", "980px");
                //$("#pageCenterContainer").css("margin-left", vML );
                //$("#pageCenterContainer").css("margin-right", vML );
            }
            else if ($(window).width() < 760 ) {
                $('#headerImg').hide();
            }


        } // end of calc....

        $(document).ready(function () {

            / /
            var vObj = document.getElementById("header");
            //vObj.style.top = "-40px";
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "180px");
            if ($(window).width() <= 767) {
                $(".residentsPage").css("top", "120px");
                $(".residentsPage").css("background-color", "white");
            }
            //$('.wrapper').attr('style', 'height: 1500px !important');

            calc_container();

            $("footer").css("position", "absolute");
            $("footer").css("top", "1100px");
            if ($(window).width() <= 767) {
                $("footer").css("display", "none");
            }


    });

    // Listen for resize changes
    window.addEventListener("resize", function () {

        calc_container();
    }, false);

</script>



</asp:Content>
