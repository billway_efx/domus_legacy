$(function () {
	// Main Template jQuery

	menuHeight();
	$(window).resize(menuHeight);
});

function menuHeight() {
	var windowHeight = $(window).height(),
		headerHeight = $('header').outerHeight(),
		ulPadding = 40,
		$sideNav = $('div.side-nav');

	// subnav height
	$('div.side-nav ul ul').each(function () {
		ulPadding = parseInt($(this).css('padding-top')) + parseInt($(this).css('padding-bottom'));

		$(this).height(windowHeight - ulPadding);
	});

	// fixed or not
	menuPosition();
	$(window).scroll(menuPosition);

	function menuPosition() {
		var scrollPosition = $(window).scrollTop();

		if (scrollPosition > headerHeight && !$sideNav.hasClass('unfix')) {
			$sideNav.addClass('fixed');
		}
		else {
			$sideNav.removeClass('fixed');
		}
	}
}