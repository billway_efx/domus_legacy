﻿//These are globally used functions to be added in the head of the master page


/*--------- Specifying global variables--------- */
var panelWidth = 0; /*For main tab section -- this defines the panel width, and give ability to change the panel within the CSS and not have to constantly modify in the JavaScript if we want to change size of panels*/
var startPanel = 1; /*For main tab section -- this defines which panel we want to start on, we're using JQuery's nth-child rule, which starts at 1 and not 0 -- can pick any one of the panels to be start panel, starts at one and goes up in newIndex var*/


// =============================================================================================
// initialize tabs
// =============================================================================================
function _global_init_tabs() {
    console.log("git...");
    /* !!--------- Tabs ---------!! */
    /*reset panelWidth variable -- Here we are using JQuery to query the current panel width*/
    window.panelWidth = $('.mainTabSection').width();

    if ($('.mainTabSection').width() == 814) { window.panelWidth = 931; }

    /*find and position each individual panel inside of the .panels container*/
    $('.panel_container .panel').each(function (index) { /*tells JQuery that for each panel we want to find that we want to keep track of the number*/
        console.log("git111...");
        /* every time we find a panel we add the css width property as the panelWidth Variable*/
        $(this).css({ 'width': window.panelWidth + 'px', 'left': (index * window.panelWidth) + 'px' });

        /*sets each panel to match the width of the overall panel container, +px because number much be defined in px.
        We then do a caculation of the index variable (*window.panelWidth) this defines the "left" css attribute -- for every panel we find inside of panel container, we set the left position (since they are absolute positioned) to whatever the panel width is (first: 0 next: width of the panel, etc)*/

        /*takes panels container, which holds all the panels, and makes sure that the width is long enough to hold all of the individual panels
        index +1, starts at 0, must add 1 so when we do a caculation we do not get 0, since the width is absolute*/
        $('.mainTabSection .panels').css('width', (index + 1) * window.panelWidth + 'px');
    });

    /*Applying click events to the tab items -- for each span item we find in the .tabs container, we tant to specify the click event*/
    $('.mainTabSection .tabs span').each(function () {

        $(this).on('click', function () {
            console.log("git click...");
            changePanels($(this).index()); /*custom function which is defined outside the document ready function -- $(this).index() is us sending the index position when changePanels is clicked JQuery does the heavy lifting here*/
        });

    });

    /*Here we are triggering the corresponding span which matches the startPanel varible -- first child based on the start panel, we're finding the first span and triggering a click event*/
    $('.mainTabSection .tabs span:nth-child(' + window.startPanel + ')').trigger('click');

} // end of initial tabs....


/*--------- Document ready function---------*/ //anything within these brackets will happen once all of the html has been loaded on the page*/
$(document).ready(function () {
    console.log("ready...");
    _global_init_tabs();
    
}); // !!--------- End of document ready function



// Listen for resize changes
window.addEventListener("resize", function () {
    console.log("resize...");
    //_global_init_tabs();
}, false);



/*--------- Defining custom functions of tabs section ---------*/

/*--- Changing Panels function--- */
function changePanels(newIndex) { /*newIndex is a variable to capture information*/

    console.log("change panels...");
    /*in this caculation, we need to know, based on the tab selected, where to animate the panels to*/
    var newPanelPosition = (window.panelWidth * newIndex) * -1; /*variable for caculations -- this will give us a negative number for the width of the panels, so we know where to slide the panels container towards the left on the x axis*/
    var newPanelHeight = $('.mainTabSection .panel:nth-child(' + (newIndex + 1) + ')').find('.panel_content').height() + 25;
    /*nth-child in JQuery picks out specific child based on an index number. Here we are figuring out which one of the panels we want to roll to based on which children we clicked on
     we're doing a calculation based on the newIndex variable. (+1 because we can't start at 0), searches for panel_content height and adds 15 pixels of space on the bottom of the panel.
    */

/*--- creating animations for panel position and height---*/

    /*animation of the .panels class to the left, using the newPanelPosition varible -- speed is at 1000 millisecs*/
    $('.mainTabSection .panels').animate({ left: newPanelPosition }, 500);
    /*animation of the .panel_container class height, using the newPanelHeight varible -- speed is at 0 millisecs, which means height adjusts without any noticable animation*/
    $('.mainTabSection .panel_container').animate({ height: newPanelHeight }, 0);

    /*adding selected panel class to any tab that is clicked on*/
    $('.mainTabSection .tabs span').removeClass('selected'); /*Removing selected class from all of the span -- clean slate*/
    $('.mainTabSection .tabs span:nth-child(' + (newIndex + 1) + ')').addClass('selected');/*Adding the selected class to the selected tab*/

}