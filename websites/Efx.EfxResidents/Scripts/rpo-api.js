﻿//helper functions
function isEmpty(value) {
    return angular.isUndefined(value) || value === '' || value === null || value !== value;
}
angular.module('API', [])
.factory('companyService', function ($rootScope, $http, $q, $filter) {
    var companies = null;
    var allCompanies = function () {
        var deferred = $q.defer();
        if (isEmpty(companies)) {
            $http.get('/api/company/all').success(function (data) {
                companies = data;
                deferred.resolve(data);
            });
        } else {
            deferred.resolve(companies);
        }
        return deferred.promise;
    };
    var deleteCompany = function (id) {
    	var deferred = $q.defer();
    	$http.get('/api/company/delete/' + id).success(function (response) {
    		deferred.resolve(response);
    	});
    	return deferred.promise;
    };
    return {
        getAllCompanies: allCompanies,
        deleteCompany: deleteCompany
    };
})
.factory('propertyService', function ($rootScope, $http, $q, $filter) {
    var properties = null;
    var staffProperties = null;
    var staffPropertiesForDropdown = null;
    var propertiesForDropdown = null;
    var property = null;
    var currentPropertyId = null;
    var allProperties = function () {
        var deferred = $q.defer();
        if (isEmpty(properties)) {
            $http.get('/api/property/all?dropdown=false').success(function (data) {
                properties = data;
                deferred.resolve(data);
            });
        } else {
            deferred.resolve(properties);
        }
        return deferred.promise;
    };
    var allPropertiesForDropdown = function () {
        var deferred = $q.defer();
        if (isEmpty(propertiesForDropdown)) {
            $http.get('/api/property/all?dropdown=true').success(function (data) {
                propertiesForDropdown = data;
                deferred.resolve(data);
            });
        } else {
            deferred.resolve(propertiesForDropdown);
        }
        return deferred.promise;
    };
    var companyProperties = function (companyId) {
        var deferred = $q.defer();
        if (isEmpty(properties)) {
            allProperties().then(function (propertyList) {
                deferred.resolve($filter('filter')(propertyList, { CompanyId: companyId }));
            });
        } else {
            deferred.resolve($filter('filter')(properties, { CompanyId: companyId }));
        }
        return deferred.promise;
    };
    var userProperties = function (userId, forDropdown) {
        var deferred = $q.defer();
        if (isEmpty(staffProperties)) {
            $http.get('/api/property/byuser/' + userId + '?dropdown=false').success(function (data) {
                staffProperties = data;

                deferred.resolve(data);
            });
        } else {
            deferred.resolve(staffProperties);
        }
        return deferred.promise;
    };
    var userPropertiesForDropdown = function (userId) {
        var deferred = $q.defer();
        if (isEmpty(staffPropertiesForDropdown)) {
            $http.get('/api/property/byuser/' + userId + '?dropdown=true').success(function (data) {
                staffPropertiesForDropdown = data;

                deferred.resolve(data);
            });
        } else {
            deferred.resolve(staffPropertiesForDropdown);
        }
        return deferred.promise;
    };
    var propertyEntity = function (propertyId) {

        var deferred = $q.defer();
        if (isEmpty(property) || currentPropertyId !== propertyId) {
            $http.get('/api/property/one/' + propertyId).success(function (data) {
                currentPropertyId = propertyId;
                property = data;
                deferred.resolve(data);
            });
        } else {
            deferred.resolve(property);
        }
        return deferred.promise;
    };
    var propertyToggleActive = function (propertyToToggle) {
        var deferred = $q.defer();
        $http.post('/api/property/toggleactive', propertyToToggle).success(function (response) {
            property.isActive = !property.isActive;
            deferred.resolve(response);
        });
        return deferred.promise;
    };
    var propertyDelete = function (propertyId) {
    	var deferred = $q.defer();
    	$http.get('/api/property/delete/' + propertyId).success(function (response) {
    		deferred.resolve(response);
    	});
    	return deferred.promise;
    };
    return {
        getAllProperties: allProperties,
        getAllPropertiesForDropdown: allPropertiesForDropdown,
        getUserPropertiesForDropdown: userPropertiesForDropdown,
        getCompanyProperties: companyProperties,
        getUserProperties: userProperties,
        getProperty: propertyEntity,
        toggleActive: propertyToggleActive,
        deleteProperty: propertyDelete
    };
})
.factory('renterService', function ($rootScope, $http, $q, $filter) {
    var renters = null;
    var renter = null;
    var currentRenterId = null;
    var allRenters = function () {
        var deferred = $q.defer();
        if (isEmpty(renters)) {
            $http.get('/api/renter/all').success(function (data) {
                renters = data;
                deferred.resolve(data);
            });
        } else {
            deferred.resolve(renters);
        }
        return deferred.promise;
    };
    var propertyRenters = function (propertyId) {
        var deferred = $q.defer();
        if (isEmpty(renters)) {
            allRenters().then(function (rentersList) {
                deferred.resolve($filter('filter')(rentersList, { RenterPropertyId: propertyId }));
            });
        } else {
            deferred.resolve($filter('filter')(renters, { RenterPropertyId: propertyId }));
        }
        return deferred.promise;
    };
    var renterEntity = function (renterId) {

        var deferred = $q.defer();
        if (isEmpty(renter) || currentRenterId !== renterId) {
            $http.get('/api/renter/one/' + renterId).success(function (data) {
                currentRenterId = renterId;
                renter = data;
                deferred.resolve(data);
            });
        } else {
            deferred.resolve(renter);
        }
        return deferred.promise;
    };
    var renterToggleActive = function (renterToToggle) {
        var deferred = $q.defer();
        $http.post('/api/renter/toggleactive', renterToToggle).success(function (response) {
            renter.isActive = !renter.isActive;
            deferred.resolve(response);
        });
        return deferred.promise;
    };
    return {
        getAllRenters: allRenters,
        getPropertyRenters: propertyRenters,
        getRenter: renterEntity,
        toggleActive: renterToggleActive
    };
})
.factory('userService', function ($rootScope, $http, $q, $filter) {
    var isAdmin = null;
    var isCorporateAdmin = null;
    var userId = null;

    return {
        getUserId:
            function () {
                var deferred = $q.defer();
                if (isEmpty(userId)) {
                    $http.get('/api/user/id').success(function (data) {
                        userId = data;
                        deferred.resolve(userId);
                    });
                } else {
                    deferred.resolve(userId);
                }
                return deferred.promise;
            },
        userIsAdmin:
            function () {
                var deferred = $q.defer();
                if (isEmpty(isAdmin)) {
                    $http.get('/api/user/isadmin').success(function (data) {
                        isAdmin = data;
                        deferred.resolve(isAdmin);
                    });
                } else {
                    deferred.resolve(isAdmin);
                }
                return deferred.promise;
            },
        userIsCorporateAdmin:
			function () {
			    var deferred = $q.defer();
			    if (isEmpty(isCorporateAdmin)) {
			        $http.get('/api/user/iscorporateadmin').success(function (data) {
			            isCorporateAdmin = data;
			            deferred.resolve(isCorporateAdmin);
			        });
			    } else {
			        deferred.resolve(isCorporateAdmin);
			    }
			    return deferred.promise;
			}
    };
})
.factory('calendarEventService', function ($rootScope, $http, $q) {
    var calendarEvent = null;
    var eventId = null;
    return {
        createEvent:
                function () {
                    $rootScope.$broadcast('event:create');
                },
        saveNewEvent:
            function (calendarEvent) {
                $rootScope.$broadcast('event:saved');
                var deferred = $q.defer();
                $http.post('/api/calendarevent/addevent', calendarEvent).success(function (data) {
                        deferred.resolve(data);
                    });
                return deferred.promise;
            },
        saveExistingEvent:
            function (calendarEvent) {
                $rootScope.$broadcast('event:saved');
                var deferred = $q.defer();
                $http.post('/api/calendarevent/updateevent', calendarEvent).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        deleteEvent:
            function (eventId) {
                var deferred = $q.defer();
                $http.post('/api/calendarevent/deleteevent/' + eventId).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        getEvents:
			function () {
				var deferred = $q.defer();
				$http.get('/api/calendarevent/all').success(function (data) {
				    deferred.resolve(data);
				});
				return deferred.promise;
			},
        getEvent:
            function(id) {
                var deferred = $q.defer();
                $http.get('/api/calendarevent/getbyeventid/'+id).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        showEvent:
            function (id) {
                console.log('setting up event to show');
                eventId = id;
                $rootScope.$broadcast('Event:Show');
            },
        getEventId:
            function () {
                return eventId;
            },
        getEventTypes:
            function () {
                var deferred = $q.defer();
                $http.get('/api/calendarevent/geteventtypes').success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            }
    };
})
.directive(
    'loading', function () {
        return {
            restrict: 'E',
            templateUrl: '/loading.html',
            replace: true,
            transclude: true
        };
    });