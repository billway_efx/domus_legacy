﻿var calendarApp = angular.module('rpoCalendar', ['$strap.directives', 'ui.calendar', 'ngSanitize'])
.factory('calendarEventService', function ($rootScope, $http, $q) {
    var eventId = null;
    return {
        getEvents:
			function () {
			    var deferred = $q.defer();
			    $http.get('/api/calendarevent/getbyresidentid').success(function (data) {
			        deferred.resolve(data);
			    });
			    return deferred.promise;
			},
        getEvent:
            function (id) {
                var deferred = $q.defer();
                $http.get('/api/calendarevent/getbyeventid/' + id).success(function (data) {
                    deferred.resolve(data);
                });
                return deferred.promise;
            },
        showEvent:
            function (id) {
                console.log('setting up event to show');
                eventId = id;
                $rootScope.$broadcast('Event:Show');
            },
        getEventId:
            function () {
                return eventId;
            }
    };
})
.directive(
    'rpoCalendar', function () {
        return {
            restrict: 'A',
            templateUrl: '/event-calendar.html',
            replace: true,
            transclude: true
        };
    });
calendarApp.value('$strapConfig', {
    datepicker: {
        language: 'en',
        format: 'mm/dd/yy'
    }
});
function CalendarController($rootScope, $scope, calendarEventService) {
    $rootScope.$broadcast('Calendar');
    var eventsPromise = calendarEventService.getEvents();
    $scope.calendar = { events: eventsPromise };
    $scope.uiConfig = {
        calendar: {
            height: 500,
            header: {
                left: 'month agendaWeek agendaDay',
                center: 'title',
                right: 'today prev,next'
            },
            dayClick: function () {
                console.log('day clicked');
            },
            eventClick: function(event) {
                if (event.id) {
                    console.log('show event: ' + event.id);
                    calendarEventService.showEvent(event.id);
                    $rootScope.$broadcast('Event:Show');
                    return false;
                }
            },
            editable: false,
            allDayDefault:false
        }
    };
    
}
function EventDetailsController($rootScope, $scope, $sanitize, calendarEventService) {
    $rootScope.$on('Event:Show', function () {
        console.log('getting event to show...');
        eventId = calendarEventService.getEventId();
        if (eventId) {
            console.log('got event id ' + eventId);
            calendarEventService.getEvent(eventId).then(function (event) {
                $scope.event = event;
                $scope.eventDetails = $sanitize(event.Details);
                $('#eventDetails').modal('show');
            });
        }
    });
}

function EventListController($rootScope, $scope, calendarEventService, userService) {
    $rootScope.$broadcast('List');
    calendarEventService.getEvents().then(function (events) {
        $scope.events = events.events;
    });
    $scope.showEvent = function (eventId) {
        console.log('show event: ' + eventId);
        calendarEventService.showEvent(eventId);
        $rootScope.$broadcast('Event:Show');
    };
    $scope.createEvent = function () {
        calendarEventService.createEvent();
        $('#eventCreate').modal('show');
    };
    $scope.deleteEvent = function (eventToDelete) {
        if (confirm('Are you sure you want to delete this event?')) {
            var index = $scope.events.indexOf(eventToDelete);
            console.log(eventToDelete);
            calendarEventService.deleteEvent(eventToDelete.id).then(
                function (deleted) {
                    if (deleted == 'true') {
                        $scope.events.splice(index, 1);
                    }
                });
            
        }
    };
    userService.userIsAdmin().then(function (isAdmin) {
        $scope.isRPOAdmin = isAdmin;
        userService.getUserId().then(function (userId) {
            $scope.userId = userId;
        });
    });
}
function TitleController($scope, $rootScope) {
    $rootScope.$on('Calendar', function () {
        $scope.PageTitle = 'Event Calendar';
    });
    $rootScope.$on('List', function () {
        $scope.PageTitle = 'Event List';
    });
    $rootScope.$on('Details', function () {
        $scope.PageTitle = 'Event Details';
    });
}
angular.bootstrap($('#calendar'), ["rpoCalendar"]);