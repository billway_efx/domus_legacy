﻿using System;
using EfxFramework.Web;
using EfxFramework.Interfaces.Account;

namespace Efx.EfxResidents.Account
{
    public partial class LogOut : BasePageV2, ILogoutV2
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var Presenter = new EfxFramework.Presenters.Account.LogoutV2(this);
        }
    }
}