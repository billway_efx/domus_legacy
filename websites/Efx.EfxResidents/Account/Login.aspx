﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Efx.EfxResidents.Account.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>


    <!--cakel: Task 00482 -->
    <asp:Panel ID="Panel1" runat="server" DefaultButton="LoginButton">



   <div class="loginCore" >
       <!-- margin-top: 165px;  -->
       <!-- CMallory - Task 00528 - Changed max-width from 430 to 500 and max-height from 420 to 1000px -->
    <div class="pageContainer loginPage login1" style="background: rgba(3, 18, 51, 0.6); border: none; padding: 20px; max-width: 500px; max-height: 1000px; text-align: center; padding-top: 10px; position:inherit; ">
        <fieldset id="loginFieldset" style="border: 1px white solid; width: 100%; height: 100%; border-radius: 6px;">

            <legend class="" style="border: 1px black solid; margin-left: 1em; padding: 0.2em 0.8em; color: white; font-size: 120%; font-weight: bold; text-align:left;">Resident Login</legend>

        <%--<div class="loginTitle"><h2>Login</h2></div>--%>
                
    <section class="section95" style="margin: 14px; width: 89% !important;">
        <div class="formHolder formContainer login1Container">
        <!--**First Row**-->
        <div class="formRow">
            <!--User Name Text Box -->
            <div class="formFullWidth">
                <span class="formLable login1Label">Email Address (username)</span>
                <!--CMallory - Task 00476 - The hidden input type box below disables the save password option for IE and partially works for Firefox by not allowing them to save a password -->
                <input type="text" style="display:none" />
                <asp:TextBox ID="InputUserNameTextBox" runat="server" CssClass="formFullWidth smInputText" Style="background-color: white !important;" AutoCompleteType="Disabled"></asp:TextBox>
            </div>
        </div>
               
        <!--**Second Row**-->
        <div class="formRow">
            <!--Password Text Box -->
            <div class="formFullWidth">
                <span class="formLable login1Label">Password</span>
                <!--CMallory - Task 00476 - The hidden input type box below disables the save password option for IE and partially works for Firefox by not allowing them to save a password -->
                <input type="text" style="display:none" />  
                <asp:TextBox ID="InputPasswordTextBox" runat="server" TextMode="Password" CssClass="formFullWidth passwordField mb-0" Style="background-color: white !important; -webkit-text-security: square !important;" AutoCompleteType="Disabled"></asp:TextBox>
                <input type="text" style="display:none" /> 
            </div>
        </div>
                
        <div class ="displayNone">
            <div id="ForgotPassword" >            
                <uc1:ForgotPasswordWindow runat="server" ID="ForgotPasswordWindow" />
            </div>
        </div>

        <asp:LinkButton ID="ForgotPasswordLinkButton" runat="server" CausesValidation="False" CssClass="smallLink fp-color" OnClick="ForgotPasswordLinkButtonClick">Forgot Password?</asp:LinkButton>
        
        <!--Salcedo - 1/15/2015 - task # 00349 - make link dynamic -->
        <asp:LinkButton ID="SignUpBtnMobile" runat="server" CausesValidation="False" CssClass="smallLink registerLinkMobile" PostBackUrl="http://www.rentpaidonline.com/CurrentResidents.aspx">Register</asp:LinkButton>

        
        <!--**Button Time**-->
        <div class="generalButtonHolder">
                <asp:Button ID="LoginButton" runat="server" CssClass="orangeButton currentResidentsSubmit loginButton" Text="Log In" OnClientClick="ChangeType()" />
        </div>
        </div> 
            <div style="margin-top: 25px" class="register">
                 <fieldset class="login-br2" style="border-radius: 6px; border-color: #06cef9; width:89%;">
                      <legend style="margin-left: 0px; text-align:left; color: white;" >
                          <span class="formLabel ntr-color" style="text-align:left; color: white !Important; ">&nbsp; Need to register? &nbsp;</span>
                      </legend>
                      <p class="frWhite" style="text-align:left; margin-left:7px;">Please click the sign in button.</p>
                      <div class="divSignup">
                          <asp:Button ID="SignUpBtn" runat="server" Text="Sign In" CssClass="signupSubmit h-28" PostBackUrl="http://www.rentpaidonline.com/CurrentResidents.aspx" CausesValidation="False" />
                      </div>
                  </fieldset>
            </div>
            <!--CMallory - Task 00528 - Added links to the mobile apps -->
            <div class="login-br2" style="margin-top:5%; width:89%; border-color: #06cef9; border-radius:6px">
                <h2 style="color: white; font-size: 120%; font-weight: bold; margin-bottom:5%" ><strong>Download the EFX Mobile App</strong></h2>
                <a href='https://play.google.com/store/search?q=rentpaidonline' target="_blank"><img src="../Images/GooglePlayIcon.png" width="46%" height="46%" /></a>
                <a href='https://itunes.apple.com/us/app/rentpaidonline-resident/id878842024?ls=1&mt=8' target="_blank"><img src="../Images/AppStoreIcon.png" width="46%" height="46%" /></a>
            </div>       
    </section>    
    </fieldset>                                    
    </div>      
</div>
        
    </asp:Panel>

    <script>
        var vOrient = 'p';

        // Listen for orientation changes
        window.addEventListener("orientationchange", function () {

             calc_container(vOrient);

        }, false);

        // portrait OR landscape
        function _portrait_landspace() {

            //
            if (window.matchMedia("(orientation: portrait)").matches) {
                vOrient = 'p';
            }

            if (window.matchMedia("(orientation: landscape)").matches) {
                vOrient = 'l';
            }
 
            var vDevice = navigator.userAgent;
            var w_width = $(window).width();
            if (vOrient == 'p') {
                if ($(window).width() >= 350 && $(window).width() < 768) {
                    $('#headerImg').hide();
                    //CMallory - Task 00528 - Changed Max-Height from 420 to 520
                    var vStyle = "border: none; padding: 10px 20px 20px; max-width: 430px; max-height: 520px; text-align: center; position: absolute; margin-top: 0px; background: rgba(3, 18, 51, 0.6); ";
                    $('.loginPage').attr('style', vStyle);
                    if (w_width >= 481 || w_width <= 767) {
                        var vML1 = (w_width - 470) / 2;
                        if (vML1 < 0) { vML1 = 0; }
                        vML1 += "px";
                        //$(".pageContainer.loginPage.login1").css("margin-left", vML1);
                    }
                    $(".mean-bar a.meanmenu-reveal").css("display", "none");
  
                }
                else if ($(window).width() < 400 && vDevice.match(/iPhone/i)) {
                    $('#headerImg').hide();
                    var vStyle = "border: none; padding: 10px 20px 20px; max-width: 430px; max-height: 420px; text-align: center; position: absolute; margin-top: 0px; background: rgba(3, 18, 51, 0.6); ";
                    $('.loginPage').attr('style', vStyle);
                    // border: 1px white solid;border-radius: 3px; 
                    vStyle = "width: 90% !important;  height: 100%; ";
                    //$('#loginFieldset').attr('style', vStyle);
                    //margin: 28px;
                    vStyle = " width: 90% !important; ";
                    $('.section95').attr('style', vStyle);
                }
            }
            if (vOrient == 'l') {
                if ($(window).width() >= 350 && $(window).width() < 768) {
                    $('#headerImg').hide();
                    //CMallory - Task 00528 - Changed Max-Height from 420 to 520
                    var vStyle = "border: none; padding: 10px 20px 20px; max-width: 430px; max-height: 520px; text-align: center; position: absolute; margin-top: 0px; background: rgba(3, 18, 51, 0.6); ";
                    $('.loginPage').attr('style', vStyle);
                }
                else if ($(window).width() < 400 && vDevice.match(/iPhone/i)) {
                    $('#headerImg').hide();
                    var vStyle = "border: none; padding: 10px 20px 20px; max-width: 430px; max-height: 420px; text-align: center; position: absolute; margin-top: 0px; background: rgba(3, 18, 51, 0.6); ";
                    $('.loginPage').attr('style', vStyle);
                    // border: 1px white solid;border-radius: 3px;
                    vStyle = "width: 90% !important;  height: 100%;  ";
                    //$('#loginFieldset').attr('style', vStyle);
                    //margin: 28px;
                    vStyle = " width: 90% !important; ";
                    $('.section95').attr('style', vStyle);

                }
            }

        } // end of p_l....


        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
            }
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);

            _portrait_landspace();

            // move footer to the bottom of page
            $("footer").addClass("footer");
            if ($(window).width() <= 767) {
                $("footer").css("display", "none");
            } else {
                $("footer").css("display", "block");
            }


        } // end of calc....

        $(document).ready(function () {

            /* */
            var w_width = $(window).width();
            var v_login_width = $(".loginPage").width();
            var vObj = document.getElementById("header");
            $(".loginPage").css("position", "absolute");
            if ($(window).width() <= 767) {

            }
            // position login for desktop
            if ($(window).width() >= 1280) {
                vLeft = (w_width - v_login_width) / 2;
                $(".loginPage").css("left", "35%");
            }

            //$('.wrapper').attr('style', 'height: 1500px !important');  
            $("#ContentPlaceHolder1_SignUpBtnMobile").css("display", "none");
            //
            calc_container();


        });



        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);

    </script>

     <!--CMallory - Task 00476 - Added Javascript to work for users using Chrome -->
    <script>
        function ChangeType() {

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE");
            var chrome = navigator.userAgent.search("Chrome");
            var firefox = navigator.userAgent.search("Firefox");
            if (chrome > 0) {
                document.getElementById('InputPasswordTextBox').type = 'text';
            }
        }
    </script>
</asp:Content>
