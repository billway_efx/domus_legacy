﻿using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using System;
using System.Globalization;
using System.Web.Security;
using System.Web.UI;
using RPO;

namespace Efx.EfxResidents.Account
{
    public partial class Login : BasePageV2, ILoginV2
    {
        public string UsernameText { get { return InputUserNameTextBox.Text; } }
        public string PasswordText { get { return InputPasswordTextBox.Text; } }
        public int LoggedInUserId { set { Redirect(value); } }
        public EventHandler LoginButtonClick { set { LoginButton.Click += value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            InputUserNameTextBox.Focus();
            var Presenter = new LoginPresenter<EfxFramework.Renter>(this);

            //Salcedo - 1/15/2015 - task # 00349 - make link dynamic
            SignUpBtn.PostBackUrl = EfxFramework.Settings.PublicSiteUrl + "/CurrentResidents.aspx";
            SignUpBtnMobile.PostBackUrl = EfxFramework.Settings.PublicSiteUrl + "/CurrentResidents.aspx";
        }

        protected void Redirect(int userId)
        {
            FormsAuthentication.RedirectFromLoginPage(userId.ToString(CultureInfo.InvariantCulture), true);

            //cakel: BUGID000133
            string _user = userId.ToString();

            RPO.ActivityLog AL = new RPO.ActivityLog();
            AL.WriteLog(_user, "Renter Logging in", (short)LogPriority.LogAlways);


        }

        protected void ForgotPasswordLinkButtonClick(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "test", "<script type='text/javascript'>ShowModal(\"#ForgotPassword\");</script>", false);
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {



        }


    }
}