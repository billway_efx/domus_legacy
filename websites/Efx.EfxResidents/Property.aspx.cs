﻿using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Web;

namespace Efx.EfxResidents
{
    public partial class Property : BasePageV2, IProperty
    {
        private PropertyPresenter _Presenter;

        public int UserId
        {
            get { return !string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? HttpContext.Current.User.Identity.Name.ToInt32() : 0; }
        }

        public IPropertyContacts PropertyContactsControl
        {
            get { return PropertyContactsTab; }
        }

        public IPropertyMaintenanceRequest PropertyMaintenanceRequestControl
        {
            get { return PropertyMaintenanceRequestTab; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            AddValidationSummaryToPage();
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0 && !String.IsNullOrEmpty(Request.QueryString["Tab"]) && Request.QueryString["Tab"].ToInt32() == 3)
            {
                const string myScript = "$(document).ready(function() {changePanels(2);});";
                Page.ClientScript.RegisterStartupScript(GetType(), "myKey", myScript, true);
            }

            _Presenter = new PropertyPresenter(this);
        }
    }

}