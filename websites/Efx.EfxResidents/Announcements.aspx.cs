﻿using System.Web.UI.WebControls;
using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using System;
using System.Collections.Generic;

namespace Efx.EfxResidents
{
    public partial class Announcements : BasePageV2, IAnnouncements
    {
        private AnnouncementsPresenter _Presenter;
        public List<Announcement> PropertyAnnouncementsListView { set { PropertyAnnouncements.DataSource = value; PropertyAnnouncements.DataBind(); } }
        public List<Announcement> RpoAnnouncementsListView { set { RPOAnnouncements.DataSource = value; RPOAnnouncements.DataBind(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new AnnouncementsPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}