﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="calendar.aspx.cs" Inherits="Efx.EfxResidents.calendar" %>

<asp:Content runat="server" ContentPlaceHolderID="head">
    <!-- <script src="//code.jquery.com/jquery.js"></script> -->
    <script src="<%= ResolveClientUrl("~/Scripts/html5shiv.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/respond.min.js") %>"></script>
    <link href="<%= ResolveClientUrl("~/Static/css/main.css")%>" rel="stylesheet" />
	<link href="<%= ResolveClientUrl("~/Static/css/fullcalendar.css")%>" rel="stylesheet" />
	<link href="<%= ResolveClientUrl("~/Static/css/fullcalendar.print.css")%>" media="print" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

	<div class="clear"></div>

    <div id="pageCenterContainer" class="homePageContainer calendarPage residentsPage" style="width: 98%; margin-left: 1%; margin-right: 1%;"  >
		<div id="calendar" class="main-container" ng-controller="CalendarController">
			<div ng-include="'/event-calendar.html'"></div>
			<div class="modal fade" ng-include="'/event-details.html'" id="eventDetails"></div>
		</div>
	</div>
	<div class="clear"></div>


    <script>

        var vOrient = 'p';
        // Listen for orientation changes
        window.addEventListener("orientationchange", function () {

            calc_container();

        }, false);

        // portrait OR landscape
        function _portrait_landspace() {

            //
            if (window.matchMedia("(orientation: portrait)").matches) {
                vOrient = 'p';
            }

            if (window.matchMedia("(orientation: landscape)").matches) {
                vOrient = 'l';
            }
            //

            var vDevice = navigator.userAgent;
            var w_width = $(window).width();
            if (vOrient == 'p') {
                if ($(window).width() < 768) {
                    $('#headerImg').hide();
                }
            }
            if (vOrient == 'l') {
                if ($(window).width() < 768) {
                    $('#headerImg').hide();
                }
            }

        } // end of p_l....


        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
                //$("#pageCenterContainer").css("margin-left", vML);
                //$("#pageCenterContainer").css("margin-right", vML);
            }
            else if ($(window).width() >= 768 && $(window).width() < 1024) {
                var vML = (w_width - 768) / 2;
                //$('#pageCenterContainer').attr('id', 'temp');
            }
            //$("footer").css("position", "absolute");
            //$("footer").css("top", "1500px");
            if ($(window).width() <= 767) {
                $("footer").css("display", "none");
            } else {
                var vHeight = "height: " + ($(".pageContainer.calendarPage.residentsPage").height() + 200) + "px !important";
                //$('.wrapper').attr('style', vHeight);
                vHeight = "position: absolute; top: " + ($(".pageContainer.calendarPage.residentsPage").height() + 500) + "px";
                //$('footer').attr('style', vHeight);
            }

        } // end of calc....

        $(document).ready(function () {

            /* */
            var vObj = document.getElementById("header");
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "180px");
            //$('.wrapper').attr('style', 'height: 1500px !important');

            calc_container();

            $(window).bind("orientationchange", function () {
                setTimeout(function () {
                    window.panelWidth = $('.mainTabSection').width();
                    $('.panel_container .panel').each(function (index) {
                        $(this).css({ 'width': window.panelWidth + 'px', 'left': (index * window.panelWidth) + 'px' });
                        $('.mainTabSection .panels').css('width', (index + 1) * window.panelWidth + 'px');
                        changePanels($(".mainTabSection .tabs .selected").index());
                    });
                }, 500);
            });

        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);

    </script>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="PageSpecificJS">
    <script src="<%= ResolveClientUrl("~/Scripts/bootstrap.min.js") %>"></script>
	<script src="<%= ResolveClientUrl("~/Scripts/jquery.design.js") %>"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.0-rc.2/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.0-rc.2/angular-route.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.0-rc.2/angular-sanitize.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.0-rc.2/angular-animate.min.js"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/angular-locale_en-us.js") %>"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular-strap/0.7.4/angular-strap.min.js"></script>
    
    <script src="<%= ResolveClientUrl("~/Scripts/underscore-min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/fullcalendar.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/calendar.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/Scripts/rpo-calendar.js") %>"></script>
</asp:Content>