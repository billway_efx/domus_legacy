﻿<%@ Page Title="RPO Error" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="Efx.EfxResidents.ErrorPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">   

    <%-- cakel: TASKID00430 --%>
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <div id="pageCenterContainer" class="pageContainer faqContainer residentsPage">
        <div class="pageTitle frPageTitle fr1Color" style="border-color: white;" ><h2>Error!</h2></div>
       
            <div class="standardPadding">
             <%--   <h4>Error</h4>--%>
                <h3 class="frBlack">An unexpected Error has occurred while processing your request.</h3>
                <h3 class="frBlack">This error has been recorded and will be corrected as soon as possible.</h3>
                <h3 class="frBlack">Please try again later.</h3>
               <asp:Button ID="GoBackToHome" runat="server" Text="Go to Home Page" CssClass="orangeButton okayButton fr2BColor" OnClick="GoBackToHome_Click" />
            </div>            

    </div> 

       <script>

        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
            }
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);
        } // end of calc....


        $(document).ready(function () {
            /* */
            var vObj = document.getElementById("header");
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "100px");

            var vHeight = "height: " + ($("#pageCenterContainer").height() + 200) + "px !important";
            //$('.wrapper').attr('style', vHeight);
            vHeight = "position: absolute; top: " + ($("#pageCenterContainer").height() + 200) + "px";
            //$('#footer').attr('style', vHeight); 

            calc_container();
        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);


        // set menu to go back to default page
        $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

            evt.preventDefault();

            var vHash = $(this).attr('href');
            window.location.href = "/Default.aspx" + vHash;

        });
    </script>

    
</asp:Content>
