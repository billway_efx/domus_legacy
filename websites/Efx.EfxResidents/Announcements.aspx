﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Announcements.aspx.cs" Inherits="Efx.EfxResidents.Announcements" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden   -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>
 
    <div id="pageCenterContainer" class="pageContainer residentsPage" > 
        <div class="pageTitle" style="border-width: 0;" ><h2  style="color: #0462c2 !important; ">Announcements</h2></div>
        <!--Main Tabs Section -->
        <div class="mainTabSection fr2BorderColor">
            <div class="tabs fr2BorderColor">
                <span class="fr2Color" style="border: 1px solid #06cef9;">RentPaidOnline</span>
                <span class="fr2Color" style="border: 1px solid #06cef9;">Property</span>
            </div>
            <!--"panel_container" is the main container for all the panels on this page, a "panel" is each man tabbing section -->
            <div class="panel_container fr2BorderColor">
                <!--this is the start of the multiple panels -->
                <div class="panels">
                    <!--this is the first panel (RPO Announcements) -->
                    <div class="panel">
                        <div class="panel_content">
                            <div class="standardPadding">
                                    <asp:ListView ID="RPOAnnouncements" runat="server">
                                    <LayoutTemplate>
                                            <span id="itemPlaceHolder" runat="server"></span>
                                    </LayoutTemplate>
                                        <ItemTemplate>
                                            <div class="announcementsTitle">
                                                <h3><asp:Label ID="AnnouncementTitleLabel" CssClass="fr1Color" runat="server" Text='<%# Bind("AnnouncementTitle") %>' /></h3>
                                            </div>
                                            <div class="announcementsDate">
                                                <span>Posted Date: <asp:Label ID="AnnouncementDate" CssClass="fr2Color" Text='<%# Bind("AnnouncementDate" , "{0:MMM dd, yyyy}") %>' runat="server"></asp:Label></span>
                                            </div>
                                            <div class="announcementsBody">
                                                <p><asp:Label ID="AnnouncementText" CssClass="frBlack" Text='<%# Bind("AnnouncementText") %>' runat="server"/></p>
                                                <asp:HyperLink ID="AnnouncementLink" CssClass="frBlack" Text='<%# Bind("AnnouncementUrl") %>' NavigateUrl='<%# Bind("AnnouncementUrl") %>' runat="server" /> 
                                            </div>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                            <p class="nothingToShow frBlack">There are no RPO Administrative Announcements to Show at this time.</p>
                                        </EmptyDataTemplate>
                                   </asp:ListView>
                            </div>
                        </div>
                    </div>
                    <!--this is the Second panel (Panels) -->
                     <div class="panel">
                        <div class="panel_content">
                            <div class="standardPadding">
                                    <asp:ListView ID="PropertyAnnouncements" runat="server">
                                    <LayoutTemplate>
                                           <span id="itemPlaceHolder" runat="server"></span>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <div class="announcementsTitle">
                                            <h3><asp:Label ID="AnnouncementTitleLabel" CssClass="fr1Color" runat="server" Text='<%# Bind("AnnouncementTitle") %>' /></h3>
                                        </div>
                                        <div class="announcementsDate">
                                            <span>Posted Date: <asp:Label ID="AnnouncementDate" CssClass="fr2Color" Text='<%# Bind("AnnouncementDate" , "{0:MMM dd, yyyy}") %>' runat="server"></asp:Label></span>
                                        </div>
                                        <div class="announcementsBody">
                                            <p><asp:Label ID="AnnouncementText" CssClass="frBlack" Text='<%# Bind("AnnouncementText") %>' runat="server"/></p>
                                            <asp:HyperLink ID="AnnouncementLink" CssClass="frBlack" Text='<%# Bind("AnnouncementUrl") %>' NavigateUrl='<%# Bind("AnnouncementUrl") %>' runat="server" /> 
                                        </div>
                                    </ItemTemplate>
                                    <EmptyDataTemplate>
                                        <p class="nothingToShow frBlack">There are no Property Announcements to Show at this time.</p>
                                    </EmptyDataTemplate>
                                </asp:ListView>
                          </div>
                        </div>
                    </div>                               
                </div>
            </div>
        </div>
    </div>


    <script>

        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
                $("#pageCenterContainer").css("margin-left", vML);
                $("#pageCenterContainer").css("margin-right", vML);
            }
            else if ($(window).width() >= 768 && $(window).width() < 1024) {
                var vML = (w_width - 768) / 2;
                $('#pageCenterContainer').attr('id', 'temp');
            }

        } // end of calc....


        $(document).ready(function () {

            /* */
            var vObj = document.getElementById("header");
            //vObj.style.top = "-40px";
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "180px");
            //$('.wrapper').attr('style', 'height: 1500px !important');

            calc_container();

            //$("footer").css("position", "absolute");
            //$("footer").css("top", "1500px");

        });

        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);

    </script>
</asp:Content>
