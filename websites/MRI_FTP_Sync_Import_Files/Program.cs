﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using System.Net;
using EnterpriseDT.Net.Ftp;
using EfxFramework;
using RPO;

namespace MRI_FTP_Sync_Import_Files
{
    class Program
    {
        static void Main(string[] args)
        {
            SyncUploadedFiles();
        }

        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

        private static void SyncUploadedFiles()
        {
            ActivityLog AL = new ActivityLog();
            String TheSql = "";

            SqlConnection con = new SqlConnection(ConnString);

            // Get list of properties to work with ...
            // Should probably move this Sql to a stored procedure, to make maintenance easier. But hopefully this sql code will never need to be changed. Hope springs eternal.
            TheSql =
                "SELECT CompanyId, MRIFtpSiteAddress, MRIFtpPort, MRIFTPAccountUserName, MRIFTPAccountPassword, a.PropertyId, b.PropertyStaffId " +
                "FROM Property a " +
                    "JOIN PropertyPropertyStaff b on a.PropertyId = b.PropertyId " +
                "WHERE MRIEnableFtpUpload = 1 " +
                    "AND ISNULL(MRIFTPAccountUserName,'') <> '' " +
                    "AND ISNULL(MRIFTPAccountPassword,'') <> '' " +
                    "AND ISNULL(MRIFtpSiteAddress,'') <> '' " +
                    "AND ISNULL(MRIFTPPort,'') <> '' " +
                    "AND ISNULL(IsDeleted,0) = 0 " +
                    "AND ISNULL(PMSTypeId,0) = 4 " +
                    "AND ISNULL(PmsId,'') <> '' " +
                    "AND b.PropertyStaffId in (SELECT TOP 1 c.PropertyStaffId " +
                        "FROM PropertyPropertyStaff c, propertystaffrole d " +
                        "WHERE a.propertyid = c.propertyid AND c.PropertyStaffId = d.PropertyStaffId AND  " +
                            "b.PropertyStaffId = c.PropertyStaffid AND d.RoleId = 3) ";   

            SqlCommand com = new SqlCommand(TheSql, con);
            com.CommandType = CommandType.Text;
            con.Open();
            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(com);
            sda.Fill(dt);
            con.Close();

            //For each property in the list ...
            foreach (DataRow dr in dt.Rows)
            {
                int CompanyId = (int)dr[0];
                String MRIFtpSiteAddress = dr[1].ToString();
                String MRIFtpPort = dr[2].ToString();
                String MRIFTPAccountUserName = dr[3].ToString();
                String MRIFTPAccountPassword = dr[4].ToString();
                int PropertyId = (int)dr[5];
                int StaffId = (int)dr[6];

                Console.WriteLine("Searching PropertyId " + PropertyId.ToString() + " for new files ...");
                if (MRIFtpSiteAddress.Length <= 6)
                    continue;

                FTPConnection MyFtpConnection = new FTPConnection();
                MyFtpConnection.ServerAddress = MRIFtpSiteAddress;
                MyFtpConnection.UserName = MRIFTPAccountUserName;
                MyFtpConnection.Password = MRIFTPAccountPassword;
                MyFtpConnection.TransferType = FTPTransferType.BINARY;
                MyFtpConnection.Connect();

                EnterpriseDT.Net.Ftp.FTPFile[] FilesInDirectory = MyFtpConnection.GetFileInfos();

                foreach (EnterpriseDT.Net.Ftp.FTPFile TheFile in FilesInDirectory)
                {
                    if (!TheFile.Dir)
                    {
                        Console.WriteLine("Copying " + TheFile.Name);
                        AL.WriteLog("System", "PropertyId " + PropertyId.ToString() + ": Copying '" + TheFile.Name + "' from Ftp site to database", (short)LogPriority.LogAlways);

                        // Copy the file into memory, and then insert it into the MRIImportFiles table
                        long FileSize = MyFtpConnection.GetSize(TheFile.Name);
                        DateTime FileDate = MyFtpConnection.GetLastWriteTime(TheFile.Name);
                        Byte[] FileBytes = MyFtpConnection.DownloadByteArray(TheFile.Name);
                        int FileId = UploadMRIImportFile(TheFile.Name, FileDate, StaffId, CompanyId, FileBytes, FileSize);

                        AL.WriteLog("System", "File copy complete for '" + TheFile.Name + "'. New FileId: " + FileId.ToString(), (short)LogPriority.LogAlways);

                        // Note: When we insert the new MRI file into the MRIImportFiles table, the database fires the insert trigger 'MRIImportFiles_Insert', 
                        // which runs the job 'Run_MRI_Import_File_Parser', which in turn parses the file and sends out notification email as necessary.
                        // So, there's no need to do anything else here, as far as processing is concerned, except to get the file into the database, and
                        // of course mark it as "archived" so we don't process it again, the next time this program runs.

                        // Make sure we have an "archived" folder - create one if necessary
                        String ArchiveDirectory = "archived";
                        try
                        {
                            MyFtpConnection.ChangeWorkingDirectory(ArchiveDirectory);
                            MyFtpConnection.ChangeWorkingDirectoryUp();
                        }
                        catch
                        {
                            // If the directory isn't there, we'll get an exception, and fall into this code, so we'll attempt to create the directory

                            AL.WriteLog("System", "PropertyId " + PropertyId.ToString() + ": " + ArchiveDirectory + "' folder was not found. Attempting creation.", (short)LogPriority.LogAlways);
                            MyFtpConnection.CreateDirectory(ArchiveDirectory);
                            AL.WriteLog("System", "PropertyId " + PropertyId.ToString() + ": " + ArchiveDirectory + "' folder was created.", (short)LogPriority.LogAlways);
                        }

                        // Rename the file to "_processed_YYYY_MM_DD_HH_MM_SS" and move to the "archive" folder
                        // We include the the date/tiem on the rename of the file so that our users can upload a file with the same original name as many times as they like
                        String NewFileName = ArchiveDirectory + "/" + TheFile.Name + "_processed_" +
                            DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() +
                            DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString();

                        AL.WriteLog("System", "PropertyId " + PropertyId.ToString() + ": Moving/renaming '" + TheFile.Name + "' to '" + NewFileName + "'.", (short)LogPriority.LogAlways);
                        MyFtpConnection.RenameFile(TheFile.Name, NewFileName);
                        AL.WriteLog("System", "PropertyId " + PropertyId.ToString() + ": Move/rename '" + TheFile.Name + "' to '" + NewFileName + "' operation was successful.", (short)LogPriority.LogAlways);
                    }
                }

                MyFtpConnection.Close();
            }
            

        }

        private static int UploadMRIImportFile(string FileName, DateTime DateUploaded, int UploadedByUserId, int CompanyId, byte[] FileData, long FileSize)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlParameter ParamFileName = null;
            SqlParameter ParamDateUploaded = null;
            SqlParameter ParamUploadedByUserId = null;
            SqlParameter ParamCompanyId = null;
            SqlParameter ParamFileData = null;
            SqlParameter ParamFileSize = null;
            SqlParameter ParamFileID = null;

            int NewId = 0;

            try
            {
                conn = new SqlConnection(EfxSettings.ConnectionString);
                cmd = new SqlCommand("UploadMRIImportFile", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                ParamFileName = new SqlParameter("@FileName", SqlDbType.VarChar, 250);
                ParamFileName.Value = FileName;

                ParamDateUploaded = new SqlParameter("@DateUploaded", SqlDbType.DateTime);
                ParamDateUploaded.Value = DateUploaded;

                ParamUploadedByUserId = new SqlParameter("@UploadedByUserId", SqlDbType.VarChar, 250);
                ParamUploadedByUserId.Value = UploadedByUserId;

                ParamCompanyId = new SqlParameter("@CompanyId", SqlDbType.VarChar, 250);
                ParamCompanyId.Value = CompanyId;

                ParamFileData = new SqlParameter("@FileData", SqlDbType.Image);
                ParamFileData.Value = FileData;

                ParamFileSize = new SqlParameter("@FileSize", SqlDbType.BigInt);
                ParamFileSize.Value = FileSize;

                ParamFileID = new SqlParameter("@FileID", SqlDbType.BigInt);
                ParamFileID.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(ParamFileName);
                cmd.Parameters.Add(ParamDateUploaded);
                cmd.Parameters.Add(ParamUploadedByUserId);
                cmd.Parameters.Add(ParamCompanyId);
                cmd.Parameters.Add(ParamFileData);
                cmd.Parameters.Add(ParamFileSize);
                cmd.Parameters.Add(ParamFileID);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                NewId = Convert.ToInt32(ParamFileID.Value);

                conn.Dispose();
                cmd.Dispose();

            }
            //catch (Exception e)
            catch
            {
                NewId = 0;
            }

            return NewId;
        }

    }
}
