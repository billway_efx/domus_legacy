Imports System.Data.SqlClient
Imports System.Xml
Imports System.Configuration
Imports System.Configuration.ConfigurationSettings
Imports System.Diagnostics
Imports System.IO
Imports Microsoft.ApplicationBlocks.ExceptionManagement

Public Class DBManager

    'private variables
    Private m_strAppType As String
    'Private m_strServer As String
    'Private m_strDatabase As String
    'Private m_strUser As String
    'Private m_strPassword As String
    Private m_objDBObject As Object
    Private sqlCn As SqlConnection

    'private structures
    Public Structure DATABASES
        Dim DATABASES As Integer
        Const ACHACQUIRE As String = "ACH_ACQUIRE"
    End Structure

#Region "Properties"

    Public Property AppType() As String
        Get
            Return m_strAppType
        End Get
        Set(ByVal appTypeValue As String)
            m_strAppType = appTypeValue
            'SetDatabase()
        End Set
    End Property

    'Public ReadOnly Property Server() As String
    '    Get
    '        Return m_strServer
    '    End Get
    'End Property

    'Public ReadOnly Property Database() As String
    '    Get
    '        Return m_strDatabase
    '    End Get
    'End Property

    'Public ReadOnly Property User() As String
    '    Get
    '        Return m_strUser
    '    End Get
    'End Property

    'Public ReadOnly Property Password() As String
    '    Get
    '        Return m_strPassword
    '    End Get
    'End Property


#End Region

    Public Sub New(Optional ByVal strAppType As String = DATABASES.ACHACQUIRE)
        'initialize the member attributes

        Me.AppType = strAppType
    End Sub

    Public Function GetConnection() As SqlConnection

        Dim oConn As SqlConnection

        'Dim sConnect As String
        'Dim sDBName As String
        'Dim sFileName As String

        Try

            'sConnect = "Initial Catalog=" & m_strDatabase & _
            '                  ";Data Source=" & m_strServer & _
            '                        ";User ID=" & m_strUser & _
            '                                ";Password=" & m_strPassword

            Dim sConnectionString As String = _
                ConfigurationManager.ConnectionStrings("ACHAcquireConnectionString").ToString()

            'oConn = New SqlConnection(sConnect)
            oConn = New SqlConnection(sConnectionString)

            oConn.Open()

            Return oConn

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

    End Function


    ' *********************************************************************************
    ' Name:         GetDBAccessor
    ' Purpose:      
    ' Inputs:
    ' Returns:      
    '**********************************************************************************
    Private Function GetDBAccessor(ByVal strSqlValue As String, _
                                        ByVal strExecutionMethodValue As String, _
                                        Optional ByVal TheParms As SqlParameterCollection = Nothing) As Object

        Dim strCn As String = ""
        Dim sqlDa As SqlDataAdapter
        Dim sqlCmd As SqlCommand

        ' SQL Server authentication string
        'strCn = "Initial Catalog=" & m_strDatabase & _
        '                  ";Data Source=" & m_strServer & _
        '                        ";User ID=" & m_strUser & _
        '                                ";Password=" & m_strPassword

        Try
            strCn = ""
            strCn = ConfigurationManager.ConnectionStrings("ACHAcquireConnectionString").ToString()

        Catch ex As Exception

        End Try


        Try
            ' Decide which DB object to instantiate and return to caller
            If Not (Left(UCase(strExecutionMethodValue), 4) = "FILL") Then
                ' Instantiate the connection object
                If strCn = "" Or strCn Is Nothing Then
                    ' This allows us to use the connection already established through SQL Server
                    ' but only when we're running an Assembly that uses this module, and this
                    ' Assembly has been installed within SQL Server. Need to add better exception 
                    ' handling above. MKS - 1/12/2010
                    sqlCn = New SqlConnection("context connection=true")
                Else
                    sqlCn = New SqlConnection(strCn)
                End If
                ' Open the connection
                If sqlCn.State <> ConnectionState.Open Then
                    sqlCn.Open()
                End If
                ' Intantiate the command object
                'sqlCmd = New SqlCommand(strSqlValue, sqlCn)
                sqlCmd = New SqlCommand
                sqlCmd.Connection = sqlCn
                sqlCmd.CommandText = strSqlValue
                'Salcedo - 7/10/03 - I pushed the timeout to 60 minutes to give us a chance to see
                'what's causing the delay, if any, when and if it occurs. For instance, there might
                'be blocking issues that would be hard to detect if the timeout is only set to 3 minutes.
                sqlCmd.CommandTimeout = 3600
                sqlCmd.CommandType = CommandType.Text

                If Not TheParms Is Nothing Then
                    Dim TempParm As SqlParameter
                    For Each TempParm In TheParms
                        sqlCmd.Parameters.Add(TempParm)
                    Next
                End If

                ' Return the command object
                GetDBAccessor = sqlCmd
            Else
                ' Instantiate the DataAdapter
                If strCn = "" Or strCn Is Nothing Then
                    ' This allows us to use the connection already established through SQL Server
                    ' but only when we're running an Assembly that uses this module, and this
                    ' Assembly has been installed within SQL Server. Need to add better exception 
                    ' handling above. MKS - 1/12/2010
                    sqlCn = New SqlConnection("context connection=true")
                    sqlDa = New SqlClient.SqlDataAdapter(strSqlValue, sqlCn)
                Else
                    sqlDa = New SqlClient.SqlDataAdapter(strSqlValue, strCn)
                End If

                If Not TheParms Is Nothing Then
                    Dim TempParm As SqlParameter
                    For Each TempParm In TheParms
                        sqlDa.SelectCommand.Parameters.Add(TempParm)
                    Next
                End If

                sqlDa.SelectCommand.CommandTimeout = 3600
                ' Return the data adapter object
                GetDBAccessor = sqlDa
            End If

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            ExceptionManager.Publish(New Exception("GetDBAccessor->strCn->" & strCn))
            Throw (ex)
        End Try

    End Function


    ' *********************************************************************************
    ' Name:         TransactSql
    ' Purpose:      
    ' Inputs:
    ' Returns:      Object
    '**********************************************************************************
    Public Function TransactSql(ByVal strSql As String, _
                                ByVal strExecutionMethod As String, _
                                Optional ByVal dsDBDataSet As Object = Nothing, _
                                    Optional ByVal sTableName As String = vbNullString) As Object

        Dim blnRetry As Boolean = True
        Dim intRetryCount As Int16 = 0

        TransactSql = Nothing

        While blnRetry

            Try
                'execute the appropriate command method
                Select Case UCase(strExecutionMethod)
                    Case "INSERT"
                        TransactSql = InsertIntoDB(strSql)
                    Case "UPDATE"
                        TransactSql = UpdateDB(strSql)
                    Case "DELETE"
                        TransactSql = DeleteFromDB(strSql)
                    Case Else
                        TransactSql = SelectFromDB(strSql, strExecutionMethod, dsDBDataSet, sTableName)
                End Select

                blnRetry = False

            Catch sqlEx As SqlClient.SqlException
                ' To prevent sql error 1205, which is deadlock error
                If sqlEx.Number <> 1205 Then
                    blnRetry = False
                    'ExceptionManager.Publish(sqlEx)
                    Throw (sqlEx)
                Else
                    intRetryCount += 1
                    ' Reflect on the errors of our ways for 30 seconds ...
                    System.Threading.Thread.Sleep(30000)
                End If
                If intRetryCount > 10 Then
                    blnRetry = False
                    Throw sqlEx
                    'ExceptionManager.Publish(sqlEx)
                    Throw (sqlEx)
                End If
            Catch ex As Exception
                blnRetry = False
                'Debug.WriteLine(ex.Message)
                'ExceptionManager.Publish(ex)
                Throw (ex)

            Finally
                'blnRetry = False

                ' Release objects
                If Not (m_objDBObject Is Nothing) Then
                    m_objDBObject.Dispose()
                    m_objDBObject = Nothing
                End If

                ' Close the connection
                If Not (sqlCn Is Nothing) Then
                    If sqlCn.State <> ConnectionState.Closed Then
                        sqlCn.Close()
                    End If
                End If

            End Try
        End While
    End Function

    Public Function TransactProcedure(ByVal strSql As String, _
                                ByVal strExecutionMethod As String, ByVal TheParms As SqlParameterCollection, _
                                Optional ByVal dsDBDataSet As Object = Nothing, _
                                    Optional ByVal sTableName As String = vbNullString) As Object

        Dim blnRetry As Boolean = True
        Dim intRetryCount As Int16 = 0

        TransactProcedure = Nothing

        While blnRetry

            Try
                TransactProcedure = SelectFromDB(strSql, strExecutionMethod, dsDBDataSet, sTableName, TheParms)

                blnRetry = False

            Catch sqlEx As SqlClient.SqlException
                ' To prevent sql error 1205, which is deadlock error
                If sqlEx.Number <> 1205 Then
                    blnRetry = False
                    ExceptionManager.Publish(sqlEx)
                    Throw (sqlEx)
                Else
                    intRetryCount += 1
                    ' Reflect on the errors of our ways for 30 seconds ...
                    System.Threading.Thread.Sleep(30000)
                End If
                If intRetryCount > 10 Then
                    blnRetry = False
                    ExceptionManager.Publish(sqlEx)
                    Throw (sqlEx)
                End If
            Catch ex As Exception
                blnRetry = False
                'Debug.WriteLine(ex.Message)
                ExceptionManager.Publish(ex)
                Throw (ex)

            Finally
                'blnRetry = False

                ' Release objects
                If Not (m_objDBObject Is Nothing) Then
                    m_objDBObject.Dispose()
                    m_objDBObject = Nothing
                End If

                ' Close the connection
                If Not (sqlCn Is Nothing) Then
                    If sqlCn.State <> ConnectionState.Closed Then
                        sqlCn.Close()
                    End If
                End If

            End Try
        End While
    End Function

    ' *********************************************************************************
    ' Name:         SelectFromDB
    ' Purpose:      
    ' Inputs:
    ' Returns:      Object
    '**********************************************************************************
    Private Function SelectFromDB(ByVal strSql As String, _
                                 ByVal strExecutionMethod As String, _
                                 ByVal dsDBDataSetValue As DataSet, _
                                    Optional ByVal sTableName As String = vbNullString, _
                                    Optional ByVal TheParms As SqlParameterCollection = Nothing) As Object

        Dim dsInfo As DataSet
        Dim dtNewDataTable As DataTable
        Dim sqlReader As SqlDataReader
        Dim xmlReader As XmlReader

        Try
            ' Retrieve the necessary DB object to perform the execution method
            m_objDBObject = GetDBAccessor(strSql, strExecutionMethod, TheParms)

            ' Execute the appropriate command method
            Select Case strExecutionMethod
                Case "EXECUTESCALAR"
                    ' Returns single value (first column of first row)
                    SelectFromDB = m_objDBObject.ExecuteScalar()
                Case "EXECUTEREADER"
                    ' Returns a sqlDataReader with no command behavior specified
                    sqlReader = m_objDBObject.ExecuteReader()
                    SelectFromDB = sqlReader
                Case "EXECUTEXMLREADER"
                    ' Returns an xmlReader
                    xmlReader = m_objDBObject.ExecuteXmlReader
                    SelectFromDB = xmlReader
                Case "FILL_DATATABLE"
                    ' Returns a datatable
                    dtNewDataTable = New DataTable(sTableName)
                    m_objDBObject.Fill(dtNewDataTable)
                    SelectFromDB = dtNewDataTable
                Case Else
                    If Not (dsDBDataSetValue Is Nothing) Then
                        ' Instantiate a new datatable
                        dtNewDataTable = New DataTable

                        ' Add table  to existing dataset
                        dsDBDataSetValue.Tables.Add(dtNewDataTable)

                        ' Create a generic table in the dataset, and
                        ' fill it with the query results
                        m_objDBObject.Fill(dsDBDataSetValue.Tables(dsDBDataSetValue.Tables.Count - 1))

                        ' Return info
                        SelectFromDB = dsDBDataSetValue
                    Else
                        ' Instantiate a DataSet with the default "NewDataSet" name
                        dsInfo = New DataSet

                        ' Create a generic table in the dataset, and
                        ' fill it with the query results
                        m_objDBObject.Fill(dsInfo)

                        ' Return info
                        SelectFromDB = dsInfo
                    End If
            End Select

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try
    End Function


    ' *********************************************************************************
    ' Name:         InsertIntoDB
    ' Purpose:      
    ' Inputs:
    ' Returns:      Object
    '**********************************************************************************
    Private Function InsertIntoDB(ByVal strSql As String) As Object

        Try
            ' Retrieve the necessary DB object to perform the execution method
            m_objDBObject = GetDBAccessor(strSql, "EXECUTENONQUERY")

            ' Returns Integer, use with UPDATE, INSERT, or DELETE stmts
            InsertIntoDB = m_objDBObject.ExecuteNonQuery()

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try
    End Function


    ' *********************************************************************************
    ' Name:         UpdateDB
    ' Purpose:      
    ' Inputs:
    ' Returns:      Object
    '**********************************************************************************
    Private Function UpdateDB(ByVal strSql As String) As Object

        Try
            ' Retrieve the necessary DB object to perform the execution method
            m_objDBObject = GetDBAccessor(strSql, "EXECUTENONQUERY")

            ' Returns integer, use with UPDATE, INSERT, or DELETE stmts
            UpdateDB = m_objDBObject.ExecuteNonQuery()

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

    End Function


    ' *********************************************************************************
    ' Name:         DeleteFromDB
    ' Purpose:      
    ' Inputs:
    ' Returns:      Object
    '**********************************************************************************
    Private Function DeleteFromDB(ByVal strSql As String) As Object

        Try
            ' Retrieve the necessary DB object to perform the execution method
            m_objDBObject = GetDBAccessor(strSql, "EXECUTENONQUERY")

            ' Returns integer, use with UPDATE, INSERT, or DELETE stmts
            DeleteFromDB = m_objDBObject.ExecuteNonQuery()

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

    End Function


    ' *********************************************************************************
    ' Name:         InsertAndGetIdentity
    ' Purpose:      
    ' Inputs:
    ' Returns:      Object
    '**********************************************************************************
    Public Function InsertAndGetIdentity(ByVal strInsertSql As String) As Integer
        Dim connSQL As SqlConnection
        connSQL = New SqlConnection

        Dim sqlDa As SqlDataAdapter
        Dim sqlCmd As SqlCommand
        sqlCmd = New SqlCommand
        Dim result As Integer

        Try


            Dim strCn As String = ConfigurationManager.ConnectionStrings("ACHAcquireConnectionString").ToString()
            connSQL.ConnectionString = strCn

            'create sql command object, using the insert sql
            sqlCmd = New SqlCommand(strInsertSql, connSQL)

            'open connection
            sqlCmd.Connection.Open()

            'if insert executes, then continue otherwise, return 0
            If sqlCmd.ExecuteNonQuery() = 0 Then
                InsertAndGetIdentity = 0
                Exit Try
            End If

            'create data adapter for select
            sqlDa = New SqlClient.SqlDataAdapter("SELECT @@IDENTITY", connSQL)

            'execute the select command to retrieve the identity
            result = sqlDa.SelectCommand.ExecuteScalar

            'close the connection to the db
            sqlCmd.Connection.Close()

            'return the identity value
            InsertAndGetIdentity = result

        Catch ex As Exception
            ExceptionManager.Publish(ex)
            Throw (ex)
        End Try

    End Function

End Class
