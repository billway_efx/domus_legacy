Imports System.Security.Cryptography
Imports System.IO
Imports System.Text

Public Class Encryption

    Public Function Encrypt(ByVal strText As String, ByVal strKey As String) As String

        Dim byKey() As Byte = {}
        Dim IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
        Dim strEncrKey As String

        Try
            strEncrKey = strKey
            byKey = System.Text.Encoding.UTF8.GetBytes(Microsoft.VisualBasic.Left(strEncrKey, 8))

            Dim des As New DESCryptoServiceProvider
            Dim inputByteArray() As Byte = Encoding.UTF8.GetBytes(strText)
            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write)

            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()

            Return Convert.ToBase64String(ms.ToArray())

        Catch ex As Exception
            Throw
        End Try

    End Function


    ' *********************************************************************************
    ' Name:         Decrypt
    ' Purpose:      
    ' Inputs:
    ' Returns:      
    '**********************************************************************************

    Public Function Decrypt(ByVal strText As String) As String

        Dim byKey() As Byte = {}
        Dim IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
        Dim inputByteArray(strText.Length) As Byte
        Dim sDecrKey As String

        Try

            'bury the key here
            sDecrKey = "[!#@%&0]"

            byKey = System.Text.Encoding.UTF8.GetBytes(Microsoft.VisualBasic.Left(sDecrKey, 8))

            Dim des As New DESCryptoServiceProvider

            inputByteArray = Convert.FromBase64String(strText)

            Dim ms As New MemoryStream

            Dim cs As New CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write)

            cs.Write(inputByteArray, 0, inputByteArray.Length)

            cs.FlushFinalBlock()

            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8

            Return encoding.GetString(ms.ToArray())

        Catch ex As Exception
            Throw
        End Try

    End Function


End Class
