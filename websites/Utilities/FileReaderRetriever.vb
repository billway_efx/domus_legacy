Imports System.IO
Imports System.Xml
Imports System.Diagnostics

Public Class FileReaderRetriever

    ' *********************************************************************************
    ' Name:         GetReader
    ' Purpose:      Provides caller with a specific type of reader object used
    '               to access contents of the input file
    ' Inputs:       strInputFileValue - identifies the file to be read
    '               strReaderType - identifies type of reader object to return
    ' Returns:      Object - type of reader object
    '**********************************************************************************
    Public Function GetReader(ByVal strInputFileValue As String, _
                              ByVal strReaderType As String) As Object

        Dim fsInputStream As FileStream
        GetReader = Nothing

        Try
            ' Instantiate FileStream object
            fsInputStream = New FileStream(strInputFileValue, FileMode.Open, _
                                           FileAccess.Read, FileShare.Read)

            ' Set the file pointer to the beginning
            fsInputStream.Seek(0, SeekOrigin.Begin)

            ' Determine type of Reader requested and return it
            Select Case strReaderType
                Case BINARY_READER
                    ' Return BinaryReader 
                    GetReader = New BinaryReader(fsInputStream)
                Case STREAM_READER
                    ' Return StreamReader 
                    GetReader = New StreamReader(fsInputStream)
                Case XML_TEXT_READER
                    ' Return XmlTextReader 
                    GetReader = New XmlTextReader(fsInputStream)
                Case Else
                    ' Let user know the type of reader requested is not available
                    MsgBox("The type of file reader requested is not an available type " & _
                           "of file reader." & vbCrLf & "Ask System Administrator to check the " & _
                           "FileReaderRetriever class in the Utilities component.", MsgBoxStyle.Critical, _
                           "Reader Not Available")
            End Select

        Catch ex As Exception
            Debug.WriteLine(ex.Message)
            Throw (ex)
        End Try
    End Function
End Class
