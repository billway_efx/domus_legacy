Friend Module AppConstants

    ' File locations
    Public Const DEFAULT_OUPUT_FILE As String = "C:\Temp\DefaultOutputFile.txt"

    ' Reader types
    Public Const BINARY_READER As String = "System.IO.BinaryReader"
    Public Const STREAM_READER As String = "System.IO.StreamReader"
    Public Const XML_TEXT_READER As String = "System.Xml.XmlTextReader"

    Public Const ENVIRONMENTS_CONFIG_SECTION As String = "environments"

End Module
