Imports System.IO
Imports System.Xml
Imports System.Configuration
Imports System.Configuration.ConfigurationSettings
Imports System.Diagnostics

Public Class FileWriterRetriever

    ' *********************************************************************************
    ' Name:         GetWriter
    ' Purpose:      Provides caller with a specific type of writer object used
    '               to write data to an output file
    ' Inputs:       strWriterType - identifies type of writer object to return
    '               strOutputFileValue - identifies the file stream to write to
    ' Returns:      Object - type of reader object
    '**********************************************************************************
    Public Function GetWriter(ByVal strWriterType As String, _
                              Optional ByVal strOutputFileValue As String = DEFAULT_OUPUT_FILE) _
                              As Object

        Dim fsOut As FileStream
        'Dim stwrOut As StreamWriter
        Dim strOutputFileLocation As String
        GetWriter = Nothing

        Try
            ' Variable set from value in <appSettings> section in the machine.config file
            strOutputFileLocation = ConfigurationManager.AppSettings("strOutputFileLocation")

            ' Instantiate fsOut as a NEW FileStream object with Write attribute
            fsOut = New FileStream((strOutputFileLocation), FileMode.Create, FileAccess.Write)

            ' Overwrite the output file and set the file pointer to the beginning
            fsOut.Seek(0, SeekOrigin.Begin)

            ' Determine type of Writer requested and return it
            Select Case strWriterType
                Case "BINARYWRITER"
                    ' Return BinaryWriter 
                    GetWriter = New BinaryWriter(fsOut)
                Case "STREAMWRITER"
                    ' Return StreamWriter 
                    GetWriter = New StreamWriter(fsOut)
                Case "XMLTEXTWRITER"
                    ' Return XmlTextWriter 
                    GetWriter = New XmlTextWriter(fsOut, Nothing)
                Case Else
                    ' Let user know the type of writer requested is not available
                    MsgBox("The type of file writer requested is not an available type " & _
                           "of file writer." & vbCrLf & "Ask System Administrator to check the " & _
                           "FileWriterRetriever class in the Utilities component.", MsgBoxStyle.Critical, _
                           "Writer Not Available")
            End Select

        Catch ex As Exception
            Debug.WriteLine(ex.Message)
            Throw (ex)
        End Try
    End Function
End Class
