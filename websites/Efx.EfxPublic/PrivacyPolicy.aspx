﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PrivacyPolicy.aspx.cs" Inherits="Efx.EfxPublic.PrivacyPolicy" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <div id="pageCenterContainer" class="ppPage pageContainer residentsPage">
        <div class="pageTitle frPageTitle fr1Color" style="border-color: white;"><h2>Privacy Policy</h2></div>
        <div class="sectionEighty legalText frBlack">
            <article class="fullrow">
                <p class="frBlack">RentPaidOnline a company of EFX Financial Services Inc. respects the privacy of every individual who visits our web site. In general, 
                    you can visit RentPaidOnline without telling us who you are or revealing any information about yourself. RentPaidOnline will not collect 
                    any personal information about you by means of this site unless you have provided such personal data to us. If you do not want your 
                    personal data collected, please don’t submit it. When you engage in certain activities on RentPaidOnline.com, such as requesting information, 
                    electronic payments, and marketing we will ask you to provide certain information about yourself by filling out and submitting an online form. 
                    It is completely optional for you to engage in these activities. If you elect to engage in these activities, however, RentPaidOnline will 
                    require that you provide your name, mailing address, e-mail address, and other personal information. RentPaidOnline also uses information that 
                    you provide as part of our effort to keep you informed about product upgrades, special offers, and other RentPaidOnline services. 
                    RentPaidOnline does not share any information with third-party marketers. The information we collect is for RentPaidOnline internal use only.</p>
            </article>
            
        </div>
    </div>


    <script>

        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
            }
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);

            // set footer
            var gvWidth = window.innerWidth || document.documentElement.clientWidth;
            var gvHeight = window.innerHeight || document.documentElement.clientHeight;
            var vFooterH = gvHeight - $("footer").height()

            var vHeight = "height: " + ($("#pageCenterContainer").height() + 200) + "px !important";
           // $('.wrapper').attr('style', vHeight);
            if (vFooterH > $("#pageCenterContainer").height() + 200) {
                vHeight = "position: absolute; top: " + (vHeight) + "px";
            } else {
                vHeight = "position: absolute; top: " + ($("#pageCenterContainer").height() + 200) + "px";
            }

            //$('#footer').attr('style', vHeight);
        } // end of calc....


        $(document).ready(function () {

            /* */
            var vObj = document.getElementById("header");

            //vObj.style.top = "-40px";
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "100px");

            calc_container();
        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);


        // set menu to go back to default page
        $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

            evt.preventDefault();

            var vHash = $(this).attr('href');
            window.location.href = "/Default.aspx" + vHash;

        });
    </script>   
</asp:Content>
