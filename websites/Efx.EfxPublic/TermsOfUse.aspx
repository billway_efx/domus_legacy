﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TermsOfUse.aspx.cs" Inherits="Efx.EfxPublic.TermsOfUse" %>

<%@ Register Src="~/UserControls/TermsAndConditions.ascx" TagPrefix="uc1" TagName="TermsAndConditions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <div id="pageCenterContainer" class="legalText residentsPage">
        <uc1:TermsAndConditions runat="server" id="TermsAndConditions" />
    </div>

    <script>

        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
            }
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);
        } // end of calc....


        $(document).ready(function () {
            /* */
            var vObj = document.getElementById("header");

            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "100px");
            var vHeight = "height: " + ($("#pageCenterContainer").height() + 200) + "px !important";
            //$('.wrapper').attr('style', vHeight);
            vHeight = "position: absolute; top: " + ($("#pageCenterContainer").height() + 200) + "px";
            //$('#footer').attr('style', vHeight);

            calc_container();
        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);


        // set menu to go back to default page
        $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

            evt.preventDefault();

            var vHash = $(this).attr('href');
            window.location.href = "/Default.aspx" + vHash;

        });
    </script>    
</asp:Content>
