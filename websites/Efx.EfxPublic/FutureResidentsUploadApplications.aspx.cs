﻿using System.Web.UI;
using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Efx.EfxPublic
{
    public partial class FutureResidentsUploadApplications : BasePageV2, IFutureResidentsUploadApplications
    {
        private FutureResidentsUploadApplicationsPresenter _Presenter;

        public int PropertyId { get { return !String.IsNullOrEmpty(Request.QueryString["PropId"]) ? Request.QueryString["PropId"].ToInt32() : 0; } }
        public int ApplicantId { get { return !String.IsNullOrEmpty(Request.QueryString["ApplicantId"]) ? Request.QueryString["ApplicantId"].ToInt32() : 0; } }
        public FileUpload FileUploadControl { get { return BrowseFileControl; } }
        public EventHandler UploadButtonClicked { set { UploadButton.Click += value; } }
        public ISelectPaymentType PaymentTypeControl { get { return SelectPaymentType; } }
        public IPayerBillingInformation PayerBillingInformationControl { get { return PayerBillingInformation; } }
        public bool AcceptTermsChecked { get { return AcceptTermsCheckBox.Checked; } }
        public EventHandler CompleteButtonClicked { set { CompleteButton.Click += value; } }
        public Page ViewPage { get { return Page; } }
        public string PropertyNameText { set { PropertyNameLabel.Text = value; } }

        public List<ApplicationFee> FeesDataSource
        {
            set
            {
                FeesListView.DataSource = value;
                FeesListView.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            AddValidationSummaryToPage();
            SelectPaymentType.RenterId = ApplicantId;
            
            _Presenter = new FutureResidentsUploadApplicationsPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}