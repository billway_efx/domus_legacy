﻿<%@ Page Title="Future Residents Status" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FutureResidentsStatus.aspx.cs" Inherits="Efx.EfxPublic.FutureResidentsStatus" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <div id="pageCenterContainer" class="pageContainer srPage residentsPage ">
        <div class="pageTitle frPageTitle srPage"><h2>Future Residents</h2></div>
        <div class="column1" style="display: inline-block; border-right-width: 0; ">           
             <div class="textCenter">
                <!-- It's flow chart time-->
                <h3 class=" fr1Color">How It Works</h3>
                <h4 class="orangeOutline centeredMargin width200 fr1Color">Find the Property Where You Want to Live</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width200 fr1Color">Register</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width200 fr1Color ">Download and Complete the Rental Application</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width200 fr1Color ">Upload Your Application<br /><span>(You may need to scan it first)</span></h4>   
                <div class="arrowDown"></div> 
                <h4 class="orangeOutline centeredMargin width200 fr1Color ">Pay Your Application Fees</h4>
                <div class="waiting"></div> 
                <div class="arrowDown"></div>
                <h4 class="orangeBlock registerNow centeredMargin width200 fr2BColor ">Application is Approved<br /><span>(Check back and see)</span></h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width200 fr1Color ">Once Your Application is Approved You Can Start Paying Your Rent, Deposits and Fees Online!</h4>         
            </div>
        </div>
        
        <div class="column2">
            <div class="formRow gridHolder">
                <h3 class="fr1Color" >Status</h3>
                <asp:GridView ID="StatusGrid" runat="server" AutoGenerateColumns="false" AlternatingRowStyle-CssClass="gridAltRow" HeaderStyle-CssClass="tableHeader">
                    <Columns>
                        <asp:BoundField HeaderText="Property" DataField="PropertyDisplayName" />
                        <asp:HyperLinkField HeaderText="Application Uploaded" DataNavigateUrlFields="ApplicationUrl" Text="Download My Application" Target="_blank"/>
                        <asp:TemplateField>
                            <HeaderTemplate>FeesPaid</HeaderTemplate>
                            <ItemTemplate><%# ReturnYesOrNo((bool)Eval("FeesPaid")) %></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Status" DataField="Status" />
                        
                            
                        
                    </Columns>
                </asp:GridView>
           </div>
        </div>
</div>

    <script>

        // calc margin left to center container
        function calc_container() {

            var vML = "0";
            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                vML = (w_width - 1024) / 2;
            }
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);
        } // end of calc....


        $(document).ready(function () {
            /* */
            var vObj = document.getElementById("header");
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "160px");
            var vHeight = "height: " + ($("#pageCenterContainer").height() + 200) + "px !important";
            //$('.wrapper').attr('style', vHeight);
            vHeight = "position: absolute; top: " + ($("#pageCenterContainer").height() + 200) + "px";
            //$('#footer').attr('style', vHeight);

            calc_container();
        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);

        // set menu to go back to default page
        $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

            evt.preventDefault();

            var vHash = $(this).attr('href');
            window.location.href = "/Default.aspx" + vHash;

        });
    </script>

</asp:Content>
