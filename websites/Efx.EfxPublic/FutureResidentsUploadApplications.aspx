﻿<%@ Page Title="Future Residents Upload Application" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FutureResidentsUploadApplications.aspx.cs" Inherits="Efx.EfxPublic.FutureResidentsUploadApplications" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pageContainer residentsPage futurePage">
        <div class="pageTitle"><h2>Future Residents</h2></div>
        <div class="column1">           
             <div class="textCenter">
                <!-- It's flow chart time-->
                <h3>How It Works</h3>
                <h4 class="orangeOutline centeredMargin width200">Find the Property Where You Want to Live</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width100">Register</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width200 ">Download and Complete the Rental Application</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeBlock registerNow centeredMargin width200 ">Upload Your Application<br /><span>(You may need to scan it first)</span></h4>   
                <div class="arrowDown"></div> 
                <h4 class="orangeBlock registerNow centeredMargin width200 ">Pay Your Application Fees</h4>
                <div class="waiting"></div> 
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width200 ">Application is Approved<br /><span>(Check back and see)</span></h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width250 ">Once Your Application is Approved You Can Start Paying Your Rent, Deposits and Fees Online!</h4>         
            </div>
        </div>
        
        <div class="column2">
            <div class="sectionSixty">
                <h3>Upload Your Application (optional)</h3>
                <div class="fullrow">
                    <span class="greenText bolded">Property:</span><asp:Label ID="PropertyNameLabel" runat="server" CssClass="propertyInfoTitle greenText" />
                </div>
                <!-- ** Upload Application Section ** -->
                <div class="formHolder borderBottom">
                   <!--**First Row**-->
                    <div class="formRow">
                       <!--Upload Application Text Box -->
                       <div>
                           <span class="formLable">Upload Application</span>
                       </div>
                        <!-- Browse Button -->
                        <div class="buttonRightMarginAdjust">
                            <asp:FileUpload ID="BrowseFileControl" runat="server" CssClass="orangeButton"/>
                        </div>
                        <!-- Upload Button -->
                        <div>
                            <asp:Button ID="UploadButton" runat="server" CssClass="orangeButton" Text="Upload" CausesValidation="False"></asp:Button>
                        </div>
                    </div>
                </div>
                
                <!-- ** Pay Your Application Fee Section ** -->
                <h3>Pay Your Fees</h3>
                <div class="formHolder borderBottom">
                <uc1:PayerBillingInformation runat="server" id="PayerBillingInformation" />
                </div>

                <!-- ** Payment Type ** -->
                <div class="formHolder borderBottom">
                <h4 class="orangeText">Payment Type</h4>
                <uc1:SelectPaymentType runat="server" ID="SelectPaymentType" IsApplicant="True" />
                </div>

                <!-- ** Charged Fees ** -->
                <div class="formHolder">
                    <h4 class="orangeText">Your Fees</h4>
                    <asp:ListView ID="FeesListView" runat="server">
                        <LayoutTemplate>
                        <div class="formRow">
                            <p id="itemPlaceholder" runat="server" class="fees"></p>
                        </div>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <p class="fees">
                                <span>
                                    <asp:Label ID="FeeNameLabel" runat="server" Text='<%# Bind("FeeName") %>' />
                                </span>
                                <span class="feeSpacing">
                                    <asp:Label ID="FeeAmountLabel" runat="server" Text='<%# Bind("FeeAmountText") %>' />
                                </span>
                            </p>
                            <br />
                        </ItemTemplate>
                    </asp:ListView>
             </div>
                
                <div id="TermsAndCC" class="hide">
                    <uc1:TermsAndConditions runat="server" ID="TermsAndConditions" />
                </div>

                <!-- ** checkbox and Button ** -->
                <div class="formHolder textRight">
                        <!--**check Box**-->
                        <div class="formRow checkBoxBlock">
                            <asp:CheckBox ID="AcceptTermsCheckBox" runat="server" CssClass="checkBoxText" CausesValidation="false" Text=" " />
                            <asp:LinkButton ID="ShowTermsButton" runat="server" Text="Accept Terms of Agreement" OnClientClick="ShowModal('#TermsAndCC')"/>
                        </div>
                        
                        <!-- Browse Button -->
                        <div class="formRow buttonTopMarginAdjust">
                            <asp:Button ID="CompleteButton" runat="server" CssClass="orangeButton" Text="Complete" CausesValidation="False"></asp:Button>
                        </div>     
             </div>
                <asp:Literal ID="PopupBox" runat="server"></asp:Literal>
                
        </div>
    </div>
    </div>     
<script>

$(document).ready(function() {
    /* Mask */
    ApplySingleMask("#<%=PayerBillingInformation.ZipCodeClientId%>", "00000");
});
</script>
</asp:Content>
