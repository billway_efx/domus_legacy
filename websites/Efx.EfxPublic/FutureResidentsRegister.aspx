﻿<%@ Page Title="Future Residents Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FutureResidentsRegister.aspx.cs" Inherits="Efx.EfxPublic.FutureResidentsRegister" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <div id="pageCenterContainer" class="pageContainer frrPage residentsPage">
        <div class="pageTitle frPageTitle fr1Color" style="border-color: white;"><h2>Future Residents</h2></div>
        <div class="frrPage column1">           
             <div class="textCenter">
                <!-- It's flow chart time-->
                <h3 class="fr1Color">How It Works</h3>
                <h4 class="orangeOutline centeredMargin width200 fr2BColor"style="color: white; border: solid; border-color: #0462c2 !important; border-width: 2px;" >Find the Property Where You Want to Live</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeBlock centeredMargin registerNow width100 fr2BColor">Register</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width200 fr1Color">Download and Complete the Rental Application</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width200 fr1Color">Upload Your Application<br /><span>(You may need to scan it first)</span></h4>   
                <div class="arrowDown"></div> 
                <h4 class="orangeOutline centeredMargin width200 fr1Color">Pay Your Application Fees</h4>
                <div class="waiting"></div> 
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width200 fr1Color">Application is Approved<br /><span>(Check back and see)</span></h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width200 fr1Color">Once Your Application is Approved You Can Start Paying Your Rent, Deposits and Fees Online!</h4>         
            </div>
        </div> 
    
        <div class="frrPage column2">
            <div class="formHolder frrPage formContainer">
            <h3 class="fr1Color">Register With RentPaidOnline</h3>
            <uc1:ResidentRegistration runat="server" ID="ResidentRegistration" AreConditionalFieldsVisible="False"/>
            </div> 
            <br />
            <br />
            <!-- CMallory - Task 0499 - Added Div to be displayed if the property is an Apply Connect Property -->
            <div id="ApplyConnect" runat="server">
                <h1 style="color:black;"><strong>Property Requires a Credit and Background Check</strong></h1>
                <br />
             <img src="Images/ApplyConnect-Logo (1).png" />
            </div>
            
            <div class="formContainer">
                <div class="sectionDivide fr1Color" style="border-bottom-color: transparent;" >
                    <h2 class="fr1Color" style="background-color: transparent; ">Or</h2>
                </div>
            </div> 

            <h3 class="greenText fr1Color">I Already Registered, Log In</h3>

            <!-- Already Registered, login from -->
            <div class="formHolder formContainer">
                
                <!--**First Row**-->
                <div class="formRow">
                   <!--User Name Text Box -->
                   <div class="formFullWidth">
                       <span class="formLable fr1Color">Email Address (username)</span>
                       <asp:TextBox ID="InputUserNameTextBox" runat="server" type="email" MaxLength="100"></asp:TextBox>
                   </div>
               </div>
               
                <!--**Second Row**-->
                <div class="formRow">
                    <!--Password Text Box -->
                   <div class="formRow">
                       <span class="formLable fr1Color">Password</span>
                       <asp:TextBox ID="InputPasswordTextBox" runat="server" TextMode="Password" CssClass="formFullWidth"></asp:TextBox>
                   </div>
                </div>
                
                <div class ="displayNone">
                    <div id="ForgotPassword">            
                        <uc1:ForgotPasswordWindow runat="server" ID="ForgotPasswordWindow" />
                    </div>
                </div>

                <asp:LinkButton ID="ForgotPasswordLinkButton" runat="server" OnClick="ForgotPasswordButtonClicked" CausesValidation="False" CssClass="smallLink">Forgot Password?</asp:LinkButton>

                <!--**Button Time**-->
                <div class="generalButtonHolder">
                     <asp:Button ID="LoginButton" runat="server" CssClass="orangeButton currentResidentsSubmit fr2BColor" Text="Log in" CausesValidation="False"></asp:Button>
                </div>
                <div class="clearFix"></div>           
            </div>
            <div class="clearFix"></div>             
        </div>
    </div>

    <script>

        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
            }
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);
        } // end of calc....


        $(document).ready(function () {

            /* */
            var vObj = document.getElementById("header");

            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "120px");
            var vHeight = "height: " + ($("#pageCenterContainer").height() + 200) + "px !important";
            //$('.wrapper').attr('style', vHeight);
            vHeight = "position: absolute; top: " + ($("#pageCenterContainer").height() + 200) + "px";
            //$('#footer').attr('style', vHeight);

            calc_container();

            /* Mask */
            ApplySingleMask("#<%=ResidentRegistration.PhoneNumberClientId%>", "(000) 000-0000");
        });

        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);

        // set menu to go back to default page
        $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

            evt.preventDefault();

            var vHash = $(this).attr('href');
            window.location.href = "/Default.aspx" + vHash;

        });


    </script>
</asp:Content>
