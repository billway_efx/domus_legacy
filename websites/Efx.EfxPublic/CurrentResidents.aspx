﻿<%@ Page Title="Current Residents" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CurrentResidents.aspx.cs" Inherits="Efx.EfxPublic.CurrentResidents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <div id="pageCenterContainer" class="crPage residentsPage" >
        <div class="pageTitle frPageTitle fr1Color" style="border-color: white;"><h2 class="crPage">Current Residents</h2></div>
        <div class="crPage column1" style="border-color: white;">
            <div class="textCenter">
                <h3 class="fr2Color" Style="color:#0462c2 !important;" >How It Works</h3>
                <h4 class="orangeBlock centeredMargin registerNow frPage width200 fr2BColor" style="border: solid; border-color: #0462c2 !important; border-width: 2px; ">Find Your Property</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width100 fr1Color ">Register</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width100 fr1Color  ">Login</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width100 fr1Color ">Pay Rent</h4>         
            </div>
        </div>
        <div class="crPage column2">
            <h3 class="fr2Color" Style="color:#0462c2 !important;">Get Started</h3>
            <h5 class="frBlack" >Find the Property Where You Live</h5>
            <uc1:FindProperty runat="server" id="FindProperty" />
            <asp:HyperLink ID="AlreadyRegisterLink" CssClass="smallLink alreadyRegLink" runat="server" Style="color: #0462c2 !important;">Already registered? Login here</asp:HyperLink>
        </div>
    </div> 

<script>

    // calc margin left to center container
    function calc_container() {

        var w_width = $(window).width();
        if ($(window).width() >= 1024) {
            var vML = (w_width - 1024) / 2;
        }
        $("#pageCenterContainer").css("margin-left", vML);
        $("#pageCenterContainer").css("margin-right", vML);
    } // end of calc....


    $(document).ready(function() {
        /* */
        var vObj = document.getElementById("header");
        $(".residentsPage").css("position","absolute");
        $(".residentsPage").css("top", "100px");

        var vHeight = "height: " + ($("#pageCenterContainer").height() + 200) + "px !important";
        //$('.wrapper').attr('style', vHeight);
        vHeight = "position: absolute; top: " + ($("#pageCenterContainer").height() + 200) + "px";
        //$('#footer').attr('style', vHeight);
        if ($(window).width() >= 760 && $(window).width() <= 810) {
            vHeight = "position: absolute; bottom: " + "00px";
        }
       // $('#footer').attr('style', vHeight);

        calc_container();

        // move footer to the bottom of page
        //$("footer").addClass("footer");
        //$("footer").css("position", "absolute");  
        //$("footer").css("top", "900px");

    });


    // Listen for resize changes
    window.addEventListener("resize", function () {

        calc_container();
    }, false);


    // set menu to go back to default page
    $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

        evt.preventDefault();

        var vHash = $(this).attr('href');
        window.location.href = "/Default.aspx" + vHash;

    });
</script>


</asp:Content>

