﻿using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using System;
using System.Web.UI.WebControls;

namespace Efx.EfxPublic
{
    public partial class CurrentResidentsRegister : BasePageV2, ICurrentResidentsRegister
    {
        private CurrentResidentsRegisterPresenter _Presenter;
		public Label ExistingUser { get { return ExistingUserLabel; } }
   
        protected void Page_Load(object sender, EventArgs e)
        {
            AddValidationSummaryToPage();
            _Presenter = new CurrentResidentsRegisterPresenter(this);

            if (!Page.IsPostBack)
            {
                _Presenter.InitializeValues();





            }
        }

        public EfxFramework.ViewPresentation.Interfaces.UserControls.IResidentRegister ResidentRegisterControl
        {
            get { return ResidentRegistration; }
        }


    }
}