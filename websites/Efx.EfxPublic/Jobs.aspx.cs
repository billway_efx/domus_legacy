﻿using System.Collections.Generic;
using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using System;
using System.Web.UI.WebControls;
using Mb2x.ExtensionMethods;

namespace Efx.EfxPublic
{
    public partial class Jobs : BasePageV2, IJobs
    {
        private JobsPresenter _Presenter;

        public int JobId { get { return !string.IsNullOrEmpty(Request.QueryString["JobId"]) ? Request.QueryString["JobId"].ToInt32() : 0; } }
        public bool AreJobsAvailable { set { NoJobsMessage.Visible = value; } }
        public EventHandler JobsListedSelectedIndexChanged { set { JobsDropDownList.SelectedIndexChanged += value; } }
        public DropDownList JobsListed { get { return JobsDropDownList; } }
        public string JobTitleText { get { return jobTitle.Text; } set { jobTitle.Text = value; } }
        public string JobPostDateText { get { return jobPostDate.Text; } set { jobPostDate.Text = value; } }
        public string JobDescriptionLiteralText { set { JobDescriptionLiteral.Text = value; } }
        public string DutiesLiteralText { set { DutiesLiteral.Text = value; } }
        public string SkillsLiteralText { set { SkillsLiteral.Text = value; } }
        public IApplyNowWindow ApplyNowModal { get { return ApplyNowWindow; } }
        public bool JobPostingVisible { set { JobPostingPanel.Visible = value; } }
        public bool ShowText { set { CurrentOppText.Visible = value; } }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new JobsPresenter(this);

            AddValidationSummaryToPage();
            if (!Page.IsPostBack)
            {
                _Presenter.InitializeValues();
            }
        }
    }
}