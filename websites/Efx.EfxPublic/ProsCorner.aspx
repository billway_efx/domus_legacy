﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProsCorner.aspx.cs" Inherits="Efx.EfxPublic.ProsCorner" MasterPageFile="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>
    <div id="pageCenterContainer" class="pcPage residentsPage " >
        <div class="pageTitle frPageTitle fr1Color" style="border-color: white;"><h2>Pro's Corner</h2></div>
        <div class="" ><img src="/Images/prosCorner.jpg" class="prosCornerImage" style="width: 100%; height: auto;" /></div>
        <uc1:BlogView runat="server" ID="BlogView" />
    </div>
    <!-- prosCornerImage  -->
<script>

    // calc margin left to center container
    function calc_container() {

        var w_width = $(window).width();
        if ($(window).width() >= 1024) {
            var vML = (w_width - 1024) / 2;
        }
        $("#pageCenterContainer").css("margin-left", vML);
        $("#pageCenterContainer").css("margin-right", vML);
    } // end of calc....


    $(document).ready(function() {
        /* */
        var vObj = document.getElementById("header");

        $(".residentsPage").css("position","absolute");
        $(".residentsPage").css("top","100px");
        var vHeight = "height: " + ($("#pageCenterContainer").height() + 200) + "px !important";
        //$('.wrapper').attr('style', vHeight);
        vHeight = "position: absolute; top: " + ($("#pageCenterContainer").height() + 200) + "px";
        //$('#footer').attr('style', vHeight);

        calc_container();
    });


    // Listen for resize changes
    window.addEventListener("resize", function () {

        calc_container();
    }, false);

    // set menu to go back to default page
    $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

        evt.preventDefault();

        var vHash = $(this).attr('href');
        window.location.href = "/Default.aspx" + vHash;

    });

</script>
</asp:Content>
