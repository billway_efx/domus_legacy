﻿<%@ Page Title="Future Residents Download Application" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FutureResidentsDownloadApplication.aspx.cs" Inherits="Efx.EfxPublic.FutureResidentsDownloadApplication" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pageContainer residentsPage">
        <div class="pageTitle"><h2>Future Residents</h2></div>
        <div class="column1">           
             <div class="textCenter">
                <!-- It's flow chart time-->
                <h3>How It Works</h3>
                <h4 class="orangeOutline centeredMargin width200">Find the Property Where You Want to Live</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width100">Register</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeBlock registerNow centeredMargin width200 ">Download and Complete the Rental Application</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width200 ">Upload Your Application<br /><span>(You may need to scan it first)</span></h4>   
                <div class="arrowDown"></div> 
                <h4 class="orangeOutline centeredMargin width200 ">Pay Your Application Fees</h4>
                <div class="waiting"></div> 
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width200 ">Application is Approved<br /><span>(Check back and see)</span></h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width250 ">Once Your Application is Approved You Can Start Paying Your Rent, Deposits and Fees Online!</h4>         
            </div>
        </div>
        
        <div class="column2">
            <div class="fullrow">
                <h3>Download Application</h3>
                <asp:Label ID="NoPropertyFoundLabel" runat="server" Text="No Property Found" Visible="False" />
                <asp:Panel ID="ApplicationDownloadPanel" runat="server">
                    <div class="fullrow">
                        <span class="greenText bolded">Property:</span><asp:Label ID="PropertyNameLabel" runat="server" CssClass="propertyInfoTitle greenText" />
                    </div>
                    <!--**Button Time**-->
                    <div class="leftButtonHolder topMarginAdjust">
                        <asp:Button ID="DownloadApplicationButton" runat="server" CssClass="orangeButton" Text="Download Application"></asp:Button>
                        <asp:Button ID="NextButton" runat="server" CssClass="orangeButton" Text="Next"></asp:Button>
                    </div>
                        
                    <div class="clearFix"></div>
                    <div class="apartmentImage">
                        <asp:Image ID="PropertyImage" runat="server" AlternateText="Property Image" Width="300px" />                      
                    </div>
                </asp:Panel>
            </div>
        </div> 
    </div>

</asp:Content>
