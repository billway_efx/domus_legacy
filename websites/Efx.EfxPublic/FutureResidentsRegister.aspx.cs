﻿using System.Web;
using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using EfxFramework.Web;
using System;
using System.Web.UI;
using Mb2x.ExtensionMethods;

namespace Efx.EfxPublic
{
    public partial class FutureResidentsRegister : BasePageV2, IFutureResidentRegister, ILoginV2
    {
        private FutureResidentRegisterPresenter _Presenter;

        public string UsernameText { get { return InputUserNameTextBox.Text; } }
        public string PasswordText { get { return InputPasswordTextBox.Text; } }
        public int LoggedInUserId { set { SaveApplication(value); } }
        public EventHandler LoginButtonClick { set { LoginButton.Click += value; } }
        public IResidentRegister ResidentRegisterControl { get { return ResidentRegistration; } }
        public IForgotPassword ForgotPasswordControl { get { return ForgotPasswordWindow; } }
        public int PropertyId { get { return !string.IsNullOrEmpty(Request.QueryString["PropId"]) ? Request.QueryString["PropId"].ToInt32() : 0; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            AddValidationSummaryToPage();

            var Login = new LoginPresenter<Applicant>(this);
            _Presenter = new FutureResidentRegisterPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();

            //CMallory - Task 00499 - Retrieve property object based on Property ID.
            ApplyConnect.Visible = false;
            
            Property propertyRetrieved = new Property(PropertyId);
            if (propertyRetrieved.UsesApplyConnect)
                {
                    ApplyConnect.Visible = true;
                }
        }

        protected void SaveApplication(int userId)
        {
            _Presenter.SaveApplication(userId);
        }

        protected void ForgotPasswordButtonClicked(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "test", "<script type='text/javascript'>ShowModal(\"#ForgotPassword\");</script>", false);
        }
    }
}