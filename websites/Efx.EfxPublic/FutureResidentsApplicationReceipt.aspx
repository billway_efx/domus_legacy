﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Print.Master" AutoEventWireup="true" CodeBehind="FutureResidentsApplicationReceipt.aspx.cs" Inherits="Efx.EfxPublic.FutureResidentsApplicationReceipt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="pageContainer residentsPage">
        <div class="pageTitle"><h2>Future Residents</h2></div>
            <div class="column1">           
                 <div class="textCenter">
                    <!-- It's flow chart time-->
                    <h3>How It Works</h3>
                    <h4 class="orangeOutline centeredMargin width200">Find the Property Where You Want to Live</h4>
                    <div class="arrowDown"></div>
                    <h4 class="orangeOutline centeredMargin width100">Register</h4>
                    <div class="arrowDown"></div>
                    <h4 class="orangeOutline centeredMargin width200 ">Download and Complete the Rental Application</h4>
                    <div class="arrowDown"></div>
                    <h4 class="orangeOutline centeredMargin width200 ">Upload Your Application<br /><span>(You may need to scan it first)</span></h4>   
                    <div class="arrowDown"></div> 
                    <h4 class="orangeOutline centeredMargin width200 ">Pay Your Application Fees</h4>
                    <div class="waiting"></div> 
                    <div class="arrowDown"></div>
                    <h4 class="orangeOutline centeredMargin width200 ">Application is Approved<br /><span>(Check back and see)</span></h4>
                    <div class="arrowDown"></div>
                    <h4 class="orangeOutline centeredMargin width250 ">Once Your Application is Approved You Can Start Paying Your Rent, Deposits and Fees Online!</h4>         
                </div>
            </div>
        
            <div class="column2">
                <div class="sectionSixty">
                    <!-- ** Info Section ** -->
                    <div class="formHolder borderBottom">
                        <div class="printLogo"><img src="/Images/RentPaidOnlineLogo.png" alt ="rentPaidOnlineLogo" /></div>
                        
                        <h3>Application Receipt</h3>
                        <div class="formRow topMarginAdjust">
                            <h4><span>Property:</span><asp:Label ID="PropertyNameLabel" runat="server" CssClass="propertyInfoTitle"/></h4>
                        </div>
                        <div class="formRow ">
                            <h5 class="greenText"><span>Application:</span><asp:Label ID="UploadStatusLabel" runat="server" CssClass="infoTitle"/></h5>
                        </div>
                    </div>
                    

                    <!-- ** Transaction Section ** -->
                    <div class="formHolder borderBottom">
                       <div class="formRow">
                           <h5 class="greenText"><span>Transaction ID:</span><asp:Label ID="TransactionIdLabel" runat="server" class="infoTitle"/></h5> 
                       </div>               
                    </div>

                    <!-- **Fees Paid** -->
                    <div class="formHolder">
                        <h4 class="orangeText">Fees Paid:</h4>
                        <asp:ListView ID="FeesList" runat="server" class="formHolder">
                            <LayoutTemplate>
                                <div class="formRow" id="itemPlaceHolder" runat="server"></div>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <div class="formRow">
                                    <p class="fees">
                                        <span>
                                            <asp:Label ID="FeeName" runat="server" Text='<%# Bind("FeeName") %>'/>
                                        </span>
                                        <span class="feeSpacing">
                                            <asp:Label ID="FeeAmount" runat="server" Text='<%# Bind("FeeAmountText") %>'/>
                                        </span>
                                    </p>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                
                    <!-- ** It's Button Time ** -->
                    <div class="formHolder">
                            <!-- Browse Button -->
                            <div class="formRow buttonTopMarginAdjust">
                                <asp:Button ID="PrintReceiptButton" runat="server" CssClass="orangeButton buttonRightMarginAdjust" Text="Print Receipt" OnClientClick="window.print(); return false;"></asp:Button>
                                <asp:Button ID="ViewStatusOfApplicationButton" runat="server" CssClass="orangeButton" Text="View Status of Application"></asp:Button>
                                <br />
                                <br />
                                <br />
                                <br />
                                <asp:ImageButton ID="ApplyConnectButton" runat="server" ImageUrl="~/Images/ApplyConnectLink.jpg" />
                            </div>     
                 </div>
                
            </div>
        </div>
    </div>
</asp:Content>
