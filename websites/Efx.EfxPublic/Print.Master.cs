﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;

namespace Efx.EfxPublic
{
    public partial class PrintMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            litYear.Text = DateTime.Now.Year.ToString(System.Globalization.CultureInfo.InvariantCulture);
            //CmMallory - Task 00556 - Added Code to make the buttons false by default.
            LogInButton.Visible = false;
            SignUpButton.Visible = false;
            var ApplicantId = HttpContext.Current.User.Identity.Name.ToNullInt32();

            if (!ApplicantId.HasValue || ApplicantId.Value < 1)
            {
                LogoutLink.Visible = false;
                return;
            }

            if (ApplicantId > 1)
            {
                LogInButton.Visible = false;
                SignUpButton.Visible = false;
            }

            var Applicant = new Applicant(ApplicantId);

            if (Applicant.ApplicantId < 1)
            {
                LogoutLink.Visible = false;
                LogInButton.Visible = true;
                SignUpButton.Visible = true;
                BasePageV2.SafePageRedirect("~/Account/LogOut.aspx");
                return;
            }

            FutureResidentLinkVisibility(Applicant);
            WelcomeMessageText.Text = String.Format("Welcome, {0} ", Applicant.FirstName);
        }

        protected void SignUpButtonClicked(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "test", "<script type='text/javascript'>ShowModal(\"#SignUp\");</script>", false);
        }

        protected void FutureResidentLinkVisibility(Applicant applicant)
        {
            var PropertyId = applicant.Applications.Count > 0 ? applicant.Applications[0].PropertyId : 0;

            FutureResidentsDownloadApplicationLink.NavigateUrl = String.Format("~/FutureResidentsDownloadApplication.aspx?PropId={0}&ApplicantId={1}", PropertyId, applicant.ApplicantId);
            FutureResidentsDownloadApplicationLink.Visible = true;
            FutureResidentsUploadApplicationLink.NavigateUrl = String.Format("~/FutureResidentsUploadApplications.aspx?PropId={0}&ApplicantId={1}", PropertyId, applicant.ApplicantId);
            FutureResidentsUploadApplicationLink.Visible = true;
            FutureResidentsStatusLink.NavigateUrl = String.Format("~/FutureResidentsStatus.aspx?ApplicantId={0}", applicant.ApplicantId);
            FutureResidentsStatusLink.Visible = true;
            PayYourFeesLink.NavigateUrl = String.Format("~/FutureResidentsUploadApplications.aspx?PropId={0}&ApplicantId={1}", PropertyId, applicant.ApplicantId);
            PayYourFeesLink.Visible = true;
        }
    }
}