﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SuccessfulRegistration.aspx.cs" Inherits="Efx.EfxPublic.SuccessfulRegistration" MasterPageFile="Site.Master" %>
    
<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" runat="server">
	<title>Successful Registration</title>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <div id="pageCenterContainer" class="pageContainer srPage residentsPage " >
        <div class="pageTitle frPageTitle" style="border-color: white;"><h2>Success!</h2></div>
        <asp:MultiView ID="ButtonInMessage" runat="server" ActiveViewIndex="0">
			<asp:View ID="ViewWithButton" runat="server">
			    <div class="resultHolder orangeOutline">
                    <h3 class="fr1Color">Congratulations!</h3>
                    <h4 class="fr1Color">You are now registered on RentPaidOnline.</h4>
                    <p class="fr1Color">Click Login to be on your way to paying your
                    rent online in no time</p>
                    <asp:LinkButton ID="SuccessNowLogInButton" runat="server" CssClass="orangeButton successLogInButton" CausesValidation="False">Log in</asp:LinkButton>
                </div>
            </asp:View>
            
            <asp:View ID="ViewWithoutButton" runat="server">
                <div class="resultHolder orangeOutline">                                                        
                    <h3 class="fr1Color">Congratulations!</h3>
                    <h4 class="fr1Color">You are now registered on RentPaidOnline.</h4>
                    <p class="fr1Color">You will receive a notification email when
                    your account is setup by your Property
                    <p class="fr1Color">Manager so that you can pay your rent.</p>
                </div>
            </asp:View>
        </asp:MultiView>
    </div>
 
    <script>

        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
            }
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);
        } // end of calc....


        $(document).ready(function () {
            /* */
            var vObj = document.getElementById("header");
            //vObj.style.top = "-40px";
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "100px");

            calc_container();

            // move footer to the bottom of page
            //$("footer").addClass("footer");
        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);


        // set menu to go back to default page
        $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

            evt.preventDefault();

            var vHash = $(this).attr('href');
            window.location.href = "/Default.aspx" + vHash;

        });


    </script>    
           
</asp:Content>


