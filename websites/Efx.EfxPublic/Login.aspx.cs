﻿using System.Web.UI;
using EfxFramework;
using EfxFramework.Web;
using System;

namespace Efx.EfxPublic
{
    public partial class Login : BasePageV2
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ApplicantLogInButton.PostBackUrl = Settings.ApplicantLoginLink;
            ResidentLogInButton.PostBackUrl = Settings.RenterLoginLink;

        }

        protected void SignUpButton_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "test", "<script type='text/javascript'>ShowModal(\"#SignUp\");</script>", false);
        }
    }
}