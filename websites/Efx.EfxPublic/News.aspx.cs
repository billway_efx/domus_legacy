﻿using System.Web.UI;
using EfxFramework.ViewPresentation.Interfaces.Pages.BlogPages;
using EfxFramework.ViewPresentation.Interfaces.UserControls.Blogs;
using EfxFramework.ViewPresentation.Presenters.Factories;
using EfxFramework.ViewPresentation.Presenters.Pages.BlogPresenters;
using EfxFramework.Web;
using System;
using Mb2x.ExtensionMethods;

namespace Efx.EfxPublic
{
    public partial class News : BasePageV2, IPublicNewsAndEvents
    {
        private BlogPresenterBase _Presenter;
        public IBlogView BlogViewUserControl { get { return BlogView; } }

        public string NewsBlogUrl
        {
            get
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Post"]) && Request.QueryString["Post"].ToNullInt32() > 0)
                {
                    return Request.QueryString["Post"];
                }
                return "~/News.aspx";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = BlogPresenterFactory.GetBlogPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}