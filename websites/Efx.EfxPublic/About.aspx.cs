﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework;
using EfxFramework.Web;
using EfxFramework.WordPress;
using EfxFramework.ExtensionMethods;

namespace Efx.EfxPublic
{
    public partial class About : BasePageV2
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NewsListView.DataSource = Announcement.GetAllActiveNewsAndEvents().OrderByDescending(blog => blog.AnnouncementDate).Take(3);
            NewsListView.DataBind();

            JobPostings.DataSource = JobPosting.GetMostRecentActiveJobPostings();
            JobPostings.DataBind();
            if (JobPostings.Items.Count < 1)
                ViewJobsPage.Visible = false;
        }
    }
}