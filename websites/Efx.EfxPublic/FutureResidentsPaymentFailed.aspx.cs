﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Efx.EfxPublic
{
    public partial class FutureResidentsPaymentFailed : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ProcessQueryStrings();
        }
        //Salcedo - 1/19/2014 - added support for more specific error information
        private void ProcessQueryStrings()
        {
            string FailureMsg = Request.QueryString["Message"];
            if (FailureMsg != null)
            {
                if (FailureMsg.Length > 0)
                {
                    lblFailureMessage.Text = "Transaction Response: " + FailureMsg;
                    lblFailureMessage.Visible = true;
                }
            }
        }
    }
}