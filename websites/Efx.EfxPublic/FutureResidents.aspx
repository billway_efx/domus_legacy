﻿<%@ Page Title="Future Residents" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FutureResidents.aspx.cs" Inherits="Efx.EfxPublic.FutureResidents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>
    <div id="pageCenterContainer" class="frPage residentsPage " >
        <div class="pageTitle frPageTitle fr1Color" style="border-color: white;"><h2 class="frPage">Future Residents</h2></div>
        <div class="frPage column1" style="border-color: white;">           
             <div class="textCenter">
                <!-- It's flow chart time-->
                <h3 class="fr1Color" >How It Works</h3>
                <h4 class="orangeBlock centeredMargin registerNow width200 fr2BColor" style="border: solid; border-color: #0462c2 !important; border-width: 2px; ">
                    Find the Property Where You Want to Live</h4>
                <div class="arrowDown"></div>
                 <p class="orangeOutline centeredMargin width200 fr1Color ">Register</p>
                
                <div class="arrowDown"></div>
                 <p class="orangeOutline centeredMargin width200 fr1Color ">Download and Complete the Rental Application</p>
                
                <div class="arrowDown"></div>
                 <p class="orangeOutline centeredMargin width200 fr1Color ">Upload Your Application<br /><span>(You may need to scan it first)</span></p>   
                <div class="arrowDown"></div> 
                  <p class="orangeOutline centeredMargin width200 fr1Color ">Pay Your Application Fees</p> 

                <div class="waiting2"></div> 
                <div class="arrowDown"></div>
                 <p class="orangeOutline centeredMargin width200 fr1Color ">Application is Approved<br /><span>(Check back and see)</span></p> 
                <div class="arrowDown"></div>
                 <p class="orangeOutline centeredMargin width200 fr1Color ">Once Your Application is Approved You Can Start Paying Your Rent, Deposits and Fees Online!</p>       
            </div>
        </div>
        <div class="frPage column2">
            <!--cakel -->
            <h3 class="fr1Color" >Get Started</h3>
            <h5 class=" " style="color: #223b67 !important;">Find the Property Where You Want to Live</h5>
            <uc1:FindProperty runat="server" id="FindProperty" />
        </div>
    </div>

    <script>

        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
            }
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);
        } // end of calc....


        $(document).ready(function () {
            /* */
            var vObj = document.getElementById("header");
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "100px");
            var vHeight = "height: " + ($("#pageCenterContainer").height() + 200) + "px !important";
            //$('.wrapper').attr('style', vHeight);
            vHeight = "position: absolute; top: " + ($("#pageCenterContainer").height() + 200) + "px";
            //$('#footer').attr('style', vHeight);

            calc_container();
        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);

        // set menu to go back to default page
        $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

            evt.preventDefault();

            var vHash = $(this).attr('href');
            window.location.href = "/Default.aspx" + vHash;

        });

    </script>
</asp:Content>
