﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Efx.EfxPublic.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Contact</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ValidateRequestMode="Inherit">
    <div style="background-image: url('/Images/BackgroundforContactUs.jpg'); background-repeat: no-repeat">

   
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>
    <div id="pageCenterContainer" class="pageContainer conPage residentsPage" >
        <div class="pageTitle frPageTitle fr1Color" style="border-color: white;"><h2>Contact</h2></div>
        <div class="contactColumn1  " style="margin-left: 5%; width: 38%; "> 
            <h3 class=" fr2Color">Contact RentPaidOnline</h3>            
                    <asp:RadioButtonList ID="ContactRentPaidOnlineRadioButtonList"  runat="server">
                        <asp:listItem Text="Participating Property Manager" class="frBlack" Value="1"></asp:listItem>
                        <asp:listItem Text="Prospective Property Manager" class="frBlack" Value="2"></asp:listItem>
                        <asp:listItem Text="Current Resident" class="frBlack" Value="3"></asp:listItem>
                        <asp:listItem Text="Future Resident" class="frBlack" Value="4"></asp:listItem>
                        <asp:listItem Text="Other" class="frBlack" Value="5"></asp:listItem>
                    </asp:RadioButtonList>
                    <uc1:ContactForm runat="server" ID="ContactForm" />          
        </div>
        
        <div class="contactColumn2">
            <h3 class="fr2Color" >Contact Info</h3>

            <div class="addressInfo">
                <h4 class="fr2Color" >Address</h4>
                <p class="frBlack">RentPaidOnline<br />
                c/o EFX Financial Services<br />
                601 Cleveland Street Suite 950<br />
                Clearwater, FL 33755
            </p>

            <br />
            <br />
                <!--CMallory - Task 00617 - Added Social Media links to the Contact page as requested. -->
                <span>Follow Us!</span>
                <br />
                <a href="https://www.facebook.com/rentpaidonline1?fref=ts" title="Facebook" target="_blank">Facebook</a>
                <br />
                <a href="https://twitter.com/RentPaidOnline" title="Twitter"  target="_blank">Twitter</a>
                <br />
                <a href="mailto:info@rentpaidonline.com" title="email" target="_blank">Email Us</a>
                <br />
            </div>
            
            <div class="phoneEmailInfo" style="margin-top:12px;">
                <h4 class="fr2Color" >Phone number</h4>
                <p class="frBlack">Toll Free: <a class="telephone" href="tel:855-769-7368">855-PMY-RENT</a></p><br />
                <h4 class="fr2Color" ><asp:HyperLink ID="Support" runat="server" Text="Support" class="fr2Color" NavigateUrl="https://rentpaidonline.rhinosupport.com/" Target="_blank" /></h4>
            </div>
                    
            <div class="locationSection">
                 <h4 class="fr2Color" >Location</h4>
                <div class="locationImage"></div>
            </div>
        </div>
    </div>


         </div><!--End Background image -->

    <script>

        // calc margin left to center container
        function calc_container() {

            var vML = "0";
            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                vML = (w_width - 1024) / 2;
            }
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);
        } // end of calc....


        $(document).ready(function () {
            /* */
            var vObj = document.getElementById("header");
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "100px");
            var vHeight = "height: " + ($("#pageCenterContainer").height() + 200) + "px !important";
            //$('.wrapper').attr('style', vHeight);
            vHeight = "position: absolute; top: " + ($("#pageCenterContainer").height() + 200) + "px";
            //$('#footer').attr('style', vHeight);

            calc_container();
        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);

        // set menu to go back to default page
        $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

            evt.preventDefault();
            var vHash = $(this).attr('href');
            window.location.href = "/Default.aspx" + vHash;

        });
    </script>


</asp:Content>
