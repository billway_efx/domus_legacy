﻿using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.Pages.FindPropertyPages;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.Factories;
using EfxFramework.ViewPresentation.Presenters.Pages.FindPropertyPresenters;
using EfxFramework.Web;
using System;

namespace Efx.EfxPublic
{
    public partial class FutureResidents : BasePageV2, IFutureResidentFindProperty
    {
        private FindPropertyPresenterBase _Presenter;
        public string GetPropertiesWithApplicationsBySearchStringUrl { get { return Settings.GetPropertiesWithApplicationsBySearchString; } }
        public IFindPropertyUserControl FindPropertyControl { get { return FindProperty; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = FindPropertyPresenterFactory.GetFindPropertyPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}