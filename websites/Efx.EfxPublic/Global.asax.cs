﻿using EfxFramework.Logging;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Net.Mail;
using System.IO;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using EfxFramework;

namespace Efx.EfxPublic
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started 
            var SessionId = Session.SessionID;
        }

        //CMallory - Task 00477 - Added to prevent Clicking Jacking Vulnerability.
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpContext.Current.Response.AddHeader("x-frame-options", "DENY");
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //cakel: TaskID00430 - Public Site
            if (HttpContext.Current.Server.GetLastError() != null)
            {

                //string _user = HttpContext.Current.Profile.UserName;

                string strIPaddress = null;
                strIPaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (strIPaddress == null)
                {
                    strIPaddress = Request.ServerVariables["REMOTE_ADDR"];
                }

                string EmailList = Settings.ErrorEmailList;

                Exception myException = HttpContext.Current.Server.GetLastError().GetBaseException();
                string mailsubject = "Error in Page for RPO Public Site " + Request.Url.ToString();
                string message = string.Empty;
                //message += "<strong>User: <strong/>" + _user + "<br />";
                message += "<strong>Message</strong><br />" + myException.Message + "<br />";
                message += "<strong>StackTrace</strong><br />" + myException.StackTrace + "<br />";
                message += "<strong>Query String</strong><br />" + Request.QueryString.ToString() + "<br />";
                message += "<strong>IP Address</strong><br />" + strIPaddress.ToString() + "<br />";
                MailMessage myMessage = new MailMessage("support@efxach.com", EmailList, "RPO Public Site Error", message);
                myMessage.IsBodyHtml = true;
                SmtpClient mySmtpClient = new SmtpClient();
                mySmtpClient.Host = Settings.SmtpHost;
                mySmtpClient.Send(myMessage);

            }
        }
    }
}