﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FindProperty.ascx.cs" Inherits="Efx.EfxPublic.UserControls.FindProperty" %>

<div class="formHolder">
                
    <!--**First Row**-->
    <div class="formRow findProperty">
        <!--Property Name Text Box -->
        <div class="frPropertyName">
            <span class="formLable frPage fr4Color">Property Name</span>           
            <asp:TextBox ID="PropertyNameTextBox" runat="server" MaxLength="200" Style="border-style: inset"></asp:TextBox>
        </div>
        <!--or -->
        <div class="orText fr1Color" style="color:#223b67 !important">
            <p style="color:#223b67 !important">or</p>
        </div>
        <!--City Text Box -->
        <div class="FrCity">
            <span class="formLable frPage fr4Color">City</span>
            <asp:TextBox ID="CityTextBox" runat="server" MaxLength="100" Style="border-style: inset"></asp:TextBox>
        </div>
        <!--State Text Box -->
        <!--cakel: BUGID00022 - Hidden state text box to prevent renter from searching entire state-->
        <!-- 
        <div class="frState">
            <span class="formLable">State</span>
            <asp:DropDownList ID="StateDropDown" runat="server" CssClass="chzn-select">
                <asp:ListItem Value ="0" Text="Select a State"></asp:ListItem>
            </asp:DropDownList>
        </div>
            -->
        <!--or -->
        <div class="orText fr1Color" style="color:#223b67 !important">
            <p style="color:#223b67 !important">or</p>
        </div>
        <!--Zip Code Text Box -->
        <div class="frZip">
            <span class="formLable frPage fr4Color">Zip Code</span>
            <asp:TextBox ID="ZipCodeTextBox" runat="server" type="number" MaxLength="10" Style="border-style: inset"></asp:TextBox>

        </div>
        <!-- Search Button -->
        <div>
            <!--cakel: BUGID00022 - Updated button to cause validation true -->
            <asp:Button ID="SearchButton" runat="server" CssClass="orangeButton searchButton fr2BColor" Text="Search" OnClick="SearchButton_Click" ValidationGroup="Val1"></asp:Button>
        </div>
        <!--cakel: BUGID00022 - Added Div with Custom Validator -->
        <div>
    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Must search using more than 3 characters" ForeColor="#FF3300" OnServerValidate="CustomValidator1_ServerValidate" ValidationGroup="Val1"></asp:CustomValidator>
        </div>
        <!--Patrick Whittingham - 4/6/15 - task #000355: Display error -->
        <%if ( (PropertyNameTextBox.Text != "" || CityTextBox.Text != "" || ZipCodeTextBox.Text != "" ) && (PropertyListGrid.Rows.Count < 1) )
          { %>
            <br /> <label style="color:#FF3300;" >Property can't be found, Please try again.</label> 
        <%  }  %>
    </div>
                
    <!--**Second Row**-->
    <!-- This is where the chart will be -->
    <!--cakel: 00498 -->
    <div class="formRow gridHolder">
        <asp:GridView ID="PropertyListGrid" runat="server" CssClass="generalGrid  fr1BColor" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="gridAltRow" HeaderStyle-CssClass="tableHeader">
        <AlternatingRowStyle CssClass="gridAltRow"></AlternatingRowStyle>
            <Columns>
                <asp:TemplateField HeaderText="Property">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PropertyName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("PropertyName") %>'></asp:Label>
                        &nbsp;<asp:Label ID="Label2" runat="server" Text='<%# Bind("PropertyName2") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle CssClass="gridControl" />
                </asp:TemplateField>
                <asp:BoundField HeaderText="City State" DataField="CityState">
                    <ItemStyle CssClass="gridControl" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Zip" DataField="PostalCode">
                    <ItemStyle CssClass="gridControl" />
                </asp:BoundField>
                <asp:HyperLinkField HeaderText="Select" DataNavigateUrlFields="LinkUrl" DataTextField="LinkText" ControlStyle-CssClass="gridControl" >
<ControlStyle CssClass="gridControl"></ControlStyle>
                </asp:HyperLinkField>
            </Columns>
        <HeaderStyle CssClass="tableHeader"></HeaderStyle>
        </asp:GridView>

    </div>

    <!-- Link for help finding your property -->
    <asp:Button ID="CantFindYourPropertyButton" runat="server" CssClass="cantFindProperty fr1Color" OnClick="HelpFindingYourPropertyButtonClicked" Text="Can't Find Your Property" CausesValidation="false"></asp:Button>
    <div class="displayNone">
        <div id="HelpFindingYourProperty">
            <uc1:InformPropertyManagerWindow runat="server" ID="InformPropertyManagerWindow" />
        </div>
    </div>
</div>