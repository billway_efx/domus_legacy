﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResidentRegistration.ascx.cs" Inherits="Efx.EfxPublic.UserControls.ResidentRegistration" %>




<!--**First Row**-->
<div class="formRow">
    <!--First Name Text Box -->
    <div class="formHalfWidth">
        <span class="formLabel fr1Color">First Name</span>
        <asp:TextBox ID="FirstNameTextBox" runat="server" MaxLength="100"></asp:TextBox>
    </div>
    <!--Last Name Text Box -->
    <div class="formHalfWidth">
        <span class="formLabel fr1Color">Last Name</span>
        <asp:TextBox ID="LastNameTextBox" runat="server" MaxLength="100"></asp:TextBox>
    </div>
</div>

<!--**Second Row**-->
<div class="formRow">
    <!--Property Name Text Box -->
    <div class="formFullWidth">
        <span class="formLabel fr1Color">Property Name</span>
        <asp:TextBox ID="PropertyNameTextBox" runat="server" ReadOnly="True"></asp:TextBox>
    </div>
</div>

<asp:Panel ID="ConditionalFields" runat="server" CssClass="formRow">
<!--**Third Row**-->
<div class="formRow">
    <!--Unit Number Text Box -->
    <div class="tabletFormHalfAdjust formOneThirdWidth">
        <span class="formLabel fr1Color">Unit Number</span>
        <asp:TextBox ID="UnitNumberTextBox" runat="server" MaxLength="20"></asp:TextBox>
    </div>
    <!--Resident ID Number Text Box -->
    <div class="tabletFormHalfAdjust formTwoThirdWidth" runat="server" id="ResidentIdDiv">
        <span class="formLabel tipItem fr1Color">Resident ID Number</span>
        
       <!-- <span class="toolTipIcon residentIdNumberTip"></span> -->
        <!--cakel: BUGID00202 added new Link -->
    <%--    <asp:HyperLink ID="RPOidSearchLink" runat="server" CssClass="residentPmsIdTip" Font-Size="22px" Font-Bold="True">&#128269;</asp:HyperLink>--%>
        <asp:HyperLink ID="RPOidSearchLink" runat="server" CssClass="residentPmsIdTip" Font-Size="22px" Font-Bold="True">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/50x50Magnifying_glassSmall.fw.png" Height="20px" Width="20px" />
        </asp:HyperLink>
        <!--cakel 2015_11_04: Updated max text limit to 30 -->
        <asp:TextBox ID="ResidentIdNumberTextBox" runat="server" MaxLength="30"></asp:TextBox>
        
    </div>
</div>
</asp:Panel>

<!--**Fourth Row**-->
<div class="formRow">
    <!--Phone Number Text Box -->
    <div class="formFullWidth">
        <span class="formLabel fr1Color">Phone (optional)</span>
        <asp:TextBox ID="PhoneTextBox" runat="server" type="tel" MaxLength="10"></asp:TextBox>
    </div>
</div>

<!--**Fifth Row**-->
<div class="formRow">
    <!--Email Address Text Box -->
    <div class="formFullWidth">
        <span class="formLabel fr1Color">Email Address (username)</span>
        <asp:TextBox ID="EmailAddressTextBox" runat="server" type="email" MaxLength="100"></asp:TextBox>
    </div>
</div>
                
<!--**Sixth Row**-->
<div class="formRow">
    <!--Password Text Box -->
    <div class="formRow">
        <span class="formLabel fr1Color">Password</span>
        <asp:TextBox ID="PasswordTextBox" runat="server" TextMode="Password" CssClass="formFullWidth"></asp:TextBox>
        <span class="underFormText fr1Color">(6 characters - atleast 1 letter and 1 number) </span>
    </div>
</div>
                
<!--**Seventh Row**-->
<div class="formRow">
    <!--Confirm Password Text Box -->
    <div class="formRow">
        <span class="formLabel fr1Color">Confirm Password</span>
        <asp:TextBox ID="ConfirmPasswordTextBox" runat="server" TextMode="Password" CssClass="formFullWidth"></asp:TextBox>
    </div>
</div>
                
<!--Validators-->
<asp:CustomValidator ID="UnitNumberValidator"
                     Display="None"
                     runat="server"
                     ErrorMessage="Unit Number is required."/>

<asp:CustomValidator ID="ResidentIdNumberValidator"
                     Display="None"
                     runat="server"
                     ErrorMessage="A valid Resident ID is required."/>

<asp:RegularExpressionValidator ID="EmailFormatValidator"
                                ControlToValidate="EmailAddressTextBox"
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                ErrorMessage="Invalid email address."
                                runat="server"
                                Display="None"/>

<asp:RegularExpressionValidator ID="PasswordStrengthValidator"
                                ControlToValidate="PasswordTextBox"
                                ValidationExpression="^.*(?=.{6,})(?=.*[\d])(?=.*[a-zA-Z]).*$"
                                ErrorMessage="Invalid format. <br />Password must be at least 6 digits and contain at least 1 letter and 1 number."
                                runat="server"
                                Display="None"/>

<asp:CompareValidator ID="passwordValidator"
                      ControlToValidate="ConfirmPasswordTextBox"
                      ControlToCompare="PasswordTextBox"
                      runat="server"
                      Type="string"
                      ErrorMessage="The passwords do not match." 
                      Display="None"/>
     
<asp:RequiredFieldValidator ID="FirstNameValidator"
                            ControlToValidate="FirstNameTextBox"
                            runat="server"
                            ErrorMessage="First Name is required."
                            Display="None"/>
<asp:RequiredFieldValidator ID="LastNameValidator"
                            ControlToValidate="LastNameTextBox"
                            runat="server"
                            ErrorMessage="Last Name is required."
                            Display="None"/>
<asp:RequiredFieldValidator ID="EmailValidator"
                            ControlToValidate="EmailAddressTextBox"
                            runat="server"
                            ErrorMessage="Email Address is required" 
                            Display="None"/>
<asp:RequiredFieldValidator ID="PasswordRequiredValidator"
                            ControlToValidate="PasswordTextBox"
                            runat="server"
                            ErrorMessage="Password is required."
                            Display="None"/>
<asp:RequiredFieldValidator ID="PasswordConfirmRequiredValidator"
                            ControlToValidate="ConfirmPasswordTextBox"
                            runat="server"
                            ErrorMessage="Confirm Password is required." 
                            Display="None"/>


<!--**Button Time**-->
<div class="generalButtonHolder">
    <asp:Button ID="SubmitButton" runat="server" CssClass="orangeButton currentResidentsSubmit fr2BColor" Text="Submit" CausesValidation="False"></asp:Button>

</div>
<div class="clearFix"></div>


