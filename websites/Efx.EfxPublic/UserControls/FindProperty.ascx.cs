﻿using System.Web.UI.WebControls;
using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System;
using System.Collections.Generic;
using System.Web.UI;
using RPO;

namespace Efx.EfxPublic.UserControls
{
    public partial class FindProperty : UserControl, IFindPropertyUserControl
    {
        public EventHandler SearchButtonClick { set { SearchButton.Click += value; } }
        public string PropertyName { get { return PropertyNameTextBox.Text; } set { PropertyNameTextBox.Text = value; } }
        public string City { get { return CityTextBox.Text; } set { CityTextBox.Text = value; } }
        public DropDownList State { get { return StateDropDown; } set { StateDropDown.DataSource = value; } }
        public string ZipCode { get { return ZipCodeTextBox.Text; } set { ZipCodeTextBox.Text = value; } }
        public string PropertyNameClientId { get { return PropertyNameTextBox.ClientID; } }
        public string ZipCodeClientId { get { return ZipCodeTextBox.ClientID; } }
        public IInformPropertyManagerWindow InformPmWindow { get { return InformPropertyManagerWindow; } }

        public List<SearchGridProperty> PropertyListDataSource
        {

            set
            {
                //cakel: BUGID00022
                if (Page.IsValid == true)
                {
                    
                    PropertyListGrid.DataSource = value;
            
                    PropertyListGrid.DataBind();
                }
            }

        }
       
        protected void Page_Load(object sender, EventArgs e)
        {
  
       
        }

        protected void HelpFindingYourPropertyButtonClicked(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "test", "<script type='text/javascript'>ShowModal(\"#HelpFindingYourProperty\");</script>", false);
        }

        //cakel: BUGID000133
        protected void SearchButton_Click(object sender, EventArgs e)
        {

            string _propertyName = PropertyNameTextBox.Text;
            string _cityName = CityTextBox.Text;
            string desc = "User Searching for properties- Property: " + _propertyName + " City: " + _cityName;
            string _user = Page.User.Identity.Name.ToString();
            ActivityLog AL = new ActivityLog();
            AL.WriteLog(_user, desc, (short)LogPriority.LogAlways);

        }

        //cakel: BUGID00022 - Added custom validator to make sure renter has at leat 3 characters in any one of the boxes
        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {

            if ((PropertyNameTextBox.Text.Length > 2) || (ZipCodeTextBox.Text.Length > 4) || (CityTextBox.Text.Length > 2))
            {
                args.IsValid = true;
            }
            else
            {

                if (ZipCodeTextBox.Text.Length < 4 && ZipCodeTextBox.Text.Length > 1)
                {
                    CustomValidator1.ErrorMessage = "Zip code must be 5 numbers";
                }
                else
                {
                    CustomValidator1.ErrorMessage = "Must search using more than 3 characters";
                }

                args.IsValid = false;

            }


        }
    }
}