﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayerBillingInformation.ascx.cs" Inherits="Efx.EfxPublic.UserControls.PayerBillingInformation" %>
                    
<!--**First Row**-->
<div class="formRow">
    <!--First Name Text Box -->
    <div class="formHalfWidth">
        <span class="formLable">First Name</span>
        <asp:TextBox ID="FirstNameTextBox" runat="server"></asp:TextBox>
    </div>
    <!--Last Name Text Box -->
    <div class="formHalfWidth">
        <span class="formLable">Last Name</span>
        <asp:TextBox ID="LastNameTextBox" runat="server"></asp:TextBox>
    </div>
</div>

<!--**Second Row**-->
<div class="formRow">
    <!--Billing Address Text Box -->
    <div class="formFullWidth">
        <span class="formLable">Billing Address</span>
        <asp:TextBox ID="BillingAddressTextBox" runat="server"></asp:TextBox>
    </div>
</div>
                    
<!--**Third Row**-->
<div class="formRow">
    <!--City Text Box -->
    <div class="city2">
        <span class="formLable">City</span>
        <asp:TextBox ID="CityTextBox" runat="server"></asp:TextBox>
    </div>
    <!--State Text Box -->
    <div class="frState">
        <span class="formLable">State</span>
        <asp:DropDownList ID="StateDropDownList" runat="server" DataTextField="Abbreviation" DataValueField="StateProvinceId" CssClass="chzn-select"></asp:DropDownList>
    </div>
    <!--Zip Code Text Box -->
    <div class="zip2">
        <span class="formLable">Zip Code</span>
        <asp:TextBox ID="ZipCodeTextBox" runat="server" MaxLength="10"></asp:TextBox>
    </div>
</div>

<asp:RequiredFieldValidator ID="RequiredFirstNameValidation"
                            runat="server"
                            ControlToValidate="FirstNameTextBox"
                            ErrorMessage="Your First Name is Required"
                            Display="None"
                            />

<asp:RequiredFieldValidator ID="RequiredLastNameValidation"
                            runat="server"
                            ControlToValidate="LastNameTextBox"
                            ErrorMessage="Your Last Name is Required"
                            Display="None"
                            />

<asp:RequiredFieldValidator ID="RequiredBillingAddressValidation"
                            runat="server"
                            ControlToValidate="BillingAddressTextBox"
                            ErrorMessage="Your Billing Address is Required"
                            Display="None"
                            />

<asp:RequiredFieldValidator ID="RequiredCityValidation"
                            runat="server"
                            ControlToValidate="CityTextBox"
                            ErrorMessage="Your City is Required"
                            Display="None"
                            />

<asp:RequiredFieldValidator ID="RequiredStateValidation"
                            runat="server"
                            ControlToValidate="StateDropDownList"
                            ErrorMessage="Your State is Required"
                            Display="None"
                            />

<asp:RequiredFieldValidator ID="RequiredZipValidation"
                            runat="server"
                            ControlToValidate="ZipCodeTextBox"
                            ErrorMessage="Your Zip Code is Required"
                            Display="None"
                            />