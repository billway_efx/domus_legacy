﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactForm.ascx.cs" Inherits="Efx.EfxPublic.UserControls.ContactForm" %>

<asp:MultiView ID="ContactMultiView" runat="server" >
    <!-- Default View -->
    <asp:View ID="DefaultView" runat="server">
        <div class="formHolder">
            <span class="formLable fr1Color">Name</span>
            <asp:TextBox ID="NameTextBox" runat="server" CssClass="seventypercent" MaxLength="200"></asp:TextBox>  
            <asp:RequiredFieldValidator ID="NameRequiredValidation" ControlToValidate="NameTextBox" runat="server" ErrorMessage="Your Name is Required" Display="None" />      
            <span class="formLable fr1Color">Email Address</span>
            <asp:TextBox ID="EmailAddressTextBox" runat="server" type="email" CssClass="seventypercent" MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ID="EmailRequiredValidation" ControlToValidate="EmailAddressTextBox" runat="server" ErrorMessage="Email Address is Required" Display="None" />
            <asp:RegularExpressionValidator ID="EmailIsCorrectValidation" ControlToValidate="EmailAddressTextBox" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" runat="server" ErrorMessage="Please Enter a Valid Email Address" Display="None" />
            <asp:Panel ID="ConditionalPhone" runat="server">           
                <span class="formLable fr1Color">Phone Number (optional)</span>
                <asp:TextBox ID="PhoneNumberTextBox" runat="server" type="tel" CssClass="seventypercent" MaxLength="10"></asp:TextBox>
            </asp:Panel>
            <asp:Panel ID="ConditionalProperty" runat="server">
                <span class="formLable fr1Color">Property Name (optional)</span>
                <asp:TextBox ID="PropertyNameTextBox" runat="server" CssClass="seventypercent" MaxLength="200"></asp:TextBox>
            </asp:Panel>
            <span class="formLable fr1Color">Message</span>    
            <textarea class="seventypercent messageBox" ID="MessageTextBox" runat="server" maxlength="4000"></textarea>
            <asp:Button ID="SendButton" runat="server" CssClass="orangeButton contactSend fr2BColor" Text="Send" CausesValidation="False"></asp:Button>
        </div>
    </asp:View>
                
    <!-- Confirmation  View -->
    <asp:View ID="ConfirmationView" runat="server">
        <p class="greenText fr1Color">Thank you for contacting RentPaidOnline.</p>
        <p class="greenText fr1Color">Your message has been sent.</p>
        <div class="contactlogoContainerCentered">
          <!--  <img src="/Images/RentPaidOnlineLogo.png" class="resizedLogo" /> -->
        </div>                    
    </asp:View>
</asp:MultiView>