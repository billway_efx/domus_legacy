﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Web.UI;

namespace Efx.EfxPublic.UserControls
{
    public partial class SelectPaymentType : UserControl, ISelectPaymentType
    {
        private SelectPaymentTypePresenter _Presenter;

        public int RenterId { get; set; }
        public EventHandler PaymentTypeChanged { set { IsCreditCardRadioButtonList.SelectedIndexChanged += value; } }
        public string PaymentTypeSelectedValue { get { return IsCreditCardRadioButtonList.SelectedValue; } set { IsCreditCardRadioButtonList.SelectedValue = value; } }
        public int PaymentTypeSelectedIndex { set { SelectedType.ActiveViewIndex = value; } }
        public ICreditCardDetails CreditCardDetailsControl { get { return CreditCardDetails; } }
        public IElectronicCheckDetails ECheckDetailsControl { get { return ElectronicCheckDetails; } }
        public bool IsApplicant { get; set; }
		public bool AcceptCCPayments { get; set; }
		public bool AcceptACHPayments { get; set; }
		public bool PNMEnabled { get; set; }
		public int PropertyId { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new SelectPaymentTypePresenter(this);

            if (!IsPostBack)
                _Presenter.InitializeValues();
        }

        public void InitializeCreditCardExpiration()
        {
            
        }
    }
}