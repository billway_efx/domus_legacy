﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ElectronicCheckDetails.ascx.cs" Inherits="Efx.EfxPublic.UserControls.PaymentType.ElectronicCheckDetails" %>

<!--If E-check -->
<div id="IfEcheck" >
    <!--**Second Row**-->
    <div class="formRow">
        <!--E-check Number Text Box -->
        <div class="formFullWidth">
            <span class="formLable tipItem">Routing Number</span><span class="toolTipIcon checkNumbersTip"></span>
            <asp:TextBox ID="RoutingNumberTextBox" runat="server" MaxLength="9"></asp:TextBox>
        </div>
    </div>
                    
    <!--**Third Row Row**-->
    <div class="formRow">
        <!--E-check Number Text Box -->
        <div class="formFullWidth">
            <span class="formLable tipItem">Checking Account Number</span><span class="toolTipIcon checkNumbersTip"></span>
            <asp:TextBox ID="AccountNumberTextBox" runat="server" MaxLength="15"></asp:TextBox>
        </div>
    </div>
</div>

<asp:RequiredFieldValidator ID="RoutingNumberRequiredValidation" 
                            ControlToValidate="RoutingNumberTextBox" 
                            runat="server" 
                            ErrorMessage="Routing Number is Required" 
                            Display="None" />

<asp:RequiredFieldValidator ID="AccountNumberRequiredValidation" 
                            ControlToValidate="AccountNumberTextBox" 
                            runat="server" 
                            ErrorMessage="Account Number is Required" 
                            Display="None" />