﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreditCardDetails.ascx.cs" Inherits="Efx.EfxPublic.UserControls.PaymentType.CreditCardDetails" %>

<!--If Credit Card -->
<div id="IfCreditCard">
    
    <!--**First Row**-->
    <div class="formRow">
        <div class="formFullWidth">
            <span class="formLable">Name on Card</span>
            <asp:TextBox ID="NameOnCardTextBox" runat="server"></asp:TextBox>            
        </div>
    </div>    

    <!--**Second Row**-->
    <div class="formRow">
        <!--Credit Card Number Text Box -->
        <div class="formFullWidth">
            <span class="formLable">Credit Card Number</span>
            <asp:TextBox ID="CreditCardNumberTextBox" runat="server" MaxLength="16"></asp:TextBox>
        </div>
    </div>
                    
    <!--**Third Row =**-->
    <div class="formRow">
        <!--Credit Card Number Text Box -->
        <div class="expHolder">
            <span class="formLable">Expiration Date</span>
                <div class="expirationDateWidth">
                    <asp:DropDownList ID="ExpirationMonthDropDown" runat="server" CssClass="chzn-select">
                        <asp:ListItem Value="01" Text="January"></asp:ListItem>
                        <asp:ListItem Value="02" Text="February"></asp:ListItem>
                        <asp:ListItem Value="03" Text="March"></asp:ListItem>
                        <asp:ListItem Value="04" Text="April"></asp:ListItem>
                        <asp:ListItem Value="05" Text="May"></asp:ListItem>
                        <asp:ListItem Value="06" Text="June"></asp:ListItem>
                        <asp:ListItem Value="07" Text="July"></asp:ListItem>
                        <asp:ListItem Value="08" Text="August"></asp:ListItem>
                        <asp:ListItem Value="09" Text="September"></asp:ListItem>
                        <asp:ListItem Value="10" Text="October"></asp:ListItem>
                        <asp:ListItem Value="11" Text="November"></asp:ListItem>
                        <asp:ListItem Value="12" Text="December"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="expirationDateWidth">
                    <asp:DropDownList ID="ExpirationYearDropDown" runat="server" CssClass="chzn-select"></asp:DropDownList>
                </div>
        </div>

        <div class="frCvv">
            <span class="formLable tipItem">CVV</span><span class="toolTipIcon cvvTip"></span>
            <asp:TextBox ID="CvvTextBox" runat="server" MaxLength="4"></asp:TextBox>
        </div>
    </div>
</div>

<asp:RequiredFieldValidator ID="NameOnCardRequiredValidation" 
                            ControlToValidate="NameOnCardTextBox" 
                            runat="server" 
                            ErrorMessage="Name on Card is Required" 
                            Display="None" />

<asp:RequiredFieldValidator ID="CreditCardNumberRequiredValidation" 
                            ControlToValidate="CreditCardNumberTextBox" 
                            runat="server" 
                            ErrorMessage="Credit Card Number is Required" 
                            Display="None" />

<asp:RequiredFieldValidator ID="CVVRequiredValidation" 
                            ControlToValidate="CvvTextBox" 
                            runat="server" 
                            ErrorMessage="CVV is Required" 
                            Display="None"/>