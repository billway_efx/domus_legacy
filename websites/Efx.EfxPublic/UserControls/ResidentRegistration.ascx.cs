﻿using System.Web.UI;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Web.UI.WebControls;

namespace Efx.EfxPublic.UserControls
{
    public partial class ResidentRegistration : System.Web.UI.UserControl, IResidentRegister
    {
        private ResidentRegisterPresenter _Presenter;

        public string FirstName { get { return FirstNameTextBox.Text; } }
        public string LastName { get { return LastNameTextBox.Text; } }
        public string PropertyName { set { PropertyNameTextBox.Text = value; } }
        public string UnitNumber { get { return UnitNumberTextBox.Text; } }
        public string PhoneNumber { get { return PhoneTextBox.Text; } }
        public string Email { get { return EmailAddressTextBox.Text; } }
        public string Password { get { return PasswordTextBox.Text; } }
        public string PasswordConfirm { get { return ConfirmPasswordTextBox.Text; } }
        public EventHandler SubmitClick { set { SubmitButton.Click += value; } }
        public ServerValidateEventHandler UnitNumberServerValidate { set { UnitNumberValidator.ServerValidate += value; } }
        public ServerValidateEventHandler ResidentIdNumberServerValidate { set { ResidentIdNumberValidator.ServerValidate += value; } }
        public string PhoneNumberClientId { get { return PhoneTextBox.ClientID; } }
        public bool AreConditionalFieldsVisible { set { ConditionalFields.Visible = value; } }
		public bool DisplayResidentId { get; set; }

        public string ResidentIdNumber
        {
            get
            {
                return ResidentIdNumberTextBox.Text;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new ResidentRegisterPresenter(this);
			ResidentIdDiv.Visible = DisplayResidentId;
            if (!Page.IsPostBack)
                _Presenter.InitializeValues();

            string PropertyString = Request.QueryString["PropId"];
            RPOidSearchLink.NavigateUrl = "~/CurrentResidentRPOidSearch.aspx?PropId=" + PropertyString;
            string IntegratedCheck = EfxFramework.Property.GetIntegrationStatus(Int32.Parse(PropertyString));

            if (IntegratedCheck != "Non-Integrated")
            {
                RPOidSearchLink.Visible = true;
            }
            else
            {
                RPOidSearchLink.Visible = false;
            }
            


            //cakel: BUGID00202
            string RPOID = Request.QueryString["RPOID"];
            string fn = Request.QueryString["fn"];
            string ln = Request.QueryString["ln"];
            if (RPOID != null)
            {
                FirstNameTextBox.Text = fn.ToString();
                LastNameTextBox.Text = ln.ToString();
                ResidentIdNumberTextBox.Text = RPOID.ToString();

            }



        }
    }
}