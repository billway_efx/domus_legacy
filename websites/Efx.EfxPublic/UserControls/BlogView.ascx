﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogView.ascx.cs" Inherits="Efx.EfxPublic.UserControls.BlogView" %>

<div class="blogContainer">
    
    <article class="blogPost">
        <div id="PostTitle" class="postTitle fr1Color"></div>
        <div id="PostDate" class="postDate fr1Color"></div>
        <div id="PostText" class="postText fr1Color" style="line-height: 1.2em !important; color:#223b67 !important"></div>
    </article>
    
    <div class="blogNavigation">
        <section class="recentPostsSection">
            <h4 class="fr1Color">Recent Posts</h4>
            <uc1:BlogPostListing ID="RecentBlogPosts" runat="server"/>
        </section>
        
        <section class="archivesSection">
            <h4 class="fr1Color">Archives</h4>
            <asp:ListView ID="ArchiveListView" runat="server">
                <LayoutTemplate>
                    <ul style="list-style-type: none;">
                        <li id="itemPlaceholder" runat="server"></li>
                    </ul>
                </LayoutTemplate>
    
                <ItemTemplate>
                    <li>
                        <%--inline styles below.... look out!--%>
                        <a id="ArchiveYearLink" runat="server" style="color: #0462c2 !important; font-size: .8em; line-height: 1.8em;"><asp:Label ID="ArchiveYear" runat="server" Text=<%# Bind("Year", "{0:MMM yyyy}") %> /></a>
                        <asp:Label ID="Label1" runat="server" style="color: #0462c2 !important; font-size: .8em;" Text=<%# Bind("Count","({0})") %> />
                        <div id="HidePost" runat="server" style="display: none;">
                            <uc1:BlogPostListing ID="ArchiveBlogPosts" runat="server"/>
                        </div>
                        <asp:HiddenField ID="PostState" runat="server" Value="false" />
                    </li>
                </ItemTemplate>
                <EmptyItemTemplate>
                    <asp:Label ID="EmptyItemLabel" runat="server" Text="No Items to Display" />
                </EmptyItemTemplate>
            </asp:ListView>
        </section>        
    </div>

    <script type="text/javascript">
        function ShowPostData(text, date, title) {
            var postText = document.getElementById("PostText");
            postText.innerHTML = text;
            var postDate = document.getElementById("PostDate");
            postDate.innerHTML = date;
            var postTitle = document.getElementById("PostTitle");
            postTitle.innerHTML = title;
        }
    </script>
</div>
