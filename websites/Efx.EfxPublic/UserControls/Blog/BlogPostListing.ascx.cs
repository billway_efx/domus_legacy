﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls.Blogs;
using EfxFramework.ViewPresentation.Presenters.UserControls.Blogs;

namespace Efx.EfxPublic.UserControls.Blog
{
    public partial class BlogPostListing : UserControl, IBlogPostListing
    {
        private BlogPostListingPresenter _Presenter;

        public ListView BlogPostList { get { return BlogPostListView; } }
        public EventHandler<ListViewItemEventArgs> BlogPostItemDataBound { set { BlogPostListView.ItemDataBound += value; } }

        protected override void OnInit(EventArgs e)
        {
            _Presenter = new BlogPostListingPresenter(this);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public void WireUpDataBinding()
        {
            _Presenter.WireUpDataBinding();
        }
    }
}