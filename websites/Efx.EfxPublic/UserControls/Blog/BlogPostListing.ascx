﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogPostListing.ascx.cs" Inherits="Efx.EfxPublic.UserControls.Blog.BlogPostListing" %>
<asp:ListView ID="BlogPostListView" runat="server">
    <LayoutTemplate>
        <ul style="list-style-type: none;">
            <li id="itemPlaceholder" runat="server"></li>
        </ul>
    </LayoutTemplate>
    <ItemTemplate>
        <li>


            <!--cakel:  -->
            <a id="PostTitleLink" runat="server" class="fr1Color" style="cursor: pointer;"><asp:Label ID="PostLinkLabel" style="line-height: 1.8em; font-size: .8em;" runat="server" Text='<%# Bind("BlogTitle") %>' /></a>
            <asp:Literal ID="AnnouncementText" runat="server" Text='<%# Bind("BlogText") %>' Visible="false" />
            <asp:Literal ID="AnnouncementDateShortDate" runat="server" Text='<%# Bind("AnnouncementDateShortDate") %>' Visible="false" />
            <asp:Literal ID="AnnouncementTitle" runat="server" Text='<%# Bind("BlogTitle") %>' Visible="false" />

            

            <%--Inline styles below.... look out!--%>
           <%-- <a id="PostTitleLink" runat="server" style="cursor: pointer;"><asp:Label ID="PostLinkLabel" runat="server" Text='<%# Bind("BlogTitle") %>' /></a>--%>
            <!--cakel -->
<%--            <asp:Literal ID="AnnouncementText" runat="server"   Text='<span class="fr1Color"><%# Bind("BlogText") %></span>' Visible="false" />
            <asp:Literal ID="AnnouncementDateShortDate" runat="server"  Text='<span class="fr1Color"><%# Bind("AnnouncementDateShortDate") %></span>' Visible="false" />
            <asp:Literal ID="AnnouncementTitle" runat="server"  Text='<span class="fr1Color"><%# Bind("BlogTitle") %></span>' Visible="false" />--%>
    
        </li>
    </ItemTemplate>
    <EmptyItemTemplate>
        <asp:Label ID="EmptyItemLabel" runat="server" Text="No Items to Display" />
    </EmptyItemTemplate>
</asp:ListView>