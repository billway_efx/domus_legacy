﻿using System;
using System.Web.UI;
using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;

namespace Efx.EfxPublic.UserControls.Modal
{
    public partial class ForgotPasswordWindow : System.Web.UI.UserControl, IForgotPassword
    {
        public string Email { get { return EmailTextBox.Text; } }
        public EventHandler SubmitClick { set { SubmitButton.Click += value; } }
        public Page ParentPage { get { return Page; } }

        private ForgotPasswordPresenter<Applicant> _Presenter;

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new ForgotPasswordPresenter<Applicant>(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}