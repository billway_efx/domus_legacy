﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SignupWindow.ascx.cs" Inherits="Efx.EfxPublic.UserControls.Modal.SignupWindow" %>

<div class="modalContentContainer">
    <div class="signUpContainer">
         <h3>Signup</h3>
        <h4>Are you a current resident?</h4>
        <div class="yesNoButtonContainer">
            <asp:Button ID="YesButton" runat="server" CssClass="orangeButton buttonOverride" BorderStyle="None" Text="Yes" CausesValidation="False" Width="60px" UseSubmitBehavior="false"></asp:Button>
            <!--cakel: BUGID00098 updated cssClass code to "orangeButton buttonOverride" from "orangeButton"   -->
            <asp:Button ID="NoButton" runat="server" CssClass="orangeButton buttonOverride" BorderStyle="None" Text="No" CausesValidation="False" Width="60px" UseSubmitBehavior="false"></asp:Button>
        </div>
    </div>
</div>