﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework;
using EfxFramework.BulkMail;
using System.Net.Mail;
using EfxFramework.Resources;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.Web;

namespace Efx.EfxPublic.UserControls.Modal
{
    public partial class InformPropertyManagerWindow : UserControl, IInformPropertyManagerWindow
    {
        public string OfficePhoneClientId { get { return OfficePhoneTextBox.ClientID; } }
        protected void Page_Load(object sender, EventArgs e)
        {
         
        }

        protected void SendEmail()
        {
            if (!BasePageV2.ValidatePage(this.Page))
                return;

            var CheckedItems = RequestedInfoCheckBoxList.Items.Cast<ListItem>().Where(li => li.Selected).Aggregate(String.Empty, (current, Li) => String.Format("{0}, and {1}", current, Li.Text));

            if (!String.IsNullOrEmpty(CheckedItems))
            {
                CheckedItems = CheckedItems.Substring(6);
            }
            

            BulkMail.SendMailMessage(null, "EFX", false, Settings.SendMailUsername, Settings.SendMailPassword, new MailAddress(Settings.ContactUsEmail), "Re: Your Request to Inform Your Property Manager",
                    String.Format(EmailTemplates.InformPropertyManagerConfirmation, FirstNameTextBox.Text), new List<MailAddress> {new MailAddress(EmailTextBox.Text) });

            BulkMail.SendMailMessage(null, "EFX", false, Settings.SendMailUsername, Settings.SendMailPassword, new MailAddress(Settings.ContactUsEmail), "Information about RentPaidOnline",
                    String.Format(EmailTemplates.InformPropertyManagerPmc, PropertyNameTextBox.Text), new List<MailAddress> { new MailAddress(OfficeEmailTextBox.Text) });

            BulkMail.SendMailMessage(null, "EFX", false, Settings.SendMailUsername, Settings.SendMailPassword, new MailAddress(Settings.ContactUsEmail), "Request from Inform My Property Manager Form",
                    String.Format(EmailTemplates.InformPropertyManagerRpo, PropertyNameTextBox.Text, OfficePhoneTextBox.Text, OfficeEmailTextBox.Text, FirstNameTextBox.Text, LastNameTextBox.Text,
                    EmailTextBox.Text, CheckedItems, ""), new List<MailAddress> { new MailAddress(Settings.ContactUsEmail) });
        }

        protected void GenerateLead()
        {
            var NewLead = new Lead
            {
                FirstName = FirstNameTextBox.Text,
                LastName = LastNameTextBox.Text,
                EmailAddress = EmailTextBox.Text,
                Description = "Lead generated from Inform My Property Manager Request",
                DateSubmitted = DateTime.Now,
                PhoneNumber = PhoneNumber.RemoveFormatting(OfficePhoneTextBox.Text),
                PropertyName = PropertyNameTextBox.Text,
                LeadTypeId = 1
            };

            Lead.Set(NewLead);
        }

        protected void SendButtonClick(object sender, EventArgs e)
        {
            SendEmail();
            GenerateLead();
            Clear();
        }

        protected void Clear()
        {
            FirstNameTextBox.Text = String.Empty;
            LastNameTextBox.Text = String.Empty;
            EmailTextBox.Text = String.Empty;
            OfficeEmailTextBox.Text = String.Empty;
            OfficePhoneTextBox.Text = String.Empty;
            PropertyNameTextBox.Text = String.Empty;
        }

    }
}