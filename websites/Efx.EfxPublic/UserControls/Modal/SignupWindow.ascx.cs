﻿using EfxFramework;
using System;

namespace Efx.EfxPublic.UserControls.Modal
{
    public partial class SignupWindow : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            YesButton.PostBackUrl = Settings.CurrentResidentSignupLink;
            NoButton.PostBackUrl = Settings.FutureResidentSignupLink;
        }
    }
}