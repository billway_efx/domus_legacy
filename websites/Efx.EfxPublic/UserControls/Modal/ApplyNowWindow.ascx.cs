﻿using System.Web.UI.WebControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.UserControls;
using System;
using System.Web.UI;

namespace Efx.EfxPublic.UserControls.Modal
{
    public partial class ApplyNowWindow : UserControl, IApplyNowWindow
    {
        private ApplyNowWindowPresenter _Presenter;

        public EventHandler SendButtonClick { set { SendButton.Click += value; } }
        public string ToText { get { return ToTextBox.Text; } set { ToTextBox.Text = value; } }
        public string FromText { get { return FromTextBox.Text; } set { FromTextBox.Text = value; } }
        public string SubjectText { get { return SubjectTextBox.Text; } set { SubjectTextBox.Text = value; } }
        public string BodyText {get { return BodyTextBox.InnerText; } set { BodyTextBox.InnerText = value; }}
        public FileUpload FileUploadControl { get { return ResumeUpload; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new ApplyNowWindowPresenter(this);
        }
    }
}