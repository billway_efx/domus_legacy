﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InformPropertyManagerWindow.ascx.cs" Inherits="Efx.EfxPublic.UserControls.Modal.InformPropertyManagerWindow" %>

<div class="modalContentContainer informPmContainer">
    <h3 class="fr1Color">Please Inform My Property Manager</h3>
    <h4 class="fr1Color">Couldn't find your property in the list?</h4><p class="fr1Color">Send your Property Manager a message letting him/her know that you would like to pay your rent using RentPaidOnline.</p>
    <div class="modalFormHolder">
            <div class="formRow modalRow">
        <div class="formEvenThirdWidth">
            <span class="formLable fr1Color">Property Name</span>
            <asp:TextBox ID="PropertyNameTextBox" runat="server" CssClass="formFullWidth" MaxLength="200"></asp:TextBox>
            <asp:RequiredFieldValidator ID="PropertyNameRequiredValidation" CssClass="required" ControlToValidate="PropertyNameTextBox" runat="server" ErrorMessage="Required" Display="Dynamic"/> 
        </div> 
        <div class="formEvenThirdWidth">
            <span class="formLable fr1Color">Property Office Phone</span>
            <asp:TextBox ID="OfficePhoneTextBox" runat="server" CssClass="formFullWidth" MaxLength="10" onkeypress="return Numbers();"></asp:TextBox>
        </div>
        <div class="formEvenThirdWidth">
            <span class="formLable fr1Color">Property Office Email</span>
            <asp:TextBox ID="OfficeEmailTextBox" runat="server" type="email" CssClass="formFullWidth"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredOfficeEmail" CssClass="required" ControlToValidate="OfficeEmailTextBox" runat="server" ErrorMessage="Required" Display="Dynamic" />
            <asp:RegularExpressionValidator ID="ValidOfficeEmail" CssClass="required" ControlToValidate="OfficeEmailTextBox" runat="server" ErrorMessage="Need Valid Email" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
        </div>
    </div>
    <div class="formRow modalRow">       
         <div class="formEvenThirdWidth">
            <span class="formLable fr1Color">Your First Name</span>
            <asp:TextBox ID="FirstNameTextBox" runat="server" CssClass="formFullWidth" MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ID="FirstNameRequiredValidation" CssClass="required" ControlToValidate="FirstNameTextBox" runat="server" ErrorMessage="Required" Display="Dynamic" />
        </div>
        <div class="formEvenThirdWidth">
            <span class="formLable fr1Color">Your Last Name</span>
            <asp:TextBox ID="LastNameTextBox" runat="server" CssClass="formFullWidth" MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ID="LastNameRequiredValidation" CssClass="required" ControlToValidate="LastNameTextBox" runat="server" ErrorMessage="Required" Display="Dynamic" />
        </div>
        <div class="formEvenThirdWidth">
            <span class="formLable fr1Color">Your Email</span>
            <asp:TextBox ID="EmailTextBox" runat="server" type="email" CssClass="formFullWidth" MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ID="EmailRequiredValidation" CssClass="required" ControlToValidate="EmailTextBox" runat="server" ErrorMessage="Required" Display="Dynamic" />
            <asp:RegularExpressionValidator ID="ValidEmailAddress" CssClass="required" ControlToValidate="EmailTextBox" runat="server" ErrorMessage="Need Valid Email" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
        </div>

    </div>
    <div class="formRow">
        <div class="ninetyFullWidth">
            <asp:CheckBoxList ID="RequestedInfoCheckBoxList" runat="server" CssClass="checkBoxText">
                <asp:ListItem class="fr1Color checkbox">Contact Me When My Property is Participating</asp:ListItem>
                <asp:ListItem class="fr1Color checkbox">Send Me a List of Participating Properties in my Zip Code.</asp:ListItem>
            </asp:CheckBoxList>
        </div>
    </div>
    </div>

    <div class="generalButtonHolder">
        <asp:Button ID="SendButton" runat="server" CssClass="orangeButton sendButton fr2BColor" Text="Send" OnClick="SendButtonClick"></asp:Button>
    </div>
  <div class="clearFix"></div>
</div>
<div class="clearFix"></div>

 <script type="text/ecmascript">
     function Numbers(evt) {
         var e = event || evt; // for trans-browser compatibility
         var charCode = e.which || e.keyCode;

         if (charCode > 31 && (charCode < 48 || charCode > 57))
             return false;

         return true;
     }
</script>