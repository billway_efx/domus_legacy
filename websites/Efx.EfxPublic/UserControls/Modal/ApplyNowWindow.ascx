﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApplyNowWindow.ascx.cs" Inherits="Efx.EfxPublic.UserControls.Modal.ApplyNowWindow" %>

<div class="modalContentContainer">
    <h3 class="fr1Color">Apply to work for RentPaidOnline</h3>
    <div class="fullrow">
        <span class="formLable">To</span>
        <asp:TextBox ID="ToTextBox" runat="server" ReadOnly="True" CssClass="ninetyFullWidth"></asp:TextBox>
    </div>
    <div class="fullrow">
        <span class="formLable">From</span>
        <asp:TextBox ID="FromTextBox" runat="server" CssClass="ninetyFullWidth"></asp:TextBox>
    </div>
    <div class="fullrow">
        <span class="formLable">Subject</span>
        <asp:TextBox ID="SubjectTextBox" runat="server" ReadOnly="True" CssClass="ninetyFullWidth"></asp:TextBox>      
    </div>
    <div class="fullrow">
        <span class="formLable">Message</span>
        <textarea ID="BodyTextBox" class="ninetyFullWidth messageBox" runat="server"></textarea>       
    </div>
    <div class="fullrow uploadResume">
        <span class="formLable">Upload Resume</span>      
        <asp:FileUpload ID="ResumeUpload" runat="server" />        
    </div>
    
    <div class="generalButtonHolder">
        <asp:Button ID="SendButton" runat="server" Text="Send" CssClass="orangeButton fr2BColor"></asp:Button>
    </div>
    <div class="clearFix"></div>
</div>

<asp:RequiredFieldValidator ID="FromRequiredValidation" 
                            ControlToValidate="FromTextBox" 
                            runat="server" 
                            Text="Your Email Address is Required" 
                            Display="None" 
                            SetFocusOnError="True" />

<asp:RegularExpressionValidator ID="EmailIsCorrectValidation" 
                                ControlToValidate="FromTextBox" 
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                runat="server" 
                                Display="Static" 
                                Text="Please Enter a Valid Email Address" />