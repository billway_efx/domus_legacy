﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectPaymentType.ascx.cs" Inherits="Efx.EfxPublic.UserControls.SelectPaymentType" %>

<!--**First Row**-->
<div class="formRow">
    <!--Radio Button list -->
    <asp:RadioButtonList ID="IsCreditCardRadioButtonList" runat="server" CssClass="radioButtonList" AutoPostBack="True">
        <asp:listItem Value="1" Selected="True" Text="Credit Card"></asp:listItem>
        <asp:listItem Value="2" Text="E-Check"></asp:listItem>
    </asp:RadioButtonList>
</div>
<asp:MultiView ID="SelectedType" runat="server">
    <asp:View ID="SelectCreditCard" runat="server">
        <uc1:CreditCardDetails runat="server" ID="CreditCardDetails" />
    </asp:View>
    <asp:View ID="SelectElectronicCheck" runat="server">
        <uc1:ElectronicCheckDetails runat="server" ID="ElectronicCheckDetails" />
    </asp:View>
</asp:MultiView>


