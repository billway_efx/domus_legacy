﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.ViewPresentation.Presenters.Pages;

namespace Efx.EfxPublic.UserControls
{
    public partial class PayerBillingInformation : UserControl, IPayerBillingInformation
    {
        private PayerBillingInformationPresenter _Presenter;

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new PayerBillingInformationPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }

        public string FirstNameText{ get { return FirstNameTextBox.Text; } }
        public string LastNameText { get { return LastNameTextBox.Text; } }
        public string BillingAddressText { get { return BillingAddressTextBox.Text; } }
        public string CityText { get { return CityTextBox.Text; } }
        public DropDownList StateDropDown { get { return StateDropDownList; } }
        public string ZipText { get { return ZipCodeTextBox.Text; } }
        public string ZipCodeClientId { get { return ZipCodeTextBox.ClientID; } }
    }
}