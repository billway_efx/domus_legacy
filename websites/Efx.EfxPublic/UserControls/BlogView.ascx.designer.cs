﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Efx.EfxPublic.UserControls {
    
    
    public partial class BlogView {
        
        /// <summary>
        /// RecentBlogPosts control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Efx.EfxPublic.UserControls.Blog.BlogPostListing RecentBlogPosts;
        
        /// <summary>
        /// ArchiveListView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListView ArchiveListView;
    }
}
