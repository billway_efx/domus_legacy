﻿using EfxFramework;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Web;
using System.Web.UI;

namespace Efx.EfxPublic
{
    public partial class Site : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string VersionNumber = "?version=" + EfxFramework.Settings.versionNumber.ToString();

            // Patrick Whittingham - 5/1/15 : DYNAMIC CSS
            chosenCSS.Href = EfxFramework.Settings.sharedCssUrl + "/chosen.css" + VersionNumber;
            modalCSS.Href = EfxFramework.Settings.sharedCssUrl + "/Modal.css" + VersionNumber;

            //cakel: 06/22/2015 - Add dynamic code to get new CSS when version changes
            DefaultCSS.Href = "/Styles/Default.css" + VersionNumber;
            ResponsiveCSS.Href = "/Styles/Responsive.css" + VersionNumber;
            FontAwesomeCSS.Href = "/Styles/font-awesome.min.css" + VersionNumber;
            PublicFavicon.Href = "/RPO-favicon-Blue.ico" + VersionNumber;

            PropertyManagerLoginLink.HRef = EfxFramework.Settings.PropertyManagerLoginLink;
            RenterLoginLink1.HRef = EfxFramework.Settings.RenterLoginLink;
            RenterLoginLink2.HRef = EfxFramework.Settings.RenterLoginLink;

            litYear.Text = DateTime.Now.Year.ToString(System.Globalization.CultureInfo.InvariantCulture);

			int? ApplicantId = null;
			if (HttpContext.Current.User != null)
			{
				ApplicantId = HttpContext.Current.User.Identity.Name.ToNullInt32();
			}

            if (!ApplicantId.HasValue || ApplicantId.Value < 1)
            {
                //LogoutLink.Visible = false;
                return;
            }

            if (ApplicantId > 1)
            {
                //LogInButton.Visible = false;
                //SignUpButton.Visible = false;
            }

            var Applicant = new Applicant(ApplicantId);

            if (Applicant.ApplicantId < 1)
            {
                //LogoutLink.Visible = false;
                //LogInButton.Visible = true;
                //SignUpButton.Visible = true;
                BasePageV2.SafePageRedirect("~/Account/LogOut.aspx");
                return;
            }

            string PageName = System.IO.Path.GetFileName(HttpContext.Current.Request.FilePath);
            //if(PageName == "")


            FutureResidentLinkVisibility(Applicant);
            //WelcomeMessageText.Text = String.Format("Welcome, {0} ", Applicant.FirstName);
        }

        protected void SignUpButtonClicked(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "test", "<script type='text/javascript'>ShowModal(\"#SignUp\");</script>", false);
        }

        protected void FutureResidentLinkVisibility(Applicant applicant)
        {
            var PropertyId = applicant.Applications.Count > 0 ? applicant.Applications[0].PropertyId : 0;

            //FutureResidentsDownloadApplicationLink.NavigateUrl = String.Format("~/FutureResidentsDownloadApplication.aspx?PropId={0}&ApplicantId={1}", PropertyId, applicant.ApplicantId);
            //FutureResidentsDownloadApplicationLink.Visible = true;
            //FutureResidentsUploadApplicationLink.NavigateUrl = String.Format("~/FutureResidentsUploadApplications.aspx?PropId={0}&ApplicantId={1}", PropertyId, applicant.ApplicantId);
            //FutureResidentsUploadApplicationLink.Visible = true;
            //FutureResidentsStatusLink.NavigateUrl = String.Format("~/FutureResidentsStatus.aspx?ApplicantId={0}", applicant.ApplicantId);
            //FutureResidentsStatusLink.Visible = true;
            //PayYourFeesLink.NavigateUrl = String.Format("~/FutureResidentsUploadApplications.aspx?PropId={0}&ApplicantId={1}", PropertyId, applicant.ApplicantId);
            //.Visible = true;
        }
    }
}