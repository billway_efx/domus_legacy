﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="CurrentResidentRPOidSearch.aspx.cs" Inherits="Efx.EfxPublic.CurrentResidentRPOidSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            

            <!--cakel: BUGID00202 - Added New Page to handle RPO ID look up***** -->
    <div id="pageCenterContainer" class="pageContainer crRPOSearch residentsPage">
     <div class="crRPOSearch column1" style="width:25%; padding-top:40px; border:none; ">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="SearchRPOID" Font-Size="18px" ForeColor="#FF6600" /> 
     </div>
      <div class="crRPOSearch column2">
          <div id="error">
             <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="SearchRPOID" Font-Size="18px" ForeColor="#FF6600" />
          </div>
          <div class="formHolder formContainer">
      <h3 class=" fr1Color" style="padding-top:20px;">Search for Your RPO ID</h3>
         <p class=" fr1Color" style="padding-bottom:30px;">Please provide the primary lease holder's First Name, Last Name and confirm the rent amount</p>

         <div class="formRow">

             <div class="formFullWidth">
                 <span class="formLabel fr1Color">First Name: </span>
                 <asp:TextBox ID="FirstNametxt" runat="server" MaxLength="70"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="FirstNametxt" ErrorMessage="First Name is Required" ValidationGroup="SearchRPOID" Font-Bold="True" ForeColor="#FF6600" Font-Size="22px" Text="*"></asp:RequiredFieldValidator>
                 <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="FirstNametxt" ErrorMessage="*" ValidationExpression="^[a-zA-Z]{2,50}$" Font-Bold="True" ForeColor="#FF6600" Font-Size="22px" ValidationGroup="SearchRPOID" Text="*"></asp:RegularExpressionValidator>
             </div>
         </div>

         <div class="formRow">

             <div class="formFullWidth">
                 <span class="formLabel fr1Color">Last Name: </span>
                 <asp:TextBox ID="LastNametxt" runat="server" MaxLength="70"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="LastNametxt" ErrorMessage="Last Name is required" ValidationGroup="SearchRPOID" Font-Bold="True" ForeColor="#FF6600" Font-Size="22px" Text="*"></asp:RequiredFieldValidator>
                  <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="LastNametxt" ErrorMessage="*" ValidationExpression="^[a-zA-Z]{2,50}$" Font-Bold="True" ForeColor="#FF6600" Font-Size="22px" ValidationGroup="SearchRPOID" Text="*"></asp:RegularExpressionValidator>
             </div>
         </div>

         <div class="formRow">

             <div class="formFullWidth">
                 <span class="formLabel fr1Color">Rent Amount: </span>
                 <asp:TextBox ID="RentAmounttxt" runat="server" MaxLength="10"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="RentAmounttxt" ErrorMessage="Rent Amount is required" ValidationGroup="SearchRPOID" Font-Bold="True" ForeColor="#FF6600" Font-Size="22px" Text="*"></asp:RequiredFieldValidator>
                 <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Numbers only for Rent Amount" ValidationExpression="[0-9]+(\.[0-9][0-9]?)?" ControlToValidate="RentAmounttxt" Font-Bold="True" ForeColor="#FF6600" Font-Size="22px" ValidationGroup="SearchRPOID" Text="*"></asp:RegularExpressionValidator>
             </div>
         </div>
         <asp:Button ID="Button1" runat="server" style="margin-left: 0; " CssClass="orangeButton fr2BColor" Text="Search" OnClick="Button1_Click" ValidationGroup="SearchRPOID" />
              </div>

          <div class="formRow gridHolder">
              <asp:Label ID="Resultlbl" runat="server"></asp:Label>
              <asp:GridView ID="RPOIDSearchResultGrid" runat="server" CssClass="generalGrid" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="gridAltRow" HeaderStyle-CssClass="tableHeader">
                <AlternatingRowStyle CssClass="gridAltRow"></AlternatingRowStyle>
                  <Columns>
                      <asp:BoundField DataField="FirstName" HeaderText="First Name" >
                      <ItemStyle CssClass="gridControl"/>
                      </asp:BoundField>
                      <asp:BoundField DataField="LastName" HeaderText="Last Name" >
                      <ItemStyle CssClass="gridControl"/>
                      </asp:BoundField>
                      <asp:BoundField DataField="RentAmount" HeaderText="Rent Amount" DataFormatString="{0:c}" >
                      <ItemStyle CssClass="gridControl" />
                      </asp:BoundField>
                      <asp:BoundField DataField="PmsID" HeaderText="RPO ID" />
                      <asp:TemplateField ShowHeader="False">
                          <ItemTemplate>
                              <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "~/CurrentResidentsRegister.aspx?PropID=" + Request.QueryString["PropID"] + "&amp;RPOID=" + Eval("PmsID").ToString() + "&amp;fn=" + Eval("FirstName").ToString() + "&amp;ln=" + Eval("LastName").ToString() %>'>Select</asp:HyperLink>
                          </ItemTemplate>
                      </asp:TemplateField>
                  </Columns>

                <HeaderStyle CssClass="tableHeader"></HeaderStyle>
              </asp:GridView>
          </div>
         </div>
     </div>

                    </ContentTemplate>
    </asp:UpdatePanel>



    <script>

        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
            }
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);
        } // end of calc....


        $(document).ready(function () {
            /* */
            var vObj = document.getElementById("header");
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "150px");

            var vHeight = "height: " + ($("#pageCenterContainer").height() + 200) + "px !important";
            //$('.wrapper').attr('style', vHeight);
            vHeight = "position: absolute; top: " + ($("#pageCenterContainer").height() + 200) + "px";
            //$('#footer').attr('style', vHeight); 

            calc_container();
        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);


        // set menu to go back to default page
        $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

            evt.preventDefault();

            var vHash = $(this).attr('href');
            window.location.href = "/Default.aspx" + vHash;

        });
    </script>
</asp:Content>