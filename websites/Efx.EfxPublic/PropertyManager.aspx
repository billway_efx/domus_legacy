﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PropertyManager.aspx.cs" Inherits="Efx.EfxPublic.PropertyManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>
    <div id="pageCenterContainer" class="pageContainer pmPage  residentsPage">
        <div class="pageTitle frPageTitle fr1Color" style="border-color: white;"><h2 class="pmPage">Property Manager</h2></div>
        
        <!-- General info section with 4 icons and paragraphs -->
            <!--** Row 1 **-->
            <div class="fullrow">
                <!-- Integrations Icon-->            
                <aside class="iconSection textCenter">
                    <div class="integrationsIcon centeredMargin"></div>
                    <h4 id="fr1Color">Integrations</h4>
                </aside>
        
                <!-- Integrations text-->            
                    <article class="iconTextBlock ">
                        <p class="frBlack" >RentPaidOnline’s philosophy is clear: simplify payment processing and seamlessly integrate those transactions into your property management software. 
                            With RentPaidOnline’s cutting edge innovations, we are providing you the most innovative and secure technologies available. With near real-time integrations, 
                            residents will never have to worry about inaccurate balances or missed deadlines.</p>
                    </article>
            </div>
        
            <!--** Row 2 **-->
            <div class="fullrow">
            <!-- Email Marketing Icon-->            
            <aside class="iconSection textCenter">
                <div class="emailMarketingIcon centeredMargin"></div>
                <h4 class="fr1Color" >Marketing Collateral</h4>
            </aside>
            <!-- Email Marketing text-->
                <article class="iconTextBlock ">
                     <p class="frBlack" >RentPaidOnline offers a revolutionary integrated marketing engine to ensure you get the adoption you have come to expect. With multi-family marketing needs in mind, 
                         RentPaidOnline has developed an on-demand system that puts marketing efforts at your fingertips. Our system gives you the flexibility to tailor your approach to meet 
                         the needs of your residents. </p>
                </article>
                
             </div>
        
             <!--** Row 3 **-->
            <div class="fullrow">
            <!-- Advanced Reporting Icon-->            
            <aside class="iconSection textCenter">
                <div class="advancedReportingIcon centeredMargin"></div>
                <h4 class="fr1Color" >Advanced Reporting</h4>
            </aside>
            <!-- Advanced Reporting text-->
                <article class="iconTextBlock ">
                     <p class="frBlack" >Frustrated with not being able to reconcile your bank statement and outdated reporting functionality? Our customized approach to reporting clearly separates us from our competitors. 
                         RentPaidOnline has designed its reports to encompass the business needs of your organization without sacrificing efficiency or flexibility.</p>
                </article>
                
             </div>
            
            <!--** Row 4 **-->
            <div class="fullrow">
                    <!-- Streamline Payments Icon-->            
                    <aside class="iconSection textCenter">
                        <div class="streamlinePaymentsIcon centeredMargin"></div>
                        <h4 class="fr1Color"  >Streamline Payments</h4>
                    </aside>
                    <!-- Streamline Payments text-->
                        <article class="iconTextBlock frBlack">
                             <p class="frBlack">With RentPaidOnline, rent week will seem like any other week. We offer a full scale solution that will allow you streamline your receivables by either utilizing one of our many integrations, 
                                 leveraging our advanced Application Program Interface, or using our industry-first technologies to process payments</p>
                        </article>
                
                     </div>
        
        <!-- Two Column section under the general information -->
        <div class="columnHolder">
            <div class="column1">
                <h3 class="blue">How It Works</h3>
                <p class="frBlack">Here is an overview of how our system works.</p>
                <div class="column50">
                    <p class="orangeOutline centeredMargin width200 fr1Color ">Sign up for RentPaidOnline service through your RPO Sales Representative</p>
                    <div class="arrowDown"></div>
                    
                    <p class="orangeOutline centeredMargin width200 fr1Color ">Login with the user ID and password provided to you</p>
                    <div class="arrowDown"></div>
                    
                    <p class="orangeOutline centeredMargin width200 fr1Color ">Enter Your properties into the system via RentPaidOnline</p>
                    <div class="arrowDown"></div>
                    
                    <p class="orangeOutline centeredMargin width200 fr1Color ">Enter Your Residents’ info into the system<br /> OR<br /> Resident self-register</p>
                    <div class="arrowDown"></div>
                    
                    <p class="orangeOutline centeredMargin width200 fr1Color ">New Residents apply online (app fees & deposits are paid online)</p>
                    <div class="arrowDown"></div>
                    
                    <p class="orangeOutline centeredMargin width200 fr1Color ">Residents Pay Rent (via web, mobile, text or phone)</p>
                    <div class="arrowDown"></div>
                    
                    <p class="orangeOutline centeredMargin width200 fr1Color ">Property Recieves Rent </p>
                </div>
                <div class="column50">
                    <div class="fullrow blockMarginAdjust">
                        <div class="arrowRight"></div>
                        <p class="orangeOutline inline width200 fr1Color ">Integrated properties will be added by RPO during your setup</p>                
                    </div>
                    
                    <div class="arrowDownLong"></div>
                    
                    <div class="fullrow">
                        <div class="arrowLeft"></div>
                        <p class="orangeOutline inline width200 fr1Color ">Integrated properties’ residents are imported automatically</p>                
                    </div>
            </div>
            </div>
            <!--Get Started Section -- Column 2-->
            <div class="column2">
                <h3 class="blue">Get Started</h3>
                <p class="frBlack">Now that you know how the system works, what would you like to do?</p> 
                
                <!--** Login **-->
                <div class="fullrow textCenter">
                    <!-- login Icon-->            
                    <div class="iconSection">
                        <a href="<%= EfxFramework.Settings.PropertyManagerLoginLink %>"><div class="loginIcon centeredMargin"></div>
                        <h4>Login</h4></a>
                        <p class="frBlack">Log into the Property Manager’s Portal</p>
                </div>
               </div>
                    
                <!--** Contact Sales **-->
                <div class="fullrow textCenter">
                    <!-- Contact Sales Icon-->            
                    <div class="iconSection">
                        <a href ="/Contact.aspx?ContactType=2"><div class="contactSalesIcon centeredMargin"></div>
                        <h4>Contact Sales</h4></a>
                        <p class="frBlack">Contact Sales to get started Collecting Rent Online</p>
                    </div>
                </div>
                
                <!--** Send me Info **-->
                <div class="fullrow textCenter">
                    <!-- Send me Info Icon-->            
                    <div class="iconSection">
                        <a href ="/Contact.aspx?ContactType=2"><div class="sendMeInfoIcon centeredMargin"></div>
                        <h4>Send me Info</h4></a>
                        <p class="frBlack">I want to learn more</p>
                    </div>
                </div>
            </div>
        </div>
        </div>

    <script>

        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1016) {
                var vML = (w_width - 1024) / 2;
            } 

            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);
        } // end of calc....


        $(document).ready(function () {
            /* */
            var vObj = document.getElementById("header");
            //vObj.style.top = "-40px";
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "100px");
            var vHeight = "height: " + ($("#pageCenterContainer").height() + 200) + "px !important";
            //$('.wrapper').attr('style', vHeight);
            vHeight = "position: absolute; top: " + ($("#pageCenterContainer").height() + 200) + "px";
            if ($(window).width() >= 601 && $(window).width() <= 1280) {
                vHeight = "position: absolute; top: " + "2500px";
            }
           // $('#footer').attr('style', vHeight);

            calc_container();
        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);

        // set menu to go back to default page
        $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

            evt.preventDefault();

            var vHash = $(this).attr('href');
            window.location.href = "/Default.aspx" + vHash;

        });

    </script>
</asp:Content>
