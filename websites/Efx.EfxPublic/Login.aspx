﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Efx.EfxPublic.Login"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>
	<!-- pgw 6/30/15 loginpage2 : style tag  -->
	<div id="pageCenterContainer" class="pageContainer loginPage loginpage2 residentsPage" style="display: block;  width: 90% !important; height: auto !important;  margin: 0 auto 0 auto !important; left: 0; right: 0; margin-left: auto !important; margin-right: auto !important; clear: both !important; " >
        <div class="pageTitle" ><h2 style="top: 2px; ">Register</h2></div>
        <div class="loginPageMain">
            <h3>Select an option below</h3>     
        
            <div class="loginHolder orangeOutline">
                <div class="loginItemHolder">
                    <h4 class="fr1Color" >Current Residents</h4>
                    <p class="fr1Color">Already a resident?</p>
                    <a id="id1" class="orangeButton logInButton fr2BColor" href='http://www.rentpaidonline.com/CurrentResidents.aspx' >Register</a>
                    <asp:LinkButton ID="ResidentLogInButton" runat="server" Visible="false" CssClass="orangeButton logInButton fr2BColor" PostBackUrl="http://www.rentpaidonline.com/CurrentResidents.aspx"  >Register</asp:LinkButton>
                </div>
            </div>
        
            <div class="loginHolder orangeOutline">
                <div class="loginItemHolder">
                  <h4 class="fr1Color">Future Residents</h4>
                    <p class="fr1Color">Applying to live in a new property?</p>
                    <a id="id2" class="orangeButton logInButton fr2BColor" href='http://www.rentpaidonline.com/FutureResidents.aspx' >Apply</a>
                    <asp:LinkButton ID="ApplicantLogInButton" runat="server" Visible="false" CssClass="orangeButton logInButton fr2BColor" PostBackUrl="http://www.rentpaidonline.com/FutureResidents.aspx" >Apply</asp:LinkButton>  
                </div>
            </div>
                    
             <div class="signupHolder" style="display:none; ">
                 <div class="loginItemHolder">                
                     <h4 class="fr1Color"> New to RentPaidOnline?</h4>
                     <p class="fr1Color">Sign up for a new account.</p>
                     <!-- OnClick="SignUpButton_Click"  -->
                     <a id="id3" class="orangeButton signUpLoginButton fr2BColor" href='http://www.rentpaidonline.com/CurrentResidents.aspx' >Sign Up Now!</a>
                      <asp:LinkButton ID="SignUpButton" runat="server" Visible="false" CssClass="orangeButton signUpLoginButton fr2BColor" PostBackUrl="http://www.rentpaidonline.com/CurrentResidents.aspx" >Sign Up Now!</asp:LinkButton>
                 </div>
             </div>
        </div>
                                     
    </div>  

    <script>

        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
            }
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);

            var vHeight = "height: " + ($("#pageCenterContainer").height() + 200) + "px !important";
            $('.wrapper').attr('style', vHeight);
            vHeight = "position: absolute; top: " + ($("#pageCenterContainer").height() + 200) + "px";
            $('footer').attr('style', vHeight);

        } // end of calc....


        $(document).ready(function () {
            /* */
            var vObj = document.getElementById("header");
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "100px");

            calc_container();
        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);


        // set menu to go back to default page
        $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

            evt.preventDefault();

            var vHash = $(this).attr('href');
            window.location.href = "/Default.aspx" + vHash;

        });
    </script>         
</asp:Content>
