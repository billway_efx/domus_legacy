﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Efx.EfxPublic.WebForm1" %>
<%@ Import Namespace="EfxFramework" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Rent Paid Online</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1"  runat="server">
    
    <!-- sliderContainer ptSlider  -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="position: relative; top: 0px; left: 0px; width: 2550px; height: 770px;" >
        <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 2550px; height: 770px; overflow: hidden; ">
            <div><img id="imgSlider" u="image" src="/Images/SlideShow-1-II-Building.jpg" />
                <div u="caption" t="FADE" d="300"  class="textSlider" style="position:absolute;left:680px;top:310px;width:300px;height:30px;color:white;"> 
                <span><h2 style="text-shadow: 2px 2px #030547;">
                    Easy Simple Fast Mobile</h2></span>
                </div>            </div>
            <div><img u="image" src="/Images/SlideShow-2-II-Easy.jpg" />
                <div u="caption" t="FADE" d="300"  class="textSlider" style="position:absolute;left:1050px;top:200px;width:300px;height:30px;color:white;"> 
                <span><h2 style="text-shadow: 2px 2px #030547;">
                    Easy</h2></span>
                </div>                <div u="caption" t="FADE" d="500"  class="textSlider" style="position:absolute;left:1026px;top:280px;width:300px;height:30px;color:white;"> 
                <span><h3 style="text-shadow: 2px 2px #030547;">
                    Pay rent online</h3></span>
                </div>            </div>
            <div><img u="image" src="/Images/SlideShow-3-II-Fast.jpg" />
                <div u="caption" t="FADE" d="300" class="textSlider" style="position:absolute;left:1086px;top:420px;width:460px;height:30px;color:white;"> 
                <span><h2 style="text-shadow: 2px 2px #030547;">
                    Fast</h2></span>
                </div>
                <div u="caption" t="FADE" d="500"  class="textSlider" style="position:absolute;left:1030px;top:490px;width:532px;height:30px;color:white;"> 
                <span><h3 style="text-shadow: 2px 2px #030547;">
                    Just click and go</h3></span>
                </div>
            </div>
            <div><img u="image" src="/Images/SlideShow-4-II-Simple.jpg" />
                <div u="caption" t="FADE" d="300" class="textSlider" style="position:absolute;left:475px;top:320px;width:300px;height:30px;color:white;"> 
                <span><h2 style="text-shadow: 2px 2px #030547;">
                    Simple</h2></span>
                </div>
                <div u="caption" t="FADE" d="500"  class="textSlider" style="position:absolute;left:282px;top:410px;width:300px;height:30px;color:white;"> 
                <span><h3 style="text-shadow: 2px 2px #030547;">
                    Use Credit Card, Bank Account, and Cash</h3></span>
                </div>
            </div>
            <div><img u="image" src="/Images/SlideShow-5-II-Mobile.jpg" />
                <div u="caption" t="FADE" d="300" class="textSlider" style="position:absolute;left:1300px;top:210px;width:300px;height:30px;color:white;"> 
                <span><h2 style="text-shadow: 2px 2px #3a829a;">
                    Mobile</h2></span>
                </div>
                <div u="caption" t="FADE" d="500"  class="textSlider" style="position:absolute;left:1204px;top:280px;width:300px;height:30px;color:white;"> 
                <span><h3 style="text-shadow: 2px 2px #3a829a;">
                    Anytime, anywhere, anyplace</h3></span>
                </div>
            </div>
        </div>
    </div>

    <%--<div style="height:10px; width:100%">&nbsp;</div>--%>
    <div id="pageCenterContainer1" class="pageContainer homepagePush opacity9 " style="padding-bottom: 0px;"> 
        <!-- <img id="imgSlider" width="0" height="0" src="Images/SlideShow1Building.jpg" style="display: none;" />-->
        <div class="mainNavImage"  >
            <div class="fullrow mainPoints">
                <div id="applySquare" class="homeColumn1 " >
                    <asp:HyperLink ID="homeColumn1Nav" runat="server" NavigateUrl="~/FutureResidents.aspx"><div id="applyTitle" class="applyTitle offTextColor" ><h3>APPLY</h3></div></asp:HyperLink>
                    <a class="mainSectionLink" href="/FutureResidents.aspx"> 
                    <%--Fill out your application and pay your fees easily with RentPaidOnline's specialized application engine.--%>
                    <p class="pl-30 pr-30 applyText offTextColor" id="applyText" >Not a resident? Click here to easily fill out your lease/rental application and pay your fees online!</p> 
                    </a>
                </div>
                <div id="registerSquare" class="homeColumn2 ">
                    <asp:HyperLink ID="homeColumn2Nav" runat="server" NavigateUrl="~/CurrentResidents.aspx"><div class="registerTitle offTextColor" id="registerTitle" ><h3>REGISTER</h3></div></asp:HyperLink>
                    <a class="mainSectionLink" href="/CurrentResidents.aspx">
                    <%--Say goodbye to late fees and lost checks with RentPaidOnline. Register here to get started!--%>
                    <p class="pl-30 pr-30 registerText offTextColor" id="registerText"  >Current resident? Say goodbye to late fees and lost checks with RentPaidOnline, and register here!<%--&nbsp;&nbsp;&nbsp;<span class="fa fa-chevron-right" ></span>--%></p>
                    </a>
                </div>

                <div id="paySquare" class="homeColumn3 ">
                    <asp:HyperLink ID="homeColumn3Nav" runat="server"><div class="payRentTitle offTextColor" id="payRentTitle"  ><h3>PAYMENT</h3></div></asp:HyperLink>
                    <asp:HyperLink ID="homeColumn3NavText" CssClass="mainSectionLink" runat="server"> 
                    <p class="pl-30 pr-30 payRentText offTextColor" id="payRentText" >Returning resident? Log in here to make a new payment or to just look around!<%--&nbsp;&nbsp;&nbsp;<span class="fa fa-chevron-right" ></span>--%></p>
                    </asp:HyperLink>
                </div>
            </div>
            <div id="archText" class="" style="background: white; padding-bottom: 20px; padding-top: 12px; box-shadow: inset 0 0 5px 1px #50719a;">
                <p class="firstText1" style="font-size:large; font-style:italic; color: #0462c2 !important">RentPaidOnline.com is an emerging leader in financial services, bringing a fresh and innovative</p>
                <p class="firstText2" style="font-size:large; font-style:italic; color: #0462c2 !important">approach to the way payments for rent and other multi-family collectibles are made.</p>
            </div>
            <div></div>
       </div>
             
        <!--Mobile -->
       <%-- <img id="imgPanel" width="0" height="0" src="Images/RPO-WebsiteScroll1.jpg" />--%>
        <a name="panel1"></a> 
        <div id="mobileImage" class="fullrow panels1" >
            <img class="panel1Image" src="/Images/RPO-PanelMobile2.jpg"  >
            <div class="panel1Text"  >
                <p id="panel1TextPar">&nbsp;<br /><br />
                    <!--cakel -->
                    <span id="sPanel1" style="font-size:1.4vw; line-height: 1.7vw;">
                        RentByCash®<br /><br />
                        iPhone, Android, Tablet, iPad<br /><br />
                        PayByText® Residents can pay rent via a text message<br /><br />
                        Pay Once or Setup Recurring Payments<br /><br />
                        Submit maintenance requests from your mobile device</span></p>
            </div>
            <div class="panel1AText">
                <p><span style="font-size:.85vw; line-height:.9vw;">
                    <div id="panel1ATextDiv" style="top: 82%; position: absolute; color: #374055; text-align: center; margin: 0 auto; width: 100%; vertical-align: top;">
                    <div id="panel1d1" style="width: 125px; display: inline-block; width: 230px; text-align: left; vertical-align: top;margin: 0 15px 0 15px;">
                        <div class="mb-default" style="text-align: center; font-weight:bold; margin-bottom: 15px;">Mobile Apps</div>
                        It's the new way to pay your rent, directly and seamlessly from your smartphone or tablet.
                        Gain the peace-of-mind that only RentPaidOnline® can provide.
                    </div>
                    <div id="panel1d2" style="width: 125px; display: inline-block; width: 230px; text-align: left; vertical-align: top;margin: 0 15px 0 15px;">
                        <div class="mb-default" style="text-align: center; font-weight:bold; margin-bottom: 15px;">Payment Methods</div>
                        RentPaidOnline® supports multiple payment methods, accepting e-checks, credit cards, cash, and pre-paid debit cards. 
                    </div>
                    <div id="panel1d3" style="width: 125px; display: inline-block; width: 230px; text-align: left; vertical-align: top;margin: 0 15px 0 15px;">
                        <div class="mb-default" style="text-align: center; font-weight:bold; margin-bottom: 15px;">Save Time</div>
                        RentPaidOnline® is a truly mobile solution. Save precious time by managing your rent payments...on the go...
                    </div>
                    <div id="panel1d4" style="width: 125px; display: inline-block; width: 230px; text-align: left; vertical-align: top;margin: 0 15px 0 15px;">
                        <div class="mb-default" style="text-align: center; font-weight:bold; margin-bottom: 15px;">Download Apps</div>
                        Gain the simple freedom that RentPaidOnline® provides, and download our mobile app now for FREE!<br /><br />
                        <a href='https://play.google.com/store/search?q=rentpaidonline' target="_blank"><div class="googlePlayIcon"></div></a>
                        <%--<a href='https://itunes.apple.com/us/app/rentpaidonline/id602671821' target="_blank"><div class="appStoreIcon"></div></a>--%>
                        <a href='https://itunes.apple.com/us/app/rentpaidonline-resident/id878842024?ls=1&mt=8' target="_blank"><div class="appStoreIcon"></div></a>
                    </div>
                </div>
                    <%--RentPaidOnline® is a Next Generation Payment Solution, providing multiple ways for residents to make rental payments. 
                    It's the new way to pay your rent, directly and seamlessly from your smartphone or tablet.<br /><br />

                    RPO supports multiple payment methods (all checks, credit cards, cash, and pre-paid debit cards). Applicants, former residents,
                    wait list, deposit, and miscellaneous accounting are accepted.<br /><br />
                
                    RPO is a truly mobile solution with apps for both residents and property managers. Save precious time by eliminating bank runs, and
                    conduct business on the go, anywyere, anytime, and anyplace!--%></span>
                </p>
            </div>
            </img>
        </div>
        <div class="sectionLinkHolder " style="display: none;">
          <%--<p><a class="mainSectionLink "  href="#top"><image class="upArrow" />&nbsp; Top</a></p>--%>
        </div>
        
        <!-- manager -->
        <a name="panel2"></a> 
        <div class="fullrow panels1" id="panel2">
            <!--cakel: adjusted Image -->
            <img class="panel2Image" src="/Images/RPO-PanelManager.jpg"  >
            <div class="panel2Text">
                <p id="panel2TextPar">&nbsp;<br /><br />
                    <span id="sPanel2" style="font-size:1.4vw; line-height: 1.4vw;">
                        First of its kind iPhone and Android app for property managers<br /><br />
                        Site staff has more time to focus on prospect and resident management<br /><br />
                        Community Connect® Property Managers can easily communicate with residents<br /><br />
                        Resident Connect® Property Managers can easily update residents on property events<br /><br />
                        Check out <a href="/ProsCorner.aspx" style="color: #fa6b02;font-weight: bold;">Pro's Corner ... </a>
                        contains useful tips for Property Managers, coupled with fantastic promotions!
                    </span></p>
            </div>
            <div class="panel2AText">
            <p><span style="font-size:.85vw; line-height:.9vw;">
                <div id="panel2ATextDiv" style="top: 75%; position: absolute; color: white; text-align: center; margin: 0 auto; width: 100%; vertical-align: top;">
                    <div id="panel2A1" style="width: 125px; display: inline-block; width: 170px; text-align: left; vertical-align: top;margin: 0 20px 0 20px;">
                        <div id="panel2A1a" style="text-align: center; font-weight:bold; margin-bottom: 15px;">Integrations</div>
                        RentPaidOnline® offers a streamlined solution for integrating with today’s most popular property management software. 
                        Take the hassle out of rent week by using one of our innovative, easy to use solutions!
                    </div>
                    <div id="panel2A2" style="width: 125px; display: inline-block; width: 170px; text-align: left; vertical-align: top;margin: 0 20px 0 20px;">
                        <div id="panel2A2a" style="text-align: center; font-weight:bold; margin-bottom: 15px;">Email Marketing</div>
                        RentPaidOnline® offers a revolutionary marketing engine to ensure you get the adoption you want. 
                        With on-demand marketing resources built in, there’s no more waiting on marketing departments!
                    </div>
                    <div id="panel2A3" style="width: 125px; display: inline-block; width: 170px; text-align: left; vertical-align: top;margin: 0 20px 0 20px;">
                        <div id="panel2A3a" style="text-align: center; font-weight:bold; margin-bottom: 15px;">Advanced Reporting</div>
                        RentPaidOnline® knows how important it is to track and reconcile payments efficiently. 
                        This is why we designed a customized report approach to meet the needs of every department in your organization!
                    </div>
                    <div id="panel2A4" style="width: 125px; display: inline-block; width: 170px; text-align: left; vertical-align: top;margin: 0 20px 0 20px;">
                        <div id="panel2A41a" style="text-align: center; font-weight:bold; margin-bottom: 15px;">Streamline Payments</div>
                        RentPaidOnline® will help your property achieve complete automation by offering multiple ways to capture your payments. 
                        You can finally say goodbye to manual entry and time wasting trips to the bank.
                    </div>
                </div>
                <div id="panel2A5" style="top: 96%; position: absolute; color: white; text-align: right; margin: 0 auto; width: 83%; vertical-align: top;">
                    <a id="panel2A5Link" href="/PropertyManager.aspx" style="color: #1bcbfe; font-weight: bold; font-size: 1vw;">Learn more today ...</a>
                </div>
                    <%--RentPaidOnline® was created by seasoned industry leaders as a solution for online payment seeking clients
                    that were dissatisfied with their complacent providers. The goal was to connect properties with their residents while 
                    making payment easy. <br /><br />

                    Why should your property staff be running around the property when most of their residents can be reached through technology?<br /><br />
                
                    Industry first solutions like Community Connect and Mobile Manager solve the lost productivity by allowing staff and residents
                    to exchange data and communicate easily through any device!<br /><br />--%>
               </span>
                </p>
            </div>
            </img>
        </div>
        <div class="sectionLinkHolder" style="display: none;">
          <%--<p><a class="mainSectionLink "  href="#top"><image class="upArrow" />&nbsp; Top</a></p>--%>
        </div>
         
        <!-- resident -->
        <a name="panel3"></a> 
        <div class="fullrow panels1" id="panel3">
           <img class="panel3Image" src="/Images/RPO-PanelResident2.jpg"  >
            <div class="panel3Text">
            <p id="panel3TextPar">&nbsp;<br /><br />
                    <span id="sPanel3" style="font-size:1.4vw; line-height: 1.7vw;">
                        Residents can view online statements<br /><br />
                        Residents can submit maintenance requests<br /><br />
                        Pay using credit card, debit card, checking account, and cash<br /><br />
                        iPhone and Android App for payments & iPad and Android Tablet Apps<br /><br />
                        PayByText® Residents can pay rent via a text message<br /><br />
                    </span></p>
            </div>
            <div class="panel3AText">
            <p><span style="font-size:.85vw; line-height:.9vw;">
                <div id="panel3A0" style="top: 82%; position: absolute; color: #374055; text-align: center; margin: 0 auto; width: 100%; vertical-align: top;">
                    <div id="panel3A1" style="width: 125px; display: inline-block; width: 350px; text-align: left; vertical-align: top;margin: 0 20px 0 20px;">
                        <div id="panel3A1a" style="text-align: center; font-weight:bold; margin-bottom: 15px;">Resident's Room</div>
                        Residents, come check out RentPaidOnline's blog for important updates. Our blog is a one-stop-shop for important news and
                        announcements. &nbsp;&nbsp;&nbsp;
                        <a href="/ResidentsRoom.aspx" style="color: #fa6b02;font-weight: bold;">Read More ...</a>
                    </div>
                   <div id="panel3A2" style="width: 125px; display: inline-block; width: 350px; text-align: left; vertical-align: top;margin: 0 20px 0 20px;">
                        <div id="panel3A2a" style="text-align: center; font-weight:bold; margin-bottom: 15px;">Intuitive By Design</div>
                        Our system is intuitive by design. We work closely with our clients to continually incorporate new and exciting features,
                        tailored carefully for both residents and staff.
                    </div>
                    
                </div>
                    <%--With flexibility in mind, RentPaidOnline® breaks all industry barriers, making online rent payment your property's new amenity. <br /><br />

                    Participating residents can use our service to make payments, create on-demand maintenance requests, report their rent to major
                    credit bureaus and explore exciting events at their community - anywhere, anytime, on any device they choose!<br /><br />--%>
                
                    <%--Our system is intuitive by design. We work closely with our clients to continually incorporate new and exciting features,
                    tailored carefully to ensure we're meeting the needs of both residents and property staff.--%><%--<br /><br />--%>
               </span>
                </p>
            </div>
            </img>
        </div>
        <div class="sectionLinkHolder" style="display: none;">
          <%--<p><a class="mainSectionLink "  href="#top"><image class="upArrow" />&nbsp; Top</a></p>--%>
        </div>          
             
        <!-- About  -->
        <a name="panel4"></a> 
        <div class="fullrow panels1" id="panel4">
            <!--cakel: adjusted Image -->
            <img class="panel4Image" src="/Images/RPO-PanelAbout2.jpg"  >
            <div class="panel4Text">
            <p id="panel4TextPar">&nbsp;<br /><br />
                    <span id="sPanel4" style="font-size:1.4vw; line-height: 1.8vw;">
                        "Our vision is to deliver customized payment solutions using innovation and 
                        technology to meet the evolving needs of our current and prospective clients."
                    </span></p>
           </div>
            <div class="panel4AText">
            <p>
            <span style="font-size:.85vw; line-height:.9vw;">
                <div id="panel4C" style="top: 83%; position: absolute; color: white; text-align: center; margin: 0 auto; width: 100%; vertical-align: top;">
                    
                    <div id="panel4D" style="display: inline-block; width: 40%; text-align: left; vertical-align: top;margin: 0 20px 0 20px;">
                        <div id="panel4Da" style="text-align: center; font-weight:bold; margin-bottom: 15px;">About RentPaidOnline®</div>
                         RentPaidOnline® brings a whole new approach to the way 
                         rent payments are made as well as how those payments are processed. 
                        With flexibility in mind, we break all industry barriers, making online rent collection
                        your property's new amenity. Residents can now make payments 
                         anywhere, anytime, on any device they choose. &nbsp;&nbsp;&nbsp;
                         <a href="/About.aspx" style="color: #fa6b02;font-weight: bold;">Learn More ...</a>
                    </div>
                    
                </div>
                    <%--With flexibility in mind, RentPaidOnline® breaks all industry barriers, making online rent payment your property's new amenity. <br /><br />

                    Participating residents can use our service to make payments, create on-demand maintenance requests, report their rent to major
                    credit bureaus and explore exciting events at their community - anywhere, anytime, on any device they choose!<br /><br />--%>
                
                    <%--Our system is intuitive by design. We work closely with our clients to continually incorporate new and exciting features,
                    tailored carefully to ensure we're meeting the needs of both residents and property staff.--%><%--<br /><br />--%>
               </span>
            </p>
            </div>
            </img>
        </div>
        <div class="sectionLinkHolder" style="display: none;">
          <%--<p><a class="mainSectionLink "  href="#top"><image class="upArrow" />&nbsp; Top</a></p>--%>
        </div>

        <!-- Contact and Partner Companies -->
        <a name="panel5"  ></a> 
        <div class="fullrow panels1" id="panel5">
            <img class="panel5Image" src="/Images/RPO-PanelContact2.jpg" >
            <div class="panel5Text">
            <p id="panel5TextPar">&nbsp;<br /><br />
                    <span id="sPanel5" style="font-size:1.3vw; line-height: 1.7vw; ">
                        <span id="sPanel5a" style="font-weight:bold; font-size:1.6vw; Color: #fa6b02;">Address</span><br />
                        RentPaidOnline®<br />
                        c/o EFX Corporation<br />
                        601 Cleveland Street<br />
                        Suite 950<br />
                        Clearwater, Florida &nbsp;&nbsp; 33755<br />
                        <br />
                        <span style="font-weight: bold; font-size:1.6vw; color: #fa6b02;">Phone number</span><br />
                        Toll Free: <a class="telephone1" href="tel:855-769-7368">855-PMY-RENT</a><br />
                        <br />
                        <!-- CMallory - Task 00577 - Changed link text for link below -->
                        <span style="font-weight: bold; font-size:1.6vw; color: #fa6b02;"><a href="https://rentpaidonline.rhinosupport.com/" style="color: #fa6b02;">Click here to contact support</a></span><br />
                        <br />
                        <span style="font-weight: bold; font-size:1.1vw; color: #1bcbfe;"><a href="/Contact.aspx" style="color: #1bcbfe;">Click here for more information, including a contact form.</a></span>
                    </span></p>
            </div>
            <div class="panel5AText">
            <p>
            
            </p>
            </div>
            </img>
        </div>
        <div class="sectionLinkHolder" style="display: none;">
          <%--<p><a class="mainSectionLink "  href="#top"><image class="upArrow" />&nbsp; Top</a></p>--%>
        </div>

    </div>


    <script>

        var vOrient = 'p';
        var vFlag = 0, vWidth1 = 0;
        // Listen for orientation changes
        window.addEventListener("orientationchange", function () {

            _portrait_landspace();
        }, false);

        // portrait OR landscape
        function _portrait_landspace() {

            //
            if (window.matchMedia("(orientation: portrait)").matches) {
                vOrient = 'p';
                default_elements();
            }

            if (window.matchMedia("(orientation: landscape)").matches) {
                vOrient = 'l';
                default_elements();
            }
        } // end pl...



        // default various elements
        function default_elements() {

            //
            var vWidth = $(window).width() + 2;
            if (vOrient == 'p') {
                if ($(window).width() >= 200 && $(window).width() < 430) {
                    var vHTML = "RentByCash®<br />";
                    vHTML +=  "iPhone, Android, Tablet, iPad<br />";
                    vHTML += "PayByText® Residents can pay rent via a text message<br />";
                    vHTML +=  "Pay Once or Setup Recurring Payments<br />";
                    vHTML +=  "Submit maintenance requests from your mobile device";
                    $("#sPanel1").html(vHTML);
                    vHTML = "First of its kind iPhone and Android app for property managers";
                    vHTML +=  "Site staff has more time to focus on prospect and resident management<br />";
                    vHTML +=  "Community Connect® Property Managers can easily communicate with residents<br />";
                    vHTML +=  "Resident Connect® Property Managers can easily update residents on property events<br />";
                    $("#sPanel2").html(vHTML);
                    vHTML = "Residents can view online statements ";
                    vHTML += "Residents can submit maintenance requests ";
                    vHTML += "Pay using credit card, debit card, checking account, and cash ";
                    vHTML += "iPhone and Android App for payments & iPad and Android Tablet Apps ";
                    vHTML += "PayByText® Residents can pay rent via a text message";
                    $("#sPanel3").html(vHTML);
                    vHTML = '"Our vision is to deliver customized payment solutions using innovation and ';
                    vHTML +=  'technology to meet the evolving needs of our current and prospective clients." ';
                    $("#sPanel4").html(vHTML);
                }
            }
            // tablet
            if (vOrient == 'l') {
                if ($(window).width() >= 950 && $(window).width() < 970) {
                    var vHTML = "";
                    vHTML = "First of its kind iPhone and Android app for property managers<br /><br />";
                    vHTML += "Site staff has more time to focus on prospect and resident management<br /><br />";
                    vHTML += "Community Connect® Property Managers can easily communicate with residents<br /><br />";
                    vHTML += "Resident Connect® Property Managers can easily update residents on property events<br />";
                    vHTML +=  "Check out <a href='/ProsCorner.aspx' style='color: #fa6b02;font-weight: bold;'>Pro\'s Corner ... </a>";
                    vHTML += "contains useful tips for Property Managers, coupled with fantastic promotions!";
                    $("#sPanel2").html(vHTML);
                }
            }

            // add margin above the mobile image
            if ((( vOrient == 'p' && $(window).width() >= 200 && $(window).width() < 592)) ||
                 (vOrient == 'l' && $(window).width() >= 592 && $(window).width() <= 792) ) {
                var vDisplay = $("#slider1_container").css("display");
                var vObj = document.getElementById("pageCenterContainer1");
                if (vDisplay == 'none') {
                    $(".mainNavImage").css("margin-top", "35px");
                    $("#pageCenterContainer1").css("margin-top", "25px");
                } else {
                    $(".mainNavImage").attr("style", "margin-top: 0 !important; ");
                    $(".pageContainer").attr("style", "margin-top: 0 !important; ");
                }
            }

            // change images 
            if ((( vOrient == 'p' && $(window).width() >= 200 && $(window).width() < 592)) ||
                 (vOrient == 'l' && ($(window).width() >= 200 && $(window).width() < 792))
                ) {
                if (vOrient == 'l') {
                    vFlag = 1;
                    $(".panel1Image").attr('src', "/Images/MobileLandscape2.jpg");
                    $("#applySquare").attr("style", "width: 90%; max-width: 500px; ");
                    $("#registerSquare").attr("style", "width: 90%; max-width: 500px; ");
                    $("#paySquare").attr("style", "width: 90%; max-width: 500px; ");
                    // pgw $("#pageCenterContainer1").attr("style", "margin-top: 0 !important; ");
                    //$(".mainNavImage").attr("style", "margin-top: 0; ");
                } else {
                    $(".panel1Image").attr('src', "/Images/MobilePortrait2.jpg");
                }

                $(".panel1Image").css('width', vWidth);
                var vHeight = $(".wrapper").css("height");
            } else {
                if (vOrient == 'l') {
                    $(".panel1Image").attr('src', "/Images/RPO-PanelMobile2.jpg");
                    if (vFlag == 1 ) {
                        $("#applySquare").attr("style", "width: 26%; max-width: 303px; ");
                        $("#registerSquare").attr("style", "width: 26%; max-width: 303px; ");
                        $("#paySquare").attr("style", "width: 26%; max-width: 303px; ");
                    }
                } else {
                    $(".panel1Image").attr('src', "/Images/RPO-PanelMobile2.jpg");
                }
                $(".panel1Image").css('width', vWidth);
            }

        } // end of default elements....

        /*Calling the mobile template*/
        $(document).ready(function () {

            /* */
            var w_width = $(window).width();
            _portrait_landspace();

        });



        // Listen for resize changes
        window.addEventListener("resize", function () {

            _portrait_landspace();
        }, false);

        </script>

</asp:Content>
