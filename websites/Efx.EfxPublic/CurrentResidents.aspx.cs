﻿using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.Pages.FindPropertyPages;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.Factories;
using EfxFramework.ViewPresentation.Presenters.Pages.FindPropertyPresenters;
using EfxFramework.Web;
using System;

namespace Efx.EfxPublic
{
    public partial class CurrentResidents : BasePageV2, ICurrentResidentFindProperty
    {
        private FindPropertyPresenterBase _Presenter;
        public string GetPropertiesBySearchStringUrl { get { return Settings.GetPropertiesBySearchString; } }
        public IFindPropertyUserControl FindPropertyControl { get { return FindProperty; } }

        protected void Page_Load(object sender, EventArgs e)
        {            
            _Presenter = FindPropertyPresenterFactory.GetFindPropertyPresenter(this);
            AlreadyRegisterLink.NavigateUrl = Settings.RenterLoginLink;
            
            if (!Page.IsPostBack)
                _Presenter.InitializeValues();  
        }
    }
}