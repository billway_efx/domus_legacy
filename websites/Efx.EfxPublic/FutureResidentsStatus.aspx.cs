﻿using EfxFramework;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;

namespace Efx.EfxPublic
{
    public partial class FutureResidentsStatus : BasePageV2
    {
        public int ApplicantId { get { var Id = Request.QueryString["ApplicantId"]; return !String.IsNullOrEmpty(Id) ? Id.ToInt32() : 0; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ApplicantId < 1)
                DisplaySystemMessage("No Applicant to show Status For.", SystemMessageType.Error);

            BindStatusGrid();
        }

        protected void BindStatusGrid()
        {
            StatusGrid.DataSource = ApplicantApplication.GetAllApplicantApplicationsByApplicantId(ApplicantId);
            StatusGrid.DataBind();
        }

        public string ReturnYesOrNo(bool bit)
        {
            return bit ? "Yes" : "No";
        }
    }
}