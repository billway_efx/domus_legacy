﻿using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using System;
using System.Web.UI.WebControls;

namespace Efx.EfxPublic
{
    public partial class WebForm2 : BasePageV2, IContact
    {
        private ContactPresenter _Presenter;
                
        public string ContactType { get { return !string.IsNullOrEmpty(Request.QueryString["ContactType"]) ? Request.QueryString["ContactType"] : null; } }        
        public RadioButtonList ContactRpoRadioButtonList { get { return ContactRentPaidOnlineRadioButtonList; } }
        public IContactForm ContactUserControl { get { return ContactForm; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new ContactPresenter(this);
            AddValidationSummaryToPage();
            //EmailLink.NavigateUrl = String.Format("mailto:{0}", Settings.ContactUsEmail);           
            
            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}