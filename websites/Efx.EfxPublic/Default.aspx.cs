﻿using EfxFramework;
using EfxFramework.Web;
using System;

namespace Efx.EfxPublic
{
    public partial class WebForm1 : BasePageV2
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //EmailLink.NavigateUrl = String.Format("mailto:{0}", Settings.ContactUsEmail);
            homeColumn3Nav.NavigateUrl = Settings.PaymentsLink;
            homeColumn3NavText.NavigateUrl = Settings.PaymentsLink;
        }
    }
}

