﻿<%@ Page Title="Current Residents Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CurrentResidentsRegister.aspx.cs" Inherits="Efx.EfxPublic.CurrentResidentsRegister" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>
    <!--  idCCRPage  -->
    <div id="pageCenterContainer" class="pageContainer residentsPage ccrPage"  >
        <div class="pageTitle frPageTitle" style="border-color: white;" ><h2>Current Residents</h2></div>
        <div class="column1">
            <div class="textCenter">
                <h3>How It Works</h3>
                <h4 class="orangeOutline centeredMargin width200 fr1Color">Find Your Property</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeBlock centeredMargin registerNow width100 fr2BColor">Register</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width100 fr1Color">Login</h4>
                <div class="arrowDown"></div>
                <h4 class="orangeOutline centeredMargin width100 fr1Color">Pay Rent</h4>         
            </div>
        </div>
        <div class="column2">
            <div class="formHolder formContainer">
				<asp:Label CssClass="alert alert-error" runat="server" ID="ExistingUserLabel" 
                    Text="An account associated with that Resident ID has already been registered, please check your ID and try again. You can also contact RentPaidOnline support team for further assistance." 
                    Visible="false"></asp:Label>
                <uc1:ResidentRegistration runat="server" id="ResidentRegistration" AreConditionalFieldsVisible="True" />
            </div>
        </div>
    </div>

<script>

    // calc margin left to center container
    function calc_container() {

        var w_width = $(window).width();
        if ($(window).width() >= 1024) {
            var vML = (w_width - 1024) / 2;
        }
        $("#pageCenterContainer").css("margin-left", vML);
        $("#pageCenterContainer").css("margin-right", vML);
    } // end of calc....


    $(document).ready(function () {
        /* */
        var vObj = document.getElementById("header");
        //vObj.style.top = "-40px";
        $(".residentsPage").css("position", "absolute");
        $(".residentsPage").css("top", "100px");
        //$(".residentsPage").css("background", "linear-gradient(#b1bdd4,#e9e9ea)");

        //$('.wrapper').attr('style', 'height: 1500px !important');

        calc_container();

        // move footer to the bottom of page
        //$("footer").addClass("footer");
    });


    // Listen for resize changes
    window.addEventListener("resize", function () {

        calc_container();
    }, false);


    // set menu to go back to default page
    $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

        evt.preventDefault();

        var vHash = $(this).attr('href');
        window.location.href = "/Default.aspx" + vHash;

    });



$(document).ready(function () {
    /* Mask */
    ApplySingleMask("#<%=ResidentRegistration.PhoneNumberClientId%>", "(000) 000-0000");
});
</script>

</asp:Content>

