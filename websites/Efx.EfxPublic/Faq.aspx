﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Faq.aspx.cs" Inherits="Efx.EfxPublic.Faq" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <div id="pageCenterContainer" class="pageContainer faqContainer residentsPage">
        <div class="pageTitle frPageTitle fr1Color" style="border-color: white;" ><h2>FAQ</h2></div>
        <div class="sectionEighty">
            <article class="fullrow">
                <h3  class="frBlack" >Renters How do I register as a resident?</h3>
                <p class="frBlack" >To sign up as a resident, simply click the resident sign up button. You will then be asked to search for your property. Once you have located your property, click "Register Now" and follow the three-step process.</p>
            </article>
            
            <article class="fullrow">
                <h3  class="frBlack" >What payment types does RentPaidOnline accept?</h3>
                <p class="frBlack" >Visa, MasterCard, Discover, AMEX (based on properties decision) and ACH transactions.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >What payment avenues does RentPaidOnline offer?</h3>
                <p class="frBlack" >RentPaidOnline™ offers the most innovative payment acceptance channels in the multifamily industry, as follows:</p>
                <ul class="brownTextList">
                    <li>Online at www.RentPaidOnline.com</li>
                    <li>IVR with 24/7 access</li>
                    <li>Phone Pay with a Client Care Representative</li>
                    <li>Pin based system for credit cards</li>
                    <li>Text message via PayByText™</li>
                    <li>iPhone and Android apps for smartphones and tablets</li>
                    <li>Email and Text reminders when rent is due</li>
                    <li>In person payments for application fees and deposits at your management office</li>
                    <li>PrePaid Debit Cards</li>
                    <li>Cash Payments via RentByCash™</li>
                    <li>NetSwipe™ from Jumio</li>
                    <li>Mobile site for smartphone access on the go</li>
                </ul>
            </article>
            <article class="fullrow frBlack">
                <h3 class="frBlack">How do I process my payments by Phone?</h3>
                <p class="frBlack" >Residents may call, toll-free, <a class="telephone" href="tel:855-769-7368">855-PMY-RENT</a> (769-7368) to make a payment by phone through our IVR. In order to use the IVR, you must have an account created and a payment type saved in your account and received your personal pin.</p>
            </article>
            <article class="fullrow">
                <h3 class="frBlack">Can I setup an automatically recurring payment via E-check?</h3>
                <p class="frBlack" >Yes, you can. Simply click “Auto Pay" on your Resident home page. Enter your ACH information and then you will be prompted to enter your recurring payment parameters.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >When will my property manager receive notification of payment?</h3>
                <p class="frBlack" >RentPaidOnline sends a real-time payment confirmation email to you and to your property manager at the same time the transaction is made. Most properties will recognize this email, which contains the time and date that the rental payment was made.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >How long does it take for my payment to clear?</h3>
                <p class="frBlack" >When making a payment with a Credit, Debit, and Pre-Paid debit cards it may take 2-3 business days to clear your bank or credit account. Card transactions are instantly authorized so money will be instantly pending for your transaction. When the transaction is complete an email payment confirmation is sent to the property management staff and yourself with a date & time stamp of the payment. When submitting a payment via an ACH it can take 3-4 business days for funds to clear your bank account.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >What if I need to get a refund or cancel a payment?</h3>
                <p class="frBlack" >Please contact your Property Manager at your community. Property staff administers handle all refunds. RentPaidOnline does not offer refunds on service fees or payments from residents. Property Managers can refund full rent amounts, duplicate payments, partial payments, holding fees, and application deposits. If an error was made when submitting your payment, please contact your property manager and they will instruct RentPaidOnline on how to handle the transaction.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >What do I do if I duplicate my payment?</h3>
                <p class="frBlack" >Contact your property manager to request a refund for the duplicate transaction.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >Will I receive reminder emails when my rent is due?</h3>
                <p class="frBlack" >Yes, RentPaidOnline will send an email reminder telling you that your rent payment is due three days before the due date.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >How can I view previous payments?</h3>
                <p class="frBlack" >RentPaidOnline will never delete any data so you will always be able to log in to your resident screen and see your history.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >Why is there a fee for my rental application?</h3>
                <p class="frBlack" >Each property management company provides RentPaidOnline with the rental application fee they charge in order to process a screening report. You may pay this fee online when you submit your application so your Manager can quickly and easily process your application. If you do not wish to pay this fee online you will need to speak with your Property Manager.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >Is there a charge to the resident when rent is paid online?</h3>
                <p class="frBlack" >If you submit a payment online, there is a convenience fee that is either paid by the Resident or by the Property Manager depending on how the property was setup by the property management company. Please ask your property manager how you are set up to pay for further instructions.</p>
            </article>
            <article class="fullrow">
                <!-- Whittingham - 08/23/2015 - task #00356: removed part of the question text  -->
                <h3  class="frBlack" >What is a convenience fee and why is it charged?</h3>
                <p class="frBlack" >A convenience fee is charged to cover the cost of processing your payment via credit card or E-check. If your property elects to absorb this fee, then there will not be a convenience fee accessed on your transaction.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >What is a Non-Sufficient Funds (NSF) charge?</h3>
                <p class="frBlack" >If your payment is returned for any reason, you will receive a real time email notification of the failed transaction. Your property manager will also charge you an NSF fee and/or a late fee for the failed transaction. RentPaidOnline will not charge you a fee for returned payments</p>
            </article>
            <article class="fullrow">
                <h3 class="frBlack">Property Managers How do I get my properties signed up?</h3>
                <p class="frBlack" >To get information about our products, click on the "property managers" link on our home page and fill out the form with your information. A representative will contact you and determine the best program for your company based on your needs.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >After signing up how long does it take to get running?</h3>
                <p class="frBlack" >After your account has cleared underwriting, RentPaidOnline can have you off the ground within 24 hours (without integration). If you have integration, we require an additional 2-4 business days to have you launched. RentPaidOnline prides itself in having the fastest activation time in our industry.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >What are the costs associated with an account with RentPaidOnline?</h3>
                <p class="frBlack" >RentPaidOnline has several models to accommodate you and your residents' needs. RentPaidOnline doesn’t believe that every property management company is the same so we are the first in this industry to tailor every company’s fees based on their needs. Please submit a request to sales@rentpaidonline.com and one of our representatives will reach out to you to customize your program to your needs.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >How long does it take for the tenant's funds to be deposited?</h3>
                <p class="frBlack" >Unlike most of RentPaidOnline’s competitors, our online ACH payment processing is on a zero day hold for everyone. What this means to you is all ACH payments that are done by 10PM eastern are deposited directly into your account the next morning excluding weekends and holidays. All credit card transactions take the standard time of 2-3 business days.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >What if a payment is NSF?</h3>
                <p class="frBlack" >You will receive an email notification with 48-72 hours of the NSF transaction. The property manager and resident will also receive an email notification of the NSF. RentPaidOnline will charge the property management company a nominal NSF fee. We will never charge the resident an NSF fee.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >What type of properties can utilize RentPaidOnline?</h3>
                <p class="frBlack" >RentPaidOnline works with all Multi Family communities, single-family homes, condominiums, industrial, vacation, and HOAs.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >Can I disable features for the residents and staff in the property manager portal?</h3>
                <p class="frBlack" >Yes, you will receive a master login and can delegate who uses every feature inside of RentPaidOnline for your staff based on their login. You will also be able to adjust the parameters for your tenants as well.</p>
            </article>
            <article class="fullrow">
                <h3  class="frBlack" >How customizable is RentPaidOnline?</h3>
                <p class="frBlack" >RentPaidOnline is the most customizable interface on the market for payments in the multifamily industry. From colors to pictures you change the features to best suit you. If you think we could be doing something better at RentPaidOnline or you have an idea to make the service better our doors are always open. This solution was built for you and will grow with you!</p>
            </article>         

            <%--Salcedo - added Faq text--%>
            <article class="fullrow">
                <h3  class="frBlack" >How do my residents make Electronic Cash Payments?</h3>
                <p class="frBlack" >Once your properties are signed up to use RentPaidOnline payment platform, all your residents need to make Electronic Cash Payments is their bar code. You can create their resident specific barcode inside the property manager portal on RentPaidOnline. The residents will then take their bar code that was either printed, emailed, or text to them, along with their cash, to their nearest 7-Eleven or ACE Cash Express location. The resident will then have the cashier scan their barcode, take their cash and they will then be provided with a receipt as proof of payment. Payments will show up in the residents payment history and all property reports within 1 minute. </p>
            </article>         
            <article class="fullrow">
                <h3  class="frBlack" >Where can my residents make their Electronic Cash Payments?</h3>
                <p class="frBlack" >Your residents can pay their rent with cash at over 10,000 7-Eleven and 1,600 ACE Cash Express locations. To find the nearest location, go to http://paynearme.com/locations and search by address or zip code.</p>
            </article>         
            <article class="fullrow">
                <h3  class="frBlack" >What is the fee for a resident to make an Electronic Cash Payment?</h3>
                <p class="frBlack" >Your residents will pay a $3.99 processing fee for each Electronic Cash Payment. The maximum amount a resident can pay with a single barcode is $1500. There is no limit to the number of payments they can make however. </p>
            </article>         
            <article class="fullrow">
                <h3  class="frBlack" >Can the bar code be reused?</h3>
                <p class="frBlack" >No. Each barcode is unique and can only be used 1 time. The bar code is good for 7 days after it has been created and the resident must pay the amount that the bar code was created for to ensure amount accuracy and management control. </p>
            </article>         
            <article class="fullrow">
                <h3  class="frBlack" >How does my resident know if their payment was accepted?</h3>
                <p class="frBlack" >Your residents will receive a receipt with a date and time stamp from the cashier as proof of their payment. The receipt will also include the residents name, property address, your company’s name and address, payment confirmation number and their account balance. Your resident will also be able to log into RentPaidOnline and see the payment in their profile.</p>
            </article>         
            <article class="fullrow">
                <h3  class="frBlack" >Do residents have to be registered in order to make a RentByCash transaction?</h3>
                <p class="frBlack" >Not at all. Each manager has the ability to create a RentByCash barcode for any of their integrated residents. You can also create a barcode for your applicants as well!</p>
            </article>         
            <article class="fullrow">
                <h3  class="frBlack" >How will I know which residents have paid via Electronic Cash Payment?</h3>
                <p class="frBlack" >Upon payment confirmation, a residents receipt will automatically be created in RentPaidOnline® for their Electronic Cash Payment. The receipt will include a reference number for the transaction. A RentPaidOnline transaction ID will also be created and attached to their resident profile.</p>
            </article>         
            <article class="fullrow">
                <h3  class="frBlack" >When will the funds arrive in my account?</h3>
                <p class="frBlack" >Funds will automatically be deposited into your account in 2-3 business days. Since they were cash payments, you never have to worry about NSFs.</p>
            </article>         
            <article class="fullrow">
                <h3  class="frBlack" >What happens if my resident is under eviction and they try to make an Electronic Cash Payment?</h3>
                <p class="frBlack" >If you place your resident in eviction status in your property management software or in RentPaidOnline's easy-to-use portal, their transaction will be declined. They’ll receive a receipt indicating their transaction was declined and to contact the property office with questions.</p>
            </article>         
            <article class="fullrow frBlack">
                <h3  class="frBlack" >Who can I contact for help?</h3>
                <p class="frBlack" >Please email RBCSupport@rentpaidonline.com with any questions you have regarding Electronic Cash Payments.</p>
            </article>         


        </div>
    </div>


    <script>

        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
            }
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);
        } // end of calc....


        $(document).ready(function () {
            /* */
            var vObj = document.getElementById("header");
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "100px");

            var vHeight = "height: " + ($("#pageCenterContainer").height() + 200) + "px !important";
            //$('.wrapper').attr('style', vHeight);
            vHeight = "position: absolute; top: " + ($("#pageCenterContainer").height() + 200) + "px";
            //$('#footer').attr('style', vHeight); 

            calc_container();
        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);


        // set menu to go back to default page
        $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

            evt.preventDefault();

            var vHash = $(this).attr('href');
            window.location.href = "/Default.aspx" + vHash;

        });
    </script>
</asp:Content>
