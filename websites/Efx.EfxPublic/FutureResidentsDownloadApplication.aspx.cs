﻿using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;

namespace Efx.EfxPublic
{
    public partial class FutureResidentsDownloadApplication : BasePageV2, IApplicationDownload
    {
        private DownloadApplicationPresenter _Presenter;

        public EventHandler DownloadButtonClick { set { DownloadApplicationButton.Click += value; } }
        public int PropertyId { get { return !string.IsNullOrEmpty(Request.QueryString["PropId"]) ? Request.QueryString["PropId"].ToInt32() : 0; } }
        public int ApplicantId { get { return !string.IsNullOrEmpty(Request.QueryString["ApplicantId"]) ? Request.QueryString["ApplicantId"].ToInt32() : 0; } }
        public bool NoPropertyFoundVisible { set { NoPropertyFoundLabel.Visible = value; } }
        public bool ApplicationDownloadPanelVisible { set { ApplicationDownloadPanel.Visible = value; } }
        public string PropertyNameText { set { PropertyNameLabel.Text = value; } }
        public string PropertyImageUrl { set { PropertyImage.ImageUrl = value; } }
        public string NextButtonUrlText { set { NextButton.PostBackUrl = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _Presenter = new DownloadApplicationPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();
        }
    }
}