﻿/* This holds the content for all the tool tips in the site, this way nothing is directly written into the markup*/


//This function is for the "what is a CVV code" tool tip, which 
$(function () {
    $('.cvvTip').data('powertip', '<div class="toolTipContent"><h4>What is a CVV Number?</h4><div class="cvvImage"></div></div>').powerTip({ placement: 'e' });
    // 'e' means that the tool tip will appear to the eastern side of the element
});

//This function is for the "echeck routing and account numbers" tool tip, which 
$(function () {
    $('.checkNumbersTip').data('powertip', '<div class="toolTipContent"><h4>Where Do I Find Routing and Checking Account Numbers?</h4><div class="eCheckImage"></div></div>').powerTip({ placement: 'e' });
    // 'e' means that the tool tip will appear to the eastern side of the element
});

//This function is for the "Residents ID Number" tool tip, which 
$(function () {
	$('.residentIdNumberTip').data('powertip', '<div class="toolTipContent"><h4>What is the Resident ID Number?</h4><p>Your RPO ID can be obtained from Rent Paid Online<br /> customer support, or you can use your Tenant ID<br /> from your Property Manager</p>').powerTip({ placement: 'e' });
    // 'nw' means that the tool tip will appear to the north western side of the element
});

//cakel: BUGID00202
//This function is for the "Residents to search for PMS ID
$(function () {
    $('.residentPmsIdTip').data('powertip', '<div class="toolTipContent"><h4>Search for Your Resident ID</h4><p>A Resident ID is required<br /> to complete your registration.<br />Please click the link to find your ID<br /> or contact your property manager</p>').powerTip({ placement: 'e' });
    // 'nw' means that the tool tip will appear to the north western side of the element
});



