﻿using EfxFramework;
using Mb2x.ExtensionMethods;
using System.Web;

namespace Efx.EfxPublic.Handlers
{
    /// <summary>
    /// Summary description for ApplicationDownloadHandler
    /// </summary>
    public class ApplicationDownloadHandler : IHttpHandler
    {
        public bool IsReusable { get { return false; } }

        public int ApplicantApplicationId { get { return !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["ApplicantApplicationId"]) ? HttpContext.Current.Request.QueryString["ApplicantApplicationId"].ToInt32() : 0; } }
        
        public bool IsPropertyApplication
        {
            get
            {
                var Result = HttpContext.Current.Request.QueryString["IsPropertyApp"].ToNullInt32();
                return Result.HasValue && Result.Value == 1;
            }
        }

        public int PropertyId
        {
            get
            {
                var Id = HttpContext.Current.Request.QueryString["PropertyId"].ToNullInt32();
                if (Id.HasValue && Id.Value > 0)
                    return Id.Value;

                return 0;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            if (IsPropertyApplication)
                DownloadPropertyApplication(context);
            else
                DownloadApplicantApplication(context);
        }

        private void DownloadApplicantApplication(HttpContext context)
        {
            var ResidencyApp = ApplicantApplication.GetCompletedApplication(ApplicantApplicationId);

            if (ResidencyApp == null)
                return;

            context.Response.ContentType = "application/pdf";
            context.Response.OutputStream.Write(ResidencyApp, 0, ResidencyApp.Length);
        }

        private void DownloadPropertyApplication(HttpContext context)
        {
            var PropertyApplication = Property.GetApplicationForResidency(PropertyId);

            if (PropertyApplication == null)
                return;

            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.OutputStream.Write(PropertyApplication, 0, PropertyApplication.Length);
        }
    }
}