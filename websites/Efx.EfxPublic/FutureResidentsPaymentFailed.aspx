﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FutureResidentsPaymentFailed.aspx.cs" Inherits="Efx.EfxPublic.FutureResidentsPaymentFailed" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>

    <div id="pageCenterContainer" class="pageContainer crRPOSearch residentsPage">
        <div class="pageTitle frPageTitle fr1Color"><h2>Payment Failed</h2></div>
        <div class="orangeOutline paymentFailed">
            <section class="paymentStatusInfoBlock">
                <h3 class="fr1Color">Payment Status</h3>
                <p class="redText">Your Transaction has NOT been processed.</p>

                    <br />
                    <asp:Label ID="lblFailureMessage" runat="server" Text="Label" Visible ="false"></asp:Label><br /><br />

                <p class="redText">Please try again</p>
            </section>
            <section class="paymentStatusInfoBlock">
                <h5 class="fr1Color">RPO Support</h5>
                <p class="fr1Color"><a class="telephone" href="tel:855-769-7368">Support Line: 1-855-PMY-RENT</a></p>
                <p class="fr1Color">Email:<a class="fr2Color" href="mailto:support@rentpaidonline.com"> support@rentpaidonline.com</a></p>
            </section>
            <section class="paymentStatusInfoBlock">
                <!--  <img src="/Images/RentPaidOnlineLogo.png" class="smallLogo" /> -->
            </section>
        </div>
    </div> 

    <script>

        // calc margin left to center container
        function calc_container() {

            var vML = "0";
            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                vML = (w_width - 1024) / 2;
            }
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);
        } // end of calc....


        $(document).ready(function () {
            /* */
            var vObj = document.getElementById("header");
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "150px");
            var vHeight = "height: " + ($("#pageCenterContainer").height() + 200) + "px !important";
            //$('.wrapper').attr('style', vHeight);
            vHeight = "position: absolute; top: " + ($("#pageCenterContainer").height() + 200) + "px";
            //$('#footer').attr('style', vHeight);

            calc_container();
        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);

        // set menu to go back to default page
        $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

            evt.preventDefault();

            var vHash = $(this).attr('href');
            window.location.href = "/Default.aspx" + vHash;

        });
    </script>
</asp:Content>
