﻿<%@ Page Title="Careers" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Jobs.aspx.cs" Inherits="Efx.EfxPublic.Jobs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>
    <div id="pageCenterContainer" class="jobContainer residentsPage" >
        <div class="pageTitle frPageTitle fr1Color" style="border-color: white;"><h2>Careers</h2></div>
        <section class="borderBottom" style="margin-left: 5%;">
            <article class="sectionEighty">
                <hgroup>
                    <h3 class="fr1Color">Job Postings</h3>
                    <h4 id="CurrentOppText" class="fr2Color" runat="server" visible="false">Currently Seeking Qualified Candidates for The Following Positions</h4>
                </hgroup>
                <p class="generalTextBlock frBlack">
                    Looking for a place to have a career and not just another job, RentPaidOnline might be the place for you! Built on respect, 
                    innovation and our current employees, RentPaidOnline is taking a fresh new approach to processing rent payments online. 
                    We are looking for energetic people who love to work hard and like to have fun. 
                    With miles of growth and opportunity ahead, we're seeking qualified candidates for the following positions below.
                    The future is now, don't let it pass you by!
                 </p>
            </article>
            
            <div class="sectionSixty">
                <span class="formLabel"><asp:Label ID="NoJobsMessage" CssClass="fr1Color" Text="No Career Opportunities Available" runat="server" Visible="true" /></span>
                <div class="formRow">
                    <div class="formHalfWidth">
                        <asp:DropDownList CausesValidation="false" ID="JobsDropDownList" runat="server" CssClass="chzn-select" DataTextField="JobTitle" DataValueField="JobPostingId" AutoPostBack="True" AppendDataBoundItems="true" Visible="false" >
                             <asp:ListItem Text="Select" Value="0" />
                         </asp:DropDownList>
                    </div>
                </div>         
            </div>             
         </section>

        <div class="fullrow">
            <!-- Job Posting -->
            <asp:Panel ID="JobPostingPanel" runat="server" Visible="false">
                <section class="jobItem">
                    <article runat="server" id="JobBorder" class="fullrow jobPosting borderBottom">
                        <div class="sectionSixty">
                            <h4><asp:Label ID="jobTitle" runat="server"></asp:Label></h4>
                            <asp:Label runat="server" ID="JobDescription" CssClass="jobDescription fr1Color">Job Description</asp:Label>
                            <asp:Label runat="server" ID="PostDate" CssClass="postDate frBlack">Posted On: <asp:Label ID="jobPostDate" runat="server"></asp:Label></asp:Label>
                            <p class="frBlack"><asp:Literal ID="JobDescriptionLiteral" runat="server" /></p>
                            
                            <asp:Label runat="server" ID="DutiesList" CssClass="jobDescription  fr1Color">Principal Duties and Responsibilities:</asp:Label>
                            <p class="frBlack"><asp:Literal ID="DutiesLiteral" runat="server" /></p>
                            <asp:Label runat="server" ID="SkillsList" CssClass="jobDescription  fr1Color">Skills and Abilities Required:</asp:Label>
                            <p><asp:Literal ID="SkillsLiteral" runat="server" /></p>

                            <!--**Button Time**-->
                            <asp:Button ID="ApplyNowButton" CssClass="orangeButton fr2BColor" Text="Apply Now" runat="server" OnClientClick="ShowModal('#ApplyNowModal')" />
                
                           <div id="ApplyNowModal" class="hide"><uc1:ApplyNowWindow runat="server" ID="ApplyNowWindow" /></div>
                       </div>
                    </article>
                </section>
            </asp:Panel>
        </div>
    </div>

    <script>

        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
            }
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);
        } // end of calc....


        $(document).ready(function () {
            /* */
            var vObj = document.getElementById("header");

            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "100px");
           // $('.wrapper').attr('style', 'height: 1200px !important');
            var vHeight = "height: " + ($("#pageCenterContainer").height() + 200) + "px !important";
            //$('.wrapper').attr('style', vHeight);
            vHeight = "position: absolute; top: " + ($("#pageCenterContainer").height() + 200) + "px";
            //$('#footer').attr('style', vHeight);


            calc_container();
        });

        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);

        // set menu to go back to default page
        $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

            evt.preventDefault();

            var vHash = $(this).attr('href');
            window.location.href = "/Default.aspx" + vHash;

        });
    </script>

</asp:Content>
