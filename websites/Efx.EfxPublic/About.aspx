﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Efx.EfxPublic.About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- make slider hidden -->
    <div id="slider1_container" class="sliderContainer ptSlider" style="width: 0px; height: 0px; display: none; " ></div>
    <div id="pageCenterContainer" class="aboutPage residentsPage" >
        <div class="pageTitle frPageTitle fr1Color" style="border-color: white;"><h2>About Us</h2></div>
        <%--<div class="aboutUsImage ml5p"></div>--%>
        <div class=""><img src="/Images/AboutEfxBuilding.png" class="aboutUsImage" style="width: 100%; height: auto;" /></div>
        <%--<article class="fullrow quote fr1Color ml5p w100p">--%>
        <article class="fullrow quote fr1Color w100p" style="border-color: white;">
            <h2 class="fr1Color">“Our vision is to deliver customized payment solutions using innovation and technology to meet the evolving needs of our current and prospective clients.”</h2>
        </article>


            <div class="fullrow">
            <div class="aboutColumn50 borderRight ml5p w40p" style="border-color: lightgray;">
                <article >
                    <h2 style="#06cef9 !important;" >About RPO</h2>
                    <p class="frBlack">Our mission statement is just three words ...  
                        <span class="bolded " style="font-style:italic; display: inline-block; margin: 10px auto 10px auto; width: 100%; text-align: center;" >Make It Easy</span>  
                        We want to make the process of paying rent for residents to be the 
                        easiest thing they do all month, so we provide the simplest to use applications for rent payments, accessible through the most convenient
                         methods possible.</p>
                    <p class="frBlack">On-line, mobile, AutoPay, text messaging, cash, even by telephone.  
                        Whatever <span class="bolded " style="font-style:italic;">Makes It Easy</span> for residents to pay their rent.</p>
                    <p class="frBlack" >We have the same mission statement for our Property Managers, 
                        <span class="bolded " style="font-style:italic;">Make it Easy</span> 
                        for them to track, account for and report on 
                        rent payments, so they can focus more on the business of customer service, rather than collecting rent checks.</p>
                    <p class="frBlack" >Help us with our mission - let us know how we can 
                        <span class="bolded " style="font-style:italic;">Make It Easy</span> for you.</p>

                </article>
            </div>            
            <div class="aboutColumn50 fr2Color w40p">
                <h2 style="">News</h2>
                <asp:ListView runat="server" ID="NewsListView">
                    <LayoutTemplate>
                        <div class="fullrow">
                            <article id="itemPlaceholder" runat="server"></article>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                       <div>

                       
                            <h3><asp:Label ID="TitleLabel" runat="server" CssClass="fr1Color" Text='<%# Bind("AnnouncementTitle") %>' /></h3>
                            
                                <asp:Label ID="ExerptLabel" runat="server" CssClass="frBlack" Text='<%# Bind("TruncateNews") %>' />

                        
                            <p><asp:HyperLink ID="ReadMoreLink" runat="server" CssClass="fr1Color" Text="Read More" NavigateUrl='<%# Bind("AboutUsNews") %>' /></p>
                      </div>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </div>
        <div class="fullrow borderTop  ml5p" style="border-color: lightgray;">
            <div class="aboutColumnThird borderRight " style="border-color: lightgray;">
                <h2>Services</h2>
                <ul>
                    <li class="fr1Color" style="font-size: .8em; line-height: 1.8em;">Credit and Debit Card Processing</li>
                    <li class="fr1Color" style="font-size: .8em; line-height: 1.8em;">One-Time and Recurring eCheck processing</li>
                    <li class="fr1Color" style="font-size: .8em; line-height: 1.8em;">Mobile Processing For Residents and Managers</li>
                    <li class="fr1Color" style="font-size: .8em; line-height: 1.8em;">Cash Payments</li>
                    <li class="fr1Color" style="font-size: .8em; line-height: 1.8em;">Prepaid Debit Cards</li>
                    <li class="fr1Color" style="font-size: .8em; line-height: 1.8em;">Next Day or Second Day Funding</li>
                    <li class="fr1Color" style="font-size: .8em; line-height: 1.8em;">On-Demand Maintenance Requests</li>
                    <li class="fr1Color" style="font-size: .8em; line-height: 1.8em;">Credit Bureau Rent Reporting</li>
                    <li class="fr1Color" style="font-size: .8em; line-height: 1.8em;">Integrated Marketing</li>
                    <li class="fr1Color" style="font-size: .8em; line-height: 1.8em;">Cross-Functional Reporting</li>
                    <li class="fr1Color" style="font-size: .8em; line-height: 1.8em;">Dedicated Client and Resident Support</li>
                </ul>
            </div>
            <div class="aboutColumnThird borderRight " style="border-color: lightgray;">
                <h2 class="textCenter">Career Opportunities</h2>
                <div class="textCenter">
                    <asp:ListView runat="server" ID="JobPostings">
                        <LayoutTemplate>
                            <div class="fullrow">
                                <article id="itemPlaceholder" runat="server"></article>
                            </div>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <article>
                                <h3><asp:Label ID="JobTitle" runat="server" CssClass="fr1Color" Text='<%# Bind("JobTitle") %>' /></h3>
                                <p class="frBlack"><asp:Label ID="JobDescription" runat="server" CssClass="frBlack" Text='<%# Bind("TruncateBlog")%>' /></p>
                                <p><asp:HyperLink ID="ViewJobLink" CssClass="linkfr2Color" runat="server" Text="View Posting" NavigateUrl='<%# Bind("ViewJobPostingUrl") %>' /></p>
                            </article>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <p class="frBlack"><asp:Label ID="NoJobsMessage"  Text="No Career Opportunities Available" runat="server" /></p>
                        </EmptyDataTemplate>
                    </asp:ListView>
                    <p><asp:LinkButton ID="ViewJobsPage" PostBackUrl="~/Jobs.aspx" CssClass="viewJobs" Text="View Job Descriptions" runat="server" />
                </div>
            </div>
            <div class="aboutColumnThird">
                <h2>Partnerships</h2>
                <div class="logoContainerCentered">
                    <div class="yardiLogo"></div>
                    <div class="jumioLogo"></div>
                    <%--<div class="spindleLogo"></div>--%>
                    <div class="rentReportersLogo"></div>
                    <div class="messageMediaLogo"></div>               
                </div>
            </div>
        </div>
    </div>


    <script>

        // calc margin left to center container
        function calc_container() {

            var w_width = $(window).width();
            if ($(window).width() >= 1024) {
                var vML = (w_width - 1024) / 2;
            }
            $("#pageCenterContainer").css("margin-left", vML);
            $("#pageCenterContainer").css("margin-right", vML);
        } // end of calc....


        $(document).ready(function () {
            /* */
            var vObj = document.getElementById("header");
            $(".residentsPage").css("position", "absolute");
            $(".residentsPage").css("top", "100px");
            var vHeight = "height: " + ($("#pageCenterContainer").height() + 200) + "px !important";
            //$('.wrapper').attr('style', vHeight);
            vHeight = "position: absolute; top: " + ($("#pageCenterContainer").height() + 200) + "px";
            //$('#footer').attr('style', vHeight);

            calc_container();
        });


        // Listen for resize changes
        window.addEventListener("resize", function () {

            calc_container();
        }, false);


        // set menu to go back to default page
        $("#panelMobile, #panelMgr, #panelResident, #panelAbout, #panelContact").click(function (evt) {

            evt.preventDefault();
            var vHash = $(this).attr('href');
            window.location.href = "/Default.aspx" + vHash;

        });
    </script>
</asp:Content>
