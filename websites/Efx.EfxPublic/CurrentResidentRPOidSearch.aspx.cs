﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


namespace Efx.EfxPublic
{
    public partial class CurrentResidentRPOidSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ID = Request.QueryString["PropId"];





        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string PropertyID = Request.QueryString["PropId"];
            string FN = FirstNametxt.Text;
            string LN = LastNametxt.Text;
            decimal RA = decimal.Parse(RentAmounttxt.Text);

            int propID = Int32.Parse(PropertyID);

            RPOIDSearchResultGrid.DataSource = GetRPOIdSearchResults(propID, FN, LN, RA);
            RPOIDSearchResultGrid.DataBind();

            if (RPOIDSearchResultGrid.Rows.Count == 0)
            {
                Resultlbl.Text = "We're sorry, but we are unable to locate a profile based on the information you provided.<br />Please contact our support department at 1-855-PMY-RENT or your Property Manager to obtain your ID.";
            }
            else
            {
                Resultlbl.Text = "Please select your name to continue";
            }

            
        }





        public SqlDataReader GetRPOIdSearchResults(int propid, string firstname, string lastname, decimal rentamount)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Renter_GetRenterPMSID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropID", SqlDbType.Int).Value = propid;
            com.Parameters.Add("@FirstName", SqlDbType.NVarChar, 100).Value = firstname;
            com.Parameters.Add("@LastName", SqlDbType.NVarChar, 100).Value = lastname;
            com.Parameters.Add("@RentAmount", SqlDbType.Decimal).Value = rentamount;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);

            }
            catch
            {
                con.Close();
                return null;
            }

        }


        //cakel: BUGID00202 - Added Connection String
        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }



    }
}