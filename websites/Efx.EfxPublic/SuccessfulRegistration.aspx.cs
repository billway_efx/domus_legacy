﻿using EfxFramework;
using EfxFramework.Web;
using System;
using System.Web;
using Mb2x.ExtensionMethods;

namespace Efx.EfxPublic
{
    public partial class SuccessfulRegistration : BasePageV2
    {
        public string PublicResidentId { get { return !string.IsNullOrEmpty(Request.QueryString["PublicResidentId"]) ? Request.QueryString["PublicResidentId"] : null; } }

		private static int PropId
		{
			get
			{
				var Id = HttpContext.Current.Request.QueryString["PropId"].ToNullInt32();
				return Id.HasValue ? Id.Value : 0;
			}
		}

        protected void Page_Load(object sender, EventArgs e)
        {
            SuccessNowLogInButton.PostBackUrl = Settings.RenterLoginLink;

            if (IsIntegratedProperty() || RenterHasLease())
                ButtonInMessage.SetActiveView(ViewWithButton);
            else
                ButtonInMessage.SetActiveView(ViewWithoutButton);
        }

        private bool RenterHasLease()
        {
            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(GetResidentInternalIdByPublicId());

            return Lease.LeaseId > 0;
        }

        private int GetResidentInternalIdByPublicId()
        {
			int? RenterId = null;
            long ResidentId;

			if (!Int64.TryParse(PublicResidentId, out ResidentId))
			{
				var currentProperty = new Property(PropId);
				if (!string.IsNullOrEmpty(currentProperty.PmsId) && currentProperty.PmsTypeId.HasValue)
				{
                    //Salcedo - 2/20/2014 - add PropertyId to call
					Renter retrievedRenter = Renter.GetRenterByPmsIdAndPmsTypeId(PublicResidentId, currentProperty.PmsTypeId.Value, PropId);
					if (retrievedRenter != null && retrievedRenter.RenterId > 0 && retrievedRenter.PmsPropertyId == currentProperty.PmsId)
					{
						return retrievedRenter.RenterId;
					}
				}
			}
			else
			{
				RenterId = Renter.GetRenterIdByPublicRenterId(ResidentId);
			}

            return !RenterId.HasValue ? 0 : RenterId.Value;
        }

        private bool IsIntegratedProperty()
        {
            var Property = GetResidentProperty(GetResidentInternalIdByPublicId());

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                return false;

            return Property.PmsTypeId.HasValue && !String.IsNullOrEmpty(Property.PmsId);
        }

        private static Property GetResidentProperty(int residentId)
        {
            return Property.GetPropertyByRenterId(residentId);
        }
    }
}