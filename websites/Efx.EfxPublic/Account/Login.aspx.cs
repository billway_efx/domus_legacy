﻿using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using System;
using System.Globalization;
using System.Web.Security;
using System.Web.UI;

namespace Efx.EfxPublic.Account
{
    public partial class Login : BasePageV2, ILoginV2
    {
        public string UsernameText { get { return InputUserNameTextBox.Text; } }
        public string PasswordText { get { return InputPasswordTextBox.Text; } }
        public int LoggedInUserId { set { Redirect(value); } }
        public EventHandler LoginButtonClick { set { LoginButton.Click += value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            InputUserNameTextBox.Focus();
            var Presenter = new LoginPresenter<Applicant>(this);
        }

        protected void Redirect(int userId)
        {
            FormsAuthentication.RedirectFromLoginPage(userId.ToString(CultureInfo.InvariantCulture), true);
        }

        protected void ForgotPasswordLinkButtonClick(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "test", "<script type='text/javascript'>ShowModal(\"#ForgotPassword\");</script>", false);
        }
    }
}