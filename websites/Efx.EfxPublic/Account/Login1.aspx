﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login1.aspx.cs" Inherits="Efx.EfxPublic.Account.Login1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div class="loginCore" >

    <div class="pageContainer loginPage login1">
        <div class="loginTitle"><h2>Login</h2></div>
                
    <section class="section95">
        <div class="formHolder formContainer login1Container">
        <!--**First Row**-->
        <div class="formRow">
            <!--User Name Text Box -->
            <div class="formFullWidth">
                <span class="formLable login1Label">Email Address (username)</span>
                <asp:TextBox ID="InputUserNameTextBox" runat="server" CssClass="formFullWidth"></asp:TextBox>
            </div>
        </div>
               
        <!--**Second Row**-->
        <div class="formRow">
            <!--Password Text Box -->
            <div class="formFullWidth">
                <span class="formLable login1Label">Password</span>
                <asp:TextBox ID="InputPasswordTextBox" runat="server" TextMode="Password" CssClass="formFullWidth passwordField mb-0"></asp:TextBox>
            </div>
        </div>
                
        <div class ="displayNone">
            <div id="ForgotPassword" >            
                <uc1:ForgotPasswordWindow runat="server" ID="ForgotPasswordWindow" />
            </div>
        </div>

        <asp:LinkButton ID="ForgotPasswordLinkButton" runat="server" CausesValidation="False" CssClass="smallLink fp-color" OnClick="ForgotPasswordLinkButtonClick">Forgot Password?</asp:LinkButton>
        
        <!--**Button Time**-->
        <div class="generalButtonHolder">
                <asp:Button ID="LoginButton" runat="server" CssClass="orangeButton currentResidentsSubmit loginButton" Text="Log In"></asp:Button>
        </div>
        </div> 

            <div style="margin-top: 50px" class="register">
                 <fieldset class="login-br2" >
                      <legend style="margin-left: 10px;" >
                          <span class="formLabel ntr-color">&nbsp; Need to register? &nbsp;</span>
                      </legend>
                      <p>Please click the sign in button.</p>
                      <div class="divSignup">
                          <asp:Button ID="SignUpBtn" runat="server" Text="Sign In" CssClass="signupSubmit h-28" PostBackUrl="http://www.rentpaidonline.comxx/CurrentResidents.aspx" CausesValidation="False" />
                      </div>
                  </fieldset>
            </div>
    </section>                                    
    </div>
</div>

    <script>
        $(function () {

            // move footer to the bottom of page
            $("footer").addClass("footer");

        });
    </script>
</asp:Content>
