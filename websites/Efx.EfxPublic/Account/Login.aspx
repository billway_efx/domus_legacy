﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Efx.EfxPublic.Account.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="pageContainer loginPage">
        <div class="pageTitle"><h2>Login</h2></div>
    <!-- Already Registered, login from -->
                
            <section class="sectionSixty">
                <div class="formHolder formContainer loginContainer">
        <!--**First Row**-->
        <div class="formRow">
            <!--User Name Text Box -->
            <div class="formFullWidth">
                <span class="formLable">Email Address (username)</span>
                <asp:TextBox ID="InputUserNameTextBox" runat="server" CssClass="formFullWidth"></asp:TextBox>
            </div>
        </div>
               
        <!--**Second Row**-->
        <div class="formRow">
            <!--Password Text Box -->
            <div class="formFullWidth">
                <span class="formLable">Password</span>
                <asp:TextBox ID="InputPasswordTextBox" runat="server" TextMode="Password" CssClass="formFullWidth passwordField"></asp:TextBox>
            </div>
        </div>
                
        <div class ="displayNone">
            <div id="ForgotPassword">            
                <uc1:ForgotPasswordWindow runat="server" ID="ForgotPasswordWindow" />
            </div>
        </div>

        <asp:LinkButton ID="ForgotPasswordLinkButton" runat="server" CausesValidation="False" CssClass="smallLink" OnClick="ForgotPasswordLinkButtonClick">Forgot Password?</asp:LinkButton>
        
        <!--**Button Time**-->
        <div class="generalButtonHolder">
                <asp:Button ID="LoginButton" runat="server" CssClass="orangeButton currentResidentsSubmit" Text="Log in"></asp:Button>
        </div>
                </div> 
           </section>                                    
    </div>
</asp:Content>
