﻿using System.Collections.Generic;
using EfxFramework;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.ViewPresentation.Presenters.Pages;
using EfxFramework.Web;
using System;
using Mb2x.ExtensionMethods;

namespace Efx.EfxPublic
{
    public partial class FutureResidentsApplicationReceipt : BasePageV2, IFutureResidentsApplicationReceipt
    {
        public int PropertyId { get { return !string.IsNullOrEmpty(Request.QueryString["PropId"]) ? Request.QueryString["PropId"].ToInt32() : 0; } }
        public string PropertyName { set { PropertyNameLabel.Text = value; } }
        public string TransactionId { get { return !string.IsNullOrEmpty(Request.QueryString["TransactionId"]) ? Request.QueryString["TransactionId"] : null; } }
        public int ApplicantId { get { return !string.IsNullOrEmpty(Request.QueryString["ApplicantId"]) ? Request.QueryString["ApplicantId"].ToInt32() : 0; } }
        public string UploadStatus { set { UploadStatusLabel.Text = value; } }
        public List<ApplicationFee> Fees { set { FeesList.DataSource = value; FeesList.DataBind(); } }
        public EventHandler PrintClick { set { PrintReceiptButton.Click += value; } }
        public string StatusButtonNavigateUrl { set { ViewStatusOfApplicationButton.PostBackUrl = value; } }
        //CMallory - Task 0499
        public string ApplyConnectButtonNavigateUrl { set { ApplyConnectButton.PostBackUrl = value; } }

        private FutureResidentsApplicationReceiptPresenter _Presenter;

        protected void Page_Load(object sender, EventArgs e)
        {
            TransactionIdLabel.Text = TransactionId;
            _Presenter = new FutureResidentsApplicationReceiptPresenter(this);

            if (!Page.IsPostBack)
                _Presenter.InitializeValues();

            //CMallory - Task 00499 - Added If/Else statement.  If the property uses Apply Connect, the user will be transferred to Apply Connect's site once their payment has been processed.  If not, they'll be directed to the receipt page.
            ApplyConnectButton.Visible = false;
            //CMallory - Task 00499 - Retrieve property object based on Property ID.
            Property propertyRetrieved = new Property(PropertyId);
               
            if (propertyRetrieved.UsesApplyConnect)
             {
                 ViewStatusOfApplicationButton.Visible = false;
                 ApplyConnectButton.Visible = true;                   
             }
        } 
    }
}