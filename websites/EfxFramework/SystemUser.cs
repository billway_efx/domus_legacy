﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using EfxFramework.Data;
using EfxFramework.Security.Authentication;


namespace EfxFramework
{
    [Obsolete]
    public class SystemUser : BaseEntity
    {
        //Public Enums
        public enum SystemRole
        {
            [Description("Unassigned")]
            Unassigned = 0,
            [Description("System Administrator")]
            SystemAdministrator = 1,
            [Description("Sales Manager")]
            SalesManager = 2,
            [Description("Sales Person")]
            SalesPerson = 3,
            [Description("Property Owner-Manager")]
            Property = 4,
            [Description("Renter")]
            Renter = 5
        }

        //Public Properties
        public int? SystemUserId { get; set; }
        public string UserName { get; set; }
        public byte[] PasswordHash { get; private set; }
        public byte[] Salt { get; private set; }
        public SystemRole SystemRoleId { get; set; }

        //Public Methods
        public bool AuthenticateUser(string password)
        {
            return AuthenticationTools.ValidatePasswordHash(password, PasswordHash, Salt);
        }
        public static SystemUser Get(int? systemUserId)
        {
            return new SystemUser(systemUserId);
        }
        public static List<SystemUser> GetAllSystemUserList()
        {
            return DataAccess.GetTypedList<SystemUser>(
                Settings.ConnectionString,
                "dbo.usp_SystemUser_GetAllSystemUserList"
                );
        }
        public static SystemUser GetByUserName(string userName)
        {
            //Get the system user id by the username
            var CurrentSystemUserId = GetSystemUserIdFromUserName(userName);

            //Return
            return CurrentSystemUserId.HasValue ? new SystemUser(CurrentSystemUserId) : null;
        }
        public SystemUserSecurity GetSystemUserSecurity()
        {
            var CurrentSystemUserSecurity = SystemUserSecurity.GetSystemUserSecurityBySystemUserId(SystemUserId);

            //Return
            return CurrentSystemUserSecurity;
        }
        public void ResetPassword()
        {
            //Set a random temporary password for the user
            var TempPassword = SetTemporaryPassword();

            //Save the change
            Set(this);

            //Email the temporary password and link to the user
            // TODO: Uncomment this when email logic is finalized
            //SendPasswordResetEmail(TempPassword);
        }
        public static void Set(SystemUser s)
        {
            Set(s.SystemUserId, s.UserName, s.PasswordHash, s.Salt, s.SystemRoleId);
        }
        public static void Set(int? systemUserId, string userName, byte[] passwordHash, byte[] salt, SystemRole systemRoleId)
        {
            DataAccess.ExecuteNoResults(
                Settings.ConnectionString,
                "dbo.usp_SystemUser_SetSingleObject",
                new[]
				{
					new SqlParameter("@SystemUserId", systemUserId),
					new SqlParameter("@UserName", userName),
					new SqlParameter("@PasswordHash", passwordHash),
					new SqlParameter("@Salt", salt),
					new SqlParameter("@SystemRoleId", systemRoleId)
				});
        }
        public void SetPassword(string newPassword)
        {
            //Generate a new salt
            Salt = AuthenticationTools.GenerateSalt();

            //Hash the password
            PasswordHash = AuthenticationTools.HashPassword(newPassword, Salt);
        }
        
        //Private Methods
        private static int? GetSystemUserIdFromUserName(string userName)
        {
            return DataAccess.ExecuteScalar<int?>(Settings.ConnectionString, "dbo.usp_SystemUser_GetSystemUserIdFromUserName", new SqlParameter("@UserName", userName));
        }
        private void SendPasswordResetEmail(string tempPassword)
        {
            // TODO: Add code to send the email
            // Use App Setting for the host
            // “/Account/ChangePassword.aspx”
            // Will this use the bulk mail engine?

            using (var Message = new MailMessage())
            {
                Message.From = new MailAddress("donotreply@rentpaidonline.com");
                Message.To.Add(UserName);
                Message.Subject = Settings.PasswordResetEmailSubject;
                Message.Body = Settings.GetPasswordResetEmailBody(tempPassword);

                using (var Smtp = new SmtpClient(Settings.SmtpHost, Settings.SmtpPort))
                {
                    Smtp.UseDefaultCredentials = false;
                    Smtp.Credentials = new NetworkCredential("SmtpUsername", "SmtpPassword");
                    Smtp.EnableSsl = true;
                    Smtp.Send(Message);
                }
            }
        }
        private string SetTemporaryPassword()
        {
            //Generate a new salt
            Salt = AuthenticationTools.GenerateSalt();

            //Generate a new random password
            var TempPassword = AuthenticationTools.GenerateRandomPassword();

            //Hash the new password
            PasswordHash = AuthenticationTools.HashPassword(TempPassword, Salt);

            return TempPassword;
        }

        //Constructors
        public SystemUser()
        {
            InitializeObject();
        }
        public SystemUser(int? systemUserId)
        {
            //Get the information from the database
            var Results = DataAccess.ExecuteOneRow(Settings.ConnectionString, "dbo.usp_SystemUser_GetSingleObjectById", new SqlParameter("@SystemUserId", systemUserId));

            if (Results == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
                SystemUserId = Results[0] as int?;
                UserName = Results[1] as string;
                PasswordHash = Results[2] as byte[];
                Salt = Results[3] as byte[];
                SystemRoleId = (SystemRole)Results[4];
            }
        }
    }
}
