using Mb2x.Data;
using Mb2x.ExtensionMethods;
using System;
using System.ComponentModel;
using System.Data.SqlClient;

namespace EfxFramework
{
    public class PropertyManagerProspect : BaseEntity
    {
        //Private Members
        private TelephoneNumber _PropertyPhoneNumber;
        private string _ContactFullName;

        //Public Enumerations
        public enum ProspectSource
        {
            [Description("Send Me Information")]
            SendMeInformation = 1,
            [Description("Inform My Manager")]
            InformMyManager = 2
        }

        //Public Properties
        public int PropertyManagerProspectId { get; set; }

        public string ContactFullName
        {
            get
            {
                return String.IsNullOrEmpty(_ContactFullName) ? String.Format("{0} {1}", ContactFirstName, ContactLastname) : _ContactFullName;
            }
            set
            {
                _ContactFullName = value;
            }
        }
        public string ContactFirstName { get; set; }
        public string ContactLastname { get; set; }
        public string ContactEmailAddress { get; set; }
        public string PropertyPhoneNumber
        {
            get
            {
                return _PropertyPhoneNumber == null ? string.Empty : _PropertyPhoneNumber.Digits; 
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _PropertyPhoneNumber = null;
                else
                {
                    if (_PropertyPhoneNumber == null)
                        _PropertyPhoneNumber = new TelephoneNumber(value);
                    else
                        _PropertyPhoneNumber.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }
        public string PropertyEmailAddress { get; set; }
        public string PropertyName { get; set; }
        public int? NumberOfUnits { get; set; }
        public string PropertyManagementCompany { get; set; }
        public string EfxCode { get; set; }
        public bool? WantsToSignUp { get; set; }
        public bool? WantsToBeContacted { get; set; }
        public bool? ContactWhenPropertyParticipating { get; set; }
        public bool? SendListOfProperties { get; set; }
        public ProspectSource ProspectSourceId { get; set; }
        public DateTime DateCreated { get; set; }
        public bool Processed { get; set; }

        public static int Set(int propertyManagerProspectId, string contactFullName, string contactFirstName, string contactLastname, string contactEmailAddress, string propertyPhoneNumber, string propertyEmailAddress, string propertyName, int? numberOfUnits, string propertyManagementCompany, string efxCode, bool? wantsToSignUp, bool? wantsToBeContacted, bool? contactWhenPropertyParticipating, bool? sendListOfProperties, ProspectSource prospectSourceId, DateTime dateCreated, bool processed)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
                EfxSettings.ConnectionString,
                "dbo.usp_PropertyManagerProspect_SetSingleObject",
            new[]
                    {
	                    new SqlParameter("@PropertyManagerProspectId", propertyManagerProspectId),
	                    new SqlParameter("@ContactFullName", contactFullName),
	                    new SqlParameter("@ContactFirstName", contactFirstName),
	                    new SqlParameter("@ContactLastname", contactLastname),
	                    new SqlParameter("@ContactEmailAddress", contactEmailAddress),
	                    new SqlParameter("@PropertyPhoneNumber", propertyPhoneNumber.EmptyStringToNull()),
	                    new SqlParameter("@PropertyEmailAddress", propertyEmailAddress),
	                    new SqlParameter("@PropertyName", propertyName),
	                    new SqlParameter("@NumberOfUnits", numberOfUnits),
	                    new SqlParameter("@PropertyManagementCompany", propertyManagementCompany),
	                    new SqlParameter("@EfxCode", efxCode),
	                    new SqlParameter("@WantsToSignUp", wantsToSignUp),
	                    new SqlParameter("@WantsToBeContacted", wantsToBeContacted),
	                    new SqlParameter("@ContactWhenPropertyParticipating", contactWhenPropertyParticipating),
	                    new SqlParameter("@SendListOfProperties", sendListOfProperties),
	                    new SqlParameter("@ProspectSourceId", prospectSourceId),
	                    new SqlParameter("@DateCreated", dateCreated),
	                    new SqlParameter("@Processed", processed)
                    });
        }

        public static int Set(PropertyManagerProspect p)
        {
            return Set(
                p.PropertyManagerProspectId,
                p.ContactFullName,
                p.ContactFirstName,
                p.ContactLastname,
                p.ContactEmailAddress,
                p.PropertyPhoneNumber,
                p.PropertyEmailAddress,
                p.PropertyName,
                p.NumberOfUnits,
                p.PropertyManagementCompany,
                p.EfxCode,
                p.WantsToSignUp,
                p.WantsToBeContacted,
                p.ContactWhenPropertyParticipating,
                p.SendListOfProperties,
                p.ProspectSourceId,
                p.DateCreated,
                p.Processed
            );
        }

        public PropertyManagerProspect(int? propertyManagerProspectId)
        {
            //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_PropertyManagerProspect_GetSingleObject", new[] { new SqlParameter("@PropertyManagerProspectId", propertyManagerProspectId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
                PropertyManagerProspectId = (int)ResultSet[0];
                ContactFullName = ResultSet[1] as string;
                ContactFirstName = ResultSet[2] as string;
                ContactLastname = ResultSet[3] as string;
                ContactEmailAddress = ResultSet[4] as string;
                PropertyPhoneNumber = ResultSet[5] as string;
                PropertyEmailAddress = ResultSet[6] as string;
                PropertyName = ResultSet[7] as string;
                NumberOfUnits = ResultSet[8] as int?;
                PropertyManagementCompany = ResultSet[9] as string;
                EfxCode = ResultSet[10] as string;
                WantsToSignUp = ResultSet[11] as bool?;
                WantsToBeContacted = ResultSet[12] as bool?;
                ContactWhenPropertyParticipating = ResultSet[13] as bool?;
                SendListOfProperties = ResultSet[14] as bool?;
                ProspectSourceId = (ProspectSource) ResultSet[15];
                DateCreated = (DateTime)ResultSet[16];
                Processed = (bool)ResultSet[17];
            }
        }

        public PropertyManagerProspect()
        {
            InitializeObject();
        }

    }
}
