﻿namespace EfxFramework
{
    public static class ServiceResponseMessages
    {
        #region General
        public const string GeneralFailure = "Failed to process the request";
        public const string Processsuccess = "Transaction completed successfully";
        public const string Processfail = "Transaction failed";
        public const string Successfullogin = "The user logged in successfully.";
        public const string Failedlogin = "The user failed to login.";
        public const string Successfullogout = "The user was logged out successfully.";
        public const string Sessiontimeout = "Session timed out";
        public const string Usernotfound = "User not found";
        public const string Userfound = "User successfully retrieved";
        public const string Invalidpaymentamount = "Invalid payment amount supplied";
        public const string LeaseNotFound = "No lease found for renter";
        public const string PropertyNotFound = "No property found for lease";
        public const string ZeroAmount = "Payment of $0.00 not processed";
        public const string CannotAcceptRenterPayment = "Cannot accept payments for this resident";
        public const string CashEquivalentRequired = "Can only accept Credit Card payments for this resident";
        public const string PayerCreditCardNotFound = "Cannot find the Payer's Credit Card record";
        public const string PayerAchNotFound = "Cannot find the Payer's ECheck record";
        public const string PaymentFailedToRecord = "The Payment record failed to write to the database";
        public const string InvalidSessionId = "The Session Id is invalid";
        public const string InvalidRenterId = "The Renter Id is invalid or unauthorized.";
        public const string InvalidExpiration = "Invalid expiration date.";
        public const string InvalidPaymentMethodId = "Invalid Payment Method Id";
        public const string InvalidPayableItemId = "Invalid Payable Item Id";
        public const string InvalidAccountTypeId = "Invalid AccountType Id";
        public const string InvalidInternalId = "Invalid Internal Id";
        public const string InvalidMaintenanceTypeId = "Invalid Maintenance Type Id";
        public const string InvalidMaintenancePriorityId = "Invalid Maintenance Priority Id";
        public const string InvalidCcResident = "Invalid CC Resident. Must be 0 or 1.";
        public const string InvalidPayableItemsId = "Invalid Payable Items Id";
        public const string InvalidPayerWalletId = "Invalid Payer Wallet Id";
        public const string InvalidPaymentAmount = "Invalid Payment Amount";
        public const string InvalidSplitPaymentGroupId = "Invalid Split Payment Group Id";
        public const string InvalidSplitPaymentGroupCompleteFlag = "Invalid Split Payment Group Complete Flag";
        public const string InvalidRoutingNumber = "Invalid routing number";
        public const string TransactionNotEligible = "Transaction not eligible for requested action";
        #endregion

        #region Expiration Date Validation Values
        public const string Invalidexpirationlength = "Invalid expiration date length";
        public const string Invalidexpirationcharacters = "Invalid characters in expiration date";
        public const string Invalidexpirationmonth = "Invalid Expiration Month";
        public const string Invalidexpirationyear = "Invalid Expiration Year";
        public const string Validexpiration = "Expiration Date Valid";
        #endregion

        #region Credit Card Decline Reasons
        public const string Bankdeclined = "Bank declined the transaction";
        public const string Avsfailed = "Address verification failed";
        public const string Cvvfailed = "Incorrect CVV";
        public const string Call = "The card must be authorized manually over the phone";
        public const string Expiredcard = "The card has expired";
        public const string Carderror = "The card number is invalid";
        public const string Authexpired = "Preauth has expired prior to Postauth attempt";
        public const string Tclinkfraud = "Fraud score was below TC CrediGuard threshold";
        public const string Tclinkblacklist = "Account blacklisted by TC CrediGuard";
        public const string Tclinkvelocity = "TC CrediGuard velocity control triggered";
        public const string Declinedefault = "Card declined for unknown reasons";
        public const string CcProcessingBusy = "Credit card gateway was unresponsive, please try your payment again later";
        #endregion

        #region Credit Card Bad Data Reasons
        public const string Missingfields = "One or more required fields were not sent-Offending Fields: ";
        public const string Extrafields = "Fields not allowed for this transaction were sent-Offending Fields: ";
        public const string Badformat = "A field was improperly formatted-Offending Fields: ";
        public const string Badlength = "A field was not of the correct length to process-Offending Fields: ";
        public const string Merchantcantaccept = "The merchant cannot accept data passed in this field-Offending Fields: ";
        public const string Mismatch = "Data in one of the offending fields did not crosscheck with the other offending field-Offending Fields: ";
        public const string Baddatadefault = "Unknown bad data type";
        #endregion

        #region Credit Card Error Reasons
        public const string Cantconnnect = "Could not connect to TrustCommerce";
        public const string Dnsvalue = "The TCLink software was not able to resolve DNS hostnames";
        public const string Linkfailure = "The connection was established but severed before the transaction could complete. To avoid duplication, check the transaction indepenedently to determine if the transaction was successfully processed";
        public const string Failtoprocess = "Transmission errors were encountered in TrustCommerce's connection to the payment processor";
        public const string Notallowed = "Action not permitted for this CustID";
        public const string Errordefault = "An unknown error occured";
        public const string ProcessorDown = "We sincerely apologize, but our credit card processor is currently down. In the interim, please consider making your payment using our E-Check payment method.";
        #endregion

        #region Base Credit Card Responses
        public const string Ccaccepted = "The credit card processing center is currently busy, please try your transaction again later";
        public const string Ccdecline = "The transaction was declined";
        public const string Ccbaddata = "Bad data was used in the transaction attempt";
        public const string Ccerror = "There was an error processing the transaction";
        public const string Ccpayernotfound = "No valid payer found";
        public const string Ccpaymentamountinvalid = "Payment amount invalid";
        public const string Ccvoidsuccessful = "Transaction successfully voided";
        public const string Ccrefundsuccessful = "Transaction successfully refunded";
        public const string MissingCardholderName = "Cardholder name not entered";
        public const string MissingCardNumber = "Card Number not entered";
        public const string MissingCvv = "Cvv Code not entered";
        public const string CcReverseSuccessful = "Transaction reversal was successful";
        #endregion

        #region PaymentHistoryResponses
        public const string Paymenthistoryretrieved = "Payment History Retrieved";
        #endregion

        #region Mobile Payment Responses
        public static string UnknownPaymentType(string amount) { return string.Format("Unknown Payment type in the amount of {0} failed", amount); }
        public static string UrlNotFoundForSpecifiedVersion { get { return "No URL found for the specified version number"; } }
        public static string UrlRetrievedSuccessfully { get { return "Url Successfully Retrieved"; } }
        #endregion

        #region Ivr Payment Responses
        public const string IvrSuccess = "Successfully processed the Payment Queue for the IVR Payment";
        public const string InvalidRenterPin = "Invalid PIN code entered for renter";
        public const string NoQueuedPaymentsFound = "No queued web payments have been found associated with this Account Code";
        public const string UnknownPaymentTypeEntered = "Unknown payment type";
        #endregion

        #region ACH Processing Responses
        public const string Invalidroutingnumber = "Invalid routing number, transaction not processed";
        public const string Missingachclientid = "Missing ACH Client ID, transaction not processed";
        public const string Missingtransactionid = "Missing Transaction ID, transaction not processed";
        public const string Missingaccountnumber = "Account number missing, transaction not processed";
        public const string MissingRoutingNumber = "Routing number not entered";
        public const string AddressNotFound = "No Address for the Resident was supplied transaction not processed";
        //cakel: BUGID 00389
        public const string InvalidAccountNumber = "Invalid Account number - Account number not formatted well and or contains symbols";
        public const string DuplicateAchTransaction = "Duplicate ACH transaction was recently processed for this account.";
        #endregion

        #region SMS Processing Responses
        public const string PhoneNumberNotFound = "The phone number received is not on record, please register your number prior to making a text payment, thank you.";
        #endregion

        #region PropertyManagerResponses

        public const string RentersSuccessfullyRetrieved = "Request for Renter Data Successful";
        public const string RenterNotFound = "Renter not found";
        public const string RenterSuccessfullyUpdated = "Successfully updated the renter account";

        #endregion

        #region ApplicantResponses
        public const string ApplicantNotFound = "Applicant applying to this property was not found";
        public const string ApplicationNotFound = "Application for residency for this applicant and property was not found";
        #endregion

        #region Maintenance Responses
        public const string RenterIdRequired = "A Renter ID is required";
        public const string MaintenanceTypeIdRequired = "A Maintenance Type ID is required";
        public const string MaintenancePriorityRequired = "A Maintenance Priority ID is required";
        public const string MaintenanceSubmissionDateRequired = "A Maintenance Submission Date is required";
        public const string MaintenanceSubmissionDateInvalid = "Maintenance Submission Date is not a valid date";
        public const string MaintenanceDescriptionRequired = "A Maintenance Description is required";
        public const string MaintenanceRequestSuccessful = "A Maintenance Request was Successfully Submitted";
        public const string InvalidMaintenanceRequestType = "The Maintenance Type ID provided does not match any of the Types available";
        public const string InvalidMaintenancePriority = "The Maintenance Priority ID provided does not match any of the Priorities available";
        public const string MaintenanceHistorySuccessfullyRetrieved = "Successfully retrieved the Maintenance History for the Resident";
        #endregion
    }
}
