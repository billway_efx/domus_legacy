﻿using System;
using System.Web.Security;
using EfxFramework.Security.Authentication;

namespace EfxFramework.Membership
{
	public class EfxMembershipProvider : MembershipProvider
	{
		public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
		{
			var CurrentSystemUser = new SystemUser();

			//Set the username and password
			CurrentSystemUser.UserName = username;
			CurrentSystemUser.SetPassword(password);
			CurrentSystemUser.SystemRoleId = EfxFramework.SystemUser.SystemRole.Unassigned;

			//Save the user
			SystemUser.Set(CurrentSystemUser);

			status = MembershipCreateStatus.Success;
			return GetUser(username, false);
		}

		public void CreateUser(string username, string password)
		{
			var Status = new System.Web.Security.MembershipCreateStatus();
			CreateUser(username, password, null, null, null, true, null, out Status);
		}

		public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
		{
			return true;
		}

		public override string GetPassword(string username, string answer)
		{
			return string.Empty;
		}

		public override bool ChangePassword(string username, string oldPassword, string newPassword)
		{
			//Attempt to lookup the system user based on the username
			var CurrentSystemUser = SystemUser.GetByUserName(username);

			//Change the password
			CurrentSystemUser.SetPassword(newPassword);

			//Save the change
			SystemUser.Set(CurrentSystemUser);

			//Return
			return true;
		}

		public override string ResetPassword(string username, string answer)
		{
			//Attempt to lookup the system user based on the username
			var CurrentSystemUser = SystemUser.GetByUserName(username);

			//Reset password
			CurrentSystemUser.ResetPassword();

			//Return
			return string.Empty;
		}

		public override void UpdateUser(MembershipUser user)
		{

		}

		public override bool ValidateUser(string username, string password)
		{
			//Attempt to lookup the system user based on the username
			var CurrentSystemUser = SystemUser.GetByUserName(username);

			//Return
			return CurrentSystemUser != null && AuthenticationTools.ValidatePasswordHash(password, CurrentSystemUser.PasswordHash, CurrentSystemUser.Salt);
		}

		public override bool UnlockUser(string userName)
		{
			return true;
		}

		public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
		{
			var u = new MembershipUser(this.Name,
											providerUserKey.ToString(),
											providerUserKey,
											providerUserKey.ToString(),
											string.Empty,
											string.Empty,
											true,
											false,
											DateTime.Now,
											DateTime.Now,
											DateTime.Now,
											DateTime.Now,
											DateTime.MinValue);
			return u;
		}

		public override MembershipUser GetUser(string username, bool userIsOnline)
		{
			var u = new MembershipUser(this.Name,
											username,
											username,
											username,
											string.Empty,
											string.Empty,
											true,
											false,
											DateTime.Now,
											DateTime.Now,
											DateTime.Now,
											DateTime.Now,
											DateTime.MinValue);
			return u;
		}

		public override string GetUserNameByEmail(string email)
		{
			return string.Empty;
		}

		public override bool DeleteUser(string username, bool deleteAllRelatedData)
		{
			return false;
		}

		public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
		{
			totalRecords = 0;
			return null;
		}

		public override int GetNumberOfUsersOnline()
		{
			return -1;
		}

		public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
		{
			totalRecords = 0;
			return null;
		}

		public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
		{
			totalRecords = 0;
			return null;
		}

		public override bool EnablePasswordRetrieval
		{
			get { return false; }
		}

		public override bool EnablePasswordReset
		{
			get { return true; }
		}

		public override bool RequiresQuestionAndAnswer
		{
			get { return false; }
		}

		public override string ApplicationName
		{
			get { return Settings.ApplicationName; }
			set { }
		}

		public override int MaxInvalidPasswordAttempts
		{
			get { return 5; }
		}

		public override int PasswordAttemptWindow
		{
			get { return 10; }
		}

		public override bool RequiresUniqueEmail
		{
			get { return true; }
		}

		public override MembershipPasswordFormat PasswordFormat
		{
			get { return MembershipPasswordFormat.Hashed; }
		}

		public override int MinRequiredPasswordLength
		{
			get { return 4; }
		}

		public override int MinRequiredNonAlphanumericCharacters
		{
			get { return 0; }
		}

		public override string PasswordStrengthRegularExpression
		{
			get { return string.Empty; }
		}
	}
}
