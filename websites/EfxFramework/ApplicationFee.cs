using System;
using EfxFramework.Web.Caching;
using Mb2x.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;

namespace EfxFramework
{
	public class ApplicationFee : BaseEntity
	{
		//Public Properties
		[DisplayName(@"ApplicationFeeId"), Required(ErrorMessage = "ApplicationFeeId is required.")]
		public int ApplicationFeeId { get; set; }

		[DisplayName(@"PropertyId"), Required(ErrorMessage = "PropertyId is required.")]
		public int PropertyId { get; set; }

		[DisplayName(@"FeeName"), Required(ErrorMessage = "FeeName is required."), StringLength(100)]
		public string FeeName { get; set; }

		[DisplayName(@"FeesPaid"), Required(ErrorMessage = "FeesPaid is required.")]
		public decimal FeesPaid { get; set; }

        [DisplayName(@"IsActive"), Required(ErrorMessage = "IsActive is required.")]
        public bool IsActive { get; set; }

        public string FeeAmountText { get { return FeesPaid.ToString("C"); } }
        public string DeleteApplicationFeeUrl { get { return String.Format("~/Handlers/GeneralHandler.ashx?RequestType=11&ApplicationFeeId={0}#RentAndFeesTab", ApplicationFeeId); } }

        public static List<ApplicationFee> GetAllApplicationFees()
        {
            return DataAccess.GetTypedList<ApplicationFee>(EfxSettings.ConnectionString, "dbo.usp_ApplicationFee_GetAllApplicationFees");
        }

        //Salcedo - 2/12/2015 - added handler
        public static List<ApplicationFee> GetApplicationFeesForProperty(int propertyId)
        {
            SqlParameter TheParam = new SqlParameter("@PropertyId", propertyId);
            return DataAccess.GetTypedList<ApplicationFee>(EfxSettings.ConnectionString, "dbo.usp_ApplicationFee_GetByPropertyId", TheParam);
        }

        public static List<ApplicationFee> GetAllApplicationFeesByPropertyId(int propertyId)
        {
            //Salcedo - 2/12/2015 - stupid code - fixed it
            //return GetAllApplicationFees().Where(f => f.PropertyId == propertyId && f.IsActive).ToList();

            return GetApplicationFeesForProperty(propertyId).Where(f => f.PropertyId == propertyId && f.IsActive).ToList();
        }

        public static void DeleteApplicationFee(int applicationFeeId)
        {
            var Fee = new ApplicationFee(applicationFeeId)
                {
                    IsActive = false
                };

            Set(Fee);
        }

        public static int Set(int applicationFeeId, int propertyId, string feeName, decimal feesPaid, bool isActive)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_ApplicationFee_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@ApplicationFeeId", applicationFeeId),
	                    new SqlParameter("@PropertyId", propertyId),
	                    new SqlParameter("@FeeName", feeName),
	                    new SqlParameter("@FeesPaid", feesPaid),
                        new SqlParameter("@IsActive", isActive)
                    });
        }

        public static int Set(ApplicationFee a)
        {
	        return Set(
		        a.ApplicationFeeId,
		        a.PropertyId,
		        a.FeeName,
		        a.FeesPaid,
                a.IsActive
	        );
        }

        public ApplicationFee(int? applicationFeeId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_ApplicationFee_GetSingleObject", new [] { new SqlParameter("@ApplicationFeeId", applicationFeeId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        ApplicationFeeId = (int)ResultSet[0];
		        PropertyId = (int)ResultSet[1];
		        FeeName = ResultSet[2] as string;
		        FeesPaid = (decimal)ResultSet[3];
                IsActive = (bool) ResultSet[4];
            }
        }

		public ApplicationFee()
		{
			InitializeObject();
		}

	}
}
