﻿using System.IO;
using System.Runtime.Serialization.Json;

namespace EfxFramework.Serialization
{
    public static class JsonSerializer
    {
        public static T SerializeJsonResponse<T>(byte[] request)
        {
            using (var MStream = new MemoryStream(request))
            {
                var Serializer = new DataContractJsonSerializer(typeof(T));

                return (T)Serializer.ReadObject(MStream);
            }
        }
    }
}
