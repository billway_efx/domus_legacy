﻿using System;
using EfxFramework.PaymentMethods;

namespace EfxFramework.Ivr
{
    public class UserInformationRequest
    {
        public Renter User { get; private set; }

        public UserInformationRequest() { }

        public UserInformationRequest(string accountCode)
        {
            User = new Renter(PaymentQueue.GetPaymentQueueByPaymentQueueNumber(accountCode).RenterId);
        }

        public UserInformationResponse GetUserInformation(string username, string password, string pin, string accountCode)
        {
            var Response = new UserInformationResponse();

            var Payment = PaymentQueue.GetPaymentQueueByPaymentQueueNumber(accountCode);

            if (User.AccountCodePin != pin)
                return new UserInformationResponse {Result = IvrResponseResult.InvalidRenterPin, ResponseMessage = ServiceResponseMessages.InvalidRenterPin};

            if (!IvrBase.AuthenticateAccount(username, password))
                return new UserInformationResponse { Result = IvrResponseResult.FailedLogin, ResponseMessage = ServiceResponseMessages.Failedlogin };

            if (User != null && User.RenterId > 0)
            {
                var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(User.RenterId);

                if (Lease.LeaseId < 1)
                    return new UserInformationResponse { Result = IvrResponseResult.LeaseNotFound, ResponseMessage = ServiceResponseMessages.LeaseNotFound };

                var Property = new Property(Lease.PropertyId);

                if (Property.PropertyId < 1)
                    return new UserInformationResponse { Result = IvrResponseResult.PropertyNotFound, ResponseMessage = ServiceResponseMessages.PropertyNotFound };

                Response.AmountDue = GetPaymentAmount(Payment);
                Response.ConvenienceFee = AssignConvenienceFee(Payment) ? Decimal.Round(Property.GetConvenienceFeesByProperty(Property).CreditCardFee, 2) : 0.00M;
                Response.TotalAmountDue = Decimal.Round(Response.AmountDue + Response.ConvenienceFee, 2);
                Response.RenterName = User.DisplayName;
                Response.Result = IvrResponseResult.Success;
                Response.ResponseMessage = ServiceResponseMessages.Userfound;
            }
            else
            {
                Response.Result = IvrResponseResult.UserNotFound;
                Response.ResponseMessage = ServiceResponseMessages.Usernotfound;
            }

            return Response;
        }

        private static bool AssignConvenienceFee(PaymentQueue payment)
        {
            if (payment.RentAmount.HasValue && payment.RentAmount.Value > 0.00M)
                return true;
            if (payment.OtherAmount1.HasValue && payment.OtherAmount1.Value > 0.00M)
                return true;
            if (payment.OtherAmount2.HasValue && payment.OtherAmount2.Value > 0.00M)
                return true;
            if (payment.OtherAmount3.HasValue && payment.OtherAmount3.Value > 0.00M)
                return true;
            if (payment.OtherAmount4.HasValue && payment.OtherAmount4.Value > 0.00M)
                return true;
            if (payment.OtherAmount5.HasValue && payment.OtherAmount5.Value > 0.00M)
                return true;

            return false;
        }

        private static decimal GetPaymentAmount(PaymentQueue payment)
        {
            var Result = 0.00M;

            if (payment.RentAmount.HasValue)
                Result += payment.RentAmount.Value;
            if (payment.OtherAmount1.HasValue)
                Result += payment.OtherAmount1.Value;
            if (payment.OtherAmount2.HasValue)
                Result += payment.OtherAmount2.Value;
            if (payment.OtherAmount3.HasValue)
                Result += payment.OtherAmount3.Value;
            if (payment.OtherAmount4.HasValue)
                Result += payment.OtherAmount4.Value;
            if (payment.OtherAmount5.HasValue)
                Result += payment.OtherAmount5.Value;
            if (payment.CharityAmount.HasValue)
                Result += payment.CharityAmount.Value;

            return Decimal.Round(Result, 2);
        }
    }
}
