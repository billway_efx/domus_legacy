﻿using EfxFramework.PaymentMethods;

namespace EfxFramework.Ivr
{
    public class UserInformationResponse
    {
        public IvrResponseResult Result { get; set; }
        public string ResponseMessage { get; set; }
        public string RenterName { get; set; }
        public decimal AmountDue { get; set; }
        public decimal ConvenienceFee { get; set; }
        public decimal TotalAmountDue { get; set; }
    }
}
