﻿using EfxFramework.PaymentMethods;

namespace EfxFramework.Ivr
{
    public class PaymentResponse
    {
        public IvrResponseResult RentAndFeeResult { get; set; }
        public string RentAndFeeResponseMessage { get; set; }
        public string RentAndFeeTransactionId { get; set; }
        public IvrResponseResult CharityResult { get; set; }
        public string CharityResponseMessage { get; set; }
        public string CharityTransactionId { get; set; }
        public decimal ConvenienceFee { get; set; }
    }
}
