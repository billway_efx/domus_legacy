﻿using System;
using EfxFramework.PaymentMethods;

namespace EfxFramework.Ivr
{
    public class PaymentRequestV2 : PaymentProcessingBase
    {
        public PaymentRequestV2(PaymentChannel channel, int propertyId) 
            : base(channel, propertyId) {}

        public PaymentRequestV2(PaymentChannel channel, int propertyId, int residentId) 
            : base(channel, propertyId, residentId) {}

        public static PaymentResponseBase ProcessPayment(PaymentChannel channel, string accountCode, string username, string password, string pin)
        {
            var PaymentQueue = EfxFramework.PaymentQueue.GetPaymentQueueByPaymentQueueNumber(accountCode);
            var Request = new PaymentRequestV2(channel, Property.GetPropertyByRenterId(PaymentQueue.RenterId).PropertyId.HasValue
                ? Property.GetPropertyByRenterId(PaymentQueue.RenterId).PropertyId.Value : 0, PaymentQueue.RenterId);

            if (!IvrBase.AuthenticateAccount(username, password))
                return new PaymentResponseBase { Result = (int)IvrResponseResult.FailedLogin, Message = ServiceResponseMessages.Failedlogin };

            if (Request.Resident.AccountCodePin != pin)
                return new PaymentResponseBase {Result = (int) IvrResponseResult.InvalidRenterPin, Message = ServiceResponseMessages.InvalidRenterPin };

            if (PaymentQueue.PaymentQueueId < 1)
                return new PaymentResponseBase {Result = (int) IvrResponseResult.NoQueuedPaymentsFound, Message = ServiceResponseMessages.NoQueuedPaymentsFound };

            return Request.ProcessResidentPayment(new CreditCardDetails(Request.GetResidentAddress(), Request.Resident.FirstName, Request.Resident.LastName, PaymentQueue.CreditCardHolderName,
                PaymentQueue.CreditCardAccountNumber, new DateTime(PaymentQueue.CreditCardExpirationYear.HasValue ? PaymentQueue.CreditCardExpirationYear.Value : 0, 
                PaymentQueue.CreditCardExpirationMonth.HasValue ? PaymentQueue.CreditCardExpirationMonth.Value : 0, 1), PaymentQueue.CreditCardSecurityCode));
        }

        protected override bool Validate(out PaymentResponseBase response)
        {
            if (!base.Validate(out response))
                return false;

            if (ResidentId < 1)
            {
                response.Result = (int) IvrResponseResult.UserNotFound;
                response.Message = ServiceResponseMessages.Usernotfound;
                return false;
            }

            if (Resident.PayerId < 1)
            {
                response.Result = (int) IvrResponseResult.PayerNotFound;
                response.Message = ServiceResponseMessages.Ccpayernotfound;
                return false;
            }

            if (Resident.AcceptedPaymentTypeId == 4)
            {
                response.Result = (int) IvrResponseResult.CannotAcceptRenterPayment;
                response.Message = ServiceResponseMessages.CannotAcceptRenterPayment;
            }

            return true;
        }
    }
}
