﻿using EfxFramework.PaymentMethods;
using EfxFramework.PaymentMethods.Ach;
using EfxFramework.PaymentMethods.CreditCard;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EfxFramework.PublicApi.Payment;
using System.Net.Mail;
using RPO;

namespace EfxFramework.Ivr
{
    public class PaymentRequest
    {
        public decimal Amount { get; set; }
        public string Description { get; set; }

        public static PaymentResponse ProcessPayment(string accountCode, string username, string password, string pin)
        {
            ActivityLog AL = new ActivityLog();
            AL.WriteLog(username, "IVR PaymentRequest.ProcessPayment initiated.", (short)LogPriority.LogAlways);

            var PaymentQueue = EfxFramework.PaymentQueue.GetPaymentQueueByPaymentQueueNumber(accountCode);
            var Renter = new Renter(PaymentQueue.RenterId) {IsParticipating = true};
            EfxFramework.Renter.Set(Renter, "System Process");

            if (Renter.RenterId < 1)
                return new PaymentResponse { RentAndFeeResult = IvrResponseResult.UserNotFound, RentAndFeeResponseMessage = ServiceResponseMessages.Usernotfound };
            if (!IvrBase.AuthenticateAccount(username, password))
                return new PaymentResponse { RentAndFeeResult = IvrResponseResult.FailedLogin, RentAndFeeResponseMessage = ServiceResponseMessages.Failedlogin };
            if (Renter.AccountCodePin != pin)
                return new PaymentResponse { RentAndFeeResult = IvrResponseResult.InvalidRenterPin, RentAndFeeResponseMessage = ServiceResponseMessages.InvalidRenterPin };
            if (Renter.PayerId < 1)
                return new PaymentResponse {RentAndFeeResult = IvrResponseResult.PayerNotFound, RentAndFeeResponseMessage = ServiceResponseMessages.Ccpayernotfound};
            if (PaymentQueue.PaymentQueueId < 1)
                return new PaymentResponse {RentAndFeeResult = IvrResponseResult.NoQueuedPaymentsFound, RentAndFeeResponseMessage = ServiceResponseMessages.NoQueuedPaymentsFound};
            if (Renter.AcceptedPaymentTypeId == 4)
                return new PaymentResponse {RentAndFeeResult = IvrResponseResult.CannotAcceptRenterPayment, RentAndFeeResponseMessage = ServiceResponseMessages.CannotAcceptRenterPayment};

            var RentPayments = GetRentPayments(PaymentQueue);
            var CharityPayments = GetCharityPayments(PaymentQueue);

            switch (PaymentQueue.PaymentTypeId)
            {
                case 1:
                    return ProcessPayment(RentPayments, CharityPayments, GetPayerCreditCard(PaymentQueue), Renter, PaymentQueue.CreditCardSecurityCode);
                case 2:
                    if (Renter.AcceptedPaymentTypeId == 3)
                        return new PaymentResponse {RentAndFeeResult = IvrResponseResult.CashEquivalentRequired, RentAndFeeResponseMessage = ServiceResponseMessages.CashEquivalentRequired};

                    return ProcessPayment(RentPayments, CharityPayments, GetPayerAch(PaymentQueue), Renter);
                default:
                    return new PaymentResponse {RentAndFeeResult = IvrResponseResult.UnknownPaymentType, RentAndFeeResponseMessage = ServiceResponseMessages.UnknownPaymentTypeEntered};
            }
        }

        private static PaymentResponse ProcessPayment(Dictionary<PaymentApplication, List<PaymentRequest>> rentPayments, IEnumerable<PaymentRequest> charityPayments, PayerAch payerAch, Renter renter)
        {
            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);
            var RentBaseAmount = rentPayments.Values.SelectMany(r => r).Where(r => !r.Description.StartsWith("convenience fee", StringComparison.OrdinalIgnoreCase)).Sum(r => r.Amount);
            var RentPaymentAmount = new PaymentAmount(RentBaseAmount, "Convenience Fee", EfxFramework.PaymentMethods.PaymentType.ECheck, renter.RenterId);
            var CharityBaseAmount = charityPayments.Sum(r => r.Amount);
            var CharityPaymentAmount = new PaymentAmount(CharityBaseAmount);
            var RentAchClientId = GetAchClientId(renter.RenterId, PaymentApplication.Rent.ToString());
            var CharityAchClientId = GetAchClientId(renter.RenterId, PaymentApplication.Charity.ToString());
            var Property = EfxFramework.Property.GetPropertyByRenterId(renter.RenterId);

            var State = "";
            if (renter.StateProvinceId.HasValue)
                State = ((StateProvince) Enum.Parse(typeof (StateProvince), renter.StateProvinceId.Value.ToString(CultureInfo.InvariantCulture))).ToString();

            var RentResponse = new AchPaymentResponse();
            var CharityResponse = new AchPaymentResponse();

            ActivityLog AL = new ActivityLog();
            AL.WriteLog("RenterID: " + renter.RenterId.ToString(), "IVR PaymentRequest.ProcessPayment - ACH - initiated.", (short)LogPriority.LogAlways);

            if (RentPaymentAmount.BaseAmount > 0.0M)
            {
                if (Property.PropertyId.HasValue && Property.PropertyId.Value > 0)
                    RentResponse = AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword, RentAchClientId, RentPaymentAmount.TotalAmount, payerAch.BankRoutingNumber,
                                    payerAch.BankAccountNumber, payerAch.BankAccountType.ToUpper(), renter.FirstName, renter.LastName, renter.StreetAddress, renter.Unit, renter.City, State,
                                    renter.PostalCode, renter.MainPhoneNumber);
                else
                    RentResponse = new AchPaymentResponse {Result = GeneralResponseResult.PropertyNotFound, ResponseDescription = ServiceResponseMessages.PropertyNotFound};
            }

            if (CharityPaymentAmount.BaseAmount > 0.0M)
            {
                CharityResponse = AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword, CharityAchClientId, CharityPaymentAmount.TotalAmount, payerAch.BankRoutingNumber,
                    payerAch.BankAccountNumber, payerAch.BankAccountType.ToUpper(), renter.FirstName, renter.LastName, renter.StreetAddress, renter.Unit, renter.City, State,
                    renter.PostalCode, renter.MainPhoneNumber);
            }

            var Response = new PaymentResponse
                {
                    RentAndFeeResult = ConvertResponseResult(RentResponse.Result),
                    RentAndFeeResponseMessage = RentResponse.ResponseDescription,
                    RentAndFeeTransactionId = RentResponse.TransactionId,
                    CharityResult = ConvertResponseResult(CharityResponse.Result),
                    CharityResponseMessage = CharityResponse.ResponseDescription,
                    CharityTransactionId = CharityResponse.TransactionId,
                    ConvenienceFee = RentPaymentAmount.FeeAmount
                };

            if (Response.RentAndFeeResult == IvrResponseResult.Success && RentPaymentAmount.BaseAmount > 0.0M)
            {
                AL.WriteLog("RenterID: " + renter.RenterId.ToString(), "IVR ACH payment successful. Recording payment of " + RentPaymentAmount.BaseAmount.ToString(), 
                    (short)LogPriority.LogAlways);

                Lease.CurrentBalanceDue -= RentPaymentAmount.BaseAmount;
                Lease.CurrentBalanceDueLastUpdated = DateTime.UtcNow;
                //Lease.PaymentDueDate = Lease.PaymentDueDate.HasValue ? Lease.PaymentDueDate.Value.AddMonths(1) : DateTime.UtcNow.AddMonths(1);
                Lease.Set(Lease);
                RecordPayments(rentPayments, renter, Response, payerAch);

                AL.WriteLog("RenterID: " + renter.RenterId.ToString(), "IVR ACH payment recorded for " + RentPaymentAmount.BaseAmount.ToString(), 
                    (short)LogPriority.LogAlways);
            }
            if (Response.CharityResult == IvrResponseResult.Success && CharityPaymentAmount.BaseAmount > 0.0M)
                RecordPayments(charityPayments, renter, Response, payerAch);

            //Salcedo - 11/25/2013 - added Email receipts for payments
            if (Response.RentAndFeeResult == IvrResponseResult.Success && RentPaymentAmount.BaseAmount > 0.0M)
            {
                SendEmailX(RentPaymentAmount.BaseAmount, Response.RentAndFeeTransactionId, RentPaymentAmount.FeeAmount, renter, true);
            }
            else
            {
                SendEmailX(RentPaymentAmount.BaseAmount, Response.RentAndFeeTransactionId, RentPaymentAmount.FeeAmount, renter, false);
            }

            return Response;
        }

        private static PaymentResponse ProcessPayment(Dictionary<PaymentApplication, List<PaymentRequest>> rentPayments, IEnumerable<PaymentRequest> charityPayments, PayerCreditCard payerCreditCard, Renter renter, string cvv)
        {
            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);
            var Property = EfxFramework.Property.GetPropertyByRenterId(renter.RenterId);
            var RentBaseAmount = rentPayments.Values.SelectMany(r => r).Where(r => !r.Description.StartsWith("convenience fee", StringComparison.OrdinalIgnoreCase)).Sum(r => r.Amount);
            var RentPaymentAmount = new PaymentAmount(RentBaseAmount, "Convenience Fee", EfxFramework.PaymentMethods.PaymentType.CreditCard, renter.RenterId);
            var CharityBaseAmount = charityPayments.Sum(r => r.Amount);
            var CharityPaymentAmount = new PaymentAmount(CharityBaseAmount);

            var RentResponse = new CreditCardPaymentResponse();
            var CharityResponse = new CreditCardPaymentResponse();

            ActivityLog AL = new ActivityLog();
            AL.WriteLog("RenterID: " + renter.RenterId.ToString(), "IVR PaymentRequest.ProcessPayment - CC - initiated.", (short)LogPriority.LogAlways);

            if (RentPaymentAmount.BaseAmount > 0.0M)
            {
                var CustomFields = new List<CustomField>
                {
                    new CustomField("customfield1", Property.PropertyId.HasValue ? Property.PropertyId.Value.ToString(CultureInfo.InvariantCulture) : "0"), 
                    new CustomField("customfield2", Lease.UnitNumber), 
                    new CustomField("customfield3", String.Format("{0}{1}", DateTime.Now.Month.ToString(CultureInfo.InvariantCulture), DateTime.Now.Year.ToString(CultureInfo.InvariantCulture))), 
                    new CustomField("customfield5", (Convert.ToInt32(RentPaymentAmount.FeeAmount * 100)).ToString(CultureInfo.InvariantCulture)), 
                    new CustomField("customfield6", (Convert.ToInt32(RentPaymentAmount.BaseAmount * 100)).ToString(CultureInfo.InvariantCulture)),
                    new CustomField("customfield10", Property.PropertyCode)
                };


                if (Property.PropertyId.HasValue && Property.PropertyId.Value > 0)
                    RentResponse = CreditCardPayment.ProcessCreditCard(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, renter.IsCreditCardTestRenter,
                                    RentPaymentAmount.TotalAmount, payerCreditCard.CreditCardAccountNumber, payerCreditCard.CreditCardExpirationDate, cvv, payerCreditCard.CreditCardHolderName, CustomFields);
                else
                    RentResponse = new CreditCardPaymentResponse {Result = GeneralResponseResult.PropertyNotFound, ResponseMessage = ServiceResponseMessages.PropertyNotFound};
            }

            if (CharityPaymentAmount.BaseAmount > 0.0M)
            {
                var CustomFields = new List<CustomField>
                {
                    new CustomField("customfield1", Property.PropertyId.HasValue ? Property.PropertyId.Value.ToString(CultureInfo.InvariantCulture) : "0"),
                    new CustomField("customfield2", Lease.UnitNumber), 
                    new CustomField("customfield3", String.Format("{0}{1}", DateTime.Now.Month.ToString(CultureInfo.InvariantCulture), DateTime.Now.Year.ToString(CultureInfo.InvariantCulture))),
                    new CustomField("customfield6", (Convert.ToInt32(CharityPaymentAmount.BaseAmount * 100)).ToString(CultureInfo.InvariantCulture)),
                    new CustomField("customfield10", Property.PropertyCode)
                };

                CharityResponse = CreditCardPayment.ProcessCreditCard(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, renter.IsCreditCardTestRenter,
                    CharityPaymentAmount.TotalAmount, payerCreditCard.CreditCardAccountNumber, payerCreditCard.CreditCardExpirationDate, cvv, payerCreditCard.CreditCardHolderName, CustomFields);
            }

            var Response = new PaymentResponse
            {
                RentAndFeeResult = ConvertResponseResult(RentResponse.Result),
                RentAndFeeResponseMessage = RentResponse.ResponseMessage,
                RentAndFeeTransactionId = RentResponse.TransactionId,
                CharityResult = ConvertResponseResult(CharityResponse.Result),
                CharityResponseMessage = CharityResponse.ResponseMessage,
                CharityTransactionId = CharityResponse.TransactionId,
                ConvenienceFee = RentPaymentAmount.FeeAmount
            };

            if (Response.RentAndFeeResponseMessage == ServiceResponseMessages.Ccaccepted)
            {
                ProcessVoid(Response.RentAndFeeTransactionId);
            }

            if (Response.CharityResponseMessage == ServiceResponseMessages.Ccaccepted)
            {
                ProcessVoid(Response.CharityTransactionId);
            }

            if (Response.RentAndFeeResult == IvrResponseResult.Success && RentPaymentAmount.BaseAmount > 0.0M)
            {
                AL.WriteLog("RenterID: " + renter.RenterId.ToString(), "IVR CC Payment successful. Recording payment of " + RentPaymentAmount.BaseAmount.ToString(), 
                    (short)LogPriority.LogAlways);

                Lease.CurrentBalanceDue -= RentPaymentAmount.BaseAmount;
                Lease.CurrentBalanceDueLastUpdated = DateTime.UtcNow;
                //Lease.PaymentDueDate = Lease.PaymentDueDate.HasValue ? Lease.PaymentDueDate.Value.AddMonths(1) : DateTime.UtcNow.AddMonths(1);
                Lease.Set(Lease);
                RecordPayments(rentPayments, renter, Response, payerCreditCard);

                AL.WriteLog("RenterID: " + renter.RenterId.ToString(), "IVR CC payment recorded for " + RentPaymentAmount.BaseAmount.ToString(), 
                    (short)LogPriority.LogAlways);
            }
            if (Response.CharityResult == IvrResponseResult.Success && CharityPaymentAmount.BaseAmount > 0.0M)
                RecordPayments(charityPayments, renter, Response, payerCreditCard);

            //Salcedo - 11/25/2013 - added Email receipts for payments
            if (Response.RentAndFeeResult == IvrResponseResult.Success && RentPaymentAmount.BaseAmount > 0.0M)
            {
                SendEmailX(RentPaymentAmount.BaseAmount, Response.RentAndFeeTransactionId, RentPaymentAmount.FeeAmount, renter, true);
            }
            else
            {
                SendEmailX(RentPaymentAmount.BaseAmount, Response.RentAndFeeTransactionId, RentPaymentAmount.FeeAmount, renter, false);
            }

            return Response;
        }

        //Salcedo - 11/25/2013 - added Email receipts for payments
        private static void SendEmailX(decimal amount, string TransactionId, decimal ConvenienceFee, Renter Resident, bool Success)
        {
            string Subject;
            string Body;
            var Property = EfxFramework.Property.GetPropertyByRenterId(Resident.RenterId);
            var Type = "Renter";

            var TheTransaction = Transaction.GetTransactionByExternalTransactionId(TransactionId);
             
            Subject = "";
            if (Success)
            {
                Subject = "Your Payment Confirmation";

                //Salcedo - 3/12/2014 - replaced with new html email template
                //Body = string.Format(EfxFramework.Resources.EmailTemplates.OneTimeRentPayment, Resident.DisplayName, amount.ToString("C"), DateTime.UtcNow.ToShortDateString(),
                //    TheTransaction.InternalTransactionId, ConvenienceFee.ToString("C"), (amount + ConvenienceFee).ToString("C"));
                String RecipientName = Resident.DisplayName;

                //cakel: BUGID000180 - Added property Name
                String RenterProperty = Property.PropertyName;

                String PaymentAmount = amount.ToString("C");
                String TransactionDate = DateTime.UtcNow.ToShortDateString();
                String InternalTransactionId = TheTransaction.InternalTransactionId;
                String ConvenienceAmount = ConvenienceFee.ToString("C");
                String TotalAmount = (amount + ConvenienceFee).ToString("C");

                //cakel: BUGID000180 - Added Property Name in Body Updated Order for email
                //0-Recipient
                //1-Property Name
                //2-Payment Amount
                //3-Transaction Date
                //4-Transaction ID
                //5-Convenience Fee Amount
                //6-Total Amount

                //cakel: BUGID000180 - Added PropertyName
                Body = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-payment-success.html").Replace("[[ROOT]]",
                    EmailTemplateService.EmailTemplateService.RootPath), RecipientName, RenterProperty, PaymentAmount, TransactionDate, InternalTransactionId, ConvenienceAmount, TotalAmount);
            }
            else
            {
                Subject = "Your Payment Failed";
                //Body = string.Format(EfxFramework.Resources.EmailTemplates.OneTimeRentPaymentFailed, Resident.DisplayName, amount.ToString("C"), DateTime.UtcNow.ToShortDateString(),
                //    "None", ConvenienceFee.ToString("C"), (amount + ConvenienceFee).ToString("C"));
                String RecipientName = Resident.DisplayName;
                //cakel: BUGID000180 - Added Renter Property
                String RenterProperty = Property.PropertyName;
                String PaymentAmount = amount.ToString("C");
                String TransactionDate = DateTime.UtcNow.ToShortDateString();
                String InternalTransactionId = "None";
                String ConvenienceAmount = ConvenienceFee.ToString("C");
                String TotalAmount = (amount + ConvenienceFee).ToString("C");
                //cakel: BUGID000180 - Added Property Name in Body Updated Order for email - Index order is same as above
                Body = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-payment-failure.html").Replace("[[ROOT]]",
                    EmailTemplateService.EmailTemplateService.RootPath), RecipientName, RenterProperty, PaymentAmount, TransactionDate, InternalTransactionId, ConvenienceAmount, TotalAmount);
            }

            //var ResultSet = DataAccess.ExecuteNoResults(Settings.ConnectionString, "dbo.usp_Transaction_GetSingleObject", new[] { new SqlParameter("@TransactionId", transactionId) });

            //Salcedo - 2/23/2014 - changed call to include staff IsMainContacts
            //BulkMail.BulkMail.SendMailMessage(
            //    Property.PropertyId,
            //    Type,
            //    false,
            //    Settings.SendMailUsername,
            //    Settings.SendMailPassword,
            //    new MailAddress(Settings.PaymentEmail),
            //    Subject,
            //    Body,
            //    new List<MailAddress> { new MailAddress(Resident.PrimaryEmailAddress) }
            //    );
            int IntPropertyId = Property.PropertyId.HasValue ? Property.PropertyId.Value : 0;
            BulkMail.BulkMail.SendMailMessageIncludePropertyEmailContacts(
                IntPropertyId,
                Type,
                EfxSettings.SendMailUsername,
                EfxSettings.SendMailPassword,
                new MailAddress(EfxSettings.PaymentEmail),
                Subject,
                Body,
                new List<MailAddress> { new MailAddress(Resident.PrimaryEmailAddress) }
                );
        }

        private static void ProcessVoid(string transactionId)
        {
            CreditCardPayment.VoidCreditCardPayment(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, transactionId);
        }

        private static void RecordPayments(Dictionary<PaymentApplication, List<PaymentRequest>> payments, Renter renter, PaymentResponse response, PayerCreditCard payerCreditCard)
        {
            var FirstPass = true;
            foreach (var Request in payments.Values.SelectMany(r => r))
            {
                SetPayment(renter, PaymentAmount.GetPaymentAmount(Request.Amount, renter, (FirstPass)), Request.Description, payerCreditCard, SetPaymentApplication(Request.Description), response);
                FirstPass = false;
            }
        }

        private static void RecordPayments(Dictionary<PaymentApplication, List<PaymentRequest>> payments, Renter renter, PaymentResponse response, PayerAch payerAch)
        {
            var FirstPass = true;
            foreach (var Request in payments.Values.SelectMany(r => r))
            {
                SetPayment(renter, PaymentAmount.GetPaymentAmount(Request.Amount, renter, EfxFramework.PaymentMethods.PaymentType.ECheck, (FirstPass)), Request.Description, payerAch, SetPaymentApplication(Request.Description), response);
                FirstPass = false;
            }
        }

        private static void RecordPayments(IEnumerable<PaymentRequest> payments, Renter renter, PaymentResponse response, PayerCreditCard payerCreditCard)
        {
            var FirstPass = true;
            foreach (var Request in payments)
            {
                SetPayment(renter, PaymentAmount.GetPaymentAmount(Request.Amount, renter, (FirstPass)), Request.Description, payerCreditCard, PaymentApplication.Charity, response);
                FirstPass = false;
            }
        }

        private static void RecordPayments(IEnumerable<PaymentRequest> payments, Renter renter, PaymentResponse response, PayerAch payerAch)
        {
            var FirstPass = true;
            foreach (var Request in payments)
            {
                SetPayment(renter, PaymentAmount.GetPaymentAmount(Request.Amount, renter, EfxFramework.PaymentMethods.PaymentType.ECheck, (FirstPass)), Request.Description, payerAch, PaymentApplication.Charity, response);
                FirstPass = false;
            }
        }

        private static PayerAch GetPayerAch(PaymentQueue paymentQueue)
        {
            return new PayerAch {BankAccountNumber = paymentQueue.BankAccountNumber, BankAccountTypeId = 1, BankRoutingNumber = paymentQueue.BankRoutingNumber};
        }

        private static PayerCreditCard GetPayerCreditCard(PaymentQueue paymentQueue)
        {
            return new PayerCreditCard
                {
                    CreditCardAccountNumber = paymentQueue.CreditCardAccountNumber, 
                    CreditCardHolderName = paymentQueue.CreditCardHolderName, 
                    CreditCardExpirationMonth = paymentQueue.CreditCardExpirationMonth.HasValue ? paymentQueue.CreditCardExpirationMonth.Value : DateTime.Now.Month, 
                    CreditCardExpirationYear = paymentQueue.CreditCardExpirationYear.HasValue ? paymentQueue.CreditCardExpirationYear.Value : DateTime.Now.Year
                };
        }

        private static IEnumerable<PaymentRequest> GetCharityPayments(PaymentQueue queue)
        {
            return queue.CharityAmount.HasValue ? new List<PaymentRequest> {new PaymentRequest { Amount = queue.CharityAmount.Value, Description = queue.CharityName }} : new List<PaymentRequest>();
        }

        private static Dictionary<PaymentApplication, List<PaymentRequest>> GetRentPayments(PaymentQueue queue)
        {
            var RentPayments = new Dictionary<PaymentApplication, List<PaymentRequest>>();

            if (queue.RentAmount.HasValue)
                RentPayments.Add(PaymentApplication.Rent, new List<PaymentRequest> { new PaymentRequest { Amount = queue.RentAmount.Value, Description = queue.RentAmountDescription } });
            if (queue.OtherAmount1.HasValue && queue.OtherAmount1.Value > 0.00M && !queue.OtherDescription1.ToLower().Contains("convenience fee"))
            {
                if (!RentPayments.ContainsKey(PaymentApplication.Other))
                    RentPayments.Add(PaymentApplication.Other, new List<PaymentRequest> { new PaymentRequest { Amount = queue.OtherAmount1.Value, Description = queue.OtherDescription1 } });
                else
                    RentPayments[PaymentApplication.Other].Add(new PaymentRequest { Amount = queue.OtherAmount1.Value, Description = queue.OtherDescription1 });
            }
            if (queue.OtherAmount2.HasValue && queue.OtherAmount2.Value > 0.00M && !queue.OtherDescription2.ToLower().Contains("convenience fee"))
            {
                if (!RentPayments.ContainsKey(PaymentApplication.Other))
                    RentPayments.Add(PaymentApplication.Other, new List<PaymentRequest> { new PaymentRequest { Amount = queue.OtherAmount2.Value, Description = queue.OtherDescription2 } });
                else
                    RentPayments[PaymentApplication.Other].Add(new PaymentRequest { Amount = queue.OtherAmount2.Value, Description = queue.OtherDescription2 });
            }
            if (queue.OtherAmount3.HasValue && queue.OtherAmount3.Value > 0.00M && !queue.OtherDescription3.ToLower().Contains("convenience fee"))
            {
                if (!RentPayments.ContainsKey(PaymentApplication.Other))
                    RentPayments.Add(PaymentApplication.Other, new List<PaymentRequest> { new PaymentRequest { Amount = queue.OtherAmount3.Value, Description = queue.OtherDescription3 } });
                else
                    RentPayments[PaymentApplication.Other].Add(new PaymentRequest { Amount = queue.OtherAmount3.Value, Description = queue.OtherDescription3 });
            }
            if (queue.OtherAmount4.HasValue && queue.OtherAmount4.Value > 0.00M && !queue.OtherDescription4.ToLower().Contains("convenience fee"))
            {
                if (!RentPayments.ContainsKey(PaymentApplication.Other))
                    RentPayments.Add(PaymentApplication.Other, new List<PaymentRequest> { new PaymentRequest { Amount = queue.OtherAmount4.Value, Description = queue.OtherDescription4 } });
                else
                    RentPayments[PaymentApplication.Other].Add(new PaymentRequest { Amount = queue.OtherAmount4.Value, Description = queue.OtherDescription4 });
            }
            if (queue.OtherAmount5.HasValue && queue.OtherAmount5.Value > 0.00M && !queue.OtherDescription5.ToLower().Contains("convenience fee"))
            {
                if (!RentPayments.ContainsKey(PaymentApplication.Other))
                    RentPayments.Add(PaymentApplication.Other, new List<PaymentRequest> { new PaymentRequest { Amount = queue.OtherAmount5.Value, Description = queue.OtherDescription5 } });
                else
                    RentPayments[PaymentApplication.Other].Add(new PaymentRequest { Amount = queue.OtherAmount5.Value, Description = queue.OtherDescription5 });
            }

            return RentPayments;
        }

        private static void SetPayment(Renter renter, PaymentAmount amount, string description, PayerCreditCard payerCreditCard, PaymentApplication paymentApplication, PaymentResponse response)
        {
            if (!renter.PayerId.HasValue)
                renter.PayerId = 0;

            int PaymentId;

            switch (paymentApplication)
            {
                case PaymentApplication.Rent:
                    PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.CreditCard, null, null, payerCreditCard.CreditCardHolderName, payerCreditCard.CreditCardAccountNumber,
                        payerCreditCard.CreditCardExpirationDate, amount, description, null, null, null, null, response.RentAndFeeTransactionId, false, null,
                        ((int)response.RentAndFeeResult).ToString(CultureInfo.InvariantCulture), response.RentAndFeeResponseMessage, null, PaymentChannel.Ivr, PaymentStatus.Approved));

                    Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int) response.RentAndFeeResult, response.RentAndFeeResponseMessage);
                    break;
                case PaymentApplication.Charity:
                    PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.CreditCard, null, null, payerCreditCard.CreditCardHolderName, payerCreditCard.CreditCardAccountNumber,
                        payerCreditCard.CreditCardExpirationDate, null, null, amount, description, null, null, response.CharityTransactionId, false, null,
                        ((int)response.CharityResult).ToString(CultureInfo.InvariantCulture), response.CharityResponseMessage, null, PaymentChannel.Ivr, PaymentStatus.Approved));

                    Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int) response.CharityResult, response.CharityResponseMessage);
                    break;
                case PaymentApplication.Other:
                    PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.CreditCard, null, null, payerCreditCard.CreditCardHolderName, payerCreditCard.CreditCardAccountNumber,
                        payerCreditCard.CreditCardExpirationDate, null, null, null, null, amount, description, response.RentAndFeeTransactionId, false, null,
                        ((int)response.RentAndFeeResult).ToString(CultureInfo.InvariantCulture), response.RentAndFeeResponseMessage, null, PaymentChannel.Ivr, PaymentStatus.Approved));

                    Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int) response.RentAndFeeResult, response.RentAndFeeResponseMessage);
                    break;
            }
        }

        private static void SetPayment(Renter renter, PaymentAmount amount, string description, PayerAch payerAch, PaymentApplication paymentApplication, PaymentResponse response)
        {
            if (!renter.PayerId.HasValue)
                renter.PayerId = 0;

            int PaymentId;

            switch (paymentApplication)
            {
                case PaymentApplication.Rent:
                    PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.ECheck, payerAch.BankAccountNumber, payerAch.BankRoutingNumber, null, null,
                        null, amount, description, null, null, null, null, response.RentAndFeeTransactionId, false, null,
                        ((int)response.RentAndFeeResult).ToString(CultureInfo.InvariantCulture), response.RentAndFeeResponseMessage, null, PaymentChannel.Ivr, PaymentStatus.Processing));

                    Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int) response.RentAndFeeResult, response.RentAndFeeResponseMessage);
                    break;
                case PaymentApplication.Charity:
                    PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.ECheck, payerAch.BankAccountNumber, payerAch.BankRoutingNumber, null, null,
                        null, null, null, amount, description, null, null, response.CharityTransactionId, false, null,
                        ((int)response.CharityResult).ToString(CultureInfo.InvariantCulture), response.CharityResponseMessage, null, PaymentChannel.Ivr, PaymentStatus.Processing));

                    Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int) response.CharityResult, response.CharityResponseMessage);
                    break;
                case PaymentApplication.Other:
                    PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.ECheck, payerAch.BankAccountNumber, payerAch.BankRoutingNumber, null, null,
                        null, null, null, null, null, amount, description, response.RentAndFeeTransactionId, false, null,
                        ((int)response.RentAndFeeResult).ToString(CultureInfo.InvariantCulture), response.RentAndFeeResponseMessage, null, PaymentChannel.Ivr, PaymentStatus.Processing));

                    Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int)response.RentAndFeeResult, response.RentAndFeeResponseMessage);
                    break;
            }
        }

        private static PaymentApplication SetPaymentApplication(string description)
        {
            if (!string.IsNullOrEmpty(description))
            {
                if (description.ToLower().Contains("rent"))
                    return PaymentApplication.Rent;
                if (description.ToLower().Contains("charity"))
                    return PaymentApplication.Charity;
            }

            return PaymentApplication.Other;
        }

        private static string GetAchClientId(int renterId, string description)
        {
            var Property = EfxFramework.Property.GetPropertyByRenterId(renterId);

            switch (SetPaymentApplication(description))
            {
                case PaymentApplication.Rent:
                    return Property.RentalAccountAchClientId;
                case PaymentApplication.Charity:
                    return Property.RentalAccountAchClientId;
                case PaymentApplication.Other:
                    return Property.FeeAccountAchClientId;
                default:
                    return Property.RentalAccountAchClientId;
            }
        }

        private static IvrResponseResult ConvertResponseResult(GeneralResponseResult result)
        {
            if (result == GeneralResponseResult.Success)
                return IvrResponseResult.Success;

            try
            {
                return (IvrResponseResult)((int)result);
            }
            catch
            {
                return IvrResponseResult.GeneralFailure;
            }
        }
    }
}
