﻿using EfxFramework.PaymentMethods;

namespace EfxFramework.WebPayments
{
    public class PaymentResponse
    {
        public GeneralResponseResult RentAndFeeResult { get; set; }
        public string RentAndFeeResponseMessage { get; set; }
        public string RentAndFeeTransactionId { get; set; }
        public GeneralResponseResult CharityResult { get; set; }
        public string CharityResponseMessage { get; set; }
        public string CharityTransactionId { get; set; }
        public decimal ConvenienceFee { get; set; }
        public bool RedirectToIvr { get; set; }
        public string PaymentQueueNumber { get; set; }
    }
}
