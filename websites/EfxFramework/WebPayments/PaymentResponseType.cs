﻿namespace EfxFramework.WebPayments
{
    public enum PaymentResponseType
    {
        IvrQueueNumber,
        RentAndFees,
        Charities
    }
}
