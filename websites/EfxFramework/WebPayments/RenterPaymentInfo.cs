﻿using System;
using EfxFramework.PaymentMethods;

namespace EfxFramework.WebPayments
{
    [Serializable]
    public class RenterPaymentInfo
    {
        public int RenterId { get; set; }
        public PaymentType PaymentType { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankRoutingNumber { get; set; }
        public string CreditCardNumber { get; set; }
        public string CreditCardHolderName { get; set; }
        public string Cvv { get; set; }
        public DateTime ExpirationDate { get; set; }
        public decimal? RentAmount { get; set; }
        public string RentAmountDescription { get; set; }
        public decimal? CharityAmount { get; set; }
        public string CharityName { get; set; }
        public decimal? OtherAmount { get; set; }
        public string OtherAmountDescription { get; set; }
        public decimal? OtherAmount2 { get; set; }
        public string OtherAmountDescription2 { get; set; }
        public decimal? OtherAmount3 { get; set; }
        public string OtherAmountDescription3 { get; set; }
        public decimal? OtherAmount4 { get; set; }
        public string OtherAmountDescription4 { get; set; }
        public decimal? OtherAmount5 { get; set; }
        public string OtherAmountDescription5 { get; set; }
        public string DisplayName { get; set; }
        public int PayerId { get; set; }
        public int PropertyId { get; set; }
        public decimal? WaivedAmount { get; set; }
        public bool WaiveConvenienceFee { get; set; }
    }
}
