﻿using EfxFramework.PaymentMethods;
using EfxFramework.PaymentMethods.Ach;
using EfxFramework.PaymentMethods.CreditCard;
using EfxFramework.PublicApi.Payment;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net.Mail;

namespace EfxFramework.WebPayments
{
    public class ApplicantPaymentRequest
    {
        public static ApplicantPaymentResponse ProcessPayment(ApplicantPaymentInfo paymentInfo)
        {
            ApplicantPaymentResponse Response;
                
            if (!ValidatePayment(paymentInfo.ApplicantId, paymentInfo.ApplicantApplicationId, paymentInfo.PropertyId, out Response))
                return Response;

            Response = paymentInfo.TypeOfPayment == PaymentType.CreditCard ? ProcessCreditCard(paymentInfo) : ProcessAch(paymentInfo);

            return Response;
        }

        private static ApplicantPaymentResponse ProcessCreditCard(ApplicantPaymentInfo paymentInfo)
        {
            ApplicantPaymentResponse Response;
            var PaymentResponse = new CreditCardPaymentResponse();

            if (!ValidateCreditCard(paymentInfo.CreditCardInfo, out Response))
                return Response;

            var Total = paymentInfo.Fees.Sum(f => f.FeesPaid) + paymentInfo.ConvenienceFeeAmount;
            var Property = new Property(paymentInfo.PropertyId);

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                return new ApplicantPaymentResponse {Result = GeneralResponseResult.PropertyNotFound, Message = ServiceResponseMessages.PropertyNotFound, TransactionAmount = Total, TransactionDate = DateTime.UtcNow};

            if (Total > 0.00M)
            {
                var CustomFields = new List<CustomField>
                    {
                        new CustomField("customfield1", Property.PropertyId.Value.ToString(CultureInfo.InvariantCulture)),
                        new CustomField("customfield3", String.Format("{0}{1}", DateTime.Now.Month.ToString(CultureInfo.InvariantCulture), DateTime.Now.Year.ToString(CultureInfo.InvariantCulture))),
                        new CustomField("customfield9", (Convert.ToInt32(Total * 100)).ToString(CultureInfo.InvariantCulture)),
                        new CustomField("customfield10", Property.PropertyCode)
                    };

                PaymentResponse = CreditCardPayment.ProcessCreditCard(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, paymentInfo.IsTestUser, Total, paymentInfo.CreditCardInfo.CreditCardNumber,
                    paymentInfo.CreditCardInfo.CreditCardExpirationString, paymentInfo.CreditCardInfo.CvvCode, paymentInfo.CreditCardInfo.CardHolderName, CustomFields);
            }

            Response.Result = PaymentResponse.Result;
            Response.Message = PaymentResponse.ResponseMessage;
            Response.TransactionAmount = Total;
            Response.TransactionDate = DateTime.UtcNow;

            var Transaction = BuildTransaction(paymentInfo, PaymentResponse, Total, Response.TransactionDate);
            Response.TransactionId = Transaction.InternalTransactionId;

            if (Response.Message == ServiceResponseMessages.Ccaccepted)
            {
                CreditCardPayment.VoidCreditCardPayment(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, Transaction.ExternalTransactionId);
                Response.Result = GeneralResponseResult.CcProcessingBusy;
                Response.Message = ServiceResponseMessages.CcProcessingBusy;
                return Response;
            }

            RecordPayment(paymentInfo, Response.Result == GeneralResponseResult.Success, Response.TransactionDate, Transaction.TransactionId);


            if (Response.Result == GeneralResponseResult.Success)
            {
                var Application = new ApplicantApplication(paymentInfo.ApplicantApplicationId) { FeesPaid = true };
                ApplicantApplication.Set(Application);
            }

            return Response;
        }

        private static ApplicantPaymentResponse ProcessAch(ApplicantPaymentInfo paymentInfo)
        {
            ApplicantPaymentResponse Response;
            var PaymentResponse = new AchPaymentResponse();
            var Property = new Property(paymentInfo.PropertyId);
            var Applicant = new Applicant(paymentInfo.ApplicantId);

            if (!ValidateEcheck(paymentInfo.EcheckInfo, Property, out Response))
                return Response;

            var Total = paymentInfo.Fees.Sum(f => f.FeesPaid) + paymentInfo.ConvenienceFeeAmount;

            if (Total > 0.00M)
            {
                PaymentResponse = AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword, Property.SecurityAccountAchClientId, Total, paymentInfo.EcheckInfo.BankRoutingNumber,
                    paymentInfo.EcheckInfo.BankAccountNumber, paymentInfo.EcheckInfo.TypeOfAccount.ToString().ToUpper(), paymentInfo.EcheckInfo.BillingFirstName, paymentInfo.EcheckInfo.BillingLastName,
                    paymentInfo.EcheckInfo.BillingAddress.AddressLine1, paymentInfo.EcheckInfo.BillingAddress.AddressLine2, paymentInfo.EcheckInfo.BillingAddress.City,
                    paymentInfo.EcheckInfo.BillingAddress.GetStateAbbeviation(), paymentInfo.EcheckInfo.BillingAddress.PostalCode, Applicant.MainPhoneNumber);
            }

            Response.Result = PaymentResponse.Result;
            Response.Message = PaymentResponse.ResponseDescription;
            Response.TransactionAmount = Total;
            Response.TransactionDate = DateTime.UtcNow;

            var Transaction = BuildTransaction(paymentInfo, PaymentResponse, Total, Response.TransactionDate);
            Response.TransactionId = Transaction.InternalTransactionId;

            RecordPayment(paymentInfo, Response.Result == GeneralResponseResult.Success, Response.TransactionDate, Transaction.TransactionId);
            

            if (Response.Result == GeneralResponseResult.Success)
            {
                var Application = new ApplicantApplication(paymentInfo.ApplicantApplicationId) {FeesPaid = true};
                ApplicantApplication.Set(Application);
            }

            return Response;
        }

        private static void RecordPayment(ApplicantPaymentInfo paymentInfo, bool paymentSuccessful, DateTime transactionDate, int transactionId)
        {
            foreach (var Fee in paymentInfo.Fees)
            {
                FeePayment.Set(BuildFeePayment(Fee.ApplicationFeeId, paymentInfo.ApplicantId, paymentSuccessful, transactionDate, transactionId));
            }
        }

        private static FeePayment BuildFeePayment(int feeId, int applicantId, bool paid, DateTime transactionDate, int transactionId)
        {
            return new FeePayment
            {
                ApplicationFeeId = feeId,
                ApplicantId = applicantId,
                FeePaid = paid,
                TransactionDate = transactionDate,
                TransactionId = transactionId
            };
        }

        //Credit Card
        private static Transaction BuildTransaction(ApplicantPaymentInfo paymentInfo, CreditCardPaymentResponse response, decimal total, DateTime transactionDate)
        {
            var Applicant = new Applicant(paymentInfo.ApplicantId);
            
            //cakel: BUGID00213 - Transaction -- added bool.  This will always be true because applications will always be submitted to ACH DB at time of Payment
            bool SetCCAchSettlement = true;
            var Transaction = new Transaction
                {
                    ExternalTransactionId = response.TransactionId,
                    PaymentTypeId = (int)paymentInfo.TypeOfPayment,
                    FirstName = paymentInfo.CreditCardInfo.BillingFirstName,
                    LastName = paymentInfo.CreditCardInfo.BillingLastName,
                    EmailAddress = Applicant.PrimaryEmailAddress,
                    StreetAddress = paymentInfo.CreditCardInfo.BillingAddress.AddressLine1,
                    StreetAddress2 = null,
                    City = paymentInfo.CreditCardInfo.BillingAddress.City,
                    StateProvinceId = paymentInfo.CreditCardInfo.BillingAddress.StateProvinceId,
                    PostalCode = paymentInfo.CreditCardInfo.BillingAddress.PostalCode,
                    PhoneNumber = !String.IsNullOrEmpty(Applicant.MainPhoneNumber) ? Applicant.MainPhoneNumber : String.Empty,
                    PaymentAmount = total,
                    CreditCardHolderName = paymentInfo.CreditCardInfo.CardHolderName,
                    CreditCardAccountNumber = paymentInfo.CreditCardInfo.CreditCardNumber,
                    CreditCardExpirationMonth = paymentInfo.CreditCardInfo.ExpirationDate.Month,
                    CreditCardExpirationYear = paymentInfo.CreditCardInfo.ExpirationDate.Year,
                    PaymentId = null,
                    PaymentStatusId = response.Result == GeneralResponseResult.Success ? (int)PaymentStatus.Approved : (int)PaymentStatus.Declined,
                    ResponseResult = (int)response.Result,
                    ResponseMessage = response.ResponseMessage,
                    TransactionDate = transactionDate,
                    PropertyId = paymentInfo.PropertyId,
                    CCAchSettledFlag = SetCCAchSettlement
                };


            Transaction newSetTrans = new Transaction(EfxFramework.Transaction.Set(Transaction));

            //cakel: BUGID00213 - Call Function for ACH INSERT 
            //CAKEL: BUGID00394 - rev2 - Moved code down so transaction will be created then we can update

            if (response.Result == GeneralResponseResult.Success)
            {
                var CCResponseResult = (int)response.Result;
                if (CCResponseResult == 0)
                {
                    //cakel: BUGID00213 rev2 - only submit if payment type is credit card
                    if ((int)paymentInfo.TypeOfPayment == 1)
                    {
                        SubmitCCAch(total, paymentInfo.PropertyId, paymentInfo.CreditCardInfo.BillingFirstName, paymentInfo.CreditCardInfo.BillingLastName, newSetTrans.InternalTransactionId);

                    }
                }

            }

            return newSetTrans;
            //return new Transaction(EfxFramework.Transaction.Set(Transaction));
        }

        //cakel: BUGID00213 - ACH INSERT
        //cakel: BUGID00394 - added ACH Trans id from response
        public static void SubmitCCAch(decimal amount, int propID, string PayerFirstName, string PayerLastName, string TranID)
        {
            var property = new EfxFramework.Property(propID);
            //cakel: BUGID00213 These fields map to "[EncryptedMonthlyFeeBankAccountNumber] and [EncryptedMonthlyFeeBankRoutingNumber] in Property table
            string BankAccount = property.MonthlyFeeBankAccountNumber;
            string BankRouting = property.MonthlyFeeBankRoutingNumber;

                //negative amount added for credit to property
                //For Applicant Payment they will need to go into Account mapped to "sett"
                decimal PropertyPaymentAmount = (Convert.ToDecimal(amount)) * -1;
                var ACH_Validate = EfxFramework.PaymentMethods.Ach.AchService.ProcessAchPayment(
                    EfxSettings.AchUserId, 
                    EfxSettings.AchPassword,
                    EfxSettings.CcSettlementAchId,
                    PropertyPaymentAmount,
                    BankRouting,
                    BankAccount, 
                    "Checking",
                    PayerFirstName, 
                    PayerLastName, property.StreetAddress, "", property.City, property.DisplayStateProvince, property.PostalCode, "");

            //cakel: BUGID00394
                var ACH_ValResult = ACH_Validate.TransactionId;
            //Add Code to Update transaction Record when Inserting into Transaction Table
                try
                {
                    //this is to update transaction record with the ACH Trans ID
                    UpdateTransactionRecordWithACHTransID(TranID, ACH_ValResult.ToString());
                }
                catch
                {
                    string TransDate = DateTime.Now.ToString();
                    string AchTranID = ACH_ValResult.ToString();
                    //cakel: Add Error catching email
                    SendAchErrorEmail(AchTranID, property.PropertyName, PayerFirstName, PayerLastName, TransDate, TranID);
                }


            //cakel: Need to Add code for response from ACH DB - This will send email to manager about the ACH insert failure.
                //cakel: Need to Add code for response from ACH DB - This will send email to manager about the ACH insert failure.
                var ACHDB_Result = ACH_Validate.Result;
                if (ACHDB_Result != 0)
                {
                    DateTime tranDate = DateTime.Now;
                    string _result = ACH_Validate.Result.ToString();
                    SendAchErrorEmail(_result, property.PropertyName, PayerFirstName, PayerLastName, tranDate.ToString(), TranID);
                }



        }

        //cakel: BUGID00213 - Sends Email to Support when ACH DB insert fails 
        private static void SendAchErrorEmail(string Transtatus, string propertyName, string Firstname, string Lastname, string transDate, string transID)
        {
            string MailBody = "<p>An error occured when trying to insert record into ACH DB for submission.  Please check the properties Credit Card Account on file.</p><br />";
            MailBody += "Status: " + Transtatus + "<br />";
            MailBody += "Property Name: " + propertyName + "<br />";
            MailBody += "Payer First Name: " + Firstname + "<br />";
            MailBody += "Payer Last Name: " + Lastname + "<br />";
            MailBody += "Transaction Date: " + transDate + "<br />";
            MailBody += "Transaction ID: " + transID + "<br />";

            MailMessage ACH_ErrorMail = new MailMessage();
            ACH_ErrorMail.Subject = "ACH DB Insert Error";
            ACH_ErrorMail.Body = MailBody;
            ACH_ErrorMail.From = new MailAddress("support@rentpaidonline.com", "RPO Support");
            ACH_ErrorMail.To.Add(new MailAddress(EfxSettings.SupportEmail, "RPO Support Admin"));
            ACH_ErrorMail.IsBodyHtml = true;
            SmtpClient RpoSmtp = new SmtpClient();

            //Salcedo - 4/3/2015 - add support for dynamic host (mainly for testing)
            RpoSmtp.Host = EfxSettings.SmtpHost;

            RpoSmtp.Send(ACH_ErrorMail);

        }


        //ACH
        private static Transaction BuildTransaction(ApplicantPaymentInfo paymentInfo, AchPaymentResponse response, decimal total, DateTime transactionDate)
        {
            var Applicant = new Applicant(paymentInfo.ApplicantId);

            var Transaction = new Transaction
                {
                    ExternalTransactionId = response.TransactionId,
                    PaymentTypeId = (int)paymentInfo.TypeOfPayment,
                    FirstName = paymentInfo.EcheckInfo.BillingFirstName,
                    LastName = paymentInfo.EcheckInfo.BillingLastName,
                    EmailAddress = Applicant.PrimaryEmailAddress,
                    StreetAddress = paymentInfo.EcheckInfo.BillingAddress.AddressLine1,
                    StreetAddress2 = null,
                    City = paymentInfo.EcheckInfo.BillingAddress.City,
                    StateProvinceId = paymentInfo.EcheckInfo.BillingAddress.StateProvinceId,
                    PostalCode = paymentInfo.EcheckInfo.BillingAddress.PostalCode,
                    PhoneNumber = !String.IsNullOrEmpty(Applicant.MainPhoneNumber) ? Applicant.MainPhoneNumber : String.Empty,
                    BankAccountNumber = paymentInfo.EcheckInfo.BankAccountNumber,
                    BankRoutingNumber = paymentInfo.EcheckInfo.BankRoutingNumber,
                    BankAccountTypeId = (int)paymentInfo.EcheckInfo.TypeOfAccount,
                    PaymentAmount = total,
                    PaymentId = null,
                    PaymentStatusId = response.Result == GeneralResponseResult.Success ? (int)PaymentStatus.Processing : (int)PaymentStatus.Declined,
                    ResponseResult = (int)response.Result,
                    ResponseMessage = response.ResponseDescription,
                    TransactionDate = transactionDate,
                    PropertyId = paymentInfo.PropertyId
                };

            return new Transaction(EfxFramework.Transaction.Set(Transaction));
        }

        private static bool ValidatePayment(int applicantId, int applicantApplicationId, int propertyId, out ApplicantPaymentResponse response)
        {
            var Applicant = new Applicant(applicantId);
            var ApplicantApplication = new ApplicantApplication(applicantApplicationId);
            var Property = new Property(propertyId);

            response = new ApplicantPaymentResponse { TransactionDate = DateTime.UtcNow };

            if (Applicant.ApplicantId < 1)
            {
                response.Result = GeneralResponseResult.ApplicantNotFound;
                response.Message = ServiceResponseMessages.ApplicantNotFound;
                return false;
            }

            if (ApplicantApplication.ApplicantApplicationId < 1)
            {
                response.Result = GeneralResponseResult.ApplicationNotFound;
                response.Message = ServiceResponseMessages.ApplicationNotFound;
                return false;
            }

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                response.Result = GeneralResponseResult.PropertyNotFound;
                response.Message = ServiceResponseMessages.PropertyNotFound;
                return false;
            }

            return true;
        }

        private static bool ValidateCreditCard(CreditCardDetails details, out ApplicantPaymentResponse response)
        {
            response = new ApplicantPaymentResponse { TransactionDate = DateTime.UtcNow };

            if (String.IsNullOrEmpty(details.CardHolderName))
            {
                response.Result = GeneralResponseResult.MissingCardholderName;
                response.Message = ServiceResponseMessages.MissingCardholderName;
                return false;
            }

            if (String.IsNullOrEmpty(details.CreditCardNumber))
            {
                response.Result = GeneralResponseResult.MissingCardNumber;
                response.Message = ServiceResponseMessages.MissingCardNumber;
                return false;
            }

            if (details.ExpirationDate.AddMonths(1) <= DateTime.UtcNow)
            {
                response.Result = GeneralResponseResult.ExpiredCard;
                response.Message = ServiceResponseMessages.Expiredcard;
                return false;
            }

            if (String.IsNullOrEmpty(details.CvvCode))
            {
                response.Result = GeneralResponseResult.MissingCvv;
                response.Message = ServiceResponseMessages.MissingCvv;
                return false;
            }

            return true;
        }

        private static bool ValidateEcheck(EcheckDetails details, Property property, out ApplicantPaymentResponse response)
        {
            response = new ApplicantPaymentResponse { TransactionDate = DateTime.UtcNow };

            if (String.IsNullOrEmpty(details.BankAccountNumber))
            {
                response.Result = GeneralResponseResult.MissingAccountNumber;
                response.Message = ServiceResponseMessages.Missingaccountnumber;
                return false;
            }

            if (String.IsNullOrEmpty(details.BankRoutingNumber))
            {
                response.Result = GeneralResponseResult.MissingRoutingNumber;
                response.Message = ServiceResponseMessages.MissingRoutingNumber;
                return false;
            }

            if (!details.BankRoutingNumber.IsValidBankRoutingNumber())
            {
                response.Result = GeneralResponseResult.InvalidRoutingNumber;
                response.Message = ServiceResponseMessages.Invalidroutingnumber;
                return false;
            }

            if (String.IsNullOrEmpty(property.FeeAccountAchClientId))
            {
                response.Result = GeneralResponseResult.MissingAchClientId;
                response.Message = ServiceResponseMessages.Missingachclientid;
                return false;
            }

            return true;
        }

        //cakel BUGID00394
        private static void UpdateTransactionRecordWithACHTransID(string TransID, string ACH_TransID)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Transaction_SetApplicantACH_Response", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@TransID", SqlDbType.NVarChar, 100).Value = TransID;
            com.Parameters.Add("@AchResponse", SqlDbType.NVarChar, 200).Value = ACH_TransID;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                throw ex;
                
            }
            finally
            {
                con.Close();
            }

        }


        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

    }
}
