﻿using EfxFramework.PaymentMethods;
using System.Collections.Generic;

namespace EfxFramework.WebPayments
{
    public class ApplicantPaymentInfo
    {
        public List<ApplicationFee> Fees { get; set; }
        public PaymentType TypeOfPayment { get; set; }
        public CreditCardDetails CreditCardInfo { get; set; }
        public EcheckDetails EcheckInfo { get; set; }
        public int PropertyId { get; set; }
        public int ApplicantId { get; set; }
        public int ApplicantApplicationId { get; set; }
        public bool IsTestUser { get; set; }
        //CMallory Convenience Fee Change
        public decimal ConvenienceFeeAmount { get; set; }
    }
}
