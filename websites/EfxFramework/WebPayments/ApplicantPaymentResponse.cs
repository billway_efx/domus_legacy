﻿using EfxFramework.PaymentMethods;
using System;

namespace EfxFramework.WebPayments
{
    public class ApplicantPaymentResponse
    {
        public GeneralResponseResult Result { get; set; }
        public string Message { get; set; }
        public string TransactionId { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal TransactionAmount { get; set; }
    }
}
