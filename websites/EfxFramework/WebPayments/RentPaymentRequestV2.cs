﻿using EfxFramework.PaymentMethods;

namespace EfxFramework.WebPayments
{
    public class RentPaymentRequestV2 : PaymentProcessingBase
    {
        public RentPaymentRequestV2(PaymentChannel channel, int propertyId)
            : base(channel, propertyId){}

        public RentPaymentRequestV2(PaymentChannel channel, int propertyId, int residentId)
            : base(channel, propertyId, residentId){}

        /// <summary>
        /// Process a Rent Payment using the Current Balance Due
        /// </summary>
        /// <param name="channel">The application from which the payment was made</param>
        /// <param name="info"></param>
        public static PaymentResponseBase ProcessPayment(PaymentChannel channel, RenterPaymentInfo info)
        {
            var Request = new RentPaymentRequestV2(channel, info.PropertyId, info.RenterId);

            if (info.PaymentType == PaymentType.CreditCard)
            {
                return Request.ProcessResidentPayment(new CreditCardDetails(Request.GetResidentAddress(), Request.Resident.FirstName, Request.Resident.LastName, info.CreditCardHolderName,
                    info.CreditCardNumber, info.ExpirationDate, info.Cvv));
            }
                
            return Request.ProcessResidentPayment(new EcheckDetails(Request.GetResidentAddress(), Request.Resident.FirstName, Request.Resident.LastName, info.BankAccountNumber, 
                info.BankRoutingNumber, AccountType.Checking));
        }

        /// <summary>
        /// Process a Rent Payment using a specified amount
        /// </summary>
        /// <param name="channel">The application from which the payment was made</param>
        /// <param name="info"></param>
        /// <param name="amount"></param>
        /// <param name="waivedAmount"></param>
        public static PaymentResponseBase ProcessPayment(PaymentChannel channel, RenterPaymentInfo info, PaymentAmount amount, decimal waivedAmount)
        {
            var Request = new RentPaymentRequestV2(channel, info.PropertyId, info.RenterId);

            if (info.PaymentType == PaymentType.CreditCard)
            {
                return Request.ProcessResidentPayment(new CreditCardDetails(Request.GetResidentAddress(), Request.Resident.FirstName, Request.Resident.LastName, info.CreditCardHolderName,
                    info.CreditCardNumber, info.ExpirationDate, info.Cvv), amount, waivedAmount);
            }

            return Request.ProcessResidentPayment(new EcheckDetails(Request.GetResidentAddress(), Request.Resident.FirstName, Request.Resident.LastName, info.BankAccountNumber, info.BankRoutingNumber,
                AccountType.Checking), amount, waivedAmount);
        }

        /// <summary>
        /// Process an Application Fee payment using the default fees
        /// </summary>
        /// <param name="channel">The application from which the payment was made</param>
        /// <param name="info"></param>
        public static PaymentResponseBase ProcessPayment(PaymentChannel channel, ApplicantPaymentInfo info)
        {
            var Request = new RentPaymentRequestV2(channel, info.PropertyId) {ApplicantId = info.ApplicantId};
            info = UpdateBillingInfo(info, Request);

            return info.TypeOfPayment == PaymentType.CreditCard ? Request.ProcessApplicantPayment(info.CreditCardInfo) : Request.ProcessApplicantPayment(info.EcheckInfo);
        }

        /// <summary>
        /// Process an Application Fee payment using a specified amount
        /// </summary>
        /// <param name="channel">The application from which the payment was made</param>
        /// <param name="info"></param>
        /// <param name="amount"></param>
        public static PaymentResponseBase ProcessPayment(PaymentChannel channel, ApplicantPaymentInfo info, PaymentAmount amount)
        {
            var Request = new RentPaymentRequestV2(channel, info.PropertyId) {ApplicantId = info.ApplicantId};
            info = UpdateBillingInfo(info, Request);

            return info.TypeOfPayment == PaymentType.CreditCard ? Request.ProcessApplicantPayment(info.CreditCardInfo, amount) : Request.ProcessApplicantPayment(info.EcheckInfo, amount);
        }

        private static ApplicantPaymentInfo UpdateBillingInfo(ApplicantPaymentInfo info, RentPaymentRequestV2 request)
        {
            info.CreditCardInfo.BillingAddress.PrimaryEmailAddress = request.Applicant.PrimaryEmailAddress;
            info.CreditCardInfo.BillingAddress.PrimaryPhoneNumber = request.Applicant.MainPhoneNumber;
            info.EcheckInfo.BillingAddress.PrimaryEmailAddress = request.Applicant.PrimaryEmailAddress;
            info.EcheckInfo.BillingAddress.PrimaryPhoneNumber = request.Applicant.MainPhoneNumber;

            return info;
        }
    }
}
