﻿using EfxFramework.PaymentMethods;
using EfxFramework.PaymentMethods.Ach;
using EfxFramework.PaymentMethods.CreditCard;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EfxFramework.PublicApi.Payment;
using EfxFramework.Resources;
using System.Net.Mail;
using System.Data.SqlClient;
using Mb2x.Data;
using EfxFramework.Helpers;
using EfxFramework.ExtensionMethods;
using RPO;
using RPO.Helpers;
using EfxFramework.Scheduler;


namespace EfxFramework.WebPayments
{
   public class PaymentRequest
   {

      public static PaymentResponse ProcessPayment(RenterPaymentInfo renterPaymentInfo, bool? useIvr = null)
      {
         return ProcessPayment(renterPaymentInfo, false, useIvr);
      }

      //Salcedo - 2/27/2014 - we need to know if the payment originated from the auto payment scheduler or not
      public static bool bScheduledPayment = false;
      public static PaymentResponse ProcessScheduledPayment(RenterPaymentInfo renterPaymentInfo, bool applyToNextMonthsRent, bool? useIvr = null)
      {
         bScheduledPayment = true;
         return ProcessPayment(renterPaymentInfo, false);
      }

      public static PaymentResponse ProcessPayment(RenterPaymentInfo renterPaymentInfo, bool applyToNextMonthsRent, bool? useIvr = null)
      {
         var Renter = new Renter(renterPaymentInfo.RenterId) { IsParticipating = true };
         EfxFramework.Renter.Set(Renter, "System Process");

         ActivityLog AL = new ActivityLog();
         AL.WriteLog("RenterId: " + renterPaymentInfo.RenterId.ToString(), "PaymentRequest.ProcessPayment started.", (short)LogPriority.LogAlways);

         var PayerAch = new PayerAch
         {
            BankAccountNumber = renterPaymentInfo.BankAccountNumber,
            BankRoutingNumber = renterPaymentInfo.BankRoutingNumber,
            PayerId = renterPaymentInfo.PayerId
         };

         var PayerCreditCard = new PayerCreditCard
         {
            CreditCardAccountNumber = renterPaymentInfo.CreditCardNumber,
            CreditCardExpirationMonth = renterPaymentInfo.ExpirationDate.Month,
            CreditCardExpirationYear = renterPaymentInfo.ExpirationDate.Year,
            CreditCardHolderName = renterPaymentInfo.CreditCardHolderName,
            PayerId = renterPaymentInfo.PayerId
         };

         var Property = new Property(renterPaymentInfo.PropertyId);

         //Salcedo - 3/25/2014 - we are no longer using the IVR
         //var UseIvr = useIvr ?? Property.ProgramId.HasValue && Property.ProgramId.Value != Property.Program.PropertyPaidProgram;
         var UseIvr = false;

         var RentPayments = GetRentPayments(renterPaymentInfo);
         var CharityPayments = GetCharityPayments(renterPaymentInfo.CharityAmount, renterPaymentInfo.CharityName);

         switch (renterPaymentInfo.PaymentType)
         {
            case EfxFramework.PaymentMethods.PaymentType.CreditCard:
               return ProcessPayment(RentPayments, CharityPayments, PayerCreditCard, Renter, UseIvr, renterPaymentInfo.WaivedAmount, renterPaymentInfo.WaiveConvenienceFee, applyToNextMonthsRent, renterPaymentInfo.Cvv);

            case EfxFramework.PaymentMethods.PaymentType.ECheck:
               return ProcessPayment(RentPayments, CharityPayments, PayerAch, Renter, renterPaymentInfo.WaivedAmount, renterPaymentInfo.WaiveConvenienceFee, applyToNextMonthsRent);
         }

         return new PaymentResponse();
      }

      private static PaymentResponse ProcessPayment(Dictionary<PaymentApplication, List<PaymentItem>> rentPayments, IList<PaymentItem> charityPayments,
         PayerCreditCard payerCreditCard, Renter renter, bool useIvr, decimal? waivedAmount, bool waiveConvenienceFee, bool applyToNextMonthsRent, string cvv = null)
      {
         //Salcedo - 3/25/2014 - We are no longer using the IVR system for CC payments
         //if (useIvr)

         var Property = EfxFramework.Property.GetPropertyByRenterId(renter.RenterId);

         //Salcedo - 3/25/2014 - We are using the queue for scheduled CC payments
         if (bScheduledPayment)
         {
            //Salcedo - 3/25/2014 - Queue payment until user logs into the system and enters their ccv code
            var Result = QueuePayment(rentPayments, charityPayments, payerCreditCard, renter, applyToNextMonthsRent);

            // Email user and inform them they need to login and enter their cvv code
            var Subject = "DOMUS Recurring Credit Card Payment | Action Required";
            String PropertyName = Property.PropertyName;
            String CardNumber = payerCreditCard.CreditCardAccountNumber.MaskAccountNumber();
            String ExpirationDate = payerCreditCard.CreditCardExpirationMonth + "/" + payerCreditCard.CreditCardExpirationYear;

            String RecipientName = renter.DisplayName;
            String Body = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml(
                "EmailTemplates/DomusMe-recurring-credit-card-payment.html").Replace("[[ROOT]]",
                EmailTemplateService.EmailTemplateService.RootPath),
                RecipientName,
                PropertyName,
                RecipientName,
                CardNumber,
                ExpirationDate);

            int IntPropertyId = Property.PropertyId.HasValue ? Property.PropertyId.Value : 0;
            var Type = "Renter";
            BulkMail.BulkMail.SendMailMessage(
                Property.PropertyId,
                Type,
                false,
                EfxSettings.SendMailUsername,
                EfxSettings.SendMailPassword,
                new MailAddress(EfxSettings.PaymentEmail),
                Subject,
                Body,
                new List<MailAddress> { new MailAddress(renter.PrimaryEmailAddress) }
                );

            return Result;
         }

         var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);

         var TestPaymentList = rentPayments;

         var RentBaseAmount = rentPayments.Values.SelectMany(r => r).Where(r => !r.Description.StartsWith("convenience fee", StringComparison.OrdinalIgnoreCase)).Sum(r => r.Amount);
         //cakel: TASK 00407
         var RentPaymentAmount = new PaymentAmount(RentBaseAmount);
         // var RentPaymentAmount = Lease.CurrentBalanceDue;

         var CharityBaseAmount = charityPayments.Sum(r => r.Amount);
         var CharityPaymentAmount = new PaymentAmount(CharityBaseAmount);
         var RentResponse = new CreditCardPaymentResponse();
         var CharityResponse = new CreditCardPaymentResponse();

         if (!waiveConvenienceFee)
         {
            //cakel: 08/09/2016
            RentPaymentAmount.CalculateFeeAmount(EfxFramework.PaymentMethods.PaymentType.CreditCard, renter.RenterId, RentBaseAmount);
            RentPaymentAmount.FeeDescription = "Convenience Fee";
         }


         if (Lease.LeaseId < 1)
            return new PaymentResponse { RentAndFeeResult = GeneralResponseResult.LeaseNotFound, RentAndFeeResponseMessage = ServiceResponseMessages.LeaseNotFound };

         if (RentPaymentAmount.BaseAmount > 0.00M)
         {
            var CustomFields = new List<CustomField>
                {
                    new CustomField("customfield1", Property.PropertyId.HasValue ? Property.PropertyId.Value.ToString(CultureInfo.InvariantCulture) : "0"),
                    new CustomField("customfield2", Lease.UnitNumber),
                    new CustomField("customfield3", String.Format("{0}{1}", DateTime.Now.Month.ToString(CultureInfo.InvariantCulture), DateTime.Now.Year.ToString(CultureInfo.InvariantCulture))),
                    new CustomField("customfield5", (Convert.ToInt32(RentPaymentAmount.FeeAmount * 100)).ToString(CultureInfo.InvariantCulture)),
                    new CustomField("customfield6", (Convert.ToInt32(RentPaymentAmount.BaseAmount * 100)).ToString(CultureInfo.InvariantCulture)),
                    new CustomField("customfield10", Property.PropertyCode)
                };

            if (Property.PropertyId.HasValue && Property.PropertyId.Value > 0)
               RentResponse = CreditCardPayment.ProcessCreditCard(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, renter.IsCreditCardTestRenter, RentPaymentAmount.TotalAmount,
                               payerCreditCard.CreditCardAccountNumber, payerCreditCard.CreditCardExpirationDate, cvv, payerCreditCard.CreditCardHolderName, CustomFields);
            else
               RentResponse = new CreditCardPaymentResponse { Result = GeneralResponseResult.PropertyNotFound, ResponseMessage = ServiceResponseMessages.PropertyNotFound };
         }

         if (CharityPaymentAmount.BaseAmount > 0.00M)
         {
            var CustomFields = new List<CustomField>
                {
                    new CustomField("customfield1", Property.PropertyId.HasValue ? Property.PropertyId.Value.ToString(CultureInfo.InvariantCulture) : "0"),
                    new CustomField("customfield2", Lease.UnitNumber),
                    new CustomField("customfield3", String.Format("{0}{1}", DateTime.Now.Month.ToString(CultureInfo.InvariantCulture), DateTime.Now.Year.ToString(CultureInfo.InvariantCulture))),
                    new CustomField("customfield6", (Convert.ToInt32(CharityPaymentAmount.BaseAmount * 100)).ToString(CultureInfo.InvariantCulture)),
                    new CustomField("customfield10", Property.PropertyCode)
                };

            CharityResponse = CreditCardPayment.ProcessCreditCard(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, renter.IsCreditCardTestRenter, CharityPaymentAmount.BaseAmount,
                payerCreditCard.CreditCardAccountNumber, payerCreditCard.CreditCardExpirationDate, cvv, payerCreditCard.CreditCardHolderName, CustomFields);
         }

         var Response = new PaymentResponse
         {
            RentAndFeeResult = RentResponse.Result,
            RentAndFeeResponseMessage = RentResponse.ResponseMessage,
            RentAndFeeTransactionId = RentResponse.TransactionId,
            CharityResult = CharityResponse.Result,
            CharityResponseMessage = CharityResponse.ResponseMessage,
            CharityTransactionId = CharityResponse.TransactionId,
            RedirectToIvr = false,
            ConvenienceFee = RentPaymentAmount.FeeAmount
         };

         if (Response.RentAndFeeResponseMessage == ServiceResponseMessages.Ccaccepted)
         {
            ProcessVoid(Response.RentAndFeeTransactionId);
         }

         if (Response.CharityResponseMessage == ServiceResponseMessages.Ccaccepted)
         {
            ProcessVoid(Response.CharityTransactionId);
         }

         if (Response.RentAndFeeResult == GeneralResponseResult.Success)
         {
            //cakel: BUGID00235 - Updated log to show both user and renter when admin is making a payment for a renter
            string _user = System.Web.HttpContext.Current.User.Identity.Name;
            ActivityLog AL = new ActivityLog();
            AL.WriteLog(_user, "CC payment successful. Recording payment of " + RentPaymentAmount.BaseAmount.ToString(),
                (short)LogPriority.LogAlways, renter.RenterId.ToString());

            //cakel: 00407 rev 3
            AdjustCurrentBalanceDue(RentPaymentAmount.BaseAmount, Lease, applyToNextMonthsRent, Property.PmsId);
            RecordPayments(rentPayments, renter, Response, payerCreditCard, RentPaymentAmount.FeeAmount);


            AL.WriteLog(_user, "CC payment recorded for " + RentPaymentAmount.BaseAmount.ToString(), (short)LogPriority.LogAlways, renter.RenterId.ToString());
         }

         if (waivedAmount.HasValue && waivedAmount.Value > 0.00M)
         {
            AdjustCurrentBalanceDue(waivedAmount.Value, Lease, applyToNextMonthsRent, Property.PmsId);

            if (String.IsNullOrEmpty(Response.RentAndFeeTransactionId))
               Response.RentAndFeeTransactionId = Guid.NewGuid().ToString();

            RecordPayments(
                new List<PaymentItem>
                    {
                            new PaymentItem {Amount = waivedAmount.Value, Description = "Waived Payments", IsCharityPayment = false, IsRentPayment = false}
                    },
                    renter,
                    Response,
                    payerCreditCard);
         }

         if (Response.CharityResult == GeneralResponseResult.Success)
            RecordPayments(charityPayments, renter, Response, payerCreditCard);

         //Salcedo - 11/25/2013 - added Email receipts for payments
         if (Response.RentAndFeeResult == GeneralResponseResult.Success)
         {
            SendEmailX(RentPaymentAmount.BaseAmount, Response.RentAndFeeTransactionId, RentPaymentAmount.FeeAmount, renter, true);
         }
         else
         {
            SendEmailX(RentPaymentAmount.BaseAmount, Response.RentAndFeeTransactionId, RentPaymentAmount.FeeAmount, renter, false);
         }

         return Response;
      }

      //cakel: 00407 rev3
      private static void AdjustCurrentBalanceDue(decimal amount, Lease lease, bool applyToNextMonthsRent, string PropPmsID)
      {
         //cakel: TASK 00407 - Needed to adjust the way we are updating balance
         //The current balance for non-integrated resident should now align with integrated due to new SP usp_Lease_AddCurrentBalanceDueForNonIntegratedProperties_v2
         bool _checkIntegrated = RPO.Helpers.Helper.IsIntegrated(PropPmsID);

         if (!applyToNextMonthsRent)
         {
            if (_checkIntegrated == true)
            {
               lease.CurrentBalanceDue -= amount;
               lease.CurrentBalanceDueLastUpdated = DateTime.UtcNow;
            }
            else
            {
               var NewLeaseBalance = (lease.CurrentBalanceDue - amount);

               lease.CurrentBalanceDue = NewLeaseBalance;
               lease.CurrentBalanceDueLastUpdated = DateTime.UtcNow;
            }

         }
         else
         {
            RPO.Credit newCredit = new RPO.Credit();
            newCredit.CreditAmount = amount;
            newCredit.LeaseId = lease.LeaseId;
            RPO.Credit.AddNewCredit(newCredit);
         }

         //if (lease.CurrentBalanceDue <= 0.00M)
         //    lease.PaymentDueDate = lease.PaymentDueDate.HasValue ? lease.PaymentDueDate.Value.AddMonths(1) : DateTime.UtcNow.AddMonths(1);

         Lease.Set(lease);
      }




      private static PaymentResponse QueuePayment(IDictionary<PaymentApplication, List<PaymentItem>> rentPayments, IList<PaymentItem> charityPayments, PayerCreditCard payerCreditCard, Renter renter,
          bool applyToNextMonthsRent)
      {
         var RentBaseAmount = rentPayments.Values.SelectMany(r => r).Where(r => !r.Description.StartsWith("convenience fee", StringComparison.OrdinalIgnoreCase)).Sum(r => r.Amount);
         var RentPaymentAmount = new PaymentAmount(RentBaseAmount, "Convenience Fee", EfxFramework.PaymentMethods.PaymentType.CreditCard, renter.RenterId);

         var PaymentQueue = new PaymentQueue
         {
            CreditCardAccountNumber = payerCreditCard.CreditCardAccountNumber,
            CreditCardHolderName = payerCreditCard.CreditCardHolderName,
            CreditCardExpirationMonth = payerCreditCard.CreditCardExpirationMonth,
            CreditCardExpirationYear = payerCreditCard.CreditCardExpirationYear,
            CreditCardSecurityCode = "none",
            RenterId = renter.RenterId,
            TransactionDateTime = DateTime.UtcNow,
            PaymentTypeId = (int)EfxFramework.PaymentMethods.PaymentType.CreditCard,
            RentAmount = RentPaymentAmount.BaseAmount,
            RentAmountDescription = "Rent and Fees",
            OtherAmount2 = RentPaymentAmount.FeeAmount,
            OtherDescription2 = RentPaymentAmount.FeeDescription
         };

         PaymentQueue = new PaymentQueue(PaymentQueue.Set(PaymentQueue));

         ActivityLog AL = new ActivityLog();
         AL.WriteLog("RenterId: " + renter.RenterId.ToString(), "CC payment queued for " + RentPaymentAmount.BaseAmount.ToString(), (short)LogPriority.LogAlways);

         return new PaymentResponse { RedirectToIvr = true, PaymentQueueNumber = PaymentQueue.PaymentQueueNumber };
      }

      private static PaymentResponse ProcessPayment(Dictionary<PaymentApplication, List<PaymentItem>> rentPayments, IEnumerable<PaymentItem> charityPayments, PayerAch payerAch, Renter renter,
          decimal? waivedAmount, bool waiveConvenienceFee, bool applyToNextMonthsRent)
      {
         var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);
         var RentBaseAmount = rentPayments.Values.SelectMany(r => r).Where(r => !r.Description.StartsWith("convenience fee", StringComparison.OrdinalIgnoreCase)).Sum(r => r.Amount);
         var RentPaymentAmount = new PaymentAmount(RentBaseAmount);
         var CharityBaseAmount = charityPayments.Sum(r => r.Amount);
         var CharityPaymentAmount = new PaymentAmount(CharityBaseAmount);
         var RentAchClientId = GetAchClientId(renter.RenterId, PaymentApplication.Rent);
         var CharityAchClientId = GetAchClientId(renter.RenterId, PaymentApplication.Charity);
         var Property = EfxFramework.Property.GetPropertyByRenterId(renter.RenterId);
         var Address = GetRenterAddress(renter);
         var RentResponse = new AchPaymentResponse();
         var CharityResponse = new AchPaymentResponse();

         if (!waiveConvenienceFee)
         {
            //cakel: 08/09/2016 - Added decimal amount to get conv fee
            RentPaymentAmount.CalculateFeeAmount(EfxFramework.PaymentMethods.PaymentType.ECheck, renter.RenterId, RentBaseAmount);
            RentPaymentAmount.FeeDescription = "Convenience Fee";
         }

         if (Address.AddressId == -1)
            return new PaymentResponse { RentAndFeeResult = GeneralResponseResult.AddressNotFound, RentAndFeeResponseMessage = ServiceResponseMessages.AddressNotFound };

         if (Lease.LeaseId < 1)
            return new PaymentResponse { RentAndFeeResult = GeneralResponseResult.LeaseNotFound, RentAndFeeResponseMessage = ServiceResponseMessages.LeaseNotFound };

         if (RentPaymentAmount.BaseAmount > 0.0M)
         {
            if (Property.PropertyId.HasValue && Property.PropertyId.Value > 0)
               RentResponse = AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword, RentAchClientId, RentPaymentAmount.TotalAmount, payerAch.BankRoutingNumber,
                   payerAch.BankAccountNumber, payerAch.BankAccountType.ToUpper(), renter.FirstName, renter.LastName, Address.AddressLine1, Address.AddressLine2, Address.City,
                   Address.GetStateAbbeviation(), Address.PostalCode, renter.MainPhoneNumber);
            else
               RentResponse = new AchPaymentResponse { Result = GeneralResponseResult.PropertyNotFound, ResponseDescription = ServiceResponseMessages.PropertyNotFound };
         }

         if (CharityPaymentAmount.BaseAmount > 0.0M)
         {
            CharityResponse = AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword, CharityAchClientId, CharityPaymentAmount.BaseAmount, payerAch.BankRoutingNumber,
                payerAch.BankAccountNumber, payerAch.BankAccountType.ToUpper(), renter.FirstName, renter.LastName, Address.AddressLine1, Address.AddressLine2, Address.City,
                ((StateProvince)Address.StateProvinceId).ToString(), Address.PostalCode, renter.MainPhoneNumber);
         }

         try
         {
            var Response = new PaymentResponse
            {
               RentAndFeeResult = RentResponse.Result,
               RentAndFeeResponseMessage = RentResponse.ResponseDescription,
               RentAndFeeTransactionId = RentResponse.TransactionId,
               CharityResult = CharityResponse.Result,
               CharityResponseMessage = CharityResponse.ResponseDescription,
               CharityTransactionId = CharityResponse.TransactionId,
               RedirectToIvr = false,
               ConvenienceFee = RentPaymentAmount.FeeAmount
            };


            if (Response.RentAndFeeResult == GeneralResponseResult.Success)
            {
               //cakel: BUGID00235 - Updated log to show both user and renter when admin is making a payment for a renter
               string _user;
               //Salcedo - 10/1/2014 - this function is not called only by the web, so corrected this logic to account for that,
               //otherwise we'll get an exception when run by the Scheduler application
               try
               {
                  _user = System.Web.HttpContext.Current.User.Identity.Name.ToString();
               }
               catch
               {
                  _user = "System (Non Web)";
               }
               ActivityLog AL = new ActivityLog();
               AL.WriteLog(_user, "ACH payment successful. Recording payment of " + RentPaymentAmount.BaseAmount.ToString(),
                   (short)LogPriority.LogAlways, renter.RenterId.ToString());

               AdjustCurrentBalanceDue(RentPaymentAmount.BaseAmount, Lease, applyToNextMonthsRent, Property.PmsId);
               RecordPayments(rentPayments, renter, Response, payerAch, RentPaymentAmount.FeeAmount);

               AL.WriteLog(_user, "ACH payment recorded for " + RentPaymentAmount.BaseAmount.ToString(), (short)LogPriority.LogAlways, renter.RenterId.ToString());
            }

            if (waivedAmount.HasValue && waivedAmount.Value > 0.00M)
            {
               AdjustCurrentBalanceDue(waivedAmount.Value, Lease, applyToNextMonthsRent, Property.PmsId);

               if (String.IsNullOrEmpty(Response.RentAndFeeTransactionId))
                  Response.RentAndFeeTransactionId = Guid.NewGuid().ToString();

               RecordPayments(
                   new List<PaymentItem>
                   {
                            new PaymentItem {Amount = waivedAmount.Value, Description = "Waived Payments", IsCharityPayment = false, IsRentPayment = false}
                   },
                       renter,
                       Response,
                       payerAch);
            }

            if (Response.CharityResult == GeneralResponseResult.Success)
               RecordPayments(charityPayments, renter, Response, payerAch);

            //Salcedo - 11/25/2013 - added Email receipts for payments
            if (Response.RentAndFeeResult == GeneralResponseResult.Success)
            {
               SendEmailX(RentPaymentAmount.BaseAmount, Response.RentAndFeeTransactionId, RentPaymentAmount.FeeAmount, renter, true);
            }
            else
            {
               SendEmailX(RentPaymentAmount.BaseAmount, Response.RentAndFeeTransactionId, RentPaymentAmount.FeeAmount, renter, false);
            }

            return Response;
         }
         catch (Exception Ex)
         {
            EfxFramework.Logging.ExceptionLogger.LogException(Ex);
            return null;
         }
      }

      //Salcedo - 11/25/2013 - added Email receipts for payments
      private static void SendEmailX(decimal amount, string TransactionId, decimal ConvenienceFee, Renter Resident, bool Success)
      {
         string Subject;
         string Body;
         var Property = EfxFramework.Property.GetPropertyByRenterId(Resident.RenterId);
         var Type = "Renter";

         var TheTransaction = Transaction.GetTransactionByExternalTransactionId(TransactionId);

         Subject = "";
         if (Success)
         {
            Subject = "Your Payment Confirmation";

            //Salcedo - 3/12/2014 - replaced with new html email template
            //Body = string.Format(EmailTemplates.OneTimeRentPayment, Resident.DisplayName, amount.ToString("C"), DateTime.UtcNow.ToShortDateString(),
            //    TheTransaction.InternalTransactionId, ConvenienceFee.ToString("C"), (amount + ConvenienceFee).ToString("C"));
            String RecipientName = Resident.DisplayName;

            //cakel: BUGID000180 - Added PropertyName
            String RenterProperty = Property.PropertyName;

            String PaymentAmount = amount.ToString("C");
            String TransactionDate = DateTime.UtcNow.ToShortDateString();
            String InternalTransactionId = TheTransaction.InternalTransactionId;
            String ConvenienceAmount = ConvenienceFee.ToString("C");
            String TotalAmount = (amount + ConvenienceFee).ToString("C");
            //cakel: BUGID000180 added RenterProperty (Property Name)
            Body = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-payment-success.html").Replace("[[ROOT]]",
                EmailTemplateService.EmailTemplateService.RootPath), RecipientName, RenterProperty, PaymentAmount, TransactionDate, InternalTransactionId, ConvenienceAmount, TotalAmount);

         }
         else
         {
            Subject = "Your Payment Failed";

            //Salcedo - 3/12/2014 - replaced with new html email template
            //Body = string.Format(EmailTemplates.OneTimeRentPaymentFailed, Resident.DisplayName, amount.ToString("C"), DateTime.UtcNow.ToShortDateString(),
            //    "None", ConvenienceFee.ToString("C"), (amount + ConvenienceFee).ToString("C"));
            String RecipientName = Resident.DisplayName;

            //cakel: BUGID000180 - Added PropertyName
            String RenterProperty = Property.PropertyName;

            String PaymentAmount = amount.ToString("C");
            String TransactionDate = DateTime.UtcNow.ToShortDateString();
            String InternalTransactionId = "None";
            String ConvenienceAmount = ConvenienceFee.ToString("C");
            String TotalAmount = (amount + ConvenienceFee).ToString("C");
            Body = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-payment-failure.html").Replace("[[ROOT]]",
                EmailTemplateService.EmailTemplateService.RootPath), RecipientName, RenterProperty, PaymentAmount, TransactionDate, InternalTransactionId, ConvenienceAmount, TotalAmount);
         }

         //var ResultSet = DataAccess.ExecuteNoResults(Settings.ConnectionString, "dbo.usp_Transaction_GetSingleObject", new[] { new SqlParameter("@TransactionId", transactionId) });

         //Salcedo - 2/28/2014 - changed to send to function that includes all property management email contacts
         //BulkMail.BulkMail.SendMailMessage(
         //    Property.PropertyId,
         //    Type,
         //    false,
         //    Settings.SendMailUsername,
         //    Settings.SendMailPassword,
         //    new MailAddress(Settings.PaymentEmail),
         //    Subject,
         //    Body,
         //    new List<MailAddress> { new MailAddress(Resident.PrimaryEmailAddress) }
         //    );
         int IntPropertyId = Property.PropertyId.HasValue ? Property.PropertyId.Value : 0;
         BulkMail.BulkMail.SendMailMessageIncludePropertyEmailContacts(
             IntPropertyId,
             Type,
             EfxSettings.SendMailUsername,
             EfxSettings.SendMailPassword,
             new MailAddress(EfxSettings.PaymentEmail),
             Subject,
             Body,
             new List<MailAddress> { new MailAddress(Resident.PrimaryEmailAddress) }
             );
      }

      private static Address GetRenterAddress(Renter renter)
      {
         var BillingAddress = Address.GetAddressByRenterIdAndAddressType(renter.RenterId, (int)TypeOfAddress.Billing);
         if (BillingAddress.AddressId > 0)
            return BillingAddress;

         var AddressId = 0;

         if (String.IsNullOrEmpty(renter.StreetAddress) || String.IsNullOrEmpty(renter.City) || !renter.StateProvinceId.HasValue || String.IsNullOrEmpty(renter.PostalCode))
            AddressId = -1;

         return new Address
         {
            AddressId = AddressId,
            AddressTypeId = (int)TypeOfAddress.Primary,
            AddressLine1 = renter.StreetAddress,
            AddressLine2 = renter.Unit,
            City = renter.City,
            StateProvinceId = renter.StateProvinceId.HasValue ? renter.StateProvinceId.Value : 1,
            PostalCode = renter.PostalCode
         };
      }

      private static void ProcessVoid(string transactionId)
      {
         CreditCardPayment.VoidCreditCardPayment(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, transactionId);
      }


      private static void RecordPayments(Dictionary<PaymentApplication, List<PaymentItem>> payments, Renter renter, PaymentResponse response, PayerCreditCard payerCreditCard, decimal feeAmount)
      {
         var FirstPass = feeAmount > 0.00M;
         foreach (var Request in payments.Values.SelectMany(r => r))
         {
            SetPayment(renter, PaymentAmount.GetPaymentAmount(Request.Amount, renter, (FirstPass)), Request.Description, payerCreditCard, SetPaymentApplication(Request), response);
            FirstPass = false;
         }
      }

      private static void RecordPayments(Dictionary<PaymentApplication, List<PaymentItem>> payments, Renter renter, PaymentResponse response, PayerAch payerAch, decimal feeAmount)
      {
         var FirstPass = feeAmount > 0.00M;
         foreach (var Request in payments.Values.SelectMany(r => r))
         {
            SetPayment(renter, PaymentAmount.GetPaymentAmount(Request.Amount, renter, EfxFramework.PaymentMethods.PaymentType.ECheck, (FirstPass)), Request.Description, payerAch, SetPaymentApplication(Request), response);
            FirstPass = false;
         }
      }

      private static void RecordPayments(IEnumerable<PaymentItem> payments, Renter renter, PaymentResponse response, PayerCreditCard payerCreditCard)
      {
         var FirstPass = true;
         foreach (var Request in payments)
         {
            SetPayment(renter, PaymentAmount.GetPaymentAmount(Request.Amount, renter, (FirstPass)), Request.Description, payerCreditCard,
                Request.IsCharityPayment ? PaymentApplication.Charity : PaymentApplication.Waived, response);
            FirstPass = false;
         }
      }

      private static void RecordPayments(IEnumerable<PaymentItem> payments, Renter renter, PaymentResponse response, PayerAch payerAch)
      {
         var FirstPass = true;
         foreach (var Request in payments)
         {
            SetPayment(renter, PaymentAmount.GetPaymentAmount(Request.Amount, renter, EfxFramework.PaymentMethods.PaymentType.ECheck, (FirstPass)), Request.Description, payerAch,
                Request.IsCharityPayment ? PaymentApplication.Charity : PaymentApplication.Waived, response);

            FirstPass = false;
         }
      }

      private static void SetPayment(Renter renter, PaymentAmount amount, string description, PayerCreditCard payerCreditCard, PaymentApplication paymentApplication, PaymentResponse response)
      {
         if (!renter.PayerId.HasValue)
            renter.PayerId = 0;

         int PaymentId;

         //cakel: BUGID00035 Uncommented out code.  I do not know why this was commented out
         PaymentChannel paymentChannel = GetPaymentChannel();

         //cakel: BUGID00035 - Commented out code 
         //PaymentChannel paymentChannel = EfxFramework.PaymentChannel.AutoPayment;

         //cakel: BUGID00213 Transaction - Set
         var property = EfxFramework.Property.GetPropertyByRenterId(renter.RenterId);
         bool GroupSettlementPaymentFlag = property.GroupSettlementPaymentFlag;
         bool CCAchSettled = false;
         CCAchSettled = !GroupSettlementPaymentFlag;

         switch (paymentApplication)
         {
            case PaymentApplication.Rent:
               PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.CreditCard, null, null, payerCreditCard.CreditCardHolderName, payerCreditCard.CreditCardAccountNumber,
                   payerCreditCard.CreditCardExpirationDate, amount, description, null, null, null, null, response.RentAndFeeTransactionId, false, null,
                   ((int)response.RentAndFeeResult).ToString(CultureInfo.InvariantCulture), response.RentAndFeeResponseMessage, null, paymentChannel, PaymentStatus.Approved));
               //cakel: BUGID00213
               Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int)response.RentAndFeeResult, response.RentAndFeeResponseMessage, CCAchSettled);

               break;
            case PaymentApplication.Charity:
               PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.CreditCard, null, null, payerCreditCard.CreditCardHolderName, payerCreditCard.CreditCardAccountNumber,
                   payerCreditCard.CreditCardExpirationDate, null, null, amount, description, null, null, response.CharityTransactionId, false, null,
                   ((int)response.CharityResult).ToString(CultureInfo.InvariantCulture), response.CharityResponseMessage, null, paymentChannel, PaymentStatus.Approved));
               //cakel: BUGID00213
               Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int)response.CharityResult, response.CharityResponseMessage, CCAchSettled);
               break;
            case PaymentApplication.Other:
            case PaymentApplication.Waived:
               PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.CreditCard, null, null, payerCreditCard.CreditCardHolderName, payerCreditCard.CreditCardAccountNumber,
                   payerCreditCard.CreditCardExpirationDate, null, null, null, null, amount, description, response.RentAndFeeTransactionId, false, null,
                   ((int)response.RentAndFeeResult).ToString(CultureInfo.InvariantCulture), response.RentAndFeeResponseMessage, null, paymentChannel, PaymentStatus.Approved));
               //cakel: BUGID00213
               Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int)response.RentAndFeeResult, response.RentAndFeeResponseMessage, CCAchSettled);
               break;
         }
      }

      private static void SetPayment(Renter renter, PaymentAmount amount, string description, PayerAch payerAch, PaymentApplication paymentApplication, PaymentResponse response)
      {
         if (!renter.PayerId.HasValue)
            renter.PayerId = 0;

         int PaymentId;

         PaymentChannel paymentChannel = GetPaymentChannel();

         switch (paymentApplication)
         {
            case PaymentApplication.Rent:
               PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.ECheck, payerAch.BankAccountNumber, payerAch.BankRoutingNumber, null, null,
                   null, amount, description, null, null, null, null, response.RentAndFeeTransactionId, false, null,
                   ((int)response.RentAndFeeResult).ToString(CultureInfo.InvariantCulture), response.RentAndFeeResponseMessage, null, paymentChannel, PaymentStatus.Processing));

               Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int)response.RentAndFeeResult, response.RentAndFeeResponseMessage);
               break;
            case PaymentApplication.Charity:
               PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.ECheck, payerAch.BankAccountNumber, payerAch.BankRoutingNumber, null, null,
                   null, null, null, amount, description, null, null, response.CharityTransactionId, false, null,
                   ((int)response.CharityResult).ToString(CultureInfo.InvariantCulture), response.CharityResponseMessage, null, paymentChannel, PaymentStatus.Processing));

               Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int)response.CharityResult, response.CharityResponseMessage);
               break;
            case PaymentApplication.Other:
            case PaymentApplication.Waived:
               PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.ECheck, payerAch.BankAccountNumber, payerAch.BankRoutingNumber, null, null,
                   null, null, null, null, null, amount, description, response.RentAndFeeTransactionId, false, null,
                   ((int)response.RentAndFeeResult).ToString(CultureInfo.InvariantCulture), response.RentAndFeeResponseMessage, null, paymentChannel, PaymentStatus.Processing));

               Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int)response.RentAndFeeResult, response.RentAndFeeResponseMessage);
               break;
         }
      }

      private static PaymentChannel GetPaymentChannel()
      {
         var paymentChannel = PaymentChannel.Web;

         //Salcedo - 2/27/2014 - if the payment was scheduled, we don't have a CurrentUserID, so just set the channel to AutoPayment
         if (bScheduledPayment)
         {
            paymentChannel = PaymentChannel.AutoPayment;
         }
         else
         {
            var renter = new Renter(EfxFramework.Helpers.Helper.CurrentUserId);
            if (renter.RenterId != 0 && EfxSettings.CurrentSite.ToLower().Equals("resident"))
            {
               paymentChannel = PaymentChannel.ResidentSite;
            }
            else if (EfxFramework.Helpers.Helper.IsRpoAdmin && EfxSettings.CurrentSite.ToLower().Equals("admin"))
            {
               paymentChannel = PaymentChannel.AdminSite;
            }
            else if ((EfxFramework.Helpers.Helper.IsCorporateAdmin || EfxFramework.Helpers.Helper.IsPropertyManager) &&
                     EfxSettings.CurrentSite.ToLower().Equals("admin"))
            {
               paymentChannel = PaymentChannel.PropertyManagerSite;
            }
            //else if ((EfxFramework.Helpers.Helper.IsCsrUser || EfxFramework.Helpers.Helper.IsPropertyManager) &&
            //         EfxSettings.CurrentSite.ToLower().Equals("admin"))
            //{
            //   paymentChannel = PaymentChannel.TelePhone;
            //}
         }

         return paymentChannel;
      }

      /// <summary>
      /// REFACTOR!
      /// </summary>
      /// <param name="renterId"></param>
      /// <param name="paymentApplication"></param>
      /// <returns></returns>
      private static string GetAchClientId(int renterId, PaymentApplication paymentApplication)
      {
         var Property = EfxFramework.Property.GetPropertyByRenterId(renterId);

         switch (paymentApplication)
         {
            case PaymentApplication.Rent:
               return Property.RentalAccountAchClientId;
            case PaymentApplication.Charity:
               return Property.RentalAccountAchClientId;
            case PaymentApplication.Other:
               return Property.FeeAccountAchClientId;
            default:
               return Property.RentalAccountAchClientId;
         }
      }


      private static Dictionary<PaymentApplication, List<PaymentItem>> GetRentPayments(RenterPaymentInfo selectedRenterPaymentInfo)
      {
         var RentPayments = new Dictionary<PaymentApplication, List<PaymentItem>>();
         var OtherPayments = new List<PaymentItem>();

         if (selectedRenterPaymentInfo.RentAmount.HasValue && selectedRenterPaymentInfo.RentAmount > 0)
            RentPayments.Add(PaymentApplication.Rent,
                new List<PaymentItem> { new PaymentItem { Amount = selectedRenterPaymentInfo.RentAmount.Value, Description = selectedRenterPaymentInfo.RentAmountDescription, IsRentPayment = true } });

         if (selectedRenterPaymentInfo.OtherAmount.HasValue && selectedRenterPaymentInfo.OtherAmount.Value > 0 && !selectedRenterPaymentInfo.OtherAmountDescription.ToLower().Contains("convenience fee"))
            OtherPayments.Add(new PaymentItem { Amount = selectedRenterPaymentInfo.OtherAmount.Value, Description = selectedRenterPaymentInfo.OtherAmountDescription });

         if (selectedRenterPaymentInfo.OtherAmount2.HasValue && selectedRenterPaymentInfo.OtherAmount2.Value > 0 && !selectedRenterPaymentInfo.OtherAmountDescription2.ToLower().Contains("convenience fee"))
            OtherPayments.Add(new PaymentItem { Amount = selectedRenterPaymentInfo.OtherAmount2.Value, Description = selectedRenterPaymentInfo.OtherAmountDescription2 });

         if (selectedRenterPaymentInfo.OtherAmount3.HasValue && selectedRenterPaymentInfo.OtherAmount3.Value > 0 && !selectedRenterPaymentInfo.OtherAmountDescription3.ToLower().Contains("convenience fee"))
            OtherPayments.Add(new PaymentItem { Amount = selectedRenterPaymentInfo.OtherAmount3.Value, Description = selectedRenterPaymentInfo.OtherAmountDescription3 });

         if (selectedRenterPaymentInfo.OtherAmount4.HasValue && selectedRenterPaymentInfo.OtherAmount4.Value > 0 && !selectedRenterPaymentInfo.OtherAmountDescription4.ToLower().Contains("convenience fee"))
            OtherPayments.Add(new PaymentItem { Amount = selectedRenterPaymentInfo.OtherAmount4.Value, Description = selectedRenterPaymentInfo.OtherAmountDescription4 });

         if (selectedRenterPaymentInfo.OtherAmount5.HasValue && selectedRenterPaymentInfo.OtherAmount5.Value > 0 && !selectedRenterPaymentInfo.OtherAmountDescription5.ToLower().Contains("convenience fee"))
            OtherPayments.Add(new PaymentItem { Amount = selectedRenterPaymentInfo.OtherAmount5.Value, Description = selectedRenterPaymentInfo.OtherAmountDescription5 });

         RentPayments.Add(PaymentApplication.Other, OtherPayments);

         return RentPayments;
      }

      /// <summary>
      /// OBSOLETE!!!
      /// </summary>
      /// <param name="donationAmount"></param>
      /// <param name="charityName"></param>
      /// <returns></returns>
      private static IList<PaymentItem> GetCharityPayments(decimal? donationAmount, string charityName)
      {
         var DonationAmount = donationAmount.HasValue ? donationAmount.Value : 0.0M;
         return DonationAmount > 0.0M ? new List<PaymentItem> { new PaymentItem { Amount = DonationAmount, Description = charityName, IsCharityPayment = true } } : new List<PaymentItem>();
      }

      /// <summary>
      /// REFACTOR!!!
      /// </summary>
      /// <param name="paymentItem"></param>
      /// <returns></returns>
      private static PaymentApplication SetPaymentApplication(PaymentItem paymentItem)
      {
         if (paymentItem.IsCharityPayment)
            return PaymentApplication.Charity;

         if (paymentItem.IsRentPayment)
            return PaymentApplication.Rent;

         return PaymentApplication.Other;
      }

      public static PaymentResponse ConvertPaymentResponse(AchPaymentResponse response, PaymentApplication application)
      {
         switch (application)
         {
            case PaymentApplication.Rent:
            case PaymentApplication.Other:
               return new PaymentResponse { RentAndFeeResult = response.Result, RentAndFeeResponseMessage = response.ResponseDescription, RentAndFeeTransactionId = response.TransactionId };
            case PaymentApplication.Charity:
               return new PaymentResponse { CharityResult = response.Result, CharityResponseMessage = response.ResponseDescription, CharityTransactionId = response.TransactionId };
            default:
               return new PaymentResponse();
         }
      }

      public static PaymentResponse ConvertPaymentResponse(CreditCardPaymentResponse response, PaymentApplication application)
      {
         switch (application)
         {
            case PaymentApplication.Rent:
            case PaymentApplication.Other:
               return new PaymentResponse { RentAndFeeResult = response.Result, RentAndFeeResponseMessage = response.ResponseMessage, RentAndFeeTransactionId = response.TransactionId, RedirectToIvr = false };
            case PaymentApplication.Charity:
               return new PaymentResponse { CharityResult = response.Result, CharityResponseMessage = response.ResponseMessage, CharityTransactionId = response.TransactionId, RedirectToIvr = false };
            default:
               return new PaymentResponse();
         }
      }

      public static RenterPaymentType GetPaymentType(string selectedPaymentType)
      {
         switch (selectedPaymentType)
         {
            case "echeck":
               return RenterPaymentType.ECheck;

            default:
               return RenterPaymentType.Credit;
         }
      }
   }
}
