﻿namespace EfxFramework.WebPayments
{
    public class PaymentItem
    {
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public bool IsCharityPayment { get; set; }
        public bool IsRentPayment { get; set; }
    }
}
