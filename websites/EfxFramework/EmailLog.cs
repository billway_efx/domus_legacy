using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Mb2x.Data;

namespace EfxFramework
{
	public class EmailLog : BaseEntity
	{
		//Public Properties
		public int EmailLogId { get; set; }
		public Guid MailMessageId { get; set; }
		public string Dissemination { get; set; }
		public DateTime DateSent { get; set; }
		public bool IsBulkMail { get; set; }

        public static List<EmailLog> GetAllBulkEmailLogs()
        {
            return DataAccess.GetTypedList<EmailLog>(EfxSettings.ConnectionString, "dbo.usp_EmailLog_GetBulkMailEmailLog");
        }

        public static List<EmailLog> GetEmailLogListByMailMessageId(Guid mailMessageId)
        {
            return DataAccess.GetTypedList<EmailLog>(EfxSettings.ConnectionString, "dbo.usp_EmailLog_GetEmailLogByMailMessageId", new SqlParameter("@MailMessageId", mailMessageId));
        }

        public static void AddPropertyToEmailLog(int emailLogId, int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_EmailLogProperty_AddPropertyToEmailLog", 
                new [] { new SqlParameter("@EmailLogId", emailLogId), new SqlParameter("@PropertyId", propertyId) });
        }

        public static int Set(int emailLogId, Guid mailMessageId, string dissemination, DateTime dateSent, bool isBulkMail)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(EfxSettings.ConnectionString, "dbo.usp_EmailLog_SetSingleObject",
                new[]
                {
	                new SqlParameter("@EmailLogId", emailLogId),
	                new SqlParameter("@MailMessageId", mailMessageId),
	                new SqlParameter("@Dissemination", dissemination),
	                new SqlParameter("@DateSent", dateSent),
	                new SqlParameter("@IsBulkMail", isBulkMail)
                });
        }

        public static int Set(EmailLog e)
        {
	        return Set(
		        e.EmailLogId,
		        e.MailMessageId,
		        e.Dissemination,
		        e.DateSent,
		        e.IsBulkMail
	        );
        }

        public EmailLog(int? emailLogId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_EmailLog_GetSingleObject", new [] { new SqlParameter("@EmailLogId", emailLogId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        EmailLogId = (int)ResultSet[0];
		        MailMessageId = (Guid)ResultSet[1];
		        Dissemination = ResultSet[2] as string;
		        DateSent = (DateTime)ResultSet[3];
		        IsBulkMail = (bool)ResultSet[4];
            }
        }

        public EmailLog()
        {
            
        }
	}
}
