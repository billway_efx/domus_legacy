using System.Collections.Generic;
using Mb2x.Data;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class PropertyAnnouncement : BaseEntity
	{
		//Public Properties
		[DisplayName(@"PropertyId"), Required(ErrorMessage = "PropertyId is required.")]
		public int PropertyId { get; set; }

		[DisplayName(@"AnnouncementId"), Required(ErrorMessage = "AnnouncementId is required.")]
		public int AnnouncementId { get; set; }


        public static int Set(int propertyId, int announcementId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_PropertyAnnouncement_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@PropertyId", propertyId),
	                    new SqlParameter("@AnnouncementId", announcementId)
                    });
        }

        public static int Set(PropertyAnnouncement p)
        {
	        return Set(
		        p.PropertyId,
		        p.AnnouncementId
	        );
        }

        public PropertyAnnouncement(int propertyId, int announcementId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_PropertyAnnouncement_GetSingleObject", new [] { new SqlParameter("@PropertyId", propertyId), new SqlParameter("@AnnouncementId", announcementId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        PropertyId = (int)ResultSet[0];
		        AnnouncementId = (int)ResultSet[1];
            }
        }

		public PropertyAnnouncement()
		{
			InitializeObject();
		}

	}
}
