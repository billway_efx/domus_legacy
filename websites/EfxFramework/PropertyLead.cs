using System;
using System.Collections.Generic;
using EfxFramework.Web.Caching;
using Mb2x.Data;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class PropertyLead : BaseEntity
	{
		//Public Properties
		[DisplayName(@"PropertyLeadId"), Required(ErrorMessage = "PropertyLeadId is required.")]
		public int PropertyLeadId { get; set; }

		[DisplayName(@"PropertyManagementCompanyName"), Required(ErrorMessage = "PropertyManagementCompanyName is required."), StringLength(100)]
		public string PropertyManagementCompanyName { get; set; }

		[DisplayName(@"PropertyName"), Required(ErrorMessage = "PropertyName is required."), StringLength(200)]
		public string PropertyName { get; set; }

		[DisplayName(@"ContactFirstName"), StringLength(100)]
		public string ContactFirstName { get; set; }

		[DisplayName(@"ContactLastName"), StringLength(100)]
		public string ContactLastName { get; set; }

		[DisplayName(@"Address"), StringLength(100)]
		public string Address { get; set; }

		[DisplayName(@"Address2"), StringLength(100)]
		public string Address2 { get; set; }

		[DisplayName(@"City"), StringLength(100)]
		public string City { get; set; }

		[DisplayName(@"StateProvinceId")]
		public int? StateProvinceId { get; set; }

		[DisplayName(@"PostalCode"), StringLength(10)]
		public string PostalCode { get; set; }

		[DisplayName(@"Phone"), StringLength(10)]
		public string Phone { get; set; }

		[DisplayName(@"Fax"), StringLength(10)]
		public string Fax { get; set; }

		[DisplayName(@"EmailAddress"), StringLength(100)]
		public string EmailAddress { get; set; }

        [DisplayName(@"DateSubmitted"), Required(ErrorMessage = "DateSubmitted is required.")]
        public DateTime DateSubmitted { get; set; }


        public static List<PropertyLead> GetAllPropertyLeads()
        {
            var Leads = CacheManager.GetCacheItem<List<PropertyLead>>(CacheKey.PropertyLeadCache);

            if (Leads == null || Leads.Count < 1)
                CacheManager.AddItemToCache(CacheKey.PropertyLeadCache, DataAccess.GetTypedList<PropertyLead>(EfxSettings.ConnectionString, "dbo.usp_PropertyLead_GetAll"), 60);

            return CacheManager.GetCacheItem<List<PropertyLead>>(CacheKey.PropertyLeadCache);
        }

        public static int Set(int propertyLeadId, string propertyManagementCompanyName, string propertyName, string contactFirstName, string contactLastName, string address, string address2, string city, int? stateProvinceId, string postalCode, string phone, string fax, string emailAddress, DateTime dateSubmitted)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_PropertyLead_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@PropertyLeadId", propertyLeadId),
	                    new SqlParameter("@PropertyManagementCompanyName", propertyManagementCompanyName),
	                    new SqlParameter("@PropertyName", propertyName),
	                    new SqlParameter("@ContactFirstName", contactFirstName),
	                    new SqlParameter("@ContactLastName", contactLastName),
	                    new SqlParameter("@Address", address),
	                    new SqlParameter("@Address2", address2),
	                    new SqlParameter("@City", city),
	                    new SqlParameter("@StateProvinceId", stateProvinceId),
	                    new SqlParameter("@PostalCode", postalCode),
	                    new SqlParameter("@Phone", phone),
	                    new SqlParameter("@Fax", fax),
	                    new SqlParameter("@EmailAddress", emailAddress),
                        new SqlParameter("@DateSubmitted", dateSubmitted)
                    });
        }

        public static int Set(PropertyLead p)
        {
	        return Set(
		        p.PropertyLeadId,
		        p.PropertyManagementCompanyName,
		        p.PropertyName,
		        p.ContactFirstName,
		        p.ContactLastName,
		        p.Address,
		        p.Address2,
		        p.City,
		        p.StateProvinceId,
		        p.PostalCode,
		        p.Phone,
		        p.Fax,
		        p.EmailAddress,
                p.DateSubmitted
	        );
        }

        public PropertyLead(int propertyLeadId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_PropertyLead_GetSingleObject", new [] { new SqlParameter("@PropertyLeadId", propertyLeadId) });

            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        PropertyLeadId = (int)ResultSet[0];
		        PropertyManagementCompanyName = ResultSet[1] as string;
		        PropertyName = ResultSet[2] as string;
		        ContactFirstName = ResultSet[3] as string;
		        ContactLastName = ResultSet[4] as string;
		        Address = ResultSet[5] as string;
		        Address2 = ResultSet[6] as string;
		        City = ResultSet[7] as string;
		        StateProvinceId = ResultSet[8] as int?;
		        PostalCode = ResultSet[9] as string;
		        Phone = ResultSet[10] as string;
		        Fax = ResultSet[11] as string;
		        EmailAddress = ResultSet[12] as string;
                DateSubmitted = (DateTime) ResultSet[13];
            }
        }

		public PropertyLead()
		{
			InitializeObject();
		}

	}
}
