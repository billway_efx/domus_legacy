﻿namespace EfxFramework.PaymentMethods
{
    public abstract class PaymentMethodsBase
    {
        public Address BillingAddress { get; set; }
        public string BillingFirstName { get; set; }
        public string BillingLastName { get; set; }

        protected PaymentMethodsBase(){}

        protected PaymentMethodsBase(Address billingAddress, string firstName, string lastName)
        {
            BillingAddress = billingAddress;
            BillingFirstName = firstName;
            BillingLastName = lastName;
        }
    }
}
