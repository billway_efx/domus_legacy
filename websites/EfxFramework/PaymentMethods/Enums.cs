﻿using System.ComponentModel;

namespace EfxFramework.PaymentMethods
{
    public enum PaymentType
    {
        [Description("Credit Card")]
        CreditCard = 1,
        [Description("Electronic Check")]
        ECheck = 2,
		[Description("Rent By Cash")]
        RentByCash = 3
    }

    public enum PaymentApplication
    {
        Rent = 1,
        Charity = 2,
        Other = 3,
        Waived = 4
    }

    public enum AccountType
    {
        Checking = 1,
        Savings = 2
    }
}
