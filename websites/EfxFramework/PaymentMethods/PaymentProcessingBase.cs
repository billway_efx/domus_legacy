﻿using System.Net.Mail;
using EfxFramework.PaymentMethods.Ach;
using EfxFramework.PaymentMethods.CreditCard;
using EfxFramework.PublicApi.Payment;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EfxFramework.Resources;
using Mb2x.ExtensionMethods;

namespace EfxFramework.PaymentMethods
{
    public abstract class PaymentProcessingBase
    {
        #region Private Variables
        private Applicant _Applicant;
        private ApplicantApplication _ApplicantApplication;
        private Renter _Resident;
        private Property _Property;
        private Lease _Lease;
        private PayerCreditCard _PayerCc;
        private PayerAch _PayerAch;
        #endregion

        #region Public Properties
        public int ApplicantId { get; set; }
        public int ApplicantApplicationId { get; set; }
        public int ResidentId { get; set; }
        public int PropertyId { get; set; }
        public int LeaseId { get; set; }
        public int PayerCreditCardId { get; set; }
        public int PayerAchId { get; set; }
        public bool UseIvr { get; set; }
        public PaymentChannel PaymentChannel { get; set; }

        public Applicant Applicant
        {
            get
            {
                if (_Applicant == null || _Applicant.ApplicantId < 1)
                    _Applicant = new Applicant(ApplicantId);

                return _Applicant;
            }
        }

        public ApplicantApplication ApplicantApplication
        {
            get
            {
                if (_ApplicantApplication == null || ApplicantApplication.ApplicantApplicationId < 1)
                    _ApplicantApplication = new ApplicantApplication(ApplicantApplicationId);

                return _ApplicantApplication;
            }
        }

        public Renter Resident
        {
            get
            {
                if (_Resident == null || _Resident.RenterId < 1)
                    _Resident = new Renter(ResidentId);

                return _Resident;
            }
        }

        public Property Property
        {
            get
            {
                if (_Property == null || !_Property.PropertyId.HasValue || _Property.PropertyId.Value < 1)
                    _Property = new Property(PropertyId);

                return _Property;
            }
        }

        public Lease Lease
        {
            get
            {
                if (_Lease == null || _Lease.LeaseId < 1)
                    _Lease = new Lease(LeaseId);

                return _Lease;
            }
        }

        public PayerCreditCard PayerCc
        {
            get
            {
                if (_PayerCc == null || _PayerCc.PayerCreditCardId < 1)
                    _PayerCc = new PayerCreditCard(PayerCreditCardId);

                return _PayerCc;
            }
        }

        public PayerAch PayerAch
        {
            get
            {
                if (_PayerAch == null || _PayerAch.PayerAchId < 1)
                    _PayerAch = new PayerAch(PayerAchId);

                return _PayerAch;
            }
        }
        #endregion

        #region Constructors
        protected PaymentProcessingBase(PaymentChannel channel)
        {
            PaymentChannel = channel;
            SetIvr();
        }
        protected PaymentProcessingBase(PaymentChannel channel, int propertyId)
        {
            PropertyId = propertyId;
            PaymentChannel = channel;
            SetIvr();
        }

        protected PaymentProcessingBase(PaymentChannel channel, int propertyId, int residentId)
        {
            PropertyId = propertyId;
            PaymentChannel = channel;
            ResidentId = residentId;
            SetIvr();
        }
        #endregion

        #region Processing Methods
        /// <summary>
        /// Processes a Credit Card Payment for Applicants paying the default application fees for the property
        /// </summary>
        protected virtual PaymentResponseBase ProcessApplicantPayment(CreditCardDetails ccDetails, bool cvvRequired = true)
        {
            PaymentResponseBase Response;

            if (!ValidateCreditCard(ccDetails, out Response, cvvRequired))
                return Response;
            //If we ever charge convenience fees to the applicant, then uncomment this to use the IVR
            //if (UseIvr)
            //    return ProcessPaymentQueue(ccDetails, GetPaymentAmount(true, PaymentType.CreditCard));

            return ProcessCreditCard(GetPaymentAmount(true, PaymentType.CreditCard), ccDetails, Response, true);
        }

        /// <summary>
        /// Processes an ACH Payment for Applicants paying the default application fees for the property
        /// </summary>
        protected virtual PaymentResponseBase ProcessApplicantPayment(EcheckDetails eCheckDetails)
        {
            PaymentResponseBase Response;

            if (!ValidateEcheck(eCheckDetails, out Response))
                return Response;

            return ProcessEcheck(GetPaymentAmount(true, PaymentType.ECheck), eCheckDetails, Response, true);
        }

        /// <summary>
        /// Processes a Credit Card Payment for Residents paying the default Rent and Fees from the Lease
        /// </summary>
        protected virtual PaymentResponseBase ProcessResidentPayment(CreditCardDetails ccDetails, bool cvvRequired = true)
        {
            PaymentResponseBase Response;

            if (!ValidateCreditCard(ccDetails, out Response, cvvRequired))
                return Response;

            //Salcedo - 3/25/2014 - we're no longer using the IVR
            //if (UseIvr)
            //    return ProcessPaymentQueue(ccDetails, GetPaymentAmount(false, PaymentType.CreditCard));

            return ProcessCreditCard(GetPaymentAmount(false, PaymentType.CreditCard), ccDetails, Response, false);
        }

        /// <summary>
        /// Processes an ACH Payment for Residents paying the default Rent and Fees from the Lease
        /// </summary>
        protected virtual PaymentResponseBase ProcessResidentPayment(EcheckDetails eCheckDetails)
        {
            PaymentResponseBase Response;

            if (!ValidateEcheck(eCheckDetails, out Response))
                return Response;

            return ProcessEcheck(GetPaymentAmount(false, PaymentType.ECheck), eCheckDetails, Response, false);
        }

        /// <summary>
        /// Processes a Credit Card Payment for an Applicant with a non-default payment amount
        /// </summary>
        protected virtual PaymentResponseBase ProcessApplicantPayment(CreditCardDetails ccDetails, PaymentAmount amount, bool cvvRequired = true)
        {
            PaymentResponseBase Response;

            if (!ValidateCreditCard(ccDetails, out Response, cvvRequired))
                return Response;
            //If we ever charge convenience fees to the applicant, then uncomment this to use the IVR
            //if (UseIvr)
            //    return ProcessPaymentQueue(ccDetails, amount);

            return ProcessCreditCard(amount, ccDetails, Response, true);
        }

        /// <summary>
        /// Processes an ACH Payment for an Applicant with a non-default payment amount
        /// </summary>
        protected virtual PaymentResponseBase ProcessApplicantPayment(EcheckDetails eCheckDetails, PaymentAmount amount)
        {
            PaymentResponseBase Response;

            if (!ValidateEcheck(eCheckDetails, out Response))
                return Response;

            return ProcessEcheck(amount, eCheckDetails, Response, true);
        }

        /// <summary>
        /// Processes a Credit Card Payment for a Resident with a non-default payment amount, adjusting the Current Balance Due for the Waived Amount
        /// </summary>
        protected virtual PaymentResponseBase ProcessResidentPayment(CreditCardDetails ccDetails, PaymentAmount amount, decimal? waivedAmount = null, bool cvvRequired = true)
        {
            PaymentResponseBase Response;

            if (!ValidateCreditCard(ccDetails, out Response, cvvRequired))
                return Response;

            //Salcedo - 3/25/2014 - we're no longer using the IVR
            //if (UseIvr)
            //    return ProcessPaymentQueue(ccDetails, amount);

            return ProcessCreditCard(amount, ccDetails, Response, false, waivedAmount);
        }

        /// <summary>
        /// Processes an ACH for a Resident with a non-default payment amount, adjusting the Current Balance Due for the Waived Amount
        /// </summary>
        protected virtual PaymentResponseBase ProcessResidentPayment(EcheckDetails eCheckDetails, PaymentAmount amount, decimal? waivedAmount = null)
        {
            PaymentResponseBase Response;

            if (!ValidateEcheck(eCheckDetails, out Response))
                return Response;

            return ProcessEcheck(amount, eCheckDetails, Response, false, waivedAmount);
        }

        /// <summary>
        /// Gets the Address for a Resident, billing address first, if none exists, then mailing.
        /// </summary>
        protected Address GetResidentAddress()
        {
            var BillingAddress = Address.GetAddressByRenterIdAndAddressType(Resident.RenterId, (int)TypeOfAddress.Billing);
            if (BillingAddress.AddressId > 0)
                return BillingAddress;

            return new Address
            {
                AddressId = BillingAddress.AddressId,
                AddressTypeId = BillingAddress.AddressId < 1 ? (int)TypeOfAddress.Primary : (int)TypeOfAddress.Billing,
                AddressLine1 = BillingAddress.AddressId < 1 ? Resident.StreetAddress : BillingAddress.AddressLine1,
                AddressLine2 = BillingAddress.AddressId < 1 ? Resident.Unit : BillingAddress.AddressLine2,
                City = BillingAddress.AddressId < 1 ? Resident.City : BillingAddress.City,
                StateProvinceId = BillingAddress.AddressId < 1 ? Resident.StateProvinceId.HasValue ? Resident.StateProvinceId.Value : 1 : BillingAddress.StateProvinceId,
                PostalCode = BillingAddress.AddressId < 1 ? Resident.PostalCode : BillingAddress.PostalCode,
                PrimaryEmailAddress = BillingAddress.AddressId < 1 || String.IsNullOrEmpty(BillingAddress.PrimaryEmailAddress) ? Resident.PrimaryEmailAddress : BillingAddress.PrimaryEmailAddress,
                PrimaryPhoneNumber = BillingAddress.AddressId < 1 || String.IsNullOrEmpty(BillingAddress.PrimaryPhoneNumber) ? Resident.MainPhoneNumber : BillingAddress.PrimaryPhoneNumber
            };
        }
        #endregion

        #region Payment Methods
        protected virtual PaymentResponseBase ProcessCreditCard(PaymentAmount amount, CreditCardDetails ccDetails, PaymentResponseBase response, bool isApplicant, decimal? waivedAmount = null)
        {
            var CcResponse = new CreditCardPaymentResponse();
            SetParticipatingRenter();
            try
            {
                CcResponse = CreditCardPayment.ProcessCreditCard(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, IsTestTransaction(PaymentType.CreditCard),
                    amount.TotalAmount, ccDetails.CreditCardNumber, ccDetails.CreditCardExpirationString, ccDetails.CvvCode, ccDetails.CardHolderName, BuildCustomFields(amount, isApplicant));

                response = ConvertPaymentResponse(response, CcResponse);
            }
            catch
            {
                response = ConvertPaymentResponse(response, CcResponse, (int)GeneralResponseResult.GeneralFailure);
            }

            if (response.Message == ServiceResponseMessages.Ccaccepted)
            {
                ProcessVoid(response.TransactionId);
            }

            if (!isApplicant && response.Result == 0)
                UpdateLeaseBalances(amount.BaseAmount + (waivedAmount.HasValue ? waivedAmount.Value : 0.00M));

            var PaymentId = RecordPayment(isApplicant, response, PaymentType.CreditCard, amount.TotalAmount, ccDetails);

            if (PaymentId == 0)
            {
                response.Result = (int) GeneralResponseResult.PaymentFailedToRecord;
                response.Message = ServiceResponseMessages.PaymentFailedToRecord;
            }

            SendEmail(amount, response, (response.Result == 0));

            return response;
        }

        protected virtual PaymentResponseBase ProcessEcheck(PaymentAmount amount, EcheckDetails eCheckDetails, PaymentResponseBase response, bool isApplicant, decimal? waivedAmount = null)
        {
            var AchResponse = new AchPaymentResponse();
            SetParticipatingRenter();
            try
            {
                AchResponse = AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword, GetAchClientId(isApplicant), amount.TotalAmount, eCheckDetails.BankRoutingNumber,
                    eCheckDetails.BankAccountNumber, eCheckDetails.TypeOfAccount.ToString().ToUpper(), eCheckDetails.BillingFirstName, eCheckDetails.BillingLastName,
                    eCheckDetails.BillingAddress.AddressLine1, eCheckDetails.BillingAddress.AddressLine2, eCheckDetails.BillingAddress.City, eCheckDetails.BillingAddress.GetStateAbbeviation(),
                    eCheckDetails.BillingAddress.PostalCode, eCheckDetails.BillingAddress.PrimaryPhoneNumber);

                response = ConvertPaymentResponse(response, AchResponse);
            }
            catch
            {
                response = ConvertPaymentResponse(response, AchResponse, (int)GeneralResponseResult.GeneralFailure);
            }

            if (!isApplicant && response.Result == 0)
                UpdateLeaseBalances(amount.BaseAmount + (waivedAmount.HasValue ? waivedAmount.Value : 0.00M));

            var PaymentId = RecordPayment(isApplicant, response, PaymentType.ECheck, amount.TotalAmount, eCheckDetails);

            if (PaymentId == 0)
            {
                response.Result = (int)GeneralResponseResult.PaymentFailedToRecord;
                response.Message = ServiceResponseMessages.PaymentFailedToRecord;
            }

            SendEmail(amount, response, (response.Result == 0));

            return response;
        }
        #endregion

        #region Helper Methods
        protected bool IsTestTransaction(PaymentType paymentType)
        {
            switch (paymentType)
            {
                case PaymentType.CreditCard:
                    return Resident.IsCreditCardTestRenter || !EfxSettings.ProcessCreditCards || EfxSettings.UseTestMode;
                    
                case PaymentType.ECheck:
                    return EfxSettings.UseTestMode;

                default:
                    return false;
            }
        }

        protected virtual PayerCreditCard GetPayerCreditCard()
        {
            _PayerCc = PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(Resident.PayerId.HasValue ? Resident.PayerId.Value : 0);

            if (_PayerCc == null || _PayerCc.PayerCreditCardId < 1)
                _PayerCc = PayerCreditCard.GetAllPayerCreditCardByPayerId(Resident.PayerId.HasValue ? Resident.PayerId.Value : 0).FirstOrDefault();

            PayerCreditCardId = _PayerCc != null ? _PayerCc.PayerCreditCardId : 0;

            return _PayerCc;
        }

        protected virtual PayerAch GetPayerAch()
        {
            _PayerAch = PayerAch.GetPrimaryPayerAchByPayerId(Resident.PayerId.HasValue ? Resident.PayerId.Value : 0);

            if (_PayerAch == null || _PayerAch.PayerAchId < 1)
                _PayerAch = PayerAch.GetAllPayerAchByPayerId(Resident.PayerId.HasValue ? Resident.PayerId.Value : 0).FirstOrDefault();

            PayerAchId = _PayerAch != null ? _PayerAch.PayerAchId : 0;

            return _PayerAch;
        }

        protected virtual List<LeaseFee> GetLeaseFees()
        {
            return LeaseFee.GetFeesByLeaseId(LeaseId);
        }

        protected virtual List<ApplicationFee> GetApplicationFees()
        {
            return ApplicationFee.GetAllApplicationFeesByPropertyId(PropertyId);
        }

        protected virtual PaymentAmount GetPaymentAmount(bool isApplicant, PaymentType paymentType)
        {
            return isApplicant ? GetApplicantPaymentAmount() : GetRentPaymentAmount(paymentType);
        }

        protected virtual PaymentAmount GetApplicantPaymentAmount()
        {
            return new PaymentAmount(GetApplicationFees().Where(f => f.IsActive).Sum(f => f.FeesPaid));
        }

        protected virtual PaymentAmount GetRentPaymentAmount(PaymentType paymentType)
        {
            //cakel: 00595 Removed Lease.TotalFees  The new method will have fee built into the current balance due
            return new PaymentAmount(Lease.CurrentBalanceDue.HasValue ? Lease.CurrentBalanceDue.Value : 0.00M, "Convenience Fee", paymentType, ResidentId);
        }

        protected virtual void SetParticipatingRenter()
        {
            if (Resident.RenterId < 1)
                return;

            Resident.IsParticipating = true;
            Renter.Set(Resident, "System Process");
        }

        protected virtual void UpdateLeaseBalances(decimal amount)
        {
            if (Lease.LeaseId < 1)
                return;

            Lease.CurrentBalanceDue -= amount;
            Lease.CurrentBalanceDueLastUpdated = DateTime.UtcNow;
            //Lease.PaymentDueDate = Lease.PaymentDueDate.HasValue ? Lease.PaymentDueDate.Value.AddMonths(1) : DateTime.UtcNow.AddMonths(1);
            Lease.Set(Lease);
        }
        #endregion

        #region Payment Recording
        protected virtual int RecordPayment(bool isApplicant, PaymentResponseBase response, PaymentType paymentType, decimal total, PaymentMethodsBase paymentMethodDetails)
        {
            var Result = 0;

            var Transaction = BuildTransaction(response, total, paymentType, paymentMethodDetails);
            if (Transaction == null)
                return Result;

            Result = isApplicant ? RecordApplicationFeePayment(response, Transaction.TransactionId) : RecordRentPayment(response);

            //TODO: After Payment is recorded, set the PaymentId on the Transaction record

            return Result;
        }

        protected virtual int RecordApplicationFeePayment(PaymentResponseBase response, int transactionId)
        {
            foreach (var Fee in GetApplicationFees())
            {
                FeePayment.Set(BuildFeePayment(Fee.ApplicationFeeId, ApplicantId, response.Result == 0, response.TransactionDate, transactionId));
            }

            return transactionId;
        }

        protected virtual int RecordRentPayment(PaymentResponseBase response)
        {
            //TODO: Finish when Payment Table refactor is complete
            return 0;
        }

        protected FeePayment BuildFeePayment(int feeId, int applicantId, bool paid, DateTime transactionDate, int transactionId)
        {
            return new FeePayment
            {
                ApplicationFeeId = feeId,
                ApplicantId = applicantId,
                FeePaid = paid,
                TransactionDate = transactionDate,
                TransactionId = transactionId
            };
        }
        #endregion

        #region Notifications
        //TODO: Refactor a little to build the message as a virtual method which can be overriden at the child class level
        protected virtual void SendEmail(PaymentAmount amount, PaymentResponseBase response ,bool success)
        {
            string Subject;
            string Body;
            string LineItem = null;
            var Type = PaymentChannel == PaymentChannel.PublicSite ? "Applicant" : "Renter";

            if (PaymentChannel == PaymentChannel.AutoPayment || PaymentChannel == PaymentChannel.Text)
            {
                if (PaymentChannel == PaymentChannel.AutoPayment)
                    LineItem = success ? "Automatic Payment Successful" : "Automatic Payment Failed to Process";

                if (PaymentChannel == PaymentChannel.Text)
                    LineItem = success ? "Sms Payment Successful" : "Sms Payment Failed to Process";

                Subject = success ? "Payment Confirmation" : "Payment Failed";

                //Salcedo - 3/12/2014 - replaced with new html email template
                String RecipientName = Resident.DisplayName;
                //cakel: BUGID 000180
                String RenterProperty = Property.PropertyName;
                String PaymentAmount = LineItem;
                String TransactionDate = DateTime.UtcNow.ToShortDateString();
                String InternalTransactionId = response.TransactionId;
                String ConvenienceAmount = response.ConvenienceFee.ToString("C");
                String TotalAmount = (amount.TotalAmount + response.ConvenienceFee).ToString("C");
                String SuccessBody = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-payment-success.html").Replace("[[ROOT]]",
                    EmailTemplateService.EmailTemplateService.RootPath), RecipientName, PaymentAmount, TransactionDate, InternalTransactionId, ConvenienceAmount, TotalAmount);
                String FailedBody = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-payment-failure.html").Replace("[[ROOT]]",
                    EmailTemplateService.EmailTemplateService.RootPath), RecipientName, RenterProperty, PaymentAmount, TransactionDate, InternalTransactionId, ConvenienceAmount, TotalAmount);

                Body = success ? SuccessBody : FailedBody;
            }
                //TODO refine this further based on Channel
            else
            {
                LineItem = success ? "Payment Successful" : "Payment Failed to Process";
                Subject = success ? "Your Payment Confirmation" : "Your Payment Failure";

                //Salcedo - 3/12/2014 - replaced with new html email template
                //Body = string.Format(EmailTemplates.OneTimeRentPayment, Resident.DisplayName, LineItem, DateTime.UtcNow.ToShortDateString(),
                //                     response.TransactionId, response.ConvenienceFee.ToString("C"), (amount.TotalAmount + response.ConvenienceFee).ToString("C"));
                String RecipientName = Resident.DisplayName;
                String RenterProperty = Property.PropertyName;
                String PaymentAmount = LineItem;
                String TransactionDate = DateTime.UtcNow.ToShortDateString();
                String InternalTransactionId = response.TransactionId;
                String ConvenienceAmount = response.ConvenienceFee.ToString("C");
                String TotalAmount = (amount.TotalAmount + response.ConvenienceFee).ToString("C");
                Body = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-payment-success.html").Replace("[[ROOT]]",
                    EmailTemplateService.EmailTemplateService.RootPath), RecipientName, RenterProperty, PaymentAmount, TransactionDate, InternalTransactionId, ConvenienceAmount, TotalAmount);

            }

            //Salcedo - 2/23/2014 - changed call to include staff IsMainContacts
            //BulkMail.BulkMail.SendMailMessage(
            //    PropertyId,
            //    Type,
            //    false,
            //    Settings.SendMailUsername,
            //    Settings.SendMailPassword,
            //    new MailAddress(Settings.PaymentEmail),
            //    Subject,
            //    Body,
            //    new List<MailAddress> { new MailAddress(Resident.PrimaryEmailAddress) }
            //    );
            BulkMail.BulkMail.SendMailMessageIncludePropertyEmailContacts(
                PropertyId,
                Type,
                EfxSettings.SendMailUsername,
                EfxSettings.SendMailPassword,
                new MailAddress(EfxSettings.PaymentEmail),
                Subject,
                Body,
                new List<MailAddress> { new MailAddress(Resident.PrimaryEmailAddress) }
                );
        }

        protected virtual void SendTextMessage()
        {
            //TODO: Modify based on the PaymentChannel
        }
        #endregion

        #region Transaction Methods
        protected Transaction BuildTransaction(PaymentResponseBase response, decimal total, PaymentType paymentType, PaymentMethodsBase paymentMethodDetails)
        {
            CreditCardDetails CcDetails = null;
            EcheckDetails EcheckDetails = null;
            int SuccesStatusId;

            if (paymentType == PaymentType.CreditCard)
            {
                CcDetails = paymentMethodDetails as CreditCardDetails;
                SuccesStatusId = (int) PaymentStatus.Approved;
            }
            else
            {
                EcheckDetails = paymentMethodDetails as EcheckDetails;
                SuccesStatusId = (int)PaymentStatus.Processing;
            }

            var Transaction = new Transaction
            {
                ExternalTransactionId = response.TransactionId,
                PaymentTypeId = (int) paymentType,
                FirstName = paymentMethodDetails.BillingFirstName,
                LastName = paymentMethodDetails.BillingLastName,
                EmailAddress = paymentMethodDetails.BillingAddress.PrimaryEmailAddress,
                StreetAddress = paymentMethodDetails.BillingAddress.AddressLine1,
                StreetAddress2 = paymentMethodDetails.BillingAddress.AddressLine2,
                City = paymentMethodDetails.BillingAddress.City,
                StateProvinceId = paymentMethodDetails.BillingAddress.StateProvinceId,
                PostalCode = paymentMethodDetails.BillingAddress.PostalCode,
                PhoneNumber = !String.IsNullOrEmpty(paymentMethodDetails.BillingAddress.PrimaryPhoneNumber) ? paymentMethodDetails.BillingAddress.PrimaryPhoneNumber : String.Empty,
                PaymentAmount = total,
                CreditCardHolderName = CcDetails != null ? CcDetails.CardHolderName : null,
                CreditCardAccountNumber = CcDetails != null ? CcDetails.CreditCardNumber : null,
                CreditCardExpirationMonth = CcDetails != null ? CcDetails.ExpirationDate.Month : (int?)null,
                CreditCardExpirationYear = CcDetails != null ? CcDetails.ExpirationDate.Year : (int?)null,
                BankAccountNumber = EcheckDetails != null ? EcheckDetails.BankAccountNumber : null,
                BankRoutingNumber = EcheckDetails != null ? EcheckDetails.BankAccountNumber : null,
                BankAccountTypeId = EcheckDetails != null ? (int)EcheckDetails.TypeOfAccount : (int?)null,
                PaymentId = null,
                PaymentStatusId = response.Result == 0 ? SuccesStatusId : (int)PaymentStatus.Declined,
                ResponseResult = response.Result,
                ResponseMessage = response.Message,
                TransactionDate = response.TransactionDate,
                PropertyId = PropertyId
            };

            try
            {
                return new Transaction(Transaction.Set(Transaction));
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Conversion Methods
        protected virtual PaymentResponseBase ConvertPaymentResponse(PaymentResponseBase baseResponse, CreditCardPaymentResponse response, int? result = null)
        {
            baseResponse.Result = result.HasValue ? result.Value : (int)response.Result;
            baseResponse.Message = response.ResponseMessage;
            baseResponse.TransactionId = response.TransactionId;

            return baseResponse;
        }

        protected virtual PaymentResponseBase ConvertPaymentResponse(PaymentResponseBase baseResponse, AchPaymentResponse response, int? result = null)
        {
            baseResponse.Result = result.HasValue ? result.Value : (int)response.Result;
            baseResponse.Message = response.ResponseDescription;
            baseResponse.TransactionId = response.TransactionId;

            return baseResponse;
        }
        #endregion

        #region Ivr Helper Methods
        // If convenience fees are charged on the Applicant portal in the future, need to add the two sites here)
        protected void SetIvr()
        {
            //Salcedo - 3/25/2014 - we are no longer using the IVR
            //if (PaymentChannel == PaymentChannel.ResidentApp || PaymentChannel == PaymentChannel.ResidentSite)
            //    UseIvr = Property.ProgramId.HasValue && Property.ProgramId.Value != Property.Program.PropertyPaidProgram;
            //else
            //    UseIvr = false;
            UseIvr = false;
        }

        protected virtual IvrPaymentResponse ProcessPaymentQueue(CreditCardDetails ccDetails, PaymentAmount amount)
        {
            var PaymentQueue = new PaymentQueue
                {
                    CreditCardAccountNumber = ccDetails.CreditCardNumber,
                    CreditCardHolderName = ccDetails.CardHolderName,
                    CreditCardExpirationMonth = ccDetails.ExpirationDate.Month,
                    CreditCardExpirationYear = ccDetails.ExpirationDate.Year,
                    CreditCardSecurityCode = ccDetails.CvvCode,
                    RenterId = ResidentId,
                    TransactionDateTime = DateTime.UtcNow,
                    PaymentTypeId = (int) PaymentType.CreditCard,
                    RentAmount = amount.BaseAmount,
                    RentAmountDescription = "Rent and Fees",
                };

            if (amount.FeeAmount > 0.00M && amount.FeeDescription.StartsWith("convenience", StringComparison.OrdinalIgnoreCase))
            {
                PaymentQueue.OtherAmount2 = amount.FeeAmount;
                PaymentQueue.OtherDescription2 = amount.FeeDescription;
            }

            try
            {
                PaymentQueue = new PaymentQueue(PaymentQueue.Set(PaymentQueue));
            }
            catch
            {
                return new IvrPaymentResponse {Result = (int) IvrResponseResult.GeneralFailure, Message = ServiceResponseMessages.GeneralFailure, TransactionDate = DateTime.UtcNow};
            }
            
            return new IvrPaymentResponse {Result = (int) IvrResponseResult.Success, Message = ServiceResponseMessages.IvrSuccess, PaymentQueueNumber = PaymentQueue.PaymentQueueNumber, TransactionDate = DateTime.UtcNow};
        }
        #endregion

        #region Credit Card Helper Methods
        protected virtual List<CustomField> BuildCustomFields(PaymentAmount amount, bool isApplicantPayment)
        {
            return isApplicantPayment ? BuildApplicantCustomFields(amount) : BuildRentCustomFields(amount);
        }

        protected virtual List<CustomField> BuildRentCustomFields(PaymentAmount amount)
        {
            return new List<CustomField>
                {
                    new CustomField("customfield1", PropertyId.ToString(CultureInfo.InvariantCulture)),
                    new CustomField("customfield2", Lease.UnitNumber),
                    new CustomField("customfield3", String.Format("{0}{1}", DateTime.UtcNow.Month.ToString(CultureInfo.InvariantCulture), DateTime.UtcNow.Year.ToString(CultureInfo.InvariantCulture))),
                    new CustomField("customfield5", (Convert.ToInt32(amount.FeeAmount * 100)).ToString(CultureInfo.InvariantCulture)),
                    new CustomField("customfield6", (Convert.ToInt32(amount.BaseAmount * 100)).ToString(CultureInfo.InvariantCulture)),
                    new CustomField("customfield10", Property.PropertyCode)
                };
        }

        protected virtual List<CustomField> BuildApplicantCustomFields(PaymentAmount amount)
        {
            return new List<CustomField>
                {
                    new CustomField("customfield1", PropertyId.ToString(CultureInfo.InvariantCulture)),
                    new CustomField("customfield3", String.Format("{0}{1}", DateTime.UtcNow.Month.ToString(CultureInfo.InvariantCulture), DateTime.UtcNow.Year.ToString(CultureInfo.InvariantCulture))),
                    new CustomField("customfield9", (Convert.ToInt32(amount.TotalAmount * 100)).ToString(CultureInfo.InvariantCulture)),
                    new CustomField("customfield10", Property.PropertyCode)
                };
        }

        protected virtual void ProcessVoid(string transactionId)
        {
            CreditCardPayment.VoidCreditCardPayment(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, transactionId);
        }
        #endregion

        #region ACH Helper Methods
        protected string GetAchClientId(bool isApplicantPayment)
        {
            return isApplicantPayment ? Property.SecurityAccountAchClientId : Property.RentalAccountAchClientId;
        }

        #endregion

        #region Validation
        protected virtual bool Validate(out PaymentResponseBase response)
        {
            response = new PaymentResponseBase { TransactionDate = DateTime.UtcNow };

            if (ApplicantId > 0)
            {
                if (Applicant.ApplicantId < 1)
                {
                    response.Result = (int) GeneralResponseResult.ApplicantNotFound;
                    response.Message = ServiceResponseMessages.ApplicantNotFound;
                    return false;
                }
            }

            if (ApplicantApplicationId > 0)
            {
                if (ApplicantApplication.ApplicantApplicationId < 1)
                {
                    response.Result = (int) GeneralResponseResult.ApplicationNotFound;
                    response.Message = ServiceResponseMessages.ApplicationNotFound;
                    return false;
                }
            }

            if (ResidentId > 0)
            {
                if (Resident.RenterId < 1)
                {
                    response.Result = (int) GeneralResponseResult.RenterNotFound;
                    response.Message = ServiceResponseMessages.RenterNotFound;
                    return false;
                }

                LeaseId = Lease.GetRenterLeaseByRenterId(ResidentId).LeaseId;
            }

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                response.Result = (int) GeneralResponseResult.PropertyNotFound;
                response.Message = ServiceResponseMessages.PropertyNotFound;
                return false;
            }

            if (LeaseId > 0)
            {
                if (Lease.LeaseId < 1)
                {
                    response.Result = (int) GeneralResponseResult.LeaseNotFound;
                    response.Message = ServiceResponseMessages.LeaseNotFound;
                    return false;
                }
            }

            if (PayerCreditCardId > 0)
            {
                if (PayerCc.PayerCreditCardId < 1)
                {
                    response.Result = (int) GeneralResponseResult.PayerCreditCardNotFound;
                    response.Message = ServiceResponseMessages.PayerCreditCardNotFound;
                    return false;
                }
            }

            if (PayerAchId > 0)
            {
                if (PayerAch.PayerAchId < 1)
                {
                    response.Result = (int) GeneralResponseResult.PayerAchNotFound;
                    response.Message = ServiceResponseMessages.PayerAchNotFound;
                    return false;
                }
            }

            return true;
        }

        protected virtual bool ValidateCreditCard(CreditCardDetails ccDetails, out PaymentResponseBase response, bool cvvRequired)
        {
            if (!Validate(out response))
                return false;

            if (String.IsNullOrEmpty(ccDetails.CardHolderName))
            {
                response.Result = (int) GeneralResponseResult.MissingCardholderName;
                response.Message = ServiceResponseMessages.MissingCardholderName;
                return false;
            }

            if (String.IsNullOrEmpty(ccDetails.CreditCardNumber))
            {
                response.Result = (int) GeneralResponseResult.MissingCardNumber;
                response.Message = ServiceResponseMessages.MissingCardNumber;
                return false;
            }

            if (ccDetails.ExpirationDate.AddMonths(1) <= DateTime.UtcNow)
            {
                response.Result = (int) GeneralResponseResult.ExpiredCard;
                response.Message = ServiceResponseMessages.Expiredcard;
                return false;
            }

            if (cvvRequired && String.IsNullOrEmpty(ccDetails.CvvCode))
            {
                response.Result = (int) GeneralResponseResult.MissingCvv;
                response.Message = ServiceResponseMessages.MissingCvv;
                return false;
            }

            return true;
        }

        protected virtual bool ValidateEcheck(EcheckDetails eCheckDetails, out PaymentResponseBase response)
        {
            if (!Validate(out response))
                return false;

            if (String.IsNullOrEmpty(eCheckDetails.BankAccountNumber))
            {
                response.Result = (int) GeneralResponseResult.MissingAccountNumber;
                response.Message = ServiceResponseMessages.Missingaccountnumber;
                return false;
            }

            if (String.IsNullOrEmpty(eCheckDetails.BankRoutingNumber))
            {
                response.Result = (int) GeneralResponseResult.MissingRoutingNumber;
                response.Message = ServiceResponseMessages.MissingRoutingNumber;
                return false;
            }

            if (!eCheckDetails.BankRoutingNumber.IsValidBankRoutingNumber())
            {
                response.Result = (int) GeneralResponseResult.InvalidRoutingNumber;
                response.Message = ServiceResponseMessages.Invalidroutingnumber;
                return false;
            }

            if (String.IsNullOrEmpty(Property.FeeAccountAchClientId))
            {
                response.Result = (int) GeneralResponseResult.MissingAchClientId;
                response.Message = ServiceResponseMessages.Missingachclientid;
                return false;
            }

            return true;
        }
        #endregion
    }
}
