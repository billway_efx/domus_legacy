﻿namespace EfxFramework.PaymentMethods
{
    public class CustomField
    {
        public string CustomFieldName { get; set; }
        public string CustomFieldValue { get; set; }

        public CustomField()
        {
        }

        public CustomField(string name, string value)
        {
            CustomFieldName = name;
            CustomFieldValue = value;
        }
    }
}
