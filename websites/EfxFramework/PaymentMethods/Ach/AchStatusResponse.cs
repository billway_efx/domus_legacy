﻿using Mb2x.ExtensionMethods;
using System;
using System.Xml.Linq;

namespace EfxFramework.PaymentMethods.Ach
{
    public class AchStatusResponse
    {
        public int? StatusCode { get; private set; }
        public string StatusDescription { get; private set; }
        public DateTime? StatusDate { get; private set; }
        public GeneralResponseResult Result { get; set; }

        public AchStatusResponse(GeneralResponseResult result, string message)
        {
            Result = result;
            StatusDescription = message;
        }

        public AchStatusResponse(string response)
        {
            var Doc = XDocument.Parse(response);

            var RpoGetTransactionStatus = Doc.Element("RPOGetTransactionStatus");
            if (RpoGetTransactionStatus == null)
                throw new InvalidOperationException("RPOGetTransactionStatus is null");

            var StatusDetail = RpoGetTransactionStatus.Element("StatusDetail");
            if (StatusDetail == null)
                throw new InvalidOperationException("StatusDetail is null");

            //All of the following NullReferenceExceptions have been checked
            // ReSharper disable PossibleNullReferenceException
            StatusCode = StatusDetail.Element("StatusCode") == null ? null : StatusDetail.Element("StatusCode").Value.ToNullInt32();
            StatusDescription = StatusDetail.Element("StatusDescription") == null ? null : StatusDetail.Element("StatusDescription").Value;
            StatusDate = StatusDetail.Element("StatusDate") == null ? null : StatusDetail.Element("StatusDate").Value.ToNullDateTime();
            // ReSharper restore PossibleNullReferenceException

            if (StatusCode.HasValue && StatusCode.Value >= 0)
                Result = GeneralResponseResult.Success;
            else if (StatusCode.HasValue)
                Result = AchResponseMap.StatusResponseMap.ContainsKey(StatusCode.Value) ? AchResponseMap.StatusResponseMap[StatusCode.Value] : AchResponseMap.StatusResponseMap[-9999];
            else
                Result = GeneralResponseResult.GeneralFailure;
        }
    }
}
