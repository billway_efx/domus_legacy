﻿using System.Collections.Generic;

namespace EfxFramework.PaymentMethods.Ach
{
    public enum AchTransactionType
    {
        Account,
        Status,
        Payment
    }

    public static class AchResponseMap
    {
        private static Dictionary<int, GeneralResponseResult> _StatusResponseMap;
        private static Dictionary<int, GeneralResponseResult> _AccountResponseMap;
        private static Dictionary<int, GeneralResponseResult> _PaymentResponseMap;

        public static Dictionary<int, GeneralResponseResult> StatusResponseMap
        {
            get { return _StatusResponseMap ?? (_StatusResponseMap = BuildResponseMap(AchTransactionType.Status)); }
        }

        public static Dictionary<int, GeneralResponseResult> AccountResponseMap
        {
            get { return _AccountResponseMap ?? (_AccountResponseMap = BuildResponseMap(AchTransactionType.Account)); }
        }

        public static Dictionary<int, GeneralResponseResult> PaymentResponseMap
        {
            get { return _PaymentResponseMap ?? (_PaymentResponseMap = BuildResponseMap(AchTransactionType.Payment)); }
        }

        private static Dictionary<int, GeneralResponseResult> BuildResponseMap(AchTransactionType transactionType)
        {
            var Result = new Dictionary<int, GeneralResponseResult>
                {
                    { -9979, GeneralResponseResult.ClientUpdateError }, 
                    { -9980, GeneralResponseResult.ClientContactUpdateError }, 
                    { -9981, GeneralResponseResult.InvalidAchClientId }, 
                    { -9982, GeneralResponseResult.InvalidAchClientId }, 
                    { -9983, GeneralResponseResult.ClientContactCreationError }, 
                    { -9984, GeneralResponseResult.ClientCreationError }, 
                    { -9985, GeneralResponseResult.FailedLogin }, 
                    { -9986, GeneralResponseResult.FailedLogin }, 
                    { -9987, GeneralResponseResult.InvalidRpoPropertyAccountId }, 
                    { -9988, GeneralResponseResult.InvalidBankName }, 
                    { -9989, GeneralResponseResult.InvalidAccountNumber }, 
                    { -9990, GeneralResponseResult.InvalidRoutingNumber }, 
                    { -9991, GeneralResponseResult.InvalidRoutingNumber }, 
                    { -9992, GeneralResponseResult.InvalidClearingDays }, 
                    { -9993, GeneralResponseResult.InvalidClearingDays }, 
                    { -9994, GeneralResponseResult.InvalidPhoneNumber }, 
                    { -9995, GeneralResponseResult.InvalidPropertyName }, 
                    { -9996, GeneralResponseResult.InvalidRpoPropertyAccountId }, 
                    { -9998, GeneralResponseResult.DatabaseError }, 
                    { -9999, GeneralResponseResult.GeneralFailure }
                };

            switch (transactionType)
            {
                case AchTransactionType.Payment:
                    Result.Add(-9997, GeneralResponseResult.InvalidAchClientId);
                    break;
                case AchTransactionType.Status:
                    Result.Add(-9997, GeneralResponseResult.TransactionNotFound);
                    break;
            }

            return Result;
        }
    }
}
