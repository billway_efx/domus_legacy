﻿using Mb2x.ExtensionMethods;
using System;
using System.Xml.Linq;

namespace EfxFramework.PaymentMethods.Ach
{
	public class AchPaymentResponse
	{
		//Public Properties
		public int? ResponseCode { get; private set; }
		public string ResponseDescription { get; set; }
		public decimal? Amount { get; private set; }
		public string AbaNumber { get; private set; }
		public string AccountNumber { get; private set; }
		public string AccountType { get; private set; }
		public string FirstName { get; private set; }
		public string LastName { get; private set; }
		public string Address1 { get; private set; }
		public string Address2 { get; private set; }
		public string City { get; private set; }
		public string State { get; private set; }
		public string PostalCode { get; private set; }
		public string PhoneNumber { get; private set; }
        public string TransactionId { get; private set; }
        public GeneralResponseResult Result { get; set; }

		//Constructor
        public AchPaymentResponse()
        {
            
        }

        public AchPaymentResponse(GeneralResponseResult result, string message, string transactionId)
        {
            Result = result;
            ResponseDescription = message;
            TransactionId = transactionId;
        }

		public AchPaymentResponse(string response)
		{
			//Parse the response
			var Doc = XDocument.Parse(response);

			//Get the EFXTransaction node
            var EfxTransaction = Doc.Element("RPOCreateACHTransaction");
			if (EfxTransaction == null)
                throw new InvalidOperationException("RPOCreateACHTransaction is null");
			
			//Get the ResponseDetail node
			var ResponseDetail = EfxTransaction.Element("ResponseDetail");
			if (ResponseDetail == null)
				throw new InvalidOperationException("ResponseDetail is null");

			//Set the properties that belong to the ResponseDetail section
            //All of the following PossibleNullReferenceExceptions have been checked
// ReSharper disable PossibleNullReferenceException
			ResponseCode = ResponseDetail.Element("ResponseCode") == null ? null : ResponseDetail.Element("ResponseCode").Value.ToNullInt32();
			ResponseDescription = ResponseDetail.Element("ResponseDescription") == null ? null : ResponseDetail.Element("ResponseDescription").Value;
            TransactionId = ResponseDetail.Element("TransactionID") == null ? null : ResponseDetail.Element("TransactionID").Value;

            if (ResponseCode.HasValue && ResponseCode.Value == 0)
            {
                //cakel: BUGID000109 - Updated(Mark) text for list of payments
                ResponseDescription = "Successfully Submitted";
                Result = GeneralResponseResult.Success;
            }
            else if (ResponseCode.HasValue)
                Result = AchResponseMap.PaymentResponseMap[ResponseCode.Value];
            else
                Result = GeneralResponseResult.GeneralFailure;

			//Get the TransactionDetail node
			var TransactionDetail = EfxTransaction.Element("TransactionDetail");
			if (TransactionDetail == null)
				throw new InvalidOperationException("TransactionDetail is null");

			//Set the properties that belong to the TransactionDetail section
			Amount = TransactionDetail.Element("Amount") == null ? null : TransactionDetail.Element("Amount").Value.ToNullDecimal();
			AbaNumber = TransactionDetail.Element("ABANumber") == null ? null : TransactionDetail.Element("ABANumber").Value;
			AccountNumber = TransactionDetail.Element("AccountNumber") == null ? null : TransactionDetail.Element("AccountNumber").Value;
			AccountType = TransactionDetail.Element("AccountType") == null ? null : TransactionDetail.Element("AccountType").Value;
			FirstName = TransactionDetail.Element("FirstName") == null ? null : TransactionDetail.Element("FirstName").Value;
			LastName = TransactionDetail.Element("LastName") == null ? null : TransactionDetail.Element("LastName").Value;
			Address1 = TransactionDetail.Element("Address1") == null ? null : TransactionDetail.Element("Address1").Value;
			Address2 = TransactionDetail.Element("Address2") == null ? null : TransactionDetail.Element("Address2").Value;
			City = TransactionDetail.Element("City") == null ? null : TransactionDetail.Element("City").Value;
			State = TransactionDetail.Element("State") == null ? null : TransactionDetail.Element("State").Value;
			PostalCode = TransactionDetail.Element("Zip") == null ? null : TransactionDetail.Element("Zip").Value;
			PhoneNumber = TransactionDetail.Element("Phone") == null ? null : TransactionDetail.Element("Phone").Value;
// ReSharper restore PossibleNullReferenceException
		}
	}
}