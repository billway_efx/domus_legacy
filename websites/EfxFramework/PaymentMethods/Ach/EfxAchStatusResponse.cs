﻿using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace EfxFramework.PaymentMethods.Ach
{
    public class EfxAchStatusResponse
    {
        public int? StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public DateTime? StatusDate { get; set; }
        public GeneralResponseResult Result { get; set; }
        public string TransactionId { get; set; }
        public string TransactionType { get; set; }
        public decimal? TransactionAmount { get; set; }
        public DateTime? LastStatusChangeTime { get; set; }
        public int? ReturnCode { get; set; }
        public string ReasonDescription { get; set; }

        public EfxAchStatusResponse(XContainer detail)
        {
            if (detail == null)
                throw new InvalidOperationException("StatusDetail is null");

            // ReSharper disable PossibleNullReferenceException
            StatusCode = detail.Element("StatusCode") == null ? null : detail.Element("StatusCode").Value.ToNullInt32();
            StatusDescription = detail.Element("StatusDescription") == null ? null : detail.Element("StatusDescription").Value;
            StatusDate = detail.Element("StatusDate") == null ? null : detail.Element("StatusDate").Value.ToNullDateTime();
            TransactionId = detail.Element("TransactionID") == null ? null : detail.Element("TransactionID").Value;
            TransactionType = detail.Element("TransactionType") == null ? null : detail.Element("TransactionType").Value;
            TransactionAmount = detail.Element("TransactionAmount") == null ? null : detail.Element("TransactionAmount").Value.ToNullDecimal();
            LastStatusChangeTime = detail.Element("LastStatusChangeTime") == null ? null : detail.Element("LastStatusChangeTime").Value.ToNullDateTime();
            ReturnCode = detail.Element("ReturnCode") == null ? null : detail.Element("ReturnCode").Value.ToNullInt32();
            ReasonDescription = detail.Element("ReasonDescription") == null ? null : detail.Element("ReasonDescription").Value;
            // ReSharper restore PossibleNullReferenceException

            if (StatusCode.HasValue)
                Result = GeneralResponseResult.Success;
            else
                Result = GeneralResponseResult.GeneralFailure;
        }
    }

    public class EfxAchStatusCollection
    {
        public List<EfxAchStatusResponse> Statuses { get; private set; }
        public GeneralResponseResult Result { get; set; }
        public int? StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public string ClientId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? CollectionCount { get; set; }

        public EfxAchStatusCollection()
        {
            Statuses = new List<EfxAchStatusResponse>();
        }

        public EfxAchStatusCollection ParseStatusesByDateRange(string response)
        {
            var StatusCollection = new EfxAchStatusCollection();
            var Doc = XDocument.Parse(response);
            var TransactionStatuses = Doc.Element("EFXTransactionStatusesForDateRange");

            if (TransactionStatuses == null)
                throw new InvalidOperationException("EFXTransactionStatusesForDateRange is null");

            var StatusDetail = TransactionStatuses.Element("StatusDetail");

            if (StatusDetail == null)
                throw new InvalidOperationException("Service StatusDetail is null");

            // ReSharper disable PossibleNullReferenceException
            StatusCollection.StatusCode = StatusDetail.Element("StatusCode") == null ? null : StatusDetail.Element("StatusCode").Value.ToNullInt32();
            StatusCollection.StatusDescription = StatusDetail.Element("StatusDescription") == null ? null : StatusDetail.Element("StatusDescription").Value;
            // ReSharper restore PossibleNullReferenceException

            var TransactionCollectionCriteria = TransactionStatuses.Element("TransactionCollectionCriteria");

            if (TransactionCollectionCriteria == null)
                throw new InvalidOperationException("TransactionCollectionCriteria is null");

            // ReSharper disable PossibleNullReferenceException
            StatusCollection.ClientId = TransactionCollectionCriteria.Element("ClientID") == null ? null : TransactionCollectionCriteria.Element("ClientID").Value;
            StatusCollection.StartDate = TransactionCollectionCriteria.Element("StartDate") == null ? null : TransactionCollectionCriteria.Element("StartDate").Value.ToNullDateTime();
            StatusCollection.EndDate = TransactionCollectionCriteria.Element("EndDate") == null ? null : TransactionCollectionCriteria.Element("EndDate").Value.ToNullDateTime();
            StatusCollection.CollectionCount = TransactionCollectionCriteria.Element("CollectionCount") == null ? null : TransactionCollectionCriteria.Element("CollectionCount").Value.ToNullInt32();
            // ReSharper restore PossibleNullReferenceException

            var TransactionStatusCollection = TransactionStatuses.Element("TransactionStatusCollection");

            if (TransactionStatusCollection == null)
                return new EfxAchStatusCollection();

            foreach (var Element in TransactionStatusCollection.Elements("StatusDetail"))
            {
                StatusCollection.Statuses.Add(new EfxAchStatusResponse(Element));
            }

            if (StatusCode.HasValue && StatusCode.Value == 0)
                Result = GeneralResponseResult.Success;
            else
                Result = GeneralResponseResult.GeneralFailure;

            return StatusCollection;
        }
    }
}
