﻿using System.Data.SqlTypes;
using EfxFramework.EfxAch;
using System;
using System.Globalization;
using System.Text.RegularExpressions;

//Salcedo - 3/31/2015 - brought external ACH services into the RPO code, to improve performance and enable easier testing/debugging
using ACH_Transactions;

namespace EfxFramework.PaymentMethods.Ach
{
	public class AchService
	{

        //Salcedo - 4/20/2015 - task 00172 - removed merchantCount
        public static AchAccountResponse CreateAchAccount(string friendlyId, string propertyName, string propertyPhone, string propertyBankName, int? clearingDays, string routingNumber, string accountNumber)
        {
            //Salcedo - 3/31/2015 - brought external ACH services into the RPO code, to improve performance and enable easier testing/debugging
            //using (var Client = new ACHTransactionClient())
            //{

            if (!ValidateRoutingCheckDigit(routingNumber))
                return new AchAccountResponse(GeneralResponseResult.InvalidRoutingNumber, ServiceResponseMessages.Invalidroutingnumber);
                
            if (!clearingDays.HasValue)
                clearingDays = 1;

            try
            {
                //Salcedo - 3/31/2015 - brought external ACH services into the RPO code, to improve performance and enable easier testing/debugging
                //var Result = Client.RPO_NewACHAccountForProperty(Settings.AchUserId, Settings.AchPassword, friendlyId, propertyName, !String.IsNullOrEmpty(propertyPhone) ? propertyPhone : "1234567890", string.Empty,
                //    clearingDays.Value.ToString(CultureInfo.InvariantCulture), routingNumber, accountNumber);
                ACHTransactions ach = new ACHTransactions();
                //Salcedo - 4/20/2015 - task 00172 - removed reference to merchantCount
                var Result = ach.RPO_NewACHAccountForProperty(EfxSettings.AchUserId, EfxSettings.AchPassword, friendlyId, propertyName, !String.IsNullOrEmpty(propertyPhone) ? propertyPhone : "1234567890", string.Empty,
                    clearingDays.Value.ToString(CultureInfo.InvariantCulture), routingNumber, accountNumber);

                return new AchAccountResponse(Result, true);
            }


            catch
            {
                return new AchAccountResponse(GeneralResponseResult.GeneralFailure, "Failed to respond");
            }


        }

        // Patrick Whittingham - 4/15/15 - task #000172
        // Salcedo - task 00172 - removed merchantCount
        //CMallory - Task 00603 - Added CCDepositResponse parameter to the function.
        public static AchAccountResponse UpdateAchAccount(string achClientId, string propertyName, string propertyPhone, string propertyBankName, int? clearingDays, string routingNumber, string accountNumber, string usermerchantid, bool CCDepositResponse)
        {
            //Salcedo - 3/31/2015 - brought external ACH services into the RPO code, to improve performance and enable easier testing/debugging
            //using (var Client = new ACHTransactionClient())
            //{

            if (!ValidateRoutingCheckDigit(routingNumber))
                return new AchAccountResponse(GeneralResponseResult.InvalidRoutingNumber, ServiceResponseMessages.Invalidroutingnumber);
            if (String.IsNullOrEmpty(achClientId))
                return new AchAccountResponse(GeneralResponseResult.MissingAchClientId, ServiceResponseMessages.Missingachclientid);

            if (!clearingDays.HasValue)
                clearingDays = 0;

            //Salcedo - 3/31/2015 - brought external ACH services into the RPO code, to improve performance and enable easier testing/debugging
            //var Result = Client.RPO_UpdateACHAccountForProperty(Settings.AchUserId, Settings.AchPassword, achClientId, propertyName, !String.IsNullOrEmpty(propertyPhone) ? propertyPhone : "1234567890", string.Empty, clearingDays.Value.ToString(CultureInfo.InvariantCulture), routingNumber,
            //        accountNumber);
            ACHTransactions ach = new ACHTransactions();
            //CMallory - Task 00603 - Added CCDepositResponse variable to the function being called.
            var Result = ach.RPO_UpdateACHAccountForProperty(EfxSettings.AchUserId, EfxSettings.AchPassword, achClientId, propertyName, !String.IsNullOrEmpty(propertyPhone) ? propertyPhone : "1234567890", string.Empty, clearingDays.Value.ToString(CultureInfo.InvariantCulture), routingNumber,
                    accountNumber, usermerchantid, CCDepositResponse);

            return new AchAccountResponse(Result);

            //}
        }

        public static AchStatusResponse GetAchStatus(string achClientId, string transactionId)
        {
            //Salcedo - 3/31/2015 - brought external ACH services into the RPO code, to improve performance and enable easier testing/debugging
            //using (var Client = new ACHTransactionClient())
            //{

            if (String.IsNullOrEmpty(achClientId))
                return new AchStatusResponse(GeneralResponseResult.MissingAchClientId, ServiceResponseMessages.Missingachclientid);
            if (String.IsNullOrEmpty(transactionId))
                return new AchStatusResponse(GeneralResponseResult.MissingTransactionId, ServiceResponseMessages.Missingtransactionid);

            //Salcedo - 3/31/2015 - brought external ACH services into the RPO code, to improve performance and enable easier testing/debugging
            //var Result = Client.RPO_GetTransactionStatus(Settings.AchUserId, Settings.AchPassword, achClientId, transactionId);
            ACHTransactions ach = new ACHTransactions();
            var Result = ach.RPO_GetTransactionStatus(EfxSettings.AchUserId, EfxSettings.AchPassword, achClientId, transactionId);

            return new AchStatusResponse(Result);

            //}
        }

        public static EfxAchStatusCollection GetAchStatusesByDate(string achClientId, DateTime startDate, DateTime endDate)
        {
            //Salcedo - 3/31/2015 - brought external ACH services into the RPO code, to improve performance and enable easier testing/debugging
            //using (var Client = new ACHTransactionClient())
            //{

            if (String.IsNullOrEmpty(achClientId))
                return new EfxAchStatusCollection {Result = GeneralResponseResult.MissingAchClientId, StatusDescription = ServiceResponseMessages.Missingachclientid};

            if (startDate < SqlDateTime.MinValue.Value)
                startDate = SqlDateTime.MinValue.Value;
            if (endDate > SqlDateTime.MaxValue.Value)
                endDate = SqlDateTime.MaxValue.Value;

            var StatusCollection = new EfxAchStatusCollection();
            //Salcedo - 3/31/2015 - brought external ACH services into the RPO code, to improve performance and enable easier testing/debugging
            //return StatusCollection.ParseStatusesByDateRange(Client.GetTransactionStatusesForDateRange(Settings.AchUserId, Settings.AchPassword, achClientId, 
            //    startDate.ToString(CultureInfo.InvariantCulture), endDate.ToString(CultureInfo.InvariantCulture)));
            ACHTransactions ach = new ACHTransactions();
            return StatusCollection.ParseStatusesByDateRange(ach.GetTransactionStatusesForDateRange(EfxSettings.AchUserId, EfxSettings.AchPassword, achClientId,
                startDate.ToString(CultureInfo.InvariantCulture), endDate.ToString(CultureInfo.InvariantCulture)));

            //}
        }

		public static AchPaymentResponse ProcessAchPayment(string rpoUserid, string rpoPassword, string achClientId, decimal amount,  string bankRoutingNumber, string bankAccountNumber, 
            string bankAccountType, string firstName, string lastName, string streetAddress, string streetAddress2, string city, string state, string postalCode, string phoneNumber)
		{
            streetAddress = String.IsNullOrEmpty(streetAddress) ? null : streetAddress;
		    streetAddress2 = String.IsNullOrEmpty(streetAddress2) ? null : String.Format("Unit {0}", streetAddress2);

            //Salcedo - 3/31/2015 - brought external ACH services into the RPO code, to improve performance and enable easier testing/debugging
            //using (var Client = new ACHTransactionClient())
            //{

            //cakel: BUGID0389 - Added regex to check for numeric only
            Regex AcctNumChex = new Regex("^[0-9]*$");

            if (!ValidateRoutingCheckDigit(bankRoutingNumber))
                return new AchPaymentResponse(GeneralResponseResult.InvalidRoutingNumber, ServiceResponseMessages.Invalidroutingnumber, null);
            if (String.IsNullOrEmpty(achClientId))
                return new AchPaymentResponse(GeneralResponseResult.MissingAchClientId, ServiceResponseMessages.Missingachclientid, null);
            if (String.IsNullOrEmpty(bankAccountNumber))
                return new AchPaymentResponse(GeneralResponseResult.MissingAccountNumber, ServiceResponseMessages.Missingaccountnumber, null);

            bool TestRegex = AcctNumChex.IsMatch(bankAccountNumber);
            bool TestAcct = bankAccountNumber.Contains(" ");

            string testAcctNum = bankAccountNumber.Replace(" ", "");

            //cakel: BUGID: 00389 Added Code to check for symbols and spaces in Account number
            if (!AcctNumChex.IsMatch(bankAccountNumber))
            {
                return new AchPaymentResponse(GeneralResponseResult.InvalidAccountNumber, ServiceResponseMessages.InvalidAccountNumber, null);
            }


            //Salcedo - 3/31/2015 - brought external ACH services into the RPO code, to improve performance and enable easier testing/debugging
            //return new AchPaymentResponse(Client.RPO_CreateACHTransaction(rpoUserid, rpoPassword, achClientId, amount.ToString(CultureInfo.InvariantCulture), bankRoutingNumber, bankAccountNumber, 
            //    bankAccountType.ToUpper(), firstName ?? "", lastName ?? "", streetAddress ?? "", streetAddress2 ?? "", city ?? "", state ?? "", postalCode ?? "", phoneNumber ?? ""));
            ACHTransactions ach = new ACHTransactions();
            return new AchPaymentResponse(ach.RPO_CreateACHTransaction(rpoUserid, rpoPassword, achClientId, amount.ToString(CultureInfo.InvariantCulture), bankRoutingNumber, bankAccountNumber,
                bankAccountType.ToUpper(), firstName ?? "", lastName ?? "", streetAddress ?? "", streetAddress2 ?? "", city ?? "", state ?? "", postalCode ?? "", phoneNumber ?? ""));

            //}
		}

        public static AchCancelResponse CancelPendingAchTransaction(string achClientId, string transactionId)
        {
            //Salcedo - 3/31/2015 - brought external ACH services into the RPO code, to improve performance and enable easier testing/debugging
            //using (var Client = new ACHTransactionClient())
            //{

            if (String.IsNullOrEmpty(achClientId))
                return new AchCancelResponse {Result = GeneralResponseResult.MissingAchClientId, StatusDescription = ServiceResponseMessages.Missingachclientid};
            if (String.IsNullOrEmpty(transactionId))
                return new AchCancelResponse {Result = GeneralResponseResult.MissingTransactionId, StatusDescription = ServiceResponseMessages.Missingtransactionid};

            //Salcedo - 3/31/2015 - brought external ACH services into the RPO code, to improve performance and enable easier testing/debugging
            //return new AchCancelResponse(Client.RPO_CancelPendingACHTransaction(Settings.AchUserId, Settings.AchPassword, transactionId));
            ACHTransactions ach = new ACHTransactions();
            return new AchCancelResponse(ach.RPO_CancelPendingACHTransaction(EfxSettings.AchUserId, EfxSettings.AchPassword, transactionId));

            //}
        }

	    public static bool ValidateRoutingCheckDigit(string routingNumber)
        {
            int Sum;

            if (routingNumber == null || routingNumber.Length != 9)
                return false;

            foreach (var C in routingNumber)
            {
                int Result;

                if (!Int32.TryParse(C.ToString(CultureInfo.InvariantCulture), out Result))
                    return false;
            }

            var MultipleOfTen = Sum = (Int32.Parse(routingNumber[0].ToString(CultureInfo.InvariantCulture)) * 3) +
                                      (Int32.Parse(routingNumber[3].ToString(CultureInfo.InvariantCulture)) * 3) +
                                      (Int32.Parse(routingNumber[6].ToString(CultureInfo.InvariantCulture)) * 3) +
                                      (Int32.Parse(routingNumber[1].ToString(CultureInfo.InvariantCulture)) * 7) +
                                      (Int32.Parse(routingNumber[4].ToString(CultureInfo.InvariantCulture)) * 7) +
                                      (Int32.Parse(routingNumber[7].ToString(CultureInfo.InvariantCulture)) * 7) +
                                      (Int32.Parse(routingNumber[2].ToString(CultureInfo.InvariantCulture))) +
                                      (Int32.Parse(routingNumber[5].ToString(CultureInfo.InvariantCulture)));

            while (MultipleOfTen % 10 != 0)
            {
                MultipleOfTen += 1;
            }

            return (MultipleOfTen - Sum) == Int32.Parse(routingNumber[8].ToString(CultureInfo.InvariantCulture));
        }
	}

 
}