﻿using Mb2x.ExtensionMethods;
using System;
using System.Xml.Linq;

namespace EfxFramework.PaymentMethods.Ach
{
    public class AchAccountResponse
    {
        public int? ResponseCode { get; private set; }
        public string ResponseDescription { get; private set; }
        public string AchClientId { get; private set; }
        public string RpoPropertyAccountId { get; private set; }
        public string RpoPropertyName { get; private set; }
        public string RpoPropertyPhoneNumber { get; private set; }
        public string AchProcessingBankName { get; private set; }
        public int? ClearingDays { get; private set; }
        public string RoutingNumber { get; private set; }
        public string AccountNumber { get; private set; }
        public GeneralResponseResult Result { get; set; }

        private static string GetRootElement(bool isCreateRequest)
        {
            if (isCreateRequest)
                return "RPONewACHAccountForProperty";

            return "RPOUpdateACHAccountForProperty";
        }

        public AchAccountResponse(GeneralResponseResult result, string message)
        {
            Result = result;
            ResponseDescription = message;
        }

        public AchAccountResponse(string response, bool isCreateRequest = false)
        {
            var Doc = XDocument.Parse(response);

            var RootElement = Doc.Element(GetRootElement(isCreateRequest));
            if (RootElement == null)
                throw new InvalidOperationException(String.Format("{0} is null", GetRootElement(isCreateRequest)));

            var ResponseDetail = RootElement.Element("ResponseDetail");
            if (ResponseDetail == null)
                throw new InvalidOperationException("ResponseDetail is null");

            //All of the PossibleNullReferenceExceptions in the following blocks of code are checked
// ReSharper disable PossibleNullReferenceException
            ResponseCode = ResponseDetail.Element("ResponseCode") == null ? null : ResponseDetail.Element("ResponseCode").Value.ToNullInt32();
            ResponseDescription = ResponseDetail.Element("ResponseDescription") == null ? null : ResponseDetail.Element("ResponseDescription").Value;

            if (isCreateRequest)
                AchClientId = ResponseDetail.Element("ACHClientID") == null ? null : ResponseDetail.Element("ACHClientID").Value;

            if (ResponseCode.HasValue && ResponseCode.Value == 0)
                Result = GeneralResponseResult.Success;
            else if (ResponseCode.HasValue)
                Result = AchResponseMap.AccountResponseMap[ResponseCode.Value];
            else
                Result = GeneralResponseResult.GeneralFailure;

            var AccountDetail = RootElement.Element("AccountDetail");
            if (AccountDetail == null)
                throw new InvalidOperationException("AccountDetail is null");


            if (isCreateRequest)
                RpoPropertyAccountId = AccountDetail.Element("RPOPropertyAccountID") == null ? null : AccountDetail.Element("RPOPropertyAccountID").Value;
            else
                AchClientId = AccountDetail.Element("ACHClientID") == null ? null : AccountDetail.Element("ACHClientID").Value;

            RpoPropertyName = AccountDetail.Element("RPOPropertyName") == null ? null : AccountDetail.Element("RPOPropertyName").Value;
            RpoPropertyPhoneNumber = AccountDetail.Element("RPOPropertyPhoneNumber") == null ? null : AccountDetail.Element("RPOPropertyPhoneNumber").Value;
            AchProcessingBankName = AccountDetail.Element("ACHProcessingBankName") == null ? null : AccountDetail.Element("ACHProcessingBankName").Value;
            ClearingDays = AccountDetail.Element("ClearingDays") == null ? null : AccountDetail.Element("ClearingDays").Value.ToNullInt32();
            RoutingNumber = AccountDetail.Element("RoutingNumber") == null ? null : AccountDetail.Element("RoutingNumber").Value;
            AccountNumber = AccountDetail.Element("AccountNumber") == null ? null : AccountDetail.Element("AccountNumber").Value;
// ReSharper restore PossibleNullReferenceException
        }
    }
}
