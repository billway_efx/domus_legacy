﻿using Mb2x.ExtensionMethods;
using System;
using System.Xml.Linq;

namespace EfxFramework.PaymentMethods.Ach
{
    public class AchCancelResponse
    {
        public string TransactionId { get; set; }
        public int? StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public GeneralResponseResult Result { get; set; }

        public AchCancelResponse()
        {}

        public AchCancelResponse(string response)
        {
            var Doc = XDocument.Parse(response);

            var EfxCancelTransaction = Doc.Element("EFXCancelTransaction");

            if (EfxCancelTransaction == null)
                throw new InvalidOperationException("EFXCancelTransaction is null");

            var StatusDetail = EfxCancelTransaction.Element("StatusDetail");
            
            if (StatusDetail == null)
                throw new InvalidOperationException("StatusDetail is null");

            // ReSharper disable PossibleNullReferenceException
            TransactionId = StatusDetail.Element("TransactionID") == null ? null : StatusDetail.Element("TransactionID").Value;
            StatusCode = StatusDetail.Element("StatusCode") == null ? null : StatusDetail.Element("StatusCode").Value.ToNullInt32();
            StatusDescription = StatusDetail.Element("StatusDescription") == null ? null : StatusDetail.Element("StatusDescription").Value;
            // ReSharper restore PossibleNullReferenceException

            if (StatusCode.HasValue && StatusCode.Value == 0)
                Result = GeneralResponseResult.Success;
            else
                Result = GeneralResponseResult.GeneralFailure;
        }
    }
}
