﻿using EfxFramework.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Net;
using RPO;

namespace EfxFramework.PaymentMethods.CreditCard
{
    public static class CreditCardPayment
    {
        private enum TcLinkTransactionType
        {
// ReSharper disable UnusedMember.Local     These are all required
            [Description("sale")]
            Sale,
            [Description("preauth")]
            Preauth,
            [Description("postauth")]
            Postauth,
            [Description("credit")]
            Credit,
            [Description("void")]
            Void,
            [Description("store")]
            Store
        }

        private enum TcLinkResponseType
        {
            [Description("transid")]
            TransactionId,
            [Description("status")]
            Status,
            [Description("declinetype")]
            DeclineType,
            [Description("error")]
            Error,
            [Description("offenders")]
            Offenders,
            [Description("errortype")]
            ErrorType,
            [Description("authcode")]
            AuthCode,
            [Description("reversal")]
            Reversal
// ReSharper restore UnusedMember.Local
        }

        //Salcedo - 9/11/2015 - remove ampersands from data sent to credit card processor
        private static String NoAmp(string InputString)
        {
            return InputString.Replace("&", "");
        }

        public static CreditCardPaymentResponse ProcessCreditCard(string gatewayUsername, string gatewayPassword, bool isTestUser, decimal amount, string creditCardAccountNumber, 
            string creditCardExpirationDate, string cvv, string creditCardHolderName, List<CustomField> customFields = null)
        {

            // Ticket - IH-14
            // Correct SSL / TLS error as this code was written in .net 2.0
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;



            var Client = new WebClient();
            Client.QueryString.Add("custid", gatewayUsername);
            Client.QueryString.Add("password", gatewayPassword);
            Client.QueryString.Add("action", TcLinkTransactionType.Sale.GetFriendlyName());
            Client.QueryString.Add("demo", GetDemoCode(isTestUser));
            Client.QueryString.Add("cc", creditCardAccountNumber.Replace("-", ""));
            Client.QueryString.Add("exp", creditCardExpirationDate);
            //Salcedo - 9/11/2015 - remove ampersands from data sent to credit card processor
            Client.QueryString.Add("name", NoAmp(creditCardHolderName));
            Client.QueryString.Add("amount", (Convert.ToInt32(amount * 100)).ToString(CultureInfo.InvariantCulture));

            if (!string.IsNullOrEmpty(cvv))
                Client.QueryString.Add("cvv", cvv);

            if (customFields != null)
            {
                foreach (var Cf in customFields)
                {
                    //Salcedo - 9/11/2015 - remove ampersands from data sent to credit card processor
                    Client.QueryString.Add(Cf.CustomFieldName, NoAmp(Cf.CustomFieldValue));
                }
            }

            //Salcedo - 4/30/2014 - added logic to catch unknown exception error and hard code reason to a processor down error

            string RawResponse = "";
            var Response = new CreditCardPaymentResponse();
            string ResponseMessage = "";

            //Salcedo - 7/7/2014 - add logging around CC transaction to catch more detail around errors that occur
            ActivityLog al = new ActivityLog();
            string ActivityDescription = "";
            ActivityDescription = "Requesting CC response from " + EfxSettings.CreditCardProcessorUrl + ". QueryString values: (" +
                "custid=" + Client.QueryString[0] + "; " +
                "password=" + Client.QueryString[1] + "; " +
                "action=" + Client.QueryString[2] + "; " +
                "demo=" + Client.QueryString[3] + "; " +
                "cc=" + Client.QueryString[4].MaskAccountNumber() + "; " +
                "exp=" + Client.QueryString[5] + "; " +
                "name=" + Client.QueryString[6] + "; " +
                "amount=" + Client.QueryString[7] + ";) ";
            al.WriteLog("EfxFramework", ActivityDescription, 3);

            try
            {
                RawResponse = Client.DownloadString(EfxSettings.CreditCardProcessorUrl);
                al.WriteLog("EfxFramework", "Raw CC response received: " + RawResponse, 3);
                Response = BuildResponse(RawResponse);
            }
            catch (Exception ex)
            {
                al.WriteLog("EfxFramework", "Error exception received while " + ActivityDescription + ". Exception details: " +  ex.ToString(), 1);

                Response.Result = GetErrorReason("processordown", out ResponseMessage);
                Response.ResponseMessage = string.Format("{0}-{1}", ServiceResponseMessages.Ccerror, ResponseMessage);
            }

            //return BuildResponse(Client.DownloadString(Settings.CreditCardProcessorUrl));
            return Response;
        }

        public static CreditCardPaymentResponse VoidCreditCardPayment(string gatewayUsername, string gatewayPassword, string transactionId)
        {
            var Client = new WebClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            Client.QueryString.Add("custid", gatewayUsername);
            Client.QueryString.Add("password", gatewayPassword);
            Client.QueryString.Add("action", TcLinkTransactionType.Void.GetFriendlyName());
            Client.QueryString.Add("transid", transactionId);

            return BuildResponse(Client.DownloadString(EfxSettings.CreditCardProcessorUrl), 2);
        }

        public static CreditCardPaymentResponse ReverseCreditCardPayment(string gatewayUsername, string gatewayPassword, string transactionId, decimal? amount)
        {
            var Client = new WebClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            Client.QueryString.Add("custid", gatewayUsername);
            Client.QueryString.Add("password", gatewayPassword);
            //cakel: BUGID0061 - Updated "action" to post "Credit" - commented out old code with TcLinkResponseType.Reversal.GetFriendlyName()
            Client.QueryString.Add("action", TcLinkTransactionType.Credit.GetFriendlyName());
            //Client.QueryString.Add("action", TcLinkResponseType.Reversal.GetFriendlyName());
            //cakel: BUGID0061 - Added demo = n to use live account
            Client.QueryString.Add("demo", "n");
            Client.QueryString.Add("transid", transactionId);
            

            if (amount.HasValue && amount > 0.00M)
            {
                var ReversalAmount = ((int) (amount*100)).ToString(CultureInfo.InvariantCulture);
                Client.QueryString.Add("amount", ReversalAmount);
            }
            //cakel: BUGID00061 - added code to check when response is sent back for review
          var  RawResponse = Client.DownloadString(EfxSettings.CreditCardProcessorUrl);
            //return BuildResponse(Client.DownloadString(Settings.CreditCardProcessorUrl), true);
          return BuildResponse(RawResponse, 1);
        }

        public static CreditCardPaymentResponse ReverseAuthorizedCreditCardPayment(string gatewayUsername, string gatewayPassword, string transactionId, decimal? amount, bool isTestUser)
        {
            var Client = new WebClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            Client.QueryString.Add("custid", gatewayUsername);
            Client.QueryString.Add("password", gatewayPassword);
            Client.QueryString.Add("action", TcLinkResponseType.Reversal.GetFriendlyName());
            Client.QueryString.Add("demo", GetDemoCode(isTestUser));
            Client.QueryString.Add("transid", transactionId);
            if (amount.HasValue && amount > 0.00M)
            {
                var ReversalAmount = ((int)(amount * 100)).ToString(CultureInfo.InvariantCulture);
                Client.QueryString.Add("amount", ReversalAmount);
            }
            var RawResponse = Client.DownloadString(EfxSettings.CreditCardProcessorUrl);
            return BuildResponse(RawResponse, 3);
        }

        private static string GetDemoCode(bool isTestUser)
        {
            return isTestUser || !EfxSettings.ProcessCreditCards ? "y" : "n";
        }

        //Salcedo - 7/18/2016 - added methods for pre/post auth
        public static CreditCardPaymentResponse PreAuthorizePayment(string gatewayUsername, string gatewayPassword, bool isTestUser, decimal amount, string creditCardAccountNumber,
            string creditCardExpirationDate, string cvv, string creditCardHolderName, List<CustomField> customFields = null)
        {
            var Client = new WebClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            Client.QueryString.Add("custid", gatewayUsername);
            Client.QueryString.Add("password", gatewayPassword);
            Client.QueryString.Add("action", TcLinkTransactionType.Preauth.GetFriendlyName());
            Client.QueryString.Add("demo", GetDemoCode(isTestUser));
            Client.QueryString.Add("cc", creditCardAccountNumber.Replace("-", ""));
            Client.QueryString.Add("exp", creditCardExpirationDate);
            Client.QueryString.Add("name", NoAmp(creditCardHolderName));
            Client.QueryString.Add("amount", (Convert.ToInt32(amount * 100)).ToString(CultureInfo.InvariantCulture));

            if (!string.IsNullOrEmpty(cvv))
                Client.QueryString.Add("cvv", cvv);

            if (customFields != null)
            {
                foreach (var Cf in customFields)
                {
                    Client.QueryString.Add(Cf.CustomFieldName, NoAmp(Cf.CustomFieldValue));
                }
            }

            string RawResponse = "";
            var Response = new CreditCardPaymentResponse();
            string ResponseMessage = "";

            ActivityLog al = new ActivityLog();
            string ActivityDescription = "";
            ActivityDescription = "Requesting CC Preauth response from " + EfxSettings.CreditCardProcessorUrl + ". QueryString values: (" +
                "custid=" + Client.QueryString[0] + "; " +
                "password=" + Client.QueryString[1] + "; " +
                "action=" + Client.QueryString[2] + "; " +
                "demo=" + Client.QueryString[3] + "; " +
                "cc=" + Client.QueryString[4].MaskAccountNumber() + "; " +
                "exp=" + Client.QueryString[5] + "; " +
                "name=" + Client.QueryString[6] + "; " +
                "amount=" + Client.QueryString[7] + ";) ";
            al.WriteLog("EfxFramework", ActivityDescription, 3);

            try
            {
                RawResponse = Client.DownloadString(EfxSettings.CreditCardProcessorUrl);
                al.WriteLog("EfxFramework", "Raw CC Preauth response received: " + RawResponse, 3);
                Response = BuildResponse(RawResponse);
            }
            catch (Exception ex)
            {
                al.WriteLog("EfxFramework", "Error exception received while " + ActivityDescription + ". Exception details: " + ex.ToString(), 1);

                Response.Result = GetErrorReason("processordown", out ResponseMessage);
                Response.ResponseMessage = string.Format("{0}-{1}", ServiceResponseMessages.Ccerror, ResponseMessage);
            }
            return Response;
        }
        public static CreditCardPaymentResponse PostAuthorizePayment(string gatewayUsername, string gatewayPassword, bool isTestUser, decimal? amount, string TransactionId)
        {
            var Client = new WebClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            Client.QueryString.Add("custid", gatewayUsername);
            Client.QueryString.Add("password", gatewayPassword);
            Client.QueryString.Add("action", TcLinkTransactionType.Postauth.GetFriendlyName());
            Client.QueryString.Add("demo", GetDemoCode(isTestUser));
            Client.QueryString.Add("transid", TransactionId);

            if (amount != null)
            { 
                Client.QueryString.Add("amount", (Convert.ToInt32(amount * 100)).ToString(CultureInfo.InvariantCulture));
            }

            string RawResponse = "";
            var Response = new CreditCardPaymentResponse();
            string ResponseMessage = "";

            ActivityLog al = new ActivityLog();
            string ActivityDescription = "";
            ActivityDescription = "Requesting CC Postauth response from " + EfxSettings.CreditCardProcessorUrl + ". QueryString values: (" +
                "custid=" + Client.QueryString[0] + "; " +
                "password=" + Client.QueryString[1] + "; " +
                "action=" + Client.QueryString[2] + "; " +
                "demo=" + Client.QueryString[3] + "; " +
                "transid=" + Client.QueryString[4] + "; ";
            if (amount != null)
            {
                ActivityDescription += "amount=" + Client.QueryString[5] + ";) ";
            }
            else
            {
                ActivityDescription += ")";
            }
            al.WriteLog("EfxFramework", ActivityDescription, 3);

            try
            {
                RawResponse = Client.DownloadString(EfxSettings.CreditCardProcessorUrl);
                al.WriteLog("EfxFramework", "Raw CC Postauth response received: " + RawResponse, 3);
                Response = BuildResponse(RawResponse,2); //2 = this is a PostAuth response
            }
            catch (Exception ex)
            {
                al.WriteLog("EfxFramework", "Error exception received while " + ActivityDescription + ". Exception details: " + ex.ToString(), 1);

                Response.Result = GetErrorReason("processordown", out ResponseMessage);
                Response.ResponseMessage = string.Format("{0}-{1}", ServiceResponseMessages.Ccerror, ResponseMessage);
            }
            return Response;
        }

        //dWillis Task 00232 removed isNull parameter and put in transactionType so that we can send the right response message on void
        private static CreditCardPaymentResponse BuildResponse(string rawResponse, int transactionType = 0)
        {
            //TODO: rename isVoid to isRefund
            var Response = new CreditCardPaymentResponse();
            var GatewayResponses = ParseResponseString(rawResponse);
            var Status = GetGatewayResponse(GatewayResponses, TcLinkResponseType.Status.GetFriendlyName());
            Response.TransactionId = GetGatewayResponse(GatewayResponses, TcLinkResponseType.TransactionId.GetFriendlyName());
            string ResponseMessage;

            switch (Status)
            {
                case "approved":
                    Response.Result = GeneralResponseResult.Success;
                    Response.ResponseMessage = ServiceResponseMessages.Processsuccess;
                    break;
                case "accepted":
                    if (transactionType == 0)
                    {
                        Response.Result = GeneralResponseResult.GeneralFailure;
                        Response.ResponseMessage = ServiceResponseMessages.Ccaccepted;
                    }

                    else if (transactionType == 1) //1 = Refunded
                    {
                        Response.Result = GeneralResponseResult.Success;
                        Response.ResponseMessage = ServiceResponseMessages.Ccrefundsuccessful;
                    }
                    else if (transactionType == 2) // 2 = PostAuth - For PostAuth, "accepted" means success
                    {
                        Response.Result = GeneralResponseResult.Success;
                        Response.ResponseMessage = ServiceResponseMessages.Processsuccess;
                    }
                    else if (transactionType == 3) //3 = Reversal
                    {
                        Response.Result = GeneralResponseResult.Success;
                        Response.ResponseMessage = ServiceResponseMessages.CcReverseSuccessful;
                    }
                    else 
                    {
                        Response.Result = GeneralResponseResult.Success;
                        Response.ResponseMessage = ServiceResponseMessages.Ccvoidsuccessful;
                    }
                    break;
                case "decline":
                    Response.Result = GetDeclineReason(GetGatewayResponse(GatewayResponses, TcLinkResponseType.DeclineType.GetFriendlyName()), out ResponseMessage);
                    Response.ResponseMessage = string.Format("{0}-{1}", ServiceResponseMessages.Ccdecline, ResponseMessage);
                    break;
                case "baddata":
                    Response.Result = GetBadDataReason(GetGatewayResponse(GatewayResponses, TcLinkResponseType.Error.GetFriendlyName()),
                        GetGatewayResponse(GatewayResponses, TcLinkResponseType.Offenders.GetFriendlyName()), out ResponseMessage);
                    Response.ResponseMessage = string.Format("{0}-{1}", ServiceResponseMessages.Ccbaddata, ResponseMessage);
                    break;
                case "error":
                    Response.Result = GetErrorReason(GetGatewayResponse(GatewayResponses, TcLinkResponseType.ErrorType.GetFriendlyName()), out ResponseMessage);
                    Response.ResponseMessage = string.Format("{0}-{1}", ServiceResponseMessages.Ccerror, ResponseMessage);
                    break;
            }

            return Response;
        }

        private static string GetGatewayResponse(Dictionary<string, string> collection, string key)
        {
            return collection.ContainsKey(key) ? collection[key] : string.Empty;
        }

        private static Dictionary<string, string> ParseResponseString(string responseString)
        {
            var Responses = new Dictionary<string, string>();

            foreach (var Line in responseString.Split(Environment.NewLine.ToCharArray()))
            {
                if (!String.IsNullOrEmpty(Line) && Line.Contains("="))
                {
                    var Kvp = Line.Split('=');

                    if (!Responses.ContainsKey(Kvp[0]))
                        Responses.Add(Kvp[0], Kvp[1]);
                    else
                        Responses[Kvp[0]] = Kvp[1];
                }
            }

            return Responses;
        }

        private static GeneralResponseResult GetDeclineReason(string declineType, out string responseMsg)
        {
            switch (declineType)
            {
                case "decline":
                    responseMsg = ServiceResponseMessages.Bankdeclined;
                    return GeneralResponseResult.BankDeclined;
                case "avs":
                    responseMsg = ServiceResponseMessages.Avsfailed;
                    return GeneralResponseResult.AvsFailed;
                case "cvv":
                    responseMsg = ServiceResponseMessages.Cvvfailed;
                    return GeneralResponseResult.CvvFailed;
                case "call":
                    responseMsg = ServiceResponseMessages.Call;
                    return GeneralResponseResult.Call;
                case "expiredcard":
                    responseMsg = ServiceResponseMessages.Expiredcard;
                    return GeneralResponseResult.ExpiredCard;
                case "carderror":
                    responseMsg = ServiceResponseMessages.Carderror;
                    return GeneralResponseResult.CardError;
                case "authexpired":
                    responseMsg = ServiceResponseMessages.Authexpired;
                    return GeneralResponseResult.AuthExpired;
                case "fraud":
                    responseMsg = ServiceResponseMessages.Tclinkfraud;
                    return GeneralResponseResult.TcLinkFraud;
                case "blacklist":
                    responseMsg = ServiceResponseMessages.Tclinkblacklist;
                    return GeneralResponseResult.TcLinkBlacklist;
                case "velocity":
                    responseMsg = ServiceResponseMessages.Tclinkvelocity;
                    return GeneralResponseResult.TcLinkVelocity;
                default:
                    responseMsg = ServiceResponseMessages.Declinedefault;
                    return GeneralResponseResult.GeneralFailure;
            }
        }

        private static GeneralResponseResult GetBadDataReason(string error, string offendingFields, out string responseMsg)
        {
            switch (error)
            {
                case "missingfields":
                    responseMsg = String.Format("{0}{1}", ServiceResponseMessages.Missingfields, offendingFields);
                    return GeneralResponseResult.MissingFields;
                case "extrafields":
                    responseMsg = String.Format("{0}{1}", ServiceResponseMessages.Extrafields, offendingFields);
                    return GeneralResponseResult.ExtraFields;
                case "badformat":
                    responseMsg = String.Format("{0}{1}", ServiceResponseMessages.Badformat, offendingFields);
                    return GeneralResponseResult.BadFormat;
                case "badlength":
                    responseMsg = String.Format("{0}{1}", ServiceResponseMessages.Badlength, offendingFields);
                    return GeneralResponseResult.BadLength;
                case "merchantcantaccept":
                    responseMsg = String.Format("{0}{1}", ServiceResponseMessages.Merchantcantaccept, offendingFields);
                    return GeneralResponseResult.MerchantCantAccept;
                case "mismatch":
                    responseMsg = String.Format("{0}{1}", ServiceResponseMessages.Mismatch, offendingFields);
                    return GeneralResponseResult.Mismatch;
                default:
                    responseMsg = ServiceResponseMessages.Baddatadefault;
                    return GeneralResponseResult.GeneralFailure;
            }
        }

        private static GeneralResponseResult GetErrorReason(string errorType, out string responseMsg)
        {
            switch (errorType)
            {
                case "cantconnect":
                    responseMsg = ServiceResponseMessages.Cantconnnect;
                    return GeneralResponseResult.CantConnect;
                case "dnsvalue":
                    responseMsg = ServiceResponseMessages.Dnsvalue;
                    return GeneralResponseResult.DnsValue;
                case "linkfailure":
                    responseMsg = ServiceResponseMessages.Linkfailure;
                    return GeneralResponseResult.LinkFailure;
                case "failtoprocess":
                    responseMsg = ServiceResponseMessages.Failtoprocess;
                    return GeneralResponseResult.FailToProcess;
                case "notallowed":
                    responseMsg = ServiceResponseMessages.Notallowed;
                    return GeneralResponseResult.NotAllowed;
                case "processordown":
                    responseMsg = ServiceResponseMessages.ProcessorDown;
                    return GeneralResponseResult.ProcessorDown;
                default:
                    responseMsg = ServiceResponseMessages.Errordefault;
                    return GeneralResponseResult.GeneralFailure;
            }
        }
    }
}
