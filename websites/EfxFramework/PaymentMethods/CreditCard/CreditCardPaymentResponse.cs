﻿namespace EfxFramework.PaymentMethods.CreditCard
{
    public class CreditCardPaymentResponse
    {
        public GeneralResponseResult Result { get; set; }
        public string ResponseMessage { get; set; }
        public string TransactionId { get; set; }
    }
}
