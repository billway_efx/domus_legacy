﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace EfxFramework.PaymentMethods
{
    public class PaymentAmount
    {
        public decimal BaseAmount { get; set; }
        public decimal FeeAmount { get; set; }
        public decimal TotalAmount { get { return BaseAmount + FeeAmount; } }
        public string FeeDescription { get; set; }

        public PaymentAmount()
        {
            BaseAmount = 0;
            FeeAmount = 0;
        }

        public PaymentAmount(decimal baseAmount)
        {
            BaseAmount = baseAmount;
            FeeAmount = 0;
        }

        public PaymentAmount(decimal baseAmount, string feeDescription, PaymentType pType, int renterId)
        {
            BaseAmount = baseAmount;
            //cakel: 08/09/2016 - Updated function to accept amount
            CalculateFeeAmount(pType, renterId, baseAmount);
            FeeDescription = feeDescription;
        }

        public static PaymentAmount GetVoidAmount(decimal paymentAmount, Renter renter, bool applyFee = false)
        {
            var VoidAmount = applyFee ? new PaymentAmount(paymentAmount, "Convenience Fee", PaymentType.CreditCard, renter.RenterId) : new PaymentAmount { BaseAmount = paymentAmount, FeeAmount = 0 };
            VoidAmount.BaseAmount *= -1;
            VoidAmount.FeeAmount *= -1;

            return VoidAmount;
        }

        public static PaymentAmount GetPaymentAmount(decimal paymentAmount, Renter renter, bool applyFee = false)
        {
            return GetPaymentAmount(paymentAmount, renter, PaymentType.CreditCard, applyFee);
        }

        public static PaymentAmount GetPaymentAmount(decimal paymentAmount, Renter renter, PaymentType paymentType, bool applyFee = false)
        {
            return applyFee ? new PaymentAmount(paymentAmount, "Convenience Fee", paymentType, renter.RenterId) : new PaymentAmount { BaseAmount = paymentAmount, FeeAmount = 0 };
        }

        public void CalculateFeeAmount(PaymentType pType, int renterId, decimal Amount)
        {
            var Property = EfxFramework.Property.GetPropertyByRenterId(renterId);

            if (Property.PropertyId.HasValue && Property.PropertyId > 0)
                CalculateFeeAmount(pType, Property, Amount);
        }
        //cakel: 08/09/2016 - Updatd convFee to return amount based on Amount and not Avg rent.
        public void CalculateFeeAmount(PaymentType pType, Property property, decimal amount)
        {
            int ProgramID = (int)property.ProgramId.Value;
            string PaymentMethod = "";
            switch (pType)
            {
                case PaymentType.CreditCard:
                    PaymentMethod = "CC";
                    //FeeAmount = ConvenienceFee.GetConvenienceFeeForProperty(property.ProgramId.HasValue ? property.ProgramId.Value : Property.Program.Unassigned, property.AverageMonthlyRent).CreditCardFee;
                    FeeAmount = GetConvFeeByProgramID(ProgramID, PaymentMethod, amount);
                    break;

                case PaymentType.ECheck:
                    PaymentMethod = "CK";
                    //FeeAmount = ConvenienceFee.GetConvenienceFeeForProperty(property.ProgramId.HasValue ? property.ProgramId.Value : Property.Program.Unassigned, property.AverageMonthlyRent).ECheckFee;
                    FeeAmount = GetConvFeeByProgramID(ProgramID, PaymentMethod, amount);
                    break;
            }
        }


        public static decimal GetConvFeeByProgramID(int ProgramID, string PaymentMethod, decimal amount)
        {
            string ConnString = EfxFramework.EfxSettings.ConnectionString;


            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("Wallet_GetConvFeeByProgramID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ProgramID", SqlDbType.Int).Value = ProgramID;
            com.Parameters.Add("@PaymentMethod", SqlDbType.NVarChar, 30).Value = PaymentMethod;
            com.Parameters.Add("@Amount", SqlDbType.Decimal).Value = amount;
            com.Parameters.Add("@ConvFee_output", SqlDbType.Money).Direction = ParameterDirection.Output;

            decimal ConvFeeAmount = 0m;
            try
            {

                con.Open();
                com.ExecuteNonQuery();
                ConvFeeAmount = (decimal)com.Parameters["@ConvFee_output"].Value;
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

            return ConvFeeAmount;

        }



    }
}
