﻿using System;

namespace EfxFramework.PaymentMethods
{
    public class CreditCardDetails : PaymentMethodsBase
    {
        public string CardHolderName { get; set; }
        public string CreditCardNumber { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string CvvCode { get; set; }
        public string CreditCardExpirationString { get { return ExpirationDate.ToString("MMyy"); } }

        public CreditCardDetails(){}

        public CreditCardDetails(Address billingAddress, string firstName, string lastName, string cardHolderName, string creditCardNumber, DateTime expirationDate, string cvvCode)
            :base (billingAddress, firstName, lastName)
        {
            CardHolderName = cardHolderName;
            CreditCardNumber = creditCardNumber;
            ExpirationDate = expirationDate;
            CvvCode = cvvCode;
        }
    }
}