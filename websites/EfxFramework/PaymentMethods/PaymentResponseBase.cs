﻿using System;

namespace EfxFramework.PaymentMethods
{
    public class PaymentResponseBase
    {
        public int Result { get; set; }
        public string Message { get; set; }
        public string TransactionId { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal ConvenienceFee { get; set; }
    }
}
