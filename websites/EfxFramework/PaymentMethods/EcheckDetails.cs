﻿namespace EfxFramework.PaymentMethods
{
    public class EcheckDetails : PaymentMethodsBase
    {
        public string BankAccountNumber { get; set; }
        public string BankRoutingNumber { get; set; }
        public AccountType TypeOfAccount { get; set; }

        public EcheckDetails()
        {
            TypeOfAccount = AccountType.Checking;
        }

        public EcheckDetails(Address billingAddress, string firstName, string lastName, string bankAccountNumber, string bankRoutingNumber, AccountType typeOfAccount)
            : base(billingAddress, firstName, lastName)
        {
            BankAccountNumber = bankAccountNumber;
            BankRoutingNumber = bankRoutingNumber;
            TypeOfAccount = typeOfAccount;
        }
    }
}
