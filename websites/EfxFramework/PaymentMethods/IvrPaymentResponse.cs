﻿namespace EfxFramework.PaymentMethods
{
    public class IvrPaymentResponse : PaymentResponseBase
    {
        public string PaymentQueueNumber { get; set; }
    }
}
