﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EfxFramework.ExtensionMethods;
using EfxFramework.Web.Caching;
using Mb2x.Data;

namespace EfxFramework
{
    public class SearchGridProperty
    {
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        //cakel: 00498
        public string PropertyName2 { get; set; }
        public string CityState { get; set; }
        public string PostalCode { get; set; }
        public string LinkUrl { get; set; }
        public string LinkText { get; set; }

        public SearchGridProperty(Property property, string nextStepUrl, string linkText)
        {
            if (!property.PropertyId.HasValue || property.PropertyId < 0)
                return;

            PropertyId = property.PropertyId.Value;
            PropertyName = property.PropertyName;
            //cakel: 00498
            PropertyName2 = property.PropertyNameTwo;
            CityState = string.Format("{0}, {1}", property.City, GetStateFriendlyName(property.StateProvinceId));
            PostalCode = property.PostalCode;
            LinkUrl = string.Format("~/{0}?PropId={1}", nextStepUrl, PropertyId);
            LinkText = linkText;
        }

        public string GetStateFriendlyName(int? stateProvinceId)
        {
            return !stateProvinceId.HasValue ? "N/A" : ((StateProvince) stateProvinceId.Value).ToString();
        }
    }
}
