using Mb2x.Data;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class MobileServiceUrl : BaseEntity
	{
		//Public Properties
		[DisplayName(@"MobileServiceUrlId"), Required(ErrorMessage = "MobileServiceUrlId is required.")]
		public int MobileServiceUrlId { get; set; }

		[DisplayName(@"MobileServiceUrlText"), Required(ErrorMessage = "MobileServiceUrlText is required."), StringLength(255)]
		public string MobileServiceUrlText { get; set; }

		[DisplayName(@"VersionNumber"), Required(ErrorMessage = "VersionNumber is required.")]
		public int VersionNumber { get; set; }

        public static MobileServiceUrl GetMobileServiceUrlByVersionNumber(int versionNumber)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new MobileServiceUrl(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_MobileServiceUrl_GetMobileServiceUrlByVersionNumber", new SqlParameter("@VersionNumber", versionNumber)));
        }

        public static void DeleteMobileServiceUrl(int mobileServiceUrlId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_MobileServiceUrl_DeleteSingleObject", new SqlParameter("@MobileServiceUrlId", mobileServiceUrlId));
        }

        public static int Set(int mobileServiceUrlId, string mobileServiceUrlText, int versionNumber)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_MobileServiceUrl_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@MobileServiceUrlId", mobileServiceUrlId),
	                    new SqlParameter("@MobileServiceUrlText", mobileServiceUrlText),
	                    new SqlParameter("@VersionNumber", versionNumber)
                    });
        }

        public static int Set(MobileServiceUrl m)
        {
	        return Set(
		        m.MobileServiceUrlId,
		        m.MobileServiceUrlText,
		        m.VersionNumber
	        );
        }

        public MobileServiceUrl(int? mobileServiceUrlId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_MobileServiceUrl_GetSingleObject", new [] { new SqlParameter("@MobileServiceUrlId", mobileServiceUrlId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        MobileServiceUrlId = (int)ResultSet[0];
		        MobileServiceUrlText = ResultSet[1] as string;
		        VersionNumber = (int)ResultSet[2];
            }
        }

		public MobileServiceUrl()
		{
			InitializeObject();
		}

	}
}
