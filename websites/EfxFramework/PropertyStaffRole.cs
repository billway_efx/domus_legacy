using Mb2x.Data;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class PropertyStaffRole : BaseEntity
	{
		//Public Properties
		[DisplayName(@"PropertyStaffId"), Required(ErrorMessage = "PropertyStaffId is required.")]
		public int PropertyStaffId { get; set; }

		[DisplayName(@"RoleId"), Required(ErrorMessage = "RoleId is required.")]
		public int RoleId { get; set; }


        public static int Set(int propertyStaffId, int roleId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_PropertyStaffRole_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@PropertyStaffId", propertyStaffId),
	                    new SqlParameter("@RoleId", roleId)
                    });
        }

        public static int Set(PropertyStaffRole p)
        {
	        return Set(
		        p.PropertyStaffId,
		        p.RoleId
	        );
        }

        public PropertyStaffRole(int propertyStaffId, int roleId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_PropertyStaffRole_GetSingleObject", new [] { new SqlParameter("@PropertyStaffId", propertyStaffId), new SqlParameter("@RoleId", roleId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        PropertyStaffId = (int)ResultSet[0];
		        RoleId = (int)ResultSet[1];
            }
        }

		public PropertyStaffRole()
		{
			InitializeObject();
		}

	}
}
