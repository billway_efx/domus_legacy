﻿using EfxFramework.Interfaces.UserControls;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EfxFramework
{
    public class DetailTile : IDetailTile
    {
        //Public properties
        public string HeaderText { get; set; }
        public string MainDetailHtml { get; set; }
        public string DetailsLinkUrl { get; set; }
        public List<string> DataSource { get; set; }
        public string AdditionalLinksHtml { get; set; }

        /// <summary>
        /// Loop through each element of DataSource if it is not null and has items, and insert the contents of the element into an HTML paragraph tag.
        /// </summary>
        public void DataBind()
        {
            //If the DataSource is not null and has elements
            if (DataSource != null && DataSource.Count > 0)
            {
                //For each element of DataSource, insert the contents into a <p> element
                MainDetailHtml = DataSource.Aggregate(String.Empty, (current, info) => String.Format("{0}<p class='padding-bottom'>{1}</p>", current, info));                
            }
        }
    }
}
