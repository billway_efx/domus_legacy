using Mb2x.Data;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class Permission : BaseEntity
	{
		//Public Properties
		[DisplayName(@"PermissionId"), Required(ErrorMessage = "PermissionId is required.")]
		public int PermissionId { get; set; }

		[DisplayName(@"PermissionName"), Required(ErrorMessage = "PermissionName is required."), StringLength(100)]
		public string PermissionName { get; set; }

		[DisplayName(@"IsActive"), Required(ErrorMessage = "IsActive is required.")]
		public bool IsActive { get; set; }


        public static int Set(int permissionId, string permissionName, bool isActive)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_Permission_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@PermissionId", permissionId),
	                    new SqlParameter("@PermissionName", permissionName),
	                    new SqlParameter("@IsActive", isActive)
                    });
        }

        public static int Set(Permission p)
        {
	        return Set(
		        p.PermissionId,
		        p.PermissionName,
		        p.IsActive
	        );
        }

        public Permission(int permissionId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_Permission_GetSingleObject", new [] { new SqlParameter("@PermissionId", permissionId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        PermissionId = (int)ResultSet[0];
		        PermissionName = ResultSet[1] as string;
		        IsActive = (bool)ResultSet[2];
            }
        }

		public Permission()
		{
			InitializeObject();
		}

	}
}
