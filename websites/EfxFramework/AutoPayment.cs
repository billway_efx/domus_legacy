using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Mb2x.Data;
using System.Data.SqlClient;

namespace EfxFramework
{
    public class AutoPayment : BaseEntity
    {
        //Public Properties
        public int AutoPaymentId { get; set; }
        public int RenterId { get; set; }
        public int PayerId { get; set; }
        public int PaymentTypeId { get; set; }
        public int? PayerCreditCardId { get; set; }
        public int? PayerAchId { get; set; }
        public decimal? RentAmount { get; set; }
        public string RentAmountDescription { get; set; }
        public decimal? CharityAmount { get; set; }
        public string CharityName { get; set; }
        public decimal? OtherAmount1 { get; set; }
        public string OtherDescription1 { get; set; }
        public decimal? OtherAmount2 { get; set; }
        public string OtherDescription2 { get; set; }
        public decimal? OtherAmount3 { get; set; }
        public string OtherDescription3 { get; set; }
        public decimal? OtherAmount4 { get; set; }
        public string OtherDescription4 { get; set; }
        public decimal? OtherAmount5 { get; set; }
        public string OtherDescription5 { get; set; }
        public int PaymentDayOfMonth { get; set; }
        public bool IsActive { get; set; }

        //Public Methods
        public static void LogAutoPayment(int autoPaymentId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_AutoPaymentLog_SetSingleObject", new[] {new SqlParameter("@AutoPaymentId", autoPaymentId), new SqlParameter("@TransactionDateTime", DateTime.UtcNow)});
        }

        public static void DeleteAutoPayment(int autoPaymentId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_AutoPayment_DeleteByAutoPaymentId", new SqlParameter("@AutoPaymentId", autoPaymentId));
        }

        public static List<AutoPayment> GetAutoPaymentListByPaymentDayOfTheMonth(int paymentDay)
        {
            return DataAccess.GetTypedList<AutoPayment>(EfxSettings.ConnectionString, "dbo.usp_AutoPayment_GetAutoPaymentByPaymentDayOfMonth", new SqlParameter("@PaymentDayOfMonth", paymentDay));
        }

        public static AutoPayment GetAutoPaymentByRenterId(int renterId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            var AutoPaymentId = NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_AutoPayment_GetAutoPaymentByRenterId", new SqlParameter("@RenterId", renterId));
            return AutoPaymentId > 0 ? new AutoPayment(AutoPaymentId) : new AutoPayment();
        }

        public static List<DateTime> GetAutoPaymentLogTransactionDate(int autoPaymentId)
        {
            using (var LogIds = DataAccess.ExecuteDataTable(EfxSettings.ConnectionString, "dbo.usp_AutoPaymentLog_GetAutoPaymentLogsByAutoPaymentId", new SqlParameter("@AutoPaymentId", autoPaymentId)))
            {
                var Dates = new List<DateTime>();

                foreach (var Id in from DataRow Row in LogIds.Rows select (int) Row["AutoPaymentLogId"])
                {
                    using (var Logs = DataAccess.ExecuteDataTable(EfxSettings.ConnectionString, "dbo.usp_AutoPaymentLog_GetSingleObject", new SqlParameter("@AutoPaymentLogId", Id)))
                    {
                        Dates.AddRange(from DataRow R in Logs.Rows select (DateTime) R["TransactionDateTime"]);
                    }
                }

                return Dates;
            }
        }

        public static int Set(int autoPaymentId, int renterId, int payerId, int paymentTypeId, int? payerCreditCardId, int? payerAchId, decimal? rentAmount, string rentAmountDescription, decimal? charityAmount, string charityName, decimal? otherAmount1, string otherDescription1, decimal? otherAmount2, string otherDescription2, decimal? otherAmount3, string otherDescription3, decimal? otherAmount4, string otherDescription4, decimal? otherAmount5, string otherDescription5, int paymentDayOfMonth)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
                EfxSettings.ConnectionString,
                "dbo.usp_AutoPayment_SetSingleObject",
            new[]
                {
	                new SqlParameter("@AutoPaymentId", autoPaymentId),
	                new SqlParameter("@RenterId", renterId),
	                new SqlParameter("@PayerId", payerId),
	                new SqlParameter("@PaymentTypeId", paymentTypeId),
	                new SqlParameter("@PayerCreditCardId", payerCreditCardId),
	                new SqlParameter("@PayerAchId", payerAchId),
	                new SqlParameter("@RentAmount", rentAmount),
	                new SqlParameter("@RentAmountDescription", rentAmountDescription),
	                new SqlParameter("@CharityAmount", charityAmount),
	                new SqlParameter("@CharityName", charityName),
	                new SqlParameter("@OtherAmount1", otherAmount1),
	                new SqlParameter("@OtherDescription1", otherDescription1),
                    new SqlParameter("@OtherAmount2", otherAmount2),
	                new SqlParameter("@OtherDescription2", otherDescription2),
                    new SqlParameter("@OtherAmount3", otherAmount3),
	                new SqlParameter("@OtherDescription3", otherDescription3),
                    new SqlParameter("@OtherAmount4", otherAmount4),
	                new SqlParameter("@OtherDescription4", otherDescription4),
                    new SqlParameter("@OtherAmount5", otherAmount5),
	                new SqlParameter("@OtherDescription5", otherDescription5),
	                new SqlParameter("@PaymentDayOfMonth", paymentDayOfMonth)
                });
        }
        public static int Set(AutoPayment a)
        {
            return Set(
                a.AutoPaymentId,
                a.RenterId,
                a.PayerId,
                a.PaymentTypeId,
                a.PayerCreditCardId,
                a.PayerAchId,
                a.RentAmount,
                a.RentAmountDescription,
                a.CharityAmount,
                a.CharityName,
                a.OtherAmount1,
                a.OtherDescription1,
                a.OtherAmount2,
                a.OtherDescription2,
                a.OtherAmount3,
                a.OtherDescription3,
                a.OtherAmount4,
                a.OtherDescription4,
                a.OtherAmount5,
                a.OtherDescription5,
                a.PaymentDayOfMonth
            );
        }

        public AutoPayment(int? autoPaymentId)
        {
            //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_AutoPayment_GetSingleObject", new[] { new SqlParameter("@AutoPaymentId", autoPaymentId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
                AutoPaymentId = (int)ResultSet[0];
                RenterId = (int)ResultSet[1];
                PayerId = (int)ResultSet[2];
                PaymentTypeId = (int)ResultSet[3];
                PayerCreditCardId = ResultSet[4] as int?;
                PayerAchId = ResultSet[5] as int?;
                RentAmount = ResultSet[6] as decimal?;
                RentAmountDescription = ResultSet[7] as string;
                CharityAmount = ResultSet[8] as decimal?;
                CharityName = ResultSet[9] as string;
                OtherAmount1 = ResultSet[10] as decimal?;
                OtherDescription1 = ResultSet[11] as string;
                OtherAmount2 = ResultSet[12] as decimal?;
                OtherDescription2 = ResultSet[13] as string;
                OtherAmount3 = ResultSet[14] as decimal?;
                OtherDescription3 = ResultSet[15] as string;
                OtherAmount4 = ResultSet[16] as decimal?;
                OtherDescription4 = ResultSet[17] as string;
                OtherAmount5 = ResultSet[18] as decimal?;
                OtherDescription5 = ResultSet[19] as string;
                PaymentDayOfMonth = (int)ResultSet[20];
            }
        }
        public AutoPayment()
        {
            InitializeObject();
        }

    }
}
