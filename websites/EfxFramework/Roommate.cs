using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Mb2x.Data;

namespace EfxFramework
{
	public class Roommate : BaseEntity
	{
		//Public Properties
		public int RoommateId { get; set; }
		public int RenterId { get; set; }
		public string Prefix { get; set; }
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string LastName { get; set; }
		public string Suffix { get; set; }
		public string MainPhoneNumber { get; set; }
		public string MobilePhoneNumber { get; set; }
		public string FaxNumber { get; set; }
		public string PrimaryEmailAddress { get; set; }
		public byte[] Photo { get; set; }
		public string StreetAddress { get; set; }
		public string Unit { get; set; }
		public string City { get; set; }
		public int? StateProvinceId { get; set; }
		public string PostalCode { get; set; }
		public string PmsId { get; set; }
		public int? PmsTypeId { get; set; }
		public bool IsActive { get; set; }
		public DateTime? ImportDateTime { get; set; }
		public DateTime? ExportDateTime { get; set; }
		public DateTime? MoveInDate { get; set; }
		public DateTime? MoveOutDate { get; set; }
		public bool IsResponsibleForLease { get; set; }

        public static Roommate GetRoommateByRenterIdAndPmsId(int renterId, string pmsId)
        {
            //return new Roommate();
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new Roommate(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_Roommate_GetRoommateByRenterIdAndPmsId", new[] {new SqlParameter("@RenterId", renterId), new SqlParameter("@PmsId", pmsId)}));
        }

        public static List<Roommate> GetRoommatesByRenterId(int renterId)
        {
            return DataAccess.GetTypedList<Roommate>(EfxSettings.ConnectionString, "dbo.usp_Roommate_GetRoommateByRenterId", new SqlParameter("@RenterId", renterId));
        }

        public static int Set(int roommateId, int renterId, string prefix, string firstName, string middleName, string lastName, string suffix, string mainPhoneNumber, string mobilePhoneNumber, string faxNumber, string primaryEmailAddress, byte[] photo, string streetAddress, string unit, string city, int? stateProvinceId, string postalCode, string pmsId, int? pmsTypeId, bool isActive, DateTime? importDateTime, DateTime? exportDateTime, DateTime? moveInDate, DateTime? moveOutDate, bool isResponsibleForLease)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_Roommate_SetSingleObject",
            new[]
            {
	            new SqlParameter("@RoommateId", roommateId),
	            new SqlParameter("@RenterId", renterId),
	            new SqlParameter("@Prefix", prefix),
	            new SqlParameter("@FirstName", firstName),
	            new SqlParameter("@MiddleName", middleName),
	            new SqlParameter("@LastName", lastName),
	            new SqlParameter("@Suffix", suffix),
	            new SqlParameter("@MainPhoneNumber", mainPhoneNumber),
	            new SqlParameter("@MobilePhoneNumber", mobilePhoneNumber),
	            new SqlParameter("@FaxNumber", faxNumber),
	            new SqlParameter("@PrimaryEmailAddress", primaryEmailAddress),
	            new SqlParameter("@Photo", photo),
	            new SqlParameter("@StreetAddress", streetAddress),
	            new SqlParameter("@Unit", unit),
	            new SqlParameter("@City", city),
	            new SqlParameter("@StateProvinceId", stateProvinceId),
	            new SqlParameter("@PostalCode", postalCode),
	            new SqlParameter("@PmsId", pmsId),
	            new SqlParameter("@PmsTypeId", pmsTypeId),
	            new SqlParameter("@IsActive", isActive),
	            new SqlParameter("@ImportDateTime", importDateTime),
	            new SqlParameter("@ExportDateTime", exportDateTime),
	            new SqlParameter("@MoveInDate", moveInDate),
	            new SqlParameter("@MoveOutDate", moveOutDate),
	            new SqlParameter("@IsResponsibleForLease", isResponsibleForLease)
            });
        }

        public static int Set(Roommate r)
        {
	        return Set(
		        r.RoommateId,
		        r.RenterId,
		        r.Prefix,
		        r.FirstName,
		        r.MiddleName,
		        r.LastName,
		        r.Suffix,
		        r.MainPhoneNumber,
		        r.MobilePhoneNumber,
		        r.FaxNumber,
		        r.PrimaryEmailAddress,
		        r.Photo,
		        r.StreetAddress,
		        r.Unit,
		        r.City,
		        r.StateProvinceId,
		        r.PostalCode,
		        r.PmsId,
		        r.PmsTypeId,
		        r.IsActive,
		        r.ImportDateTime,
		        r.ExportDateTime,
		        r.MoveInDate,
		        r.MoveOutDate,
		        r.IsResponsibleForLease
	        );
        }

        public Roommate(int? roommateId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_Roommate_GetSingleObject", new [] { new SqlParameter("@RoommateId", roommateId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        RoommateId = (int)ResultSet[0];
		        RenterId = (int)ResultSet[1];
		        Prefix = ResultSet[2] as string;
		        FirstName = ResultSet[3] as string;
		        MiddleName = ResultSet[4] as string;
		        LastName = ResultSet[5] as string;
		        Suffix = ResultSet[6] as string;
		        MainPhoneNumber = ResultSet[7] as string;
		        MobilePhoneNumber = ResultSet[8] as string;
		        FaxNumber = ResultSet[9] as string;
		        PrimaryEmailAddress = ResultSet[10] as string;
		        Photo = ResultSet[11] as byte[];
		        StreetAddress = ResultSet[12] as string;
		        Unit = ResultSet[13] as string;
		        City = ResultSet[14] as string;
		        StateProvinceId = ResultSet[15] as int?;
		        PostalCode = ResultSet[16] as string;
		        PmsId = ResultSet[17] as string;
		        PmsTypeId = ResultSet[18] as int?;
		        IsActive = (bool)ResultSet[19];
		        ImportDateTime = ResultSet[20] as DateTime?;
		        ExportDateTime = ResultSet[21] as DateTime?;
		        MoveInDate = ResultSet[22] as DateTime?;
		        MoveOutDate = ResultSet[23] as DateTime?;
		        IsResponsibleForLease = (bool)ResultSet[24];
            }
        }

        public Roommate()
        {
            InitializeObject();
        }
	}
}
