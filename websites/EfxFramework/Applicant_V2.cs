﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using EfxFramework.Security.Authentication;



//cakel: 00546
namespace EfxFramework
{


    public static class Applicant_V2
    {

        //cakel: 00546
        public class GetPropertyApplicationDetail
        {
            public int PropertyID;
            public string PropertyName;
            public string DocumentName;
            public byte[] DocumentBinary;
            public int PmsTypeId;
            public string PmsId;
            public int IsIntegrated;
        }


        //cakel: 00546
        public static int DownLoadPropertyApplication(int PropID)
        {
            int FileStatus = 0;
            using (SqlDataReader Reader = Document_GetPropertyApplication(PropID))
            {
                Reader.Read();
                if (Reader.HasRows)
                {
                    string _FileName = Reader["DocumentName"].ToString();
                    string _ContentType = "application/pdf";
                    byte[] _BinaryData = (byte[])Reader["DocumentBinary"];

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.Buffer = true;
                    HttpContext.Current.Response.Charset = "";
                    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    HttpContext.Current.Response.ContentType = _ContentType;
                    HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=" + _FileName);
                    HttpContext.Current.Response.BinaryWrite(_BinaryData);
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();

                    FileStatus = 1;
                }
                else
                {
                    FileStatus = 0;
                }

                return FileStatus;

            }

        }

        public static int DownloadApplicantsApplication(int ApplicantID)
        {
            int FileStatus = 0;
            using (SqlDataReader Reader = Document_GetApplicantsApplication(ApplicantID))
            {
                Reader.Read();
                if (Reader.HasRows)
                {
                    string _FileName = Reader["DocumentName"].ToString();
                    string _ContentType = "application/pdf";
                    byte[] _BinaryData = (byte[])Reader["CompletedApplication"];

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.Buffer = true;
                    HttpContext.Current.Response.Charset = "";
                    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    HttpContext.Current.Response.ContentType = _ContentType;
                    HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=" + _FileName);
                    HttpContext.Current.Response.BinaryWrite(_BinaryData);
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();

                    FileStatus = 1;
                }
                else
                {
                    FileStatus = 0;
                }

                return FileStatus;

            }

        }


        //cakel: 00546
        private static SqlDataReader Document_GetPropertyApplication(int PropertyID)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Property_GetApplicationForDownLoad", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = PropertyID;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);

            }
            catch (Exception)
            {
                con.Close();
                throw;
            }

        }

        //usp_Applicant_GetApplicationForDownLoad
        private static SqlDataReader Document_GetApplicantsApplication(int ApplicantID)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Applicant_GetApplicationForDownLoad", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ApplicantID", SqlDbType.Int).Value = ApplicantID;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);

            }
            catch (Exception)
            {
                con.Close();
                throw;
            }


        }

        public static void Applicant_ValidateIntegrated(int PropertyID, string PmsID)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Applicant_ValidateIntegrated", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = PropertyID;
            com.Parameters.Add("@PmsID", SqlDbType.Int).Value = PmsID;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

            }
            catch(Exception e)
            {
                con.Close();
                throw e;

            }


        }


        //cakel: 00546
        public static GetPropertyApplicationDetail GetPropertyAppDetail(int PropertyID)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Property_GetApplicationForDownLoad", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = PropertyID;

            try
            {
                con.Open();
                using (SqlDataReader Reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    
                    GetPropertyApplicationDetail Gd = new GetPropertyApplicationDetail();
                    Reader.Read();
                    if (Reader.HasRows)
                    {
                        Gd.PropertyID = (int)Reader["PropertyId"];
                        Gd.PropertyName = Reader["PropertyName"].ToString();
                        Gd.DocumentName = Reader["DocumentName"].ToString();
                        Gd.DocumentBinary = (byte[])Reader["DocumentBinary"];
                        Gd.PmsTypeId = (int)Reader["PmsTypeId"];
                        Gd.PmsId = Reader["PmsId"].ToString();
                        Gd.IsIntegrated = (int)Reader["IsIntegrated"];

                    }
                    return Gd;

                }

            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

        }

        //New function to use new procedure that adds phone number into database
        public static int Applicant_InsertUpdate_v2(int ApplicantID, int PropertyID, string FirstName, string LastName, string PMSID,
            bool PmsValidate, string emailaddress, string phoneNum)
        {

            //Generate a new random password
            var TempPassword = AuthenticationTools.GenerateRandomPassword();
            //Generate a new salt and set it
            byte[] _Salt = AuthenticationTools.GenerateSalt();
            //Hash the new password and set it
            byte[] _PasswordHash = AuthenticationTools.HashPassword(TempPassword, _Salt);

            //usp_Applicant_InsertUpdate
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Applicant_InsertUpdateSingle_v2", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ApplicantId", SqlDbType.Int).Value = ApplicantID;
            com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = PropertyID;
            com.Parameters.Add("@FirstName", SqlDbType.NVarChar, 150).Value = FirstName;
            com.Parameters.Add("@LastName", SqlDbType.NVarChar, 150).Value = LastName;
            com.Parameters.Add("@PMSID", SqlDbType.NVarChar, 150).Value = PMSID;
            com.Parameters.Add("@Isvalidated", SqlDbType.Bit).Value = PmsValidate;
            com.Parameters.Add("@PrimaryEmailAddress", SqlDbType.NVarChar, 150).Value = emailaddress;
            com.Parameters.Add("@PasswordHash", SqlDbType.Binary, 64).Value = _PasswordHash;
            com.Parameters.Add("@Salt", SqlDbType.Binary, 16).Value = _Salt;
            com.Parameters.Add("@phoneNum", SqlDbType.NVarChar, -1).Value = phoneNum;
            com.Parameters.Add("@ApplicantiD_out", SqlDbType.Int).Direction = ParameterDirection.Output;

            int _ApplicantIDOut = 0;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                _ApplicantIDOut = (int)com.Parameters["@ApplicantID_out"].Value;
                con.Close();
                return _ApplicantIDOut;
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }

        }

        //cakel: 00546
        public static int Applicant_InsertUpdate(int ApplicantID, int PropertyID, string FirstName, string LastName, string PMSID,
            bool PmsValidate, string emailaddress)
        {

            //Generate a new random password
            var TempPassword = AuthenticationTools.GenerateRandomPassword();
            //Generate a new salt and set it
            byte[] _Salt = AuthenticationTools.GenerateSalt();
            //Hash the new password and set it
            byte[] _PasswordHash = AuthenticationTools.HashPassword(TempPassword, _Salt);

            //usp_Applicant_InsertUpdate
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Applicant_InsertUpdateSingle", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ApplicantId", SqlDbType.Int).Value = ApplicantID;
            com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = PropertyID;
            com.Parameters.Add("@FirstName", SqlDbType.NVarChar, 150).Value = FirstName;
            com.Parameters.Add("@LastName", SqlDbType.NVarChar, 150).Value = LastName;
            com.Parameters.Add("@PMSID", SqlDbType.NVarChar, 150).Value = PMSID;
            com.Parameters.Add("@Isvalidated", SqlDbType.Bit).Value = PmsValidate;
            com.Parameters.Add("@PrimaryEmailAddress", SqlDbType.NVarChar, 150).Value = emailaddress;
            com.Parameters.Add("@PasswordHash", SqlDbType.Binary,64).Value = _PasswordHash;
            com.Parameters.Add("@Salt", SqlDbType.Binary,16).Value = _Salt;
            com.Parameters.Add("@ApplicantiD_out", SqlDbType.Int).Direction = ParameterDirection.Output;

            int _ApplicantIDOut = 0;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                _ApplicantIDOut = (int)com.Parameters["@ApplicantID_out"].Value;
                con.Close();
                return _ApplicantIDOut;
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }

        }

        public static void ApplicantApplication_InsertUpdate(int ApplicationID, int PropertyID, int AppStatus, bool feesPaid, 
            int AppSubmission, DateTime AppDate, DateTime AppStatusDate, string UpdatedBY )
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_ApplicantApplication_InsertUpdate", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ApplicantId", SqlDbType.Int).Value = ApplicationID;
            com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = PropertyID;
            com.Parameters.Add("@ApplicationStatusId", SqlDbType.Int).Value = AppStatus;
            com.Parameters.Add("@FeesPaid", SqlDbType.Bit).Value = feesPaid;
            com.Parameters.Add("@ApplicationSubmissionTypeId", SqlDbType.Int).Value = AppSubmission;
            com.Parameters.Add("@ApplicationDate", SqlDbType.DateTime).Value = AppDate;
            com.Parameters.Add("@ApplicationStatusDate", SqlDbType.DateTime).Value = AppStatusDate;
            com.Parameters.Add("@StatusUpdatedBy", SqlDbType.NVarChar,100).Value = UpdatedBY;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

            }
            catch(Exception e)
            {
                con.Close();
                throw e;

            }

        }

        //usp_ApplicantApplication_InsertUploadedApplication
        public static void ApplicantApplication_InsertUploadedApplication(int ApplicantID, byte[] UploadedApplication)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_ApplicantApplication_InsertUploadedApplication", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ApplicationID", SqlDbType.Int).Value = ApplicantID;
            com.Parameters.Add("@UploadedApplication", SqlDbType.VarBinary, -1).Value = UploadedApplication;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                con.Close();
                throw e;

            }

        }



        //cakel: 00546
        public static bool CheckforExistingRenterEmail(string Email)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("CheckforExistingRenterEmail", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@Email", SqlDbType.NVarChar, 150).Value = Email;
            com.Parameters.Add("@Result", SqlDbType.Bit).Direction = ParameterDirection.Output;

            bool _result = false;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                _result = (bool)com.Parameters["@Result"].Value;
                con.Close();
                return _result;
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

        }

        public static SqlDataReader Applicant_StatusGetList()
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Applicant_StatusGetList", con);
            com.CommandType = CommandType.StoredProcedure;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);

            }
            catch (Exception)
            {
                con.Close();
                throw;
            }

        }

        public static SqlDataReader Applicant_SubmissionTypeGetList()
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Applicant_SubmissionTypeGetList", con);
            com.CommandType = CommandType.StoredProcedure;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);

            }
            catch (Exception)
            {
                con.Close();
                throw;
            }

        }

        public static SqlDataReader Applicant_GetApplicantDetail(int ApplicantID)
        {
            
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Applicant_GetApplicantDetail", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AppID", SqlDbType.Int).Value = ApplicantID;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);

            }
            catch (Exception)
            {
                con.Close();
                throw;
            }

        }

        //SWitherspoon: New function to retrieve email body text
        public static String get_EmailTemplate_ByTemplateID(int templateID)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("EmailTemplate_GetSingle_Text", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@templateID", SqlDbType.Int).Value = templateID;
            com.Parameters.Add("@templateText", SqlDbType.NVarChar, -1).Direction = ParameterDirection.Output;

            String templateText = "";
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                templateText = com.Parameters["@templateText"].Value.ToString();
                con.Close();
                return templateText;
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

        }

        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }


    }

}
