﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace EfxFramework.Security.Authentication
{
	public class AuthenticationTools
	{
        /// <summary>
        /// This method will generate a string of random digits.
        /// </summary>
        /// <param name="digits">The number of requested digits</param>
        /// <param name="allowZeros">If this is true, zeros will be allowed, otherwise, the result will only have non-zero values</param>
        /// <returns></returns>
        public static string GenerateRandomNumberString(int digits, bool allowZeros)
        {
            //Create a random number generator
            var Rng = RandomNumberGenerator.Create();

            //Create a byte array to hold the random number
            var RandomBytes = new byte[digits];

            //Generate one random byte for each digit
            if (allowZeros)
            {
                Rng.GetBytes(RandomBytes);
            }
            else
            {
                Rng.GetNonZeroBytes(RandomBytes);
            }

            //Append each byte mod 10 to the return value
            var RetVal = new StringBuilder();
            if (allowZeros)
            {
                foreach (var B in RandomBytes)
                {
                    RetVal.Append(B % 10);
                }
            }
            else
            {
                foreach (var B in RandomBytes)
                {
                    RetVal.Append(B % 10 == 0 ? 1 : B % 10);
                }
            }

            //Return
            return RetVal.ToString();
        }

        public static string GenerateRandomPassword()
        {
            return System.Web.Security.Membership.GeneratePassword(8, 2);
        }

		public static byte[] GenerateSalt()
		{
			//Return a GUID string
			return Guid.NewGuid().ToByteArray();
		}

		public static byte[] HashPassword(string password, byte[] salt)
		{
			//Get the password as a unicode encoded byte array
			var PasswordBytes = Encoding.Unicode.GetBytes(password); 

			//Encode the password as a Unicode array
			using (var Buffer = new MemoryStream())
			{
				//Write the password bytes
				Buffer.Write(PasswordBytes, 0, PasswordBytes.Length);

				//Write the salt bytes
				Buffer.Write(salt, 0, salt.Length);

				//Hash and return
				using (var Hasher = SHA512.Create())
				{
					return Hasher.ComputeHash(Buffer.ToArray());
				}
			}
		}

		public static bool ValidatePasswordHash(string password, byte[] passwordHash, byte[] salt)
		{
			//Hash the passed password
			var NewHash = HashPassword(password, salt);

			//Compare the hashes
			return NewHash.SequenceEqual(passwordHash);
		}

		public static bool IsCurrentlyLoggedIn()
		{
			return HttpContext.Current.User.Identity.IsAuthenticated;
		}
	}
}