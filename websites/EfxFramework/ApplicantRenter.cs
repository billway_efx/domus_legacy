using Mb2x.Data;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class ApplicantRenter : BaseEntity
	{
		//Public Properties
		[DisplayName(@"ApplicantId"), Required(ErrorMessage = "ApplicantId is required.")]
		public int ApplicantId { get; set; }

		[DisplayName(@"RenterId"), Required(ErrorMessage = "RenterId is required.")]
		public int RenterId { get; set; }

		[DisplayName(@"TransactionDate"), Required(ErrorMessage = "TransactionDate is required.")]
		public DateTime TransactionDate { get; set; }

        public static int Set(int applicantId, int renterId, DateTime transactionDate)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_ApplicantRenter_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@ApplicantId", applicantId),
	                    new SqlParameter("@RenterId", renterId),
	                    new SqlParameter("@TransactionDate", transactionDate)
                    });
        }

        public static int Set(ApplicantRenter a)
        {
	        return Set(
		        a.ApplicantId,
		        a.RenterId,
		        a.TransactionDate
	        );
        }

        public ApplicantRenter(int? applicantId, int? renterId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_ApplicantRenter_GetSingleObject", new [] { new SqlParameter("@ApplicantId", applicantId), new SqlParameter("@RenterId", renterId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        ApplicantId = (int)ResultSet[0];
		        RenterId = (int)ResultSet[1];
		        TransactionDate = (DateTime)ResultSet[2];
            }
        }

		public ApplicantRenter()
		{
			InitializeObject();
		}

	}
}
