﻿using System;
using System.Runtime.Serialization;

namespace EfxFramework
{
	public class InvalidTelephoneNumberException : Exception
	{
		public InvalidTelephoneNumberException()
		{
		}
		public InvalidTelephoneNumberException(string message)
			: base(message)
		{
		}
		protected InvalidTelephoneNumberException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}