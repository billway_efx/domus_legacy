using Mb2x.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class FeePayment : BaseEntity
	{
		//Public Properties
		[DisplayName(@"FeePaymentId"), Required(ErrorMessage = "FeePaymentId is required.")]
		public int FeePaymentId { get; set; }

		[DisplayName(@"ApplicationFeeId"), Required(ErrorMessage = "ApplicationFeeId is required.")]
		public int ApplicationFeeId { get; set; }

		[DisplayName(@"ApplicantId"), Required(ErrorMessage = "ApplicantId is required.")]
		public int ApplicantId { get; set; }

		[DisplayName(@"FeePaid"), Required(ErrorMessage = "FeePaid is required.")]
		public bool FeePaid { get; set; }

		[DisplayName(@"TransactionDate"), Required(ErrorMessage = "TransactionDate is required.")]
		public DateTime TransactionDate { get; set; }

        [DisplayName(@"TransactionId"), Required(ErrorMessage = "Transaction Id is required.")]
        public int TransactionId { get; set; }

        public static List<FeePayment> GetAllFeePaymentsByTransactionId(int transactionId)
        {
            return DataAccess.GetTypedList<FeePayment>(EfxSettings.ConnectionString, "dbo.usp_FeePayment_GetFeePaymentsByTransactionId", new SqlParameter("@TransactionId", transactionId));
        }

        public static List<FeePayment> GetAllFeePaymentsByApplicantId(int applicantId)
        {
            return DataAccess.GetTypedList<FeePayment>(EfxSettings.ConnectionString, "dbo.usp_FeePayment_GetByApplicantId", new SqlParameter("@ApplicantId", applicantId));
        }

        public static int Set(int feePaymentId, int applicationFeeId, int applicantId, bool feePaid, DateTime transactionDate, int transactionId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_FeePayment_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@FeePaymentId", feePaymentId),
	                    new SqlParameter("@ApplicationFeeId", applicationFeeId),
	                    new SqlParameter("@ApplicantId", applicantId),
	                    new SqlParameter("@FeePaid", feePaid),
	                    new SqlParameter("@TransactionDate", transactionDate),
                        new SqlParameter("@TransactionId", transactionId)
                    });
        }

        public static int Set(FeePayment f)
        {
	        return Set(
		        f.FeePaymentId,
		        f.ApplicationFeeId,
		        f.ApplicantId,
		        f.FeePaid,
		        f.TransactionDate,
                f.TransactionId
	        );
        }

        public FeePayment(int? feePaymentId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_FeePayment_GetSingleObject", new [] { new SqlParameter("@FeePaymentId", feePaymentId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        FeePaymentId = (int)ResultSet[0];
		        ApplicationFeeId = (int)ResultSet[1];
		        ApplicantId = (int)ResultSet[2];
		        FeePaid = (bool)ResultSet[3];
		        TransactionDate = (DateTime)ResultSet[4];
                TransactionId = (int)ResultSet[5];
            }
        }

		public FeePayment()
		{
			InitializeObject();
		}
	}
}
