﻿using Mb2x.ExtensionMethods;
using System;
using System.Linq;

namespace EfxFramework
{
	public class TelephoneNumber
	{	
		//Private Member Variables
		private string _Digits;
		private string _FormatString;

		//Properties
		public string Digits
		{
			get
			{
				return _Digits;
			}
			set
			{
				if (value.TrimNull().LengthNull() == 10)
				{
					_Digits = value.Trim();
				}
				else
				{
					throw new InvalidTelephoneNumberException("Telephone numbers must contain exactly 10 digits.");
				}
			}
		}
		public string FormatString
		{
			get
			{
				return _FormatString.IsTrimNullOrEmpty() ? "{0}{1}{2}" : _FormatString;
			}
			set
			{
				_FormatString = value.TrimNull();
			}
		}
		public string AreaCode
		{
			get
			{
				return Digits.Substring(0, 3);
			}
		}
		public string CityCode
		{
			get
			{
				return Digits.Substring(3, 3);
			}
		}
		public string SubscriberNumber
		{
			get
			{
				return Digits.Substring(6, 4);
			}
		}

		//Static Methods
		public static bool IsValidPhoneNumber(string input)
		{
			//Extract the digits
			var Digits = input.ExtractDigits();

			//If the length of the digits is 10 and the digits match the input exactly, then we have a valid phone number
			return Digits.Length == 10 && string.Equals(input, Digits, StringComparison.Ordinal);
		}

		public static string FormatPhoneNumber(string digits, string format)
		{
			//If the input string is null or empty, just return the input string
			if (digits.IsTrimNullOrEmpty())
			{
				return digits;
			}

			//If we have 10 digits, we can use the phone number format string //If we do not have 10 digits, we cannot format the string
			return digits.Length == 10
					? string.Format(format, digits.Substring(0, 3), digits.Substring(3, 3), digits.Substring(6, 4))
					: string.Empty;
		}

		public static string RemovePhoneNumberFormatting(string phoneNumber)
		{
			//Extract the digits from the input
			var RetVal = phoneNumber.ExtractDigits();

			return string.IsNullOrEmpty(RetVal) ? string.Empty : RetVal.Substring(0, RetVal.Length > 10 ? 10 : RetVal.Length);
		}

		public static bool ValidatePhoneNumber(string input)
		{
			if (string.IsNullOrEmpty(input) || input.Length != 10)
			{
				return false;
			}

			//Split the input into a character array //Loop through all of the characters in the array //If we find anything but a digit, return false
			var Characters = input.ToCharArray();
			return Characters.All(char.IsDigit);
		}

		//Public Overrides
		public override string ToString()
		{
			//Return the default format of the phone number
			return FormatPhoneNumber(Digits, FormatString);
		}
		public string ToString(string formatString)
		{
			return FormatPhoneNumber(Digits, formatString);
		}

		//Constructors
		public TelephoneNumber(string digits)
			: this(digits, null)
		{
		}
		public TelephoneNumber(string digits, string formatString)
		{
			//Extract the digits from the input and set the Digits property
			Digits = RemovePhoneNumberFormatting(digits);

			//Set the FormatString property
			FormatString = formatString;
		}
	}
}