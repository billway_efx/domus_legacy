using System.Web;
using EfxFramework.Web.Caching;
using Mb2x.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;

namespace EfxFramework
{
	public class Wallet_PaymentMethod : BaseEntity
	{
		//Public Properties
        [DisplayName(@"PayerWalletId"), Required(ErrorMessage = "PayerWalletId is required.")]
        public int PayerWalletId { get; set; }

        [DisplayName(@"PayerId"), Required(ErrorMessage = "PayerId is required.")]
        public int PayerId { get; set; }

        [DisplayName(@"PayerMethodId"), Required(ErrorMessage = "PayerMethodId is required.")]
        public int PayerMethodId { get; set; }

        // True = Credit Card payment method, False = ACH payment method
        [DisplayName(@"PaymentMethod"), Required(ErrorMessage = "PaymentMethod is required.")]
        public bool PaymentMethod { get; set; }

        [DisplayName(@"IsPrimary"), Required(ErrorMessage = "IsPrimary is required.")]
        public bool IsPrimary { get; set; }

        [DisplayName(@"AutoSet"), Required(ErrorMessage = "AutoSet is required.")]
        public bool AutoSet { get; set; }

        [DisplayName(@"EXP_Date"), Required(ErrorMessage = "EXP_Date is required.")]
        public string EXP_Date { get; set; }

        [DisplayName(@"Editable"), Required(ErrorMessage = "Editable is required.")]
        public bool Editable { get; set; }

        [DisplayName(@"PayChoose"), Required(ErrorMessage = "PayChoose is required.")]
        public bool PayChoose { get; set; }

        [DisplayName(@"UsedLastTime"), Required(ErrorMessage = "UsedLastTime is required.")]
        public bool UsedLastTime { get; set; }
        
        [DisplayName(@"Description"), Required(ErrorMessage = "Description is required.")]
        public string Description { get; set; }

        [DisplayName(@"PaymentLimit"), Required(ErrorMessage = "RequestSecurityCode is required.")]
        public bool RequestSecurityCode { get; set; }

        //Salcedo - 7/11/2016 - removed ConvenienceFee and replaced with ProgramId
        //[DisplayName(@"ConvenienceFee")]
        //public decimal ConvenienceFee { get; set; }

        [DisplayName(@"ProgramId")]
        public int ProgramId { get; set; }

        public Wallet_PaymentMethod()
		{
			InitializeObject();
		}
	}
}
