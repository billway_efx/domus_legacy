﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using Mb2x.ExtensionMethods;
using Mb2x.Lighthouse;
using System.Security.Cryptography;

namespace EfxFramework.Logging
{
    public class ExceptionLogger
    {
        private static string Details { get; set; }

        public static void LogException(Exception inputException)
        {
            //Task # 0000127 - 6/5/2014 - Salcedo
            //The web service application pool was stopping if the EfxFramework attempted
            //to log an Elmah error, because the HttpApplication variable is null in that context (I think). It might be 
            //stopping for another reason, but there are multiple events in the server's Application log indicating this is
            //a problem.
            //So, my solution, for now, was to wrap this call in an exception handler. Not sure if this will solve the problem.
            //It might be better to test for HttpApplication, but I'm not sure how to reliably do that in all contexts that use
            //this EfxFramework.Logging module. 
            try
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(inputException);
            }
            catch (Exception ex)
            {
                BulkMail.BulkMail.SendMailMessage(null, "EFX", false, EfxSettings.SendMailUsername, EfxSettings.SendMailPassword,
                    new System.Net.Mail.MailAddress(EfxSettings.TechSupportEmail), "LogException Error",
                    "An error occured while attempting to log an error. Here is the original error that was being logged: " + inputException.ToString() +
                    " And here is the error that occurred while attempting to log the error: " + ex.ToString(),
                    new System.Collections.Generic.List<System.Net.Mail.MailAddress> { new System.Net.Mail.MailAddress(EfxSettings.TechSupportEmail) });
            }
        }

        public static void LogMessage(string message)
        {
            var exception = new Exception(message);
            Elmah.ErrorSignal.FromCurrentContext().Raise(exception);
        }

        public static void CaptureDetails(Exception ex)
        {
            if (String.IsNullOrEmpty(Details)) Details = ex.StackTrace; else String.Format("{0}\r\n{1}", Details, ex.StackTrace);

            if (ex.InnerException != null)
                CaptureDetails(ex.InnerException);           
        }

        protected static string LoggedInUser(int iD)
        {
            var UserId = HttpContext.Current.User.Identity.Name.ToNullInt32();
            var NoUserFound = HttpContext.Current.Request.Browser.Crawler ? HttpContext.Current.Request.UserAgent  : "No User Found";

            switch (iD)
            {
                case 1:
                    return String.Format("Portal: Public Site \n UserName: {0}", new Applicant(UserId).ApplicantId > 0 ? new Applicant(UserId).DisplayName : NoUserFound);
                case 2:
                    return String.Format("Portal: Resident Site \n UserName: {0}", new Renter(UserId).RenterId > 0 ? new Renter(UserId).DisplayName : NoUserFound);
                case 3:
                    return String.Format("Portal: Property Manager Site \n UserName: {0}", new PropertyStaff(UserId).PropertyStaffId > 0 ? new PropertyStaff(UserId).DisplayName : NoUserFound);
                case 4:
                    return String.Format("Portal: Admin Site \n UserName: {0}", new EfxAdministrator(UserId).EfxAdministratorId > 0 ? new EfxAdministrator(UserId).DisplayName : NoUserFound);
                default:
                    return NoUserFound;
            }
        }

        protected static string RequestStats()
        {
            var Stats = new StringBuilder();
            Stats.Append(String.Format("Browser: {0}, Version: {1}", HttpContext.Current.Request.Browser.Browser, HttpContext.Current.Request.Browser.Version));

            if (HttpContext.Current.Request.Browser.IsMobileDevice)
                Stats.Append(String.Format("\n\n Mobile: {0}, Model: {1}", HttpContext.Current.Request.Browser.MobileDeviceManufacturer, HttpContext.Current.Request.Browser.MobileDeviceModel));

            return Stats.ToString();
        }

        protected static bool KnownException(string ticketTitle)
        {
            const string pageNoExist = "C781508D4ECDC1F935202591FFCCE5D";
            const string wrongWebResource = "67F8D523AB3EB05F92E32AAA24F0B6F1";
            const string invalidScriptResource = "2B91A0C168E07414714F43AF789AC957";

            var KnownErrors = new[]
                                  {
                                      pageNoExist,
                                      wrongWebResource,
                                      invalidScriptResource
                                  };

            return KnownErrors.Any(ticketTitle.Contains);
        }
    }
}
