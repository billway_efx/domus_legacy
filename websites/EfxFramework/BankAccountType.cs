using Mb2x.Data;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class BankAccountType : BaseEntity
	{
		//Public Properties
		public int BankAccountTypeId { get; set; }
		public string BankAccountTypeName { get; set; }

        public static int Set(int bankAccountTypeId, string bankAccountTypeName)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(EfxSettings.ConnectionString, "dbo.usp_BankAccountType_SetSingleObject",
                new[]
                {
	                new SqlParameter("@BankAccountTypeId", bankAccountTypeId),
	                new SqlParameter("@BankAccountTypeName", bankAccountTypeName)
                });
        }
    
        //TODO: These Set methods have no usage -- remove?
        public static int Set(BankAccountType b)
        {
	        return Set(b.BankAccountTypeId, b.BankAccountTypeName);
        }

        public BankAccountType(int bankAccountTypeId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_BankAccountType_GetSingleObject", new [] { new SqlParameter("@BankAccountTypeId", bankAccountTypeId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        BankAccountTypeId = (int)ResultSet[0];
		        BankAccountTypeName = ResultSet[1] as string;
            }
        }

        public BankAccountType() { }
	}
}
