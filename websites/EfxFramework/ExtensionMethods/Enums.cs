﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace EfxFramework.ExtensionMethods
{
	public static class Enums
	{
		public static string GetFriendlyName(this Enum input)
		{
			//Identify the member of the enum with the specified name
			var MemberInfo = input.GetType().GetMember(input.ToString());

			if (MemberInfo.Length > 0)
			{
				//Identify the "DescriptionAttribute" of the element
				var Attributes = MemberInfo[0].GetCustomAttributes(typeof (DescriptionAttribute), false);

				//Return
				return Attributes.Length > 0 ? ((DescriptionAttribute) Attributes[0]).Description : input.ToString();
			}

			//Return
			return input.ToString();
		}

		public static List<Tuple<int, string, string>> GetIdNameDescription(this Enum input)
		{
			//Create the return value
			var RetVal = new List<Tuple<int, string, string>>();

			//Determine the type
			var CurrentType = input.GetType();

			//Get the names and values
			var ItemNames = Enum.GetNames(CurrentType);
			var ItemValues = Enum.GetValues(CurrentType).Cast<int>().ToArray();

			//Add the items to the Combobox
			for (var CurrentIndex = 0; CurrentIndex < ItemValues.Length; CurrentIndex++)
			{
				//Identify the "DescriptionAttribute" of the element
				var Attributes = CurrentType.GetMember(ItemNames[CurrentIndex])[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

				//Get the "Description" name if possible, otherwise, use the actual StateProvince name
				var ItemText = Attributes.Length > 0 ? ((DescriptionAttribute) Attributes[0]).Description : ItemNames[CurrentIndex];

				//Add the item to the return value
				RetVal.Add(new Tuple<int, string, string>(ItemValues[CurrentIndex], ItemNames[CurrentIndex], ItemText));
			}

			//Return
			return RetVal;
		}
	}
}