﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.UI.WebControls;

namespace EfxFramework.ExtensionMethods
{
    //TODO Clean up
	public static class ComboBoxes
	{
		public static void BindToSortedEnum(this DropDownList l, Type e, bool useDescription = true)
		{
			var ItemNames = Enum.GetNames(e);
			var ItemValues = Enum.GetValues(e).Cast<int>().Select(s => s.ToString(EfxSettings.DefaultCulture)).ToList();
			var ItemDescriptions = new Dictionary<string, string>();

			if (useDescription)
			{
				for (var I = 0; I < ItemNames.Length; I++)
				{
					var MemberInfo = e.GetMember(ItemNames[I]);
					var Attributes = MemberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
					ItemDescriptions.Add((Attributes.Length > 0 ? ((DescriptionAttribute)Attributes[0]).Description : ItemNames[I]), ItemValues[I]);
				}
			}

			foreach (var Kvp in ItemDescriptions.OrderBy(d => d.Key))
			{
				l.Items.Add(new ListItem(Kvp.Key, Kvp.Value));
			}
		}

        public static void BindToSortedValue(this DropDownList l, Type e, bool useDescription = true)
        {
            var ItemNames = Enum.GetNames(e);
            var ItemValues = Enum.GetValues(e).Cast<int>().Select(s => s.ToString(EfxSettings.DefaultCulture)).ToList();
            var ItemDescriptions = new Dictionary<string, string>();

            if (useDescription)
            {
                for (var I = 0; I < ItemNames.Length; I++)
                {
                    var MemberInfo = e.GetMember(ItemNames[I]);
                    var Attributes = MemberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                    ItemDescriptions.Add((Attributes.Length > 0 ? ((DescriptionAttribute)Attributes[0]).Description : ItemNames[I]), ItemValues[I]);
                }
            }

            foreach (var Kvp in ItemDescriptions.OrderBy(d => d.Value))
            {
                l.Items.Add(new ListItem(Kvp.Key, Kvp.Value));
            }
        }
	}
}