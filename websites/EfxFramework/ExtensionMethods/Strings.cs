﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EfxFramework.ExtensionMethods
{
	public static class StringExtensions
	{
		public static string FormatPhoneNumber(this string inputValue)
		{
			var P = inputValue.ExtractNumber().Replace(".", "").Replace("-", "");
			long PLong;
			return long.TryParse(P, out PLong) ? string.Format("{0:###-###-####}", PLong) : string.Empty;
		}

		public static string ExtractNumber(this string input)
		{
			//Ensure that we have a string to work with
			if (string.IsNullOrEmpty(input))
			{
				return string.Empty;
			}

			//Split the string into a char array
			var StringChars = input.ToCharArray();

			//Create the valid list of characters
			var ValidCharList = new List<char>
									{
										'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '-'
									};

			//Return
			return string.Concat(StringChars.Where(ValidCharList.Contains).ToList());
		}

		public static string Right(this string input, int numberOfCharacters)
		{
			//If the input is null or the numberOfCharacters matches or exceeds the string length, return the original string
			if (input == null || numberOfCharacters >= input.Length)
			{
				return input;
			}

			//If the number of characters specified is less than 0, return an empty string otherwise return the substring
			return numberOfCharacters < 0 ? string.Empty : input.Substring(input.Length - numberOfCharacters, numberOfCharacters);
		}

		public static string ToNthValue(this string input)
		{
// ReSharper disable RedundantAssignment
			var Value = 0;
// ReSharper restore RedundantAssignment

			if (Int32.TryParse(input, out Value))
			{
				if (input.EndsWith("1") && !input.EndsWith("11"))
					return String.Format("{0}st", input);
				if (input.EndsWith("2") && !input.EndsWith("12"))
					return String.Format("{0}nd", input);
				if (input.EndsWith("3") && !input.EndsWith("13"))
					return String.Format("{0}rd", input);

				return String.Format("{0}th", input);
			}

			return String.Empty;
		}

		public static string HtmlizeCrLf(this string input)
		{
			//If the input is NULL, return NULL
			if (input == null)
			{
				return null;
			}

			//Strip all CrLf from the input
			var RetVal = input.Replace("\r\n", "<br />");

			//Strip all Cr by itself
			RetVal = RetVal.Replace("\r", "<br />");

			//Strip all Lf by itself
			RetVal = RetVal.Replace("\n", "<br />");

			//Return
			return RetVal;
		}

        public static string MaskAccountNumber(this string input)
        {
            var Result = new StringBuilder();

            if (input == null)
                return Result.ToString();

            if (input.Length > 4)
            {
                for (var I = 0; I < input.Length - 4; I++)
                {
                    Result.Append("*");
                }

                Result.Append(input.Substring(input.Length - 4));
            }
            else
                Result.Append(input);

            return Result.ToString();
        }

        public static string TruncateSentence(this string input, int length)
        {
            if (input == null || input.Length < length)
                return input;
            var Sentence = input.LastIndexOf(".", length, StringComparison.Ordinal);
            return string.Format("{0}.", input.Substring(0, (Sentence > 0) ? Sentence : length).Trim());
        }
	}
}