﻿using System.Xml;
using System.Xml.Linq;

namespace EfxFramework.ExtensionMethods
{
    public static class Xml
    {
        public static XElement GetXElement(this XmlNode node)
        {
            var Xdoc = new XDocument();

            using (var Xwriter = Xdoc.CreateWriter())
            {
                node.WriteTo(Xwriter);
                return Xdoc.Root;
            }
        }

        public static XmlNode GetXmlNode(this XElement element)
        {
            using (var XReader = element.CreateReader())
            {
                var Xdoc = new XmlDocument();
                Xdoc.Load(XReader);
                return Xdoc;
            }
        }
    }
}
