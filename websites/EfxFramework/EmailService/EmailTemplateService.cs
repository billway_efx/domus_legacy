﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Configuration;

namespace EfxFramework.EmailTemplateService
{
	public class EmailTemplateService
	{
		public static string GetEmailHtml(string applicationPath)
		{
			string text = string.Empty;
            string serverPath = string.Empty;

            //Salcedo - 3/12/2014 - it was necessary to check for null because this function is sometimes referenced from a console app (non-http) context
            if (HttpContext.Current != null)
            {
                serverPath = HttpContext.Current.Server.MapPath(applicationPath);
            }
            else
            {
                //Salcedo - task 337 - 1/13/2015
                //String HtmlEmailImageFilePath = ConfigurationManager.AppSettings["HtmlEmailImageFilePath"];
                String HtmlEmailImageFilePath = EfxSettings.HtmlEmailImageFilePath;
                serverPath = applicationPath.Replace("~", HtmlEmailImageFilePath);

                if (!File.Exists(serverPath))
                {
                    //If we haven't found the file so far, try looking for it in the current application directory
                    serverPath = applicationPath.Replace("~", AppDomain.CurrentDomain.BaseDirectory);
                    
                    if (!File.Exists(serverPath))
                        return "";
                }
            }
			using (StreamReader reader = new StreamReader(serverPath))
			{
				text = reader.ReadToEnd();
				reader.Close();
			}
			return text;
		}

        public static string RootPath
        {
            get 
            {
                //Salcedo - 3/12/2014 - it was necessary to check for null because this property is sometimes referenced from a console app (non-http) context
                if (HttpContext.Current != null)
                {
                    return HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/";
                }
                else
                {
                    //Salcedo - task 337 - 1/13/2015
                    //String HtmlEmailImageWebSite = ConfigurationManager.AppSettings["HtmlEmailImageWebSite"];
                    //return HtmlEmailImageWebSite;
                    return EfxSettings.HtmlEmailImageWebSite;
                }
            }
        }
	}
}
