﻿using EfxFramework.ExtensionMethods;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace EfxFramework
{
    public enum RoleType
    {
        [Description("Applicant")]
        Applicant = 1,
        [Description("EFX Administrator")]
        EfxAdministrator = 2,
        [Description("Property Staff")]
        PropertyStaff = 3,
        [Description("Resident")]
        Resident = 4,
        [Description("Customer Service Staff")]
        CustomerServiceStaff = 5
    }

    public enum RoleName
    {
        [Description("Property Manager")]
        PropertyManager = 1,
        [Description("Leasing Agent")]
        LeasingAgent = 2,
        [Description("Corporate Admin")]
        CorporateAdmin = 3,
        [Description("Accountant")]
        Accountant = 4,
        [Description("EFX Power Administrator")]
        EfxPowerAdministrator = 5,
        [Description("EFX Sales")]
        EfxSales = 6,
        [Description("EFX Support/Customer Service")]
        EfxCustomerService = 7
    }

    public class RoleManager
    {
        public static bool IsUserInRole(RoleType roleType, int userId, RoleName authorizedRole)
        {
            var Roles = new List<Role>();

            switch (roleType)
            {
                case RoleType.EfxAdministrator:
                    Roles = GetAdminRoles(userId);
                    break;

                case RoleType.PropertyStaff:
                    Roles = GetStaffRoles(userId);
                    break;
            }

            return IsUserInRole(Roles, authorizedRole);
        }

        public static bool IsUserInRole(RoleType roleType, string userName, RoleName authorizedRole)
        {
            var Roles = new List<Role>();

            switch (roleType)
            {
                case RoleType.EfxAdministrator:
                    Roles = GetAdminRoles(userName);
                    break;

                case RoleType.PropertyStaff:
                    Roles = GetStaffRoles(userName);
                    break;
            }

            return IsUserInRole(Roles, authorizedRole);
        }

        public static bool IsUserInRole(RoleType roleType, int userId, List<RoleName> authorizedRoles)
        {
            var Roles = new List<Role>();

            switch (roleType)
            {
                case RoleType.EfxAdministrator:
                    Roles = GetAdminRoles(userId);
                    break;

                case RoleType.PropertyStaff:
                    Roles = GetStaffRoles(userId);
                    break;
                case RoleType.CustomerServiceStaff:
                    Roles = GetAdminRoles(userId);
                    break;
            }

            //bool Test = IsUserInRole(Roles, authorizedRoles);
            return IsUserInRole(Roles, authorizedRoles);
        }

        public static bool IsUserInRole(RoleType roleType, string userName, List<RoleName> authorizedRoles)
        {
            var Roles = new List<Role>();

            switch (roleType)
            {
                case RoleType.EfxAdministrator:
                    Roles = GetAdminRoles(userName);
                    break;

                case RoleType.PropertyStaff:
                    Roles = GetStaffRoles(userName);
                    break;
                case RoleType.CustomerServiceStaff:
                    Roles = GetAdminRoles(userName);
                    break;
            }

            return IsUserInRole(Roles, authorizedRoles);
        }

        private static bool IsUserInRole(IEnumerable<Role> userRoles, RoleName authorizedRole)
        {
            return userRoles.Any(r => r.RoleName == authorizedRole.GetFriendlyName());
        }

        private static bool IsUserInRole(IEnumerable<Role> userRoles, IEnumerable<RoleName> authorizedRoles)
        {
            //bool Test = authorizedRoles.Any(ar => userRoles.Any(ur => ur.RoleName == ar.GetFriendlyName()));
            return authorizedRoles.Any(ar => userRoles.Any(ur => ur.RoleName == ar.GetFriendlyName()));
        }

        private static List<Role> GetStaffRoles(int userId)
        {
            var Staff = new PropertyStaff(userId);
            return Staff.PropertyStaffId.HasValue && Staff.PropertyStaffId.Value > 0 ? Role.GetRolesByStaffId(Staff.PropertyStaffId.Value).Where(r => r.RoleId != (int)RoleName.Accountant).ToList() : new List<Role>();
        }

        private static List<Role> GetStaffRoles(string userName)
        {
            var Staff = new PropertyStaff(userName);
			return Staff.PropertyStaffId.HasValue && Staff.PropertyStaffId.Value > 0 ? Role.GetRolesByStaffId(Staff.PropertyStaffId.Value).Where(r => r.RoleId != (int)RoleName.Accountant).ToList() : new List<Role>();
        }

        private static List<Role> GetAdminRoles(int userId)
        {
            var Admin = new EfxAdministrator(userId);
            return Admin.EfxAdministratorId > 0 ? Role.GetRolesByEfxAdministratorId(Admin.EfxAdministratorId) : new List<Role>();
        }

        private static List<Role> GetAdminRoles(string userName)
        {
            var Admin = new EfxAdministrator(userName);
            return Admin.EfxAdministratorId > 0 ? Role.GetRolesByEfxAdministratorId(Admin.EfxAdministratorId) : new List<Role>();
        }
    }
}
