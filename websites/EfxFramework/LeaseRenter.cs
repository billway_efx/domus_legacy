﻿namespace EfxFramework
{
    public class LeaseRenter
    {
        public Lease LeaseObject { get; set; }
        public Renter RenterObject { get; set; }

        public LeaseRenter()
        {
        }

        public LeaseRenter(Lease lease, Renter renter)
        {
            LeaseObject = lease;
            RenterObject = renter;
        }
    }
}
