using System.Web;
using EfxFramework.Web.Caching;
using Mb2x.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;

namespace EfxFramework
{
	public class Wallet_PaymentHistoryItem : BaseEntity
	{
        [DisplayName(@"PaymentDate")]
        public DateTime PaymentDate { get; set; }

        [DisplayName(@"PaymentItemDescription")]
        public string PaymentItemDescription { get; set; }

        [DisplayName(@"PaymountAmount")]
        public decimal PaymountAmount { get; set; }

        //In this context, will typically have 'Credit' or 'eCheck', as returned from database
        [DisplayName(@"PaymentMethod")]
        public string PaymentMethod { get; set; }

        [DisplayName(@"PaymentMethodDescription")]
        public string PaymentMethodDescription { get; set; }

        public Wallet_PaymentHistoryItem()
		{
			InitializeObject();
		}
	}
}
