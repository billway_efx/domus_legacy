using System;
using System.Data.SqlClient;
using Mb2x.Data;
using System.Collections.Generic;
using EfxFramework.Security.Authentication;

namespace EfxFramework
{
	public class PaymentQueue : BaseEntity
	{
		//Public Properties
		public int PaymentQueueId { get; set; }
		public string PaymentQueueNumber { get; set; }
		public int RenterId { get; set; }
		public int PaymentTypeId { get; set; }
		public string BankAccountNumber { get; set; }
		public string BankRoutingNumber { get; set; }
		public string CreditCardHolderName { get; set; }
		public string CreditCardAccountNumber { get; set; }
        public int? CreditCardExpirationMonth { get; set; }
        public int? CreditCardExpirationYear { get; set; }
		public decimal? RentAmount { get; set; }
		public string RentAmountDescription { get; set; }
		public decimal? CharityAmount { get; set; }
		public string CharityName { get; set; }
		public decimal? OtherAmount1 { get; set; }
		public string OtherDescription1 { get; set; }
		public decimal? OtherAmount2 { get; set; }
		public string OtherDescription2 { get; set; }
		public decimal? OtherAmount3 { get; set; }
		public string OtherDescription3 { get; set; }
		public decimal? OtherAmount4 { get; set; }
		public string OtherDescription4 { get; set; }
		public decimal? OtherAmount5 { get; set; }
		public string OtherDescription5 { get; set; }
		public DateTime TransactionDateTime { get; set; }
        public string CreditCardSecurityCode { get; set; }
        public Boolean IsProcessed { get; set; }

        public static PaymentQueue GetPaymentQueueByPaymentQueueNumber(string paymentQueueNumber)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new PaymentQueue(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_PaymentQueue_GetPaymentQueueByPaymentQueueNumber", new SqlParameter("@PaymentQueueNumber", paymentQueueNumber)));
        }

        //Salcedo - 3/28/2014 - return the most recent PaymentQueue record for a given RenterId
        public static PaymentQueue GetPaymentQueueByRenterId(string RenterId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new PaymentQueue(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_PaymentQueue_GetPaymentQueueByRenterId",
                new SqlParameter("@RenterId", RenterId)));
        }

        private static Dictionary<string, string> CreatePaymentQueueNumber(int paymentQueueId, string paymentQueueNumber)
        {
            var Results = new Dictionary<string, string>();
            string PaymentQueueNumber = null;

            if (paymentQueueId > 0)
            {
                var PaymentQueue = new PaymentQueue(paymentQueueId);
                PaymentQueueNumber = PaymentQueue.PaymentQueueNumber;
            }

            if (string.IsNullOrEmpty(paymentQueueNumber))
            {
                if (string.IsNullOrEmpty(PaymentQueueNumber))
                    PaymentQueueNumber = AuthenticationTools.GenerateRandomNumberString(10, true);
            }
            else
                PaymentQueueNumber = paymentQueueNumber;

            Results.Add("PaymentQueueNumber", PaymentQueueNumber);

            return Results;
        }

        public static int Set(int paymentQueueId, string paymentQueueNumber, int renterId, int paymentTypeId, string bankAccountNumber, 
            string bankRoutingNumber, string creditCardHolderName, string creditCardAccountNumber, decimal? rentAmount, 
            string rentAmountDescription, decimal? charityAmount, string charityName, decimal? otherAmount1, string otherDescription1, 
            decimal? otherAmount2, string otherDescription2, decimal? otherAmount3, string otherDescription3, decimal? otherAmount4, 
            string otherDescription4, decimal? otherAmount5, string otherDescription5, DateTime transactionDateTime, 
            int? creditCardExpirationMonth, int? creditCardExpirationYear, string creditCardSecurityCode, Boolean IsProcessed)
        {
            var GeneratedCodes = CreatePaymentQueueNumber(paymentQueueId, paymentQueueNumber);

            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_PaymentQueue_SetSingleObject",
                new[]
                {
	                new SqlParameter("@PaymentQueueId", paymentQueueId),
	                new SqlParameter("@PaymentQueueNumber", GeneratedCodes["PaymentQueueNumber"]),
	                new SqlParameter("@RenterId", renterId),
	                new SqlParameter("@PaymentTypeId", paymentTypeId),
	                new SqlParameter("@BankAccountNumber", bankAccountNumber),
	                new SqlParameter("@BankRoutingNumber", bankRoutingNumber),
	                new SqlParameter("@CreditCardHolderName", creditCardHolderName),
	                new SqlParameter("@CreditCardAccountNumber", creditCardAccountNumber),
	                new SqlParameter("@RentAmount", rentAmount),
	                new SqlParameter("@RentAmountDescription", rentAmountDescription),
	                new SqlParameter("@CharityAmount", charityAmount),
	                new SqlParameter("@CharityName", charityName),
	                new SqlParameter("@OtherAmount1", otherAmount1),
	                new SqlParameter("@OtherDescription1", otherDescription1),
	                new SqlParameter("@OtherAmount2", otherAmount2),
	                new SqlParameter("@OtherDescription2", otherDescription2),
	                new SqlParameter("@OtherAmount3", otherAmount3),
	                new SqlParameter("@OtherDescription3", otherDescription3),
	                new SqlParameter("@OtherAmount4", otherAmount4),
	                new SqlParameter("@OtherDescription4", otherDescription4),
	                new SqlParameter("@OtherAmount5", otherAmount5),
	                new SqlParameter("@OtherDescription5", otherDescription5),
	                new SqlParameter("@TransactionDateTime", transactionDateTime),
                    new SqlParameter("@CreditCardExpirationMonth", creditCardExpirationMonth),
                    new SqlParameter("@CreditCardExpirationYear", creditCardExpirationYear),
                    new SqlParameter("@CreditCardSecurityCode", creditCardSecurityCode),
                    new SqlParameter("@IsProcessed", IsProcessed)
                });
        }

        public static int Set(PaymentQueue p)
        {
	        return Set(
		        p.PaymentQueueId,
                p.PaymentQueueNumber,
		        p.RenterId,
		        p.PaymentTypeId,
		        p.BankAccountNumber,
		        p.BankRoutingNumber,
		        p.CreditCardHolderName,
		        p.CreditCardAccountNumber,
		        p.RentAmount,
		        p.RentAmountDescription,
		        p.CharityAmount,
		        p.CharityName,
		        p.OtherAmount1,
		        p.OtherDescription1,
		        p.OtherAmount2,
		        p.OtherDescription2,
		        p.OtherAmount3,
		        p.OtherDescription3,
		        p.OtherAmount4,
		        p.OtherDescription4,
		        p.OtherAmount5,
		        p.OtherDescription5,
		        p.TransactionDateTime,
                p.CreditCardExpirationMonth,
                p.CreditCardExpirationYear,
                p.CreditCardSecurityCode,
                p.IsProcessed 
	        );
        }

        public PaymentQueue(int? paymentQueueId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_PaymentQueue_GetSingleObject", new [] { new SqlParameter("@PaymentQueueId", paymentQueueId) });

            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
            //Otherwise, populate this object
		        PaymentQueueId = (int)ResultSet[0];
		        PaymentQueueNumber = ResultSet[1] as string;
		        RenterId = (int)ResultSet[2];
		        PaymentTypeId = (int)ResultSet[3];
		        BankAccountNumber = ResultSet[4] as string;
		        BankRoutingNumber = ResultSet[5] as string;
		        CreditCardHolderName = ResultSet[6] as string;
		        CreditCardAccountNumber = ResultSet[7] as string;
		        RentAmount = ResultSet[8] as decimal?;
		        RentAmountDescription = ResultSet[9] as string;
		        CharityAmount = ResultSet[10] as decimal?;
		        CharityName = ResultSet[11] as string;
		        OtherAmount1 = ResultSet[12] as decimal?;
		        OtherDescription1 = ResultSet[13] as string;
		        OtherAmount2 = ResultSet[14] as decimal?;
		        OtherDescription2 = ResultSet[15] as string;
		        OtherAmount3 = ResultSet[16] as decimal?;
		        OtherDescription3 = ResultSet[17] as string;
		        OtherAmount4 = ResultSet[18] as decimal?;
		        OtherDescription4 = ResultSet[19] as string;
		        OtherAmount5 = ResultSet[20] as decimal?;
		        OtherDescription5 = ResultSet[21] as string;
                TransactionDateTime = (DateTime) ResultSet[22];
                CreditCardExpirationMonth = ResultSet[23] as int?;
                CreditCardExpirationYear = ResultSet[24] as int?;
                CreditCardSecurityCode = ResultSet[25] as string;
                IsProcessed = (Boolean)ResultSet[26];
            }
        }

	    public PaymentQueue()
	    {
	    }
	}
}
