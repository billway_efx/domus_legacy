﻿using System.Web.Security;
using EfxFramework.ExtensionMethods;

namespace EfxFramework.Role
{
    public class EfxRoleProvider : RoleProvider
    {
        public override bool IsUserInRole(string username, string roleName)
        {
            //Attempt to lookup the system user based on the username
            var CurrentSystemUser = SystemUser.GetByUserName(username);

            //Compare the roles
            return string.CompareOrdinal(CurrentSystemUser.SystemRoleId.GetFriendlyName(), roleName) == 0;
        }

        public override string[] GetRolesForUser(string username)
        {
            return null;
        }

        public override void CreateRole(string roleName)
        {

        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            return false;
        }

        public override bool RoleExists(string roleName)
        {
            return true;
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {

        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {

        }

        public override string[] GetUsersInRole(string roleName)
        {
            return null;
        }

        public override string[] GetAllRoles()
        {
            return null;
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            return null;
        }

        public override string ApplicationName
        {
            get { return Settings.ApplicationName; }
            set { }
        }
    }
}
