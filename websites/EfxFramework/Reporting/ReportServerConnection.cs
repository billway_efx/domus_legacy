﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Principal;
using System.Net;
using Microsoft.Reporting.WebForms;

namespace EfxFramework.Reporting
{
    public sealed class ReportServerConnection : IReportServerConnection2
    {
        public WindowsIdentity ImpersonationUser
        {
            get
            {
                // Use the default Windows user.  Credentials will be
                // provided by the NetworkCredentials property.
                return null;
            }
        }

        public ICredentials NetworkCredentials
        {
            get
            {
                // Read the user information from the web.config file.  
                // By reading the information on demand instead of 
                // storing it, the credentials will not be stored in 
                // session, reducing the vulnerable surface area to the
                // web.config file, which can be secured with an ACL.

                // User name
                string UserName = ConfigurationManager.AppSettings["ReportServicesUserName"];

                if (string.IsNullOrEmpty(UserName))
                    throw new Exception("Missing reporting services user name from Web.config file");

                // Password
                string Password = ConfigurationManager.AppSettings["ReportServicesPassword"];

                if (string.IsNullOrEmpty(Password))
                    throw new Exception("Missing reporting services password from Web.config file");

                // Domain
                string Domain = ConfigurationManager.AppSettings["ReportServicesDomain"];

                if (string.IsNullOrEmpty(Domain))
                    throw new Exception("Missing reporting services domain from Web.config file");

                return new NetworkCredential(UserName, Password, Domain);
            }
        }

        public bool GetFormsCredentials(out Cookie authCookie, out string userName, out string password, out string authority)
        {
            authCookie = null;
            userName = null;
            password = null;
            authority = null;

            // Not using form credentials
            return false;
        }

        public Uri ReportServerUrl
        {
            get
            {
                string Url =
                    ConfigurationManager.AppSettings["ReportServicesServer"];

                if (string.IsNullOrEmpty(Url))
                    throw new Exception("Missing reporting services url from the Web.config file");

                return new Uri(Url);
            }
        }

        public int Timeout
        {
            get
            {
                return 60000; // 60 seconds
            }
        }

        public IEnumerable<Cookie> Cookies
        {
            get
            {
                // No custom cookies
                return null;
            }
        }

        public IEnumerable<string> Headers
        {
            get
            {
                // No custom headers
                return null;
            }
        }

    }
}
