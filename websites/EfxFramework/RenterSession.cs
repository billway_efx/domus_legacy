using System;
using System.Data.SqlClient;
using Mb2x.Data;

namespace EfxFramework
{
	public class RenterSession : BaseEntity
	{
		//Public Properties
		public int RenterSessionId { get; set; }
		public int RenterId { get; set; }
		public Guid SessionGuid { get; set; }
		public DateTime ExpirationDate { get; set; }

        public static bool RefreshSessionBySessionGuid(Guid sessionGuid, bool endSession = false)
        {
            var Session = GetRenterSessionBySessionGuid(sessionGuid);

            if (Session.ExpirationDate > DateTime.UtcNow)
            {
                Session.ExpirationDate = !endSession ? DateTime.UtcNow.AddMinutes(10) : DateTime.UtcNow;
                Set(Session);

                return true;
            }

            return false;
        }

        public static RenterSession SaveRenterSessionByRenterId(int renterId)
        {
            var Session = new RenterSession { RenterId = renterId, SessionGuid = Guid.NewGuid(), ExpirationDate = DateTime.UtcNow.AddMinutes(10) };
            Session.RenterSessionId = Set(Session);

            return Session;
        }

        public static RenterSession GetRenterSessionBySessionGuid(Guid sessionGuid)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new RenterSession(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_RenterSession_GetBySessionGuid", new[] { new SqlParameter("@SessionGuid", sessionGuid) }));
        }

	    public static int Set(int renterSessionId, int renterId, Guid sessionGuid, DateTime expirationDate)
        {
            try
            {
                //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
                return NewDataAccess.ExecuteScalar<int>(EfxSettings.ConnectionString, "dbo.usp_RenterSession_SetSingleObject",
                    new[]
                {
	                new SqlParameter("@RenterSessionId", renterSessionId),
	                new SqlParameter("@RenterId", renterId),
	                new SqlParameter("@SessionGuid", sessionGuid),
	                new SqlParameter("@ExpirationDate", expirationDate)
                });
            }
            catch (SqlException E)
            {
                return E.Number == 2601 ? Set(renterSessionId, renterId, Guid.NewGuid(), expirationDate) : 0;
            }
        }

        public static int Set(RenterSession r)
        {
	        return Set(
		        r.RenterSessionId,
		        r.RenterId,
		        r.SessionGuid,
		        r.ExpirationDate
	        );
        }

        public RenterSession(int? renterSessionId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_RenterSession_GetSingleObject", new [] { new SqlParameter("@RenterSessionId", renterSessionId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        RenterSessionId = (int)ResultSet[0];
		        RenterId = (int)ResultSet[1];
		        SessionGuid = (Guid)ResultSet[2];
		        ExpirationDate = (DateTime)ResultSet[3];
            }
        }

        public RenterSession()
        {
            InitializeObject();
        }
	}
}
