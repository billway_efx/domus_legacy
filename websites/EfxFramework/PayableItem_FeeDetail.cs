using System.Web;
using EfxFramework.Web.Caching;
using Mb2x.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;

namespace EfxFramework
{
	public class PayableItem_FeeDetail : BaseEntity
	{
		//Public Properties
        [DisplayName(@"RenterID")]
        public int RenterID { get; set; }

        [DisplayName(@"MonthlyFeeId")]
        public int MonthlyFeeId { get; set; }

        [DisplayName(@"FeeAmount")]
        public decimal FeeAmount { get; set; }

        [DisplayName(@"FeeName")]
        public string FeeName { get; set; }

        [DisplayName(@"ChargeCode")]
        public string ChargeCode { get; set; }

        public PayableItem_FeeDetail()
		{
			InitializeObject();
		}
	}
}
