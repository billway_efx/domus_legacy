﻿using System.ComponentModel;

namespace EfxFramework
{
    public enum ApplicationSubmissionType
    {
        [Description("Online")]
        Online = 1,
        [Description("Mail")]
        Mail = 2,
        [Description("Email")]
        Email = 3,
        [Description("Fax")]
        Fax = 4,
        [Description("In Person")]
        InPerson = 5
    }

    public enum PetType
    {
        [Description("Dog")]
        Dog = 1,
        [Description("Cat")]
        Cat = 2,
        [Description("Other")]
        Other = 3
    }

    public enum TypeOfAddress
    {
        Primary = 1,
        Billing = 2
    }

    public enum SessionVariable
    {
        ImageBytes
    }

    public enum AnnouncementType
    {
        Property = 1,
        Admin = 2,
        ProsCorner = 3,
        ResidentsRoom = 4,
        News = 5,
        Events = 6
    }

    public enum MaintenanceRequestPriority
    {
        Low = 1,
        Medium = 2,
        High = 3
    }

    public enum MaintenanceRequestType
    {
        [Description("Broken/Damage")]
        BrokenDamage = 1,
        [Description("Not Working Properly")]
        NotWorkingProperly = 2,
        [Description("General Maintenance")]
        GeneralMaintenance = 3,
        [Description("Emergency")]
        Emergency = 4
    }

    public enum RenterPaymentType
    {
        Credit = 1,
        ECheck = 2,
        Text = 3
    }

    public enum PmsType
    {
        [Description("YARDI")]
        Yardi = 1,
        [Description("AMSI")]
        Amsi = 2,
        [Description("Real Page")]
        RealPage = 3,
        [Description("MRI")]
        MRI = 4
    }

    public enum UserType
    {
        EfxAdministrator,
        PropertyStaff,
        Renter,
        Applicant
    }

    //TODO: Modify with new payment channels after refactor
    public enum PaymentChannel
    {
        Web = 1,
        Mobile = 2, 
        Text = 3,
        Ivr = 4,
        Api = 5,
        PublicSite = 6,
        ResidentSite = 7,
        PropertyManagerSite = 8,
        AdminSite = 9,
        ResidentApp = 10,
        PropertyManagerApp = 11,
        AutoPayment = 12,
        TelePhone = 13
    }

    public enum LeadType
    {
        PropertyManager = 1,
        Resident = 2
    }

    public enum ApplicationStatus
    {
        Approved = 1,
        Denied = 2,
        Pending = 3
    }

    public enum Carrier
    {
        [Description("Other")]
        Other = 0,
        [Description("AT&T")]
        Att = 1,
        [Description("Boost")]
        Boost = 2,
        [Description("Sprint")]
        Sprint = 3,
// ReSharper disable InconsistentNaming
        [Description("T-Mobile")]
        TMobile = 4,
// ReSharper restore InconsistentNaming
        [Description("US Cellular")]
        UsCellular = 5,
        [Description("Verizon")]
        Verizon = 6,
        [Description("Virgin Mobile")]
        Virgin = 7
    }

    public enum SiteNavigationGroup
    {
        Property, Renters
    }

    public enum SystemMessageType
    {
        Information, Error, Success, NotSet
    }

    public enum StateProvince
    {
// ReSharper disable InconsistentNaming
        [Description("Alaska")]
        AK = 1,
        [Description("Alabama")]
        AL = 2,
        [Description("Arkansas")]
        AR = 3,
        [Description("Arizona")]
        AZ = 4,
        [Description("California")]
        CA = 5,
        [Description("Colorado")]
        CO = 6,
        [Description("Connecticut")]
        CT = 7,
        [Description("Delaware")]
        DE = 8,
        [Description("Florida")]
        FL = 9,
        [Description("Georgia")]
        GA = 10,
        [Description("Hawaii")]
        HI = 11,
        [Description("Iowa")]
        IA = 12,
        [Description("Idaho")]
        ID = 13,
        [Description("Illinois")]
        IL = 14,
        [Description("Indiana")]
        IN = 15,
        [Description("Kansas")]
        KS = 16,
        [Description("Kentucky")]
        KY = 17,
        [Description("Louisiana")]
        LA = 18,
        [Description("Massachusetts")]
        MA = 19,
        [Description("Maryland")]
        MD = 20,
        [Description("Maine")]
        ME = 21,
        [Description("Michigan")]
        MI = 22,
        [Description("Minnesota")]
        MN = 23,
        [Description("Missouri")]
        MO = 24,
        [Description("Mississippi")]
        MS = 25,
        [Description("Montana")]
        MT = 26,
        [Description("North Carolina")]
        NC = 27,
        [Description("North Dakota")]
        ND = 28,
        [Description("Nebraska")]
        NE = 29,
        [Description("New Hampshire")]
        NH = 30,
        [Description("New Jersey")]
        NJ = 31,
        [Description("New Mexico")]
        NM = 32,
        [Description("Nevada")]
        NV = 33,
        [Description("New York")]
        NY = 34,
        [Description("Ohio")]
        OH = 35,
        [Description("Oklahoma")]
        OK = 36,
        [Description("Oregon")]
        OR = 37,
        [Description("Pennsylvania")]
        PA = 38,
        [Description("Rhode Island")]
        RI = 39,
        [Description("South Carolina")]
        SC = 40,
        [Description("South Dakota")]
        SD = 41,
        [Description("Tennessee")]
        TN = 42,
        [Description("Texas")]
        TX = 43,
        [Description("Utah")]
        UT = 44,
        [Description("Virginia")]
        VA = 45,
        [Description("Vermont")]
        VT = 46,
        [Description("Washington")]
        WA = 47,
        [Description("Wisconsin")]
        WI = 48,
        [Description("West Virginia")]
        WV = 49,
        [Description("Wyoming")]
        WY = 50,
        [Description("District of Columbia")]
        DC = 51
// ReSharper restore InconsistentNaming
    }

    public enum CapitalCity
    {
        [Description("Juneau")]
        Juneau = 1,
        [Description("Montgomery")]
        Montgomery = 2,
        [Description("Little Rock")]
        LittleRock = 3,
        [Description("Phoenix")]
        Phoenix = 4,
        [Description("Sacramento")]
        Sacramento = 5,
        [Description("Denver")]
        Denver = 6,
        [Description("Hartford")]
        Hartford = 7,
        [Description("Dover")]
        Dover = 8,
        [Description("Tallahassee")]
        Tallahassee = 9,
        [Description("Atlanta")]
        Atlanta = 10,
        [Description("Honolulu")]
        Honolulu = 11,
        [Description("Des Moines")]
        DesMoines = 12,
        [Description("Boise")]
        Boise = 13,
        [Description("Springfield")]
        Springfield = 14,
        [Description("Indianapolis")]
        Indianapolis = 15,
        [Description("Topeka")]
        Topeka = 16,
        [Description("Frankfurt")]
        Frankfurt = 17,
        [Description("Baton Rouge")]
        BatonRouge = 18,
        [Description("Boston")]
        Boston = 19,
        [Description("Annapolis")]
        Annapolis = 20,
        [Description("Augusta")]
        Augusta = 21,
        [Description("Lansing")]
        Lansing = 22,
        [Description("Saint Paul")]
        SaintPaul = 23,
        [Description("Jefferson City")]
        JeffersonCity = 24,
        [Description("Jackson")]
        Jackson = 25,
        [Description("Helena")]
        Helena = 26,
        [Description("Raleigh")]
        Raleigh = 27,
        [Description("Bismarck")]
        Bismarck = 28,
        [Description("Lincoln")]
        Lincoln = 29,
        [Description("Concord")]
        Concord = 30,
        [Description("Trenton")]
        Trenton = 31,
        [Description("Santa Fe")]
        SantaFe = 32,
        [Description("Carson City")]
        CarsonCity = 33,
        [Description("Albany")]
        Albany = 34,
        [Description("Columbus")]
        Columbus = 35,
        [Description("Oklahoma City")]
        OklahomaCity = 36,
        [Description("Salem")]
        Salem = 37,
        [Description("Harrisburg")]
        Harrisburg = 38,
        [Description("Providence")]
        Providence = 39,
        [Description("Columbia")]
        Columbia = 40,
        [Description("Pierre")]
        Pierre = 41,
        [Description("Nashville")]
        TN = 42,
        [Description("Austin")]
        TX = 43,
        [Description("Salt Lake City")]
        SaltLakeCity = 44,
        [Description("Richmond")]
        Richmond = 45,
        [Description("Montpelier")]
        Montpelier = 46,
        [Description("Olympia")]
        Olympia = 47,
        [Description("Madison")]
        Madison = 48,
        [Description("Charleston")]
        Charleston = 49,
        [Description("Cheyenne")]
        Cheyenne = 50,
        [Description("District of Columbia")]
        DistrictOfColumbia = 51
    }
}
