﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using RPO;
using EfxFramework;

namespace EfxFramework
{
    public class MiscDatabase
    {
        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

        //Salcedo - 2/11/2015 - task # 00366 - added ability to use/not use native maintenance request system
        public static SqlDataReader GetPropertyMaintenanceSystemForRenter(int _RenterId)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_PropertyMaintenanceSystem_GetAll_ForRenterId", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@RenterId", SqlDbType.Int).Value = _RenterId;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                //con.Close();
                throw ex;
            }

        }

        //Salcedo - 2/11/2015 - task # 00366 - added ability to use/not use native maintenance request system
        public static SqlDataReader GetPropertyMaintenanceSystemForProperty(int _PropertyId)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_PropertyMaintenanceSystem_GetAll_ForPropertyId", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropertyId", SqlDbType.Int).Value = _PropertyId;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                //con.Close();
                throw ex;
            }

        }

        public static int PropertyMaintenanceSystem_SetSingleObject(int PropertyId, bool UseNativeMaintenanceSystem, bool UseExternalMaintenanceSystem,
            string ExternalMaintenanceSiteUrl)
        {
            return NewDataAccess.ExecuteScalar<int>(
                EfxSettings.ConnectionString,
                "dbo.usp_PropertyMaintenanceSystem_SetSingleObject",
            new[]
                {
                    new SqlParameter("@PropertyId", PropertyId),
                    new SqlParameter("@UseNativeMaintenanceSystem", UseNativeMaintenanceSystem),
                    new SqlParameter("@UseExternalMaintenanceSystem", UseExternalMaintenanceSystem),
                    new SqlParameter("@ExternalMaintenanceSiteUrl", ExternalMaintenanceSiteUrl)
                });
        }

        //Salcedo - added this function to support changes made in conjunction with task # 000138
        public static SqlDataReader GetReader(string SqlStatement)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand(SqlStatement, con);
            com.CommandType = CommandType.Text;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                //con.Close();
                throw ex;
            }

        }

        //Salcedo - 10/30/2015 - Moved this function here from NewDataAccess.cs
        public static void UpdateVoidStatusForPayment(int PaymentID, int PaymentStatusID)
        {
            SqlConnection con = new SqlConnection(EfxSettings.ConnectionString);
            SqlCommand com = new SqlCommand("Payment_UpdatePaymentStatus", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@PaymentID", SqlDbType.Int).Value = PaymentID;
            com.Parameters.Add("@PaymentStatusID", SqlDbType.Int).Value = PaymentStatusID;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }
        }
    }
}
