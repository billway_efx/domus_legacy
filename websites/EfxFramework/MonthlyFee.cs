using System;
using Mb2x.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;

namespace EfxFramework
{
	public class MonthlyFee : BaseEntity
	{
		//Public Properties
		[DisplayName(@"MonthlyFeeId"), Required(ErrorMessage = "MonthlyFeeId is required.")]
		public int MonthlyFeeId { get; set; }

		[DisplayName(@"PropertyId"), Required(ErrorMessage = "PropertyId is required.")]
		public int PropertyId { get; set; }

		[DisplayName(@"FeeName"), Required(ErrorMessage = "FeeName is required."), StringLength(100)]
		public string FeeName { get; set; }

		[DisplayName(@"DefaultFeeAmount"), Required(ErrorMessage = "DefaultFeeAmount is required.")]
		public decimal DefaultFeeAmount { get; set; }

        public string ChargeCode { get; set; }
        public bool IsActive { get; set; }

        public string FeeAmountText { get { return DefaultFeeAmount.ToString("C"); } }
        public string DeleteMonthlyFeeUrl { get { return String.Format("~/Handlers/GeneralHandler.ashx?RequestType=12&MonthlyFeeId={0}#RentAndFeesTab", MonthlyFeeId); } }

        public static List<MonthlyFee> GetAllMonthlyFees()
        {
            return DataAccess.GetTypedList<MonthlyFee>(EfxSettings.ConnectionString, "dbo.usp_MonthlyFee_GetAllMonthlyFees");
        }

        public static List<MonthlyFee> GetMonthlyFeesForProperty(int propertyId)
        {
            SqlParameter TheParam = new SqlParameter("@PropertyId", propertyId);
            return DataAccess.GetTypedList<MonthlyFee>(EfxSettings.ConnectionString, "dbo.usp_MonthlyFee_GetMonthlyFeesByPropertyId", TheParam);
        }

        public static List<MonthlyFee> GetMonthlyFeesByPropertyId(int propertyId)
        {
            //Salcedo - 2/12/2015 - I have no idea why this code ever was written to first retrieve all records from the MonthlyFee table into memory,
            //and then, select only the ones for the matching property id. But, this fix should hopefully end this nonsense.
            //return GetAllMonthlyFees().Where(f => f.PropertyId == propertyId && f.IsActive).ToList();

            return GetMonthlyFeesForProperty(propertyId).Where(f => f.PropertyId == propertyId && f.IsActive).ToList();
        }

        public static void DeleteMonthlyFee(int monthlyFeeId)
        {
            var MonthFees = new MonthlyFee(monthlyFeeId) {IsActive = false};
            Set(MonthFees);
        }

        public static int Set(int monthlyFeeId, int propertyId, string feeName, decimal defaultFeeAmount, string chargeCode, bool isActive)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_MonthlyFee_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@MonthlyFeeId", monthlyFeeId),
	                    new SqlParameter("@PropertyId", propertyId),
	                    new SqlParameter("@FeeName", feeName),
	                    new SqlParameter("@DefaultFeeAmount", defaultFeeAmount),
                        new SqlParameter("@ChargeCode", chargeCode),
                        new SqlParameter("@IsActive", isActive)
                    });
        }

        public static int Set(MonthlyFee m)
        {
	        return Set(
		        m.MonthlyFeeId,
		        m.PropertyId,
		        m.FeeName,
		        m.DefaultFeeAmount,
                m.ChargeCode,
                m.IsActive
	        );
        }

        //Salcedo - 11/4/2015 - new function/SP - this SP removes all references of the fee from the LeaseFee table, then
        //deletes it also from the MonthlyFee table
        public static int DeleteFromLeaseFeeAndMonthlyFee(int propertyId, int monthlyFeeId)
        {
            return NewDataAccess.ExecuteScalar<int>(
                EfxSettings.ConnectionString,
                "dbo.usp_MonthlyFee_DeleteFromLeaseFeeAndMonthlyFee",
                    new[]
                    {
	                    new SqlParameter("@PropertyId", propertyId),
	                    new SqlParameter("@MonthlyFeeId", monthlyFeeId)
                    });
        }

        public MonthlyFee(int monthlyFeeId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_MonthlyFee_GetSingleObject", new [] { new SqlParameter("@MonthlyFeeId", monthlyFeeId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        MonthlyFeeId = (int)ResultSet[0];
		        PropertyId = (int)ResultSet[1];
		        FeeName = ResultSet[2] as string;
		        DefaultFeeAmount = (decimal)ResultSet[3];
                IsActive = (bool)ResultSet[4];
                ChargeCode = ResultSet[5] as string;
            }
        }

		public MonthlyFee()
		{
			InitializeObject();
		}

	}
}
