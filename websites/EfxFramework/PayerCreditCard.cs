using System.Collections.Generic;
using EfxFramework.ExtensionMethods;
using Mb2x.Data;
using System;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class PayerCreditCard : BaseEntity
	{
		//Public Properties
		public int PayerCreditCardId { get; set; }
		public int PayerId { get; set; }
		public string CreditCardHolderName { get; set; }
		public string CreditCardAccountNumber { get; set; }
		public int CreditCardExpirationMonth { get; set; }
        public int CreditCardExpirationYear { get; set; }
		public bool IsPrimary { get; set; }
        public bool ReminderSent { get; set; }

        public string MaskedCreditCardAccountNumber { get { return CreditCardAccountNumber.MaskAccountNumber(); } }

	    public string CreditCardExpirationDate
	    {
	        get
	        {
	            if (CreditCardExpirationYear < DateTime.MinValue.Year || CreditCardExpirationMonth < 1 || CreditCardExpirationMonth > 12)
	            {
	                CreditCardExpirationYear = DateTime.UtcNow.Year - 1;
	                CreditCardExpirationMonth = DateTime.UtcNow.Month;
	            }

	            return new DateTime(CreditCardExpirationYear, CreditCardExpirationMonth, 1).ToString("MMyy");
	        }
	    }

        public static List<PayerCreditCard> GetAllPayerCreditCardByPayerId(int payerId)
        {
            return DataAccess.GetTypedList<PayerCreditCard>(EfxSettings.ConnectionString, "dbo.usp_PayerCreditCard_GetAllPayerCreditCardByPayerId", new SqlParameter("@PayerId", payerId));
        }

        public static PayerCreditCard GetPrimaryPayerCreditCardByPayerId(int payerId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new PayerCreditCard(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_PayerCreditCard_GetPrimaryPayerCreditCardByPayerId", new SqlParameter("@PayerId", payerId)));
        }
        
        public static List<PayerCreditCard> GetExpiringCreditCards()
        {
            return DataAccess.GetTypedList<PayerCreditCard>(EfxSettings.ConnectionString, "dbo.usp_PayerCreditCard_GetExpiringCreditCards");
        }

        public static int Set(int payerCreditCardId, int payerId, string creditCardHolderName, string creditCardAccountNumber, int creditCardExpirationMonth, int creditCardExpirationYear, bool isPrimary, bool reminderSent)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_PayerCreditCard_SetSingleObject",
                new[]
                {
	                new SqlParameter("@PayerCreditCardId", payerCreditCardId),
	                new SqlParameter("@PayerId", payerId),
	                new SqlParameter("@CreditCardHolderName", creditCardHolderName),
	                new SqlParameter("@CreditCardAccountNumber", creditCardAccountNumber),
	                new SqlParameter("@CreditCardExpirationMonth", creditCardExpirationMonth),
                    new SqlParameter("@CreditCardExpirationYear", creditCardExpirationYear),
	                new SqlParameter("@IsPrimary", isPrimary),
                    new SqlParameter("@ReminderSent", reminderSent)
                });
        }

        public static int Set(PayerCreditCard p)
        {
	        return Set(
		        p.PayerCreditCardId,
		        p.PayerId,
		        p.CreditCardHolderName,
		        p.CreditCardAccountNumber,
		        p.CreditCardExpirationMonth,
                p.CreditCardExpirationYear,
		        p.IsPrimary,
                p.ReminderSent
	        );
        }

        public PayerCreditCard(int? payerCreditCardId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_PayerCreditCard_GetSingleObject", new [] { new SqlParameter("@PayerCreditCardId", payerCreditCardId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        PayerCreditCardId = (int)ResultSet[0];
		        PayerId = (int)ResultSet[1];
		        CreditCardHolderName = ResultSet[2] as string;
		        CreditCardAccountNumber = ResultSet[3] as string;
		        CreditCardExpirationMonth = (int)ResultSet[4];
                CreditCardExpirationYear = (int) ResultSet[5];
		        IsPrimary = (bool)ResultSet[6];
                ReminderSent = (bool) ResultSet[7];
            }
        }

	    public PayerCreditCard()
	    {
	    }

        //CMallory - Task 00173 - Added method below.
        public static void DeletePayerSavedCreditCard(int renterId)
	    {
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_PayerCreditCard_DeleteSingleObject", new SqlParameter("@RenterID", renterId));
	    }

        public static int CheckIfRenterCCExist(string RenterId, string CCAccountNumber, string CCExpMonth, string CCExpYear)
        {
            int RecordCount = 0;
            RecordCount = (int)EfxFramework.NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "usp_PayerCreditCard_CheckIfRenterCCExist",
                     new[] { 
                    new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) ,

                    //The @PaymentMethodId is used to update the correct record in the PayerCreditCard table (where PayerCreditCardId = @PaymentMethodId)
                    //It can be retrieved from the Pay_PayableControllerWallet table's PayerMethodId column.
                    new SqlParameter("@CCAccountNumber", CCAccountNumber) ,
                    new SqlParameter("@CCExpMonth", CCExpMonth) ,
                    new SqlParameter("@CCExpYear", CCExpYear) });

            return RecordCount;
        }

        public static void MakePrimaryCreditCard(int PayerCreditCardID)
	    {
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_PayerCreditCard_MakePrimaryCreditCard", 
                new[]{
                //new SqlParameter("@RenterID", RenterId),
                new SqlParameter("@PayerCreditCardID", PayerCreditCardID)});
	    }
    }
}
