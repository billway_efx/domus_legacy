using System.Web;
using EfxFramework.Web.Caching;
using Mb2x.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;

namespace EfxFramework
{
	public class CalendarEvent : BaseEntity
	{
		//Public Properties
        [DisplayName(@"CalendarEventId")]
        public int CalendarEventId { get; set; }

        [DisplayName(@"Title")]
        public string Title { get; set; }

        [DisplayName(@"Location")]
        public string Location { get; set; }

        [DisplayName(@"Details")]
        public string Details { get; set; }

        [DisplayName(@"CalendarEventTypeId")]
        public int? CalendarEventTypeId { get; set; }

        [DisplayName(@"StartDate")]
        public DateTime StartDate { get; set; }

        [DisplayName(@"EndDate")]
        public DateTime EndDate { get; set; }

        [DisplayName(@"Contact")]
        public string Contact { get; set; }

        [DisplayName(@"DateCreated")]
        public DateTime DateCreated { get; set; }

        [DisplayName(@"DateLastModified")]
        public DateTime DateLastModified { get; set; }

        [DisplayName(@"Active")]
        public bool Active { get; set; }

        [DisplayName(@"PropertyId")]
        public int? PropertyId { get; set; }

        [DisplayName(@"allproperties")]
        public bool allproperties { get; set; }

        [DisplayName(@"CompanyId")]
        public int? CompanyId { get; set; }

        [DisplayName(@"Recurring")]
        public bool? Recurring { get; set; }

        [DisplayName(@"AssociatedCalendarEventId")]
        public int? AssociatedCalendarEventId { get; set; }

        public CalendarEvent()
		{
			InitializeObject();
		}
	}
}
