//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EfxFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class PaymentExportDetail
    {
        public int PaymentExportDetailId { get; set; }
        public int PaymentExportSummaryId { get; set; }
        public bool SuccessfulExport { get; set; }
        public string PaymentDetailXml { get; set; }
        public string FailedExportDescription { get; set; }
    
        public virtual PaymentExportSummary PaymentExportSummary { get; set; }
    }
}
