﻿using Mb2x.DataValidation;
using Mb2x.ExtensionMethods;
using System;
using System.Linq;

namespace EfxFramework
{
    [Serializable]
	public abstract class BaseEntity
	{
		//Public Methods
		public DataAnnotationValidationResult CheckDataValidations<T>() where T : BaseEntity
		{
			//Call the Mb2x extension method to validate DataAnnotations on this class
		    return (this as T).AreDataAnnotationsValid();
		}

		//Private Methods
		protected void InitializeObject()
		{
			//Initialize all of the properties
			GetType()
				.GetProperties()
				.Where(i => i.CanWrite)
				.ForEach(i => i.SetValue(this, null, null));
		}
	}
}