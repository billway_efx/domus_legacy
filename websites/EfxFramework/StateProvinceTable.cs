using EfxFramework.Web.Caching;
using Mb2x.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class StateProvinceTable : BaseEntity
	{
		//Public Properties
		[DisplayName(@"StateProvinceId"), Required(ErrorMessage = "StateProvinceId is required.")]
		public int StateProvinceId { get; set; }

		[DisplayName(@"StateProvinceName"), Required(ErrorMessage = "StateProvinceName is required."), StringLength(50)]
		public string StateProvinceName { get; set; }

		[DisplayName(@"Abbreviation"), Required(ErrorMessage = "Abbreviation is required."), StringLength(2)]
		public string Abbreviation { get; set; }

        public static List<StateProvinceTable> GetAllStates()
        {
            var States = CacheManager.GetCacheItem<List<StateProvinceTable>>(CacheKey.StateProvinceCache);

            if (States == null || States.Count < 1)
                CacheManager.AddItemToCache(CacheKey.StateProvinceCache, DataAccess.GetTypedList<StateProvinceTable>(EfxSettings.ConnectionString, "dbo.usp_StateProvince_GetAll"), 1440);

            return CacheManager.GetCacheItem<List<StateProvinceTable>>(CacheKey.StateProvinceCache);
        }

        public static int Set(int stateProvinceId, string stateProvinceName, string abbreviation)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_StateProvince_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@StateProvinceId", stateProvinceId),
	                    new SqlParameter("@StateProvinceName", stateProvinceName),
	                    new SqlParameter("@Abbreviation", abbreviation)
                    });
        }

        public static int Set(StateProvinceTable s)
        {
	        return Set(
		        s.StateProvinceId,
		        s.StateProvinceName,
		        s.Abbreviation
	        );
        }

        public StateProvinceTable(int stateProvinceId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_StateProvince_GetSingleObject", new [] { new SqlParameter("@StateProvinceId", stateProvinceId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        StateProvinceId = (int)ResultSet[0];
		        StateProvinceName = ResultSet[1] as string;
		        Abbreviation = ResultSet[2] as string;
            }
        }

		public StateProvinceTable()
		{
			InitializeObject();
		}

	}
}
