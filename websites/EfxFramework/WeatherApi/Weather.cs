﻿using System;
using System.Collections.Generic;
using EfxFramework.Serialization;
using EfxFramework.Web.Caching;
using Mb2x.ExtensionMethods;
using System.Net;

namespace EfxFramework.WeatherApi
{
    public class Weather
    {
        private static string Location { get; set; }

        public static WeatherResults GetWeather(string city, string state)
        {
            //Create location string with city and state
            Location = string.Format("{0}, {1}", city, state);

            //Get the WeatherResults cache
            var Cachelist = CacheManager.GetCacheItem<Dictionary<string, WeatherResults>>(CacheKey.WeatherResults);

            //If the cachelist does not have items
            if (Cachelist == null || Cachelist.Count < 1)
            {
                //Build a new dictionary with the city and state
                Cachelist = BuildWeatherDictionary(city, state);
                //Add the cachelist to the WeatherResults cache
                CacheManager.AddItemToCache(CacheKey.WeatherResults, Cachelist, 60);
            }

            //If the cachelists does not contain a key with the current location
            else if (!Cachelist.ContainsKey(Location))
            {
                try
                {
                    //Download the weather data and add it to the cachelist for the current location
                    Cachelist.Add(Location, DownloadWeather(city, state));
                }
                catch (ArgumentException Ex)
                {
                    if (!Ex.Message.ToLower().Contains("an item with the same key has been added"))
                        throw;
                }

                //Remove the WeatherResults cache
                CacheManager.RemoveItemFromCache(CacheKey.WeatherResults);
                //Add cachelist to the WeatherResultsCache
                CacheManager.AddItemToCache(CacheKey.WeatherResults, Cachelist, 60);
            }
            
            //Return the cache list
            return Cachelist[Location];
        }

        private static WeatherResults DownloadWeather(string city, string state)
        {
            using (var Client = new WebClient())
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //Create download url using current city and state
                var DownloadUrl = string.Format("https://api.wunderground.com/api/{0}/conditions/q/{1}/{2}.json", EfxSettings.WeatherKey.UrlEncode(), state.UrlEncode(), city.UrlEncode());

                //Download the data
                var Request = Client.DownloadData(DownloadUrl);

                //Serialize the response to WeatherResults and return
                return JsonSerializer.SerializeJsonResponse<WeatherResults>(Request);
            }
        }

        private static Dictionary<string, WeatherResults> BuildWeatherDictionary(string city, string state)
        {
            using (var Client = new WebClient())
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //Create download url using current city and state
                var DownloadUrl = string.Format("https://api.wunderground.com/api/{0}/conditions/q/{1}/{2}.json", EfxSettings.WeatherKey.UrlEncode(), state.UrlEncode(), city.UrlEncode());
                
                //Download the data
                var Request = Client.DownloadData(DownloadUrl);

                //Create a new dictionary for the location after serializing the response, then return
                return new Dictionary<string, WeatherResults> {{Location, JsonSerializer.SerializeJsonResponse<WeatherResults>(Request)}};
            }
        }
    }
}
