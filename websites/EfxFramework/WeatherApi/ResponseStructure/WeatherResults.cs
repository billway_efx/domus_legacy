﻿using EfxFramework.WeatherApi.ResponseStructure;
using System.Runtime.Serialization;

namespace EfxFramework.WeatherApi
{
    [DataContract]
    public class WeatherResults
    {
        [DataMember]
        public Observation current_observation { get; set; }

        public WeatherResults() {}
    }
}
