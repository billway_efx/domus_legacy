﻿using System.Runtime.Serialization;

namespace EfxFramework.WeatherApi.ResponseStructure
{
    [DataContract]
    public class Observation
    {
        [DataMember]
        public string temperature_string { get; set; }

        [DataMember]
        public string relative_humidity { get; set; }

        [DataMember]
        public string weather { get; set; }

        [DataMember]
        public string wind_mph { get; set; }

        [DataMember]
        public string wind_gust_mph { get; set; }

        [DataMember]
        public string icon_url {get; set; }

        public Observation() {} 
    }
}
