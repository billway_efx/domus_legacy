﻿using System.Data;
using System.Data.SqlClient;

namespace EfxFramework
{
    public class ConvenienceFee
    {
        public decimal CreditCardFee { get; set; }
        public decimal ECheckFee { get; set; }
        public decimal AppDepositFee { get; set; }

        public static ConvenienceFee GetConvenienceFeeForProperty(Property.Program programId, decimal? avgRent)
        {
            switch (programId)
            {
                case Property.Program.Unassigned:
                    return new ConvenienceFee
                    {
                        CreditCardFee = CalculateFee(avgRent, .024M),
                        ECheckFee = CalculateFee(avgRent, .024M),
                        AppDepositFee = CalculateFee(avgRent, .024M)
                    };
                case Property.Program.AirProgram:
                    return new ConvenienceFee
                    {
                        CreditCardFee = CalculateFee(avgRent, .024M),
                        ECheckFee = 0.0M,
                        AppDepositFee = CalculateFee(avgRent, .0255M, .15M)
                    };
                case Property.Program.SpaProgram:
                    return new ConvenienceFee
                    {
                        CreditCardFee = CalculateFee(avgRent, .024M),
                        ECheckFee = 0.89M,
                        AppDepositFee = CalculateFee(avgRent, .0255M, .10M)
                    };
                case Property.Program.PropertyPaidProgram:
                    return new ConvenienceFee
                    {
                        CreditCardFee = 0.0M,
                        ECheckFee = 0.0M,
                        AppDepositFee = CalculateFee(avgRent, .024M)
                    };
                //cakel:10/19/2016 updated this for a new program
                case Property.Program.Air2Legacy:
                    return new ConvenienceFee
                    {
                        CreditCardFee = 0.0M,
                        ECheckFee = 0.0M,
                        AppDepositFee = CalculateFee(avgRent, .024M)
                    };
                default:
                    return new ConvenienceFee
                    {
                        CreditCardFee = CalculateFee(avgRent, .024M),
                        ECheckFee = 0.0M,
                        AppDepositFee = CalculateFee(avgRent, .024M)
                    };
            }
        }


        //cakel: need to revist this and adjust the was conv fee are gathered within the site
        //public static ConvenienceFee GetConvenienceFeeForProperty(int programId, decimal? avgRent)
        //{
        //    SqlDataReader Reader = GetConvDataByProgramID(programId);
        //    Reader.Read();
        //    if(Reader.HasRows)
        //    {
        //        decimal cc = (decimal)Reader["CreditCardFee"];
        //        decimal ec = (decimal)Reader["CreditCardFee"];
        //        decimal af = (decimal)Reader["CreditCardFee"];

        //        return new ConvenienceFee
        //        {
        //            CreditCardFee = cc,
        //            ECheckFee = ec,
        //            AppDepositFee = af
        //        };
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        public static SqlDataReader GetConvDataByProgramID(int ProgramID)
        {
            SqlConnection con = new SqlConnection(EfxSettings.ConnectionString);
            SqlCommand com = new SqlCommand("ProgramEntity_GetFeesBYProgramID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ProgramID", SqlDbType.Int).Value = ProgramID;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch(SqlException ex)
            {
                con.Close();
                throw ex;
            }

        }


        public static ConvenienceFee GetConvenienceFeeForProperty(Property property, decimal? avgRent)
        {
            if (!property.ProgramId.HasValue)
                return new ConvenienceFee
                {
                    CreditCardFee = CalculateFee(avgRent, .024M),
                    ECheckFee = CalculateFee(avgRent, .024M),
                    AppDepositFee = CalculateFee(avgRent, .024M)
                };

            return GetConvenienceFeeForProperty(property.ProgramId.Value, avgRent);
        }

        public static ConvenienceFee GetConvenienceFeeForProperty(Property property)
        {
            return GetConvenienceFeeForProperty(property, property.AverageMonthlyRent);
        }

        private static decimal CalculateFee(decimal? averageRent, decimal rate, decimal additionalAmount = 0.0M)
        {
            if (!averageRent.HasValue)
                return 24.95M;

            return (averageRent.Value * rate) + additionalAmount;
        }
    }
}
