﻿namespace EfxFramework.PublicApi.Payment
{
    public enum PaymentResponseResult
    {
        Success = 0,
        GeneralFailure = 2,
        InvalidLogin = 3,
        ResidentNotFound = 9999,
        FirstNameRequired = 9998,
        LastNameRequired = 9997,
        EmailAddressRequired = 9996,
        InvalidEmail = 9995,
        Address1Required = 9994,
        CityRequired = 9993,
        StateRequired = 9992,
        InvalidStateFormat = 9991,
        PostalCodeRequired = 9990,
        TelephoneNumberRequired = 9989,
        PaymentAmountRequired = 9988,
        AccountNumberRequired = 9987,
        RoutingNumberRequired = 9986,
        InvalidRoutingNumber = 9985,
        AccountTypeRequired = 9984,
        NameOnCardRequired = 9983,
        CreditCardNumberRequired = 9982,
        CreditCardExpired = 9981,
        CvvCodeRequired = 9980,
        RecurringStartDateInvalid = 9979,
        RecurringEndDateInvalid = 9978,
        TransactionIdRequired = 9977,
        InvalidTransactionId = 9976,
        InvalidReversalAmount = 9975,
        InvalidPropertyId = 9974,
        PropertyNotFound = 9973,
        InvalidPaymentCategory = 9972,
        AchAccountNotFound = 9971,
        PaymentFailed = 9970,
        ResidentRequired = 9969,
        InvalidPaymentType = 9968,
        ReversalFailed = 9967,
        CannotAcceptPayment = 9966,
        CashEquivalentRequired = 9965,
        AutoPaymentNotFound = 9964
    }

    public static class PaymentResponseMessage
    {
        public static string Success { get { return "Successfully completed the operation"; } }
        public static string GeneralFailure { get { return "A general failure occurred, please check your inputs and try the operation again"; } }
        public static string InvalidLogin { get { return "Invalid login credentials were supplied.  Please check your information and try again"; } }
        public static string ResidentNotFound { get { return "Resident not found with the supplied Public Resident ID. Please check your information try again"; } }
        public static string FirstNameRequired { get { return "First Name is required"; } }
        public static string LastNameRequired { get { return "Last Name is required"; } }
        public static string EmailAddressRequired { get { return "Email Address is required"; } }
        public static string InvalidEmail { get { return "Email address is not valid"; } }
        public static string Address1Required { get { return "Address 1 is required"; } }
        public static string CityRequired { get { return "City is required"; } }
        public static string StateRequired { get { return "State is required"; } }
        public static string InvalidStateFormat { get { return "State must be in the two character state code format"; } }
        public static string PostalCodeRequired { get { return "Postal Code is required"; } }
        public static string TelephoneNumberRequired { get { return "Telephone Number is required"; } }
        public static string PaymentAmountRequired { get { return "A Payment Amount greater than $0.00 is required"; } }
        public static string AccountNumberRequired { get { return "A Bank Account Number is required"; } }
        public static string RoutingNumberRequired { get { return "A Routing Number is required"; } }
        public static string InvalidRoutingNumber { get { return "Routing number is not valid"; } }
        public static string AccountTypeRequired { get { return "An Account Type of 1 for checking or 2 for savings is required"; } }
        public static string NameOnCardRequired { get { return "Cardholder Name is required"; } }
        public static string CreditCardNumberRequired { get { return "Credit Card Number is required"; } }
        public static string CreditCardExpired { get { return "Credit Card Expiration Date has passed.  Please check your information and try again"; } }
        public static string CvvCodeRequired { get { return "CVV Code is required"; } }
        public static string RecurringStartDateInvalid { get { return "Start Date must be a date greater than today's date.  Please check your information and try again"; } }
        public static string RecurringEndDateInvalid { get { return "End Date must be a date greater than today's date and greater than the Start Date.  Please check your information and try again"; } }
        public static string TransactionIdRequired { get { return "A Transaction ID is requried"; } }
        public static string InvalidTransactionId { get { return "No Transaction could be found with the Transaction ID specified.  Please check your information and try again"; } }
        public static string InvalidReversalAmount { get { return "The Reversal Amount exceeds the remaining balance from the payment.  Please check your information and try again"; } }
        public static string InvalidPropertyId { get { return "A valid Property Id is required"; } }
        public static string PropertyNotFound { get { return "The Property specified was not found.  Please check your information and try again"; } }
        public static string InvalidPaymentCategory { get { return "Payment Category must be a 0, 1 or 2 depending on what is being paid.  Please check your information and try again"; } }
        public static string AchAccountNotFound { get { return "Could not find the appropriate ACH Account for the Property and/or Payment Category. Please check your information and try again"; } }
        public static string ResidentRequired { get { return "Recurring Payments require an active Resident account.  Please check your information and try again"; } }
        public static string InvalidPaymentType { get { return "An Invalid Payment Type was specified for this transaction, cannot process reversal"; } }
        public static string CannotAcceptPayment { get { return "Cannot accept payments for this resident"; } }
        public static string CashEquivalentRequired { get { return "Can only accept Credit Card payments for this resident"; } }
        public static string AutoPaymentNotFound { get { return "Recurring Payment record not found, please check your information and try again"; } }
        public static string AutoPaymentCancelled { get { return "Recurring Payment successfully cancelled"; } }
        public static string AutoPaymentCancelFailed { get { return "Recurring Payment failed to cancel, please check your information and try again"; } }
    }
}
