﻿using System;
using System.Runtime.Serialization;
using EfxFramework.PaymentMethods;
using Mb2x.ExtensionMethods;

namespace EfxFramework.PublicApi.Payment
{
    [DataContract]
    public abstract class RecurringPaymentRequest : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public long PublicResidentId { get; set; }

        [DataMember(IsRequired = true)]
        public int PropertyId { get; set; }

        [DataMember(IsRequired = true)]
        public string FirstName { get; set; }

        [DataMember(IsRequired = true)]
        public string LastName { get; set; }

        [DataMember(IsRequired = true)]
        public string EmailAddress { get; set; }

        [DataMember(IsRequired = true)]
        public string Address1 { get; set; }

        [DataMember]
        public string Address2 { get; set; }

        [DataMember(IsRequired = true)]
        public string City { get; set; }

        [DataMember(IsRequired = true)]
        public string State { get; set; }

        [DataMember(IsRequired = true)]
        public string PostalCode { get; set; }

        [DataMember(IsRequired = true)]
        public string TelephoneNumber { get; set; }

        [DataMember]
        public string PaymentMemo { get; set; }

        [DataMember(IsRequired = true)]
        public int PaymentCategory { get; set; }

        [DataMember(IsRequired = true)]
        public decimal PaymentAmount { get; set; }

        [DataMember]
        public decimal? ConvenienceFeeAmount { get; set; }

        [DataMember(IsRequired = true)]
        public DateTime StartDate { get; set; }

        [DataMember(IsRequired = true)]
        public DateTime EndDate { get; set; }

        public virtual RecurringPaymentResponse CancelRecurringPayment()
        {
            var ResidentId = Renter.GetRenterIdByPublicRenterId(PublicResidentId);

            if (!ResidentId.HasValue || ResidentId.Value < 1)
                return new RecurringPaymentResponse {Result = (int) PaymentResponseResult.ResidentNotFound, Message = PaymentResponseMessage.ResidentNotFound, TransactionDate = DateTime.UtcNow};

            var AutoPayment = EfxFramework.AutoPayment.GetAutoPaymentByRenterId(ResidentId.Value);

            if (AutoPayment.AutoPaymentId < 1)
                return new RecurringPaymentResponse {Result = (int) PaymentResponseResult.AutoPaymentNotFound, Message = PaymentResponseMessage.AutoPaymentNotFound, TransactionDate = DateTime.UtcNow};

            try
            {
                AutoPayment.DeleteAutoPayment(AutoPayment.AutoPaymentId);
                return new RecurringPaymentResponse {Result = (int) PaymentResponseResult.Success, Message = PaymentResponseMessage.AutoPaymentCancelled, TransactionDate = DateTime.UtcNow};
            }
            catch
            {
                return new RecurringPaymentResponse {Result = (int) PaymentResponseResult.GeneralFailure, Message = PaymentResponseMessage.AutoPaymentCancelFailed, TransactionDate = DateTime.UtcNow};
            }
        }

        public abstract RecurringPaymentResponse ProcessRecurringPayment();
        protected abstract int BuildPaymentMethod(int payerId);

        protected virtual bool ValidatePaymentRequest(out RecurringPaymentResponse response)
        {
            response = new RecurringPaymentResponse {TransactionDate = DateTime.UtcNow};
            StateProvince StateProvince;

            if (PublicResidentId < 1)
            {
                response.Result = (int) PaymentResponseResult.ResidentRequired;
                response.Message = PaymentResponseMessage.ResidentRequired;
                return false;
            }

            var ResidentId = Renter.GetRenterIdByPublicRenterId(PublicResidentId);

            if (!ResidentId.HasValue || ResidentId.Value < 1)
            {
                response.Result = (int) PaymentResponseResult.ResidentNotFound;
                response.Message = PaymentResponseMessage.ResidentNotFound;
                return false;
            }

            if (PropertyId < 1)
            {
                response.Result = (int) PaymentResponseResult.InvalidPropertyId;
                response.Message = PaymentResponseMessage.InvalidPropertyId;
                return false;
            }

            var P = new EfxFramework.Property(PropertyId);

            if (!P.PropertyId.HasValue || P.PropertyId.Value < 1)
            {
                response.Result = (int) PaymentResponseResult.PropertyNotFound;
                response.Message = PaymentResponseMessage.PropertyNotFound;
                return false;
            }

            if (String.IsNullOrEmpty(FirstName))
            {
                response.Result = (int) PaymentResponseResult.FirstNameRequired;
                response.Message = PaymentResponseMessage.FirstNameRequired;
                return false;
            }

            if (String.IsNullOrEmpty(LastName))
            {
                response.Result = (int) PaymentResponseResult.LastNameRequired;
                response.Message = PaymentResponseMessage.LastNameRequired;
                return false;
            }

            if (String.IsNullOrEmpty(EmailAddress))
            {
                response.Result = (int) PaymentResponseResult.EmailAddressRequired;
                response.Message = PaymentResponseMessage.EmailAddressRequired;
                return false;
            }

            if (!EmailAddress.IsValidEmailAddress())
            {
                response.Result = (int) PaymentResponseResult.InvalidEmail;
                response.Message = PaymentResponseMessage.InvalidEmail;
                return false;
            }

            if (String.IsNullOrEmpty(Address1))
            {
                response.Result = (int) PaymentResponseResult.Address1Required;
                response.Message = PaymentResponseMessage.Address1Required;
                return false;
            }

            if (String.IsNullOrEmpty(City))
            {
                response.Result = (int) PaymentResponseResult.CityRequired;
                response.Message = PaymentResponseMessage.CityRequired;
                return false;
            }

            if (String.IsNullOrEmpty(State))
            {
                response.Result = (int) PaymentResponseResult.StateRequired;
                response.Message = PaymentResponseMessage.StateRequired;
                return false;
            }

            if (!Enum.TryParse(State, out StateProvince))
            {
                response.Result = (int) PaymentResponseResult.InvalidStateFormat;
                response.Message = PaymentResponseMessage.InvalidStateFormat;
                return false;
            }

            if (String.IsNullOrEmpty(PostalCode))
            {
                response.Result = (int) PaymentResponseResult.PostalCodeRequired;
                response.Message = PaymentResponseMessage.PostalCodeRequired;
                return false;
            }

            if (String.IsNullOrEmpty(TelephoneNumber))
            {
                response.Result = (int) PaymentResponseResult.TelephoneNumberRequired;
                response.Message = PaymentResponseMessage.TelephoneNumberRequired;
                return false;
            }

            if (PaymentAmount < .01M)
            {
                response.Result = (int) PaymentResponseResult.PaymentAmountRequired;
                response.Message = PaymentResponseMessage.PaymentAmountRequired;
                return false;
            }

            if (StartDate <= DateTime.UtcNow)
            {
                response.Result = (int) PaymentResponseResult.RecurringStartDateInvalid;
                response.Message = PaymentResponseMessage.RecurringStartDateInvalid;
                return false;
            }

            if (EndDate <= DateTime.UtcNow || EndDate <= StartDate)
            {
                response.Result = (int) PaymentResponseResult.RecurringEndDateInvalid;
                response.Message = PaymentResponseMessage.RecurringEndDateInvalid;
                return false;
            }

            if (PaymentCategory < 0 || PaymentCategory > 2)
            {
                response.Result = (int) PaymentResponseResult.InvalidPaymentCategory;
                response.Message = PaymentResponseMessage.InvalidPaymentCategory;
                return false;
            }

            return true;
        }

        protected virtual AutoPayment BuildAutoPayment(Renter resident, PaymentAmount amount, PaymentType paymentType)
        {
            if (!resident.PayerId.HasValue || resident.PayerId.Value < 1)
                resident.PayerId = Payer.SetPayerFromRenter(resident).PayerId;

            var Payment = AutoPayment.GetAutoPaymentByRenterId(resident.RenterId);

            Payment.RenterId = resident.RenterId;
            Payment.PayerId = resident.PayerId.Value;
            Payment.PaymentTypeId = (int)paymentType;
            Payment.PayerCreditCardId = paymentType == PaymentType.CreditCard ? BuildPaymentMethod(resident.PayerId.Value) : (int?)null;
            Payment.PayerAchId = paymentType == PaymentType.ECheck ? BuildPaymentMethod(resident.PayerId.Value) : (int?)null;
            Payment.CharityAmount = null;
            Payment.CharityName = null;
            Payment.OtherAmount2 = amount.FeeAmount;
            Payment.OtherDescription2 = amount.FeeDescription;
            Payment.OtherAmount3 = null;
            Payment.OtherDescription3 = null;
            Payment.OtherAmount4 = null;
            Payment.OtherDescription4 = null;
            Payment.OtherAmount5 = null;
            Payment.OtherDescription5 = null;
            Payment.PaymentDayOfMonth = StartDate.Day;

            switch ((PaymentCategory) PaymentCategory)
            {
                case PublicApi.Payment.PaymentCategory.Rent:
                    Payment.RentAmount = amount.BaseAmount;
                    Payment.RentAmountDescription = ((PaymentCategory) PaymentCategory).ToString();
                    Payment.OtherAmount1 = null;
                    Payment.OtherDescription1 = null;
                    break;

                default:
                    Payment.RentAmount = null;
                    Payment.RentAmountDescription = null;
                    Payment.OtherAmount1 = amount.BaseAmount;
                    Payment.OtherDescription1 = ((PaymentCategory) PaymentCategory).ToString();
                    break;
            }

            return Payment;
        }
    }
}
