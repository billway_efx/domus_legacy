﻿using EfxFramework.PaymentMethods;
using EfxFramework.PaymentMethods.Ach;
using EfxFramework.PaymentMethods.CreditCard;
using System;
using System.Linq;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Payment
{
    [DataContract]
    public class ReversalRequest : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public string TransactionId { get; set; }

        [DataMember]
        public decimal? ReversalAmount { get; set; }

        public ReversalResponse ProcessReversal()
        {
            if (!IsAuthenticatedUser())
                return new ReversalResponse {TransactionDate = DateTime.UtcNow, Result = (int) PaymentResponseResult.InvalidLogin, Message = PaymentResponseMessage.InvalidLogin};

            ReversalResponse Response;

            if (!ValidateReversal(out Response))
                return Response;

            var Transaction = EfxFramework.Transaction.GetTransactionByInternalTransactionId(TransactionId);

            switch (((PaymentType)Transaction.PaymentTypeId))
            {
                case PaymentType.CreditCard:
                    Response = ReverseCreditCard(Response, Transaction);
                    break;
                case PaymentType.ECheck:
                    Response = ReverseAch(Response, Transaction);
                    break;
                default:
                    Response.Result = (int) PaymentResponseResult.InvalidPaymentType;
                    Response.Message = PaymentResponseMessage.InvalidPaymentType;
                    break;
            }

            return Response;
        }

        private bool ValidateReversal(out ReversalResponse response)
        {
            response = new ReversalResponse {TransactionDate = DateTime.UtcNow};

            if (String.IsNullOrEmpty(TransactionId))
            {
                response.Result = (int) PaymentResponseResult.TransactionIdRequired;
                response.Message = PaymentResponseMessage.TransactionIdRequired;
                return false;
            }

            var Transaction = EfxFramework.Transaction.GetTransactionByInternalTransactionId(TransactionId);

            if (Transaction.TransactionId < 1)
            {
                response.Result = (int) PaymentResponseResult.InvalidTransactionId;
                response.Message = PaymentResponseMessage.InvalidTransactionId;
                return false;
            }

            if (ReversalAmount.HasValue && ReversalAmount.Value > 0)
            {
                var TotalAmountReversed = TransactionReversal.GetReversalsByInternalTransactionId(TransactionId).Sum(reversal => reversal.ReversalAmount) + ReversalAmount.Value;

                if (Transaction.PaymentAmount < TotalAmountReversed)
                {
                    response.Result = (int) PaymentResponseResult.InvalidReversalAmount;
                    response.Message = PaymentResponseMessage.InvalidReversalAmount;
                    return false;
                }
            }

            return true;
        }

        private ReversalResponse ReverseCreditCard(ReversalResponse response, EfxFramework.Transaction transaction)
        {
            try
            {
                if (String.IsNullOrEmpty(transaction.ExternalTransactionId))
                {
                    response.Result = (int) PaymentResponseResult.InvalidTransactionId;
                    response.Message = PaymentResponseMessage.InvalidTransactionId;
                    return response;
                }

                var VoidResponse = CreditCardPayment.ReverseCreditCardPayment(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, transaction.ExternalTransactionId, ReversalAmount);

                if (VoidResponse.Result != GeneralResponseResult.Success)
                {
                    response.Result = (int) PaymentResponseResult.ReversalFailed;
                    response.Message = VoidResponse.ResponseMessage;
                }

                response.Result = (int) PaymentResponseResult.Success;
                response.Message = PaymentResponseMessage.Success;
                response.TransactionAmount = ReversalAmount.HasValue ? ReversalAmount.Value : transaction.PaymentAmount;
                response.TransactionId = LogReversal(transaction);
                response.TransactionType = (int) TransactionType.CreditCard;
            }
            catch
            {
                response.Result = (int) PaymentResponseResult.GeneralFailure;
                response.Message = PaymentResponseMessage.GeneralFailure;
            }

            return response;
        }

        private ReversalResponse ReverseAch(ReversalResponse response, EfxFramework.Transaction transaction)
        {
            var Payment = EfxFramework.Payment.GetPaymentByTransactionId(TransactionId);
            var Resident = new Renter(Payment.RenterId);
            var Property = EfxFramework.Property.GetPropertyByRenterId(Resident.RenterId);
            var Transaction = EfxFramework.Transaction.GetTransactionByInternalTransactionId(TransactionId);

            try
            {
                if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                {
                    response.Result = (int) PaymentResponseResult.PropertyNotFound;
                    response.Message = PaymentResponseMessage.PropertyNotFound;
                    return response;
                }

                if (String.IsNullOrEmpty(transaction.ExternalTransactionId))
                {
                    response.Result = (int) PaymentResponseResult.InvalidTransactionId;
                    response.Message = PaymentResponseMessage.InvalidTransactionId;
                    return response;
                }

                var State = Resident.StateProvinceId.HasValue ? ((StateProvince)Resident.StateProvinceId.Value).ToString() : "AK";
                var Status = AchService.GetAchStatus(Property.RentalAccountAchClientId, Transaction.ExternalTransactionId);

                if (Status.StatusCode.HasValue && Status.StatusCode.Value == 1)
                {
                    var Result = AchService.CancelPendingAchTransaction(Property.RentalAccountAchClientId, Transaction.ExternalTransactionId);

                    if (Result.Result != GeneralResponseResult.Success)
                    {
                        response.Result = (int)PaymentResponseResult.ReversalFailed;
                        response.Message = Result.StatusDescription;
                        return response;
                    }
                }
                else if (Status.StatusCode.HasValue && (Status.StatusCode.Value == 2 || Status.StatusCode.Value == 3 || Status.StatusCode.Value == 4))
                {
                    // ReSharper disable PossibleInvalidOperationException
                    var Result = AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword, Property.RentalAccountAchClientId, ReversalAmount.Value, Payment.BankRoutingNumber,
                        Payment.BankAccountNumber, "CHECKING", Resident.FirstName, Resident.LastName, Resident.StreetAddress, Resident.Unit, Resident.City, State, Resident.PostalCode,
                        Resident.MainPhoneNumber);
                    // ReSharper restore PossibleInvalidOperationException

                    response.Result = (int) PaymentResponseResult.ReversalFailed;
                    response.Message = Result.ResponseDescription;
                    return response;
                }
                else
                {
                    response.Result = (int) PaymentResponseResult.ReversalFailed;
                    response.Message = "Unable to Process the Reversal because the ACH Transaction is not in a state to be able to cancel or refund.";
                    return response;
                }

                response.Result = (int) PaymentResponseResult.Success;
                response.Message = PaymentResponseMessage.Success;

                LogReversal(transaction);
            }
            catch
            {
                response.Result = (int) PaymentResponseResult.GeneralFailure;
                response.Message = PaymentResponseMessage.GeneralFailure;
            }

            return response;
        }

        private string LogReversal(EfxFramework.Transaction transaction)
        {
            var Reversal = new TransactionReversal
                {
                    InternalTransactionId = transaction.InternalTransactionId,
                    ReversalAmount = ReversalAmount.HasValue && ReversalAmount.Value > 0.00M ? ReversalAmount.Value : transaction.PaymentAmount,
                    ReversalDate = DateTime.UtcNow
                };

            return new TransactionReversal(TransactionReversal.Set(Reversal)).InternalTransactionId;
        }
    }
}
