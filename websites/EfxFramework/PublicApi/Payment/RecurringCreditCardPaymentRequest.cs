﻿using System;
using System.Runtime.Serialization;
using EfxFramework.PaymentMethods;

namespace EfxFramework.PublicApi.Payment
{
    [DataContract]
    public class RecurringCreditCardPaymentRequest : RecurringPaymentRequest
    {
        [DataMember(IsRequired = true)]
        public string NameOnCard { get; set; }

        [DataMember(IsRequired = true)]
        public string CreditCardNumber { get; set; }

        [DataMember(IsRequired = true)]
        public DateTime CreditCardExpiration { get; set; }

        public override RecurringPaymentResponse ProcessRecurringPayment()
        {
            if (!IsAuthenticatedUser())
                return new RecurringPaymentResponse { TransactionDate = DateTime.UtcNow, Result = (int)PaymentResponseResult.InvalidLogin, Message = PaymentResponseMessage.InvalidLogin };

            RecurringPaymentResponse Response;

            if (!ValidatePaymentRequest(out Response))
                return Response;

            var Amount = new PaymentAmount { BaseAmount = PaymentAmount };

            if (ConvenienceFeeAmount.HasValue && ConvenienceFeeAmount.Value > 0.00M)
            {
                Amount.FeeAmount = ConvenienceFeeAmount.Value;
                Amount.FeeDescription = "Convenience Fee";
            }

            var Resident = new Renter(Renter.GetRenterIdByPublicRenterId(PublicResidentId));

            if (Resident.RenterId < 1)
            {
                Response.Result = (int)PaymentResponseResult.ResidentNotFound;
                Response.Message = PaymentResponseMessage.ResidentNotFound;
                return Response;
            }

            try
            {
                var Payment = BuildAutoPayment(Resident, Amount, PaymentType.CreditCard);
                Payment = new AutoPayment(AutoPayment.Set(Payment));

                if (Payment.AutoPaymentId < 1)
                {
                    Response.Result = (int)PaymentResponseResult.GeneralFailure;
                    Response.Message = PaymentResponseMessage.GeneralFailure;
                    return Response;
                }

                Response.Result = (int) PaymentResponseResult.Success;
                Response.Message = PaymentResponseMessage.Success;
                Response.TransactionAmount = PaymentAmount;
                Response.TransactionType = TransactionType.CreditCard.ToString();
            }
            catch
            {
                Response.Result = (int)PaymentResponseResult.GeneralFailure;
                Response.Message = PaymentResponseMessage.GeneralFailure;
            }

            return Response;
        }

        protected override bool ValidatePaymentRequest(out RecurringPaymentResponse response)
        {
            if (!base.ValidatePaymentRequest(out response))
                return false;

            if (String.IsNullOrEmpty(NameOnCard))
            {
                response.Result = (int)PaymentResponseResult.NameOnCardRequired;
                response.Message = PaymentResponseMessage.NameOnCardRequired;
                return false;
            }

            if (String.IsNullOrEmpty(CreditCardNumber))
            {
                response.Result = (int)PaymentResponseResult.CreditCardNumberRequired;
                response.Message = PaymentResponseMessage.CreditCardNumberRequired;
                return false;
            }

            if (DateTime.UtcNow.AddMonths(-1) > CreditCardExpiration)
            {
                response.Result = (int)PaymentResponseResult.CreditCardExpired;
                response.Message = PaymentResponseMessage.CreditCardExpired;
                return false;
            }

            return true;
        }

        protected override int BuildPaymentMethod(int payerId)
        {
            var PayerCreditCard = new PayerCreditCard
                {
                    PayerId = payerId,
                    CreditCardAccountNumber = CreditCardNumber,
                    CreditCardExpirationMonth = CreditCardExpiration.Month,
                    CreditCardExpirationYear = CreditCardExpiration.Year,
                    CreditCardHolderName = NameOnCard,
                    IsPrimary = true
                };

            return EfxFramework.PayerCreditCard.Set(PayerCreditCard);
        }
    }
}
