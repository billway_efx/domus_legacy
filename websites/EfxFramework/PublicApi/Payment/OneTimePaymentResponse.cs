﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Payment
{
    [DataContract]
    public class OneTimePaymentResponse : ApiBaseResponse
    {
        [DataMember]
        public decimal TransactionAmount { get; set; }

        [DataMember]
        public string TransactionType { get; set; }

        [DataMember]
        public string TransactionId { get; set; }
    }
}
