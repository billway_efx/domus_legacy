﻿using EfxFramework.PaymentMethods;
using EfxFramework.PaymentMethods.CreditCard;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Mail;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Payment
{
    [DataContract]
    public class OneTimeCreditCardPaymentRequest : OneTimePaymentRequest
    {
        [DataMember(IsRequired = true)]
        public string NameOnCard { get; set; }

        [DataMember(IsRequired = true)]
        public string CreditCardNumber { get; set; }

        [DataMember(IsRequired = true)]
        public DateTime CreditCardExpiration { get; set; }

        [DataMember(IsRequired = true)]
        public string CvvCode { get; set; }

        [DataMember]
        public string UnitNumber { get; set; }

        [DataMember]
        public bool IsTestTransaction { get; set; }

        public override OneTimePaymentResponse ProcessPayment()
        {
            if (!IsAuthenticatedUser())
                return new OneTimePaymentResponse { TransactionDate = DateTime.UtcNow, Result = (int)PaymentResponseResult.InvalidLogin, Message = PaymentResponseMessage.InvalidLogin };

            OneTimePaymentResponse Response;

            if (!ValidatePaymentRequest(out Response))
                return Response;

            var Property = new EfxFramework.Property(PropertyId);
            var Amount = new PaymentAmount {BaseAmount = PaymentAmount};

            if (ConvenienceFeeAmount.HasValue && ConvenienceFeeAmount.Value > 0.00M)
            {
                Amount.FeeAmount = ConvenienceFeeAmount.Value;
                Amount.FeeDescription = "Convenience Fee";
            }

            var CustomFields = new List<CustomField>
                {
                    new CustomField("customfield1", Property.PropertyId.HasValue ? Property.PropertyId.Value.ToString(CultureInfo.InvariantCulture) : "0"),
                    new CustomField("customfield2", UnitNumber),
                    new CustomField("customfield3", String.Format("{0}{1}", DateTime.Now.Month.ToString(CultureInfo.InvariantCulture), DateTime.Now.Year.ToString(CultureInfo.InvariantCulture))),
                    new CustomField("customfield5", (Convert.ToInt32(Amount.FeeAmount*100)).ToString(CultureInfo.InvariantCulture)),
                    new CustomField("customfield6", (Convert.ToInt32(Amount.BaseAmount*100)).ToString(CultureInfo.InvariantCulture)),
                    new CustomField("customfield10", Property.PropertyCode)
                };

            try
            {
                var CreditCardResponse = CreditCardPayment.ProcessCreditCard(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, IsTestTransaction, Amount.TotalAmount, CreditCardNumber, GetExpirationDateString(),
                                                                             CvvCode, NameOnCard, CustomFields);

                if (CreditCardResponse.Result != GeneralResponseResult.Success)
                {
                    Response.Result = (int)PaymentResponseResult.PaymentFailed;
                }
                else
                {
                    //cakel:BUGID00213 - ACH INSERT
                    var property = new EfxFramework.Property(PropertyId);
                    
                    if (property.GroupSettlementPaymentFlag == false)
                    {
                        
                        //For Properties who want an individual payment processed at the time the payment occurs
                        //negative amount added for credit to property
                        decimal PropertyPaymentAmount = (Convert.ToDecimal(Amount.BaseAmount)) * -1;
                        var ACH_Validate =
                        EfxFramework.PaymentMethods.Ach.AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword, EfxSettings.CcSettlementAchId, 
                            PropertyPaymentAmount, property.CCDepositRoutingNumber, property.CCDepositAccountNumber, "Checking",
                             property.PropertyName, property.PropertyName, property.StreetAddress, "", property.City, property.DisplayStateProvince, property.PostalCode, "");

                        //cakel: Need to Add code for response from ACH DB - This will send email to manager about the ACH insert failure.
                        var ACHDB_Result = ACH_Validate.Result;
                        if (ACHDB_Result != 0)
                        {
                            DateTime TransDate = DateTime.Now;
                            string _result = ACH_Validate.Result.ToString();
                            SendAchErrorEmail(_result, property.PropertyName, property.PropertyName, "Public API", TransDate.ToString() ,ACH_Validate.TransactionId);
                        }

                    }


                    Response.Result = (int)PaymentResponseResult.Success;

                }

                Response.Message = CreditCardResponse.ResponseMessage;
                Response.TransactionAmount = Amount.TotalAmount;
                Response.TransactionType = TransactionType.CreditCard.ToString();
                Response.TransactionId = CreditCardResponse.TransactionId;

                Response.TransactionId = LogTransaction(Amount, Response);

                if (PublicResidentId.HasValue && Response.Result == (int) PaymentResponseResult.Success)
                    LogBalanceDue();
            }
            catch
            {
                Response.Result = (int) PaymentResponseResult.GeneralFailure;
                Response.Message = PaymentResponseMessage.GeneralFailure;
            }

            return Response;
        }

        //cakel: BUGID00213 - Sends Email to Support when ACH DB insert fails 
        private static void SendAchErrorEmail(string Transtatus, string propertyName, string Firstname, string Lastname, string transDate, string transID)
        {
            string MailBody = "<p>An error occured when trying to insert record into ACH DB for submission.  Please check the properties Credit Card Account on file.</p><br />";
            MailBody += "Status: " + Transtatus + "<br />";
            MailBody += "Property Name: " + propertyName + "<br />";
            MailBody += "Payer First Name: " + Firstname + "<br />";
            MailBody += "Payer Last Name: " + Lastname + "<br />";
            MailBody += "Transaction Date: " + transDate + "<br />";
            MailBody += "Transaction ID: " + transID + "<br />";

            MailMessage ACH_ErrorMail = new MailMessage();
            ACH_ErrorMail.Subject = "ACH DB Insert Error";
            ACH_ErrorMail.Body = MailBody;
            ACH_ErrorMail.From = new MailAddress("support@rentpaidonline.com", "RPO Support");
            ACH_ErrorMail.To.Add(new MailAddress(EfxSettings.SupportEmail, "RPO Support Admin"));
            ACH_ErrorMail.IsBodyHtml = true;
            SmtpClient RpoSmtp = new SmtpClient();

            //Salcedo - 4/3/2015 - add support for dynamic host (mainly for testing)
            RpoSmtp.Host = EfxSettings.SmtpHost;

            RpoSmtp.Send(ACH_ErrorMail);

        }

        protected override bool ValidatePaymentRequest(out OneTimePaymentResponse response)
        {
            if (!base.ValidatePaymentRequest(out response))
                return false;

            if (String.IsNullOrEmpty(NameOnCard))
            {
                response.Result = (int) PaymentResponseResult.NameOnCardRequired;
                response.Message = PaymentResponseMessage.NameOnCardRequired;
                return false;
            }

            if (String.IsNullOrEmpty(CreditCardNumber))
            {
                response.Result = (int) PaymentResponseResult.CreditCardNumberRequired;
                response.Message = PaymentResponseMessage.CreditCardNumberRequired;
                return false;
            }

            if (DateTime.UtcNow.AddMonths(-1) > CreditCardExpiration)
            {
                response.Result = (int) PaymentResponseResult.CreditCardExpired;
                response.Message = PaymentResponseMessage.CreditCardExpired;
                return false;
            }

            if (String.IsNullOrEmpty(CvvCode))
            {
                response.Result = (int) PaymentResponseResult.CvvCodeRequired;
                response.Message = PaymentResponseMessage.CvvCodeRequired;
                return false;
            }

            return true;
        }

        protected override int? SetPayment(PaymentAmount amount, OneTimePaymentResponse response)
        {
            if (!PublicResidentId.HasValue || PublicResidentId.Value < 1)
                return null;

            var Resident = new Renter(Renter.GetRenterIdByPublicRenterId(PublicResidentId.Value));

            if (Resident.RenterId < 1 || (!Resident.PayerId.HasValue || Resident.PayerId.Value < 1))
                return null;

            var Status = response.Result == (int) PaymentResponseResult.Success ? PaymentStatus.Processing : PaymentStatus.Declined;




            if ((PaymentCategory) PaymentCategory == Payment.PaymentCategory.Rent)
                return EfxFramework.Payment.Set(EfxFramework.Payment.BuildPayment(Resident.RenterId, Resident.PayerId.Value, PaymentType.CreditCard, null, null, NameOnCard, CreditCardNumber,
                    GetExpirationDateString(), amount, ((PaymentCategory) PaymentCategory).ToString(), null, null, null, null, response.TransactionId, false, null,
                    response.Result.ToString(CultureInfo.InvariantCulture), response.Message, null, EfxFramework.PaymentChannel.Api, Status));


            return EfxFramework.Payment.Set(EfxFramework.Payment.BuildPayment(Resident.RenterId, Resident.PayerId.Value, PaymentType.CreditCard, null, null, NameOnCard, CreditCardNumber,
                    GetExpirationDateString(), null, null, null, null, amount, ((PaymentCategory) PaymentCategory).ToString(), response.TransactionId, false, null,
                    response.Result.ToString(CultureInfo.InvariantCulture), response.Message, null, EfxFramework.PaymentChannel.Api, Status));
        }

        protected override string SetTransaction(int? paymentId, OneTimePaymentResponse response)
        {

            //cakel: BUGID00213 - Transaction -- Added flag to insert into payment table "CCAchSettledFlag" to Payment.Set
            var property = new EfxFramework.Property(PropertyId);
            bool GroupSettlementPaymentFlag = property.GroupSettlementPaymentFlag;
            bool CCAchSettled = false;
            if (GroupSettlementPaymentFlag == true)
            {
                CCAchSettled = false;
            }
            else
            {
                CCAchSettled = true;
            }


            var StateProvinceId = GetStateProvinceId(State);

            var Transaction = new EfxFramework.Transaction
            {
                ExternalTransactionId = response.TransactionId,
                PaymentTypeId = (int) PaymentType.CreditCard,
                FirstName = FirstName,
                LastName = LastName,
                StreetAddress = Address1,
                StreetAddress2 = Address2,
                City = City,
                StateProvinceId = StateProvinceId.HasValue ? StateProvinceId.Value : 1,
                PhoneNumber = TelephoneNumber,
                PostalCode = PostalCode,
                EmailAddress = EmailAddress,
                Memo = PaymentMemo,
                PaymentAmount = PaymentAmount,
                ConvenienceFeeAmount = ConvenienceFeeAmount,
                CreditCardHolderName = NameOnCard,
                CreditCardAccountNumber = CreditCardNumber,
                CreditCardExpirationMonth = CreditCardExpiration.Month,
                CreditCardExpirationYear = CreditCardExpiration.Year,
                BankAccountNumber = null,
                BankRoutingNumber = null,
                PaymentId = paymentId,
                PaymentStatusId = response.Result == (int) PaymentResponseResult.Success ? (int) PaymentStatus.Processing : (int) PaymentStatus.Declined,
                ResponseResult = response.Result,
                ResponseMessage = response.Message,
                TransactionDate = response.TransactionDate,
                PropertyId = PropertyId,
                CCAchSettledFlag = CCAchSettled
            };

            return new EfxFramework.Transaction(EfxFramework.Transaction.Set(Transaction)).InternalTransactionId;
        }

        private string GetExpirationDateString()
        {
            var Month = CreditCardExpiration.Month > 9 ? CreditCardExpiration.Month.ToString(CultureInfo.InvariantCulture) : String.Format("0{0}", CreditCardExpiration.Month.ToString(CultureInfo.InvariantCulture));
            var Year = CreditCardExpiration.Year.ToString(CultureInfo.InvariantCulture).Substring(2, 2);

            return String.Format("{0}{1}", Month, Year);
        }
    }
}
