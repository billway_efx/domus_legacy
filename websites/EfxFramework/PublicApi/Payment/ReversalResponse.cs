﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Payment
{
    [DataContract]
    public class ReversalResponse : ApiBaseResponse
    {
        [DataMember]
        public decimal TransactionAmount { get; set; }

        [DataMember]
        public int TransactionType { get; set; }

        [DataMember]
        public string TransactionId { get; set; }
    }
}
