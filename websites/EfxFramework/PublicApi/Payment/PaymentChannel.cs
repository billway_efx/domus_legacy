﻿namespace EfxFramework.PublicApi.Payment
{
    public enum PaymentChannel
    {
        Web = 1,
        Mobile = 2,
        Text = 3,
        Ivr = 4,
        Api = 5
    }
}
