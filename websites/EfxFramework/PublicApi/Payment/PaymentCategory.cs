﻿namespace EfxFramework.PublicApi.Payment
{
    public enum PaymentCategory
    {
        Rent = 1,
        SecurityDeposit = 2,
        MonthlyFee = 3
    }
}
