﻿using EfxFramework.PaymentMethods;
using EfxFramework.PaymentMethods.Ach;
using Mb2x.ExtensionMethods;
using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Payment
{
    [DataContract]
    public class OneTimeAchPaymentRequest : OneTimePaymentRequest
    {
        [DataMember(IsRequired = true)]
        public string AccountNumber { get; set; }

        [DataMember(IsRequired = true)]
        public string RoutingNumber { get; set; }

        [DataMember(IsRequired = true)]
        public int BankAccountType { get; set; }

        public override OneTimePaymentResponse ProcessPayment()
        {
            if (!IsAuthenticatedUser())
                return new OneTimePaymentResponse {TransactionDate = DateTime.UtcNow, Result = (int) PaymentResponseResult.InvalidLogin, Message = PaymentResponseMessage.InvalidLogin};

            OneTimePaymentResponse Response;

            if (!ValidatePaymentRequest(out Response))
                return Response;

            string AchClientId;

            if (!GetAchClientId(out AchClientId))
            {
                Response.Result = (int)PaymentResponseResult.AchAccountNotFound;
                Response.Message = PaymentResponseMessage.AchAccountNotFound;
                return Response;
            }

            var Amount = new PaymentAmount { BaseAmount = PaymentAmount };

            if (ConvenienceFeeAmount.HasValue && ConvenienceFeeAmount.Value > 0.00M)
            {
                Amount.FeeAmount = ConvenienceFeeAmount.Value;
                Amount.FeeDescription = "Convenience Fee";
            }

            try
            {
                var AchPaymentResponse = AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword, AchClientId, Amount.TotalAmount, RoutingNumber, AccountNumber, GetBankAccountType(),
                                                                      FirstName, LastName, Address1, Address2, City, State, PostalCode, TelephoneNumber);

                if (AchPaymentResponse.Result != GeneralResponseResult.Success)
                    Response.Result = (int) PaymentResponseResult.PaymentFailed;
                else
                    Response.Result = (int) PaymentResponseResult.Success;

                Response.Message = AchPaymentResponse.ResponseDescription;
                Response.TransactionAmount = Amount.TotalAmount;
                Response.TransactionType = TransactionType.Ach.ToString();
                Response.TransactionId = AchPaymentResponse.TransactionId;

                Response.TransactionId = LogTransaction(Amount, Response);

                if (PublicResidentId.HasValue && Response.Result == (int) PaymentResponseResult.Success)
                    LogBalanceDue();
            }
            catch
            {
                Response.Result = (int) PaymentResponseResult.GeneralFailure;
                Response.Message = PaymentResponseMessage.GeneralFailure;
            }

            return Response;
        }

        protected override bool ValidatePaymentRequest(out OneTimePaymentResponse response)
        {
            if (!base.ValidatePaymentRequest(out response))
                return false;

            if (PublicResidentId.HasValue && PublicResidentId.Value > 0)
            {
                var ResidentId = Renter.GetRenterIdByPublicRenterId(PublicResidentId.Value);

                if (!ResidentId.HasValue || ResidentId.Value < 1)
                {
                    response.Result = (int)PaymentResponseResult.ResidentNotFound;
                    response.Message = PaymentResponseMessage.ResidentNotFound;
                    return false;
                }

                var Resident = new Renter(ResidentId);

                if (Resident.AcceptedPaymentTypeId == 3)
                {
                    response.Result = (int)PaymentResponseResult.CashEquivalentRequired;
                    response.Message = PaymentResponseMessage.CashEquivalentRequired;
                    return false;
                }
            }

            if (String.IsNullOrEmpty(AccountNumber))
            {
                response.Result = (int) PaymentResponseResult.AccountNumberRequired;
                response.Message = PaymentResponseMessage.AccountNumberRequired;
                return false;   
            }

            if (String.IsNullOrEmpty(RoutingNumber))
            {
                response.Result = (int) PaymentResponseResult.RoutingNumberRequired;
                response.Message = PaymentResponseMessage.RoutingNumberRequired;
                return false;
            }

            if (!RoutingNumber.IsValidBankRoutingNumber())
            {
                response.Result = (int) PaymentResponseResult.InvalidRoutingNumber;
                response.Message = PaymentResponseMessage.InvalidRoutingNumber;
                return false;
            }

            if (BankAccountType < 1 || BankAccountType > 2)
            {
                response.Result = (int)PaymentResponseResult.AccountTypeRequired;
                response.Message = PaymentResponseMessage.AccountTypeRequired;
                return false;
            }

            return true;
        }

        private bool GetAchClientId(out string achClientId)
        {
            var Category = (PaymentCategory) PaymentCategory;
            var Property = new EfxFramework.Property(PropertyId);

            switch (Category)
            {
                case Payment.PaymentCategory.Rent:
                    achClientId = Property.RentalAccountAchClientId;
                    break;

                case Payment.PaymentCategory.SecurityDeposit:
                    achClientId = Property.SecurityAccountAchClientId;
                    break;

                case Payment.PaymentCategory.MonthlyFee:
                    achClientId = Property.FeeAccountAchClientId;
                    break;

                default:
                    achClientId = null;
                    break;
            }

            return !String.IsNullOrEmpty(achClientId);
        }

        private string GetBankAccountType()
        {
            return BankAccountType == 1 ? "CHECKING" : "SAVINGS";
        }

        protected override int? SetPayment(PaymentAmount amount, OneTimePaymentResponse response)
        {
            if (!PublicResidentId.HasValue || PublicResidentId.Value < 1)
                return null;
    
            var Resident = new Renter(Renter.GetRenterIdByPublicRenterId(PublicResidentId.Value));

            if (Resident.RenterId < 1 || (!Resident.PayerId.HasValue || Resident.PayerId.Value < 1))
                return null;

            var Status = response.Result == (int) PaymentResponseResult.Success ? PaymentStatus.Processing : PaymentStatus.Declined;


            if ((PaymentCategory) PaymentCategory == Payment.PaymentCategory.Rent)
                return EfxFramework.Payment.Set(EfxFramework.Payment.BuildPayment(Resident.RenterId, Resident.PayerId.Value, PaymentType.ECheck, AccountNumber, RoutingNumber, null, null, null, amount,
                    ((PaymentCategory) PaymentCategory).ToString(), null, null, null, null, response.TransactionId, false, null, response.Result.ToString(CultureInfo.InvariantCulture),
                    response.Message, null, EfxFramework.PaymentChannel.Api, Status));
                
            return EfxFramework.Payment.Set(EfxFramework.Payment.BuildPayment(Resident.RenterId, Resident.PayerId.Value, PaymentType.ECheck, AccountNumber, RoutingNumber, null, null, null, null,
                    null, null, null, amount, ((PaymentCategory)PaymentCategory).ToString(), response.TransactionId, false, null, response.Result.ToString(CultureInfo.InvariantCulture),
                    response.Message, null, EfxFramework.PaymentChannel.Api, Status));
        }

        protected override string SetTransaction(int? paymentId, OneTimePaymentResponse response)
        {

            //cakel: BUGID00213 - Transaction --Added flag to insert into payment table "CCAchSettledFlag" to Payment.Set
            var property = new EfxFramework.Property(PropertyId);
            bool GroupSettlementPaymentFlag = property.GroupSettlementPaymentFlag;
            bool CCAchSettled = false;
            if (GroupSettlementPaymentFlag == true)
            {
                CCAchSettled = false;
            }
            else
            {
                CCAchSettled = true;
            }

            var StateProvinceId = GetStateProvinceId(State);

            var Transaction = new EfxFramework.Transaction
                {
                    ExternalTransactionId = response.TransactionId,
                    PaymentTypeId = (int) PaymentType.ECheck,
                    FirstName = FirstName,
                    LastName = LastName,
                    StreetAddress = Address1,
                    StreetAddress2 = Address2,
                    City = City,
                    StateProvinceId = StateProvinceId.HasValue ? StateProvinceId.Value : 1,
                    PhoneNumber = TelephoneNumber,
                    PostalCode = PostalCode,
                    EmailAddress = EmailAddress,
                    Memo = PaymentMemo,
                    PaymentAmount = PaymentAmount,
                    ConvenienceFeeAmount = ConvenienceFeeAmount,
                    CreditCardHolderName = null,
                    CreditCardAccountNumber = null,
                    CreditCardExpirationMonth = null,
                    CreditCardExpirationYear = null,
                    BankAccountNumber = AccountNumber,
                    BankRoutingNumber = RoutingNumber,
                    BankAccountTypeId = BankAccountType,
                    PaymentId = paymentId,
                    PaymentStatusId = response.Result == (int) PaymentResponseResult.Success ? (int) PaymentStatus.Processing : (int) PaymentStatus.Declined,
                    ResponseResult = response.Result,
                    ResponseMessage = response.Message,
                    TransactionDate = response.TransactionDate,
                    PropertyId = PropertyId,
                    CCAchSettledFlag = CCAchSettled
                };

            return new EfxFramework.Transaction(EfxFramework.Transaction.Set(Transaction)).InternalTransactionId;
        }
    }
}
