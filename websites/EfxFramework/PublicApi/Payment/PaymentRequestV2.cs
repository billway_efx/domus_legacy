﻿using System;
using EfxFramework.PaymentMethods;

namespace EfxFramework.PublicApi.Payment
{
    public class PaymentRequestV2 : PaymentProcessingBase
    {
        /// <summary>
        /// If no resident use this constructor
        /// </summary>
        /// <param name="channel">Api Channel</param>
        /// <param name="propertyId">PropertyId of the Property</param>
        public PaymentRequestV2(EfxFramework.PaymentChannel channel, int propertyId)
            : base(channel, propertyId)
        {
            UseIvr = false;
        }

        /// <summary>
        /// If you have the RenterId already, use this constructor
        /// </summary>
        /// <param name="channel">Api Channel</param>
        /// <param name="propertyId">PropertyId of the Property</param>
        /// <param name="residentId">The RenterId of the Resident</param>
        public PaymentRequestV2(EfxFramework.PaymentChannel channel, int propertyId, int residentId)
            : base(channel, propertyId, residentId)
        {
            UseIvr = false;
        }

        /// <summary>
        /// If there is a resident use this constructor
        /// </summary>
        /// <param name="channel">Api Channel</param>
        /// <param name="propertyId">PropertyId of the Property</param>
        /// <param name="publicResidentId">The PublicResidentId of the Resident</param>
        public PaymentRequestV2(EfxFramework.PaymentChannel channel, int propertyId, long publicResidentId)
            : base(channel, propertyId)
        {
            UseIvr = false;

            var Id = Renter.GetRenterIdByPublicRenterId(publicResidentId);
            if (Id.HasValue && Id.Value > 0)
                ResidentId = Id.Value;
        }

        //TODO: Override SendEmail to use the EmailAddress on the OneTimePaymentRequest Object
        

        public OneTimePaymentResponse ProcessPayment(CreditCardDetails ccDetails, PaymentAmount amount)
        {
            return ConvertBaseResponse(ProcessResidentPayment(ccDetails, amount));
        }

        public OneTimePaymentResponse ProcessPayment(EcheckDetails eCheckDetails, PaymentAmount amount)
        {
            return ConvertBaseResponse(ProcessResidentPayment(eCheckDetails, amount));
        }

        protected override bool Validate(out PaymentResponseBase response)
        {
            response = new PaymentResponseBase {TransactionDate = DateTime.UtcNow};

            if (ResidentId > 0)
                LeaseId = EfxFramework.Lease.GetRenterLeaseByRenterId(ResidentId).LeaseId;

            return true;
        }
        
        private static OneTimePaymentResponse ConvertBaseResponse(PaymentResponseBase response)
        {
            //TODO: Make a conversion method to convert PaymentResponseBase to OneTimePaymentResponse so as not to mess up the data contract
            return new OneTimePaymentResponse();
        }

        private static PaymentResponseBase ConvertOneTimeResponse(OneTimePaymentResponse response)
        {
            //TODO: Make a conversion method to convert OneTimePaymentResponse to PaymentResponseBase so as not to mess up the data contract
            return new PaymentResponseBase();
        }
    }
}
