﻿using EfxFramework.PaymentMethods;
using Mb2x.ExtensionMethods;
using System;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Payment
{
    [DataContract]
    public class RecurringAchPaymentRequest : RecurringPaymentRequest
    {
        [DataMember(IsRequired = true)]
        public string AccountNumber { get; set; }

        [DataMember(IsRequired = true)]
        public string RoutingNumber { get; set; }

        [DataMember(IsRequired = true)]
        public int BankAccountType { get; set; }

        public override RecurringPaymentResponse ProcessRecurringPayment()
        {
            if (!IsAuthenticatedUser())
                return new RecurringPaymentResponse{TransactionDate = DateTime.UtcNow, Result = (int)PaymentResponseResult.InvalidLogin, Message = PaymentResponseMessage.InvalidLogin};

            RecurringPaymentResponse Response;

            if (!ValidatePaymentRequest(out Response))
                return Response;

            var Amount = new PaymentAmount {BaseAmount = PaymentAmount};

            if (ConvenienceFeeAmount.HasValue && ConvenienceFeeAmount.Value > 0.00M)
            {
                Amount.FeeAmount = ConvenienceFeeAmount.Value;
                Amount.FeeDescription = "Convenience Fee";
            }

            var Resident = new Renter(Renter.GetRenterIdByPublicRenterId(PublicResidentId));

            if (Resident.RenterId < 1)
            {
                Response.Result = (int) PaymentResponseResult.ResidentNotFound;
                Response.Message = PaymentResponseMessage.ResidentNotFound;
                return Response;
            }

            try
            {
                var Payment = BuildAutoPayment(Resident, Amount, PaymentType.ECheck);
                Payment = new AutoPayment(AutoPayment.Set(Payment));

                if (Payment.AutoPaymentId < 1)
                {
                    Response.Result = (int) PaymentResponseResult.GeneralFailure;
                    Response.Message = PaymentResponseMessage.GeneralFailure;
                    return Response;
                }

                Response.Result = (int) PaymentResponseResult.Success;
                Response.Message = PaymentResponseMessage.Success;
                Response.TransactionAmount = PaymentAmount;
                Response.TransactionType = TransactionType.Ach.ToString();
            }
            catch
            {
                Response.Result = (int)PaymentResponseResult.GeneralFailure;
                Response.Message = PaymentResponseMessage.GeneralFailure;
            }

            return Response;
        }

        protected override bool ValidatePaymentRequest(out RecurringPaymentResponse response)
        {
            if (!base.ValidatePaymentRequest(out response))
                return false;

            if (String.IsNullOrEmpty(AccountNumber))
            {
                response.Result = (int)PaymentResponseResult.AccountNumberRequired;
                response.Message = PaymentResponseMessage.AccountNumberRequired;
                return false;
            }

            if (String.IsNullOrEmpty(RoutingNumber))
            {
                response.Result = (int)PaymentResponseResult.RoutingNumberRequired;
                response.Message = PaymentResponseMessage.RoutingNumberRequired;
                return false;
            }

            if (!RoutingNumber.IsValidBankRoutingNumber())
            {
                response.Result = (int)PaymentResponseResult.InvalidRoutingNumber;
                response.Message = PaymentResponseMessage.InvalidRoutingNumber;
                return false;
            }

            if (BankAccountType < 1 || BankAccountType > 2)
            {
                response.Result = (int)PaymentResponseResult.AccountTypeRequired;
                response.Message = PaymentResponseMessage.AccountTypeRequired;
                return false;
            }

            return true;
        }

        protected override int BuildPaymentMethod(int payerId)
        {
            var PayerAch = new PayerAch
                {
                    PayerId = payerId,
                    BankAccountNumber = AccountNumber,
                    BankRoutingNumber = RoutingNumber,
                    BankAccountTypeId = BankAccountType,
                    IsPrimary = true
                };

            return EfxFramework.PayerAch.Set(PayerAch);
        }
    }
}
