﻿using System.ComponentModel;

namespace EfxFramework.PublicApi.Payment
{
    public enum PaymentStatus
    {
        [Description("5")]
        Approved = 1,
        [Description("9")]
        Declined = 2,
        [Description("3")]
        Processing = 3,
        [Description("4")]
        Cleared = 4,
        [Description("2")]
        Pending = 5,
        [Description("6")]
        Returned = 6,
        [Description("7")]
        Refunded = 7,
        [Description("8")]
        Voided = 8,
        [Description("")]
        Working = 9,
        [Description("")]
        Processed = 10,
        [Description("")]
        PartiallyRefunded = 11


    }
}
