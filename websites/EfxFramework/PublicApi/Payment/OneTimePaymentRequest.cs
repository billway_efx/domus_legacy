﻿using System;
using System.Runtime.Serialization;
using EfxFramework.PaymentMethods;
using Mb2x.ExtensionMethods;

namespace EfxFramework.PublicApi.Payment
{
    [DataContract]
    public abstract class OneTimePaymentRequest : ApiBaseRequest
    {
        [DataMember]
        public long? PublicResidentId { get; set; }

        [DataMember(IsRequired = true)]
        public int PropertyId { get; set; }

        [DataMember(IsRequired = true)]
        public string FirstName { get; set; }

        [DataMember(IsRequired = true)]
        public string LastName { get; set; }

        [DataMember(IsRequired = true)]
        public string EmailAddress { get; set; }

        [DataMember(IsRequired = true)]
        public string Address1 { get; set; }

        [DataMember]
        public string Address2 { get; set; }

        [DataMember(IsRequired = true)]
        public string City { get; set; }

        [DataMember(IsRequired = true)]
        public string State { get; set; }

        [DataMember(IsRequired = true)]
        public string PostalCode { get; set; }

        [DataMember(IsRequired = true)]
        public string TelephoneNumber { get; set; }

        [DataMember]
        public string PaymentMemo { get; set; }

        [DataMember(IsRequired = true)]
        public int PaymentCategory { get; set; }

        [DataMember(IsRequired = true)]
        public decimal PaymentAmount { get; set; }

        [DataMember]
        public decimal? ConvenienceFeeAmount { get; set; }

        public abstract OneTimePaymentResponse ProcessPayment();
        protected abstract int? SetPayment(PaymentAmount amount, OneTimePaymentResponse response);
        protected abstract string SetTransaction(int? paymentId, OneTimePaymentResponse response);

        protected virtual void LogBalanceDue()
        {
            if (!PublicResidentId.HasValue || PublicResidentId.Value < 1)
                return;

            var ResidentId = Renter.GetRenterIdByPublicRenterId(PublicResidentId.Value);

            if (!ResidentId.HasValue || ResidentId.Value < 1)
                return;

            var Resident = new Renter(ResidentId.Value);

            if (Resident.RenterId < 1)
                return;

            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(Resident.RenterId);

            if (Lease.LeaseId < 1)
                return;

            Lease.CurrentBalanceDue -= PaymentAmount;
            Lease.CurrentBalanceDueLastUpdated = DateTime.UtcNow;
            //Lease.PaymentDueDate = Lease.PaymentDueDate.HasValue ? Lease.PaymentDueDate.Value.AddMonths(1) : DateTime.UtcNow.AddMonths(1);
            EfxFramework.Lease.Set(Lease);
        }

        protected virtual bool ValidatePaymentRequest(out OneTimePaymentResponse response)
        {
            response = new OneTimePaymentResponse {TransactionDate = DateTime.UtcNow};
            StateProvince StateProvince;

            if (PublicResidentId.HasValue && PublicResidentId.Value > 0)
            {
                var ResidentId = Renter.GetRenterIdByPublicRenterId(PublicResidentId.Value);

                if (!ResidentId.HasValue || ResidentId.Value < 1)
                {
                    response.Result = (int) PaymentResponseResult.ResidentNotFound;
                    response.Message = PaymentResponseMessage.ResidentNotFound;
                    return false;
                }

                var Resident = new Renter(ResidentId);

                if (Resident.AcceptedPaymentTypeId == 4)
                {
                    response.Result = (int)PaymentResponseResult.CannotAcceptPayment;
                    response.Message = PaymentResponseMessage.CannotAcceptPayment;
                    return false;
                }
            }

            if (PropertyId < 1)
            {
                response.Result = (int) PaymentResponseResult.InvalidPropertyId;
                response.Message = PaymentResponseMessage.InvalidPropertyId;
                return false;
            }

            var P = new EfxFramework.Property(PropertyId);

            if (!P.PropertyId.HasValue || P.PropertyId.Value < 1)
            {
                response.Result = (int) PaymentResponseResult.PropertyNotFound;
                response.Message = PaymentResponseMessage.PropertyNotFound;
                return false;
            }

            if (String.IsNullOrEmpty(FirstName))
            {
                response.Result = (int) PaymentResponseResult.FirstNameRequired;
                response.Message = PaymentResponseMessage.FirstNameRequired;
                return false;
            }

            if (String.IsNullOrEmpty(LastName))
            {
                response.Result = (int) PaymentResponseResult.LastNameRequired;
                response.Message = PaymentResponseMessage.LastNameRequired;
                return false;
            }

            if (String.IsNullOrEmpty(EmailAddress))
            {
                response.Result = (int) PaymentResponseResult.EmailAddressRequired;
                response.Message = PaymentResponseMessage.EmailAddressRequired;
                return false;
            }

            if (!EmailAddress.IsValidEmailAddress())
            {
                response.Result = (int) PaymentResponseResult.InvalidEmail;
                response.Message = PaymentResponseMessage.InvalidEmail;
                return false;
            }

            if (String.IsNullOrEmpty(Address1))
            {
                response.Result = (int) PaymentResponseResult.Address1Required;
                response.Message = PaymentResponseMessage.Address1Required;
                return false;
            }

            if (String.IsNullOrEmpty(City))
            {
                response.Result = (int) PaymentResponseResult.CityRequired;
                response.Message = PaymentResponseMessage.CityRequired;
                return false;
            }

            if (String.IsNullOrEmpty(State))
            {
                response.Result = (int) PaymentResponseResult.StateRequired;
                response.Message = PaymentResponseMessage.StateRequired;
                return false;
            }

            if (!Enum.TryParse(State, out StateProvince))
            {
                response.Result = (int) PaymentResponseResult.InvalidStateFormat;
                response.Message = PaymentResponseMessage.InvalidStateFormat;
                return false;
            }

            if (String.IsNullOrEmpty(PostalCode))
            {
                response.Result = (int) PaymentResponseResult.PostalCodeRequired;
                response.Message = PaymentResponseMessage.PostalCodeRequired;
                return false;
            }

            if (String.IsNullOrEmpty(TelephoneNumber))
            {
                response.Result = (int) PaymentResponseResult.TelephoneNumberRequired;
                response.Message = PaymentResponseMessage.TelephoneNumberRequired;
                return false;
            }

            if (PaymentAmount < .01M)
            {
                response.Result = (int) PaymentResponseResult.PaymentAmountRequired;
                response.Message = PaymentResponseMessage.PaymentAmountRequired;
                return false;
            }

            if (PaymentCategory < 1 || PaymentCategory > 3)
            {
                response.Result = (int) PaymentResponseResult.InvalidPaymentCategory;
                response.Message = PaymentResponseMessage.InvalidPaymentCategory;
                return false;
            }

            return true;
        }

        protected virtual string LogTransaction(PaymentAmount amount, OneTimePaymentResponse response)
        {
            return SetTransaction(SetPayment(amount, response), response);
            
        }
    }
}
