﻿namespace EfxFramework.PublicApi.Payment
{
    public enum TransactionType
    {
        CreditCard = 1,
        Ach = 2
    }
}
