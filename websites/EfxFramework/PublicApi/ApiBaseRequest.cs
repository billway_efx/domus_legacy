﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using System.Linq;

namespace EfxFramework.PublicApi
{
    [DataContract]
    public abstract class ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public string Username { get; set; }

        [DataMember(IsRequired = true)]
        public string Password { get; set; }

        protected virtual bool IsAuthenticatedUser()
        {
            //Salcedo - 11/10/2015 - this function was bombing downstream if the user doesn't exist
            var TheUser = new ApiUser(Username);
            if (TheUser != null && TheUser.Username != null)
                return TheUser.Authenticate(Password);
            else
                return false;

            //return new ApiUser(Username).Authenticate(Password);
        }

        protected virtual bool IsUserAuthorized(int? propertyId)
        {
            var User = new ApiUser(Username);

            if (propertyId.HasValue && propertyId.Value > 0)
            {
                if (User.UserProperties.All(u => u.PropertyId != propertyId))
                    return false;
            }

            return true;
        }

        protected virtual bool IsUserAuthorized(List<int?> propertyIds)
        {
            var User = new ApiUser(Username);
            return propertyIds.Any(p => User.UserProperties.Any(u => u.PropertyId == p));
        }

        //TODO: Factor out in favor of the new address class
        protected static int? GetStateProvinceId(string value)
        {
            if (String.IsNullOrEmpty(value))
                return null;

            StateProvince State;

            if (!Enum.TryParse(value, out State))
                return null;

            return (int)State;
        }

        //TODO: Factor out in favor of the new address class
        protected static string GetState(int? stateProvinceId)
        {
            if (!stateProvinceId.HasValue || stateProvinceId.Value < 1 || stateProvinceId.Value > 50)
                return StateProvince.AK.ToString();

            return ((StateProvince)stateProvinceId).ToString();
        }
    }
}
