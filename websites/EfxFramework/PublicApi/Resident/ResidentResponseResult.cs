﻿namespace EfxFramework.PublicApi.Resident
{
    public enum ResidentResponseResult
    {
        Success = 0,
        GeneralFailure = 2,
        InvalidLogin = 3,
        ResidentNotFound = 9999,
        MoreInfoRequired = 9998,
        FirstNameRequired = 9997,
        LastNameRequired = 9996,
        EmailAddressRequired = 9995,
        DuplicateResidentFound = 9994,
        ResidentHasTransactions = 9993,
        PublicResidentIdRequired = 9992,
        InvalidPropertyId = 9991,
        PropertyNotFound = 9990,
        InvalidStateFormat = 9989,
        PropertyAssignmentFailed = 9988,
        InvalidEmail = 9987,
        PasswordRequired = 9986,
        UserNotAuthorized = 9985
    }

    public static class ResidentResponseMessage
    {
        public static string Success { get { return "Successfully completed the operation"; } }
        public static string GeneralFailure { get { return "A general failure occurred, please check your inputs and try the operation again"; } }
        public static string InvalidLogin { get { return "Invalid login credentials were supplied.  Please check your information and try again"; } }
        public static string ResidentNotFound { get { return "No Resident found with the provided information"; } }
        public static string MoreInfoRequired { get { return "More information is required to find a Resident"; } }
        public static string FirstNameRequired { get { return "First Name is required"; } }
        public static string LastNameRequired { get { return "Last Name is required"; } }
        public static string EmailAddressRequired { get { return "Email Address is required"; } }
        public static string DuplicateResidentfound { get { return "A Resident with the same email address already exists"; } }
        public static string ResidentHasTransactions { get { return "Cannot delete a Resident with transactions associated with them"; } }
        public static string PublicResidentIdRequired { get { return "Public Resident ID is required to complete the transaction"; } }
        public static string InvalidPropertyId { get { return "Property ID must be a non-negative integer greater than zero"; } }
        public static string PropertyNotFound { get { return "No Property found with the specified ID"; } }
        public static string InvalidStateFormat { get { return "State must be in the two character state code format"; } }
        public static string PropertyAssignmentAndOperationFailed { get { return "The Property Assignment failed causing the Resident Creation to be rejected, please check your information and try again"; } }
        public static string PropertyAssignmentFailedOperationSuccess { get { return "Resident successfully updated, but the Property assignment failed, please check your information and try again"; } }
        public static string InvalidEmail { get { return "Email address is not valid"; } }
        public static string PasswordRequired { get { return "A Password is required"; } }
        public static string UserNotAuthorized { get { return "The logged in user is not authorized to perform this operation"; } }
    }
}
