﻿using System;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using System.Linq;

namespace EfxFramework.PublicApi.Resident
{
    [DataContract]
    public class DeleteResidentRequest : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public long PublicResidentId { get; set; }

        public DeleteResidentResponse DeleteResident()
        {
            if (!IsAuthenticatedUser())
                return new DeleteResidentResponse { TransactionDate = DateTime.UtcNow, Result = (int) ResidentResponseResult.InvalidLogin, Message = ResidentResponseMessage.InvalidLogin };

            DeleteResidentResponse Response;
            var ResidentId = GetResidentId(out Response);

            if (ResidentId == 0)
                return Response;

            if (!IsUserAuthorized(EfxFramework.Property.GetPropertyListByRenterId(ResidentId).Select(p => p.PropertyId).ToList()))
                return new DeleteResidentResponse { TransactionDate = DateTime.UtcNow, Result = (int)ResidentResponseResult.UserNotAuthorized, Message = ResidentResponseMessage.UserNotAuthorized };

            try
            {
                Renter.DeleteRenterByRenterId(ResidentId, "Public API Delete Resident Service");
                Response.Result = (int) ResidentResponseResult.Success;
                Response.Message = ResidentResponseMessage.Success;
            }
            catch (SqlException Ex)
            {
                if (Ex.Number == 547)
                {
                    Response.Result = (int) ResidentResponseResult.ResidentHasTransactions;
                    Response.Message = ResidentResponseMessage.ResidentHasTransactions;
                }
                else
                {
                    Response.Result = (int) ResidentResponseResult.GeneralFailure;
                    Response.Message = ResidentResponseMessage.GeneralFailure;
                }
            }
            catch
            {
                Response.Result = (int)ResidentResponseResult.GeneralFailure;
                Response.Message = ResidentResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private int GetResidentId(out DeleteResidentResponse response)
        {
            response = new DeleteResidentResponse{TransactionDate = DateTime.UtcNow};

            if (PublicResidentId < 1)
            {
                response.Result = (int) ResidentResponseResult.PublicResidentIdRequired;
                response.Message = ResidentResponseMessage.PublicResidentIdRequired;
                return 0;
            }
            
            var RenterId = Renter.GetRenterIdByPublicRenterId(PublicResidentId);

            if (!RenterId.HasValue || RenterId.Value < 1)
            {
                response.Result = (int) ResidentResponseResult.ResidentNotFound;
                response.Message = ResidentResponseMessage.ResidentNotFound;
                return 0;
            }

            return RenterId.Value;
        }
    }
}