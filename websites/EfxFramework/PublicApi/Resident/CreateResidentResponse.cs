﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Resident
{
    [DataContract]
    public class CreateResidentResponse : ApiBaseResponse
    {
        [DataMember]
        public long PublicResidentId { get; set; }
    }
}