﻿using Mb2x.ExtensionMethods;
using System;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Resident
{
    [DataContract]
    public class CreateResidentRequest : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public Resident Resident { get; set; }

        [DataMember(IsRequired = true)]
        public int PropertyId { get; set; }

        public CreateResidentResponse CreateResident()
        {
            if (!IsAuthenticatedUser())
                return new CreateResidentResponse {TransactionDate = DateTime.UtcNow, Result = (int) ResidentResponseResult.InvalidLogin, Message = ResidentResponseMessage.InvalidLogin};

            CreateResidentResponse Response;

            if (!ValidateResident(out Response))
                return Response;

            if (!IsUserAuthorized(PropertyId))
                return new CreateResidentResponse { TransactionDate = DateTime.UtcNow, Result = (int)ResidentResponseResult.UserNotAuthorized, Message = ResidentResponseMessage.UserNotAuthorized };

            var Renter = new Renter
                {
                    Prefix = Resident.Prefix,
                    FirstName = Resident.FirstName,
                    MiddleName = Resident.MiddleName,
                    LastName = Resident.LastName,
                    Suffix = Resident.Suffix,
                    MainPhoneNumber = Resident.MainPhoneNumber,
                    MobilePhoneNumber = Resident.MobilePhoneNumber,
                    PrimaryEmailAddress = Resident.PrimaryEmailAddress,
                    StreetAddress = Resident.StreetAddress,
                    Unit = Resident.StreetAddress2,
                    City = Resident.City,
                    StateProvinceId = GetStateProvinceId(Resident.State),
                    PostalCode = Resident.PostalCode,
                    IsActive = true
                };

            Renter.SetPassword(Resident.Password);

            try
            {
                Renter = new Renter(Renter.Set(Renter, "Public API Create Resident Service"));
                Response = AssignResidentToProperty(Renter, Response);

                if (Response.Result == (int) ResidentResponseResult.PropertyAssignmentFailed || Response.Result == (int) ResidentResponseResult.GeneralFailure)
                    return Response;

                Response.Result = (int) ResidentResponseResult.Success;
                Response.Message = ResidentResponseMessage.Success;
                Response.PublicResidentId = Renter.PublicRenterId;
            }
            catch
            {
                Response.Result = (int) ResidentResponseResult.GeneralFailure;
                Response.Message = ResidentResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private CreateResidentResponse AssignResidentToProperty(Renter renter, CreateResidentResponse response)
        {
            if (renter.RenterId < 1)
            {
                response.Result = (int) ResidentResponseResult.GeneralFailure;
                response.Message = ResidentResponseMessage.GeneralFailure;
                return response;
            }

            try
            {
                Renter.AssignRenterToProperty(PropertyId, renter.RenterId);
            }
            catch
            {
                Renter.DeleteRenterByRenterId(renter.RenterId, "Public API Create Resident Service");
                response.Result = (int) ResidentResponseResult.PropertyAssignmentFailed;
                response.Message = ResidentResponseMessage.PropertyAssignmentAndOperationFailed;
            }

            return response;
        }

        private bool ValidateResident(out CreateResidentResponse response)
        {
            response = new CreateResidentResponse {TransactionDate = DateTime.UtcNow};
            StateProvince State;

            if (Resident == null)
            {
                response.Result = (int) ResidentResponseResult.MoreInfoRequired;
                response.Message = ResidentResponseMessage.MoreInfoRequired;
                return false;
            }

            if (String.IsNullOrEmpty(Resident.FirstName))
            {
                response.Result = (int) ResidentResponseResult.FirstNameRequired;
                response.Message = ResidentResponseMessage.FirstNameRequired;
                return false;
            }

            if (String.IsNullOrEmpty(Resident.LastName))
            {
                response.Result = (int) ResidentResponseResult.LastNameRequired;
                response.Message = ResidentResponseMessage.LastNameRequired;
                return false;
            }

            if (String.IsNullOrEmpty(Resident.PrimaryEmailAddress))
            {
                response.Result = (int) ResidentResponseResult.EmailAddressRequired;
                response.Message = ResidentResponseMessage.EmailAddressRequired;
                return false;
            }

            if (!Resident.PrimaryEmailAddress.IsValidEmailAddress())
            {
                response.Result = (int) ResidentResponseResult.InvalidEmail;
                response.Message = ResidentResponseMessage.InvalidEmail;
                return false;
            }

            if (String.IsNullOrEmpty(Resident.Password))
            {
                response.Result = (int) ResidentResponseResult.PasswordRequired;
                response.Message = ResidentResponseMessage.PasswordRequired;
                return false;
            }

            if (!String.IsNullOrEmpty(Resident.State) && !Enum.TryParse(Resident.State, out State))
            {
                response.Result = (int) ResidentResponseResult.InvalidStateFormat;
                response.Message = ResidentResponseMessage.InvalidStateFormat;
                return false;
            }

            if (new Renter(Resident.PrimaryEmailAddress).RenterId > 0)
            {
                response.Result = (int) ResidentResponseResult.DuplicateResidentFound;
                response.Message = ResidentResponseMessage.DuplicateResidentfound;
                return false;
            }

            if (PropertyId < 1)
            {
                response.Result = (int) ResidentResponseResult.InvalidPropertyId;
                response.Message = ResidentResponseMessage.InvalidPropertyId;
                return false;
            }

            var Property = new EfxFramework.Property(PropertyId);

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                response.Result = (int) ResidentResponseResult.PropertyNotFound;
                response.Message = ResidentResponseMessage.PropertyNotFound;
                return false;
            }

            return true;
        }
    }
}