﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Resident
{
    [DataContract]
    public class UpdateResidentRequest : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public Resident Resident { get; set; }

        [DataMember]
        public int PropertyId { get; set; }

        public UpdateResidentResponse UpdateResident()
        {
            if (!IsAuthenticatedUser())
                return new UpdateResidentResponse { TransactionDate = DateTime.UtcNow, Result = (int) ResidentResponseResult.InvalidLogin, Message = ResidentResponseMessage.InvalidLogin };

            UpdateResidentResponse Response;

            if (!ValidateResident(out Response))
                return Response;

            var RenterId = EfxFramework.Renter.GetRenterIdByPublicRenterId(Resident.PublicResidentId);

            if (!RenterId.HasValue || RenterId.Value < 1)
            {
                Response.Result = (int) ResidentResponseResult.ResidentNotFound;
                Response.Message = ResidentResponseMessage.ResidentNotFound;
                return Response;
            }

            if (!IsUserAuthorized(EfxFramework.Property.GetPropertyListByRenterId(RenterId.Value).Select(p => p.PropertyId).ToList()))
                return new UpdateResidentResponse { TransactionDate = DateTime.UtcNow, Result = (int)ResidentResponseResult.UserNotAuthorized, Message = ResidentResponseMessage.UserNotAuthorized };

            if (PropertyId > 0)
            {
                if (!IsUserAuthorized(PropertyId))
                    return new UpdateResidentResponse { TransactionDate = DateTime.UtcNow, Result = (int)ResidentResponseResult.UserNotAuthorized, Message = ResidentResponseMessage.UserNotAuthorized };
            }

            var Renter = new Renter(RenterId.Value)
            {
                Prefix = Resident.Prefix,
                FirstName = Resident.FirstName,
                MiddleName = Resident.MiddleName,
                LastName = Resident.LastName,
                Suffix = Resident.Suffix,
                MainPhoneNumber = Resident.MainPhoneNumber,
                MobilePhoneNumber = Resident.MobilePhoneNumber,
                PrimaryEmailAddress = Resident.PrimaryEmailAddress,
                StreetAddress = Resident.StreetAddress,
                Unit = Resident.StreetAddress2,
                City = Resident.City,
                StateProvinceId = GetStateProvinceId(Resident.State),
                PostalCode = Resident.PostalCode
            };

            if (!String.IsNullOrEmpty(Resident.Password))
            {
                Renter.SetPassword(Resident.Password);
            }
            else
            {
                var TempRenter = new Renter(Renter.RenterId);
                Renter.PasswordHash = TempRenter.PasswordHash;
                Renter.Salt = TempRenter.Salt;
            }

            try
            {
                Renter = new Renter(Renter.Set(Renter, "Public API Update Resident Service"));

                if (Renter.RenterId < 1)
                {
                    Response.Result = (int) ResidentResponseResult.GeneralFailure;
                    Response.Message = ResidentResponseMessage.GeneralFailure;
                    return Response;
                }

                Response.Result = (int) ResidentResponseResult.Success;
                Response.Message = AssignResidentToProperty(Renter) ? ResidentResponseMessage.Success : ResidentResponseMessage.PropertyAssignmentFailedOperationSuccess;
                Response.PublicResidentId = Renter.PublicRenterId;
            }
            catch
            {
                Response.Result = (int) ResidentResponseResult.GeneralFailure;
                Response.Message = ResidentResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool AssignResidentToProperty(Renter renter)
        {
            if (PropertyId > 0)
            {
                try
                {
                    Renter.AssignRenterToProperty(PropertyId, renter.RenterId);
                }
                catch
                {
                    return false;
                }
            }

            return true;
        }

        private bool ValidateResident(out UpdateResidentResponse response)
        {
            response = new UpdateResidentResponse {TransactionDate = DateTime.UtcNow};
            StateProvince State;

            if (Resident == null)
            {
                response.Result = (int) ResidentResponseResult.MoreInfoRequired;
                response.Message = ResidentResponseMessage.MoreInfoRequired;
                return false;
            }

            if (Resident.PublicResidentId < 1)
            {
                response.Result = (int) ResidentResponseResult.PublicResidentIdRequired;
                response.Message = ResidentResponseMessage.PublicResidentIdRequired;
                return false;
            }

            if (Renter.GetRenterIdByPublicRenterId(Resident.PublicResidentId) < 1)
            {
                response.Result = (int) ResidentResponseResult.ResidentNotFound;
                response.Message = ResidentResponseMessage.ResidentNotFound;
                return false;
            }

            if (String.IsNullOrEmpty(Resident.FirstName))
            {
                response.Result = (int) ResidentResponseResult.FirstNameRequired;
                response.Message = ResidentResponseMessage.FirstNameRequired;
                return false;
            }

            if (String.IsNullOrEmpty(Resident.LastName))
            {
                response.Result = (int) ResidentResponseResult.LastNameRequired;
                response.Message = ResidentResponseMessage.LastNameRequired;
                return false;
            }

            if (String.IsNullOrEmpty(Resident.PrimaryEmailAddress))
            {
                response.Result = (int) ResidentResponseResult.EmailAddressRequired;
                response.Message = ResidentResponseMessage.EmailAddressRequired;
                return false;
            }

            if (!String.IsNullOrEmpty(Resident.State) && !Enum.TryParse(Resident.State, out State))
            {
                response.Result = (int) ResidentResponseResult.InvalidStateFormat;
                response.Message = ResidentResponseMessage.InvalidStateFormat;
                return false;
            }

            if (PropertyId > 0)
            {
                var Property = new EfxFramework.Property(PropertyId);

                if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                {
                    response.Result = (int) ResidentResponseResult.PropertyNotFound;
                    response.Message = ResidentResponseMessage.PropertyNotFound;
                    return false;
                }
            }

            return true;
        }
    }
}