﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Resident
{
    [DataContract]
    public class GetResidentRequest : ApiBaseRequest
    {
        [DataMember]
        public long PublicResidentId { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        public GetResidentResponse GetResident()
        {
            if (!IsAuthenticatedUser())
                return new GetResidentResponse { TransactionDate = DateTime.UtcNow, Result = (int) ResidentResponseResult.InvalidLogin, Message = ResidentResponseMessage.InvalidLogin };

            GetResidentResponse Response;
            var Resident = new Renter();

            if (!ValidateResident(out Response))
                return Response;

            try
            {
                if (PublicResidentId > 0)
                    Resident = new Renter(Renter.GetRenterIdByPublicRenterId(PublicResidentId));
                else if (!String.IsNullOrEmpty(EmailAddress))
                    Resident = new Renter(EmailAddress);

                if (Resident.RenterId < 1)
                {
                    Response.Result = (int) ResidentResponseResult.ResidentNotFound;
                    Response.Message = ResidentResponseMessage.ResidentNotFound;
                    return Response;
                }

                if (!IsUserAuthorized(EfxFramework.Property.GetPropertyListByRenterId(Resident.RenterId).Select(p => p.PropertyId).ToList()))
                    return new GetResidentResponse { TransactionDate = DateTime.UtcNow, Result = (int)ResidentResponseResult.UserNotAuthorized, Message = ResidentResponseMessage.UserNotAuthorized };

                Response.Result = (int) ResidentResponseResult.Success;
                Response.Message = ResidentResponseMessage.Success;
                Response.Resident = BuildResident(Resident);
            }
            catch
            {
                Response.Result = (int) ResidentResponseResult.GeneralFailure;
                Response.Message = ResidentResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool ValidateResident(out GetResidentResponse response)
        {
            response = new GetResidentResponse {TransactionDate = DateTime.UtcNow};

            if (PublicResidentId < 1 && String.IsNullOrEmpty(EmailAddress))
            {
                response.Result = (int) ResidentResponseResult.MoreInfoRequired;
                response.Message = ResidentResponseMessage.MoreInfoRequired;
                return false;
            }

            return true;
        }

        private static Resident BuildResident(Renter renter)
        {
            return new Resident
                {
                    PublicResidentId = renter.PublicRenterId,
                    Prefix = renter.Prefix,
                    FirstName = renter.FirstName,
                    MiddleName = renter.MiddleName,
                    LastName = renter.LastName,
                    Suffix = renter.Suffix,
                    MainPhoneNumber = renter.MainPhoneNumber,
                    MobilePhoneNumber = renter.MobilePhoneNumber,
                    PrimaryEmailAddress = renter.PrimaryEmailAddress,
                    StreetAddress = renter.StreetAddress,
                    StreetAddress2 = renter.Unit,
                    City = renter.City,
                    State = GetState(renter.StateProvinceId),
                    PostalCode = renter.PostalCode
                };
        }
    }
}