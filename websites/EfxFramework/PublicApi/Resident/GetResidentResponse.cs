﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Resident
{
    [DataContract]
    public class GetResidentResponse : ApiBaseResponse
    {
        [DataMember]
        public Resident Resident { get; set; }
    }
}