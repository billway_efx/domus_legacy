﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class Property
    {
        [DataMember]
        public int PropertyId { get; set; }

        [DataMember]
        public string PropertyName { get; set; }

        [DataMember]
        public string PropertyCode { get; set; }

        [DataMember]
        public int? NumberOfUnits { get; set; }

        [DataMember]
        public string StreetAddress { get; set; }

        [DataMember]
        public string StreetAddress2 { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string PostalCode { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string MainPhoneNumber { get; set; }

        public Property()
        {
            
        }
    }
}
