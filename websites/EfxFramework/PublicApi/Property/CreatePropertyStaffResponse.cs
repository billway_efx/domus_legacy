﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class CreatePropertyStaffResponse : ApiBaseResponse
    {
        [DataMember]
        public int PropertyStaffId { get; set; }
    }
}
