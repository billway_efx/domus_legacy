﻿using Mb2x.ExtensionMethods;
using System;
using System.Linq;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class UpdatePropertyStaffRequest : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public PropertyStaff PropertyStaff { get; set; }

        [DataMember]
        public int PropertyId { get; set; }

        public UpdatePropertyStaffResponse UpdatePropertyStaff()
        {
            if (!IsAuthenticatedUser())
                return new UpdatePropertyStaffResponse { TransactionDate = DateTime.UtcNow, Result = (int) PropertyStaffResponseResult.InvalidLogin, Message = PropertyStaffResponseMessage.InvalidLogin };

            UpdatePropertyStaffResponse Response;

            if (!ValidatePropertyStaff(out Response))
                return Response;

            if (!IsUserAuthorized(EfxFramework.Property.GetPropertyListByPropertyStaffId(PropertyStaff.PropertyStaffId).Select(p => p.PropertyId).ToList()))
                return new UpdatePropertyStaffResponse { TransactionDate = DateTime.UtcNow, Result = (int)PropertyStaffResponseResult.UserNotAuthorized, Message = PropertyStaffResponseMessage.UserNotAuthorized };

            if (PropertyId > 0)
            {
                if (!IsUserAuthorized(PropertyId))
                    return new UpdatePropertyStaffResponse { TransactionDate = DateTime.UtcNow, Result = (int)PropertyStaffResponseResult.UserNotAuthorized, Message = PropertyStaffResponseMessage.UserNotAuthorized };
            }

            var Staff = new EfxFramework.PropertyStaff(PropertyStaff.PropertyStaffId)
                {
                    FirstName = PropertyStaff.FirstName,
                    MiddleName = PropertyStaff.MiddleName,
                    LastName = PropertyStaff.LastName,
                    StreetAddress = PropertyStaff.StreetAddress,
                    StreetAddress2 = PropertyStaff.StreetAddress2,
                    City = PropertyStaff.City,
                    StateProvinceId = GetStateProvinceId(PropertyStaff.State),
                    PostalCode = PropertyStaff.PostalCode,
                    PrimaryEmailAddress = PropertyStaff.EmailAddress,
                    MainPhoneNumber = PropertyStaff.MainPhoneNumber,
                    MobilePhoneNumber = PropertyStaff.MobilePhoneNumber
                };

            if (!String.IsNullOrEmpty(PropertyStaff.Password))
            {
                Staff.SetPassword(PropertyStaff.Password);
            }
            else
            {
                var TempStaff = new EfxFramework.PropertyStaff(Staff.PropertyStaffId);
                Staff.PasswordHash = TempStaff.PasswordHash;
                Staff.Salt = TempStaff.Salt;
            }

            try
            {
                var StaffId = EfxFramework.PropertyStaff.Set(Staff, "Public API Update Property Staff Service");

                if (StaffId < 1)
                {
                    Response.Result = (int) PropertyStaffResponseResult.GeneralFailure;
                    Response.Message = PropertyStaffResponseMessage.GeneralFailure;
                    return Response;
                }

                Response.Result = (int) PropertyStaffResponseResult.Success;
                Response.Message = AssignStaffToProperty(StaffId) ? PropertyStaffResponseMessage.Success : PropertyStaffResponseMessage.PropertyAssignmentFailedOperationSuccess;
                Response.PropertyStaffId = StaffId;
            }
            catch
            {
                Response.Result = (int) PropertyStaffResponseResult.GeneralFailure;
                Response.Message = PropertyStaffResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool AssignStaffToProperty(int staffId)
        {
            if (PropertyId > 0)
            {
                try
                {
                    EfxFramework.Property.AssignStaffToProperty(staffId, PropertyId, false);
                }
                catch
                {
                    return false;
                }
            }

            return true;
        }

        private bool ValidatePropertyStaff(out UpdatePropertyStaffResponse response)
        {
            response = new UpdatePropertyStaffResponse {TransactionDate = DateTime.UtcNow};
            StateProvince State;

            if (PropertyStaff == null)
            {
                response.Result = (int) PropertyStaffResponseResult.MoreInfoRequired;
                response.Message = PropertyStaffResponseMessage.MoreInfoRequired;
                return false;
            }

            if (PropertyStaff.PropertyStaffId < 1)
            {
                response.Result = (int) PropertyStaffResponseResult.PropertyStaffIdRequired;
                response.Message = PropertyStaffResponseMessage.PropertyStaffIdRequired;
                return false;
            }

            var Staff = new EfxFramework.PropertyStaff(PropertyStaff.PropertyStaffId);

            if (!Staff.PropertyStaffId.HasValue || Staff.PropertyStaffId.Value < 1)
            {
                response.Result = (int) PropertyStaffResponseResult.PropertyStaffNotFound;
                response.Message = PropertyStaffResponseMessage.PropertyStaffNotFound;
                return false;
            }

            if (String.IsNullOrEmpty(PropertyStaff.FirstName))
            {
                response.Result = (int) PropertyStaffResponseResult.FirstNameRequired;
                response.Message = PropertyStaffResponseMessage.FirstNameRequired;
                return false;
            }

            if (String.IsNullOrEmpty(PropertyStaff.LastName))
            {
                response.Result = (int) PropertyStaffResponseResult.LastNameRequired;
                response.Message = PropertyStaffResponseMessage.LastNameRequired;
                return false;
            }

            if (String.IsNullOrEmpty(PropertyStaff.EmailAddress))
            {
                response.Result = (int) PropertyStaffResponseResult.EmailAddressRequired;
                response.Message = PropertyStaffResponseMessage.EmailAddressRequired;
                return false;
            }

            if (!PropertyStaff.EmailAddress.IsValidEmailAddress())
            {
                response.Result = (int) PropertyStaffResponseResult.InvalidEmail;
                response.Message = PropertyStaffResponseMessage.InvalidEmail;
                return false;
            }

            if (!String.IsNullOrEmpty(PropertyStaff.State) && !Enum.TryParse(PropertyStaff.State, out State))
            {
                response.Result = (int) PropertyStaffResponseResult.InvalidStateFormat;
                response.Message = PropertyStaffResponseMessage.InvalidStateFormat;
                return false;
            }

            if (PropertyId > 0)
            {
                var Property = new EfxFramework.Property(PropertyId);

                if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                {
                    response.Result = (int) PropertyStaffResponseResult.PropertyNotFound;
                    response.Message = PropertyStaffResponseMessage.PropertyNotFound;
                    return false;
                }
            }

            return true;
        }
    }
}
