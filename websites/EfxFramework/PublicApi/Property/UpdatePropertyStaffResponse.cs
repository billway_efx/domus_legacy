﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class UpdatePropertyStaffResponse : ApiBaseResponse
    {
        [DataMember]
        public int PropertyStaffId { get; set; }
    }
}
