﻿namespace EfxFramework.PublicApi.Property
{
    public enum PropertyFeesResponseResult
    {
        Success = 0,
        GeneralFailure = 2,
        InvalidLogin = 3,
        InvalidEndDate = 9999,
        MoreInfoRequired = 9998,
        FeeNameRequired = 9997,
        InvalidChargeCode = 9996,
        InvalidFeeAmount = 9991,
        UserNotAuthorized = 9987,
        InvalidFeeId = 9986,
        InvalidPropertyId = 9985
    }

    public static class PropertyFeesResponseMessage
    {
        public static string Success { get { return "Successfully completed the operation"; } }
        public static string GeneralFailure { get { return "A general failure occurred, please check your inputs and try the operation again"; } }
        public static string InvalidLogin { get { return "Invalid login credentials were supplied.  Please check your information and try again"; } }
        public static string InvalidEndDate { get { return "The End Date must be a later date than the start date.  Please check your information and try again"; } }
        public static string UserNotAuthorized { get { return "The logged in user is not authorized to perform this operation"; } }
        public static string MoreInfoRequired { get { return "More information is required to find a Property Fee"; } }
        public static string FeeNameRequired { get { return "Fee Name is required"; } }
        public static string InvalidFeeAmount { get { return "Default Fee Amount must be non-negative."; } }
        public static string InvalidChargeCode { get { return "Invalid Charge Code. The supplied value must be 50 characters or less."; } }
        public static string InvalidFeeId { get { return "Invalid Fee Id."; } }
        public static string InvalidPropertyId { get { return "Invalid Property Id."; } }
    }
}
