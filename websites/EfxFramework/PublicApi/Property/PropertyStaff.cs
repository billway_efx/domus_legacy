﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class PropertyStaff
    {
        [DataMember]
        public int PropertyStaffId { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string StreetAddress { get; set; }

        [DataMember]
        public string StreetAddress2 { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string PostalCode { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string MainPhoneNumber { get; set; }

        [DataMember]
        public string MobilePhoneNumber { get; set; }

        public PropertyStaff()
        {
            
        }
    }
}
