﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class RentReportersProperty
    {
        [DataMember]
        public int PropertyId { get; set; }

        [DataMember]
        public string PropertyName { get; set; }

        [DataMember]
        public string PropertyManagementCompanyName { get; set; }
    }

    [DataContract]
    public class GetRentReportersPropertiesResponse : ApiBaseResponse
    {
        [DataMember]
        public List<RentReportersProperty> RentReportersProperties { get; set; }
    }
}
