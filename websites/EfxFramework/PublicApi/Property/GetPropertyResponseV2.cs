﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class GetPropertyResponseV2 : ApiBaseResponse
    {
        [DataMember]
        public PropertyV2 Property { get; set; }
    }
}
