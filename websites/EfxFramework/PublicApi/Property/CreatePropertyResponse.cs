﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class CreatePropertyResponse : ApiBaseResponse
    {
        [DataMember]
        public int PropertyId { get; set;}
    }
}
