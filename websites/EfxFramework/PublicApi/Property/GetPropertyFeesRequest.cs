﻿using EfxFramework.PublicApi.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class GetPropertyFeesRequest : ApiBaseRequest
    {
        [DataMember]
        public int PropertyId { get; set; }
        
        //[DataMember]
        //public DateTime StartDate { get; set; }

        //[DataMember]
        //public DateTime EndDate { get; set; }

        public GetPropertyFeesResponse GetPropertyFees()
        {
            if (!IsAuthenticatedUser())
                return new GetPropertyFeesResponse {
                    TransactionDate = DateTime.UtcNow, 
                    Result = (int) PropertyFeesResponseResult.InvalidLogin, 
                    Message = PropertyFeesResponseMessage.InvalidLogin
                };

            GetPropertyFeesResponse Response;

            if (!ValidateRequest(out Response))
                return Response;

            if (!IsUserAuthorized(PropertyId))
                return new GetPropertyFeesResponse
                {
                    TransactionDate = DateTime.UtcNow,
                    Result = (int)PropertyFeesResponseResult.UserNotAuthorized,
                    Message = PropertyFeesResponseMessage.UserNotAuthorized
                };

            Response.PropertyFees = new List<ApiPropertyFee>();

            try
            {
                Response.PropertyFees = GetPropertyFeesList();

                Response.Result = (int) PropertyFeesResponseResult.Success;
                Response.Message = PropertyFeesResponseMessage.Success;
            }
            catch
            {
                Response.Result = (int)PropertyFeesResponseResult.GeneralFailure;
                Response.Message = PropertyFeesResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private List<ApiPropertyFee> GetPropertyFeesList()
        {
            List<ApiPropertyFee> fees = new List<ApiPropertyFee>();
            SqlDataReader dr = NewDataAccess.ExecuteProcedure(EfxSettings.ConnectionString, 
                "dbo.usp_MonthlyFee_GetMonthlyFeeDetailsByPropertyId",
                new[] { new SqlParameter("@PropertyId", PropertyId) } );
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    fees.Add(new ApiPropertyFee() {
                        ChargeCode = dr.GetString(dr.GetOrdinal("ChargeCode")),
                        DefaultFeeAmount = dr.GetDecimal(dr.GetOrdinal("DefaultFeeAmount")),
                        FeeName = dr.GetString(dr.GetOrdinal("FeeName")),
                        //IsActive = dr.GetBoolean(dr.GetOrdinal("IsActive")),
                        MonthlyFeeId = dr.GetInt32(dr.GetOrdinal("MonthlyFeeId")),
                        PropertyId = PropertyId});
                }
            }
            return fees;
        }

        private bool ValidateRequest(out GetPropertyFeesResponse response)
        {
            response = new GetPropertyFeesResponse {TransactionDate = DateTime.UtcNow};

            //if (StartDate >= EndDate)
            //{
            //    response.Result = (int)PropertyFeesResponseResult.InvalidEndDate;
            //    response.Message = PropertyFeesResponseMessage.InvalidEndDate;
            //    return false;
            //}

            return true;
        }

    }
}
