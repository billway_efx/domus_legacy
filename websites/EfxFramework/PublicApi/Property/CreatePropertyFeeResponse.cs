﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class CreatePropertyFeeResponse : ApiBaseResponse
    {
        [DataMember]
        public int PropertyFeeId { get; set;}
    }
}
