﻿using System;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class CreatePropertyFeeRequest : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public ApiPropertyFee PropertyFee { get; set; }

        public CreatePropertyFeeResponse CreatePropertyFee()
        {
            if (!IsAuthenticatedUser())
                return new CreatePropertyFeeResponse { 
                    TransactionDate = DateTime.UtcNow, 
                    Result = (int) PropertyFeesResponseResult.InvalidLogin, 
                    Message = PropertyFeesResponseMessage.InvalidLogin };

            CreatePropertyFeeResponse Response;

            if (!ValidatePropertyFee(out Response))
                return Response;

            //Create the monthly fee object
            var mf = new EfxFramework.MonthlyFee
            {
                PropertyId = PropertyFee.PropertyId,
                MonthlyFeeId = 0,
                FeeName = PropertyFee.FeeName,
                DefaultFeeAmount = PropertyFee.DefaultFeeAmount,
                IsActive = true,
                ChargeCode = PropertyFee.ChargeCode
            };

            try
            {
                //Add the new fee to the MonthlyFee table
                mf = new EfxFramework.MonthlyFee(EfxFramework.MonthlyFee.Set(mf));

                if (mf.MonthlyFeeId < 1)
                {
                    Response.Result = (int) PropertyFeesResponseResult.GeneralFailure;
                    Response.Message = PropertyFeesResponseMessage.GeneralFailure;
                    return Response;
                }

                Response.Result = (int) PropertyFeesResponseResult.Success;
                Response.Message = PropertyFeesResponseMessage.Success;
                Response.PropertyFeeId = mf.MonthlyFeeId;
            }
            catch
            {
                Response.Result = (int) PropertyResponseResult.GeneralFailure;
                Response.Message = PropertyResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool ValidatePropertyFee(out CreatePropertyFeeResponse response)
        {
            response = new CreatePropertyFeeResponse();

            if (PropertyFee == null)
            {
                response.Result = (int) PropertyFeesResponseResult.MoreInfoRequired;
                response.Message = PropertyFeesResponseMessage.MoreInfoRequired;
                return false;
            }

            if (PropertyFee.PropertyId <= 0)
            {
                response.Result = (int)PropertyFeesResponseResult.InvalidPropertyId;
                response.Message = PropertyFeesResponseMessage.InvalidPropertyId;
                return false;
            }

            if (!IsUserAuthorized(PropertyFee.PropertyId))
            {
                response.Result = (int)PropertyFeesResponseResult.UserNotAuthorized;
                response.Message = PropertyFeesResponseMessage.UserNotAuthorized;
                return false;
            }

            if (String.IsNullOrEmpty(PropertyFee.FeeName))
            {
                response.Result = (int)PropertyFeesResponseResult.FeeNameRequired;
                response.Message = PropertyFeesResponseMessage.FeeNameRequired;
                return false;
            }

            if (PropertyFee.DefaultFeeAmount < 0)
            {
                response.Result = (int)PropertyFeesResponseResult.InvalidFeeAmount;
                response.Message = PropertyFeesResponseMessage.InvalidFeeAmount;
                return false;
            }

            if (PropertyFee.ChargeCode.Length > 50)
            {
                response.Result = (int)PropertyFeesResponseResult.InvalidChargeCode;
                response.Message = PropertyFeesResponseMessage.InvalidChargeCode;
                return false;
            }

            return true;
        }
    }
}
