﻿using Mb2x.ExtensionMethods;
using System;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class CreatePropertyStaffRequest : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public PropertyStaff PropertyStaff { get; set; }

        [DataMember(IsRequired = true)]
        public int PropertyId { get; set; }

        public CreatePropertyStaffResponse CreatePropertyStaff()
        {
            if (!IsAuthenticatedUser())
                return new CreatePropertyStaffResponse { TransactionDate = DateTime.UtcNow, Result = (int) PropertyStaffResponseResult.InvalidLogin, Message = PropertyStaffResponseMessage.InvalidLogin };

            CreatePropertyStaffResponse Response;

            if (!ValidatePropertyStaff(out Response))
                return Response;

            if (!IsUserAuthorized(PropertyId))
                return new CreatePropertyStaffResponse { TransactionDate = DateTime.UtcNow, Result = (int)PropertyStaffResponseResult.UserNotAuthorized, Message = PropertyStaffResponseMessage.UserNotAuthorized };

            var Staff = new EfxFramework.PropertyStaff
                {
                    FirstName = PropertyStaff.FirstName,
                    MiddleName = PropertyStaff.MiddleName,
                    LastName = PropertyStaff.LastName,
                    StreetAddress = PropertyStaff.StreetAddress,
                    StreetAddress2 = PropertyStaff.StreetAddress2,
                    City = PropertyStaff.City,
                    StateProvinceId = GetStateProvinceId(PropertyStaff.State),
                    PostalCode = PropertyStaff.PostalCode,
                    PrimaryEmailAddress = PropertyStaff.EmailAddress,
                    MainPhoneNumber = PropertyStaff.MainPhoneNumber,
                    MobilePhoneNumber = PropertyStaff.MobilePhoneNumber
                };

            Staff.SetPassword(PropertyStaff.Password);

            try
            {
                var StaffId = EfxFramework.PropertyStaff.Set(Staff, "Public API Create Property Staff Service");
                Response = AssignStaffToProperty(StaffId, Response);

                if (Response.Result == (int) PropertyStaffResponseResult.PropertyAssignmentFailed || Response.Result == (int) PropertyStaffResponseResult.GeneralFailure)
                    return Response;

                Response.Result = (int) PropertyStaffResponseResult.Success;
                Response.Message = PropertyStaffResponseMessage.Success;
                Response.PropertyStaffId = StaffId;
            }
            catch
            {
                Response.Result = (int) PropertyStaffResponseResult.GeneralFailure;
                Response.Message = PropertyStaffResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private CreatePropertyStaffResponse AssignStaffToProperty(int staffId, CreatePropertyStaffResponse response)
        {
            if (staffId < 1)
            {
                response.Result = (int) PropertyStaffResponseResult.GeneralFailure;
                response.Message = PropertyStaffResponseMessage.GeneralFailure;
                return response;
            }

            try
            {
                EfxFramework.Property.AssignStaffToProperty(staffId, PropertyId, false);
            }
            catch
            {
                EfxFramework.PropertyStaff.DeletePropertyStaffByPropertyStaffId(staffId, "Public API Create Property Staff Service");
                response.Result = (int) PropertyStaffResponseResult.PropertyAssignmentFailed;
                response.Message = PropertyStaffResponseMessage.PropertyAssignmentAndOperationFailed;
            }

            return response;
        }

        private bool ValidatePropertyStaff(out CreatePropertyStaffResponse response)
        {
            response = new CreatePropertyStaffResponse {TransactionDate = DateTime.UtcNow};
            StateProvince State;

            if (PropertyStaff == null)
            {
                response.Result = (int) PropertyStaffResponseResult.MoreInfoRequired;
                response.Message = PropertyStaffResponseMessage.MoreInfoRequired;
                return false;
            }

            if (String.IsNullOrEmpty(PropertyStaff.FirstName))
            {
                response.Result = (int) PropertyStaffResponseResult.FirstNameRequired;
                response.Message = PropertyStaffResponseMessage.FirstNameRequired;
                return false;
            }

            if (String.IsNullOrEmpty(PropertyStaff.LastName))
            {
                response.Result = (int) PropertyStaffResponseResult.LastNameRequired;
                response.Message = PropertyStaffResponseMessage.LastNameRequired;
                return false;
            }

            if (String.IsNullOrEmpty(PropertyStaff.EmailAddress))
            {
                response.Result = (int) PropertyStaffResponseResult.EmailAddressRequired;
                response.Message = PropertyStaffResponseMessage.EmailAddressRequired;
                return false;
            }

            if (!PropertyStaff.EmailAddress.IsValidEmailAddress())
            {
                response.Result = (int) PropertyStaffResponseResult.InvalidEmail;
                response.Message = PropertyStaffResponseMessage.InvalidEmail;
                return false;
            }

            if (String.IsNullOrEmpty(PropertyStaff.Password))
            {
                response.Result = (int) PropertyStaffResponseResult.PasswordRequired;
                response.Message = PropertyStaffResponseMessage.PasswordRequired;
                return false;
            }

            if (!String.IsNullOrEmpty(PropertyStaff.State) && !Enum.TryParse(PropertyStaff.State, out State))
            {
                response.Result = (int) PropertyStaffResponseResult.InvalidStateFormat;
                response.Message = PropertyStaffResponseMessage.InvalidStateFormat;
                return false;
            }

            var Staff = new EfxFramework.PropertyStaff(PropertyStaff.EmailAddress);

            if (Staff.PropertyStaffId.HasValue && Staff.PropertyStaffId.Value > 0)
            {
                response.Result = (int) PropertyStaffResponseResult.DuplicateStaffFound;
                response.Message = PropertyStaffResponseMessage.DuplicateStafffound;
                return false;
            }

            if (PropertyId < 1)
            {
                response.Result = (int) PropertyStaffResponseResult.InvalidPropertyId;
                response.Message = PropertyStaffResponseMessage.InvalidPropertyId;
                return false;
            }

            var Property = new EfxFramework.Property(PropertyId);

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                response.Result = (int) PropertyStaffResponseResult.PropertyNotFound;
                response.Message = PropertyStaffResponseMessage.PropertyNotFound;
                return false;
            }

            return true;
        }
    }
}
