﻿using System;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class CreatePropertyRequestV2 : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public PropertyV2 PropertyV2 { get; set; }

        public CreatePropertyResponseV2 CreateProperty()
        {
            if (!IsAuthenticatedUser())
                return new CreatePropertyResponseV2 { TransactionDate = DateTime.UtcNow, Result = (int)PropertyResponseResult.InvalidLogin, Message = PropertyResponseMessage.InvalidLogin };

            CreatePropertyResponseV2 Response;

            if (!ValidateProperty(out Response))
                return Response;

            var P = new EfxFramework.Property
            {
                PropertyName = PropertyV2.PropertyName,
                PropertyCode = PropertyV2.PropertyCode,
                NumberOfUnits = PropertyV2.NumberOfUnits,
                StreetAddress = PropertyV2.StreetAddress,
                StreetAddress2 = PropertyV2.StreetAddress2,
                City = PropertyV2.City,
                StateProvinceId = GetStateProvinceId(PropertyV2.State),
                PostalCode = PropertyV2.PostalCode,
                OfficeEmailAddress = PropertyV2.EmailAddress,
                MainPhoneNumber = new PhoneNumber(PropertyV2.MainPhoneNumber),
                FaxNumber = new PhoneNumber(PropertyV2.FaxNumber)
            };

            try
            {
                P = new EfxFramework.Property(EfxFramework.Property.Set(P));

                if (!P.PropertyId.HasValue || P.PropertyId.Value < 1)
                {
                    Response.Result = (int)PropertyResponseResult.GeneralFailure;
                    Response.Message = PropertyResponseMessage.GeneralFailure;
                    return Response;
                }

                var User = new ApiUser(Username);

                if (User.ApiUserId > 0)
                {
                    User.AddPropertyToApiUser(P.PropertyId.Value);
                }
                else
                {
                    EfxFramework.Property.DeletePropertyByPropertyId(P.PropertyId.Value);
                    Response.Result = (int)PropertyResponseResult.GeneralFailure;
                    Response.Message = PropertyResponseMessage.GeneralFailure;
                    return Response;
                }

                Response.Result = (int)PropertyResponseResult.Success;
                Response.Message = PropertyResponseMessage.Success;
                Response.PropertyId = P.PropertyId.Value;
            }
            catch
            {
                Response.Result = (int)PropertyResponseResult.GeneralFailure;
                Response.Message = PropertyResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool ValidateProperty(out CreatePropertyResponseV2 response)
        {
            response = new CreatePropertyResponseV2{TransactionDate = DateTime.UtcNow};
            StateProvince State;

            if (PropertyV2 == null)
            {
                response.Result = (int) PropertyResponseResult.MoreInfoRequired;
                response.Message = PropertyResponseMessage.MoreInfoRequired;
                return false;
            }

            if (String.IsNullOrEmpty(PropertyV2.PropertyName))
            {
                response.Result = (int) PropertyResponseResult.PropertyNameRequired;
                response.Message = PropertyResponseMessage.PropertyNameRequired;
                return false;
            }

            if (String.IsNullOrEmpty(PropertyV2.StreetAddress))
            {
                response.Result = (int) PropertyResponseResult.StreetAddressRequired;
                response.Message = PropertyResponseMessage.StreetAddressRequired;
                return false;
            }

            if (String.IsNullOrEmpty(PropertyV2.City))
            {
                response.Result = (int) PropertyResponseResult.CityRequired;
                response.Message = PropertyResponseMessage.CityRequired;
                return false;
            }

            if (String.IsNullOrEmpty(PropertyV2.State))
            {
                response.Result = (int) PropertyResponseResult.StateRequired;
                response.Message = PropertyResponseMessage.StateRequired;
                return false;
            }

            if (!Enum.TryParse(PropertyV2.State, out State))
            {
                response.Result = (int) PropertyResponseResult.InvalidStateFormat;
                response.Message = PropertyResponseMessage.InvalidStateFormat;
                return false;
            }

            if (String.IsNullOrEmpty(PropertyV2.PostalCode))
            {
                response.Result = (int) PropertyResponseResult.PostalCodeRequired;
                response.Message = PropertyResponseMessage.PostalCodeRequired;
                return false;
            }

            if (String.IsNullOrEmpty(PropertyV2.MainPhoneNumber))
            {
                response.Result = (int) PropertyResponseResult.MainPhoneNumberRequired;
                response.Message = PropertyResponseMessage.MainPhoneNumberRequired;
                return false;
            }

            //FaxNumber is not required, but here's a start on the code if that ever changes
            //if (String.IsNullOrEmpty(PropertyV2.FaxNumber))
            //{
            //    response.Result = (int)PropertyResponseResult.MainPhoneNumberRequired;
            //    response.Message = PropertyResponseMessage.MainPhoneNumberRequired;
            //    return false;
            //}

            return true;
        }
    }
}
