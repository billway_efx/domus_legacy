﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class UpdatePropertyResponse : ApiBaseResponse
    {
        [DataMember]
        public int PropertyId { get; set; }
    }
}
