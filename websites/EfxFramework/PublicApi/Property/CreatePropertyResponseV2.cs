﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class CreatePropertyResponseV2 : ApiBaseResponse
    {
        [DataMember]
        public int PropertyId { get; set;}
    }
}
