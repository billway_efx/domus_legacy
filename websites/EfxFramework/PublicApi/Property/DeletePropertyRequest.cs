﻿using System;
using System.Data.SqlClient;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class DeletePropertyRequest : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public int PropertyId { get; set; }

        public DeletePropertyResponse DeleteProperty()
        {
            if (!IsAuthenticatedUser())
                return new DeletePropertyResponse { TransactionDate = DateTime.UtcNow, Result = (int) PropertyResponseResult.InvalidLogin, Message = PropertyResponseMessage.InvalidLogin };

            DeletePropertyResponse Response;

            if (!ValidateProperty(out Response))
                return Response;

            if (!IsUserAuthorized(PropertyId))
                return new DeletePropertyResponse { TransactionDate = DateTime.UtcNow, Result = (int)PropertyResponseResult.UserNotAuthorized, Message = PropertyResponseMessage.UserNotAuthorized };

            try
            {
                EfxFramework.Property.DeletePropertyByPropertyId(PropertyId);
                Response.Result = (int) PropertyResponseResult.Success;
                Response.Message = PropertyResponseMessage.Success;
            }
            catch (SqlException Ex)
            {
                if (Ex.Number == 547)
                {
                    Response.Result = (int) PropertyResponseResult.PropertyHasAssociations;
                    Response.Message = PropertyResponseMessage.PropertyHasAssociations;
                }
                else
                {
                    Response.Result = (int) PropertyResponseResult.GeneralFailure;
                    Response.Message = PropertyResponseMessage.GeneralFailure;
                }
            }
            catch
            {
                Response.Result = (int) PropertyResponseResult.GeneralFailure;
                Response.Message = PropertyResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool ValidateProperty(out DeletePropertyResponse response)
        {
            response = new DeletePropertyResponse {TransactionDate = DateTime.UtcNow};

            if (PropertyId < 1)
            {
                response.Result = (int) PropertyResponseResult.MoreInfoRequired;
                response.Message = PropertyResponseMessage.MoreInfoRequired;
                return false;
            }

            var Property = new EfxFramework.Property(PropertyId);

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                response.Result = (int) PropertyResponseResult.PropertyNotFound;
                response.Message = PropertyResponseMessage.PropertyNotFound;
                return false;
            }

            return true;
        }
    }
}
