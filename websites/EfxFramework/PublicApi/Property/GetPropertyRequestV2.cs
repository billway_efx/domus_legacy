﻿using System;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class GetPropertyRequestV2 : ApiBaseRequest
    {
        [DataMember]
        public int PropertyId { get; set; }

        [DataMember]
        public string PropertyName { get; set; }

        public GetPropertyResponseV2 GetProperty()
        {
            if (!IsAuthenticatedUser())
                return new GetPropertyResponseV2 { TransactionDate = DateTime.UtcNow, Result = (int) PropertyResponseResult.InvalidLogin, Message = PropertyResponseMessage.InvalidLogin };

            GetPropertyResponseV2 Response;
            var Property = new EfxFramework.Property();

            if (!ValidateProperty(out Response))
                return Response;

            try
            {
                if (PropertyId > 1)
                    Property = new EfxFramework.Property(PropertyId);
                else if (!String.IsNullOrEmpty(PropertyName))
                    Property = EfxFramework.Property.GetFirstPropertyByPropertyName(PropertyName);

                if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                {
                    Response.Result = (int) PropertyResponseResult.PropertyNotFound;
                    Response.Message = PropertyResponseMessage.PropertyNotFound;
                    return Response;
                }

                if (!IsUserAuthorized(Property.PropertyId))
                    return new GetPropertyResponseV2 { TransactionDate = DateTime.UtcNow, Result = (int)PropertyResponseResult.UserNotAuthorized, Message = PropertyResponseMessage.UserNotAuthorized };

                Response.Result = (int) PropertyResponseResult.Success;
                Response.Message = PropertyResponseMessage.Success;
                Response.Property = BuildProperty(Property);
            }
            catch
            {
                Response.Result = (int) PropertyResponseResult.GeneralFailure;
                Response.Message = PropertyResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool ValidateProperty(out GetPropertyResponseV2 response)
        {
            response = new GetPropertyResponseV2 { TransactionDate = DateTime.UtcNow };

            if (PropertyId < 1 && String.IsNullOrEmpty(PropertyName))
            {
                response.Result = (int) PropertyResponseResult.MoreInfoRequired;
                response.Message = PropertyResponseMessage.MoreInfoRequired;
                return false;
            }

            return true;
        }

        private static PropertyV2 BuildProperty(EfxFramework.Property property)
        {
            if (!property.PropertyId.HasValue || property.PropertyId.Value < 1)
                return new PropertyV2();

            return new PropertyV2
                {
                    PropertyId = property.PropertyId.Value,
                    PropertyName = property.PropertyName,
                    PropertyCode = property.PropertyCode,
                    NumberOfUnits = property.NumberOfUnits,
                    StreetAddress = property.StreetAddress,
                    StreetAddress2 = property.StreetAddress2,
                    City = property.City,
                    State = GetState(property.StateProvinceId),
                    PostalCode = property.PostalCode,
                    EmailAddress = property.OfficeEmailAddress,
                    MainPhoneNumber = property.MainPhoneNumber.ToString(false),
                    FaxNumber = property.FaxNumber.ToString(false)
                };
        }
    }
}
