﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class GetPropertyStaffRequest : ApiBaseRequest
    {
        [DataMember]
        public int PropertyStaffId { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        public GetPropertyStaffResponse GetPropertyStaff()
        {
            if (!IsAuthenticatedUser())
                return new GetPropertyStaffResponse { TransactionDate = DateTime.UtcNow, Result = (int) PropertyStaffResponseResult.InvalidLogin, Message = PropertyStaffResponseMessage.InvalidLogin };

            GetPropertyStaffResponse Response;
            var Staff = new EfxFramework.PropertyStaff();

            if (!ValidatePropertyStaff(out Response))
                return Response;

            try
            {
                if (PropertyStaffId > 0)
                    Staff = new EfxFramework.PropertyStaff(PropertyStaffId);
                else if (!String.IsNullOrEmpty(EmailAddress))
                    Staff = new EfxFramework.PropertyStaff(EmailAddress);

                if (!Staff.PropertyStaffId.HasValue || Staff.PropertyStaffId.Value < 1)
                {
                    Response.Result = (int) PropertyStaffResponseResult.PropertyStaffNotFound;
                    Response.Message = PropertyStaffResponseMessage.PropertyStaffNotFound;
                    return Response;
                }

                if (!IsUserAuthorized(EfxFramework.Property.GetPropertyListByPropertyStaffId(Staff.PropertyStaffId.Value).Select(p => p.PropertyId).ToList()))
                    return new GetPropertyStaffResponse { TransactionDate = DateTime.UtcNow, Result = (int)PropertyStaffResponseResult.UserNotAuthorized, Message = PropertyStaffResponseMessage.UserNotAuthorized };

                Response.Result = (int) PropertyStaffResponseResult.Success;
                Response.Message = PropertyStaffResponseMessage.Success;
                Response.PropertyStaff = BuildPropertyStaff(Staff);
            }
            catch
            {
                Response.Result = (int) PropertyStaffResponseResult.GeneralFailure;
                Response.Message = PropertyStaffResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool ValidatePropertyStaff(out GetPropertyStaffResponse response)
        {
            response = new GetPropertyStaffResponse {TransactionDate = DateTime.UtcNow};

            if (PropertyStaffId < 1 && String.IsNullOrEmpty(EmailAddress))
            {
                response.Result = (int) PropertyStaffResponseResult.MoreInfoRequired;
                response.Message = PropertyStaffResponseMessage.MoreInfoRequired;
                return false;
            }

            return true;
        }

        private PropertyStaff BuildPropertyStaff(EfxFramework.PropertyStaff staff)
        {
            if (!staff.PropertyStaffId.HasValue || staff.PropertyStaffId.Value < 1)
                return new PropertyStaff();

            return new PropertyStaff
                {
                    PropertyStaffId = staff.PropertyStaffId.Value,
                    FirstName = staff.FirstName,
                    MiddleName = staff.MiddleName,
                    LastName = staff.LastName,
                    StreetAddress = staff.StreetAddress,
                    StreetAddress2 = staff.StreetAddress2,
                    City = staff.City,
                    State = GetState(staff.StateProvinceId),
                    PostalCode = staff.PostalCode,
                    EmailAddress = staff.PrimaryEmailAddress,
                    MainPhoneNumber = staff.MainPhoneNumber,
                    MobilePhoneNumber = staff.MobilePhoneNumber
                };
        }
    }
}
