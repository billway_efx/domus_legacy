﻿using System;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class CreatePropertyRequest : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public Property Property { get; set; }

        public CreatePropertyResponse CreateProperty()
        {
            if (!IsAuthenticatedUser())
                return new CreatePropertyResponse { TransactionDate = DateTime.UtcNow, Result = (int) PropertyResponseResult.InvalidLogin, Message = PropertyResponseMessage.InvalidLogin };

            CreatePropertyResponse Response;

            if (!ValidateProperty(out Response))
                return Response;

            var P = new EfxFramework.Property
                {
                    PropertyName = Property.PropertyName,
                    PropertyCode = Property.PropertyCode,
                    NumberOfUnits = Property.NumberOfUnits,
                    StreetAddress = Property.StreetAddress,
                    StreetAddress2 = Property.StreetAddress2,
                    City = Property.City,
                    StateProvinceId = GetStateProvinceId(Property.State),
                    PostalCode = Property.PostalCode,
                    OfficeEmailAddress = Property.EmailAddress,
                    MainPhoneNumber = new PhoneNumber(Property.MainPhoneNumber)
                };

            try
            {
                P = new EfxFramework.Property(EfxFramework.Property.Set(P));

                if (!P.PropertyId.HasValue || P.PropertyId.Value < 1)
                {
                    Response.Result = (int) PropertyResponseResult.GeneralFailure;
                    Response.Message = PropertyResponseMessage.GeneralFailure;
                    return Response;
                }

                var User = new ApiUser(Username);

                if (User.ApiUserId > 0)
                {
                    User.AddPropertyToApiUser(P.PropertyId.Value);
                }
                else
                {
                    EfxFramework.Property.DeletePropertyByPropertyId(P.PropertyId.Value);
                    Response.Result = (int) PropertyResponseResult.GeneralFailure;
                    Response.Message = PropertyResponseMessage.GeneralFailure;
                    return Response;
                }

                Response.Result = (int) PropertyResponseResult.Success;
                Response.Message = PropertyResponseMessage.Success;
                Response.PropertyId = P.PropertyId.Value;
            }
            catch
            {
                Response.Result = (int) PropertyResponseResult.GeneralFailure;
                Response.Message = PropertyResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool ValidateProperty(out CreatePropertyResponse response)
        {
            response = new CreatePropertyResponse{TransactionDate = DateTime.UtcNow};
            StateProvince State;

            if (Property == null)
            {
                response.Result = (int) PropertyResponseResult.MoreInfoRequired;
                response.Message = PropertyResponseMessage.MoreInfoRequired;
                return false;
            }

            if (String.IsNullOrEmpty(Property.PropertyName))
            {
                response.Result = (int) PropertyResponseResult.PropertyNameRequired;
                response.Message = PropertyResponseMessage.PropertyNameRequired;
                return false;
            }

            if (String.IsNullOrEmpty(Property.StreetAddress))
            {
                response.Result = (int) PropertyResponseResult.StreetAddressRequired;
                response.Message = PropertyResponseMessage.StreetAddressRequired;
                return false;
            }

            if (String.IsNullOrEmpty(Property.City))
            {
                response.Result = (int) PropertyResponseResult.CityRequired;
                response.Message = PropertyResponseMessage.CityRequired;
                return false;
            }

            if (String.IsNullOrEmpty(Property.State))
            {
                response.Result = (int) PropertyResponseResult.StateRequired;
                response.Message = PropertyResponseMessage.StateRequired;
                return false;
            }

            if (!Enum.TryParse(Property.State, out State))
            {
                response.Result = (int) PropertyResponseResult.InvalidStateFormat;
                response.Message = PropertyResponseMessage.InvalidStateFormat;
                return false;
            }

            if (String.IsNullOrEmpty(Property.PostalCode))
            {
                response.Result = (int) PropertyResponseResult.PostalCodeRequired;
                response.Message = PropertyResponseMessage.PostalCodeRequired;
                return false;
            }

            if (String.IsNullOrEmpty(Property.MainPhoneNumber))
            {
                response.Result = (int) PropertyResponseResult.MainPhoneNumberRequired;
                response.Message = PropertyResponseMessage.MainPhoneNumberRequired;
                return false;
            }

            return true;
        }
    }
}
