﻿using System;
using System.Data.SqlClient;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class DeletePropertyFeeRequest : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public ApiPropertyFee PropertyFee { get; set; }

        public DeletePropertyFeeResponse DeletePropertyFee()
        {
            if (!IsAuthenticatedUser())
                return new DeletePropertyFeeResponse { 
                    TransactionDate = DateTime.UtcNow, 
                    Result = (int) PropertyFeesResponseResult.InvalidLogin, 
                    Message = PropertyFeesResponseMessage.InvalidLogin };

            DeletePropertyFeeResponse Response;

            if (!ValidatePropertyFee(out Response))
                return Response;

            if (!IsUserAuthorized(PropertyFee.PropertyId))
                return new DeletePropertyFeeResponse { 
                    TransactionDate = DateTime.UtcNow, 
                    Result = (int)PropertyResponseResult.UserNotAuthorized, 
                    Message = PropertyResponseMessage.UserNotAuthorized };

            try
            {
                EfxFramework.MonthlyFee.DeleteFromLeaseFeeAndMonthlyFee(PropertyFee.PropertyId, PropertyFee.MonthlyFeeId);
                Response.Result = (int) PropertyResponseResult.Success;
                Response.Message = PropertyResponseMessage.Success;
            }
            catch
            {
                Response.Result = (int) PropertyResponseResult.GeneralFailure;
                Response.Message = PropertyResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool ValidatePropertyFee(out DeletePropertyFeeResponse response)
        {
            response = new DeletePropertyFeeResponse {TransactionDate = DateTime.UtcNow};

            if (PropertyFee == null)
            {
                response.Result = (int)PropertyFeesResponseResult.MoreInfoRequired;
                response.Message = PropertyFeesResponseMessage.MoreInfoRequired;
                return false;
            }

            if (PropertyFee.MonthlyFeeId <= 0)
            {
                response.Result = (int)PropertyFeesResponseResult.InvalidFeeId;
                response.Message = PropertyFeesResponseMessage.InvalidFeeId;
                return false;
            }

            if (PropertyFee.PropertyId <= 0)
            {
                response.Result = (int)PropertyFeesResponseResult.InvalidPropertyId;
                response.Message = PropertyFeesResponseMessage.InvalidPropertyId;
                return false;
            }

            return true;
        }
    }
}
