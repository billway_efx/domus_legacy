﻿namespace EfxFramework.PublicApi.Property
{
    public enum PropertyResponseResult
    {
        Success = 0,
        GeneralFailure = 2,
        InvalidLogin = 3,
        PropertyNotFound = 9999,
        MoreInfoRequired = 9998,
        PropertyNameRequired = 9997,
        StreetAddressRequired = 9996,
        CityRequired = 9995,
        StateRequired = 9994,
        PostalCodeRequired = 9993,
        MainPhoneNumberRequired = 9992,
        InvalidStateFormat = 9991,
        PropertyHasAssociations = 9990,
        PropertyIdRequired = 9989,
        UserNotAuthorized = 9987,
        InvalidEndDate = 9986
    }

    public static class PropertyResponseMessage
    {
        public static string Success { get { return "Successfully completed the operation"; } }
        public static string GeneralFailure { get { return "A general failure occurred, please check your inputs and try the operation again"; } }
        public static string InvalidLogin { get { return "Invalid login credentials were supplied.  Please check your information and try again"; } }
        public static string PropertyNotFound { get { return "No Property found with the provided information"; } }
        public static string MoreInfoRequired { get { return "More information is required to find a Property"; } }
        public static string PropertyNameRequired { get { return "Property Name is required"; } }
        public static string StreetAddressRequired { get { return "A Street Address is required"; } }
        public static string CityRequired { get { return "A City is required"; } }
        public static string StateRequired { get { return "A State is required"; } }
        public static string PostalCodeRequired { get { return "A Postal Code is required"; } }
        public static string MainPhoneNumberRequired { get { return "A Phone Number is required"; } }
        public static string InvalidStateFormat { get { return "State must be in the two character state code format"; } }
        public static string PropertyHasAssociations { get { return "The Property has Residents, Leases and/or Log files against it and can cannot be deleted"; } }
        public static string PropertyIdRequired { get { return "A Property ID is required to perform the operation"; } }
        public static string UserNotAuthorized { get { return "The logged in user is not authorized to perform this operation"; } }
        public static string InvalidEndDate { get { return "The End Date must be a later date than the start date.  Please check your information and try again"; } }
    }
}
