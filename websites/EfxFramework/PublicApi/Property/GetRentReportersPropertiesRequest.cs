﻿using System;
using System.Data.SqlTypes;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class GetRentReportersPropertiesRequest : ApiBaseRequest
    {
        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        public GetRentReportersPropertiesResponse GetRentReportersProperties()
        {
            if (!IsAuthenticatedUser())
                return new GetRentReportersPropertiesResponse {TransactionDate = DateTime.UtcNow, Result = (int) PropertyResponseResult.InvalidLogin, Message = PropertyResponseMessage.InvalidLogin};

            GetRentReportersPropertiesResponse Response;

            if (!ValidateRequest(out Response))
                return Response;

            try
            {
                if (StartDate.HasValue && EndDate.HasValue)
                    Response.RentReportersProperties = EfxFramework.Property.GetRentReportersPropertiesByEntryDates(StartDate.Value, EndDate.Value);
                else if (StartDate.HasValue && !EndDate.HasValue)
                    Response.RentReportersProperties = EfxFramework.Property.GetRentReportersPropertiesByEntryDates(StartDate.Value, SqlDateTime.MaxValue.Value);
                else if (!StartDate.HasValue && EndDate.HasValue)
                    Response.RentReportersProperties = EfxFramework.Property.GetRentReportersPropertiesByEntryDates(SqlDateTime.MinValue.Value, EndDate.Value);
                else
                    Response.RentReportersProperties = EfxFramework.Property.GetAllRentReportersProperties();
            }
            catch
            {
                Response.Result = (int) PropertyResponseResult.GeneralFailure;
                Response.Message = PropertyResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool ValidateRequest(out GetRentReportersPropertiesResponse response)
        {
            response = new GetRentReportersPropertiesResponse {TransactionDate = DateTime.UtcNow};

            if (StartDate.HasValue && EndDate.HasValue)
            {
                if (StartDate.Value >= EndDate.Value)
                {
                    response.Result = (int) PropertyResponseResult.InvalidEndDate;
                    response.Message = PropertyResponseMessage.InvalidEndDate;
                    return false;
                }
            }

            return true;
        }
    }
}
