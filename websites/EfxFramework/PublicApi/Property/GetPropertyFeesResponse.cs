﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class GetPropertyFeesResponse : ApiBaseResponse
    {
        [DataMember]
        public List<ApiPropertyFee> PropertyFees { get; set; }
    }
}
