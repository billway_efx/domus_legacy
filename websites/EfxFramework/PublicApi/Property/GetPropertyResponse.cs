﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class GetPropertyResponse : ApiBaseResponse
    {
        [DataMember]
        public Property Property { get; set; }
    }
}
