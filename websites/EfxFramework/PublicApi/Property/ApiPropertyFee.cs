﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class ApiPropertyFee
    {
        [DataMember]
        public int MonthlyFeeId { get; set; }
        
        [DataMember]
        public int PropertyId { get; set; }

        [DataMember]
        public string FeeName { get; set; }

        [DataMember]
        public decimal DefaultFeeAmount { get; set; }

        //[DataMember]
        //[OptionalField]
        //public Boolean IsActive { get; }

        [DataMember]
        public string ChargeCode { get; set; }
    }

}
