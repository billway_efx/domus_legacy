﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class GetPropertyStaffResponse : ApiBaseResponse
    {
        [DataMember]
        public PropertyStaff PropertyStaff { get; set; }
    }
}
