﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Property
{
    [DataContract]
    public class DeletePropertyStaffRequest : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public int PropertyStaffId { get; set; }

        public DeletePropertyStaffResponse DeletePropertyStaff()
        {
            if (!IsAuthenticatedUser())
                return new DeletePropertyStaffResponse { TransactionDate = DateTime.UtcNow, Result = (int) PropertyStaffResponseResult.InvalidLogin, Message = PropertyStaffResponseMessage.InvalidLogin };

            DeletePropertyStaffResponse Response;

            if (!ValidatePropertyStaff(out Response))
                return Response;

            if (!IsUserAuthorized(EfxFramework.Property.GetPropertyListByPropertyStaffId(PropertyStaffId).Select(p => p.PropertyId).ToList()))
                return new DeletePropertyStaffResponse { TransactionDate = DateTime.UtcNow, Result = (int)PropertyStaffResponseResult.UserNotAuthorized, Message = PropertyStaffResponseMessage.UserNotAuthorized };

            try
            {
                EfxFramework.PropertyStaff.DeletePropertyStaffByPropertyStaffId(PropertyStaffId, "Public API Delete Property Staff Service");
                Response.Result = (int) PropertyStaffResponseResult.Success;
                Response.Message = PropertyStaffResponseMessage.Success;
            }
            catch (SqlException Ex)
            {
                if (Ex.Number == 547)
                {
                    Response.Result = (int) PropertyStaffResponseResult.HasAssociations;
                    Response.Message = PropertyStaffResponseMessage.HasAssociations;
                }
                else
                {
                    Response.Result = (int) PropertyStaffResponseResult.GeneralFailure;
                    Response.Message = PropertyStaffResponseMessage.GeneralFailure;
                }
            }
            catch
            {
                Response.Result = (int)PropertyStaffResponseResult.GeneralFailure;
                Response.Message = PropertyStaffResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool ValidatePropertyStaff(out DeletePropertyStaffResponse response)
        {
            response = new DeletePropertyStaffResponse {TransactionDate = DateTime.UtcNow};

            if (PropertyStaffId < 1)
            {
                response.Result = (int) PropertyStaffResponseResult.MoreInfoRequired;
                response.Message = PropertyStaffResponseMessage.MoreInfoRequired;
                return false;
            }

            var Staff = new EfxFramework.PropertyStaff(PropertyStaffId);

            if (!Staff.PropertyStaffId.HasValue || Staff.PropertyStaffId.Value < 1)
            {
                response.Result = (int) PropertyStaffResponseResult.PropertyStaffNotFound;
                response.Message = PropertyStaffResponseMessage.PropertyStaffNotFound;
                return false;
            }
            
            return true;
        }
    }
}
