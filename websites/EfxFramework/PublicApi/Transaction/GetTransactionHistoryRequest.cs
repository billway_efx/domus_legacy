﻿using EfxFramework.PublicApi.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Transaction
{
    [DataContract]
    public class GetTransactionHistoryRequest : ApiBaseRequest
    {
        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        public GetTransactionHistoryResponse GetTransactionHistory()
        {
            if (!IsAuthenticatedUser())
                return new GetTransactionHistoryResponse {TransactionDate = DateTime.UtcNow, Result = (int) TransactionResponseResult.InvalidLogin, Message = TransactionResponseMessage.InvalidLogin};

            GetTransactionHistoryResponse Response;

            if (!ValidateRequest(out Response))
                return Response;

            Response.Transactions = new List<ApiTransaction>();

            try
            {
                var ApiUser = new ApiUser(Username);

                if (ApiUser.PartnerCompany == null)
                    Response.Transactions = GetTransactions(ApiUser);
                else
                    Response.Transactions = ApiUser.PartnerCompany.PartnerCompanyName == "Rent Reporters" ? GetTransactions() : 
                        GetTransactions(ApiUser);

                Response.Result = (int) TransactionResponseResult.Success;
                Response.Message = TransactionResponseMessage.Success;
            }
            catch
            {
                Response.Result = (int) TransactionResponseResult.GeneralFailure;
                Response.Message = TransactionResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private List<ApiTransaction> GetTransactions(ApiUser user = null)
        {
            return user == null ? GetRentReportersTransactions() : 
                (from Property in user.UserProperties where Property.PropertyId.HasValue && Property.PropertyId.Value > 0 
                 let Payments = EfxFramework.Payment.GetPaymentListByPropertyIdAndDateRange(Property.PropertyId.Value, StartDate, EndDate) 
                 from Payment in Payments 
                 select BuildApiTransaction(Property, Payment)).ToList();
        }

        private List<ApiTransaction> GetRentReportersTransactions()
        {
            return (from P in EfxFramework.Payment.GetPaymentListForRentReportersByDateRange(StartDate, EndDate) 
                    let Property = EfxFramework.Property.GetPropertyByRenterId(P.RenterId) 
                        where Property.PropertyId.HasValue && Property.PropertyId.Value > 0 
                    select BuildApiTransaction(Property, P)).ToList();
        }

        private bool ValidateRequest(out GetTransactionHistoryResponse response)
        {
            response = new GetTransactionHistoryResponse {TransactionDate = DateTime.UtcNow};

            if (StartDate.HasValue && EndDate.HasValue)
            {
                if (StartDate.Value >= EndDate.Value)
                {
                    response.Result = (int) TransactionResponseResult.InvalidEndDate;
                    response.Message = TransactionResponseMessage.InvalidEndDate;
                    return false;
                }
            }

            return true;
        }

        private static ApiTransaction BuildApiTransaction(EfxFramework.Property property, EfxFramework.Payment payment)
        {
            string CompanyName = null;
            // ReSharper disable PossibleInvalidOperationException
            var Companies = Company.GetCompanyListByPropertyId(property.PropertyId.Value);
            // ReSharper restore PossibleInvalidOperationException

            var Resident = new Renter(payment.RenterId);
            var TransactionAmount = 0.00M;

            TransactionAmount += payment.RentAmount.HasValue ? payment.RentAmount.Value : 0.00M;
            TransactionAmount += payment.CharityAmount.HasValue ? payment.CharityAmount.Value : 0.00M;
            TransactionAmount += payment.OtherAmount1.HasValue ? payment.OtherAmount1.Value : 0.00M;
            TransactionAmount += payment.OtherAmount2.HasValue ? payment.OtherAmount2.Value : 0.00M;
            TransactionAmount += payment.OtherAmount3.HasValue ? payment.OtherAmount3.Value : 0.00M;
            TransactionAmount += payment.OtherAmount4.HasValue ? payment.OtherAmount4.Value : 0.00M;
            TransactionAmount += payment.OtherAmount5.HasValue ? payment.OtherAmount5.Value : 0.00M;

            if (Companies.Count > 0)
                CompanyName = Companies[0].CompanyName;

            return new ApiTransaction
                {
                    // ReSharper disable PossibleInvalidOperationException
                    PropertyId = property.PropertyId.Value,
                    // ReSharper restore PossibleInvalidOperationException
                    PropertyManagementCompanyName = CompanyName,
                    PropertyName = property.PropertyName,
                    RentReporterId = Resident.RentReporterId,
                    PublicResidentId = Resident.PublicRenterId,
                    ResidentFirstName = Resident.FirstName,
                    ResidentLastName = Resident.LastName,
                    PaymentStatus = payment.PaymentStatus.ToString(),
                    TransactionId = payment.TransactionId,
                    TransactionAmount = TransactionAmount,
                    ProcessDateTime = payment.TransactionDateTime,
                    PaymentChannel = payment.PaymentChannel.ToString(),
                    TransactionType = ((TransactionType)payment.PaymentTypeId).ToString()
                };
        }
    }
}
