﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Transaction
{
    [DataContract]
    public class ApiTransaction
    {
        [DataMember]
        public int PropertyId { get; set; }

        [DataMember]
        public string PropertyManagementCompanyName { get; set; }

        [DataMember]
        public string PropertyName { get; set; }

        [DataMember]
        public string RentReporterId { get; set; }

        [DataMember]
        public long? PublicResidentId { get; set; }

        [DataMember]
        public string ResidentFirstName { get; set; }

        [DataMember]
        public string ResidentLastName { get; set; }

        [DataMember]
        public string PaymentStatus { get; set; }

        [DataMember]
        public string TransactionId { get; set; }

        [DataMember]
        public decimal TransactionAmount { get; set; }

        [DataMember]
        public DateTime ProcessDateTime { get; set; }

        [DataMember]
        public string PaymentChannel { get; set; }

        [DataMember]
        public string TransactionType { get; set; }
    }

    [DataContract]
    public class GetTransactionHistoryResponse : ApiBaseResponse
    {
        [DataMember]
        public List<ApiTransaction> Transactions { get; set; }
    }
}
