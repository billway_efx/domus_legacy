﻿namespace EfxFramework.PublicApi.Transaction
{
    public enum TransactionResponseResult
    {
        Success = 0,
        GeneralFailure = 2,
        InvalidLogin = 3,
        InvalidEndDate = 9999
    }

    public static class TransactionResponseMessage
    {
        public static string Success { get { return "Successfully completed the operation"; } }
        public static string GeneralFailure { get { return "A general failure occurred, please check your inputs and try the operation again"; } }
        public static string InvalidLogin { get { return "Invalid login credentials were supplied.  Please check your information and try again"; } }
        public static string InvalidEndDate { get { return "The End Date must be a later date than the start date.  Please check your information and try again"; } }
    }
}
