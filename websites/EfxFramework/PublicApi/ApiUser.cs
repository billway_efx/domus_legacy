﻿using System;
using EfxFramework.Security.Authentication;
using Mb2x.Data;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace EfxFramework.PublicApi
{
    public class ApiUser : BaseEntity
    {
        private List<EfxFramework.Property> _userProperties;
        private PartnerCompany _partnerCompany;

        public int ApiUserId { get; set; }
        public string Username { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] Salt { get; set; }
        public int? PartnerCompanyId { get; set; }
        public bool IsActive { get; set; }
        public string ViewApiUserUrl { get { return String.Format("{0}?ApiUserId={1}", EfxSettings.ViewApiUserUrl, ApiUserId); } }
        public string DeleteApiUserUrl { get { return String.Format("~/Handlers/GeneralHandler.ashx?RequestType=18&ApiUserId={0}#ApiUsersTab", ApiUserId); } }
        public string PartnerCompanyName { get { return PartnerCompany != null ? PartnerCompany.PartnerCompanyName : "None Assigned"; }}

        public PartnerCompany PartnerCompany
        {
            get
            {
                if (_partnerCompany != null && _partnerCompany.PartnerCompanyId > 0)
                    return _partnerCompany;

                if (PartnerCompanyId.HasValue && PartnerCompanyId.Value > 0)
                    _partnerCompany = new PartnerCompany(PartnerCompanyId.Value);

                if (_partnerCompany != null && _partnerCompany.PartnerCompanyId < 1)
                    _partnerCompany = null;

                return _partnerCompany;
            }

            set
            {
                _partnerCompany = value;
            }
        }
        
        public List<EfxFramework.Property> UserProperties
        {
            get
            {
                if (_userProperties == null)
                    _userProperties = new List<EfxFramework.Property>();

                _userProperties = GetApiUserPropertyListByApiUserId(ApiUserId);

                return _userProperties;
            }
        }

        public void SetPassword(string newPassword)
        {
            Salt = AuthenticationTools.GenerateSalt();
            PasswordHash = AuthenticationTools.HashPassword(newPassword, Salt);
        }

        public string ResetPassword()
        {
            var TempPassword = AuthenticationTools.GenerateRandomPassword();

            Salt = AuthenticationTools.GenerateSalt();
            PasswordHash = AuthenticationTools.HashPassword(TempPassword, Salt);

            return TempPassword;
        }

        public static List<ApiUser> GetAllApiUsers()
        {
            return DataAccess.GetTypedList<ApiUser>(EfxSettings.ConnectionString, "dbo.usp_ApiUser_GetAllApiUsers");
        }

        public static void DeleteApiUser(int apiUserId)
        {
            var User = new ApiUser(apiUserId) { IsActive = false };
            Set(User);
        }

        public List<EfxFramework.Property> GetApiUserPropertyListByApiUserId(int apiUserId)
        {
            var Data = DataAccess.ExecuteDataTable(EfxSettings.ConnectionString, "dbo.usp_ApiUserProperty_GetApiUserPropertyListByApiUserId", new SqlParameter("@ApiUserId", apiUserId));
            return Data.Rows.Cast<DataRow>().Select(row => new EfxFramework.Property((int) row["PropertyId"])).Where(property => property.PropertyId.HasValue && property.PropertyId.Value > 0).ToList();
        }

        public List<ApiUser> GetApiUserListByPropertyId(int propertyId)
        {
            var Data = DataAccess.ExecuteDataTable(EfxSettings.ConnectionString, "dbo.usp_ApiUserProperty_GetApiUserPropertyListByPropertyId", new SqlParameter("@PropertyId", propertyId));
            return Data.Rows.Cast<DataRow>().Select(row => new ApiUser((int) row["ApiUserId"])).Where(user => user.ApiUserId > 0).ToList();
        }

        public List<ApiUser> GetApiUserListByPartnerCompanyId(int partnerCompanyId)
        {
            var Data = DataAccess.ExecuteDataTable(EfxSettings.ConnectionString, "dbo.usp_ApiUser_GetApiUserByPartnerCompanyId", new SqlParameter("@PartnerCompanyId", partnerCompanyId));
            return Data.Rows.Cast<DataRow>().Select(row => new ApiUser((int) row["ApiUserId"])).Where(user => user.ApiUserId > 0).ToList();

        }

        public void AddPropertyToApiUser(int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_ApiUserProperty_SetSingleObject", new[] {new SqlParameter("@ApiUserId", ApiUserId), new SqlParameter("@PropertyId", propertyId)});
        }

        public void DeletePropertyFromApiUser(int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_ApiUserProperty_DeleteSingleObject", new[] {new SqlParameter("@ApiUserId", ApiUserId), new SqlParameter("@PropertyId", propertyId)});
        }

        public bool Authenticate(string password)
        {
            return AuthenticationTools.ValidatePasswordHash(password, PasswordHash, Salt);
        }

        public static int Set(int apiUserId, string username, byte[] passwordHash, byte[] salt, int? partnerCompanyId, bool isActive)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(EfxSettings.ConnectionString, "dbo.usp_ApiUser_SetSingleObject",
                new[]
                    {
                        new SqlParameter("@ApiUserId", apiUserId),
                        new SqlParameter("@Username", username),
                        new SqlParameter("@PasswordHash", passwordHash),
                        new SqlParameter("@Salt", salt),
                        new SqlParameter("@PartnerCompanyId", partnerCompanyId),
                        new SqlParameter("@IsActive", isActive)
                    });
        }

        public static int Set(ApiUser a)
        {
            return Set(a.ApiUserId, a.Username, a.PasswordHash, a.Salt, a.PartnerCompanyId, a.IsActive);
        }

        public ApiUser(int? apiUserId)
        {
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_ApiUser_GetSingleObject", new SqlParameter("@ApiUserId", apiUserId));
            if (ResultSet == null)
            {
                InitializeObject();
            }
            else
            {
                ApiUserId = (int) ResultSet[0];
                Username = ResultSet[1] as string;
                PasswordHash = ResultSet[2] as byte[];
                Salt = ResultSet[3] as byte[];
                PartnerCompanyId = ResultSet[4] as int?;
                IsActive = (bool)ResultSet[5];
            }
        }

        //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
        public ApiUser(string username)
            : this(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_ApiUser_GetApiUserByUsername", new SqlParameter("@Username", username)))
        {
        }

        public ApiUser()
        {
            InitializeObject();
        }
    }
}
