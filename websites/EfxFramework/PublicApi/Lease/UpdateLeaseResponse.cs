﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Lease
{
    [DataContract]
    public class UpdateLeaseResponse : ApiBaseResponse
    {
        [DataMember]
        public int LeaseId { get; set; }
    }
}
