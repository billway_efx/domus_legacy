﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Lease
{
    [DataContract]
    public class GetLeaseResponseV2 : ApiBaseResponse
    {
        [DataMember]
        public LeaseV2 Lease { get; set; }
    }
}
