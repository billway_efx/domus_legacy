﻿using System;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Lease
{
    [DataContract]
    public class GetLeaseRequestV2 : ApiBaseRequest
    {
        [DataMember]
        public int LeaseId { get; set; }

        [DataMember]
        public long PublicResidentId { get; set; }

        public GetLeaseResponseV2 GetLease()
        {
            if (!IsAuthenticatedUser())
                return new GetLeaseResponseV2 { 
                    TransactionDate = DateTime.UtcNow, 
                    Result = (int) LeaseResponseResult.InvalidLogin, 
                    Message = LeaseResponseMessage.InvalidLogin };

            GetLeaseResponseV2 Response;

            if (!ValidateLease(out Response))
                return Response;

            var Lease = new EfxFramework.Lease();
            var ResidentId = Renter.GetRenterIdByPublicRenterId(PublicResidentId);

            try
            {
                //Salcedo - 11/16/2015 - changed comparison operator to >= since it's possible to have LeaseId 1 in the system
                if (LeaseId >= 1)
                    Lease = new EfxFramework.Lease(LeaseId);
                else if (ResidentId > 1)
                    Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(ResidentId.Value);

                if (Lease.LeaseId < 1)
                {
                    Response.Result = (int) LeaseResponseResult.LeaseNotFound;
                    Response.Message = LeaseResponseMessage.LeaseNotFound;
                    return Response;
                }

                if (!IsUserAuthorized(Lease.PropertyId))
                    return new GetLeaseResponseV2 { 
                        TransactionDate = DateTime.UtcNow, 
                        Result = (int)LeaseResponseResult.UserNotAuthorized, 
                        Message = LeaseResponseMessage.UserNotAuthorized };

                Response.Result = (int) LeaseResponseResult.Success;
                Response.Message = LeaseResponseMessage.Success;
                Response.Lease = BuildLease(Lease);
            }
            catch
            {
                Response.Result = (int) LeaseResponseResult.GeneralFailure;
                Response.Message = LeaseResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private static LeaseV2 BuildLease(EfxFramework.Lease lease)
        {
            return new LeaseV2
                {
                    LeaseId = lease.LeaseId,
                    PropertyId = lease.PropertyId,
                    PropertyStaffId = lease.PropertyStaffId,
                    UnitNumber = lease.UnitNumber,
                    RentAmount = lease.RentAmount,
                    RentDueDayOfTheMonth = lease.RentDueDayOfMonth,
                    NumberOfTenants = lease.NumberOfTenants,
                    StartDate = lease.StartDate,
                    EndDate = lease.EndDate,
                    BeginningBalance = lease.BeginningBalance,
                    BeginningBalanceDate = lease.BeginningBalanceDate,
                    ExpectedMoveInDate = lease.ExpectedMoveInDate,
                    ExpectedMoveOutDate = lease.ExpectedMoveOutDate,
                    ActualMoveInDate = lease.ActualMoveInDate,
                    ActualMoveOutDate = lease.ActualMoveOutDate,
                    LeaseSignDate = lease.LeaseSignDate,
                    CurrentBalanceDue = lease.CurrentBalanceDue,
                    CurrentBalanceDueLastUpdated = lease.CurrentBalanceDueLastUpdated 
                };
        }

        private bool ValidateLease(out GetLeaseResponseV2 response)
        {
            response = new GetLeaseResponseV2 {TransactionDate = DateTime.UtcNow};

            if (LeaseId < 1 && PublicResidentId < 1)
            {
                response.Result = (int) LeaseResponseResult.MoreInfoRequired;
                response.Message = LeaseResponseMessage.MoreInfoRequired;
                return false;
            }

            return true;
        }
    }
}
