﻿using System;
using System.Data.SqlClient;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Lease
{
    [DataContract]
    public class RemovePropertyFeeFromLeaseRequest : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public ApiLeaseFee LeaseFee { get; set; }

        //This variable is set in the ValidateLeaseFee function
        private int? RenterId;

        public RemovePropertyFeeFromLeaseResponse RemovePropertyFeeFromLease()
        {
            if (!IsAuthenticatedUser())
                return new RemovePropertyFeeFromLeaseResponse
                {
                    TransactionDate = DateTime.UtcNow,
                    Result = (int)LeaseFeesResponseResult.InvalidLogin,
                    Message = LeaseFeesResponseMessage.InvalidLogin
                };

            RemovePropertyFeeFromLeaseResponse Response;

            if (!ValidateLeaseFee(out Response))
                return Response;

            var PropertyId = EfxFramework.Renter.GetPropertyByRenterId(LeaseFee.LeaseId);

            if (!IsUserAuthorized(PropertyId))
                return new RemovePropertyFeeFromLeaseResponse
                {
                    TransactionDate = DateTime.UtcNow,
                    Result = (int)LeaseFeesResponseResult.UserNotAuthorized,
                    Message = LeaseFeesResponseMessage.UserNotAuthorized
                };

            try
            {
                EfxFramework.LeaseFee.DeleteLeaseFee(LeaseFee.MonthlyFeeId, LeaseFee.LeaseId, RenterId.Value);
                Response.Result = (int)LeaseFeesResponseResult.Success;
                Response.Message = LeaseFeesResponseMessage.Success;
            }
            catch
            {
                Response.Result = (int)LeaseFeesResponseResult.GeneralFailure;
                Response.Message = LeaseFeesResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool ValidateLeaseFee(out RemovePropertyFeeFromLeaseResponse response)
        {
            response = new RemovePropertyFeeFromLeaseResponse { TransactionDate = DateTime.UtcNow };

            if (LeaseFee == null)
            {
                response.Result = (int)LeaseFeesResponseResult.MoreInfoRequired;
                response.Message = LeaseFeesResponseMessage.MoreInfoRequired;
                return false;
            }

            if (LeaseFee.MonthlyFeeId <= 0)
            {
                response.Result = (int)LeaseFeesResponseResult.InvalidFeeId;
                response.Message = LeaseFeesResponseMessage.InvalidFeeId;
                return false;
            }

            RenterId = Renter.GetRenterIdByPublicRenterId(LeaseFee.PublicResidentId);
            if (!RenterId.HasValue)
            {
                response.Result = (int)LeaseFeesResponseResult.ResidentNotFound;
                response.Message = LeaseFeesResponseMessage.ResidentNotFound;
                return false;
            }

            var RentersOnLease = EfxFramework.Renter.GetRenterListByLeaseId(LeaseFee.LeaseId);
            bool bFound = false;
            foreach (var Re in RentersOnLease)
            {
                if (Re.RenterId == RenterId.Value)
                {
                    bFound = true;
                    break;
                }
            }
            if (!bFound)
            {
                response.Result = (int)LeaseFeesResponseResult.ResidentNotOnLease;
                response.Message = LeaseFeesResponseMessage.ResidentNotOnLease;
                return false;
            }

            return true;
        }
    }
}
