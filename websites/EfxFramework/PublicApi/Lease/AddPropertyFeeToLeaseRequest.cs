﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Lease
{
    [DataContract]
    public class AddPropertyFeeToLeaseRequest : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public ApiLeaseFee LeaseFee { get; set; }

        //This variable is set in the ValidateLeaseFee function
        private int? RenterId;

        public AddPropertyFeeToLeaseResponse AddPropertyFeeToLease()
        {
            if (!IsAuthenticatedUser())
                return new AddPropertyFeeToLeaseResponse
                {
                    TransactionDate = DateTime.UtcNow,
                    Result = (int)LeaseFeesResponseResult.InvalidLogin,
                    Message = LeaseFeesResponseMessage.InvalidLogin
                };

            AddPropertyFeeToLeaseResponse Response;

            if (!ValidateLeaseFee(out Response))
                return Response;

            try
            {
                //Add the new fee to the LeaseFee table
                var lf = new EfxFramework.LeaseFee(
                    EfxFramework.LeaseFee.Set(
                        LeaseFee.MonthlyFeeId,
                        LeaseFee.LeaseId,
                        RenterId.Value,
                        LeaseFee.FeeAmount,
                        true), 
                    LeaseFee.LeaseId,
                    RenterId.Value,
                    true);

                if (lf.MonthlyFeeId < 1)
                {
                    Response.Result = (int)LeaseFeesResponseResult.GeneralFailure;
                    Response.Message = LeaseFeesResponseMessage.GeneralFailure;
                    return Response;
                }

                Response.Result = (int)LeaseFeesResponseResult.Success;
                Response.Message = LeaseFeesResponseMessage.Success;
                Response.LeaseFeeId = lf.MonthlyFeeId;
            }
            catch
            {
                Response.Result = (int)LeaseFeesResponseResult.GeneralFailure;
                Response.Message = LeaseFeesResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool ValidateLeaseFee(out AddPropertyFeeToLeaseResponse response)
        {
            response = new AddPropertyFeeToLeaseResponse();

            if (LeaseFee == null)
            {
                response.Result = (int)LeaseFeesResponseResult.MoreInfoRequired;
                response.Message = LeaseFeesResponseMessage.MoreInfoRequired;
                return false;
            }

            var Lease = new EfxFramework.Lease(LeaseFee.LeaseId);
            if (Lease.LeaseId != LeaseFee.LeaseId)
            {
                response.Result = (int)LeaseFeesResponseResult.LeaseNotFound;
                response.Message = LeaseFeesResponseMessage.LeaseNotFound;
                return false;
            }

            RenterId = Renter.GetRenterIdByPublicRenterId(LeaseFee.PublicResidentId);
            if (!RenterId.HasValue)
            {
                response.Result = (int)LeaseFeesResponseResult.ResidentNotFound;
                response.Message = LeaseFeesResponseMessage.ResidentNotFound;
                return false;
            }

            var PropertyId = EfxFramework.Renter.GetPropertyByRenterId(RenterId.Value);

            if (!IsUserAuthorized(PropertyId))
            {
                response.Result = (int)LeaseFeesResponseResult.UserNotAuthorized;
                response.Message = LeaseFeesResponseMessage.UserNotAuthorized;
                return false;
            }

            var RentersOnLease = EfxFramework.Renter.GetRenterListByLeaseId(LeaseFee.LeaseId);
            bool bFound = false;
            foreach (var Re in RentersOnLease)
            {
                if (Re.RenterId == RenterId.Value)
                { 
                    bFound = true;
                    break;
                }
            }
            if (!bFound)
            {
                response.Result = (int)LeaseFeesResponseResult.ResidentNotOnLease;
                response.Message = LeaseFeesResponseMessage.ResidentNotOnLease;
                return false;
            }

            if (LeaseFee.FeeAmount < 0)
            {
                response.Result = (int)LeaseFeesResponseResult.InvalidFeeAmount;
                response.Message = LeaseFeesResponseMessage.InvalidFeeAmount;
                return false;
            }

            return true;
        }
    }
}
