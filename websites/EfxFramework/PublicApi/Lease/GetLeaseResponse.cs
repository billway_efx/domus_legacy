﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Lease
{
    [DataContract]
    public class GetLeaseResponse : ApiBaseResponse
    {
        [DataMember]
        public Lease Lease { get; set; }
    }
}
