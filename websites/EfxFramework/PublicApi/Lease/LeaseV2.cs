﻿using System;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Lease
{
    [DataContract]
    public class LeaseV2
    {
        [DataMember]
        public int LeaseId { get; set; }

        [DataMember]
        public int PropertyId { get; set; }

        [DataMember]
        public int PropertyStaffId { get; set; }

        [DataMember]
        public string UnitNumber { get; set; }

        [DataMember]
        public decimal? RentAmount { get; set; }

        [DataMember]
        public int? RentDueDayOfTheMonth { get; set; }

        [DataMember]
        public int? NumberOfTenants { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public decimal? BeginningBalance { get; set; }

        [DataMember]
        public DateTime? BeginningBalanceDate { get; set; }

        [DataMember]
        public DateTime? ExpectedMoveInDate { get; set; }

        [DataMember]
        public DateTime? ExpectedMoveOutDate { get; set; }

        [DataMember]
        public DateTime? ActualMoveInDate { get; set; }

        [DataMember]
        public DateTime? ActualMoveOutDate { get; set; }

        [DataMember]
        public DateTime? LeaseSignDate { get; set; }

        [DataMember]
        public decimal? CurrentBalanceDue { get; set; }

        [DataMember]
        public DateTime? CurrentBalanceDueLastUpdated { get; set; }

        public LeaseV2()
        {
            
        }
    }
}
