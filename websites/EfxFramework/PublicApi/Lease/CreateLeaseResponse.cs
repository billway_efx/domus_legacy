﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Lease
{
    [DataContract]
    public class CreateLeaseResponse : ApiBaseResponse
    {
        [DataMember]
        public int LeaseId { get; set; }
    }
}
