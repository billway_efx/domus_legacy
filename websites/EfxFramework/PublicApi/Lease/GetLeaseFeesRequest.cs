﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace EfxFramework.PublicApi.Lease
{
    [DataContract]
    public class GetLeaseFeesRequest : ApiBaseRequest
    {
        [DataMember]
        public int LeaseId { get; set; }

        public GetLeaseFeesResponse GetLeaseFees()
        {
            if (!IsAuthenticatedUser())
                return new GetLeaseFeesResponse
                {
                    TransactionDate = DateTime.UtcNow,
                    Result = (int)LeaseFeesResponseResult.InvalidLogin,
                    Message = LeaseFeesResponseMessage.InvalidLogin
                };

            GetLeaseFeesResponse Response;

            if (!ValidateRequest(out Response))
                return Response;

            var PropertyId = EfxFramework.Renter.GetPropertyByRenterId(LeaseId);

            if (!IsUserAuthorized(PropertyId))
                return new GetLeaseFeesResponse
                {
                    TransactionDate = DateTime.UtcNow,
                    Result = (int)LeaseFeesResponseResult.UserNotAuthorized,
                    Message = LeaseFeesResponseMessage.UserNotAuthorized
                };

            Response.LeaseFees = new List<ApiLeaseFee>();

            try
            {
                Response.LeaseFees = GetLeaseFeesList();

                Response.Result = (int)LeaseFeesResponseResult.Success;
                Response.Message = LeaseFeesResponseMessage.Success;
            }
            catch
            {
                Response.Result = (int)LeaseFeesResponseResult.GeneralFailure;
                Response.Message = LeaseFeesResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private List<ApiLeaseFee> GetLeaseFeesList()
        {
            List<ApiLeaseFee> fees = new List<ApiLeaseFee>();
            SqlDataReader dr = NewDataAccess.ExecuteProcedure(EfxSettings.ConnectionString,
                "dbo.usp_LeaseFee_GetLeaseFeeDetails",
                new[] { new SqlParameter("@LeaseId", LeaseId) });
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    fees.Add(new ApiLeaseFee()
                    {
                        MonthlyFeeId = dr.GetInt32(dr.GetOrdinal("MonthlyFeeId")),
                        LeaseId = LeaseId,
                        PublicResidentId = (long)dr.GetDecimal(dr.GetOrdinal("PublicRenterId")),
                        FeeAmount = dr.GetDecimal(dr.GetOrdinal("FeeAmount")),
                        DateCreated = dr.GetDateTime(dr.GetOrdinal("DateCreated"))
                    });
                }
            }
            return fees;
        }

        private bool ValidateRequest(out GetLeaseFeesResponse response)
        {
            response = new GetLeaseFeesResponse { TransactionDate = DateTime.UtcNow };

            //if (StartDate >= EndDate)
            //{
            //    response.Result = (int)LeaseFeesResponseResult.InvalidEndDate;
            //    response.Message = LeaseFeesResponseMessage.InvalidEndDate;
            //    return false;
            //}

            return true;
        }

    }
}
