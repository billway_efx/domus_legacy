﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Lease
{
    [DataContract]
    public class UpdateLeaseRequestV2 : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public LeaseV2 Lease { get; set; }

        [DataMember]
        public long PublicResidentId { get; set; }

        public UpdateLeaseResponseV2 UpdateLease()
        {
            if (!IsAuthenticatedUser())
                return new UpdateLeaseResponseV2 { 
                    TransactionDate = DateTime.UtcNow, 
                    Result = (int) LeaseResponseResult.InvalidLogin, 
                    Message = LeaseResponseMessage.InvalidLogin };

            UpdateLeaseResponseV2 Response;

            if (!ValidateLease(out Response))
                return Response;

            if (!IsUserAuthorized(new EfxFramework.Lease(Lease.LeaseId).PropertyId))
                return new UpdateLeaseResponseV2 { 
                    TransactionDate = DateTime.UtcNow, 
                    Result = (int)LeaseResponseResult.UserNotAuthorized, 
                    Message = LeaseResponseMessage.UserNotAuthorized };
            
            if (!IsUserAuthorized(Lease.PropertyId))
                return new UpdateLeaseResponseV2 { 
                    TransactionDate = DateTime.UtcNow, 
                    Result = (int)LeaseResponseResult.UserNotAuthorized, 
                    Message = LeaseResponseMessage.UserNotAuthorized };

            var L = new EfxFramework.Lease(Lease.LeaseId)
                {
                    PropertyId = Lease.PropertyId,
                    PropertyStaffId = Lease.PropertyStaffId,
                    UnitNumber = Lease.UnitNumber,
                    RentAmount = Lease.RentAmount.HasValue ? Lease.RentAmount.Value : 0.00M,
                    RentDueDayOfMonth = Lease.RentDueDayOfTheMonth.HasValue ? Lease.RentDueDayOfTheMonth.Value : 1,
                    NumberOfTenants = Lease.NumberOfTenants.HasValue ? Lease.NumberOfTenants.Value : 0,
                    StartDate = Lease.StartDate.HasValue ? Lease.StartDate.Value : DateTime.UtcNow,
                    EndDate = Lease.EndDate.HasValue ? Lease.EndDate.Value : DateTime.UtcNow.AddMonths(1),
                    BeginningBalance = Lease.BeginningBalance.HasValue ? Lease.BeginningBalance.Value : 0.00M,
                    BeginningBalanceDate = Lease.BeginningBalanceDate.HasValue ? Lease.BeginningBalanceDate.Value : DateTime.UtcNow,
                    ExpectedMoveInDate = Lease.ExpectedMoveInDate,
                    ExpectedMoveOutDate = Lease.ExpectedMoveOutDate,
                    ActualMoveInDate = Lease.ActualMoveInDate,
                    ActualMoveOutDate = Lease.ActualMoveOutDate,
                    LeaseSignDate = Lease.LeaseSignDate,
                    CurrentBalanceDue = Lease.CurrentBalanceDue,
                    CurrentBalanceDueLastUpdated = Lease.CurrentBalanceDueLastUpdated.HasValue ? 
                        Lease.CurrentBalanceDueLastUpdated.Value : DateTime.UtcNow 
                };

            try
            {
                var LeaseId = EfxFramework.Lease.Set(L);

                if (LeaseId < 1)
                {
                    Response.Result = (int) LeaseResponseResult.GeneralFailure;
                    Response.Message = LeaseResponseMessage.GeneralFailure;
                    return Response;
                }

                Response.Result = (int) LeaseResponseResult.Success;
                Response.Message = AssignResidentToLease(LeaseId) ? 
                    LeaseResponseMessage.Success : LeaseResponseMessage.ResidentAssignmentFailedOperationSuccess;
                Response.LeaseId = LeaseId;
            }
            catch
            {
                Response.Result = (int) LeaseResponseResult.GeneralFailure;
                Response.Message = LeaseResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool AssignResidentToLease(int leaseId)
        {
            if (PublicResidentId > 0)
            {
                var ResidentId = Renter.GetRenterIdByPublicRenterId(PublicResidentId);

                if (ResidentId.HasValue && ResidentId.Value > 0)
                {
                    try
                    {
                        EfxFramework.Lease.AssignRenterToLease(leaseId, ResidentId.Value);
                    }
                    catch
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private bool ValidateLease(out UpdateLeaseResponseV2 response)
        {
            response = new UpdateLeaseResponseV2{TransactionDate = DateTime.UtcNow};

            if (Lease == null)
            {
                response.Result = (int) LeaseResponseResult.MoreInfoRequired;
                response.Message = LeaseResponseMessage.MoreInfoRequired;
                return false;
            }

            if (Lease.LeaseId < 1)
            {
                response.Result = (int) LeaseResponseResult.LeaseIdRequired;
                response.Message = LeaseResponseMessage.LeaseIdRequired;
                return false;
            }

            if (new EfxFramework.Lease(Lease.LeaseId).LeaseId < 1)
            {
                response.Result = (int) LeaseResponseResult.LeaseNotFound;
                response.Message = LeaseResponseMessage.LeaseNotFound;
                return false;
            }

            if (Lease.PropertyId < 1)
            {
                response.Result = (int) LeaseResponseResult.PropertyIdRequired;
                response.Message = LeaseResponseMessage.PropertyIdRequired;
                return false;
            }

            if (Lease.PropertyStaffId < 1)
            {
                response.Result = (int) LeaseResponseResult.PropertyStaffIdRequired;
                response.Message = LeaseResponseMessage.PropertyStaffIdRequired;
                return false;
            }

            if (Lease.RentDueDayOfTheMonth.HasValue && (Lease.RentDueDayOfTheMonth.Value < 1 || Lease.RentDueDayOfTheMonth.Value > 31))
            {
                response.Result = (int)LeaseResponseResult.RentDueDayOutOfRange;
                response.Message = LeaseResponseMessage.RentDueDayOutOfRange;
                return false;
            }

            var Property = new EfxFramework.Property(Lease.PropertyId);
            var PropertyStaff = new PropertyStaff(Lease.PropertyStaffId);

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                response.Result = (int)LeaseResponseResult.PropertyNotFound;
                response.Message = LeaseResponseMessage.PropertyNotFound;
                return false;
            }

            if (!PropertyStaff.PropertyStaffId.HasValue || PropertyStaff.PropertyStaffId.Value < 1)
            {
                response.Result = (int)LeaseResponseResult.PropertyStaffNotFound;
                response.Message = LeaseResponseMessage.PropertyStaffNotFound;
                return false;
            }

            if (PublicResidentId > 0)
            {
                var ResidentId = Renter.GetRenterIdByPublicRenterId(PublicResidentId);

                if (!ResidentId.HasValue || ResidentId.Value < 1)
                {
                    response.Result = (int) LeaseResponseResult.ResidentNotFound;
                    response.Message = LeaseResponseMessage.ResidentNotFound;
                    return false;
                }

                var ResidentProperty = Renter.GetPropertyByRenterId(ResidentId.Value);

                if (ResidentProperty != Property.PropertyId.Value)
                {
                    response.Result = (int)LeaseResponseResult.NoResidentPropertyMatch;
                    response.Message = LeaseResponseMessage.NoResidentPropertyMatch;
                    return false;
                }
            }

            var StaffProperty = (from P in EfxFramework.Property.GetPropertyListByPropertyStaffId(PropertyStaff.PropertyStaffId.Value)
                                 where P.PropertyId.Value == Lease.PropertyId
                                 select P);

            if (!StaffProperty.Any())
            {
                response.Result = (int)LeaseResponseResult.NoPropertyStaffPropertyMatch;
                response.Message = LeaseResponseMessage.NoPropertyStaffPropertyMatch;
                return false;
            }

            return true;
        }
    }
}
