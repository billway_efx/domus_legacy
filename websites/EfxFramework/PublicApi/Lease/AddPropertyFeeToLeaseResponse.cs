﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Lease
{
    [DataContract]
    public class AddPropertyFeeToLeaseResponse : ApiBaseResponse
    {
        [DataMember]
        public int LeaseFeeId { get; set; }
    }
}
