﻿using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Lease
{
    [DataContract]
    public class UpdateLeaseResponseV2 : ApiBaseResponse
    {
        [DataMember]
        public int LeaseId { get; set; }
    }
}
