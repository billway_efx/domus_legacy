﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Lease
{
    [DataContract]
    public class GetLeaseFeesResponse : ApiBaseResponse
    {
        [DataMember]
        public List<ApiLeaseFee> LeaseFees { get; set; }
    }
}
