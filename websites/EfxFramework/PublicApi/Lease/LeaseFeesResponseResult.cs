﻿namespace EfxFramework.PublicApi.Lease
{
    public enum LeaseFeesResponseResult
    {
        Success = 0,
        GeneralFailure = 2,
        InvalidLogin = 3,
        LeaseNotFound = 9999,
        MoreInfoRequired = 9998,
        PropertyIdRequired = 9997,
        PropertyStaffIdRequired = 9996,
        PublicResidentIdRequired = 9995,
        PropertyNotFound = 9994,
        PropertyStaffNotFound = 9993,
        ResidentNotFound = 9992,
        NoPropertyStaffPropertyMatch = 9991,
        NoResidentPropertyMatch = 9990,
        ResidentAssignmentFailed = 9989,
        LeaseIdRequired = 9987,
        ResidentHasTransactions = 9986,
        RentDueDayOutOfRange = 9985,
        UserNotAuthorized = 9984,
        ResidentNotOnLease = 9983,
        InvalidFeeAmount = 9982,
        InvalidEndDate = 9981,
        InvalidFeeId = 9980
    }

    public static class LeaseFeesResponseMessage
    {
        public static string Success { get { return "Successfully completed the operation"; } }
        public static string GeneralFailure { get { return "A general failure occurred, please check your inputs and try the operation again"; } }
        public static string InvalidLogin { get { return "Invalid login credentials were supplied.  Please check your information and try again"; } }
        public static string LeaseNotFound { get { return "No Lease found with the provided information"; } }
        public static string MoreInfoRequired { get { return "More information is required to find a Lease"; } }
        public static string PropertyIdRequired { get { return "Property ID is required"; } }
        public static string PropertyStaffIdRequired { get { return "Property Staff ID is required"; } }
        public static string PublicResidentIdRequired { get { return "Public Resident ID is required"; } }
        public static string PropertyNotFound { get { return "Property not found"; } }
        public static string PropertyStaffNotFound { get { return "Property Stafff not found"; } }
        public static string ResidentNotFound { get { return "Resident not found"; } }
        public static string NoPropertyStaffPropertyMatch { get { return "Property Staff is not part of the Property"; } }
        public static string NoResidentPropertyMatch { get { return "Resident is not a resident of the Property"; } }
        public static string ResidentAssignmentAndOperationFailed { get { return "The Resident assignment failed causing the Lease creation to be rejected, please check your information and try again"; } }
        public static string ResidentAssignmentFailedOperationSuccess { get { return "Lease successfully updated, but the Resident assignment failed, please check your information and try again"; } }
        public static string LeaseIdRequired { get { return "Lease ID is required to complete the operation"; } }
        public static string ResidentHasTransactions { get { return "Resident associated with Lease has transactions against them, cannot delete Lease"; } }
        public static string RentDueDayOutOfRange { get { return "Rent Due Day of the Month must be a number between 1 and 31, please try again"; } }
        public static string UserNotAuthorized { get { return "The logged in user is not authorized to perform this operation"; } }
        public static string ResidentNotOnLease { get { return "The specified resident is not on the specified lease"; } }
        public static string InvalidFeeAmount { get { return "The Fee Amount must be non-negative."; } }
        public static string InvalidEndDate { get { return "The End Date must be a later date than the start date.  Please check your information and try again."; } }
        public static string InvalidFeeId { get { return "Invalid Fee Id."; } }
    }
}
