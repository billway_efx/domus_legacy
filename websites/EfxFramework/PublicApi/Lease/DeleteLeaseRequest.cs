﻿using System;
using System.Data.SqlClient;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Lease
{
    [DataContract]
    public class DeleteLeaseRequest : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public int LeaseId { get; set; }

        public DeleteLeaseResponse DeleteLease()
        {
            if (!IsAuthenticatedUser())
                return new DeleteLeaseResponse { TransactionDate = DateTime.UtcNow, Result = (int) LeaseResponseResult.InvalidLogin, Message = LeaseResponseMessage.InvalidLogin };

            DeleteLeaseResponse Response;

            if (!ValidateLease(out Response))
                return Response;

            if (!IsUserAuthorized(new EfxFramework.Lease(LeaseId).PropertyId))
                return new DeleteLeaseResponse { TransactionDate = DateTime.UtcNow, Result = (int)LeaseResponseResult.UserNotAuthorized, Message = LeaseResponseMessage.UserNotAuthorized };

            try
            {
                EfxFramework.Lease.DeleteLeaseByLeaseId(LeaseId);
                Response.Result = (int) LeaseResponseResult.Success;
                Response.Message = LeaseResponseMessage.Success;
            }
            catch (SqlException Ex)
            {
                if (Ex.Number == 547)
                {
                    Response.Result = (int) LeaseResponseResult.ResidentHasTransactions;
                    Response.Message = LeaseResponseMessage.ResidentHasTransactions;
                }
                else
                {
                    Response.Result = (int) LeaseResponseResult.GeneralFailure;
                    Response.Message = LeaseResponseMessage.GeneralFailure;
                }
            }
            catch
            {
                Response.Result = (int) LeaseResponseResult.GeneralFailure;
                Response.Message = LeaseResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool ValidateLease(out DeleteLeaseResponse response)
        {
            response = new DeleteLeaseResponse {TransactionDate = DateTime.UtcNow};

            if (LeaseId < 1)
            {
                response.Result = (int) LeaseResponseResult.LeaseIdRequired;
                response.Message = LeaseResponseMessage.LeaseIdRequired;
                return false;
            }

            var Lease = new EfxFramework.Lease(LeaseId);

            if (Lease.LeaseId < 1)
            {
                response.Result = (int) LeaseResponseResult.LeaseNotFound;
                response.Message = LeaseResponseMessage.LeaseNotFound;
                return false;
            }

            return true;
        }
    }
}
