﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Lease
{
    [DataContract]
    public class CreateLeaseRequest : ApiBaseRequest
    {
        [DataMember(IsRequired = true)]
        public Lease Lease { get; set; }

        [DataMember(IsRequired = true)]
        public long PublicResidentId { get; set; }

        public CreateLeaseResponse CreateLease()
        {
            if (!IsAuthenticatedUser())
                return new CreateLeaseResponse { TransactionDate = DateTime.UtcNow, Result = (int) LeaseResponseResult.InvalidLogin, Message = LeaseResponseMessage.InvalidLogin };

            CreateLeaseResponse Response;

            if (!ValidateLease(out Response))
                return Response;

            if (!IsUserAuthorized(Lease.PropertyId))
                return new CreateLeaseResponse { TransactionDate = DateTime.UtcNow, Result = (int)LeaseResponseResult.UserNotAuthorized, Message = LeaseResponseMessage.UserNotAuthorized };

            var L = new EfxFramework.Lease
                {
                    PropertyId = Lease.PropertyId,
                    PropertyStaffId = Lease.PropertyStaffId,
                    UnitNumber = Lease.UnitNumber ?? String.Empty,
                    RentAmount = Lease.RentAmount.HasValue ? Lease.RentAmount.Value : 0.00M,
                    RentDueDayOfMonth = Lease.RentDueDayOfTheMonth.HasValue ? Lease.RentDueDayOfTheMonth.Value : 1,
                    NumberOfTenants = Lease.NumberOfTenants.HasValue ? Lease.NumberOfTenants.Value : 0,
                    StartDate = Lease.StartDate.HasValue ? Lease.StartDate.Value : DateTime.UtcNow,
                    EndDate = Lease.EndDate.HasValue ? Lease.EndDate.Value : DateTime.UtcNow.AddMonths(1),
                    BeginningBalance = Lease.BeginningBalance.HasValue ? Lease.BeginningBalance.Value : 0.00M,
                    BeginningBalanceDate = Lease.BeginningBalanceDate.HasValue ? Lease.BeginningBalanceDate.Value : DateTime.UtcNow,
                    ExpectedMoveInDate = Lease.ExpectedMoveInDate,
                    ExpectedMoveOutDate = Lease.ExpectedMoveOutDate,
                    ActualMoveInDate = Lease.ActualMoveInDate,
                    ActualMoveOutDate = Lease.ActualMoveOutDate,
                    LeaseSignDate = Lease.LeaseSignDate,
                    CurrentBalanceDue = 0.00M,
                    PaymentDueDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.AddMonths(1).Month, Lease.RentDueDayOfTheMonth.HasValue ? Lease.RentDueDayOfTheMonth.Value : 1),
                    CurrentBalanceDueLastUpdated = DateTime.UtcNow
                };

            try
            {
                var LeaseId = EfxFramework.Lease.Set(L);

                if (LeaseId < 1)
                {
                    Response.Result = (int) LeaseResponseResult.GeneralFailure;
                    Response.Message = LeaseResponseMessage.GeneralFailure;
                    return Response;
                }

                Response = AssignResidentToLease(LeaseId, Response);

                if (Response.Result == (int) LeaseResponseResult.GeneralFailure || Response.Result == (int) LeaseResponseResult.ResidentAssignmentFailed)
                    return Response;

                Response.Message = LeaseResponseMessage.Success;
                Response.Result = (int) LeaseResponseResult.Success;
                Response.LeaseId = LeaseId;
            }
            catch
            {
                Response.Result = (int) LeaseResponseResult.GeneralFailure;
                Response.Message = LeaseResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private CreateLeaseResponse AssignResidentToLease(int leaseId, CreateLeaseResponse response)
        {
            var RenterId = Renter.GetRenterIdByPublicRenterId(PublicResidentId);

            if (!RenterId.HasValue || RenterId.Value < 1)
            {
                response.Result = (int)LeaseResponseResult.GeneralFailure;
                response.Message = LeaseResponseMessage.GeneralFailure;
                return response;
            }

            try
            {
                EfxFramework.Lease.AssignRenterToLease(leaseId, RenterId.Value);
            }
            catch
            {
                EfxFramework.Lease.DeleteLeaseByLeaseId(leaseId);
                response.Result = (int) LeaseResponseResult.ResidentAssignmentFailed;
                response.Message = LeaseResponseMessage.ResidentAssignmentAndOperationFailed;
            }

            return response;
        }

        private bool ValidateLease(out CreateLeaseResponse response)
        {
            response = new CreateLeaseResponse {TransactionDate = DateTime.UtcNow};

            if (Lease == null)
            {
                response.Result = (int) LeaseResponseResult.MoreInfoRequired;
                response.Message = LeaseResponseMessage.MoreInfoRequired;
                return false;
            }

            if (Lease.PropertyId < 1)
            {
                response.Result = (int) LeaseResponseResult.PropertyIdRequired;
                response.Message = LeaseResponseMessage.PropertyIdRequired;
                return false;
            }

            if (Lease.PropertyStaffId < 1)
            {
                response.Result = (int) LeaseResponseResult.PropertyStaffIdRequired;
                response.Message = LeaseResponseMessage.PropertyStaffIdRequired;
                return false;
            }

            if (Lease.RentDueDayOfTheMonth.HasValue && (Lease.RentDueDayOfTheMonth.Value < 1 || Lease.RentDueDayOfTheMonth.Value > 31))
            {
                response.Result = (int) LeaseResponseResult.RentDueDayOutOfRange;
                response.Message = LeaseResponseMessage.RentDueDayOutOfRange;
                return false;
            }

            if (PublicResidentId < 1)
            {
                response.Result = (int) LeaseResponseResult.PublicResidentIdRequired;
                response.Message = LeaseResponseMessage.PublicResidentIdRequired;
                return false;
            }

            var Property = new EfxFramework.Property(Lease.PropertyId);
            var PropertyStaff = new PropertyStaff(Lease.PropertyStaffId);
            var Resident = new Renter(Renter.GetRenterIdByPublicRenterId(PublicResidentId));

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                response.Result = (int) LeaseResponseResult.PropertyNotFound;
                response.Message = LeaseResponseMessage.PropertyNotFound;
                return false;
            }

            if (!PropertyStaff.PropertyStaffId.HasValue || PropertyStaff.PropertyStaffId.Value < 1)
            {
                response.Result = (int) LeaseResponseResult.PropertyStaffNotFound;
                response.Message = LeaseResponseMessage.PropertyStaffNotFound;
                return false;
            }

            if (Resident.RenterId < 1)
            {
                response.Result = (int) LeaseResponseResult.ResidentNotFound;
                response.Message = LeaseResponseMessage.ResidentNotFound;
                return false;    
            }

            //Salcedo - 11/19/2015 - task #00523 - if this resident already has a lease, disallow creation of another lease
            var TestLease = EfxFramework.Lease.GetRenterLeaseByRenterId(Resident.RenterId);
            if (TestLease.LeaseId > 0)
            {
                response.Result = (int)LeaseResponseResult.ResidentAlreadyHasLease;
                response.Message = LeaseResponseMessage.ResidentAlreadyHasLease;
                return false;
            }

            var ResidentProperty = Renter.GetPropertyByRenterId(Resident.RenterId);
            var StaffProperty = (from P in EfxFramework.Property.GetPropertyListByPropertyStaffId(PropertyStaff.PropertyStaffId.Value)
                                 where P.PropertyId.Value == Lease.PropertyId
                                 select P);

            if (!StaffProperty.Any())
            {
                response.Result = (int) LeaseResponseResult.NoPropertyStaffPropertyMatch;
                response.Message = LeaseResponseMessage.NoPropertyStaffPropertyMatch;
                return false;
            }

            if (ResidentProperty != Property.PropertyId.Value)
            {
                response.Result = (int) LeaseResponseResult.NoResidentPropertyMatch;
                response.Message = LeaseResponseMessage.NoResidentPropertyMatch;
                return false;
            }

            return true;
        }
    }
}
