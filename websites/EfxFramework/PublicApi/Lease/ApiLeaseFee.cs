﻿using System;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi.Lease
{
    [DataContract]
    public class ApiLeaseFee
    {
        [DataMember]
        public int MonthlyFeeId { get; set; }

        [DataMember]
        public int LeaseId { get; set; }

        [DataMember]
        public long PublicResidentId { get; set; }

        [DataMember]
        public decimal FeeAmount { get; set; }

        [DataMember]
        public DateTime? DateCreated { get; set; }

    }
}
