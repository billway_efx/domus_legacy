﻿using System;
using System.Runtime.Serialization;
using Mb2x.ExtensionMethods;

namespace EfxFramework.PublicApi.Marketing
{
    [DataContract]
    public class AddPropertyLeadRequest : ApiBaseRequest
    {
        [DataMember]
        public string PropertyManagementCompanyName { get; set; }

        [DataMember]
        public string PropertyName { get; set; }

        [DataMember]
        public string Address1 { get; set; }

        [DataMember]
        public string Address2 { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string PostalCode { get; set; }

        [DataMember]
        public string TelephoneNumber { get; set; }

        [DataMember]
        public string FaxNumber { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember(IsRequired = true)]
        public string ContactFirstName { get; set; }

        [DataMember(IsRequired = true)]
        public string ContactLastName { get; set; }

        public AddPropertyLeadResponse AddPropertyLead()
        {
            if (!IsAuthenticatedUser())
                return new AddPropertyLeadResponse {TransactionDate = DateTime.UtcNow, Result = (int) MarketingResponseResult.InvalidLogin, Message = MarketingResponseMessage.InvalidLogin};

            AddPropertyLeadResponse Response;

            if (!ValidatePropertyLead(out Response))
                return Response;

            var Lead = new PropertyLead
                {
                    PropertyManagementCompanyName = PropertyManagementCompanyName,
                    PropertyName = PropertyName,
                    ContactFirstName = ContactFirstName,
                    ContactLastName = ContactLastName,
                    Address = Address1,
                    Address2 = Address2,
                    City = City,
                    StateProvinceId = GetStateProvinceId(State),
                    PostalCode = PostalCode,
                    Phone = TelephoneNumber,
                    Fax = FaxNumber,
                    EmailAddress = EmailAddress,
                    DateSubmitted = DateTime.Now 
                };

            try
            {
                Lead = new PropertyLead(PropertyLead.Set(Lead));
                
                if (Lead.PropertyLeadId > 0)
                {
                    Response.Result = (int) MarketingResponseResult.Success;
                    Response.Message = MarketingResponseMessage.Success;
                }
                else
                {
                    Response.Result = (int)MarketingResponseResult.GeneralFailure;
                    Response.Message = MarketingResponseMessage.GeneralFailure;
                }
            }
            catch
            {
                Response.Result = (int) MarketingResponseResult.GeneralFailure;
                Response.Message = MarketingResponseMessage.GeneralFailure;
            }

            return Response;
        }

        private bool ValidatePropertyLead(out AddPropertyLeadResponse response)
        {
            response = new AddPropertyLeadResponse {TransactionDate = DateTime.UtcNow};
            StateProvince StateProvince;

            if (String.IsNullOrEmpty(TelephoneNumber) && String.IsNullOrEmpty(EmailAddress))
            {
                response.Result = (int) MarketingResponseResult.PhoneNumberOrEmailRequired;
                response.Message = MarketingResponseMessage.PhoneNumberOrEmailRequired;
                return false;
            }

            if (!String.IsNullOrEmpty(EmailAddress) && !EmailAddress.IsValidEmailAddress())
            {
                response.Result = (int) MarketingResponseResult.InvalidEmail;
                response.Message = MarketingResponseMessage.InvalidEmail;
                return false;
            }

            if (String.IsNullOrEmpty(ContactFirstName))
            {
                response.Result = (int) MarketingResponseResult.FirstNameRequired;
                response.Message = MarketingResponseMessage.FirstNameRequired;
                return false;
            }

            if (String.IsNullOrEmpty(ContactLastName))
            {
                response.Result = (int) MarketingResponseResult.LastNameRequired;
                response.Message = MarketingResponseMessage.LastNameRequired;
                return false;
            }

            if (!String.IsNullOrEmpty(State) && !Enum.TryParse(State, out StateProvince))
            {
                response.Result = (int) MarketingResponseResult.InvalidStateFormat;
                response.Message = MarketingResponseMessage.InvalidStateFormat;
                return false;
            }

            return true;
        }
    }
}
