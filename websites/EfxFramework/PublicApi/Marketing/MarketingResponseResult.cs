﻿namespace EfxFramework.PublicApi.Marketing
{
    public enum MarketingResponseResult
    {
        Success = 0,
        GeneralFailure = 2,
        InvalidLogin = 3,
        PhoneNumberOrEmailRequired = 9999,
        FirstNameRequired = 9998,
        LastNameRequired = 9997,
        InvalidStateFormat = 9996,
        InvalidEmail = 9995
    }

    public static class MarketingResponseMessage
    {
        public static string Success { get { return "Successfully completed the operation"; } }
        public static string GeneralFailure { get { return "A general failure occurred, please check your inputs and try the operation again"; } }
        public static string InvalidLogin { get { return "Invalid login credentials were supplied.  Please check your information and try again"; } }
        public static string PhoneNumberOrEmailRequired { get { return "Either a telephone number or an email address is required"; } }
        public static string FirstNameRequired { get { return "Contact First Name is required"; } }
        public static string LastNameRequired { get { return "Contact Last Name is required"; } }
        public static string InvalidStateFormat { get { return "State must be in the two character state code format"; } }
        public static string InvalidEmail { get { return "Email address is not valid"; } }
    }
}
