﻿using System;
using System.Runtime.Serialization;

namespace EfxFramework.PublicApi
{
    [DataContract]
    public abstract class ApiBaseResponse
    {
        [DataMember]
        public DateTime TransactionDate { get; set; }

        [DataMember]
        public int Result { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
