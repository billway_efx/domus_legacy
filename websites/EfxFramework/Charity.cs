using Mb2x.Data;
using Mb2x.ExtensionMethods;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace EfxFramework
{
    public class Charity : BaseEntity
    {
        //Private Members
        private TelephoneNumber _ContactPhone;
        private TelephoneNumber _Phone;
        private TelephoneNumber _Fax;

        //Public Properties
        public int CharityId { get; set; }
        public string CharityName { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactTitle { get; set; }
        public string ContactPhone
        {
            get
            {
                return _ContactPhone== null ? string.Empty : _ContactPhone.Digits;
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _ContactPhone = null;
                else
                {
                    if (_ContactPhone == null)
                        _ContactPhone = new TelephoneNumber(value);
                    else
                        _ContactPhone.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }
        public string ContactEmailAddress { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public StateProvince? StateProvinceId { get; set; }
        public string PostalCode { get; set; }
        public string Phone
        {
            get
            {
                return _Phone== null ? string.Empty : _Phone.Digits;
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _Phone = null;
                else
                {
                    if (_Phone == null)
                        _Phone = new TelephoneNumber(value);
                    else
                        _Phone.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }
        public string Fax
        {
            get
            {
                return _Fax== null ? string.Empty : _Fax.Digits;
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _Fax = null;
                else
                {
                    if (_Fax == null)
                        _Fax = new TelephoneNumber(value);
                    else
                        _Fax.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }
        public string WebSite { get; set; }
        public string EmailAddress { get; set; }

        //Public Methods
        /// <summary>
        /// Gets a list of all Charity objects.
        /// </summary>
        public static List<Charity> GetAllCharityList()
        {
            return DataAccess.GetTypedList<Charity>(
                EfxSettings.ConnectionString,
                "dbo.usp_Charity_GetAllCharityList");
        }
        /// <summary>
        /// Gets a dictionary containing CharityId, CharityName.
        /// </summary>
        public static Dictionary<int, string> GetAllCharityDictionary()
        {
            var Charities = GetAllCharityList();
            return Charities.ToDictionary(c => c.CharityId, c => c.CharityName);
        }
        public static int Set(int charityId, string charityName, string contactFirstName, string contactLastName, string contactTitle, string contactPhone, string contactEmailAddress, string address, string address2, string city, StateProvince? stateProvinceId, string postalCode, string phone, string fax, string webSite, string emailAddress)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
                EfxSettings.ConnectionString,
                "dbo.usp_Charity_SetSingleObject",
            new[]
                {
	                new SqlParameter("@CharityId", charityId),
	                new SqlParameter("@CharityName", charityName),
	                new SqlParameter("@ContactFirstName", contactFirstName),
	                new SqlParameter("@ContactLastName", contactLastName),
	                new SqlParameter("@ContactTitle", contactTitle),
	                new SqlParameter("@ContactPhone", contactPhone.EmptyStringToNull()),
	                new SqlParameter("@ContactEmailAddress", contactEmailAddress),
	                new SqlParameter("@Address", address),
	                new SqlParameter("@Address2", address2),
	                new SqlParameter("@City", city),
	                new SqlParameter("@StateProvinceId", stateProvinceId),
	                new SqlParameter("@PostalCode", postalCode),
	                new SqlParameter("@Phone", phone.EmptyStringToNull()),
	                new SqlParameter("@Fax", fax.EmptyStringToNull()),
	                new SqlParameter("@WebSite", webSite),
	                new SqlParameter("@EmailAddress", emailAddress)
                });
        }
        public static int Set(Charity c)
        {
            return Set(
                c.CharityId,
                c.CharityName,
                c.ContactFirstName,
                c.ContactLastName,
                c.ContactTitle,
                c.ContactPhone,
                c.ContactEmailAddress,
                c.Address,
                c.Address2,
                c.City,
                c.StateProvinceId,
                c.PostalCode,
                c.Phone,
                c.Fax,
                c.WebSite,
                c.EmailAddress
            );
        }
        public Charity(int? charityId)
        {
            //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_Charity_GetSingleObject", new[] { new SqlParameter("@CharityId", charityId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
                CharityId = (int)ResultSet[0];
                CharityName = ResultSet[1] as string;
                ContactFirstName = ResultSet[2] as string;
                ContactLastName = ResultSet[3] as string;
                ContactTitle = ResultSet[4] as string;
                ContactPhone = ResultSet[5] as string;
                ContactEmailAddress = ResultSet[6] as string;
                Address = ResultSet[7] as string;
                Address2 = ResultSet[8] as string;
                City = ResultSet[9] as string;
                StateProvinceId = ResultSet[10] as StateProvince?;
                PostalCode = ResultSet[11] as string;
                Phone = ResultSet[12] as string;
                Fax = ResultSet[13] as string;
                WebSite = ResultSet[14] as string;
                EmailAddress = ResultSet[15] as string;
            }
        }
        public Charity()
        {
            InitializeObject();
        }

    }
}
