﻿using System.Web;
using EfxFramework.Interfaces;
using Mb2x.ExtensionMethods;
using System;
using System.Linq;

namespace EfxFramework.Presenters
{
    public class PropertyAdd : PropertyAddEditBase
    {
        private readonly IPropertyAdd _View;

        #region Constructors
        public PropertyAdd(IPropertyAdd view)
            : base(view)
        {
            _View = view;
            _View.SaveButtonClick = GoToNextStep;
        }
        #endregion

        #region Intitializers
        public void InitializeValues()
        {
            _View.NewPropertySteps.SetActiveView(_View.StepOne);
        }
        #endregion

        #region Event Handlers
        private void GoToNextStep(object sender, EventArgs e)
        {
            if (!Validate())
                return;

            SaveProperty();
            using(var entity = new RPO.RPOEntities())
            {
                var companyId = entity.Properties.First(p => p.PropertyId == _View.CurrentPropertyId).CompanyId;
                HttpContext.Current.Response.Redirect("~/staff.aspx?companyId=" + companyId + "&propertyId=" + _View.CurrentPropertyId);
            }
            //_View.AddStaffToProperty.AddEditPropertyStaffControl.PropertyId = _View.AddStaffToProperty.CurrentPropertyId;
            //_View.AddStaffToProperty.AddEditPropertyStaffControl.BuildPropertyStaffList();
            //_View.NewPropertySteps.SetActiveView(_View.StepTwo);
        }
        #endregion

        #region Update Methods
        protected override void SaveProperty(Property property = null)
        {
            try
            {
                var Property = property ?? BuildProperty();

                Property = BuildBankingInformation(Property);

                base.SaveProperty(Property);
            }
            catch
            {
                _View.SafeRedirect("~/ErrorPage.html");
            }
        }
        #endregion

        #region Helper Methods
        protected override Property BuildProperty()
        {
            // Patrick Whittingham - 9/2/15 - task #371 : add notification email address 
            return new Property(_View.CurrentPropertyId)
            {
                PropertyCode = _View.PropertyDetailInfo.PropertyCodeText,
                PropertyName = _View.PropertyDetailInfo.PropertyNameText,
                NumberOfUnits = _View.PropertyDetailInfo.NumberOfUnitsText.ToNullInt32(),
                StreetAddress = _View.PropertyDetailInfo.PropertyAddressText,
                StreetAddress2 = _View.PropertyDetailInfo.PropertyAddress2Text,
                City = _View.PropertyDetailInfo.CityText,
                StateProvinceId = _View.PropertyDetailInfo.StateList.SelectedValue.ToNullInt32(),
                PostalCode = _View.PropertyDetailInfo.PostalCodeText,
                MainPhoneNumber = new PhoneNumber(_View.PropertyDetailInfo.MainTelephoneText),
                AlternatePhoneNumber = new PhoneNumber(_View.PropertyDetailInfo.AlternateTelephoneText),
                FaxNumber = new PhoneNumber(_View.PropertyDetailInfo.FaxNumberText),
                OfficeEmailAddress = _View.PropertyDetailInfo.OfficeEmailText,
                NotificationEmailAddress = _View.PropertyDetailInfo.NotificationEmailText,
                AverageMonthlyRent = _View.PropertyDetailInfo.AverageRentText.Replace("$", "").ToNullDecimal(),
                ShowPaymentDetail = _View.PropertyDetailInfo.LineItemDetailChecked,
                IsVerifiedRentReportersProperty = _View.PropertyDetailInfo.RentReportersVerifiedChecked,
                //CMallory - Task 0600 - Added
                CompanyID = _View.PropertyDetailInfo.PmCompanySelectedValue.ToInt32()
            };
        }
        #endregion

        #region Validation
        protected virtual bool Validate()
        {
			return ValidatePropertyDetails(_View.ParentPage) && ValidateBankingDetails(_View.ParentPage) && ValidatePmsInformation(_View.ParentPage) && ValidateProgramInformation(_View.ParentPage);
        }
        #endregion

        #region Permission Checks
        public void CheckPermissions()
        {
            var StaffId = !String.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? HttpContext.Current.User.Identity.Name.ToInt32() : (int?)null;

            if (StaffId.HasValue && StaffId > 0)
            {
                var Staff = new PropertyStaff(StaffId);

                if ((Staff.PropertyStaffId.HasValue && Staff.PropertyStaffId.Value > 0) && Staff.CanAddProperty)
                {
                    _View.NewPropertySteps.Visible = true;
                }
                else
                {
                    _View.NewPropertySteps.Visible = false;
                    _View.PageMessage.DisplayMessage(SystemMessageType.Error, "Not Authorized to Add a New Property");
                }
            }
            else
                _View.SafeRedirect("~/Account/Login.aspx");
        }
        #endregion
    }
}
