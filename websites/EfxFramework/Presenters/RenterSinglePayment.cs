﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Mail;
using System.Text;
using EfxFramework.Interfaces;
using EfxFramework.WebPayments;
using Mb2x.ExtensionMethods;

namespace EfxFramework.Presenters
{
    internal class RenterSinglePayment : SinglePaymentBase<Renter>
    {
        private readonly IRenterSinglePayment _View;

        public RenterSinglePayment(IRenterSinglePayment view)
            : base(view)
        {
            _View = view;
        }

        protected override void ConfirmPayment(object sender, EventArgs e)
        {
            if (!ValidatePage())
                return;

            var PaymentInfo = BuildRenterPaymentInfo();
            var PaymentResponse = PaymentRequest.ProcessPayment(PaymentInfo);
            SendNotifications(PaymentResponse, PaymentInfo);
            RedirectToThankYou(PaymentResponse, PaymentInfo);
        }

        protected override void SendNotifications(PaymentResponse response, RenterPaymentInfo info)
        {
            if (response.RedirectToIvr)
                return;

            base.SendNotifications(response, info);
        }

        protected override void RedirectToThankYou(PaymentResponse response, RenterPaymentInfo info)
        {
            var QueryStringParams = new Dictionary<String, String>
                {
                    {"RentAndFeeResult", response.RentAndFeeResult.ToString().UrlEncode()},
                    {"RentAndFeeResponseMessage", response.RentAndFeeResponseMessage.UrlEncode()},
                    {"RentAndFeeTransactionId", response.RentAndFeeTransactionId.UrlEncode()},
                    {"ConvenienceFee", response.ConvenienceFee.ToString(CultureInfo.InvariantCulture).UrlEncode()},
                    {"CharityResult", response.CharityResult.ToString().UrlEncode()},
                    {"CharityResponseMessage", response.CharityResponseMessage.UrlEncode()},
                    {"CharityTransactionId", response.CharityTransactionId.UrlEncode()}, 
                    {"RentStateId", response.RentAndFeeResult == PaymentMethods.GeneralResponseResult.Success ? "1" : "2"}, 
                    {"CharityStateId", response.CharityResult == PaymentMethods.GeneralResponseResult.Success ? "1" : "2"}
                };

            if (!HasRentAmount(info))
            {
                QueryStringParams["RentStateId"] = "0";
            }

            if (!info.CharityAmount.HasValue || info.CharityAmount.Value <= 0.0M)
                QueryStringParams["CharityStateId"] = "0";

            if (response.RedirectToIvr)
            {
                QueryStringParams["RentStateId"] = "3";
                QueryStringParams["CharityStateId"] = "3";
                QueryStringParams.Add("PaymentQueueNumber", response.PaymentQueueNumber.UrlEncode());
            }

            RedirectToThankYou(QueryStringParams);
        }

        protected override void RedirectToThankYou(Dictionary<string, string> queryStringParams)
        {
            var QueryString = new StringBuilder();

            foreach (var Kvp in queryStringParams)
            {
                QueryString.AppendFormat("{0}={1}&", Kvp.Key, Kvp.Value);
            }

            //Redirect to Thankyou.aspx
            BaseView.SafeRedirect(String.Format("/OneTimePayment/ThankYou.aspx?{0}", QueryString));
        }

        protected override void SendEmail(int propertyId, string subject, string body, List<MailAddress> addresses)
        {
            //Salcedo - 2/23/2014 - changed call to include staff IsMainContacts
            //BulkMail.BulkMail.SendMailMessage(
            //    propertyId,
            //    "Renter",
            //    false,
            //    Settings.SendMailUsername,
            //    Settings.SendMailPassword,
            //    new MailAddress(Settings.PaymentEmail),
            //    subject,
            //    body,
            //    addresses
            //    );
            BulkMail.BulkMail.SendMailMessageIncludePropertyEmailContacts(
                propertyId,
                "Renter",
                EfxSettings.SendMailUsername,
                EfxSettings.SendMailPassword,
                new MailAddress(EfxSettings.PaymentEmail),
                subject,
                body,
                addresses
                );
        }

        protected override Property GetProperty()
        {
            var Renter = new Renter(Int32.Parse(_View.StepOneControl.RenterSelectionControl.RenterDropdownSelectedValue));

            if (!Renter.IsValid)
                return new Property();

            var Property = EfxFramework.Property.GetPropertyByRenterId(Renter.RenterId);

            if (Property.PropertyId.HasValue && Property.PropertyId.Value > 0)
                return Property;

            return new Property();
        }
    }
}
