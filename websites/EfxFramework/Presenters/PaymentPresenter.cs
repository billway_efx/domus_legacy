﻿using EfxFramework.ExtensionMethods;
using EfxFramework.Interfaces;
using EfxFramework.PaymentMethods;
using EfxFramework.Web;
using EfxFramework.Web.Facade;
using EfxFramework.WebPayments;
using Mb2x.ExtensionMethods;
using RPO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

namespace EfxFramework.Presenters
{
    public class PaymentPresenter
    {
        private readonly IPayment _view;

        private readonly ActivityLog _al = new ActivityLog();

        protected static string OnClass => "btn btn-primary orangeButton buttonInline";

        protected static string OffClass => "btn btn-blank grayOffButton buttonInline";

        public bool EnablePartialPayments { get; set; }

        private Property _currentlySelectedProperty;

        public Property CurrentProperty
        {
            get =>
                _currentlySelectedProperty ?? (_currentlySelectedProperty =
                    new Property(_view.PropertyList.SelectedValue.ToInt32()));
            set => _currentlySelectedProperty = value;
        }

        public PaymentPresenter(IPayment view)
        {
            _view = view;
            _view.LoadButtonClicked = LoadButtonClicked;
            _view.DifferentPayerChecked = DifferentPayerChecked;
            _view.PayNowClicked = PayNowClicked;
            _view.PayerTypeChanged = PayerTypeChanged;
            _view.PropertyList.SelectedIndexChanged += PropertyListSelectedIndexChanged;
            _view.SelectPaymentTypeControl.PaymentTypeChanged = PaymentTypeChanged;

            if (!Helpers.Helper.IsCsrUser)
            {
                _view.PaymentAmountsControl.UpdateTotalClicked = UpdateTotalClicked;
            }

            _view.PaymentAmountsControl.FeeListViewItemDataBind = FeeListViewItemDataBind;
        }

        protected void FeeListViewItemDataBind(object sender, ListViewItemEventArgs e)
        {
            var currentFee = (LeaseFee)e.Item.DataItem;

            if (currentFee == null || CurrentProperty == null) return;

            var propertyIsIntegrated = !string.IsNullOrEmpty(CurrentProperty.PmsId);

            if (!propertyIsIntegrated) return;
            var feeCheckBox = (CheckBox)e.Item.FindControl("MonthlyFeeWaiveCheckBox");

            if (feeCheckBox == null) return;

            feeCheckBox.Enabled = false;
            feeCheckBox.Visible = false;
        }

        public void InitializeValues()
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            if (!Helpers.Helper.IsRpoAdmin)
            {
                _view.PaymentAmountsControl.WaiveConvenienceFee.Checked = false;
            }

            if (_view.IsPropertyManagerSite && !_view.IsCsrUser)
                BindPmPropertyList();
            else
                BindAdminPropertyList();
            _view.ApplyToNextMonthCheckBox.Enabled = false;
            _view.RedirectApplicantLink.Enabled = false;
            TurnOff();
        }

        private void BindAdminPropertyList()
        {
            using (var entity = new RPOEntities())
            {
                var properties = entity.Properties
                    .Select(p => new PropertyListItem
                    {
                        PropertyId = p.PropertyId,
                        PropertyName = p.PropertyName
                    })
                    .OrderBy(p => p.PropertyName).ToList();

                _view.PropertyList.DataSource = properties;
                _view.PropertyList.DataBind();
            }
        }

        private void BindPmPropertyList()
        {
            var user = UIFacade.GetCurrentUser(UserType.PropertyStaff);
            if (!user.UserId.HasValue || user.UserId < 1)
            {
                BasePage.SafePageRedirect("~/Account/Login.aspx");
                return;
            }

            List<PropertyListItem> properties = null;
            using (var entity = new RPOEntities())
            {
                if (!Helpers.Helper.IsRpoAdmin)
                {
                    properties = entity.Properties
                    .Select(p => new PropertyListItem
                    {
                        PropertyId = p.PropertyId,
                        PropertyName = p.PropertyName
                    })
                    .OrderBy(p => p.PropertyName).ToList();
                    var userProperties = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyStaffId == Helpers.Helper.CurrentUserId).ToList();
                    properties = properties.Where(p => userProperties.Exists(up => up.PropertyId == p.PropertyId)).ToList();
                }

                _view.PropertyList.DataSource = properties;
                _view.PropertyList.DataBind();
            }
        }

        private void PopulateFields()
        {
            var payer = _view.PayerList.SelectedValue.ToNullInt32();
            if (!payer.HasValue || payer.Value < 1)
            {
                _view.PayNowButtonEnabled = false;
                return;
            }

            _view.PayNowButtonEnabled = true;

            var payerType = _view.SelectedPayerType.ToInt32();

            if (payerType == 0)
                PopulateRenter();
            else
                PopulateApplicant();
        }

        private void PopulateRenter()
        {
            //CMallory - Ticket 00672
            ClearFields();
            var resident = new Renter(_view.PayerList.SelectedValue.ToNullInt32());
            if (resident.RenterId < 1)
                return;
            const string errorString = "We're sorry, but this resident is currently on " + @"""Do Not Accept Payment""" + " status. Please update the resident's status in your property management software and try again!";
            var err = new CustomValidator();

            if (resident.AcceptedPaymentTypeId == 4)
            {
                err.ValidationGroup = "Payment";
                err.IsValid = false;
                err.ErrorMessage = errorString;
                _view.ParentPage.Validators.Add(err);
                _view.PayNowButtonEnabled = false;
            }

            switch (resident.PmsTypeId)
            {
                case 1 when resident.PmsId != "":
                    Pms.Yardi.ImportRequest.UpdateRenterBalanceDueRealTimeYardi(resident);
                    break;
                case 2 when resident.PmsId != "":
                    Pms.AMSI.AmsiRequest.AMSI_RequestXmlForRenterImport(2, _RenterID: resident.RenterId);
                    break;
            }

            var address = Address.GetAddressByRenterIdAndAddressType(resident.RenterId, (int)TypeOfAddress.Billing);
            var useBillingAddress = address.AddressId > 0;

            _view.PersonalInformationControl.FirstNameText = resident.FirstName;
            _view.PersonalInformationControl.MiddleNameText = resident.MiddleName;
            _view.PersonalInformationControl.LastNameText = resident.LastName;
            _view.PersonalInformationControl.SuffixText = resident.Suffix;

            if (_view.ResidentBillingInformationControl.IsSameAsMailingChecked || !useBillingAddress)
            {
                _view.ResidentBillingInformationControl.BillingAddressText =
                    $"{resident.StreetAddress} {resident.Unit}";
                _view.ResidentBillingInformationControl.CityText = resident.City;
                _view.ResidentBillingInformationControl.StateDropDown.SelectedValue = resident.StateProvinceId.HasValue ? resident.StateProvinceId.Value.ToString(CultureInfo.InvariantCulture) : "1";
                _view.ResidentBillingInformationControl.ZipText = resident.PostalCode;
            }
            else
            {
                _view.ResidentBillingInformationControl.BillingAddressText =
                    $"{address.AddressLine1} {address.AddressLine2}";
                _view.ResidentBillingInformationControl.CityText = address.City;
                _view.ResidentBillingInformationControl.StateDropDown.SelectedValue = address.StateProvinceId.ToString(CultureInfo.InvariantCulture);
                _view.ResidentBillingInformationControl.ZipText = address.PostalCode;
            }

            _view.PersonalInformationControl.EmailAddressText = resident.PrimaryEmailAddress;
            _view.PersonalInformationControl.PhoneNumberText = resident.MainPhoneNumber;

            PopulateCreditCard(resident);
            PopulateEcheck(resident);
            PopulateAutoPay(resident);
            SetRentAmounts();
        }

        private void PopulateCreditCard(Renter resident)
        {
            _view.SelectPaymentTypeControl.InitializeCreditCardExpiration();

            if (!resident.PayerId.HasValue || resident.PayerId.Value < 1)
                return;

            var creditCard = PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(resident.PayerId.Value);
            if (creditCard.PayerCreditCardId < 1)
                return;


            _view.SelectPaymentTypeControl.CreditCardDetailsControl.NameOnCardText = creditCard.CreditCardHolderName;
            _view.SelectPaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText = creditCard.CreditCardAccountNumber.MaskAccountNumber();

            if (_view.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationYearList.Items.FindByValue(creditCard.CreditCardExpirationYear.ToString(CultureInfo.InvariantCulture)) == null)
                _view.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationYearList.Items.Add(
                    new ListItem(creditCard.CreditCardExpirationYear.ToString(CultureInfo.InvariantCulture),
                        creditCard.CreditCardExpirationYear.ToString(CultureInfo.InvariantCulture)));

            _view.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationYearList.SelectedValue = creditCard.CreditCardExpirationYear.ToString(CultureInfo.InvariantCulture);
            _view.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationMonthSelectedValue = creditCard.CreditCardExpirationMonth.ToString(CultureInfo.InvariantCulture);
        }

        private void PopulateEcheck(Renter resident)
        {
            if (!resident.PayerId.HasValue || resident.PayerId.Value < 1)
                return;

            var echeck = PayerAch.GetPrimaryPayerAchByPayerId(resident.PayerId.Value);
            if (echeck.PayerAchId < 1)
                return;

            _view.SelectPaymentTypeControl.ECheckDetailsControl.AccountNumberText = echeck.BankAccountNumber.MaskAccountNumber();
            _view.SelectPaymentTypeControl.ECheckDetailsControl.RoutingNumberText = echeck.BankRoutingNumber;
        }

        private void PopulateAutoPay(Renter resident)
        {
            if (!resident.PayerId.HasValue || resident.PayerId.Value < 1)
                return;

            var autoPay = AutoPayment.GetAutoPaymentByRenterId(resident.RenterId);
            if (autoPay.AutoPaymentId < 1)
                return;

            var lease = Lease.GetRenterLeaseByRenterId(resident.RenterId);
            if (lease.LeaseId < 1)
                return;

            TurnOn();
            _view.AutoPayTileControl.IsOn = true;
            _view.AutoPayTileControl.DayOfTheMonthDropdown.SelectedValue = autoPay.PaymentDayOfMonth.ToString(CultureInfo.InvariantCulture);
            _view.AutoPayTileControl.PaymentTypeDropdown.SelectedValue = autoPay.PaymentTypeId.ToString(CultureInfo.InvariantCulture);
        }

        private void PopulateApplicant()
        {
            var applicant = new Applicant(_view.PayerList.SelectedValue.ToNullInt32());
            if (applicant.ApplicantId < 1)
                return;

            _view.PersonalInformationControl.FirstNameText = applicant.FirstName;
            _view.PersonalInformationControl.LastNameText = applicant.LastName;
            _view.PersonalInformationControl.EmailAddressText = applicant.PrimaryEmailAddress;
            _view.PersonalInformationControl.PhoneNumberText = applicant.MainPhoneNumber;
            _view.SelectPaymentTypeControl.InitializeCreditCardExpiration();
        }

        private void ClearFields()
        {
            _view.PersonalInformationControl.FirstNameText = string.Empty;
            _view.PersonalInformationControl.MiddleNameText = string.Empty;
            _view.PersonalInformationControl.LastNameText = string.Empty;
            _view.PersonalInformationControl.SuffixText = string.Empty;
            _view.ResidentBillingInformationControl.IsSameAsMailingChecked = false;
            _view.ResidentBillingInformationControl.BillingAddressText = string.Empty;
            _view.ResidentBillingInformationControl.CityText = string.Empty;
            _view.ResidentBillingInformationControl.StateDropDown.SelectedValue = "1";
            _view.PersonalInformationControl.EmailAddressText = string.Empty;
            _view.PersonalInformationControl.PhoneNumberText = string.Empty;
            _view.SelectPaymentTypeControl.CreditCardDetailsControl.NameOnCardText = string.Empty;
            _view.SelectPaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText = string.Empty;
            _view.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationMonthSelectedValue = "1";
            _view.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationYearList.SelectedIndex = -1;
            _view.SelectPaymentTypeControl.CreditCardDetailsControl.CvvText = string.Empty;
            _view.TermsChecked = false;
        }

        public void PopulateFieldsOnLoadClick()
        {
            PopulateFields();
        }

        private void LoadButtonClicked(object sender, EventArgs e)
        {
            PopulateFields();
        }

        private void DifferentPayerChecked(object sender, EventArgs e)
        {
            var differentPayer = (CheckBox)sender;

            if (differentPayer.Checked)
                ClearFields();
            else
                PopulateFields();
        }

        private void PayNowClicked(object sender, EventArgs e)
        {
            _al.WriteLog(_view.ParentPage.Page.User.Identity.Name, "Admin clicked Pay Now", (short)LogPriority.LogAlways);

            if (!_view.TermsChecked)
            {
                var err = new CustomValidator
                {
                    ValidationGroup = "Payment",
                    IsValid = false,
                    ErrorMessage = @"You must accept the Terms of Agreement before paying"
                };

                _view.ParentPage.Validators.Add(err);
                _al.WriteLog(_view.ParentPage.Page.User.Identity.Name, "Error message displayed: " + err.ErrorMessage, (short)LogPriority.LogAlways);
                return;
            }

            _view.ParentPage.Validate("Payment");
            if (!_view.ParentPage.IsValid)
            {
                _al.WriteLog(_view.ParentPage.Page.User.Identity.Name, "Parent page validation failed.", (short)LogPriority.LogAlways);
                return;
            }

            var resident = new Renter(_view.PayerList.SelectedValue.ToInt32());
            var currentCompany = Company.GetCompanyByRenterId(resident.RenterId);

            if (_view.SelectPaymentTypeControl.PaymentTypeSelectedValue == "3")
            {
                _al.WriteLog(_view.ParentPage.Page.User.Identity.Name, "Admin initiated cash payment", (short)LogPriority.LogAlways, resident.RenterId.ToString(), true);

                var rentByCash = new RentByCash();
                string creator;
                
                if (int.TryParse(_view.PropertyList.SelectedValue, out var selectedProperty) &&
                    currentCompany != null && !string.IsNullOrEmpty(currentCompany.PNMSiteId) &&
                    !string.IsNullOrEmpty(currentCompany.PNMSecretKey))
                {
                    var pnmLimit = EfxSettings.PNMLimit.ToDecimal();
                    var amountToCharge = Math.Round(GetFeeAmountsForRenter(), 2);
                    var address = Address.GetAddressByRenterIdAndAddressType(_view.PayerList.SelectedValue.ToInt32(), (int)TypeOfAddress.Billing);

                    if (amountToCharge <= pnmLimit)
                    {
                        address.AddressTypeId = (int)TypeOfAddress.Billing;
                        address.AddressLine1 = _view.ResidentBillingInformationControl.BillingAddressText;
                        address.City = _view.ResidentBillingInformationControl.CityText;
                        address.StateProvinceId = _view.ResidentBillingInformationControl.StateDropDown.SelectedValue.ToInt32();
                        address.PostalCode = _view.ResidentBillingInformationControl.ZipText;
                        address.PrimaryEmailAddress = _view.PersonalInformationControl.EmailAddressText;
                        address.PrimaryPhoneNumber = PhoneNumber.RemoveFormatting(_view.PersonalInformationControl.PhoneNumberText);

                        int? efxAdminId = null;
                        int? propertyStaffId = null;
                        var currentAdmin = new EfxAdministrator();
                        var currentPs = new PropertyStaff(Helpers.Helper.CurrentUserId);

                        if (Helpers.Helper.IsRpoAdmin)
                        {
                            var baseAdmin = currentAdmin.GetUserById(Helpers.Helper.CurrentUserId);
                            creator = baseAdmin.FirstName + " " + baseAdmin.LastName;
                            efxAdminId = baseAdmin.UserId;
                        }
                        else
                        {
                            creator = currentPs.FirstName + " " + currentPs.LastName;
                            propertyStaffId = currentPs.PropertyStaffId;
                        }

                        var pnmRequest = new PayNearMeRequest
                        {
                            Amount = Math.Round(GetFeeAmountsForRenter(), 2),
                            CreatorIdentifier = creator,
                            Currency = "USD",
                            CustomerCity = address.City,
                            CustomerEmail = address.PrimaryEmailAddress,
                            CustomerIdentifier = resident.PublicRenterId.ToString(),
                            CustomerName = resident.FirstName + " " + resident.LastName,
                            CustomerPhone = address.PrimaryPhoneNumber,
                            CustomerPostalCode = address.PostalCode,
                            CustomerState = _view.ResidentBillingInformationControl.StateDropDown.SelectedItem.Text,
                            CustomerStreet = address.AddressLine1,
                            MinimumPaymentAmount = 1,
                            MinimumPaymentCurrency = "USD",
                            OrderDescription = "Administrative Take-A-Payment",
                            OrderDuration = 3,
                            OrderIdentifier = Guid.NewGuid().ToString(),
                            OrderType = PayNearMeRequest.PNMOrderType.Exact,
                            RenterId = resident.RenterId,
                            PropertyId = selectedProperty,
                            CompanyId = currentCompany.CompanyId,
                            EfxAdministratorId = efxAdminId,
                            PropertyStaffId = propertyStaffId,
                            SiteIdentifier = currentCompany.PNMSiteId,
                            SecretKey = currentCompany.PNMSecretKey,
                            Version = EfxSettings.PNMVersion
                        };

                        if (_view.SelectedPayerType == "0")
                        {
                            var autoPay = AutoPayment.GetAutoPaymentByRenterId(_view.PayerList.SelectedValue.ToInt32());
                            if (autoPay.AutoPaymentId < 1 || ((!autoPay.PayerAchId.HasValue || autoPay.PayerAchId.Value < 1) && (!autoPay.PayerCreditCardId.HasValue || autoPay.PayerCreditCardId.Value < 1)))
                            {
                                var addressId = Address.Set(address);
                                Address.AssignAddressToRenter(_view.PayerList.SelectedValue.ToInt32(), addressId);

                                var log = new ActivityLog();
                                log.WriteLog(_view.ParentPage.Page.User.Identity.Name, "Address has been updated (PNM)", 0, resident.RenterId.ToString(), true);
                            }
                        }

                        rentByCash.SandBoxMode = EfxSettings.PNMUseSandboxMode.ToBool();

                        var response = rentByCash.CreatePayNearMeRequest(pnmRequest, rentByCash.SandBoxMode);

                        var cs = _view.ParentPage.ClientScript;
                        if (response.ToLower().Equals("error"))
                        {
                            Logging.ExceptionLogger.LogException(new Exception("Received an error from PayNearMe"));
                            _al.WriteLog(_view.ParentPage.Page.User.Identity.Name,
                                "Cash Payment error received from PayNearMe", (short)LogPriority.LogAlways,
                                resident.RenterId.ToString(), true);
                        }
                        else
                        {
                            const string csName1 = "PNMPopupScript";
                            var csType = _view.ParentPage.GetType();

                            if (cs.IsStartupScriptRegistered(csType, csName1)) return;

                            var resp = response.Replace("PayNearMe status Ok;", "");
                            var csText = "window.open('" + resp + "','_blank');";

                            cs.RegisterStartupScript(csType, csName1, csText, true);
                        }
                    }
                    else
                    {
                        var pnmLimitError = decimal.Parse(EfxSettings.PNMLimit).ToString("C");

                        var err = new CustomValidator
                        {
                            ValidationGroup = "Payment",
                            IsValid = false,
                            ErrorMessage = @"You cannot create a RentByCash payment greater than " + pnmLimitError
                        };
                        _view.ParentPage.Validators.Add(err);
                        _al.WriteLog(_view.ParentPage.Page.User.Identity.Name, "Cash Payment error: " + err.ErrorMessage, (short)LogPriority.LogAlways, resident.RenterId.ToString(), true);
                    }
                }
                else
                {
                    var err = new CustomValidator
                    {
                        ValidationGroup = "Payment",
                        IsValid = false,
                        ErrorMessage =
                            @"You must select a Property and a Resident to use Rent By Cash.  If you have selected both, please have an RPO administrator verify the parent company is configured to use Rent by Cash."
                    };
                    _view.ParentPage.Validators.Add(err);
                    _al.WriteLog(_view.ParentPage.Page.User.Identity.Name, "Cash Payment error: " + err.ErrorMessage,
                        (short)LogPriority.LogAlways, resident.RenterId.ToString(), true);
                }
            }
            else
            {
                SaveBillingInfo();

                switch (_view.SelectedPayerType)
                {
                    case "0":
                        ProcessResidentPayment();
                        break;
                    case "1":
                        ProcessApplicantPayment();
                        break;
                }
            }
        }
        private void ProcessApplicantPayment()
        {
            var paymentInfo = BuildApplicantPaymentInfo();
            if (paymentInfo == null)
                return;

            var result = ApplicantPaymentRequest.ProcessPayment(paymentInfo);

            BasePage.SafePageRedirect(result.Result == GeneralResponseResult.Success
                ? $"~/PaymentStatus.aspx?TransactionId={result.TransactionId}&ApplicantId={_view.PayerList.SelectedValue}"
                : "~/PaymentFailed.aspx");
        }

        private void ProcessResidentPayment()
        {
            var resident = new Renter(_view.PayerList.SelectedValue.ToInt32());
            if (resident.RenterId < 1)
                return;

            _al.WriteLog(_view.ParentPage.Page.User.Identity.Name, "Admin initiated resident payment", (short)LogPriority.LogAlways, resident.RenterId.ToString(), true);

            var paymentInfo = BuildPaymentInfo(resident);

            if (_view.IsOverride)
            {
                var property = new Property(_view.PropertyList.SelectedValue.ToNullInt32());
                paymentInfo.OtherAmount2 = _view.PaymentAmountsControl.ConvenienceFeeAmount;
            }

            if (resident.AcceptedPaymentTypeId == 4)
            {
                var err = new CustomValidator
                {
                    ValidationGroup = "Payment",
                    IsValid = false,
                    ErrorMessage = @"Cannot accept a payment for the selected resident."
                };
                _view.ParentPage.Validators.Add(err);
                _view.PayNowButtonEnabled = false;
                _al.WriteLog(_view.ParentPage.Page.User.Identity.Name, "Error message displayed: " + err.ErrorMessage,
                    (short)LogPriority.LogAlways, resident.RenterId.ToString(), true);
                return;
            }

            if (paymentInfo.PaymentType == PaymentMethods.PaymentType.ECheck && resident.AcceptedPaymentTypeId == 3)
            {
                var err = new CustomValidator
                {
                    ValidationGroup = "Payment",
                    IsValid = false,
                    ErrorMessage = @"Can only accept Credit Card Payments from this resident."
                };
                _view.ParentPage.Validators.Add(err);
                _al.WriteLog(_view.ParentPage.Page.User.Identity.Name, "Error message displayed: " + err.ErrorMessage,
                    (short)LogPriority.LogAlways, resident.RenterId.ToString(), true);
                return;
            }

            if (paymentInfo.RentAmount < 0.01M && paymentInfo.WaivedAmount < 0.01M)
            {
                var err = new CustomValidator
                {
                    ValidationGroup = "Payment",
                    IsValid = false,
                    ErrorMessage = @"Cannot Make a payment for a Negative or Zero Amount."
                };
                _view.ParentPage.Validators.Add(err);
                _al.WriteLog(_view.ParentPage.Page.User.Identity.Name, "Error message displayed: " + err.ErrorMessage,
                    (short)LogPriority.LogAlways, resident.RenterId.ToString(), true);
                return;
            }

            var result = PaymentRequest.ProcessPayment(paymentInfo, _view.ApplyPaymentToNextMonth, false);
            var rentAndFeeFailureMessage = "";
            
            if (result.RentAndFeeResult == GeneralResponseResult.Success)
            {
                switch (paymentInfo.PaymentType)
                {
                    case PaymentMethods.PaymentType.CreditCard when _view.AddToAutoPay:
                        {
                            var payerCreditCardId = Renter.AddPayerCreditCard(0, paymentInfo.PayerId, paymentInfo.DisplayName,
                                paymentInfo.CreditCardNumber,
                                _view.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationMonthSelectedValue
                                    .ToInt32(),
                                _view.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationYearList.SelectedValue.ToInt32(), true, true);
                            Renter.InsertAutoPayment(paymentInfo.RenterId, paymentInfo.PayerId, 1, payerCreditCardId, _view.PaymentDay); //Change later PaymentProcessingDateComboBox.SelectedValue.ToInt32());
                            break;
                        }
                    case PaymentMethods.PaymentType.ECheck when _view.AddToAutoPay:
                        {
                            var payerAchId = Renter.AddPayerAch(0, paymentInfo.PayerId, paymentInfo.BankAccountNumber, paymentInfo.BankRoutingNumber, 1, true);
                            Renter.InsertAutoPayment(paymentInfo.RenterId, paymentInfo.PayerId, 2, payerAchId, _view.PaymentDay); //Change later PaymentProcessingDateComboBox.SelectedValue.ToInt32());
                            break;
                        }
                }
            }
            else
            {
                if (result.RentAndFeeResult != GeneralResponseResult.Success)
                {
                    rentAndFeeFailureMessage = result.RentAndFeeResponseMessage;
                }
                _al.WriteLog(_view.ParentPage.Page.User.Identity.Name,
                    "Payment failure. Redirecting user to failure page: " +
                    $"~/PaymentFailed.aspx?RentAndFeeFailureMessage={rentAndFeeFailureMessage}",
                    (short)LogPriority.LogAlways, resident.RenterId.ToString(), true);
                BasePage.SafePageRedirect($"~/PaymentFailed.aspx?RentAndFeeFailureMessage={rentAndFeeFailureMessage}");
            }

            BasePage.SafePageRedirect(result.RentAndFeeResult == GeneralResponseResult.Success ? $"~/PaymentStatus.aspx?TransactionId={result.RentAndFeeTransactionId}"
                : "~/PaymentFailed.aspx");
        }

        private void SaveBillingInfo()
        {
            if (_view.SelectedPayerType != "0") return;

            var autoPay = AutoPayment.GetAutoPaymentByRenterId(_view.PayerList.SelectedValue.ToInt32());
            if (autoPay.AutoPaymentId < 1 || ((!autoPay.PayerAchId.HasValue || autoPay.PayerAchId.Value < 1) &&
                                              (!autoPay.PayerCreditCardId.HasValue ||
                                               autoPay.PayerCreditCardId.Value < 1)))
                return;

            var address = Address.GetAddressByRenterIdAndAddressType(_view.PayerList.SelectedValue.ToInt32(), (int)TypeOfAddress.Billing);

            if (address.AddressTypeId == (int)TypeOfAddress.Billing &&
                address.AddressLine1.Trim() == _view.ResidentBillingInformationControl.BillingAddressText.Trim() &&
                address.City.Trim() == _view.ResidentBillingInformationControl.CityText.Trim() &&
                address.StateProvinceId ==
                _view.ResidentBillingInformationControl.StateDropDown.SelectedValue.ToInt32() &&
                address.PostalCode.Trim() == _view.ResidentBillingInformationControl.ZipText.Trim() &&
                address.PrimaryEmailAddress.Trim() == _view.PersonalInformationControl.EmailAddressText.Trim() &&
                address.PrimaryPhoneNumber.Trim() == PhoneNumber
                    .RemoveFormatting(_view.PersonalInformationControl.PhoneNumberText).Trim()) return;
            address.AddressTypeId = (int)TypeOfAddress.Billing;
            address.AddressLine1 = _view.ResidentBillingInformationControl.BillingAddressText;
            address.City = _view.ResidentBillingInformationControl.CityText;
            address.StateProvinceId = _view.ResidentBillingInformationControl.StateDropDown.SelectedValue.ToInt32();
            address.PostalCode = _view.ResidentBillingInformationControl.ZipText;
            address.PrimaryEmailAddress = _view.PersonalInformationControl.EmailAddressText;
            address.PrimaryPhoneNumber = PhoneNumber.RemoveFormatting(_view.PersonalInformationControl.PhoneNumberText);

            var addressId = Address.Set(address);
            Address.AssignAddressToRenter(_view.PayerList.SelectedValue.ToInt32(), addressId);

            var log = new ActivityLog();
            log.WriteLog(_view.ParentPage.Page.User.Identity.Name, "Address has been updated", 0,
                _view.PayerList.SelectedValue.ToInt32().ToString(), true);
        }

        public void PropertyHasBeenSelected()
        {
            _view.RedirectApplicantLink.NavigateUrl =
                $"~/MyProperties/PropertyDetails.aspx?propertyId={_view.PropertyList.SelectedValue.ToInt32()}#ApplicationTab";
            _view.RedirectApplicantLink.Enabled = true;
            var propertyIsNotIntegrated = false;
            if (CurrentProperty != null)
            {
                _view.ApplyToNextMonthCheckBox.Enabled = string.IsNullOrEmpty(CurrentProperty.PmsId);
                propertyIsNotIntegrated = string.IsNullOrEmpty(CurrentProperty.PmsId);
                if (CurrentProperty.PropertyId != null)
                    _view.SelectPaymentTypeControl.PropertyId = CurrentProperty.PropertyId.Value;
            }

            var monthlyFeeCheckbox = (CheckBox)_view.PaymentAmountsControl.MonthlyFeesList.FindControl("MonthlyFeeWaiveCheckBox");
            if (monthlyFeeCheckbox != null)
            {
                monthlyFeeCheckbox.Enabled = true;
                monthlyFeeCheckbox.Visible = true;
            }
            if (propertyIsNotIntegrated)
            {
                _view.ApplyToNextMonthCheckBox.Enabled = true;
                if (Helpers.Helper.IsRpoAdmin)
                {
                    _view.PaymentAmountsControl.WaiveRent.Enabled = true;
                    _view.PaymentAmountsControl.WaiveRent.Visible = true;
                    _view.PaymentAmountsControl.WaivePastDue.Enabled = true;
                    _view.PaymentAmountsControl.WaivePastDue.Visible = true;
                }
                else
                {
                    _view.PaymentAmountsControl.WaiveRent.Enabled = false;
                    _view.PaymentAmountsControl.WaiveRent.Visible = false;
                    _view.PaymentAmountsControl.WaivePastDue.Enabled = false;
                    _view.PaymentAmountsControl.WaivePastDue.Visible = false;
                }
            }
            else
            {
                _view.PaymentAmountsControl.WaiveRent.Enabled = false;
                _view.PaymentAmountsControl.WaiveRent.Visible = false;
                _view.PaymentAmountsControl.WaivePastDue.Enabled = false;
                _view.PaymentAmountsControl.WaivePastDue.Visible = false;
            }

            if (CurrentProperty != null)
                EnablePartialPayments =
                    Helpers.Helper.IsRpoAdmin ? true : CurrentProperty?.AllowPartialPayments ?? false;

            if (Helpers.Helper.IsCsrUser && !Helpers.Helper.IsRpoAdmin)
            {
                EnablePartialPayments = false;
            }

            _view.PaymentAmountsControl.OverrideText.Enabled = EnablePartialPayments;

            ChangePayerType();
        }

        private void PropertyListSelectedIndexChanged(object sender, EventArgs e)
        {
            PropertyHasBeenSelected();
        }

        private void PaymentTypeChanged(object sender, EventArgs e)
        {
            if (_view.SelectedPayerType == "0")
                SetRentAmounts();
        }

        private void PayerTypeChanged(object sender, EventArgs e)
        {
            ChangePayerType();
        }

        private void UpdateTotalClicked(object sender, EventArgs e)
        {
            var amount = _view.SelectedPayerType == "0" ? GetFeeAmountsForRenter() : GetFeesForApplicant().Sum(f => f.FeesPaid);
            if (_view.SelectedPayerType == "0")
            {
                var resident = new Renter(_view.PayerList.SelectedValue.ToNullInt32());
                if (resident.RenterId < 1) return;

                var lease = Lease.GetRenterLeaseByRenterId(resident.RenterId);
                if (lease.LeaseId < 1) return;

                if (!_view.IsOverride && !lease.CurrentBalanceDue.HasValue) return;

                if (_view.IsOverride)
                {
                    var property = new Property(_view.PropertyList.SelectedValue.ToNullInt32());
                    _view.PaymentAmountsControl.ConvenienceFeeText = "$" + Math.Round(GetConvenienceFee(_view.SelectPaymentTypeControl.PaymentTypeSelectedValue, amount), 2);

                    if (!_view.PaymentAmountsControl.WaiveConvenienceFee.Checked)
                    {
                        _view.PaymentAmountsControl.ConvenienceFeeAmount = GetConvenienceFee(_view.SelectPaymentTypeControl.PaymentTypeSelectedValue, amount);
                    }
                    else
                    {
                        _view.PaymentAmountsControl.ConvenienceFeeAmount = 0.00M;
                        _view.PaymentAmountsControl.WaiveConvenienceFee.Checked = true;
                    }
                }
            }
            else
            {
                var applicant = new Applicant(_view.PayerList.SelectedValue.ToNullInt32());
                if (applicant.ApplicantId < 1) return;
            }

            if (Helpers.Helper.IsRpoAdmin)
            {
                switch (_view.SelectedPayerType)
                {
                    case "0" when !_view.PaymentAmountsControl.WaiveConvenienceFee.Checked && amount > 0.00M:
                        {
                            if (!string.IsNullOrEmpty(_view.PaymentAmountsControl.ConvenienceFeeText) && _view.SelectPaymentTypeControl.PaymentTypeSelectedValue != "3")
                            {
                                //CMallory
                                amount += GetConvenienceFee(_view.SelectPaymentTypeControl.PaymentTypeSelectedValue, amount); //_View.PaymentAmountsControl.ConvenienceFeeText.Replace("$", "").ToDecimal();
                            }
                            else
                            {
                                amount += 0.00M;
                                _view.PaymentAmountsControl.WaiveConvenienceFee.Checked = true;
                            }

                            break;
                        }
                    case "1" when _view.PaymentAmountsControl.AppWaiveConvenienceFee.Checked && amount > 0.00M:
                        amount += 0.00M;
                        _view.PaymentAmountsControl.AppConvenienceFeeAmount = 0.00M;
                        _view.PaymentAmountsControl.AppConvenienceFeeText = "$0.00";
                        _view.PaymentAmountsControl.AppWaiveConvenienceFee.Checked = true;
                        break;
                    default:
                        amount += 0.00M;
                        _view.PaymentAmountsControl.ConvenienceFeeAmount = 0.00M;
                        _view.PaymentAmountsControl.ConvenienceFeeText = "$0.00";
                        _view.PaymentAmountsControl.WaiveConvenienceFee.Checked = true;
                        break;
                }
            } // not admin
            else
            {
                if (_view.SelectedPayerType == "0" && amount > 0.00M)
                {
                    if (!string.IsNullOrEmpty(_view.PaymentAmountsControl.ConvenienceFeeText) && _view.SelectPaymentTypeControl.PaymentTypeSelectedValue != "3")
                    {
                        amount += GetConvenienceFee(_view.SelectPaymentTypeControl.PaymentTypeSelectedValue, amount); 
                    }
                    else
                    {
                        amount += 0.00M;
                        _view.PaymentAmountsControl.WaiveConvenienceFee.Checked = true;
                    }
                }
            }

            if (_view.SelectPaymentTypeControl.PaymentTypeSelectedValue == "3")
            {
                _view.PaymentAmountsControl.WaiveConvenienceFee.Checked = true;
            }

            if (amount > 0.00M || GetWaivedAmount() > 0.00M && _view.IsOverride)
            {
                _view.PaymentAmountsControl.TotalAmountText = amount.ToString("C");
                _view.PayNowButtonEnabled = true;
            }
            else
            {
                _view.PaymentAmountsControl.TotalAmountText = "$0.00";
                _view.PayNowButtonEnabled = false;
            }
        }

        protected virtual void TurnOn()
        {
            _view.AutoPayTileControl.IsOn = true;
            _view.AutoPayTileControl.OnButtonCssClass = OnClass;
            _view.AutoPayTileControl.OffButtonCssClass = OffClass;
        }

        protected virtual void TurnOff()
        {
            _view.AutoPayTileControl.IsOn = false;
            _view.AutoPayTileControl.OnButtonCssClass = OffClass;
            _view.AutoPayTileControl.OffButtonCssClass = OnClass;
        }

        private void ChangePayerType()
        {
            ClearFields();

            var payerType = _view.SelectedPayerType.ToInt32();
            var propertyId = _view.PropertyList.SelectedValue.ToInt32();

            if (payerType < 0 || payerType > 1 || propertyId < 1) return;

            if (payerType == 0)
            {
                _view.PayerLabelText = "Select a Resident";
                _view.PayerList.DataTextField = "DisplayName";
                _view.PayerList.DataValueField = "RenterId";
                _view.PayerList.DataSource = Renter.GetActiveRenterListByPropertyId(propertyId).OrderBy(r => r.LastName)
                    .ThenBy(r => r.FirstName).ToList();
                _view.PayerList.DataBind();
                _view.PaymentAmountsControl.PaymentAmountsView.SetActiveView(_view.PaymentAmountsControl.RentAmountsView);
                SetRentAmounts();
                if (!_view.IsPropertyManagerSite)
                {
                    _view.PaymentAmountsControl.OverrideLabel.Visible = EnablePartialPayments;
                    _view.PaymentAmountsControl.OverrideText.Enabled = EnablePartialPayments;
                    _view.PaymentAmountsControl.OverrideText.Visible = EnablePartialPayments;
                }
            }
            else
            {
                _view.PayerLabelText = "Select an Applicant";
                _view.PayerList.DataTextField = "DisplayName";
                _view.PayerList.DataValueField = "ApplicantId";
                _view.PayerList.DataSource = Applicant.GetAllApplicantsByPropertyId(propertyId).OrderBy(a => a.LastName)
                    .ThenBy(a => a.FirstName).ToList();
                _view.PayerList.DataBind();
                _view.PaymentAmountsControl.PaymentAmountsView.SetActiveView(_view.PaymentAmountsControl.ApplicationAmountsView);
                SetApplicationAmounts();
                if (!_view.IsPropertyManagerSite)
                {
                    _view.PaymentAmountsControl.OverrideLabel.Visible = false;
                    _view.PaymentAmountsControl.OverrideText.Visible = false;
                }
            }

            _view.PayerList.DataBind();
        }

        private void SetApplicationAmounts()
        {
            _view.PaymentAmountsControl.ApplicationFeesList.DataSource = ApplicationFee.GetAllApplicationFeesByPropertyId(_view.PropertyList.SelectedValue.ToInt32());
            _view.PaymentAmountsControl.ApplicationFeesList.DataBind();

            if (!_view.PaymentAmountsControl.AppWaiveConvenienceFee.Checked)
            {
                _view.PaymentAmountsControl.AppConvenienceFeeAmount = 0.00M;
            }

            _view.PaymentAmountsControl.AppConvenienceFeeText = "$" + _view.PaymentAmountsControl.AppConvenienceFeeAmount;
            _view.PaymentAmountsControl.TotalAmountText = (GetFeesForApplicant().Sum(f => f.FeesPaid) + _view.PaymentAmountsControl.AppConvenienceFeeAmount).ToString("C");
        }
        private void SetRentAmounts()
        {
            var resident = new Renter(_view.PayerList.SelectedValue.ToNullInt32());
            if (resident.RenterId < 1) return;

            var lease = Lease.GetRenterLeaseByRenterId(resident.RenterId);
            if (lease.LeaseId < 1)
            {
                SetValuesZero();
                _view.PayNowButtonEnabled = false;
                return;
            }

            var property = new Property(_view.PropertyList.SelectedValue.ToNullInt32());
            if (!property.PropertyId.HasValue || property.PropertyId.Value < 1)
                return;

            var rentAmount = lease.RentAmount;
            var currentBalanceDue = lease.CurrentBalanceDue;
            var totalFees = LeaseFee.GetFeesByLeaseId(lease.LeaseId).Sum(lf => lf.FeeAmount).Value;

            RPO.Helpers.Helper.IsIntegrated(property.PmsId);

            if (currentBalanceDue == null) return;

            var total = currentBalanceDue.Value;
            decimal convenienceFee;

            if (!_view.PaymentAmountsControl.WaiveConvenienceFee.Checked)
            {
                convenienceFee = GetConvenienceFee(_view.SelectPaymentTypeControl.PaymentTypeSelectedValue, total);
            }
            else
            {
                convenienceFee = 0.00M;
                _view.PaymentAmountsControl.WaiveConvenienceFee.Checked = true;
            }

            _view.PaymentAmountsControl.RentAmountText = lease.RentAmount.ToString("C");
            _view.PaymentAmountsControl.MonthlyFeesList.DataSource = LeaseFee.GetFeesByLeaseId(lease.LeaseId);
            _view.PaymentAmountsControl.MonthlyFeesList.DataBind();
            _view.PaymentAmountsControl.ConvenienceFeeText = convenienceFee.ToString("C");
            _view.PaymentAmountsControl.RentAmount = total;
            _view.PaymentAmountsControl.ConvenienceFeeAmount = convenienceFee;

            if (!lease.CurrentBalanceDue.HasValue || lease.CurrentBalanceDue.Value + lease.TotalFees < 0.01M)
            {
                SetValuesZero();
                _view.PayNowButtonEnabled = false;
                return;
            }
            _view.PaymentAmountsControl.PastDueHeaderText = "";
            _view.PaymentAmountsControl.PastDueAmountText = "";
            _view.PaymentAmountsControl.PastDueVisible = false;
            _view.PaymentAmountsControl.WaivePastDue.Visible = false;

            if (total != lease.RentAmount + lease.TotalFees)
            {
                if (total < 0.01M)
                {
                    SetValuesZero();
                    _view.PayNowButtonEnabled = false;
                    return;
                }

                decimal difference;

                if (lease.CurrentBalanceDue.Value != 0.00M)
                {
                    difference = currentBalanceDue.Value - (rentAmount + totalFees);
                }
                else
                {
                    difference = lease.CurrentBalanceDue.Value;
                }

                if (difference > 0.00M)
                {
                    _view.PaymentAmountsControl.PastDueVisible = true;
                    _view.PaymentAmountsControl.PastDueHeaderText = "Past Due Amount:";
                    _view.PaymentAmountsControl.PastDueAmountText = difference.ToString("C");
                    _view.PaymentAmountsControl.WaivePastDue.Visible = true;
                }
                else if (difference < 0.00M)
                {
                    _view.PaymentAmountsControl.PastDueVisible = true;
                    _view.PaymentAmountsControl.PastDueHeaderText = "Credit Amount:";
                    _view.PaymentAmountsControl.PastDueAmountText = difference.ToString("C");
                    _view.PaymentAmountsControl.WaivePastDue.Visible = false;
                }
                else
                    _view.PaymentAmountsControl.PastDueVisible = false;

                total = lease.CurrentBalanceDue.Value;
                _view.PaymentAmountsControl.RentAmount = total;
            }

            if (_view.SelectPaymentTypeControl.PaymentTypeSelectedValue != "3")
            {
                total += convenienceFee;
            }

            _view.PaymentAmountsControl.TotalAmountText = total.ToString("C");
        }

        private void SetValuesZero()
        {
            _view.PaymentAmountsControl.PastDueVisible = false;
            _view.PaymentAmountsControl.MonthlyFeesList.DataSource = null;
            _view.PaymentAmountsControl.MonthlyFeesList.DataBind();
            _view.PaymentAmountsControl.RentAmountText = "$0.00";
            _view.PaymentAmountsControl.ConvenienceFeeText = "$0.00";
            _view.PaymentAmountsControl.TotalAmountText = "$0.00";
            _view.PaymentAmountsControl.RentAmount = 0.00M;
            _view.PaymentAmountsControl.ConvenienceFeeAmount = 0.00M;
            _view.PaymentAmountsControl.MonthlyFeesList.DataSource = new List<LeaseFee>();
            _view.PaymentAmountsControl.MonthlyFeesList.DataBind();
        }

        private RenterPaymentInfo BuildPaymentInfo(Renter resident)
        {
            var payer = Payer.GetPayerByRenterId(resident.RenterId);
            var cardNumber = _view.SelectPaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText;
            var accountNumber = _view.SelectPaymentTypeControl.ECheckDetailsControl.AccountNumberText;
            var creditCard = PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(payer.PayerId);
            var echeck = PayerAch.GetPrimaryPayerAchByPayerId(payer.PayerId);

            if (cardNumber.Contains("*"))
                cardNumber = creditCard.CreditCardAccountNumber;
            if (accountNumber.Contains("*"))
                accountNumber = echeck.BankAccountNumber;

            return new RenterPaymentInfo
            {
                RenterId = resident.RenterId,
                PaymentType = (PaymentMethods.PaymentType)_view.SelectPaymentTypeControl.PaymentTypeSelectedValue.ToInt32(),
                BankAccountNumber = accountNumber,
                BankRoutingNumber = _view.SelectPaymentTypeControl.ECheckDetailsControl.RoutingNumberText,
                CreditCardHolderName = _view.SelectPaymentTypeControl.CreditCardDetailsControl.NameOnCardText,
                CreditCardNumber = cardNumber,
                Cvv = _view.SelectPaymentTypeControl.CreditCardDetailsControl.CvvText,
                ExpirationDate = new DateTime(_view.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationYearList.SelectedValue.ToInt32(), _view.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationMonthSelectedValue.ToInt32(), 1),
                RentAmount = GetFeeAmountsForRenter(),
                RentAmountDescription = "Rent and Fees",
                OtherAmount2 = !_view.PaymentAmountsControl.WaiveConvenienceFee.Checked ? _view.PaymentAmountsControl.ConvenienceFeeAmount : 0.00M,
                OtherAmountDescription2 = !_view.PaymentAmountsControl.WaiveConvenienceFee.Checked ? "Convenience Fee" : null,
                DisplayName =
                    $"{_view.PersonalInformationControl.FirstNameText} {_view.PersonalInformationControl.LastNameText}",
                PayerId = payer.PayerId,
                PropertyId = _view.PropertyList.SelectedValue.ToInt32(),
                WaivedAmount = GetWaivedAmount(),
                WaiveConvenienceFee = _view.PaymentAmountsControl.WaiveConvenienceFee.Checked
            };
        }

        private decimal GetFeeAmountsForRenter()
        {
            decimal amount;

            if (!_view.IsOverride)
            {
                SetRentAmounts();
                amount = _view.PaymentAmountsControl.RentAmount;
            }
            else
            {
                amount = _view.PaymentAmountsControl.OverrideText.Text.Replace("$", "").ToDecimal();

                if (amount < 0.01M)
                    amount = 0.00M;
            }

            return amount;
        }

        private decimal GetWaivedAmount()
        {
            var amount = 0.00M;

            if (_view.SelectedPayerType != "0") return amount;

            amount = (from item in _view.PaymentAmountsControl.MonthlyFeesList.Items
                      let waive = item.FindControl("MonthlyFeeWaiveCheckBox") as CheckBox
                      where waive != null && waive.Checked
                      select item.FindControl("MonthlyFeeIdField") as HiddenField
                into feeIdControl
                      select feeIdControl != null && feeIdControl.Value.ToInt32() > 0 ? feeIdControl.Value.ToInt32() : 0
                into feeId
                      where feeId > 0
                      select LeaseFee.GetFeesByRenterId(_view.PayerList.SelectedValue.ToInt32())
                          .FirstOrDefault(lf => lf.MonthlyFeeId == feeId)
                into fee
                      select fee?.FeeAmount ?? 0.00M).Sum();

            if (_view.PaymentAmountsControl.WaiveRent.Checked)
                amount += !string.IsNullOrEmpty(_view.PaymentAmountsControl.RentAmountText)
                    ? _view.PaymentAmountsControl.RentAmountText.Replace("$", "").ToDecimal()
                    : 0.00M;
            if (_view.PaymentAmountsControl.WaivePastDue.Checked)
                amount += !string.IsNullOrEmpty(_view.PaymentAmountsControl.PastDueAmountText)
                    ? _view.PaymentAmountsControl.PastDueAmountText.Replace("$", "").ToDecimal()
                    : 0.00M;

            return amount;
        }

        private ApplicantPaymentInfo BuildApplicantPaymentInfo()
        {
            var application = ApplicantApplication
                .GetAllApplicantApplicationsByApplicantId(_view.PayerList.SelectedValue.ToInt32())
                .FirstOrDefault(a => a.PropertyId == _view.PropertyList.SelectedValue.ToInt32());
            var err = new CustomValidator();
            if (application == null || application.ApplicantApplicationId < 1)
            {
                err.ValidationGroup = "Payment";
                err.IsValid = false;
                err.ErrorMessage = @"Unable to Process Payment.  No Application in the System for this Applicant and Property.";
                _view.ParentPage.Validators.Add(err);
                //BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Unable to Process Payment.  No Application in the System for this Applicant and Property");
                return null;
            }

            if (application.FeesPaid)
            {
                err.ValidationGroup = "Payment";
                err.IsValid = false;
                err.ErrorMessage = @"Records indicate that the Applicant has already paid the Application Fees for this Property.";
                _view.ParentPage.Validators.Add(err);
                return null;
            }

            _view.PaymentAmountsControl.AppConvenienceFeeAmount = 0.00M;

            return new ApplicantPaymentInfo
            {
                Fees = GetFeesForApplicant(),
                TypeOfPayment = ((PaymentMethods.PaymentType)_view.SelectPaymentTypeControl.PaymentTypeSelectedValue.ToInt32()),
                CreditCardInfo = _view.SelectPaymentTypeControl.PaymentTypeSelectedValue == "1" ? GetCreditCardDetails() : null,
                EcheckInfo = _view.SelectPaymentTypeControl.PaymentTypeSelectedValue == "2" ? GetEcheckDetails() : null,
                PropertyId = _view.PropertyList.SelectedValue.ToInt32(),
                ApplicantId = _view.PayerList.SelectedValue.ToInt32(),
                ApplicantApplicationId = application.ApplicantApplicationId,
                IsTestUser = false,
                ConvenienceFeeAmount = 0.00M
            };
        }

        private CreditCardDetails GetCreditCardDetails()
        {
            return new CreditCardDetails
            {
                CardHolderName = _view.SelectPaymentTypeControl.CreditCardDetailsControl.NameOnCardText,
                CreditCardNumber = _view.SelectPaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText,
                ExpirationDate = new DateTime(_view.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationYearList.SelectedValue.ToInt32(),
                        _view.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationMonthSelectedValue.ToInt32(), 1),
                CvvCode = _view.SelectPaymentTypeControl.CreditCardDetailsControl.CvvText,
                BillingAddress = new Address
                {
                    AddressLine1 = _view.ResidentBillingInformationControl.BillingAddressText,
                    City = _view.ResidentBillingInformationControl.CityText,
                    StateProvinceId = _view.ResidentBillingInformationControl.StateDropDown.SelectedValue.ToInt32(),
                    PostalCode = _view.ResidentBillingInformationControl.ZipText
                },
                BillingFirstName = _view.PersonalInformationControl.FirstNameText,
                BillingLastName = _view.PersonalInformationControl.LastNameText
            };
        }

        private EcheckDetails GetEcheckDetails()
        {
            return new EcheckDetails
            {
                BankAccountNumber = _view.SelectPaymentTypeControl.ECheckDetailsControl.AccountNumberText,
                BankRoutingNumber = _view.SelectPaymentTypeControl.ECheckDetailsControl.RoutingNumberText,
                TypeOfAccount = AccountType.Checking,
                BillingAddress = new Address
                {
                    AddressLine1 = _view.ResidentBillingInformationControl.BillingAddressText,
                    City = _view.ResidentBillingInformationControl.CityText,
                    StateProvinceId = _view.ResidentBillingInformationControl.StateDropDown.SelectedValue.ToInt32(),
                    PostalCode = _view.ResidentBillingInformationControl.ZipText
                },
                BillingFirstName = _view.PersonalInformationControl.FirstNameText,
                BillingLastName = _view.PersonalInformationControl.LastNameText
            };
        }

        private List<ApplicationFee> GetFeesForApplicant()
        {
            return (from item in _view.PaymentAmountsControl.ApplicationFeesList.Items
                    let waived = item.FindControl("ApplicationFeeWaiveCheckBox") as CheckBox
                    where waived != null && !waived.Checked
                    select item.FindControl("ApplicationFeeIdField") as HiddenField
                into feeIdField
                    where feeIdField != null && feeIdField.Value.ToInt32() > 0
                    select new ApplicationFee(feeIdField.Value.ToInt32())
                into fee
                    where fee.ApplicationFeeId > 0
                    select fee).ToList();
        }

        public decimal GetConvenienceFee(string selectedPaymentType, decimal amount)
        {
            var property = new Property(_view.PropertyList.SelectedValue.ToNullInt32());
            if (!property.PropertyId.HasValue || property.PropertyId.Value < 1) return 0.0M;

            string paymentType;
            switch (selectedPaymentType)
            {
                case "1":
                    paymentType = "CC";
                    break;
                case "2":
                    paymentType = "CK";
                    break;
                default:
                    paymentType = "RBC";
                    break;
            }

            var programId = 0;
            if (property.ProgramId != null)
            {
                programId = (int)property.ProgramId.Value;
            }

            return PaymentAmount.GetConvFeeByProgramID(programId, paymentType, amount);
        }
    }
}