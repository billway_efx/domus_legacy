﻿using EfxFramework.Interfaces.Account;
using EfxFramework.Resources;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace EfxFramework.Presenters.Account
{
    public class ForgotPassword<T> where T : BaseUser, new()
    {
        private readonly IForgotPassword _View;

        public ForgotPassword(IForgotPassword view)
        {
            _View = view;
            _View.SubmitButtonClick = ResetPassword;
        }

        private void ResetPassword(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_View.EmailAddressText))
            {
                var User = new T().GetUserByEmailAddress(_View.EmailAddressText);

                //Salcedo - 6/10/2014 - Task # 00000120 - if this user isn't an EFX Admin, see if they are instead a Property Staff admin user (Property Manager, Leasing Agent, or Corporate Admin)
                if (!User.IsValid)
                {
                    User = new PropertyStaff().GetUserByEmailAddress(_View.EmailAddressText); 
                }

                if (User.IsValid)
                {
                    var Password = User.ResetPassword();
                    User.SetUser(User, "Password Reset Tool");

                    //Salcedo - 3/12/2014 - replaced with new html email template
                    //var Message = string.Format(EmailTemplates.PasswordResetTool, User.DisplayName, Password);
                    var Message = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-password-reset.html").Replace("[[ROOT]]",
                        EmailTemplateService.EmailTemplateService.RootPath), User.DisplayName, Password);

                    BulkMail.BulkMail.SendMailMessage
                        (
                            null, 
                            "UserAccount", 
                            false, 
                            EfxSettings.SendMailUsername, 
                            EfxSettings.SendMailPassword, 
                            new MailAddress(EfxSettings.SupportEmail), 
                            "Your new password", 
                            Message, 
                            new List<MailAddress> {new MailAddress(User.PrimaryEmailAddress)}
                        );

						_View.SuccessMessagePlaceHolder.Visible = true;
                    //_View.PageMessage.DisplayMessage(SystemMessageType.Success, "An email has been sent with your new temporary password.");
                }
                else
                {
					CustomValidator err = new CustomValidator();
					err.ValidationGroup = "Password";
					err.IsValid = false;
					err.ErrorMessage = "User account not found.";
					_View.ParentPage.Validators.Add(err);
                    //_View.PageMessage.DisplayMessage(SystemMessageType.Error, "User account not found");
                }
            }
            else
            {
				CustomValidator err = new CustomValidator();
				err.ValidationGroup = "Password";
				err.IsValid = false;
				err.ErrorMessage = "Please enter a valid email address.";
				_View.ParentPage.Validators.Add(err);
				//_View.PageMessage.DisplayMessage(SystemMessageType.Error, "Please enter a valid email address");
            }
        }
    }
}
