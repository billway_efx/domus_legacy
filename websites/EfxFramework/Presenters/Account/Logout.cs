﻿using EfxFramework.Interfaces.Account;
using System;
using System.Web;
using System.Web.Security;

namespace EfxFramework.Presenters.Account
{
    public class Logout
    {
        private readonly ILogout _View;

        public Logout(ILogout view)
        {
            _View = view;
            ProcessLogout();
        }

        private void ProcessLogout()
        {
            if (HttpContext.Current.Request.Cookies[EfxSettings.FirstTimeCookie] != null)
            {
                var Cookie = new HttpCookie(EfxSettings.FirstTimeCookie) {Expires = DateTime.Now.AddDays(-1)};
                HttpContext.Current.Response.Cookies.Add(Cookie);
            }
			HttpContext.Current.Session.Clear();
			HttpContext.Current.Session.Abandon();
			HttpContext.Current.User = null;
            FormsAuthentication.SignOut();
            _View.SafeRedirect("~/Account/Login.aspx");
        }
    }
}
