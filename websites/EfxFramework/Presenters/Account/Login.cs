﻿using EfxFramework.Interfaces.Account;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;

namespace EfxFramework.Presenters.Account
{
    public class Login<T> where T : BaseUser, new()
    {
        private readonly ILogin _view;

        public Login(ILogin view)
        {
            _view = view;
            _view.LoginButtonClick = ProcessLogin;
        }

        private void ProcessLogin(object sender, EventArgs e)
        {
            var userIdIsFromEfxAdministratorTable = false;
            if (!string.IsNullOrEmpty(_view.UsernameText) && !string.IsNullOrEmpty(_view.PasswordText))
            {
                var user = new EfxAdministrator().GetUserByEmailAddress(_view.UsernameText);
                var userExists = user.IsValid;
                if (!userExists)
                {
                    user = new PropertyStaff().GetUserByEmailAddress(_view.UsernameText);
                    userExists = user.IsValid;
                }
                else
                {
                    userIdIsFromEfxAdministratorTable = true;
                }

                var err = new CustomValidator();
                if (userExists && user.IsValid && user.Authenticate(_view.PasswordText))
                {
                    var userInCsrRole = new EfxAdministratorRole(user.UserId.Value, 7);
                    if (userInCsrRole.EfxAdministratorId > 0)
                    {
                        // This user has the CSR role (RoleId = 7). Let's see if they also have Power Administrator (RoleId = 5)
                        var userInAdminRole = new EfxAdministratorRole(user.UserId.Value, 5);

                        // If the user doesn't have the Power Administrator role, then we need to apply the AllowCSRLogin setting logic to the user
                        err = new CustomValidator();
                        if (userInAdminRole.EfxAdministratorId == 0 && !EfxSettings.AllowCSRLogin && userInCsrRole.EfxAdministratorId == 0)
                        { 
                            err.ValidationGroup = "Login";
                            err.IsValid = false;
                            err.ErrorMessage = @"Username/password is invalid, please try again.";  // I changed the wording slightly to help with troubleshooting - it makes this error message unique
                            _view.ParentPage.Validators.Add(err);
                            return;
                        }
                    }

                    if (string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["ReturnUrl"]))
                    {
                        // ReSharper disable PossibleInvalidOperationException
                        FormsAuthentication.SetAuthCookie(user.UserId.Value.ToString(CultureInfo.InvariantCulture), true);
                        SetUserRole(user.UserId.Value, userIdIsFromEfxAdministratorTable, userInCsrRole.EfxAdministratorId);
                        // ReSharper restore PossibleInvalidOperationException
                        _view.SafeRedirect(EfxSettings.HomeLinkFirstTime);
                    }
                    else
                    {
                        var returnUrl = !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["ReturnUrl"]) ? HttpContext.Current.Request.QueryString["ReturnUrl"] : "~/";
                        if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["r"]) && returnUrl.Contains("export.ashx"))
                            returnUrl = "~/rpo/reports/" + HttpContext.Current.Request.QueryString["r"];
                        FormsAuthentication.SetAuthCookie(user.UserId.Value.ToString(CultureInfo.InvariantCulture), true);
                        SetUserRole(user.UserId.Value, userIdIsFromEfxAdministratorTable, userInCsrRole.EfxAdministratorId);
                        HttpContext.Current.Response.Redirect(returnUrl);
                    }
                }
                else
                {
                    err.ValidationGroup = "Login";
                    err.IsValid = false;
                    err.ErrorMessage = @"Invalid username/password, please try again.";
                    _view.ParentPage.Validators.Add(err);
                }
            }
            else
            {
                var err = new CustomValidator
                {
                    ValidationGroup = "Login",
                    IsValid = false,
                    ErrorMessage = @"Username or password missing, please try again."
                };
                _view.ParentPage.Validators.Add(err);
            }
        }

        private static void SetUserRole(int userId, bool userIdIsFromEfxAdministratorTable, int efxCustomerServiceId) 
        {
            if (userIdIsFromEfxAdministratorTable && efxCustomerServiceId<=0)
            {
                var roleListAdmin = new List<RoleName> { RoleName.EfxPowerAdministrator };
                var hasAdminPermission = RoleManager.IsUserInRole(EfxFramework.RoleType.EfxAdministrator, userId, roleListAdmin);
                HttpContext.Current.Session["IsRpoAdmin"] = hasAdminPermission;
            }

            else if(userIdIsFromEfxAdministratorTable && efxCustomerServiceId>0)
            {
                var roleListAdmin = new List<RoleName>
                {
                    RoleName.EfxCustomerService
                };
                HttpContext.Current.Session["IsRPOCSR"] = true;
            }
            else
            {
                HttpContext.Current.Session["IsRpoAdmin"] = false;
                HttpContext.Current.Session["IsRPOCSR"] = false;
            }
        }
    }
}