﻿using System;
using EfxFramework.Interfaces.Account;
using System.Web;
using System.Web.Security;

namespace EfxFramework.Presenters.Account
{
    public class LogoutV2
    {
        private readonly ILogoutV2 _View;

        public LogoutV2(ILogoutV2 view)
        {
            _View = view;
            ProcessLogout();
        }

        private void ProcessLogout()
        {
            if (HttpContext.Current.Request.Cookies[EfxSettings.FirstTimeCookie] != null)
            {
                var Cookie = new HttpCookie(EfxSettings.FirstTimeCookie) {Expires = DateTime.Now.AddDays(-1)};
                HttpContext.Current.Response.Cookies.Add(Cookie);
            }

            FormsAuthentication.SignOut();
            _View.SafeRedirect("~/Account/Login.aspx");
        }
    }
}
