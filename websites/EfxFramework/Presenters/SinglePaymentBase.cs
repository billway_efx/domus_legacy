﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Mail;
using System.Text;
using System.Web;
using EfxFramework.ExtensionMethods;
using EfxFramework.Interfaces;
using EfxFramework.PaymentMethods;
using EfxFramework.PaymentMethods.Ach;
using EfxFramework.Resources;
using EfxFramework.WebPayments;
using Mb2x.ExtensionMethods;
using System.Linq;

namespace EfxFramework.Presenters
{
    public abstract class SinglePaymentBase<T> where T : BaseUser, new()
    {
        protected readonly ISinglePayment BaseView;

        protected BaseUser CurrentUser
        {
            get
            {
                int UserId;
                BaseUser User = new T();

                if (Int32.TryParse(HttpContext.Current.User.Identity.Name, out UserId))
                    User = User.GetUserById(UserId);

                return User;
            }
        }

        protected SinglePaymentBase(ISinglePayment view)
        {
            BaseView = view;
            BaseView.StepTwoControl.RenterInformationConfirmation.EditButtonClick = MoveToPreviousStep;
            BaseView.StepTwoControl.PayerInformationConfirmation.EditButtonClick = MoveToPreviousStep;
            BaseView.StepTwoControl.LeasingAgentConfirmation.EditButtonClick = MoveToPreviousStep;
            BaseView.StepTwoControl.PaymentInformationConfirmation.EditButtonClick = MoveToPreviousStep;
            BaseView.StepTwoControl.PropertyInformationConfirmation.EditButtonClick = MoveToPreviousStep;
            BaseView.StepTwoControl.CharityDonationConfirmation.EditButtonClick = MoveToPreviousStep;
            BaseView.NextButtonClicked = MoveToNextStep;
            BaseView.BackButtonClicked = MoveToPreviousStep;
            BaseView.ConfirmButtonClicked = ConfirmPayment;
        }

        public virtual void InitializeValues()
        {
            BaseView.PaymentView.SetActiveView(BaseView.StepOne);
        }

        protected abstract void ConfirmPayment(object sender, EventArgs e);
        protected abstract void SendEmail(int propertyId, string subject, string body, List<MailAddress> addresses);
        protected abstract void RedirectToThankYou(Dictionary<String, String> queryStringParams);
        protected abstract Property GetProperty();

        protected virtual void MoveToNextStep(object sender, EventArgs e)
        {
            if (!ValidateStepOne()) 
                return;

            if (BaseView.PaymentView.GetActiveView() != BaseView.StepOne) 
                return;

            BaseView.PaymentView.SetActiveView(BaseView.StepTwo);
            BaseView.StepTwoControl.RenterInformationConfirmation.DataSource = BuildRenterInformation();
            BaseView.StepTwoControl.PayerInformationConfirmation.DataSource = BuildPayerInformation();
            BaseView.StepTwoControl.LeasingAgentConfirmation.DataSource = BuildLeasingAgentInformation();
            BaseView.StepTwoControl.PropertyInformationConfirmation.DataSource = BuildPropertyInformation();
            BaseView.StepTwoControl.PaymentInformationConfirmation.DataSource = BuildPaymentInformation();
            BaseView.StepTwoControl.CharityDonationConfirmation.DataSource = BuildCharityInformation();   
        }

        protected virtual void MoveToPreviousStep(object sender, EventArgs e)
        {
            if (BaseView.PaymentView.GetActiveView() == BaseView.StepTwo)
            {
                BaseView.StepTwoControl.TermsAccepted = false;
                BaseView.PaymentView.SetActiveView(BaseView.StepOne);
            }
        }

        protected virtual List<string> BuildRenterInformation()
        {
            var Renter = GetSelectedRenter();
            return new List<string> {Renter.DisplayName, Renter.DisplayAddress, Renter.MainPhoneNumber.FormatPhoneNumber(), Renter.PrimaryEmailAddress};
        }

        protected virtual List<string> BuildPayerInformation()
        {
            var Payer = GetPayer();
            return new List<string> {Payer.DisplayName, Payer.DisplayAddress, Payer.MainPhoneNumber.FormatPhoneNumber(), Payer.PrimaryEmailAddress};
        }

        protected virtual List<string> BuildLeasingAgentInformation()
        {
            var LeasingAgent = GetLeasingAgent();
            return new List<string> {LeasingAgent.DisplayName, LeasingAgent.DisplayAddress, LeasingAgent.MainPhoneNumber.FormatPhoneNumber(), LeasingAgent.PrimaryEmailAddress};
        }

        protected virtual List<string> BuildPropertyInformation()
        {
            var Property = GetProperty();
            // Patrick Whittingham - 9/2/15 - task #371 : add notification email address 
            return new List<string> { Property.PropertyName, Property.DisplayAddress, Property.MainPhoneNumber.ToString(false).FormatPhoneNumber(), Property.OfficeEmailAddress, Property.NotificationEmailAddress };
        }

        protected virtual List<string> BuildPaymentInformation()
        {
            var Info = new List<string>();
            var PaymentMethod = GetPaymentMethod();
            var PaymentAmount = GetPaymentAmount();
            var Property = GetProperty();
            bool IsCreditCard;
            var Total = 0.0M;

            if (GetSelectedPaymentType() == PaymentType.CreditCard)
            {
                Info.Add("Credit Card");
                Info.Add(String.Format("Credit Card Number: {0}", PaymentMethod.CreditCardAccountNumber.MaskAccountNumber()));
                IsCreditCard = true;
            }
            else
            {
                Info.Add("ECheck");
                Info.Add(String.Format("Account Number: {0}", PaymentMethod.BankAccountNumber.MaskAccountNumber()));
                Info.Add(String.Format("Routing Number: {0}", PaymentMethod.BankRoutingNumber));
                IsCreditCard = false;
            }

            if (PaymentAmount.RentAmount.HasValue && PaymentAmount.RentAmount.Value > 0.0M)
            {
                Info.Add(String.Format("Rent Payment: {0}", PaymentAmount.RentAmount.Value.ToString("C")));
                Total += PaymentAmount.RentAmount.Value;
            }
            if (PaymentAmount.OtherAmount1.HasValue && PaymentAmount.OtherAmount1.Value > 0.0M)
            {
                Info.Add(String.Format("Additional Payment 1: {0}", PaymentAmount.OtherAmount1.Value.ToString("C")));
                Total += PaymentAmount.OtherAmount1.Value;
            }
            if (PaymentAmount.OtherAmount2.HasValue && PaymentAmount.OtherAmount2.Value > 0.0M)
            {
                Info.Add(String.Format("Additional Payment 2: {0}", PaymentAmount.OtherAmount2.Value.ToString("C")));
                Total += PaymentAmount.OtherAmount2.Value;
            }
            if (PaymentAmount.OtherAmount3.HasValue && PaymentAmount.OtherAmount3.Value > 0.0M)
            {
                Info.Add(String.Format("Additional Payment 3: {0}", PaymentAmount.OtherAmount3.Value.ToString("C")));
                Total += PaymentAmount.OtherAmount3.Value;
            }
            if (PaymentAmount.OtherAmount4.HasValue && PaymentAmount.OtherAmount4.Value > 0.0M)
            {
                Info.Add(String.Format("Additional Payment 4: {0}", PaymentAmount.OtherAmount4.Value.ToString("C")));
                Total += PaymentAmount.OtherAmount4.Value;
            }
            if (PaymentAmount.OtherAmount5.HasValue && PaymentAmount.OtherAmount5.Value > 0.0M)
            {
                Info.Add(String.Format("Additional Payment 5: {0}", PaymentAmount.OtherAmount5.Value.ToString("C")));
                Total += PaymentAmount.OtherAmount5.Value;
            }

            if ((Property.PropertyId.HasValue && Property.PropertyId > 0) && HasRentAmount(BuildRenterPaymentInfo()))
            {
                var Fee = Property.GetConvenienceFeesByProperty(Property);
                Info.Add(IsCreditCard ? String.Format("Convenience Fee: {0} ", Fee.CreditCardFee.ToString("C")) : String.Format("Convenience Fee: {0} ", Fee.ECheckFee.ToString("C")));
                Total += IsCreditCard ? Fee.CreditCardFee : Fee.ECheckFee;
            }

            Info.Add(String.Format("<hr />Total: {0}", Total.ToString("C")));

            return Info;
        }

        protected virtual List<string> BuildCharityInformation()
        {
            var Charity = GetCharity();
            return new List<string> {Charity.CharityName, Charity.CharityAmount.HasValue ? Charity.CharityAmount.Value.ToString("C") : string.Empty};
        }

        protected virtual bool ValidatePage()
        {
            if (!BaseView.StepTwoControl.TermsAccepted)
            {
                BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "You must accept the terms before we can process your payment.");
                return false;
            }

            var Renter = GetSelectedRenter();

            if (Renter.AcceptedPaymentTypeId == 4)
            {
                    BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Cannot accept a payment for the selected resident.");
                    return false;
            }

            if (!Renter.IsValid)
            {
                BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Please select a valid resident.");
                return false;
            }

            var Property = GetProperty();

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Please select a valid property.");
                return false;
            }

            return true;
        }

        protected virtual bool ValidateStepOne()
        {
            var Payer = GetPayer();
            var PaymentMethod = GetPaymentMethod();
            var CharityDonation = GetCharity();
            var PaymentType = GetSelectedPaymentType();

            if (String.IsNullOrEmpty(Payer.FirstName) || String.IsNullOrEmpty(Payer.LastName))
            {
                BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Payer First and Last Name are required, please try again.");
                return false;
            }

            if (String.IsNullOrEmpty(Payer.PrimaryEmailAddress))
            {
                BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Payer email address is required, please try again.");
                return false;
            }

            if (PaymentType == PaymentType.ECheck)
            {
                var Renter = GetSelectedRenter();

                if (Renter.AcceptedPaymentTypeId == 3)
                {
                    BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Can only accept Credit Card Payments from this resident.");
                    return false;
                }

                if (String.IsNullOrEmpty(PaymentMethod.BankAccountNumber) || String.IsNullOrEmpty(PaymentMethod.BankRoutingNumber))
                {
                    BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "A valid bank account number and routing number are required to process an eCheck, please try again.");
                    return false;
                }

                if (!AchService.ValidateRoutingCheckDigit(PaymentMethod.BankRoutingNumber))
                {
                    BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Invalid routing number entered, please try again.");
                    return false;
                }
            }

            if (PaymentType == PaymentType.CreditCard)
            {
                if (String.IsNullOrEmpty(PaymentMethod.CreditCardAccountNumber) || String.IsNullOrEmpty(PaymentMethod.CreditCardCode) || String.IsNullOrEmpty(PaymentMethod.CreditCardHolderName) ||
                    !PaymentMethod.CreditCardExpirationMonth.HasValue || !PaymentMethod.CreditCardExpirationYear.HasValue)
                {
                    BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Required credit card data has not been filled in, please try again.");
                    return false;
                }

                if (new DateTime(PaymentMethod.CreditCardExpirationYear.Value, PaymentMethod.CreditCardExpirationMonth.Value, DateTime.UtcNow.Day, 11, 59, 59, 999) < DateTime.UtcNow)
                {
                    BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Invalid or expired expiration date, please try again");
                    return false;
                }

                if (PaymentMethod.CreditCardCode.Length < 3 || PaymentMethod.CreditCardCode.Length > 4)
                {
                    BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Invalid Card Verification Code entered, please try again.");
                return false;
                }
            }

            if (!HasRentAmount(BuildRenterPaymentInfo()) && (!CharityDonation.CharityAmount.HasValue || CharityDonation.CharityAmount.Value < 0.01M))
            {
                BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "You have not entered any payment amounts to process, please try again.");
                return false;
            }

            if (!OtherAmountsHaveDescription(BuildRenterPaymentInfo()))
            {
                BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "If you have entered a value for an Additional amount, you must also provide a description.");
                return false;
            }

            if ((CharityDonation.CharityAmount.HasValue && CharityDonation.CharityAmount.Value > 0.0M) && String.IsNullOrEmpty(CharityDonation.CharityName))
            {
                BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "You entered a charity amount but didn't select a charity to donate to, please try again.");
                return false;
            }

            return true;
        }

        protected virtual RenterPaymentInfo BuildRenterPaymentInfo()
        {
            var Payer = GetPayer();
            var Renter = GetSelectedRenter();
            var Property = GetProperty();
            var PaymentMethod = GetPaymentMethod();
            var PaymentAmount = GetPaymentAmount();
            var CharityDonation = GetCharity();
            var ExpirationYear = PaymentMethod.CreditCardExpirationYear.HasValue ? PaymentMethod.CreditCardExpirationYear.Value : DateTime.UtcNow.Year - 1;
            var ExpirationMonth = PaymentMethod.CreditCardExpirationMonth.HasValue ? PaymentMethod.CreditCardExpirationMonth.Value : DateTime.UtcNow.Month;
            var PaymentType = GetSelectedPaymentType();

            return new RenterPaymentInfo
            {
                RenterId = Renter.RenterId,
                BankAccountNumber = PaymentMethod.BankAccountNumber,
                BankRoutingNumber = PaymentMethod.BankRoutingNumber,
                CreditCardHolderName = PaymentMethod.CreditCardHolderName,
                CreditCardNumber = PaymentMethod.CreditCardAccountNumber,
                Cvv = PaymentMethod.CreditCardCode,
                ExpirationDate = new DateTime(ExpirationYear, ExpirationMonth, 1),
                PayerId = Payer.PayerId,
                PropertyId = Property.PropertyId.HasValue ? Property.PropertyId.Value : 0,
                CharityAmount = CharityDonation.CharityAmount.HasValue ? CharityDonation.CharityAmount.Value : 0.0M,
                CharityName = CharityDonation.CharityName,
                RentAmount = PaymentAmount.RentAmount.HasValue ? PaymentAmount.RentAmount.Value : 0.0M,
                RentAmountDescription = PaymentAmount.RentAmountDescription,
                OtherAmount = PaymentAmount.OtherAmount1.HasValue ? PaymentAmount.OtherAmount1.Value : 0.0M,
                OtherAmountDescription = PaymentAmount.OtherDescription1,
                OtherAmount2 = PaymentAmount.OtherAmount2.HasValue ? PaymentAmount.OtherAmount2.Value : 0.0M,
                OtherAmountDescription2 = PaymentAmount.OtherDescription2,
                OtherAmount3 = PaymentAmount.OtherAmount3.HasValue ? PaymentAmount.OtherAmount3.Value : 0.0M,
                OtherAmountDescription3 = PaymentAmount.OtherDescription3,
                OtherAmount4 = PaymentAmount.OtherAmount4.HasValue ? PaymentAmount.OtherAmount4.Value : 0.0M,
                OtherAmountDescription4 = PaymentAmount.OtherDescription4,
                OtherAmount5 = PaymentAmount.OtherAmount5.HasValue ? PaymentAmount.OtherAmount5.Value : 0.0M,
                OtherAmountDescription5 = PaymentAmount.OtherDescription5,
                DisplayName = Renter.DisplayName,
                PaymentType = PaymentType
            };
        }

        protected virtual void SendNotifications(PaymentResponse response, RenterPaymentInfo info)
        {
            var Renter = GetSelectedRenter();
            //var LeasingAgentEmail = GetLeasingAgent().PrimaryEmailAddress;
            var Total = 0.0M;

            if (HasRentAmount(info) && (response.RentAndFeeResult == PaymentMethods.GeneralResponseResult.Success))
            {
                //Handle successful Rent and Fees payment
                var PaymentDescription = new StringBuilder();
                if (info.RentAmount.HasValue && info.RentAmount.Value > 0.0M)
                {
                    PaymentDescription.AppendFormat("{0} {1}", info.RentAmountDescription, info.RentAmount.Value.ToString("C"));
                    Total += info.RentAmount.Value;
                }
                if (info.OtherAmount.HasValue && info.OtherAmount.Value > 0.0M)
                {
                    PaymentDescription.AppendFormat(", {0} {1}", info.OtherAmountDescription, info.OtherAmount.Value.ToString("C"));
                    Total += info.OtherAmount.Value;
                }
                if (info.OtherAmount2.HasValue && info.OtherAmount2.Value > 0.0M)
                {
                    PaymentDescription.AppendFormat(", {0} {1}", info.OtherAmountDescription2, info.OtherAmount2.Value.ToString("C"));
                    Total += info.OtherAmount2.Value;
                }
                if (info.OtherAmount3.HasValue && info.OtherAmount3.Value > 0.0M)
                {
                    PaymentDescription.AppendFormat(", {0} {1}", info.OtherAmountDescription3, info.OtherAmount3.Value.ToString("C"));
                    Total += info.OtherAmount3.Value;
                }
                if (info.OtherAmount4.HasValue && info.OtherAmount4.Value > 0.0M)
                {
                    PaymentDescription.AppendFormat(", {0} {1}", info.OtherAmountDescription4, info.OtherAmount4.Value.ToString("C"));
                    Total += info.OtherAmount4.Value;
                }
                if (info.OtherAmount5.HasValue && info.OtherAmount5.Value > 0.0M)
                {
                    PaymentDescription.AppendFormat(", {0} {1}", info.OtherAmountDescription5, info.OtherAmount5.Value.ToString("C"));
                    Total += info.OtherAmount5.Value;
                }

                Total += response.ConvenienceFee;
                var Property = EfxFramework.Property.GetPropertyByRenterId(Renter.RenterId);
                //Salcedo - 3/12/2014 - replaced with new html email template
                //SendEmailX(info.PropertyId, "Payment Confirmation", string.Format(EmailTemplates.OneTimeRentPayment, Renter.DisplayName, PaymentDescription, 
                //    DateTime.UtcNow.ToShortDateString(), response.RentAndFeeTransactionId, response.ConvenienceFee.ToString("C"), Total.ToString("C")), Renter.PrimaryEmailAddress);
                String RecipientName = Renter.DisplayName;
                //cakel: BUGID000180
                String RenterProperty = Property.PropertyName;
                String PaymentAmount = PaymentDescription.ToString();
                String TransactionDate = DateTime.UtcNow.ToShortDateString();
                String InternalTransactionId = response.RentAndFeeTransactionId;
                String ConvenienceAmount = response.ConvenienceFee.ToString("C");
                String TotalAmount = Total.ToString("C");
                String Body = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-payment-success.html").Replace("[[ROOT]]",
                    EmailTemplateService.EmailTemplateService.RootPath), RecipientName, RenterProperty, PaymentAmount, TransactionDate, InternalTransactionId, ConvenienceAmount, TotalAmount);
                SendEmailX(info.PropertyId, "Payment Confirmation", Body, Renter.PrimaryEmailAddress);

            }

            if (info.CharityAmount.HasValue && info.CharityAmount.Value > 0 && response.CharityResult == PaymentMethods.GeneralResponseResult.Success)
                SendEmailX(info.PropertyId, "Charity Payment Confirmation", string.Format(EmailTemplates.OneTimeCharityPayment, Renter.DisplayName, 
                    string.Format("{0} {1}", info.CharityName, info.CharityAmount.Value.ToString("C")), 
                    DateTime.UtcNow.ToShortDateString(), response.CharityTransactionId), Renter.PrimaryEmailAddress);
        }

        private void SendEmailX(int propertyId, string subject, string body, string renterEmail)
        {
            //var MailAddresses = new List<MailAddress>();
            var MailAddresses = Enumerable.Empty<MailAddress>().ToList();

            if (renterEmail.IsValidEmailAddress())
                MailAddresses.Add(new MailAddress(renterEmail));

            //Salcedo - 2/28/2014 - changed to always send, and to automatically include property management contacts

            //if (leasingAgentEmail.IsValidEmailAddress())
            //    MailAddresses.Add(new MailAddress(leasingAgentEmail));
            
            //if (MailAddresses.Count > 0)
            //    SendEmail(propertyId, subject, body, MailAddresses);

            SendEmail(propertyId, subject, body, MailAddresses);
        }

        protected virtual void RedirectToThankYou(PaymentResponse response, RenterPaymentInfo info)
        {
            var QueryStringParams = new Dictionary<String, String>
                {
                    {"RentAndFeeResult", response.RentAndFeeResult.ToString().UrlEncode()},
                    {"RentAndFeeResponseMessage", response.RentAndFeeResponseMessage.UrlEncode()},
                    {"RentAndFeeTransactionId", response.RentAndFeeTransactionId.UrlEncode()},
                    {"ConvenienceFee", response.ConvenienceFee.ToString(CultureInfo.InvariantCulture).UrlEncode()},
                    {"CharityResult", response.CharityResult.ToString().UrlEncode()},
                    {"CharityResponseMessage", response.CharityResponseMessage.UrlEncode()},
                    {"CharityTransactionId", response.CharityTransactionId.UrlEncode()}, 
                    {"RentStateId", response.RentAndFeeResult == PaymentMethods.GeneralResponseResult.Success ? "1" : "2"}, 
                    {"CharityStateId", response.CharityResult == PaymentMethods.GeneralResponseResult.Success ? "1" : "2"}
                };

            if (!HasRentAmount(info))
            {
                QueryStringParams["RentStateId"] = "0";
            }

            if (!info.CharityAmount.HasValue || info.CharityAmount.Value <= 0.0M)
                QueryStringParams["CharityStateId"] = "0";

            RedirectToThankYou(QueryStringParams);
        }

        protected virtual bool HasRentAmount(RenterPaymentInfo info)
        {
            var HasRentValue = info.RentAmount.HasValue && info.RentAmount.Value > 0.0M;
            var HasOther1Value = info.OtherAmount.HasValue && info.OtherAmount.Value > 0.0M;
            var HasOther2Value = info.OtherAmount2.HasValue && info.OtherAmount2.Value > 0.0M;
            var HasOther3Value = info.OtherAmount3.HasValue && info.OtherAmount3.Value > 0.0M;
            var HasOther4Value = info.OtherAmount4.HasValue && info.OtherAmount4.Value > 0.0M;
            var HasOther5Value = info.OtherAmount5.HasValue && info.OtherAmount5.Value > 0.0M;

            return HasRentValue || HasOther1Value || HasOther2Value || HasOther3Value || HasOther4Value || HasOther5Value;
        }

        protected virtual bool OtherAmountsHaveDescription(RenterPaymentInfo info)
        {
            if ((info.OtherAmount.HasValue && info.OtherAmount.Value > 0.0M) && String.IsNullOrEmpty(info.OtherAmountDescription))
                return false;
            if ((info.OtherAmount2.HasValue && info.OtherAmount2.Value > 0.0M) && String.IsNullOrEmpty(info.OtherAmountDescription2))
                return false;
            if ((info.OtherAmount3.HasValue && info.OtherAmount3.Value > 0.0M) && String.IsNullOrEmpty(info.OtherAmountDescription3))
                return false;
            if ((info.OtherAmount4.HasValue && info.OtherAmount4.Value > 0.0M) && String.IsNullOrEmpty(info.OtherAmountDescription4))
                return false;
            if ((info.OtherAmount5.HasValue && info.OtherAmount5.Value > 0.0M) && String.IsNullOrEmpty(info.OtherAmountDescription5))
                return false;

            return true;
        }

        protected virtual Renter GetSelectedRenter()
        {
            var RenterId = Int32.Parse(BaseView.StepOneControl.RenterSelectionControl.RenterDropdownSelectedValue);

            if (RenterId < 1)
                return new Renter();

            var Renter = new Renter(RenterId);

            return !Renter.IsValid ? new Renter() : Renter;
        }

        protected virtual Payer GetPayer()
        {
            return new Payer
            {
                FirstName = BaseView.StepOneControl.PayerInformationControl.PayersFirstName,
                MiddleName = BaseView.StepOneControl.PayerInformationControl.PayersMiddleName,
                LastName = BaseView.StepOneControl.PayerInformationControl.PayersLastName,
                Suffix = BaseView.StepOneControl.PayerInformationControl.PayersSuffix,
                PrimaryEmailAddress = BaseView.StepOneControl.PayerInformationControl.PayersEmail,
                StreetAddress = BaseView.StepOneControl.PayerInformationControl.PayersAddress,
                StreetAddress2 = BaseView.StepOneControl.PayerInformationControl.PayersAddress2,
                City = BaseView.StepOneControl.PayerInformationControl.PayersCity,
                PostalCode = BaseView.StepOneControl.PayerInformationControl.PayersZipCode,
                MobilePhoneNumber = BaseView.StepOneControl.PayerInformationControl.PayersCellPhone,
                MainPhoneNumber = BaseView.StepOneControl.PayerInformationControl.PayersHomePhone,
                StateProvinceId = Int32.Parse(BaseView.StepOneControl.PayerInformationControl.StateDropdown.SelectedValue)
            };
        }

        protected virtual PropertyStaff GetLeasingAgent()
        {
            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(Int32.Parse(BaseView.StepOneControl.RenterSelectionControl.RenterDropdownSelectedValue));

            if (Lease.PropertyStaffId > 0)
                return new PropertyStaff(Lease.PropertyStaffId);

            var Property = GetProperty();
            if (Property.PropertyId.HasValue && Property.PropertyId > 0)
            {
                var MainContact = Property.GetMainPropertyContactByPropertyId(Property.PropertyId.Value);

                if (MainContact.PropertyStaffId.HasValue && MainContact.PropertyStaffId > 0)
                    return MainContact;
            }

            return new PropertyStaff();
        }

        protected virtual PaymentType GetSelectedPaymentType()
        {
            switch (BaseView.StepOneControl.PaymentMethodsControl.PaymentMethodsSelectedValue)
            {
                case "echeck":
                    return PaymentType.ECheck;

                case "credit":
                    return PaymentType.CreditCard;

                default:
                    return PaymentType.ECheck;
            }
        }

        protected Payment GetPaymentAmount()
        {
            decimal Rent;
            decimal Other1;
            decimal Other2;
            decimal Other3;
            decimal Other4;
            decimal Other5;

            decimal.TryParse(BaseView.StepOneControl.PaymentAmountControl.RentAmountText.Replace("$", ""), out Rent);
            decimal.TryParse(BaseView.StepOneControl.PaymentAmountControl.OtherAmount1Text.Replace("$", ""), out Other1);
            decimal.TryParse(BaseView.StepOneControl.PaymentAmountControl.OtherAmount2Text.Replace("$", ""), out Other2);
            decimal.TryParse(BaseView.StepOneControl.PaymentAmountControl.OtherAmount3Text.Replace("$", ""), out Other3);
            decimal.TryParse(BaseView.StepOneControl.PaymentAmountControl.OtherAmount4Text.Replace("$", ""), out Other4);
            decimal.TryParse(BaseView.StepOneControl.PaymentAmountControl.OtherAmount5Text.Replace("$", ""), out Other5);

            return new Payment
            {
                RentAmount = Rent,
                OtherAmount1 = Other1,
                OtherAmount2 = Other2,
                OtherAmount3 = Other3,
                OtherAmount4 = Other4,
                OtherAmount5 = Other5,
                RentAmountDescription = BaseView.StepOneControl.PaymentAmountControl.RentDescriptionText,
                OtherDescription1 = BaseView.StepOneControl.PaymentAmountControl.OtherDescription1Text,
                OtherDescription2 = BaseView.StepOneControl.PaymentAmountControl.OtherDescription2Text,
                OtherDescription3 = BaseView.StepOneControl.PaymentAmountControl.OtherDescription3Text,
                OtherDescription4 = BaseView.StepOneControl.PaymentAmountControl.OtherDescription4Text,
                OtherDescription5 = BaseView.StepOneControl.PaymentAmountControl.OtherDescription5Text
            };
        }

        public virtual Payment GetPaymentMethod()
        {
            switch (BaseView.StepOneControl.PaymentMethodsControl.PaymentMethodsSelectedValue)
            {
                case "echeck":
                    return GetAchInfo();

                case "credit":
                    return GetCreditCardInfo();

                default:
                    return GetAchInfo();
            }
        }

        public Payment GetCharity()
        {
            decimal Amount;
            Decimal.TryParse(BaseView.StepOneControl.CharityDonationControl.DonationAmountText.Replace("$", ""), out Amount);

            return new Payment
            {
                CharityAmount = Amount,
                CharityName = BaseView.StepOneControl.CharityDonationControl.SelectedCharityValue != "-1" ? BaseView.StepOneControl.CharityDonationControl.SelectedCharityText : string.Empty
            };
        }

        protected virtual Payment GetAchInfo()
        {
            string AccountNumber;

            if (BaseView.StepOneControl.PaymentMethodsControl.BankAccountNumberText.Contains("*"))
            {
                var Renter = GetSelectedRenter();
                var Payer = EfxFramework.Payer.GetPayerByRenterId(Renter.RenterId);
                var PayerAch = new PayerAch();

                if (Payer.PayerId > 0)
                    PayerAch = EfxFramework.PayerAch.GetPrimaryPayerAchByPayerId(Payer.PayerId);

                AccountNumber = PayerAch.BankAccountNumber;
            }
            else
            {
                AccountNumber = BaseView.StepOneControl.PaymentMethodsControl.BankAccountNumberText;
            }

            return new Payment
            {
                BankAccountNumber = AccountNumber,
                BankRoutingNumber = BaseView.StepOneControl.PaymentMethodsControl.RoutingNumberText
            };
        }

        protected virtual Payment GetCreditCardInfo()
        {
            string AccountNumber;

            if (BaseView.StepOneControl.PaymentMethodsControl.CreditCardNumberText.Contains("*"))
            {
                var Renter = GetSelectedRenter();
                var Payer = EfxFramework.Payer.GetPayerByRenterId(Renter.RenterId);
                var PayerCreditCard = new PayerCreditCard();

                if (Payer.PayerId > 0)
                    PayerCreditCard = EfxFramework.PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(Payer.PayerId);

                AccountNumber = PayerCreditCard.CreditCardAccountNumber;
            }
            else
            {
                AccountNumber = BaseView.StepOneControl.PaymentMethodsControl.CreditCardNumberText;
            }

            var ExpirationDate = ParseExpirationDate();

            return new Payment
            {
                CreditCardHolderName = BaseView.StepOneControl.PaymentMethodsControl.NameOnCardText,
                CreditCardAccountNumber = AccountNumber,
                CreditCardExpirationMonth = ExpirationDate.Month,
                CreditCardExpirationYear = ExpirationDate.Year,
                CreditCardCode = BaseView.StepOneControl.PaymentMethodsControl.CvvText
            };
        }

        protected virtual DateTime ParseExpirationDate()
        {
            DateTime ExpirationDate;
            var Month = DateTime.UtcNow.Month;
            var Year = DateTime.UtcNow.Year - 1;

            if (BaseView.StepOneControl.PaymentMethodsControl.ExpirationDateText.Length == 4)
            {
                Int32.TryParse(BaseView.StepOneControl.PaymentMethodsControl.ExpirationDateText.Substring(0, 2), out Month);
                Int32.TryParse(String.Format("20{0}", BaseView.StepOneControl.PaymentMethodsControl.ExpirationDateText.Substring(2, 2)), out Year);

                ExpirationDate = new DateTime(Year, Month, 1);
            }
            else
            {
                if (!DateTime.TryParse(BaseView.StepOneControl.PaymentMethodsControl.ExpirationDateText, out ExpirationDate))
                    ExpirationDate = new DateTime(Year, Month, 1);
            }

            return ExpirationDate;
        }
    }
}