﻿using EfxFramework.Interfaces;

namespace EfxFramework.Presenters
{
    public static class SinglePaymentFactory
    {
        public static SinglePaymentBase<EfxAdministrator> GetSinglePayment(IEfxAdministratorSinglePayment view)
        {
            return new EfxAdministratorSinglePayment(view);
        }

        public static SinglePaymentBase<PropertyStaff> GetSinglePayment(IPropertyStaffSinglePayment view)
        {
            return new PropertyStaffSinglePayment(view);
        }

        public static SinglePaymentBase<Renter> GetSinglePayment(IRenterSinglePayment view)
        {
            return new RenterSinglePayment(view);
        }
    }
}
