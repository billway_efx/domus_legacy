﻿using System.Web;
using EfxFramework.Interfaces;
using EfxFramework.Interfaces.UserControls;
using EfxFramework.Web.Facade;
using Mb2x.ExtensionMethods;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;

namespace EfxFramework.Presenters
{
    public class PropertyEdit : PropertyAddEditBase
    {
        private readonly IPropertyEdit _View;
        private Property CurrentProperty;

        #region Constructors
        public PropertyEdit(IPropertyEdit view)
            : base(view)
        {
            _View = view;
            _View.SavePropertyDetailsClicked = SavePropertyDetails;
            _View.SaveBankingInfoClicked = SaveBankingInfo;
        }
        #endregion

        #region Initializers
        public void IntitializeValues()
        {
            CurrentProperty = _View.CurrentPropertyId > 0 ? new Property(_View.CurrentPropertyId) : new Property();
            SetNameAndDetails();
        }

        private void SetNameAndDetails()
        {
            if (!CurrentProperty.PropertyId.HasValue || CurrentProperty.PropertyId.Value < 1)
                return;

            _View.NameTabText = CurrentProperty.PropertyName;
            _View.PropertyDetailInfo.PropertyId = CurrentProperty.PropertyId.Value;
            _View.PmsSelection.PropertyId = CurrentProperty.PropertyId.Value;
            _View.ProgramInformation.PropertyId = CurrentProperty.PropertyId.Value;
            _View.BankingInformation.PropertyId = CurrentProperty.PropertyId.Value;

            SetPropertyPhotos(CurrentProperty.PropertyId.Value);

        }

        private void SetPropertyPhotos(int propertyId)
        {
            var Photo1 = Property.GetPhoto1(propertyId);
            var Photo2 = Property.GetPhoto2(propertyId);
            var Photo3 = Property.GetPhoto3(propertyId);
            var Photo4 = Property.GetPhoto4(propertyId);

            _View.ImageAdder.CurrentPhotos = new List<byte[]>();

            if (Photo1 != null && Photo1.Length > 0)
                _View.ImageAdder.CurrentPhotos.Add(Photo1);
            if (Photo2 != null && Photo2.Length > 0)
                _View.ImageAdder.CurrentPhotos.Add(Photo2);
            if (Photo3 != null && Photo3.Length > 0)
                _View.ImageAdder.CurrentPhotos.Add(Photo3);
            if (Photo4 != null && Photo4.Length > 0)
                _View.ImageAdder.CurrentPhotos.Add(Photo4);
        }

        #endregion

        #region Event Handlers
        private void SavePropertyDetails(object sender, EventArgs e)
        {
            if (!ValidatePropertyDetails(_View.ParentPage) || !ValidatePmsInformation(_View.ParentPage) || !ValidateProgramInformation(_View.ParentPage))
                return;

            SaveProperty(BuildProperty(_View.CurrentPropertyId > 0 ? new Property(_View.CurrentPropertyId) : new Property()));
            //CMallory - Task 00425 - Added to delete disable the payments not accessible by the front end if the Disable All Payments
            //Checkbox is checked.
            if (_View.PropertyDetailInfo.DisableAllPayments)
            {
                NewDataAccess.ExecuteProcedure(EfxSettings.ConnectionString, "dbo.usp_Property_DiableAllPayments",
               new[]
                {
                    new SqlParameter("@PropertyId", _View.CurrentPropertyId)
                }
               );
            }

            //CMallory - Task 00554 - Added to delete disable the payments not accessible by the front end if the Disable All Payments
            if (_View.PropertyDetailInfo.DisablePayByText)
            {
                NewDataAccess.ExecuteProcedure(EfxSettings.ConnectionString, "usp_Property_DiablePayByText",
               new[]
                {
                    new SqlParameter("@PropertyId", _View.CurrentPropertyId)
                }
               );
            }
        }

        private void SaveBankingInfo(object sender, EventArgs e)
        {
            if (!ValidateBankingDetails(_View.ParentPage))
                return;
            if (CurrentProperty == null)
                CurrentProperty = _View.CurrentPropertyId > 0 ? new Property(_View.CurrentPropertyId) : new Property();
            if (!CurrentProperty.PropertyId.HasValue || CurrentProperty.PropertyId.Value < 1)
                return;

            ;
            try
            {
                var Property = BuildBankingInformation(CurrentProperty);
                //CMallory - 11/01/2016 - Commented out line below and replaced it with the one below that because it wasn't updating the property in the database.
                //EfxFramework.Property.Set(Property);
                SaveProperty(BuildProperty(_View.CurrentPropertyId > 0 ? new Property(_View.CurrentPropertyId) : new Property()));
                //_View.PageMessage.DisplayMessage(SystemMessageType.Success, "Banking Information Successfully Saved.");
                _View.SuccessMessagePlaceHolder.Visible = true;
            }
            catch (Exception ex)
            {
                //cakel: Added "throw" to see what the error is - For testing only
                bool checkLocal = HttpContext.Current.Request.IsLocal;
                if (checkLocal == true)
                {
                    throw ex;
                }
                else
                {
                    _View.SafeRedirect("~/ErrorPage.html");
                }
            }
        }
        #endregion

        #region Helper Methods
        protected override Property BuildProperty()
        {
            var Property = new Property();

            Property.PropertyId = _View.CurrentPropertyId > 0 ? _View.CurrentPropertyId : 0;
            Property.PropertyCode = _View.PropertyDetailInfo.PropertyCodeText;
            Property.PropertyName = _View.PropertyDetailInfo.PropertyNameText;
            //cakel: 00498
            Property.PropertyNameTwo = _View.PropertyDetailInfo.PropertyNameTwoText;
            Property.NumberOfUnits = _View.PropertyDetailInfo.NumberOfUnitsText.ToNullInt32();
            Property.StreetAddress = _View.PropertyDetailInfo.PropertyAddressText;
            Property.StreetAddress2 = _View.PropertyDetailInfo.PropertyAddress2Text;
            Property.City = _View.PropertyDetailInfo.CityText;
            Property.StateProvinceId = _View.PropertyDetailInfo.StateList.SelectedValue.ToNullInt32();
            Property.PostalCode = _View.PropertyDetailInfo.PostalCodeText;
            Property.MainPhoneNumber = new PhoneNumber(_View.PropertyDetailInfo.MainTelephoneText);
            Property.AlternatePhoneNumber = new PhoneNumber(_View.PropertyDetailInfo.AlternateTelephoneText);
            Property.FaxNumber = new PhoneNumber(_View.PropertyDetailInfo.FaxNumberText);
            Property.OfficeEmailAddress = _View.PropertyDetailInfo.OfficeEmailText;
            // Patrick Whittingham - 9/2/15 - task #371 : add notification email address 
            Property.NotificationEmailAddress = _View.PropertyDetailInfo.NotificationEmailText;
            Property.AverageMonthlyRent = _View.PropertyDetailInfo.AverageRentText.Replace("$", "").ToNullDecimal();
            Property.IsVerifiedRentReportersProperty = _View.PropertyDetailInfo.RentReportersVerifiedChecked;
            Property.ShowPaymentDetail = _View.PropertyDetailInfo.LineItemDetailChecked;
            Property.TimeZone = _View.PropertyDetailInfo.TimeZoneList.SelectedValue;
            Property.AllowPartialPayments = _View.PropertyDetailInfo.AllowPartialPayments;

            //cakel: Added BugID 007
            Property.NumOfPrePayDays = _View.PropertyDetailInfo.NumOfPrePayDays;
            Property.AllowPrePayments = _View.PropertyDetailInfo.AllowPrePayments;
            Property.PaymentDueDay = _View.PropertyDetailInfo.PaymentDueDay;
            //cakel BUGID00019, BUGID00276
            Property.AllowPartialPaymentsResidentPortal = _View.PropertyDetailInfo.AllowPartialPaymentsResidentPortal;

            Property.AcceptACHPayments = _View.PropertyDetailInfo.AcceptACHPayments;
            Property.AcceptCCPayments = _View.PropertyDetailInfo.AcceptCCPayments;
            Property.PNMEnabled = _View.PropertyDetailInfo.PNMEnabled;
            //CMallory - Task 00499
            Property.DisableAllPayments = _View.PropertyDetailInfo.DisableAllPayments;
            //CMallory - Task 00554
            Property.DisablePayByText = _View.PropertyDetailInfo.DisablePayByText;
            //CMallory - Task 00166 - Added
            Property.PropertyIsPublic = _View.PropertyDetailInfo.PropertyIsPublic;
            //CMallory - Task 00600 - Added
            Property.CompanyID = _View.PropertyDetailInfo.PmCompanySelectedValue.ToInt32();

            return Property;
        }

        protected Property BuildProperty(Property currentProperty)
        {
            currentProperty.PropertyId = _View.CurrentPropertyId > 0 ? _View.CurrentPropertyId : 0;
            currentProperty.PropertyCode = _View.PropertyDetailInfo.PropertyCodeText;
            currentProperty.PropertyName = _View.PropertyDetailInfo.PropertyNameText;
            //cakel: 00498
            currentProperty.PropertyNameTwo = _View.PropertyDetailInfo.PropertyNameTwoText;
            currentProperty.NumberOfUnits = _View.PropertyDetailInfo.NumberOfUnitsText.ToNullInt32();
            currentProperty.StreetAddress = _View.PropertyDetailInfo.PropertyAddressText;
            currentProperty.StreetAddress2 = _View.PropertyDetailInfo.PropertyAddress2Text;
            currentProperty.City = _View.PropertyDetailInfo.CityText;
            currentProperty.StateProvinceId = _View.PropertyDetailInfo.StateList.SelectedValue.ToNullInt32();
            currentProperty.PostalCode = _View.PropertyDetailInfo.PostalCodeText;
            currentProperty.MainPhoneNumber = new PhoneNumber(_View.PropertyDetailInfo.MainTelephoneText);
            currentProperty.AlternatePhoneNumber = new PhoneNumber(_View.PropertyDetailInfo.AlternateTelephoneText);
            currentProperty.FaxNumber = new PhoneNumber(_View.PropertyDetailInfo.FaxNumberText);
            currentProperty.OfficeEmailAddress = _View.PropertyDetailInfo.OfficeEmailText;
            // Patrick Whittingham - 9/2/15 - task #371 : add notification email address  
            currentProperty.NotificationEmailAddress = _View.PropertyDetailInfo.NotificationEmailText;
            currentProperty.AverageMonthlyRent = _View.PropertyDetailInfo.AverageRentText.Replace("$", "").ToNullDecimal();
            currentProperty.IsVerifiedRentReportersProperty = _View.PropertyDetailInfo.RentReportersVerifiedChecked;
            currentProperty.ShowPaymentDetail = _View.PropertyDetailInfo.LineItemDetailChecked;
            currentProperty.TimeZone = _View.PropertyDetailInfo.TimeZoneList.SelectedValue;
            currentProperty.AllowPartialPayments = _View.PropertyDetailInfo.AllowPartialPayments;

            //cakel: BUGID00019, BUGID00276
            currentProperty.AllowPartialPaymentsResidentPortal = _View.PropertyDetailInfo.AllowPartialPaymentsResidentPortal;

            //cakel: Added BugID 007
            currentProperty.NumOfPrePayDays = _View.PropertyDetailInfo.NumOfPrePayDays;
            currentProperty.AllowPrePayments = _View.PropertyDetailInfo.AllowPrePayments;
            currentProperty.PaymentDueDay = _View.PropertyDetailInfo.PaymentDueDay;


            currentProperty.AcceptACHPayments = _View.PropertyDetailInfo.AcceptACHPayments;
            currentProperty.AcceptCCPayments = _View.PropertyDetailInfo.AcceptCCPayments;
            currentProperty.PNMEnabled = _View.PropertyDetailInfo.PNMEnabled;
            //CMallory - Task 00499
            currentProperty.DisableAllPayments = _View.PropertyDetailInfo.DisableAllPayments;
            //CMallory - Task 00554 
            currentProperty.DisablePayByText = _View.PropertyDetailInfo.DisablePayByText;
            //Cmallory - Task 00166 - Added
            currentProperty.PropertyIsPublic = _View.PropertyDetailInfo.PropertyIsPublic;
            //CMallory - Task 00600 - Added
            currentProperty.CompanyID = _View.PropertyDetailInfo.PmCompanySelectedValue.ToInt32();
            return currentProperty;
        }
        #endregion

    }
}
