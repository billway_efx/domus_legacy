﻿using EfxFramework.Interfaces;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RPO;
using System.Linq;

namespace EfxFramework.Presenters
{
   public abstract class PropertyAddEditBase
   {
      protected IPropertyAddEdit BaseView { get; set; }

      #region Constructors

      protected PropertyAddEditBase(IPropertyAddEdit view)
      {
         BaseView = view;
      }

      #endregion

      #region Update Methods
      protected virtual void SaveProperty(Property property = null)
      {
         try
         {
            var Property = property ?? BuildProperty();
            Property = BuildBankingInformation(Property);
            Property = BuildInitialPmsInformation(Property);
            Property = BuildProgramInformation(Property);
            BaseView.CurrentPropertyId = EfxFramework.Property.Set(Property);
            //BaseView.AddStaffToProperty.CurrentPropertyId = BaseView.CurrentPropertyId;



            SavePropertyPhotos();
            SavePropertyCompany();
            SavePropertyPartialPaymentOption(property);

            //BaseView.PageMessage.DisplayMessage(SystemMessageType.Success, "Property Succesfully Saved.");
         }
         catch (Exception ex)
         {
            //cakel: checking for errors when running local
            if (HttpContext.Current.Request.IsLocal)
            {
               throw ex;
            }
            else
            {
               BaseView.SafeRedirect("~/ErrorPage.html");
            }

         }
      }
      private void SavePropertyPartialPaymentOption(Property property)
      {
         using (var entity = new RPOEntities())
         {
            var propertyEntity = entity.Properties.First(p => p.PropertyId == property.PropertyId);
            propertyEntity.AllowPartialPayments = property.AllowPartialPayments;
            propertyEntity.AllowPartialPaymentsResidentialPortal = property.AllowPartialPaymentsResidentPortal;

            //Added 05.29.2020 - BW to Set the AllowPartialPaymentResidentPortal field in the Renter Table
            SetAllowPartialPaymentResidentPortal(propertyEntity.PropertyId, Boolean.Parse(property.AllowPartialPaymentsResidentPortal.ToString()));

            entity.SaveChanges();
         }
      }

      private void SetAllowPartialPaymentResidentPortal(int propertyId, bool AllowPartialPayment)
      {
         SqlConnection con = new SqlConnection();
         SqlCommand cmd = new SqlCommand();

         con.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

         cmd.Connection = con;
         if (AllowPartialPayment == true)
         {
            cmd.CommandText = "update Pay_PayableControllerItems set AllowPartialPayments = 1 where PropertyId = " + propertyId.ToString();
         }
         else
         {
            cmd.CommandText = "update Pay_PayableControllerItems set AllowPartialPayments = 0 where PropertyId = " + propertyId.ToString();
         }


         con.Open();
         cmd.ExecuteNonQuery();
         con.Close();
      }

      protected virtual void SavePropertyPhotos()
      {
         var Photos = HttpContext.Current.Session[SessionVariable.ImageBytes.ToString()] as List<Photo>;

         if (Photos == null)
            return;

         if (Photos.Count > 0)
            Property.SetPhoto1(BaseView.CurrentPropertyId, Photos[0].PhotoBytes);
         if (Photos.Count > 1)
            Property.SetPhoto2(BaseView.CurrentPropertyId, Photos[1].PhotoBytes);
         if (Photos.Count > 2)
            Property.SetPhoto3(BaseView.CurrentPropertyId, Photos[2].PhotoBytes);
         if (Photos.Count > 3)
            Property.SetPhoto4(BaseView.CurrentPropertyId, Photos[3].PhotoBytes);
      }

      protected virtual void SavePropertyCompany()
      {
         if (BaseView.PropertyDetailInfo.PmCompanySelectedValue != "-1")
            Company.AssignPropertyToCompany(BaseView.CurrentPropertyId, BaseView.PropertyDetailInfo.PmCompanySelectedValue.ToInt32());
      }
      #endregion

      #region Validation
      protected virtual bool ValidatePropertyDetails(Page _view = null)
      {
         if (String.IsNullOrEmpty(BaseView.PropertyDetailInfo.PropertyNameText))
         {
            if (_view != null)
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Property Name Required.";
               _view.Validators.Add(err);
            }
            //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Property Name Required");
            return false;
         }

         if (String.IsNullOrEmpty(BaseView.PropertyDetailInfo.PropertyAddressText))
         {
            if (_view != null)
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Property Address Required.";
               _view.Validators.Add(err);
            }
            //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Property Address Required");
            return false;
         }

         if (String.IsNullOrEmpty(BaseView.PropertyDetailInfo.CityText))
         {
            if (_view != null)
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "City Required.";
               _view.Validators.Add(err);
            }
            //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "City Required");
            return false;
         }

         if (BaseView.PropertyDetailInfo.StateList.SelectedValue.ToInt32() < 1)
         {
            if (_view != null)
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "State Selection Required.";
               _view.Validators.Add(err);
            }
            //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "State Selection Required");
            return false;
         }

         if (String.IsNullOrEmpty(BaseView.PropertyDetailInfo.PostalCodeText))
         {
            if (_view != null)
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Postal Code Required.";
               _view.Validators.Add(err);
            }
            //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Postal Code Required");
            return false;
         }

         if (String.IsNullOrEmpty(BaseView.PropertyDetailInfo.MainTelephoneText))
         {
            if (_view != null)
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Main Phone Number Required.";
               _view.Validators.Add(err);
            }
            //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Main Phone Number Required");
            return false;
         }

         if (!IsValidPhoneNumber(BaseView.PropertyDetailInfo.MainTelephoneText))
         {
            if (_view != null)
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Main Phone Number Invalid.  Phone numbers must contain 10 numeric characters.";
               _view.Validators.Add(err);
            }
            //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Main Phone Number Invalid.  Phone numbers must contain 10 numeric characters.");
            return false;
         }

         if (!String.IsNullOrEmpty(BaseView.PropertyDetailInfo.AlternateTelephoneText) && !IsValidPhoneNumber(BaseView.PropertyDetailInfo.AlternateTelephoneText))
         {
            if (_view != null)
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Alternate Phone Number Invalid.  Phone numbers must contain 10 numeric characters.";
               _view.Validators.Add(err);
            }
            //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Alternate Phone Number Invalid.  Phone numbers must contain 10 numeric characters.");
            return false;
         }

         if (!String.IsNullOrEmpty(BaseView.PropertyDetailInfo.FaxNumberText) && !IsValidPhoneNumber(BaseView.PropertyDetailInfo.FaxNumberText))
         {
            if (_view != null)
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Fax Number Invalid.  Phone numbers must contain 10 numeric characters.";
               _view.Validators.Add(err);
            }
            //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Fax Number Invalid.  Phone numbers must contain 10 numeric characters.");
            return false;
         }

         if (!String.IsNullOrEmpty(BaseView.PropertyDetailInfo.OfficeEmailText) && !BaseView.PropertyDetailInfo.OfficeEmailText.IsValidEmailAddress())
         {
            if (_view != null)
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Office Email Address Invalid.";
               _view.Validators.Add(err);
            }
            //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Office Email Address Invalid");
            return false;
         }

         // Patrick Whittingham - 9/2/15 - task #371 : add notification email address 
         if (!String.IsNullOrEmpty(BaseView.PropertyDetailInfo.NotificationEmailText) && !BaseView.PropertyDetailInfo.NotificationEmailText.IsValidEmailAddress())
         {
            if (_view != null)
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Notification Email Address Invalid.";
               _view.Validators.Add(err);
            }
            //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Notification Email Address Invalid");
            return false;
         }

         if (!String.IsNullOrEmpty(BaseView.PropertyDetailInfo.AverageRentText) && !IsDecimalValue(BaseView.PropertyDetailInfo.AverageRentText.Replace("$", "")))
         {
            if (_view != null)
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Invalid Value for Average Rent.  This must be a numeric value.";
               _view.Validators.Add(err);
            }
            //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Invalid Value for Average Rent.  This must be a numeric value.");
            return false;
         }

         return true;
      }

      protected virtual bool ValidateBankingDetails(Page _view = null)
      {
         if (!String.IsNullOrEmpty(BaseView.BankingInformation.RentalBankNameAccountText) || !String.IsNullOrEmpty(BaseView.BankingInformation.RentalBankNameRoutingText) ||
             !String.IsNullOrEmpty(BaseView.BankingInformation.RentalBankNameText))
         {
            if (String.IsNullOrEmpty(BaseView.BankingInformation.RentalBankNameAccountText) || String.IsNullOrEmpty(BaseView.BankingInformation.RentalBankNameRoutingText) ||
                String.IsNullOrEmpty(BaseView.BankingInformation.RentalBankNameText))
            {
               if (_view != null)
               {
                  CustomValidator err = new CustomValidator();
                  err.ValidationGroup = "Banking";
                  err.IsValid = false;
                  err.ErrorMessage = "When entering bank account information, all fields for that account are required.";
                  _view.Validators.Add(err);
               }
               //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "When entering bank account information, all fields for that account are required.");
               return false;
            }

            if (!IsNumericWithMaxLength(BaseView.BankingInformation.RentalBankNameAccountText, 15))
            {
               if (_view != null)
               {
                  CustomValidator err = new CustomValidator();
                  err.ValidationGroup = "Banking";
                  err.IsValid = false;
                  err.ErrorMessage = "Bank Account Number must be numeric and no more than fifteen characters in length.";
                  _view.Validators.Add(err);
               }
               //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Bank Account Number must be numeric and no more than fifteen characters in length.");
               return false;
            }

            if (!BaseView.BankingInformation.RentalBankNameRoutingText.IsValidBankRoutingNumber())
            {
               if (_view != null)
               {
                  CustomValidator err = new CustomValidator();
                  err.ValidationGroup = "Banking";
                  err.IsValid = false;
                  err.ErrorMessage = "Please Enter a Valid Routing Number.";
                  _view.Validators.Add(err);
               }
               //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Please Enter a Valid Routing Number");
               return false;
            }
         }
         if (!String.IsNullOrEmpty(BaseView.BankingInformation.FeeBankNameAccountText) || !String.IsNullOrEmpty(BaseView.BankingInformation.FeeBankNameRoutingText) ||
             !String.IsNullOrEmpty(BaseView.BankingInformation.FeeBankNameText))
         {
            if (String.IsNullOrEmpty(BaseView.BankingInformation.FeeBankNameAccountText) || String.IsNullOrEmpty(BaseView.BankingInformation.FeeBankNameRoutingText) ||
                String.IsNullOrEmpty(BaseView.BankingInformation.FeeBankNameText))
            {
               if (_view != null)
               {
                  CustomValidator err = new CustomValidator();
                  err.ValidationGroup = "Banking";
                  err.IsValid = false;
                  err.ErrorMessage = "When entering bank account information, all fields for that account are required.";
                  _view.Validators.Add(err);
               }
               //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "When entering bank account information, all fields for that account are required.");
               return false;
            }

            if (!IsNumericWithMaxLength(BaseView.BankingInformation.FeeBankNameAccountText, 15))
            {
               if (_view != null)
               {
                  CustomValidator err = new CustomValidator();
                  err.ValidationGroup = "Banking";
                  err.IsValid = false;
                  err.ErrorMessage = "Bank Account Number must be numeric and no more than fifteen characters in length.";
                  _view.Validators.Add(err);
               }
               //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Bank Account Number must be numeric and no more than fifteen characters in length.");
               return false;
            }

            if (!BaseView.BankingInformation.FeeBankNameRoutingText.IsValidBankRoutingNumber())
            {
               if (_view != null)
               {
                  CustomValidator err = new CustomValidator();
                  err.ValidationGroup = "Banking";
                  err.IsValid = false;
                  err.ErrorMessage = "Please Enter a Valid Routing Number.";
                  _view.Validators.Add(err);
               }
               //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Please Enter a Valid Routing Number");
               return false;
            }
         }

         if (!String.IsNullOrEmpty(BaseView.BankingInformation.SecurityBankNameAccountText) || !String.IsNullOrEmpty(BaseView.BankingInformation.SecurityBankNameRoutingText) ||
             !String.IsNullOrEmpty(BaseView.BankingInformation.SecurityBankNameText))
         {
            if (String.IsNullOrEmpty(BaseView.BankingInformation.SecurityBankNameAccountText) || String.IsNullOrEmpty(BaseView.BankingInformation.SecurityBankNameRoutingText) ||
                String.IsNullOrEmpty(BaseView.BankingInformation.SecurityBankNameText))
            {
               if (_view != null)
               {
                  CustomValidator err = new CustomValidator();
                  err.ValidationGroup = "Banking";
                  err.IsValid = false;
                  err.ErrorMessage = "When entering bank account information, all fields for that account are required.";
                  _view.Validators.Add(err);
               }
               //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "When entering bank account information, all fields for that account are required.");
               return false;
            }

            if (!IsNumericWithMaxLength(BaseView.BankingInformation.SecurityBankNameAccountText, 15))
            {
               if (_view != null)
               {
                  CustomValidator err = new CustomValidator();
                  err.ValidationGroup = "Banking";
                  err.IsValid = false;
                  err.ErrorMessage = "Bank Account Number must be numeric and no more than fifteen characters in length.";
                  _view.Validators.Add(err);
               }
               //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Bank Account Number must be numeric and no more than fifteen characters in length.");
               return false;
            }

            if (!BaseView.BankingInformation.SecurityBankNameRoutingText.IsValidBankRoutingNumber())
            {
               if (_view != null)
               {
                  CustomValidator err = new CustomValidator();
                  err.ValidationGroup = "Banking";
                  err.IsValid = false;
                  err.ErrorMessage = "Please Enter a Valid Routing Number.";
                  _view.Validators.Add(err);
               }
               //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Please Enter a Valid Routing Number");
               return false;
            }
         }

         if (String.IsNullOrEmpty(BaseView.BankingInformation.RentalBankNameAccountText) && String.IsNullOrEmpty(BaseView.BankingInformation.FeeBankNameAccountText) &&
             String.IsNullOrEmpty(BaseView.BankingInformation.SecurityBankNameAccountText))
         {
            CustomValidator err = new CustomValidator();
            err.ValidationGroup = "Banking";
            err.IsValid = false;
            err.ErrorMessage = "In order to accept Electronic Check Payments, you must enter bank account information, please edit your property prior to allowing residents to use Electronic Checks.";
            _view.Validators.Add(err);
            return false;
            //HttpContext.Current.Response.Write("<script type=\"text/javascript\">alert(\"In order to accept Electronic Check Payments, you must enter bank account information, please edit your property prior to allowing residents to use Electronic Checks.\");</script>");
         }

         return true;
      }

      protected virtual bool ValidatePmsInformation(Page _view = null)
      {
         if (_view == null)
            return false;

         if (BaseView.PmsSelection.PmsDropdownList.SelectedValue == "1")
         {
            if (String.IsNullOrEmpty(BaseView.PmsSelection.YardiControl.UsernameText))
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Yardi Username Required.";
               _view.Validators.Add(err);
               //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Yardi Username Required");
               return false;
            }

            if (String.IsNullOrEmpty(BaseView.PmsSelection.YardiControl.PasswordText))
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Yardi Password Required.";
               _view.Validators.Add(err);
               //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Yardi Password Required");
               return false;
            }

            if (String.IsNullOrEmpty(BaseView.PmsSelection.YardiControl.ServerNameText))
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Yardi Password Required.";
               _view.Validators.Add(err);
               //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Yardi Server Name Required");
               return false;
            }

            if (String.IsNullOrEmpty(BaseView.PmsSelection.YardiControl.DatabaseNameText))
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Yardi Database Name Required.";
               _view.Validators.Add(err);
               //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Yardi Database Name Required");
               return false;
            }

            if (String.IsNullOrEmpty(BaseView.PmsSelection.YardiControl.PlatformText))
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Yardi Platform Required.";
               _view.Validators.Add(err);
               //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Yardi Platform Required");
               return false;
            }

            if (String.IsNullOrEmpty(BaseView.PmsSelection.YardiControl.EndpointText))
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Yardi Url Required.";
               _view.Validators.Add(err);
               //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Yardi Url Required");
               return false;
            }

            if (String.IsNullOrEmpty(BaseView.PmsSelection.YardiControl.PropertyIdText))
            {
               CustomValidator err = new CustomValidator();
               err.ValidationGroup = "PropertySummary";
               err.IsValid = false;
               err.ErrorMessage = "Yardi Property ID Required.";
               _view.Validators.Add(err);
               //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "Yardi Property ID Required");
               return false;
            }
         }

         return true;
      }

      protected virtual bool ValidateProgramInformation(Page _view = null)
      {
         if (BaseView.ProgramInformation.ProgramSelectedValue == "-1")
         {
            CustomValidator err = new CustomValidator();
            err.ValidationGroup = "PropertySummary";
            err.IsValid = false;
            err.ErrorMessage = "A Value for Program Must Be Selected.";
            _view.Validators.Add(err);
            //BaseView.PageMessage.DisplayMessage(SystemMessageType.Error, "A Value for Program Must Be Selected");
            return false;
         }

         return true;
      }
      #endregion

      #region Calculations
      public string CalculateConvenienceFee(string[] values)
      {
         decimal AvgRent;
         var Program = Property.Program.Unassigned;

         if (values.Length < 2)
            return "$0.00";

         var ProgramId = values[1].ToInt32();

         if (ProgramId != -1)
            Program = (Property.Program)ProgramId;

         if (!Decimal.TryParse(values[0].Replace("$", ""), out AvgRent))
            AvgRent = 0.00M;

         return ConvenienceFee.GetConvenienceFeeForProperty(Program, AvgRent).CreditCardFee.ToString("C");
      }
      #endregion

      #region Helper Methods
      protected abstract Property BuildProperty();

      protected virtual Property BuildInitialPmsInformation(Property property)
      {
         var SelectedValue = BaseView.PmsSelection.PmsDropdownList.SelectedValue.ToInt32();

         //Add more cases when other PMS are implemented
         switch (SelectedValue)
         {
            case 1:
               return BuildYardiProperty(property);

            //Salcedo - 7/11/2014 - add MRI
            //case 4:
            //    return BuildMRIProperty(property);

            case 4:
               return BuildMriProperty2(property);

            //cakel: BUGID00316
            case 2:
               return BuildAmsiProperty(property);
            //cakel: 00351    
            case 3:
               return BuildRealPageProperty(property);

            default:
               property = ClearMriInformation(property);
               return ClearYardiInformation(property);
         }
      }


      //cakel: BUGID00316
      protected virtual Property BuildAmsiProperty(Property property)
      {
         property.AmsiUserName = BaseView.PmsSelection.AmsiControl.UsernameText;
         property.AmsiPassword = BaseView.PmsSelection.AmsiControl.PasswordText;
         property.AmsiDatabaseName = BaseView.PmsSelection.AmsiControl.DatabaseNameText;
         property.AmsiClientMerchantID = BaseView.PmsSelection.AmsiControl.ClientMerchantID;
         //cakel: 00551
         property.AmsiAchClientMerchantID = BaseView.PmsSelection.AmsiControl.AchClientMerchantID;
         property.AmsiCashClientMerchantID = BaseView.PmsSelection.AmsiControl.CashClientMerchantID;

         property.AmsiImportEndPoint = BaseView.PmsSelection.AmsiControl.AmsiImportEndPoint;
         property.AmsiExportEndPoint = BaseView.PmsSelection.AmsiControl.AmsiExportEndPoint;
         property.PmsTypeId = 2;

         property.PmsId = BaseView.PmsSelection.AmsiControl.PropertyIdText;

         return property;

      }

      protected virtual Property BuildYardiProperty(Property property)
      {
         property.YardiUserName = BaseView.PmsSelection.YardiControl.UsernameText;
         property.YardiPassword = BaseView.PmsSelection.YardiControl.PasswordText;
         property.YardiServerName = BaseView.PmsSelection.YardiControl.ServerNameText;
         property.YardiDatabaseName = BaseView.PmsSelection.YardiControl.DatabaseNameText;
         property.YardiPlatform = BaseView.PmsSelection.YardiControl.PlatformText;
         property.YardiEndpoint = BaseView.PmsSelection.YardiControl.EndpointText;
         property.PmsTypeId = 1;
         property.PmsId = BaseView.PmsSelection.YardiControl.PropertyIdText;

         //Salcedo - 8/26/2014 - task 0000179
         property.YardiUseDepositDateAPI = BaseView.PmsSelection.YardiControl.YardiDepositDateCheckbox;

         return property;
      }

      //cakel: 00382 Commented out old code to use new code below
      //protected virtual Property BuildMRIProperty(Property property)
      //{
      //    property.MRIEnableFtpUpload = BaseView.PmsSelection.MRIControl.EnableFtpUpload;
      //    property.MRIEnableFtpDownload = BaseView.PmsSelection.MRIControl.EnableFtpDownload;
      //    property.MRIFtpSiteAddress = BaseView.PmsSelection.MRIControl.FTPSiteAddress;
      //    property.MRIFtpPort = BaseView.PmsSelection.MRIControl.FTPPort;
      //    property.MRIFtpAccountUserName = BaseView.PmsSelection.MRIControl.FTPAccountUserName;
      //    property.MRIFtpAccountPassword = BaseView.PmsSelection.MRIControl.FTPAccountPassword;
      //    property.PmsTypeId = 4;
      //    property.PmsId = BaseView.PmsSelection.MRIControl.MRIPropertyIDText;

      //    return property;
      //}

      //cakel: TASK 00382 MRI
      protected virtual Property BuildMriProperty2(Property property)
      {
         property.MRIClientID = BaseView.PmsSelection.MRIControl2.MRIClientID;
         property.MRIDatabaseName = BaseView.PmsSelection.MRIControl2.MRIDatabaseName;
         property.MRIWebServiceUserName = BaseView.PmsSelection.MRIControl2.MRIWebServiceUserName;
         property.MRIPartnerKey = BaseView.PmsSelection.MRIControl2.MRIPartnerKey;
         property.MRIImportEndPoint = BaseView.PmsSelection.MRIControl2.MRIImportEndPoint;
         property.MRIExportEndPoint = BaseView.PmsSelection.MRIControl2.MRIExportEndPoint;
         property.PmsId = BaseView.PropertyDetailInfo.PropertyCodeText.ToString();
         property.PmsTypeId = 4;
         property.MRIWebServicePassword = BaseView.PmsSelection.MRIControl2.MRIWebServicePassword;


         return property;
      }

      //cakel: 00351 RealPage
      protected virtual Property BuildRealPageProperty(Property property)
      {
         property.RealPageUserName = BaseView.PmsSelection.RealPageControl.UsernameText;
         property.RealPagePassword = BaseView.PmsSelection.RealPageControl.PasswordText;
         property.RealPagePMCID = BaseView.PmsSelection.RealPageControl.RealPagePMCID;
         property.RealPageImportURL = BaseView.PmsSelection.RealPageControl.RealPageImportEndPoint;
         property.RealPageExportURL = BaseView.PmsSelection.RealPageControl.RealPageExportEndPoint;
         property.RealPageInternalUser = BaseView.PmsSelection.RealPageControl.RealPageInternalUser;
         property.RealPageSiteID = BaseView.PmsSelection.RealPageControl.RealPageSiteID;
         property.PmsTypeId = 3;
         property.PmsId = BaseView.PmsSelection.RealPageControl.RealPageSiteID;

         return property;
      }


      protected virtual Property ClearYardiInformation(Property property)
      {
         property.PmsId = null;
         property.PmsTypeId = null;
         property.YardiUserName = null;
         property.YardiPassword = null;
         property.YardiServerName = null;
         property.YardiDatabaseName = null;
         property.YardiPlatform = null;
         property.YardiEndpoint = null;

         return property;
      }

      protected virtual Property ClearMriInformation(Property property)
      {
         property.PmsId = null;
         property.PmsTypeId = null;
         property.MRIEnableFtpUpload = false;
         property.MRIEnableFtpDownload = false;
         property.MRIFtpSiteAddress = null;
         property.MRIFtpPort = null;
         property.MRIFtpAccountUserName = null;
         property.MRIFtpAccountPassword = null;

         return property;
      }

      protected virtual Property BuildProgramInformation(Property property)
      {
         var SelectedValue = BaseView.ProgramInformation.ProgramSelectedValue.ToInt32();
         //cakel:10/19/2016 added new programID Air-2
         if (SelectedValue > -1 && SelectedValue < 5)
            property.ProgramId = (Property.Program)SelectedValue;
         else
            property.ProgramId = Property.Program.Unassigned;

         return property;
      }

      protected virtual Property BuildBankingInformation(Property property)
      {
         property.RentalDepositBankName = BaseView.BankingInformation.RentalBankNameText;
         property.RentalDepositBankAccountNumber = BaseView.BankingInformation.RentalBankNameAccountText.Contains("*") ? property.RentalDepositBankAccountNumber : BaseView.BankingInformation.RentalBankNameAccountText;
         property.RentalDepositBankRoutingNumber = BaseView.BankingInformation.RentalBankNameRoutingText;
         property.SecurityDepositBankName = BaseView.BankingInformation.SecurityBankNameText;
         property.SecurityDepositBankAccountNumber = BaseView.BankingInformation.SecurityBankNameAccountText.Contains("*") ? property.SecurityDepositBankAccountNumber : BaseView.BankingInformation.SecurityBankNameAccountText;
         property.SecurityDepositBankRoutingNumber = BaseView.BankingInformation.SecurityBankNameRoutingText;
         property.MonthlyFeeBankName = BaseView.BankingInformation.FeeBankNameText;
         property.MonthlyFeeBankAccountNumber = BaseView.BankingInformation.FeeBankNameAccountText.Contains("*") ? property.MonthlyFeeBankAccountNumber : BaseView.BankingInformation.FeeBankNameAccountText;
         property.MonthlyFeeBankRoutingNumber = BaseView.BankingInformation.FeeBankNameRoutingText;
         //cakel: BUGID00213
         property.CCDepositBankName = BaseView.BankingInformation.CCDepositBankNameText;
         property.CCDepositAccountNumber = BaseView.BankingInformation.CCDepositAccountNumberText.Contains("*") ? property.CCDepositAccountNumber : BaseView.BankingInformation.CCDepositAccountNumberText;
         property.CCDepositRoutingNumber = BaseView.BankingInformation.CCDepositRoutingNumberText;

         //cakel: BUGID00266 - Use the same account number as Rental Deposit
         property.CashDepositBankName = property.RentalDepositBankName;
         property.CashDepositAccountNumber = property.RentalDepositBankAccountNumber;
         property.CashDepositRoutingNumber = property.RentalDepositBankRoutingNumber;


         return property;
      }

      protected static bool IsValidPhoneNumber(string phoneNumber)
      {
         var Value = String.Empty;

         for (var I = 0; I < phoneNumber.Length; I++)
         {
            int Result;

            if (Int32.TryParse(phoneNumber[I].ToString(CultureInfo.InvariantCulture), out Result))
               Value = String.Format("{0}{1}", Value, phoneNumber[I]);
         }

         return Value.Length == 10;
      }

      protected static bool IsNumericWithMaxLength(string value, int maxLength)
      {
         long Result;

         if (value.Contains("*") && value.Length <= 15)
            return true;

         return value.Length <= 15 && Int64.TryParse(value, out Result);
      }

      protected static bool IsDecimalValue(string value)
      {
         decimal Result;
         return Decimal.TryParse(value, out Result);
      }
      #endregion
   }
}
