﻿using EfxFramework.Interfaces;
using EfxFramework.Interfaces.UserControls.Renter;
using EfxFramework.WebPayments;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace EfxFramework.Presenters
{
    internal class EfxAdministratorSinglePayment : SinglePaymentBase<EfxAdministrator>
    {
        private readonly IEfxAdministratorSinglePayment _View;

        public EfxAdministratorSinglePayment(IEfxAdministratorSinglePayment view)
            : base(view)
        {
            _View = view;
        }

        protected override void ConfirmPayment(object sender, EventArgs e)
        {
            if (!ValidatePage())
                return;

            var PaymentInfo = BuildRenterPaymentInfo();
            var PaymentResponse = PaymentRequest.ProcessPayment(PaymentInfo, false);
            SendNotifications(PaymentResponse, PaymentInfo);
            RedirectToThankYou(PaymentResponse, PaymentInfo);
        }

        protected override void RedirectToThankYou(Dictionary<string, string> queryStringParams)
        {
            var QueryString = new StringBuilder();

            foreach (var Kvp in queryStringParams)
            {
                QueryString.AppendFormat("{0}={1}&", Kvp.Key, Kvp.Value);
            }

            //Redirect to Thankyou.aspx
            BaseView.SafeRedirect(String.Format("/TakeAPayment/ThankYou.aspx?{0}", QueryString));
        }

        protected override void SendEmail(int propertyId, string subject, string body, List<MailAddress> addresses)
        {
            if (CurrentUser.PrimaryEmailAddress.IsValidEmailAddress())
                addresses.Add(new MailAddress(CurrentUser.PrimaryEmailAddress));

            //Salcedo - 2/23/2014 - changed call to include staff IsMainContacts
            //BulkMail.BulkMail.SendMailMessage(
            //    propertyId,
            //    "Efx Administrator",
            //    false,
            //    Settings.SendMailUsername,
            //    Settings.SendMailPassword,
            //    new MailAddress(Settings.PaymentEmail),
            //    subject,
            //    body,
            //    addresses
            //    );
            BulkMail.BulkMail.SendMailMessageIncludePropertyEmailContacts(
                propertyId,
                "Efx Administrator",
                EfxSettings.SendMailUsername,
                EfxSettings.SendMailPassword,
                new MailAddress(EfxSettings.PaymentEmail),
                subject,
                body,
                addresses
                );
        }

        protected override Property GetProperty()
        {
            var RenterSelection = _View.StepOneControl.RenterSelectionControl as IEfxAdministratorSiteRenterSelection;

            if (RenterSelection == null)
                return new Property();

            var PropertyId = Int32.Parse(RenterSelection.PropertyDropdownSelectedValue);

            if (PropertyId < 1)
                return new Property();

            var Property = new Property(PropertyId);

            if (Property.PropertyId.HasValue && Property.PropertyId.Value > 0)
                return Property;

            return new Property();
        }
    }
}
