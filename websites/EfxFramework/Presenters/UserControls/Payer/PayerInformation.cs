﻿using EfxFramework.ExtensionMethods;
using EfxFramework.Interfaces.UserControls.Payer;

namespace EfxFramework.Presenters.UserControls.Payer
{
    public class PayerInformation
    {
        private readonly IPayerInformation _View;

        public PayerInformation(IPayerInformation view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            _View.StateDropdown.BindToSortedEnum(typeof (StateProvince));
        }
    }
}
