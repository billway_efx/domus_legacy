﻿using System.Collections.Generic;
using System.Data.SqlClient;
using EfxFramework.ExtensionMethods;
using EfxFramework.Interfaces.UserControls.Users.PropertyStaff;
using EfxFramework.Web;
using EfxFramework.Web.Facade;
using Mb2x.ExtensionMethods;
using System;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using RPO;

namespace EfxFramework.Presenters.UserControls.Users.PropertyStaff
{
    public class AddEditPropertyStaffPresenter
    {
        private readonly IAddEditPropertyStaff _View;

        public AddEditPropertyStaffPresenter(IAddEditPropertyStaff view)
        {
            _View = view;
            _View.StaffListChanged = StaffListChanged;
            _View.EmailAddressChanged = EmailAddressChanged;
            _View.RolesListChanged = RolesListChanged;
            _View.SaveButtonClicked = SaveButtonClicked;

            if (_View.ShowStaffSelection && _View.PropertyStaffId > 0)
                _View.StaffList.SelectedValue = _View.PropertyStaffId.ToString(CultureInfo.InvariantCulture);
        }

        public void InitializeValues()
        {
            _View.StateList.BindToSortedEnum(typeof (StateProvince));
			_View.RolesList.DataSource = Role.GetRolesByRoleTypeId((int)RoleType.PropertyStaff).Where(r => r.RoleId != (int)EfxFramework.RoleName.Accountant);
            _View.RolesList.DataBind();
            _View.PermissionsDataSource = RolePermission.GetRolePermissionsByRoleId(_View.RolesList.SelectedValue.ToInt32());

            if (_View.ShowStaffSelection)
            {
                _View.StaffList.DataSource = EfxFramework.PropertyStaff.GetPropertyStaffListByPropertyId(_View.PropertyId);
                _View.StaffList.DataBind();
            }

            PopulateStaffList();
            ChangeStaffSelection();
        }

        public void PopulateStaffList()
        {
            //var Company = EfxFramework.Company.GetCompanyListByPropertyId(_View.PropertyId).FirstOrDefault();
            //if (Company != null)
            //{
            //    //var Properties = EfxFramework.Property.GetPropertyListByCompanyId(Company.CompanyId);
            //    //var StaffList = new List<EfxFramework.PropertyStaff>();

            //    //foreach (var P in Properties.Where(p => (p.PropertyId.HasValue && p.PropertyId.Value > 0) && p.PropertyId.Value != _View.PropertyId))
            //    //{
            //    //    // ReSharper disable PossibleInvalidOperationException
            //    //    StaffList.AddRange(EfxFramework.PropertyStaff.GetPropertyStaffListByPropertyId(P.PropertyId.Value));
            //    //    // ReSharper restore PossibleInvalidOperationException
            //    //}
                


                
            //}
            using (var entity = new RPOEntities())
            {

                var propertyStaffList = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyId == _View.PropertyId).Select(pps => pps.PropertyStaffId).ToList();
                var staffList = entity.PropertyStaffs.Where(ps => propertyStaffList.Any(propertyStaffId => propertyStaffId == ps.PropertyStaffId)).ToList();
                _View.StaffList.DataSource = staffList;
                _View.StaffList.DataBind();
            }
        }

        public void PopulateFields()
        {
            var Staff = new EfxFramework.PropertyStaff(_View.ShowStaffSelection ? _View.StaffList.SelectedValue.ToInt32() : _View.PropertyStaffId);

            if (!Staff.PropertyStaffId.HasValue || Staff.PropertyStaffId.Value < 1)
                return;

            var Role = EfxFramework.Role.GetRolesByStaffId(Staff.PropertyStaffId.Value).FirstOrDefault();

            _View.FirstNameText = Staff.FirstName;
            _View.MiddleNameText = Staff.MiddleName;
            _View.LastNameText = Staff.LastName;
            _View.Address1Text = Staff.StreetAddress;
            _View.Address2Text = Staff.StreetAddress2;
            _View.CityText = Staff.City;
            _View.StateList.SelectedValue = Staff.StateProvinceId.HasValue ? Staff.StateProvinceId.Value.ToString(CultureInfo.InvariantCulture) : "1";
            _View.ZipCodeText = Staff.PostalCode;
            _View.PrimaryEmailAddressText = Staff.PrimaryEmailAddress;
            _View.OfficePhoneText = Staff.MainPhoneNumber;
            _View.MobilePhoneText = Staff.MobilePhoneNumber;
            _View.FaxNumberText = Staff.FaxNumber;
            _View.IsMainContactChecked = EfxFramework.PropertyStaff.IsPropertyStaffMainContactForProperty(Staff.PropertyStaffId.Value, _View.PropertyId);
            _View.IsGeneralManagerChecked = Staff.IsPropertyGeneralManager;
            _View.UsernameText = Staff.PrimaryEmailAddress;
            _View.RolesList.SelectedValue = Role != null ? Role.RoleId.ToString(CultureInfo.InvariantCulture) : "1";
            _View.PermissionsDataSource = RolePermission.GetRolePermissionsByRoleId(_View.RolesList.SelectedValue.ToInt32());

           }

        private void ClearFields()
        {
            _View.FirstNameText = String.Empty;
            _View.MiddleNameText = String.Empty;
            _View.LastNameText = String.Empty;
            _View.Address1Text = String.Empty;
            _View.Address2Text = String.Empty;
            _View.CityText = String.Empty;
            _View.StateList.SelectedValue = "1";
            _View.ZipCodeText = String.Empty;
            _View.PrimaryEmailAddressText = String.Empty;
            _View.OfficePhoneText = String.Empty;
            _View.MobilePhoneText = String.Empty;
            _View.FaxNumberText = String.Empty;
            _View.IsMainContactChecked = false;
            _View.IsGeneralManagerChecked = false;
            _View.UsernameText = String.Empty;
        }

        private void StaffListChanged(object sender, EventArgs e)
        {
            ChangeStaffSelection();
        }

        private void EmailAddressChanged(object sender, EventArgs e)
        {
            _View.UsernameText = _View.PrimaryEmailAddressText;
        }

        private void RolesListChanged(object sender, EventArgs e)
        {
            _View.PermissionsDataSource = RolePermission.GetRolePermissionsByRoleId(_View.RolesList.SelectedValue.ToInt32());
        }

        private void SaveButtonClicked(object sender, EventArgs e)
        {
            SaveStaff();
        }

        private void ChangeStaffSelection()
        {
            if ((_View.ShowStaffSelection && _View.StaffList.SelectedValue.ToInt32() > 0) || !_View.ShowStaffSelection)
                PopulateFields();
            else
                ClearFields();
        }

        private void SaveStaff()
        {
			_View.ParentPage.Validate("AddEditPS");
            if (!_View.ParentPage.IsValid)
                return;

            var PropertyStaffId = _View.ShowStaffSelection ? _View.StaffList.SelectedValue.ToInt32() : _View.PropertyStaffId;

            try
            {
                var Staff = new EfxFramework.PropertyStaff(PropertyStaffId)
                    {
                        FirstName = _View.FirstNameText,
                        MiddleName = _View.MiddleNameText,
                        LastName = _View.LastNameText,
                        StreetAddress = _View.Address1Text,
                        StreetAddress2 = _View.Address2Text,
                        City = _View.CityText,
                        StateProvinceId = _View.StateList.SelectedValue.ToInt32(),
                        PostalCode = _View.ZipCodeText,
                        PrimaryEmailAddress = _View.PrimaryEmailAddressText,
                        MainPhoneNumber = _View.OfficePhoneText,
                        MobilePhoneNumber = _View.MobilePhoneText,
                        FaxNumber = _View.FaxNumberText,
                        IsPropertyGeneralManager = _View.IsGeneralManagerChecked,
                    };

                if ((!String.IsNullOrEmpty(_View.PasswordText) && !String.IsNullOrEmpty(_View.ConfirmPasswordText)) && _View.PasswordText == _View.ConfirmPasswordText)
                    Staff.SetPassword(_View.PasswordText);

                var StaffId = EfxFramework.PropertyStaff.Set(Staff, UIFacade.GetAuthenticatedUserString(UserType.PropertyStaff));
                EfxFramework.Property.AssignStaffToProperty(StaffId, _View.PropertyId, _View.IsMainContactChecked);
                AddStaffToRole(StaffId);

                //BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Success, "Account Successfully Updated");
				_View.SuccessMessagePlaceHolder.Visible = true;
            }
            catch (SqlException Ex)
            {
				if (Ex.Number == 2601)
				{
					//BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Duplicate Email Address Used");
					CustomValidator err = new CustomValidator();
					err.ValidationGroup = "AddEditPS";
					err.IsValid = false;
					err.ErrorMessage = "Duplicate Email Address Used.";
					_View.ParentPage.Validators.Add(err);
				}
				else
					BasePage.SafePageRedirect("~/ErrorPage.html");
            }
            catch
            {
                BasePage.SafePageRedirect("~/ErrorPage.html");
            }
        }

        private void AddStaffToRole(int propertyStaffId)
        {
            //Delete any existing roles to add new ones.
            var Roles = EfxFramework.Role.GetRolesByStaffId(propertyStaffId);

            foreach (var R in Roles)
            {
                EfxFramework.Role.DeletePropertyStaffRole(propertyStaffId, R.RoleId);
            }
            
            var Role = new PropertyStaffRole
                {
                    PropertyStaffId = propertyStaffId,
                    RoleId = _View.RolesList.SelectedValue.ToInt32()
                };

            PropertyStaffRole.Set(Role);
        }


    }
}
