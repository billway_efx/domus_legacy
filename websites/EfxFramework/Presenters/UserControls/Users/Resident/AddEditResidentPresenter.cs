﻿using System.Data.SqlClient;
using EfxFramework.ExtensionMethods;
using EfxFramework.Interfaces.UserControls.Users.Resident;
using EfxFramework.Web;
using EfxFramework.Web.Facade;
using Mb2x.ExtensionMethods;
using System;
using System.Globalization;
using System.Web.UI.WebControls;

namespace EfxFramework.Presenters.UserControls.Users.Resident
{
    public class AddEditResidentPresenter
    {
        private readonly IAddEditResident _View;

        public AddEditResidentPresenter(IAddEditResident view)
        {
            _View = view;
            _View.PrimaryEmailAddressChanged = PrimaryEmailAddressChanged;
            _View.SaveButtonClicked = SaveButtonClicked;
        }

        public void InitializeValues()
        {
            _View.StateList.BindToSortedEnum(typeof (StateProvince));
            SetPmsVisibility();
            PopulateFields();
        }

        public void PopulateFields()
        {
            if (_View.ResidentId < 1)
                return;

            var Resident = new EfxFramework.Renter(_View.ResidentId);
            _View.FirstNameText = Resident.FirstName;
            _View.MiddleNameText = Resident.MiddleName;
            _View.LastNameText = Resident.LastName;
            _View.Address1Text = Resident.StreetAddress;
            _View.Address2Text = Resident.Unit;
            _View.CityText = Resident.City;
            _View.StateList.SelectedValue = Resident.StateProvinceId.HasValue ? Resident.StateProvinceId.Value.ToString(CultureInfo.InvariantCulture) : "1";
            _View.ZipCodeText = Resident.PostalCode;
            _View.MainPhoneText = Resident.MainPhoneNumber;
            _View.MobilePhoneText = Resident.MobilePhoneNumber;
            _View.FaxNumberText = Resident.FaxNumber;
            _View.PrimaryEmailAddressText = Resident.PrimaryEmailAddress;
            _View.PmsIdText = Resident.PmsId;
            _View.PmsTypeList.SelectedValue = Resident.PmsTypeId.HasValue ? Resident.PmsTypeId.ToString() : "0";
            _View.UsernameText = Resident.PrimaryEmailAddress;


        }

        public void SetPmsVisibility()
        {
            var Property = new EfxFramework.Property(_View.PropertyId);
            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                return;

            _View.PmsPanelVisible = Property.PmsTypeId.HasValue && !String.IsNullOrEmpty(Property.PmsId);
        }

        private void PrimaryEmailAddressChanged(object sender, EventArgs e)
        {
            _View.UsernameText = _View.PrimaryEmailAddressText;
        }

        private void SaveButtonClicked(object sender, EventArgs e)
        {
            SaveResident();
        }

        private void SaveResident()
        {
            _View.ParentPage.Validate();
            if (!_View.ParentPage.IsValid)
                return;

            var Property = new EfxFramework.Property(_View.PropertyId);
            var PropertyPmsId = String.Empty;

            if (Property.PropertyId.HasValue && Property.PropertyId.Value > 0)
                PropertyPmsId = Property.PmsId;

            try
            {
                var Resident = new EfxFramework.Renter(_View.ResidentId)
                    {
                        FirstName = _View.FirstNameText,
                        MiddleName = _View.MiddleNameText,
                        LastName = _View.LastNameText,
                        StreetAddress = _View.Address1Text,
                        Unit = _View.Address2Text,
                        City = _View.CityText,
                        StateProvinceId = _View.StateList.SelectedValue.ToInt32(),
                        PostalCode = _View.ZipCodeText,
                        MainPhoneNumber = _View.MainPhoneText,
                        MobilePhoneNumber = _View.MobilePhoneText,
                        FaxNumber = _View.FaxNumberText,
                        PrimaryEmailAddress = _View.PrimaryEmailAddressText,
                        PmsId = _View.PmsIdText,
                        PmsTypeId = _View.PmsTypeList.SelectedValue.ToInt32() > 0 ? _View.PmsTypeList.SelectedValue.ToInt32() : (int?) null,
                        PmsPropertyId = PropertyPmsId,
                        IsActive = true
                    };

                //Salcedo - 2/25/2015 - task # 00360
                if (Resident.RenterId == 0)
                {
                    var TestResidentId = new EfxFramework.Renter(_View.PrimaryEmailAddressText);
                    if (TestResidentId.RenterId != 0)
                    {
                        CustomValidator err = new CustomValidator();
                        err.ValidationGroup = "AddResident";
                        err.IsValid = false;
                        err.ErrorMessage = "Email Address already used - please try again.";
                        _View.ParentPage.Validators.Add(err);
                        return;
                    }
                }

                if ((!String.IsNullOrEmpty(_View.PasswordText) && !String.IsNullOrEmpty(_View.ConfirmPasswordText)) && _View.PasswordText == _View.ConfirmPasswordText)
                    Resident.SetPassword(_View.PasswordText);

                var ResidentId = EfxFramework.Renter.Set(Resident, UIFacade.GetAuthenticatedUserString(UserType.Renter));
                EfxFramework.Renter.AssignRenterToProperty(_View.PropertyId, ResidentId);

				_View.SuccessMessagePlaceHolder.Visible = true;
                //BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Success, "User Successfully Added");
            }
            catch (SqlException Ex)
            {
				if (Ex.Number == 2601)
				{
					CustomValidator err = new CustomValidator();
					err.ValidationGroup = "AddResident";
					err.IsValid = false;
					err.ErrorMessage = "Duplicate Email Address Used.";
					_View.ParentPage.Validators.Add(err);
				}
				//BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Duplicate Email Address Used");
				else
					BasePage.SafePageRedirect("~/ErrorPage.html");
            }
            catch
            {
                BasePage.SafePageRedirect("~/ErrorPage.html");
            }
        }
    }
}
