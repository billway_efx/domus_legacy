﻿using EfxFramework.Interfaces.UserControls.Users.Admin;
using EfxFramework.Web;
using EfxFramework.Web.Facade;
using Mb2x.ExtensionMethods;
using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace EfxFramework.Presenters.UserControls.Users.Admin
{
    public class AddEditEfxAdministratorPresenter
    {
        private readonly IAddEditEfxAdministrator _View;

        public AddEditEfxAdministratorPresenter(IAddEditEfxAdministrator view)
        {
            _View = view;
            _View.PrimaryEmailAddressChanged = PrimaryEmailAddressChanged;
            _View.RolesListChanged = RolesListChanged;
            _View.SaveButtonClicked = SaveButtonClicked;
        }

        public void InitializeValues()
        {
            _View.RolesList.DataSource = Role.GetRolesByRoleTypeId((int)RoleType.EfxAdministrator);
            _View.RolesList.DataBind();
            _View.PermissionsDataSource = RolePermission.GetRolePermissionsByRoleId(_View.RolesList.SelectedValue.ToInt32());
            PopulateFields();
        }

        public void PopulateFields()
        {
            var Admin = new EfxAdministrator(_View.EfxAdministratorId);

            if (Admin.EfxAdministratorId < 1)
                return;

            var Role = EfxFramework.Role.GetRolesByEfxAdministratorId(_View.EfxAdministratorId).FirstOrDefault();

            _View.FirstNameText = Admin.FirstName;
            _View.LastNameText = Admin.LastName;
            _View.PrimaryEmailAddressText = Admin.PrimaryEmailAddress;
            _View.UsernameText = Admin.PrimaryEmailAddress;
            _View.RolesList.SelectedValue = Role != null ? Role.RoleId.ToString(CultureInfo.InvariantCulture) : "1";
            _View.PermissionsDataSource = RolePermission.GetRolePermissionsByRoleId(_View.RolesList.SelectedValue.ToInt32());
        }

        private void PrimaryEmailAddressChanged(object sender, EventArgs e)
        {
            _View.UsernameText = _View.PrimaryEmailAddressText;
        }

        private void RolesListChanged(object sender, EventArgs e)
        {
            _View.PermissionsDataSource = RolePermission.GetRolePermissionsByRoleId(_View.RolesList.SelectedValue.ToInt32());
        }

        private void SaveButtonClicked(object sender, EventArgs e)
        {
            SaveAdministrator();
        }

        private void SaveAdministrator()
        {
			_View.ParentPage.Validate("AddEfxAdmin");
            if (!_View.ParentPage.IsValid)
                return;

            try
            {
                var Admin = new EfxAdministrator(_View.EfxAdministratorId)
                    {
                        FirstName = _View.FirstNameText,
                        LastName = _View.LastNameText,
                        PrimaryEmailAddress = _View.PrimaryEmailAddressText
                    };

                //Salcedo - 2/25/2015 - task # 00360
                if (Admin.EfxAdministratorId == 0)
                {
                    var AdminDupTest = new EfxAdministrator(_View.PrimaryEmailAddressText);
                    if (AdminDupTest.EfxAdministratorId != 0)
                    {
                        CustomValidator err = new CustomValidator();
                        err.ValidationGroup = "AddEfxAdmin";
                        err.IsValid = false;
                        err.ErrorMessage = "Email Address already used (by RPO Administrator) - please try again.";
                        _View.ParentPage.Validators.Add(err);
                        return;
                    }
                    var TestStaffId = new EfxFramework.PropertyStaff(_View.PrimaryEmailAddressText);
                    if (TestStaffId.PropertyStaffId.HasValue && TestStaffId.PropertyStaffId.Value != 0)
                    {
                        CustomValidator err = new CustomValidator();
                        err.ValidationGroup = "AddEfxAdmin";
                        err.IsValid = false;
                        err.ErrorMessage = "Email Address already used (by Property Staff) - please try again.";
                        _View.ParentPage.Validators.Add(err);
                        return;
                    }
                }

                if ((!String.IsNullOrEmpty(_View.PasswordText) && !String.IsNullOrEmpty(_View.ConfirmPasswordText)) && _View.PasswordText == _View.ConfirmPasswordText)
                    Admin.SetPassword(_View.PasswordText);

                var AdminId = EfxAdministrator.Set(Admin, UIFacade.GetAuthenticatedUserString(UserType.EfxAdministrator));
                AddAdminToRole(AdminId);

                //BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Success, "Account Successfully Updated");
				_View.SuccessMessagePlaceHolder.Visible = true;
				
            }
            catch (SqlException Ex)
            {
				if (Ex.Number == 2601)
				{
					//BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Duplicate Email Address Used");
					CustomValidator err = new CustomValidator();
					err.ValidationGroup = "AddEfxAdmin";
					err.IsValid = false;
					err.ErrorMessage = "Duplicate Email Address Used.";
					_View.ParentPage.Validators.Add(err);
				}
				else
					BasePage.SafePageRedirect("~/ErrorPage.html");
            }
            catch(Exception ex)
            {
				Logging.ExceptionLogger.LogException(ex);
                BasePage.SafePageRedirect("~/ErrorPage.html");
            }
        }

        private void AddAdminToRole(int efxAdministratorId)
        {
            var Roles = EfxFramework.Role.GetRolesByEfxAdministratorId(efxAdministratorId);

            foreach (var R in Roles)
            {
                EfxFramework.Role.DeleteEfxAdministratorRole(efxAdministratorId, R.RoleId);
            }

            var Role = new EfxAdministratorRole
                {
                    EfxAdministratorId = efxAdministratorId,
                    RoleId = _View.RolesList.SelectedValue.ToInt32()
                };

            EfxAdministratorRole.Set(Role);
        }
    }
}
