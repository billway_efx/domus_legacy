﻿using EfxFramework.Interfaces.UserControls.Blogs;
using System;
using EfxFramework.Web;
using EfxFramework.Web.Facade;
using System.Web;

namespace EfxFramework.Presenters.UserControls.Blogs
{
    public class JobPostingPresenter
    {
        private readonly IJobPosting _View;

        public JobPostingPresenter(IJobPosting view)
        {
            _View = view;
            _View.MultiViewChanged = MultiViewChanged;
            _View.AddNewJobClicked = AddNewJobClicked;
            _View.SaveClicked = SaveClicked;
            _View.JobsGridDataSource = JobPosting.GetAllJobPostings();

			_View.JobPostingsView.SetActiveView(_View.JobPostingId > 0 ? _View.JobDetails : _View.JobPostingsView.ActiveViewIndex == -1 ||_View.JobPostingsView.ActiveViewIndex == 0 ? _View.JobsOverview : _View.JobDetails);
        }

        public void InitializeValues()
        {
            if (_View.JobPostingId > 0)
                PopulateFields();
        }

        private void PopulateFields()
        {
            var Job = new JobPosting(_View.JobPostingId);
            if (Job.JobPostingId < 1)
                return;

            _View.JobPostingsView.SetActiveView(_View.JobDetails);
            _View.JobTitleText = Job.JobTitle;
            _View.JobDescriptionText = Job.JobDescription;
            _View.DutiesText = Job.PrincipleDuties;
            _View.SkillsText = Job.SkillsAndAbilities;
            _View.PublishChecked = Job.IsActive;
        }

        private void AddNewJobClicked(object sender, EventArgs e)
        {
            _View.JobPostingsView.SetActiveView(_View.JobDetails);
        }

        private void SaveClicked(object sender, EventArgs e)
        {
            var Job = new JobPosting(_View.JobPostingId)
                {
                    Headline = _View.JobTitleText,
                    DisplaySummary = _View.JobDescriptionText,
                    JobTitle = _View.JobTitleText,
                    JobDescription = _View.JobDescriptionText,
                    PrincipleDuties = _View.DutiesText,
                    SkillsAndAbilities = _View.SkillsText,
                    IsActive = _View.PublishChecked,
                    DatePosted = DateTime.UtcNow,
                    PostedBy = UIFacade.GetCurrentUser(UserType.EfxAdministrator).DisplayName
                };

            JobPosting.Set(Job);
			_View.JobPostingsView.SetActiveView(_View.JobsOverview);
			HttpContext.Current.Response.Redirect("~/renters/community-connect.aspx");
            //BasePage.SafePageRedirect("~/renters/community-connect.aspx#JobPostsTab");
        }

        private void MultiViewChanged(object sender, EventArgs e)
        {
            if (_View.JobPostingId > 0)
                PopulateFields();
        }
    }
}
