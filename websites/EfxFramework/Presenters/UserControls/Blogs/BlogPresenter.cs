﻿using System.Web;
using EfxFramework.Interfaces.UserControls.Blogs;
using EfxFramework.Web;
using EfxFramework.Web.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using Mb2x.ExtensionMethods;
using RPO;

namespace EfxFramework.Presenters.UserControls.Blogs
{
    public class BlogPresenter
    {
        private readonly IBlog _View;

        public BlogPresenter(IBlog view)
        {
            _View = view;
            _View.MultiViewChanged = MultiViewChanged;
            _View.AddNewBlogClicked = AddNewBlogClicked;
            _View.SaveClicked = SaveClicked;
            _View.PreviewClicked = PreviewClicked;
            _View.CancelClicked = CancelClicked;
            _View.BlogsGridDataSource = BuildBlogsGridData();

            _View.BlogsView.SetActiveView(_View.AnnouncementId > 0 && _View.AnnouncementTypeId == (int) _View.TypeOfAnnouncement ? _View.BlogDetails : _View.BlogsOverview);
        }

        public void InitializeValues()
        {
            if (_View.AnnouncementId > 0 && _View.AnnouncementTypeId == (int)_View.TypeOfAnnouncement)
                PopulateFields();
            _View.PreviewLocation = String.Format("~{0}#{1}", HttpContext.Current.Request.RawUrl, GetTabName());
        }

        private void PopulateFields()
        {
            var Blog = new Announcement(_View.AnnouncementId);
            if (Blog.AnnouncementId < 1)
                return;

            _View.BlogsView.SetActiveView(_View.BlogDetails);
            _View.BlogTitleText = Blog.AnnouncementTitle;
            _View.BlogContentText = Blog.AnnouncementText;
            _View.PublishChecked = Blog.IsActive;
        }

        private List<Announcement> BuildBlogsGridData()
        {
            var Blogs = GetBlogs();
            
            foreach (var B in Blogs)
            {
                B.EditUrl = GetEditUrl(B);
                B.DeleteUrl = GetDeleteUrl(B);
            }

            return Blogs;
        }

        private List<Announcement> GetBlogs()
        {
            switch (_View.TypeOfAnnouncement)
            {
                case AnnouncementType.News:
                case AnnouncementType.Events:
                    return Announcement.GetAllAnnouncements().Where(a => a.AnnouncementTypeId == (int)AnnouncementType.News || a.AnnouncementTypeId == (int)AnnouncementType.Events).ToList();
                case AnnouncementType.ProsCorner:
                    return Announcement.GetAllAnnouncements().Where(a => a.AnnouncementTypeId == (int)AnnouncementType.ProsCorner).ToList();
                case AnnouncementType.ResidentsRoom:
                    return Announcement.GetAllAnnouncements().Where(a => a.AnnouncementTypeId == (int)AnnouncementType.ResidentsRoom).ToList();
                default:
                    return new List<Announcement>();
            }
        }

        private string GetEditUrl(Announcement blog)
        {
            switch (_View.TypeOfAnnouncement)
            {
                case AnnouncementType.News:
                case AnnouncementType.Events:
                    return blog.EditNewsAndEventsUrl;
                case AnnouncementType.ProsCorner:
                    return blog.EditProsCornerUrl;
                case AnnouncementType.ResidentsRoom:
                    return blog.EditResidentsRoomUrl;
                default:
                    return String.Empty;
            }
        }

        private string GetDeleteUrl(Announcement blog)
        {
            switch (_View.TypeOfAnnouncement)
            {
                case AnnouncementType.News:
                case AnnouncementType.Events:
                    return blog.DeleteNewsAndEventsUrl;
                case AnnouncementType.ProsCorner:
                    return blog.DeleteProsCornerUrl;
                case AnnouncementType.ResidentsRoom:
                    return blog.DeleteResidentsRoomUrl;
                default:
                    return String.Empty;
            }
        }

        private void MultiViewChanged(object sender, EventArgs e)
        {
            if (_View.AnnouncementId > 0 && _View.AnnouncementTypeId == (int)_View.TypeOfAnnouncement)
                PopulateFields();
        }

        private void AddNewBlogClicked(object sender, EventArgs e)
        {
            BasePage.SafePageRedirect(String.Format("~/renters/community-connect.aspx?AnnouncementId=1000000000&AnnouncementTypeId={0}#{1}", (int)_View.TypeOfAnnouncement, GetTabName()));
        }

        private void SaveClicked(object sender, EventArgs e)
        {
            var Blog = new Announcement(_View.AnnouncementId)
                {
                    AnnouncementTypeId = (int) _View.TypeOfAnnouncement,
                    AnnouncementDate = DateTime.UtcNow,
                    AnnouncementTitle = _View.BlogTitleText,
                    AnnouncementText = _View.BlogContentText,
                    IsActive = _View.PublishChecked,
                    PostedBy = UIFacade.GetCurrentUser(UserType.EfxAdministrator).DisplayName
                };

            Announcement.Set(Blog);
            BasePage.SafePageRedirect(String.Format("~/renters/community-connect.aspx#{0}", GetTabName()));
        }

        private void PreviewClicked(object sender, EventArgs e)
        {
            _View.ParentPage.ClientScript.RegisterStartupScript(_View.ParentPage.GetType(), "Preview", String.Format("ShowEditorHtmlPreview(\"{0}\");", _View.BlogContentText.HtmlEncodeCrLf()), true);
        }

        private void CancelClicked(object sender, EventArgs e)
        {
            BasePage.SafePageRedirect(String.Format("~/renters/community-connect.aspx#{0}", GetTabName()));
        }

        private string GetTabName()
        {
            switch (_View.TypeOfAnnouncement)
            {
                case AnnouncementType.News:
                case AnnouncementType.Events:
                    return "NewsAndEventsTab";
                case AnnouncementType.ResidentsRoom:
                    return "ResidentsRoomTab";
                case AnnouncementType.ProsCorner:
                    return "ProsCornerTab";
                default:
                    return String.Empty;
            }
        }
    }
}
