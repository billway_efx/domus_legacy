﻿using EfxFramework.Interfaces.UserControls.Renter;

namespace EfxFramework.Presenters.UserControls.Renter
{
    public static class RenterSelectionFactory
    {
        public static RenterSelectionBase<EfxFramework.Renter> GetRenterSelection(IRenterSiteRenterSelection view)
        {
            return new RenterSiteRenterSelection(view);
        }

        public static RenterSelectionBase<PropertyStaff> GetRenterSelection(IPropertyStaffSiteRenterSelection view)
        {
            return new PropertyStaffSiteRenterSelection(view);
        }

        public static RenterSelectionBase<EfxAdministrator> GetRenterSelection(IEfxAdministratorSiteRenterSelection view)
        {
            return new EfxAdministratorSiteRenterSelection(view);
        }
    }
}
