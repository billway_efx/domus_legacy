﻿using System;
using System.Web;
using EfxFramework.Interfaces.UserControls.Renter;

namespace EfxFramework.Presenters.UserControls.Renter
{
    public abstract class RenterSelectionBase<T> where T : BaseUser, new()
    {
        protected IRenterSelection BaseView;
        protected BaseUser CurrentUser
        {
            get
            {
                int UserId;
                BaseUser User = new T();

                if (Int32.TryParse(HttpContext.Current.User.Identity.Name, out UserId))
                    User = User.GetUserById(UserId);

                return User;
            }
        }

        protected RenterSelectionBase(IRenterSelection view)
        {
            BaseView = view;
            BaseView.AddRenterClicked = RenterButtonSelected;
        }

        public virtual void InitializeValues()
        {
            
        }

        protected virtual void RenterButtonSelected(object sender, EventArgs e)
        {
            if (BaseView.RenterDropdownSelectedValue != "-1")
            {
                BaseView.RenterInformationPanelVisible = true;
                SetRenterInformation();
            }
            else
            {
                BaseView.RenterInformationPanelVisible = false;
            }
        }

        protected virtual void SetRenterInformation()
        {
            var Renter = new EfxFramework.Renter(Int32.Parse(BaseView.RenterDropdownSelectedValue));

            if (Renter.IsValid)
            {
                BaseView.RenterDisplayNameText = Renter.DisplayName;
                BaseView.RenterMainPhoneNumberText = Renter.MainPhoneNumber;
                BaseView.RenterPrimaryEmailAddressText = Renter.PrimaryEmailAddress;
            }
        }
    }
}
