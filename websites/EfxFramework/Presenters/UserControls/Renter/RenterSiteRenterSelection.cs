﻿using EfxFramework.Interfaces.UserControls.Renter;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EfxFramework.Presenters.UserControls.Renter
{
    internal class RenterSiteRenterSelection : RenterSelectionBase<EfxFramework.Renter>
    {
        private readonly IRenterSiteRenterSelection _View;

        protected EfxFramework.Renter CurrentRenter { get { return CurrentUser as EfxFramework.Renter; } }

        protected EfxFramework.Payer CurrentPayer
        {
            get
            {
                var Payer = new EfxFramework.Payer();

                if (CurrentRenter != null && (CurrentRenter.IsValid && CurrentRenter.PayerId.HasValue))
                    Payer = new EfxFramework.Payer(CurrentRenter.PayerId.Value);

                return Payer;
            }
        }

        protected bool IsRenterPayer
        {
            get
            {
                if (CurrentRenter != null && (CurrentRenter.IsValid && CurrentPayer.PayerId > 0))
                    return CurrentPayer.PrimaryEmailAddress.Equals(CurrentRenter.PrimaryEmailAddress, StringComparison.InvariantCultureIgnoreCase);

                return false;
            }
        }

        public RenterSiteRenterSelection(IRenterSiteRenterSelection view)
            : base(view)
        {
            _View = view;
        }

        public override void InitializeValues()
        {
            var RenterList = new List<EfxFramework.Renter>();

            if (IsRenterPayer)
                RenterList = EfxFramework.Renter.GetRenterListByPayerId(CurrentPayer.PayerId);

            if (CurrentRenter != null && (CurrentRenter.IsValid && RenterList.All(r => r.RenterId != CurrentRenter.RenterId)))
                RenterList.Add(CurrentRenter);

            _View.RenterDropdownDataSource = RenterList;
        }

        protected override void RenterButtonSelected(object sender, EventArgs e)
        {
            if (BaseView.RenterDropdownSelectedValue != "-1")
            {
                _View.RenterIsPayerPanelVisible = false;
                _View.RenterIsNotPayerPanelVisible = true;
            }

            base.RenterButtonSelected(sender, e);
        }

        protected override void SetRenterInformation()
        {
            var Renter = new EfxFramework.Renter(Int32.Parse(BaseView.RenterDropdownSelectedValue));

            if (Renter.IsValid)
                BaseView.RenterImageUrl = String.Format("~/Handlers/RenterHandler.ashx?requestType=0&renterId={0}", Renter.RenterId);

            base.SetRenterInformation();
        }
    }
}
