﻿using System;
using EfxFramework.Interfaces.UserControls.Renter;

namespace EfxFramework.Presenters.UserControls.Renter
{
    internal class PropertyStaffSiteRenterSelection : RenterSelectionBase<PropertyStaff>
    {
        private readonly IPropertyStaffSiteRenterSelection _View;

        protected PropertyStaff CurrentStaff { get { return CurrentUser as PropertyStaff; } }

        public PropertyStaffSiteRenterSelection(IPropertyStaffSiteRenterSelection view)
            : base(view)
        {
            _View = view;
            _View.PropertySelected = PropertySelected;
        }

        public override void InitializeValues()
        {
            if (CurrentStaff != null && CurrentStaff.PropertyStaffId.HasValue)
                _View.PropertyDropdownDataSource = EfxFramework.Property.GetPropertyListByPropertyStaffId(CurrentStaff.PropertyStaffId.Value);
        }

        protected virtual void PropertySelected(object sender, EventArgs e)
        {
            _View.RenterDropdownDataSource = EfxFramework.Renter.GetRenterListByPropertyId(Int32.Parse(_View.PropertyDropdownSelectedValue));
            _View.RenterDropdownListEnabled = true;
        }

        protected override void SetRenterInformation()
        {
            var Renter = new EfxFramework.Renter(Int32.Parse(BaseView.RenterDropdownSelectedValue));

            if (Renter.IsValid)
                BaseView.RenterImageUrl = String.Format("~/Handlers/PropertyManagerHandler.ashx?requestType=0&renterId={0}", Renter.RenterId);

            base.SetRenterInformation();
        }
    }
}
