﻿using System;
using EfxFramework.Interfaces.UserControls.Renter;

namespace EfxFramework.Presenters.UserControls.Renter
{
    internal class EfxAdministratorSiteRenterSelection : RenterSelectionBase<EfxAdministrator>
    {
        private readonly IEfxAdministratorSiteRenterSelection _View;

        public EfxAdministratorSiteRenterSelection(IEfxAdministratorSiteRenterSelection view)
            : base(view)
        {
            _View = view;
            _View.PropertySelected = PropertySelected;
        }

        public override void InitializeValues()
        {
            _View.PropertyDropdownDataSource = EfxFramework.Property.GetAllPropertyList();
        }

        protected virtual void PropertySelected(object sender, EventArgs e)
        {
            _View.RenterDropdownDataSource = EfxFramework.Renter.GetRenterListByPropertyId(Int32.Parse(_View.PropertyDropdownSelectedValue));
            _View.RenterDropdownListEnabled = true;
        }

        protected override void SetRenterInformation()
        {
            var Renter = new EfxFramework.Renter(Int32.Parse(BaseView.RenterDropdownSelectedValue));

            if (Renter.IsValid)
                BaseView.RenterImageUrl = String.Format("~/Handlers/EFXAdminHandler.ashx?requestType=0&renterId={0}", Renter.RenterId);

            base.SetRenterInformation();
        }
    }
}
