﻿using EfxFramework.Interfaces.UserControls.Renter;
using EfxFramework.Web;
using EfxFramework.Web.Facade;
using System;
using System.Web;
namespace EfxFramework.Presenters.UserControls.Renter
{
    public class RenterNotesPresenter
    {
        private readonly IRenterNotes _View;

        public RenterNotesPresenter(IRenterNotes view)
        {
            _View = view;
            _View.SaveButtonClicked = SaveButtonClicked;

            if (_View.IsPropertyManagerSite)
                _View.NotesGridDataSource = RenterNote.GetRenterNoteByRenterIdAndRoleTypeId(_View.RenterId, (int)RoleType.PropertyStaff);
            else
                _View.NotesGridDataSource = RenterNote.GetRenterNoteByRenterId(_View.RenterId);

            if (_View.NoteId > 0)
                PopulateNoteView();
            else
                _View.ResidentNotesVisible = false;
        }

        public void InitializeValues()
        {
            
        }

        private void PopulateNoteView()
        {
            _View.ResidentNotesVisible = true;

            var Note = new RenterNote(_View.NoteId);
            if (Note.RenterNoteId < 1)
                return;

            _View.NoteTextView = Note.Note;
        }

        //CMallory - Task 00566 - Modified function to determine if the logged in user is an EFX Administrator or a Property Staff member and if so, the resident notes can be saved.
        private void SaveButtonClicked(object sender, EventArgs e)
        {
            _View.ParentPage.Validate("Notes");
            if (!_View.ParentPage.IsValid)
                return;

            var RoleTypeId = _View.IsPropertyManagerSite ? (int) RoleType.PropertyStaff : (int) RoleType.EfxAdministrator;
            var User = new PropertyStaff(EfxFramework.Helpers.Helper.CurrentUserId);
            var EFXAdmin = new EfxAdministrator(EfxFramework.Helpers.Helper.CurrentUserId);

            if (User.DisplayName==null || User.DisplayName=="" || User.DisplayName==" ")
            {
                User = null;
            }
            if (User==null && EFXAdmin==null)
            {
                if (_View != null)
                {
                    if (_View.ParentPage != null)
                    {
                        BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Unable to retrive the logged in user");
                    }
                }
                return;
            }

            if (User!=null)
            {
                EfxFramework.Renter.AddNoteForRenter(0, _View.RenterId, _View.NoteTextInput, User.DisplayName, RoleTypeId);
            }

            else if(EFXAdmin!=null)
            {
                EfxFramework.Renter.AddNoteForRenter(0, _View.RenterId, _View.NoteTextInput, EFXAdmin.DisplayName, RoleTypeId);
            }
            

            if (_View.IsPropertyManagerSite)
                _View.NotesGridDataSource = RenterNote.GetRenterNoteByRenterIdAndRoleTypeId(_View.RenterId, (int)RoleType.PropertyStaff);
            else
                _View.NotesGridDataSource = RenterNote.GetRenterNoteByRenterId(_View.RenterId);
        }
    }
}
