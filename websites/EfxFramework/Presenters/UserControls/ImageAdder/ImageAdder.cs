﻿using System.Drawing;
using System.IO;
using EfxFramework.Interfaces.UserControls.ImageAdder;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;

namespace EfxFramework.Presenters.UserControls.ImageAdder
{
    public class ImageAdder
    {
        private readonly IImageAdder _View;
        private List<Photo> _Photos;

        public ImageAdder(IImageAdder view)
        {
            _View = view;
            _View.AddPhotoClicked = AddPhoto;
            _View.ItemCommandClicked = DeletePhoto;
            InitializePhotos();
        }

        public void InitializeValues()
        {
            PopulateCurrentPhotos();
        }

        public void ClearPhotos()
        {
            if (_Photos == null || _Photos.Count < 1)
                return;

            _Photos = new List<Photo>();
            SetImageByteSession();
            _View.PhotoDataSource = _Photos;
            _View.CanAddMorePhotos = _Photos.Count < _View.MaxNumberOfPhotos;
        }

        private void InitializePhotos()
        {
            if (HttpContext.Current.Session[SessionVariable.ImageBytes.ToString()] != null)
                _Photos = (List<Photo>)HttpContext.Current.Session[SessionVariable.ImageBytes.ToString()];
            else
                _Photos = new List<Photo>();

            _View.CanAddMorePhotos = _Photos.Count < _View.MaxNumberOfPhotos;
        }

        private void PopulateCurrentPhotos()
        {
            _Photos = new List<Photo>();
            SetImageByteSession();

            if (_View.CurrentPhotos != null)
            {
                foreach (var P in _View.CurrentPhotos)
                {
                    var Photo = new Photo 
                    {
                        PhotoUrl = string.Format("~/Handlers/PhotoHandler.ashx?RequestType=8&photoNumber={0}", _View.CurrentPhotos.IndexOf(P)), 
                        PhotoBytes = P, 
                        Description = "Photo goes here"
                    };

                    _Photos.Add(Photo);
                }
            }

            SetImageByteSession();
            _View.PhotoDataSource = _Photos;
        }

        private void AddPhoto(object sender, EventArgs e)
        {
            if (_View.FileUploadBytes != null && _Photos.Count < _View.MaxNumberOfPhotos)
            {
                //Test to see if this is an image being uploaded
                try
                {
                    using (var MStream = new MemoryStream(_View.FileUploadBytes))
                    {
                        var Image = new Bitmap(MStream);
                    }
                }
                catch
                {
                    return;
                }

                var Photo = new Photo 
                {
                    PhotoBytes = _View.FileUploadBytes, 
                    Description = "Photo goes here",
                    PhotoUrl = string.Format("~/Handlers/PhotoHandler.ashx?RequestType=8&photoNumber={0}", _Photos.Count)
                };

                _Photos.Add(Photo);
                _View.CanAddMorePhotos = _Photos.Count < _View.MaxNumberOfPhotos;
                SetImageByteSession();
                _View.PhotoDataSource = _Photos;
            }
        }

        private void DeletePhoto(object sender, ListViewCommandEventArgs e)
        {
            if (_Photos.Count > e.Item.DataItemIndex)
                _Photos.RemoveAt(e.Item.DataItemIndex);

            foreach (var P in _Photos)
            {
                P.PhotoUrl = string.Format("~/Handlers/PhotoHandler.ashx?RequestType=8&photoNumber={0}", _Photos.IndexOf(P));
            }

            _View.CanAddMorePhotos = _Photos.Count < _View.MaxNumberOfPhotos;

            SetImageByteSession();
            _View.PhotoDataSource = _Photos;
        }

        private void SetImageByteSession()
        {
            HttpContext.Current.Session.Remove(SessionVariable.ImageBytes.ToString());

            if (_Photos.Count > 0)
                HttpContext.Current.Session.Add(SessionVariable.ImageBytes.ToString(), _Photos);
        }
    }
}
