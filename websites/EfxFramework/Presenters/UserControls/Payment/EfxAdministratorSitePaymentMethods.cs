﻿using EfxFramework.Interfaces.UserControls.Payment;

namespace EfxFramework.Presenters.UserControls.Payment
{
    internal class EfxAdministratorSitePaymentMethods : PaymentMethodsBase
    {
// ReSharper disable NotAccessedField.Local
        private readonly IEfxAdministratorSitePaymentMethods _View;
// ReSharper restore NotAccessedField.Local

        public EfxAdministratorSitePaymentMethods(IEfxAdministratorSitePaymentMethods view)
            : base(view)
        {
            _View = view;
        }
    }
}
