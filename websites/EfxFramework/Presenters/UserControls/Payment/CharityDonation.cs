﻿using EfxFramework.Interfaces.UserControls.Payment;

namespace EfxFramework.Presenters.UserControls.Payment
{
    public class CharityDonation
    {
        private readonly ICharityDonation _View;

        public CharityDonation(ICharityDonation view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            _View.CharityDropdownDataSource = Charity.GetAllCharityList();
        }
    }
}
