﻿using EfxFramework.Interfaces.UserControls.Payment;

namespace EfxFramework.Presenters.UserControls.Payment
{
    internal class RenterSitePaymentMethods : PaymentMethodsBase
    {
// ReSharper disable NotAccessedField.Local
        private readonly IRenterSitePaymentMethods _View;
// ReSharper restore NotAccessedField.Local

        public RenterSitePaymentMethods(IRenterSitePaymentMethods view)
            : base(view)
        {
            _View = view;
        }
    }
}
