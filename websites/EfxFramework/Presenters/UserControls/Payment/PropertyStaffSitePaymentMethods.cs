﻿using EfxFramework.Interfaces.UserControls.Payment;

namespace EfxFramework.Presenters.UserControls.Payment
{
    internal class PropertyStaffSitePaymentMethods : PaymentMethodsBase
    {
// ReSharper disable NotAccessedField.Local
        private readonly IPropertyStaffSitePaymentMethods _View;
// ReSharper restore NotAccessedField.Local

        public PropertyStaffSitePaymentMethods(IPropertyStaffSitePaymentMethods view)
            : base(view)
        {
            _View = view;
        }
    }
}
