﻿using System;
using System.Web.UI;
using EfxFramework.Interfaces.UserControls.Payment;

namespace EfxFramework.Presenters.UserControls.Payment
{
    public abstract class PaymentMethodsBase
    {
        protected readonly IPaymentMethods BaseView;

        protected PaymentMethodsBase(IPaymentMethods view)
        {
            BaseView = view;
            BaseView.UpdatePanelPreRender = RenderUpdatePanel;
            BaseView.PaymentMethodsSelected = PaymentMethodSelected;
        }

        public virtual void InitializeValues()
        {
            BaseView.PaymentMethodsSelectedValue = "echeck";
        }

        protected virtual void RenderUpdatePanel(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(BaseView.PaymentMethodsPanel, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$(EFXClient.WireUpDatePickers);", true);
            ScriptManager.RegisterStartupScript(BaseView.PaymentMethodsPanel, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$(EFXClient.WireUpValidation);", true);
        }

        protected virtual void PaymentMethodSelected(object sender, EventArgs e)
        {
            switch (BaseView.PaymentMethodsSelectedValue)
            {
                case "echeck":
                    BaseView.EcheckPanelVisible = true;
                    BaseView.CreditCardPanelVisible = false;
                    break;

                case "credit":
                    BaseView.EcheckPanelVisible = false;
                    BaseView.CreditCardPanelVisible = true;
                    break;

                default:
                    BaseView.EcheckPanelVisible = true;
                    BaseView.CreditCardPanelVisible = false;
                    break;
            }
        }
    }
}
