﻿using EfxFramework.Interfaces.UserControls.Payment;

namespace EfxFramework.Presenters.UserControls.Payment
{
    public static class PaymentMethodsFactory
    {
        public static PaymentMethodsBase GetPaymentMethods(IEfxAdministratorSitePaymentMethods view)
        {
            return new EfxAdministratorSitePaymentMethods(view);
        }

        public static PaymentMethodsBase GetPaymentMethods(IPropertyStaffSitePaymentMethods view)
        {
            return new PropertyStaffSitePaymentMethods(view);
        }

        public static PaymentMethodsBase GetPaymentMethods(IRenterSitePaymentMethods view)
        {
            return new RenterSitePaymentMethods(view);
        }
    }
}
