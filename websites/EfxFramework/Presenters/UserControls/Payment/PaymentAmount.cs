﻿using EfxFramework.Interfaces.UserControls.Payment;

namespace EfxFramework.Presenters.UserControls.Payment
{
    public class PaymentAmount
    {
// ReSharper disable NotAccessedField.Local
        private readonly IPaymentAmount _View;
// ReSharper restore NotAccessedField.Local

        public PaymentAmount(IPaymentAmount view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            
        }
    }
}
