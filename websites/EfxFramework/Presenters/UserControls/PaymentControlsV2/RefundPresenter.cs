﻿using EfxFramework.ExtensionMethods;
using EfxFramework.Interfaces.UserControls.PaymentControlsV2;
using EfxFramework.PaymentMethods;
using EfxFramework.PaymentMethods.Ach;
using EfxFramework.PublicApi.Payment;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Linq;

namespace EfxFramework.Presenters.UserControls.PaymentControlsV2
{
    public class RefundPresenter
    {
        private readonly IRefund _View;

        public RefundPresenter(IRefund view)
        {
            _View = view;
            _View.SubmitRefundBtn.Click += SubmitRefund;

            if (IsAchProcessing())
                _View.SubmitRefundBtn.Attributes["OnClick"] = "return window.confirm('This payment is still being processed, if the payment does not clear, the property will be responsible for the full amount refunded.')";
        }

        public void IntializeValues()
        {
            var Payment = EfxFramework.Payment.GetPaymentByTransactionId(_View.TransactionId);
            if (Payment.PaymentId < 1)
            {
                BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "No Payment Found");
                return;
            }

            var Resident = new EfxFramework.Renter(Payment.RenterId);
            if (Resident.RenterId < 1)
            {
                BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "No Resident Found");
                return;
            }

            _View.ResidentNameText = Resident.DisplayName;
            _View.TransactionIdText = _View.TransactionId;
            _View.RefundAmountText = CalculatePaymentAmount(Payment).ToString("C");
            _View.ProcessingDateText = Payment.TransactionDateTime.ToShortDateString();
            _View.RefundDateText = DateTime.Now.ToShortDateString();
            _View.PaymentMethodText = ((PaymentType) Payment.PaymentTypeId).GetFriendlyName();
        }

        private static decimal CalculatePaymentAmount(EfxFramework.Payment payment)
        {
            var Result = 0.00M;

            Result += payment.RentAmount.HasValue ? payment.RentAmount.Value : 0.00M;
            Result += payment.OtherAmount1.HasValue && (!String.IsNullOrEmpty(payment.OtherDescription1) && payment.OtherDescription1.ToLower() != "convenience fee") ? payment.OtherAmount1.Value : 0.00M;
            Result += payment.OtherAmount2.HasValue && (!String.IsNullOrEmpty(payment.OtherDescription2) && payment.OtherDescription2.ToLower() != "convenience fee") ? payment.OtherAmount2.Value : 0.00M;
            Result += payment.OtherAmount3.HasValue && (!String.IsNullOrEmpty(payment.OtherDescription3) && payment.OtherDescription3.ToLower() != "convenience fee") ? payment.OtherAmount3.Value : 0.00M;
            Result += payment.OtherAmount4.HasValue && (!String.IsNullOrEmpty(payment.OtherDescription4) && payment.OtherDescription4.ToLower() != "convenience fee") ? payment.OtherAmount4.Value : 0.00M;
            Result += payment.OtherAmount5.HasValue && (!String.IsNullOrEmpty(payment.OtherDescription5) && payment.OtherDescription5.ToLower() != "convenience fee") ? payment.OtherAmount5.Value : 0.00M;
            Result += payment.CharityAmount.HasValue ? payment.CharityAmount.Value : 0.00M;

            return Result;
        }

        private void SubmitRefund(object sender, EventArgs e)
        {
            var Payment = EfxFramework.Payment.GetPaymentByTransactionId(_View.TransactionId);
            if (Payment.PaymentId < 1)
            {
                BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "No Payment Found");
                return;
            }

            var TotalReversedAmounts = TransactionReversal.GetReversalsByInternalTransactionId(_View.TransactionId).Sum(r => r.ReversalAmount);

            var ReversalAmount = _View.RefundAmountText.Replace("$", "").ToNullDecimal();
            if (!ReversalAmount.HasValue || ReversalAmount.Value < 0.01M)
            {
                BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Invalid Refund Amount");
                return;
            }

            if ((TotalReversedAmounts + ReversalAmount.Value) <= BuildPaymentAmount(Payment))
            {
                switch (Payment.PaymentTypeId)
                {
                    case 1:
                        SubmitCreditCardRefund(ReversalAmount.Value);
                        break;
                    case 2:
                        SubmitAchRefund(Payment, ReversalAmount.Value);
                        break;
                }
            }
            else
                BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Cumulative refund amount exceeds original payment made");
        }

        private decimal BuildPaymentAmount(EfxFramework.Payment payment)
        {
            var Sum = payment.RentAmount.HasValue ? payment.RentAmount.Value : 0.00M;

            if (payment.OtherAmount1.HasValue && (!String.IsNullOrEmpty(payment.OtherDescription1) && !payment.OtherDescription1.StartsWith("convenience fee", StringComparison.OrdinalIgnoreCase)))
                Sum += payment.OtherAmount1.Value;
            if (payment.OtherAmount2.HasValue && (!String.IsNullOrEmpty(payment.OtherDescription2) && !payment.OtherDescription2.StartsWith("convenience fee", StringComparison.OrdinalIgnoreCase)))
                Sum += payment.OtherAmount2.Value;
            if (payment.OtherAmount3.HasValue && (!String.IsNullOrEmpty(payment.OtherDescription3) && !payment.OtherDescription3.StartsWith("convenience fee", StringComparison.OrdinalIgnoreCase)))
                Sum += payment.OtherAmount3.Value;
            if (payment.OtherAmount4.HasValue && (!String.IsNullOrEmpty(payment.OtherDescription4) && !payment.OtherDescription4.StartsWith("convenience fee", StringComparison.OrdinalIgnoreCase)))
                Sum += payment.OtherAmount4.Value;
            if (payment.OtherAmount5.HasValue && (!String.IsNullOrEmpty(payment.OtherDescription5) && !payment.OtherDescription5.StartsWith("convenience fee", StringComparison.OrdinalIgnoreCase)))
                Sum += payment.OtherAmount5.Value;

            return Sum;
        }

        private bool IsAchProcessing()
        {
            var Payment = EfxFramework.Payment.GetPaymentByTransactionId(_View.TransactionId);
            if (Payment.PaymentId < 1)
            {
                BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "No Payment Found");
                return false;
            }

            var Resident = new EfxFramework.Renter(Payment.RenterId);
            if (Resident.RenterId < 1)
            {
                BasePage.SafePageRedirect("~/PaymentFailed.aspx");
                return false;
            }

            var Property = EfxFramework.Property.GetPropertyByRenterId(Resident.RenterId);
            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                BasePage.SafePageRedirect("~/PaymentFailed.aspx");
                return false;
            }

            var Transaction = EfxFramework.Transaction.GetTransactionByInternalTransactionId(_View.TransactionId);
            if (Transaction.TransactionId < 1)
                Transaction = Transaction.GetTransactionByExternalTransactionId(_View.TransactionId);

            var TransactionId = Transaction.TransactionId < 1 ? Payment.TransactionId : Transaction.ExternalTransactionId;

            var Status = AchService.GetAchStatus(Property.RentalAccountAchClientId, TransactionId);

            if (Status.StatusCode.HasValue && Status.StatusCode.Value == 3)
                return true;

            return false;
        }

        //todo implement using code found below
        //ACH Status of Pending == User sees 'Processing'  == You can void it
        //ACH Status of Working/In Process == User sees 'Processed'  == You can refund
        //ACH Status of Cleared == User sees 'Cleared' == You can't do either/or
        //ACH Status of all others == don't do anything
        //this needs to link to a parent transaction
        private void VoidAchTransaction(EfxFramework.Payment payment)
        {
            var Resident = new EfxFramework.Renter(payment.RenterId);
            if (Resident.RenterId < 1)
            {
                BasePage.SafePageRedirect("~/PaymentFailed.aspx");
                return;
            }

            var Property = EfxFramework.Property.GetPropertyByRenterId(Resident.RenterId);
            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                BasePage.SafePageRedirect("~/PaymentFailed.aspx");
                return;
            }

            var Transaction = EfxFramework.Transaction.GetTransactionByInternalTransactionId(_View.TransactionId);
            if (Transaction.TransactionId < 1)
                Transaction = Transaction.GetTransactionByExternalTransactionId(_View.TransactionId);

            var TransactionId = Transaction.TransactionId < 1 ? payment.TransactionId : Transaction.ExternalTransactionId;

            var Status = AchService.GetAchStatus(Property.RentalAccountAchClientId, TransactionId);
            if (Status.Result == GeneralResponseResult.Success && (Status.StatusCode.HasValue && Status.StatusCode.Value == 1))
            {
                var CancelResult = AchService.CancelPendingAchTransaction(Property.RentalAccountAchClientId, TransactionId);

                if (CancelResult.Result != GeneralResponseResult.Success)
                {
                    BasePage.SafePageRedirect("~/PaymentFailed.aspx");
                    return;
                }
                else
                {
                    //create a new void record
                    var SubTransaction = new Transaction
                    {
                        InternalTransactionId = Transaction.GetNewInternalTransactionId(),
                        ExternalTransactionId = CancelResult.TransactionId,
                        PaymentTypeId = (int)payment.PaymentTypeId,
                        FirstName = Transaction.FirstName,
                        LastName = Transaction.LastName,
                        EmailAddress = Transaction.EmailAddress,
                        StreetAddress = Transaction.StreetAddress,
                        StreetAddress2 = Transaction.StreetAddress2,
                        City = Transaction.City,
                        StateProvinceId = Transaction.StateProvinceId,
                        PostalCode = Transaction.PostalCode,
                        PhoneNumber = Transaction.PhoneNumber,
                        BankAccountNumber = Transaction.BankAccountNumber,
                        BankRoutingNumber = Transaction.BankRoutingNumber,
                        BankAccountTypeId = Transaction.BankAccountTypeId,
                        PaymentAmount = 0,
                        PaymentId = null,
                        PaymentStatusId = CancelResult.Result == GeneralResponseResult.Success ? (int)PaymentStatus.Processing : (int)PaymentStatus.Declined,
                        ResponseResult = (int)CancelResult.Result,
                        ResponseMessage = "VOID",
                        TransactionDate = DateTime.UtcNow,
                        PropertyId = Property.PropertyId.HasValue ? Property.PropertyId.Value : 0
                    };
                    EfxFramework.Transaction.SetRPOTransaction(SubTransaction, Transaction.InternalTransactionId);
                }
            }
        }

        /// <summary>
        /// Reverses a previous ACH transation by submitting a new ACH payment with a negative amount.  
        /// Creates a new transaction record with a link to it's parent transaction.
        /// </summary>
        /// <param name="payment"></param>
        /// <param name="reversalAmount">Should be submitted as a negative, but will convert to negative if not.</param>
        private void SubmitAchRefund(EfxFramework.Payment payment, decimal reversalAmount)
        {
            AchPaymentResponse AchResponse = null;
            if (reversalAmount > 0)
                reversalAmount *= -1;

            var Resident = new EfxFramework.Renter(payment.RenterId);
            if (Resident.RenterId < 1)
            {
                BasePage.SafePageRedirect("~/PaymentFailed.aspx");
                return;
            }

            var Property = EfxFramework.Property.GetPropertyByRenterId(Resident.RenterId);
            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                BasePage.SafePageRedirect("~/PaymentFailed.aspx");
                return;
            }

            var Transaction = EfxFramework.Transaction.GetTransactionByInternalTransactionId(_View.TransactionId);
            if (Transaction.TransactionId < 1)
                Transaction = Transaction.GetTransactionByExternalTransactionId(_View.TransactionId);

            var TransactionId = Transaction.TransactionId < 1 ? payment.TransactionId : Transaction.ExternalTransactionId;

            var State = Resident.StateProvinceId.HasValue ? ((StateProvince) Resident.StateProvinceId.Value).ToString() : "AK";
            var Status = AchService.GetAchStatus(Property.RentalAccountAchClientId, TransactionId);

            if ((Status.Result == GeneralResponseResult.Success && Status.StatusCode.HasValue) && (Status.StatusCode.Value == 2 || Status.StatusCode.Value == 3))
            {
                AchResponse = AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword, Property.RentalAccountAchClientId, reversalAmount, payment.BankRoutingNumber,
                    payment.BankAccountNumber, "CHECKING", Resident.FirstName, Resident.LastName, Resident.StreetAddress, Resident.Unit, Resident.City, State, Resident.PostalCode,
                    Resident.MainPhoneNumber);
                
                if (AchResponse.Result != GeneralResponseResult.Success)
                {
                    BasePage.SafePageRedirect("~/PaymentFailed.aspx");
                    return;
                }
            }
            else
            {
                BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Unable to process refund, please contact support.");
                return;
            }

            var Reversal = new TransactionReversal
                {
                    InternalTransactionId = _View.TransactionId,
                    ReversalAmount = reversalAmount,
                    ReversalDate = DateTime.UtcNow
                };

            var TransactionReversalId = TransactionReversal.Set(Reversal);

            var SubTransaction = new Transaction
                {
                    InternalTransactionId = Transaction.GetNewInternalTransactionId(),
                    ExternalTransactionId = AchResponse.TransactionId,
                    PaymentTypeId = (int)payment.PaymentTypeId,
                    FirstName = Transaction.FirstName,
                    LastName = Transaction.LastName,
                    EmailAddress = Transaction.EmailAddress,
                    StreetAddress = Transaction.StreetAddress,
                    StreetAddress2 = Transaction.StreetAddress2,
                    City = Transaction.City,
                    StateProvinceId = Transaction.StateProvinceId,
                    PostalCode = Transaction.PostalCode,
                    PhoneNumber = Transaction.PhoneNumber,
                    BankAccountNumber = Transaction.BankAccountNumber,
                    BankRoutingNumber = Transaction.BankRoutingNumber,
                    BankAccountTypeId = Transaction.BankAccountTypeId,
                    PaymentAmount = reversalAmount,
                    PaymentId = null,
                    PaymentStatusId = AchResponse.Result == GeneralResponseResult.Success ? (int)PaymentStatus.Processing : (int)PaymentStatus.Declined,
                    ResponseResult = (int)AchResponse.Result,
                    ResponseMessage = AchResponse.ResponseDescription,
                    TransactionDate = DateTime.UtcNow,
                    PropertyId = Property.PropertyId.HasValue ?  Property.PropertyId.Value : 0
                };
            EfxFramework.Transaction.SetRPOTransaction(SubTransaction, Transaction.InternalTransactionId);
            BasePage.SafePageRedirect(String.Format("~/RefundReceipt.aspx?TransactionReversalId={0}", TransactionReversalId));
        }

        private void SubmitCreditCardRefund(decimal reversalAmount)
        {
            var Transaction = EfxFramework.Transaction.GetTransactionByInternalTransactionId(_View.TransactionId);
            if (Transaction.TransactionId < 1 || String.IsNullOrEmpty(Transaction.ExternalTransactionId))
                BasePage.SafePageRedirect("~/PaymentFailed.aspx");

            var Result = PaymentMethods.CreditCard.CreditCardPayment.ReverseCreditCardPayment(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, Transaction.ExternalTransactionId, reversalAmount);

            if (Result.Result != GeneralResponseResult.Success)
            {
                BasePage.SafePageRedirect("~/PaymentFailed.aspx");
                return;
            }

            var Reversal = new TransactionReversal
            {
                InternalTransactionId = _View.TransactionId,
                ReversalAmount = reversalAmount,
                ReversalDate = DateTime.UtcNow
            };

            var TransactionReversalId = TransactionReversal.Set(Reversal);
            BasePage.SafePageRedirect(String.Format("~/RefundReceipt.aspx?TransactionReversalId={0}", TransactionReversalId));
        }
    }
}
