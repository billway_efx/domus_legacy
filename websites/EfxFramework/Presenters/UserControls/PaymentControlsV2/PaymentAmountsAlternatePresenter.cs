﻿using EfxFramework.Interfaces.UserControls.PaymentControlsV2;

namespace EfxFramework.Presenters.UserControls.PaymentControlsV2
{
    public class PaymentAmountsAlternatePresenter
    {
        private readonly IPaymentAmountsAlternate _View;

        private Lease Lease
        {
            get { return Lease.GetRenterLeaseByRenterId(_View.RenterId); }
        }

        private EfxFramework.Property Property
        {
            get { return EfxFramework.Property.GetPropertyByRenterId(_View.RenterId); }
        }

        public PaymentAmountsAlternatePresenter(IPaymentAmountsAlternate view)
        {
            _View = view;
        }
    }
}
