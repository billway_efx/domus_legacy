﻿using System.Web;
using EfxFramework.ExtensionMethods;
using EfxFramework.Interfaces.UserControls.PaymentControlsV2;
using EfxFramework.Web;
using EfxFramework.Web.Facade;
using Mb2x.ExtensionMethods;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.Presenters.UserControls.PaymentControlsV2
{
    public class PaymentInfoTabPresenter
    {


        private readonly IPaymentInfoTab _View;

        //cakel: TASK 00502
        private static EfxFramework.Renter Resident { get; set; }

        private static int ResidentId
        {
            get
            {
                var Id = HttpContext.Current.Request.QueryString["renterId"].ToNullInt32();
                if (!Id.HasValue || Id.Value < 1)
                {
                    BasePage.SafePageRedirect("~/Renters/Default.aspx");
                    return 0;
                }

                return Id.Value;
            }
        }

        private static EfxFramework.Payer Payer
        {
            get
            {
                //cakel: task 00502 v2
                var id = ResidentId;
                var Renter = new EfxFramework.Renter(id);

                var P = new EfxFramework.Payer(Renter.PayerId.HasValue ? Renter.PayerId.Value : 0);
                return P.PayerId < 1 ? new EfxFramework.Payer() : P;
            }
        }

        public PaymentInfoTabPresenter(IPaymentInfoTab view)
        {
            Resident = new EfxFramework.Renter(ResidentId);

            _View = view;
            _View.SaveButtonClicked = SavePaymentMethods;
            _View.PaymentSuccessPlaceHolder.Visible = false;
        }

        public void InitializeValues()
        {

        }

        private void SavePaymentMethods(object sender, EventArgs e)
        {
            try
            {
                _View.ParentPage.Validate("Payment");
                if (!_View.ParentPage.IsValid)
                    return;

                var CreditCard = _View.SelectPaymentTypeControl.PaymentTypeSelectedValue == "1" ? SaveCreditCardInfo() : null;
                var Echeck = _View.SelectPaymentTypeControl.PaymentTypeSelectedValue == "2" ? SaveEcheckInfo() : null;
                SavePayerInfo();
                SaveBillingAddress();

                if (CreditCard == 0 || Echeck == 0)
                    return;

                if (_View.TextToPayTileControl.IsOn)
                    SaveTextToPay();
                else
                    TurnOffTextToPay();

                if (_View.AutoPayTileControl.IsOn)
                {
                    SaveAutoPay(CreditCard, Echeck);
                    ScriptManager.RegisterStartupScript(_View.ParentPage, _View.ParentPage.GetType(), "test", String.Format("<script type='text/javascript'>alert(\"{0}\");</script>", String.Format("AutoPay setup has been completed. Your payments will automatically process on the {0} of each month", _View.AutoPayTileControl.DayOfTheMonthDropdown.SelectedValue.ToNthValue())), false);
                }
                else
                    TurnOffAutoPay();

                _View.PaymentSuccessPlaceHolder.Visible = true;


            }

            catch
            {
                CustomValidator err = new CustomValidator();
                err.ValidationGroup = "Payment";
                err.IsValid = false;
                err.ErrorMessage = "Failed to update payer information";
                _View.ParentPage.Validators.Add(err);
            }
        }

        private void TurnOffAutoPay()
        {
            //cakel: TASK 00502
            // var AutoPay = AutoPayment.GetAutoPaymentByRenterId(Resident.RenterId);
            var AutoPay = AutoPayment.GetAutoPaymentByRenterId(ResidentId);
            if (AutoPay.AutoPaymentId < 1)
                return;

            //TODO: When AutoPayment has an Active bit, just set it to false
            AutoPayment.DeleteAutoPayment(AutoPay.AutoPaymentId);

            //dwillis Task 00076
            var Log = new RPO.ActivityLog();

            //cakel: TASK 00502
            //Log.WriteLog(_View.ParentPage.Page.User.Identity.Name, "AutoPay was disabled", 0, Resident.RenterId.ToString(), true);
            Log.WriteLog(_View.ParentPage.Page.User.Identity.Name, "AutoPay was disabled", 0, ResidentId.ToString(), true);

        }

        private void TurnOffTextToPay()
        {
            //cakel: TASK 00502
            Resident = new EfxFramework.Renter(ResidentId);

            //Salcedo - 11/8/2014 - task 00076
            if (Resident.SmsPaymentPhoneNumber != "" ||
                Resident.SmsPaymentTypeId != null ||
                Resident.SmsReminderDayOfMonth != null
                )
            {
                Resident.SmsPaymentPhoneNumber = null;
                Resident.SmsPaymentTypeId = null;
                Resident.SmsReminderDayOfMonth = null;
                EfxFramework.Renter.Set(Resident, UIFacade.GetAuthenticatedUserString(UserType.Renter));

                //dwillis Task 00076
                var Log = new RPO.ActivityLog();

                //cakel: TASK 00502 v2
                Log.WriteLog(_View.ParentPage.Page.User.Identity.Name, "Text To Pay was disabled", 0, ResidentId.ToString(), true);

            }
        }

        private void SavePayerInfo()
        {
            //cakel: TASK 00502
            Resident = new EfxFramework.Renter(ResidentId);

            //Salcedo - 11/8/2014 - task 00076
            if (
                Resident.FirstName != _View.PersonalInformationControl.FirstNameText ||
                Resident.MiddleName != _View.PersonalInformationControl.MiddleNameText ||
                Resident.LastName != _View.PersonalInformationControl.LastNameText ||
                Resident.Suffix != _View.PersonalInformationControl.SuffixText
            )
            {
                Resident.FirstName = _View.PersonalInformationControl.FirstNameText;
                Resident.MiddleName = _View.PersonalInformationControl.MiddleNameText;
                Resident.LastName = _View.PersonalInformationControl.LastNameText;
                Resident.Suffix = _View.PersonalInformationControl.SuffixText;

                Resident = new EfxFramework.Renter(EfxFramework.Renter.Set(Resident, _View.IsPropertyManagerSite ? UIFacade.GetAuthenticatedUserString(UserType.PropertyStaff) : UIFacade.GetAuthenticatedUserString(UserType.EfxAdministrator)));

                //dwillis 00076
                var Log = new RPO.ActivityLog();
                //cakel: TASK 00502 v2
                Log.WriteLog(_View.ParentPage.Page.User.Identity.Name, "Payer Info was saved", 0, ResidentId.ToString(), true);
            }
        }

        private int? SaveCreditCardInfo()
        {
            if (String.IsNullOrEmpty(_View.SelectPaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText) || String.IsNullOrEmpty(_View.SelectPaymentTypeControl.CreditCardDetailsControl.NameOnCardText))
            {
                BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Missing Credit Card Details");
                return 0;
            }

            var CreditCard = PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(Payer.PayerId);
            var CardNumber = _View.SelectPaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText;

            CreditCard.PayerId = Payer.PayerId;
            CreditCard.CreditCardHolderName = _View.SelectPaymentTypeControl.CreditCardDetailsControl.NameOnCardText;
            CreditCard.CreditCardAccountNumber = !CardNumber.Contains("*") ? CardNumber : CreditCard.CreditCardAccountNumber;
            CreditCard.CreditCardExpirationMonth = _View.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationMonthSelectedValue.ToInt32();
            CreditCard.CreditCardExpirationYear = _View.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationYearList.SelectedValue.ToInt32();
            CreditCard.IsPrimary = true;
            CreditCard.ReminderSent = false;

            return PayerCreditCard.Set(CreditCard);
        }

        private int? SaveEcheckInfo()
        {
            if (String.IsNullOrEmpty(_View.SelectPaymentTypeControl.ECheckDetailsControl.AccountNumberText) || String.IsNullOrEmpty(_View.SelectPaymentTypeControl.ECheckDetailsControl.RoutingNumberText))
            {
                BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Missing ACH Details");
                return 0;
            }

            var Echeck = PayerAch.GetPrimaryPayerAchByPayerId(Payer.PayerId);
            var AccountNumber = _View.SelectPaymentTypeControl.ECheckDetailsControl.AccountNumberText;

            Echeck.PayerId = Payer.PayerId;

            Echeck.BankAccountNumber = !AccountNumber.Contains("*") ? AccountNumber : Echeck.BankAccountNumber;
            Echeck.BankRoutingNumber = _View.SelectPaymentTypeControl.ECheckDetailsControl.RoutingNumberText;
            Echeck.IsPrimary = true;

            //dwillis Task 00076
            var Log = new RPO.ActivityLog();
            //cakel: TASK 00502 v2
            Log.WriteLog(_View.ParentPage.Page.User.Identity.Name, "eCheck info was saved", 0, ResidentId.ToString(), true);

            return PayerAch.Set(Echeck);
        }

        private void SaveBillingAddress()
        {
            var BillingInfo = Address.GetAddressByRenterIdAndAddressType(ResidentId, (int)TypeOfAddress.Billing);

            //Salcedo Task 00076
            if (
                BillingInfo.AddressLine1 != _View.ResidentBillingInformationControl.BillingAddressText ||
                BillingInfo.City != _View.ResidentBillingInformationControl.CityText ||
                BillingInfo.StateProvinceId != _View.ResidentBillingInformationControl.StateDropDown.SelectedValue.ToInt32() ||
                BillingInfo.PostalCode != _View.ResidentBillingInformationControl.ZipText ||
                BillingInfo.PrimaryEmailAddress != _View.PersonalInformationControl.EmailAddressText ||
                BillingInfo.PrimaryPhoneNumber != _View.PersonalInformationControl.PhoneNumberText ||
                BillingInfo.AddressTypeId != (int)TypeOfAddress.Billing
                )
            {
                BillingInfo.AddressLine1 = _View.ResidentBillingInformationControl.BillingAddressText;
                BillingInfo.City = _View.ResidentBillingInformationControl.CityText;
                BillingInfo.StateProvinceId = _View.ResidentBillingInformationControl.StateDropDown.SelectedValue.ToInt32();
                BillingInfo.PostalCode = _View.ResidentBillingInformationControl.ZipText;
                BillingInfo.PrimaryEmailAddress = _View.PersonalInformationControl.EmailAddressText;
                BillingInfo.PrimaryPhoneNumber = _View.PersonalInformationControl.PhoneNumberText;
                BillingInfo.AddressTypeId = (int)TypeOfAddress.Billing;

                var AddressId = Address.Set(BillingInfo);
                Address.AssignAddressToRenter(ResidentId, AddressId);

                //Salcedo Task 00076
                var Log = new RPO.ActivityLog();
                Log.WriteLog(_View.ParentPage.Page.User.Identity.Name, "Address has been updated (Payment Info)", 0, ResidentId.ToString(), true);
            }
        }

        private void SaveAutoPay(int? payerCreditCardId, int? payerAchId)
        {
            var AutoPay = AutoPayment.GetAutoPaymentByRenterId(ResidentId);

            AutoPay.PaymentDayOfMonth = _View.AutoPayTileControl.DayOfTheMonthDropdown.SelectedValue.ToInt32();
            AutoPay.PaymentTypeId = _View.AutoPayTileControl.PaymentTypeDropdown.SelectedValue.ToInt32();
            AutoPay.RenterId = Resident.RenterId;
            AutoPay.PayerAchId = payerAchId;
            AutoPay.PayerCreditCardId = payerCreditCardId;
            AutoPay.PayerId = Payer.PayerId;
            AutoPay.RentAmount = _View.PaymentAmountsControl.RentAmount;
            AutoPay.RentAmountDescription = "Rent And Fees";
            AutoPay.OtherAmount2 = _View.PaymentAmountsControl.ConvenienceFeeAmount;
            AutoPay.OtherDescription2 = "Convenience Fee";

            AutoPayment.Set(AutoPay);

            //Salcedo Task 00076
            var Log = new RPO.ActivityLog();
            //cakel: BUGID00364 - Updated code to show on admin portal
            //Log.WriteLog(_View.ParentPage.Page.User.Identity.Name, "AutoPay information has been saved and (re)enabled.", 0, Resident.RenterId.ToString(), true);
            Log.WriteLog(_View.ParentPage.Page.User.Identity.Name, "AutoPay information has been saved and (re)enabled.", 1, Resident.RenterId.ToString(), true);




        }

        private void SaveTextToPay()
        {
            Resident.SmsPaymentPhoneNumber = _View.TextToPayTileControl.MobileNumberText;
            Resident.SmsPaymentTypeId = _View.TextToPayTileControl.PaymentTypeDropdown.SelectedValue.ToInt32();
            Resident.SmsReminderDayOfMonth = _View.TextToPayTileControl.DayOfTheMonthDropdown.SelectedValue.ToInt32();
            EfxFramework.Renter.Set(Resident, UIFacade.GetAuthenticatedUserString(UserType.Renter));

            //dwillis Task 00076
            var Log = new RPO.ActivityLog();
            //cakel: BUGID00364 - Updated code to show on admin portal
            //Log.WriteLog(_View.ParentPage.Page.User.Identity.Name, "Text To Pay information has been updated/saved", 0, Resident.RenterId.ToString(), true);
            //cakel: TASK 00502 v2.1
            Log.WriteLog(_View.ParentPage.Page.User.Identity.Name, "Text To Pay information has been updated/saved", 1, ResidentId.ToString(), true);
        }
    }
}
