﻿using EfxFramework.Interfaces.UserControls;
using System;
using System.Linq;

namespace EfxFramework.Presenters.UserControls
{
    public class ConfirmationCard
    {
        private readonly IConfirmationCard _View;

        public ConfirmationCard(IConfirmationCard view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            BindDataSource();
        }

        public void DataBind()
        {
            BindDataSource();
        }

        private void BindDataSource()
        {
            if (_View.DataSource != null && _View.DataSource.Count > 0)
                _View.ConfirmationTextHtml = _View.DataSource.Aggregate(String.Empty, (current, info) => String.Format("{0}<p>{1}</p>", current, info));
        }
    }
}
