﻿using System;
using System.Linq;
using EfxFramework.Interfaces.UserControls;

namespace EfxFramework.Presenters.UserControls
{
    public class DetailTile
    {
        private readonly IDetailTile _View;

        public DetailTile(IDetailTile view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            BindDataSource();
        }

        public void DataBind()
        {
            BindDataSource();
        }

        private void BindDataSource()
        {
            if (_View.DataSource != null && _View.DataSource.Count > 0)
                _View.MainDetailHtml = _View.DataSource.Aggregate(String.Empty, (current, info) => String.Format("{0}<p class='padding-bottom'>{1}</p>", current, info));
        }
    }
}
