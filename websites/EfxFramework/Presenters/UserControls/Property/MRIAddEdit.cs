﻿using EfxFramework.Interfaces.UserControls.Property;

namespace EfxFramework.Presenters.UserControls.Property
{
    //Salcedo - 7/11/2014 - new class for MRI integration
    public class MRIAddEdit
    {
        private readonly IMRIAddEdit _View;

        public MRIAddEdit(IMRIAddEdit view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            InitializeValues(new EfxFramework.Property(_View.PropertyId));
        }

        public void InitializeValues(EfxFramework.Property property)
        {
            if (property.PropertyId.HasValue)
            {
                _View.EnableFtpUpload = property.MRIEnableFtpUpload;
                _View.EnableFtpDownload = property.MRIEnableFtpDownload;
                _View.MRIPropertyIDText = property.PmsId;
                _View.FTPSiteAddress = property.MRIFtpSiteAddress;
                _View.FTPPort = property.MRIFtpPort;
                _View.FTPAccountUserName = property.MRIFtpAccountUserName;
                _View.FTPAccountPassword = property.MRIFtpAccountPassword;
            }
        }
    }
}
