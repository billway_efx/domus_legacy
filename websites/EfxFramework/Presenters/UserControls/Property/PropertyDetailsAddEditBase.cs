﻿using System.Collections.Generic;
using EfxFramework.ExtensionMethods;
using EfxFramework.Interfaces.UserControls.Property;
using System.Globalization;
using System;
using RPO;
using System.Linq;

namespace EfxFramework.Presenters.UserControls.Property
{
    public abstract class PropertyDetailsAddEditBase
    {
        protected IPropertyDetailsAddEdit BaseView;
         
        protected PropertyDetailsAddEditBase(IPropertyDetailsAddEdit view)
        {
            BaseView = view;
        }

        public virtual void InitializeValues()
        {
            InitializeValues(new EfxFramework.Property(BaseView.PropertyId));
        }

        public virtual void InitializeValues(EfxFramework.Property property)
        {
            BaseView.StateList.BindToSortedEnum(typeof (StateProvince));

            if (!property.PropertyId.HasValue || property.PropertyId.Value < 1)
                return;

            BaseView.PropertyCodeText = property.PropertyCode;
            BaseView.PropertyNameText = property.PropertyName;
            //cakel: 00498
            BaseView.PropertyNameTwoText = property.PropertyNameTwo;
            BaseView.NumberOfUnitsText = property.NumberOfUnits.ToString();
            BaseView.PropertyAddressText = property.StreetAddress;
            BaseView.PropertyAddress2Text = property.StreetAddress2;
            BaseView.CityText = property.City;
            BaseView.StateList.SelectedValue = property.StateProvinceId.HasValue ? property.StateProvinceId.Value.ToString(CultureInfo.InvariantCulture) : "1";
            BaseView.TimeZoneList.SelectedValue = property.TimeZone;
            BaseView.PostalCodeText = property.PostalCode;
            BaseView.MainTelephoneText = property.MainPhoneNumber.ToString(false);
            BaseView.AlternateTelephoneText = property.AlternatePhoneNumber.ToString(false);
            BaseView.FaxNumberText = property.FaxNumber.ToString(false);
            BaseView.OfficeEmailText = property.OfficeEmailAddress;
            // Patrick Whittingham - 9/2/15 - task #371 : add notification email address 
            BaseView.NotificationEmailText = property.NotificationEmailAddress;
            BaseView.AverageRentText = property.AverageMonthlyRent.HasValue ? property.AverageMonthlyRent.Value.ToString("C") : "$0.00";
            BaseView.ConvenienceFeeText = EfxFramework.Property.GetConvenienceFeesByProperty(property).CreditCardFee.ToString("C");
            BaseView.RentReportersVerifiedChecked = property.IsVerifiedRentReportersProperty;
            BaseView.LineItemDetailChecked = property.ShowPaymentDetail;
            BaseView.AllowPartialPayments = property.AllowPartialPayments;

            //cakel: Added BugID 007
            BaseView.NumOfPrePayDays = property.NumOfPrePayDays;
            BaseView.AllowPrePayments = property.AllowPrePayments;
            BaseView.PaymentDueDay = property.PaymentDueDay;

            //cakel: BUGID0019, BUGID00276
            BaseView.AllowPartialPaymentsResidentPortal = property.AllowPartialPaymentsResidentPortal;

			BaseView.AcceptACHPayments = property.AcceptACHPayments;
			BaseView.AcceptCCPayments = property.AcceptCCPayments;
			BaseView.PNMEnabled = property.PNMEnabled;
            //CMallory - Task 00425 
            BaseView.DisableAllPayments = property.DisableAllPayments;
            //CMallory - Task 00554 
            BaseView.DisablePayByText = property.DisablePayByText;
            //CMallory - Task 00166 - Added
            BaseView.PropertyIsPublic = property.PropertyIsPublic;
        }

        protected string GetCompanyId(EfxFramework.Property property)
        {
            var Companies = new List<Company>();
            using (var entity = new RPOEntities())
            {
                if (property.PropertyId.HasValue && property.PropertyId.Value > 0)
                {
                    return entity.Properties.First(p => p.PropertyId == property.PropertyId.Value).CompanyId.ToString();
                }
                return "-1";
            }
        }
    }
}
