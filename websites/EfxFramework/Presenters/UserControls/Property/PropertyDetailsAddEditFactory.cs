﻿using EfxFramework.Interfaces.UserControls.Property;

namespace EfxFramework.Presenters.UserControls.Property
{
    public static class PropertyDetailsAddEditFactory
    {
        public static PropertyDetailsAddEditBase GetPropertyDetailsAddEdit(IEfxAdministratorPropertyDetailsAddEdit view)
        {
            return new EfxAdministratorPropertyDetailsAddEdit(view);
        }

        public static PropertyDetailsAddEditBase GetPropertyDetailsAddEdit(IPropertyStaffPropertyDetailsAddEdit view)
        {
            return new PropertyStaffPropertyDetailsAddEdit(view);
        }
    }
}
