﻿using EfxFramework.ExtensionMethods;
using EfxFramework.Interfaces.UserControls.Property;

namespace EfxFramework.Presenters.UserControls.Property
{
    public class EfxSalesAddEdit
    {
        private readonly IEfxSalesAddEdit _View;

        public EfxSalesAddEdit(IEfxSalesAddEdit view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            var Company = new Company(_View.CompanyId);

            if (Company.CompanyId < 1)
                return;

            _View.FirstNameText = Company.SalesFirstName;
            _View.LastNameText = Company.SalesLastName;
            _View.TelephoneNumberText = Company.SalesPhone.FormatPhoneNumber();
        }
    }
}
