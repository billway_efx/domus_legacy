﻿using EfxFramework.Interfaces.UserControls.Property;

namespace EfxFramework.Presenters.UserControls.Property
{

    public class MRIAddEdit2
    {
        private readonly IMRIAddEdit2 _View;

        public MRIAddEdit2(IMRIAddEdit2 view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            InitializeValues(new EfxFramework.Property(_View.PropertyId));
        }

        public void InitializeValues(EfxFramework.Property property)
        {

            if (property.PropertyId.HasValue)
            {
                _View.MRIClientID = property.MRIClientID;
                _View.MRIDatabaseName = property.MRIDatabaseName;
                _View.MRIWebServiceUserName = property.MRIWebServiceUserName;
                _View.MRIPartnerKey = property.MRIPartnerKey;
                _View.MRIImportEndPoint = property.MRIImportEndPoint;
                _View.MRIExportEndPoint = property.MRIExportEndPoint;
                _View.MRIWebServicePassword = property.MRIWebServicePassword;


            }

        }

    }
}
