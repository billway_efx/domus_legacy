﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EfxFramework.Interfaces.UserControls.Property;
using Mb2x.ExtensionMethods;

namespace EfxFramework.Presenters.UserControls.Property
{
    internal class PropertyStaffPropertyDetailsAddEdit : PropertyDetailsAddEditBase
    {
        private readonly IPropertyStaffPropertyDetailsAddEdit _View;

        public PropertyStaffPropertyDetailsAddEdit(IPropertyStaffPropertyDetailsAddEdit view)
            : base(view)
        {
            _View = view;
        }

        public override void InitializeValues(EfxFramework.Property property)
        {
            _View.PmCompanyDataSource = BuildCompanyList();
            _View.PmCompanySelectedValue = GetCompanyId(property);
            base.InitializeValues(property);
        }

        private static List<Company> BuildCompanyList()
        {
            var Companies = new List<Company>();
            var StaffId = !String.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) ? HttpContext.Current.User.Identity.Name.ToInt32() : (int?)null;

            if (!StaffId.HasValue || StaffId.Value < 1)
                return Companies;

            //Null reference check already exists
            // ReSharper disable PossibleInvalidOperationException
            foreach (var C in from P in EfxFramework.Property.GetPropertyListByPropertyStaffId(StaffId.Value) where P.PropertyId.HasValue && P.PropertyId.Value > 0 from C in Company.GetCompanyListByPropertyId(P.PropertyId.Value) where Companies.All(c => c.CompanyId != C.CompanyId) select C)
            // ReSharper restore PossibleInvalidOperationException
            {
                Companies.Add(C);
            }

            return Companies;
        }
    }
}
