﻿using EfxFramework.Interfaces.UserControls.Property;

namespace EfxFramework.Presenters.UserControls.Property
{
    internal class EfxAdministratorPropertyDetailsAddEdit : PropertyDetailsAddEditBase
    {
        private readonly IEfxAdministratorPropertyDetailsAddEdit _View;

        public EfxAdministratorPropertyDetailsAddEdit(IEfxAdministratorPropertyDetailsAddEdit view)
            : base(view)
        {
            _View = view;
        }

        public override void InitializeValues(EfxFramework.Property property)
        {
            
            _View.PmCompanyDataSource = Company.GetAllCompanyList();
            _View.PmCompanySelectedValue = GetCompanyId(property);
            base.InitializeValues(property);
        }
    }
}
