﻿using EfxFramework.Interfaces.UserControls.Property;
//cakel: BUGID00351 RealPage integration

namespace EfxFramework.Presenters.UserControls.Property
{
    public class RealPageAddEdit
    {
        private readonly IRealPageAddEdit _View;

        public RealPageAddEdit(IRealPageAddEdit view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            InitializeValues(new EfxFramework.Property(_View.PropertyId));
        }


        public void InitializeValues(EfxFramework.Property property)
        {
            if (property.PropertyId.HasValue)
            {
                _View.UsernameText = property.RealPageUserName;
                _View.PasswordText = property.RealPagePassword;
                _View.RealPagePMCID = property.RealPagePMCID;
                _View.RealPageImportEndPoint = property.RealPageImportURL;
                _View.RealPageExportEndPoint = property.RealPageExportURL;
                _View.RealPageInternalUser = property.RealPageInternalUser;
                _View.RealPageSiteID = property.RealPageSiteID;

            }
        }


    }
}
