﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using EfxFramework.ExtensionMethods;
using EfxFramework.Interfaces.UserControls.Property;
using EfxFramework.PaymentMethods;
using EfxFramework.Web;
using EfxFramework.Web.Facade;
using EfxFramework.WebPayments;
using Mb2x.ExtensionMethods;
using System;
using System.Web.UI.WebControls;

namespace EfxFramework.Presenters.UserControls.Property
{
    public class ApplicationsPresenter
    {
        private readonly IApplications _View;

        public ApplicationsPresenter(IApplications view)
        {
            _View = view;
            _View.UploadButtonClicked = UploadButtonClicked;
            _View.DeleteButtonClicked = DeleteButtonClicked;
            _View.AddApplicantClicked = AddApplicantClicked;
            _View.SaveApplicantsClicked = SaveApplicantsClicked;
            _View.SubmitPaymentClicked = SubmitPaymentClicked;
            _View.PaymentAmountsControl.UpdateTotalClicked = UpdateTotalClicked;
            _View.RowBound = RowBound;
        }

        public void InitializeValues()
        {
            SetApplicationLabel();

            _View.ApplicationStatusList.BindToSortedEnum(typeof (ApplicationStatus));
            _View.ApplicationReceivedList.BindToSortedEnum(typeof (ApplicationSubmissionType));

            BindGrid();

            if (_View.ApplicantApplicationId.HasValue && _View.ApplicantApplicationId > 0)
                PopulatePaymentFields();
            else
                _View.PaymentButtonEnabled = false;
        }

        private void BindGrid()
        {
            _View.ApplicantsGrid.DataSource = ApplicantApplication.GetAllApplicantApplicationsByPropertyId(_View.PropertyId);
            _View.ApplicantsGrid.DataBind();
        }

        private void SetApplicationLabel()
        {
            var Property = new EfxFramework.Property(_View.PropertyId);
            if (!Property.PropertyId.HasValue || Property.PropertyId < 1)
                return;

            var Application = EfxFramework.Property.GetApplicationForResidency(_View.PropertyId);
            if (Application != null)
            {
                _View.ApplicationLabelText = String.Format("{0} Application Form.", Property.PropertyName);
                _View.DeleteButtonVisible = true;
            }
            else
            {
                _View.ApplicationLabelText = String.Empty;
                _View.DeleteButtonVisible = false;
            }
        }

        private void PopulatePaymentFields()
        {
            _View.PaymentButtonEnabled = true;

            var ApplicantApplication = new ApplicantApplication(_View.ApplicantApplicationId);
            if (ApplicantApplication.ApplicantApplicationId < 1)
                return;

            var Applicant = new Applicant(ApplicantApplication.ApplicantId);
            if (Applicant.ApplicantId < 1)
                return;

            var Property = new EfxFramework.Property(ApplicantApplication.PropertyId);
            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                return;

            _View.PersonalInformationControl.FirstNameText = Applicant.FirstName;
            _View.PersonalInformationControl.LastNameText = Applicant.LastName;
            _View.PersonalInformationControl.EmailAddressText = Applicant.PrimaryEmailAddress;
            _View.PaymentAmountsControl.PaymentAmountsView.SetActiveView(_View.PaymentAmountsControl.ApplicationAmountsView);

            SetApplicationAmounts();
        }

        private void UploadButtonClicked(object sender, EventArgs e)
        {
            if (_View.ApplicationBytes != null && _View.ApplicationBytes.Length > 0)
                EfxFramework.Property.SetApplicationForResidency(_View.PropertyId, _View.ApplicationBytes);

            SetApplicationLabel();
        }

        private void DeleteButtonClicked(object sender, EventArgs e)
        {
            EfxFramework.Property.SetApplicationForResidency(_View.PropertyId, null);
            SetApplicationLabel();
        }

        private static void SendEmailX(BaseUser applicant, string password)
        {
            if (String.IsNullOrEmpty(applicant.PrimaryEmailAddress))
                return;

            BulkMail.BulkMail.SendMailMessage(
                null,
                "EFX",
                false,
                EfxSettings.SendMailUsername,
                EfxSettings.SendMailPassword,
                new MailAddress(EfxSettings.SupportEmail),
                "Forgot Password",
                string.Format(Resources.EmailTemplates.ApplicantPasswordSet, applicant.FirstName, password),
                new List<MailAddress> { new MailAddress(applicant.PrimaryEmailAddress) });
        }
        
        private void AddApplicantClicked(object sender, EventArgs e)
        {
            _View.ParentPage.Validate("NewApplicant");
            if (!_View.ParentPage.IsValid)
                return;

            var Applicant = new Applicant
                {
                    FirstName = _View.FirstNameText,
                    LastName = _View.LastNameText,
                    PrimaryEmailAddress = _View.EmailAddressText
                };

            var NewPassword = Applicant.ResetPassword();

            try
            {
                var ApplicantId = Applicant.Set(Applicant);

                var ApplicantApplication = new ApplicantApplication
                    {
                        ApplicantId = ApplicantId,
                        PropertyId = _View.PropertyId,
                        ApplicationStatusId = _View.ApplicationStatusList.SelectedValue.ToInt32(),
                        FeesPaid = false,
                        ApplicationSubmissionTypeId = _View.ApplicationReceivedList.SelectedValue.ToInt32(),
                        ApplicationDate = DateTime.UtcNow,
                        ApplicationStatusDate = DateTime.UtcNow,
                        StatusUpdatedBy = _View.IsPropertyManagerSite ? UIFacade.GetCurrentUser(UserType.PropertyStaff).DisplayName : UIFacade.GetCurrentUser(UserType.EfxAdministrator).DisplayName
                    };

                var ApplicationId = EfxFramework.ApplicantApplication.Set(ApplicantApplication);

                if (_View.ApplicationStatusList.SelectedValue.ToInt32() == 1 && ApplicationId > 0)
                    PromoteApplicant(ApplicationId);

                BindGrid();
                SendEmailX(Applicant, NewPassword);
            }
            catch (SqlException Ex)
            {
                if (Ex.Number == 2601 || Ex.Number == 2627 || Ex.Number == 50000)
                    BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Duplicate Email Address Used");
                else
                    BasePage.SafePageRedirect("~/ErrorPage.html");
            }
            catch
            {
                BasePage.SafePageRedirect("~/ErrorPage.html");
            }
        }

        private void SaveApplicantsClicked(object sender, EventArgs e)
        {
            foreach (GridViewRow Row in _View.ApplicantsGrid.Rows)
            {
                var ApplicantApplicationId = Row.Cells[0].FindControl("ApplicantApplicationId") as HiddenField;
                var Received = Row.Cells[3].FindControl("ReceivedDropDown") as DropDownList;
                var Status = Row.Cells[5].FindControl("StatusDropDown") as DropDownList;

                if (ApplicantApplicationId != null && Received != null && Status != null)
                    SaveApplicantApplications(ApplicantApplicationId.Value, Received.SelectedValue, Status.SelectedValue);

                var Id = ApplicantApplicationId != null && ApplicantApplicationId.Value.ToNullInt32().HasValue ? ApplicantApplicationId.Value.ToInt32() : 0;
                var StatusId = Status != null && Status.SelectedValue != null ? Status.SelectedValue.ToInt32() : 0;

                if (StatusId == 1 && Id > 0)
                    PromoteApplicant(Id);
            }

            BindGrid();
        }

        private void PromoteApplicant(int id)
        {
            var Application = new ApplicantApplication(id);
            var ApplicantId = Application.ApplicantId;
            var Resident = new EfxFramework.Renter();

            if (ApplicantId > 0)
                Resident = EfxFramework.Renter.PromoteApplicantToRenter(ApplicantId, UIFacade.GetAuthenticatedUserString(_View.IsPropertyManagerSite ? UserType.PropertyStaff : UserType.EfxAdministrator),
                    _View.PropertyId);
            
            if (!String.IsNullOrEmpty(Resident.FirstName) && !String.IsNullOrEmpty(Resident.LastName))
                EfxFramework.Payer.SetPayerFromRenter(Resident);

            if (Application.PropertyId > 0 && Resident.RenterId > 0)
                EfxFramework.Renter.AssignRenterToProperty(Application.PropertyId, Resident.RenterId);
        }

        private void RowBound(object sender, GridViewRowEventArgs e)
        {
            var ApplicantApplicationId = e.Row.Cells[0].FindControl("ApplicantApplicationId") as HiddenField;

            if (ApplicantApplicationId == null)
                return;

            var ApplicationId = !String.IsNullOrEmpty(ApplicantApplicationId.Value) ? ApplicantApplicationId.Value.ToInt32() : 0;
            var Application = new ApplicantApplication(ApplicationId);

            if (Application.ApplicantApplicationId < 1)
                return;

            if ((e.Row.Cells[3].FindControl("ReceivedDropDown") as DropDownList) != null)
                // ReSharper disable PossibleNullReferenceException
                (e.Row.Cells[3].FindControl("ReceivedDropDown") as DropDownList).SelectedValue = Application.ApplicationSubmissionTypeId.ToString(CultureInfo.InvariantCulture);
                // ReSharper restore PossibleNullReferenceException

            if ((e.Row.Cells[5].FindControl("StatusDropDown") as DropDownList) != null)
                // ReSharper disable PossibleNullReferenceException
                (e.Row.Cells[5].FindControl("StatusDropDown") as DropDownList).SelectedValue = Application.ApplicationStatusId.ToString(CultureInfo.InvariantCulture);
                // ReSharper restore PossibleNullReferenceException

            var DownloadLink = ((HyperLink) e.Row.Cells[2].Controls[0]);
            // ReSharper disable LocalizableElement
            if (DownloadLink.Text == "N/A")
            // ReSharper restore LocalizableElement
            {
                DownloadLink.CssClass = "receivedText";
            }
            else
            {
                DownloadLink.ForeColor = System.Drawing.Color.Blue;
                e.Row.Cells[2].ControlStyle.ForeColor = System.Drawing.Color.Blue;
                e.Row.Cells[2].Font.Underline = true;
            }            
        }

        private void SaveApplicantApplications(string applicantApplicationId, string submissionMethod, string applicationStatus)
        {
            var ApplicantApplicationId = !String.IsNullOrEmpty(applicantApplicationId) ? applicantApplicationId.ToInt32() : 0;
            var SubmissionMethodId = !String.IsNullOrEmpty(submissionMethod) ? submissionMethod.ToInt32() : 1;
            var ApplicationStatus = !String.IsNullOrEmpty(applicationStatus) ? applicationStatus.ToInt32() : 1;

            var Application = new ApplicantApplication(ApplicantApplicationId)
                {
                    PropertyId = _View.PropertyId,
                    ApplicationStatusId = ApplicationStatus,
                    ApplicationSubmissionTypeId = SubmissionMethodId,
                    ApplicationStatusDate = DateTime.UtcNow,
                    StatusUpdatedBy = _View.IsPropertyManagerSite ? UIFacade.GetCurrentUser(UserType.PropertyStaff).DisplayName : UIFacade.GetCurrentUser(UserType.EfxAdministrator).DisplayName
                };

            ApplicantApplication.Set(Application);
        }

        private void SubmitPaymentClicked(object sender, EventArgs e)
        {
            _View.ParentPage.Validate("Payment");
            if (!_View.ParentPage.IsValid)
                return;

            var PaymentInfo = BuildApplicantPaymentInfo();
            if (PaymentInfo == null)
                return;

            var Result = ApplicantPaymentRequest.ProcessPayment(PaymentInfo);
            BasePage.SafePageRedirect(Result.Result == GeneralResponseResult.Success ? String.Format("~/PaymentStatus.aspx?TransactionId={0}&ApplicantId={1}", Result.TransactionId, PaymentInfo.ApplicantId) : "~/PaymentFailed.aspx");
        }

        private void SetApplicationAmounts()
        {
            _View.PaymentAmountsControl.ApplicationFeesList.DataSource = ApplicationFee.GetAllApplicationFeesByPropertyId(_View.PropertyId);
            _View.PaymentAmountsControl.ApplicationFeesList.DataBind();
            _View.PaymentAmountsControl.TotalAmountText = GetFeesForApplicant().Sum(f => f.FeesPaid).ToString("C");
        }

        private List<ApplicationFee> GetFeesForApplicant()
        {
            return (from Item in _View.PaymentAmountsControl.ApplicationFeesList.Items let Waived = Item.FindControl("ApplicationFeeWaiveCheckBox") as CheckBox where Waived != null && !Waived.Checked select Item.FindControl("ApplicationFeeIdField") as HiddenField into FeeIdField where FeeIdField != null && FeeIdField.Value.ToInt32() > 0 select new ApplicationFee(FeeIdField.Value.ToInt32()) into Fee where Fee.ApplicationFeeId > 0 select Fee).ToList();
        }

        private ApplicantPaymentInfo BuildApplicantPaymentInfo()
        {
            var Application = new ApplicantApplication(_View.ApplicantApplicationId);
            if (Application.ApplicantApplicationId < 1)
            {
                BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Unable to Process Payment.  No Application in the System for this Applicant and Property");
                return null;
            }

            if (Application.FeesPaid)
            {
                BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Records indicate that the Applicant has already paid the Application Fees for this Property");
                return null;
            }

            return new ApplicantPaymentInfo
            {
                Fees = GetFeesForApplicant(),
                TypeOfPayment = ((PaymentType)_View.SelectPaymentControl.PaymentTypeSelectedValue.ToInt32()),
                CreditCardInfo = _View.SelectPaymentControl.PaymentTypeSelectedValue == "1" ? GetCreditCardDetails() : null,
                EcheckInfo = _View.SelectPaymentControl.PaymentTypeSelectedValue == "2" ? GetEcheckDetails() : null,
                PropertyId = _View.PropertyId,
                ApplicantId = Application.ApplicantId,
                ApplicantApplicationId = Application.ApplicantApplicationId,
                IsTestUser = false
            };
        }

        private CreditCardDetails GetCreditCardDetails()
        {
            return new CreditCardDetails
            {
                CardHolderName = _View.SelectPaymentControl.CreditCardDetailsControl.NameOnCardText,
                CreditCardNumber = _View.SelectPaymentControl.CreditCardDetailsControl.CreditCardNumberText,
                ExpirationDate = new DateTime(_View.SelectPaymentControl.CreditCardDetailsControl.ExpirationYearList.SelectedValue.ToInt32(),
                    _View.SelectPaymentControl.CreditCardDetailsControl.ExpirationMonthSelectedValue.ToInt32(), 1),
                CvvCode = _View.SelectPaymentControl.CreditCardDetailsControl.CvvText,
                BillingAddress = new Address
                {
                    AddressLine1 = _View.BillingInformationControl.BillingAddressText,
                    City = _View.BillingInformationControl.CityText,
                    StateProvinceId = _View.BillingInformationControl.StateDropDown.SelectedValue.ToInt32(),
                    PostalCode = _View.BillingInformationControl.ZipText
                },
                BillingFirstName = _View.PersonalInformationControl.FirstNameText,
                BillingLastName = _View.PersonalInformationControl.LastNameText
            };
        }

        private EcheckDetails GetEcheckDetails()
        {
            return new EcheckDetails
            {
                BankAccountNumber = _View.SelectPaymentControl.ECheckDetailsControl.AccountNumberText,
                BankRoutingNumber = _View.SelectPaymentControl.ECheckDetailsControl.RoutingNumberText,
                TypeOfAccount = AccountType.Checking,
                BillingAddress = new Address
                {
                    AddressLine1 = _View.BillingInformationControl.BillingAddressText,
                    City = _View.BillingInformationControl.CityText,
                    StateProvinceId = _View.BillingInformationControl.StateDropDown.SelectedValue.ToInt32(),
                    PostalCode = _View.BillingInformationControl.ZipText
                },
                BillingFirstName = _View.PersonalInformationControl.FirstNameText,
                BillingLastName = _View.PersonalInformationControl.LastNameText
            };
        }

        private void UpdateTotalClicked(object sender, EventArgs e)
        {
            var Amount = GetFeesForApplicant().Sum(f => f.FeesPaid);

            if (!_View.PaymentAmountsControl.WaiveConvenienceFee.Checked)
                Amount += !String.IsNullOrEmpty(_View.PaymentAmountsControl.ConvenienceFeeText) ? _View.PaymentAmountsControl.ConvenienceFeeText.Replace("$", "").ToDecimal() : 0.00M;

            _View.PaymentAmountsControl.TotalAmountText = Amount.ToString("C");
        }
    }
}
