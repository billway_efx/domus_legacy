﻿using EfxFramework.Interfaces.UserControls.Property;

namespace EfxFramework.Presenters.UserControls.Property
{
    public class YardiAddEdit
    {
        private readonly IYardiAddEdit _View;

        public YardiAddEdit(IYardiAddEdit view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            InitializeValues(new EfxFramework.Property(_View.PropertyId));
        }

        public void InitializeValues(EfxFramework.Property property)
        {
            if (property.PropertyId.HasValue)
            {
                _View.UsernameText = property.YardiUserName;
                _View.PasswordText = property.YardiPassword;
                _View.ServerNameText = property.YardiServerName;
                _View.DatabaseNameText = property.YardiDatabaseName;
                _View.PlatformText = property.YardiPlatform;
                _View.EndpointText = property.YardiEndpoint;
                _View.PropertyIdText = property.PmsId;

                // Salcedo - 8/24/2014 - issue # 0000179
                _View.YardiDepositDateCheckbox = property.YardiUseDepositDateAPI; 
            }
        }

        //Salcedo - 7/11/2014 - I don't think this function is ever used. The save functionality is in Property.cs and PropertyAddEditBase
        public void SaveYardiPropertyInfo(EfxFramework.Property property)
        {
            property.PmsId = _View.PropertyIdText;
            property.YardiUserName = _View.UsernameText;
            property.YardiPassword = _View.PasswordText;
            property.YardiServerName = _View.ServerNameText;
            property.YardiDatabaseName = _View.DatabaseNameText;
            property.YardiPlatform = _View.PlatformText;
            property.YardiEndpoint = _View.EndpointText;

            // Salcedo - 8/24/2014 - issue # 0000179
            property.YardiUseDepositDateAPI = _View.YardiDepositDateCheckbox;

            EfxFramework.Property.Set(property);
        }
    }

}
