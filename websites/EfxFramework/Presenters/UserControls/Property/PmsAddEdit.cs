﻿using System.Globalization;
using EfxFramework.ExtensionMethods;
using EfxFramework.Interfaces.UserControls.Property;
using Mb2x.ExtensionMethods;
using System;

namespace EfxFramework.Presenters.UserControls.Property
{
    public class PmsAddEdit
    {
        private readonly IPmsAddEdit _View;

        public PmsAddEdit(IPmsAddEdit view)
        {
            _View = view;
            _View.PmsDropdownList.SelectedIndexChanged += PmsChanged;
        }

        public void InitializeValues()
        {
            _View.PmsDropdownList.BindToSortedEnum(typeof (PmsType));
            _View.YardiControl.PropertyId = _View.PropertyId;
            
            //Salcedo - 7/24/2014 - new control for MRI integration
            //cakel: 00382 - Old Control for MRI
            //_View.MRIControl.PropertyId = _View.PropertyId;
            //cakel: 00382 - New Control for MRI 
            _View.MRIControl2.PropertyId = _View.PropertyId;

            //cakel: BUGID00316 - AMSI Integration
            _View.AmsiControl.PropertyId = _View.PropertyId;

            //cakel: 00351 RealPage
            _View.RealPageControl.PropertyId = _View.PropertyId;

            var Property = new EfxFramework.Property(_View.PropertyId);

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value <= 0) 
                return;

            _View.PmsDropdownList.SelectedValue = Property.PmsTypeId.HasValue ? Property.PmsTypeId.Value.ToString(CultureInfo.InvariantCulture) : "-1";
            ChangeView(Property.PmsTypeId.HasValue ? Property.PmsTypeId.Value : 0);
        }

        private void PmsChanged(object sender, EventArgs e)
        {
            ChangeView(_View.PmsDropdownList.SelectedValue.ToInt32());
        }

        private void ChangeView(int selectedValue)
        {
            switch (selectedValue)
            {
                case 1:
                    _View.PmsViews.SetActiveView(_View.Yardi);
                    break;

                case 2:
                    _View.PmsViews.SetActiveView(_View.Amsi);
                    break;

                case 3:
                    _View.PmsViews.SetActiveView(_View.RealPage);
                    break;

                case 4:
                    _View.PmsViews.SetActiveView(_View.MRI);
                    break;

                default:
                    _View.PmsViews.SetActiveView(_View.Blank);
                    break;
            }
        }
    }
}
