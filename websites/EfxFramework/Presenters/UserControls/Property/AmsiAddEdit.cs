﻿using EfxFramework.Interfaces.UserControls.Property;
//cakel: BUGID00316
namespace EfxFramework.Presenters.UserControls.Property
{
    public class AmsiAddEdit
    {
        private readonly IAmsiAddEdit _View;

        public AmsiAddEdit(IAmsiAddEdit view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            InitializeValues(new EfxFramework.Property(_View.PropertyId));
        }


        public void InitializeValues(EfxFramework.Property property)
        {
            if (property.PropertyId.HasValue)
            {
                _View.UsernameText = property.AmsiUserName;
                _View.PasswordText = property.AmsiPassword;
                _View.DatabaseNameText = property.AmsiDatabaseName;
                _View.ClientMerchantID = property.AmsiClientMerchantID;
                //cakel: 00551
                _View.AchClientMerchantID = property.AmsiAchClientMerchantID;
                _View.CashClientMerchantID = property.AmsiCashClientMerchantID;

                _View.PropertyIdText = property.PmsId;
                //cakel BUGID00316 AMSI Revision 04/28/2015
                _View.AmsiImportEndPoint = property.AmsiImportEndPoint;
                _View.AmsiExportEndPoint = property.AmsiExportEndPoint;
            }
        }


    }
}
