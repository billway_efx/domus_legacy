﻿using System.Globalization;
using System.Web.UI;
using EfxFramework.Interfaces;
using EfxFramework.Interfaces.UserControls.Property;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;

namespace EfxFramework.Presenters.UserControls.Property
{
    public class ProgramInformationAddEdit
    {
        private readonly IProgramInformationAddEdit _View;

        public ProgramInformationAddEdit(IProgramInformationAddEdit view)
        {
            _View = view;
            _View.ProgramSelected = SelectedProgramChanged;
            BindProgramData();
        }

        public void InitializeValues()
        {
            var Property = new EfxFramework.Property(_View.PropertyId);

            if (Property.PropertyId.HasValue && Property.PropertyId.Value > 0)
                _View.ProgramSelectedValue = Property.ProgramId.HasValue ? ((int)Property.ProgramId.Value).ToString(CultureInfo.InvariantCulture) : "0";

            SetProgramMultiView();
        }

        private void SelectedProgramChanged(object sender, EventArgs e)
        {
            SetConvenienceFee();
            SetProgramMultiView();
        }

        private void BindProgramData()
        {
            _View.PropertyPaidProgramDataSource = BuildPropertyPaidProgram();
            _View.SpaProgramDataSource = BuildSpaProgram();
            _View.AirProgramDataSource = BuildAirProgram();
        }

        private void SetConvenienceFee()
        {
            var Control = (_View as UserControl);
            //cakel: 11/01/2016
            
            var Program = EfxFramework.Property.Program.Unassigned;

            if (Control == null)
                return;

            var ParentPage = Control.Page.Master != null ? Control.Page.Master.Parent as IPropertyAddEdit: null;

            if (ParentPage == null)
                return;

            var AvgRent = !String.IsNullOrEmpty(ParentPage.PropertyDetailInfo.AverageRentText) ? ParentPage.PropertyDetailInfo.AverageRentText.Replace("$", "").ToDecimal() : 0.00M;

            if (_View.ProgramSelectedValue != "-1")
                Program = (EfxFramework.Property.Program) _View.ProgramSelectedValue.ToInt32();

            ParentPage.PropertyDetailInfo.ConvenienceFeeText = ConvenienceFee.GetConvenienceFeeForProperty(Program, AvgRent).CreditCardFee.ToString("C");
        }

        private void SetProgramMultiView()
        {
            var ProgramId = _View.ProgramSelectedValue.ToInt32();

            switch (ProgramId)
            {
                case 0:
                    _View.ProgramMultiview.SetActiveView(_View.NoneSelectedView);
                    break;

                case 1:
                    _View.ProgramMultiview.SetActiveView(_View.AirView);
                    break;

                case 2:
                    _View.ProgramMultiview.SetActiveView(_View.SpaView);
                    break;

                case 3:
                    _View.ProgramMultiview.SetActiveView(_View.PropertyPaidView);
                    break;

                default:
                    _View.ProgramMultiview.SetActiveView(_View.NoneSelectedView);
                    break;
            }
        }

        private static List<PropertyPaidProgram> BuildPropertyPaidProgram()
        {
            return new List<PropertyPaidProgram> 
                {
                    new PropertyPaidProgram()
                };
        }

        private static List<SpaProgram> BuildSpaProgram()
        {
            return new List<SpaProgram> 
                {
                    new SpaProgram()
                };
        }

        private static List<AirProgram> BuildAirProgram()
        {
            return new List<AirProgram>
                {
                    new AirProgram
                        {
                            AllInclusiveRate = "$18.00",
                            UnitCount = "1-35"
                        },
                    new AirProgram
                        {
                            AllInclusiveRate = "$27.00",
                            UnitCount = "36-76"
                        },
                    new AirProgram
                        {
                            AllInclusiveRate = "$40.00",
                            UnitCount = "77-100"
                        },
                    new AirProgram
                        {
                            AllInclusiveRate = "$45.00",
                            UnitCount = "101-150"
                        },
                    
                    new AirProgram
                        {
                            AllInclusiveRate = "$54.00",
                            UnitCount = "151-250"
                        },
                    new AirProgram
                        {
                            AllInclusiveRate = "$63.00",
                            UnitCount = "251-355"
                        },
                    new AirProgram
                        {
                            AllInclusiveRate = "$72.00",
                            UnitCount = "356-453"
                        },
                    new AirProgram
                        {
                            AllInclusiveRate = "$81.00",
                            UnitCount = "454-555"
                        },
                    new AirProgram
                        {
                            AllInclusiveRate = "$90.00",
                            UnitCount = "556-657"
                        },
                    new AirProgram
                        {
                            AllInclusiveRate = "$99.00",
                            UnitCount = "658-760"
                        },
                    new AirProgram
                        {
                            AllInclusiveRate = "$108.00",
                            UnitCount = "761-850"
                        },
                    new AirProgram
                        {
                            AllInclusiveRate = "$117.00",
                            UnitCount = "851-950"
                        },
                    new AirProgram
                        {
                            AllInclusiveRate = "$126.00",
                            UnitCount = "950-1050"
                        }
                };
        }
    }
}
