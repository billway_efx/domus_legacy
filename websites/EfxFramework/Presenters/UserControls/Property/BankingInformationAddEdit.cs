﻿using EfxFramework.ExtensionMethods;
using EfxFramework.Interfaces.UserControls.Property;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace EfxFramework.Presenters.UserControls.Property
{
    public class BankingInformationAddEdit
    {
        private readonly IBankingInformationAddEdit _View;

        public BankingInformationAddEdit(IBankingInformationAddEdit view)
        {
            _View = view;
        }

        public void IntializeValues()
        {

            var Property = new EfxFramework.Property(_View.PropertyId);

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1) 
                return;

            _View.RentalBankNameText = Property.RentalDepositBankName;
            _View.RentalBankNameAccountText = Property.RentalDepositBankAccountNumber.MaskAccountNumber();
            _View.RentalBankNameRoutingText = Property.RentalDepositBankRoutingNumber;
            _View.SecurityBankNameText = Property.SecurityDepositBankName;
            _View.SecurityBankNameAccountText = Property.SecurityDepositBankAccountNumber.MaskAccountNumber();
            _View.SecurityBankNameRoutingText = Property.SecurityDepositBankRoutingNumber;
            _View.FeeBankNameText = Property.MonthlyFeeBankName;
            _View.FeeBankNameAccountText = Property.MonthlyFeeBankAccountNumber.MaskAccountNumber();
            _View.FeeBankNameRoutingText = Property.MonthlyFeeBankRoutingNumber;
            //cakel: BUGID00213
            _View.CCDepositAccountNumberText = Property.CCDepositAccountNumber.MaskAccountNumber();
            _View.CCDepositBankNameText = Property.CCDepositBankName;
            _View.CCDepositRoutingNumberText = Property.CCDepositRoutingNumber;
            
        }

    }
}
