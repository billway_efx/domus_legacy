﻿using EfxFramework.Interfaces.UserControls.PaymentSteps;

namespace EfxFramework.Presenters.UserControls.PaymentSteps
{
    public static class OneTimePaymentStepOneFactory
    {
        public static OneTimePaymentStepOneBase GetOneTimePaymentStepOne(IRenterOneTimePaymentStepOne view)
        {
            return new RenterOneTimePaymentStepOne(view);
        }

        public static OneTimePaymentStepOneBase GetOneTimePaymentStepOne(IEfxAdministratorOneTimePaymentStepOne view)
        {
            return new EfxAdministratorOneTimePaymentStepOne(view);
        }

        public static OneTimePaymentStepOneBase GetOneTimePaymentStepOne(IPropertyStaffOneTimePaymentStepOne view)
        {
            return new PropertyStaffOneTimePaymentStepOne(view);
        }
    }
}
