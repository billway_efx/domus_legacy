﻿using System;
using System.Globalization;
using EfxFramework.ExtensionMethods;
using EfxFramework.Interfaces.UserControls.PaymentSteps;

namespace EfxFramework.Presenters.UserControls.PaymentSteps
{
    public abstract class OneTimePaymentStepOneBase
    {
        protected readonly IOneTimePaymentStepOne BaseView;

        protected OneTimePaymentStepOneBase(IOneTimePaymentStepOne view)
        {
            BaseView = view;
            BaseView.RenterSelectionControl.AddRenterClicked = RenterSelectionChanged;
            BaseView.PayerInformationControl.RenterInfoPayerInfoChecked = PayerInfoChanged;
        }

        protected virtual void RenterSelectionChanged(object sender, EventArgs e)
        {
            ChangePayer();
        }

        protected virtual void PayerInfoChanged(object sender, EventArgs e)
        {
            ChangePayer();
        }

        protected virtual void ChangePayer()
        {
            var RenterId = Int32.Parse(BaseView.RenterSelectionControl.RenterDropdownSelectedValue);

            if (RenterId == -1)
                return;

            var Renter = new EfxFramework.Renter(RenterId);

            if (!Renter.IsValid)
                return;

            var UserId = BaseView.PayerInformationControl.RenterIsPayerChecked ? Renter.RenterId : (Renter.PayerId.HasValue ? Renter.PayerId.Value : 0);

            SetPayerInformation(UserId);
            SetPaymentMethodInformation(UserId, Renter.RenterId);
        }

        protected virtual void SetPayerInformation(int userId)
        {
            EfxFramework.Payer Payer;

            if (BaseView.PayerInformationControl.RenterIsPayerChecked)
            {
                var Renter = new EfxFramework.Renter(userId);

                if (!Renter.IsValid)
                    return;

                Payer = new EfxFramework.Payer(Renter.PrimaryEmailAddress);
            }
            else
            {
                Payer = new EfxFramework.Payer(userId);
            }

            BuildPayerInformation(Payer);
        }

        protected virtual void SetPaymentMethodInformation(int userId, int renterId)
        {
            BaseView.PaymentMethodsControl.PaymentMethodsSelectedValue = "echeck";
            PaymentMethodSelected();

            EfxFramework.Payer Payer;

            if (BaseView.PayerInformationControl.RenterIsPayerChecked)
            {
                var Renter = new EfxFramework.Renter(userId);

                if (!Renter.IsValid)
                    return;

                Payer = new EfxFramework.Payer(Renter.PrimaryEmailAddress);
            }
            else
            {
                Payer = new EfxFramework.Payer(userId);
            }

            if (Payer.PayerId < 1)
                return;

            var PayerAch = EfxFramework.PayerAch.GetPrimaryPayerAchByPayerId(Payer.PayerId);
            var PayerCreditCard = EfxFramework.PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(Payer.PayerId);

            BuildPaymentMethods(PayerAch, PayerCreditCard, renterId);
        }

        protected virtual void BuildPayerInformation(EfxFramework.Payer payer)
        {
            if (payer.PayerId > 0)
            {
                BaseView.PayerInformationControl.PayersFirstName = payer.FirstName;
                BaseView.PayerInformationControl.PayersMiddleName = payer.MiddleName;
                BaseView.PayerInformationControl.PayersLastName = payer.LastName;
                BaseView.PayerInformationControl.PayersSuffix = payer.Suffix;
                BaseView.PayerInformationControl.PayersEmail = payer.PrimaryEmailAddress;
                BaseView.PayerInformationControl.PayersAddress = payer.StreetAddress;
                BaseView.PayerInformationControl.PayersAddress2 = payer.StreetAddress2;
                BaseView.PayerInformationControl.PayersCity = payer.City;
                BaseView.PayerInformationControl.PayersZipCode = payer.PostalCode;
                BaseView.PayerInformationControl.PayersCellPhone = payer.MobilePhoneNumber;
                BaseView.PayerInformationControl.PayersHomePhone = payer.MainPhoneNumber;
                BaseView.PayerInformationControl.StateDropdown.SelectedValue = payer.StateProvinceId.HasValue ? payer.StateProvinceId.Value.ToString(CultureInfo.InvariantCulture) : "1";
            }
            else
            {
                ClearForm();
            }
        }

        protected virtual void ClearForm()
        {
            BaseView.PayerInformationControl.PayersFirstName = string.Empty;
            BaseView.PayerInformationControl.PayersMiddleName = string.Empty;
            BaseView.PayerInformationControl.PayersLastName = string.Empty;
            BaseView.PayerInformationControl.PayersSuffix = string.Empty;
            BaseView.PayerInformationControl.PayersEmail = string.Empty;
            BaseView.PayerInformationControl.PayersAddress = string.Empty;
            BaseView.PayerInformationControl.PayersAddress2 = string.Empty;
            BaseView.PayerInformationControl.PayersCity = string.Empty;
            BaseView.PayerInformationControl.PayersZipCode = string.Empty;
            BaseView.PayerInformationControl.PayersCellPhone = string.Empty;
            BaseView.PayerInformationControl.PayersHomePhone = string.Empty;
            BaseView.PayerInformationControl.StateDropdown.SelectedIndex = 0;
        }

        protected virtual void PaymentMethodSelected()
        {
            switch (BaseView.PaymentMethodsControl.PaymentMethodsSelectedValue)
            {
                case "echeck":
                    BaseView.PaymentMethodsControl.EcheckPanelVisible = true;
                    BaseView.PaymentMethodsControl.CreditCardPanelVisible = false;
                    break;

                case "credit":
                    BaseView.PaymentMethodsControl.EcheckPanelVisible = false;
                    BaseView.PaymentMethodsControl.CreditCardPanelVisible = true;
                    break;

                default:
                    BaseView.PaymentMethodsControl.EcheckPanelVisible = true;
                    BaseView.PaymentMethodsControl.CreditCardPanelVisible = false;
                    break;
            }
        }

        protected virtual void BuildPaymentMethods(PayerAch payerAch, PayerCreditCard payerCreditCard, int renterId)
        {
            BuildAch(payerAch);
            BuildCreditCard(payerCreditCard, renterId);
        }

        protected virtual void BuildAch(PayerAch payerAch)
        {
            if (payerAch.PayerAchId > 0)
            {
                BaseView.PaymentMethodsControl.BankAccountNumberText = payerAch.BankAccountNumber.MaskAccountNumber();
                BaseView.PaymentMethodsControl.RoutingNumberText = payerAch.BankRoutingNumber;
            }
            else
            {
                BaseView.PaymentMethodsControl.BankAccountNumberText = String.Empty;
                BaseView.PaymentMethodsControl.RoutingNumberText = String.Empty;
            }
        }

        protected virtual void BuildCreditCard(PayerCreditCard payerCreditCard, int renterId)
        {
            if (payerCreditCard.PayerCreditCardId > 0)
            {
                BaseView.PaymentMethodsControl.NameOnCardText = payerCreditCard.CreditCardHolderName;
                BaseView.PaymentMethodsControl.CreditCardNumberText = payerCreditCard.CreditCardAccountNumber.MaskAccountNumber();
                BaseView.PaymentMethodsControl.ExpirationDateText = payerCreditCard.CreditCardExpirationDate;
            }
            else
            {
                BaseView.PaymentMethodsControl.NameOnCardText = String.Empty;
                BaseView.PaymentMethodsControl.CreditCardNumberText = String.Empty;
                BaseView.PaymentMethodsControl.ExpirationDateText = String.Empty;
            }
        }
    }
}
