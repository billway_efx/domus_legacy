﻿using EfxFramework.Interfaces.UserControls.Payment;
using EfxFramework.Interfaces.UserControls.PaymentSteps;

namespace EfxFramework.Presenters.UserControls.PaymentSteps
{
    internal class RenterOneTimePaymentStepOne : OneTimePaymentStepOneBase
    {
        private readonly IRenterOneTimePaymentStepOne _View;

        public RenterOneTimePaymentStepOne(IRenterOneTimePaymentStepOne view)
            : base(view)
        {
            _View = view;
        }

        protected override void BuildCreditCard(PayerCreditCard payerCreditCard, int renterId)
        {
            var PaymentMethods = _View.PaymentMethodsControl as IRenterSitePaymentMethods;
            if (PaymentMethods == null)
                return;

            var Renter = new EfxFramework.Renter();
            var Property = new EfxFramework.Property();

            if (renterId > 0)
                Renter = new EfxFramework.Renter(renterId);

            if (Renter.IsValid)
                Property = EfxFramework.Property.GetPropertyByRenterId(Renter.RenterId);

            if (Property.ProgramId.HasValue && Property.ProgramId.Value == EfxFramework.Property.Program.PropertyPaidProgram)
            {
                PaymentMethods.ConvenienceFeeVisible = false;
            }
            else
            {
                PaymentMethods.ConvenienceFeeVisible = true;

                if (payerCreditCard.PayerCreditCardId > 0)
                    PaymentMethods.ConvenienceFeeText = Property.PropertyId > 0 ? EfxFramework.Property.GetConvenienceFeesByProperty(Property).CreditCardFee.ToString("C") : "0";
                else
                    PaymentMethods.ConvenienceFeeText = Property.PropertyId > 0 ? EfxFramework.Property.GetConvenienceFeesByProperty(Property).CreditCardFee.ToString("C") : "0";
            }

            base.BuildCreditCard(payerCreditCard, renterId);
        }
    }
}
