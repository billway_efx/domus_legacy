﻿using EfxFramework.Interfaces.UserControls.PaymentSteps;

namespace EfxFramework.Presenters.UserControls.PaymentSteps
{
    internal class PropertyStaffOneTimePaymentStepOne : OneTimePaymentStepOneBase
    {
// ReSharper disable NotAccessedField.Local
        private readonly IPropertyStaffOneTimePaymentStepOne _View;
// ReSharper restore NotAccessedField.Local

        public PropertyStaffOneTimePaymentStepOne(IPropertyStaffOneTimePaymentStepOne view)
            : base(view)
        {
            _View = view;
        }
    }
}
