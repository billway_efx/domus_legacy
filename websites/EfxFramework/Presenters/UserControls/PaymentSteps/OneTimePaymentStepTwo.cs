﻿using EfxFramework.Interfaces.UserControls.PaymentSteps;
using EfxFramework.Resources;

namespace EfxFramework.Presenters.UserControls.PaymentSteps
{
    public class OneTimePaymentStepTwo
    {
        private readonly IOneTimePaymentStepTwo _View;

        public OneTimePaymentStepTwo(IOneTimePaymentStepTwo view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            _View.TermsAndConditionsText = HtmlTemplates.TermsAndConditions;
        }
    }
}
