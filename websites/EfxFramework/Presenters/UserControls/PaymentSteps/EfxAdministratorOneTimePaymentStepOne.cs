﻿using EfxFramework.Interfaces.UserControls.PaymentSteps;

namespace EfxFramework.Presenters.UserControls.PaymentSteps
{
    internal class EfxAdministratorOneTimePaymentStepOne : OneTimePaymentStepOneBase
    {
// ReSharper disable NotAccessedField.Local
        private readonly IEfxAdministratorOneTimePaymentStepOne _View;
// ReSharper restore NotAccessedField.Local

        public EfxAdministratorOneTimePaymentStepOne(IEfxAdministratorOneTimePaymentStepOne view)
            : base(view)
        {
            _View = view;
        }
    }
}
