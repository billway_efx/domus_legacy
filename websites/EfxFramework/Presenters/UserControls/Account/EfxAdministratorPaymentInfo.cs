﻿using EfxFramework.Interfaces.UserControls.Account;
using System;
using System.Web;

namespace EfxFramework.Presenters.UserControls.Account
{
    internal class EfxAdministratorPaymentInfo : PaymentInfoBase<EfxAdministrator>
    {
// ReSharper disable NotAccessedField.Local
        private IEfxAdministratorPaymentInfo _View;
// ReSharper restore NotAccessedField.Local

        protected override EfxFramework.Renter SelectedRenter
        {
            get
            {
                var RenterId = 0;

                if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["renterId"]))
                    Int32.TryParse(HttpContext.Current.Request.QueryString["renterId"], out RenterId);

                return new EfxFramework.Renter(RenterId);
            }
        }

        public EfxAdministratorPaymentInfo(IEfxAdministratorPaymentInfo view)
            : base(view)
        {
            _View = view;
        }
    }
}
