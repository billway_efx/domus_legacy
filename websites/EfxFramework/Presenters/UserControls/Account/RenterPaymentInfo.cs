﻿using EfxFramework.Interfaces.UserControls.Account;

namespace EfxFramework.Presenters.UserControls.Account
{
    internal class RenterPaymentInfo : PaymentInfoBase<EfxFramework.Renter>
    {
// ReSharper disable NotAccessedField.Local
        private IRenterPaymentInfo _View;
// ReSharper restore NotAccessedField.Local

        protected override EfxFramework.Renter SelectedRenter { get { return CurrentUser as EfxFramework.Renter; } }

        public RenterPaymentInfo(IRenterPaymentInfo view)
            : base(view)
        {
            _View = view;
        }
    }
}
