﻿using EfxFramework.Interfaces.UserControls.Account;

namespace EfxFramework.Presenters.UserControls.Account
{
    public static class PaymentInfoFactory
    {
        public static PaymentInfoBase<EfxFramework.Renter> GetPaymentInfo(IRenterPaymentInfo view)
        {
            return new RenterPaymentInfo(view);
        }

        public static PaymentInfoBase<EfxAdministrator> GetPaymentInfo(IEfxAdministratorPaymentInfo view)
        {
            return new EfxAdministratorPaymentInfo(view);
        }

        public static PaymentInfoBase<PropertyStaff> GetPaymentInfo(IPropertyStaffPaymentInfo view)
        {
            return new PropertyStaffPaymentInfo(view);
        }
    }
}
