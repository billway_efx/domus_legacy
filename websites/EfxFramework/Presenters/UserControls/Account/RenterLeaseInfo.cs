﻿using EfxFramework.Interfaces.UserControls.Account;
using System;

namespace EfxFramework.Presenters.UserControls.Account
{
    internal class RenterLeaseInfo : LeaseInfoBase<EfxFramework.Renter>
    {
// ReSharper disable NotAccessedField.Local
        private readonly IRenterLeaseInfo _View;
// ReSharper restore NotAccessedField.Local

        protected override EfxFramework.Renter SelectedRenter { get { return CurrentUser as EfxFramework.Renter; } }

        public RenterLeaseInfo(IRenterLeaseInfo view)
            : base(view)
        {
            _View = view;
        }

        protected override void BindPropertyInfoTile(EfxFramework.Property property)
        {
            base.BindPropertyInfoTile(property);

            var PropertyId = 0;

            if (property.PropertyId.HasValue)
                PropertyId = property.PropertyId.Value;

            BaseView.SelectedPropertyPhotoUrl = String.Format("~/Handlers/PropertyManagerHandler.ashx?RequestType=0&photoIndex=1&propertyId={0}", PropertyId);
        }

        protected override void BindLeasingAgentInfoTile(int leasingAgentId)
        {
            var Staff = new PropertyStaff(leasingAgentId);
            var StaffId = 0;

            if (Staff.PropertyStaffId.HasValue)
                StaffId = Staff.PropertyStaffId.Value;

            base.BindLeasingAgentInfoTile(Staff);
            BaseView.SelectedLeasingAgentPhotoUrl = String.Format("~/Handlers/PropertyManagerHandler.ashx?RequestType=0&propertyStaffId={0}", StaffId);
        }

        protected override void ClearLeasingAgentTile()
        {
            base.ClearLeasingAgentTile();
            BaseView.SelectedLeasingAgentPhotoUrl = "/Handlers/PropertyManagerHandler.ashx?RequestType=0&propertyStaffId=0";
        }

        protected override void ClearPropertyTile()
        {
            base.ClearPropertyTile();
            BaseView.SelectedPropertyPhotoUrl = "~/Handlers/PropertyManagerHandler.ashx?RequestType=0&photoIndex=1&propertyId=0";
        }
    }
}
