﻿using EfxFramework.ExtensionMethods;
using EfxFramework.Interfaces.UserControls.Account;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Web.UI.WebControls;

namespace EfxFramework.Presenters.UserControls.Account
{
    public abstract class LeaseInfoBase<T> where T : BaseUser, new()
    {
        #region Properties and Variables
        private BaseUser _CurrentUser;

        protected abstract EfxFramework.Renter SelectedRenter { get; }
        protected readonly ILeaseInfo BaseView;

        protected BaseUser CurrentUser
        {
            get
            {
                if (_CurrentUser == null)
                {
                    int UserId;
                    _CurrentUser = new T();

                    if (Int32.TryParse(HttpContext.Current.User.Identity.Name, out UserId))
                        _CurrentUser = _CurrentUser.GetUserById(UserId);
                }

                return _CurrentUser;
            }
        }
        #endregion

        #region Constructors
        protected LeaseInfoBase(ILeaseInfo view)
        {
            BaseView = view;
            BaseView.PropertySelected = PropertySelected;
            BaseView.LeasingAgentSelected = LeasingAgentSelected;
            BaseView.SaveButtonClicked = SaveLeaseInfo;
        }
        #endregion

        #region Initializers
        public virtual void InitializeValues()
        {
            BindPropertyInfo();
            BindLeasingAgentInfo();
            BindLeaseInfo();
        }

        protected virtual void BindPropertyInfo()
        {
            BindPropertyInfo(EfxFramework.Property.GetAllPropertyList());
        }

        protected virtual void BindPropertyInfo(List<EfxFramework.Property> propertyList)
        {
            ClearPropertyTile();
            BaseView.PropertyList.Items.Clear();

            if (propertyList.Count > 0)
            {
                BaseView.PropertyList.DataSource = propertyList;
                BaseView.PropertyList.DataBind();
                BaseView.PropertyList.Items.Insert(0, new ListItem("-- Select Property --", "-1"));
            }
            else
                BaseView.PropertyList.Items.Insert(0, new ListItem("-- No Properties Available --", "-1"));

            if (SelectedRenter == null || SelectedRenter.RenterId < 1)
                return;

            var Property = EfxFramework.Property.GetPropertyByRenterId(SelectedRenter.RenterId);

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                return;

            try
            {
                BaseView.PropertyList.SelectedValue = Property.PropertyId.Value.ToString(CultureInfo.InvariantCulture);
            }
            catch (ArgumentOutOfRangeException)
            {
                BaseView.PropertyList.SelectedIndex = 0;
            }

            BindPropertyInfoTile(Property);
        }

        protected virtual void BindLeasingAgentInfo()
        {
            var PropertyId = BaseView.PropertyList.SelectedValue.ToInt32();

            if (PropertyId < 1)
                return;

            var StaffList = PropertyStaff.GetPropertyStaffListByPropertyId(PropertyId);

            ClearLeasingAgentTile();
            BaseView.LeasingAgentList.Items.Clear();

            if (StaffList.Count > 0)
            {
                BaseView.LeasingAgentList.DataSource = StaffList;
                BaseView.LeasingAgentList.DataBind();
                BaseView.LeasingAgentList.Items.Insert(0, new ListItem("-- Select Leasing Agent --", "-1"));
            }
            else
                BaseView.LeasingAgentList.Items.Clear();

            if (SelectedRenter == null || SelectedRenter.RenterId < 1)
                return;

            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(SelectedRenter.RenterId);

            if (Lease.LeaseId < 1 || Lease.PropertyStaffId < 1)
                return;

            try
            {
                BaseView.LeasingAgentList.SelectedValue = Lease.PropertyStaffId.ToString(CultureInfo.InvariantCulture);
            }
            catch (ArgumentOutOfRangeException)
            {
                BaseView.LeasingAgentList.SelectedIndex = 0;
            }

            if (Lease.PropertyId == PropertyId)
                BindLeasingAgentInfoTile(Lease.PropertyStaffId);
        }

        protected virtual void BindLeaseInfo()
        {
            if (SelectedRenter == null || SelectedRenter.RenterId < 1)
                return;

            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(SelectedRenter.RenterId);

            if (Lease.LeaseId < 1)
                return;

            BaseView.RentAmountText = Lease.RentAmount.ToString("C");
            BaseView.DueDayOfMonthText = Lease.RentDueDayOfMonth.ToString(CultureInfo.InvariantCulture);
            BaseView.LeaseStartText = Lease.StartDate.ToShortDateString();
            BaseView.LeaseEndText = Lease.EndDate.ToShortDateString();
            BaseView.UnitNumberText = Lease.UnitNumber;
            BaseView.NumberOfPeopleOnLeaseText = Lease.NumberOfTenants.ToString(CultureInfo.InvariantCulture);
        }

        protected virtual void ClearPropertyTile()
        {
            BaseView.SelectedPropertyName = String.Empty;
            BaseView.SelectedPropertyAddress = String.Empty;
            BaseView.SelectedPropertyPhone = String.Empty;
        }

        protected virtual void ClearLeasingAgentTile()
        {
            BaseView.SelectedLeasingAgentName = String.Empty;
            BaseView.SelectedLeasingAgentMainPhone = String.Empty;
            BaseView.SelectedLeasingAgentCellPhone = String.Empty;
            BaseView.SelectedLeasingAgentEmail = String.Empty;
        }

        protected virtual void BindPropertyInfoTile(EfxFramework.Property property)
        {
            if (!property.PropertyId.HasValue || property.PropertyId.Value < 1)
                return;

            BaseView.SelectedPropertyName = property.PropertyName;
            BaseView.SelectedPropertyAddress = property.DisplayAddress;
            BaseView.SelectedPropertyPhone = property.MainPhoneNumber.ToString(false).FormatPhoneNumber();
        }

        protected virtual void BindLeasingAgentInfoTile(int leasingAgentId)
        {
            var Staff = new PropertyStaff(leasingAgentId);

            if (!Staff.PropertyStaffId.HasValue || Staff.PropertyStaffId.Value < 1)
                return;

            BindLeasingAgentInfoTile(Staff);
        }

        protected virtual void BindLeasingAgentInfoTile(PropertyStaff staff)
        {
            BaseView.SelectedLeasingAgentName = staff.DisplayName;
            BaseView.SelectedLeasingAgentMainPhone = staff.MainPhoneNumber.FormatPhoneNumber();
            BaseView.SelectedLeasingAgentCellPhone = staff.MobilePhoneNumber.FormatPhoneNumber();
            BaseView.SelectedLeasingAgentEmail = staff.PrimaryEmailAddress;
        }
        #endregion

        #region Event Handlers
        protected virtual void PropertySelected(object sender, EventArgs e)
        {
            var Property = new EfxFramework.Property(BaseView.PropertyList.SelectedValue.ToInt32());

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                ClearPropertyTile();
                return;
            }

            BindPropertyInfoTile(Property);
            BindLeasingAgentInfo();
        }

        protected virtual void LeasingAgentSelected(object sender, EventArgs e)
        {
            var LeasingAgentId = BaseView.LeasingAgentList.SelectedValue.ToInt32();

            if (LeasingAgentId > 0)
                BindLeasingAgentInfoTile(LeasingAgentId);
            else
                ClearLeasingAgentTile();
        }

        protected virtual void SaveLeaseInfo(object sender, EventArgs e)
        {
            try
            {
                if (!ValidatePage())
                    return;

                var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(SelectedRenter.RenterId) ?? new Lease();
                var PropertyId = BaseView.PropertyList.SelectedValue.ToInt32();

                if (PropertyId > 0)
                {
                    EfxFramework.Renter.AssignRenterToProperty(PropertyId, SelectedRenter.RenterId);
                    SelectedRenter.PmsPropertyId = new EfxFramework.Property(PropertyId).PmsId;
                    EfxFramework.Renter.Set(SelectedRenter, "System Association");
                    Lease.PropertyId = PropertyId;
                }

                Lease.UnitNumber = BaseView.UnitNumberText;
                Lease.RentAmount = GetDecimalValue(BaseView.RentAmountText.Replace("$", ""));
                Lease.RentDueDayOfMonth = GetIntValue(BaseView.DueDayOfMonthText);
                Lease.StartDate = GetDateTimeValue(BaseView.LeaseStartText);
                Lease.EndDate = GetDateTimeValue(BaseView.LeaseEndText);
                Lease.BeginningBalance = 0.00M;
                Lease.BeginningBalanceDate = Lease.StartDate;
                Lease.NumberOfTenants = GetIntValue(BaseView.NumberOfPeopleOnLeaseText);

                if (BaseView.LeasingAgentList.SelectedValue.ToInt32() > 0)
                    Lease.PropertyStaffId = BaseView.LeasingAgentList.SelectedValue.ToInt32();

                Lease.AssignRenterToLease(EfxFramework.Lease.Set(Lease), SelectedRenter.RenterId);
                BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Success, "Successfully updated lease information for renter.");
            }
            catch
            {
                BaseView.ParentPage.SafeRedirect("~/ErrorPage.html");
            }
        }
        #endregion

        #region Validation
        protected virtual bool ValidatePage()
        {
            if (SelectedRenter == null || SelectedRenter.RenterId < 1)
            {
                BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "No valid renter selected.");
                return false;
            }

            if (BaseView.PropertyList.SelectedValue.ToInt32() == -1 && BaseView.PropertyList.Items.Count > 1)
            {
                BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Valid property selection is required.");
                return false;
            }

            if (BaseView.LeasingAgentList.SelectedValue.ToInt32() == -1 && BaseView.LeasingAgentList.Items.Count > 1)
            {
                BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Valid leasing agent selection is required.");
                return false;
            }

            if (String.IsNullOrEmpty(BaseView.UnitNumberText))
            {
                BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Unit number is required.");
                return false;
            }

            if (String.IsNullOrEmpty(BaseView.RentAmountText))
            {
                BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Rent amount is required.");
                return false;
            }

            if (String.IsNullOrEmpty(BaseView.DueDayOfMonthText))
            {
                BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Rent due day is required.");
                return false;
            }

            if (String.IsNullOrEmpty(BaseView.LeaseStartText))
            {
                BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Lease start date is required.");
                return false;
            }

            if (String.IsNullOrEmpty(BaseView.LeaseEndText))
            {
                BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Lease end date is required.");
                return false;
            }

            if (String.IsNullOrEmpty(BaseView.NumberOfPeopleOnLeaseText))
            {
                BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Number of people on lease is required.");
                return false;
            }

            return true;
        }
        #endregion

        #region Helper Methods
        protected virtual decimal GetDecimalValue(string value)
        {
            decimal Result;

            if (!Decimal.TryParse(value, out Result))
                Result = 0.0M;

            return Result;
        }

        protected virtual int GetIntValue(string value)
        {
            int Result;

            if (!Int32.TryParse(value, out Result))
                Result = 0;

            return Result;
        }

        protected virtual DateTime GetDateTimeValue(string value)
        {
            DateTime Result;

            if (!DateTime.TryParse(value, out Result))
                Result = new DateTime(2000, 1, 1);

            return Result;
        }
        #endregion
    }
}
