﻿using EfxFramework.Interfaces.UserControls.Account;
using System;
using System.Web;

namespace EfxFramework.Presenters.UserControls.Account
{
    internal class PropertyStaffLeaseInfo : LeaseInfoBase<PropertyStaff>
    {
// ReSharper disable NotAccessedField.Local
        private IPropertyStaffLeaseInfo _View;
// ReSharper restore NotAccessedField.Local
        private PropertyStaff CurrentStaff { get { return (CurrentUser as PropertyStaff); } }

        protected override EfxFramework.Renter SelectedRenter
        {
            get
            {
                var RenterId = 0;

                if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["renterId"]))
                    Int32.TryParse(HttpContext.Current.Request.QueryString["renterId"], out RenterId);

                return new EfxFramework.Renter(RenterId);
            }
        }

        public PropertyStaffLeaseInfo(IPropertyStaffLeaseInfo view)
            : base(view)
        {
            _View = view;
        }

        protected override void BindPropertyInfo()
        {
            var StaffId = 0;

            if (CurrentStaff != null && CurrentStaff.PropertyStaffId.HasValue)
                StaffId = CurrentStaff.PropertyStaffId.Value;

            base.BindPropertyInfo(EfxFramework.Property.GetPropertyListByPropertyStaffId(StaffId));
        }

        protected override void BindPropertyInfoTile(EfxFramework.Property property)
        {
            base.BindPropertyInfoTile(property);

            var PropertyId = 0;

            if (property.PropertyId.HasValue)
                PropertyId = property.PropertyId.Value;

            BaseView.SelectedPropertyPhotoUrl = String.Format("~/Handlers/PropertyManagerHandler.ashx?RequestType=0&photoIndex=1&propertyId={0}", PropertyId);
        }

        protected override void BindLeasingAgentInfoTile(int leasingAgentId)
        {
            var Staff = new PropertyStaff(leasingAgentId);
            var StaffId = 0;

            if (Staff.PropertyStaffId.HasValue)
                StaffId = Staff.PropertyStaffId.Value;

            base.BindLeasingAgentInfoTile(Staff);
            BaseView.SelectedLeasingAgentPhotoUrl = String.Format("~/Handlers/PropertyManagerHandler.ashx?RequestType=0&propertyStaffId={0}", StaffId);
        }

        protected override void ClearLeasingAgentTile()
        {
            base.ClearLeasingAgentTile();
            BaseView.SelectedLeasingAgentPhotoUrl = "/Handlers/PropertyManagerHandler.ashx?RequestType=0&propertyStaffId=0";
        }

        protected override void ClearPropertyTile()
        {
            base.ClearPropertyTile();
            BaseView.SelectedPropertyPhotoUrl = "~/Handlers/PropertyManagerHandler.ashx?RequestType=0&photoIndex=1&propertyId=0";
        }
    }
}
