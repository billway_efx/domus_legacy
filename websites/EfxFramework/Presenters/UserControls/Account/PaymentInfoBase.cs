﻿using EfxFramework.ExtensionMethods;
using EfxFramework.Interfaces.UserControls.Account;
using EfxFramework.PaymentMethods.Ach;
using EfxFramework.Web;
using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace EfxFramework.Presenters.UserControls.Account
{
    public abstract class PaymentInfoBase<T> where T : BaseUser, new()
    {
        #region Properties and Variables
        private BaseUser _CurrentUser;

        protected abstract EfxFramework.Renter SelectedRenter { get; }
        protected readonly IPaymentInfo BaseView;

        protected BaseUser CurrentUser
        {
            get
            {
                if (_CurrentUser == null)
                {
                    int UserId;
                    _CurrentUser = new T();

                    if (Int32.TryParse(HttpContext.Current.User.Identity.Name, out UserId))
                        _CurrentUser = _CurrentUser.GetUserById(UserId);
                }

                return _CurrentUser;
            }
        }

        protected PayerAch ECheckPayer
        {
            get
            {
                if (SelectedRenter == null || !SelectedRenter.PayerId.HasValue || SelectedRenter.PayerId.Value < 1)
                    return new PayerAch();

                return PayerAch.GetPrimaryPayerAchByPayerId(SelectedRenter.PayerId.Value);
            }
        }

        protected PayerCreditCard CreditCardPayer
        {
            get
            {
                if (SelectedRenter == null || !SelectedRenter.PayerId.HasValue || SelectedRenter.PayerId.Value < 1)
                    return new PayerCreditCard();

                return PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(SelectedRenter.PayerId.Value);
            }
        }
        #endregion

        #region Constructors
        protected PaymentInfoBase(IPaymentInfo view)
        {
            BaseView = view;
            BaseView.PaymentInfoPreRender = PreRenderUpdatePanel;
            BaseView.PaymentMethodSelectionChanged = PaymentMethodChanged;
            BaseView.SaveButtonClicked = SavePaymentInfo;
            BaseView.EcheckAutoPaymentChanged = EcheckAutopaymentChanged;
            BaseView.CreditCardAutoPaymentChanged = CreditCardAutopaymentChanged;
            BaseView.SmsScheduleChanged = SetSmsSchedule;
        }
        #endregion

        #region Public Methods
        public virtual void IntializeValues()
        {
            SetPayerDetails();
        }
        #endregion

        #region Intialization Methods
        protected virtual void SetPayerDetails()
        {
            //if the renter doesn't have a payer yet, make the renter the payer
            if (!SelectedRenter.PayerId.HasValue || SelectedRenter.PayerId.Value <= 0)
                SelectedRenter.PayerId = EfxFramework.Payer.SetPayerFromRenter(SelectedRenter).PayerId;

            var AutoPayment = EfxFramework.AutoPayment.GetAutoPaymentByRenterId(SelectedRenter.RenterId);

            BindEcheckData(AutoPayment);
            BindCreditCardData(AutoPayment);
            BindSmsData();
            SetAutoPaymentDetails();
        }

        protected virtual void BindEcheckData(AutoPayment autoPayment)
        {
            if (SelectedRenter == null || SelectedRenter.RenterId < 1)
                return;

            if ((autoPayment.AutoPaymentId > 0 && autoPayment.PaymentTypeId == 2) && autoPayment.PaymentDayOfMonth > 0)
            {
                BaseView.EcheckAutoPaymentChecked = true;
                BaseView.DayOfEachMonthText = autoPayment.PaymentDayOfMonth.ToString(CultureInfo.InvariantCulture);
                BindAutoPaymentAmounts(autoPayment);
            }

            BaseView.BankAccountNumberText = ECheckPayer.BankAccountNumber.MaskAccountNumber();
            BaseView.RoutingNumberText = ECheckPayer.BankRoutingNumber;
            BaseView.IsPrimaryCheckingAccount = ECheckPayer.IsPrimary;
        }

        protected virtual void BindCreditCardData(AutoPayment autoPayment)
        {
            if (SelectedRenter == null || SelectedRenter.RenterId < 1)
                return;

            if ((autoPayment.AutoPaymentId > 0 && autoPayment.PaymentTypeId == 1) && autoPayment.PaymentDayOfMonth > 0)
            {
                BaseView.CreditCardAutoPaymentChecked = true;
                BaseView.DayOfEachMonthText = autoPayment.PaymentDayOfMonth.ToString(CultureInfo.InvariantCulture);
                BindAutoPaymentAmounts(autoPayment);
            }

            BaseView.NameOnCardText = CreditCardPayer.CreditCardHolderName;
            BaseView.CreditCardNumberText = CreditCardPayer.CreditCardAccountNumber.MaskAccountNumber();
            BaseView.ExpirationDateText = CreditCardPayer.CreditCardExpirationDate;
            BaseView.IsPrimaryCreditCard = CreditCardPayer.IsPrimary;
        }

        protected virtual void BindSmsData()
        {
            BaseView.CarrierDropdownList.Items.Clear();
            BaseView.CarrierDropdownList.BindToSortedEnum(typeof(Carrier));

            if (SelectedRenter == null || SelectedRenter.RenterId < 1)
                return;

            if (SelectedRenter.SmsReminderDayOfMonth.HasValue && SelectedRenter.SmsReminderDayOfMonth.Value > 0)
            {
                BaseView.SmsReminderChecked = true;
                BaseView.SmsReminderDay = SelectedRenter.SmsReminderDayOfMonth.Value.ToString(CultureInfo.InvariantCulture);
            }

            BaseView.SmsCellNumberText = SelectedRenter.SmsPaymentPhoneNumber;
            BaseView.SmsPaymentMethodSelectedValue = SelectedRenter.SmsPaymentTypeId.HasValue ? SelectedRenter.SmsPaymentTypeId.Value.ToString(CultureInfo.InvariantCulture) : "-1";
            BaseView.CarrierDropdownList.SelectedValue = SelectedRenter.SmsPaymentCarrierId.HasValue ? SelectedRenter.SmsPaymentCarrierId.Value.ToString(CultureInfo.InvariantCulture) : "-1";
        }

        protected virtual void SetAutoPaymentDetails()
        {
            if (SelectedRenter == null || SelectedRenter.RenterId < 1)
                return;

            switch (BaseView.PaymentMethodSelectedValue)
            {
                case "echeck":
                    BaseView.PaymentMultiView.SetActiveView(BaseView.EcheckPaymentView);
                    SetEcheckPaymentDetails();
                    break;

                case "credit":
                    BaseView.PaymentMultiView.SetActiveView(BaseView.CreditCardPaymentView);
                    SetCreditCardAutoPaymentDetails();
                    break;

                case "text":
                    BaseView.PaymentMultiView.SetActiveView(BaseView.SmsPaymentView);
                    SetSmsPaymentDetails();
                    break;

                default:
                    BaseView.PaymentMultiView.SetActiveView(BaseView.EcheckPaymentView);
                    SetEcheckPaymentDetails();
                    break;
            }
        }

        protected virtual void BindAutoPaymentAmounts(AutoPayment autoPayment)
        {
            var CharityId = -1;

            if (!String.IsNullOrEmpty(autoPayment.CharityName))
            {
                foreach (var Charity in EfxFramework.Charity.GetAllCharityList().Where(charity => charity.CharityName == autoPayment.CharityName))
                {
                    CharityId = Charity.CharityId;
                }
            }

            BaseView.AmountControl.RentAmountText = autoPayment.RentAmount.HasValue ? autoPayment.RentAmount.Value.ToString("C") : String.Empty;
            BaseView.AmountControl.RentDescriptionText = autoPayment.RentAmountDescription;
            BaseView.AmountControl.OtherAmount1Text = autoPayment.OtherAmount1.HasValue ? autoPayment.OtherAmount1.Value.ToString("C") : String.Empty;
            BaseView.AmountControl.OtherDescription1Text = autoPayment.OtherDescription1;
            BaseView.AmountControl.OtherAmount2Text = autoPayment.OtherAmount2.HasValue ? autoPayment.OtherAmount2.Value.ToString("C") : String.Empty;
            BaseView.AmountControl.OtherDescription2Text = autoPayment.OtherDescription2;
            BaseView.AmountControl.OtherAmount3Text = autoPayment.OtherAmount3.HasValue ? autoPayment.OtherAmount3.Value.ToString("C") : String.Empty;
            BaseView.AmountControl.OtherDescription3Text = autoPayment.OtherDescription3;
            BaseView.AmountControl.OtherAmount4Text = autoPayment.OtherAmount4.HasValue ? autoPayment.OtherAmount4.Value.ToString("C") : String.Empty;
            BaseView.AmountControl.OtherDescription4Text = autoPayment.OtherDescription4;
            BaseView.AmountControl.OtherAmount5Text = autoPayment.OtherAmount5.HasValue ? autoPayment.OtherAmount5.Value.ToString("C") : String.Empty;
            BaseView.AmountControl.OtherDescription5Text = autoPayment.OtherDescription5;
            BaseView.CharityControl.DonationAmountText = autoPayment.CharityAmount.HasValue ? autoPayment.CharityAmount.Value.ToString("C") : String.Empty;
            BaseView.CharityControl.SelectedCharityValue = CharityId.ToString(CultureInfo.InvariantCulture);
        }

        protected virtual void SetEcheckPaymentDetails()
        {
            BaseView.AutoPaymentAmountVisible = BaseView.EcheckAutoPaymentChecked;
            BaseView.AutoDebitPanelVisible = BaseView.EcheckAutoPaymentChecked;
        }

        protected virtual void SetCreditCardAutoPaymentDetails()
        {
            var Property = EfxFramework.Property.GetPropertyByRenterId(SelectedRenter.RenterId);

            if (!Property.PropertyId.HasValue || Property.PropertyId < 1)
            {
                DisableCreditCardAutoPayment();
            }
            else if (!Property.ProgramId.HasValue || Property.ProgramId.Value != EfxFramework.Property.Program.PropertyPaidProgram)
            {
                DisableCreditCardAutoPayment();
            }
            else
            {
                BaseView.CreditCardAutoPaymentCheckboxVisible = true;
                BaseView.AutoDebitPanelVisible = BaseView.CreditCardAutoPaymentChecked;
                BaseView.AutoPaymentAmountVisible = BaseView.CreditCardAutoPaymentChecked;
            }
        }

        protected virtual void DisableCreditCardAutoPayment()
        {
            BaseView.CreditCardAutoPaymentCheckboxVisible = false;
            BaseView.AutoPaymentAmountVisible = false;
            BaseView.AutoDebitPanelVisible = false;
        }

        protected virtual void SetSmsPaymentDetails()
        {
            BaseView.AutoDebitPanelVisible = false;
            BaseView.AutoPaymentAmountVisible = false;
            BaseView.SmsReminderPanelVisible = BaseView.SmsReminderChecked;
        }
        #endregion

        #region Event Handlers
        protected virtual void PreRenderUpdatePanel(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(BaseView.PaymentInfoUpdatePanel, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$(EFXClient.WireUpDatePickers);", true);
                ScriptManager.RegisterStartupScript(BaseView.PaymentInfoUpdatePanel, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$(EFXClient.WireUpValidation);", true);
            }
            catch
            {
                BasePage.SafePageRedirect("~/ErrorPage.html");
            }
        }

        protected virtual void PaymentMethodChanged(object sender, EventArgs e)
        {
            SetAutoPaymentDetails();
        }

        protected virtual void SetSmsSchedule(object sender, EventArgs e)
        {
            BaseView.SmsReminderPanelVisible = BaseView.SmsReminderChecked;
        }

        protected virtual void EcheckAutopaymentChanged(object sender, EventArgs e)
        {
            if (BaseView.EcheckAutoPaymentChecked)
                BaseView.CreditCardAutoPaymentChecked = false;

            SetAutoPaymentDetails();
        }

        protected virtual void CreditCardAutopaymentChanged(object sender, EventArgs e)
        {
            if (BaseView.CreditCardAutoPaymentChecked)
                BaseView.EcheckAutoPaymentChecked = false;

            SetAutoPaymentDetails();
        }

        protected virtual void SavePaymentInfo(object sender, EventArgs e)
        {
            if (!ValidatePage())
                return;

            if (!SelectedRenter.PayerId.HasValue || SelectedRenter.PayerId.Value < 1)
            {
                SelectedRenter.PayerId = EfxFramework.Payer.SetPayerFromRenter(SelectedRenter).PayerId;
                EfxFramework.Renter.Set(SelectedRenter, String.Format("{0}-{1}{2}", CurrentUser.PrimaryEmailAddress, CurrentUser.FirstName, CurrentUser.LastName));
            }

            UpdatePayerAch();
            UpdatePayerCreditCard();
            UpdateAutoPayment();
            UpdateSmsInformation();

            BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Success, "Your Payment Information has been successfully saved.");
        }
        #endregion

        #region Validation Methods
        protected virtual bool ValidatePage()
        {
            if (SelectedRenter == null || SelectedRenter.RenterId < 1)
            {
                BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Invalid renter account selected.");
                return false;
            }

            if (!String.IsNullOrEmpty(BaseView.RoutingNumberText) && !AchService.ValidateRoutingCheckDigit(BaseView.RoutingNumberText))
            {
                BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Please a valid routing number for your bank account to be Auto Debited.");
                return false;
            }

            if (!String.IsNullOrEmpty(BaseView.ExpirationDateText) && ParseExpirationDate().AddMonths(1) < DateTime.UtcNow)
            {
                BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Credit Card expired or about to expire, please update the expiration date.");
            }

            if (BaseView.CreditCardAutoPaymentChecked || BaseView.EcheckAutoPaymentChecked)
            {
                if (String.IsNullOrEmpty(BaseView.DayOfEachMonthText))
                {
                    int DayOfMonth;

                    if (!Int32.TryParse(BaseView.DayOfEachMonthText, out DayOfMonth))
                        DayOfMonth = 0;

                    if (DayOfMonth < 1 || DayOfMonth > 31)
                    {
                        BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Please enter a valid day of the month for your Auto Debit to occur.");
                        return false;
                    }
                }

                if (BaseView.CreditCardAutoPaymentChecked)
                {
                    if (String.IsNullOrEmpty(BaseView.NameOnCardText) || String.IsNullOrEmpty(BaseView.CreditCardNumberText) || String.IsNullOrEmpty(BaseView.ExpirationDateText))
                    {
                        BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Please fill out all of the credit card information for your credit card to be Auto Debited.");
                        return false;
                    }
                }

                if (BaseView.EcheckAutoPaymentChecked)
                {
                    if (String.IsNullOrEmpty(BaseView.BankAccountNumberText) || String.IsNullOrEmpty(BaseView.RoutingNumberText))
                    {
                        BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Please fill out all of the bank account information for your bank account to be Auto Debited.");
                        return false;
                    }

                    if (!AchService.ValidateRoutingCheckDigit(BaseView.RoutingNumberText))
                    {
                        BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Please a valid routing number for your bank account to be Auto Debited.");
                        return false;
                    }
                }

                if (!HasRentAmount() && (String.IsNullOrEmpty(BaseView.CharityControl.SelectedCharityValue)))
                {
                    BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Please enter at least one payment amount to be Auto Debited.");
                    return false;
                }
            }

            if (BaseView.SmsReminderChecked)
            {
                int ReminderDay;

                if (!Int32.TryParse(BaseView.SmsReminderDay, out ReminderDay))
                    ReminderDay = 0;

                if (ReminderDay < 1 || ReminderDay > 31)
                {
                    BaseView.ParentPage.PageMessage.DisplayMessage(SystemMessageType.Error, "Please select a valid day of the month to have your text message reminder to be sent.");
                    return false;
                }
            }

            return true;
        }

        protected virtual bool HasRentAmount()
        {
            var HasRentValue = !String.IsNullOrEmpty(BaseView.AmountControl.RentAmountText);
            var HasOther1Value = !String.IsNullOrEmpty(BaseView.AmountControl.OtherAmount1Text);
            var HasOther2Value = !String.IsNullOrEmpty(BaseView.AmountControl.OtherAmount2Text);
            var HasOther3Value = !String.IsNullOrEmpty(BaseView.AmountControl.OtherAmount3Text);
            var HasOther4Value = !String.IsNullOrEmpty(BaseView.AmountControl.OtherAmount4Text);
            var HasOther5Value = !String.IsNullOrEmpty(BaseView.AmountControl.OtherAmount5Text);

            return HasRentValue || HasOther1Value || HasOther2Value || HasOther3Value || HasOther4Value || HasOther5Value;
        }

        protected virtual bool OtherAmountsHaveDescription()
        {
            if (!String.IsNullOrEmpty(BaseView.AmountControl.OtherAmount1Text) && String.IsNullOrEmpty(BaseView.AmountControl.OtherDescription1Text))
                return false;
            if (!String.IsNullOrEmpty(BaseView.AmountControl.OtherAmount2Text) && String.IsNullOrEmpty(BaseView.AmountControl.OtherDescription2Text))
                return false;
            if (!String.IsNullOrEmpty(BaseView.AmountControl.OtherAmount3Text) && String.IsNullOrEmpty(BaseView.AmountControl.OtherDescription3Text))
                return false;
            if (!String.IsNullOrEmpty(BaseView.AmountControl.OtherAmount4Text) && String.IsNullOrEmpty(BaseView.AmountControl.OtherDescription4Text))
                return false;
            if (!String.IsNullOrEmpty(BaseView.AmountControl.OtherAmount5Text) && String.IsNullOrEmpty(BaseView.AmountControl.OtherDescription5Text))
                return false;

            return true;
        }
        #endregion

        #region Update Methods
        protected virtual void UpdateAutoPayment()
        {
            var AutoPayment = EfxFramework.AutoPayment.GetAutoPaymentByRenterId(SelectedRenter.RenterId);
            int PaymentType;
            int DayOfMonth;

            if (!Int32.TryParse(BaseView.DayOfEachMonthText, out DayOfMonth))
                DayOfMonth = 0;

            if (BaseView.CreditCardAutoPaymentChecked)
            {
                PaymentType = 1;
            }
            else if (BaseView.EcheckAutoPaymentChecked)
            {
                PaymentType = 2;
            }
            else
            {
                AutoPayment.DeleteAutoPayment(AutoPayment.AutoPaymentId);
                return;
            }

            int? AchId = null;
            int? CreditCardId = null;

            if (ECheckPayer.PayerAchId > 0)
                AchId = ECheckPayer.PayerAchId;

            if (CreditCardPayer.PayerCreditCardId > 0)
                CreditCardId = CreditCardPayer.PayerCreditCardId;

            AutoPayment = new AutoPayment
                {
                    AutoPaymentId = AutoPayment.AutoPaymentId > 0 ? AutoPayment.AutoPaymentId : 0,
                    CharityAmount = GetAmount(BaseView.CharityControl.DonationAmountText), 
                    CharityName = BaseView.CharityControl.SelectedCharityValue != "-1" ? BaseView.CharityControl.SelectedCharityText : null, 
                    RentAmount = GetAmount(BaseView.AmountControl.RentAmountText), 
                    RentAmountDescription = BaseView.AmountControl.RentDescriptionText, 
                    OtherAmount1 = GetAmount(BaseView.AmountControl.OtherAmount1Text), 
                    OtherDescription1 = BaseView.AmountControl.OtherDescription1Text, 
                    OtherAmount2 = GetAmount(BaseView.AmountControl.OtherAmount2Text), 
                    OtherDescription2 = BaseView.AmountControl.OtherDescription2Text, 
                    OtherAmount3 = GetAmount(BaseView.AmountControl.OtherAmount3Text), 
                    OtherDescription3 = BaseView.AmountControl.OtherDescription3Text, 
                    OtherAmount4 = GetAmount(BaseView.AmountControl.OtherAmount4Text), 
                    OtherDescription4 = BaseView.AmountControl.OtherDescription4Text, 
                    OtherAmount5 = GetAmount(BaseView.AmountControl.OtherAmount5Text), 
                    OtherDescription5 = BaseView.AmountControl.OtherDescription5Text, 
                    PayerAchId = AchId, 
                    PayerCreditCardId = CreditCardId,
                    PaymentTypeId = PaymentType,
                    RenterId = SelectedRenter.RenterId, 
                    PayerId = SelectedRenter.PayerId.HasValue ? SelectedRenter.PayerId.Value : 0, 
                    PaymentDayOfMonth = DayOfMonth
                };

            AutoPayment.Set(AutoPayment);
        }

        protected virtual void UpdatePayerAch()
        {
            var AccountNumber = BaseView.BankAccountNumberText.Contains("*") ? ECheckPayer.BankAccountNumber : BaseView.BankAccountNumberText;

            var PayerAch = new PayerAch
                {
                    PayerAchId = ECheckPayer.PayerAchId > 0 ? ECheckPayer.PayerAchId : 0,
                    BankAccountTypeId = 1,
                    PayerId = SelectedRenter.PayerId.HasValue ? SelectedRenter.PayerId.Value : 0,
                    IsPrimary = BaseView.IsPrimaryCheckingAccount,
                    BankRoutingNumber = BaseView.RoutingNumberText,
                    BankAccountNumber = AccountNumber
                };

            ECheckPayer.PayerAchId = PayerAch.Set(PayerAch);
        }

        protected virtual void UpdatePayerCreditCard()
        {
            var AccountNumber = BaseView.CreditCardNumberText.Contains("*") ? CreditCardPayer.CreditCardAccountNumber : BaseView.CreditCardNumberText;
            var ExpirationDate = ParseExpirationDate();

            var PayerCreditCard = new PayerCreditCard
                {
                    PayerCreditCardId = CreditCardPayer.PayerCreditCardId > 0 ? CreditCardPayer.PayerCreditCardId : 0,
                    CreditCardHolderName = BaseView.NameOnCardText,
                    CreditCardAccountNumber = AccountNumber,
                    CreditCardExpirationMonth = ExpirationDate.Month,
                    CreditCardExpirationYear = ExpirationDate.Year,
                    IsPrimary = BaseView.IsPrimaryCreditCard,
                    PayerId = SelectedRenter.PayerId.HasValue ? SelectedRenter.PayerId.Value : 0
                };

            CreditCardPayer.PayerCreditCardId = PayerCreditCard.Set(PayerCreditCard);
        }

        protected virtual void UpdateSmsInformation()
        {
            var CarrierId = Int32.Parse(BaseView.CarrierDropdownList.SelectedValue);
            var PaymentMethodId = Int32.Parse(BaseView.SmsPaymentMethodSelectedValue);

            if (BaseView.SmsReminderChecked)
            {
                int ReminderDay;

                if (!Int32.TryParse(BaseView.SmsReminderDay, out ReminderDay))
                    ReminderDay = 0;

                if (ReminderDay > 0 && ReminderDay < 32)
                {
                    SelectedRenter.SmsReminderDayOfMonth = ReminderDay;

                    var Message = String.Format("This is a confirmation that you have chosen to have a text message to remind you on the {0} day of the month that your rent is due.", 
                        ReminderDay.ToString(CultureInfo.InvariantCulture).ToNthValue());

                    Sms.PaymentRequest.ReplyToSender(Message, SelectedRenter.SmsPaymentPhoneNumber, "1");
                }
            }

            if (CarrierId > 0)
                SelectedRenter.SmsPaymentCarrierId = CarrierId;
            if (PaymentMethodId > 0)
                SelectedRenter.SmsPaymentTypeId = PaymentMethodId;
            if (!String.IsNullOrEmpty(BaseView.SmsCellNumberText))
                SelectedRenter.SmsPaymentPhoneNumber = BaseView.SmsCellNumberText;

            EfxFramework.Renter.Set(SelectedRenter, String.Format("{0}-{1}{2}", CurrentUser.PrimaryEmailAddress, CurrentUser.FirstName, CurrentUser.LastName));
        }
        #endregion

        #region Helper Methods
        protected static decimal? GetAmount(string value)
        {
            decimal Result;

            value = value.Replace("$", "");

            if (Decimal.TryParse(value, out Result))
                return Result;

            return null;
        }

        protected virtual DateTime ParseExpirationDate()
        {
            DateTime ExpirationDate;
            var Month = DateTime.UtcNow.Month;
            var Year = DateTime.UtcNow.Year - 1;

            if (BaseView.ExpirationDateText.Length == 4)
            {
                Int32.TryParse(BaseView.ExpirationDateText.Substring(0, 2), out Month);
                Int32.TryParse(String.Format("20{0}", BaseView.ExpirationDateText.Substring(2, 2)), out Year);

                ExpirationDate = new DateTime(Year, Month, 1);
            }
            else
            {
                if (!DateTime.TryParse(BaseView.ExpirationDateText, out ExpirationDate))
                    ExpirationDate = new DateTime(Year, Month, 1);
            }

            return ExpirationDate;
        }
        #endregion
    }
}
