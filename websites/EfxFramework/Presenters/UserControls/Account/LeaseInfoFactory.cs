﻿using EfxFramework.Interfaces.UserControls.Account;

namespace EfxFramework.Presenters.UserControls.Account
{
    public static class LeaseInfoFactory
    {
        public static LeaseInfoBase<EfxFramework.Renter> GetLeaseInfo(IRenterLeaseInfo view)
        {
            return new RenterLeaseInfo(view);
        }

        public static LeaseInfoBase<PropertyStaff> GetLeaseInfo(IPropertyStaffLeaseInfo view)
        {
            return new PropertyStaffLeaseInfo(view);
        }

        public static LeaseInfoBase<EfxAdministrator> GetLeaseInfo(IEfxAdministratorLeaseInfo view)
        {
            return new EfxAdministratorLeaseInfo(view);
        }
    }
}
