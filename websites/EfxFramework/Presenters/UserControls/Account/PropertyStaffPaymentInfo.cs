﻿using EfxFramework.Interfaces.UserControls.Account;
using System;
using System.Web;

namespace EfxFramework.Presenters.UserControls.Account
{
    internal class PropertyStaffPaymentInfo : PaymentInfoBase<PropertyStaff>
    {
// ReSharper disable NotAccessedField.Local
        private IPropertyStaffPaymentInfo _View;
// ReSharper restore NotAccessedField.Local

        protected override EfxFramework.Renter SelectedRenter
        {
            get
            {
                var RenterId = 0;

                if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["renterId"]))
                    Int32.TryParse(HttpContext.Current.Request.QueryString["renterId"], out RenterId);

                return new EfxFramework.Renter(RenterId);
            }
        }

        public PropertyStaffPaymentInfo(IPropertyStaffPaymentInfo view)
            : base(view)
        {
            _View = view;
        }
    }
}
