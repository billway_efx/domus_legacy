﻿using EfxFramework.Interfaces.UserControls.Account;
using System;
using System.Web;

namespace EfxFramework.Presenters.UserControls.Account
{
    internal class EfxAdministratorLeaseInfo : LeaseInfoBase<EfxAdministrator>
    {
// ReSharper disable NotAccessedField.Local
        private IEfxAdministratorLeaseInfo _View;
// ReSharper restore NotAccessedField.Local

        protected override EfxFramework.Renter SelectedRenter
        {
            get
            {
                var RenterId = 0;

                if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["renterId"]))
                    Int32.TryParse(HttpContext.Current.Request.QueryString["renterId"], out RenterId);

                return new EfxFramework.Renter(RenterId);
            }
        }

        public EfxAdministratorLeaseInfo(IEfxAdministratorLeaseInfo view)
            : base(view)
        {
            _View = view;
        }

        protected override void BindPropertyInfoTile(EfxFramework.Property property)
        {
            base.BindPropertyInfoTile(property);

            var PropertyId = 0;

            if (property.PropertyId.HasValue)
                PropertyId = property.PropertyId.Value;

            BaseView.SelectedPropertyPhotoUrl = String.Format("~/Handlers/EfxAdminHandler.ashx?RequestType=0&photoIndex=1&propertyId={0}", PropertyId);
        }

        protected override void BindLeasingAgentInfoTile(int leasingAgentId)
        {
            var Staff = new PropertyStaff(leasingAgentId);
            var StaffId = 0;

            if (Staff.PropertyStaffId.HasValue)
                StaffId = Staff.PropertyStaffId.Value;

            base.BindLeasingAgentInfoTile(Staff);
            BaseView.SelectedLeasingAgentPhotoUrl = String.Format("~/Handlers/EfxAdminHandler.ashx?RequestType=0&propertyStaffId={0}", StaffId);
        }

        protected override void ClearLeasingAgentTile()
        {
            base.ClearLeasingAgentTile();
            BaseView.SelectedLeasingAgentPhotoUrl = "/Handlers/EfxAdminHandler.ashx?RequestType=0&propertyStaffId=0";
        }

        protected override void ClearPropertyTile()
        {
            base.ClearPropertyTile();
            BaseView.SelectedPropertyPhotoUrl = "~/Handlers/EfxAdminHandler.ashx?RequestType=0&photoIndex=1&propertyId=0";
        }
    }
}
