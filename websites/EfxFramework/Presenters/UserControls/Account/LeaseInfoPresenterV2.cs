﻿using System.Net.Mail;
using EfxFramework.ExtensionMethods;
using EfxFramework.Interfaces.UserControls.Account;
using EfxFramework.Web;
using EfxFramework.Web.Session;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

namespace EfxFramework.Presenters.UserControls.Account
{
    public class LeaseInfoPresenterV2
    {
        private readonly ILeaseInfoV2 _View;
        private EfxFramework.Renter Resident { get { return new EfxFramework.Renter(_View.RenterId); } }
        private Lease Lease { get { return Lease.GetRenterLeaseByRenterId(Resident.RenterId); }}
        private EfxFramework.Property Property { get { return EfxFramework.Property.GetPropertyByRenterId(Resident.RenterId); } }
        private static List<LeaseFee> LeaseFees { get { return SessionManager.GetSessionItem<List<LeaseFee>>(SessionKey.LeaseLeaseFees); } set { SessionManager.AddItemToSession(SessionKey.LeaseLeaseFees, value); } }
        private static List<MonthlyFee> MonthlyFees { get { return SessionManager.GetSessionItem<List<MonthlyFee>>(SessionKey.LeaseMonthlyFees); } set { SessionManager.AddItemToSession(SessionKey.LeaseMonthlyFees, value); } }
        private static List<Pet> Pets { get { return SessionManager.GetSessionItem<List<Pet>>(SessionKey.LeasePets); } set { SessionManager.AddItemToSession(SessionKey.LeasePets, value); } }

        public LeaseInfoPresenterV2(ILeaseInfoV2 view)
        {
            _View = view;
            _View.AddFeeClicked = AddFeeClicked;
            _View.AddPetClicked = AddPetClicked;
            _View.SaveButtonClicked = SaveButtonClicked;
            _View.FeeList.SelectedIndexChanged += FeeChanged;
            _View.LeasingAgentList.SelectedIndexChanged += AgentChanged;
			_View.SuccessMessagePlaceHolder.Visible = false;

            SetPets();
        }

		public bool DisplayFees 
		{
			get
			{
				return string.IsNullOrEmpty(Property.PmsId);
			}
		}

        public void InitializeValues()
        {
            if (Property.PropertyId.HasValue && Property.PropertyId.Value > 0)
            {
                _View.LeasingAgentList.DataSource = PropertyStaff.GetPropertyStaffListByPropertyId(Property.PropertyId.Value);
                _View.LeasingAgentList.DataBind();
            }

            _View.PetTypeList.BindToSortedEnum(typeof (PetType));
            SetFees();

            if (Lease.LeaseId > 0)
            {
                PopulateFields();
                return;
            }

            _View.FirstMonthStartControl.StartYear = DateTime.UtcNow.Year;
            _View.FirstMonthStartControl.NumberOfYears = 5;
            _View.FirstMonthStartControl.SetYearList();

            if (Property.PropertyId.HasValue && Property.PropertyId.Value > 0)
            {
                MonthlyFees = MonthlyFee.GetMonthlyFeesByPropertyId(Property.PropertyId.Value);
                _View.FeeList.DataSource = MonthlyFees;
                _View.FeeList.DataBind();
            }

            _View.LeaseStartControl.StartYear = DateTime.UtcNow.Year - 5;
            _View.LeaseStartControl.NumberOfYears = 15;
            _View.LeaseStartControl.SetYearList();
            _View.LeaseEndControl.StartYear = DateTime.UtcNow.Year - 5;
            _View.LeaseEndControl.NumberOfYears = 15;
            _View.LeaseEndControl.SetYearList();

            _View.FirstMonthStartControl.SetDefaults();
            _View.LeaseStartControl.SetDefaults();
            _View.LeaseEndControl.SetDefaults();
        }

        private void PopulateFields()
        {
            _View.RentAmountText = Lease.RentAmount.ToString("F");

            SetDueDate();
            SetLeasingAgent();
            SetLeaseGeneralInfo();
        }

        private void SetDueDate()
        {
            if (!Lease.PaymentDueDate.HasValue)
            {
                _View.FirstMonthStartControl.StartYear = DateTime.UtcNow.Year;
                _View.FirstMonthStartControl.DayList.SelectedValue = Lease.RentDueDayOfMonth.ToString(CultureInfo.InvariantCulture);
                _View.FirstMonthStartControl.MonthList.SelectedValue = DateTime.UtcNow.Day < Lease.RentDueDayOfMonth ? DateTime.UtcNow.Month.ToString(CultureInfo.InvariantCulture) : DateTime.UtcNow.AddMonths(1).Month.ToString(CultureInfo.InvariantCulture);
                _View.FirstMonthStartControl.NumberOfYears = 5;
                _View.FirstMonthStartControl.SetYearList();
                return;
            }

            _View.FirstMonthStartText = "Next Payment Due Date:";
            _View.FirstMonthStartControl.StartYear = Lease.PaymentDueDate.Value.Year;
            _View.FirstMonthStartControl.NumberOfYears = 5; //Salcedo - changed NumberOfYears from 1 to 5, per Nick - 12/23/2013 
            _View.FirstMonthStartControl.MonthList.SelectedValue = Lease.PaymentDueDate.Value.Month.ToString(CultureInfo.InvariantCulture);
            _View.FirstMonthStartControl.DayList.SelectedValue = Lease.PaymentDueDate.Value.Day.ToString(CultureInfo.InvariantCulture);
            //_View.FirstMonthStartControl.MonthList.Enabled = false;
            //_View.FirstMonthStartControl.DayList.Enabled = false;
            //_View.FirstMonthStartControl.YearList.Enabled = false;
            _View.FirstMonthStartControl.SetYearList();
            _View.FirstMonthStartControl.YearList.SelectedValue = Lease.PaymentDueDate.Value.Year.ToString(CultureInfo.InvariantCulture);
        }

        private void SetPets()
        {
            var PetList = SessionManager.GetSessionItem<List<Pet>>(SessionKey.LeasePets);
            var DbPets = Pet.GetPetsByLeaseId(Lease.LeaseId);
            var PetDictionary = new Dictionary<int, Pet>();

            foreach (var P in PetList.Where(p => !PetDictionary.ContainsKey(p.PetId)))
            {
                PetDictionary.Add(P.PetId, P);
            }

            foreach (var P in DbPets.Where(p => !PetDictionary.ContainsKey(p.PetId)))
            {
                PetDictionary.Add(P.PetId, P);
            }

            Pets = PetDictionary.Select(pd => pd.Value).ToList();
            _View.PetGridDataSource = Pets;
        }

        private void SetFees()
        {
            var Fees = SessionManager.GetSessionItem<List<LeaseFee>>(SessionKey.LeaseLeaseFees);
            var DbLeaseFees = LeaseFee.GetFeesByLeaseId(Lease.LeaseId);
            var FeeDictionary = new Dictionary<int, LeaseFee>();

            foreach (var F in Fees.Where(f => !FeeDictionary.ContainsKey(f.MonthlyFeeId)))
            {
                FeeDictionary.Add(F.MonthlyFeeId, F);
            }

            foreach (var F in DbLeaseFees.Where(f => !FeeDictionary.ContainsKey(f.MonthlyFeeId)))
            {
                FeeDictionary.Add(F.MonthlyFeeId, F);
            }

            LeaseFees = FeeDictionary.Select(lf => lf.Value).ToList();
            var Results = new List<MonthlyFee>();

            if (Property.PropertyId.HasValue && Property.PropertyId.Value > 0)
            {
                Results = MonthlyFee.GetMonthlyFeesByPropertyId(Property.PropertyId.Value).Where(fee => MonthlyFees.All(mf => mf.MonthlyFeeId != fee.MonthlyFeeId)).ToList();
                MonthlyFees.AddRange(Results);
            }
                
            _View.FeeList.DataSource = MonthlyFees;
            _View.FeeList.DataBind();

            if (_View.FeeList.Items.Count < 1)
                _View.FeeList.Items.Add(new ListItem("-- No Available Fees --", "0"));
			//LeaseFees.ForEach(lf => lf.LeaseId = Lease.LeaseId);
            _View.FeeGridDataSource = LeaseFees;

            if (MonthlyFees.Count > 0)
                _View.FeeAmountText = MonthlyFees[0].DefaultFeeAmount.ToString("F");
        }

        private void SetLeasingAgent()
        {
            var Staff = new PropertyStaff(Lease.PropertyStaffId);
            if (!Staff.PropertyStaffId.HasValue || Staff.PropertyStaffId.Value < 1)
                return;

            _View.LeasingAgentList.SelectedValue = Staff.PropertyStaffId.Value.ToString(CultureInfo.InvariantCulture);
            _View.AgentNameText = Staff.DisplayName;
            _View.AgentPhoneText = Staff.MainPhoneNumber;
            _View.AgentEmailText = Staff.PrimaryEmailAddress;
            _View.AgentImageUrl = String.Format("~/Handlers/PhotoHandler.ashx?RequestType=0&propertyStaffId={0}&photoIndex=0", Staff.PropertyStaffId.Value);
        }

        private void AgentChanged(object sender, EventArgs e)
        {
            if (_View.LeasingAgentList.SelectedValue.ToInt32() == 0)
            {
                _View.AgentNameText = String.Empty;
                _View.AgentPhoneText = String.Empty;
                _View.AgentEmailText = String.Empty;
                _View.AgentImageUrl = String.Empty;
                return;
            }

            var Staff = new PropertyStaff(_View.LeasingAgentList.SelectedValue.ToInt32());
            if (!Staff.PropertyStaffId.HasValue || Staff.PropertyStaffId.Value < 1)
                return;

            _View.AgentNameText = Staff.DisplayName;
            _View.AgentPhoneText = Staff.MainPhoneNumber;
            _View.AgentEmailText = Staff.PrimaryEmailAddress;
            _View.AgentImageUrl = String.Format("~/Handlers/PhotoHandler.ashx?RequestType=0&propertyStaffId={0}&photoIndex=0", Staff.PropertyStaffId.Value);
        }

        private void SetLeaseGeneralInfo()
        {
            _View.LeaseStartControl.StartYear = Lease.StartDate.Year;
            _View.LeaseStartControl.NumberOfYears = Math.Abs(Lease.StartDate.Year - DateTime.UtcNow.Year) + 5;
            _View.LeaseStartControl.SetYearList();
            _View.LeaseStartControl.MonthList.SelectedValue = Lease.StartDate.Month.ToString(CultureInfo.InvariantCulture);
            _View.LeaseStartControl.DayList.SelectedValue = Lease.StartDate.Day.ToString(CultureInfo.InvariantCulture);
            _View.LeaseStartControl.YearList.SelectedValue = Lease.StartDate.Year.ToString(CultureInfo.InvariantCulture);
            _View.LeaseEndControl.StartYear = Lease.StartDate.Year;
            _View.LeaseEndControl.NumberOfYears = Math.Abs(Lease.StartDate.Year - DateTime.UtcNow.Year) + 5;
            _View.LeaseEndControl.SetYearList();
            _View.LeaseEndControl.MonthList.SelectedValue = Lease.EndDate.Month.ToString(CultureInfo.InvariantCulture);
            _View.LeaseEndControl.DayList.SelectedValue = Lease.EndDate.Day.ToString(CultureInfo.InvariantCulture);
            _View.LeaseEndControl.YearList.SelectedValue = Lease.EndDate.Year.ToString(CultureInfo.InvariantCulture);
            _View.UnitNumberText = Lease.UnitNumber;
            _View.NumberOfPeopleOnLeaseText = Lease.NumberOfTenants.ToString(CultureInfo.InvariantCulture);
        }

        private void FeeChanged(object sender, EventArgs e)
        {
            var Fee = new MonthlyFee(_View.FeeList.SelectedValue.ToInt32());
            _View.FeeAmountText = Fee.DefaultFeeAmount.ToString("C");
        }

        private void AddFeeClicked(object sender, EventArgs e)
        {
            _View.ParentPage.Validate("MonthlyFees");
            if (!_View.ParentPage.IsValid)
                return;

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1 || String.IsNullOrWhiteSpace(_View.FeeList.SelectedValue) || _View.FeeList.SelectedValue == "0")
                return;

            var Fee = new LeaseFee
                {
                    MonthlyFeeId = _View.FeeList.SelectedValue.ToInt32(),
                    PropertyId = Property.PropertyId.Value,
                    RenterId = Resident.RenterId,
                    FeeAmount = _View.FeeAmountText.Replace("$", "").ToNullDecimal(),
                    FeeName = _View.FeeList.SelectedItem.Text,
                    IsActive = true//,
					//LeaseId = Lease.LeaseId
                };

            MonthlyFees = MonthlyFees.Where(mf => mf.MonthlyFeeId != Fee.MonthlyFeeId).ToList();
            LeaseFees.Add(Fee);
            LeaseFees = LeaseFees;

            _View.FeeList.DataSource = MonthlyFees;
            _View.FeeList.DataBind();
            _View.FeeGridDataSource = LeaseFees;
            _View.FeeAmountText = String.Empty;

            if (_View.FeeList.Items.Count < 1)
                _View.FeeList.Items.Add(new ListItem("-- No Available Fees --", "0"));

            if (MonthlyFees.Count > 0)
                _View.FeeAmountText = MonthlyFees[0].DefaultFeeAmount.ToString("F");
        }

        private void AddPetClicked(object sender, EventArgs e)
        {
            _View.ParentPage.Validate("Pets");
            if (!_View.ParentPage.IsValid)
                return;

            var Lcd = Pets.Count > 0 ? Pets.Select(p => p.PetId).Min() : 0;
            if (Lcd > 0)
                Lcd = 0;
            else
                Lcd -= 1;

            var Pet = new Pet
                {
                    PetId = Lcd,
                    PetTypeId = _View.PetTypeList.SelectedValue.ToInt32(),
                    PetBreed = _View.BreedText,
                    PetName = _View.PetNameText
                };

            Pets.Add(Pet);
            Pets = Pets;
            _View.PetGridDataSource = Pets;
            _View.PetNameText = String.Empty;
            _View.BreedText = String.Empty;
        }

        private void SaveButtonClicked(object sender, EventArgs e)
        {
			_View.SuccessMessagePlaceHolder.Visible = false;
			//if (_View.LeasingAgentList.SelectedValue.ToInt32() < 1)
			//{
			//	CustomValidator err = new CustomValidator();
			//	err.ValidationGroup = "LeaseInfo";
			//	err.IsValid = false;
			//	err.ErrorMessage = "Please select a Leasing Agent";
			//	_View.ParentPage.Validators.Add(err);
			//	//BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Please select a Leasing Agent");
			//	return;
			//}

			//if (_View.NumberOfPeopleOnLeaseText.Length < 1)
			//{
			//	CustomValidator err = new CustomValidator();
			//	err.ValidationGroup = "LeaseInfo";
			//	err.IsValid = false;
			//	err.ErrorMessage = "Please enter the number of people on the Lease";
			//	_View.ParentPage.Validators.Add(err);
			//	//BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Please enter the number of people on the Lease");
			//	return;
			//}

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
				CustomValidator err = new CustomValidator();
				err.ValidationGroup = "LeaseInfo";
				err.IsValid = false;
				err.ErrorMessage = "Property Not Found";
				_View.ParentPage.Validators.Add(err);
                //BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Property Not Found");
                return;
            }

            var ValidRent = _View.RentAmountText.ToNullDecimal();
            
            if (_View.RentAmountText.Length < 1 || ValidRent == null)
            {
				CustomValidator err = new CustomValidator();
				err.ValidationGroup = "LeaseInfo";
				err.IsValid = false;
				err.ErrorMessage = "Rent Amount Required";
				_View.ParentPage.Validators.Add(err);
                //BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Error, "Rent Amount Required");
                return;
            }

            try
            {
				TextBox UnitNumber = (TextBox)_View.ParentPage.FindControl("AddressLine2Textbox");
				string unitNumber = UnitNumber != null ? UnitNumber.Text : "";
                var L = new Lease(Lease.LeaseId)
                    {
                        RentAmount = _View.RentAmountText.Replace("$", "").ToDecimal(),
                        PropertyId = Property.PropertyId.Value,
                        //PropertyStaffId = _View.LeasingAgentList.SelectedValue.ToInt32(),
                        //UnitNumber = _View.UnitNumberText,
                        //NumberOfTenants = _View.NumberOfPeopleOnLeaseText.ToInt32(),
						UnitNumber = unitNumber,
                        PaymentDueDate = _View.FirstMonthStartControl.GetDate(),
                        StartDate = _View.LeaseStartControl.GetDate(),
                        EndDate = _View.LeaseEndControl.GetDate(),
                        RentDueDayOfMonth = _View.FirstMonthStartControl.GetDate().Day
                    };

                var LeaseId = Lease.Set(L);
                SaveFees(LeaseId);
                SavePets(LeaseId);

				//running these stored procs here will allow an admin to add a fee and then immediately see it
				//reflected in the take a payment page, otherwise, a new fee will be mis-calculated as a credit
				Lease.UpdateLeaseFees();
				Lease.UpdateNonIntegratedLeases();

                if (Lease.LeaseId < 1)
                {
                    SendEmailX();
                }

                Lease.AssignRenterToLease(LeaseId, Resident.RenterId);
				_View.SuccessMessagePlaceHolder.Visible = true;
                //BasePage.GetPageMessage(_View.ParentPage).DisplayMessage(SystemMessageType.Success, "Successfully Updated the Lease Record");
            }
            catch(Exception ex)
            {
				Logging.ExceptionLogger.LogException(ex);
                BasePage.SafePageRedirect("~/ErrorPage.html");
            }
        }

        private void SaveFees(int leaseId)
        {
            foreach (var Fee in LeaseFees)
            {
                Fee.LeaseId = leaseId;
                LeaseFee.Set(Fee);
            }

            _View.FeeGridDataSource = LeaseFees;
            SessionManager.RemoveItemFromSession(SessionKey.LeaseLeaseFees);
        }

        private void SavePets(int leaseId)
        {
            foreach (var Animal in Pets)
            {
                Animal.LeaseId = leaseId;
                Pet.Set(Animal);
            }

            _View.PetGridDataSource = Pet.GetPetsByLeaseId(leaseId);
            SessionManager.RemoveItemFromSession(SessionKey.LeasePets);
        }

        private void SendEmailX()
        {
            //Salcedo - 2/23/2014 - changed call to include staff IsMainContacts
            //BulkMail.BulkMail.SendMailMessage(null, "EFX", false, Settings.SendMailUsername, Settings.SendMailPassword, new MailAddress(Settings.SupportEmail), "Your RPO Account",
            //        String.Format(Resources.EmailTemplates.LeaseInfoSetupForResident, Resident.DisplayName), new List<MailAddress> {new MailAddress(Resident.PrimaryEmailAddress)});
            int IntPropertyId = Property.PropertyId.HasValue ? Property.PropertyId.Value : 0;
            BulkMail.BulkMail.SendMailMessageIncludePropertyEmailContacts(IntPropertyId, "EFX", EfxSettings.SendMailUsername, EfxSettings.SendMailPassword, new MailAddress(EfxSettings.SupportEmail), "Your RPO Account",
                    String.Format(Resources.EmailTemplates.LeaseInfoSetupForResident, Resident.DisplayName), new List<MailAddress> { new MailAddress(Resident.PrimaryEmailAddress) });
        }
    }
}
