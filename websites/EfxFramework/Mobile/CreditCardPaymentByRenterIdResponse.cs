﻿using EfxFramework.PaymentMethods;

namespace EfxFramework.Mobile
{
    public class CreditCardPaymentByRenterIdResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public string TransactionId { get; set; }
        public decimal ConvenienceFee { get; set; }
    }
}
