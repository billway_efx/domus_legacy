﻿using EfxFramework.PaymentMethods;

namespace EfxFramework.Mobile
{
    public class MaintenanceResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
    }
}
