﻿using EfxFramework.PaymentMethods;
using EfxFramework.PaymentMethods.Ach;
using EfxFramework.PaymentMethods.CreditCard;
using EfxFramework.PublicApi.Payment;
using EfxFramework.Resources;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;

namespace EfxFramework.Mobile
{
	public class PaymentRequest
	{
		//Public Enumerations
		public enum PaymentMethodEnum
		{
			ECheck = 1,
			CreditCardOnFile = 2,
			ScanCreditCard = 3
		}

	    private string _ExpirationDate;

		//Public Properties
        public List<PaymentItem> PaymentItems { get; set; }
		public PaymentMethodEnum PaymentMethod { get; set; }
		public string SessionId { get; set; }
        public string CreditCardNumber { get; set; }
        public string ExpirationDate { get { return _ExpirationDate; } set { _ExpirationDate = value != null ? FormatExpirationDate(value) : null; } }
        public string CvvCode { get; set; }
        public string NameOnCard { get; set; }

		//Public Methods
        public PaymentResponse ProcessPayment()
		{
            var Renter = new EfxFramework.Renter(RenterSession.GetRenterSessionBySessionGuid(Guid.Parse(SessionId)).RenterId) {IsParticipating = true};
            EfxFramework.Renter.Set(Renter, "System Process");

            var Payer = EfxFramework.Payer.GetPayerByRenterId(Renter.RenterId);
		    var PayerAch = EfxFramework.PayerAch.GetPrimaryPayerAchByPayerId(Payer.PayerId);
		    var PayerCreditCard = EfxFramework.PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(Payer.PayerId);
            var Property = EfxFramework.Property.GetPropertyByRenterId(Renter.RenterId);

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                return new PaymentResponse {RentAndFeeResult = MobileResponseResult.PropertyNotFound, RentAndFeeResponseMessage = ServiceResponseMessages.PropertyNotFound};

            if (Renter.AcceptedPaymentTypeId == 4)
                return new PaymentResponse {RentAndFeeResult = MobileResponseResult.CannotAcceptRenterPayment, RentAndFeeResponseMessage = ServiceResponseMessages.CannotAcceptRenterPayment};

            //Salcedo - 3/25/2014 - we're no longer using the IVR
            //var UseIvr = Property.ProgramId.HasValue && Property.ProgramId.Value != EfxFramework.Property.Program.PropertyPaidProgram;
            var UseIvr = false;

            var RentPayments = GetRentPayments();
            var CharityPayments = GetCharityPayments();

            if (RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
		    {
		        switch (PaymentMethod)
		        {
		            case PaymentMethodEnum.ECheck:
                        if (Renter.AcceptedPaymentTypeId == 3)
                            return new PaymentResponse {RentAndFeeResult = MobileResponseResult.CashEquivalentRequired, RentAndFeeResponseMessage = ServiceResponseMessages.CashEquivalentRequired};

		                return ProcessPayment(RentPayments, CharityPayments, PayerAch, Renter);
		            case PaymentMethodEnum.CreditCardOnFile:
		                CreditCardNumber = PayerCreditCard.CreditCardAccountNumber;
		                ExpirationDate = PayerCreditCard.CreditCardExpirationDate;
		                NameOnCard = PayerCreditCard.CreditCardHolderName;
		                return UseIvr ? ProcessPayment(RentPayments, CharityPayments, this, Renter) : ProcessPayment(RentPayments, CharityPayments, PayerCreditCard, Renter);
		            case PaymentMethodEnum.ScanCreditCard:

                        //Salcedo - 3/25/2014 - we're no longer using the IVR
                        //if (UseIvr)
                        //    return ProcessPayment(RentPayments, CharityPayments, this, Renter);

                        return ProcessPayment(
                            RentPayments, 
                            CharityPayments, 
                            new PayerCreditCard
                                {
                                    CreditCardAccountNumber = CreditCardNumber, 
                                    CreditCardHolderName = NameOnCard, 
                                    CreditCardExpirationMonth = GetExpirationMonth(), 
                                    CreditCardExpirationYear = GetExpirationYear()
                                }, 
                            Renter,
                            CvvCode);
		            default:
                        return new PaymentResponse { RentAndFeeResult = MobileResponseResult.UnknownPaymentType, RentAndFeeResponseMessage = ServiceResponseMessages.UnknownPaymentTypeEntered, RentAndFeeTransactionId = null };
		        }
		    }

            return new PaymentResponse { RentAndFeeResult = MobileResponseResult.SessionTimeout, RentAndFeeResponseMessage = ServiceResponseMessages.Sessiontimeout };
		}

        private static PaymentResponse ProcessPayment(Dictionary<PaymentApplication, List<PaymentItem>> rentPayments, IEnumerable<PaymentItem> charityPayments, PayerCreditCard payerCreditCard, EfxFramework.Renter renter, string cvv = null)
        {
            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);
            var Property = EfxFramework.Property.GetPropertyByRenterId(renter.RenterId);
            var RentBaseAmount = rentPayments.Values.SelectMany(r => r).Where(r => !r.Description.StartsWith("convenience fee", StringComparison.OrdinalIgnoreCase)).Sum(r => r.Amount);
            var RentPaymentAmount = new PaymentAmount(RentBaseAmount);
            var CharityBaseAmount = charityPayments.Sum(r => r.Amount);
            var CharityPaymentAmount = new PaymentAmount(CharityBaseAmount);

            if (Lease.LeaseId < 1)
                return new PaymentResponse { RentAndFeeResult = MobileResponseResult.LeaseNotFound, RentAndFeeResponseMessage = ServiceResponseMessages.LeaseNotFound };

            var ConvenienceFee = rentPayments[PaymentApplication.Other].FirstOrDefault(r => r.Description == "Convenience Fees");

            if (ConvenienceFee != null)
            {
                RentPaymentAmount.BaseAmount -= ConvenienceFee.Amount;
                RentPaymentAmount.FeeAmount = ConvenienceFee.Amount;
                RentPaymentAmount.FeeDescription = "Convenience Fee";
            }

            var RentResponse = new CreditCardPaymentResponse();
            var CharityResponse = new CreditCardPaymentResponse();

            if (RentPaymentAmount.BaseAmount > 0.0M)
            {
                var CustomFields = new List<CustomField>
                {
                    new CustomField("customfield1", Property.PropertyId.HasValue ? Property.PropertyId.Value.ToString(CultureInfo.InvariantCulture) : "0"),
                    new CustomField("customfield2", Lease.UnitNumber), 
                    new CustomField("customfield3", String.Format("{0}{1}", DateTime.Now.Month.ToString(CultureInfo.InvariantCulture), DateTime.Now.Year.ToString(CultureInfo.InvariantCulture))), 
                    new CustomField("customfield5", (Convert.ToInt32(RentPaymentAmount.FeeAmount * 100)).ToString(CultureInfo.InvariantCulture)), 
                    new CustomField("customfield6", (Convert.ToInt32(RentPaymentAmount.BaseAmount * 100)).ToString(CultureInfo.InvariantCulture)),
                    new CustomField("customfield10", Property.PropertyCode)
                };

                if (Property.PropertyId.HasValue && Property.PropertyId.Value > 0)
                    RentResponse = CreditCardPayment.ProcessCreditCard(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, renter.IsCreditCardTestRenter, RentPaymentAmount.TotalAmount,
                                    payerCreditCard.CreditCardAccountNumber, payerCreditCard.CreditCardExpirationDate, cvv, payerCreditCard.CreditCardHolderName, CustomFields);
                else
                    RentResponse = new CreditCardPaymentResponse {Result = GeneralResponseResult.PropertyNotFound, ResponseMessage = ServiceResponseMessages.PropertyNotFound};
            }

            if (CharityPaymentAmount.BaseAmount > 0.0M)
            {
                var CustomFields = new List<CustomField>
                {
                    new CustomField("customfield1", Property.PropertyId.HasValue ? Property.PropertyId.Value.ToString(CultureInfo.InvariantCulture) : "0"),
                    new CustomField("customfield2", Lease.UnitNumber), 
                    new CustomField("customfield3", String.Format("{0}{1}", DateTime.Now.Month.ToString(CultureInfo.InvariantCulture), DateTime.Now.Year.ToString(CultureInfo.InvariantCulture))),
                    new CustomField("customfield6", (Convert.ToInt32(CharityPaymentAmount.BaseAmount * 100)).ToString(CultureInfo.InvariantCulture)),
                    new CustomField("customfield10", Property.PropertyCode)
                };

                CharityResponse = CreditCardPayment.ProcessCreditCard(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, renter.IsCreditCardTestRenter, CharityPaymentAmount.TotalAmount,
                    payerCreditCard.CreditCardAccountNumber, payerCreditCard.CreditCardExpirationDate, cvv, payerCreditCard.CreditCardHolderName, CustomFields);
            }

            var Response = new PaymentResponse
            {
                RentAndFeeResult = ConvertResponseResult(RentResponse.Result),
                RentAndFeeResponseMessage = RentResponse.ResponseMessage,
                RentAndFeeTransactionId = RentResponse.TransactionId,
                CharityResult = ConvertResponseResult(CharityResponse.Result),
                CharityResponseMessage = CharityResponse.ResponseMessage,
                CharityTransactionId = CharityResponse.TransactionId,
                ConvenienceFee = RentPaymentAmount.FeeAmount
            };

            if (Response.RentAndFeeResponseMessage == ServiceResponseMessages.Ccaccepted)
            {
                ProcessVoid(Response.RentAndFeeTransactionId);
            }

            if (Response.CharityResponseMessage == ServiceResponseMessages.Ccaccepted)
            {
                ProcessVoid(Response.CharityTransactionId);
            }

            if (Response.RentAndFeeResult == MobileResponseResult.Success && RentPaymentAmount.BaseAmount > 0.0M)
            {
                Lease.CurrentBalanceDue -= RentPaymentAmount.BaseAmount;
                //Lease.PaymentDueDate = Lease.PaymentDueDate.HasValue ? Lease.PaymentDueDate.Value.AddMonths(1) : DateTime.UtcNow.AddMonths(1);
                Lease.Set(Lease);

                //Salcedo - 3/12/2014 - replaced with new html email template
                //SendEmail(Property.PropertyId.HasValue ? Property.PropertyId.Value : 0, "Payment Confirmation", 
                //    string.Format(EmailTemplates.OneTimeRentPayment, renter.DisplayName, 
                //        "Mobile Payment", DateTime.UtcNow.ToShortDateString(), Response.RentAndFeeTransactionId, Response.ConvenienceFee.ToString("C"), 
                //        RentPaymentAmount.BaseAmount.ToString("C")), 
                //    renter.PrimaryEmailAddress);
                String RecipientName = renter.DisplayName;

                //cakel: BUGID000180 - Added PropertyName
                String RenterProperty = Property.PropertyName;

                String PaymentAmount = "Mobile Payment";
                String TransactionDate = DateTime.UtcNow.ToShortDateString();
                String InternalTransactionId = Response.RentAndFeeTransactionId;
                String ConvenienceAmount = Response.ConvenienceFee.ToString("C");
                String TotalAmount = RentPaymentAmount.BaseAmount.ToString("C");
                //cakel: BUGID000180 added RenterProperty (Property Name)
                String Body = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-payment-success.html").Replace("[[ROOT]]",
                    EmailTemplateService.EmailTemplateService.RootPath), RecipientName, RenterProperty, PaymentAmount, TransactionDate, InternalTransactionId, ConvenienceAmount, TotalAmount);
                SendEmail(Property.PropertyId.HasValue ? Property.PropertyId.Value : 0, "Payment Confirmation", Body, renter.PrimaryEmailAddress);

                RecordPayments(rentPayments, renter, Response, payerCreditCard, RentPaymentAmount.FeeAmount);
            }
            if (Response.CharityResult == MobileResponseResult.Success && CharityPaymentAmount.BaseAmount > 0.0M)
            {
                SendEmail(Property.PropertyId.HasValue ? Property.PropertyId.Value : 0, "Charity Payment Confirmation", 
                    string.Format(EmailTemplates.OneTimeCharityPayment, renter.DisplayName, 
                    string.Format("{0} {1}", "Charity", CharityPaymentAmount.BaseAmount.ToString("C")), DateTime.UtcNow.ToShortDateString(), Response.CharityTransactionId), 
                    renter.PrimaryEmailAddress);
                RecordPayments(charityPayments, renter, Response, payerCreditCard);
            }

            return Response;
        }

        private static PaymentResponse ProcessPayment(Dictionary<PaymentApplication, List<PaymentItem>> rentPayments, IEnumerable<PaymentItem> charityPayments, PayerAch payerAch, EfxFramework.Renter renter)
        {
            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);
            var Property = EfxFramework.Property.GetPropertyByRenterId(renter.RenterId);
            var RentBaseAmount = rentPayments.Values.SelectMany(r => r).Where(r => !r.Description.StartsWith("convenience fee", StringComparison.OrdinalIgnoreCase)).Sum(r => r.Amount);
            var RentPaymentAmount = new PaymentAmount(RentBaseAmount);
            var CharityBaseAmount = charityPayments.Sum(r => r.Amount);
            var CharityPaymentAmount = new PaymentAmount(CharityBaseAmount);
            var RentAchClientId = GetAchClientId(renter.RenterId, PaymentApplication.Rent);
            var CharityAchClientId = GetAchClientId(renter.RenterId, PaymentApplication.Charity);
            var Address = GetRenterAddress(renter);
            var RentResponse = new AchPaymentResponse();
            var CharityResponse = new AchPaymentResponse();

            if (Address.AddressId == -1)
                return new PaymentResponse { RentAndFeeResult = MobileResponseResult.AddressNotFound, RentAndFeeResponseMessage = ServiceResponseMessages.AddressNotFound };

            if (Lease.LeaseId < 1)
                return new PaymentResponse { RentAndFeeResult = MobileResponseResult.LeaseNotFound, RentAndFeeResponseMessage = ServiceResponseMessages.LeaseNotFound };

            var ConvenienceFee = rentPayments.ContainsKey(PaymentApplication.Other) ? rentPayments[PaymentApplication.Other].FirstOrDefault(r => r.Description == "Convenience Fees") : null;

            if (ConvenienceFee != null)
            {
                RentPaymentAmount.BaseAmount -= ConvenienceFee.Amount;
                RentPaymentAmount.FeeAmount = ConvenienceFee.Amount;
                RentPaymentAmount.FeeDescription = "Convenience Fee";
            }

            if (RentPaymentAmount.BaseAmount > 0.0M)
            {
                if (Property.PropertyId.HasValue && Property.PropertyId > 0)
                    RentResponse = AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword, RentAchClientId, RentPaymentAmount.TotalAmount, payerAch.BankRoutingNumber,
                        payerAch.BankAccountNumber, payerAch.BankAccountType.ToUpper(), renter.FirstName, renter.LastName, Address.AddressLine1, Address.AddressLine2, Address.City,
                        Address.GetStateAbbeviation(), Address.PostalCode, renter.MainPhoneNumber);
                else
                    RentResponse = new AchPaymentResponse {Result = GeneralResponseResult.PropertyNotFound, ResponseDescription = ServiceResponseMessages.PropertyNotFound};
            }

            if (CharityPaymentAmount.BaseAmount > 0.0M)
            {
                CharityResponse = AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword, CharityAchClientId, CharityPaymentAmount.TotalAmount, payerAch.BankRoutingNumber,
                    payerAch.BankAccountNumber, payerAch.BankAccountType.ToUpper(), renter.FirstName, renter.LastName, Address.AddressLine1, Address.AddressLine2, Address.City,
                    ((StateProvince)Address.StateProvinceId).ToString(), Address.PostalCode, renter.MainPhoneNumber);
            }

            var Response = new PaymentResponse
            {
                RentAndFeeResult = ConvertResponseResult(RentResponse.Result),
                RentAndFeeResponseMessage = RentResponse.ResponseDescription,
                RentAndFeeTransactionId = RentResponse.TransactionId,
                CharityResult = ConvertResponseResult(CharityResponse.Result),
                CharityResponseMessage = CharityResponse.ResponseDescription,
                CharityTransactionId = CharityResponse.TransactionId,
                ConvenienceFee = RentPaymentAmount.FeeAmount
            };

            if (Response.RentAndFeeResult == MobileResponseResult.Success && RentPaymentAmount.BaseAmount > 0.0M)
            {
                Lease.CurrentBalanceDue -= RentPaymentAmount.BaseAmount;
                Lease.CurrentBalanceDueLastUpdated = DateTime.UtcNow;
                //Lease.PaymentDueDate = Lease.PaymentDueDate.HasValue ? Lease.PaymentDueDate.Value.AddMonths(1) : DateTime.UtcNow.AddMonths(1);
                Lease.Set(Lease);

                //Salcedo - 3/12/2014 - replaced with new html email template
                //SendEmail(Property.PropertyId.HasValue ? Property.PropertyId.Value : 0, "Payment Confirmation", 
                //    string.Format(EmailTemplates.OneTimeRentPayment, renter.DisplayName, "Mobile Payment", DateTime.UtcNow.ToShortDateString(), 
                //        Response.RentAndFeeTransactionId, Response.ConvenienceFee.ToString("C"), RentPaymentAmount.BaseAmount.ToString("C")), 
                //    renter.PrimaryEmailAddress);
                String RecipientName = renter.DisplayName;
                //cakel: BUGID00180
                String RenterProperty = Property.PropertyName;
                String PaymentAmount = "Mobile Payment";
                String TransactionDate = DateTime.UtcNow.ToShortDateString();
                String InternalTransactionId = Response.RentAndFeeTransactionId;
                String ConvenienceAmount = Response.ConvenienceFee.ToString("C");
                String TotalAmount = RentPaymentAmount.BaseAmount.ToString("C");
                String Body = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-payment-success.html").Replace("[[ROOT]]",
                    EmailTemplateService.EmailTemplateService.RootPath), RecipientName, RenterProperty, PaymentAmount, TransactionDate, InternalTransactionId, ConvenienceAmount, TotalAmount);
                SendEmail(Property.PropertyId.HasValue ? Property.PropertyId.Value : 0, "Payment Confirmation", Body, renter.PrimaryEmailAddress);

                RecordPayments(rentPayments, renter, Response, payerAch, RentPaymentAmount.FeeAmount);
            }
            if (Response.CharityResult == MobileResponseResult.Success && CharityPaymentAmount.BaseAmount > 0.0M)
            {
                SendEmail(Property.PropertyId.HasValue ? Property.PropertyId.Value : 0, "Charity Payment Confirmation", 
                    string.Format(EmailTemplates.OneTimeCharityPayment, renter.DisplayName, string.Format("{0} {1}", "Charity", CharityPaymentAmount.BaseAmount.ToString("C")), 
                        DateTime.UtcNow.ToShortDateString(), Response.CharityTransactionId), 
                    renter.PrimaryEmailAddress);
                RecordPayments(charityPayments, renter, Response, payerAch);
            }

            return Response;
        }

        private static PaymentResponse ProcessPayment(IDictionary<PaymentApplication, List<PaymentItem>> rentPayments, IList<PaymentItem> charityPayments, PaymentRequest request, EfxFramework.Renter renter)
        {
            var PaymentQueue = new PaymentQueue
                {
                    CreditCardAccountNumber = request.CreditCardNumber, 
                    CreditCardHolderName = request.NameOnCard, 
                    CreditCardExpirationMonth = request.GetExpirationMonth(), 
                    CreditCardExpirationYear = request.GetExpirationYear(),
                    CreditCardSecurityCode = request.CvvCode,
                    RenterId = renter.RenterId, 
                    TransactionDateTime = DateTime.UtcNow, 
                    PaymentTypeId = (int) PaymentType.CreditCard, 
                    CharityAmount = charityPayments.Count > 0 ? charityPayments[0].Amount : 0.0M, 
                    CharityName = charityPayments.Count > 0 ? charityPayments[0].Description : null, 
                    RentAmount = rentPayments.ContainsKey(PaymentApplication.Rent) && rentPayments[PaymentApplication.Rent].Count > 0 ? rentPayments[PaymentApplication.Rent][0].Amount : 0.0M, 
                    RentAmountDescription = rentPayments.ContainsKey(PaymentApplication.Rent) && rentPayments[PaymentApplication.Rent].Count > 0 ? rentPayments[PaymentApplication.Rent][0].Description : null, 
                    OtherAmount1 = rentPayments.ContainsKey(PaymentApplication.Other) && rentPayments[PaymentApplication.Other].Count > 0 ? rentPayments[PaymentApplication.Other][0].Amount : 0.0M, 
                    OtherDescription1 = rentPayments.ContainsKey(PaymentApplication.Other) && rentPayments[PaymentApplication.Other].Count > 0 ? rentPayments[PaymentApplication.Other][0].Description : null, 
                    OtherAmount2 = rentPayments.ContainsKey(PaymentApplication.Other) && rentPayments[PaymentApplication.Other].Count > 1 ? rentPayments[PaymentApplication.Other][1].Amount : 0.0M, 
                    OtherDescription2 = rentPayments.ContainsKey(PaymentApplication.Other) && rentPayments[PaymentApplication.Other].Count > 1 ? rentPayments[PaymentApplication.Other][1].Description : null, 
                    OtherAmount3 = rentPayments.ContainsKey(PaymentApplication.Other) && rentPayments[PaymentApplication.Other].Count > 2 ? rentPayments[PaymentApplication.Other][2].Amount : 0.0M, 
                    OtherDescription3 = rentPayments.ContainsKey(PaymentApplication.Other) && rentPayments[PaymentApplication.Other].Count > 2 ? rentPayments[PaymentApplication.Other][2].Description : null, 
                    OtherAmount4 = rentPayments.ContainsKey(PaymentApplication.Other) && rentPayments[PaymentApplication.Other].Count > 3 ? rentPayments[PaymentApplication.Other][3].Amount : 0.0M, 
                    OtherDescription4 = rentPayments.ContainsKey(PaymentApplication.Other) && rentPayments[PaymentApplication.Other].Count > 3 ? rentPayments[PaymentApplication.Other][3].Description : null, 
                    OtherAmount5 = rentPayments.ContainsKey(PaymentApplication.Other) && rentPayments[PaymentApplication.Other].Count > 4 ? rentPayments[PaymentApplication.Other][4].Amount : 0.0M, 
                    OtherDescription5 = rentPayments.ContainsKey(PaymentApplication.Other) && rentPayments[PaymentApplication.Other].Count > 4 ? rentPayments[PaymentApplication.Other][4].Description : null
                };

            PaymentQueue = new PaymentQueue(PaymentQueue.Set(PaymentQueue));

            return new PaymentResponse {RedirectToIvr = true, PaymentQueueNumber = PaymentQueue.PaymentQueueNumber};
        }

        private static Address GetRenterAddress(EfxFramework.Renter renter)
        {
            var BillingAddress = Address.GetAddressByRenterIdAndAddressType(renter.RenterId, (int)TypeOfAddress.Billing);
            if (BillingAddress.AddressId > 0)
                return BillingAddress;

            var AddressId = 0;

            if (String.IsNullOrEmpty(renter.StreetAddress) || String.IsNullOrEmpty(renter.City) || !renter.StateProvinceId.HasValue || String.IsNullOrEmpty(renter.PostalCode))
                AddressId = -1;

            return new Address
            {
                AddressId = AddressId,
                AddressTypeId = (int)TypeOfAddress.Primary,
                AddressLine1 = renter.StreetAddress,
                AddressLine2 = renter.Unit,
                City = renter.City,
                StateProvinceId = renter.StateProvinceId.HasValue ? renter.StateProvinceId.Value : 1,
                PostalCode = renter.PostalCode
            };
        }

        private static void ProcessVoid(string transactionId)
        {
            CreditCardPayment.VoidCreditCardPayment(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, transactionId);
        }

        private static void RecordPayments(Dictionary<PaymentApplication, List<PaymentItem>> payments, EfxFramework.Renter renter, PaymentResponse response, PayerCreditCard payerCreditCard, decimal feeAmount)
        {
            var FirstPass = true;
            foreach (var Request in payments.Values.SelectMany(r => r))
            {
                // ReSharper disable ConvertIfStatementToConditionalTernaryExpression
                if (FirstPass)
                // ReSharper restore ConvertIfStatementToConditionalTernaryExpression
                    SetPayment(renter, new PaymentAmount {BaseAmount = Request.Amount, FeeAmount = feeAmount, FeeDescription = "Convenience Fee"}, Request.Description, payerCreditCard, SetPaymentApplication(Request), response);
                else
                    SetPayment(renter, PaymentAmount.GetPaymentAmount(Request.Amount, renter), Request.Description, payerCreditCard, SetPaymentApplication(Request), response);
                
                FirstPass = false;
            }
        }

        private static void RecordPayments(Dictionary<PaymentApplication, List<PaymentItem>> payments, EfxFramework.Renter renter, PaymentResponse response, PayerAch payerAch, decimal feeAmount)
        {
            var FirstPass = true;
            foreach (var Request in payments.Values.SelectMany(r => r))
            {
                // ReSharper disable ConvertIfStatementToConditionalTernaryExpression
                if (FirstPass)
                // ReSharper restore ConvertIfStatementToConditionalTernaryExpression
                    SetPayment(renter, new PaymentAmount {BaseAmount = Request.Amount, FeeAmount = feeAmount, FeeDescription = "Convenience Fee"}, Request.Description, payerAch, SetPaymentApplication(Request), response);
                else
                    SetPayment(renter, PaymentAmount.GetPaymentAmount(Request.Amount, renter, PaymentType.ECheck), Request.Description, payerAch, SetPaymentApplication(Request), response);
                FirstPass = false;
            }
        }

        private static void RecordPayments(IEnumerable<PaymentItem> payments, EfxFramework.Renter renter, PaymentResponse response, PayerCreditCard payerCreditCard)
        {
            foreach (var Request in payments)
            {
                SetPayment(renter, PaymentAmount.GetPaymentAmount(Request.Amount, renter), Request.Description, payerCreditCard, PaymentApplication.Charity, response);
            }
        }

        private static void RecordPayments(IEnumerable<PaymentItem> payments, EfxFramework.Renter renter, PaymentResponse response, PayerAch payerAch)
        {
            foreach (var Request in payments)
            {
                SetPayment(renter, PaymentAmount.GetPaymentAmount(Request.Amount, renter, PaymentType.ECheck), Request.Description, payerAch, PaymentApplication.Charity, response);
            }
        }

        private static void SetPayment(EfxFramework.Renter renter, PaymentAmount amount, string description, PayerCreditCard payerCreditCard, PaymentApplication paymentApplication,
            PaymentResponse response)
        {
            if (!renter.PayerId.HasValue)
                renter.PayerId = 0;

            int PaymentId;

            switch (paymentApplication)
            {
                case PaymentApplication.Rent:
                    PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, PaymentType.CreditCard, null, null, payerCreditCard.CreditCardHolderName, payerCreditCard.CreditCardAccountNumber,
                        payerCreditCard.CreditCardExpirationDate, amount, description, null, null, null, null, response.RentAndFeeTransactionId, false, null,
                        ((int)response.RentAndFeeResult).ToString(CultureInfo.InvariantCulture), response.RentAndFeeResponseMessage, null, PaymentChannel.Mobile, PaymentStatus.Approved));

                    Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int) response.RentAndFeeResult, response.RentAndFeeResponseMessage);
                    break;
                case PaymentApplication.Charity:
                    PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, PaymentType.CreditCard, null, null, payerCreditCard.CreditCardHolderName, payerCreditCard.CreditCardAccountNumber,
                        payerCreditCard.CreditCardExpirationDate, null, null, amount, description, null, null, response.CharityTransactionId, false, null,
                        ((int)response.CharityResult).ToString(CultureInfo.InvariantCulture), response.CharityResponseMessage, null, PaymentChannel.Mobile, PaymentStatus.Approved));

                    Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int) response.CharityResult, response.CharityResponseMessage);
                    break;
                case PaymentApplication.Other:
                    PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, PaymentType.CreditCard, null, null, payerCreditCard.CreditCardHolderName, payerCreditCard.CreditCardAccountNumber,
                        payerCreditCard.CreditCardExpirationDate, null, null, null, null, amount, description, response.RentAndFeeTransactionId, false, null,
                        ((int)response.RentAndFeeResult).ToString(CultureInfo.InvariantCulture), response.RentAndFeeResponseMessage, null, PaymentChannel.Mobile, PaymentStatus.Approved));

                    Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int) response.RentAndFeeResult, response.RentAndFeeResponseMessage);
                    break;
            }
        }

        private static void SetPayment(EfxFramework.Renter renter, PaymentAmount amount, string description, PayerAch payerAch, PaymentApplication paymentApplication,
            PaymentResponse response)
        {
            if (!renter.PayerId.HasValue)
                renter.PayerId = 0;

            int PaymentId;

            switch (paymentApplication)
            {
                case PaymentApplication.Rent:
                    PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, PaymentType.ECheck, payerAch.BankAccountNumber, payerAch.BankRoutingNumber, null, null,
                        null, amount, description, null, null, null, null, response.RentAndFeeTransactionId, false, null,
                        ((int)response.RentAndFeeResult).ToString(CultureInfo.InvariantCulture), response.RentAndFeeResponseMessage, null, PaymentChannel.Mobile, PaymentStatus.Processing));

                    Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int)response.RentAndFeeResult, response.RentAndFeeResponseMessage);
                    break;
                case PaymentApplication.Charity:
                    PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, PaymentType.ECheck, payerAch.BankAccountNumber, payerAch.BankRoutingNumber, null, null,
                        null, null, null, amount, description, null, null, response.CharityTransactionId, false, null,
                        ((int)response.CharityResult).ToString(CultureInfo.InvariantCulture), response.CharityResponseMessage, null, PaymentChannel.Mobile, PaymentStatus.Processing));

                    Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int)response.CharityResult, response.CharityResponseMessage);
                    break;
                case PaymentApplication.Other:
                    PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, PaymentType.ECheck, payerAch.BankAccountNumber, payerAch.BankRoutingNumber, null, null,
                        null, null, null, null, null, amount, description, response.RentAndFeeTransactionId, false, null,
                        ((int)response.RentAndFeeResult).ToString(CultureInfo.InvariantCulture), response.RentAndFeeResponseMessage, null, PaymentChannel.Mobile, PaymentStatus.Processing));

                    Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int)response.RentAndFeeResult, response.RentAndFeeResponseMessage);
                    break;
            }
        }

        //Salcedo - 4/9/2014 - Issue # 0000012 - changed line item logic to match the logic used for payment on the website (in EfxFramework\WebPayments\PaymentRequest.cs)
        //private Dictionary<PaymentApplication, List<PaymentItem>> GetRentPayments()
        //{
        //    var RentPayments = new Dictionary<PaymentApplication, List<PaymentItem>>();

        //    foreach (var Item in PaymentItems)
        //    {
        //        switch (SetPaymentApplication(Item))
        //        {
        //            case PaymentApplication.Rent:
        //                if (!RentPayments.Keys.Contains(PaymentApplication.Rent))
        //                    RentPayments.Add(PaymentApplication.Rent, new List<PaymentItem> { Item });
        //                else
        //                    RentPayments[PaymentApplication.Rent].Add(Item);
        //                break;
        //            case PaymentApplication.Other:
        //                if (!RentPayments.Keys.Contains(PaymentApplication.Other))
        //                    RentPayments.Add(PaymentApplication.Other, new List<PaymentItem> { Item });
        //                else
        //                    RentPayments[PaymentApplication.Other].Add(Item);
        //                break;
        //        }
        //    }

        //    return RentPayments;
        //}
        
        private Dictionary<PaymentApplication, List<PaymentItem>> GetRentPayments()
        {
            var RentPayments = new Dictionary<PaymentApplication, List<PaymentItem>>();
            var OtherPayments = new List<PaymentItem>();

            foreach (var Item in PaymentItems)
            {
                switch (SetPaymentApplication(Item))
                {
                    case PaymentApplication.Rent:
                        if (!RentPayments.Keys.Contains(PaymentApplication.Rent))
                            RentPayments.Add(PaymentApplication.Rent, new List<PaymentItem> { Item });
                        else
                            RentPayments[PaymentApplication.Rent].Add(Item);
                        break;
                    case PaymentApplication.Other:
                        if (Item.Amount > 0 && !Item.Description.ToLower().Contains("convenience fee"))
                            OtherPayments.Add(new PaymentItem { Amount = Item.Amount, Description = Item.Description });
                        break;
                }
            }

            RentPayments.Add(PaymentApplication.Other, OtherPayments);

            return RentPayments;
        }

        private List<PaymentItem> GetCharityPayments()
        {
            return PaymentItems.Where(item => item.IsCharityPayment).ToList();
        }

        private static PaymentApplication SetPaymentApplication(PaymentItem paymentItem)
        {
            if (paymentItem.IsCharityPayment)
                return PaymentApplication.Charity;

            if (!String.IsNullOrEmpty(paymentItem.Description))
            {
                if (paymentItem.Description.ToLower().Contains("rent"))
                    return PaymentApplication.Rent;
            }

            return PaymentApplication.Other;
        }

        private static string GetAchClientId(int renterId, PaymentApplication paymentApplication)
        {
            var Property = EfxFramework.Property.GetPropertyByRenterId(renterId);

            switch (paymentApplication)
            {
                case PaymentApplication.Rent:
                    return Property.RentalAccountAchClientId;
                case PaymentApplication.Charity:
                    return Property.RentalAccountAchClientId;
                case PaymentApplication.Other:
                    return Property.FeeAccountAchClientId;
                default:
                    return Property.RentalAccountAchClientId;
            }
        }

	    private int GetExpirationMonth()
        {
            return Int32.Parse(ExpirationDate.Substring(0, 2));
        }

        private int GetExpirationYear()
        {
            return Int32.Parse(ExpirationDate.Substring(2, 2));
        }

        private static string FormatExpirationDate(string expirationDate)
        {
            expirationDate = expirationDate.Replace("/", "");
            expirationDate = expirationDate.Replace("\\", "");
            expirationDate = expirationDate.Replace("-", "");

            return expirationDate;
        }

        public static PaymentResponse ConvertToMobilePaymentResponse(AchPaymentResponse response, PaymentApplication application)
	    {
            switch (application)
            {
                case PaymentApplication.Rent:
                case PaymentApplication.Other:
                    return new PaymentResponse { RentAndFeeResult = ConvertResponseResult(response.Result), RentAndFeeResponseMessage = response.ResponseDescription, RentAndFeeTransactionId = response.TransactionId };
                case PaymentApplication.Charity:
                    return new PaymentResponse { CharityResult = ConvertResponseResult(response.Result), CharityResponseMessage = response.ResponseDescription, CharityTransactionId = response.TransactionId };
                default:
                    return new PaymentResponse();
            }
	    }

        public static PaymentResponse ConvertToMobilePaymentResponse(CreditCardPaymentResponse response, PaymentApplication application)
	    {
            switch (application)
            {
                case PaymentApplication.Rent:
                case PaymentApplication.Other:
                    return new PaymentResponse { RentAndFeeResult = ConvertResponseResult(response.Result), RentAndFeeResponseMessage = response.ResponseMessage, RentAndFeeTransactionId = response.TransactionId };
                case PaymentApplication.Charity:
                    return new PaymentResponse { CharityResult = ConvertResponseResult(response.Result), CharityResponseMessage = response.ResponseMessage, CharityTransactionId = response.TransactionId };
                default:
                    return new PaymentResponse();
            }
	    }

	    private static MobileResponseResult ConvertResponseResult(GeneralResponseResult result)
	    {
	        if (result == GeneralResponseResult.Success)
	            return MobileResponseResult.Success;

	        try
	        {
	            return (MobileResponseResult)((int)result);
	        }
	        catch
	        {
	            return MobileResponseResult.GeneralFailure;
	        }
	    }

        public static void SendEmail(int propertyId, string subject, string body, string renterEmail)
        {
            //Salcedo - 2/23/2014 - changed call to include staff IsMainContacts
            //BulkMail.BulkMail.SendMailMessage(
            //    propertyId,
            //    "Renter",
            //    false,
            //    Settings.SendMailUsername,
            //    Settings.SendMailPassword,
            //    new MailAddress(Settings.PaymentEmail),
            //    subject,
            //    body,
            //    new List<MailAddress> { new MailAddress(renterEmail) }
            //    );
            BulkMail.BulkMail.SendMailMessageIncludePropertyEmailContacts(
                propertyId,
                "Renter",
                EfxSettings.SendMailUsername,
                EfxSettings.SendMailPassword,
                new MailAddress(EfxSettings.PaymentEmail),
                subject,
                body,
                new List<MailAddress> { new MailAddress(renterEmail) }
                );
        }
	}
}