﻿
namespace EfxFramework.Mobile
{
    public class PaymentItem
    {
        public decimal Amount { get; set; }
        public bool IsCharityPayment { get; set; }
        public string Description { get; set; }
    }
}
