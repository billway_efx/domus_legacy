﻿using EfxFramework.ExtensionMethods;
using EfxFramework.PaymentMethods;
using System.Collections.Generic;
using System.Linq;

namespace EfxFramework.Mobile
{
    public class AuthenticatePropertyStaffRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public AuthenticatePropertyStaffResponse AuthenticatePropertyStaff()
        {
            var PropertyStaff = new PropertyStaff(Username);
            
            if (!PropertyStaff.PropertyStaffId.HasValue || PropertyStaff.PropertyStaffId < 1)
                return new AuthenticatePropertyStaffResponse {Result = MobileResponseResult.FailedLogin, Message = ServiceResponseMessages.Failedlogin};

            var PropertyStaffId = PropertyStaff.PropertyStaffId.Value;
            var Session = RenterSession.SaveRenterSessionByRenterId(PropertyStaffId);

            if (PropertyStaff.Authenticate(Password))
            {
                return new AuthenticatePropertyStaffResponse
                    {
                        Result = MobileResponseResult.Success,
                        Message = ServiceResponseMessages.Successfullogin,
                        AuthenticatedStaff = BuildPropertyStaff(PropertyStaff),
                        SessionId = Session.SessionGuid.ToString(),
                        SessionExpiration = Session.ExpirationDate.ToString(EfxSettings.MobileDateFormatString),
                        Properties = BuildPropertyList(PropertyStaffId)
                    };
            }

            return new AuthenticatePropertyStaffResponse { Result = MobileResponseResult.FailedLogin, Message = ServiceResponseMessages.Failedlogin };
        }

        private static List<Property> BuildPropertyList(int propertyStaffId)
        {
            return (from Property in EfxFramework.Property.GetPropertyListByPropertyStaffId(propertyStaffId) 
                    let PropertyId = Property.PropertyId
                    where PropertyId.HasValue 
                    select new Property {PropertyId = PropertyId.Value, PropertyName = Property.PropertyName}).ToList();
        }

        private static MobilePropertyStaff BuildPropertyStaff(PropertyStaff staff)
        {
            return new MobilePropertyStaff
                {
                    PropertyStaffId = staff.PropertyStaffId.HasValue ? staff.PropertyStaffId.Value : 0,
                    FirstName = staff.FirstName,
                    LastName = staff.LastName,
                    MainPhoneNumber = staff.MainPhoneNumber.FormatPhoneNumber(),
                    MobilePhoneNumber = staff.MobilePhoneNumber.FormatPhoneNumber(),
                    PrimaryEmailAddress = staff.PrimaryEmailAddress
                };
        }
    }
}
