﻿using EfxFramework.PaymentMethods;
using System.Collections.Generic;

//Salcedo - 4/29/2016 - added new response classes for Wallet functionality, designed for use with new mobile app

namespace EfxFramework.Mobile
{
    public class Wallet_GetPaymentMethodsResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public List<EfxFramework.Wallet_PaymentMethod> Wallet_PaymentMethods { get; set; }
    }
    public class Wallet_AddCCPaymentMethodResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public int Wallet_PaymentMethodId { get; set; }
    }
    public class Wallet_AddACHPaymentMethodResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public int Wallet_PaymentMethodId { get; set; }
    }
    public class Wallet_UpdateCCPaymentMethodResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
    }
    public class Wallet_UpdateACHPaymentMethodResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
    }
    public class Wallet_RemovePaymentMethodResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
    }
    public class Wallet_PaySingleItemResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public string TransactionId { get; set; }
    }

    //Salcedo - 7/18/2016 - added response objects for pre/post auth API functions
    public class Wallet_PreAuthCCResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public string TransactionId { get; set; }
    }
    public class Wallet_PostAuthCCResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public string TransactionId { get; set; }
    }
    public class Wallet_ReverseAuthResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public string TransactionId { get; set; }
    }
    public class Wallet_GetRenterPaymentHistoryResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public List<EfxFramework.Wallet_PaymentHistoryItem> Wallet_PaymentHistoryItems { get; set; }
    }
    public class GetPayableFeeDetailResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public List<EfxFramework.PayableItem_FeeDetail> PayableItem_FeeDetailItems { get; set; }
    }

    public class GetCalendarEventsForRenter_Response
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public List<EfxFramework.CalendarEvent> CalendarEventItems { get; set; }
    }

    //cakel: 08/09/2016
    public class GetConvFeeForPayment_Response
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public string ConvenienceFee { get; set; }
    }

    public class Wallet_PayByCash
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public string OrderUrl { get; set; }
    }


}
