﻿using EfxFramework.ExtensionMethods;

namespace EfxFramework.Mobile
{
	public class Contact
	{
        private string _PhoneNumber;

		public string Name { get; set; }
		public string EmailAddress { get; set; }
        public string PhoneNumber { get { return _PhoneNumber.FormatPhoneNumber(); } set { _PhoneNumber = value; } }
	}
}