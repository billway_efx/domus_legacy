﻿using System.Collections.Generic;
using System.Linq;
using EfxFramework.PaymentMethods;

namespace EfxFramework.Mobile
{
	public class AuthenticationRequest
	{
		public string Username { get; set; }
		public string Password { get; set; }

		public AuthenticationResponse AuthenticateUser()
		{
			//Get the requested renter
			var CurrentRenter = new EfxFramework.Renter(Username);
            var Property = EfxFramework.Property.GetPropertyByRenterId(CurrentRenter.RenterId);

            if (!CurrentRenter.IsValid || !CurrentRenter.Authenticate(Password))
            {
                //The user failed to authenticate
                return new AuthenticationResponse
                {
                    Result = MobileResponseResult.FailedLogin,
                    Message = ServiceResponseMessages.Failedlogin,
                    SessionId = null,
                    ExpirationDate = null,
                    AuthenticatedRenter = null,
                    CharityList = null,
                    ContactList = null,
                    ECheckFee = 0.0M,
                    CreditCardFee = 0.0M,
                    Program = EfxFramework.Property.Program.Unassigned,
                    PaymentItems = new List<PaymentItem>(),
                    ShowDetailPaymentItems = Property.PropertyId.HasValue && Property.PropertyId.Value > 0 && Property.ShowPaymentDetail
                };
            }

            var Session = RenterSession.SaveRenterSessionByRenterId(CurrentRenter.RenterId);
		    
			//Create an authentication response
			var RetVal = new AuthenticationResponse
			{
				Result = MobileResponseResult.Success,
				Message = ServiceResponseMessages.Successfullogin,
                SessionId = Session.SessionGuid.ToString(),
                ExpirationDate = Session.ExpirationDate.ToString(EfxSettings.MobileDateFormatString),
				AuthenticatedRenter = new Renter { RenterId = CurrentRenter.RenterId, RentDueDate = Lease.GetRentDueDate(CurrentRenter.RenterId) },
				CharityList = Charity.GetAllCharityDictionary(),
                ContactList = EfxFramework.Property.GetPropertyAndPropertyStaffMobileContactListByRenterId(CurrentRenter.RenterId),
                ECheckFee = PaymentAmount.GetPaymentAmount(0.0M, CurrentRenter, PaymentType.ECheck, true).FeeAmount,
                CreditCardFee = PaymentAmount.GetPaymentAmount(0.0M, CurrentRenter, PaymentType.CreditCard, true).FeeAmount,
                Program = EfxFramework.Property.GetProgramByProgramEntity(ProgramEntity.GetProgramEntityByRenterId(CurrentRenter.RenterId)),
                PaymentItems = BuildPaymentItems(CurrentRenter),
                ShowDetailPaymentItems = Property.PropertyId.HasValue && Property.PropertyId.Value > 0 && Property.ShowPaymentDetail
			};

			//Return
		    return RetVal;
		}

        private static List<PaymentItem> BuildPaymentItems(EfxFramework.Renter renter)
        {
            var Items = new List<PaymentItem>();
            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);

            if (Lease.LeaseId < 1)
            {
                Items.Add(new PaymentItem {Amount = 0.00M, Description = "Lease record not found, cannot apply payment."});
                return Items;
            }

            if (!Lease.CurrentBalanceDue.HasValue || Lease.CurrentBalanceDue.Value + Lease.TotalFees < 0.01M)
            {
                Items.Add(new PaymentItem { Amount = 0.00M, Description = "There is no current balance due at this time." });
                return Items;
            }

            Items.Add(new PaymentItem {Amount = Lease.RentAmount, Description = "Rent"});
            // ReSharper disable PossibleInvalidOperationException
            Items.AddRange(from Fee in LeaseFee.GetFeesByLeaseId(Lease.LeaseId) where Fee.FeeAmount.HasValue && Fee.FeeAmount.Value > 0.00M select new PaymentItem {Amount = Fee.FeeAmount.Value, Description = Fee.FeeName});
            // ReSharper restore PossibleInvalidOperationException

            var Total = Lease.RentAmount + CalculateMonthlyFees(Lease.LeaseId);

            if (Total != Lease.CurrentBalanceDue.Value + Lease.TotalFees)
            {
                var Difference = Lease.CurrentBalanceDue.Value + Lease.TotalFees - Total;

                Items.Add(Difference > 0.00M ? new PaymentItem {Amount = Difference, Description = "Past due amount."} : new PaymentItem {Amount = Difference, Description = "Credit amount."});
            }

            return Items;
        }

        private static decimal CalculateMonthlyFees(int leaseId)
        {
            return LeaseFee.GetFeesByLeaseId(leaseId).Sum(lf => lf.FeeAmount).Value;
        }
	}
}