﻿using EfxFramework.PaymentMethods;
using System.Collections.Generic;

namespace EfxFramework.Mobile
{
    public class AuthenticationResponse
	{
		public MobileResponseResult Result { get; set; }
		public string Message { get; set; }
        public string SessionId { get; set; }
        public string ExpirationDate { get; set; }
		public Renter AuthenticatedRenter { get; set; }
		public Dictionary<int, string> CharityList { get; set; }
		public List<Contact> ContactList { get; set; }
        public decimal ECheckFee { get; set; }
        public decimal CreditCardFee { get; set; }
        public EfxFramework.Property.Program Program { get; set; }
        public List<PaymentItem> PaymentItems { get; set; }
        public bool ShowDetailPaymentItems { get; set; }
	}
}