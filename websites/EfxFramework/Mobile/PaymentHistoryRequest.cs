﻿using EfxFramework.PaymentMethods;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace EfxFramework.Mobile
{
	public class PaymentHistoryRequest
	{
		//Public Properties
		public string SessionId { get; set; }

		//Public Methods
		public PaymentHistoryResponse GetPaymentHistory()
		{
            if (RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new PaymentHistoryResponse { Result = MobileResponseResult.Success, Message = ServiceResponseMessages.Paymenthistoryretrieved, 
                    PaymentHistory = BuildPaymentHistory(Guid.Parse(SessionId)) };

            return new PaymentHistoryResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };
		}

        private static List<PaymentHistory> BuildPaymentHistory(Guid sessionId)
        {
            var Session = RenterSession.GetRenterSessionBySessionGuid(sessionId);
            var Results = new List<PaymentHistory>();

            foreach (var P in Payment.GetMobilePaymentListByRenterId(Session.RenterId))
            {
                var History = new PaymentHistory {Amount = 0, Description = string.Empty};

                History.Amount += AddAmount(P.RentAmount);
                History.Amount += AddAmount(P.CharityAmount);
                History.Amount += AddAmount(P.OtherAmount1);

                if (!String.IsNullOrEmpty(P.OtherDescription2) && !P.OtherDescription2.ToLower().Contains("convenience fee"))
                    History.Amount += AddAmount(P.OtherAmount2);

                History.Amount += AddAmount(P.OtherAmount3);
                History.Amount += AddAmount(P.OtherAmount4);
                History.Amount += AddAmount(P.OtherAmount5);
                History.Description = AddDescription(History.Description, P.RentAmountDescription);
                History.Description = AddDescription(History.Description, P.CharityName);
                History.Description = AddDescription(History.Description, P.OtherDescription1);

                if (!String.IsNullOrEmpty(P.OtherDescription2) && !P.OtherDescription2.ToLower().Contains("convenience fee"))
                    History.Description = AddDescription(History.Description, P.OtherDescription2);

                History.Description = AddDescription(History.Description, P.OtherDescription3);
                History.Description = AddDescription(History.Description, P.OtherDescription4);
                History.Description = AddDescription(History.Description, P.OtherDescription5);
                History.PaymentDate = P.TransactionDateTime.ToString(EfxSettings.MobileDateFormatString);
                History.PaymentMethod = (PaymentRequest.PaymentMethodEnum)Enum.Parse(typeof(PaymentRequest.PaymentMethodEnum), P.PaymentTypeId.ToString(CultureInfo.InvariantCulture));

                Results.Add(History);
            }

            return Results;
        }

        private static decimal AddAmount(decimal? amount)
        {
            return amount.HasValue ? amount.Value : 0.0M;
        }

        private static string AddDescription(string baseDescription, string appendedDescription)
        {
            if (!string.IsNullOrEmpty(baseDescription))
                return !String.IsNullOrEmpty(appendedDescription) ? String.Format("{0}, {1}", baseDescription, appendedDescription) : baseDescription;

            return appendedDescription;
        }
	}
}