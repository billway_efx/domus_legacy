﻿using EfxFramework.PaymentMethods;
using System.Collections.Generic;

//Salcedo - 5/31/2016 - added new response classes for Renter Gate Code functionality, designed for use with new mobile app

namespace EfxFramework.Mobile
{
    public class Renter_GateCode_GetAll_Response
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public List<EfxFramework.RenterGateCode> RenterGateCodes { get; set; }
    }
    public class Renter_GateCode_GetByInternalId_Response
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public EfxFramework.RenterGateCode RenterGateCode { get; set; }
    }
    public class Renter_GateCode_GetByUserId_Response
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public EfxFramework.RenterGateCode RenterGateCode { get; set; }
    }
    public class Renter_GateCode_Add_Response
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public int GateCodeId { get; set; }
    }
    public class Renter_GateCode_UpdateByInternalId_Response
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
    }
    public class Renter_GateCode_UpdateByUserId_Response
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
    }
    public class Renter_GateCode_DeleteByInternalId_Response
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
    }
    public class Renter_GateCode_DeleteByUserId_Response
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
    }

}
