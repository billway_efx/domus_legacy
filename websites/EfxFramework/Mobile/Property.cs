﻿namespace EfxFramework.Mobile
{
    public class Property
    {
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
    }
}
