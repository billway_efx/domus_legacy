﻿using EfxFramework.PaymentMethods;

namespace EfxFramework.Mobile
{
    public class SetRenterResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
    }
}
