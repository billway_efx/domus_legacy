﻿using EfxFramework.PaymentMethods;
using System.Collections.Generic;

namespace EfxFramework.Mobile
{
    public class AuthenticatePropertyStaffResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public string SessionId { get; set; }
        public string SessionExpiration { get; set; }
        public List<Property> Properties { get; set; }
        public MobilePropertyStaff AuthenticatedStaff { get; set; }
    }
}
