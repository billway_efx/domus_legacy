﻿using System;
using EfxFramework.PaymentMethods;
using Mb2x.ExtensionMethods;

namespace EfxFramework.Mobile
{
    public class PaymentRequestV2 : PaymentProcessingBase
    {
        public enum PaymentMethodEnum
        {
            ECheck = 1,
            CreditCardOnFile = 2,
            ScanCreditCard = 3
        }
        public PaymentMethodEnum PaymentMethod { get; set; }
        public string SessionId { get; set; }

        public PaymentRequestV2(PaymentChannel channel, int propertyId) 
            : base(channel, propertyId){}

        public PaymentRequestV2(PaymentChannel channel, int propertyId, int residentId) 
            : base(channel, propertyId, residentId){}

        public PaymentResponseBase ProcessPayment()
        {
            var Renter = new EfxFramework.Renter(RenterSession.GetRenterSessionBySessionGuid(Guid.Parse(SessionId)).RenterId) { IsParticipating = true };
            EfxFramework.Renter.Set(Renter, "System Process");

            var Request = new PaymentRequestV2(PaymentChannel.Mobile, Property.PropertyId.HasValue ? Property.PropertyId.Value : 0, Renter.RenterId);

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                return new PaymentResponseBase { Result = (int) MobileResponseResult.PropertyNotFound, Message = ServiceResponseMessages.PropertyNotFound };

            if (Renter.AcceptedPaymentTypeId == 4)
                return new PaymentResponseBase { Result = (int) MobileResponseResult.CannotAcceptRenterPayment, Message = ServiceResponseMessages.CannotAcceptRenterPayment };

            if (RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
            
            switch (PaymentMethod)
            {
                case PaymentMethodEnum.ECheck:
                    if (Renter.AcceptedPaymentTypeId == 3)
                        return new PaymentResponseBase { Result = (int) MobileResponseResult.CashEquivalentRequired, Message = ServiceResponseMessages.CashEquivalentRequired };

                    return Request.ProcessResidentPayment(new EcheckDetails(Request.GetResidentAddress(), Renter.FirstName, Renter.LastName, Request.PayerAch.BankAccountNumber, 
                            Request.PayerAch.BankRoutingNumber, AccountType.Checking), Request.GetPaymentAmount(false, PaymentType.ECheck));
                    
                case PaymentMethodEnum.CreditCardOnFile:
                    return Request.ProcessResidentPayment(new CreditCardDetails(Request.GetResidentAddress(), Renter.FirstName, Renter.LastName, Request.PayerCc.CreditCardHolderName,
                            Request.PayerCc.CreditCardAccountNumber, Request.PayerCc.CreditCardExpirationDate.ToDateTime(), null));
                case PaymentMethodEnum.ScanCreditCard:
                    return Request.ProcessResidentPayment(new CreditCardDetails(Request.GetResidentAddress(), Renter.FirstName, Renter.LastName, Request.PayerCc.CreditCardHolderName,
                            Request.PayerCc.CreditCardAccountNumber, Request.PayerCc.CreditCardExpirationDate.ToDateTime(), null));
                default:
                    return new PaymentResponseBase { Result = (int) MobileResponseResult.UnknownPaymentType, Message = ServiceResponseMessages.UnknownPaymentTypeEntered, TransactionId = null };
            }

            return new PaymentResponseBase { Result = (int) MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };
        }
    }
}
