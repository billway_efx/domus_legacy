﻿namespace EfxFramework.Mobile
{
    public class MobilePropertyStaff
    {
        public int PropertyStaffId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MainPhoneNumber { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string PrimaryEmailAddress { get; set; }
    }
}
