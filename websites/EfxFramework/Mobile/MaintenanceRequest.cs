﻿using EfxFramework.ExtensionMethods;
using EfxFramework.PaymentMethods;
using EfxFramework.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace EfxFramework.Mobile
{
    public class MaintenanceRequest
    {
        private EfxFramework.Renter _Resident;
        private int _PropertyId;
        private string _RequestType;
        private string _Priority;

        public int RenterId { get; set; }
        public int MaintenanceTypeId { get; set; }
        public int MaintenancePriorityId { get; set; }
        public string MaintenanceSubmissionDate { get; set; }
        public string MaintenanceDescription { get; set; }
        public string SessionId { get; set; }
        public bool CcResident { get; set; }

        public MaintenanceResponse SubmitMaintenanceRequest()
        {
            if (!RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new MaintenanceResponse {Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout};

            return CreateMaintenanceRequest();
        }

        

        public MaintenanceResponse CreateMaintenanceRequest()
        {
            MaintenanceResponse Response;

            if (!ValidateMaintenanceRequest(RenterId, out Response))
                return Response;

            var Request = new EfxFramework.MaintenanceRequest
                {
                    MaintenanceRequestTypeId = MaintenanceTypeId,
                    MaintenanceRequestPriorityId = MaintenancePriorityId,
                    PropertyId = _PropertyId,
                    RenterId = _Resident.RenterId,
                    MaintenanceRequestDate = DateTime.Parse(MaintenanceSubmissionDate),
                    MaintenanceRequestDescription = MaintenanceDescription,
                    IsActive = true
                };

            try
            {
                EfxFramework.MaintenanceRequest.Set(Request);
                SendRequestMessage();
            }
            catch
            {
                Response.Result = MobileResponseResult.GeneralFailure;
                Response.Message = ServiceResponseMessages.GeneralFailure;
                return Response;
            }

            Response.Result = MobileResponseResult.Success;
            Response.Message = ServiceResponseMessages.MaintenanceRequestSuccessful;

            return Response;
        }

        private bool ValidateMaintenanceRequest(int renterId, out MaintenanceResponse response)
        {
            response = new MaintenanceResponse();

            if (RenterId < 1)
            {
                response.Result = MobileResponseResult.RenterIdRequired;
                response.Message = ServiceResponseMessages.RenterIdRequired;
                return false;
            }

            var Resident = new EfxFramework.Renter(RenterId);

            if (Resident.RenterId < 1)
            {
                response.Result = MobileResponseResult.RenterNotFound;
                response.Message = ServiceResponseMessages.RenterNotFound;
                return false;
            }

            _Resident = Resident;

            if (MaintenanceTypeId < 1)
            {
                response.Result = MobileResponseResult.MaintenanceTypeIdRequired;
                response.Message = ServiceResponseMessages.MaintenanceTypeIdRequired;
                return false;
            }

            try
            {
                _RequestType = ((MaintenanceRequestType) MaintenanceTypeId).GetFriendlyName();
            }
            catch
            {
                response.Result = MobileResponseResult.InvalidMaintenanceRequestType;
                response.Message = ServiceResponseMessages.InvalidMaintenanceRequestType;
                return false;
            }

            if (MaintenancePriorityId < 1)
            {
                response.Result = MobileResponseResult.MaintenancePriorityRequired;
                response.Message = ServiceResponseMessages.MaintenancePriorityRequired;
                return false;
            }

            try
            {
                _Priority = ((MaintenanceRequestPriority) MaintenancePriorityId).ToString();
            }
            catch
            {
                response.Result = MobileResponseResult.InvalidMaintenancePriority;
                response.Message = ServiceResponseMessages.InvalidMaintenancePriority;
                return false;
            }

            var Property = EfxFramework.Property.GetPropertyByRenterId(renterId);

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                response.Result = MobileResponseResult.PropertyNotFound;
                response.Message = ServiceResponseMessages.PropertyNotFound;
                return false;
            }

            _PropertyId = Property.PropertyId.Value;

            if (String.IsNullOrEmpty(MaintenanceSubmissionDate))
            {
                response.Result = MobileResponseResult.MaintenanceSubmissionDateRequired;
                response.Message = ServiceResponseMessages.MaintenanceSubmissionDateRequired;
                return false;
            }

            DateTime SubmissionDate;

            if (!DateTime.TryParse(MaintenanceSubmissionDate, out SubmissionDate))
            {
                response.Result = MobileResponseResult.MaintenanceSubmissionDateInvalid;
                response.Message = ServiceResponseMessages.MaintenanceSubmissionDateInvalid;
                return false;
            }

            if (String.IsNullOrEmpty(MaintenanceDescription))
            {
                response.Result = MobileResponseResult.MaintenanceDescriptionRequired;
                response.Message = ServiceResponseMessages.MaintenanceDescriptionRequired;
                return false;
            }

            return true;
        }

        private void SendRequestMessage()
        {
            var StaffList = PropertyStaff.GetPropertyStaffListByPropertyId(_PropertyId);
            var Manager = StaffList.FirstOrDefault(s => s.IsPropertyGeneralManager);

            if (Manager == null || !Manager.PropertyStaffId.HasValue || Manager.PropertyStaffId.Value < 1)
                Manager = StaffList.FirstOrDefault(s => s.PropertyStaffId.HasValue && s.PropertyStaffId.Value > 0);

            if (Manager == null)
                return;

            var Sender = new MailAddress(_Resident.PrimaryEmailAddress);
            //Salcedo - 2/23/2014 - changed call to only include staff IsMainContacts
            //var Recipients = new List<MailAddress>
            //    {
            //        new MailAddress(Manager.PrimaryEmailAddress)
            //    };
            var Recipients = Enumerable.Empty<MailAddress>().ToList();

            //SendMailX will automatically include all IsMainContact staff members by calling the new SendMailMessageIncludePropertyEmailContacts function.

            if (CcResident)
                Recipients.Add(Sender);

            SendMailX(Sender, Recipients);
        }

        private void SendMailX(MailAddress sender, List<MailAddress> recipients)
        {
            //Salcedo - 2/23/2014 - changed call to only include staff IsMainContacts
            //BulkMail.BulkMail.SendMailMessage(
            //    null,
            //    "EFX",
            //    false,
            //    Settings.SendMailUsername,
            //    Settings.SendMailPassword,
            //       sender,
            //    "Maintenance Request",
            //    string.Format(EmailTemplates.MaintenanceRequest, _RequestType, _Priority, MaintenanceDescription, string.Empty, string.Empty),
            //       recipients);
            
            //Salcedo - 6/29/2016 - bad logic on my part - removing - it's possible for there to be 0 recipients at this point in the process, 
            //but SendMailMessageIncludePropertyEmailContacts will still send the email to the property contacts anyway
            ////Salcedo - 6/10/2016 - accounting for no recipients
            //if (recipients.Count == 0)
            //    return;

            try
            {
                BulkMail.BulkMail.SendMailMessageIncludePropertyEmailContacts(
                    _PropertyId,
                    "EFX",
                    EfxSettings.SendMailUsername,
                    EfxSettings.SendMailPassword,
                       sender,
                    "Maintenance Request",
                    string.Format(EmailTemplates.MaintenanceRequest, _RequestType, _Priority, MaintenanceDescription, _Resident.DisplayName, _Resident.PrimaryEmailAddress, _Resident.MainPhoneNumber, 
                        _Resident.Unit),
                       recipients);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
