﻿using EfxFramework.PaymentMethods;

namespace EfxFramework.Mobile
{
    public class GetRenterByRenterIdResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public RenterByPropertyIdRenter Renter { get; set; }
        public bool ShowDetailPaymentItems { get; set; } 
    }
}
