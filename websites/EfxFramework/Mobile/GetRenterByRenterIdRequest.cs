﻿using EfxFramework.PaymentMethods;
using System;

namespace EfxFramework.Mobile
{
    public class GetRenterByRenterIdRequest
    {
        public string SessionId { get; set; }
        public int RenterId { get; set; }

        public GetRenterByRenterIdResponse GetRenterByRenterId()
        {
            var Renter = new EfxFramework.Renter(RenterId);
            var Property = EfxFramework.Property.GetPropertyByRenterId(Renter.RenterId);

            if (!Renter.IsValid)
                return new GetRenterByRenterIdResponse {Result = MobileResponseResult.GeneralFailure, Message = ServiceResponseMessages.RenterNotFound};
            
            return RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)) ? 
                new GetRenterByRenterIdResponse 
                {
                    Result = MobileResponseResult.Success, 
                    Message = ServiceResponseMessages.RentersSuccessfullyRetrieved, 
                    Renter = BuildRenter(Renter),
                    ShowDetailPaymentItems = Property.PropertyId.HasValue && Property.PropertyId.Value > 0 && Property.ShowPaymentDetail
                } : 
                new GetRenterByRenterIdResponse
                    {
                        Result = MobileResponseResult.SessionTimeout, 
                        Message = ServiceResponseMessages.Sessiontimeout
                    };
        }

        private static RenterByPropertyIdRenter BuildRenter(EfxFramework.Renter renter)
        {
            return new RenterByPropertyIdRenter(renter.RenterId)
                {
                    RenterId = renter.RenterId,
                    RentDueDate = Lease.GetRentDueDate(renter.RenterId),
                    FirstName = renter.FirstName,
                    LastName = renter.LastName,
                    PhoneNumber = renter.MainPhoneNumber,
                    EmailAddress = renter.PrimaryEmailAddress,
                    IsRentLate = renter.IsRentLate,
                    Address = new RenterAddress
                    {
                        StreetAddress = renter.StreetAddress,
                        Unit = renter.Unit,
                        City = renter.City,
                        PostalCode = renter.PostalCode,
                        StateProvince = BuildStateProvince(renter.StateProvinceId)
                    }
                };
        }

        private static string BuildStateProvince(int? stateProvinceId)
        {
            return stateProvinceId.HasValue ? ((StateProvince)stateProvinceId.Value).ToString() : string.Empty;
        }
    }
}
