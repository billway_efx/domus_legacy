﻿using EfxFramework.PaymentMethods;

namespace EfxFramework.Mobile
{
	public class PaymentResponse
	{
        public MobileResponseResult RentAndFeeResult { get; set; }
        public string RentAndFeeResponseMessage { get; set; }
        public string RentAndFeeTransactionId { get; set; }
        public MobileResponseResult CharityResult { get; set; }
        public string CharityResponseMessage { get; set; }
        public string CharityTransactionId { get; set; }
        public bool RedirectToIvr { get; set; }
        public string PaymentQueueNumber { get; set; }
        public decimal ConvenienceFee { get; set; }
	}
}