﻿namespace EfxFramework.Mobile
{
	public class PaymentHistory
	{
		public decimal Amount { get; set; }
		public PaymentRequest.PaymentMethodEnum PaymentMethod { get; set; }
		public string Description { get; set; }
		public string PaymentDate { get; set; }
	}
}