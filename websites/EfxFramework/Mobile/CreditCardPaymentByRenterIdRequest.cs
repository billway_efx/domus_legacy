﻿using EfxFramework.PaymentMethods;
using EfxFramework.PaymentMethods.CreditCard;
using System;
using System.Collections.Generic;
using System.Globalization;
using EfxFramework.PublicApi.Payment;
using EfxFramework.Resources;
using RPO;

namespace EfxFramework.Mobile
{
    public class CreditCardPaymentByRenterIdRequest
    {
        private string _ExpirationDate;

        public string SessionId { get; set; }
        public int RenterId { get; set; }
        public decimal Amount { get; set; }
        public string CardNumber { get; set; }
        public string ExpirationDate { get { return FormatExpirationDate(_ExpirationDate); } set { _ExpirationDate = value; } }
        public string Cvv { get; set; }
        public string CardholderName { get; set; }

        public CreditCardPaymentByRenterIdResponse ProcessPayment()
        {
            //return new CreditCardPaymentByRenterIdResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            if (!RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new CreditCardPaymentByRenterIdResponse {Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout};

            ActivityLog AL = new ActivityLog();
            AL.WriteLog("SessionId: " + SessionId, "CreditCardPaymentByRenterIdResponse Mobile.ProcessPayment() started.", (short)LogPriority.LogAlways);

            var Renter = new EfxFramework.Renter(RenterId) { IsParticipating = true };

            //Salcedo - 4/14/2014 - We should do this before attempting to set a value in the Renter table
            if (!Renter.IsValid)
                return new CreditCardPaymentByRenterIdResponse { Result = MobileResponseResult.GeneralFailure, Message = ServiceResponseMessages.RenterNotFound };

            EfxFramework.Renter.Set(Renter, "System Process");

            var PayerCreditCard = new PayerCreditCard
                {
                    CreditCardAccountNumber = CardNumber,
                    CreditCardExpirationMonth = GetExpirationMonth(),
                    CreditCardExpirationYear = GetExpirationYear(),
                    CreditCardHolderName = CardholderName
                };

            //Salcedo - 4/14/2014 - We should do this before attempting to set a value in the Renter table
            //if (!Renter.IsValid)
            //    return new CreditCardPaymentByRenterIdResponse {Result = MobileResponseResult.GeneralFailure, Message = ServiceResponseMessages.RenterNotFound};

            return ProcessPayment(this, Renter, PayerCreditCard);
        }

        private static CreditCardPaymentByRenterIdResponse ProcessPayment(CreditCardPaymentByRenterIdRequest request, EfxFramework.Renter renter, PayerCreditCard payerCreditCard)
        {

            ActivityLog AL = new ActivityLog();
            AL.WriteLog("RenterId: " + renter.RenterId.ToString(), "CreditCardPaymentByRenterIdResponse Mobile.ProcessPayment(request, renter, payerCreditCard) started.",
                (short)LogPriority.LogAlways);

            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);
            var Property = EfxFramework.Property.GetPropertyByRenterId(renter.RenterId);

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                return new CreditCardPaymentByRenterIdResponse {Result = MobileResponseResult.PropertyNotFound, Message = ServiceResponseMessages.PropertyNotFound};

            if (renter.AcceptedPaymentTypeId == 4)
                return new CreditCardPaymentByRenterIdResponse {Result = MobileResponseResult.CannotAcceptRenterPayment, Message = ServiceResponseMessages.CannotAcceptRenterPayment};

            var CustomFields = new List<CustomField>
                {
                    new CustomField("customfield1", Property.PropertyId.Value.ToString(CultureInfo.InvariantCulture)), 
                    new CustomField("customfield2", Lease.UnitNumber), 
                    new CustomField("customfield3", String.Format("{0}{1}", DateTime.Now.Month.ToString(CultureInfo.InvariantCulture), DateTime.Now.Year.ToString(CultureInfo.InvariantCulture))), 
                    new CustomField("customfield5", "0"), 
                    new CustomField("customfield6", (Convert.ToInt32(request.Amount * 100)).ToString(CultureInfo.InvariantCulture)),
                    new CustomField("customfield10", Property.PropertyCode)
                };

            var RentResponse = new CreditCardPaymentResponse();

            if (request.Amount > 0.0M)
            {

                AL.WriteLog("RenterId: " + renter.RenterId.ToString(),
                    "CreditCardPaymentByRenterIdResponse Mobile.ProcessPayment(request, renter, payerCreditCard) ProcessCreditCard for " + request.Amount.ToString(),
                    (short)LogPriority.LogAlways);

                RentResponse = CreditCardPayment.ProcessCreditCard(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, renter.IsCreditCardTestRenter, request.Amount, payerCreditCard.CreditCardAccountNumber,
                    payerCreditCard.CreditCardExpirationDate, request.Cvv, payerCreditCard.CreditCardHolderName, CustomFields);
            }

            var Response = new CreditCardPaymentByRenterIdResponse
                {
                    Result = ConvertResponseResult(RentResponse.Result),
                    Message = RentResponse.ResponseMessage,
                    TransactionId = RentResponse.TransactionId
                };

            if (Response.Message == ServiceResponseMessages.Ccaccepted)
                ProcessVoid(Response.TransactionId);

            if (Response.Result == MobileResponseResult.Success)
            {
                AL.WriteLog("RenterId: " + renter.RenterId.ToString(),
                    "CreditCardPaymentByRenterIdResponse Mobile.ProcessPayment(request, renter, payerCreditCard) Payment Success for " + request.Amount.ToString(),
                    (short)LogPriority.LogAlways);

                Lease.CurrentBalanceDue -= request.Amount;
                Lease.CurrentBalanceDueLastUpdated = DateTime.UtcNow;
                //Lease.PaymentDueDate = Lease.PaymentDueDate.HasValue ? Lease.PaymentDueDate.Value.AddMonths(1) : DateTime.UtcNow.AddMonths(1);
                Lease.Set(Lease);
                SetPayment(request, renter, payerCreditCard, Response);

                //Salcedo - 4/14/2014 - Need to use the credit card company Id, so get the Transaction
                var TheTransaction = Transaction.GetTransactionByExternalTransactionId(Response.TransactionId);

                //Salcedo - 3/12/2014 - replaced with new html email template
                //PaymentRequest.SendEmail(Property.PropertyId.Value, "Payment Confirmation", string.Format(EmailTemplates.OneTimeRentPayment, 
                //    renter.DisplayName, "Mobile Payment", DateTime.UtcNow.ToShortDateString(), Response.Result, Response.ConvenienceFee.ToString("C"), 
                //    request.Amount.ToString("C")), renter.PrimaryEmailAddress);
                String RecipientName = renter.DisplayName;
                //cakel: BUGID000180 
                String RenterProperty = Property.PropertyName;
                String PaymentAmount = "Mobile Payment";
                String TransactionDate = DateTime.UtcNow.ToShortDateString();

                //Salcedo - 4/14/2014 - changed to use the credit card company ID
                String InternalTransactionId = TheTransaction.InternalTransactionId;

                String ConvenienceAmount = Response.ConvenienceFee.ToString("C");
                String TotalAmount = request.Amount.ToString("C");
                String Body = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-payment-success.html").Replace("[[ROOT]]",
                    EmailTemplateService.EmailTemplateService.RootPath), RecipientName, RenterProperty, PaymentAmount, TransactionDate, InternalTransactionId, ConvenienceAmount, TotalAmount);
                
                AL.WriteLog("RenterId: " + renter.RenterId.ToString(),
                    "CreditCardPaymentByRenterIdResponse Mobile.ProcessPayment(request, renter, payerCreditCard) Sending payment success email to " + renter.PrimaryEmailAddress,
                    (short)LogPriority.LogAlways);
                
                PaymentRequest.SendEmail(Property.PropertyId.Value, "Payment Confirmation", Body, renter.PrimaryEmailAddress);

                
            }

            return Response;
        }

        private static void ProcessVoid(string transactionId)
        {
            CreditCardPayment.VoidCreditCardPayment(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, transactionId);
        }

        private static void SetPayment(CreditCardPaymentByRenterIdRequest request, EfxFramework.Renter renter, PayerCreditCard payerCreditCard, 
            CreditCardPaymentByRenterIdResponse response)
        {
            if (!renter.PayerId.HasValue)
                renter.PayerId = 0;

            var Amount = new PaymentAmount(request.Amount);

            var PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.CreditCard, null, null, payerCreditCard.CreditCardHolderName, payerCreditCard.CreditCardAccountNumber,
                payerCreditCard.CreditCardExpirationDate, Amount, PaymentApplication.Rent.ToString(), null, null, null, null, response.TransactionId, false, null, 
                ((int)response.Result).ToString(CultureInfo.InvariantCulture), response.Message, null, PaymentChannel.Mobile, PaymentStatus.Approved));

            Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int) response.Result, response.Message);
        }

        private int GetExpirationMonth()
        {
            return Int32.Parse(ExpirationDate.Substring(0, 2));
        }

        private int GetExpirationYear()
        {
            return Int32.Parse(ExpirationDate.Substring(2, 2));
        }

        private static string FormatExpirationDate(string expirationDate)
        {
            expirationDate = expirationDate.Replace("/", "");
            expirationDate = expirationDate.Replace("\\", "");
            expirationDate = expirationDate.Replace("-", "");

            return expirationDate;
        }

        public static CreditCardPaymentByRenterIdResponse ConvertToMobilePaymentResponse(CreditCardPaymentResponse response)
        {
            return new CreditCardPaymentByRenterIdResponse { Result = ConvertResponseResult(response.Result), Message = response.ResponseMessage, TransactionId = response.TransactionId };
        }

        private static MobileResponseResult ConvertResponseResult(GeneralResponseResult result)
        {
            if (result == GeneralResponseResult.Success)
                return MobileResponseResult.Success;

            try
            {
                return (MobileResponseResult)((int)result);
            }
            catch
            {
                return MobileResponseResult.GeneralFailure;
            }
        }
    }
}
