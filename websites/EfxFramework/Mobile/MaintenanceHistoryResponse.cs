﻿using EfxFramework.PaymentMethods;
using System.Collections.Generic;

namespace EfxFramework.Mobile
{
    public class MaintenanceHistoryResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public List<EfxFramework.MaintenanceRequest> MaintenanceRequests { get; set; }
    }
}
