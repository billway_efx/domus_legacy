﻿using EfxFramework.PaymentMethods;
using EfxFramework.PaymentMethods.Ach;
using System;
using System.Globalization;
using EfxFramework.PublicApi.Payment;
using EfxFramework.Resources;
using RPO;

namespace EfxFramework.Mobile
{
    public class EcheckPaymentByRenterIdRequest
    {
        public string SessionId { get; set; }
        public int RenterId { get; set; }
        public string BankAccountNumber { get; set; }
        public string RoutingNumber { get; set; }
        public decimal Amount { get; set; }
    
        public EcheckPaymentByRenterIdResponse ProcessPayment()
        {
            if (!RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new EcheckPaymentByRenterIdResponse {Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout};

            ActivityLog AL = new ActivityLog();
            AL.WriteLog("SessionId: " + SessionId, "EcheckPaymentByRenterIdResponse Mobile.ProcessPayment() started.", (short)LogPriority.LogAlways);

            var Renter = new EfxFramework.Renter(RenterId) {IsParticipating = true};

            if (!Renter.IsValid)
                return new EcheckPaymentByRenterIdResponse { Result = MobileResponseResult.GeneralFailure, Message = ServiceResponseMessages.RenterNotFound };

            EfxFramework.Renter.Set(Renter, "System Process");

            var PayerAch = new PayerAch
                {
                    BankAccountNumber = BankAccountNumber,
                    BankRoutingNumber = RoutingNumber,
                    BankAccountTypeId = 1
                };


            return ProcessPayment(this, Renter, PayerAch);
        }

        private static EcheckPaymentByRenterIdResponse ProcessPayment(EcheckPaymentByRenterIdRequest request, EfxFramework.Renter renter, PayerAch payerAch)
        {

            ActivityLog AL = new ActivityLog();
            AL.WriteLog("RenterId: " + renter.RenterId.ToString(), "EcheckPaymentByRenterIdResponse Mobile.ProcessPayment(request, renter, PayerAch) started.", 
                (short)LogPriority.LogAlways);

            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);
            var RentAchClientId = GetAchClientId(renter.RenterId);
            var Property = EfxFramework.Property.GetPropertyByRenterId(renter.RenterId);

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                return new EcheckPaymentByRenterIdResponse {Result = MobileResponseResult.PropertyNotFound, Message = ServiceResponseMessages.PropertyNotFound};

            if (renter.AcceptedPaymentTypeId == 4)
                return new EcheckPaymentByRenterIdResponse {Result = MobileResponseResult.CannotAcceptRenterPayment, Message = ServiceResponseMessages.CannotAcceptRenterPayment};

            if (renter.AcceptedPaymentTypeId == 3)
                return new EcheckPaymentByRenterIdResponse {Result = MobileResponseResult.CashEquivalentRequired, Message = ServiceResponseMessages.CashEquivalentRequired};

            var State = "";
            if (renter.StateProvinceId.HasValue)
                State = ((StateProvince)Enum.Parse(typeof(StateProvince), renter.StateProvinceId.Value.ToString(CultureInfo.InvariantCulture))).ToString();

            var RentResponse = new AchPaymentResponse();

            if (request.Amount > 0.0M)
            {
                AL.WriteLog("RenterId: " + renter.RenterId.ToString(), 
                    "EcheckPaymentByRenterIdResponse Mobile.ProcessPayment(request, renter, PayerAch) ProcessingAchPayment for " + request.Amount.ToString(),
                    (short)LogPriority.LogAlways);

                RentResponse = AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword, RentAchClientId, request.Amount, payerAch.BankRoutingNumber, payerAch.BankAccountNumber,
                    payerAch.BankAccountType.ToUpper(), renter.FirstName, renter.LastName, renter.StreetAddress, renter.Unit, renter.City, State, renter.PostalCode, renter.MainPhoneNumber);
            }

            var Response = new EcheckPaymentByRenterIdResponse
                {
                    Result = ConvertResponseResult(RentResponse.Result),
                    Message = RentResponse.ResponseDescription,
                    TransactionId = RentResponse.TransactionId
                };

            if (Response.Result == MobileResponseResult.Success)
            {
                AL.WriteLog("RenterId: " + renter.RenterId.ToString(),
                    "EcheckPaymentByRenterIdResponse Mobile.ProcessPayment(request, renter, PayerAch) Payment Success for " + request.Amount.ToString(),
                    (short)LogPriority.LogAlways);

                Lease.CurrentBalanceDue -= request.Amount;
                Lease.CurrentBalanceDueLastUpdated = DateTime.UtcNow;
                //Lease.PaymentDueDate = Lease.PaymentDueDate.HasValue ? Lease.PaymentDueDate.Value.AddMonths(1) : DateTime.UtcNow.AddMonths(1);
                Lease.Set(Lease);
                SetPayment(request, renter, payerAch, Response);

                //Salcedo - 4/14/2014 - Need to use the actual internal Id, so get the Transaction
                var TheTransaction = Transaction.GetTransactionByExternalTransactionId(Response.TransactionId);

                //Salcedo - 3/12/2014 - replaced with new html email template
                //PaymentRequest.SendEmail(Property.PropertyId.Value, "Payment Confirmation", 
                //    string.Format(EmailTemplates.OneTimeRentPayment, renter.DisplayName, "Mobile Payment", DateTime.UtcNow.ToShortDateString(), Response.Result, 
                //    Response.ConvenienceFee.ToString("C"), request.Amount.ToString("C")), renter.PrimaryEmailAddress);
                String RecipientName = renter.DisplayName;
                //cakel: BUGID000180
                String RenterProperty = Property.PropertyName;
                String PaymentAmount = "Mobile Payment";
                String TransactionDate = DateTime.UtcNow.ToShortDateString();

                //Salcedo - 4/14/2014 - changed to use the actual internal ID
                String InternalTransactionId = TheTransaction.InternalTransactionId;

                String ConvenienceAmount = Response.ConvenienceFee.ToString("C");
                String TotalAmount = request.Amount.ToString("C");
                String Body = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-payment-success.html").Replace("[[ROOT]]",
                    EmailTemplateService.EmailTemplateService.RootPath), RecipientName, PaymentAmount, TransactionDate, InternalTransactionId, ConvenienceAmount, TotalAmount);

                AL.WriteLog("RenterId: " + renter.RenterId.ToString(),
                    "EcheckPaymentByRenterIdResponse Mobile.ProcessPayment(request, renter, PayerAch) Sending payment success email to " + renter.PrimaryEmailAddress,
                    (short)LogPriority.LogAlways);

                PaymentRequest.SendEmail(Property.PropertyId.Value, "Payment Confirmation", Body, renter.PrimaryEmailAddress);

                
            }

            return Response;
        }

        private static void SetPayment(EcheckPaymentByRenterIdRequest request, EfxFramework.Renter renter, PayerAch payerAch, EcheckPaymentByRenterIdResponse response)
        {
            if (!renter.PayerId.HasValue)
                renter.PayerId = 0;

            var Amount = new PaymentAmount(request.Amount);

            var PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.ECheck, payerAch.BankAccountNumber, payerAch.BankRoutingNumber, null, null,
                null, Amount, PaymentApplication.Rent.ToString(), null, null, null, null, response.TransactionId, false, null, ((int)response.Result).ToString(CultureInfo.InvariantCulture), 
                response.Message, null, PaymentChannel.Mobile, PaymentStatus.Processing));

            Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int) response.Result, response.Message);
        }

        private static string GetAchClientId(int renterId)
        {
            var Property = EfxFramework.Property.GetPropertyByRenterId(renterId);
            return Property.RentalAccountAchClientId;
        }

        private static MobileResponseResult ConvertResponseResult(GeneralResponseResult result)
        {
            if (result == GeneralResponseResult.Success)
                return MobileResponseResult.Success;

            try
            {
                return (MobileResponseResult)((int)result);
            }
            catch
            {
                return MobileResponseResult.GeneralFailure;
            }
        }
    }
}
