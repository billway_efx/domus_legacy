﻿using EfxFramework.PaymentMethods;
using System;

namespace EfxFramework.Mobile
{
	public class LogoutRequest
	{
		public string SessionId { get; set; }

		public LogoutResponse LogoutUser()
		{
            RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId), true);

			//Logout the user with the specified SessionId
			return new LogoutResponse
			    {
				    Result = MobileResponseResult.Success, 
				    Message = ServiceResponseMessages.Successfullogout
			    };
		}
	}
}