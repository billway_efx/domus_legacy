﻿using EfxFramework.PaymentMethods;
using System;

namespace EfxFramework.Mobile
{
    public class SetRenterRequest
    {
        public string SessionId { get; set; }
        public RenterByPropertyIdRenter Renter { get; set; }

        public SetRenterResponse SetRenter()
        {
            if (!RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new SetRenterResponse {Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout};

            var PropertyStaff = new PropertyStaff(RenterSession.GetRenterSessionBySessionGuid(Guid.Parse(SessionId)).RenterId);

            if (PropertyStaff.PropertyStaffId < 1)
                return new SetRenterResponse {Result = MobileResponseResult.FailedLogin, Message = ServiceResponseMessages.Failedlogin};

            var AuthenticatedUser = String.Format("{0}-{1}{2}", PropertyStaff.PrimaryEmailAddress, PropertyStaff.FirstName, PropertyStaff.LastName);

            var EfxRenter = BuildEfxRenter();

            if (!EfxRenter.IsValid)
                return new SetRenterResponse {Result = MobileResponseResult.GeneralFailure, Message = ServiceResponseMessages.RenterNotFound};
                
            EfxFramework.Renter.Set(EfxRenter, AuthenticatedUser);

            return new SetRenterResponse {Result = MobileResponseResult.Success, Message = ServiceResponseMessages.RenterSuccessfullyUpdated};
        }

        private EfxFramework.Renter BuildEfxRenter()
        {
            var EfxRenter = new EfxFramework.Renter(Renter.RenterId)
                {
                    FirstName = Renter.FirstName, 
                    LastName = Renter.LastName, 
                    MainPhoneNumber = Renter.PhoneNumber, 
                    PrimaryEmailAddress = Renter.EmailAddress, 
                    StreetAddress = Renter.Address.StreetAddress, 
                    Unit = Renter.Address.Unit, 
                    City = Renter.Address.City, 
                    StateProvinceId = (int) ((StateProvince) Enum.Parse(typeof (StateProvince), 
                    Renter.Address.StateProvince)), 
                    PostalCode = Renter.Address.PostalCode,
                    IsActive = true
                };

            return EfxRenter;
        }
    }
}
