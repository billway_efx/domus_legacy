﻿using EfxFramework.PaymentMethods;
using System.Collections.Generic;

namespace EfxFramework.Mobile
{
    public class PayablesResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public List<EfxFramework.PayableItem> PayableItems { get; set; }
    }
}
