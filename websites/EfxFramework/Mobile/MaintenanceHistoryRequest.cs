﻿using EfxFramework.PaymentMethods;
using System;
using System.Collections.Generic;

namespace EfxFramework.Mobile
{
    public class MaintenanceHistoryRequest
    {
        public string SessionId { get; set; }
        public int RenterId { get; set; }

        public MaintenanceHistoryResponse GetMaintenanceHistory()
        {
            if (!RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new MaintenanceHistoryResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout, MaintenanceRequests = new List<EfxFramework.MaintenanceRequest>() };

            MaintenanceHistoryResponse Response;

            if (!ValidateRequest(out Response))
                return Response;

            try
            {
                Response.MaintenanceRequests = EfxFramework.MaintenanceRequest.GetAllMaintenanceRequestsByRenterId(RenterId);
            }
            catch
            {
                Response.Result = MobileResponseResult.GeneralFailure;
                Response.Message = ServiceResponseMessages.GeneralFailure;
                return Response;
            }

            Response.Result = MobileResponseResult.Success;
            Response.Message = ServiceResponseMessages.MaintenanceHistorySuccessfullyRetrieved;

            return Response;
        }

        private bool ValidateRequest(out MaintenanceHistoryResponse response)
        {
            response = new MaintenanceHistoryResponse {MaintenanceRequests = new List<EfxFramework.MaintenanceRequest>()};

            if (RenterId < 1)
            {
                response.Result = MobileResponseResult.RenterIdRequired;
                response.Message = ServiceResponseMessages.RenterIdRequired;
                return false;
            }

            var Resident = new EfxFramework.Renter(RenterId);

            if (Resident.RenterId < 1)
            {
                response.Result = MobileResponseResult.RenterNotFound;
                response.Message = ServiceResponseMessages.RenterNotFound;
                return false;
            }

            return true;
        }
    }
}
