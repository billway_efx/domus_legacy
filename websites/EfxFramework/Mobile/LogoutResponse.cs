﻿using EfxFramework.PaymentMethods;

namespace EfxFramework.Mobile
{
	public class LogoutResponse
	{
		public MobileResponseResult Result { get; set; }
		public string Message { get; set; }
	}
}