﻿using EfxFramework.PaymentMethods;

namespace EfxFramework.Mobile
{
    public class MobileServiceUrlResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public int MobileServiceUrlId { get; set; }
        public string MobileServiceUrlBase { get; set; }
        public int VersionNumber { get; set; }
    }
}
