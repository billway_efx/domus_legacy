﻿using EfxFramework.PaymentMethods;
using System.Collections.Generic;

namespace EfxFramework.Mobile
{
	public class PaymentHistoryResponse
	{
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
		public List<PaymentHistory> PaymentHistory { get; set; }
	}
}