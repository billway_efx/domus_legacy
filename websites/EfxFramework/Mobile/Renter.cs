﻿using System.Collections.Generic;
using System.Linq;

namespace EfxFramework.Mobile
{
	public class Renter
	{
        public int RenterId { get; set; }
		public string RentDueDate { get; set; }		//NOTE: Due to formatting issues, we are using a string data type so we can explicitly format the result
	}

    public class RenterByPropertyIdRenter : Renter
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsRentLate { get; set; }
        public RenterAddress Address { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public List<PaymentItem> PaymentItems { get; set; }

        public RenterByPropertyIdRenter()
        {
            
        }

        public RenterByPropertyIdRenter(int renterId)
        {
            RenterId = renterId;
            PaymentItems = BuildPaymentItems();
        }

        private List<PaymentItem> BuildPaymentItems()
        {
            var Items = new List<PaymentItem>();
            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(RenterId);

            if (Lease.LeaseId < 1)
            {
                Items.Add(new PaymentItem { Amount = 0.00M, Description = "Lease record not found, cannot apply payment." });
                return Items;
            }

            if (!Lease.CurrentBalanceDue.HasValue || Lease.CurrentBalanceDue + Lease.TotalFees < 0.01M)
            {
                Items.Add(new PaymentItem { Amount = 0.00M, Description = "There is no current balance due at this time." });
                return Items;
            }

            Items.Add(new PaymentItem { Amount = Lease.RentAmount, Description = "Rent" });
            // ReSharper disable PossibleInvalidOperationException
            Items.AddRange(from Fee in LeaseFee.GetFeesByLeaseId(Lease.LeaseId) where Fee.FeeAmount.HasValue && Fee.FeeAmount.Value > 0.00M select new PaymentItem { Amount = Fee.FeeAmount.Value, Description = Fee.FeeName });
            // ReSharper restore PossibleInvalidOperationException

            var Total = Lease.RentAmount + CalculateMonthlyFees(Lease.LeaseId);

            if (Total != Lease.CurrentBalanceDue.Value + Lease.TotalFees)
            {
                var Difference = Lease.CurrentBalanceDue.Value + Lease.TotalFees - Total;

                Items.Add(Difference > 0.00M ? new PaymentItem { Amount = Difference, Description = "Past due amount." } : new PaymentItem { Amount = Difference, Description = "Credit amount." });
            }

            return Items;
        }

        private static decimal CalculateMonthlyFees(int leaseId)
        {
            return LeaseFee.GetFeesByLeaseId(leaseId).Sum(lf => lf.FeeAmount).Value;
        }
    }

    public class RenterAddress
    {
        public string StreetAddress { get; set; }
        public string Unit { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string PostalCode { get; set; }
    }
}