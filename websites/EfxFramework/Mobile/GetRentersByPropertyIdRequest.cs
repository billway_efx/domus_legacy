﻿using EfxFramework.PaymentMethods;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EfxFramework.Mobile
{
    public class GetRentersByPropertyIdRequest
    {
        public string SessionId { get; set; }
        public string PropertyId { get; set; }
        public bool OnlyLate { get; set; }

        public GetRentersByPropertyIdResponse GetRentersByPropertyId()
        {
            var Property = new EfxFramework.Property(Int32.Parse(PropertyId));

            if (RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new GetRentersByPropertyIdResponse 
                {
                    Result = MobileResponseResult.Success, 
                    Message = ServiceResponseMessages.RentersSuccessfullyRetrieved,
                    Renters = BuildRenterList(),
                    ShowDetailPaymentItems = Property.PropertyId.HasValue && Property.PropertyId.Value > 0 && Property.ShowPaymentDetail
                };

            return new GetRentersByPropertyIdResponse {Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout};
        }

        private List<RenterByPropertyIdRenter> BuildRenterList()
        {
            // ReSharper disable RedundantAssignment
            var PropId = 0;
            // ReSharper restore RedundantAssignment
            Int32.TryParse(PropertyId, out PropId);

            var Renters = OnlyLate ? EfxFramework.Property.GetPastDueRenterListByPropertyId(PropId) : EfxFramework.Renter.GetRenterListByPropertyId(PropId);

            return Renters.Select(renter => new RenterByPropertyIdRenter(renter.RenterId)
                {
                    RenterId = renter.RenterId, 
                    FirstName = renter.FirstName, 
                    LastName = renter.LastName, 
                    RentDueDate = Lease.GetRentDueDate(renter.RenterId), 
                    IsRentLate = renter.IsRentLate,
                    EmailAddress = renter.PrimaryEmailAddress,
                    PhoneNumber = renter.MainPhoneNumber,
                    Address = new RenterAddress
                    {
                        StreetAddress = renter.StreetAddress,
                        Unit = renter.Unit,
                        City = renter.City,
                        PostalCode = renter.PostalCode,
                        StateProvince = BuildStateProvince(renter.StateProvinceId)
                    }
                }).ToList();
        }

        private static string BuildStateProvince(int? stateProvinceId)
        {
            return stateProvinceId.HasValue ? ((StateProvince)stateProvinceId.Value).ToString() : string.Empty;
        }
    }
}
