﻿using EfxFramework.PaymentMethods;
using System.Collections.Generic;

namespace EfxFramework.Mobile
{
    public class GetRentersByPropertyIdResponse
    {
        public MobileResponseResult Result { get; set; }
        public string Message { get; set; }
        public List<RenterByPropertyIdRenter> Renters { get; set; }
        public bool ShowDetailPaymentItems { get; set; }
    }
}
