﻿using EfxFramework.PaymentMethods;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

//Salcedo - 4/29/2016 - added new class for use by mobile app services

namespace EfxFramework.Mobile
{
    public class PayablesRequest
    {
        public string SessionId { get; set; }
        public int RenterId { get; set; }

        public PayablesResponse GetPayablesForRenter()
        {
            PayablesResponse Response;
            Response = new PayablesResponse();

            Response.PayableItems = new List<EfxFramework.PayableItem>();

            try
            {
                SqlDataReader dr = EfxFramework.NewDataAccess.ExecuteProcedure(EfxSettings.ConnectionString, "pay_GetPayableItemsForRenter",
                     new[] { new SqlParameter("@RenterId", RenterId) });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        PayableItem ThePayableItem = new PayableItem();

                        ThePayableItem.PayableItemId = dr.GetInt32(dr.GetOrdinal("PayableItemsId"));
                        ThePayableItem.RenterId = dr.GetInt32(dr.GetOrdinal("RenterId"));
                        ThePayableItem.LeaseId = dr.GetInt32(dr.GetOrdinal("LeaseId"));
                        ThePayableItem.PropertyId = dr.GetInt32(dr.GetOrdinal("PropertyId"));
                        ThePayableItem.PayableDueDate = dr.GetDateTime(dr.GetOrdinal("PaymentDueDate"));
                        ThePayableItem.Description = dr.GetString(dr.GetOrdinal("Pay_Description"));
                        ThePayableItem.Editable = dr.GetBoolean(dr.GetOrdinal("Editable"));
                        ThePayableItem.PayChoose = dr.GetBoolean(dr.GetOrdinal("PayChoose"));
                        ThePayableItem.AverageMonthlyRent = dr.GetDecimal(dr.GetOrdinal("AverageMonthlyRent"));
                        ThePayableItem.CurrentBalanceDue = dr.GetDecimal(dr.GetOrdinal("CurrentBalanceDue"));
                        //Salcedo - 6/14/2016 - Add a element to retrieve total fees
                        ThePayableItem.FeeTotal = dr.GetDecimal(dr.GetOrdinal("FeeTotal"));
                        //Salcedo - 7/12/20116 - Added RentAmount
                        ThePayableItem.RentAmount = dr.GetDecimal(dr.GetOrdinal("RentAmount"));
                        ThePayableItem.AllowPartialPayments = dr.GetBoolean(dr.GetOrdinal("AllowPartialPayments"));
                        ThePayableItem.PayLimit = dr.GetDecimal(dr.GetOrdinal("PayLimit"));
                        ThePayableItem.PayMessage = dr.GetString(dr.GetOrdinal("PayMessage"));
                        //cakel: 08/25/2016
                        ThePayableItem.AcceptedPaymentTypeID = dr.GetInt32(dr.GetOrdinal("AcceptedPaymentTypeID"));

                        Response.PayableItems.Add(ThePayableItem);
                    }
                    Response.Message = "Successfully retrieved all payable items.";
                    Response.Result = MobileResponseResult.Success;
                }
                else
                {
                    PayableItem ThePayableItem = new PayableItem();
                    ThePayableItem.PayableItemId = 0;
                    ThePayableItem.Description = "No payable items found.";
                    Response.PayableItems.Add(ThePayableItem);

                    Response.Message = "No payable items found.";
                    Response.Result = MobileResponseResult.NoPayableItems;
                }

                dr.Close();
            }
            catch (Exception ex)
            {
                Response.Message = "Error reading database: " + ex.Message;
                Response.Result = MobileResponseResult.GeneralFailure;
            }

            return Response;
        }
    }
}
