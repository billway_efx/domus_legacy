﻿namespace EfxFramework.Mobile
{
    public class MobileServiceUrlRequest
    {
        public int MobileVersionNumber { get; set; }

        public MobileServiceUrlResponse GetMobileServiceUrlBase()
        {
            var Url = MobileServiceUrl.GetMobileServiceUrlByVersionNumber(MobileVersionNumber);
            if (Url.MobileServiceUrlId < 1)
                return new MobileServiceUrlResponse {Result = PaymentMethods.MobileResponseResult.UrlNotFoundForSpecifiedVersion, Message = ServiceResponseMessages.UrlNotFoundForSpecifiedVersion};

            return new MobileServiceUrlResponse
                {
                    Result = PaymentMethods.MobileResponseResult.Success, 
                    Message = ServiceResponseMessages.UrlRetrievedSuccessfully,
                    MobileServiceUrlId = Url.MobileServiceUrlId,
                    MobileServiceUrlBase = Url.MobileServiceUrlText,
                    VersionNumber = Url.VersionNumber
                };
        }
    }
}
