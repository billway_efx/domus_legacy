﻿using EfxFramework.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EfxFramework
{
    public class PaymentHistory
    {
        public string TransactionId { get; set; }
        public string TransactionDateTime { get; set; }
        public string PaymentAmount { get; set; }
        public string Description { get; set; }
        public string PaymentMethod { get; set; }
        public string IsTextToPay { get; set; }
        public string IsAutoPay { get; set; }
        public string Status { get; set; }
        public string RefundUrl { get { return String.Format("~/Refund.aspx?TransactionId={0}", TransactionId); } }
        public string VoidUrl { get { return String.Format("~/Void.aspx?TransactionId={0}", TransactionId); } }

        public PaymentHistory(Payment pmt)
        {
            TransactionId = pmt.TransactionId;
            TransactionDateTime = pmt.TransactionDateTime.Date.ToString("MM/dd/yy");
            PaymentAmount = GetSumOfPayments(pmt).ToString("C");
            Description = GetDescriptionOfPayments(pmt);
            PaymentMethod = GetPaymentType(pmt);
            IsAutoPay = GetAutoPayStatus(pmt);
            Status = GetPaymentStatus(pmt);
        }

        private static decimal GetSumOfPayments(Payment pmt)
        {
            var Sum = 0.0M;

            Sum += pmt.RentAmount.HasValue ? pmt.RentAmount.Value : 0;
            Sum += pmt.CharityAmount.HasValue ? pmt.CharityAmount.Value : 0;
            Sum += pmt.OtherAmount1.HasValue ? pmt.OtherAmount1.Value : 0;
            Sum += pmt.OtherAmount2.HasValue ? pmt.OtherAmount2.Value : 0;
            Sum += pmt.OtherAmount3.HasValue ? pmt.OtherAmount3.Value : 0;
            Sum += pmt.OtherAmount4.HasValue ? pmt.OtherAmount4.Value : 0;
            Sum += pmt.OtherAmount5.HasValue ? pmt.OtherAmount5.Value : 0;

            return Sum;
        }

        public string GetDescriptionOfPayments(Payment pmt)
        {
            var DescriptionList = new List<string>();
            
            if (pmt.RentAmount.HasValue)
                DescriptionList.Add(pmt.RentAmountDescription);
            if (pmt.CharityAmount.HasValue)
                DescriptionList.Add(pmt.CharityName);
            if (pmt.OtherAmount1.HasValue)
                DescriptionList.Add(pmt.OtherDescription1);
            if (pmt.OtherAmount2.HasValue)
                DescriptionList.Add(pmt.OtherDescription2);
            if (pmt.OtherAmount3.HasValue)
                DescriptionList.Add(pmt.OtherDescription3);
            if (pmt.OtherAmount4.HasValue)
                DescriptionList.Add(pmt.OtherDescription4);
            if (pmt.OtherAmount5.HasValue)
                DescriptionList.Add(pmt.OtherDescription5);

            var Sb = new StringBuilder();
            
            foreach (var D in DescriptionList.Where(d => !String.IsNullOrEmpty(d) && !String.IsNullOrEmpty(d.Trim())))
            {
                Sb.Append(D + ", ");
            }

            var RetString = Sb.ToString();
            return RetString.TrimEnd(',', ' ');
        }

        public string GetPaymentType(Payment pmt)
        {
            if(!pmt.IsTextMessagePayment)
            return pmt.PaymentTypeId == 1 ? "Credit Card" : "E-Check";

            return "Text-To-Pay";
        }

        public string GetAutoPayStatus(Payment pmt)
        {
            var AutoPmt = AutoPayment.GetAutoPaymentByRenterId(pmt.RenterId);
            var AutoPmtDate = AutoPayment.GetAutoPaymentLogTransactionDate(AutoPmt.AutoPaymentId);

            return AutoPmtDate.Any(d => d.Date == pmt.TransactionDateTime.Date) ? "Yes" : "No";
        }

        public string GetPaymentStatus(Payment pmt)
        {
            return pmt.PaymentStatus.HasValue ? pmt.PaymentStatus.Value.ToString() : "Not Available";
        }
    }
}
