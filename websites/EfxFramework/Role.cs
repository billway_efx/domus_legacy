using System.Collections.Generic;
using Mb2x.Data;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
    public class Role : BaseEntity
    {
        //Public Properties
        [DisplayName(@"RoleId"), Required(ErrorMessage = "RoleId is required.")]
        public int RoleId { get; set; }

        [DisplayName(@"RoleName"), Required(ErrorMessage = "RoleName is required."), StringLength(100)]
        public string RoleName { get; set; }

        [DisplayName(@"RoleTypeId"), Required(ErrorMessage = "RoleTypeId is required.")]
        public int RoleTypeId { get; set; }

        [DisplayName(@"IsActive"), Required(ErrorMessage = "IsActive is required.")]
        public bool IsActive { get; set; }

        public static List<Role> GetActiveRolesByRoleTypeId(int roleTypeId)
        {
            return DataAccess.GetTypedList<Role>(EfxSettings.ConnectionString, "dbo.usp_Role_GetActiveRoleByRoleTypeId", new SqlParameter("@RoleTypeId", roleTypeId));
        }

        public static List<Role> GetRolesByRoleTypeId(int roleTypeId)
        {
            return DataAccess.GetTypedList<Role>(EfxSettings.ConnectionString, "dbo.usp_Role_GetRoleByRoleTypeId", new SqlParameter("@RoleTypeId", roleTypeId));
        }

        public static List<Role> GetRolesByStaffId(int propertyStaffId)
        {
            return DataAccess.GetTypedList<Role>(EfxSettings.ConnectionString, "dbo.usp_PropertyStaffRole_GetRoleByPropertyStaffId", new SqlParameter("@PropertyStaffId", propertyStaffId));
        }

        public static List<Role> GetRolesByEfxAdministratorId(int efxAdministratorId)
        {
            return DataAccess.GetTypedList<Role>(EfxSettings.ConnectionString, "dbo.usp_EfxAdministratorRole_GetRoleByEfxAdministratorId", new SqlParameter("@EfxAdministratorId", efxAdministratorId));
        }

        public static void DeletePropertyStaffRole(int staffId, int roleId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_PropertyStaffRole_DeleteSingleObject", new[]
                {
                    new SqlParameter("@PropertyStaffId", staffId),
                    new SqlParameter("@RoleId", roleId)
                });
        }

        public static void DeleteEfxAdministratorRole(int efxAdministratorId, int roleId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_EfxAdministratorRole_DeleteSingleObject", new[]
                {
                    new SqlParameter("@EfxAdministratorId", efxAdministratorId),
                    new SqlParameter("@RoleId", roleId)
                });
        }

        public static int Set(int roleId, string roleName, int roleTypeId, bool isActive)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_Role_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@RoleId", roleId),
	                    new SqlParameter("@RoleName", roleName),
	                    new SqlParameter("@RoleTypeId", roleTypeId),
	                    new SqlParameter("@IsActive", isActive)
                    });
        }

        public static int Set(Role r)
        {
	        return Set(
		        r.RoleId,
		        r.RoleName,
		        r.RoleTypeId,
		        r.IsActive
	        );
        }

        public Role(int roleId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_Role_GetSingleObject", new [] { new SqlParameter("@RoleId", roleId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        RoleId = (int)ResultSet[0];
		        RoleName = ResultSet[1] as string;
		        RoleTypeId = (int)ResultSet[2];
		        IsActive = (bool)ResultSet[3];
            }
        }

		public Role()
		{
			InitializeObject();
		}

	}
}
