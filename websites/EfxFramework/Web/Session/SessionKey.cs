﻿namespace EfxFramework.Web.Session
{
    public enum SessionKey
    {
        LeasePets,
        LeaseMonthlyFees,
        LeaseLeaseFees,
        FeesMonthlyFees,
        FeesApplicantFees
    }
}
