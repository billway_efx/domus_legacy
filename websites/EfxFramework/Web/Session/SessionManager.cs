﻿using System.Web;
using System.Web.SessionState;

namespace EfxFramework.Web.Session
{
    public static class SessionManager
    {
        private static HttpSessionState Session { get { return HttpContext.Current.Session; } }

        public static void AddItemToSession(SessionKey key, object value)
        {
            if (Session[key.ToString()] == null)
                Session.Add(key.ToString(), value);
            else
                Session[key.ToString()] = value;
        }

        public static void RemoveItemFromSession(SessionKey key)
        {
            if (Session[key.ToString()] != null)
                Session.Remove(key.ToString());
        }

        public static T GetSessionItem<T>(SessionKey key) where T : new()
        {
            if (Session[key.ToString()] != null)
                return (T) Session[key.ToString()];

            Session.Add(key.ToString(), new T());
            return new T();
        }
    }
}
