﻿using System;
using EfxFramework.Interfaces;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.Web.Facade;

namespace EfxFramework.Web
{
    public class BasePageV2 : Page, IBasePageV2
    {
        public string ValidationSummaryOpenText { get { return "<div id='ValidationSummaryModal' class='hide modalContentContainer'>"; } }
        public string ValidationSummaryHeaderText { get { return "<h3 class='fr1Color' >Address the following errors</h3>"; } }
        public string ValidationSummaryCloseText { get { return "</div>"; } }

        UIFacade _Facade;
        public UIFacade Facade
        {
            get
            {
                if (_Facade == null)
                {
                    _Facade = new UIFacade();
                }

                return _Facade;
            }
        }

        public void SafeRedirect(string redirectTo)
        {
            SafePageRedirect(redirectTo);
        }

        public static void SafePageRedirect(string redirectTo)
        {
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            HttpContext.Current.Response.Redirect(redirectTo, false);
        }

        public static void DisplaySystemMessage(Page page, string message, SystemMessageType messageType)
        {
            var SystemMessageControl = new Control();
            var SystemMessageOpen = new Literal();
            var SystemMessageIcon = new Image();
            var SystemMessageMessage = new Literal();
            var SystemMessageClose = new Literal();

            SystemMessageOpen.Text = String.Format("<div id='SystemMessage' class='hide modalContentContainer'>");
            SystemMessageIcon.ImageUrl = GetNotificationIcon(messageType);
            SystemMessageIcon.AlternateText = String.Format("Notification Icon");
            SystemMessageMessage.Text = String.Format("<span>{0}</span>", message);
            SystemMessageClose.Text = String.Format("</div>");
            SystemMessageControl.Controls.Add(SystemMessageOpen);
            //SystemMessageControl.Controls.Add(SystemMessageIcon);
            SystemMessageControl.Controls.Add(SystemMessageMessage);
            SystemMessageControl.Controls.Add(SystemMessageClose);
            page.Form.Controls.Add(SystemMessageControl);

            ScriptManager.RegisterStartupScript(page, page.GetType(), "test", "<script type='text/javascript'>ShowModal(\"#SystemMessage\");</script>", false);
        }

        public void DisplaySystemMessage(string message, SystemMessageType messageType)
        {
            DisplaySystemMessage(this, message, messageType);
        }

        public void AddValidationSummaryToPage()
        {
            var ValidationControl = new Control();
            var ValidationSummaryOpen = new Literal();
            var ValidationSummaryBody = new ValidationSummary();
            var ValidationSummaryClose = new Literal();

            ValidationSummaryOpen.Text = ValidationSummaryOpenText;
            ValidationSummaryBody.ID = "ValidationSummary";
            ValidationSummaryBody.DisplayMode = ValidationSummaryDisplayMode.BulletList;
            ValidationSummaryBody.ShowSummary = true;
            ValidationSummaryBody.HeaderText = ValidationSummaryHeaderText;
            ValidationSummaryClose.Text = ValidationSummaryCloseText;

            ValidationControl.Controls.Add(ValidationSummaryOpen);
            ValidationControl.Controls.Add(ValidationSummaryBody);
            ValidationControl.Controls.Add(ValidationSummaryClose);
            Page.Form.Controls.Add(ValidationControl);
        }

        public static bool ValidatePage(Page page)
        {
            page.Validate();

            if (!page.IsValid)
                ScriptManager.RegisterStartupScript(page, page.GetType(), "test", "<script type='text/javascript'>ShowModal(\"#ValidationSummaryModal\");</script>", false);

            return page.IsValid;
        }

        public bool ValidatePage()
        {
            return ValidatePage(Page);
        }

        private static string GetNotificationIcon(SystemMessageType messageType)
        {
            //This will get populated later.
            switch (messageType)
            {
                case SystemMessageType.Error:
                    return "";

                case SystemMessageType.Information:
                    return "";

                case SystemMessageType.Success:
                    return "";

                case SystemMessageType.NotSet:
                    return "";

                default:
                    return "";
            }
        }
    }
}
