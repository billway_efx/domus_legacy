﻿namespace EfxFramework.Web.Caching
{
    public enum CacheKey
    {
        PropertyCache,
        ResidentCache,
        StateProvinceCache,
        AnnouncementCache,
        MonthlyFeeCache,
        PetCache,
        MaintenanceRequestCache,
        ApplicationFeeCache,
        LeadCache,
        PropertyLeadCache,
        ApplicantCache,
        JobPostingCache,
        NewsResults,
        WeatherResults
    }
}
