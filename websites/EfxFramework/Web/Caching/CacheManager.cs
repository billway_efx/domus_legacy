﻿using System;
using System.Web;
using System.Web.Caching;

namespace EfxFramework.Web.Caching
{
    public static class CacheManager
    {
        private static Cache Cache { get { return HttpContext.Current.Cache; } }

        public static void AddItemToCache(CacheKey key, object value)
        {
            if (Cache[key.ToString()] == null)
                Cache.Add(key.ToString(), value, null, DateTime.Now.AddMinutes(30), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);  //TODO: Settings for this
            else
                Cache[key.ToString()] = value;
        }

        public static void AddItemToCache(CacheKey key, object value, int expirationInMinutes)
        {
            if (Cache[key.ToString()] == null)
                Cache.Add(key.ToString(), value, null, DateTime.Now.AddMinutes(expirationInMinutes), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);  //TODO: Settings for this
            else
                Cache[key.ToString()] = value;
        }

        public static void RemoveItemFromCache(CacheKey key)
        {
            if (Cache[key.ToString()] != null)
                Cache.Remove(key.ToString());
        }

        public static T GetCacheItem<T>(CacheKey key) where T : new()
        {
            if (Cache[key.ToString()] != null)
                return (T)Cache[key.ToString()];

            return new T();
        }
    }
}
