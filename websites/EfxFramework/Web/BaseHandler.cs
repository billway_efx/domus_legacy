﻿using System.Collections.Generic;
using System.Linq;
using EfxFramework.PublicApi;
using EfxFramework.Resources;
using EfxFramework.Web.Session;
using Mb2x.ExtensionMethods;
using System;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;

namespace EfxFramework.Web
{
    public enum EfxRequestType
    {
        GetPhoto = 0,
        PostPhoto = 1,
        GetInfo = 2,
        GetStaff = 3,
        LookUpRenterById = 4,
        GetArticle = 5,
        GetConvenienceFee = 6,
        GetRentersListByPropertyId = 7,
        GeneratePhotoPreview = 8,
        DeleteLeaseFee = 9,
        DeletePet = 10,
        DeleteApplicationFee = 11,
        DeleteMonthlyFee = 12,
        DeleteJobPosting = 13,
        DeleteAnnouncement = 14,
        DeleteRenterNote = 15,
        EmailApplicant = 16,
        DeletePartnerCompany = 17,
        DeleteApiUser = 18,
        Unknown = 999
    }

    public class BaseHandler<T> : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        byte[] DefaultPropertyImage
        {
            get 
            {
                return Resources.Icons.default_icons_house.ToByteArray();
            }
        }

        byte[] DefaultCustomerImage
        {
            get
            {
                return Resources.Icons.default_icons_person.ToByteArray();
            }
        }

        public bool IsValidRequest
        {
            get
            {
                return CurrentRequestType != EfxRequestType.Unknown;
            }
        }

        public EfxRequestType CurrentRequestType
        {
            get
            {
                //Get the value from the query string
                var Value = HttpContext.Current.Request.QueryString["RequestType"].ToNullInt32();

                //If we don't get a value, throw an exception
                if (!Value.HasValue)
                {
                    throw new InvalidOperationException("RequestType is missing or invalid.");
                }

                return (EfxRequestType)Value.Value;
            }
        }

        public T CurrentRequestData
        {
            get
            {
                T RetObj = default(T);

                //Get the value from the query string
                var Value = HttpContext.Current.Request.QueryString["RequestData"];

                // If we have something then try to deserialize
                if (!string.IsNullOrEmpty(Value))
                {
                    // Url Decode
                    Value = HttpUtility.UrlDecode(Value);

                    // Trim
                    Value = Value.TrimStart(new[] { '\"' });
                    Value = Value.TrimEnd(new[] { '\"' });

                    // Deserialize and return
                    var Json = new JavaScriptSerializer();
                    RetObj = Json.Deserialize<T>(Value);
                }

                return RetObj;
            }
        }

        public bool IsReusable
        {
            get { return false; }
        }

        public virtual void ProcessRequest(HttpContext context)
        {
            // Validate request before anything.
            if (!IsValidRequest) throw new Exception("Invalid request to handler.");

            if (CurrentRequestType == EfxRequestType.GetRentersListByPropertyId)
            {
                string ResponseJson = "{ }";

                var PropertyIds = context.Request.Params["propertyIds"];

                if (!string.IsNullOrEmpty(PropertyIds))
                {
                    var Renters = new Facade.UIFacade().GetRentersAutoCompleteDataSource(PropertyIds);
                    if (Renters != null)
                    {
                        var Cereal = new JavaScriptSerializer();
                        ResponseJson = Cereal.Serialize(Renters);
                    }
                }

                context.Response.ContentType = "application/json";
                context.Response.Write(ResponseJson);
            }
            else if (CurrentRequestType == EfxRequestType.GetConvenienceFee)
            {
                var ProgramId = context.Request.Params["program"];
                var AverageRent = context.Request.Params["averageRent"];

                var Program = (Property.Program)Enum.Parse(typeof(Property.Program), ProgramId);
                decimal AvgRent;
                Decimal.TryParse(AverageRent, out AvgRent);
                var Cf = Property.GetConvenienceFeesByProgram(Program, AvgRent);

                context.Response.Write(string.Format("{0:C}", (Cf.ECheckFee + Cf.CreditCardFee + Cf.AppDepositFee) / 3));
            }
            else if (CurrentRequestType == EfxRequestType.GetArticle)
            {
                var ArticleCode = context.Request.Params["articleCode"];
                var Facade = new Facade.UIFacade();
                var Response = Facade.HtmlTemplateContent[ArticleCode];

                context.Response.Write(Response);
            }
            else if (CurrentRequestType == EfxRequestType.LookUpRenterById)
            {
                string ResponseJson = "{ }";

                var RenterId = context.Request.Params["renterId"].ToNullInt32();

                if (RenterId.HasValue)
                {
                    var Renter = new Renter(RenterId.Value);
                    if (Renter != null && Renter.RenterId > 0)
                    {
                        var Cereal = new JavaScriptSerializer();
                        var LeaseRenter = new LeaseRenter { RenterObject = Renter };

	                    var CurrentLease = Lease.GetRenterLeaseByRenterId(Renter.RenterId);
                        if (CurrentLease != null && CurrentLease.LeaseId > 0)
                            LeaseRenter.LeaseObject = CurrentLease;

                        ResponseJson = Cereal.Serialize(LeaseRenter);
                    }
                }

                context.Response.ContentType = "application/json";
                context.Response.Write(ResponseJson);
            }
            else if (CurrentRequestType == EfxRequestType.GetStaff)
            {
                string ResponseJson = "{ }";

                // Check for property id
                var CurrentPropertyId = context.Request.Params["propertyId"].ToNullInt32();
                if (CurrentPropertyId.HasValue)
                {
                    var CurrentPropertyStaff = PropertyStaff.GetPropertyStaffListByPropertyId(CurrentPropertyId.Value);
                    var Cereal = new JavaScriptSerializer();
                    ResponseJson = Cereal.Serialize(CurrentPropertyStaff);
                }

                context.Response.ContentType = "application/json";
                context.Response.Write(ResponseJson);
            }
            else if (CurrentRequestType == EfxRequestType.GetInfo)
            {
                string ResponseJson = "{ }";

                // Check for property id
                var PropertyId = context.Request.Params["propertyId"].ToNullInt32();
                if (PropertyId.HasValue)
                {
                    var CurrentProperty = new Property(PropertyId);
                    var Cereal = new JavaScriptSerializer();
                    ResponseJson = Cereal.Serialize(CurrentProperty);
                }

                // Check for property staff id
                var PropertyStaffId = context.Request.Params["propertyStaffId"].ToNullInt32();
                if (PropertyStaffId.HasValue)
                {
                    var CurrentPropertyStaff = new PropertyStaff(PropertyStaffId);
                    var Cereal = new JavaScriptSerializer();
                    ResponseJson = Cereal.Serialize(CurrentPropertyStaff);
                }

                context.Response.ContentType = "application/json";
                context.Response.Write(ResponseJson);
            }
            else if (CurrentRequestType == EfxRequestType.PostPhoto)
            {
                if (context.Request.Files != null && context.Request.Files.Count > 0)
                {
                    var File = context.Request.Files[0];

	                byte[] Photo;
	                using (var BinaryReader = new BinaryReader(File.InputStream))
                    {
                        Photo = BinaryReader.ReadBytes(File.ContentLength);
                    }

                    // TODO: Upload files

                    if (Photo != null && Photo.Length > 0)
                    {
                        context.Response.ContentType = File.ContentType;
                        context.Response.BufferOutput = false;
                        context.Response.OutputStream.Write(Photo, 0, Photo.Length);
                    }
                }
            }
            else if (CurrentRequestType == EfxRequestType.GetPhoto)
            {
                byte[] Photo = null;
                var UseDefaultPropertyIcon = false;

                // Check for renter photo
                var RenterId = context.Request.Params["renterId"].ToNullInt32();
                if (RenterId.HasValue)
                    Photo = Renter.GetPhoto(RenterId.Value);

                // Check for property staff photo
                var PropertyStaffId = context.Request.Params["propertyStaffId"].ToNullInt32();
                if (PropertyStaffId.HasValue)
                    Photo = PropertyStaff.GetPhoto(PropertyStaffId.Value);

                // Check for property photo
                var PropertyId = context.Request.Params["propertyId"].ToNullInt32();
                var PhotoIndex = context.Request.Params["photoIndex"].ToNullInt32();
                if (PropertyId.HasValue)
                {
	                switch (PhotoIndex)
                    {
		                case 0:
		                case 1:
			                Photo = Property.GetPhoto1(PropertyId.Value);
			                break;
		                case 2:
			                Photo = Property.GetPhoto2(PropertyId.Value);
			                break;
		                case 3:
			                Photo = Property.GetPhoto3(PropertyId.Value);
			                break;
		                case 4:
			                Photo = Property.GetPhoto4(PropertyId.Value);
			                break;
						default:
							UseDefaultPropertyIcon = true;
							break;
	                }
                }

                // Check for company logo
                var CompanyId = context.Request.Params["companyId"].ToNullInt32();
                if (CompanyId.HasValue)
                {
                    Photo = Company.GetLogo(CompanyId.Value);
                    UseDefaultPropertyIcon = true;
                }

                // Use defaults if not set
                if (Photo == null)
                    Photo = UseDefaultPropertyIcon ? DefaultPropertyImage : DefaultCustomerImage;
                else
                {
                    try
                    {
                        var Img = Image.FromStream(new MemoryStream(Photo));
                    }
                    catch
                    {
                        Photo = UseDefaultPropertyIcon ? DefaultPropertyImage : DefaultCustomerImage;
                    }
                }

                if (Photo != null && Photo.Length > 0)
                {
                    context.Response.ContentType = "image/png";
                    context.Response.BufferOutput = true;
	                try
	                {
		                context.Response.OutputStream.Write(Photo, 0, Photo.Length);
	                }
	                catch (HttpException Ex)
	                {
		                switch ((uint)Ex.ErrorCode)
		                {
							case 0x80070040:	//The specified network name is no longer available.
								break;
							case 0x800703E3:	//The I/O operation has been aborted because of either a thread exit or an application request.
								break;
							case 0x800704CD:	//An operation was attempted on a non-existent network connection.
								break;
							case 0x80070057:	//The parameter is incorrect.
								break;
							default:			//This is an unknown exception, re-throw
								throw;
		                }
	                }
                }
            }
            else if (CurrentRequestType == EfxRequestType.GeneratePhotoPreview)
            {
                var PhotoNumber = context.Request.Params["photoNumber"].ToNullInt32();

                if (PhotoNumber.HasValue)
                {
                    if (context.Session[SessionVariable.ImageBytes.ToString()] != null)
                    {
                        var Photos = (List<Photo>)(context.Session[SessionVariable.ImageBytes.ToString()]);

                        if (Photos.Count > PhotoNumber)
                        {
                            context.Response.ContentType = "image/JPEG";
                            context.Response.BinaryWrite(Photos[PhotoNumber.Value].PhotoBytes.StandardizeImage(100));
                        }
                    }
                }
            }
            else if (CurrentRequestType == EfxRequestType.DeleteLeaseFee)
            {
                var FeeId = context.Request.Params["MonthlyFeeId"].ToNullInt32();
                var LeaseId = context.Request.Params["LeaseId"].ToNullInt32();
                var RenterId = context.Request.Params["RenterId"].ToNullInt32();
                var LeaseFees = SessionManager.GetSessionItem<List<LeaseFee>>(SessionKey.LeaseLeaseFees);

                LeaseFees.Remove(LeaseFees.FirstOrDefault(lf => lf.MonthlyFeeId == FeeId.Value && lf.LeaseId == LeaseId.Value && lf.RenterId == RenterId.Value));
                SessionManager.AddItemToSession(SessionKey.LeaseLeaseFees, LeaseFees);

                if (!FeeId.HasValue || !LeaseId.HasValue || !RenterId.HasValue || FeeId.Value < 1 || LeaseId.Value < 1 || RenterId.Value < 1)
                {
                    BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
                    return;
                }

                LeaseFee.DeleteLeaseFee(FeeId.Value, LeaseId.Value, RenterId.Value);
                BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
            }
            else if (CurrentRequestType == EfxRequestType.DeletePet)
            {
                var PetId = context.Request.Params["PetId"].ToNullInt32();
                var Pets = SessionManager.GetSessionItem<List<Pet>>(SessionKey.LeasePets);

                Pets.Remove(Pets.FirstOrDefault(p => p.PetId == PetId.Value));
                SessionManager.AddItemToSession(SessionKey.LeasePets, Pets);

                if (!PetId.HasValue || PetId.Value < 1)
                {
                    BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
                    return;
                }

                Pet.DeletePet(PetId.Value);
                BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
            }
            else if (CurrentRequestType == EfxRequestType.DeleteApplicationFee)
            {
                var ApplicationFeeId = context.Request.Params["ApplicationFeeId"].ToNullInt32();
                var Fees = SessionManager.GetSessionItem<List<ApplicationFee>>(SessionKey.FeesApplicantFees);

                Fees.Remove(Fees.FirstOrDefault(p => p.ApplicationFeeId == ApplicationFeeId.Value));
                SessionManager.AddItemToSession(SessionKey.FeesApplicantFees, Fees);

                if (!ApplicationFeeId.HasValue || ApplicationFeeId.Value < 1)
                {
                    BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
                    return;
                }

                ApplicationFee.DeleteApplicationFee(ApplicationFeeId.Value);
                BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
            }
            else if (CurrentRequestType == EfxRequestType.DeleteMonthlyFee)
            {
                var MonthlyFeeId = context.Request.Params["MonthlyFeeId"].ToNullInt32();
                var Fees = SessionManager.GetSessionItem<List<MonthlyFee>>(SessionKey.FeesMonthlyFees);

                Fees.Remove(Fees.FirstOrDefault(p => p.MonthlyFeeId == MonthlyFeeId.Value));
                SessionManager.AddItemToSession(SessionKey.FeesMonthlyFees, Fees);

                if (!MonthlyFeeId.HasValue || MonthlyFeeId.Value < 1)
                {
                    BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
                    return;
                }
                try
                {
                    MonthlyFee.DeleteMonthlyFee(MonthlyFeeId.Value);
                }
                catch
                {
                    BasePage.SafePageRedirect("~/ErrorPage.html");
                    return;
                }
                BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
            }
            else if (CurrentRequestType == EfxRequestType.DeleteRenterNote)
            {
                var NoteId = context.Request.Params["RenterNoteId"].ToNullInt32();

                if (!NoteId.HasValue || NoteId.Value < 1)
                {
                    BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
                    return;
                }

                RenterNote.DeleteNote(NoteId.Value);
                BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
            }
            else if (CurrentRequestType == EfxRequestType.EmailApplicant)
            {
                var ApplicantId = context.Request.Params["ApplicantId"].ToNullInt32();
                var PropertyId = context.Request.Params["PropertyId"].ToNullInt32();
                var StatusId = context.Request.Params["StatusId"].ToNullInt32();

                if (!ApplicantId.HasValue || ApplicantId.Value < 1)
                {
                    BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
                    return;
                }

                var Applicant = new Applicant(ApplicantId.Value);
                if (Applicant.ApplicantId < 1)
                {
                    BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
                    return;
                }

                if (!StatusId.HasValue || StatusId.Value < 1)
                {
                    BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
                    return;
                }

                var Status = ((ApplicationStatus) StatusId).ToString();

                if (((ApplicationStatus) StatusId) == ApplicationStatus.Pending)
                    Status = String.Format("{0}. We apologize the application process is taking longer than expected.", Status);

                //Salcedo - 2/28/2014 - changed to send to function that includes all property management email contacts
                //BulkMail.BulkMail.SendMailMessage(PropertyId, null, false, Settings.SendMailUsername, Settings.SendMailPassword, new System.Net.Mail.MailAddress(Settings.SupportEmail),
                //    "Application Status", String.Format(EmailTemplates.FutureResidentStatusEmail, Applicant.DisplayName, Status), 
                //    new List<System.Net.Mail.MailAddress>{new System.Net.Mail.MailAddress(Applicant.PrimaryEmailAddress)});
                int IntPropertyId = PropertyId.HasValue ? PropertyId.Value : 0;
                
                // Salcedo - 10/30/2014 - Task # 0000293
                //I can't find any legitimate reason that we would ever send this email
                // I don't think this code is actually used anymore, so I've disabled it, as it appears to be the source of hundreds of duplicate applicant status emails

                //BulkMail.BulkMail.SendMailMessageIncludePropertyEmailContacts(IntPropertyId, null, Settings.SendMailUsername, Settings.SendMailPassword, new System.Net.Mail.MailAddress(Settings.SupportEmail),
                //    "Application Status", String.Format(EmailTemplates.FutureResidentStatusEmail, Applicant.DisplayName, Status),
                //    new List<System.Net.Mail.MailAddress> { new System.Net.Mail.MailAddress(Applicant.PrimaryEmailAddress) });

                BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
            }
            else if (CurrentRequestType == EfxRequestType.DeletePartnerCompany)
            {
                var PartnerCompanyId = context.Request.Params["PartnerCompanyId"].ToNullInt32();

                if (!PartnerCompanyId.HasValue || PartnerCompanyId.Value < 1)
                {
                    BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
                    return;
                }

                PartnerCompany.DeletePartner(PartnerCompanyId.Value);
                BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
            }
            else if (CurrentRequestType == EfxRequestType.DeleteJobPosting)
            {
                var JobPostingId = context.Request.Params["JobPostingId"].ToNullInt32();

                if (!JobPostingId.HasValue || JobPostingId.Value < 1)
                {
                    BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
                    return;
                }

                JobPosting.DeleteJobPosting(JobPostingId.Value);
                BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
            }
            else if (CurrentRequestType == EfxRequestType.DeleteApiUser)
            {
                var ApiUserId = context.Request.Params["ApiUserId"].ToNullInt32();

                if (!ApiUserId.HasValue || ApiUserId.Value < 1)
                {
                    BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
                    return;
                }

                ApiUser.DeleteApiUser(ApiUserId.Value);
                BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
            }
            else if (CurrentRequestType == EfxRequestType.DeleteAnnouncement)
            {
                var AnnouncementId = context.Request.Params["AnnouncementId"].ToNullInt32();

                if (!AnnouncementId.HasValue || AnnouncementId.Value < 1)
                {
                    BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
                    return;
                }

                Announcement.DeleteAnnouncement(AnnouncementId.Value);
                BasePage.SafePageRedirect(context.Request.UrlReferrer.AbsoluteUri);
            }
        }
    }
}
