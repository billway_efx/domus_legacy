﻿using EfxFramework.Resources;
using EfxFramework.Security.Authentication;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace EfxFramework.Web.Facade
{
    // ReSharper disable InconsistentNaming
    public class UIFacade
    // ReSharper restore InconsistentNaming
    {
        private readonly PropertyStaff _CurrentStaff = null;
        public PropertyStaff CurrentStaff { get { return _CurrentStaff ?? GetCurrentUser(UserType.PropertyStaff) as PropertyStaff; } }

        /// <summary>
        /// Gets the current renter.
        /// </summary>
        readonly Renter _Renter = null;
        public Renter CurrentRenter { get { return _Renter ?? (AuthenticationTools.IsCurrentlyLoggedIn() ? GetCurrentUser(UserType.Renter) as Renter : null); } }

        Property _RenterProperty;
        public Property CurrentRenterProperty
        {
            get
            {
                if (_RenterProperty == null && CurrentRenter != null)
                {
                    var Id = Renter.GetPropertyByRenterId(CurrentRenter.RenterId);
                    if (Id.HasValue)
                        _RenterProperty = new Property(Id.Value);         
                }

                return _RenterProperty;
            }
        }

        Lease _Lease;
        public Lease CurrentRenterLease
        {
            get
            {
                if (_Lease == null && CurrentRenter != null)
                    _Lease = Lease.GetRenterLeaseByRenterId(CurrentRenter.RenterId);
                
                return _Lease;
            }
        }
       
        /// <summary>
        /// Gets the current renter.
        /// </summary>
        EfxAdministrator _Efxuser;
        public EfxAdministrator CurrentEfxUser
        {
            get
            {
                if (_Efxuser == null)
                {
                    if (AuthenticationTools.IsCurrentlyLoggedIn())
                        _Efxuser = GetCurrentUser(UserType.EfxAdministrator) as EfxAdministrator;
                }

                return _Efxuser;
            }
        }

        /// <summary>
        /// Autocomplete data source for properties 
        /// </summary>
        public string GetRentersAutoCompleteDataSource(string propertyIds)
        {
            var PropNamesArray = propertyIds.Split(',');
            
            // Get props
            var Props = from Pr in Property.GetAllPropertyList()
                        join PropName in PropNamesArray
                        on Pr.PropertyName.ToLower() equals PropName.ToLower()
                        select Pr;
           
            // Get renters
            var Renters = new List<Renter>();
            Props.ForEach(rpr => { if (rpr.PropertyId != null) Renters.AddRange(Renter.GetRenterListByPropertyId(rpr.PropertyId.Value)); });

            // Clean up
            var RetRenters = Renters.GroupBy(re => re.RenterId).Select(ren => ren.First()).ToArray();

            // Return string
            var S = new StringBuilder();
            RetRenters.ForEach(p => S.AppendFormat("\"{0}\",", p.RenterId));
            return S.ToString().TrimEnd(new[] { ',' });
        }

        /// <summary>
        /// Exposes out HTML for UI layer stored in resource files.
        /// </summary>
        /// <returns></returns>
        public HybridDictionary HtmlTemplateContent
        {
            get
            {
                var Output = new HybridDictionary
                                 {
                                     {"RentersRoom", HtmlTemplates.RentersRoom}, 
                                     {"RenterRoomInterview", HtmlTemplates.RenterRoomInterview}, 
                                     {"RentersRoomCarRental", HtmlTemplates.RentersRoomCarRental}, 
                                     {"RentersRoomSavingTips", HtmlTemplates.RentersRoomSavingTips}, 
                                     {"ProsCorner", HtmlTemplates.ProsCorner}, 
                                     {"ProsCornerAllStar", HtmlTemplates.ProsCornerAllStar}, 
                                     {"ProsCornerGreen", HtmlTemplates.ProsCornerGreen}, 
                                     {"ProsCornerRecycling", HtmlTemplates.ProsCornerRecycling}, 
                                     {"ProsCornerStayingUpdated", HtmlTemplates.ProsCornerStayingUpdated}, 
                                     {"PrivacyPolicy", HtmlTemplates.PrivacyPolicy}, 
                                     {"TermsAndConditions", HtmlTemplates.TermsAndConditions}
                                 };

                return Output;
            }
        }

        public static BaseUser GetCurrentUser(UserType userType)
        {
            switch (userType)
            {
                case UserType.EfxAdministrator:
                    return GetAuthenticatedAdministrator();

                case UserType.PropertyStaff:
                    return GetAuthenticatedPropertyStaff();

                case UserType.Renter:
                    return GetAuthenticatedRenter();

                case UserType.Applicant:
                    return GetAuthenticatedApplicant();
            }

            return null;
        }

        public static string GetAuthenticatedUserString(UserType userType)
        {
            switch (userType)
            {
                case UserType.EfxAdministrator:
                    var Administrator = GetAuthenticatedAdministrator();
                    return Administrator != null ? String.Format("{0} - {1} {2} (Admin)", Administrator.PrimaryEmailAddress, Administrator.FirstName, Administrator.LastName) : string.Empty;

                case UserType.PropertyStaff:
                    var Staff = GetAuthenticatedPropertyStaff();
                    //Salcedo - task 00076 - when testing as an administrator, it was discovered that the user name was not resolving because some clients of this code
                    //were passing UserType.PropertyStaff even when the user is an admin
                    if (Staff.UserName == null)
                    {
                        var AdministratorTest = GetAuthenticatedAdministrator();
                        if (AdministratorTest.UserName != null)
                        {
                            return AdministratorTest != null ? String.Format("{0} - {1} {2} (Admin)", AdministratorTest.PrimaryEmailAddress, AdministratorTest.FirstName, AdministratorTest.LastName) : string.Empty;
                        }
                        else
                            return "Unknown";
                    }
                    else
                    {
                        return Staff != null ? String.Format("{0} - {1} {2} (Staff)", Staff.PrimaryEmailAddress, Staff.FirstName, Staff.LastName) : string.Empty;
                    }
                case UserType.Renter:
                    var Renter = GetAuthenticatedRenter();
                    return Renter != null ? String.Format("{0} - {1} {2}", Renter.PrimaryEmailAddress, Renter.FirstName, Renter.LastName) : string.Empty;
            }

            return string.Empty;
        }

        private static EfxAdministrator GetAuthenticatedAdministrator()
        {
            // ReSharper disable RedundantAssignment
            var Id = 0;
            // ReSharper restore RedundantAssignment
            return Int32.TryParse(HttpContext.Current.User.Identity.Name, out Id) ? new EfxAdministrator(Id) : null;
        }

        private static PropertyStaff GetAuthenticatedPropertyStaff()
        {
            // ReSharper disable RedundantAssignment
            var Id = 0;
            // ReSharper restore RedundantAssignment
            return Int32.TryParse(HttpContext.Current.User.Identity.Name, out Id) ? new PropertyStaff(Id) : null;
        }

        private static Renter GetAuthenticatedRenter()
        {
            // ReSharper disable RedundantAssignment
            var Id = 0;
            // ReSharper restore RedundantAssignment
            return Int32.TryParse(HttpContext.Current.User.Identity.Name, out Id) ? new Renter(Id) : null;
        }

        private static Applicant GetAuthenticatedApplicant()
        {
            // ReSharper disable RedundantAssignment
            var Id = 0;
            // ReSharper restore RedundantAssignment
            return Int32.TryParse(HttpContext.Current.User.Identity.Name, out Id) ? new Applicant(Id) : null;
        }


        #region Email Engine

        public void SendEmail(int? propertyId, string disseminationGroup, bool isBulkMail, string to, MailAddress from, string subject, string body)
        {
            SendEmail(propertyId, disseminationGroup, isBulkMail, new List<MailAddress> { new MailAddress(to) }, from, subject, body);
        }

        public void SendEmail(int? propertyId, string disseminationGroup, bool isBulkMail, List<MailAddress> recipients, MailAddress from, string subject, string body)
        {
            //Salcedo - 2/28/2014 - changed to send to function that includes all property management email contacts
            //BulkMail.BulkMail.SendMailMessage(propertyId, disseminationGroup, isBulkMail, Settings.SendMailUsername, Settings.SendMailPassword, from, subject, body, recipients);
            int IntPropertyId = propertyId.HasValue ? propertyId.Value : 0;
            BulkMail.BulkMail.SendMailMessageIncludePropertyEmailContacts(IntPropertyId, disseminationGroup, EfxSettings.SendMailUsername, EfxSettings.SendMailPassword, from, subject, body, recipients);
        }

        public void SendFeedbackEmail(int? propertyId, string disseminationGroup, bool isBulkMail, string email, string message)
        {
            SendEmail(propertyId, disseminationGroup, isBulkMail, EfxSettings.FeedBackEmail, new MailAddress(email), "RentPaidOnline User Feedback", message);
        }

        #endregion
    }
}
