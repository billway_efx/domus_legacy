﻿using EfxFramework.Interfaces;
using EfxFramework.Web.Facade;
using EfxFramework.Web.WebControls;
using System.Web;
using System.Web.UI;

namespace EfxFramework.Web
{
    public class BasePage : Page, IBasePage
    {
        public SystemMessageControl PageMessage
        {
            get
            {
                if (Master != null) 
                    return ((Master).FindControl("MySystemMessageControl") as SystemMessageControl);

                return new SystemMessageControl();
            }
        }

        UIFacade _Facade;
        public UIFacade Facade
        {
            get
            {
                if (_Facade == null)
                {
                    _Facade = new UIFacade();
                }

                return _Facade;
            }
        }

        public static SystemMessageControl GetPageMessage(Page page)
        {
            if (page.Master != null)
                return ((page.Master).FindControl("MySystemMessageControl") as SystemMessageControl);

            return new SystemMessageControl();
        }

        public void SafeRedirect(string redirectTo)
        {
            SafePageRedirect(redirectTo);
        }

        public static void SafePageRedirect(string redirectTo)
        {
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            HttpContext.Current.Response.Redirect(redirectTo, false);
        }

		public BasePage()
		{
			//If this is a mobile device, redirect to the MobileRedirectUrl
			if (HttpContext.Current.Request.Browser.IsMobileDevice)
			{
                // Salcedo - per Nick, removed redirect for mobile devices
                //SafeRedirect(Settings.MobileRedirectUrl);
			}
		}

        public BasePage(bool overrideMobileRedirect)
        {
            if (HttpContext.Current.Request.Browser.IsMobileDevice && !overrideMobileRedirect)
            {
                SafeRedirect(EfxSettings.MobileRedirectUrl);
            }
        }
    }
}