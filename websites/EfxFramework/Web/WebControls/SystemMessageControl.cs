﻿using System;
using System.Web;
using System.Web.UI.WebControls;

namespace EfxFramework.Web.WebControls
{
    public sealed class SystemMessageControl : WebControl
    {
        SystemMessageType _MessageType = SystemMessageType.NotSet;
        private SystemMessageType CurrentMessageType
        {
            get
            {
                return _MessageType;
            }
            set
            {
                _MessageType = value;
            }
        }

        private string CurrentMessage
        {
            get;
            set;
        }

        public void DisplayMessage(SystemMessageType messageType, string message)
        {
            CurrentMessageType = messageType;
            CurrentMessage = message;
        }
        
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            // HttpContext is required.
            if (HttpContext.Current == null)
                throw new Exception("Invalid HTTP Context.");

            const string id = "MainSystemMessage";

            // Message Wrap
            writer.Write("<div id='{1}' class='SystemMessageWrap SystemMessage-{0}'>", CurrentMessageType, id);
            writer.Write("<span class='SystemMessageBody'>{0}</span>", CurrentMessage);
            writer.Write("<a href='#' class='SystemMessageHideMe' onclick='return EFXClient.HideMainSystemMessage();'>Hide Message</a></div>");
            base.Render(writer);
        }
    }
}
