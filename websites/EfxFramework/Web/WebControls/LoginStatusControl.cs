﻿using EfxFramework.Web.Facade;
using System;
using System.Web;
using System.Web.UI.WebControls;

namespace EfxFramework.Web.WebControls
{
    public sealed class LoginStatusControl : WebControl
    {
        /// <summary>
        /// Link to log out page.
        /// </summary>
        public string LogOutLink
        {
            get;
            set;
        }

        /// <summary>
        /// Link to log in page.
        /// </summary>
        public string LogInLink
        {
            get;
            set;
        }

        public string UserType { get; set; }

        public UserType SiteType { get { return (UserType) Enum.Parse(typeof (UserType), UserType); } }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            // HttpContext is required.
            if (HttpContext.Current == null)
                throw new Exception("Invalid HTTP Context.");

            // Login Wrap
            writer.Write("<div class='LoginStatus'><span class='l'></span><span class='r'></span>");
            
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                writer.Write("<span class='is-auth'>");

                // Check for first time cookie
                var ShowWelcomeOnly = HttpContext.Current.Request.QueryString["f"] != null && HttpContext.Current.Request.QueryString["f"].Equals("1");

                var Welcome = ShowWelcomeOnly ? "Welcome" : "Welcome back,";

                // Welcome...
                writer.Write("<span class='WelcomeMessage'>{0}<span>{1}</span></span>", Welcome, 
                    UIFacade.GetCurrentUser(SiteType) != null ? UIFacade.GetCurrentUser(SiteType).UserName : string.Empty);

                // Log out link
                writer.Write("<a href='{0}' class='{1}'>{2}</a>", LogOutLink, "log-off-button", EfxSettings.CopyLogout);
            }
            else
            {
                writer.Write("<span class='not-auth'>");

                // Log in link
                writer.Write("<a href='{0}' class='{1}'>{2}</a>", LogInLink, "log-on-button", EfxSettings.CopyLogin);
            }
             
            // End of Login Wrap
            writer.Write("</span></div>");

            base.Render(writer);
        }
    }
}
