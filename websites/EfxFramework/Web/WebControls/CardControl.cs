﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.Web.WebControls
{
    /// <summary>
    /// Represents the card for an object, e.g. Renter
    /// </summary>
    public class CardControl : WebControl, INamingContainer
    {
        /// <summary>
        /// Image to display if one is not provided
        /// </summary>
        public string PlaceHolderImage { get; set; }
        /// <summary>
        /// By default a colored background will be used, if this is set to true the background will be white.
        /// </summary>
        public bool LightScheme { get; set; }
        /// <summary>
        /// Adds css class to wrapper for card.
        /// </summary>
// ReSharper disable InconsistentNaming
        public string CustomCSSClass { get; set; }
// ReSharper restore InconsistentNaming
        /// <summary>
        /// The title for the card
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Line 1 of the card
        /// </summary>
        public string Line1 { get; set; }
        /// <summary>
        /// Line 2 of the card
        /// </summary>
        public string Line2 { get; set; }
        /// <summary>
        /// Line 3 of the card
        /// </summary>
        public string Line3 { get; set; }
        /// <summary>
        /// Line 4 of the card
        /// </summary>
        public string Line4 { get; set; }
        /// <summary>
        /// Line 5 of the card
        /// </summary>
        public string Line5 { get; set; }
        /// <summary>
        /// Line 6 of the card
        /// </summary>
        public string Line6 { get; set; }
        /// <summary>
        /// Image Url for image in card, will not add image if not set.
        /// </summary>
        public string ImageUrl { get; set; }

        private ITemplate _FooterTemplate;
        [TemplateContainer(typeof(CardControl))]
        public ITemplate FooterTemplate
        {
            get { return _FooterTemplate; }
            set { _FooterTemplate = value; }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            // HttpContext is required.
            if (HttpContext.Current == null)
                throw new Exception("Invalid HTTP Context.");

            writer.Write("<div class='CardWrap {0} {1}'><div class='CardLeft'>", CustomCSSClass, LightScheme ? "CardWrapLight" : string.Empty);
            // Title
            writer.Write("<h4>{0}</h4>", string.IsNullOrEmpty(Title) ? "[Title Here]" : Title);
            // Lines
            if (!String.IsNullOrEmpty(Line1)) writer.Write("<p>{0}</p>", string.IsNullOrEmpty(Line1) ? "This is line 1." : Line1);
            if (!String.IsNullOrEmpty(Line2)) writer.Write("<p>{0}</p>", string.IsNullOrEmpty(Line2) ? "This is line 2." : Line2);
            if (!String.IsNullOrEmpty(Line3)) writer.Write("<p>{0}</p>", string.IsNullOrEmpty(Line3) ? "This is line 3." : Line3);
            if (!String.IsNullOrEmpty(Line4)) writer.Write("<p>{0}</p>", Line4);
            if (!String.IsNullOrEmpty(Line5)) writer.Write("<p>{0}</p>", Line5);
            if (!String.IsNullOrEmpty(Line6)) writer.Write("<p>{0}</p>", Line6);

            writer.Write("</div><div class='CardRight'>");

            // Only add image if url is provided
            if (!String.IsNullOrEmpty(ImageUrl))
                writer.Write("<img src='{0}' alt='{1}' />", ImageUrl, Title);
            else
                writer.Write("<img src='{0}' alt='Not Set' />", String.IsNullOrEmpty(PlaceHolderImage) ? "/Images/icon_customer.png" : PlaceHolderImage);
            writer.Write("</div>");

            // Add footer template if specified
            if (FooterTemplate != null)
            {
                var Holder = new Panel();
                Holder.CssClass = "CardFooter";
                FooterTemplate.InstantiateIn(Holder);
                Holder.RenderControl(writer);
            }

            // End of cardwrap
            writer.Write("</div>");

            base.Render(writer);
        }
    }
}
