﻿using System;
using System.Web;
using System.Web.UI.WebControls;

namespace EfxFramework.Web.WebControls
{
    public class DeviceDetectControl : WebControl
    {
        public bool RedirectIfMobile
        {
            get;
            set;
        }

        public string RedirectUrl
        {
            get;
            set;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // Only check if we need to
            if (RedirectIfMobile)
            {
                // Are we mobile?
                bool IsMobile = HttpContext.Current.Request.Browser != null &&
                                HttpContext.Current.Request.Browser.IsMobileDevice;

                if (IsMobile && !String.IsNullOrEmpty(RedirectUrl))
                {
                    HttpContext.Current.Response.Redirect(RedirectUrl);
                }
            }
        }
    }
}
