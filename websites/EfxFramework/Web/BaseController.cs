﻿using System.Web.Mvc;
namespace EfxFramework.Web
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            // Mobile redirect
            if (Request.Browser.IsMobileDevice)
            {
                Response.Redirect(EfxSettings.MobileRedirectUrl);
            }
        }
    }
}
