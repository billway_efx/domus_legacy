//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EfxFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class ApplicationSubmissionType
    {
        public ApplicationSubmissionType()
        {
            this.ApplicantApplications = new HashSet<ApplicantApplication>();
        }
    
        public int ApplicationSubmissionTypeId { get; set; }
        public string ApplicationSubmissionTypeName { get; set; }
    
        public virtual ICollection<ApplicantApplication> ApplicantApplications { get; set; }
    }
}
