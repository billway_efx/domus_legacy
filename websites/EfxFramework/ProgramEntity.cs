using System.Data.SqlClient;
using Mb2x.Data;

namespace EfxFramework
{
    public abstract class BaseProgram
    {
        public string SetupFee { get; set; }
        public string AppAndDepositFee { get; set; }

        protected BaseProgram()
        {
            SetupFee = "Waived";
            AppAndDepositFee = "2.5% Flat";
        }
    }

    public class PropertyPaidProgram : BaseProgram
    {
        public string MonthlyFeeToPmc { get; set; }
        public string InterchangeCostCcToPmc { get; set; }
        public string AchCostToPmc { get; set; }

        public PropertyPaidProgram()
        {
            MonthlyFeeToPmc = "Waived";
            AchCostToPmc = "$0.89";
            InterchangeCostCcToPmc = "1.99%";
        }
    }

    public class AirProgram : BaseProgram
    {
        public string UnitCount { get; set; }
        public string AllInclusiveRate { get; set; }
        public string RentByCashFee { get; set; }

        public AirProgram()
        {
            RentByCashFee = "Tenant Paid: $3.99";
        }
    }

    public class SpaProgram : BaseProgram
    {
        public string MonthlyFeeToPmc { get; set; }
        public string CcFeeToResident { get; set; }
        public string AchFeeToResidentSingle { get; set; }
        public string AchFeeToResidentAuto { get; set; }

        public SpaProgram()
        {
            MonthlyFeeToPmc = "Waived";
            AchFeeToResidentAuto = "$0.89";
            AchFeeToResidentSingle = "$0.89";
            CcFeeToResident = "2.4% Flat*";
        }
    }

    public class ProgramEntity : BaseEntity
    {
        //Public Properties
        public int ProgramId { get; set; }
        public string ProgramDescription { get; set; }
        public decimal? CreditCardFee { get; set; }
        public decimal? ECheckFee { get; set; }
        public decimal? AppDepositFee { get; set; }

        public static ProgramEntity GetProgramEntityByPropertyId(int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new ProgramEntity(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_Property_GetProgramEntityByPropertyId", new SqlParameter("@PropertyId", propertyId)));
        }

        public static ProgramEntity GetProgramEntityByRenterId(int renterId)
        {
            var Property = EfxFramework.Property.GetPropertyByRenterId(renterId);
            return Property.PropertyId.HasValue ? GetProgramEntityByPropertyId(Property.PropertyId.Value) : new ProgramEntity();
        }

        public static int Set(int programId, string programDescription, decimal? creditCardFee, decimal? eCheckFee, decimal? appDepositFee)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
                EfxSettings.ConnectionString,
                "dbo.usp_ProgramEntity_SetSingleObject",
            new[]
                {
	                new SqlParameter("@ProgramId", programId),
	                new SqlParameter("@ProgramDescription", programDescription),
	                new SqlParameter("@CreditCardFee", creditCardFee),
	                new SqlParameter("@ECheckFee", eCheckFee),
	                new SqlParameter("@AppDepositFee", appDepositFee)
                });
        }

        public static int Set(ProgramEntity p)
        {
            return Set(
                p.ProgramId,
                p.ProgramDescription,
                p.CreditCardFee,
                p.ECheckFee,
                p.AppDepositFee
            );
        }

        public ProgramEntity(int? programId)
        {
            //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_ProgramEntity_GetSingleObject", new[] { new SqlParameter("@ProgramId", programId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
                ProgramId = (int)ResultSet[0];
                ProgramDescription = ResultSet[1] as string;
                CreditCardFee = ResultSet[2] as decimal?;
                ECheckFee = ResultSet[3] as decimal?;
                AppDepositFee = ResultSet[4] as decimal?;
            }
        }

        public ProgramEntity()
        {
            InitializeObject();
        }
    }
}
