﻿using Mb2x.ExtensionMethods;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using static System.Boolean;

namespace EfxFramework
{
    public static class EfxSettings
	{
        public static string ConnectionString => ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public static string ContactUsEmail => GetKeyValue("ContactUsEmail");

        public static string CcSettlementAchId => GetKeyValue("CCSettlementAchID");

        public static string MobileRedirectUrl => GetKeyValue("MobileRedirectUrl");

        private static string GetKeyValue(string key)
		{
			return NewDataAccess.ExecuteScalar<string>(ConnectionString, "dbo.usp_SystemSetting_GetValue", new SqlParameter("@SettingKey", key));
		}

        public static string ApplicationName => GetKeyValue("ApplicationName");

        public static string FirstTimeCookie => GetKeyValue("FirstTimeCookie");

        public static string CopyLogin => GetKeyValue("CopyLogin");

        public static string CopyLogout => GetKeyValue("CopyLogout");

        public static string OrganizationName => GetKeyValue("OrganizationName");

        public static int DisplayedItemsCount => GetKeyValue("DisplayedItemsCount").ToInt32();

        public static string ReportConnectionString => ConfigurationManager.ConnectionStrings["ReportsConnection"].ConnectionString;

        public static CultureInfo DefaultCulture
		{
            get
            {
                var theKeyValue = GetKeyValue("DefaultCulture");
                return CultureInfo.CreateSpecificCulture(theKeyValue);
            }
		}

        public static string SmtpHost => GetKeyValue("SmtpHost");

        public static string UserName365 => GetKeyValue("Email_365_userName");

        public static string password365 => GetKeyValue("Email_365_password");

        public  static string SmtpHostLocal => GetKeyValue("SmptHostLocal");

        public static string ErrorEmailList => GetKeyValue("ErrorEmailList");

        public static string MobileDateFormatString => GetKeyValue("MobileDateFormatString");

        public static string CreditCardProcessorUrl => GetKeyValue("CreditCardProcessorUrl");

        public static string CreditCardProcessorCustomerId => GetKeyValue("CreditCardProcessorCustomerId");

        public static string CreditCardProcessorCustomerPassword => GetKeyValue("CreditCardProcessorCustomerPassword");

        public static string AchUserId => GetKeyValue("AchUserId");

	    public static string AchPassword => GetKeyValue("AchPassword");

        public static string EfxSmsUserId => GetKeyValue("EfxSmsUserId");

        public static string EfxSmsPassword => GetKeyValue("EfxSmsPassword");

        public static string EfxYardiUsername => GetKeyValue("EfxYardiUsername");

        public static string EfxYardiPassword => GetKeyValue("EfxYardiPassword");

        public static string EfxYardiDatabaseName => GetKeyValue("EfxYardiDatabaseName");

        public static string EfxYardiDatabaseServer => GetKeyValue("EfxYardiDatabaseServer");

        public static string EfxYardiPlatform => GetKeyValue("EfxYardiPlatform");

        public static string EfxYardiEntity => GetKeyValue("EfxYardiEntity");

        public static string[] EfxYardiPropertyList
	    {
            get { return new[] { "billpay", "billpay1", "billpay2", "mcaprop" }; }
	    }
	    public static string EfxYardiLicenseInformation => GetKeyValue("EfxYardiLicenseInformation");

        public static bool UseTestMode => Parse(GetKeyValue("UseTestMode"));

        public static bool ProcessCreditCards => Parse(GetKeyValue("ProcessCreditCards"));

        public static string SendMailServerName => GetKeyValue("SendMailServerName");

        public static string SendMailUsername => GetKeyValue("SendMailUsername");

        public static string SendMailPassword => GetKeyValue("SendMailPassword");

        public static string SupportPhone => GetKeyValue("SupportPhone");

        public static string SupportPhoneResident => GetKeyValue("SupportPhoneResident");

        public static string SupportEmail => GetKeyValue("SupportEmail");

        public static string PaymentEmail => GetKeyValue("PaymentEmail");

        public static string FeedBackEmail => GetKeyValue("FeedBackEmail");

        public static string HomeLinkFirstTime => GetKeyValue("HomeLinkFirstTime");

        public static int RecentNewsCount => GetKeyValue("RecentNewsCount").ToInt32();

        public static int ArchivedYears => GetKeyValue("ArchivedYears").ToInt32();

        public static string ViewJobPostingUrl => GetKeyValue("ViewJobPostingUrl");

        public static string ViewApiUserUrl => GetKeyValue("ViewApiUserUrl");

        public static int RecentAnnouncmentCount => GetKeyValue("RecentAnnouncmentCount").ToInt32();

        public static string BingSearchKey => GetKeyValue("BingSearchKey");

        public static string WeatherKey => GetKeyValue("WeatherKey");

        public static int BingNewsResultsCount => GetKeyValue("BingNewsResultsCount").ToInt32();

        public static bool EnableExports => Parse(GetKeyValue("EnableExports"));

        public static bool UseYardiBatching => Parse(GetKeyValue("UseYardiBatching"));

        public static bool EnableNews => Parse(GetKeyValue("EnableNews"));

        public static bool EnableWeather => Parse(GetKeyValue("EnableWeather"));

        public static int EmailMaxRecipientLimit => GetKeyValue("EmailMaxRecipientLimit").ToInt32();

        public static string CurrentSite => ConfigurationManager.AppSettings["CurrentSite"] != null
            ? ConfigurationManager.AppSettings["CurrentSite"]
            : "";

        public static int AutoPaymentReminderDays => GetKeyValue("AutoPaymentReminderDays").ToInt32();

        public static string MarketingImageFolderLocation => GetKeyValue("MarketingImageFolderLocation");

        public static string PnmachClientId => GetKeyValue("PNMACHClientId");

        public static string PNMVersion => GetKeyValue("PNMVersion");

        public static string PNMUseSandboxMode => GetKeyValue("PNMUseSandboxMode");

        public static string PNMLimit => GetKeyValue("PNMLimit");

        public static string HtmlEmailImageFilePath => GetKeyValue("HtmlEmailImageFilePath");

        public static string HtmlEmailImageWebSite => GetKeyValue("HtmlEmailImageWebSite");

        public static string TechSupportEmail => GetKeyValue("TechSupportEmail");

        public static string DuplicateTransactionHourWindow => GetKeyValue("DuplicateTransactionHourWindow");

        public static string TermsOfUse => GetKeyValue("TermsOfUse");

        public static string PrivacyPolicy => GetKeyValue("PrivacyPolicy");

        public static string VersionNumber => GetKeyValue("versionNumber");

        public static bool AllowCSRLogin => Convert.ToBoolean(ConfigurationManager.AppSettings["AllowCSRLogin"]);

        public static string DomusAPIURL => GetKeyValue("DomusAPIEndPoint");

        public static bool UseDomusQueue => Parse(GetKeyValue("UseDomusQueue"));

        public static string ResetPasswordTimeLimit => GetKeyValue("ResetPasswordTimeLimit");
    }
}
