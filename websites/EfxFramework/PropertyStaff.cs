using System.Globalization;
using Mb2x.Data;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace EfxFramework
{
    public class PropertyStaff : BaseUser
    {
        //Private Members
        private TelephoneNumber _MainPhoneNumber;
        private TelephoneNumber _MobilePhoneNumber;
        private TelephoneNumber _FaxNumber;

        //Public Properties
        public override int? UserId { get { return PropertyStaffId; } }
        public int? PropertyStaffId { get; set; }
        public string MiddleName { get; set; }
        public string StreetAddress { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public int? StateProvinceId { get; set; }
        public string PostalCode { get; set; }
        public string MainPhoneNumber
        {
            get
            {
                return _MainPhoneNumber== null ? string.Empty : _MainPhoneNumber.Digits;
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _MainPhoneNumber = null;
                else
                {
                    if (_MainPhoneNumber == null)
                        _MainPhoneNumber = new TelephoneNumber(value);
                    else
                        _MainPhoneNumber.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }
        public string MobilePhoneNumber
        {
            get
            {
                return _MobilePhoneNumber == null ? string.Empty : _MobilePhoneNumber.Digits;
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _MobilePhoneNumber = null;
                else
                {
                    if (_MobilePhoneNumber == null)
                        _MobilePhoneNumber = new TelephoneNumber(value);
                    else
                        _MobilePhoneNumber.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }
        public string FaxNumber
        {
            get
            {
                return _FaxNumber == null ? string.Empty : _FaxNumber.Digits; 
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _FaxNumber = null;
                else
                {
                    if (_FaxNumber == null)
                        _FaxNumber = new TelephoneNumber(value);
                    else
                        _FaxNumber.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }
        public string AlternateEmailAddress1 { get; set; }
        public string AlternateEmailAddress2 { get; set; }
        public DateTime? BirthDate { get; set; }
        public decimal? CommissionRate { get; set; }
        public bool CanViewReports { get; set; }
        public bool CanViewPropertyDetail { get; set; }
        public bool CanCreateNewSalesStaffAccounts { get; set; }
        public bool CanViewSalesStaffAccounts { get; set; }
        public bool CanCreateNewRenterAccounts { get; set; }
        public bool CanViewRenterDetails { get; set; }
        public bool CanSendBulkMail { get; set; }
        public bool IsPropertyGeneralManager { get; set; }
        public bool CanChangePictures { get; set; }
        public bool CanChangeLeaseInfo { get; set; }
        public bool CanTakePaymentsForEfx { get; set; }
        public bool CanTakePaymentsForProperties { get; set; }
        public bool CanAddProperty { get; set; }

        public string DisplayAddress
        {
            get
            {
                var Prov = StateProvince.AK;
                if (StateProvinceId.HasValue)
                    Prov = (StateProvince)Enum.Parse(typeof(StateProvince), StateProvinceId.Value.ToString(CultureInfo.InvariantCulture));

                return string.Format("{0} {1} {2} {3} {4}", StreetAddress, StreetAddress2, City, Prov, PostalCode);
            }
        }

        protected override bool IsValidUser()
        {
            return PropertyStaffId.HasValue && PropertyStaffId.Value > 0;
        }

        public override BaseUser GetUserById(int userId)
        {
            return new PropertyStaff(userId);
        }

        public override BaseUser GetUserByEmailAddress(string emailAddress)
        {
            return new PropertyStaff(emailAddress);
        }

        public override int SetUser(BaseUser user, string authenticatedUser)
        {
            return Set((user as PropertyStaff), authenticatedUser);
        }

        //Public Methods
        public static bool IsPropertyStaffMainContactForProperty(int propertyStaffId, int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            var Result = NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_PropertyPropertyStaff_GetMainContactByPropertyIdAndPropertyStaffId", new[]
                {
                    new SqlParameter("@PropertyStaffId", propertyStaffId),
                    new SqlParameter("@PropertyId", propertyId)
                });

            return Result.HasValue && Result.Value > 0;
        }

        public static PropertyStaff GetMainContactByPropertyId(int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new PropertyStaff(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_PropertyPropertyStaff_GetMainContactByPropertyId", new SqlParameter("@PropertyId", propertyId)));
        }

        public static void DeletePropertyStaffByPropertyStaffId(int propertyStaffId, string authenticatedUser)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_PropertyStaff_DeletePropertyStaffByPropertyStaffId", new[] { new SqlParameter("@PropertyStaffId", propertyStaffId), new SqlParameter("@AuthenticatedUser", authenticatedUser) });
        }

        public static List<Mobile.Contact> GetPropertyStaffMobileContactListByPropertyId(int propertyId)
        {
            return GetPropertyStaffListByPropertyId(propertyId).Select(ps => new Mobile.Contact {EmailAddress = ps.PrimaryEmailAddress, Name = ps.DisplayName, PhoneNumber = ps.MainPhoneNumber}).ToList();
        }

        /// <summary>
        /// Recursive Method to get a Property Staff Member in hierarchical order, it will start at the number passed in and climb to 5 or default.
        /// You can skip passes by entering a number greater than 1 and going directly to that role
        /// </summary>
        /// <param name="staffList">The list of staff to iterate through</param>
        /// <param name="propertyId">The property Id to search</param>
        /// <param name="passNumber">The pass number to resolve (1 = MainContact, 2 = General Manager, 3 = Property Manager Role, 4 = Leasing Agent role, 5 = First member found)</param>
        /// <returns></returns>
        public static PropertyStaff GetStaffMemberHierarchicaly(List<PropertyStaff> staffList, int propertyId, int passNumber = 1)
        {
            var Staff = new PropertyStaff();

            switch (passNumber)
            {
                //First Pass get the main contact, if none exists, come back for another pass
                case 1:
                    Staff = PropertyStaff.GetMainContactByPropertyId(propertyId);
                    if (!Staff.PropertyStaffId.HasValue || Staff.PropertyStaffId.Value < 1)
                        return GetStaffMemberHierarchicaly(staffList, propertyId, 2);

                    return Staff;
                //Second pass get the general manager, if none exists, come back for another pass
                case 2:
                    Staff = staffList.FirstOrDefault(s => s.IsPropertyGeneralManager);
                    if (Staff == null || !Staff.PropertyStaffId.HasValue || Staff.PropertyStaffId.Value < 1)
                        return GetStaffMemberHierarchicaly(staffList, propertyId, 3);

                    return Staff;
                //Third pass get the first property staff member in the Property Manager role, if none exists, come back for another pass
                case 3:
                    foreach (var S in staffList)
                    {
                        var Roles = new List<Role>();

                        if (S.PropertyStaffId.HasValue && S.PropertyStaffId.Value > 0)
                            Roles = Role.GetRolesByStaffId(S.PropertyStaffId.Value);

                        var R = Roles.FirstOrDefault(r => r.RoleName == "Property Manager");

                        if (R != null)
                            return S;
                    }

                    return GetStaffMemberHierarchicaly(staffList, propertyId, 4);
                //Fourth pass get the first property staff member in the Leasing Agent role, if none exists, come back for another pass
                case 4:
                    foreach (var S in staffList)
                    {
                        var Roles = new List<Role>();

                        if (S.PropertyStaffId.HasValue && S.PropertyStaffId.Value > 0)
                            Roles = Role.GetRolesByStaffId(S.PropertyStaffId.Value);

                        var R = Roles.FirstOrDefault(r => r.RoleName == "Leasing Agent");

                        if (R != null)
                            return S;
                    }

                    return GetStaffMemberHierarchicaly(staffList, propertyId, 5);
                //If it got this far, return the first property staff member for the property, if none exists send an empty PropertyStaff object
                default:
                    return staffList.Count > 0 ? staffList[0] : Staff;
            }
        }

        /// <summary>
        /// Gets all property staff for all properties.
        /// </summary>
        public static List<PropertyStaff> GetAllPropertyStaffList()
        {
            //Return a list of Properties that are participating
            return DataAccess.GetTypedList<PropertyStaff>(
                EfxSettings.ConnectionString,
                "dbo.usp_PropertyStaff_GetAll"
                );
        }

        // task#: joselist-1a - Patrick Whittingham - 7/20/15 
        public static List<PropertyStaff> GetStaffListByProperty(int propertyId)
        {
            //Return a list of Properties that are participating
            return DataAccess.GetTypedList<PropertyStaff>(
                EfxSettings.ConnectionString,
                "dbo.usp_Property_LeasingAgentsByPropertyId",
                new SqlParameter("@PropertyId", propertyId)
                );
        }

        /// <summary>
        /// Gets all property staff for a given property.
        /// </summary>
        public static List<PropertyStaff> GetPropertyStaffListByPropertyId(int propertyId)
        {
            //Return a list of Properties that are participating
            return DataAccess.GetTypedList<PropertyStaff>(
                EfxSettings.ConnectionString,
                "dbo.usp_PropertyStaff_GetByPropertyId",
                new SqlParameter("@PropertyId", propertyId)
                );
        }

        public static byte[] GetPhoto(int propertyStaffId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<byte[]>(
                EfxSettings.ConnectionString,
                "dbo.usp_PropertyStaff_GetPhoto",
                new SqlParameter("@PropertyStaffId", propertyStaffId));
        }

        public static void SetPhoto(int propertyStaffId, byte[] photo, string authenticatedUser)
        {
            //Resize the photo
            var ResizedPhoto = photo.StandardizeImage(300);

            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(
                EfxSettings.ConnectionString,
                "dbo.usp_PropertyStaff_SetPhoto",
                new[]
                    {
                        new SqlParameter("@PropertyStaffId", propertyStaffId),
                        new SqlParameter("@Photo", ResizedPhoto),
                        new SqlParameter("@AuthenticatedUser", authenticatedUser)
                    });
        }

        public static int Set(int? propertyStaffId, string firstName, string middleName, string lastName, string streetAddress, string streetAddress2, string city, int? stateProvinceId, string postalCode, string mainPhoneNumber, string mobilePhoneNumber, string faxNumber, string primaryEmailAddress, string alternateEmailAddress1, string alternateEmailAddress2, DateTime? birthDate, decimal? commissionRate, byte[] passwordHash, byte[] salt, bool canViewReports, bool canViewPropertyDetail, bool canCreateNewSalesStaffAccounts, bool canViewSalesStaffAccounts, bool canCreateNewRenterAccounts, bool canViewRenterDetails, bool canSendBulkMail, bool isPropertyGeneralManager, bool canChangePictures, bool canChangeLeaseInfo, string authenticatedUser, bool canTakePaymentsForEfx, bool canTakePaymentsForProperties, bool canAddProperty)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
                EfxSettings.ConnectionString,
                "dbo.usp_PropertyStaff_SetSingleObject",
            new[]
                {
	                new SqlParameter("@PropertyStaffId", propertyStaffId),
	                new SqlParameter("@FirstName", firstName),
	                new SqlParameter("@MiddleName", middleName),
	                new SqlParameter("@LastName", lastName),
	                new SqlParameter("@StreetAddress", streetAddress),
	                new SqlParameter("@StreetAddress2", streetAddress2),
	                new SqlParameter("@City", city),
	                new SqlParameter("@StateProvinceId", stateProvinceId),
	                new SqlParameter("@PostalCode", postalCode),
	                new SqlParameter("@MainPhoneNumber", mainPhoneNumber.EmptyStringToNull()),
	                new SqlParameter("@MobilePhoneNumber", mobilePhoneNumber.EmptyStringToNull()),
	                new SqlParameter("@FaxNumber", faxNumber.EmptyStringToNull()),
	                new SqlParameter("@PrimaryEmailAddress", primaryEmailAddress),
	                new SqlParameter("@AlternateEmailAddress1", alternateEmailAddress1),
	                new SqlParameter("@AlternateEmailAddress2", alternateEmailAddress2),
	                new SqlParameter("@BirthDate", birthDate),
	                new SqlParameter("@CommissionRate", commissionRate),
                    new SqlParameter("@PasswordHash", passwordHash),
					new SqlParameter("@Salt", salt),
                    new SqlParameter("@CanViewReports", canViewReports),
                    new SqlParameter("@CanViewPropertyDetail", canViewPropertyDetail),
                    new SqlParameter("@CanCreateNewSalesStaffAccounts", canCreateNewSalesStaffAccounts),
                    new SqlParameter("@CanViewSalesStaffAccounts", canViewSalesStaffAccounts),
                    new SqlParameter("@CanCreateNewRenterAccounts", canCreateNewRenterAccounts),
                    new SqlParameter("@CanViewRenterDetails", canViewRenterDetails),
                    new SqlParameter("@CanSendBulkMail", canSendBulkMail),
                    new SqlParameter("@IsPropertyGeneralManager", isPropertyGeneralManager),
                    new SqlParameter("@AuthenticatedUser", authenticatedUser),
                    new SqlParameter("@CanChangePictures", canChangePictures),
                    new SqlParameter("@CanChangeLeaseInfo", canChangeLeaseInfo),
                    new SqlParameter("@CanTakePaymentsForEfx", canTakePaymentsForEfx),
                    new SqlParameter("@CanTakePaymentsForProperties", canTakePaymentsForProperties),
                    new SqlParameter("@CanAddProperty", canAddProperty)
                });
        }

        public static int Set(PropertyStaff p, string authenticatedUser)
        {
            return Set(
                p.PropertyStaffId,
                p.FirstName,
                p.MiddleName,
                p.LastName,
                p.StreetAddress,
                p.StreetAddress2,
                p.City,
                p.StateProvinceId,
                p.PostalCode,
                p.MainPhoneNumber,
                p.MobilePhoneNumber,
                p.FaxNumber,
                p.PrimaryEmailAddress,
                p.AlternateEmailAddress1,
                p.AlternateEmailAddress2,
                p.BirthDate,
                p.CommissionRate,
                p.PasswordHash,
                p.Salt,
                p.CanViewReports,
                p.CanViewPropertyDetail,
                p.CanCreateNewSalesStaffAccounts,
                p.CanViewSalesStaffAccounts,
                p.CanCreateNewRenterAccounts,
                p.CanViewRenterDetails,
                p.CanSendBulkMail,
                p.IsPropertyGeneralManager,
                p.CanChangePictures,
                p.CanChangeLeaseInfo,
                authenticatedUser,
                p.CanTakePaymentsForEfx,
                p.CanTakePaymentsForProperties,
                p.CanAddProperty
            );
        }

        public PropertyStaff(int? propertyStaffId)
        {
            //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_PropertyStaff_GetSingleObject", new[] { new SqlParameter("@PropertyStaffId", propertyStaffId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
                PropertyStaffId = (int)ResultSet[0];
                FirstName = ResultSet[1] as string;
                MiddleName = ResultSet[2] as string;
                LastName = ResultSet[3] as string;
                StreetAddress = ResultSet[4] as string;
                StreetAddress2 = ResultSet[5] as string;
                City = ResultSet[6] as string;
                StateProvinceId = ResultSet[7] as int?;
                PostalCode = ResultSet[8] as string;
                MainPhoneNumber = (ResultSet[9] as string);
                MobilePhoneNumber = (ResultSet[10] as string);
                FaxNumber = ResultSet[11] as string;
                PrimaryEmailAddress = ResultSet[12] as string;
                AlternateEmailAddress1 = ResultSet[13] as string;
                AlternateEmailAddress2 = ResultSet[14] as string;
                BirthDate = ResultSet[15] as DateTime?;
                CommissionRate = ResultSet[16] as decimal?;
                PasswordHash = ResultSet[17] as byte[];
                Salt = ResultSet[18] as byte[];
                CanViewReports = (bool) ResultSet[19];
                CanViewPropertyDetail = (bool) ResultSet[20];
                CanCreateNewSalesStaffAccounts = (bool) ResultSet[21];
                CanViewSalesStaffAccounts = (bool) ResultSet[22];
                CanCreateNewRenterAccounts = (bool) ResultSet[23];
                CanViewRenterDetails = (bool) ResultSet[24];
                CanSendBulkMail = (bool)ResultSet[25];
                IsPropertyGeneralManager = (bool) ResultSet[26];
                CanChangePictures = (bool) ResultSet[27];
                CanChangeLeaseInfo = (bool) ResultSet[28];
                CanTakePaymentsForEfx = (bool) ResultSet[29];
                CanTakePaymentsForProperties = (bool) ResultSet[30];
                CanAddProperty = (bool) ResultSet[31];
            }
        }

        //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
        public PropertyStaff(string primaryEmailAddress)
			: this(NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "dbo.usp_PropertyStaff_GetPropertyStaffByEmailAddress", new SqlParameter("@PrimaryEmailAddress", primaryEmailAddress)) as int?)
		{ }

        public PropertyStaff()
        {
            InitializeObject();
        }

    }
}
