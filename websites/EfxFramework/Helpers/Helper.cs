﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web;

namespace EfxFramework.Helpers
{
    public class Helper
    {
        public static bool IsRpoAdmin
        {
            get
            {
                var roleListAdmin = new List<RoleName> { RoleName.EfxPowerAdministrator };
                return RoleManager.IsUserInRole(RoleType.EfxAdministrator, CurrentUserId, roleListAdmin);
            }
        }

        public static bool IsCsrUser
        {
            get
            {
                var roleListAdmin = new List<RoleName> { RoleName.EfxCustomerService };
                return RoleManager.IsUserInRole(RoleType.EfxAdministrator, CurrentUserId, roleListAdmin);
            }
        }

        public static int CurrentUserId => HttpContext.Current.User.Identity.IsAuthenticated ? Convert.ToInt32(HttpContext.Current.User.Identity.Name) : 0;

        public static bool IsCorporateAdmin
		{
			get
			{
				var roleListCorporateAdmin = new List<RoleName> { RoleName.CorporateAdmin };
				return RoleManager.IsUserInRole(RoleType.PropertyStaff, CurrentUserId, roleListCorporateAdmin);
			}
		}

        public static bool IsPropertyManager
		{
			get
			{
				var roleListPropertyManager = new List<RoleName> { RoleName.PropertyManager };
				return RoleManager.IsUserInRole(RoleType.PropertyStaff, CurrentUserId, roleListPropertyManager);
			}
		}

        public static bool HasIntegrationManagerAccess
        {
            get
            {
                var roleListPropertyManager = new List<RoleName> { RoleName.PropertyManager };
                var roleListCorporateAdmin = new List<RoleName> { RoleName.CorporateAdmin };
                var roleListAdmin = new List<RoleName> { RoleName.EfxPowerAdministrator };

                var userInRole = false;
                var thisUserId = CurrentUserId;

                if (thisUserId == 0) return false;

                if (RoleManager.IsUserInRole(RoleType.EfxAdministrator, thisUserId, roleListAdmin))
                {
                    //EfxAdministrators always have access to Integration Manager
                    userInRole = true;
                }
                else
                {
                    if (!RoleManager.IsUserInRole(RoleType.PropertyStaff, thisUserId,
                            roleListPropertyManager) && !RoleManager.IsUserInRole(RoleType.PropertyStaff,
                            thisUserId, roleListCorporateAdmin)) return userInRole;
                    // Corporate Administrators and Property Managers have access if they have access to one or more properties that
                    // are integrated with MRI (Property.PMSTypeId = 4)
                    var theList = Property.GetPropertyListByPropertyStaffIdAndPmsTypeId(thisUserId, 4);
                    if (theList.Count > 0)
                    {
                        userInRole = true;
                    }
                }
                return userInRole;
            }
        }

        public static bool HasCsrAccess
        {
            get
            {
                var roleListPropertyManager = new List<RoleName>
                {
                    RoleName.PropertyManager
                };
                var roleListCorporateAdmin = new List<RoleName>
                {
                    RoleName.CorporateAdmin
                };
                var roleListAdmin = new List<RoleName> { RoleName.EfxPowerAdministrator };

                var userInRole = false;
                var thisUserId = CurrentUserId;

                if (thisUserId == 0) return false;

                if (RoleManager.IsUserInRole(RoleType.EfxAdministrator, thisUserId, roleListAdmin))
                {
                    userInRole = true;
                }
                else
                {
                    var TheCount = NewDataAccess.ExecuteScalar<int>(EfxSettings.ConnectionString, "dbo.usp_GetCsrAccess", 
                        new[] {new SqlParameter("@UserId", thisUserId)} );

                    userInRole = TheCount == 1;
                }
                return userInRole;
            }
        }
    }
}
