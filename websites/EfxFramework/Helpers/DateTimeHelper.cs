﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfxFramework.Helpers
{
    public class DateTimeHelper
    {
        /// <summary>
        /// Takes a UTC DateTime and a string representation of a time zone and converts the datetime to the specified timezone.
        /// </summary>
        public static DateTime setDateTimeZone(DateTime timeUtc, String zone)
        {
            TimeZoneInfo tzone = TimeZoneInfo.FindSystemTimeZoneById(zone);
            return TimeZoneInfo.ConvertTimeFromUtc(timeUtc, tzone);
        }
    }
}
