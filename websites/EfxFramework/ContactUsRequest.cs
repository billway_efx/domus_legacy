using Mb2x.Data;
using Mb2x.ExtensionMethods;
using System;
using System.ComponentModel;
using System.Data.SqlClient;

namespace EfxFramework
{
    public class ContactUsRequest : BaseEntity
    {
        //Private Members
        private TelephoneNumber _PropertyPhoneNumber;

        //Public Enumerations
        public enum ContactUsType
        {
            [Description("Property Management Company")]
            PropertyManagementCompany = 1,
            [Description("Renter")]
            Renter = 2,
            [Description("Other")]
            Other = 3
        }

        //Public Properties
        public int ContactUsRequestId { get; set; }
        public string PropertyName { get; set; }
        public string PropertyPhoneNumber
        {
            get
            {
                return _PropertyPhoneNumber== null ? string.Empty : _PropertyPhoneNumber.Digits;
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _PropertyPhoneNumber = null;
                else
                {
                    if (_PropertyPhoneNumber == null)
                        _PropertyPhoneNumber = new TelephoneNumber(value);
                    else
                        _PropertyPhoneNumber.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }
        public string PropertyEmailAddress { get; set; }
        public string PropertyContact { get; set; }
        public string TenantFullName { get; set; }
        public string TenantEmailAddress { get; set; }
        public string PostalCode { get; set; }
        public string Message { get; set; }
        public ContactUsType ContactUsTypeId { get; set; }
        public DateTime DateCreated { get; set; }
        public bool Processed { get; set; }

        public static int Set(int contactUsRequestId, string propertyName, string propertyPhoneNumber, string propertyEmailAddress, string propertyContact, string tenantFullName, string tenantEmailAddress, string postalCode, string message, ContactUsType contactUsTypeId, DateTime dateCreated, bool processed)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
                EfxSettings.ConnectionString,
                "dbo.usp_ContactUsRequest_SetSingleObject",
            new[]
                {
	                new SqlParameter("@ContactUsRequestId", contactUsRequestId),
	                new SqlParameter("@PropertyName", propertyName),
	                new SqlParameter("@PropertyPhoneNumber", propertyPhoneNumber.EmptyStringToNull()),
	                new SqlParameter("@PropertyEmailAddress", propertyEmailAddress),
	                new SqlParameter("@PropertyContact", propertyContact),
	                new SqlParameter("@TenantFullName", tenantFullName),
	                new SqlParameter("@TenantEmailAddress", tenantEmailAddress),
	                new SqlParameter("@PostalCode", postalCode),
	                new SqlParameter("@Message", message),
	                new SqlParameter("@ContactUsTypeId", contactUsTypeId),
	                new SqlParameter("@DateCreated", dateCreated),
	                new SqlParameter("@Processed", processed)
                });
        }

        public static int Set(ContactUsRequest c)
        {
            return Set(
                c.ContactUsRequestId,
                c.PropertyName,
                c.PropertyPhoneNumber,
                c.PropertyEmailAddress,
                c.PropertyContact,
                c.TenantFullName,
                c.TenantEmailAddress,
                c.PostalCode,
                c.Message,
                c.ContactUsTypeId,
                c.DateCreated,
                c.Processed
            );
        }

        public ContactUsRequest(int? contactUsRequestId)
        {
            //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_ContactUsRequest_GetSingleObject", new[] { new SqlParameter("@ContactUsRequestId", contactUsRequestId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
                ContactUsRequestId = (int)ResultSet[0];
                PropertyName = ResultSet[1] as string;
                PropertyPhoneNumber = ResultSet[2] as string;
                PropertyEmailAddress = ResultSet[3] as string;
                PropertyContact = ResultSet[4] as string;
                TenantFullName = ResultSet[5] as string;
                TenantEmailAddress = ResultSet[6] as string;
                PostalCode = ResultSet[7] as string;
                Message = ResultSet[8] as string;
                ContactUsTypeId = (ContactUsType)ResultSet[9];
                DateCreated = (DateTime)ResultSet[10];
                Processed = (bool)ResultSet[11];
            }
        }

        public ContactUsRequest()
        {
            InitializeObject();
        }

    }
}
