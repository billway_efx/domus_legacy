using System.Collections.Generic;
using Mb2x.Data;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class TransactionReversal : BaseEntity
	{
		//Public Properties
		[DisplayName(@"TransactionReversalId"), Required(ErrorMessage = "TransactionReversalId is required.")]
		public int TransactionReversalId { get; set; }

		[DisplayName(@"InternalTransactionId"), Required(ErrorMessage = "InternalTransactionId is required."), StringLength(6)]
		public string InternalTransactionId { get; set; }

		[DisplayName(@"ReversalAmount"), Required(ErrorMessage = "ReversalAmount is required.")]
		public decimal ReversalAmount { get; set; }

		[DisplayName(@"ReversalDate"), Required(ErrorMessage = "ReversalDate is required.")]
		public DateTime ReversalDate { get; set; }

	    public static List<TransactionReversal> GetReversalsByInternalTransactionId(string internalTransactionId)
	    {
	        return DataAccess.GetTypedList<TransactionReversal>(EfxSettings.ConnectionString, "dbo.usp_TransactionReversal_GetReversalsByInternalTransactionId", new SqlParameter("@InternalTransactionId", internalTransactionId));
	    }

	    public static int Set(int transactionReversalId, string internalTransactionId, decimal reversalAmount, DateTime reversalDate)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_TransactionReversal_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@TransactionReversalId", transactionReversalId),
	                    new SqlParameter("@InternalTransactionId", internalTransactionId),
	                    new SqlParameter("@ReversalAmount", reversalAmount),
	                    new SqlParameter("@ReversalDate", reversalDate)
                    });
        }

        public static int Set(TransactionReversal t)
        {
	        return Set(
		        t.TransactionReversalId,
		        t.InternalTransactionId,
		        t.ReversalAmount,
		        t.ReversalDate
	        );
        }

        public TransactionReversal(int transactionReversalId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_TransactionReversal_GetSingleObject", new [] { new SqlParameter("@TransactionReversalId", transactionReversalId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        TransactionReversalId = (int)ResultSet[0];
		        InternalTransactionId = ResultSet[1] as string;
		        ReversalAmount = (decimal)ResultSet[2];
		        ReversalDate = (DateTime)ResultSet[3];
            }
        }

		public TransactionReversal()
		{
			InitializeObject();
		}

	}
}
