using System.Web;
using EfxFramework.Web.Caching;
using Mb2x.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;

namespace EfxFramework
{
	public class PayableItem : BaseEntity
	{
      
        // Table Pay_PayableControllerItems ...
        // -----------------------------------------------------------

        //[RenterId] [int] NOT NULL,
        //[LeaseId] [int] NOT NULL,
        //[PropertyId] [int] NOT NULL,
        //[PaymentDueDate] [datetime] NOT NULL,
        //[Description] [nvarchar](100) NOT NULL,
        //[Amount_PAY] [money] NOT NULL,
        //[Editable] [bit] NULL,
        //[PayChoose] [bit] NULL,
        //[AverageMonthlyRent] [money] NOT NULL,
        //[CurrentBalanceDue] [money] NOT NULL

		//Public Properties
        [DisplayName(@"PayableItemId"), Required(ErrorMessage = "PayableItemId is required.")]
        public long PayableItemId { get; set; }

        [DisplayName(@"RenterId"), Required(ErrorMessage = "RenterId is required.")]
        public int RenterId { get; set; }

        [DisplayName(@"LeaseId"), Required(ErrorMessage = "LeaseId is required.")]
        public int LeaseId { get; set; }

        [DisplayName(@"PropertyId"), Required(ErrorMessage = "PropertyId is required.")]
        public int PropertyId { get; set; }

        [DisplayName(@"PayableDueDate"), Required(ErrorMessage = "PayableDueDate is required.")]
        public DateTime PayableDueDate { get; set; }

        [DisplayName(@"Description"), Required(ErrorMessage = "Description is required.")]
        public string Description { get; set; }

        //[DisplayName(@"Amount_PAY"), Required(ErrorMessage = "Amount_PAY is required.")]
        //public decimal Amount_PAY { get; set; }

        [DisplayName(@"Editable"), Required(ErrorMessage = "Editable is required.")]
        public bool Editable { get; set; }

        [DisplayName(@"PayChoose"), Required(ErrorMessage = "PayChoose is required.")]
        public bool PayChoose { get; set; }

        [DisplayName(@"AverageMonthlyRent"), Required(ErrorMessage = "AverageMonthlyRent is required.")]
        public decimal AverageMonthlyRent { get; set; }

        [DisplayName(@"CurrentBalanceDue"), Required(ErrorMessage = "CurrentBalanceDue is required.")]
        public decimal CurrentBalanceDue { get; set; }

        [DisplayName(@"FeeTotal")]
        public decimal FeeTotal { get; set; }

        [DisplayName(@"RentAmount")]
        public decimal RentAmount { get; set; }

        //cakel: 08/09/2016
        [DisplayName(@"AllowPartialPayments")]
        public bool AllowPartialPayments { get; set; }

        //BW: 05.29.2020
        [DisplayName(@"AllowPartialPaymentsResidentPortal")]
        public bool AllowPartialPaymentsResidentPortal { get; set; }

        //cakel: 08/17/2016
        [DisplayName(@"PayLimit")]
        public decimal PayLimit { get; set; }

        //cakel: 08/17/2016
        [DisplayName(@"PayMessage")]
        public string PayMessage { get; set; }

        //cakel: 08/25/2016
        [DisplayName(@"AcceptedPaymentTypeID")]
        public int AcceptedPaymentTypeID { get; set; }


        public PayableItem()
		{
			InitializeObject();
		}

	}
}
