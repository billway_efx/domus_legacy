USE [EFX]
GO

/****** Object:  StoredProcedure [dbo].[usp_Property_GetSingleObject_v14]    Script Date: 1/9/2019 2:22:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*
    Author: Legacy
	Abstract: 
	Date Created: 01.08.2019

	Revision History

	1/9/2019 - JBane - Add CompanyID to result set

*/ 

ALTER PROCEDURE [dbo].[usp_Property_GetSingleObject_v14]
@PropertyId INT
AS

--Open the Symmetric Key
OPEN SYMMETRIC KEY EfxSymmetricKey DECRYPTION BY CERTIFICATE EfxCertificate
 
--Get the Symmetric Key's GUID
DECLARE @KeyGuid UNIQUEIDENTIFIER = KEY_GUID('EfxSymmetricKey')

SELECT
	PropertyId,
	PropertyCode,
	PropertyName,
	--cakel: 00498
	PropertyName2,
	NumberOfUnits,
	StreetAddress,
	StreetAddress2,
	City,
	StateProvinceId,
	PostalCode,
	OfficeEmailAddress,
	MainPhoneNumber,
	AlternatePhoneNumber,
	FaxNumber,
	AverageMonthlyRent,
	AverageConvenienceFee,
	RentalDepositBankName,
	CONVERT(NVARCHAR(50), DECRYPTBYKEY(EncryptedRentalDepositBankAccountNumber)) AS RentalDepositBankAccountNumber,
	CONVERT(NVARCHAR(20), DECRYPTBYKEY(EncryptedRentalDepositBankRoutingNumber)) AS RentalDepositBankRoutingNumber,
	RentalAccountAchClientId,
	RentalAccountFriendlyId,
	RentalAccountClearingDays,
	SecurityDepositBankName,
	CONVERT(NVARCHAR(50), DECRYPTBYKEY(EncryptedSecurityDepositBankAccountNumber)) AS SecurityDepositBankAccountNumber,
	CONVERT(NVARCHAR(20), DECRYPTBYKEY(EncryptedSecurityDepositBankRoutingNumber)) AS SecurityDepositBankRoutingNumber,
	SecurityAccountAchClientId,
	SecurityAccountFriendlyId,
	SecurityAccountClearingDays,
	MonthlyFeeBankName,
	CONVERT(NVARCHAR(50), DECRYPTBYKEY(EncryptedMonthlyFeeBankAccountNumber)) AS MonthlyFeeBankAccountNumber,
	CONVERT(NVARCHAR(20), DECRYPTBYKEY(EncryptedMonthlyFeeBankRoutingNumber)) AS MonthlyFeeBankRoutingNumber,	
	FeeAccountAchClientId,
	FeeAccountFriendlyId,
	FeeAccountClearingDays,
	IsParticipating,
	IsDeleted,
	ProgramId,
	PmsTypeId,
	PmsId,
	YardiUsername,
	YardiPassword,
	YardiServerName,
	YardiDatabaseName,
	YardiPlatform,
	YardiEndpoint,
	IsVerifiedRentReportersProperty,
	DateAdded,
	ShowPaymentDetail,
	TimeZone,
--cakel: BUGID0008
	PaymentDueDay,
--cakel: BUGID0008
	AllowPrePayments,
	AllowPartialPayments,
--cakel: BUGID0008
	NumOfPrePayDays,
	AcceptACH,
	AcceptCC,
	PayNearMeEnabled,
	--Salcedo - 7/11/2014 - added MRI integration support
	IsNull(MRIEnableFtpUpload,0) as MRIEnableFtpUpload,
	IsNull(MRIEnableFtpDownload,0) as MRIEnableFtpDownload,
	IsNull(MRIFtpSiteAddress,'') as MRIFtpSiteAddress,
	IsNull(MRIFtpPort,'') as MRIFtpPort,
	IsNull(MRIFtpAccountUserName,'') as MRIFtpAccountUserName,
	IsNull(MRIFtpAccountPassword,'') as MRIFtpAccountPassword,
	CCDepositBankName,
	CONVERT(NVARCHAR(50), DECRYPTBYKEY([EncryptedCCDepositBankAccountNumber])) as CCDepositBankAccountNumber,
	CONVERT(NVARCHAR(20), DECRYPTBYKEY([EncryptedCCDepositBankRoutingNumber])) as CCDepositBankRoutingNumber,
	CCDepositAccountAchClientID,
	CCDepositAccountFriendlyID,
	CCDepositAccountClearingDays,
	GroupSettlementPaymentFlag,
	YardiUseDepositDateAPI,
	AllowPartialPaymentsResidentPortal,
	GroupSettlementCashPaymentFlag,
	AMSIUserId,
	AMSIPassword,
	AMSIDatabase,
	AMSIClientMerchantID,
	AMSIImportEndPoint,
	AMSIExportEndPoint,
	MRIClientID,
	MRIDatabaseName,
	MRIWebServiceUserName,
	MRIPartnerKey,
	MRIImportEndPoint,
	MRIExportEndPoint,
	MRIWebServicePassword,
	NotificationEmailAddress,
	--cakel: 00551
	AMSIAchClientMerchantID,
	AMSICashClientMerchantID,
	--cmallory
	UsesApplyConnect,
	DisableAllPayments,
	DisablePayByText,

	--cakel: 00351 RealPage
	RealPagePmcID,
	RealPageUserName,
	RealPagePassword,
	RealPageImportURL,
	RealPageExportUrl,
	RealPageInternalUser,
	RealPageSiteID,
	PropertyIsPublic,
	--SWitherspoon: PropertyDetails page requires COmpanyId for some checks
	CompanyId


FROM
	dbo.Property
WHERE
	PropertyId = @PropertyId

--Close the Symmetric Key
CLOSE SYMMETRIC KEY EfxSymmetricKey
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO


