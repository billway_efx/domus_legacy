using Mb2x.Data;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class EfxAdministratorRole : BaseEntity
	{
		//Public Properties
		[DisplayName(@"EfxAdministratorId"), Required(ErrorMessage = "EfxAdministratorId is required.")]
		public int EfxAdministratorId { get; set; }

		[DisplayName(@"RoleId"), Required(ErrorMessage = "RoleId is required.")]
		public int RoleId { get; set; }


        public static int Set(int efxAdministratorId, int roleId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_EfxAdministratorRole_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@EfxAdministratorId", efxAdministratorId),
	                    new SqlParameter("@RoleId", roleId)
                    });
        }

        public static int Set(EfxAdministratorRole e)
        {
	        return Set(
		        e.EfxAdministratorId,
		        e.RoleId
	        );
        }

        public EfxAdministratorRole(int efxAdministratorId, int roleId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_EfxAdministratorRole_GetSingleObject", new [] { new SqlParameter("@EfxAdministratorId", efxAdministratorId), new SqlParameter("@RoleId", roleId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        EfxAdministratorId = (int)ResultSet[0];
		        RoleId = (int)ResultSet[1];
            }
        }

		public EfxAdministratorRole()
		{
			InitializeObject();
		}

	}
}
