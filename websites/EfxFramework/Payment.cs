using System.Data.SqlTypes;
using EfxFramework.PaymentMethods;
using EfxFramework.PublicApi.Payment;
using Mb2x.Data;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EfxFramework
{
   public class Payment : BaseEntity
   {
      //Private Members
      private TelephoneNumber _TextMessagePhoneNumber;

      //Public Properties
      public int PaymentId { get; set; }
      public int RenterId { get; set; }
      public int PayerId { get; set; }
      public int PaymentTypeId { get; set; }
      public string BankAccountNumber { get; set; }
      public string BankRoutingNumber { get; set; }
      public string CreditCardHolderName { get; set; }
      public string CreditCardAccountNumber { get; set; }
      public int? CreditCardExpirationMonth { get; set; }
      public int? CreditCardExpirationYear { get; set; }
      public string CreditCardCode { get; set; }
      public decimal? RentAmount { get; set; }
      public string RentAmountDescription { get; set; }
      public decimal? CharityAmount { get; set; }
      public string CharityName { get; set; }
      public decimal? OtherAmount1 { get; set; }
      public string OtherDescription1 { get; set; }
      public decimal? OtherAmount2 { get; set; }
      public string OtherDescription2 { get; set; }
      public decimal? OtherAmount3 { get; set; }
      public string OtherDescription3 { get; set; }
      public decimal? OtherAmount4 { get; set; }
      public string OtherDescription4 { get; set; }
      public decimal? OtherAmount5 { get; set; }
      public string OtherDescription5 { get; set; }
      public bool IsTextMessagePayment { get; set; }
      public string TextMessagePhoneNumber
      {
         get
         {
            return _TextMessagePhoneNumber == null ? string.Empty : _TextMessagePhoneNumber.Digits;
         }

         set
         {
            if (value.TrimNull().LengthNull() == 0)
               _TextMessagePhoneNumber = null;
            else
            {
               if (_TextMessagePhoneNumber == null)
                  _TextMessagePhoneNumber = new TelephoneNumber(value);
               else
                  _TextMessagePhoneNumber.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
            }
         }
      }
      public Carrier? CarrierId { get; set; }
      public DateTime TransactionDateTime { get; set; }
      public string TransactionId { get; set; }
      public string ResponseCode { get; set; }
      public string ResponseMessage { get; set; }
      public bool IsPmsProcessed { get; set; }
      public string PmsBatchId { get; set; }
      public PaymentChannel PaymentChannel { get; set; }
      public PaymentStatus? PaymentStatus { get; set; }


      //Public Methods
      public static Payment GetPaymentByTransactionId(string transactionId)
      {
         //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
         return new Payment(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_Payment_GetPaymentByTransactionId", new SqlParameter("@TransactionId", transactionId)));
      }

      public static Payment GetEarliestPaymentWithProcessingPendingWorkingStatus()
      {
         //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
         return new Payment(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_Payment_GetEarliestPaymentWithProcessingPendingWorkingStatus"));
      }


      public static List<Payment> GetPaymentListForRentReportersByDateRange(DateTime? startDate, DateTime? endDate)
      {
         if (!startDate.HasValue || startDate.Value < SqlDateTime.MinValue.Value)
            startDate = SqlDateTime.MinValue.Value;

         if (!endDate.HasValue || endDate.Value > SqlDateTime.MaxValue.Value)
            endDate = SqlDateTime.MaxValue.Value;

         return DataAccess.GetTypedList<Payment>(EfxSettings.ConnectionString, "dbo.usp_Property_GetPaymentListForRentReportersByDateRange", new[] { new SqlParameter("@BeginningDate", startDate), new SqlParameter("@EndingDate", endDate) });
      }

      public static List<Payment> GetPaymentListByPropertyIdAndDateRange(int propertyId, DateTime? startDate, DateTime? endDate)
      {
         if (!startDate.HasValue || startDate.Value < SqlDateTime.MinValue.Value)
            startDate = SqlDateTime.MinValue.Value;

         if (!endDate.HasValue || endDate.Value > SqlDateTime.MaxValue.Value)
            endDate = SqlDateTime.MaxValue.Value;

         return DataAccess.GetTypedList<Payment>(EfxSettings.ConnectionString, "dbo.usp_Payment_GetPaymentListByPropertyIdAndDateRange", new[] { new SqlParameter("@PropertyId", propertyId), new SqlParameter("@BeginningDate", startDate), new SqlParameter("@EndingDate", endDate) });
      }

      public static List<Payment> GetAllUnprocessedPmsPaymentsByPmsTypeId(int pmsTypeId)
      {
         //cakel: BUGID000179
         return DataAccess.GetTypedList<Payment>(EfxSettings.ConnectionString, "dbo.usp_Payment_GetAllUnprocessPmsPaymentsByPmsTypeId_V2", new SqlParameter("@PmsTypeId", pmsTypeId));
         //cake: BUGID000179 Commented out old code with old SP and replaced with new
         //return DataAccess.GetTypedList<Payment>(Settings.ConnectionString, "dbo.usp_Payment_GetAllUnprocessPmsPaymentsByPmsTypeId", new SqlParameter("@PmsTypeId", pmsTypeId));
      }

      //cakel: BUGID000179 - added new function to get only the properties who want the deposit date in the XML
      public static List<Payment> GetAllUnprocessedPmsPaymentsByPmsTypeIdWithDepositDate(int pmsTypeId)
      {
         //cakel: BUGID000179
         return DataAccess.GetTypedList<Payment>(EfxSettings.ConnectionString, "dbo.usp_Payment_GetAllUnprocessPmsPaymentsByPmsTypeIdWithDepositDate", new SqlParameter("@PmsTypeId", pmsTypeId));

      }

      public static List<Payment> GetMobilePaymentListByRenterId(int renterId)
      {
         return DataAccess.GetTypedList<Payment>(EfxSettings.ConnectionString, "dbo.usp_Payment_GetMobilePaymentListByRenterId", new SqlParameter("@RenterId", renterId));
      }

      public static List<Payment> GetPaymentListByRenterId(int renterId)
      {
         return DataAccess.GetTypedList<Payment>(
             EfxSettings.ConnectionString,
             "dbo.usp_Payment_GetPaymentListByRenterId",
             new SqlParameter("@RenterId", renterId)
             );
      }

      //CMallory - Task 00685 - Added method below.
      public static void AdminVoidEcheck(string InternalTransactionID)
      {
         NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_Payment_VoidECheck", new SqlParameter("@InternalTransactionID", InternalTransactionID));
      }

      public static Payment BuildPayment(int renterId, int payerId, PaymentType paymentType, string bankAccountNumber, string bankRoutingNumber, string cardHolderName, string ccNumber,
          string ccExpiration, PaymentAmount rentAmount, string rentDescription, PaymentAmount charityAmount, string charityName, PaymentAmount otherAmount, string otherDescription,
          string transactionId, bool isTextMessagePayment, string textMessagePhoneNumber, string responseCode, string responseMessage, Carrier? carrier, PaymentChannel paymentChannel,
          PaymentStatus? paymentStatus)
      {
         DateTime? Expiration = null;
         Decimal? Fee = null;
         string FeeType = null;

         if (!String.IsNullOrEmpty(ccExpiration))
            Expiration = ParseExpirationDate(ccExpiration);

         if (rentAmount != null)
         {
            Fee = rentAmount.FeeAmount;
            FeeType = Fee.Value > 0.00M ? rentAmount.FeeDescription : null;
         }
         else if (otherAmount != null && !otherDescription.ToLower().Contains("waived payments"))
         {
            Fee = otherAmount.FeeAmount;
            FeeType = Fee.Value > 0.00M ? otherAmount.FeeDescription : null;
         }

         return new Payment
         {
            RenterId = renterId,
            PayerId = payerId,
            PaymentTypeId = (int)paymentType,
            BankAccountNumber = bankAccountNumber,
            BankRoutingNumber = bankRoutingNumber,
            CreditCardHolderName = cardHolderName,
            CreditCardAccountNumber = ccNumber,
            CreditCardExpirationMonth = Expiration.HasValue ? Expiration.Value.Month : DateTime.UtcNow.Month,
            CreditCardExpirationYear = Expiration.HasValue ? Expiration.Value.Year : DateTime.UtcNow.Year,
            RentAmount = rentAmount != null ? rentAmount.BaseAmount : 0,
            RentAmountDescription = rentDescription,
            CharityAmount = charityAmount != null ? charityAmount.BaseAmount : 0,
            CharityName = charityName,
            OtherAmount1 = otherAmount != null ? otherAmount.BaseAmount : 0,
            OtherDescription1 = otherDescription,
            OtherAmount2 = Fee,
            OtherDescription2 = FeeType,
            TransactionId = transactionId,
            TransactionDateTime = DateTime.UtcNow,
            IsTextMessagePayment = isTextMessagePayment,
            TextMessagePhoneNumber = textMessagePhoneNumber,
            ResponseCode = responseCode,
            ResponseMessage = responseMessage,
            CarrierId = carrier,
            IsPmsProcessed = false,
            PaymentChannel = paymentChannel,
            PaymentStatus = paymentStatus


         };
      }

      private static DateTime ParseExpirationDate(string expirationDate)
      {
         int Result;
         return Int32.TryParse(expirationDate, out Result) ? new DateTime(Int32.Parse(expirationDate.Substring(2, 2)), Int32.Parse(expirationDate.Substring(0, 2)), 1) : DateTime.UtcNow;
      }

      private static Carrier? ProcessCarrier(int? carrierId)
      {
         if (carrierId.HasValue)
            return (Carrier)carrierId;

         return null;
      }
      public static int Set(int paymentId, int renterId, int payerId, int paymentTypeId, string bankAccountNumber, string bankRoutingNumber, string creditCardHolderName,
          string creditCardAccountNumber, int? creditCardExpirationMonth, int? creditCardExpirationYear, decimal? rentAmount, string rentAmountDescription, decimal? charityAmount,
          string charityName, decimal? otherAmount1, string otherDescription1, decimal? otherAmount2, string otherDescription2, decimal? otherAmount3,
          string otherDescription3, decimal? otherAmount4, string otherDescription4, decimal? otherAmount5,
          string otherDescription5, bool isTextMessagePayment, string textMessagePhoneNumber, Carrier? carrierId, DateTime transactionDateTime, string transactionId,
          string responseCode, string responseMessage, bool isPmsProcessed, string pmsBatchId, PaymentChannel paymentChannel, PaymentStatus? paymentStatus)
      {
         //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
         return NewDataAccess.ExecuteScalar<int>(
             EfxSettings.ConnectionString,
             "dbo.usp_Payment_SetSingleObject",
         new[]
             {
                   new SqlParameter("@PaymentId", paymentId),
                   new SqlParameter("@RenterId", renterId),
                   new SqlParameter("@PayerId", payerId),
                   new SqlParameter("@PaymentTypeId", paymentTypeId),
                   new SqlParameter("@BankAccountNumber", bankAccountNumber),
                   new SqlParameter("@BankRoutingNumber", bankRoutingNumber),
                    new SqlParameter("@CreditCardHolderName", creditCardHolderName),
                   new SqlParameter("@CreditCardAccountNumber", creditCardAccountNumber),
                   new SqlParameter("@CreditCardExpirationMonth", creditCardExpirationMonth),
                    new SqlParameter("@CreditCardExpirationYear", creditCardExpirationYear),
                   new SqlParameter("@RentAmount", rentAmount),
                   new SqlParameter("@RentAmountDescription", rentAmountDescription),
                   new SqlParameter("@CharityAmount", charityAmount),
                    new SqlParameter("@CharityName", charityName),
                    new SqlParameter("@OtherAmount1", otherAmount1),
                    new SqlParameter("@OtherDescription1", otherDescription1),
                    new SqlParameter("@OtherAmount2", otherAmount2),
                    new SqlParameter("@OtherDescription2", otherDescription2),
                    new SqlParameter("@OtherAmount3", otherAmount3),
                    new SqlParameter("@OtherDescription3", otherDescription3),
                    new SqlParameter("@OtherAmount4", otherAmount4),
                    new SqlParameter("@OtherDescription4", otherDescription4),
                    new SqlParameter("@OtherAmount5", otherAmount5),
                    new SqlParameter("@OtherDescription5", otherDescription5),
                   new SqlParameter("@IsTextMessagePayment", isTextMessagePayment),
                   new SqlParameter("@TextMessagePhoneNumber", textMessagePhoneNumber.EmptyStringToNull()),
                   new SqlParameter("@CarrierId", (int?)carrierId),
                    new SqlParameter("@TransactionDateTime", transactionDateTime),
                    new SqlParameter("@TransactionId", transactionId),
                    new SqlParameter("@ResponseCode", responseCode),
                    new SqlParameter("@ResponseMessage", responseMessage),
                    new SqlParameter("@IsPmsProcessed", isPmsProcessed),
                    new SqlParameter("@PmsBatchId", pmsBatchId),
                    new SqlParameter("@PaymentChannelId", (int) paymentChannel),
                    new SqlParameter("@PaymentStatusId", paymentStatus.HasValue ? (int) paymentStatus.Value : (int?) null),

             });
      }

      public static int Set(Payment p)
      {
         return Set(
             p.PaymentId,
             p.RenterId,
             p.PayerId,
             p.PaymentTypeId,
             p.BankAccountNumber,
             p.BankRoutingNumber,
             p.CreditCardHolderName,
             p.CreditCardAccountNumber,
             p.CreditCardExpirationMonth,
             p.CreditCardExpirationYear,
             p.RentAmount,
             p.RentAmountDescription,
             p.CharityAmount,
             p.CharityName,
             p.OtherAmount1,
             p.OtherDescription1,
             p.OtherAmount2,
             p.OtherDescription2,
             p.OtherAmount3,
             p.OtherDescription3,
             p.OtherAmount4,
             p.OtherDescription4,
             p.OtherAmount5,
             p.OtherDescription5,
             p.IsTextMessagePayment,
             p.TextMessagePhoneNumber,
             p.CarrierId,
             p.TransactionDateTime,
             p.TransactionId,
             p.ResponseCode,
             p.ResponseMessage,
             p.IsPmsProcessed,
             p.PmsBatchId,
             p.PaymentChannel,
             p.PaymentStatus

         );
      }
      public Payment(int? paymentId)
      {
         //Attempt to pull the data from the database
         //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
         var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_Payment_GetSingleObject", new[] { new SqlParameter("@PaymentId", paymentId) });
         if (ResultSet == null)
         {
            //If we don't get a record, initialize the object
            InitializeObject();
         }
         else
         {
            //Otherwise, populate this object
            PaymentId = (int)ResultSet[0];
            RenterId = (int)ResultSet[1];
            PayerId = (int)ResultSet[2];
            PaymentTypeId = (int)ResultSet[3];
            BankAccountNumber = ResultSet[4] as string;
            BankRoutingNumber = ResultSet[5] as string;
            CreditCardHolderName = ResultSet[6] as string;
            CreditCardAccountNumber = ResultSet[7] as string;
            CreditCardExpirationMonth = ResultSet[8] as int?;
            CreditCardExpirationYear = ResultSet[9] as int?;
            RentAmount = ResultSet[10] as decimal?;
            RentAmountDescription = ResultSet[11] as string;
            CharityAmount = ResultSet[12] as decimal?;
            CharityName = ResultSet[13] as string;
            OtherAmount1 = ResultSet[14] as decimal?;
            OtherDescription1 = ResultSet[15] as string;
            OtherAmount2 = ResultSet[16] as decimal?;
            OtherDescription2 = ResultSet[17] as string;
            OtherAmount3 = ResultSet[18] as decimal?;
            OtherDescription3 = ResultSet[19] as string;
            OtherAmount4 = ResultSet[20] as decimal?;
            OtherDescription4 = ResultSet[21] as string;
            OtherAmount5 = ResultSet[22] as decimal?;
            OtherDescription5 = ResultSet[23] as string;
            IsTextMessagePayment = (bool)ResultSet[24];
            TextMessagePhoneNumber = ResultSet[25] as string;
            CarrierId = ProcessCarrier(ResultSet[26] as int?);
            TransactionDateTime = (DateTime)ResultSet[27];
            TransactionId = ResultSet[28] as string;
            ResponseCode = ResultSet[29] as string;
            ResponseMessage = ResultSet[30] as string;
            IsPmsProcessed = (bool)ResultSet[31];
            PmsBatchId = ResultSet[32] as string;
            PaymentChannel = (PaymentChannel)ResultSet[33];

            var CurrentPaymentStatus = ResultSet[34] as int?;
            PaymentStatus = CurrentPaymentStatus.HasValue ? ((PaymentStatus)CurrentPaymentStatus.Value) : (PaymentStatus?)null;
         }
      }
      public Payment()
      {
         InitializeObject();
      }

   }
}
