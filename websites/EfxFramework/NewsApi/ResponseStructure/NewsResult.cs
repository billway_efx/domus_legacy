﻿using System;
using System.Runtime.Serialization;

namespace EfxFramework.NewsApi.ResponseStructure
{
    [DataContract]
    public class NewsResult
    {
        public string FormattedDate
        {
            get
            { 
                var NewDate = new DateTime();
                return !DateTime.TryParse(Date, out NewDate) ? string.Empty : NewDate.ToLongDateString();
            }
        }

        [DataMember]
        public string Date { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Url { get; set; }

        public NewsResult() {}
    }
}
