﻿using System.Runtime.Serialization;

namespace EfxFramework.NewsApi.ResponseStructure
{
    [DataContract]
    public class NewsResultsContainer
    {
        [DataMember]
        public NewsResults d { get; set; }

        public NewsResultsContainer() {}
    }
}
