﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EfxFramework.NewsApi.ResponseStructure
{
    [DataContract]
    public class NewsResults
    {
        [DataMember]
        public List<NewsResult> results { get; set; }

        public NewsResults() {}
    }
}
