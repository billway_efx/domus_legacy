﻿using EfxFramework.NewsApi.ResponseStructure;
using EfxFramework.Serialization;
using EfxFramework.Web.Caching;
using Mb2x.ExtensionMethods;
using System.Collections.Generic;
using System.Net;

namespace EfxFramework.NewsApi
{
    public static class BingNews
    {
        public static NewsResultsContainer GetNews(string location)
        {
            //Get the NewsResults cache
            var Cachelist = CacheManager.GetCacheItem<Dictionary<string, NewsResultsContainer>>(CacheKey.NewsResults);

            //If the cache does not have items
            if (Cachelist == null || Cachelist.Count < 1)
            {
                //Build a new cachelist dictionary
                Cachelist = BuildNewsDictionary(location);

                //Add the cachelist to the cache
                CacheManager.AddItemToCache(CacheKey.NewsResults, Cachelist, 60);
            }
            //If the cachelist does not contain the current location
            else if (!Cachelist.ContainsKey(location))
            {
                //Call DownloadNews and add the current location's news to the cachelist
                Cachelist.Add(location, DownloadNews(location));

                //Remove the NewsResults cache
                CacheManager.RemoveItemFromCache(CacheKey.NewsResults);
                //Add cachelist to the Cache
                CacheManager.AddItemToCache(CacheKey.NewsResults, Cachelist, 60);
            }

            //Return the cache list
            return Cachelist[location];
        }

        private static NewsResultsContainer DownloadNews(string location)
        {
            using (var Client = new WebClient())
            {
                //Create download url with the current location in the query string
                var DownloadUrl = string.Format("https://api.datamarket.azure.com/Bing/Search/v1/News?$format=json&Query=%27{0}%27", location.UrlEncode());

                //Set network credentials for the client
                Client.Credentials = new NetworkCredential("dog", EfxSettings.BingSearchKey);
                
                //Download the data
                var Request = Client.DownloadData(DownloadUrl);

                //Call JsonSerialier to serialize the response and return
                return JsonSerializer.SerializeJsonResponse<NewsResultsContainer>(Request);
            }
        }

        private static Dictionary<string, NewsResultsContainer> BuildNewsDictionary(string location)
        {
            using (var Client = new WebClient())
            {
                //Create download url with the current location in the query string
                var DownloadUrl = string.Format("https://api.datamarket.azure.com/Bing/Search/v1/News?$format=json&Query=%27{0}%27", location.UrlEncode());

                //Set network credentials for the client
                Client.Credentials = new NetworkCredential("dog", EfxSettings.BingSearchKey);
                
                //Download the data
                var Request = Client.DownloadData(DownloadUrl);

                //Build a dictionary with the JsonSerialized response and return
                return new Dictionary<string, NewsResultsContainer> {{location, JsonSerializer.SerializeJsonResponse<NewsResultsContainer>(Request)}};
            }
        }
    }
}
