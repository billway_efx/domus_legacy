﻿using System.Net.Mail;

namespace EfxFramework
{
	public class EmailAddress
	{
		//Properties
		public string User { get; private set; }
		public string Host { get; private set; }

		public override string ToString()
		{
			return string.Format("{0}@{1}", User, Host);
		}

		public EmailAddress(string e)
		{
			//Attempt to parse the email address
			var M = new MailAddress(e);
			User = M.User;
			Host = M.Host;
		}
	}
}