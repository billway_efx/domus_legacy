using EfxFramework.PaymentMethods;
using Mb2x.Data;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework.Pms
{
	public class PaymentExportSummary : BaseEntity
	{
		//Public Properties
		[DisplayName(@"PaymentExportSummaryId"), Required(ErrorMessage = "PaymentExportSummaryId is required.")]
		public int PaymentExportSummaryId { get; set; }

		[DisplayName(@"PropertyId"), Required(ErrorMessage = "PropertyId is required.")]
		public int PropertyId { get; set; }

		[DisplayName(@"Amount"), Required(ErrorMessage = "Amount is required.")]
		public decimal Amount { get; set; }

		[DisplayName(@"Status"), Required(ErrorMessage = "Status is required.")]
		public bool Status { get; set; }

		[DisplayName(@"BatchId"), StringLength(20)]
		public string BatchId { get; set; }

		[DisplayName(@"RequestXml"), Required(ErrorMessage = "RequestXml is required.")]
		public string RequestXml { get; set; }

		[DisplayName(@"ResponseXml"), Required(ErrorMessage = "ResponseXml is required.")]
		public string ResponseXml { get; set; }

		[DisplayName(@"PaymentTypeId"), Required(ErrorMessage = "PaymentTypeId is required.")]
		public int PaymentTypeId { get; set; }

		[DisplayName(@"DepositDate"), Required(ErrorMessage = "DepositDate is required.")]
		public DateTime DepositDate { get; set; }

		[DisplayName(@"ExportedPaymentCount"), Required(ErrorMessage = "ExportedPaymentCount is required.")]
		public int ExportedPaymentCount { get; set; }

		[DisplayName(@"ExportDate"), Required(ErrorMessage = "ExportDate is required.")]
		public DateTime ExportDate { get; set; }

        [DisplayName(@"BatchDetail")]
        public string BatchDetail { get; set; }

        public static void DeletePaymentExportSummary(int paymentExportSummaryId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_PaymentExportSummary_DeleteSingleObject", new SqlParameter("@PaymentExportSummaryId", paymentExportSummaryId));
        }

	    public void BuildBatchDetails()
	    {
	        if (Status)
	            BatchDetail = String.Format("{0} Payments Successfully Exported to Batch ID {1} for {2} with a deposit date of {3}", ExportedPaymentCount, BatchId, ((PaymentType) PaymentTypeId).ToString(), DepositDate.ToShortDateString());
	        else
	            BatchDetail = String.Format("Failure: {0}", ResponseXml);
	    }

	    public static int Set(int paymentExportSummaryId, int propertyId, decimal amount, bool status, string batchId, string requestXml, string responseXml, int paymentTypeId, DateTime depositDate, int exportedPaymentCount, DateTime exportDate, string batchDetail)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_PaymentExportSummary_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@PaymentExportSummaryId", paymentExportSummaryId),
	                    new SqlParameter("@PropertyId", propertyId),
	                    new SqlParameter("@Amount", amount),
	                    new SqlParameter("@Status", status),
	                    new SqlParameter("@BatchId", batchId),
	                    new SqlParameter("@RequestXml", requestXml),
	                    new SqlParameter("@ResponseXml", responseXml),
	                    new SqlParameter("@PaymentTypeId", paymentTypeId),
	                    new SqlParameter("@DepositDate", depositDate),
	                    new SqlParameter("@ExportedPaymentCount", exportedPaymentCount),
	                    new SqlParameter("@ExportDate", exportDate),
                        new SqlParameter("@BatchDetail", batchDetail)
                    });
        }

        public static int Set(PaymentExportSummary p)
        {
	        return Set(
		        p.PaymentExportSummaryId,
		        p.PropertyId,
		        p.Amount,
		        p.Status,
		        p.BatchId,
		        p.RequestXml,
		        p.ResponseXml,
		        p.PaymentTypeId,
		        p.DepositDate,
		        p.ExportedPaymentCount,
		        p.ExportDate,
                p.BatchDetail
	        );
        }

        public PaymentExportSummary(int? paymentExportSummaryId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_PaymentExportSummary_GetSingleObject", new [] { new SqlParameter("@PaymentExportSummaryId", paymentExportSummaryId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        PaymentExportSummaryId = (int)ResultSet[0];
		        PropertyId = (int)ResultSet[1];
		        Amount = (decimal)ResultSet[2];
		        Status = (bool)ResultSet[3];
		        BatchId = ResultSet[4] as string;
		        RequestXml = ResultSet[5] as string;
		        ResponseXml = ResultSet[6] as string;
		        PaymentTypeId = (int)ResultSet[7];
		        DepositDate = (DateTime)ResultSet[8];
		        ExportedPaymentCount = (int)ResultSet[9];
		        ExportDate = (DateTime)ResultSet[10];
                BatchDetail = ResultSet[11] as string;
            }
        }

		public PaymentExportSummary()
		{
			InitializeObject();
		}

	}
}
