using Mb2x.Data;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework.Pms
{
	public class PaymentExportDetail : BaseEntity
	{
		//Public Properties
		[DisplayName(@"PaymentExportDetailId"), Required(ErrorMessage = "PaymentExportDetailId is required.")]
		public int PaymentExportDetailId { get; set; }

		[DisplayName(@"PaymentExportSummaryId"), Required(ErrorMessage = "PaymentExportSummaryId is required.")]
		public int PaymentExportSummaryId { get; set; }

		[DisplayName(@"SuccessfulExport"), Required(ErrorMessage = "SuccessfulExport is required.")]
		public bool SuccessfulExport { get; set; }

		[DisplayName(@"PaymentDetailXml"), Required(ErrorMessage = "PaymentDetailXml is required.")]
		public string PaymentDetailXml { get; set; }

		[DisplayName(@"FailedExportDescription")]
		public string FailedExportDescription { get; set; }

        public static void DeletePaymentExportDetail(int paymentExportDetailId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_PaymentExportDetail_DeleteSingleObject", new SqlParameter("@PaymentExportDetailId", paymentExportDetailId));
        }

        public static int Set(int paymentExportDetailId, int paymentExportSummaryId, bool successfulExport, string paymentDetailXml, string failedExportDescription)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_PaymentExportDetail_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@PaymentExportDetailId", paymentExportDetailId),
	                    new SqlParameter("@PaymentExportSummaryId", paymentExportSummaryId),
	                    new SqlParameter("@SuccessfulExport", successfulExport),
	                    new SqlParameter("@PaymentDetailXml", paymentDetailXml),
	                    new SqlParameter("@FailedExportDescription", failedExportDescription)
                    });
        }

        public static int Set(PaymentExportDetail p)
        {
	        return Set(
		        p.PaymentExportDetailId,
		        p.PaymentExportSummaryId,
		        p.SuccessfulExport,
		        p.PaymentDetailXml,
		        p.FailedExportDescription
	        );
        }

        public PaymentExportDetail(int? paymentExportDetailId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_PaymentExportDetail_GetSingleObject", new [] { new SqlParameter("@PaymentExportDetailId", paymentExportDetailId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        PaymentExportDetailId = (int)ResultSet[0];
		        PaymentExportSummaryId = (int)ResultSet[1];
		        SuccessfulExport = (bool)ResultSet[2];
		        PaymentDetailXml = ResultSet[3] as string;
		        FailedExportDescription = ResultSet[4] as string;
            }
        }

		public PaymentExportDetail()
		{
			InitializeObject();
		}

	}
}
