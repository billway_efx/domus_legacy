﻿using System;
using Mb2x.ExtensionMethods;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Text;
using EfxFramework.Scheduler;
using System.Net;
using System.Net.Mail;
using EfxFramework;

//cakel: BUGID00316 - AMSI integration - Exporting Payments to AMSI

namespace EfxFramework.Pms.AMSI
{

    public class PaymentDetail
    {
        public string AmsiPropertyID;
        public string Bldg;
        public string unitNumber;
        public string ResidentID;
        public string TransactionID;
        public string Amount;
        public DateTime DepositDate;
        public string ErrorCode;
        public string ErrorDesc;
        public string BatchID;

    }

    public class ExportStats
    {
        public string AmsiPropertyID;
        public int SucessCount;
        public double SuccessAmount;
        public int FailedCount;
        public double FailedAmount;
    }


    public class AmsiExportRequest
    {

        public static void ProcessAmsiPaymentExport(int PropertyID = 0)
        {
            //DataSet AmsiDataSet = GetAmsiProperties();
            DataSet AmsiDataSet = null;



            if (PropertyID == 0)
            {
                AmsiDataSet = GetAmsiProperties();
            }
            else
            {
                AmsiDataSet = GetSingleAmsiProperty(PropertyID);
            }


            if (AmsiDataSet.Tables[0].Rows.Count > 0)
            {
                int ExportSuccessCounter = 0;
                int ExportFailedCounter = 0;
                //loop through all AMSI properties
                foreach (DataRow dr in AmsiDataSet.Tables[0].Rows)
                {
                    
                    string _ReturnXml = "";
                    int _PropertyID = int.Parse(dr["PropertyId"].ToString());
                    string PropertyPmsID = dr["PmsId"].ToString();
                    string _endPoint = dr["AmsiExportEndPoint"].ToString();
                    
                    string _method = "AddPayment";
                    //cakel: added Logging
                    Console.WriteLine("AMSI begin Export Request For PropertyID" + _PropertyID + " EndPoint: " + _endPoint);

                    string _PaymentData = GetXMLAmsiPaymentExportData(PropertyPmsID);
                    
                    if(_PaymentData == null || _PaymentData == "")
                    {
                        //this will go to the next iteration in the loop
                        Console.WriteLine("There was no data for PropertyID " + _PropertyID.ToString());
                        continue;
                    }

                    string _request = PropertyPaymentSoapRequest(PropertyPmsID,_PaymentData);
                   
                    string ErrorCode = "";

                    //This will call AMSI with Payment data and return results
                    _ReturnXml = AmsiRequest.WebServiceCall(_endPoint, _method, _request);

                    //Test Response Exmple: for testing only
                    //_ReturnXml = TestReturn();

                    byte[] byteArray = Encoding.ASCII.GetBytes(_ReturnXml);
                   using (var OutputStream = new MemoryStream(byteArray))
                   {
                       var Xml = XDocument.Load(OutputStream);
                       var Element = AmsiRequest.GetRootElement(Xml.Root, "Payments");

                      

                       if (Element != null)
                       {
                           var ExportDetail = new ExportStats();
                           ExportDetail.FailedCount = 0;
                           ExportDetail.SucessCount = 0;

                           foreach(XElement PaymentResponse in Element.Elements())
                           {
                               
                               ExportDetail.AmsiPropertyID = GetAttributeValue(PaymentResponse, "PropertyID");
                               var p = new PaymentDetail();
                               p.Bldg = GetAttributeValue(PaymentResponse, "BldgID");
                               p.unitNumber = GetAttributeValue(PaymentResponse, "UnitID");
                               p.Amount = GetAttributeValue(PaymentResponse, "Amount");
                               p.TransactionID = GetAttributeValue(PaymentResponse, "ClientTransactionID");
                               p.DepositDate = CalculateDepositDate(DateTime.Parse(GetAttributeValue(PaymentResponse, "ClientTransactionDate")));
                               int _PaymentType = 0;
                               string _paymentDesc = GetAttributeValue(PaymentResponse, "Description");
                               //cakel: Using returned values to determine what type of payment was made.  AMSI does not return the type of payment and this 
                               //code is used to tell us what the payment type is and insert into table.
                               switch(_paymentDesc)
                               {
                                   case "RPO CC":
                                       _PaymentType = 1;
                                       break;
                                   case "RPO ACH":
                                       _PaymentType = 2;
                                       break;
                                   case "RPO RBC":
                                       _PaymentType = 3;
                                       break;
                               }

                               foreach (XElement Payment in PaymentResponse.Elements())
                               {

                                   var NodeType = Payment.Name.LocalName;
                                   if (NodeType == "ErrorCode")
                                   {
                                       p.ErrorCode = Payment.Value.ToString();
                                       if(p.ErrorCode == "0")
                                       {
                                           ExportDetail.SucessCount += 1;
                                           ExportSuccessCounter += 1;
                                       }
                                       else
                                       {
                                           ExportDetail.FailedCount += 1;
                                           ExportFailedCounter += 1;
                                       }
                                       
                                   }

                                   if(NodeType == "ErrorDescription")
                                   {
                                       p.ErrorDesc = Payment.Value.ToString();
                                   }


                                   if (NodeType == "AdditionalData")
                                   {
                                       foreach(XElement AddInfo in Payment.Elements())
                                       {
                                           var AddNode = AddInfo.Name.LocalName;
                                           if(AddNode.ToString() == "eSiteBatchNo")
                                           {

                                               p.BatchID = AddInfo.Value.ToString();
                                           }
                                          
                                       }
 
                                   }   
                               }
                               
                               //Update Payment Table with BatchID if no error and update PaymentExport Summary Table
                                if(p.ErrorCode == "0")
                                {
                                    
                                    AmsiUpdatePaymentWithBatchID(int.Parse(p.TransactionID), p.BatchID, true);
                                    PaymentExportSummary.Set(0, _PropertyID, decimal.Parse(p.Amount), true, p.BatchID, _request, _ReturnXml, _PaymentType, p.DepositDate, 1, DateTime.Now, " ");

                                }
                               //Only Update PaymentExportSummary Table with error result
                               if(p.ErrorCode != "0")
                               {
                                   PaymentExportSummary.Set(0, _PropertyID, decimal.Parse(p.Amount), false, " ", _request, _ReturnXml, _PaymentType, p.DepositDate, 1, DateTime.Now, p.ErrorDesc);
                               }

                           }
                           
                           
                       }

                       
                       //Console stuff goes here
                       Console.WriteLine("Successfully Exported " + ExportSuccessCounter.ToString() + "records for PropertyID:" + _PropertyID);
                       Console.WriteLine("Failed to Export " + ExportFailedCounter.ToString() + "records for PropertyID:" + _PropertyID);

                   }

                }

                
            }
        }


        //This code is to make sure there is a node with value.
        //The AMSI is not consistent where not all levels contain the same nodes with attributes
        private static string GetAttributeValue(XElement _Node, string attributeName)
        {
            foreach (XAttribute _att in _Node.Attributes())
            {
                if (_att.Name.LocalName == attributeName)
                {
                    return _att.Value;
                }

            }
            return "";
        }

        //creates the xml data within the Parent xml call
        public static string GetXMLAmsiPaymentExportData(string AmsiPropertyPmsID)
        {
            string PaymentType = "";
            StringBuilder output = new StringBuilder();
            XmlWriterSettings xmlsettings = new XmlWriterSettings();
            //xmlsettings.IndentChars = "\t";
            xmlsettings.Indent = true;
            xmlsettings.OmitXmlDeclaration = true;
            XmlWriter writer = XmlWriter.Create(output, xmlsettings);

            SqlDataReader reader = GetAmsiPaymentsByProperty(AmsiPropertyPmsID);
            if (reader != null)
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Payments");

                while (reader.Read())
                {
                    writer.WriteStartElement("Payment");
                    writer.WriteAttributeString("PropertyID", reader["PmsId"].ToString());
                    writer.WriteAttributeString("BldgID", reader["AMSIBldgID"].ToString());
                    writer.WriteAttributeString("UnitID", reader["Unit"].ToString());
                    writer.WriteAttributeString("ResiID", reader["AMSIResidentID"].ToString());
                    writer.WriteAttributeString("ClientMerchantID", reader["AMSIClientMerchantID"].ToString());
                    writer.WriteAttributeString("ClientTransactionID", reader["PaymentID"].ToString());
                    writer.WriteAttributeString("ClientTransactionDate", reader["TransactionDate"].ToString());
                    writer.WriteAttributeString("Amount", reader["RentAmount"].ToString());
                    //cakel: Passing the values for RPO description to AMSI. These values are sent back to us in description.  Since we do not get back a payment type
                    //I added the values below.  
                    switch (reader["AmsiPaymentType"].ToString())
                    {
                        case "P":
                            PaymentType = "RPO CC";
                            break;
                        case "C":
                            PaymentType = "RPO ACH";
                            break;
                        case "$":
                            PaymentType = "RPO RBC";
                            break;

                    }

                    writer.WriteAttributeString("Description", PaymentType);
                    writer.WriteAttributeString("ClientJnlNo", "");
                    writer.WriteAttributeString("CheckNo", "");
                    writer.WriteAttributeString("ChargeIncCode", "");
                    writer.WriteAttributeString("ChargeDateFrom", "");
                    writer.WriteAttributeString("ChargeDateThru", "");
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Flush();

                //For testing output of string 
                string TestString = output.ToString();

                return output.ToString();
            }
            else
            {
                return "";
            }
            

        }


        public static SqlDataReader GetAmsiPaymentsByProperty(string AmsiPropertyID)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("AmsiGetAllUnprocessedPayments", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropertyPmsID", SqlDbType.NVarChar, 50).Value = AmsiPropertyID;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                ex.ToString();
                con.Close();
                return null;
            }
        }


        //Gets a List of all Amsi Properties for Importing residents, leases and properties
        public static DataSet GetAmsiProperties()
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                SqlCommand sqlComm = new SqlCommand("AmsiGetPropertiesForPaymentExport", conn);
                sqlComm.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;
                da.Fill(ds);
            }
            return ds;
        }

        public static DataSet GetSingleAmsiProperty(int _propID)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sqlstr = "SELECT PropertyId, PropertyCode, PropertyName, PmsId, AMSIUserId, AMSIPassword, AMSIDatabase, AMSIExportEndPoint FROM Property Where PmsTypeID = 2 and PropertyID = " + _propID;
                SqlCommand sqlComm = new SqlCommand(sqlstr, conn);
                sqlComm.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;
                da.Fill(ds);
            }
            return ds;
        }



        //Creates XML string for request
        public static string PropertyPaymentSoapRequest(string _AmsiPropertyID,string _XmlData)
        {
            

            SqlDataReader reader = AmsiRequest.GetPropertyAMSIlogin(_AmsiPropertyID);
            reader.Read();

            if (reader.HasRows)
            {

                string _AmsiPropID = reader["PmsId"].ToString();
                string _amsiUser = reader["AMSIUserId"].ToString();
                string _amsiPW = reader["AMSIPassword"].ToString();
                string _asmiPortFolio = reader["AMSIDatabase"].ToString();
                string _request =
            @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"">
                <soap:Body>
                 <AddPayment xmlns=""http://tempuri.org/"">
                     <UserID>##User</UserID>
                     <Password>##Password</Password>
                     <PortfolioName>##Portfolio</PortfolioName>
                     <XMLData>
                    <![CDATA[<EDEX>
                    ##XMlData 
                    </EDEX>]]> 
                    </XMLData>
                     </AddPayment>
                </soap:Body>
            </soap:Envelope>";

                _request = _request.Replace("##User", _amsiUser);
                _request = _request.Replace("##Password", _amsiPW);
                _request = _request.Replace("##Portfolio", _asmiPortFolio);
                _request = _request.Replace("##User", _amsiUser);
                _request = _request.Replace(" ##XMlData", _XmlData);
                return _request;

            }
            else 
            {
                return null;
            }
            

        }

        //Code copied from ExportRequestV2
        private static DateTime CalculateDepositDate(DateTime depositDate)
        {
            switch (depositDate.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                case DayOfWeek.Monday:
                    depositDate = depositDate.AddDays(2);
                    break;
                case DayOfWeek.Tuesday:
                    depositDate = depositDate.AddDays(1);
                    break;
            }

            if (depositDate.IsUnitedStatesBankingHoliday())
                depositDate = CalculateDepositDate(depositDate.AddDays(1));

            return depositDate;
        }



        public static void AmsiUpdatePaymentWithBatchID(int PaymentID, string BatchID, bool IsPmsProcesed)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("AmsiUpdatePaymentWithBatchID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PaymentID", SqlDbType.Int).Value = PaymentID;
            com.Parameters.Add("@BatchID", SqlDbType.NVarChar, 200).Value = BatchID;
            com.Parameters.Add("@IsPmsProcessed", SqlDbType.Bit).Value = IsPmsProcesed;
            
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }
        }


        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }


        //cakel: This is for testing only - example of one record that failed and 2 records that suceeced
        public static string TestReturn()
        {
            string Test =
                @"<soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                      <soap:Body>
                        <AddPaymentResponse xmlns=""http://tempuri.org/"">
                          <AddPaymentResult>
                            <Payments>
                              <Payment PropertyID=""001"" BldgID=""02"" UnitID=""205"" ResiID=""1"" ClientMerchantID=""987654321"" ClientTransactionID=""2348"" ClientTransactionDate=""3/9/2015 12:00:00 AM"" Amount=""1950.0000"" Description=""RPO CC"" ClientJnlNo="""" CheckNo="""" ChargeIncCode="""" ChargeDateFrom="""" ChargeDateThru="""">
                                <ErrorCode>1</ErrorCode>
                                <ErrorDescription>Client transaction id 2348 already exists for property-bldg-unit-resident 001-02-205-1 with an evolution reference number of 115084591 (created on 3/10/2015 11:18:08 AM).</ErrorDescription>
                                <EvolutionReference></EvolutionReference>
                                <EvolutionReferenceDescription></EvolutionReferenceDescription>
                                <AdditionalData>
                                  <ClientTransactionID>2348</ClientTransactionID>
                                  <PropertyID>001</PropertyID>
                                  <BldgID>02</BldgID>
                                  <UnitID>205</UnitID>
                                  <ResiID></ResiID>
                                  <TransHeaderNo>115084591</TransHeaderNo>
                                  <EntryCreatedDateTime>03/10/2015 11:18:08</EntryCreatedDateTime>
                                </AdditionalData>
                              </Payment>
                              <Payment PropertyID=""001"" BldgID=""03"" UnitID=""305"" ResiID=""1"" ClientMerchantID=""987654321"" ClientTransactionID=""2350"" ClientTransactionDate=""3/8/2015 12:00:00 AM"" Amount=""3900.0000"" Description=""RPO CC"" ClientJnlNo="""" CheckNo="""" ChargeIncCode="""" ChargeDateFrom="""" ChargeDateThru="""">
                                <ErrorCode>0</ErrorCode>
                                <ErrorDescription></ErrorDescription>
                                <EvolutionReference>115084592</EvolutionReference>
                                <EvolutionReferenceDescription>eSite transaction header table TransHeaderNo</EvolutionReferenceDescription>
                                <AdditionalData>
                                  <eSiteBatchNo>15678</eSiteBatchNo>
                                  <eSiteBankBookID>001</eSiteBankBookID>
                                </AdditionalData>
                              </Payment>
                              <Payment PropertyID=""001"" BldgID=""04"" UnitID=""403"" ResiID=""1"" ClientMerchantID=""987654321"" ClientTransactionID=""2351"" ClientTransactionDate=""3/8/2015 12:00:00 AM"" Amount=""2410.0000"" Description=""RPO CC"" ClientJnlNo="""" CheckNo="""" ChargeIncCode="""" ChargeDateFrom="""" ChargeDateThru="""">
                                <ErrorCode>0</ErrorCode>
                                <ErrorDescription></ErrorDescription>
                                <EvolutionReference>115084593</EvolutionReference>
                                <EvolutionReferenceDescription>eSite transaction header table TransHeaderNo</EvolutionReferenceDescription>
                                <AdditionalData>
                                  <eSiteBatchNo>15678</eSiteBatchNo>
                                  <eSiteBankBookID>001</eSiteBankBookID>
                                </AdditionalData>
                              </Payment>
                            </Payments>
                          </AddPaymentResult>
                        </AddPaymentResponse>
                      </soap:Body>
                    </soap:Envelope>";
            return Test;

        }



    }
}
