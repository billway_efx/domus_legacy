﻿using System;
using System.Net;
using System.IO;
using System.Web;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using EfxFramework.Security.Authentication;

//cakel: BUGID00316 Amsi Integration

namespace EfxFramework.Pms.AMSI
{
    public class AmsiPropertyDetails
    {
        public int PropertyID;
        public string PropertyName;
        public string StreetAddress1;
        public string StreetAddress2;
        public string City;
        public string State;
        public string PostalCode;
        public string MainPhone;
        public string Fax;
        public string email;
        public bool Isactive;
              

    }


    public class AmsiProperty
    {



       


        public static void AMSI_ImportSingleProperty(string PropertyPMSID)
        {
            string _Method = "GetPropertyList";
            string _xml = GetSinglePropertyXmlSoapRequest(PropertyPMSID);
            string _xmlRoot = "Properties";

            SqlDataReader Reader = AmsiRequest.GetPropertyAMSIlogin(PropertyPMSID);
            Reader.Read();
            if (Reader.HasRows)
            {
                int _RpoPropertyID = (int)Reader["PropertyId"];
               string _AmsiPropID = Reader["PmsId"].ToString();
               string _amsiUser = Reader["AMSIUserId"].ToString();
               string _amsiPW = Reader["AMSIPassword"].ToString();
               string _asmiPortFolio = Reader["AMSIDatabase"].ToString();
               string _endPoint = Reader["AmsiImportEndPoint"].ToString();

               string _Request = GetSinglePropertyXmlSoapRequest(PropertyPMSID);
               string _WebServiceType = "leasing.asmx";
                
                _xml = AmsiRequest.WebServiceCall(_endPoint, _Method, _Request);


                if (_xml != "")
                {

                    byte[] byteArray = Encoding.ASCII.GetBytes(_xml);
                    using (var OutputStream = new MemoryStream(byteArray))
                    {
                        var Xml = XDocument.Load(OutputStream);
                        var Element = AmsiRequest.GetRootElement(Xml.Root, _xmlRoot);

                        var XmlError = AmsiRequest.GetRootElement(Xml.Root, "Error");
                        //need to make sure we are returning data before running code
                        if (XmlError != null)
                        {
                            //Add code to record the error
                            return;
                        }

                        if (Element != null)
                        {
                            foreach (XElement LeaseProperty in Element.Elements())
                            {
                                var p = new AmsiPropertyDetails();
                                p.PropertyName = GetAttributeValue(LeaseProperty, "PropertyName1");
                                p.StreetAddress1 = GetAttributeValue(LeaseProperty, "PropertyAddrLine2");
                                p.City = GetAttributeValue(LeaseProperty, "PropertyAddrCity");
                                p.State = GetAttributeValue(LeaseProperty, "PropertyAddrState");
                                p.PostalCode = GetAttributeValue(LeaseProperty, "PropertyAddrZipCode");
                                p.MainPhone = AmsiRequest.CleanPhoneNumber(GetAttributeValue(LeaseProperty, "MgrOffPhoneNo"));
                                p.Fax = AmsiRequest.CleanPhoneNumber(GetAttributeValue(LeaseProperty, "PropertyName1"));
                                p.email = GetAttributeValue(LeaseProperty, "PropertyName1");

                                string PropertyStatus = GetAttributeValue(LeaseProperty, "LiveFlag");

                                if (PropertyStatus == "Y")
                                {
                                    p.Isactive = true;
                                }
                                else
                                {
                                    p.Isactive = false;
                                }

                                AmsiImportPropertyDetails(PropertyPMSID, p.PropertyName, p.StreetAddress1, p.City, p.State, p.PostalCode, p.MainPhone, p.Fax, p.email, p.Isactive);

                            }

                        }




                    }//End Using


                }//End if (_xml != "")





            }



        }





        public static void AmsiImportPropertyDetails(string pmsID, string PropertyName, string Address, string city, string state, string postalcode,
            string MainPhone, string FaxNumber, string propertyEmail, bool IsActive)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("AmsiImportPropertyDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropertyPmsID", SqlDbType.Int).Value = pmsID;
            com.Parameters.Add("@PropertyName", SqlDbType.NVarChar, 50).Value = PropertyName;
            com.Parameters.Add("@PropertyAddress", SqlDbType.NVarChar, 100).Value = Address;
            com.Parameters.Add("@PropertyCity", SqlDbType.NVarChar, 100).Value = city;
            com.Parameters.Add("@PropertyState", SqlDbType.NVarChar, 100).Value = state;
            com.Parameters.Add("@PropertyPostalCode", SqlDbType.NVarChar, 50).Value = postalcode;
            com.Parameters.Add("@PropertyMainPhone", SqlDbType.NVarChar, 50).Value = MainPhone;
            com.Parameters.Add("@PropertyFax", SqlDbType.NVarChar, 50).Value = FaxNumber;
            com.Parameters.Add("@PropertyEmail", SqlDbType.NVarChar, 50).Value = propertyEmail;
            com.Parameters.Add("@IsActive", SqlDbType.NVarChar, 100).Value = IsActive;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }

        }


        //This code is to make sure there is a node with value.
        //The AMSI is not consistent where not all levels contain the same nodes with attributes
        private static string GetAttributeValue(XElement _Node, string attributeName)
        {
            foreach (XAttribute _att in _Node.Attributes())
            {
                if (_att.Name.LocalName == attributeName)
                {
                    return _att.Value;
                }

            }
            return "";
        }


        public static string GetSinglePropertyXmlSoapRequest(string PropertyPMSID)
        {
            
            string PropertyRequest =
            @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"">
                <soap:Body>
                 <GetPropertyList xmlns=""http://tempuri.org/"">
                     <UserID>rentpaidonline</UserID>
                     <Password>rentpaid</Password>
                     <PortfolioName>RentPaidOnline</PortfolioName>
                     <XMLData>
                    <![CDATA[<edex> 
                     <propertyid>##PropID</propertyid> 
                        <includemarketingsources>0</includemarketingsources>
                        <includeamenities>1</includeamenities> 
                        <includeunittypes>0</includeunittypes>
                        <includememo></includememo>
                     </edex>]]> 
                    </XMLData>
                     </GetPropertyList>
                </soap:Body>
            </soap:Envelope>";

            PropertyRequest = PropertyRequest.Replace("##PropID", PropertyPMSID);

            return PropertyRequest;

        }

        //Connection String 
        private static String ConnectionString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }



    }
}
