﻿using System;
using System.Net;
using System.IO;
using System.Web;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using EfxFramework.Security.Authentication;
//cakel: 01/26/2016: Added Regex
using System.Text.RegularExpressions;

//cakel: BUGID00316

namespace EfxFramework.Pms.AMSI
{

    public class AmsiRenterDetails
    {
        public string PmsPropertyID;
        public int RenterPropertyID;
        public string FirstName;
        public string Lastname;
        public string MiddleName;
        public string MainPhoneNumber;
        public string MobilePhone;
        public string PrimaryEmailAddress;
        public string UnitNumber;
        public string BldgID;
        public string AMSIOccuSeqno;
        public string AmsiResidentID;
        public string PmsID;
        public int StateId;
        public string AmsiLeaseStatus;
        public bool IsActive;
        
    }

    public class AmsiRenterAddress
    {
        public string PmsPropertyID;
        public string AmsiResidentID;
        public string PmsID;
        public string UnitNumber;
        public string BldgID;
        public string AMSIOccuSeqno;
        public string address;
        public string city;
        public string state;
        public string PostalCode;
    }

    public class AmsiRenterLease
    {
        public string Leasepmsid; //ExternalReferenceID from Amsi xml 
        public string AmsiResidentID;
        public string LeaseStatus;
        public string LeaseAmsiPropertyID;
        public string LeaseUnitNumber;
        public string LeaseBldgID;
        public string LeaseName;
        public string LeaseEmail;
        public string LeasecreditStatus;
        public string LeasecreditStatusDesc;
        public string LeaseBeginDate;
        public string LeaseEndDate;
        public string LeaseBlockPaymentAccess;
        public string LeaseBlockAllAccess;
        public string LeaseChargeSeqNo;
        public string LeaseIncCode;
        public string LeaseIncDesc;
        public string LeaseChargeBeginDate;
        public string LeaseFrequency;
        public string LeaseDateLastCharged;
        public string LeaseNextDueDate;
        public string LeaseAmount;
        public string LeaseEndBalance;
    }

    public class AmsiRenterLeaseFee
    {
        public string Leasepmsid; //ExternalReferenceID from Amsi xml 
        public string AmsiResidentID;
        public string LeaseStatus;
        public string LeaseAmsiPropertyID;
        public string LeaseUnitNumber;
        public string LeaseBldgID;
        public string LeaseName;
        public string LeaseEmail;
        public string LeaseBeginDate;
        public string LeaseEndDate;
        public string LeaseChargeSeqNo;
        public string LeaseIncCode;
        public string LeaseIncDesc;
        public string LeaseChargeBeginDate;
        public string LeaseFrequency;
        public string LeaseDateLastCharged;
        public string LeaseNextDueDate;
        public string LeaseFeeAmount;
    }

    public enum RequestType
    {
        GetAllRenters = 1,
        GetSingleRenter = 2,

    }

    public class ErrorImport
    {
        public string ErrorDesc;


    }


    /*NOTES:
     * A resident’s (aka lease, aka tenant) account number consists of :
     * Property Id and Building Id and Unit ID and Resi ID
     *
     * 
     * 
     * 
     */


    public class AmsiRequest
    {
       //This request is for all rentes under property
       public static void AMSI_RequestXmlForRenterImport(int ReqType, string PropertyPmsID = "0", int _RenterID = 0)
       {
           int ImportCount = 0;
           string _PropertyPmsID = PropertyPmsID;
           string _AmsiPropID = "";
           string _amsiUser = "";
           string _amsiPW = "";
           string _asmiPortFolio = "";
           string _Request = "";
           string _xml = "";
           string _AmsiEndPoint = "";
           int _RpoPropertyID = 0;

           

           if (ReqType == 1)
           {
               SqlDataReader Reader = AmsiRequest.GetPropertyAMSIlogin(_PropertyPmsID);
               Reader.Read();
               if (Reader.HasRows)
               {
                   _RpoPropertyID = (int)Reader["PropertyId"];
                   _AmsiPropID = Reader["PmsId"].ToString();
                   _amsiUser = Reader["AMSIUserId"].ToString();
                   _amsiPW = Reader["AMSIPassword"].ToString();
                   _asmiPortFolio = Reader["AMSIDatabase"].ToString();
                   _AmsiEndPoint = Reader["AmsiImportEndPoint"].ToString();

                   _Request = AmsiRequest.GetResidentsByPropertyXmlSoapRequest(_PropertyPmsID);

                   //This request "GetPropertyResidents" can be used to get all or just a single renter for a property
                   _xml = AmsiRequest.WebServiceCall(_AmsiEndPoint, "GetPropertyResidents", _Request);

               }
           }

           if (ReqType == 2)
           {
               SqlDataReader Reader = AmsiRequest.GetRenterDetailsforAmsi(_RenterID);
               Reader.Read();
               if (Reader.HasRows)
               {
                   _RpoPropertyID = (int)Reader["PropertyId"];
                   _AmsiPropID = Reader["PropertyPmsID"].ToString();
                   _amsiUser = Reader["AMSIUserId"].ToString();
                   _amsiPW = Reader["AMSIPassword"].ToString();
                   _asmiPortFolio = Reader["AMSIDatabase"].ToString();
                   _AmsiEndPoint = Reader["AMSIImportEndPoint"].ToString();

                   _Request = AmsiRequest.GetSingleResidentXmlSoapRequest(_RenterID);

                   //This request "GetPropertyResidents" can be used to get all or just a single renter for a property
                   _xml = AmsiRequest.WebServiceCall(_AmsiEndPoint, "GetPropertyResidents", _Request);

               }
           }

            

           //Need to make sure we do not return an empty string 
           //cakel: BUGID00316 revision to if statement - 04/28/2015
            if (_xml != null && _xml.Length > 0)
            {
               byte[] byteArray = Encoding.ASCII.GetBytes(_xml);
               using (var OutputStream = new MemoryStream(byteArray))
                {

               var Xml = XDocument.Load(OutputStream);
               var Element = GetRootElement(Xml.Root, "PropertyResidents");

               var XmlError = GetRootElement(Xml.Root, "Error");
               if (XmlError != null)
               {
                   string _reqType = "";
                   switch (ReqType)
                   {
                       case 1:
                           _reqType = "AmsiRequest.GetResidentsByPropertyXmlSoapRequest";
                               break;
                       case 2:
                           _reqType = "AmsiRequest.GetSingleResidentXmlSoapRequest";
                           break;
                   }
                   //Add code to record the error and abort process
                   AmsiInsertResidentImportSummary(_RpoPropertyID, DateTime.Now, 0, 1, _Request, _xml);
                   foreach (XElement _error in XmlError.Elements())
                   {
                       var NodeType = _error.Name.LocalName;
                       string ErrorDesc = GetAttributeValue(_error, "ErrorDescription");
                       //Log the error from Xml response
                       var log = new RPO.ActivityLog();
                       if (NodeType == "ErrorDescription")
                       {
                           log.WriteLog("AMSI Resident Import Request - " + _reqType, ErrorDesc, 1, _RenterID.ToString(), false);
                       }
                       
                   }
                   
                   return;
               }

               //need to make sure we returned data
               if (Element != null)
               {
               //Start Renter Import
               foreach (XElement _Lease in Element.Elements())
               {
                   var log = new RPO.ActivityLog();

                   int ImportedRenterID = 0;
                   var r = new AmsiRenterDetails();
                   r.RenterPropertyID = _RpoPropertyID;
                   r.AmsiLeaseStatus = _Lease.Attribute("occustatuscode").Value.ToString();

                   //using a counter to only take first Renter
                   int OccupantCounter = 1;

                   foreach (XElement OccupantNodes in _Lease.Elements())
                   {
                       
                       var NodeType = OccupantNodes.Name.LocalName;
                       switch (NodeType)
                       {

                           case "Occupant":
                               string RespFlag = OccupantNodes.Attribute("ResponsibleFlag").Value.ToString();
                               
                               if (RespFlag == "Responsible")
                               {
                                   
                                   r.PmsPropertyID = _Lease.Attribute("PropertyId").Value.ToString();
                                   r.FirstName = GetAttributeValue(OccupantNodes,"OccuFirstName");
                                   r.Lastname = GetAttributeValue(OccupantNodes, "OccuLastName");
                                   r.MainPhoneNumber = CleanPhoneNumber(GetAttributeValue(OccupantNodes, "Phone1No"));
                                   r.MobilePhone = CleanPhoneNumber(GetAttributeValue(OccupantNodes, "Phone2No"));
                                   r.PrimaryEmailAddress = GetAttributeValue(OccupantNodes,"Email");

                                   if (r.PrimaryEmailAddress.Length < 2)
                                   {
                                       //need to add a default email address
                                       r.PrimaryEmailAddress = String.Format("{0}@unknown.com", Guid.NewGuid());
                                   }
                                   r.UnitNumber = GetAttributeValue(OccupantNodes, "UnitID");
                                   r.BldgID = GetAttributeValue(OccupantNodes, "BldgID");
                                   r.AMSIOccuSeqno = GetAttributeValue(OccupantNodes, "OccuSeqNo");
                                   r.AmsiResidentID = GetAttributeValue(OccupantNodes, "ResiID");
                                   //cakel: 03/23/2015 Updated PMS id to include delimiter
                                   r.PmsID = (r.PmsPropertyID + "-" + r.BldgID + "-" + r.UnitNumber+ "-" + r.AmsiResidentID);



                                   if (r.AmsiLeaseStatus == "P" || r.AmsiLeaseStatus == "X")
                                   {
                                       r.IsActive = false;
                                   }
                                   else
                                   {
                                       r.IsActive = true;
                                   }


                                   //Insert Renter Detail into table
                                   //--> Adds Renter to Payer Table if not exists
                                   //   --> Creates New Lease in Lease Table if not exists
                                   if (OccupantCounter == 1)
                                   {
                                       try
                                       {
                                       //cakel: BUGID00316 rev2 Update AMSI 05/07/2015 updated r.PmsPropertyID
                                       //cakel: Updated has registered
                                       ImportedRenterID = AmsiInsertSingleRenterByProperty(0, "", r.FirstName, "", r.Lastname, "", r.MainPhoneNumber, r.MobilePhone, "", r.PrimaryEmailAddress,
                                       "", "", "", r.UnitNumber, "", 1, "", true, true, true, "", false, "", r.PmsID, 2, r.IsActive,
                                       DateTime.Now, r.PmsPropertyID, true, 1, "", r.RenterPropertyID, false, "", r.AMSIOccuSeqno, r.AmsiResidentID, r.BldgID);

                                       //Need to create a blank lease record to avoid error in site 
                                       if (ImportedRenterID != 0)
                                       {
                                       AmsiCreateNewLeaseForRenter(ImportedRenterID, 0, _RpoPropertyID, r.UnitNumber);
                                       }

                                       }
                                       catch(Exception ex)
                                       {
                                           log.WriteLog("AmsiInsertSingleRenterByProperty", "Cannot Import AMSI Leasid=" + r.AmsiResidentID, 1, "", false);
                                           continue;
                                       }

                                       ImportCount += 1;

                                   }
                                   
                                   OccupantCounter += 1;
                                   //Removes old leases from renter in the AmsiLeaseFeeStagetbl and Rpo LeaseFee Table
                                   try
                                   {
                                       AmsiRemoveOldLeaseFees(r.PmsID);
                                   }
                                   catch (Exception ex)
                                   {
                                       continue;
                                   }
                                   

                               }
                               break;
                       }//End Switch

                   }//End OccupantNode
               }//End RenterImport


               //Update Renter Address
               foreach (XElement _LeaseAddress in Element.Elements())
               {
                   foreach (XElement AddressNodes in _LeaseAddress.Elements())
                   {
                       var NodeType = AddressNodes.Name.LocalName;
                       var a = new AmsiRenterAddress();
                       

                       switch (NodeType)
                       {
                           case "Address":

                                a.PmsPropertyID = GetAttributeValue(_LeaseAddress, "PropertyId");
                                a.BldgID = GetAttributeValue(_LeaseAddress, "BldgID");
                                a.UnitNumber = _LeaseAddress.Attribute("UnitID").Value.ToString();
                                a.AmsiResidentID = GetAttributeValue(_LeaseAddress, "ResiID");
                                a.address = GetAttributeValue(AddressNodes, "AddressLine2");
                                a.city = GetAttributeValue(AddressNodes, "City");
                                a.state = GetAttributeValue(AddressNodes, "State");
                                a.PostalCode = GetAttributeValue(AddressNodes, "ZipCode");
                               //cakel: 03/23/2015 added delimiter to avoid duplicates
                                a.PmsID = (a.PmsPropertyID + "-" + a.BldgID + "-" + a.UnitNumber + "-" + a.AmsiResidentID);

                                AmsiInsertRenterAddress(a.UnitNumber, a.PmsID, a.address, a.city, a.state, a.PostalCode);

                               break;
                       }
                   }//End AddressNodes
               }//End Update RenterAddress

               //Update Renter Lease
               foreach (XElement _Leases in Element.Elements())
               {
                   var log = new RPO.ActivityLog();
                   foreach (XElement _RenterLeaseNodes in _Leases.Elements())
                   {
                       var LeaseNodeType = _RenterLeaseNodes.Name.LocalName;
                       var l = new AmsiRenterLease();

                       switch (LeaseNodeType)
                       {
                           case "RecurringCharge":
                               string ChargeType = _RenterLeaseNodes.Attribute("IncCode").Value.ToString();
                               if (ChargeType == "RENT")
                               {
                                   

                                   l.LeaseStatus = GetAttributeValue(_Leases, "occustatuscode");
                                   l.LeaseAmsiPropertyID = GetAttributeValue(_Leases, "PropertyId");
                                   l.LeaseUnitNumber = GetAttributeValue(_Leases, "UnitID");
                                   l.LeaseBldgID = GetAttributeValue(_Leases, "BldgID");
                                   l.AmsiResidentID = GetAttributeValue(_Leases, "ResiID");
                                   l.LeaseName = GetAttributeValue(_Leases, "Name");
                                   l.LeaseEmail = GetAttributeValue(_Leases, "Email");
                                   l.LeasecreditStatus = GetAttributeValue(_Leases, "CreditStatus");
                                   l.LeasecreditStatusDesc = GetAttributeValue(_Leases, "CreditStatusDescription");
                                   l.LeaseBeginDate = GetAttributeValue(_Leases, "leasebegindate");
                                   l.LeaseEndDate = GetAttributeValue(_Leases, "leaseenddate");
                                   l.LeaseBlockPaymentAccess = GetAttributeValue(_Leases, "BlockPaymentAccess");
                                   l.LeaseBlockAllAccess = GetAttributeValue(_Leases, "BlockAllAccess");
                                   l.LeaseChargeSeqNo = GetAttributeValue(_RenterLeaseNodes, "ChargeSeqNo");
                                   l.LeaseIncCode = GetAttributeValue(_RenterLeaseNodes, "IncCode");
                                   l.LeaseIncDesc = GetAttributeValue(_RenterLeaseNodes, "IncCodeDesc");
                                   l.LeaseFrequency = GetAttributeValue(_RenterLeaseNodes, "FreqCode");
                                   l.LeaseAmount = GetAttributeValue(_RenterLeaseNodes, "Amount");
                                   l.LeaseEndBalance = GetAttributeValue(_Leases, "EndBalance");
                                   l.Leasepmsid = (l.LeaseAmsiPropertyID + "-" + l.LeaseBldgID + "-" + l.LeaseUnitNumber + "-" + l.AmsiResidentID);

                                   string occuStatus = l.LeaseStatus;
                                   if (occuStatus == "X" || occuStatus == "A")
                                   {
                                       l.LeaseChargeBeginDate = l.LeaseBeginDate;
                                       l.LeaseNextDueDate = l.LeaseEndDate;
                                       l.LeaseDateLastCharged = l.LeaseEndDate;
                                   }

                                   else
                                   {
                                       l.LeaseChargeBeginDate = GetAttributeValue(_RenterLeaseNodes, "ChargeBeginDate");
                                       l.LeaseNextDueDate = GetAttributeValue(_RenterLeaseNodes, "NextDueDate");
                                       l.LeaseDateLastCharged = GetAttributeValue(_RenterLeaseNodes, "DateLastCharged");
                                   }

                                   //cakel: 06/26/2015
                                   if (l.LeaseNextDueDate == "")
                                   {
                                       l.LeaseNextDueDate = l.LeaseEndDate;
                                   }
                                   if (l.LeaseDateLastCharged == "")
                                   {
                                       l.LeaseDateLastCharged = l.LeaseBeginDate;
                                   }


                                   if (l.LeaseChargeBeginDate != "")
                                   {
                                       try
                                       {
                                       AmsiInsertLeaseToStagetbl(l.Leasepmsid, l.LeaseStatus, l.LeaseAmsiPropertyID, l.LeaseUnitNumber, l.LeaseBldgID, l.AmsiResidentID, l.LeaseName, l.LeaseEmail,
                                           l.LeasecreditStatus, l.LeasecreditStatusDesc, DateTime.Parse(l.LeaseBeginDate), DateTime.Parse(l.LeaseEndDate),
                                           l.LeaseBlockPaymentAccess, l.LeaseBlockAllAccess, l.LeaseChargeSeqNo, l.LeaseIncCode, l.LeaseIncDesc, DateTime.Parse(l.LeaseChargeBeginDate),
                                           l.LeaseFrequency, DateTime.Parse(l.LeaseDateLastCharged), DateTime.Parse(l.LeaseNextDueDate), l.LeaseAmount, l.LeaseEndBalance, DateTime.Now);
                                   }
                                       catch(Exception ex)
                                       {
                                           //cakel: 08/01/2015
                                           log.WriteLog("AmsiInsertLeaseToStagetbl", "Cannot Import AMSI Leasid=" + l.Leasepmsid, 1, "", false);
                                           continue;
                                       }
                                       
                               }

                               }

                           break;
                       }

                   }
               }//End Renter Lease

               foreach (XElement LeaseFees in Element.Elements())
               {    //cakel: 08/01/2015
                   var log = new RPO.ActivityLog();

                   int RecurringCounter = 1;
                   int OpenItemCounter = 1;
                   string LeaseAccountID = "";
                   foreach (XElement RenterLeaseFee in LeaseFees.Elements())
                   {
                       var RenterLeaseFeeNode = RenterLeaseFee.Name.LocalName;

                       var rl = new AmsiRenterLeaseFee();

                       rl.LeaseStatus = GetAttributeValue(LeaseFees, "occustatuscode");
                       string occuStatus = rl.LeaseStatus;

                       //do not import lease fees for cancelled renters
                       if (occuStatus != "X" || occuStatus != "C" || occuStatus != "P")
                       {
                           rl.LeaseAmsiPropertyID = GetAttributeValue(LeaseFees, "PropertyId");
                           rl.LeaseUnitNumber = GetAttributeValue(LeaseFees, "UnitID");
                           rl.LeaseBldgID = GetAttributeValue(LeaseFees, "BldgID");
                           rl.AmsiResidentID = GetAttributeValue(LeaseFees, "ResiID");
                           rl.LeaseName = GetAttributeValue(LeaseFees, "Name");
                           rl.LeaseEmail = GetAttributeValue(LeaseFees, "Email");
                           rl.LeaseBeginDate = GetAttributeValue(LeaseFees, "leasebegindate");
                           rl.LeaseEndDate = GetAttributeValue(LeaseFees, "leaseenddate");
                           rl.LeaseChargeSeqNo = GetAttributeValue(RenterLeaseFee, "ChargeSeqNo");
                           rl.LeaseIncCode = GetAttributeValue(RenterLeaseFee, "IncCode");
                           rl.LeaseIncDesc = GetAttributeValue(RenterLeaseFee, "IncCodeDesc");
                           rl.LeaseFrequency = GetAttributeValue(RenterLeaseFee, "FreqCode");

                           //cakel: 07/01/2015  Fixed Lease ID Naming with "-".  Thye were missing and this caused a disconnect.
                           rl.Leasepmsid = (rl.LeaseAmsiPropertyID + "-" + rl.LeaseBldgID + "-" + rl.LeaseUnitNumber + "-" + rl.AmsiResidentID);


                           if (occuStatus == "X")
                           {
                               rl.LeaseDateLastCharged = rl.LeaseEndDate;
                           }

                           else
                           {
                               rl.LeaseDateLastCharged = GetAttributeValue(RenterLeaseFee, "DateLastCharged");
                           }


                           if (RenterLeaseFeeNode == "RecurringCharge")
                           {
                               if (rl.LeaseIncCode != "RENT")
                               {
                                   rl.LeaseFeeAmount = GetAttributeValue(RenterLeaseFee, "Amount");

                                   string RpoLeaseChargeCode = rl.LeaseIncCode + RecurringCounter.ToString();

                                   if(occuStatus != "X")
                                   {
                                       try
                                       {
                                   AmsiInsertLeaseFeeToStagetbl(rl.Leasepmsid, RenterLeaseFeeNode.ToString(), rl.LeaseStatus, rl.LeaseAmsiPropertyID, rl.LeaseUnitNumber,
                                           rl.LeaseBldgID, rl.AmsiResidentID, rl.LeaseName, rl.LeaseEmail, DateTime.Parse(rl.LeaseBeginDate), DateTime.Parse(rl.LeaseEndDate),
                                           rl.LeaseChargeSeqNo, rl.LeaseIncCode, rl.LeaseIncDesc, RpoLeaseChargeCode, rl.LeaseFrequency,
                                           rl.LeaseFeeAmount, DateTime.Now);

                                   AmsiAddLeaseFeeToRenter(rl.Leasepmsid, RpoLeaseChargeCode);

                                   RecurringCounter += 1;
                                       }
                                       catch(Exception ex)
                                       {
                                           log.WriteLog("AmsiInsertLeaseFeeToStagetbl - RecurringCharge", "Cannot Import Fee for LeaseID= " + rl.Leasepmsid, 1, "", false);
                                           continue;

                                       }
                                       
      
                                   }
                                   

                               }

                           }


                           if (RenterLeaseFeeNode == "OpenItem" && occuStatus != "X")
                           {

                               if (rl.LeaseIncCode != "")
                               {
                                   rl.LeaseFeeAmount = GetAttributeValue(RenterLeaseFee, "BalDue");

                                   string RpoLeaseChargeCode = rl.LeaseIncCode + "_OPENITEM" + OpenItemCounter.ToString();

                                   try
                                   {
                                   AmsiInsertLeaseFeeToStagetbl(rl.Leasepmsid, RenterLeaseFeeNode.ToString(), rl.LeaseStatus, rl.LeaseAmsiPropertyID, rl.LeaseUnitNumber,
                                          rl.LeaseBldgID, rl.AmsiResidentID, rl.LeaseName, rl.LeaseEmail, DateTime.Parse(rl.LeaseBeginDate), DateTime.Parse(rl.LeaseEndDate),
                                          rl.LeaseChargeSeqNo, rl.LeaseIncCode, rl.LeaseIncDesc, RpoLeaseChargeCode, rl.LeaseFrequency,
                                          rl.LeaseFeeAmount, DateTime.Now);

                                   AmsiAddLeaseFeeToRenter(rl.Leasepmsid, RpoLeaseChargeCode);

                                   OpenItemCounter += 1;
                               }
                                   catch(Exception ex)
                                   {
                                       log.WriteLog("AmsiInsertLeaseFeeToStagetbl - OpenItem", "Cannot Import Fee for LeaseID= " + rl.Leasepmsid, 1, "", false);
                                       continue;

                                   }
                                   
                               }

                           }
                           LeaseAccountID = rl.Leasepmsid;


                       }

                       
                   } //End RenterLeaseFee

                   //Add Update to Current Balance Here 
                   AmsiUpdateRenterBalance(LeaseAccountID);

               }//End LeaseFees

            } //End If(_xml != "")

           } //End If(Element != null) - Need to make sure we did not get an error
            
                //Record Renter Import Summary
           
           DateTime ImportDate = DateTime.Now;
           int ImportRecordCount = ImportCount;
           string RequestXML = _Request;
           string ResponseXML = _xml;
           AmsiInsertResidentImportSummary(_RpoPropertyID, ImportDate, ImportRecordCount, 0, RequestXML, ResponseXML);

           }

       }

       





       //This code is to make sure there is a node with value.
       //The AMSI is not consistent where not all levels contain the same nodes with attributes
       private static string GetAttributeValue(XElement _Node, string attributeName)
       {
           foreach (XAttribute _att in _Node.Attributes())
           {
               if (_att.Name.LocalName == attributeName)
               {
                   return _att.Value;
               }
               
           }
           return "";
       }


       //Imports Single AMSI Renter into DB - will update existing AMSI Renter
       public static int AmsiInsertSingleRenterByProperty(
           int PayerId,string Prefix,string FirstName,string MiddleName,string LastName,
            string Suffix,string MainPhoneNumber,string MobilePhoneNumber,string FaxNumber,string PrimaryEmailAddress,string AlternateEmailAddress1,
            string AlternateEmailAddress2,string StreetAddress,string Unit,string City,int StateProvinceId,string PostalCode,bool EmailNotifications,bool TextNotifications,
            bool IsParticipating, string AccountCodePin,bool IsCreditCardTestRenter,string SmsPaymentPhoneNumber,
            string PmsId,int PmsTypeId,bool IsActive, DateTime ExportDateTime,string PmsPropertyId,bool UserModified,int AcceptedPaymentTypeId,
            string RentReporterId,int RenterPropertyId,bool HasRegistered,string PNMCustomerIdentifier, string AMSIOccuSeqno, string AMSIResidentID, string AMSIBldgID)
       {
           //Generate a new random password
           var TempPassword = AuthenticationTools.GenerateRandomPassword();

           //Generate a new salt and set it
            byte[] _Salt = AuthenticationTools.GenerateSalt();

           //Hash the new password and set it
           byte[] _PasswordHash = AuthenticationTools.HashPassword(TempPassword, _Salt);
           
           SqlConnection con = new SqlConnection(ConnectionString);
           SqlCommand com = new SqlCommand("AmsiImportSingleRenterByProperty", con);
           com.CommandType = CommandType.StoredProcedure;
           com.Parameters.Add("@PayerId", SqlDbType.Int).Value = DBNull.Value;
           com.Parameters.Add("@Prefix", SqlDbType.NVarChar, 50).Value = Prefix;
           com.Parameters.Add("@FirstName", SqlDbType.NVarChar, 100).Value = FirstName;
           com.Parameters.Add("@MiddleName", SqlDbType.NVarChar, 100).Value = MiddleName;
           com.Parameters.Add("@LastName", SqlDbType.NVarChar, 100).Value = LastName;
           com.Parameters.Add("@Suffix", SqlDbType.NVarChar, 50).Value = Suffix;
           com.Parameters.Add("@MainPhoneNumber", SqlDbType.NVarChar, 50).Value = MainPhoneNumber;
           com.Parameters.Add("@MobilePhoneNumber", SqlDbType.NVarChar, 50).Value = MobilePhoneNumber;
           com.Parameters.Add("@FaxNumber", SqlDbType.NVarChar, 50).Value = FaxNumber;
           com.Parameters.Add("@PrimaryEmailAddress", SqlDbType.NVarChar, 100).Value = PrimaryEmailAddress;
           com.Parameters.Add("@AlternateEmailAddress1", SqlDbType.NVarChar, 100).Value = AlternateEmailAddress1;
           com.Parameters.Add("@AlternateEmailAddress2", SqlDbType.NVarChar, 100).Value = AlternateEmailAddress2;
           com.Parameters.Add("@StreetAddress", SqlDbType.NVarChar, 100).Value = StreetAddress;
           com.Parameters.Add("@Unit", SqlDbType.NVarChar,20).Value = Unit;
           com.Parameters.Add("@City", SqlDbType.NVarChar,100).Value = City;
           com.Parameters.Add("@StateProvinceId", SqlDbType.Int).Value = StateProvinceId;
           com.Parameters.Add("@PostalCode", SqlDbType.NVarChar, 20).Value = PostalCode;
           com.Parameters.Add("@EmailNotifications", SqlDbType.Bit).Value = EmailNotifications;
           com.Parameters.Add("@TextNotifications", SqlDbType.Bit).Value = TextNotifications;
           com.Parameters.Add("@IsParticipating", SqlDbType.Bit).Value = IsParticipating;
           com.Parameters.Add("@PasswordHash", SqlDbType.Binary).Value = _PasswordHash;
           com.Parameters.Add("@Salt", SqlDbType.Binary).Value = _Salt;
           com.Parameters.Add("@AccountCodePin", SqlDbType.NVarChar, 20).Value = AccountCodePin;
           com.Parameters.Add("@IsCreditCardTestRenter", SqlDbType.Bit).Value = IsCreditCardTestRenter;
           com.Parameters.Add("@IsCreditCardApproved", SqlDbType.Bit).Value = false;
           com.Parameters.Add("@SmsPaymentPhoneNumber", SqlDbType.NVarChar, 40).Value = DBNull.Value;
           com.Parameters.Add("@PmsId", SqlDbType.NVarChar, 50).Value = PmsId;
           com.Parameters.Add("@PmsTypeId", SqlDbType.Int).Value = PmsTypeId;
           com.Parameters.Add("@IsActive", SqlDbType.Bit).Value = IsActive;
           com.Parameters.Add("@ImportDateTime", SqlDbType.DateTime).Value = DateTime.Now;
           com.Parameters.Add("@ExportDateTime", SqlDbType.DateTime).Value = ExportDateTime;
           com.Parameters.Add("@PmsPropertyId", SqlDbType.NVarChar, 20).Value = PmsPropertyId;
           com.Parameters.Add("@UserModified", SqlDbType.Bit).Value = UserModified;
           com.Parameters.Add("@AcceptedPaymentTypeId", SqlDbType.Int).Value = AcceptedPaymentTypeId;
           com.Parameters.Add("@RentReporterId", SqlDbType.NVarChar, 20).Value = RentReporterId;
           com.Parameters.Add("@RenterPropertyId", SqlDbType.Int).Value = RenterPropertyId;
           com.Parameters.Add("@HasRegistered", SqlDbType.Bit).Value = HasRegistered;
           com.Parameters.Add("@PNMCustomerIdentifier", SqlDbType.NVarChar, 20).Value = PNMCustomerIdentifier;
           com.Parameters.Add("@FirstImportDateTime", SqlDbType.DateTime).Value = DateTime.Now;
           com.Parameters.Add("@AMSIOccuSeqno", SqlDbType.NVarChar,40).Value = AMSIOccuSeqno;
           com.Parameters.Add("@AMSIResidentID", SqlDbType.Int).Value = Int32.Parse(AMSIResidentID);
           com.Parameters.Add("@AMSIBldgID", SqlDbType.NVarChar, 50).Value = AMSIBldgID;
           com.Parameters.Add("@RenterIdOutput", SqlDbType.Int).Direction = ParameterDirection.Output;
           try
           {
               int _RenterID = 0;
               con.Open();
               com.ExecuteNonQuery();
               _RenterID = (int)com.Parameters["@RenterIdOutput"].Value;
               return _RenterID;
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
               con.Close();
           }
       }

       //cakel: Inserts/Updates Renters address to 
       private static void AmsiInsertRenterAddress(string unitNo, string pmsid, string street, string city, string state, string postalcode)
       {
           SqlConnection con = new SqlConnection(ConnectionString);
           SqlCommand com = new SqlCommand("AmsiImportRenterAddress", con);
           com.CommandType = CommandType.StoredProcedure;
           com.Parameters.Add("@Unit", SqlDbType.NVarChar, 20).Value = unitNo;
           com.Parameters.Add("@PmsID", SqlDbType.NVarChar, 20).Value = pmsid;
           com.Parameters.Add("@Street", SqlDbType.NVarChar, 100).Value = street;
           com.Parameters.Add("@City", SqlDbType.NVarChar, 100).Value = city;
           com.Parameters.Add("@State", SqlDbType.NVarChar, 20).Value = state;
           com.Parameters.Add("@PostalCode", SqlDbType.NVarChar, 20).Value = postalcode;
           try
           {
               con.Open();
               com.ExecuteNonQuery();
               con.Close();
           }
           catch (Exception ex)
           {
               con.Close();
               throw ex;
           }
       }

        public static void AmsiCreateNewLeaseForRenter(int RenterID, int LeaseID, int propertyID, string unitNumber)
        {
           SqlConnection con = new SqlConnection(ConnectionString);
           SqlCommand com = new SqlCommand("AmsiCreateNewLeaseForRenter", con);
           com.CommandType = CommandType.StoredProcedure;
           com.Parameters.Add("@RenterID", SqlDbType.NVarChar, 20).Value = RenterID;
           com.Parameters.Add("@LeaseID", SqlDbType.NVarChar, 20).Value = LeaseID;
           com.Parameters.Add("@PropertyID", SqlDbType.NVarChar, 100).Value = propertyID;
           com.Parameters.Add("@unitNumber", SqlDbType.NVarChar, 100).Value = unitNumber;
           try
           {
               con.Open();
               com.ExecuteNonQuery();
               con.Close();
           }
           catch (Exception ex)
           {
               con.Close();
               throw ex;
           }
        }



        //This will remove old lease fees from the staging table and the LeaseFee table
       public static void AmsiRemoveOldLeaseFees(string LeaseAccount)
       {
           
           SqlConnection con = new SqlConnection(ConnectionString);
           SqlCommand com = new SqlCommand("AmsiRemoveOldLeaseFees", con);
           com.CommandType = CommandType.StoredProcedure;
           com.Parameters.Add("@LeaseAccountID", SqlDbType.NVarChar, 50).Value = LeaseAccount;
           try
           {
               con.Open();
               com.ExecuteNonQuery();

           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
               con.Close();
           }

       }

       

       public static void AmsiInsertLeaseToStagetbl(string leaseAccountID, string leaseStatus, string pmsPropertyID, string leaseUnitno,
           string leaseBldgID, string LeaseAmsiResidentID,string leasename,
           string leaseEmail, string leaseCreditStatus, string leaseCreditstatusDesc, DateTime LeaseBeginDate, DateTime LeaseEndDate,
           string LeaseBlockPaymentAccess, string LeaseBlockAllAccess, string LeaseChargeSeqNo, string LeaseIncCode, string LeaseIncCodeDesc,
           DateTime LeaseChargeBeginDate, string LeaseFrequency,DateTime LeaseDateLastCharged, DateTime LeaseNextDueDate, string LeaseAmount, string LeaseEndBalance, DateTime AmsiLeaseLastUpdated)
       {

           var log = new RPO.ActivityLog();

           SqlConnection con = new SqlConnection(ConnectionString);
           SqlCommand com = new SqlCommand("AmsiImportLeaseToStagetbl", con);
           com.CommandType = CommandType.StoredProcedure;
           com.Parameters.Add("@LeaseAccountID", SqlDbType.NVarChar, 50).Value = leaseAccountID;
           com.Parameters.Add("@LeaseStatus", SqlDbType.NVarChar, 20).Value = leaseStatus;
           com.Parameters.Add("@PmsPropertyID", SqlDbType.NVarChar, 50).Value = pmsPropertyID;
           com.Parameters.Add("@LeaseUnitNo", SqlDbType.NVarChar, 50).Value = leaseUnitno;
           com.Parameters.Add("@LeaseBldgID", SqlDbType.NVarChar, 50).Value = leaseBldgID;
           com.Parameters.Add("@LeaseAmsiResiID", SqlDbType.NVarChar, 50).Value = LeaseAmsiResidentID;
           com.Parameters.Add("@LeaseName", SqlDbType.NVarChar, 50).Value = leasename;
           com.Parameters.Add("@LeaseEmail", SqlDbType.NVarChar, 60).Value = leaseEmail;
           com.Parameters.Add("@LeaseCreditStatus", SqlDbType.NVarChar, 50).Value = leaseCreditStatus;
           com.Parameters.Add("@LeaseCreditStatusDesc", SqlDbType.NVarChar, 50).Value = leaseCreditstatusDesc;
           com.Parameters.Add("@LeaseBeginDate", SqlDbType.DateTime).Value = LeaseBeginDate;
           com.Parameters.Add("@LeaseEndDate", SqlDbType.DateTime).Value = LeaseEndDate;
           com.Parameters.Add("@LeaseBlockPaymentAccess", SqlDbType.NVarChar, 50).Value = LeaseBlockPaymentAccess;
           com.Parameters.Add("@LeaseBlockAllAccess", SqlDbType.NVarChar, 50).Value = LeaseBlockAllAccess;
           com.Parameters.Add("@LeaseChargeSeqNo", SqlDbType.NVarChar, 50).Value = LeaseChargeSeqNo;
           com.Parameters.Add("@LeaseIncCode", SqlDbType.NVarChar, 50).Value = LeaseIncCode;
           com.Parameters.Add("@LeaseIncCodeDesc", SqlDbType.NVarChar, 50).Value = LeaseIncCodeDesc;
           com.Parameters.Add("@LeaseChargeBeginDate", SqlDbType.DateTime).Value = LeaseChargeBeginDate;
           com.Parameters.Add("@LeaseFrequency", SqlDbType.NVarChar, 50).Value = LeaseFrequency;
           com.Parameters.Add("@LeaseDateLastCharged", SqlDbType.DateTime).Value = LeaseDateLastCharged;
           com.Parameters.Add("@LeaseNextDueDate", SqlDbType.DateTime).Value = LeaseNextDueDate;
           com.Parameters.Add("@LeaseAmount", SqlDbType.NVarChar, 50).Value = LeaseAmount;
           com.Parameters.Add("@LeaseEndBalance", SqlDbType.NVarChar, 50).Value = LeaseEndBalance;
           com.Parameters.Add("@AmsiLeaseLastUpdated", SqlDbType.DateTime).Value = AmsiLeaseLastUpdated;
           try
           {
               con.Open();
               com.ExecuteNonQuery();
           }
           catch (Exception ex)
           {
               log.WriteLog("public static void AmsiInsertLeaseToStagetbl", ex.ToString(), 1, "", false);
               
           }
           finally
           {
               con.Close();
           }

       }


       //Note: Within this SP there is another SP - "AmsiAddLeaseFeesToPropery" whichh will add a fee to the property fee list if one does not exist
       public static void AmsiInsertLeaseFeeToStagetbl(string leaseAccountID, string leaseFeeType, string leaseStatus, string pmsPropertyID, string leaseUnitno,
          string leaseBldgID, string LeaseAmsiResidentID, string leasename, string leaseEmail, DateTime LeaseBeginDate, DateTime LeaseEndDate,
          string LeaseChargeSeqNo, string LeaseIncCode, string LeaseIncCodeDesc, string RPOChargeCode, string LeaseFrequency, 
          string LeaseAmount, DateTime AmsiLeaseLastUpdated)
       {

           var log = new RPO.ActivityLog();

           SqlConnection con = new SqlConnection(ConnectionString);
           SqlCommand com = new SqlCommand("AmsiImportLeaseFeeToStagetbl", con);
           com.CommandType = CommandType.StoredProcedure;
           com.Parameters.Add("@LeaseAccountID", SqlDbType.NVarChar, 50).Value = leaseAccountID;
           com.Parameters.Add("@LeaseFeeType", SqlDbType.NVarChar, 50).Value = leaseFeeType;
           com.Parameters.Add("@LeaseStatus", SqlDbType.NVarChar, 20).Value = leaseStatus;
           com.Parameters.Add("@PmsPropertyID", SqlDbType.NVarChar, 50).Value = pmsPropertyID;
           com.Parameters.Add("@LeaseUnitNo", SqlDbType.NVarChar, 50).Value = leaseUnitno;
           com.Parameters.Add("@LeaseBldgID", SqlDbType.NVarChar, 50).Value = leaseBldgID;
           com.Parameters.Add("@LeaseAmsiResiID", SqlDbType.NVarChar, 50).Value = LeaseAmsiResidentID;
           com.Parameters.Add("@LeaseName", SqlDbType.NVarChar, 50).Value = leasename;
           com.Parameters.Add("@LeaseEmail", SqlDbType.NVarChar, 60).Value = leaseEmail;
           com.Parameters.Add("@LeaseBeginDate", SqlDbType.DateTime).Value = LeaseBeginDate;
           com.Parameters.Add("@LeaseEndDate", SqlDbType.DateTime).Value = LeaseEndDate;
           com.Parameters.Add("@LeaseChargeSeqNo", SqlDbType.NVarChar, 50).Value = LeaseChargeSeqNo;
           com.Parameters.Add("@LeaseIncCode", SqlDbType.NVarChar, 50).Value = LeaseIncCode;
           com.Parameters.Add("@LeaseIncCodeDesc", SqlDbType.NVarChar, 50).Value = LeaseIncCodeDesc;
           com.Parameters.Add("@RPOChargeCode", SqlDbType.NVarChar, 50).Value = RPOChargeCode;
           com.Parameters.Add("@LeaseFrequency", SqlDbType.NVarChar, 50).Value = LeaseFrequency;
           com.Parameters.Add("@LeaseFeeAmount", SqlDbType.NVarChar, 50).Value = LeaseAmount;
           com.Parameters.Add("@AmsiLeaseFeeLastUpdated", SqlDbType.DateTime).Value = AmsiLeaseLastUpdated;
           try
           {
               con.Open();
               com.ExecuteNonQuery();
           }
           catch (Exception ex)
           {
               log.WriteLog("public static void AmsiInsertLeaseFeeToStagetbl", "AMSI LeaseID = " + leaseAccountID + ex.ToString(), 1, "", false);
           }
           finally
           {
               con.Close();
           }

       }

       public static void AmsiAddLeaseFeeToRenter(string AmsiLeaseLid, string RpoLeaseChargeCode)
       {
           var log = new RPO.ActivityLog();

           SqlConnection con = new SqlConnection(ConnectionString);
           SqlCommand com = new SqlCommand("AmsiAddLeaseFeeToRenter", con);
           com.CommandType = CommandType.StoredProcedure;
           com.Parameters.Add("@AmsiLeaseID", SqlDbType.NVarChar, 50).Value = AmsiLeaseLid;
           com.Parameters.Add("@RPOLeaseChargeCode", SqlDbType.NVarChar, 50).Value = RpoLeaseChargeCode;
   
           try
           {
               con.Open();
               com.ExecuteNonQuery();
           }
           catch (Exception ex)
           {
               log.WriteLog("public static void AmsiAddLeaseFeeToRenter", "AMSI LeaseID = " + AmsiLeaseLid + ex.ToString(), 1, "", false);
           }
           finally
           {
               con.Close();
           }
       }

       //AmsiUpdateRenterBalance
       public static void AmsiUpdateRenterBalance(string AmsiLeaseID)
       {
           SqlConnection con = new SqlConnection(ConnectionString);
           SqlCommand com = new SqlCommand("AmsiUpdateRenterBalance", con);
           com.CommandType = CommandType.StoredProcedure;
           com.Parameters.Add("@AmsiLeaseAccountID", SqlDbType.NVarChar, 50).Value = AmsiLeaseID;
          
           try
           {
               con.Open();
               com.ExecuteNonQuery();
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
               con.Close();
           }
       }



       //Returns Data by property for AMSI Login
       public static SqlDataReader GetPropertyAMSIlogin(string AMSIPropertyID)
       {
           SqlConnection con = new SqlConnection(ConnectionString);
           SqlCommand com = new SqlCommand("GetAMSIPropertyLogin", con);
           com.CommandType = CommandType.StoredProcedure;
           com.Parameters.Add("@PMSID", SqlDbType.NVarChar, 20).Value = AMSIPropertyID;
           try
           {
               con.Open();
               return com.ExecuteReader(CommandBehavior.CloseConnection);
           }
           catch (Exception ex)
           {
               con.Close();
               throw ex;
           }

       }

       public static SqlDataReader GetRenterDetailsforAmsi(int RenterID)
       {
           SqlConnection con = new SqlConnection(ConnectionString);
           SqlCommand com = new SqlCommand("GetRenterDetailsforAmsi", con);
           com.CommandType = CommandType.StoredProcedure;
           com.Parameters.Add("@RenterID", SqlDbType.Int).Value = RenterID;
           try
           {
               con.Open();
               return com.ExecuteReader(CommandBehavior.CloseConnection);
           }
           catch (Exception ex)
           {
               con.Close();
               throw ex;
           }

       }


       public static void AmsiInsertResidentImportSummary(int _propertyID, DateTime _ImportDate, int _SuccessfulRecords, int _FailedRecords, string _request, string _response)
       {
           string CleanXmlRequest = _request;
           CleanXmlRequest = CleanXmlRequest.Replace("<![CDATA[", "");
           CleanXmlRequest = CleanXmlRequest.Replace("]", "");
           CleanXmlRequest = XElement.Parse(CleanXmlRequest).ToString(SaveOptions.DisableFormatting);

           string CleanXmlResponse = XElement.Parse(_response).ToString(SaveOptions.DisableFormatting);


           SqlConnection con = new SqlConnection(ConnectionString);
           SqlCommand com = new SqlCommand("AmsiInsertResidentImportSummary", con);
           com.CommandType = CommandType.StoredProcedure;
           com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = _propertyID;
           com.Parameters.Add("@ImportDate", SqlDbType.DateTime).Value = _ImportDate;
           com.Parameters.Add("@SuccessfulRecordCount", SqlDbType.Int).Value = _SuccessfulRecords;
           com.Parameters.Add("@FailedRecordCount", SqlDbType.Int).Value = _FailedRecords;
           com.Parameters.Add("@Request", SqlDbType.NVarChar, -1).Value = CleanXmlRequest;
           com.Parameters.Add("@Response", SqlDbType.NVarChar, -1).Value = CleanXmlResponse;

           try
           {
               con.Open();
               com.ExecuteNonQuery();
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
               con.Close();
           }
       }
   

       public static string GetSingleResidentXmlSoapRequest(int _RenterID)
       {
           //To return a single renter you must use all variables : AMSIPropertyID, Unit, Bldgid, ResiID, and OccuSeqNo 
           SqlDataReader RenterReader = AmsiRequest.GetRenterDetailsforAmsi(_RenterID);
           RenterReader.Read();

           if (RenterReader.HasRows)
           {
               string _OccuSeqNo = RenterReader["AMSIOccuSeqno"].ToString();
               string _AmsiPropertyID = RenterReader["PropertyPmsID"].ToString();
               string _AmsiUserID = RenterReader["AMSIUserId"].ToString();
               string _Amsipassword = RenterReader["AMSIPassword"].ToString();
               string _AmsiPortfolio = RenterReader["AMSIDatabase"].ToString();
               string _AMSIBldgID = RenterReader["AMSIBldgID"].ToString();
               string _AMSIUnit = RenterReader["Unit"].ToString();
               string _AMSIResidentID = RenterReader["AMSIResidentID"].ToString();

               string ResidentRequest =
                   @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"">
                <soap:Body>
                 <GetPropertyResidents xmlns=""http://tempuri.org/"">
                     <UserID>##User</UserID>
                     <Password>##Password</Password>
                     <PortfolioName>##Portfolio</PortfolioName>
                     <XMLData>
                    <![CDATA[<EDEX>
                    <propertyid>##PropID</propertyid>
                    <bldgid>##BLDGID</bldgid>
                    <unitid>##UNIT</unitid>
                    <Resiid>##RESID</Resiid>
                    <occuseqno>##SEQNO</occuseqno>
                    <leasestatus></leasestatus>
                    <includeprimaryaddress>1</includeprimaryaddress> 
                    <includemailingaddress>0</includemailingaddress>
                    <includebillingaddress>0</includebillingaddress>
                    <includestatementaddress>0</includestatementaddress> 
                    <includeotheraddress>0</includeotheraddress> 
                    <includecontactdetails>1</includecontactdetails>
                    <includerecurringcharges>1</includerecurringcharges> 
                    <includememo></includememo> 
                    <includeoccupantmemo></includeoccupantmemo>
                    <includerenewals>0</includerenewals> 
                    <renewalcompletionstatus>1</renewalcompletionstatus>
                    <sodacompleteddatefrom></sodacompleteddatefrom> 
                    <sodacompleteddatethru></sodacompleteddatethru> 
                    <hidedob>0</hidedob> 
                    </EDEX>]]> 
                    </XMLData>
                     </GetPropertyResidents>
                </soap:Body>
            </soap:Envelope>";

               ResidentRequest = ResidentRequest.Replace("##PropID", _AmsiPropertyID);
               ResidentRequest = ResidentRequest.Replace("##User", _AmsiUserID);
               ResidentRequest = ResidentRequest.Replace("##Password", _Amsipassword);
               ResidentRequest = ResidentRequest.Replace("##Portfolio", _AmsiPortfolio);
               ResidentRequest = ResidentRequest.Replace("##SEQNO", _OccuSeqNo);

               ResidentRequest = ResidentRequest.Replace("##BLDGID", _AMSIBldgID);
               ResidentRequest = ResidentRequest.Replace("##UNIT", _AMSIUnit);
               ResidentRequest = ResidentRequest.Replace("##RESID", _AMSIResidentID);


               return ResidentRequest;
           }
           else
           {
               return "";
           }
       }

        //To return just a properties resident you only need to inlcude property id and credentials
       public static string GetResidentsByPropertyXmlSoapRequest(string _AmsiPropertyID)
       {
           SqlDataReader PropertyReader = GetPropertyAMSIlogin(_AmsiPropertyID);
           PropertyReader.Read();

           if (PropertyReader.HasRows)
           {

               string _AmsiUserID = PropertyReader["AMSIUserId"].ToString();
               string _Amsipassword = PropertyReader["AMSIPassword"].ToString();
               string _AmsiPortfolio = PropertyReader["AMSIDatabase"].ToString();

               string ResidentRequest =
               @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"">
                <soap:Body>
                 <GetPropertyResidents xmlns=""http://tempuri.org/"">
                     <UserID>##User</UserID>
                     <Password>##Password</Password>
                     <PortfolioName>##Portfolio</PortfolioName>
                     <XMLData>
                    <![CDATA[<EDEX>
                    <propertyid>##PropID</propertyid>
                    <bldgid></bldgid>
                    <unitid></unitid>
                    <Resiid></Resiid>
                    <occuseqno></occuseqno>
                    <leasestatus></leasestatus>
                    <includeprimaryaddress>1</includeprimaryaddress> 
                    <includemailingaddress>0</includemailingaddress>
                    <includebillingaddress>0</includebillingaddress>
                    <includestatementaddress>0</includestatementaddress> 
                    <includeotheraddress>0</includeotheraddress> 
                    <includecontactdetails>1</includecontactdetails>
                    <includerecurringcharges>1</includerecurringcharges> 
                    <includememo></includememo> 
                    <includeoccupantmemo></includeoccupantmemo>
                    <includerenewals>0</includerenewals> 
                    <renewalcompletionstatus>1</renewalcompletionstatus>
                    <sodacompleteddatefrom></sodacompleteddatefrom> 
                    <sodacompleteddatethru></sodacompleteddatethru> 
                    <hidedob>0</hidedob> 
                    </EDEX>]]> 
                    </XMLData>
                     </GetPropertyResidents>
                </soap:Body>
            </soap:Envelope>";

               ResidentRequest = ResidentRequest.Replace("##PropID", _AmsiPropertyID);
               ResidentRequest = ResidentRequest.Replace("##User", _AmsiUserID);
               ResidentRequest = ResidentRequest.Replace("##Password", _Amsipassword);
               ResidentRequest = ResidentRequest.Replace("##Portfolio", _AmsiPortfolio);
               return ResidentRequest;
           }
           else
           {
               return "";
           }
           

       }

       //Removes all special characters from string
       public static string CleanPhoneNumber(string _phone)
       {
           string _newPhone = _phone;
           string _AltPhone = "0000000000";

           _newPhone = _newPhone.Replace("(", "");
           _newPhone = _newPhone.Replace(")", "");
           _newPhone = _newPhone.Replace("-", "");
           _newPhone = _newPhone.Replace(" ", "");

           //cakel: 01/26/2016 Added new replace code because we are getting real phone numbers back with text at end
           _newPhone = _newPhone.Replace("cell", "");
           _newPhone = _newPhone.Replace("Cell", "");
           _newPhone = _newPhone.Replace("home", "");
           _newPhone = _newPhone.Replace("Home", "");

           //cakel: 01/26/2016 added regex to check for letters in Phone
           

           bool CheckForLetters = Regex.IsMatch(_newPhone, @"[A-Za-z]+");//@"[A-Za-z]+",

           if(CheckForLetters == true)
           {
               _newPhone = _AltPhone;
           }
           //Check the length and if longer or shorter than 10 replace.
           int PhoneLength = _newPhone.Length;
           if(PhoneLength > 10 || PhoneLength < 10)
           {
               _newPhone = _AltPhone;
           }


           return _newPhone;
       }


       //Find Root Element 
        public static XElement GetRootElement(XElement element, string localName)
        {
            XElement Element = null;

            if (element != null && element.Name.LocalName == localName)
                return element;

            if (element != null && element.HasElements)
            {
                foreach (var E in element.Elements())
                {
                    Element = GetRootElement(E, localName);
                }
            }

            return Element;
        }


        public static string WebServiceCall(string EndPoint, string _MethodName, string xmlstring)
        {
            string _xml = xmlstring;
            string _url = EndPoint;
            var log = new RPO.ActivityLog();
            try
            {
                //WebRequest webRequest = WebRequest.Create("https://amsitest.infor.com/AmsiWeb/edexweb/esite/" + WebServiceType);
                WebRequest webRequest = WebRequest.Create(EndPoint);
                HttpWebRequest httpRequest = (HttpWebRequest)webRequest;
                httpRequest.Method = "POST";
                httpRequest.ContentType = "text/xml; charset=utf-8";
                httpRequest.Headers.Add("SOAPAction: http://tempuri.org/" + _MethodName);
                Stream requestStream = httpRequest.GetRequestStream();
                //Create Stream and Complete Request             
                StreamWriter streamWriter = new StreamWriter(requestStream, Encoding.ASCII);

                streamWriter.Write(_xml);
                streamWriter.Close();
                //Get the Response    
                HttpWebResponse wr = (HttpWebResponse)httpRequest.GetResponse();
                string testresponse = wr.ToString();
                StreamReader srd = new StreamReader(wr.GetResponseStream());
                string resulXmlFromWebService = srd.ReadToEnd();
                //cakel: Testing - returning string as xml formated
                string decodedString = HttpUtility.HtmlDecode(resulXmlFromWebService);
                log.WriteLog("AMSI Webservice Call Success", "Call -" + _MethodName, 1, DisplayOnAdminPortal: false);
                return decodedString;
            }
            catch(Exception ex)
            {
                //add endpoint to log
                log.WriteLog("EFXFramework", "AMSI Webservice Call Failure -" + _MethodName + " End Point: " + EndPoint + " Exception: " + ex.InnerException.ToString() + " XML String:" + xmlstring, 1, DisplayOnAdminPortal: false);
                return null;
            }
            
            
        }


      
       //Connection String 
        private static String ConnectionString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }


    } // End Public Class AmsiRequest

} // End EfxFramework.Pms.AMSI
