﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


//cakel: BUGID00316 - Amsi Import Request

namespace EfxFramework.Pms.AMSI
{
     public static class AmsiImportRequest
    {



         public static void ProcessAmsiPropertiesImport()
         {
             DataSet AmsiDataSet = GetAmsiProperties();

             if (AmsiDataSet.Tables[0].Rows.Count > 0)
             {
                 foreach (DataRow dr in AmsiDataSet.Tables[0].Rows)
                 {
                     try
                     {
                         string PropertyPmsID = dr["PmsId"].ToString();
                         AmsiProperty.AMSI_ImportSingleProperty(PropertyPmsID);
                     }
                     catch(Exception ex)
                     {
                         throw ex;
                     }
                     
                 }
             }

         }

         public static void ProcessSingleAmsiPropertyImport(int PropertyID)
         {
             DataSet AmsiSIngleDataSet = GetSingleAmsiProperty(PropertyID);

             if (AmsiSIngleDataSet.Tables[0].Rows.Count > 0)
             {
                 foreach (DataRow dr in AmsiSIngleDataSet.Tables[0].Rows)
                 {
                     try
                     {
                         string PropertyPmsID = dr["PmsId"].ToString();
                         AmsiProperty.AMSI_ImportSingleProperty(PropertyPmsID);
                     }
                     catch(Exception ex)
                     {
                         throw ex;
                     }
                     

                 }
             }

         }

         


         public static void ProcessAmsiRentersByProperty()
         {
             DataSet AmsiDataSet = GetAmsiProperties();

             if (AmsiDataSet.Tables[0].Rows.Count > 0)
             {
                 foreach (DataRow dr in AmsiDataSet.Tables[0].Rows)
                 {
                     string PropertyPmsID = dr["PmsId"].ToString();
                     AmsiRequest.AMSI_RequestXmlForRenterImport((int)RequestType.GetAllRenters, PropertyPmsID);

                 }
             }

         }



         //Gets a List of all Amsi Properties for Importing residents, leases and properties
         public static DataSet GetAmsiProperties()
         {
             DataSet ds = new DataSet();
             using (SqlConnection conn = new SqlConnection(ConnectionString))
             {
                 SqlCommand sqlComm = new SqlCommand("AmsiGetProperties", conn);
                 sqlComm.CommandType = CommandType.StoredProcedure;
                 SqlDataAdapter da = new SqlDataAdapter();
                 da.SelectCommand = sqlComm;
                 da.Fill(ds);
             }
             return ds;
         }

         public static DataSet GetSingleAmsiProperty(int _propID)
         {
              DataSet ds = new DataSet();
             using (SqlConnection conn = new SqlConnection(ConnectionString))
             {
                 string sqlstr = "SELECT PropertyId, PropertyCode, PropertyName, PmsId, AMSIUserId, AMSIPassword, AMSIDatabase FROM Property Where PmsTypeID = 2 and PropertyID = " + _propID;
                 SqlCommand sqlComm = new SqlCommand(sqlstr, conn);
                 sqlComm.CommandType = CommandType.Text;
                 SqlDataAdapter da = new SqlDataAdapter();
                 da.SelectCommand = sqlComm;
                 da.Fill(ds);
             }
             return ds;
         }



         //Connection String 
         private static String ConnectionString
         {
             get
             {
                 return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
             }
         }


    }
}
