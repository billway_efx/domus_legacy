﻿using System;
using Mb2x.ExtensionMethods;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Text;
using EfxFramework.Scheduler;
using System.Net;
using System.Net.Mail;
using EfxFramework;



//cakel: TASK 00382 MRI API integration

namespace EfxFramework.Pms.MRI
{

    public class MriXmlDetail
    {
        public string MriXmlRequest;
        public string MriXmlReturnResponse;
    }


    public class MriExport
    {


        public static void ProcessMriPaymentExport(int PropertyID = 0)
        {
            DataSet MriDataSet = null;
            
            //This will determine if we are exporting all properties or single property
            if(PropertyID == 0)
            {
                MriDataSet = MriDataAccess.GetMriPropertiesForPaymentExport();
            }
            else
            {
                MriDataSet = MriDataAccess.MriGetSinglePropertyForPaymentExport(PropertyID);
            }

            //Make sure we are returning Data
            if(MriDataSet.Tables[0].Rows.Count > 0)
            {
                int ExportSuccessCounter = 0;
                int ExportFailedCounter = 0;

                //loop through all MRI properties
                foreach (DataRow dr in MriDataSet.Tables[0].Rows)
                {

                    string _ReturnXml = "";
                    string _ReturnResponse = "";

                    int _PropertyID = int.Parse(dr["PropertyId"].ToString());
                    string PropertyPmsID = dr["PmsId"].ToString();
                    string _endPoint = dr["MRIExportEndPoint"].ToString();



                    var _MriPostDetail = MriExport.MRIWebServicePost(PropertyPmsID);
                    

                    byte[] byteArray = Encoding.ASCII.GetBytes(_MriPostDetail.MriXmlReturnResponse);
                    using (var OutputStream = new MemoryStream(byteArray))
                    {
                        var Xml = XDocument.Load(OutputStream);

                        var Element = GetRootElement(Xml.Root, "mri_s-pmrm_paymentdetailsbypropertyid");

                        string _Error = "";
                        string MRITransactionID = "";
                        string ResidentNameID = "";
                        string _PropID = "";
                        string SiteID = "";
                        string ChargeCode = "";
                        string PaymentInitiationDatetime = "";
                        string PaymentAmount = "";
                        string PaymentType = "";
                        string ExternalTransactionNumber = ""; //this is the paymentID from RPO
                        string BatchID = "";

                        string paymentTypeID = "";

                        foreach (XElement _ReturnResp in Element.Elements())
                        {

                            _Error = TryGetElementValue(_ReturnResp, "Error");
                            MRITransactionID = TryGetElementValue(_ReturnResp, "TransactionID");
                            ResidentNameID = TryGetElementValue(_ReturnResp, "ResidentNameID");
                            _PropID = TryGetElementValue(_ReturnResp, "PropertyID");
                            SiteID = TryGetElementValue(_ReturnResp, "SiteID");
                            ChargeCode = TryGetElementValue(_ReturnResp, "ChargeCode");
                            PaymentInitiationDatetime = TryGetElementValue(_ReturnResp, "PaymentInitiationDatetime");
                            PaymentAmount = TryGetElementValue(_ReturnResp, "PaymentAmount");
                            ExternalTransactionNumber = TryGetElementValue(_ReturnResp, "ExternalTransactionNumber");
                            BatchID = _ReturnResp.Element("ExternalBatchID").Value.ToString();

                            switch(ChargeCode)
                            {
                                case "K":
                                    paymentTypeID = "1";
                                    break;
                                case "C":
                                    paymentTypeID = "2";
                                    break;
                                default:
                                    paymentTypeID = "3";
                                    break;
                            }

                            int _PayMentTypeID = int.Parse(paymentTypeID);


                            int RpoPaymentID = int.Parse(ExternalTransactionNumber);

                            if (_Error.Length < 1)
                            {
                                //Process the sucessful Export
                                string _Success = "";
                                MriDataAccess.MRIUpdatePaymentWithBatchID(RpoPaymentID, MRITransactionID, true);
                                PaymentExportSummary.Set(0, _PropertyID, decimal.Parse(PaymentAmount), true, MRITransactionID, _MriPostDetail.MriXmlRequest, _MriPostDetail.MriXmlReturnResponse, _PayMentTypeID, DateTime.Now, 1, DateTime.Now, "");
                            }
                            else
                            {
                                //Handle the Failed export
                                string Err = _Error;
                                PaymentExportSummary.Set(0, _PropertyID, decimal.Parse(PaymentAmount), false, MRITransactionID, _MriPostDetail.MriXmlRequest, _MriPostDetail.MriXmlReturnResponse, _PayMentTypeID, DateTime.Now, 1, DateTime.Now, _Error);
                            }

                        }//ENd foreach (XElement

                        string stopString = "";

                    }//end using 


                }// END --- foreach (DataRow dr in MriDataSet.Tables[0].Rows)


            } //End If MriDataSet
            else
            {
                //Do something here if there are no records to export



            }//End else If MriDataSet



        }//End ProcessMriPaymentExport()


        //creates the xml data within the Parent xml call
        public static string GetXMLMriPaymentExportData(string MriPropertyPmsID)
        {
            string PaymentType = "";
            StringBuilder output = new StringBuilder();
            XmlWriterSettings xmlsettings = new XmlWriterSettings();
            //xmlsettings.IndentChars = "\t";
            xmlsettings.Indent = true;
            xmlsettings.OmitXmlDeclaration = true;
            XmlWriter writer = XmlWriter.Create(output, xmlsettings);

            SqlDataReader reader = MriDataAccess.GetMriPaymentsByProperty(MriPropertyPmsID);
            if (reader != null)
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("mri_s-pmrm_paymentdetailsbypropertyid");

                while (reader.Read())
                {
                    writer.WriteStartElement("entry");

                    writer.WriteStartElement("ResidentNameID");
                    writer.WriteString(reader["MRINameID"].ToString());
                    writer.WriteEndElement();

                    writer.WriteStartElement("PropertyID");
                    writer.WriteString(reader["PmsId"].ToString());
                    writer.WriteEndElement();

                    writer.WriteStartElement("ChargeCode");
                    writer.WriteString(reader["ChargeCode"].ToString());
                    writer.WriteEndElement();

                    writer.WriteStartElement("PaymentInitiationDatetime");
                    writer.WriteString(reader["TransactionDate"].ToString());
                    writer.WriteEndElement();

                    writer.WriteStartElement("PaymentAmount");
                    writer.WriteString(reader["PaymentAmount"].ToString());
                    writer.WriteEndElement();

                    writer.WriteStartElement("PaymentType");
                    writer.WriteString(reader["MRIPaymentType"].ToString());
                    writer.WriteEndElement();

                    writer.WriteStartElement("PartnerName");
                    writer.WriteString(reader["PartnerName"].ToString());
                    writer.WriteEndElement();

                    writer.WriteStartElement("ExternalTransactionNumber");
                    writer.WriteString(reader["PaymentID"].ToString());
                    writer.WriteEndElement();

                    writer.WriteEndElement(); //END writer.WriteStartElement("entry");
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Flush();

                //For testing output of string 
                string TestString = output.ToString();

                return output.ToString();
            }
            else
            {
                return "";
            }


        }


        public static MriXmlDetail MRIWebServicePost(string MRIPropertyID)
        {

            MriXmlDetail MriDetail = new MriXmlDetail();

            int RPO_PropertyID = 0;
            string MRIClientID = "";
            string MRIDatabase = "";
            string MRIWebUserID = "";
            string MRIWebPw = "";
            string MRI_ExportEndPoint = "";
            string _xml = "";
            string APi_call = "mri_s-pmrm_paymentdetailsbypropertyid";

            SqlDataReader Reader = MriDataAccess.GetMriPropertyLogin(MRIPropertyID);
            Reader.Read();

            if (Reader.HasRows)
            {
                RPO_PropertyID = (int)Reader["PropertyId"];
                MRI_ExportEndPoint = Reader["MRIExportEndPoint"].ToString();
                MRIClientID = Reader["MRIClientID"].ToString();
                MRIWebUserID = Reader["MRI_APIUserName"].ToString();
                MRIWebPw = Reader["MRIWebServicePassword"].ToString();

            }

            string MriEndPointString = MRI_ExportEndPoint + "/mriapiservices/api.asp?$api=" + APi_call;

            //testing only
            //string MriEndPoint = "https://mri45pcapi.saas.mrisoftware.com/mriapiservices/api.asp?$api=mri_s-pmrm_paymentdetailsbypropertyid";
     
            string ApiUserName = MRIClientID;
            string ApiPassword = MRIWebPw;

            Uri address = new Uri(MriEndPointString);

            // Create the web request  
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "POST";
            request.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(MRIWebUserID + ":" + ApiPassword));

            //This indicates what return format we want to get - We want XML!!!!!!
            request.ContentType = "application/xml";
            request.Accept = "application/xml";


            string _xmlExport = GetXMLMriPaymentExportData(MRIPropertyID);

            MriDetail.MriXmlRequest = _xmlExport;

            // Create a byte array of the data we want to send  
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(_xmlExport.ToString());

            // Set the content length in the request headers  
            request.ContentLength = byteData.Length;
            

            // Write data  
            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }

            // try to Get response  
            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    // Get the response stream  
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    var _response = reader.ReadToEnd();
                    string ReturnResp = _response.ToString();

                    MriDetail.MriXmlReturnResponse = ReturnResp;

                    return MriDetail;
                }  
            }
                //Catch error and log it
            catch(Exception ex)
            {
                throw ex;
            }


        }


        //Find Root Element 
        public static XElement GetRootElement(XElement element, string localName)
        {
            XElement Element = null;

            if (element != null && element.Name.LocalName == localName)
                return element;

            if (element != null && element.HasElements)
            {
                foreach (var E in element.Elements())
                {
                    Element = GetRootElement(E, localName);
                }
            }

            return Element;
        }

        //Checks to make sure the element exists and if not return empty string 
        public static string TryGetElementValue(XElement parentEl, string elementName, string defaultValue = "")
        {
            var foundEl = parentEl.Element(elementName);

            if (foundEl != null)
            {
                return foundEl.Value;
            }

            return defaultValue;
        }




    } //END class MriExport
} //END Namespace
