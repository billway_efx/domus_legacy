﻿using System;
using System.Net;
using System.IO;
using System.Web;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using EfxFramework.Security.Authentication;



//cakel: TASK 00351 RealPage Integration

namespace EfxFramework.Pms.RealPage
{
    public class RealPropertyLogin
    {
        public string PropertyId;
        public string PropertyName;
        public string PmsTypeId;
        public string PmsId;
        public string RealPagePmcID;
        public string RealPageUserName;
        public string RealPagePassword;
        public string RealPageImportURL;
        public string RealPageExportUrl;
        public string RealPageInternalUser;
        public string RealPageSiteID;

    }

    public class RealPageFileIdByProperty
    {
        public string RealPageFileID;
        public string RealPageSiteName;
        public string masShortName;

    }

    public class RealPageValidateByProperty
    {
        public int TransCount;
        public decimal TotalAmount;

    }



    public class RealPageDataAccess
    {


        public static RealPageFileIdByProperty GetFileID(string PropertyPmsID)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("RealPage_GetLatestFileForDownLoad", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@RealPageSiteID", SqlDbType.NVarChar, 100).Value = PropertyPmsID;

            try
            {
                con.Open();
                using (SqlDataReader Reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    RealPageFileIdByProperty RPFile = new RealPageFileIdByProperty();
                    Reader.Read();
                    if(Reader.HasRows)
                    {
                        RPFile.RealPageFileID = Reader["RealPageFileID"].ToString();
                        RPFile.RealPageSiteName = Reader["RealPageSiteName"].ToString();
                        RPFile.masShortName = Reader["masShortName"].ToString(); 
                    }
                    return RPFile;

                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }

        }



        public static RealPropertyLogin GetLogin(string PropertyPmsID)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("RealPage_GetPropertyLogin", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PmsID", SqlDbType.NVarChar, 50).Value = PropertyPmsID;

            try
            {
                con.Open();
                using(SqlDataReader Reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    RealPropertyLogin RP = new RealPropertyLogin();
                    Reader.Read();
                    if(Reader.HasRows)
                    {
                        RP.PmsTypeId = Reader["PmsTypeId"].ToString();
                        RP.PropertyId = Reader["PropertyId"].ToString();
                        RP.PropertyName = Reader["PropertyName"].ToString();
                        RP.PmsId = Reader["PmsId"].ToString();
                        RP.RealPagePmcID = Reader["RealPagePmcID"].ToString();
                        RP.RealPageUserName = Reader["RealPageUserName"].ToString();
                        RP.RealPagePassword = Reader["RealPagePassword"].ToString();
                        RP.RealPageSiteID = Reader["RealPageSiteID"].ToString();
                        RP.RealPageImportURL = Reader["RealPageImportURL"].ToString();
                        RP.RealPageExportUrl = Reader["RealPageExportUrl"].ToString();
                        RP.RealPageInternalUser = Reader["RealPageInternalUser"].ToString();

                    }
                    return RP;

                }

            }
            catch(Exception ex)
            {
                con.Close();
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        //This is needed to send RealPage a validation summary for each property exported;
        public static RealPageValidateByProperty GetValidation(string PropertyPMSid)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("RealPageValidateExportTotalsByProperty", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropertyPmsID", SqlDbType.NVarChar, 50).Value = PropertyPMSid;

            try
            {
                con.Open();
                using (SqlDataReader Reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    RealPageValidateByProperty Val = new RealPageValidateByProperty();
                    Reader.Read();
                    if (Reader.HasRows)
                    {
                        Val.TransCount = (int)Reader["TransCount"];
                        Val.TotalAmount = (decimal)Reader["TotalAmount"];

                    }
                    return Val;
                }

            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }



        public static void RealPageInsertResidentStagetbl(string ResidentKeyID, string ResidentID, string RP_PropertyID, string RP_UnitID, string RP_LeaseID,
            string ResidentMemberID, string UseUnitAddress, string HouseHoldStatus, string ResidentBillingName, string ResidentMailingName, string ResidentPmtLatCount,
            string ResidentNSFCount, string ResidentNoChk, string ResidentInColl, string LeaseSigner, string FirstName, string LastName, string ResidentAddress1,
            string ResidentAddress2,string City,string ResidentState,string Zip,string Country,string Phone,string Phone2,
            string PhoneExt,string PhoneExt2,string EmailAddress,string FaxNumber, string cellPhone)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("RealPageInsertResidentStagetbl", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ResidentKeyID", SqlDbType.NVarChar, 100).Value = ResidentKeyID;
            com.Parameters.Add("@ResidentID", SqlDbType.NVarChar,50).Value = ResidentID;
            com.Parameters.Add("@RP_PropertyID", SqlDbType.NVarChar,100).Value = RP_PropertyID;
            com.Parameters.Add("@RP_UnitID", SqlDbType.NVarChar,20).Value = RP_UnitID;
            com.Parameters.Add("@RP_LeaseID", SqlDbType.NVarChar,100).Value = RP_LeaseID;
            com.Parameters.Add("@ResidentMemberID", SqlDbType.NVarChar,50).Value = ResidentMemberID;
            com.Parameters.Add("@UseUnitAddress", SqlDbType.NVarChar, 50).Value = UseUnitAddress;
            com.Parameters.Add("@HouseHoldStatus", SqlDbType.NVarChar, 50).Value = HouseHoldStatus;
            com.Parameters.Add("@ResidentBillingName", SqlDbType.NVarChar, 250).Value = ResidentBillingName;
            com.Parameters.Add("@ResidentMailingName", SqlDbType.NVarChar, 250).Value = ResidentMailingName;
            com.Parameters.Add("@ResidentPmtLatCount", SqlDbType.Int).Value = ResidentPmtLatCount;
            com.Parameters.Add("@ResidentNSFCount", SqlDbType.NVarChar,20).Value = ResidentNSFCount;
            com.Parameters.Add("@ResidentNoChk", SqlDbType.NVarChar, 20).Value = ResidentNoChk;
            com.Parameters.Add("@ResidentInColl", SqlDbType.NVarChar, 100).Value = ResidentInColl;
            com.Parameters.Add("@LeaseSigner", SqlDbType.NVarChar,10).Value = LeaseSigner;
            com.Parameters.Add("@FirstName", SqlDbType.NVarChar, 150).Value = FirstName;
            com.Parameters.Add("@LastName", SqlDbType.NVarChar, 150).Value = LastName;
            com.Parameters.Add("@ResidentAddress1", SqlDbType.NVarChar, 250).Value = ResidentAddress1;
            com.Parameters.Add("@ResidentAddress2", SqlDbType.NVarChar, 250).Value = ResidentAddress2;
            com.Parameters.Add("@City", SqlDbType.NVarChar, 150).Value = City;
            com.Parameters.Add("@ResidentState", SqlDbType.NVarChar, 20).Value = ResidentState;
            com.Parameters.Add("@Zip", SqlDbType.NVarChar, 20).Value = Zip;
            com.Parameters.Add("@Country", SqlDbType.NVarChar, 100).Value = Country;
            com.Parameters.Add("@Phone", SqlDbType.NVarChar, 20).Value = Phone;
            com.Parameters.Add("@Phone2", SqlDbType.NVarChar, 20).Value = Phone2;
            com.Parameters.Add("@PhoneExt", SqlDbType.NVarChar, 10).Value = PhoneExt;
            com.Parameters.Add("@PhoneExt2", SqlDbType.NVarChar, 10).Value = PhoneExt2;
            com.Parameters.Add("@EmailAddress", SqlDbType.NVarChar, 150).Value = EmailAddress;
            com.Parameters.Add("@FaxNumber", SqlDbType.NVarChar, 20).Value = FaxNumber;
            com.Parameters.Add("@CellPhone", SqlDbType.NVarChar, 20).Value = cellPhone;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();

            }
            catch(Exception ex)
            {
                con.Close();
                throw ex;
            }
        }


        public static int RealPage_InsertUpdateRenterbyKey(string ResidentKeyID,string FirstName, string LastName,string MainPhoneNumber,string MobilePhoneNumber,string FaxNumber,string PrimaryEmailAddress,
            string StreetAddress,string Unit,string City, string StateProvinceId, string PostalCode,bool EmailNotifications,bool TextNotifications,bool IsParticipating,
            string AccountCodePin,bool IsCreditCardTestRenter,bool IsCreditCardApproved,string PmsId,int PmsTypeId,bool IsActive,
            string PmsPropertyId,bool UserModified,int AcceptedPaymentTypeId)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("RealPage_InsertUpdateRenterbyKey", con);
            com.CommandType = CommandType.StoredProcedure;

            //Generate a new random password
            var TempPassword = AuthenticationTools.GenerateRandomPassword();

            //Generate a new salt and set it
            byte[] _Salt = AuthenticationTools.GenerateSalt();

            //Hash the new password and set it
            byte[] _PasswordHash = AuthenticationTools.HashPassword(TempPassword, _Salt);

            com.Parameters.Add("@ResidentKeyID", SqlDbType.NVarChar, 100).Value = ResidentKeyID;
            com.Parameters.Add("@FirstName", SqlDbType.NVarChar, 100).Value = FirstName;
            com.Parameters.Add("@LastName", SqlDbType.NVarChar, 100).Value = LastName;
            com.Parameters.Add("@MainPhoneNumber", SqlDbType.NVarChar, 100).Value = MainPhoneNumber;
            com.Parameters.Add("@MobilePhoneNumber", SqlDbType.NVarChar, 100).Value = MobilePhoneNumber;
            com.Parameters.Add("@FaxNumber", SqlDbType.NVarChar, 100).Value = FaxNumber;
            com.Parameters.Add("@PrimaryEmailAddress", SqlDbType.NVarChar, 100).Value = PrimaryEmailAddress;
            com.Parameters.Add("@StreetAddress", SqlDbType.NVarChar, 100).Value = StreetAddress;
            com.Parameters.Add("@Unit", SqlDbType.NVarChar, 100).Value = Unit;
            com.Parameters.Add("@City", SqlDbType.NVarChar, 100).Value = City;
            com.Parameters.Add("@StateProvinceAbr", SqlDbType.NVarChar, 100).Value = StateProvinceId;
            com.Parameters.Add("@PostalCode", SqlDbType.NVarChar, 100).Value = PostalCode;
            com.Parameters.Add("@EmailNotifications", SqlDbType.Bit).Value = EmailNotifications;
            com.Parameters.Add("@TextNotifications", SqlDbType.Bit).Value = TextNotifications;
            com.Parameters.Add("@IsParticipating", SqlDbType.Bit).Value = IsParticipating;
            com.Parameters.Add("@PasswordHash", SqlDbType.Binary).Value = _PasswordHash;
            com.Parameters.Add("@Salt", SqlDbType.Binary).Value = _Salt;
            com.Parameters.Add("@AccountCodePin", SqlDbType.NVarChar, 100).Value = AccountCodePin;
            com.Parameters.Add("@IsCreditCardTestRenter", SqlDbType.Bit).Value = IsCreditCardTestRenter;
            com.Parameters.Add("@IsCreditCardApproved", SqlDbType.Bit).Value = IsCreditCardApproved;
            com.Parameters.Add("@PmsId", SqlDbType.NVarChar, 100).Value = PmsId;
            com.Parameters.Add("@PmsTypeId", SqlDbType.Int).Value = PmsTypeId;
            com.Parameters.Add("@IsActive", SqlDbType.Bit).Value = IsActive;
            com.Parameters.Add("@PmsPropertyId", SqlDbType.NVarChar, 100).Value = PmsPropertyId;
            com.Parameters.Add("@UserModified", SqlDbType.NVarChar, 100).Value = UserModified;
            com.Parameters.Add("@AcceptedPaymentTypeId", SqlDbType.NVarChar, 100).Value = AcceptedPaymentTypeId;

            com.Parameters.Add("@RenterIDOutPut", SqlDbType.Int).Direction = ParameterDirection.Output;

            int RenterOutPut = 0;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                RenterOutPut = (int)com.Parameters["@RenterIDOutPut"].Value;
                con.Close();
                return RenterOutPut;
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

        }


        public static void RealPageInsertLeaseStagetbl(string ResidentKeyID, string ResidentID, string RP_PropertyID, string RP_UnitID, string RP_LeaseID, string LastRenewalDate, string LeaseBeginDate,
        string LeaseEndDate, string InactiveDate, string MoveInDate, string MoveOutDate, string NoticeOnDate, string NoticeForDate, string MonthToMonthDate, string CancelDate,
        string BillingDate, string RequiredDaysNotice, string Eviction, string InitialLeaseDate, string PaymentDueDate, string RP_LeaseStatus, string LeaseType,
        string LateDayOfMonth, string LateChargeFixedAmount, string LateChargePerDay, string LateChargePercentage, string Occupants, string BasicRent,
        string CurrentBalance, string TotalPaid)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("RealPageInsertLeaseStagetbl", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@ResidentKeyID", SqlDbType.NVarChar, 100).Value = ResidentKeyID;
            com.Parameters.Add("@ResidentID", SqlDbType.NVarChar,100).Value = ResidentID;
            com.Parameters.Add("@RP_PropertyID", SqlDbType.NVarChar).Value = RP_PropertyID;
            com.Parameters.Add("@RP_UnitID", SqlDbType.NVarChar, 100).Value = RP_UnitID;
            com.Parameters.Add("@RP_LeaseID", SqlDbType.NVarChar, 100).Value = RP_LeaseID;
            com.Parameters.Add("@LastRenewalDate", SqlDbType.NVarChar, 100).Value = LastRenewalDate;
            com.Parameters.Add("@LeaseBeginDate", SqlDbType.NVarChar, 100).Value = LeaseBeginDate;
            com.Parameters.Add("@LeaseEndDate", SqlDbType.NVarChar, 100).Value = LeaseEndDate;
            com.Parameters.Add("@InactiveDate", SqlDbType.NVarChar, 100).Value = InactiveDate;
            com.Parameters.Add("@MoveInDate", SqlDbType.NVarChar, 100).Value = MoveInDate;
            com.Parameters.Add("@MoveOutDate", SqlDbType.NVarChar, 100).Value = MoveOutDate;
            com.Parameters.Add("@NoticeOnDate", SqlDbType.NVarChar, 100).Value = NoticeOnDate;
            com.Parameters.Add("@NoticeForDate", SqlDbType.NVarChar, 100).Value = NoticeForDate;
            com.Parameters.Add("@MonthToMonthDate", SqlDbType.NVarChar, 100).Value = MonthToMonthDate;
            com.Parameters.Add("@CancelDate", SqlDbType.NVarChar, 100).Value = CancelDate;
            com.Parameters.Add("@BillingDate", SqlDbType.NVarChar, 100).Value = BillingDate;
            com.Parameters.Add("@RequiredDaysNotice", SqlDbType.NVarChar, 100).Value = RequiredDaysNotice;
            com.Parameters.Add("@Eviction", SqlDbType.NVarChar, 5).Value = Eviction;
            com.Parameters.Add("@InitialLeaseDate", SqlDbType.NVarChar, 100).Value = InitialLeaseDate;
            com.Parameters.Add("@PaymentDueDate", SqlDbType.NVarChar, 100).Value = PaymentDueDate;
            com.Parameters.Add("@RP_LeaseStatus", SqlDbType.NVarChar, 50).Value = RP_LeaseStatus;
            com.Parameters.Add("@LeaseType", SqlDbType.NVarChar, 50).Value = LeaseType;
            com.Parameters.Add("@LateDayOfMonth", SqlDbType.NVarChar, 100).Value = LateDayOfMonth;
            com.Parameters.Add("@LateChargeFixedAmount", SqlDbType.NVarChar, 100).Value = LateChargeFixedAmount;
            com.Parameters.Add("@LateChargePercentage", SqlDbType.NVarChar, 100).Value = LateChargePercentage;
            com.Parameters.Add("@LateChargePerDay", SqlDbType.Money).Value = LateChargePerDay;
            com.Parameters.Add("@Occupants", SqlDbType.NVarChar, 100).Value = Occupants;
            com.Parameters.Add("@BasicRent", SqlDbType.NVarChar, 100).Value = BasicRent;
            com.Parameters.Add("@CurrentBalance", SqlDbType.NVarChar, 100).Value = CurrentBalance;
            com.Parameters.Add("@TotalPaid", SqlDbType.NVarChar, 100).Value = TotalPaid;


            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            catch(Exception ex)
            {
                con.Close();
                throw ex;
            }

        }

        public static void RealPageRemoveLeaseChargesByProperty(string PropertyPmsID)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("RealPageRemoveLeaseChargesByProperty", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropertySiteID", SqlDbType.NVarChar, 100).Value = PropertyPmsID;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

        }


        public static void RealPageInsertOpenTransStagetbl(string ResidentKeyID, string LeaseFeeType, string RP_LeaseID, string ResidentID, string RP_PropertyID, string RP_UnitID,
            string TransactionCategory, string TransactionName, string TransactionDesc, string TransactionAmount, string TransactionCodeDescription,
            string TransactionBatchID, string TransactivityID, string TransactionDate, string SubJournal, string GroupToRent)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("RealPageInsertOpenTransStagetbl", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@ResidentKeyID", SqlDbType.NVarChar, 150).Value = ResidentKeyID;
            com.Parameters.Add("@LeaseFeeType", SqlDbType.NVarChar, 100).Value = LeaseFeeType;
            com.Parameters.Add("@RP_LeaseID", SqlDbType.NVarChar,150).Value = RP_LeaseID;
            com.Parameters.Add("@ResidentID", SqlDbType.NVarChar,100).Value = ResidentID;
            com.Parameters.Add("@RP_PropertyID", SqlDbType.NVarChar,100).Value = RP_PropertyID;
            com.Parameters.Add("@RP_UnitID", SqlDbType.NVarChar,100).Value = RP_UnitID;
            com.Parameters.Add("@TransactionCategory", SqlDbType.NVarChar, 100).Value = TransactionCategory;
            com.Parameters.Add("@TransactionName", SqlDbType.NVarChar, 100).Value = TransactionName;
            com.Parameters.Add("@TransactionDesc", SqlDbType.NVarChar, 100).Value = TransactionDesc;
            com.Parameters.Add("@TransactionAmount", SqlDbType.Money).Value = TransactionAmount;
            com.Parameters.Add("@TransactionCodeDescription", SqlDbType.NVarChar, 100).Value = TransactionCodeDescription;
            com.Parameters.Add("@TransactionBatchID", SqlDbType.NVarChar, 100).Value = TransactionBatchID;
            com.Parameters.Add("@TransactivityID", SqlDbType.NVarChar,100).Value = TransactivityID;
            com.Parameters.Add("@TransactionDate", SqlDbType.NVarChar, 100).Value = TransactionDate;
            com.Parameters.Add("@SubJournal", SqlDbType.NVarChar, 100).Value = SubJournal;
            com.Parameters.Add("@GroupToRent", SqlDbType.NVarChar,20).Value = GroupToRent;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            catch(Exception ex)
            {
                con.Close();
                throw ex;
            }
        }

        public static void RealPageInsertFileList(string RealPageSiteID, string RealPageSiteName, int RealPageFileID, string masShortName, string logDate, 
            string FileFormat, string RealPageGroup, string fileinstancelabel, string RealPageFileName)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("RealPageInsertFileList", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@RealPageSiteID", SqlDbType.NVarChar, 150).Value = RealPageSiteID;
            com.Parameters.Add("@RealPageSiteName", SqlDbType.NVarChar, 150).Value = RealPageSiteName;
            com.Parameters.Add("@RealPageFileID", SqlDbType.Int).Value = RealPageFileID;
            com.Parameters.Add("@masShortName", SqlDbType.NVarChar, 150).Value = masShortName;
            com.Parameters.Add("@logDate", SqlDbType.NVarChar, 150).Value = logDate;
            com.Parameters.Add("@FileFormat", SqlDbType.NVarChar, 150).Value = FileFormat;
            com.Parameters.Add("@RealPageGroup", SqlDbType.NVarChar, 150).Value = RealPageGroup;
            com.Parameters.Add("@fileinstancelabel", SqlDbType.NVarChar, 150).Value = fileinstancelabel;
            com.Parameters.Add("@RealPageFileName", SqlDbType.NVarChar, 150).Value = RealPageFileName;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            catch(Exception ex)
            {
                con.Close();
                throw ex;
            }

        }

        //this will update the RPO lease by Resident Key from Real Page
        public static void RealPage_UpdateRenterLeaseByRPkey(string ResidentKeyID)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("RealPage_UpdateRenterLeaseByRPkey", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ResidentKeyID", SqlDbType.NVarChar, 100).Value = ResidentKeyID;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

        }

        //public static void RealPageUpdateRenterRpoLeaseFee(string ResidentKeyID, string ChargeCode)
        //{
        //    SqlConnection con = new SqlConnection(ConnectionString);
        //    SqlCommand com = new SqlCommand("RealPageUpdateRenterRpoLeaseFee", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.Parameters.Add("@ResidentKeyID", SqlDbType.NVarChar, 100).Value = ResidentKeyID;
        //    com.Parameters.Add("@RPOChargeCode", SqlDbType.NVarChar, 100).Value = ChargeCode;

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();
        //        con.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        con.Close();
        //        throw ex;
        //    }

        //}


        public static void RealPageUpdateLeaseFeeByProperty(string PropertyPMSID)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("RealPageUpdateLeaseFeeByProperty", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropertyPmsID", SqlDbType.NVarChar, 150).Value = PropertyPMSID;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

        }




        public static void RealPage_AddLeaseFeesToProperyByPmsID(string PropertyPmsID, string FeeName, decimal FeeAmount, bool isActive, string ChargeCode)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("RealPage_AddLeaseFeesToProperyByPmsID", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@PropertyPmsId", SqlDbType.NVarChar, 100).Value = PropertyPmsID;
            com.Parameters.Add("@FeeName", SqlDbType.NVarChar, 100).Value = FeeName;
            com.Parameters.Add("@DefaultFeeAmount", SqlDbType.Money).Value = FeeAmount;
            com.Parameters.Add("@IsActive", SqlDbType.Bit).Value = isActive;
            com.Parameters.Add("@ChargeCode", SqlDbType.NVarChar, 100).Value = ChargeCode;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

        }

        public static void RealPage_RemoveFeeByResidentKeyID(string ResidentKeyID)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("RealPage_RemoveFeeByResidentKeyID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ResidentKeyID", SqlDbType.NVarChar, 100).Value = ResidentKeyID;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }


        }


        public static SqlDataReader RealPage_GetUnprocessedPaymentsByProperty(string PropertyPMSid)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("RealPageGetAllUnprocessedPaymentsByProperty", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropertyPmsID", SqlDbType.NVarChar, 100).Value = PropertyPMSid;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

        }

        public static SqlDataReader RealPageGetPropertiesForPaymentExport()
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("RealPageGetPropertiesForPaymentExport", con);
            com.CommandType = CommandType.StoredProcedure;
           

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

        }



        public static void RealPageUpdateRPOPaymentWithBatchID(int PaymentID, string BatchNum, bool isPmsProcess)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("AmsiUpdatePaymentWithBatchID", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@PaymentID", SqlDbType.Int).Value = PaymentID;
            com.Parameters.Add("@BatchID", SqlDbType.NVarChar, 100).Value = BatchNum;
            com.Parameters.Add("@IsPmsProcessed", SqlDbType.Bit).Value = isPmsProcess;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

        }

        //Gets a List of all RealPage Properties for Importing residents and leases
        public static DataSet RealPage_GetAllPropertiesForImport()
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                SqlCommand sqlComm = new SqlCommand("RealPageGetProperties", conn);
                sqlComm.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;
                da.Fill(ds);
            }
            return ds;
        }


        //Gets a List of all RealPage Properties for Exporting Payments
        public static DataSet RealPage_GetAllPropertiesForExport()
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                SqlCommand sqlComm = new SqlCommand("RealPageGetPropertiesForPaymentExport", conn);
                sqlComm.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;
                da.Fill(ds);
            }
            return ds;
        }


        //Connection String 
        private static String ConnectionString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }


    }//END RealPageDataAccess
}
