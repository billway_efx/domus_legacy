﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Text;
using EfxFramework.Scheduler;
using System.Net;
using System.Net.Mail;
using EfxFramework;

namespace EfxFramework.Pms.RealPage
{

    class RealPageRequest
    {

        public static string WebServiceCall(string _MethodName, string _RealPagePropertyPmsID = "", string _FileID = "")
        {
            //cakel: 00351 rev3 added logging around request
            var log = new RPO.ActivityLog();

            string methodName = _MethodName;

            string _xml = "";
            switch (methodName)
            {
                case "DownloadListGroup":
                    _xml = DownloadListGroup(_RealPagePropertyPmsID);
                    break;
                case "DownloadBin":
                    _xml = DownloadBin(_RealPagePropertyPmsID, _FileID);
                    break;
                case "DownloadLen":
                    _xml = DownloadLen();
                    break;

            }

            //Sample String
            //https://onesite.realpage.com/webservices/generalServices/file.asmx?op=DownloadListGroup

            WebRequest webRequest = WebRequest.Create("https://onesite.realpage.com/webservices/generalServices/file.asmx");
            HttpWebRequest httpRequest = (HttpWebRequest)webRequest;
            httpRequest.Method = "POST";
            httpRequest.ContentType = "text/xml; charset=utf-8";
            httpRequest.ContentLength = _xml.Length;
            httpRequest.Headers.Add("SOAPAction: http://realpage.com/webservices/" + methodName);
            Stream requestStream = httpRequest.GetRequestStream();
            //Create Stream and Complete Request             
            StreamWriter streamWriter = new StreamWriter(requestStream, Encoding.ASCII);

            streamWriter.Write(_xml);
            streamWriter.Close();


            //Get the Response    
            try
            {
                HttpWebResponse wr = (HttpWebResponse)httpRequest.GetResponse();
                string testresponse = wr.ToString();
                StreamReader srd = new StreamReader(wr.GetResponseStream());
                string resulXmlFromWebService = srd.ReadToEnd();
                //cakel: Testing - returning string as xml formated
                string decodedString = HttpUtility.HtmlDecode(resulXmlFromWebService);
                wr.Close();
                return decodedString;
                
            }
            catch(Exception ex)
            {
                string WebRequestError = ex.ToString();
                //cakel: 00351 rev3 added logging around request
                Console.WriteLine("RealPage - Error for Web Request " + _MethodName + " " + WebRequestError);
                log.WriteLog("RealPage-Request", "The following request [" + _MethodName + "]returned an Error" + WebRequestError, 1, "-999", false);
                return "Error";
            }

        }


        public static string WebserviceCall_Export(string _xml, string _RealPagePropertyPmsID, string EndPoint)
        {
            

            WebRequest webRequest = WebRequest.Create(EndPoint);
            HttpWebRequest httpRequest = (HttpWebRequest)webRequest;
            httpRequest.Method = "POST";
            httpRequest.ContentType = "text/xml; charset=utf-8";
            httpRequest.ContentLength = _xml.Length;
            httpRequest.Headers.Add("SOAPAction: http://tempuri.org/Upload");
            Stream requestStream = httpRequest.GetRequestStream();
            //Create Stream and Complete Request             
            StreamWriter streamWriter = new StreamWriter(requestStream, Encoding.ASCII);

            streamWriter.Write(_xml);
            streamWriter.Close();


            //Get the Response    
            try
            {
                HttpWebResponse wr = (HttpWebResponse)httpRequest.GetResponse();
                string testresponse = wr.ToString();
                StreamReader srd = new StreamReader(wr.GetResponseStream());
                string resulXmlFromWebService = srd.ReadToEnd();
                //cakel: Testing - returning string as xml formated
                string decodedString = HttpUtility.HtmlDecode(resulXmlFromWebService);
                wr.Close();
                return decodedString;

            }
            catch (Exception ex)
            {
                string WebRequestError = ex.ToString();
                return "Error";
            }

        }





        public static string DownloadListGroup(string PropertyPmsID)
        {
            RealPropertyLogin RPLogin = new RealPropertyLogin();
            RPLogin = RealPageDataAccess.GetLogin(PropertyPmsID);

            string UserName = RPLogin.RealPageUserName;
            string Pw = RPLogin.RealPagePassword;
            string SiteID = RPLogin.RealPageSiteID;
            string PmcID = RPLogin.RealPagePmcID;
            string InternalUser = RPLogin.RealPageInternalUser;

            string myString =
            @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
              <soap:Header>
                <UserAuthInfo xmlns=""http://realpage.com/webservices"">
                  <UserName>##UserName##</UserName>
                  <Password>##Password##</Password>
                  <SiteID>##SiteID##</SiteID>
                  <PmcID>##PmcID##</PmcID>
                  <InternalUser>##InternalUser##</InternalUser>
                </UserAuthInfo>
              </soap:Header>
                 <soap:Body>
                    <DownloadListGroup xmlns=""http://realpage.com/webservices"">
                      <GroupName>##InternalUser##</GroupName>
                      <Range>New</Range>
                      <Allsites>0</Allsites>
                      <xmlerrors>1</xmlerrors>
                    </DownloadListGroup>
                  </soap:Body>
            </soap:Envelope>";

            myString = myString.Replace("##UserName##", UserName);
            myString = myString.Replace("##Password##", Pw);
            myString = myString.Replace("##SiteID##", SiteID);
            myString = myString.Replace("##PmcID##", PmcID);
            myString = myString.Replace("##InternalUser##", InternalUser);

            return myString;
        }



        public static string DownloadLen()
        {
            string myString =
            @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
              <soap:Header>
                <UserAuthInfo xmlns=""http://realpage.com/webservices"">
                  <UserName>C_35046166-828D</UserName>
                  <Password>ST5y3p</Password>
                  <SiteID>3790036</SiteID>
                  <PmcID>3790035</PmcID>
                  <InternalUser>EFXFS</InternalUser>
                </UserAuthInfo>
              </soap:Header>
               <soap:Body>
                <DownloadLen xmlns=""http://realpage.com/webservices"">
                  <Interface>1</Interface>
                  <Format>xml</Format>
                  <FileID>409232</FileID>
                </DownloadLen>
              </soap:Body>
            </soap:Envelope>";

            return myString;

        }

        public static string DownloadBin(string PropertyPmsID, string FileID)
        {

            RealPropertyLogin RPLogin = new RealPropertyLogin();
            RPLogin = RealPageDataAccess.GetLogin(PropertyPmsID);

            string UserName = RPLogin.RealPageUserName;
            string Pw = RPLogin.RealPagePassword;
            string SiteID = RPLogin.RealPageSiteID;
            string PmcID = RPLogin.RealPagePmcID;
            string InternalUser = RPLogin.RealPageInternalUser;

            string myString =
            @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
              <soap:Header>
                <UserAuthInfo xmlns=""http://realpage.com/webservices"">
                  <UserName>##UserName##</UserName>
                  <Password>##Password##</Password>
                  <SiteID>##SiteID##</SiteID>
                  <PmcID>##PmcID##</PmcID>
                  <InternalUser>##InternalUser##</InternalUser>
                </UserAuthInfo>
              </soap:Header>
                  <soap:Body>
                    <DownloadBin xmlns=""http://realpage.com/webservices"">
                      <Interface></Interface>
                      <Format></Format>
                      <FileID>##FileID##</FileID>
                      <markdownloaded>false</markdownloaded>
                    </DownloadBin>
                  </soap:Body>
            </soap:Envelope>";

            myString = myString.Replace("##UserName##", UserName);
            myString = myString.Replace("##Password##", Pw);
            myString = myString.Replace("##SiteID##", SiteID);
            myString = myString.Replace("##PmcID##", PmcID);
            myString = myString.Replace("##InternalUser##", InternalUser);
            myString = myString.Replace("##FileID##", FileID);

            return myString;
        }





    }
}
