﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using static System.Xml.XmlWriter;

namespace EfxFramework.Pms.RealPage
{
    public class RealPageExport
    {
        public static void RealPageProcessExportAllProperties()
        {
            Console.WriteLine(@"RealPage - Start Export Payments for All Properties");
            var realPageDataSet = RealPageDataAccess.RealPage_GetAllPropertiesForExport();

            if (realPageDataSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in realPageDataSet.Tables[0].Rows)
                {
                    var propertyPmsId = dr["PmsId"].ToString();
                    var endPoint = dr["RealPageExportUrl"].ToString();
                    var xmlExportString = RealPageExport.RealPageGetXmlPaymentDataByProperty(propertyPmsId);

                    RealPageExportByProperty(propertyPmsId, xmlExportString, endPoint);
                    RealPageExportSummary(xmlExportString);
                }
            }
            Console.WriteLine(@"RealPage - End Export Payments for All Properties");
        }

        public static void RealPageProcessExportSingleProperty(int propertyId)
        {
            Console.WriteLine(string.Concat("RealPage - Start Export Payments for PropertyID = ", propertyId));

            var p = new Property(propertyId);
            var propertyPmsId = p.PmsId;
            var endPoint = p.RealPageExportURL;

            var xmlExportString = RealPageGetXmlPaymentDataByProperty(propertyPmsId);

            if (xmlExportString != "")
            {
                RealPageExportByProperty(propertyPmsId, xmlExportString, endPoint);
                RealPageExportSummary(xmlExportString);
                Console.WriteLine(string.Concat("RealPage - End Export Payments for PropertyID = ", propertyId));
            }
            else
            {
                Console.Write(string.Concat("RealPage - No Export data for PropertyId = ", propertyId));
            }
        }

        public static string RealPageExportByProperty(string propertyPmsId, string _xml, string rpEndPoint)
        {
            if (propertyPmsId == null) throw new ArgumentNullException(nameof(propertyPmsId));

            var xDocument = new XmlDocument();
            var doc = new XmlDocument();
            doc.LoadXml(_xml);

            var xmlData = Encoding.ASCII.GetBytes(doc.OuterXml);
            var wc = new WebClient();
            wc.Headers.Add("Content-Type", "text/xml");
            byte[] bResp;
            bResp = wc.UploadData(rpEndPoint, xmlData);
            var resp = Encoding.ASCII.GetString(bResp);

            xDocument.LoadXml(resp);
            var respText = xDocument.InnerText;

            return respText;
        }

        public static string RealPageGetXmlPaymentDataByProperty(string propertyPmsId)
        {
            Console.WriteLine(string.Concat("RealPage - Get Payments for Export by Property PMS ID = ", propertyPmsId));
            var rp = RealPageDataAccess.GetLogin(propertyPmsId);
            var propertyId = rp.PropertyId;
            var batchNum = CreateBatchNumber(propertyId);
            var userName = rp.RealPageUserName;
            var password = rp.RealPagePassword;
            var siteId = propertyPmsId;

            var val = RealPageDataAccess.GetValidation(propertyPmsId);

            var output = new StringBuilder();
            var xmlSettings = new XmlWriterSettings();
            xmlSettings.Indent = true;
            xmlSettings.OmitXmlDeclaration = true;
            var writer = Create(output, xmlSettings);

            var reader = RealPageDataAccess.RealPage_GetUnprocessedPaymentsByProperty(propertyPmsId);
            if (reader.HasRows)
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("root");
                writer.WriteStartElement("Request");

                writer.WriteStartElement("Authorization");
                writer.WriteAttributeString("UserName", userName);
                writer.WriteAttributeString("Password", password);
                writer.WriteAttributeString("SiteID", siteId);
                writer.WriteEndElement();//Authorization

                writer.WriteStartElement("Parameters");
                writer.WriteAttributeString("Interface", "TransactionsConvergent");
                writer.WriteAttributeString("ShortDesc", "RentPaidOnline");
                writer.WriteAttributeString("NotificationEmail", "support@rentpaidonline.com");
                writer.WriteAttributeString("NotificationURL", "http//www.RentPaidOnline.com");
                writer.WriteAttributeString("UsePropDate", "Y");
                writer.WriteAttributeString("BatchNumber", batchNum);
                writer.WriteEndElement();//Parameters

                writer.WriteStartElement("FileContents");
                writer.WriteStartElement("TransactionList");

                writer.WriteStartElement("Validation");
                writer.WriteStartElement("Audit");
                writer.WriteAttributeString("Node", "Transaction");
                writer.WriteAttributeString("Count", val.TransCount.ToString());
                writer.WriteAttributeString("Sum", val.TotalAmount.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();//Audit
                writer.WriteEndElement();//Validation

                while (reader.Read())
                {
                    writer.WriteStartElement("Transaction");
                    writer.WriteAttributeString("SiteID", reader["SiteID"].ToString());
                    writer.WriteAttributeString("ReshID", reader["ResidentID"].ToString());
                    writer.WriteAttributeString("LeaID", reader["RP_LeaseID"].ToString());
                    writer.WriteAttributeString("UnitID", reader["RP_UnitID"].ToString());
                    writer.WriteAttributeString("PropNo", "");
                    writer.WriteAttributeString("TransAmt", reader["RentAmount"].ToString());
                    writer.WriteAttributeString("TransDate", reader["TransactionDate"].ToString());
                    writer.WriteAttributeString("CatCode", reader["RP_CategoryCode"].ToString());
                    writer.WriteAttributeString("TransName", reader["TransName"].ToString());
                    writer.WriteAttributeString("TransDesc", reader["TransDesc"].ToString());
                    writer.WriteAttributeString("FiscalPeriodEnd", "N");
                    writer.WriteAttributeString("TransactionBatchID", reader["PaymentId"].ToString());
                    writer.WriteEndElement();//Transaction
                }
                writer.WriteEndElement();//TransactionList
                writer.WriteEndElement();//FileContents


                writer.WriteEndElement();//Request
                writer.WriteEndElement();//root

                writer.WriteEndDocument();
                writer.Flush();

                var testString = output.ToString();

                return testString;
            }
            return "";
        }


        //This will create a new batch number and is driven by day and propertyid so that each daily batch is unique by property
        private static string CreateBatchNumber(string rpoPropertyId)
        {
            var year = DateTime.Now.Year.ToString();
            var month = DateTime.Now.Month.ToString();
            var day = DateTime.Now.Day.ToString();
            var hourMin = DateTime.Now.Hour + DateTime.Now.Minute.ToString();
            return year + month + day + hourMin + '-' + rpoPropertyId;
        }
        public static void RealPageExportSummary(string _xml)
        {
            const string xmlRoot = "Request";
            var byteArray = Encoding.ASCII.GetBytes(_xml);

            using (var outputStream = new MemoryStream(byteArray))
            {

                var xDocument = XDocument.Load(outputStream);
                var element = RealPageImport.GetRootElement(xDocument.Root, xmlRoot);
                var xmlError = RealPageImport.GetRootElement(xDocument.Root, "Errors");

                if (xmlError != null) return; 

                var batchNumber = "";
                foreach (var paymentList in element.Elements())
                {
                    var node1 = paymentList.Name.LocalName;

                    switch (node1)
                    {
                        case "Parameters":
                            batchNumber = paymentList.Attribute("BatchNumber")?.Value.ToString();
                            break;
                        case "FileContents":
                        {
                            foreach (var fileContents in paymentList.Elements())
                            {
                                var node2 = fileContents.Name.LocalName;

                                if (node2 != "TransactionList") continue;

                                foreach (var transactionList in fileContents.Elements())
                                {
                                    var node3 = transactionList.Name.LocalName;

                                    if (node3 != "Transaction") continue;

                                    var rpoPaymentId = transactionList.Attribute("TransactionBatchID")?.Value;
                                    var batchNum = batchNumber;
                                    var paymentId = int.Parse(rpoPaymentId);
                                    RealPageDataAccess.RealPageUpdateRPOPaymentWithBatchID(paymentId, batchNum, true);
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
    }
}
