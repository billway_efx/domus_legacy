﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Text;
using EfxFramework.Scheduler;
using System.Net;
using System.Net.Mail;
using EfxFramework;


//cakel: 00351 RealPage
namespace EfxFramework.Pms.RealPage
{
    public class RealPageImport
    {


        public static void ProcessAllRealPagePropertiesImport()
        {
            DataSet RealPageDataSet = RealPageDataAccess.RealPage_GetAllPropertiesForImport();

            //cakel: Added log
            var log = new RPO.ActivityLog();

            if (RealPageDataSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in RealPageDataSet.Tables[0].Rows)
                {

                        string PropertyPmsID = dr["PmsId"].ToString();
                        
                        //1. We import file list to see what is available
                        ImportFileList(PropertyPmsID);

                        //2. Get latest file
                        RealPageFileIdByProperty RP_File = new RealPageFileIdByProperty();
                        RP_File = RealPageDataAccess.GetFileID(PropertyPmsID);

                        //3. Start Import
                        RealPageImport.ImportRealPageResidentDetail(PropertyPmsID, RP_File.RealPageFileID);

                        //4.After Import Update all Residents leases
                        RealPageDataAccess.RealPageUpdateLeaseFeeByProperty(PropertyPmsID);

                }
            }

        }


        public static void ProcessRealPageSingleProperty(int PropertyID)
        {
            Property p = new Property(PropertyID);
            string PropertyPmsID = p.PmsId;

            //1. We import file list to see what is available
            ImportFileList(PropertyPmsID);

            //2. Get latest file
            RealPageFileIdByProperty RP_File = new RealPageFileIdByProperty();
            RP_File = RealPageDataAccess.GetFileID(PropertyPmsID);

            //3. Start Import
            RealPageImport.ImportRealPageResidentDetail(PropertyPmsID, RP_File.RealPageFileID);

            //4.After Import Update all Residents leases
            RealPageDataAccess.RealPageUpdateLeaseFeeByProperty(PropertyPmsID);



        }



        //RealPage returns xml file as Binary64 and we need to convert back to Xml file to read
        public static string ConvertBinary64ToXml(string _Binary64)
        {
            try
            {
                System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
                System.Text.Decoder utf8Decode = encoder.GetDecoder();

                byte[] todecode_byte = Convert.FromBase64String(_Binary64);
                int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                char[] decoded_char = new char[charCount];
                utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                string result = new String(decoded_char);

                return result;

            }
            catch (Exception e)
            {
                //cakel: Added variable
                throw e;
            }

        }


        //This code will import all available files by property to download
        public static void ImportFileList(string _PropertyIDPmsID)
        {
            Console.WriteLine("RealPage - Start Import file list for propertyID = " + _PropertyIDPmsID);
            string _xml = RealPageRequest.WebServiceCall("DownloadListGroup", _PropertyIDPmsID);

            string _xmlRoot = "Response";

            byte[] byteArray = Encoding.ASCII.GetBytes(_xml);


            //cakel: 01/19/2016
            if(_xml != "Error")
            {

            using (var OutputStream = new MemoryStream(byteArray))
            {
                var Xml = XDocument.Load(OutputStream);
                var Element = GetRootElement(Xml.Root, _xmlRoot);


                foreach (XElement DocList in Element.Elements())
                {
                    string node1 = DocList.Name.LocalName;

                    foreach(XElement Doc in DocList.Elements())
                    {
                        string node2 = Doc.Name.LocalName;

                        if(node2 == "Validation")
                        {
                            foreach(XElement Validate in Doc.Elements())
                            {
                                string Audit = Validate.Attribute("Count").Value.ToString();
                            }

                        }

                        if(node2 == "File")
                        {
                            string SiteID = Doc.Attribute("siteID").Value.ToString();
                            string SiteName = Doc.Attribute("siteName").Value.ToString();
                            string ShortName = Doc.Attribute("masShortName").Value.ToString();
                            int fileId = (int)Doc.Attribute("fileID");
                            string LogDate = Doc.Attribute("logDate").Value.ToString();
                            string FileFormat = Doc.Attribute("FileFormat").Value.ToString();
                            string Group = Doc.Attribute("Group").Value.ToString();
                            string FileInstance = Doc.Attribute("fileinstancelabel").Value.ToString();
                            string FileName = Doc.Attribute("Filename").Value.ToString();

                            RealPageDataAccess.RealPageInsertFileList(SiteID, SiteName, fileId, ShortName, LogDate, FileFormat, Group, FileInstance, FileName);

                        }

                    }

                }

            }

            }

            Console.WriteLine("RealPage - End Import file list for propertyID = " + _PropertyIDPmsID);

        }


        public static void ImportRealPageResidentDetail(string PropertyPmsID, string FileID)
        {
            string StopString = "";

            Console.WriteLine("RealPage - Start Resident Import for Property PMS iD =" + PropertyPmsID);
            
            string _xml = RealPageRequest.WebServiceCall("DownloadBin", PropertyPmsID, FileID);

            string _xmlRoot = "DownloadBinResult";
            string _result = "";

            string _xmlRoot2 = "ConvergentBilling";

            if (_xml == "Errors")
            {
                return;
            }

            byte[] byteArray = Encoding.ASCII.GetBytes(_xml);

            using (var OutputStream = new MemoryStream(byteArray))
            {
                //cakel: Added log
                var log = new RPO.ActivityLog();

                var Xml = XDocument.Load(OutputStream);
                var Element = GetRootElement(Xml.Root, _xmlRoot);

                string XmlString = Xml.ToString();

                if (Element != null)
                {
                    var XmlResponseString = Element.Value;
                    try
                    {
                        _result = ConvertBinary64ToXml(XmlResponseString);
                    }
                    catch(Exception e)
                    {
                        //cakel: Added event log
                        log.WriteLog("RealPageImport", "ConvertBinary Failure" + e.ToString(), 0, "", false);

                        Console.WriteLine("RealPage - Failed to convert binary file for Property PMS iD =" + PropertyPmsID);
                        //return from function to not continue with import if file does not convert
                        return;
                    }
                    
                }

            }

            byte[] RespByteArray = Encoding.ASCII.GetBytes(_result);
            using (var OutputStream = new MemoryStream(RespByteArray))
            {
                //cakel: Added log
                var log = new RPO.ActivityLog();

                var XmlResp = XDocument.Load(OutputStream);

                var RespElement = GetRootElement(XmlResp.Root, _xmlRoot2);
                


                foreach (XElement ConvBill in RespElement.Elements())
                {
                    string _node1 = ConvBill.Name.LocalName;

                    foreach(XElement node2 in ConvBill.Elements())
                    {
                        string node3 = node2.Name.LocalName;

                        if (node3 == "Resident")
                        {
                            
                            string ResidentID = node2.Attribute("ReshID").Value.ToString();
                            string RP_PropertyID = node2.Attribute("SiteID").Value.ToString();
                            string RP_UnitID = node2.Attribute("UnitID").Value.ToString();
                            string RP_LeaseID = node2.Attribute("LeaID").Value.ToString();
                            string ResidentMemberID = node2.Attribute("ResmID").Value.ToString();
                            string UseUnitAddress = node2.Attribute("UseUnitAddress").Value.ToString();
                            string HouseHoldStatus = node2.Attribute("Status").Value.ToString();
                            string ResidentBillingName = node2.Attribute("BillName").Value.ToString();
                            string ResidentMailingName = node2.Attribute("MailName").Value.ToString();
                            string ResidentPmtLatCount = node2.Attribute("LatePmtCnt").Value.ToString();
                            string ResidentNSFCount = node2.Attribute("NSFCnt").Value.ToString();
                            string ResidentNoChk = node2.Attribute("NoChk").Value.ToString();
                            string ResidentInColl = node2.Attribute("InColl").Value.ToString();
                            string LeaseSigner = node2.Attribute("LeaseSigner").Value.ToString();
                            string FirstName = node2.Attribute("FirstName").Value.ToString();
                            string LastName = node2.Attribute("LastName").Value.ToString();

                            string ResidentKeyID = RP_PropertyID + "-" + ResidentID + "-" + RP_LeaseID;

                            string ResidentAddress1 = "";
                            string ResidentAddress2 = "";
                            string City = "";
                            string ResidentState = "";
                            string Zip = "";
                            string Country = "";
                            string Phone = "";
                            string Phone2 = "";
                            string PhoneExt = "";
                            string PhoneExt2 = "";
                            string EmailAddress = "";
                            string FaxNumber = "";
                            string cellPhone = "";


                            foreach(XElement _Address in node2.Elements())
                            {

                                if (_Address.Name.LocalName == "RegularAddress")
                                {

                                    ResidentAddress1 = _Address.Attribute("Adr1").Value.ToString();
                                    ResidentAddress2 = _Address.Attribute("Adr2").Value.ToString();
                                    City = _Address.Attribute("City").Value.ToString();
                                    ResidentState = _Address.Attribute("State").Value.ToString();
                                    Zip = _Address.Attribute("Zip").Value.ToString();
                                    Country = _Address.Attribute("Country").Value.ToString();
                                    Phone = CleanPhoneNumber(_Address.Attribute("Phone1").Value.ToString());
                                    Phone2 = CleanPhoneNumber(_Address.Attribute("Phone2").Value.ToString());
                                    PhoneExt = CleanPhoneNumber(_Address.Attribute("Ext1").Value.ToString());
                                    PhoneExt2 = _Address.Attribute("Ext2").Value.ToString();
                                    EmailAddress = System.Guid.NewGuid().ToString() + "@unknown.com"; //Do not take in email address and use Guid to prevent errors
                                    cellPhone = CleanPhoneNumber(_Address.Attribute("Cell").Value.ToString());

                                    if(ResidentState == "NA")
                                    {
                                        ResidentState = "AK";
                                    }

                                }
                                        
                            }

                            try
                            {
                                ////save the resident data to table here
                            RealPageDataAccess.RealPageInsertResidentStagetbl(ResidentKeyID, ResidentID, RP_PropertyID, RP_UnitID, RP_LeaseID, ResidentMemberID, UseUnitAddress, HouseHoldStatus,
                                ResidentBillingName, ResidentMailingName, ResidentPmtLatCount, ResidentNSFCount, ResidentNoChk, ResidentInColl, LeaseSigner, FirstName, LastName, ResidentAddress1,
                                ResidentAddress2, City, ResidentState, Zip, Country, Phone, Phone2, PhoneExt, PhoneExt2, EmailAddress, FaxNumber, cellPhone);

                            RealPageDataAccess.RealPage_InsertUpdateRenterbyKey(ResidentKeyID,FirstName, LastName, Phone, cellPhone, FaxNumber, EmailAddress, ResidentAddress1, RP_UnitID, City, ResidentState, Zip,
                                true, true, false, "0000", false, true, ResidentKeyID, 3, true, RP_PropertyID, true, 1);

                            //Remove Fees For resident in RealPage Stage tbl and RPO LeaseFee Table
                            RealPageDataAccess.RealPage_RemoveFeeByResidentKeyID(ResidentKeyID);


                            }
                            catch(Exception e)
                            {
                                //cakel: Added event log
                                log.WriteLog("RealPageImport", "Resident Import Failure - Resident Key:" + ResidentKeyID + "Name: " + FirstName + " " + LastName + e.ToString(), 0, "", false);

                                Console.WriteLine("RealPage - Error Resident Import for resident key ID =" + ResidentKeyID + "Name: " + FirstName + " " + LastName);
                                continue;
                            }
                            
                        }

                       
                    }

                    if (_node1 == "LeaseList")
                    {

                        string ResidentKeyID = "";
                        string ResidentID = "";
                        string RP_PropertyID = "";
                        string RP_UnitID = "";
                        string RP_LeaseID = "";
                        string LastRenewalDate = "";
                        string LeaseBeginDate = "";
                        string LeaseEndDate = "";
                        string InactiveDate = "";
                        string MoveInDate = "";
                        string MoveOutDate = "";
                        string NoticeOnDate = "";
                        string NoticeForDate = "";
                        string MonthToMonthDate = "";
                        string CancelDate = "";
                        string BillingDate = "";
                        string RequiredDaysNotice = "";
                        string Eviction = "";
                        string InitialLeaseDate = "";
                        string PaymentDueDate = "";
                        string RP_LeaseStatus = "";
                        string LeaseType = "";
                        string LateDayOfMonth = "";
                        string LateChargeFixedAmount = "";
                        string LateChargePerDay = "";
                        string LateChargePercentage = "";
                        string Occupants = "";
                        string BasicRent = "";
                        string CurrentBalance = "";
                        string TotalPaid = "";


                        foreach(XElement LeaseElement in ConvBill.Elements())
                        {
                            string LeaseNodeTest = LeaseElement.Name.LocalName;

                            ResidentID = LeaseElement.Attribute("ReshID").Value.ToString();
                            RP_PropertyID = LeaseElement.Attribute("SiteID").Value.ToString();
                            RP_UnitID = LeaseElement.Attribute("UnitID").Value.ToString();
                            RP_LeaseID = LeaseElement.Attribute("LeaID").Value.ToString();
                            LastRenewalDate = LeaseElement.Attribute("LastRenewalDate").Value.ToString();
                            LeaseBeginDate = LeaseElement.Attribute("LBDate").Value.ToString();
                            LeaseEndDate = LeaseElement.Attribute("LEDate").Value.ToString();
                            InactiveDate = LeaseElement.Attribute("InactiveDate").Value.ToString();
                            MoveInDate = LeaseElement.Attribute("MIDate").Value.ToString();

                            MoveOutDate = LeaseElement.Attribute("MODate").Value.ToString();
                            NoticeOnDate = LeaseElement.Attribute("NTVOnDate").Value.ToString();
                            NoticeForDate = LeaseElement.Attribute("NTVForDate").Value.ToString();
                            MonthToMonthDate = LeaseElement.Attribute("MTMDate").Value.ToString();

                            CancelDate = LeaseElement.Attribute("CancelDate").Value.ToString();
                            BillingDate = LeaseElement.Attribute("BillDate").Value.ToString();
                            RequiredDaysNotice = LeaseElement.Attribute("Notice").Value.ToString();
                            Eviction = LeaseElement.Attribute("Evict").Value.ToString();
                            InitialLeaseDate = LeaseElement.Attribute("InitialLeaseDate").Value.ToString();

                            PaymentDueDate = LeaseElement.Attribute("PaymentDueDate").Value.ToString();
                            RP_LeaseStatus = LeaseElement.Attribute("Status").Value.ToString();
                            LeaseType = LeaseElement.Attribute("Type").Value.ToString();
                            LateDayOfMonth = LeaseElement.Attribute("LateDOM").Value.ToString();
                            LateChargeFixedAmount = LeaseElement.Attribute("LCFixedAmt").Value.ToString();
                            LateChargePerDay = LeaseElement.Attribute("LCPerDay").Value.ToString();
                            LateChargePercentage = LeaseElement.Attribute("LCPct").Value.ToString();
                            Occupants = LeaseElement.Attribute("Occ").Value.ToString();
                            BasicRent = LeaseElement.Attribute("Rent").Value.ToString();
                            CurrentBalance = LeaseElement.Attribute("CurBal").Value.ToString();
                            TotalPaid = LeaseElement.Attribute("TotPaid").Value.ToString();

                            ResidentKeyID = RP_PropertyID.ToString() + "-" + ResidentID.ToString() + "-" + RP_LeaseID.ToString();

                            //Insert into Lease Stage table
                            RealPageDataAccess.RealPageInsertLeaseStagetbl(ResidentKeyID, ResidentID, RP_PropertyID, RP_UnitID, RP_LeaseID, LastRenewalDate, LeaseBeginDate,
                                LeaseEndDate, InactiveDate, MoveInDate, MoveOutDate, NoticeOnDate, NoticeForDate, MonthToMonthDate, CancelDate, BillingDate, RequiredDaysNotice, Eviction,
                                InitialLeaseDate, PaymentDueDate, RP_LeaseStatus, LeaseType, LateDayOfMonth, LateChargeFixedAmount, LateChargePerDay, LateChargePercentage, Occupants,
                                BasicRent, CurrentBalance, TotalPaid);

                            Console.WriteLine("RealPage - Start Resident Lease update resident key ID =" + ResidentKeyID);
                            RealPageDataAccess.RealPage_UpdateRenterLeaseByRPkey(ResidentKeyID);
                            Console.WriteLine("RealPage - End Resident Lease update resident key ID =" + ResidentKeyID);

                        }

                    }

                    //ChargeList
                    if (_node1 == "ChargeList")
                    {
 
                        string ResidentKeyID = "";
                        string RP_LeaseID = "";
                        string ResidentID = "";
                        string RP_PropertyID = "";
                        string RP_UnitID = "";
                        string TransactionCategory = "";
                        string TransactionName = "";
                        string TransactionDesc = "";
                        string TransactionAmount = "";
                        string TransactionCodeDescription = "";
                        string TransactionBatchID = "";
                        string TransactivityID = "";
                        string TransactionDate = "";
                        string SubJournal = "";
                        string GroupToRent = "";

                        foreach (XElement ChargeList in ConvBill.Elements())
                        {
                            string Charge_List = ChargeList.Name.LocalName;


                            RP_LeaseID = ChargeList.Attribute("LeaID").Value.ToString();
                            ResidentID = ChargeList.Attribute("ReshID").Value.ToString();
                            RP_PropertyID = ChargeList.Attribute("SiteID").Value.ToString();

                            RP_UnitID = ChargeList.Attribute("UnitID").Value.ToString();
                            TransactionCategory = ChargeList.Attribute("CatCode").Value.ToString();

                            TransactionName = ChargeList.Attribute("TranName").Value.ToString();

                            TransactionDesc = ChargeList.Attribute("TransDesc").Value.ToString();
                            TransactionAmount = ChargeList.Attribute("TransAmt").Value.ToString();

                            TransactionCodeDescription = ChargeList.Attribute("TransDesc").Value.ToString();
                            TransactionBatchID = "";
                            TransactivityID = "";

                            TransactionDate = ChargeList.Attribute("BillStart").Value.ToString();

                            SubJournal = ChargeList.Attribute("SubJournal").Value.ToString();
                            GroupToRent = ChargeList.Attribute("Grouptorent").Value.ToString();

                            ResidentKeyID = RP_PropertyID.ToString() + "-" + ResidentID.ToString() + "-" + RP_LeaseID.ToString();


                            Console.WriteLine("RealPage - Start Resident Lease Fee Import resident key ID =" + ResidentKeyID);

                            try
                            {

                                //Insert into Open Charges table
                                if (TransactionCategory == "C" && TransactionName != "RENT")
                                {

                                    RealPageDataAccess.RealPageInsertOpenTransStagetbl(ResidentKeyID,"ChargeList", RP_LeaseID, ResidentID, RP_PropertyID, RP_UnitID, TransactionCategory,
                                    TransactionName, TransactionDesc, TransactionAmount, TransactionCodeDescription, TransactionBatchID, TransactivityID, TransactionDate, SubJournal, GroupToRent);

                                    decimal amount = decimal.Parse(TransactionAmount);
                                    //Add Fees to Property - the SP will add new fee to property if they do not exist
                                    RealPageDataAccess.RealPage_AddLeaseFeesToProperyByPmsID(RP_PropertyID, TransactionCodeDescription, amount, true, TransactionName);

                                
                                }

                            }
                            catch (Exception e)
                            {
                                //cakel: Added event log
                                log.WriteLog("RealPageImport", "Resident Charge Import Failure - Resident Key:" + ResidentKeyID + e.ToString(), 0, "", false);
                                Console.WriteLine("RealPage - Error Resident Lease Fee Import resident key ID =" + ResidentKeyID + "ChargeList Fee Name: " + TransactionName);
                                continue;
                            }

                        }

                    }


                    //OpenTransactions
                    if (_node1 == "OpenTransactionList")
                    {

                        string ResidentKeyID = "";
                        string RP_LeaseID = "";
                        string ResidentID = "";
                        string RP_PropertyID = "";
                        string RP_UnitID = "";
                        string TransactionCategory = "";
                        string TransactionName = "";
                        string TransactionDesc = "";
                        string TransactionAmount = "";
                        string TransactionCodeDescription = "";
                        string TransactionBatchID = "";
                        string TransactivityID = "";
                        string TransactionDate = "";
                        string SubJournal = "";
                        string GroupToRent = "";

                        foreach (XElement OpenCharges in ConvBill.Elements())
                        {
                            string OpenChargeNode = OpenCharges.Name.LocalName;

                            RP_LeaseID = OpenCharges.Attribute("LeaID").Value.ToString();
                            ResidentID = OpenCharges.Attribute("ReshID").Value.ToString();
                            RP_PropertyID = OpenCharges.Attribute("SiteID").Value.ToString();

                            RP_UnitID = OpenCharges.Attribute("UnitID").Value.ToString();
                            TransactionCategory = OpenCharges.Attribute("CatCode").Value.ToString();

                            TransactionName = OpenCharges.Attribute("TransName").Value.ToString();

                            TransactionDesc = OpenCharges.Attribute("TransDesc").Value.ToString();
                            TransactionAmount = OpenCharges.Attribute("TransAmt").Value.ToString();
                            TransactionCodeDescription = OpenCharges.Attribute("TransCodeDesc").Value.ToString();
                            TransactionBatchID = OpenCharges.Attribute("TransBatchID").Value.ToString();
                            TransactivityID = OpenCharges.Attribute("ldtID").Value.ToString();

                            TransactionDate = OpenCharges.Attribute("TransactionDate").Value.ToString();

                            SubJournal = OpenCharges.Attribute("SubJournal").Value.ToString();
                            GroupToRent = OpenCharges.Attribute("Grouptorent").Value.ToString();

                            ResidentKeyID = RP_PropertyID.ToString() + "-" + ResidentID.ToString() + "-" + RP_LeaseID.ToString();

                            try
                            {

                                //Insert into Open Charges table
                                if(TransactionCategory == "C" && TransactionName != "RENT")
                                {

                                    RealPageDataAccess.RealPageInsertOpenTransStagetbl(ResidentKeyID,"Open Charges", RP_LeaseID, ResidentID, RP_PropertyID, RP_UnitID, TransactionCategory,
                                    TransactionName, TransactionDesc, TransactionAmount, TransactionCodeDescription, TransactionBatchID, TransactivityID, TransactionDate, SubJournal, GroupToRent);

                                    decimal amount = decimal.Parse(TransactionAmount);
                                    //Add Fees to Property - the SP will add new fee to property if they do not exist
                                    RealPageDataAccess.RealPage_AddLeaseFeesToProperyByPmsID(RP_PropertyID, TransactionCodeDescription, amount, true, TransactionName);
  
                                }

                            }
                            catch(Exception ex)
                            {
                                //cakel: Added event log
                                log.WriteLog("RealPageImport", "Resident Open Charge Import Failure - Resident Key:" + ResidentKeyID + ex.ToString(), 0, "", false);
                                Console.WriteLine("RealPage - Error Resident Lease Fee Import resident key ID =" + ResidentKeyID + "Open Charges Fee Name: " + TransactionName);
                                continue;
                            }
                            
                        }

                    }// END if (_node1 == "OpenTransactionList")

                }

            }//end Using
        }



        //Find Root Element 
        public static XElement GetRootElement(XElement element, string localName)
        {
            XElement Element = null;

            if (element != null && element.Name.LocalName == localName)
                return element;

            if (element != null && element.HasElements)
            {
                foreach (var E in element.Elements())
                {
                    Element = GetRootElement(E, localName);
                }
            }

            return Element;
        }

        private static string GetAttributeValue(XElement _Node, string attributeName)
        {
            foreach (XAttribute _att in _Node.Attributes())
            {
                if (_att.Name.LocalName == attributeName)
                {
                    return _att.Value;
                }

            }
            return "";
        }

        private static object CleanAttribute(string _XmlValue)
        {
            if(_XmlValue == "")
            {
                return null;
            }
            else
            {
                return _XmlValue;
            }
        }

        //Removes all special characters from string
        public static string CleanPhoneNumber(string _phone)
        {
            string _newPhone = _phone;
            _newPhone = _newPhone.Replace("(", "");
            _newPhone = _newPhone.Replace(")", "");
            _newPhone = _newPhone.Replace("-", "");
            _newPhone = _newPhone.Replace(" ", "");
            _newPhone = _newPhone.Replace("+", "");

            return _newPhone;
        }



    }
}
