﻿using EfxFramework.PaymentMethods;
using EfxFramework.Pms.Yardi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace EfxFramework.Pms.Yardi
{
    public class YardiRequestV2
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ServerName { get; set; }
        public string Database { get; set; }
        public string Platform { get; set; }
        public string[] YardiPropertyIds { get; set; }
        public string YardiPropertyId { get; set; }
        public string YardiTenantId { get; set; }
        public string InterfaceEntity { get; set; }
        public string InterfaceLicense { get; set; }

        public YardiRequestV2()
        {
            UserName = Settings.EfxYardiUsername;
            Password = Settings.EfxYardiPassword;
            ServerName = Settings.EfxYardiDatabaseServer;
            Database = Settings.EfxYardiDatabaseName;
            Platform = Settings.EfxYardiPlatform;
            YardiPropertyIds = Settings.EfxYardiPropertyList;
            YardiPropertyId = Settings.EfxYardiPropertyList[0];
            InterfaceEntity = Settings.EfxYardiEntity;
            InterfaceLicense = Settings.EfxYardiLicenseInformation;
        }

        public YardiRequestV2(string propertyId)
        {
            UserName = Settings.EfxYardiUsername;
            Password = Settings.EfxYardiPassword;
            ServerName = Settings.EfxYardiDatabaseServer;
            Database = Settings.EfxYardiDatabaseName;
            Platform = Settings.EfxYardiPlatform;
            YardiPropertyId = propertyId;
            InterfaceEntity = Settings.EfxYardiEntity;
            InterfaceLicense = Settings.EfxYardiLicenseInformation;
        }

        public YardiRequestV2(string userName, string password, string serverName, string database, string platform)
        {
            UserName = userName;
            Password = password;
            ServerName = serverName;
            Database = database;
            Platform = platform;
            InterfaceEntity = Settings.EfxYardiEntity;
            InterfaceLicense = Settings.EfxYardiLicenseInformation;
        }

        public YardiRequestV2(string userName, string password, string serverName, string database, string platform, string yardiPropertyId)
        {
            UserName = userName;
            Password = password;
            ServerName = serverName;
            Database = database;
            Platform = platform;
            YardiPropertyId = yardiPropertyId;
            InterfaceEntity = Settings.EfxYardiEntity;
            InterfaceLicense = Settings.EfxYardiLicenseInformation;
        }

        public YardiRequestV2(string userName, string password, string serverName, string database, string platform, string yardiPropertyId, string yardiTenantId)
        {
            UserName = userName;
            Password = password;
            ServerName = serverName;
            Database = database;
            Platform = platform;
            YardiPropertyId = yardiPropertyId;
            YardiTenantId = yardiTenantId;
            InterfaceEntity = Settings.EfxYardiEntity;
            InterfaceLicense = Settings.EfxYardiLicenseInformation;
        }

        public string GetSoapRequest(string method)
        {
            var Sb = new StringBuilder();
            Sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            Sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
            Sb.Append("<soap:Body>");
            Sb.AppendFormat("<{0} xmlns=\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20\">", method);
            Sb.AppendFormat("<UserName>{0}</UserName>", UserName);
            Sb.AppendFormat("<Password>{0}</Password>", Password);
            Sb.AppendFormat("<ServerName>{0}</ServerName>", ServerName);
            Sb.AppendFormat("<Database>{0}</Database>", Database);
            Sb.AppendFormat("<Platform>{0}</Platform>", Platform);
            Sb.AppendFormat("<YardiPropertyId>{0}</YardiPropertyId>", YardiPropertyId);
            Sb.AppendFormat("<InterfaceEntity>{0}</InterfaceEntity>", InterfaceEntity);
            Sb.AppendFormat("<InterfaceLicense>{0}</InterfaceLicense>", InterfaceLicense);
            Sb.AppendFormat("</{0}>", method);
            Sb.Append("</soap:Body>");
            Sb.Append("</soap:Envelope>");

            return Sb.ToString();
        }

        public string GetLeaseChargeSoapRequest(string method, bool singleTenant = false)
        {
            var Sb = new StringBuilder();
            Sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            Sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
            Sb.Append("<soap:Body>");
            Sb.AppendFormat("<{0} xmlns=\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20\">", method);
            Sb.AppendFormat("<UserName>{0}</UserName>", UserName);
            Sb.AppendFormat("<Password>{0}</Password>", Password);
            Sb.AppendFormat("<ServerName>{0}</ServerName>", ServerName);
            Sb.AppendFormat("<Database>{0}</Database>", Database);
            Sb.AppendFormat("<Platform>{0}</Platform>", Platform);
            Sb.AppendFormat("<YardiPropertyId>{0}</YardiPropertyId>", YardiPropertyId);
            Sb.AppendFormat("<PostMonth>{0:yyyy-MM-ddTHH:mm:ss.fffZ}</PostMonth>", DateTime.UtcNow);

            if (singleTenant)
                Sb.AppendFormat("<TenantId>{0}</TenantId>", YardiTenantId);

            Sb.AppendFormat("<InterfaceEntity>{0}</InterfaceEntity>", InterfaceEntity);
            Sb.AppendFormat("<InterfaceLicense>{0}</InterfaceLicense>", InterfaceLicense);
            Sb.AppendFormat("</{0}>", method);
            Sb.Append("</soap:Body>");
            Sb.Append("</soap:Envelope>");

            return Sb.ToString();
        }

        public string GetSoapRequest(string method, string transactionXml)
        {
            var Sb = new StringBuilder();
            Sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            Sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
            Sb.Append("<soap:Body>");
            Sb.AppendFormat("<{0} xmlns=\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20\">", method);
            Sb.AppendFormat("<UserName>{0}</UserName>", UserName);
            Sb.AppendFormat("<Password>{0}</Password>", Password);
            Sb.AppendFormat("<ServerName>{0}</ServerName>", ServerName);
            Sb.AppendFormat("<Database>{0}</Database>", Database);
            Sb.AppendFormat("<Platform>{0}</Platform>", Platform);
            Sb.AppendFormat("<InterfaceEntity>{0}</InterfaceEntity>", InterfaceEntity);
            Sb.AppendFormat("<InterfaceLicense>{0}</InterfaceLicense>", InterfaceLicense);
            Sb.AppendFormat("<TransactionXml>{0}</TransactionXml>", transactionXml);
            Sb.AppendFormat("</{0}>", method);
            Sb.Append("</soap:Body>");
            Sb.Append("</soap:Envelope>");

            return Sb.ToString();
        }

        public string GetConfigurationRequest()
        {
            var Sb = new StringBuilder();
            Sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            Sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
            Sb.Append("<soap:Body>");
            Sb.AppendFormat("<{0} xmlns=\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20\">", "GetPropertyConfigurations");
            Sb.AppendFormat("<UserName>{0}</UserName>", UserName);
            Sb.AppendFormat("<Password>{0}</Password>", Password);
            Sb.AppendFormat("<ServerName>{0}</ServerName>", ServerName);
            Sb.AppendFormat("<Database>{0}</Database>", Database);
            Sb.AppendFormat("<Platform>{0}</Platform>", Platform);
            Sb.AppendFormat("<InterfaceEntity>{0}</InterfaceEntity>", InterfaceEntity);
            Sb.AppendFormat("<InterfaceLicense>{0}</InterfaceLicense>", InterfaceLicense);
            Sb.AppendFormat("</{0}>", "GetPropertyConfigurations");
            Sb.Append("</soap:Body>");
            Sb.Append("</soap:Envelope>");

            return Sb.ToString();
        }

        public static string BuildTransactionXml(IEnumerable<Payment> payments)
        {
            var YardiPayments = payments.Select(ConvertPayment).Where(yardiPayment => yardiPayment != null).ToList();
            var Transaction = new ResidentTransactions { Property = new[] { new ResidentTransactionsProperty() } };

            var Customers = new Dictionary<string, List<ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPayment>>();

            foreach (var P in YardiPayments)
            {
                if (Customers.ContainsKey(P.Detail.CustomerID))
                    Customers[P.Detail.CustomerID].Add(P);
                else
                    Customers.Add(P.Detail.CustomerID, new List<ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPayment> {P});
            }

            Transaction.Property[0].RT_Customer = new ResidentTransactionsPropertyRT_Customer[Customers.Keys.Count];

            var Index = 0;

            foreach (var Kvp in Customers)
            {
                Transaction.Property[0].RT_Customer[Index] = new ResidentTransactionsPropertyRT_Customer
                    {
                        CustomerID = Kvp.Key,
                        RTServiceTransactions = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactions
                            {
                                Transactions = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactions[Kvp.Value.Count]
                            }
                    };

                for (var I = 0; I < Kvp.Value.Count; I++)
                {
                    Transaction.Property[0].RT_Customer[Index].RTServiceTransactions.Transactions[I] = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactions {Payment = Kvp.Value[I]};
                }

                Index++;
            }

            var Xml = new StringBuilder();

            using (var Writer = XmlWriter.Create(Xml, new XmlWriterSettings { Indent = true, ConformanceLevel = ConformanceLevel.Auto, OmitXmlDeclaration = true }))
            {
                var Serializer = new XmlSerializer(typeof(ResidentTransactions));
                Serializer.Serialize(Writer, Transaction);
            }

            return Xml.ToString();
        }

        public static string BuildReversalTransactionXml(Payment payment, ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentDetailReversalType reversalType)
        {
            var YardiReversal = ConvertPayment(payment, reversalType);
            var Transaction = new ResidentTransactions { Property = new[] { new ResidentTransactionsProperty() } };

            Transaction.Property[0].RT_Customer = new[] { new ResidentTransactionsPropertyRT_Customer() };
            Transaction.Property[0].RT_Customer[0].RTServiceTransactions = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactions
            {
                Transactions = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactions[1]
            };

            Transaction.Property[0].RT_Customer[0].RTServiceTransactions.Transactions[0] = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactions { Payment = YardiReversal };

            var Xml = new StringBuilder();

            using (var Writer = XmlWriter.Create(Xml, new XmlWriterSettings { Indent = true, ConformanceLevel = ConformanceLevel.Auto, OmitXmlDeclaration = true }))
            {
                var Serializer = new XmlSerializer(typeof(ResidentTransactions));
                Serializer.Serialize(Writer, Transaction);
            }

            return Xml.ToString();
        }

        private static ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPayment ConvertPayment(Payment payment)
        {
            var YardiPayment = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPayment();
            var Renter = new Renter(payment.RenterId);
            var Payer = new Payer(payment.PayerId);
            var Amount = GetPaymentAmount(payment);
            var Description = string.Empty;
            

            if (Renter.RenterId < 1 || !Renter.PmsTypeId.HasValue || Renter.PmsTypeId.Value != 1 || String.IsNullOrEmpty(Renter.PmsId))
                return null;

            var ParentYardiId = Renter.PmsPropertyId;

            if (String.IsNullOrEmpty(ParentYardiId))
                return null;

            if (Amount == null)
                return null;

            if (payment.RentAmount.HasValue && payment.RentAmount.Value > 0.0M)
                Description = payment.RentAmountDescription;
            else if (payment.OtherAmount1.HasValue && payment.OtherAmount1.Value > 0.0M && !String.IsNullOrEmpty(payment.OtherDescription1))
                Description = String.Format("{0}, {1}", Description, payment.OtherDescription1);
            else if (payment.OtherAmount2.HasValue && payment.OtherAmount2.Value > 0.0M && !String.IsNullOrEmpty(payment.OtherDescription2))
                Description = String.Format("{0}, {1}", Description, payment.OtherDescription2);
            else if (payment.OtherAmount3.HasValue && payment.OtherAmount3.Value > 0.0M && !String.IsNullOrEmpty(payment.OtherDescription3))
                Description = String.Format("{0}, {1}", Description, payment.OtherDescription3);
            else if (payment.OtherAmount4.HasValue && payment.OtherAmount4.Value > 0.0M && !String.IsNullOrEmpty(payment.OtherDescription4))
                Description = String.Format("{0}, {1}", Description, payment.OtherDescription4);
            else if (payment.OtherAmount5.HasValue && payment.OtherAmount5.Value > 0.0M && !String.IsNullOrEmpty(payment.OtherDescription5))
                Description = String.Format("{0}, {1}", Description, payment.OtherDescription5);

            YardiPayment.Type = payment.PaymentTypeId == 1 ? ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentType.Cash : ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentType.Check;
            YardiPayment.Detail = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentDetail
            {
                BatchID = null,
                DocumentNumber = payment.TransactionId,
                TransactionDateSpecified = true,
                TransactionDate = payment.TransactionDateTime,
                GLAccountNumber = null,
                CustomerID = Renter.PmsId,
                PaidBy = Payer.DisplayName,
                Amount = Amount.BaseAmount.ToString("f2"),
                Comment = String.Format("RPO-{0}-{1}-{2}", ParentYardiId, payment.PaymentTypeId == 1 ? "CC" : "ACH", ExportRequestV2.GetPaymentDateString(payment.TransactionDateTime).Replace("/", "")),
                PropertyPrimaryID = ParentYardiId,
                Description = Description
            };

            return YardiPayment;
        }

        private static ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPayment ConvertPayment(Payment payment, ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentDetailReversalType reversalType)
        {
            var YardiPayment = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPayment();
            var YardiReversal = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentDetailReversal();
            var Renter = new Renter(payment.RenterId);
            var Payer = new Payer(payment.PayerId);
            var Amount = GetPaymentAmount(payment);

            YardiReversal.Type = reversalType;

            if (Renter.RenterId < 1 || !Renter.PmsTypeId.HasValue || Renter.PmsTypeId.Value != 1 || String.IsNullOrEmpty(Renter.PmsId))
                return null;

            if (!Renter.PmsId.ToLower().StartsWith("r") && !Renter.PmsId.ToLower().StartsWith("t"))
                return null;

            var ParentYardiId = Renter.PmsPropertyId;

            if (String.IsNullOrEmpty(ParentYardiId))
                return null;

            if (Amount == null)
                return null;

            YardiPayment.Type = ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentType.Other;
            YardiPayment.Detail = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentDetail
            {
                DocumentNumber = payment.TransactionId,
                Reversal = YardiReversal,
                TransactionDateSpecified = true,
                TransactionDate = payment.TransactionDateTime,
                CustomerID = Renter.PmsId,
                PaidBy = Payer.DisplayName,
                Amount = Amount.BaseAmount.ToString("f2"),
                Comment = "NSF Received for transaction",
                PropertyPrimaryID = ParentYardiId
            };

            return YardiPayment;
        }

        private static PaymentAmount GetPaymentAmount(Payment payment)
        {
            var Amount = new PaymentAmount();

            if (payment.RentAmount.HasValue && payment.RentAmount.Value > 0.0M)
            {
                Amount.BaseAmount = payment.RentAmount.Value;
                Amount.FeeDescription = payment.RentAmountDescription;
                return Amount;
            }
            if (payment.OtherAmount1.HasValue && payment.OtherAmount1.Value > 0.0M)
            {
                if (!String.IsNullOrEmpty(payment.OtherDescription1) && !payment.OtherDescription1.ToLower().Contains("convenience fee"))
                {
                    Amount.BaseAmount = payment.OtherAmount1.Value;
                    Amount.FeeDescription = payment.OtherDescription1;
                    return Amount;
                }
            }
            if (payment.OtherAmount2.HasValue && payment.OtherAmount2.Value > 0.0M)
            {
                if (!String.IsNullOrEmpty(payment.OtherDescription2) && !payment.OtherDescription2.ToLower().Contains("convenience fee"))
                {
                    Amount.BaseAmount = payment.OtherAmount2.Value;
                    Amount.FeeDescription = payment.OtherDescription2;
                    return Amount;
                }
            }
            if (payment.OtherAmount3.HasValue && payment.OtherAmount3.Value > 0.0M)
            {
                if (!String.IsNullOrEmpty(payment.OtherDescription3) && !payment.OtherDescription3.ToLower().Contains("convenience fee"))
                {
                    Amount.BaseAmount = payment.OtherAmount3.Value;
                    Amount.FeeDescription = payment.OtherDescription3;
                    return Amount;
                }
            }
            if (payment.OtherAmount4.HasValue && payment.OtherAmount4.Value > 0.0M)
            {
                if (!String.IsNullOrEmpty(payment.OtherDescription4) && !payment.OtherDescription4.ToLower().Contains("convenience fee"))
                {
                    Amount.BaseAmount = payment.OtherAmount4.Value;
                    Amount.FeeDescription = payment.OtherDescription4;
                    return Amount;
                }
            }
            if (payment.OtherAmount5.HasValue && payment.OtherAmount5.Value > 0.0M)
            {
                if (!String.IsNullOrEmpty(payment.OtherDescription5) && !payment.OtherDescription5.ToLower().Contains("convenience fee"))
                {
                    Amount.BaseAmount = payment.OtherAmount5.Value;
                    Amount.FeeDescription = payment.OtherDescription5;
                    return Amount;
                }
            }

            return null;
        }
    }
}
