﻿using EfxFramework.PaymentMethods;
using EfxFramework.Pms.Yardi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace EfxFramework.Pms.Yardi
{
    public class YardiRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ServerName { get; set; }
        public string Database { get; set; }
        public string Platform { get; set; }
        public string[] YardiPropertyIds { get; set; }
        public string YardiPropertyId { get; set; }
        public string YardiTenantId { get; set; }
        public string InterfaceEntity { get; set; }
        public string InterfaceLicense { get; set; }

        public YardiRequest()
        {
            UserName = EfxSettings.EfxYardiUsername;
            Password = EfxSettings.EfxYardiPassword;
            ServerName = EfxSettings.EfxYardiDatabaseServer;
            Database = EfxSettings.EfxYardiDatabaseName;
            Platform = EfxSettings.EfxYardiPlatform;
            YardiPropertyIds = EfxSettings.EfxYardiPropertyList;
            YardiPropertyId = EfxSettings.EfxYardiPropertyList[0];
            InterfaceEntity = EfxSettings.EfxYardiEntity;
            InterfaceLicense = EfxSettings.EfxYardiLicenseInformation;
        }

        public YardiRequest(string propertyId)
        {
            UserName = EfxSettings.EfxYardiUsername;
            Password = EfxSettings.EfxYardiPassword;
            ServerName = EfxSettings.EfxYardiDatabaseServer;
            Database = EfxSettings.EfxYardiDatabaseName;
            Platform = EfxSettings.EfxYardiPlatform;
            YardiPropertyId = propertyId;
            InterfaceEntity = EfxSettings.EfxYardiEntity;
            InterfaceLicense = EfxSettings.EfxYardiLicenseInformation;
        }

        public YardiRequest(string userName, string password, string serverName, string database, string platform)
        {
            UserName = userName;
            Password = password;
            ServerName = serverName;
            Database = database;
            Platform = platform;
            InterfaceEntity = EfxSettings.EfxYardiEntity;
            InterfaceLicense = EfxSettings.EfxYardiLicenseInformation;
        }

        public YardiRequest(string userName, string password, string serverName, string database, string platform, string yardiPropertyId)
        {
            UserName = userName;
            Password = password;
            ServerName = serverName;
            Database = database;
            Platform = platform;
            YardiPropertyId = yardiPropertyId;
            InterfaceEntity = EfxSettings.EfxYardiEntity;
            InterfaceLicense = EfxSettings.EfxYardiLicenseInformation;
        }

        public YardiRequest(string userName, string password, string serverName, string database, string platform, string yardiPropertyId, string yardiTenantId)
        {
            UserName = userName;
            Password = password;
            ServerName = serverName;
            Database = database;
            Platform = platform;
            YardiPropertyId = yardiPropertyId;
            YardiTenantId = yardiTenantId;
            InterfaceEntity = EfxSettings.EfxYardiEntity;
            InterfaceLicense = EfxSettings.EfxYardiLicenseInformation;
        }

        public string GetSoapRequest(string method)
        {
            var Sb = new StringBuilder();
            Sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            Sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
            Sb.Append("<soap:Body>");
            Sb.AppendFormat("<{0} xmlns=\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20\">", method);
            Sb.AppendFormat("<UserName>{0}</UserName>", UserName);
            Sb.AppendFormat("<Password>{0}</Password>", Password);
            Sb.AppendFormat("<ServerName>{0}</ServerName>", ServerName);
            Sb.AppendFormat("<Database>{0}</Database>", Database);
            Sb.AppendFormat("<Platform>{0}</Platform>", Platform);
            Sb.AppendFormat("<YardiPropertyId>{0}</YardiPropertyId>", YardiPropertyId);
            Sb.AppendFormat("<InterfaceEntity>{0}</InterfaceEntity>", InterfaceEntity);
            Sb.AppendFormat("<InterfaceLicense>{0}</InterfaceLicense>", InterfaceLicense);
            Sb.AppendFormat("</{0}>", method);
            Sb.Append("</soap:Body>");
            Sb.Append("</soap:Envelope>");

            return Sb.ToString();
        }

        public string GetLeaseChargeSoapRequest(string method, bool singleTenant = false)
        {
            var Sb = new StringBuilder();
            Sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            Sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
            Sb.Append("<soap:Body>");
            Sb.AppendFormat("<{0} xmlns=\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20\">", method);
            Sb.AppendFormat("<UserName>{0}</UserName>", UserName);
            Sb.AppendFormat("<Password>{0}</Password>", Password);
            Sb.AppendFormat("<ServerName>{0}</ServerName>", ServerName);
            Sb.AppendFormat("<Database>{0}</Database>", Database);
            Sb.AppendFormat("<Platform>{0}</Platform>", Platform);
            Sb.AppendFormat("<YardiPropertyId>{0}</YardiPropertyId>", YardiPropertyId);
            Sb.AppendFormat("<PostMonth>{0:yyyy-MM-ddTHH:mm:ss.fffZ}</PostMonth>", DateTime.UtcNow);

            if (singleTenant)
                Sb.AppendFormat("<TenantId>{0}</TenantId>", YardiTenantId);

            Sb.AppendFormat("<InterfaceEntity>{0}</InterfaceEntity>", InterfaceEntity);
            Sb.AppendFormat("<InterfaceLicense>{0}</InterfaceLicense>", InterfaceLicense);
            Sb.AppendFormat("</{0}>", method);
            Sb.Append("</soap:Body>");
            Sb.Append("</soap:Envelope>");

            return Sb.ToString();
        }

        public string GetLeaseChargeSoapRequest(string method, DateTime PostMonth, bool singleTenant = false)
        {
            var Sb = new StringBuilder();
            Sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            Sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
            Sb.Append("<soap:Body>");
            Sb.AppendFormat("<{0} xmlns=\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20\">", method);
            Sb.AppendFormat("<UserName>{0}</UserName>", UserName);
            Sb.AppendFormat("<Password>{0}</Password>", Password);
            Sb.AppendFormat("<ServerName>{0}</ServerName>", ServerName);
            Sb.AppendFormat("<Database>{0}</Database>", Database);
            Sb.AppendFormat("<Platform>{0}</Platform>", Platform);
            Sb.AppendFormat("<YardiPropertyId>{0}</YardiPropertyId>", YardiPropertyId);

            //Salcedo - 7/29/2014 - we found it necessary to remove the time component from the date when specifying PostMonth
            //Sb.AppendFormat("<PostMonth>{0:yyyy-MM-ddTHH:mm:ss.fffZ}</PostMonth>", PostMonth);
            Sb.AppendFormat("<PostMonth>{0:yyyy-MM-dd}</PostMonth>", PostMonth);

            if (singleTenant)
                Sb.AppendFormat("<TenantId>{0}</TenantId>", YardiTenantId);

            Sb.AppendFormat("<InterfaceEntity>{0}</InterfaceEntity>", InterfaceEntity);
            Sb.AppendFormat("<InterfaceLicense>{0}</InterfaceLicense>", InterfaceLicense);
            Sb.AppendFormat("</{0}>", method);
            Sb.Append("</soap:Body>");
            Sb.Append("</soap:Envelope>");

            return Sb.ToString();
        }

        //cakel: BUGID00074 - Real time call to Yardi for single tenant
        public string GetSingleLeaseChargeSoapRequest(string method, bool singleTenant = true)
        {
            var Sb = new StringBuilder();
            Sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            Sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
            Sb.Append("<soap:Body>");
            Sb.AppendFormat("<{0} xmlns=\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20\">", method);
            Sb.AppendFormat("<UserName>{0}</UserName>", UserName);
            Sb.AppendFormat("<Password>{0}</Password>", Password);
            Sb.AppendFormat("<ServerName>{0}</ServerName>", ServerName);
            Sb.AppendFormat("<Database>{0}</Database>", Database);
            Sb.AppendFormat("<Platform>{0}</Platform>", Platform);
            Sb.AppendFormat("<YardiPropertyId>{0}</YardiPropertyId>", YardiPropertyId);
            if (singleTenant)
                Sb.AppendFormat("<TenantId>{0}</TenantId>", YardiTenantId);

            Sb.AppendFormat("<InterfaceEntity>{0}</InterfaceEntity>", InterfaceEntity);
            Sb.AppendFormat("<InterfaceLicense>{0}</InterfaceLicense>", InterfaceLicense);
            Sb.AppendFormat("</{0}>", method);
            Sb.Append("</soap:Body>");
            Sb.Append("</soap:Envelope>");

            return Sb.ToString();
        }


        public string GetSoapRequest(string method, string transactionXml)
        {
            var Sb = new StringBuilder();
            Sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            Sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
            Sb.Append("<soap:Body>");
            Sb.AppendFormat("<{0} xmlns=\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20\">", method);
            Sb.AppendFormat("<UserName>{0}</UserName>", UserName);
            Sb.AppendFormat("<Password>{0}</Password>", Password);
            Sb.AppendFormat("<ServerName>{0}</ServerName>", ServerName);
            Sb.AppendFormat("<Database>{0}</Database>", Database);
            Sb.AppendFormat("<Platform>{0}</Platform>", Platform);
            Sb.AppendFormat("<InterfaceEntity>{0}</InterfaceEntity>", InterfaceEntity);
            Sb.AppendFormat("<InterfaceLicense>{0}</InterfaceLicense>", InterfaceLicense);
            Sb.AppendFormat("<TransactionXml>{0}</TransactionXml>", transactionXml);
            Sb.AppendFormat("</{0}>", method);
            Sb.Append("</soap:Body>");
            Sb.Append("</soap:Envelope>");

            return Sb.ToString();
        }

        // Salcedo - 8/23/2014 - issue # 0000179
        public string GetSoapRequestDepositDate(string method, string transactionXml, string DepositDate, string DepositMemo)
        {
            var Sb = new StringBuilder();
            Sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            Sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
            Sb.Append("<soap:Body>");
            Sb.AppendFormat("<{0} xmlns=\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20\">", method);
            Sb.AppendFormat("<UserName>{0}</UserName>", UserName);
            Sb.AppendFormat("<Password>{0}</Password>", Password);
            Sb.AppendFormat("<ServerName>{0}</ServerName>", ServerName);
            Sb.AppendFormat("<Database>{0}</Database>", Database);
            Sb.AppendFormat("<Platform>{0}</Platform>", Platform);
            Sb.AppendFormat("<InterfaceEntity>{0}</InterfaceEntity>", InterfaceEntity);
            Sb.AppendFormat("<InterfaceLicense>{0}</InterfaceLicense>", InterfaceLicense);
            Sb.AppendFormat("<TransactionXml>{0}</TransactionXml>", transactionXml);

            // Salcedo - 8/24/2014 - task # 0000179
            // Include the DepositDate and DepositMemo elements
            Sb.AppendLine();
            Sb.AppendFormat("<DepositDate>{0}</DepositDate>", DepositDate);

            Sb.AppendLine();
            Sb.AppendFormat("<DepositMemo>{0}</DepositMemo>", DepositMemo);
            Sb.AppendLine();

            Sb.AppendFormat("</{0}>", method);
            Sb.Append("</soap:Body>");
            Sb.Append("</soap:Envelope>");

            return Sb.ToString();
        }

        public string GetConfigurationRequest()
        {
            var Sb = new StringBuilder();
            Sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            Sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
            Sb.Append("<soap:Body>");
            Sb.AppendFormat("<{0} xmlns=\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20\">", "GetPropertyConfigurations");
            Sb.AppendFormat("<UserName>{0}</UserName>", UserName);
            Sb.AppendFormat("<Password>{0}</Password>", Password);
            Sb.AppendFormat("<ServerName>{0}</ServerName>", ServerName);
            Sb.AppendFormat("<Database>{0}</Database>", Database);
            Sb.AppendFormat("<Platform>{0}</Platform>", Platform);
            Sb.AppendFormat("<InterfaceEntity>{0}</InterfaceEntity>", InterfaceEntity);
            Sb.AppendFormat("<InterfaceLicense>{0}</InterfaceLicense>", InterfaceLicense);
            Sb.AppendFormat("</{0}>", "GetPropertyConfigurations");
            Sb.Append("</soap:Body>");
            Sb.Append("</soap:Envelope>");

            return Sb.ToString();
        }

        public static string BuildTransactionXml(IEnumerable<Payment> payments)
        {
            var YardiPayments = payments.Where(p => !p.IsPmsProcessed).Select(ConvertPayment).Where(yardiPayment => yardiPayment != null).ToList();
            var Transaction = new ResidentTransactions { Property = new[] { new ResidentTransactionsProperty() } };

            Transaction.Property[0].RT_Customer = new[] { new ResidentTransactionsPropertyRT_Customer() };
            Transaction.Property[0].RT_Customer[0].RTServiceTransactions = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactions
            {
                Transactions = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactions[YardiPayments.Count]
            };

            foreach (var YardiPayment in YardiPayments)
            {
                Transaction.Property[0].RT_Customer[0].RTServiceTransactions.Transactions[YardiPayments.IndexOf(YardiPayment)] = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactions { Payment = YardiPayment };
            }

            var Xml = new StringBuilder();

            using (var Writer = XmlWriter.Create(Xml, new XmlWriterSettings { Indent = true, ConformanceLevel = ConformanceLevel.Auto, OmitXmlDeclaration = true }))
            {
                var Serializer = new XmlSerializer(typeof(ResidentTransactions));
                Serializer.Serialize(Writer, Transaction);
            }

            return Xml.ToString();
        }

        public static string BuildReversalTransactionXml(Payment payment, ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentDetailReversalType reversalType)
        {
            var YardiReversal = ConvertPayment(payment, reversalType);
            var Transaction = new ResidentTransactions { Property = new[] { new ResidentTransactionsProperty() } };

            Transaction.Property[0].RT_Customer = new[] { new ResidentTransactionsPropertyRT_Customer() };
            Transaction.Property[0].RT_Customer[0].RTServiceTransactions = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactions
            {
                Transactions = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactions[1]
            };

            Transaction.Property[0].RT_Customer[0].RTServiceTransactions.Transactions[0] = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactions { Payment = YardiReversal };

            var Xml = new StringBuilder();

            using (var Writer = XmlWriter.Create(Xml, new XmlWriterSettings { Indent = true, ConformanceLevel = ConformanceLevel.Auto, OmitXmlDeclaration = true }))
            {
                var Serializer = new XmlSerializer(typeof(ResidentTransactions));
                Serializer.Serialize(Writer, Transaction);
            }

            return Xml.ToString();
        }

        private static ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPayment ConvertPayment(Payment payment)
        {
            var YardiPayment = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPayment();
            var Renter = new Renter(payment.RenterId);
            var Payer = new Payer(payment.PayerId);
            var Amount = GetPaymentAmount(payment);
            var Description = string.Empty;

            if (Renter.RenterId < 1 || !Renter.PmsTypeId.HasValue || Renter.PmsTypeId.Value != 1 || String.IsNullOrEmpty(Renter.PmsId))
                return null;

            var ParentYardiId = Renter.PmsPropertyId;

            if (String.IsNullOrEmpty(ParentYardiId))
                return null;

            if (Amount == null)
                return null;

            if (payment.RentAmount.HasValue && payment.RentAmount.Value > 0.0M)
                Description = payment.RentAmountDescription;
            else if (payment.OtherAmount1.HasValue && payment.OtherAmount1.Value > 0.0M)
                Description = payment.OtherDescription1;
            else if (payment.OtherAmount2.HasValue && payment.OtherAmount2.Value > 0.0M)
                Description = payment.OtherDescription2;
            else if (payment.OtherAmount3.HasValue && payment.OtherAmount3.Value > 0.0M)
                Description = payment.OtherDescription3;
            else if (payment.OtherAmount4.HasValue && payment.OtherAmount4.Value > 0.0M)
                Description = payment.OtherDescription4;
            else if (payment.OtherAmount5.HasValue && payment.OtherAmount5.Value > 0.0M)
                Description = payment.OtherDescription5;

            //Salcedo - 2/22/2014 - changed comment to include payment type and date
            string PaymentTypeComment = "";
            if (payment.PaymentTypeId == 1) PaymentTypeComment = "CC ";
            if (payment.PaymentTypeId == 2) PaymentTypeComment = "ACH ";
            if (payment.PaymentTypeId == 3) PaymentTypeComment = "Cash ";
            //cakel: BUGID00283
            DateTime TranDate = payment.TransactionDateTime.ToLocalTime();
            //cakel: BUGID00283 commented out below to replace with new statement
            //PaymentTypeComment = "RPO " + PaymentTypeComment + payment.TransactionDateTime.ToString("MM-dd-yy");
            PaymentTypeComment = "RPO " + PaymentTypeComment + TranDate.ToString("MM-dd-yy");


            //Salcedo - 6/4/2014 - task # 0000122 - RentByCash transactions should be exported as type "Cash"
            //YardiPayment.Type = payment.PaymentTypeId == 1 ? ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentType.Cash : ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentType.Check;
            if (payment.PaymentTypeId == 1)
            {
                //Type = 1 = CC - mark as cash
                YardiPayment.Type = ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentType.Cash;
            }
            else if (payment.PaymentTypeId == 2)
            {
                //Type = 2 = ACH
                YardiPayment.Type = ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentType.Check;
            }
            else
            {
                //Type = 3 = RentByCash
                YardiPayment.Type = ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentType.Cash;
            }
            YardiPayment.Detail = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentDetail
            {
                BatchID = null,
                DocumentNumber = payment.TransactionId,
                TransactionDateSpecified = true,
                //cakel: BUGID00283 Commented out below and replaced with new statement
                //TransactionDate = payment.TransactionDateTime,
                TransactionDate = TranDate,
                GLAccountNumber = null,
                CustomerID = Renter.PmsId,
                PaidBy = Payer.DisplayName,
                Amount = Amount.BaseAmount.ToString("f2"),
                Comment = PaymentTypeComment, //Description, - Salcedo - 2/22/2014
                PropertyPrimaryID = ParentYardiId
            };

            return YardiPayment;
        }

        private static ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPayment ConvertPayment(Payment payment, ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentDetailReversalType reversalType)
        {
            var YardiPayment = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPayment();
            var YardiReversal = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentDetailReversal();
            var Renter = new Renter(payment.RenterId);
            var Payer = new Payer(payment.PayerId);
            var Amount = GetPaymentAmount(payment);

            YardiReversal.Type = reversalType;

            if (Renter.RenterId < 1 || !Renter.PmsTypeId.HasValue || Renter.PmsTypeId.Value != 1 || String.IsNullOrEmpty(Renter.PmsId))
                return null;

            if (!Renter.PmsId.ToLower().StartsWith("r") && !Renter.PmsId.ToLower().StartsWith("t"))
                return null;

            var ParentYardiId = Renter.PmsPropertyId;

            if (String.IsNullOrEmpty(ParentYardiId))
                return null;

            if (Amount == null)
                return null;

            YardiPayment.Type = ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentType.Other;
            YardiPayment.Detail = new ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentDetail
            {
                DocumentNumber = payment.TransactionId,
                Reversal = YardiReversal,
                TransactionDateSpecified = true,
                TransactionDate = payment.TransactionDateTime,
                CustomerID = Renter.PmsId,
                PaidBy = Payer.DisplayName,
                Amount = Amount.BaseAmount.ToString("f2"),
                Comment = "NSF Received for transaction",
                PropertyPrimaryID = ParentYardiId
            };

            return YardiPayment;
        }

        private static PaymentAmount GetPaymentAmount(Payment payment)
        {
            var Amount = new PaymentAmount();

            if (payment.RentAmount.HasValue && payment.RentAmount.Value > 0.0M)
            {
                Amount.BaseAmount = payment.RentAmount.Value;
                Amount.FeeDescription = payment.RentAmountDescription;
                return Amount;
            }
            if (payment.OtherAmount1.HasValue && payment.OtherAmount1.Value > 0.0M)
            {
                Amount.BaseAmount = payment.OtherAmount1.Value;
                Amount.FeeDescription = payment.OtherDescription1;
                return Amount;
            }
            if (payment.OtherAmount2.HasValue && payment.OtherAmount2.Value > 0.0M)
            {
                if (!String.IsNullOrEmpty(payment.OtherDescription2) && !payment.OtherDescription2.ToLower().Contains("convenience fee"))
                {
                    Amount.BaseAmount = payment.OtherAmount2.Value;
                    Amount.FeeDescription = payment.OtherDescription2;
                    return Amount;
                }
            }
            if (payment.OtherAmount3.HasValue && payment.OtherAmount3.Value > 0.0M)
            {
                Amount.BaseAmount = payment.OtherAmount3.Value;
                Amount.FeeDescription = payment.OtherDescription3;
                return Amount;
            }
            if (payment.OtherAmount4.HasValue && payment.OtherAmount4.Value > 0.0M)
            {
                Amount.BaseAmount = payment.OtherAmount4.Value;
                Amount.FeeDescription = payment.OtherDescription4;
                return Amount;
            }
            if (payment.OtherAmount5.HasValue && payment.OtherAmount5.Value > 0.0M)
            {
                Amount.BaseAmount = payment.OtherAmount5.Value;
                Amount.FeeDescription = payment.OtherDescription5;
                return Amount;
            }

            return null;
        }
    }
}
