﻿using EfxFramework.Pms.Yardi.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml.Linq;

namespace EfxFramework.Pms.Yardi
{
    public class ExportRequest
    {
        public static void ExportPayments()
        {
            var Payments = new Dictionary<string, List<Payment>>();

            foreach (var P in Payment.GetAllUnprocessedPmsPaymentsByPmsTypeId(1))
            {
                var Renter = new Renter(P.RenterId);

                if (Renter.RenterId > 0 && !String.IsNullOrEmpty(Renter.PmsPropertyId))
                {
                    if (Payments.ContainsKey(Renter.PmsPropertyId))
                        Payments[Renter.PmsPropertyId].Add(P);
                    else
                        Payments.Add(Renter.PmsPropertyId, new List<Payment> { P });
                }
            }

            foreach (var K in Payments.Keys)
            {
                foreach (var P in Payments[K])
                {
                    var Property = EfxFramework.Property.GetPropertyByRenterId(P.RenterId);

                    if ((Property.PropertyId.HasValue && Property.PropertyId.Value > 0) && !String.IsNullOrEmpty(Property.YardiEndpoint))
                    {
                        var Request = new YardiRequest(Property.YardiUserName, Property.YardiPassword, Property.YardiServerName, Property.YardiDatabaseName, Property.YardiPlatform, K);
                        ExportPayments(Property.YardiEndpoint, Request, P);
                    }
                }
            }
        }

        private static void ExportPayments(string endpoint, YardiRequest request, Payment payment)
        {
            var Element = YardiRequest.BuildTransactionXml(new List<Payment> { payment });

            if (Element == null)
                return;

            var SoapRequest = request.GetSoapRequest("ImportResidentTransactions_Login", Element);

            using (var Client = new WebClient())
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var InputRequest = Encoding.UTF8.GetBytes(SoapRequest);

                Client.Headers[HttpRequestHeader.ContentType] = "text/xml; charset=utf-8";
                Client.Headers.Add("SOAPAction", "\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20/ImportResidentTransactions_Login\"");

                var RawResponse = Client.UploadData(endpoint, "POST", InputRequest);

                using (var OutputStream = new MemoryStream(RawResponse))
                {
                    var Xml = XDocument.Load(OutputStream);
                    var ResponseElement = YardiCommon.GetRootElement(Xml.Root, "Messages");

                    if (ResponseElement == null || !ResponseElement.HasElements) 
                        return;

                    foreach (var E in ResponseElement.Elements("Message"))
                    {
                        if (E.Value.ToLower().Contains("batch"))
                        {
                            var BatchId = E.Value.Remove(0, (E.Value.IndexOf("batch", StringComparison.OrdinalIgnoreCase) + 6));
                            BatchId = BatchId.Replace(".", "");

                            payment.IsPmsProcessed = true;
                            payment.PmsBatchId = BatchId;
                            Payment.Set(payment);
                        }

                        Console.WriteLine(E.Value);
                    }
                }
            }
        }

        // ReSharper disable UnusedMember.Local     Coming soon!
        private static void ExportReversal(string endpoint, YardiRequest request, Payment payment, ResidentTransactionsPropertyRT_CustomerRTServiceTransactionsTransactionsPaymentDetailReversalType reversalType)
        // ReSharper restore UnusedMember.Local
        {
            var Element = YardiRequest.BuildReversalTransactionXml(payment, reversalType);

            if (Element == null)
                return;

            var SoapRequest = request.GetSoapRequest("ImportResidentTransactions_Login", Element);

            using (var Client = new WebClient())
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var InputRequest = Encoding.UTF8.GetBytes(SoapRequest);

                Client.Headers[HttpRequestHeader.ContentType] = "text/xml; charset=utf-8";
                Client.Headers.Add("SOAPAction", "\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20/ImportResidentTransactions_Login\"");

                var RawResponse = Client.UploadData(endpoint, "POST", InputRequest);

                using (var OutputStream = new MemoryStream(RawResponse))
                {
                    var Xml = XDocument.Load(OutputStream);
                    var ResponseElement = YardiCommon.GetRootElement(Xml.Root, "Messages");

                    if (ResponseElement == null || !ResponseElement.HasElements)
                        return;

                    foreach (var E in ResponseElement.Elements("Message"))
                    {
                        Console.WriteLine(E.Value);
                    }
                }
            }
        }
    }
}
