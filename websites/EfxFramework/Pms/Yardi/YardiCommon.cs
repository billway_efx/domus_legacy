﻿using System;
using System.Linq;
using System.Xml.Linq;
//cakel
using System.Text;
using System.Xml;
using EfxFramework;
using System.IO;
using System.Net;
using System.Web;


namespace EfxFramework.Pms.Yardi
{
    public enum EmailType
    {
        Primary,
        Alternate1,
        Alternate2
    }

    public static class YardiCommon
    {
        public static string SetHttpsUrl(string url)
        {
            return (url.Replace("http", "https"));
        }

        public static XElement GetRootElement(XElement element, string localName)
        {
            XElement Element = null;

            if (element != null && element.Name.LocalName == localName)
                return element;

            if (element != null && element.HasElements)
            {
                foreach (var E in element.Elements())
                {
                    Element = GetRootElement(E, localName);
                }
            }

            return Element;
        }

        public static decimal GetAmount(string value)
        {
            decimal Result;

            if (!decimal.TryParse(value, out Result))
                Result = 0.0M;

            return Result;
        }

        public static bool IsValidFeeChargeCode(string chargeCode)
        {
            //cakel: 04/19/2016 Updated ref and removed ref to xml file.

            bool isValid = true;

            switch (chargeCode)
            {
                case "rent":
                    isValid = false;
                    break;
                case "subr/8":
                    isValid = false;
                    break;
                case "appf":
                    isValid = false;
                    break;
                case "deposit":
                    isValid = false;
                    break;
                default:
                    isValid = true;
                    break;

            }
            return isValid;
                
            //cakel: removed ref to file
            //var Doc = XDocument.Load("/IgnoredChargeCodes.xml");
            //var Root = Doc.Element("IgnoredChargeCodes");

            //if (Root != null && Root.HasElements)
            //{
            //    return Root.Elements().All(e => String.Compare(chargeCode, e.Value, StringComparison.OrdinalIgnoreCase) != 0);
            //}

            //return true;
        }


        //cakel: 00546
        //This function will check if the Applicant is in the list for the property as an applicant or future resident.
        public static bool IsValidApplicant(string ApplicantPmsId, int PropertyID)
        {
            bool ValidatedApplicant = false;

            string method = "GetResidentTransaction_Login";
            string _xml = WebServiceCall(method, ApplicantPmsId, PropertyID);


             byte[] byteArray = Encoding.ASCII.GetBytes(_xml);
             using (var OutputStream = new MemoryStream(byteArray))
             {
                 var Xml = XDocument.Load(OutputStream);

                 var Element = GetRootElement(Xml.Root, "RT_Customer");
                 var XmlError = GetRootElement(Xml.Root, "Message");

                 if (XmlError != null)
                 {
                     ValidatedApplicant = false;
                     return ValidatedApplicant;
                 }

                 foreach (XElement _RT_Customer in Element.Elements())
                 {
                     var NodeType = _RT_Customer.Name.LocalName;

                     if (NodeType == "Customers")
                     {
                         foreach (XElement _Customers in _RT_Customer.Elements())
                        {
                            string ResType = "";

                            var node2 = _Customers.Name.LocalName;

                            if (node2 == "Customer")
                            {
                                
                                ResType = _Customers.Attribute("Type").Value.ToString();
                                //we only want to check for these values
                                if (ResType == "future resident" || ResType == "applicant")
                                {
                                    ValidatedApplicant = true;
                                    return ValidatedApplicant;
                                 }
                                else
                                {
                                    ValidatedApplicant = false;
                                }


                            }

                        }

                     }

                 }

             }

             return ValidatedApplicant;
        }


        //cakel: 00546
        public static void ImportYardiApplicantsByProperty(int PropertyID)
        {
            var log = new RPO.ActivityLog();
            string method = "GetResidentTransactions_Login";
            string _xml = WebServiceCall(method, "", PropertyID);

            //_xml = _xml.Replace("Water & Sewer", "Water and Sewer");

            byte[] byteArray = Encoding.ASCII.GetBytes(_xml);
            log.WriteLog("System", "Start Applicant import for property " + PropertyID,1,"",false);
            using (var OutputStream = new MemoryStream(byteArray))
            {
                var Xml = XDocument.Load(OutputStream);

                var Element = GetRootElement(Xml.Root, "Property");
                var XmlError = GetRootElement(Xml.Root, "Message");

                if (XmlError != null)
                {
                    //This will log any error in the activity table
                    log.WriteLog("System", "Error Applicant import for property " + PropertyID + XmlError.ToString(), 1, "", false);

                }

                foreach (XElement _RT_Customer in Element.Elements())
                {
                    var RT_CustomerNode = _RT_Customer.Name.LocalName;

                    if (RT_CustomerNode == "RT_Customer")
                    {
                        foreach (XElement _Customers in _RT_Customer.Elements())
                        {
                           

                            var CustomersNode = _Customers.Name.LocalName;

                            if (CustomersNode == "Customers")
                            {
                                foreach(XElement _Customer in _Customers.Elements())
                                {

                                    string _Type = _Customer.Attribute("Type").Value.ToString();
                                    if (_Type == "future resident" || _Type == "applicant")
                                    {

                                    var CustomerNode = _Customer.Name.LocalName;

                                          if(CustomerNode == "Customer")
                                          {
                                     
                                                  foreach (XElement _CustomerDetail in _Customers.Elements())
                                                  {
                                                      string PropertyPmsID = "";
                                                      string ResidentPmsID = "";
                                                      string FirstName = "";
                                                      string LastName = "";
                                                      string Email = "";
                                                      string ResType = "";

                                                      ResType = _CustomerDetail.Attribute("Type").Value.ToString();
                                                      if (ResType == "future resident" || ResType == "applicant")
                                                      {

                                                          var CustomerDetailNode = _CustomerDetail.Name.LocalName;

                                                          foreach (XElement _Detail in _CustomerDetail.Elements())
                                                          {
                                                              var _detailNode = _Detail.Name.LocalName;

                                                              if (_detailNode == "CustomerID")
                                                              {
                                                                  ResidentPmsID = _Detail.FirstNode.ToString();
                                                              }

                                                              if (_detailNode == "Property")
                                                              {
                                                                  foreach (XElement _Property in _Detail.Elements())
                                                                  {
                                                                      var PropertyNode = _Property.Name.LocalName;
                                                                      if (PropertyNode == "PrimaryID")
                                                                      {
                                                                          PropertyPmsID = _Property.FirstNode.ToString();
                                                                      }

                                                                  }

                                                              }

                                                              if (_detailNode == "Name")
                                                              {
                                                                  foreach (XElement _Name in _Detail.Elements())
                                                                  {
                                                                      var _NameNode = _Name.Name.LocalName;
                                                                      if (_NameNode == "FirstName")
                                                                      {
                                                                          FirstName = _Name.FirstNode.ToString();
                                                                      }
                                                                      if (_NameNode == "LastName")
                                                                      {
                                                                          LastName = _Name.FirstNode.ToString();
                                                                      }
                                                                  }

                                                              }

                                                              if (_detailNode == "Address")
                                                              {
                                                                  foreach (XElement _Address in _Detail.Elements())
                                                                  {
                                                                      var _AddressNode = _Address.Name.LocalName;
                                                                      if (_AddressNode == "Email")
                                                                      {
                                                                          Email = _Address.FirstNode.ToString();

                                                                  
                                                                      }

                                                                  }

                                                              }

                                                          }

                                                          if (ResType == "future resident" || ResType == "applicant")
                                                          {

                                                              if (Email == null || Email == "")
                                                              {
                                                                  Email = Guid.NewGuid().ToString() + "@unknown.com";
                                                              }

                                                              int Appid = Applicant_V2.Applicant_InsertUpdate(0, PropertyID, FirstName, LastName, ResidentPmsID, true, Email);
                                                              Console.WriteLine("Yardi Applicant Import - " + ResidentPmsID);
                                                              if(Appid != 0)
                                                              {
                                                                  Applicant_V2.ApplicantApplication_InsertUpdate(Appid, PropertyID, 3, false, 6, DateTime.Now, DateTime.Now, "System");
                                                              }
                                                      


                                                          }
 
                                                      }

                                              
                                                  }

                                        
                                          }

                                    }
                              }

                             }

                          } 

                           
                        }

                    }


                }

                    log.WriteLog("System", "End Applicant import for property " + PropertyID, 1, "", false);
            }




        //cakel: 00546
        public static string WebServiceCall(string _MethodName, string ApplicantPmsID, int PropertyID)
        {
            var log = new RPO.ActivityLog();
            var P = new Property(PropertyID);
            string _xml = "";

            if (ApplicantPmsID != "")
            {
                _xml = GetSingleSoapRequest(_MethodName, ApplicantPmsID, PropertyID);

                log.WriteLog("System", "Start SoapRequest:  Yardi Applicant GetSingleSoapRequest", 1, "-999", false);

            }
            else
            {
                _xml = GetPropertySoapRequest(_MethodName, PropertyID);
                log.WriteLog("System", "Start SoapRequest:  Yardi Applicant GetPropertySoapRequest", 1, "-999", false);
            }
            

            WebRequest webRequest = WebRequest.Create(P.YardiEndpoint);
            HttpWebRequest httpRequest = (HttpWebRequest)webRequest;
            httpRequest.Method = "POST";
            httpRequest.ContentType = "text/xml; charset=utf-8";
            httpRequest.ContentLength = _xml.Length;
            httpRequest.Headers.Add("SOAPAction: http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20/" + _MethodName);
            Stream requestStream = httpRequest.GetRequestStream();
            //Create Stream and Complete Request             
            StreamWriter streamWriter = new StreamWriter(requestStream, Encoding.ASCII);

            streamWriter.Write(_xml);
            streamWriter.Close();


            //Get the Response    
            try
            {
                HttpWebResponse wr = (HttpWebResponse)httpRequest.GetResponse();
                string testresponse = wr.ToString();
                StreamReader srd = new StreamReader(wr.GetResponseStream());
                string resulXmlFromWebService = srd.ReadToEnd();
                //cakel: Testing - returning string as xml formated
                string decodedString = HttpUtility.HtmlDecode(resulXmlFromWebService);
                wr.Close();


                return resulXmlFromWebService;
               // return decodedString;

            }
            catch (Exception ex)
            {
                string WebRequestError = ex.ToString();
                log.WriteLog("System", "Error SoapRequest:  Yardi Applicant Request", 1, "-999", false);
              
                return "Error";
            }

        }


        public static string ExportWebserviceCall(string _MethodName, string _XML, int PropertyID)
        {

            var log = new RPO.ActivityLog();
            var P = new Property(PropertyID);
            string _xml = _XML;

            log.WriteLog("System", "Start Export Request:  Yardi Applicant Export Request", 1, "-999", false);

            XmlDocument xresp = new XmlDocument();
           
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.LoadXml(_xml);

            Byte[] Xmldata = System.Text.Encoding.ASCII.GetBytes(doc.OuterXml);
            System.Net.WebClient wc = new System.Net.WebClient();
            // Correct SSL / TLS error as this code was written in .net 2.0
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            wc.Headers.Add("Content-Type", "text/xml");
            Byte[] bresp;
            bresp = wc.UploadData(P.YardiEndpoint, Xmldata);

            string resp = System.Text.Encoding.ASCII.GetString(bresp);

            xresp.LoadXml(resp);
            string RespText = xresp.ToString();

            return resp;

        }


        //00546
        public static string GetSingleSoapRequest(string method, string ApplicantPMSID, int PropertyID)
        {

            var P = new Property(PropertyID);
            string UserName = P.YardiUserName;
            string Password = P.YardiPassword;
            string ServerName = P.YardiServerName;
            string Database = P.YardiDatabaseName;
            string Platform = P.YardiPlatform;
            string InterfaceEntity = EfxSettings.EfxYardiEntity;
            string InterfaceLicense = EfxSettings.EfxYardiLicenseInformation;
            string YardiPropertyId = P.PmsId;
            

            string myString =
           @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
              <soap:Body>
                <GetResidentTransaction_Login xmlns=""http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20"">
                  <UserName>##UserName</UserName>
                  <Password>##Password</Password>
                  <ServerName>##ServerName</ServerName>
                  <Database>##Database</Database>
                  <Platform>##Platform</Platform>
                  <YardiPropertyId>##YardiPropertyId</YardiPropertyId>
                  <TenantId>##TenantId</TenantId>
                  <InterfaceEntity>##InterfaceEntity</InterfaceEntity>
                  <InterfaceLicense>##InterfaceLicense</InterfaceLicense>
                </GetResidentTransaction_Login>
              </soap:Body>
            </soap:Envelope>";

            myString = myString.Replace("##UserName", UserName);
            myString = myString.Replace("##Password", Password);
            myString = myString.Replace("##ServerName", ServerName);
            myString = myString.Replace("##Database", Database);
            myString = myString.Replace("##Platform", Platform);
            myString = myString.Replace("##YardiPropertyId", YardiPropertyId);
            myString = myString.Replace("##TenantId", ApplicantPMSID);
            myString = myString.Replace("##InterfaceEntity", InterfaceEntity);
            myString = myString.Replace("##InterfaceLicense", InterfaceLicense);

            return myString;
        }



        //cakel:00546
        public static string GetPropertySoapRequest(string method, int PropertyID)
        {
            var P = new Property(PropertyID);
            string UserName = P.YardiUserName;
            string Password = P.YardiPassword;
            string ServerName = P.YardiServerName;
            string Database = P.YardiDatabaseName;
            string Platform = P.YardiPlatform;
            string InterfaceEntity = EfxSettings.EfxYardiEntity;
            string InterfaceLicense = EfxSettings.EfxYardiLicenseInformation;
            string YardiPropertyId = P.PmsId;


            string myString =
           @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
              <soap:Body>
                <GetResidentTransactions_Login xmlns=""http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20"">
                  <UserName>##UserName</UserName>
                  <Password>##Password</Password>
                  <ServerName>##ServerName</ServerName>
                  <Database>##Database</Database>
                  <Platform>##Platform</Platform>
                  <YardiPropertyId>##YardiPropertyId</YardiPropertyId>
                  <InterfaceEntity>##InterfaceEntity</InterfaceEntity>
                  <InterfaceLicense>##InterfaceLicense</InterfaceLicense>
                </GetResidentTransactions_Login>
              </soap:Body>
            </soap:Envelope>";

            myString = myString.Replace("##UserName", UserName);
            myString = myString.Replace("##Password", Password);
            myString = myString.Replace("##ServerName", ServerName);
            myString = myString.Replace("##Database", Database);
            myString = myString.Replace("##Platform", Platform);
            myString = myString.Replace("##YardiPropertyId", YardiPropertyId);
            myString = myString.Replace("##InterfaceEntity", InterfaceEntity);
            myString = myString.Replace("##InterfaceLicense", InterfaceLicense);

            return myString;


        }


        //cakel:00546
        public static string GetApplicantPaymentSoapRequest(string method, string _PaymentsXml, int PropertyID)
        {
            var P = new Property(PropertyID);
            string UserName = P.YardiUserName;
            string Password = P.YardiPassword;
            string ServerName = P.YardiServerName;
            string Database = P.YardiDatabaseName;
            string Platform = P.YardiPlatform;
            string InterfaceEntity = EfxSettings.EfxYardiEntity;
            string InterfaceLicense = EfxSettings.EfxYardiLicenseInformation;
            string YardiPropertyId = P.PmsId;


            string myString =
           @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
              <soap:Body>
                <ImportResidentTransactions_Login xmlns=""http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20"">
                    <UserName>##UserName</UserName>
                    <Password>##Password</Password>
                    <ServerName>##ServerName</ServerName>
                    <Database>##Database</Database>
                    <Platform>##Platform</Platform>
                    <YardiPropertyId>##YardiPropertyId</YardiPropertyId>
                    <InterfaceEntity>##InterfaceEntity</InterfaceEntity>
                    <InterfaceLicense>##InterfaceLicense</InterfaceLicense>
                    <TransactionXml>##Xml</TransactionXml>
                </ImportResidentTransactions_Login>
              </soap:Body>
            </soap:Envelope>";

            myString = myString.Replace("##UserName", UserName);
            myString = myString.Replace("##Password", Password);
            myString = myString.Replace("##ServerName", ServerName);
            myString = myString.Replace("##Database", Database);
            myString = myString.Replace("##Platform", Platform);
            myString = myString.Replace("##YardiPropertyId", YardiPropertyId);
            myString = myString.Replace("##InterfaceEntity", InterfaceEntity);
            myString = myString.Replace("##InterfaceLicense", InterfaceLicense);
            myString = myString.Replace("##Xml", _PaymentsXml);

            return myString;


        }



    }
}
