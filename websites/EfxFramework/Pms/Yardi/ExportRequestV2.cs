﻿using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace EfxFramework.Pms.Yardi
{
    public class ExportRequestV2
    {
        public static void SendExportFailureOrRemovedFromIntegrationEmail(int templateId, string infoToReplace, int propId)
        {
            if (propId <= 0) return;

            var connProp = new SqlConnection(ConnString);
            var getPropertyMainContacts = new SqlCommand("SELECT PRIMARYEMAILADDRESS " +
                                                                "FROM PROPERTYSTAFF P " +
                                                                "JOIN PropertyPropertyStaff S " +
                                                                "ON S.PropertyStaffId = P.PropertyStaffId " +
                                                                "WHERE S.IsMainContact = 1 AND S.PropertyId = " + propId, connProp);
            var dtt = new DataTable();
            connProp.Open();
            dtt.Load(getPropertyMainContacts.ExecuteReader());
            connProp.Close();
            var listOfEmailAddresses = (from DataRow row in dtt.Rows select row["PRIMARYEMAILADDRESS"].ToString() into emailAdd select new MailAddress(emailAdd)).ToList();

            var connEMail = new SqlConnection(ConnString);
            var commEmail =
                new SqlCommand("SELECT TEMPLATETEXT FROM EMAILTEMPLATES WHERE TEMPLATEID = " + templateId)
                {
                    Connection = connEMail
                };
            connEMail.Open();
            var dt = new DataTable();
            dt.Load(commEmail.ExecuteReader());
            connEMail.Close();
            var message = dt.Rows[0]["TEMPLATETEXT"].ToString();
            if (templateId == 10)
            {
                var realMessage = Convert.ToString(message).Replace("##PROPERTYNAME##", Convert.ToString(infoToReplace));
                realMessage = realMessage.Replace("[[ROOT]]", EmailTemplateService.EmailTemplateService.RootPath);
                const string subject = "Export Failure To Whom It May Concern";

                BulkMail.BulkMail.SendMailMessage(
                    null,
                    "EFX",
                    false,
                    EfxSettings.SendMailUsername,
                    EfxSettings.SendMailPassword,
                    new MailAddress(EfxSettings.SupportEmail),
                    subject,
                    Convert.ToString(realMessage),
                    listOfEmailAddresses);
            }
            else
            {
                var realMessage = message.Replace("[[ROOT]]", EmailTemplateService.EmailTemplateService.RootPath);
                realMessage = realMessage.Replace("##PMID##", infoToReplace);
                const string subject = "A Payment Has Been Removed From Your Batch";
                BulkMail.BulkMail.SendMailMessage(
                    null,
                    "EFX",
                    false,
                    EfxSettings.SendMailUsername,
                    EfxSettings.SendMailPassword,
                    new MailAddress(EfxSettings.SupportEmail),
                    subject,
                    Convert.ToString(realMessage),
                    listOfEmailAddresses);
            }
        }
        public static void ExportPayments()
        {
            var payments = BatchPayments();

            foreach (var kvp in payments)
            {
                var property = GetProperty(kvp.Key);

                if (IsValidYardiProperty(property))
                {
                    var request = new YardiRequest(property.YardiUserName, property.YardiPassword,
                        property.YardiServerName, property.YardiDatabaseName, property.YardiPlatform,
                        property.PmsId);
                    // ReSharper disable PossibleInvalidOperationException
                    ExportPayments(property.YardiEndpoint, request, kvp.Value, property.PropertyId.Value);
                    // ReSharper restore PossibleInvalidOperationException
                }
                else
                {
                    Console.WriteLine(@"Property is missing necessary information to process the Payment Export");
                    SendExportFailureOrRemovedFromIntegrationEmail(28, property.PropertyName, Convert.ToInt16(property.PropertyId));
                }
            }
        }

        public static void ExportPaymentsForProperty(int propertyIdToExport)
        {
            var payments = BatchPayments();

            foreach (var kvp in payments)
            {
                var property = GetProperty(kvp.Key);
                if (property.PropertyId != propertyIdToExport) continue;

                if (IsValidYardiProperty(property))
                {
                    var request = new YardiRequest(property.YardiUserName, property.YardiPassword,
                        property.YardiServerName, property.YardiDatabaseName, property.YardiPlatform, property.PmsId);
                    // ReSharper disable PossibleInvalidOperationException
                    ExportPayments(property.YardiEndpoint, request, kvp.Value, property.PropertyId.Value);
                    // ReSharper restore PossibleInvalidOperationException
                }
                else
                {
                    SendExportFailureOrRemovedFromIntegrationEmail(28, property.PropertyName, Convert.ToInt16(property.PropertyId));
                }
            }
        }
        private static void ExportPayments(string endpoint, YardiRequest request, List<Payment> payments, int propertyId)
        {
            var theProperty = new Property(propertyId);
            if (theProperty.YardiUseDepositDateAPI)
            {
                ExportPaymentsWithDepositDate(endpoint, request, payments, propertyId);
                return;
            }

            Console.WriteLine($@"Exporting payment data for PropertyId: {propertyId}");
            var transactionXml = YardiRequest.BuildTransactionXml(payments);

            if (transactionXml == null)
            {
                Console.WriteLine(@"Failed to assemble the XML for the transactions");
                SendExportFailureOrRemovedFromIntegrationEmail(28, theProperty.PropertyName, Convert.ToInt32(theProperty.PropertyId));
                return;
            }

            var soapRequest = request.GetSoapRequest("ImportResidentTransactions_Login", transactionXml);

            using (var client = new WebClient())
            {
                var inputRequest = Encoding.UTF8.GetBytes(soapRequest);

                client.Headers[HttpRequestHeader.ContentType] = "text/xml; charset=utf-8";
                client.Headers.Add("SOAPAction", "\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20/ImportResidentTransactions_Login\"");

                var rawResponse = client.UploadData(endpoint, "POST", inputRequest);
                try
                {
                    using (var outputStream = new MemoryStream(rawResponse))
                    {
                        var xml = XDocument.Load(outputStream);
                        LogExportSummaryAndDetail(soapRequest, xml, propertyId, payments, endpoint, request);
                    }
                }
                catch (Exception error)
                {
                    var theResponse = Encoding.Default.GetString(rawResponse);
                    Console.WriteLine(string.Concat(
                        "Error occurred while parsing/logging the response XML request. Endpoint: ", endpoint,
                        "; Request: ", soapRequest, "; Response: ", theResponse, "; PropertyId: ",
                        propertyId.ToString()));

                    Console.WriteLine(error.ToString());
                    Console.WriteLine(@"Attempting to send Export Failure email");
                    SendExportFailureOrRemovedFromIntegrationEmail(28, theProperty.PropertyName, Convert.ToInt32(theProperty.PropertyId));
                }
            }
        }

        private static void ExportPaymentsWithDepositDate(string endpoint, YardiRequest request, List<Payment> payments, int propertyId)
        {
            var conn = new SqlConnection(ConnString);
            var cmdPropertyName = new SqlCommand("SELECT PROPERTYNAME FROM PROPERTY WHERE PROPERTYID = " + propertyId, conn);
            conn.Open();
            var dt = new DataTable();
            dt.Load(cmdPropertyName.ExecuteReader());
            conn.Close();
            var result = dt.Rows[0]["PROPERTYNAME"].ToString();

            for (var i = 1; i <= 3; i++)
            {
                if (payments.Count(item => item.PaymentTypeId == i) <= 0) continue;
                Console.WriteLine(string.Concat("Exporting payment data (using DepositDate API) for PropertyId: ", propertyId, " PaymentType: ", i.ToString()));

                var transactionXml = YardiRequest.BuildTransactionXml(payments.Where(item => item.PaymentTypeId == i));

                if (transactionXml == null)
                {
                    SendExportFailureOrRemovedFromIntegrationEmail(28, result, propertyId);
                    return;
                }

                var firstTransaction = payments.First(item => item.PaymentTypeId == i);
                var depositDate = GetDepositDate(firstTransaction.TransactionDateTime);

                var depositMemo = "";
                switch (i)
                {
                    case 1:
                        depositMemo = "RPO CC " + depositDate;
                        break;
                    case 2:
                        depositMemo = "RPO ACH " + depositDate;
                        break;
                    case 3:
                        depositMemo = "RPO RBC " + depositDate; 
                        break;
                }

                var soapRequest = request.GetSoapRequestDepositDate("ImportResidentTransactions_DepositDate", transactionXml, depositDate, depositMemo);
                try
                {
                    using (var client = new WebClient())
                    {
                        var inputRequest = Encoding.UTF8.GetBytes(soapRequest);

                        client.Headers[HttpRequestHeader.ContentType] = "text/xml; charset=utf-8";
                        client.Headers.Add("SOAPAction", "\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20/ImportResidentTransactions_DepositDate\"");

                        var log = new RPO.ActivityLog();
                        log.WriteLog("AppDev", "SOAPRequest before Yardi Upload: " + soapRequest, 1, "-13");

                        var rawResponse = client.UploadData(endpoint, "POST", inputRequest);
                        using (var outputStream = new MemoryStream(rawResponse))
                        {
                            var xmlDoc = XDocument.Load(outputStream);
                            LogExportSummaryAndDetail(soapRequest, xmlDoc, propertyId, payments, endpoint, request);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(
                        $@"Error occurred while exporting payment data using DepositDate API for PropertyId: {propertyId}");
                    Console.WriteLine(ex.ToString());
                    Console.WriteLine(@"Attempting to send Export Failure email");
                    SendExportFailureOrRemovedFromIntegrationEmail(28, result, propertyId);
                }
            }
        }
        private static string ConnString => System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public static string GetDepositDate(DateTime transactionDate)
        {
            var con = new SqlConnection(ConnString);
            var theSql = "Select CONVERT(NVARCHAR(20),dbo.GetESTDateTime(dbo.udf_CalculateDepositDate_V2(dbo.udf_CalculatePaymentDate_V2('";
            theSql = theSql + transactionDate + "' ))), 101) AS DepositDate";

            var depositDate = "";
            var com = new SqlCommand(theSql, con)
            {
                CommandType = CommandType.Text
            };
            try
            {
                con.Open();
                var reader = com.ExecuteReader(CommandBehavior.CloseConnection);

                while (reader.Read())
                {
                    depositDate = Convert.ToDateTime(reader[0].ToString()).ToString("yyyy-MM-dd");
                }
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }
            return depositDate;
        }

        private static void RemoveFailedPaymentsAndReprocess(string requestXml, IReadOnlyDictionary<int, string> failedPayments, List<Payment> payments, string endpoint, YardiRequest request, int propertyId)
        {
            var xmlDoc = XDocument.Parse(requestXml);
            var transactions = YardiCommon.GetRootElement(xmlDoc.Root, "RTServiceTransactions");
            var paymentIndex = 0;
            try
            {
                if (transactions == null || !transactions.HasElements)
                    return;

                foreach (var xElement in from T in transactions.Elements() where T.Name.LocalName == "Transactions" from p in T.Elements() where p.Name.LocalName == "Payment" select p)
                {
                    if (failedPayments.ContainsKey(paymentIndex) && failedPayments[paymentIndex].ToLower().Contains("error"))
                    {
                        var transactionId = (from d in xElement.Elements() where d.Name.LocalName == "Detail" from dn in d.Elements() where dn.Name.LocalName == "DocumentNumber" select dn).FirstOrDefault();

                        if (transactionId != null)
                        {
                            var payment = payments.FirstOrDefault(p => p.TransactionId == transactionId.Value);

                            if (payment != null)
                            {
                                payments.Remove(payment);
                                var paymentId = payment.PaymentId.ToString();
                                SendExportFailureOrRemovedFromIntegrationEmail(29, paymentId, propertyId);
                            }
                        }
                    }

                    paymentIndex++;
                }

                ExportPayments(endpoint, request, payments, propertyId);
            }

            catch (Exception error)
            {
                Console.WriteLine($@"Payment Failed To Remove From Integration. Exception: {error}");
            }
        }

        private static Dictionary<string, List<Payment>> BatchPayments()
        {
            //TODO: Update this with the new Payment Table when it becomes available
            var payments = Payment.GetAllUnprocessedPmsPaymentsByPmsTypeId(1).Where(p => p.ResponseCode == "0" && p.ResponseMessage != null && p.ResponseMessage.ToLower().Contains("success") && !p.IsPmsProcessed).ToList();
            var batch = new Dictionary<string, List<Payment>>();


            foreach (var payment in payments)
            {
                var renter = new Renter(payment.RenterId);

                if (renter.RenterId <= 0 || !IsValidYardiRenter(renter)) continue;

                var property = Property.GetPropertyByRenterId(renter.RenterId);

                if (!property.PropertyId.HasValue || property.PropertyId.Value <= 0) continue;

                var key =
                    $"RPO|{property.PropertyId.Value}|{payment.PaymentTypeId}|{GetPaymentDateString(payment.TransactionDateTime)}";

                if (batch.ContainsKey(key))
                    batch[key].Add(payment);
                else
                    batch.Add(key, new List<Payment> { payment });
            }

            return batch;
        }
        private static bool IsValidYardiProperty(Property property)
        {
            if (!property.PropertyId.HasValue)
                return false;

            if (property.PropertyId.Value < 1)
                return false;

            if (property.PmsTypeId != 1)
                return false;

            if (String.IsNullOrEmpty(property.PmsId))
                return false;

            if (String.IsNullOrEmpty(property.YardiDatabaseName))
                return false;

            if (String.IsNullOrEmpty(property.YardiEndpoint))
                return false;

            if (String.IsNullOrEmpty(property.YardiPassword))
                return false;

            if (String.IsNullOrEmpty(property.YardiPlatform))
                return false;

            if (String.IsNullOrEmpty(property.YardiServerName))
                return false;

            if (String.IsNullOrEmpty(property.YardiUserName))
                return false;

            return true;
        }

        private static bool IsValidYardiRenter(Renter renter)
        {
            return !String.IsNullOrEmpty(renter.PmsPropertyId) && !String.IsNullOrEmpty(renter.PmsId) && renter.PmsTypeId == 1;
        }

        private static Property GetProperty(string key)
        {
            var keyArray = key.Split('|');

            if (keyArray.Length < 2)
                return new Property();

            return !int.TryParse(keyArray[1], out var propertyId) ? new Property() : new Property(propertyId);
        }

        public static string GetPaymentDateString(DateTime transactionDate)
        {
            return GetPaymentDate(transactionDate).ToShortDateString();
        }

        public static DateTime GetPaymentDate(DateTime transactionDate)
        {
            var date = TimeZoneInfo.ConvertTimeFromUtc(transactionDate, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var cutOffDate = new DateTime(date.Year, date.Month, date.Day, 18, 0, 0);

            if (date > cutOffDate)
                date = date.AddDays(1);

            return date;
        }

        private static int GetFailedItemNumber(string elementValue)
        {
            var itemNumber = 0;

            if (!elementValue.ToLower().Contains("item number=")) return itemNumber;
            var tempString = string.Empty;
            var startIndex = elementValue.IndexOf("item number=", StringComparison.OrdinalIgnoreCase);
            elementValue = elementValue.ToLower();
            elementValue = elementValue.Replace("item number=", "");


            for (var I = startIndex; I < elementValue.Length; I++)
            {
                tempString = string.Concat(tempString, elementValue[I]);
                if (int.TryParse(tempString, out var tempNumber))
                    itemNumber = tempNumber;
                else
                    break;
            }

            return itemNumber;
        }

        private static int GetPaymentCount(string requestXml)
        {
            var xDocument = XDocument.Parse(requestXml);
            var transactions = YardiCommon.GetRootElement(xDocument.Root, "RTServiceTransactions");

            if (transactions == null || !transactions.HasElements)
                return 0;

            return (from T in transactions.Elements() where T.Name.LocalName == "Transactions" from p in T.Elements() where p.Name.LocalName == "Payment" select p).Count();
        }

        private static DateTime CalculateDepositDate(DateTime depositDate)
        {
            switch (depositDate.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                case DayOfWeek.Monday:
                    depositDate = depositDate.AddDays(2);
                    break;
                case DayOfWeek.Tuesday:
                    depositDate = depositDate.AddDays(1);
                    break;
            }

            if (depositDate.IsUnitedStatesBankingHoliday())
                depositDate = CalculateDepositDate(depositDate.AddDays(1));

            return depositDate;
        }

        private static decimal GetPaymentsTotal(List<Payment> payments)
        {
            var total = 0.00M;
            total += payments.Sum(p => p.RentAmount ?? 0.00M);
            total += payments.Sum(p => p.OtherAmount1.HasValue && p.OtherDescription1 != null && !p.OtherDescription1.ToLower().Contains("convenience fee") ? p.OtherAmount1.Value : 0.00M);
            total += payments.Sum(p => p.OtherAmount2.HasValue && p.OtherDescription2 != null && !p.OtherDescription2.ToLower().Contains("convenience fee") ? p.OtherAmount2.Value : 0.00M);
            total += payments.Sum(p => p.OtherAmount3.HasValue && p.OtherDescription3 != null && !p.OtherDescription3.ToLower().Contains("convenience fee") ? p.OtherAmount3.Value : 0.00M);
            total += payments.Sum(p => p.OtherAmount4.HasValue && p.OtherDescription4 != null && !p.OtherDescription4.ToLower().Contains("convenience fee") ? p.OtherAmount4.Value : 0.00M);
            total += payments.Sum(p => p.OtherAmount5.HasValue && p.OtherDescription5 != null && !p.OtherDescription5.ToLower().Contains("convenience fee") ? p.OtherAmount5.Value : 0.00M);
            return total;
        }

        private static DateTime GetDepositDate(Payment payment)
        {
            return payment == null ? DateTime.UtcNow : CalculateDepositDate(GetPaymentDate(payment.TransactionDateTime).AddDays(2));
        }

        private static void LogExportSummaryAndDetail(string requestXml, XDocument responseXml, int propertyId, List<Payment> payments, string endpoint, YardiRequest request)
        {
            var status = IsSuccessfulExport(responseXml, out var batchId, out var failed);
            var summary = LogExportPaymentSummary(propertyId, status, batchId, requestXml, responseXml, payments);

            LogExportPaymentDetails(failed, requestXml, summary);

            if (status)
                LogSuccessfulPayments(payments, summary);

            if (failed.Count > 0)
                RemoveFailedPaymentsAndReprocess(requestXml, failed, payments, endpoint, request, propertyId);
        }

        private static PaymentExportSummary LogExportPaymentSummary(int propertyId, bool status, string batchId, string requestXml, XDocument responseXml, List<Payment> payments)
        {
            var firstPayment = payments.FirstOrDefault();

            var summary = new PaymentExportSummary
            {
                PropertyId = propertyId,
                Amount = GetPaymentsTotal(payments),
                Status = status,
                BatchId = batchId,
                RequestXml = requestXml,
                ResponseXml = responseXml.ToString(),
                PaymentTypeId = firstPayment?.PaymentTypeId ?? 1,
                DepositDate = GetDepositDate(firstPayment),
                ExportedPaymentCount = status ? GetPaymentCount(requestXml) : 0,
                ExportDate = DateTime.UtcNow
            };

            summary.BuildBatchDetails();
            summary = new PaymentExportSummary(PaymentExportSummary.Set(summary));

            return summary;
        }

        private static void LogSuccessfulPayments(IEnumerable<Payment> payments, PaymentExportSummary summary)
        {
            if (!summary.Status)
                return;

            foreach (var p in payments)
            {
                p.PmsBatchId = summary.BatchId;
                p.IsPmsProcessed = true;
                Payment.Set(p);
            }
        }

        private static void LogExportPaymentDetails(IReadOnlyDictionary<int, string> failedItems, string requestXml, PaymentExportSummary summary)
        {
            var xDocument = XDocument.Parse(requestXml);
            var transactions = YardiCommon.GetRootElement(xDocument.Root, "RTServiceTransactions");
            var paymentIndex = 0;

            if (transactions == null || !transactions.HasElements)
                return;

            foreach (var p in from T in transactions.Elements() where T.Name.LocalName == "Transactions" from p in T.Elements() where p.Name.LocalName == "Payment" select p)
            {
                string failedPaymentMessage = null;
                if (failedItems.ContainsKey(paymentIndex))
                    failedPaymentMessage = failedItems[paymentIndex];

                PaymentExportDetail.Set(BuildDetail(summary.PaymentExportSummaryId, summary.Status, p.ToString(), failedPaymentMessage));
                paymentIndex++;
            }
        }

        private static bool IsSuccessfulExport(XDocument responseXml, out string batchId, out Dictionary<int, string> failedItems)
        {
            var status = true;
            var responseElement = YardiCommon.GetRootElement(responseXml.Root, "Messages");
            batchId = string.Empty;
            failedItems = new Dictionary<int, string>();

            if (responseElement == null || !responseElement.HasElements) return true;

            foreach (var e in responseElement.Elements("Message"))
            {
                switch (e.Attribute("messageType")?.Value.ToLower())
                {
                    case "error":
                    {
                        status = false;
                        var itemNumber = GetFailedItemNumber(e.Value);

                        if (itemNumber > 0)
                        {
                            if (failedItems.ContainsKey(itemNumber))
                                failedItems[itemNumber] = $"Error: {failedItems[itemNumber]}, {e.Value}";
                            else
                            {
                                try   // Salcedo - added logic to try and catch the error that is aborting the export process
                                {
                                    failedItems.Add(itemNumber, $"Error: {failedItems[itemNumber]}, {e.Value}");
                                }
                                catch
                                {
                                    failedItems.Add(itemNumber, $"Error: Unknown, {e.Value}");
                                }
                            }
                        }
                    }
                        break;
                    case "warning":
                    {
                        var itemNumber = GetFailedItemNumber(e.Value);

                        if (itemNumber > 0)
                        {
                            if (failedItems.ContainsKey(itemNumber))
                                failedItems[itemNumber] = $"Warning: {failedItems[itemNumber]}, {e.Value}";
                            else
                                failedItems.Add(itemNumber, $"Warning: {failedItems[itemNumber]}, {e.Value}");
                        }
                    }
                        break;
                    case "fyi":
                    {
                        // Salcedo - 7/26/0216 - added code to prevent error if "batch" is not in the string
                        if (e.Value.Contains("batch"))
                        {
                            batchId = e.Value.Remove(0, (e.Value.IndexOf("batch", StringComparison.OrdinalIgnoreCase) + 6));
                            batchId = batchId.Replace(".", "");
                        }
                    }
                        break;
                }

                Console.WriteLine(e.Value);
            }

            return status;
        }

        private static PaymentExportDetail BuildDetail(int paymentExportSummaryId, bool success, string xmlDetail, string failedExportDescription)
        {
            return new PaymentExportDetail
            {
                PaymentExportSummaryId = paymentExportSummaryId,
                SuccessfulExport = success,
                PaymentDetailXml = xmlDetail,
                FailedExportDescription = failedExportDescription
            };
        }
        
        public static SqlDataReader GetAllUnprocessedApplicantPayments(int propertyId, int pmsTypeId)
        {
            SqlConnection con = new SqlConnection(ConnString);
            SqlCommand com = new SqlCommand("usp_Payment_GetAllUnProcAPPLICANTPaymentsByPmsTypeId", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@PropertyID", SqlDbType.Int).Value = propertyId;
            com.Parameters.Add("@PmsTypeId", SqlDbType.Int).Value = pmsTypeId;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }

        }

        public static string GetYardiApplicantPaymentXml(int propertyId, int pmsTypeId)
        {
            var output = new StringBuilder();
            var settings = new XmlWriterSettings {Indent = true, OmitXmlDeclaration = true};
            var writer = XmlWriter.Create(output, settings);

            var reader = GetAllUnprocessedApplicantPayments(propertyId, pmsTypeId);

            if (reader.HasRows)
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("ResidentTransactions");
                writer.WriteStartElement("Property");
                writer.WriteStartElement("RT_Customer");
                writer.WriteStartElement("RTServiceTransactions");

                while (reader.Read())
                {
                    writer.WriteStartElement("Transactions");
                    writer.WriteStartElement("Payment");
                    writer.WriteAttributeString("Type", reader["PaymentType"].ToString());
                    writer.WriteStartElement("Detail");

                    writer.WriteElementString("DocumentNumber", reader["InternalTransactionId"].ToString());
                    writer.WriteElementString("TransactionDate", reader["TransactionDate"].ToString());
                    writer.WriteElementString("CustomerID", reader["PmsID"].ToString());
                    writer.WriteElementString("PaidBy", reader["PaidBy"].ToString());
                    writer.WriteElementString("Amount", reader["Amount"].ToString());
                    writer.WriteElementString("Comment", reader["Comment"].ToString());
                    writer.WriteElementString("PropertyPrimaryID", reader["PropertyPmsID"].ToString());

                    writer.WriteEndElement();//Detail
                    writer.WriteEndElement();//Payment
                    writer.WriteEndElement();//Transactions
                }

                writer.WriteEndElement();//Property
                writer.WriteEndElement();//ResidentTransactions
                writer.WriteEndElement();//RT_Customer
                writer.WriteEndElement();//RTServiceTransactions

                writer.WriteEndDocument();
                writer.Flush();

                //For testing output of string 
                var testString = output.ToString();

                return testString;
            }

            return "";
        }

        public static void ExportApplicantPaymentByPropertyId(int propertyId)
        {
            var log = new RPO.ActivityLog();
            //"1" is for Yardi
            const int pmsTypeId = 1;
            const string method = "ImportResidentTransactions_Login";
            var paymentsXml = GetYardiApplicantPaymentXml(propertyId, pmsTypeId);
            if (paymentsXml == "")
            {
                //record that a property has no payments
                log.WriteLog("System", "No Payments to export for PropertyID:" + propertyId, 1, "-999", false);
                return;

            }

            // get full string for submission
            var requestXml = YardiCommon.GetApplicantPaymentSoapRequest(method, paymentsXml, propertyId);

            //submit xml to return export results
            var responseXml = YardiCommon.ExportWebserviceCall(method, requestXml, propertyId);

            var batchId = string.Empty;
            var byteArray = Encoding.ASCII.GetBytes(responseXml);
            using (var outputStream = new MemoryStream(byteArray))
            {
                var xDocument = XDocument.Load(outputStream);

                var element = YardiCommon.GetRootElement(xDocument.Root, "Messages");
                foreach (var exportMessage in element.Elements())
                {
                    switch (exportMessage.Attribute("messageType")?.Value.ToLower())
                    {
                        case "error":
                            {
                                batchId = "Error";
                            }
                            break;
                        case "warning":
                            {
                                batchId = "Warning";
                            }
                            break;
                        case "fyi":
                            if (exportMessage.Value != "")
                            {
                                // Salcedo - 7/26/2016 - added code to check for existence of "batch" in string
                                if (exportMessage.Value.Contains("batch"))
                                {
                                    batchId = exportMessage.Value.Remove(0, (exportMessage.Value.IndexOf("batch", StringComparison.OrdinalIgnoreCase) + 6));
                                    batchId = batchId.Replace(".", "");
                                    var returnBatchId = batchId;
                                    //insert import detail
                                    AMSI.AmsiRequest.AmsiInsertResidentImportSummary(propertyId, DateTime.Now, 0, 0, requestXml, responseXml);
                                }
                            }
                            break;
                    }
                }

                if (batchId == "Error" && batchId == "Warning") return;

                var paymentByteArray = Encoding.ASCII.GetBytes(requestXml);
                using (var paymentStream = new MemoryStream(paymentByteArray))
                {
                    var paymentXml = XDocument.Load(paymentStream);
                    var paymentElement = YardiCommon.GetRootElement(paymentXml.Root, "RTServiceTransactions");
                    foreach (var paymentList in paymentElement.Elements())
                    {
                        var rtNode = paymentList.Name.LocalName;

                        if (rtNode != "Transactions") continue;

                        foreach (var transactions in paymentList.Elements())
                        {
                            var transNode = transactions.Name.LocalName;

                            if (transNode != "Payment") continue;

                            foreach (var payments in transactions.Elements())
                            {
                                var paymentNode = payments.Name.LocalName;

                                if (paymentNode != "Detail") continue;

                                foreach (var details in payments.Elements())
                                {
                                    var detailNode = details.Name.LocalName;

                                    if (detailNode != "DocumentNumber") continue;

                                    var transId = details.FirstNode.ToString();
                                    Applicant_UpdateFeePaymentWithBatchID(transId, batchId);
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void ExportAllPropertiesApplicationPayments()
        {
            var yardiAppPayments = GetPropertiesForAppPayments();

            if (yardiAppPayments.Tables[0].Rows.Count <= 0) return;

            foreach (DataRow dr in yardiAppPayments.Tables[0].Rows)
            {
                var propId = int.Parse(dr["PropertyId"].ToString());
                ExportApplicantPaymentByPropertyId(propId);
            }
        }

        public static DataSet GetPropertiesForAppPayments()
        {
            var ds = new DataSet();
            new SqlConnection(ConnString);
            var da = new SqlDataAdapter();
            using (var conn = new SqlConnection(ConnString))
            {
                var sqlComm = new SqlCommand("usp_Applicant_GetPropertiesForAppPayments", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                da.SelectCommand = sqlComm;
                da.Fill(ds);
            }
            return ds;


        }

        private static void Applicant_UpdateFeePaymentWithBatchID(string transId, string batchId)
        {
            var con = new SqlConnection(ConnString);
            var com = new SqlCommand("usp_Applicant_UpdateFeePaymentWithBatchID", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            com.Parameters.Add("@InternalTransID", SqlDbType.NVarChar, 20).Value = transId;
            com.Parameters.Add("@BatchID", SqlDbType.NVarChar, 20).Value = batchId;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }

        }
    }
}
