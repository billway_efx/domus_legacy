﻿using System.Data.SqlTypes;
using EfxFramework.Pms.Yardi.Core;
using EfxFramework.Security.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EfxFramework.Pms.Yardi
{
    public class YardiResidentData
    {
        public Renter Resident { get; private set; }
        public Lease ResidentLease { get; private set; }
        public Dictionary<string, Roommate> Roommates { get; private set; }
        public Dictionary<string, Lease> RoomateLeases { get; private set; }

        public YardiResidentData()
        {
            Resident = null;
            ResidentLease = new Lease();
            Roommates = new Dictionary<string, Roommate>();
            RoomateLeases = new Dictionary<string, Lease>();
        }

        public static YardiResidentData SetYardiResidentTransaction(ResidentTransactionsPropertyRT_Customer yardiCustomer)
        {
            var ResidentData = new YardiResidentData();

            ResidentData.SetYardiResidentData(yardiCustomer);
            ResidentData.SetYardiResidentBalances(yardiCustomer);
            ResidentData.SetYardiResidentRoommateData(yardiCustomer);
            ResidentData.ResidentLease.NumberOfTenants = ResidentData.Roommates.Count + 1;

            return ResidentData;
        }

        private void SetYardiResidentData(ResidentTransactionsPropertyRT_Customer yardiCustomer)
        {
            foreach (var Cust in yardiCustomer.Customers.Where(cust => IsValidRenter(cust.Type) && cust.CustomerID == yardiCustomer.CustomerID))
            {
                SetResident(Cust, yardiCustomer.PaymentAccepted);
                SetResidentLease(Cust);
            }
        }

        private void SetYardiResidentRoommateData(ResidentTransactionsPropertyRT_Customer yardiCustomer)
        {
            foreach (var Cust in yardiCustomer.Customers.Where(cust => IsValidRenter(cust.Type) && (cust.CustomerID != yardiCustomer.CustomerID && Resident != null)))
            {
                if (!Roommates.ContainsKey(Cust.CustomerID))
                {
                    Roommates.Add(Cust.CustomerID, BuildRoommate(Cust));
                    Console.WriteLine("Roommate {0} of Yardi Customer {1} imported.", Cust.CustomerID, yardiCustomer.CustomerID);
                }
                else
                    Roommates[Cust.CustomerID] = BuildRoommate(Cust);

                if (!RoomateLeases.ContainsKey(Cust.CustomerID))
                {
                    RoomateLeases.Add(Cust.CustomerID, BuildLease(Cust));
                    Console.WriteLine("Lease information for Roommate {0} of Yardi Customer {1} imported.", Cust.CustomerID, yardiCustomer.CustomerID);
                }
                else
                    RoomateLeases[Cust.CustomerID] = BuildLease(Cust);
            }
        }

        private void SetResident(CustomersTypeCustomer yardiCustomer, string paymentAccepted)
        {
            Resident = BuildRenter(yardiCustomer, paymentAccepted);
            Console.WriteLine("\nResident information for Yardi Customer {0} was imported.", yardiCustomer.CustomerID);
        }

        private void SetResidentLease(CustomersTypeCustomer yardiCustomer)
        {
            if (yardiCustomer.Lease == null)
            {
                ResidentLease = new Lease();
                return;
            }

            ResidentLease = BuildLease(yardiCustomer);
            Console.WriteLine("Lease Information for Yardi Customer {0} was imported.", yardiCustomer.CustomerID);
        }

        private void SetYardiResidentBalances(ResidentTransactionsPropertyRT_Customer yardiCustomer)
        {
            if (Resident == null) 
                return;

            //Salcedo - 2/10/2014 - Sometimes the RTServiceTransactions element is missing from the response XML. Added code to test for that specific condition.
            //If this occurs, it means our customer has not checked the "Include Open Charges" option on the Yardi Interfaces->Configuration page.
            if (yardiCustomer.RTServiceTransactions != null)
            {
                ResidentLease.CurrentBalanceDue = yardiCustomer.RTServiceTransactions.Transactions != null ? GetBalanceDue(yardiCustomer.RTServiceTransactions) : 0.0M;
                
                //Salcedo - 5/1/2014 - correct the console output to actually show the current balance due, instead of the beginning balance
                Decimal TempBalanceDue = 0;
                if (ResidentLease.CurrentBalanceDue.HasValue)
                {
                    TempBalanceDue = ResidentLease.CurrentBalanceDue.Value;
                }
                //Console.WriteLine("Current Balance Due for Yardi Customer {0} of {1} was imported.", yardiCustomer.CustomerID, ResidentLease.BeginningBalance.ToString("C"));
                Console.WriteLine("Current Balance Due for Yardi Customer {0} of {1} was imported.", yardiCustomer.CustomerID, TempBalanceDue.ToString("C"));
            }
            else
            {
                throw new Exception(String.Format("Yardi Customer {0} is missing RTServiceTransactions element. " +
                    "Verify client has checked the 'Include Open Charges' option on the Yardi Interfaces -> Configuration page.", 
                    yardiCustomer.CustomerID));
            }
        }

        private static Lease BuildLease(CustomersTypeCustomer yardiCustomer)
        {
            var YardiCustomerLease = yardiCustomer.Lease;

            return new Lease
            {
                UnitNumber = yardiCustomer.Property != null && !String.IsNullOrEmpty(yardiCustomer.Property.UnitID) ? yardiCustomer.Property.UnitID : string.Empty,
                RentAmount = YardiCustomerLease.CurrentRent,
                RentDueDayOfMonth = YardiCustomerLease.LeaseFromDate.Day,
                StartDate = YardiCustomerLease.LeaseFromDate,
                EndDate = YardiCustomerLease.LeaseToDate,
                BeginningBalance = 0.00M,
                BeginningBalanceDate = YardiCustomerLease.LeaseFromDate,
                ExpectedMoveInDate = YardiCustomerLease.ExpectedMoveInDateSpecified ? GetDate(YardiCustomerLease.ExpectedMoveInDate) : null,
                ExpectedMoveOutDate = YardiCustomerLease.ExpectedMoveOutDateSpecified ? GetDate(YardiCustomerLease.ExpectedMoveOutDate) : null,
                ActualMoveInDate = YardiCustomerLease.ActualMoveInSpecified ? GetDate(YardiCustomerLease.ActualMoveIn) : null,
                ActualMoveOutDate = YardiCustomerLease.ActualMoveOutSpecified ? GetDate(YardiCustomerLease.ActualMoveOut) : null,
                LeaseSignDate = YardiCustomerLease.LeaseSignDateSpecified ? GetDate(YardiCustomerLease.LeaseSignDate) : null,
                CurrentBalanceDueLastUpdated = DateTime.UtcNow
            };
        }

        private static Renter BuildRenter(CustomersTypeCustomer yardiCustomer, string paymentAccepted)
        {
            var Renter = new Renter();
            var Address = GetAddress(yardiCustomer.Address);
            var AcceptedPaymentTypeId = 1;

            if (!String.IsNullOrEmpty(paymentAccepted))
            {
                switch (paymentAccepted)
                {
                    case "0":
                        AcceptedPaymentTypeId = 1;
                        break;
                        
                    case "1":
                        AcceptedPaymentTypeId = 4;
                        break;

                    case "2":
                        AcceptedPaymentTypeId = 3;
                        break;

                    default:
                        AcceptedPaymentTypeId = 1;
                        break;
                }
            }

            Renter.SetPassword(AuthenticationTools.GenerateRandomPassword());
            Renter.Prefix = yardiCustomer.Name.NamePrefix;
            Renter.FirstName = yardiCustomer.Name.FirstName;
            Renter.MiddleName = yardiCustomer.Name.MiddleName;
            Renter.LastName = yardiCustomer.Name.LastName;
            Renter.Suffix = yardiCustomer.Name.NameSuffix;
            Renter.MainPhoneNumber = yardiCustomer.Phone != null ? GetPhoneNumber(phoneinfo.home, yardiCustomer.Phone) : null;
            Renter.MobilePhoneNumber = yardiCustomer.Phone != null ? GetPhoneNumber(phoneinfo.cell, yardiCustomer.Phone) : null;
            Renter.FaxNumber = yardiCustomer.Phone != null ? GetPhoneNumber(phoneinfo.fax, yardiCustomer.Phone) : null;
            Renter.PrimaryEmailAddress = yardiCustomer.Address != null ? GetEmailAddress(yardiCustomer.Address) : String.Format("{0}@unknown.com", Guid.NewGuid());
            Renter.StreetAddress = Address != null ? Address.Address1 : string.Empty;
            Renter.Unit = yardiCustomer.Property != null && !String.IsNullOrEmpty(yardiCustomer.Property.UnitID) ? yardiCustomer.Property.UnitID : string.Empty;
            Renter.City = Address != null ? Address.City : string.Empty;
            Renter.StateProvinceId = GetStateId(Address);
            Renter.PostalCode = Address != null ? Address.PostalCode : string.Empty;
            Renter.IsCreditCardTestRenter = false;
            Renter.IsCreditCardApproved = false;
            Renter.PmsTypeId = 1;
            Renter.PmsId = !String.IsNullOrEmpty(yardiCustomer.CustomerID) ? yardiCustomer.CustomerID : string.Empty;
            Renter.IsActive = true;
            Renter.PmsPropertyId = yardiCustomer.Property != null ? yardiCustomer.Property.PrimaryID : string.Empty;
            Renter.AcceptedPaymentTypeId = AcceptedPaymentTypeId;

            return Renter;
        }

        public static Roommate BuildRoommate(CustomersTypeCustomer yardiCustomer)
        {
            var Roommate = new Roommate();
            var Address = GetAddress(yardiCustomer.Address);

            Roommate.Prefix = yardiCustomer.Name.NamePrefix;
            Roommate.FirstName = yardiCustomer.Name.FirstName;
            Roommate.MiddleName = yardiCustomer.Name.MiddleName;
            Roommate.LastName = yardiCustomer.Name.LastName;
            Roommate.Suffix = yardiCustomer.Name.NameSuffix;
            Roommate.MainPhoneNumber = yardiCustomer.Phone != null ? GetPhoneNumber(phoneinfo.home, yardiCustomer.Phone) : null;
            Roommate.MobilePhoneNumber = yardiCustomer.Phone != null ? GetPhoneNumber(phoneinfo.cell, yardiCustomer.Phone) : null;
            Roommate.FaxNumber = yardiCustomer.Phone != null ? GetPhoneNumber(phoneinfo.fax, yardiCustomer.Phone) : null;
            Roommate.PrimaryEmailAddress = yardiCustomer.Address != null ? GetEmailAddress(yardiCustomer.Address) : String.Format("{0}@unknown.com", Guid.NewGuid());
            Roommate.Photo = new byte[0];
            Roommate.StreetAddress = Address != null ? Address.Address1 : string.Empty;
            Roommate.Unit = yardiCustomer.Property != null && !String.IsNullOrEmpty(yardiCustomer.Property.UnitID) ? yardiCustomer.Property.UnitID : string.Empty;
            Roommate.City = Address != null ? Address.City : string.Empty;
            Roommate.StateProvinceId = GetStateId(Address);
            Roommate.PostalCode = Address != null ? Address.PostalCode : string.Empty;
            Roommate.PmsTypeId = 1;
            Roommate.PmsId = !String.IsNullOrEmpty(yardiCustomer.CustomerID) ? yardiCustomer.CustomerID : string.Empty;
            Roommate.IsActive = true;
            Roommate.ImportDateTime = DateTime.UtcNow;
            Roommate.ExportDateTime = null;
            Roommate.MoveInDate = yardiCustomer.Lease.ActualMoveInSpecified ? GetDate(yardiCustomer.Lease.ActualMoveIn) : null;
            Roommate.MoveOutDate = yardiCustomer.Lease.ActualMoveOutSpecified ? GetDate(yardiCustomer.Lease.ActualMoveOut) : null;
            Roommate.IsResponsibleForLease = yardiCustomer.Lease != null && yardiCustomer.Lease.ResponsibleForLease;

            return Roommate;
        }

        private static bool IsValidRenter(customerinfo type)
        {
            switch (type)
            {
                case customerinfo.currentresident:
                case customerinfo.customer:
                //cakel: BUGID00093 - (mark) commented this code out to not include future residents
                //case customerinfo.futureresident:
                    return true;
            }

            return false;
        }

        private static decimal GetBalanceDue(ResidentTransactionsPropertyRT_CustomerRTServiceTransactions transactions)
        {
            var BalanceDue = 0.0M;

            foreach (var Transaction in transactions.Transactions)
            {
                //cakel: BUGID00224
                //if (Transaction.Payment != null)
                //    BalanceDue -= YardiCommon.GetAmount(Transaction.Payment.Detail.Amount);

                if (Transaction.Payment != null)
                {
                    if (Transaction.Payment.Detail.Comment != null)
                    {   //cakel: BUGID00224
                        var GetComment = Transaction.Payment.Detail.Comment.ToString(); //cakel: code to check comment
                        if (Transaction.Payment.Detail.Comment.IndexOf(":HAP") < 0 || GetComment.Length == 0)
                        {
                            BalanceDue -= YardiCommon.GetAmount(Transaction.Payment.Detail.Amount);
                        }
                    }
                    else
                    {
                        BalanceDue -= YardiCommon.GetAmount(Transaction.Payment.Detail.Amount);
                    }
                }

                if (Transaction.Charge != null)
                    BalanceDue += YardiCommon.GetAmount(Transaction.Charge.Detail.BalanceDue);
            }

            return BalanceDue;
        }

        private static DateTime? GetDate(DateTime value)
        {
            if (value < SqlDateTime.MinValue.Value || value > SqlDateTime.MaxValue.Value)
                return null;

            return value;
        }

        private static string GetPhoneNumber(phoneinfo phoneType, IEnumerable<PhoneType> numbers)
        {
            return (from Phone in numbers.Where(phone => phone.Type == phoneType) where Phone.PhoneNumber.Length == 10 select Phone.PhoneNumber).FirstOrDefault();
        }

        private static AddressType GetAddress(IEnumerable<AddressType> addresses)
        {
            return (from addressinfo AddressInfo in Enum.GetValues(typeof(addressinfo)) select GetAddress(AddressInfo, addresses)).FirstOrDefault(address => address != null);
        }

        private static AddressType GetAddress(addressinfo addressType, IEnumerable<AddressType> addresses)
        {
            return addresses.FirstOrDefault(address => address.Type == addressType);
        }

        private static string GetEmailAddress(IEnumerable<AddressType> addresses)
        {
            foreach (var Email in Enum.GetValues(typeof(addressinfo)).Cast<addressinfo>().Select(addressInfo => GetEmailAddress(addressInfo, addresses)).Where(email => !String.IsNullOrEmpty(email)))
            {
                return Email;
            }

            return String.Format("{0}@unknown.com", Guid.NewGuid());
        }

        private static string GetEmailAddress(addressinfo addressType, IEnumerable<AddressType> addresses)
        {
            foreach (var Address in addresses.Where(address => addressType == address.Type && !String.IsNullOrEmpty(address.Email)))
            {
                return Address.Email;
            }

            return String.Empty;
        }

        private static int? GetStateId(AddressType state)
        {
            try
            {
                int? StateId = null;

                if (state != null)
                    StateId = (int)((StateProvince)Enum.Parse(typeof(StateProvince), state.State));

                return StateId;
            }
            catch
            {
                return null;
            }
        }
    }
}
