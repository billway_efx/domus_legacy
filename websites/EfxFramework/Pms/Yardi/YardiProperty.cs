﻿using System;
using System.Xml.Linq;

namespace EfxFramework.Pms.Yardi
{
    public class YardiProperty
    {
        public string Code { get; set; }
        public string MarketingName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string AccountsReceivable { get; set; }
        public string AccountsPayable { get; set; }

        public YardiProperty()
        {

        }

        public YardiProperty(XContainer element)
        {
            var Elem = element.Element("Code");
            Code = Elem != null ? Elem.Value : string.Empty;

            Elem = element.Element("MarketingName");
            MarketingName = Elem != null ? Elem.Value : string.Empty;

            Elem = element.Element("AddressLine1");
            AddressLine1 = Elem != null ? Elem.Value : string.Empty;

            Elem = element.Element("AddressLine2");
            AddressLine2 = Elem != null ? Elem.Value : string.Empty;

            Elem = element.Element("AddressLine3");
            AddressLine3 = Elem != null ? Elem.Value : string.Empty;

            Elem = element.Element("City");
            City = Elem != null ? Elem.Value : string.Empty;

            Elem = element.Element("State");
            State = Elem != null ? Elem.Value : string.Empty;

            Elem = element.Element("PostalCode");
            PostalCode = Elem != null ? Elem.Value : string.Empty;

            Elem = element.Element("AccountsReceivable");
            AccountsReceivable = Elem != null ? Elem.Value : string.Empty;

            Elem = element.Element("AccountsPayable");
            AccountsPayable = Elem != null ? Elem.Value : string.Empty;

            SendPropertyToOutput();
        }

        private void SendPropertyToOutput()
        {
            Console.WriteLine("Property Configuration for Property Code {0} Retrieved.", Code);
        }
    }
}
