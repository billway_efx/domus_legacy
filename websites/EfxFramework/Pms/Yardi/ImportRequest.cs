﻿using EfxFramework.Pms.Yardi.Core;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using static System.String;

namespace EfxFramework.Pms.Yardi
{
    public class WebClientWithTimeout : WebClient
    {
        protected override WebRequest GetWebRequest(Uri uri)
        {
            var webRequest = base.GetWebRequest(uri);
            webRequest.Timeout = 360 * 1000; //360 seconds
            return webRequest;
        }
    }

    public class ImportRequest
    {
        public static void UpdateRenterBalanceDueRealTimeYardi(Renter resident)
        {
            var theProperty = Property.GetPropertyByRenterId(resident.RenterId);
            if (theProperty == null) return;
            if (theProperty.YardiEndpoint == "") return;
            switch (resident.PmsId)
            {
                case null:
                case "":
                    return;
            }

            switch (theProperty.PmsId)
            {
                case null:
                case "":
                    return;
            }

            if (resident.PmsTypeId != 1) return;

            var postMonthDate = DateTime.UtcNow.AddMonths(1);
            postMonthDate = new DateTime(postMonthDate.Year, postMonthDate.Month, 1);


            if (theProperty.AllowPrePayments)
            {

                var now = DateTime.Now;
                var diff = postMonthDate - now;
                var days = (int)Math.Ceiling(diff.TotalDays);
                decimal residentNextMonthBalanceDue = 0;
                decimal residentCurrentMonthBalanceDue = 0;

                if (days <= Convert.ToInt32(theProperty.NumOfPrePayDays) && days > 0)   //Only update if we're in the pre-payment days window
                {
                    // 1) Get the current month's balance for this resident
                    TheYardiCallSingleTenant(resident);

                    // 2) Store this resident's current month balance temporarily
                    var residentLease = Lease.GetRenterLeaseByRenterId(resident.RenterId);
                    if (residentLease.CurrentBalanceDue.HasValue)
                        residentCurrentMonthBalanceDue = residentLease.CurrentBalanceDue.Value;

                    // 3) Get next month's charges for this resident
                    TheYardiCallPrePay(postMonthDate, resident);
                    residentLease = Lease.GetRenterLeaseByRenterId(resident.RenterId);
                    if (residentLease.CurrentBalanceDue.HasValue)
                        residentNextMonthBalanceDue = residentLease.CurrentBalanceDue.Value;

                    // 4) Add current month's balance to next month's charges
                    var residentTotalCharges = residentCurrentMonthBalanceDue + residentNextMonthBalanceDue;

                    // 5) Update the current balance for this resident to include both the current month's balance and next month's charges
                    residentLease.CurrentBalanceDue = residentTotalCharges;
                    Lease.Set(residentLease);
                }
                else
                {   
                    TheYardiCallSingleTenant(resident);
                }
            }
            else
            {
                TheYardiCallSingleTenant(resident);
            }
        }

        public static void TheYardiCallPrePay(DateTime callMonth, Renter renter)
        {
            var theProperty = EfxFramework.Property.GetPropertyByRenterId(renter.RenterId);
            if (theProperty == null) return;
            if (theProperty.YardiEndpoint == "") return;
            switch (renter.PmsId)
            {
                case null:
                case "":
                    return;
            }

            switch (theProperty.PmsId)
            {
                case null:
                case "":
                    return;
            }

            var lease = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);
            if (lease == null) return;

            var requestObject = new YardiRequest(theProperty.YardiUserName, theProperty.YardiPassword, theProperty.YardiServerName, theProperty.YardiDatabaseName,
                theProperty.YardiPlatform, theProperty.PmsId, renter.PmsId);
            var theRequest = requestObject.GetLeaseChargeSoapRequest("GetResidentLeaseCharges_Login", callMonth, true);

            ImportRenterForPrePay(theProperty.YardiEndpoint, theRequest, renter, lease, theProperty);
        }

        public static void TheYardiCallSingleTenant(Renter renter)
        {
            var theProperty = Property.GetPropertyByRenterId(renter.RenterId);
            if (theProperty == null) return;
            if (theProperty.YardiEndpoint == "") return;
            switch (renter.PmsId)
            {
                case null:
                case "":
                    return;
            }

            switch (theProperty.PmsId)
            {
                case null:
                case "":
                    return;
            }

            var lease = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);
            if (lease == null) return;

            var requestObject = new YardiRequest(theProperty.YardiUserName, theProperty.YardiPassword, theProperty.YardiServerName, theProperty.YardiDatabaseName,
                theProperty.YardiPlatform, theProperty.PmsId, renter.PmsId);

            var theRequest = requestObject.GetSingleLeaseChargeSoapRequest("GetResidentTransaction_Login", true);

            ImportSingleRenterRealTime(theProperty.YardiEndpoint, theRequest, renter, theProperty);
        }
        public static void ImportRenters()
        {
            
            foreach (var property in Property.GetAllPropertyList().Where(property => property.PmsTypeId == 1 && !IsNullOrEmpty(property.PmsId)))
            {
                if (IsNullOrEmpty(property.YardiEndpoint) ||
                    (!property.PropertyId.HasValue || property.PropertyId.Value <= 0)) continue;
                var request = new YardiRequest(property.YardiUserName, property.YardiPassword, property.YardiServerName, property.YardiDatabaseName, property.YardiPlatform, property.PmsId);
                ImportRenters(property.YardiEndpoint, request.GetSoapRequest("GetResidentTransactions_Login"), property);
            }
        }
        public static void ImportSingleProperty(int propertyId)
        {
            var myProperty = new Property(propertyId);
            ImportRenters(myProperty);
        }

        public static void ImportRenters(Property property)
        {
            if (IsNullOrEmpty(property.YardiEndpoint) || (!property.PropertyId.HasValue || property.PropertyId.Value <= 0)) return;

            var request = new YardiRequest(property.YardiUserName, property.YardiPassword, property.YardiServerName, property.YardiDatabaseName, property.YardiPlatform, property.PmsId);
            ImportRenters(property.YardiEndpoint, request.GetSoapRequest("GetResidentTransactions_Login"), property);
        }

        public static void ImportRenters(List<Property> properties)
        {
            foreach (var property in properties)
            {
                if ((property.PropertyId.HasValue && property.PropertyId.Value > 0) && property.PmsTypeId == 1)
                {
                    if (IsNullOrEmpty(property.YardiEndpoint)) continue;
                    var request = new YardiRequest(property.YardiUserName, property.YardiPassword, property.YardiServerName, property.YardiDatabaseName, property.YardiPlatform);
                    var yardiProperties = GetPropertyConfigurations(property.YardiEndpoint, request.GetConfigurationRequest());

                    foreach (var yardiProperty in yardiProperties)
                    {
                        request.YardiPropertyId = yardiProperty.Code;
                        ImportRenters(property.YardiEndpoint, request.GetSoapRequest("GetResidentTransactions_Login"), property);
                    }
                }
            }
        }
        
        private static void ImportRenterForPrePay(string endpoint, string soapRequest, Renter renter, Lease lease, Property property)
        {
            using (var client = new WebClientWithTimeout())
            {
                var inputRequest = Encoding.UTF8.GetBytes(soapRequest);

                //Set the content-type header for the request
                client.Headers[HttpRequestHeader.ContentType] = "text/xml; charset=utf-8";
                client.Headers.Add("SOAPAction", "\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20/GetResidentLeaseCharges_Login\"");

                try
                {
                    var rawResponse = client.UploadData(endpoint, "POST", inputRequest);

                    using (var outputStream = new MemoryStream(rawResponse))
                    {
                        var xml = XDocument.Load(outputStream);
                        var element = YardiCommon.GetRootElement(xml.Root, "RTServiceTransactions");

                        if (element == null || !element.HasElements) return;

                        decimal charges = 0;
                        decimal rent = 0;
                        var bCharges = false;
                        try
                        {
                            var reader = element.CreateReader();
                            var bRent = false;

                            while (reader.Read())
                            {
                                if (!reader.ReadToFollowing("Transactions")) continue;

                                if (!reader.ReadToFollowing("Charge")) continue;

                                if (!reader.ReadToFollowing("Detail")) continue;

                                if (!reader.ReadToFollowing("ChargeCode")) continue;

                                var chargeCode = reader.ReadElementContentAsString();

                                if (!reader.ReadToFollowing("Amount")) continue;

                                var amount = Convert.ToDecimal(reader.ReadElementContentAsString());

                                if (chargeCode.ToUpper() != "SUBR/8" && chargeCode.ToUpper() != "HAP")
                                {
                                    charges += amount;
                                    bCharges = true;
                                }

                                if (chargeCode.ToUpper() != "RENT") continue;

                                rent = amount;
                                bRent = true;
                            }

                            if (bRent)
                            {
                                lease.RentAmount = rent;
                            }

                            if (bCharges)
                            {
                                lease.CurrentBalanceDue = charges;
                            }

                            if (bRent || bCharges)
                            {
                                //Update the Rent and CurrentBalanceDue as necessary
                                Lease.Set(lease);
                            }

                            //Deserialize for call to SetMonthlyFees
                            element = YardiCommon.GetRootElement(xml.Root, "ResidentTransactions");
                            var deSerializer = new XmlSerializer(typeof(ResidentTransactions));
                            var transactions = (ResidentTransactions)deSerializer.Deserialize(element.CreateReader());

                            //Clear out the lease fees that might already exist for this lease
                            LeaseFee.DeleteLeaseFeesByLeaseId(lease.LeaseId);

                            //This function adds new fees to the MonthlyFees table for the property as necessary
                            //It also adds/updates any fees for the resident's lease, using the Yardi CustomerID to determine which lease fees to update
                            SetMonthlyFees(transactions, property);
                        }
                        catch (Exception e)
                        {
                            var renterId = renter.RenterId;
                            BulkMail.BulkMail.SendMailMessage(null, "EFX", false, EfxSettings.SendMailUsername, EfxSettings.SendMailPassword,
                                new MailAddress(EfxSettings.TechSupportEmail), "Yardi Import Error",
                                "An error occured while importing RenterId (A) " + renterId + ": " + e,
                                new List<MailAddress> { new MailAddress(EfxSettings.TechSupportEmail) });
                        }
                    }
                }
                catch (Exception e)
                {
                    var renterId = renter.RenterId;
                    BulkMail.BulkMail.SendMailMessage(null, "EFX", false, EfxSettings.SendMailUsername, EfxSettings.SendMailPassword,
                        new MailAddress(EfxSettings.TechSupportEmail), "Yardi Import Error",
                        "An error occured while attempting to import RenterId (B) " + renterId + ": " + e,
                        new List<MailAddress> { new MailAddress(EfxSettings.TechSupportEmail) });
                }
            }
        }
        private static void ImportSingleRenterRealTime(string endpoint, string soapRequest, Renter renter, Property property)
        {
            var residents = new List<YardiResidentData>();

            //Create the WebClient
            using (var client = new WebClientWithTimeout())
            {
                var inputRequest = Encoding.UTF8.GetBytes(soapRequest);

                //Set the content-type header for the request
                client.Headers[HttpRequestHeader.ContentType] = "text/xml; charset=utf-8";
                client.Headers.Add("SOAPAction", "\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20/GetResidentTransaction_Login\"");

                try
                {

                    var rawResponse = client.UploadData(endpoint, "POST", inputRequest);

                    using (var outputStream = new MemoryStream(rawResponse))
                    {
                        var xml = XDocument.Load(outputStream);
                        var element = YardiCommon.GetRootElement(xml.Root, "ResidentTransactions");

                        if (element != null && element.HasElements)
                        {
                            //Salcedo - 2/10/2014 - let's not abort the import process if we had trouble with this one property
                            try
                            {
                                //Deserialize
                                var deSerializer = new XmlSerializer(typeof(ResidentTransactions));
                                var results = (ResidentTransactions)deSerializer.Deserialize(element.CreateReader());
                                var customers = from prop in results.Property from Custs in prop.RT_Customer select Custs;

                                var residentTransactionsPropertyRtCustomers = customers.ToList();
                                residents.AddRange(residentTransactionsPropertyRtCustomers.Select(YardiResidentData.SetYardiResidentTransaction).Where(r => r.Resident != null));
                                ImportPmsData(residents, property, endpoint, results);

                                var success = residents.Count;
                                var failed = residentTransactionsPropertyRtCustomers.Count() - success;
                                var newSummary = new ResidentImportSummary
                                {
                                    PropertyId = property.PropertyId ?? 0,
                                    ImportDatetime = DateTime.UtcNow,
                                    SuccessfulRecordCount = success,
                                    FailedRecordCount = failed,
                                    RequestXml = soapRequest,
                                    ResponseXml = xml.ToString()
                                };

                                ResidentImportSummary.Set(newSummary);
                            }
                            catch (Exception e)
                            {
                                var propertyId = property.PropertyId ?? 0;
                                
                                BulkMail.BulkMail.SendMailMessage(null, "EFX", false, EfxSettings.SendMailUsername, EfxSettings.SendMailPassword,
                                    new MailAddress(EfxSettings.TechSupportEmail), "Yardi Import Error",
                                    "An error occured while importing RenterId " + renter.RenterId + " in PropertyId " + propertyId + ": " + e,
                                    new List<MailAddress> { new MailAddress(EfxSettings.TechSupportEmail) });
                            }
                        }
                        else
                        {
                            //Salcedo - 2/19/2014 - Even if we had no elements, we still need to log that something happened. 
                            //Likely the response XML will contain an error message.
                            const int success = 0;
                            const int failed = 0;
                            var newSummary = new ResidentImportSummary
                            {
                                PropertyId = property.PropertyId ?? 0,
                                ImportDatetime = DateTime.UtcNow,
                                SuccessfulRecordCount = success,
                                FailedRecordCount = failed,
                                RequestXml = soapRequest,
                                ResponseXml = xml.ToString()
                            };

                            ResidentImportSummary.Set(newSummary);
                        }
                    }
                }
                catch (Exception e)
                {
                    var PropertyId = property.PropertyId ?? 0;
                    BulkMail.BulkMail.SendMailMessage(null, "EFX", false, EfxSettings.SendMailUsername, EfxSettings.SendMailPassword,
                        new MailAddress(EfxSettings.TechSupportEmail), "Yardi Import Error",
                        "An error occured while attempting to import PropertyId " + PropertyId + ": " + e,
                        new List<MailAddress> { new MailAddress(EfxSettings.TechSupportEmail) });
                }
            }
        }//End ImportSingleRenterRealTime

        private static void ImportRenters(string endpoint, string soapRequest, Property property)
        {
            var residents = new List<YardiResidentData>();

            //Create the WebClient
            using (var client = new WebClientWithTimeout())
            {
                var inputRequest = Encoding.UTF8.GetBytes(soapRequest);

                //Set the content-type header for the request
                client.Headers[HttpRequestHeader.ContentType] = "text/xml; charset=utf-8";
                client.Headers.Add("SOAPAction", "\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20/GetResidentTransactions_Login\"");

                try
                {
                    
                    var rawResponse = client.UploadData(endpoint, "POST", inputRequest);

                    using (var outputStream = new MemoryStream(rawResponse))
                    {
                        var xml = XDocument.Load(outputStream);
                        var element = YardiCommon.GetRootElement(xml.Root, "ResidentTransactions");

                        if (element != null && element.HasElements)
                        {
                            //Salcedo - 2/10/2014 - let's not abort the import process if we had trouble with this one property
                            try
                            {
                                //Deserialize
                                var deSerializer = new XmlSerializer(typeof(ResidentTransactions));
                                var results = (ResidentTransactions)deSerializer.Deserialize(element.CreateReader());
                                var customers = from Prop in results.Property from Custs in Prop.RT_Customer select Custs;

                                var residentTransactionsPropertyRtCustomers = customers as ResidentTransactionsPropertyRT_Customer[] ?? customers.ToArray();
                                residents.AddRange(residentTransactionsPropertyRtCustomers.Select(YardiResidentData.SetYardiResidentTransaction).Where(r => r.Resident != null));
                                ImportPmsData(residents, property, endpoint, results);

                                var success = residents.Count;
                                var failed = residentTransactionsPropertyRtCustomers.Count() - success;
                                var newSummary = new ResidentImportSummary
                                {
                                    PropertyId = property.PropertyId ?? 0,
                                    ImportDatetime = DateTime.UtcNow,
                                    SuccessfulRecordCount = success,
                                    FailedRecordCount = failed,
                                    RequestXml = soapRequest,
                                    ResponseXml = xml.ToString()
                                };

                                ResidentImportSummary.Set(newSummary);
                            }
                            catch (Exception e)
                            {
                                var propertyId = property.PropertyId ?? 0;
                                BulkMail.BulkMail.SendMailMessage(null, "EFX", false, EfxSettings.SendMailUsername, EfxSettings.SendMailPassword,
                                    new MailAddress(EfxSettings.TechSupportEmail), "Yardi Import Error",
                                    "An error occured while importing PropertyId " + propertyId + ": " + e,
                                    new List<MailAddress> { new MailAddress(EfxSettings.TechSupportEmail) });
                            }
                        }
                        else
                        {
                            //Salcedo - 2/19/2014 - Even if we had no elements, we still need to log that something happened. 
                            //Likely the response XML will contain an error message.
                            const int success = 0;
                            const int failed = 0;
                            var newSummary = new ResidentImportSummary
                            {
                                PropertyId = property.PropertyId ?? 0,
                                ImportDatetime = DateTime.UtcNow,
                                SuccessfulRecordCount = success,
                                FailedRecordCount = failed,
                                RequestXml = soapRequest,
                                ResponseXml = xml.ToString()
                            };

                            ResidentImportSummary.Set(newSummary);
                        }
                    }
                }
                catch (Exception e)
                {
                    var propertyId = property.PropertyId ?? 0;
                    BulkMail.BulkMail.SendMailMessage(null, "EFX", false, EfxSettings.SendMailUsername, EfxSettings.SendMailPassword,
                        new MailAddress(EfxSettings.TechSupportEmail), "Yardi Import Error",
                        "An error occured while attempting to import PropertyId " + propertyId + ": " + e,
                        new List<MailAddress> { new MailAddress(EfxSettings.TechSupportEmail) });
                }
            }
        }

        public static List<YardiProperty> GetPropertyConfigurations(string endpoint, string soapRequest)
        {
            using (var Client = new WebClient())
            {
                var inputRequest = Encoding.UTF8.GetBytes(soapRequest);

                //Set the content-type header for the request
                Client.Headers[HttpRequestHeader.ContentType] = "text/xml; charset=utf-8";
                Client.Headers.Add("SOAPAction", "\"http://tempuri.org/YSI.Interfaces.WebServices/ItfResidentTransactions20/GetPropertyConfigurations\"");

                var rawResponse = Client.UploadData(endpoint, "POST", inputRequest);

                using (var outputStream = new MemoryStream(rawResponse))
                {
                    var xml = XDocument.Load(outputStream);
                    var element = YardiCommon.GetRootElement(xml.Root, "Properties");
                    var yardiProperties = new List<YardiProperty>();

                    if (element != null && element.HasElements)
                    {
                        yardiProperties.AddRange(element.Elements().Select(e => new YardiProperty(e)));
                    }

                    return yardiProperties;
                }
            }
        }

        private static void ImportPmsData(IEnumerable<YardiResidentData> residents, Property property, string endpoint, ResidentTransactions transactions)
        {
            if (!property.PropertyId.HasValue || property.PropertyId.Value < 1) return;

            foreach (var resident in residents)
            {
                //Salcedo - 2/20/2014 - add PropertyId to call
                var renter = SetRenter(resident.Resident, (int)property.PropertyId);
                var lease = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);
                SetPayer(renter);
                SetLease(lease, resident.ResidentLease, renter.RenterId, property.PropertyId.Value);
                SetProperty(Property.GetPropertyByRenterId(renter.RenterId), property.PropertyId.Value, renter.RenterId);
                SetRoommates(resident, renter.RenterId);

                //Clear out the lease fees so they are not getting charged incorrect fees
                LeaseFee.DeleteLeaseFeesByLeaseId(lease.LeaseId);
            }

            //After the residents have been setup, set the fees up for the Property and it's residents
            SetFees(property, endpoint, transactions);
        }

        private static void SetFees(Property property, string endpoint, ResidentTransactions transactions)
        {
            SetMonthlyFees(transactions, property);
        }

        private static void SetMonthlyFees(ResidentTransactions transactions, Property property)
        {
            var monthlyFees = new Dictionary<string, MonthlyFee>();

            if (!property.PropertyId.HasValue || property.PropertyId.Value < 1)
                return;
            if (transactions.Property.Length < 1)
                return;
            if (transactions.Property[0].RT_Customer.Length < 1)
                return;

            //Get monthly fees from the database and add them to the MonthlyFees dictionary variable 
            foreach (var mfee in MonthlyFee.GetMonthlyFeesByPropertyId(property.PropertyId.Value).Where(mfee => mfee.ChargeCode != null && !monthlyFees.ContainsKey(mfee.ChargeCode)))
            {
                monthlyFees.Add(mfee.ChargeCode, mfee);
            }

            var customers = transactions.Property[0].RT_Customer.ToList().Where(t => t.RTServiceTransactions.Transactions != null).Select(c => c.RTServiceTransactions.Transactions.Select(t => t.Charge));

            //The Fees are located in Charges on the Transactions
            foreach (var cust in customers.Select(c => c))
            {
                foreach (var cst in cust)
                {
                    //If the Transaction doesn't have a charge or charge detail then it is not a fee
                    if (cst?.Detail == null) continue;
                    var mfee = new MonthlyFee
                    {
                        MonthlyFeeId = 0,
                        ChargeCode = cst.Detail.ChargeCode,
                        FeeName = cst.Detail.Description.Replace("[Lease Charge]", "").Trim(),
                        DefaultFeeAmount = YardiCommon.GetAmount(cst.Detail.Amount),
                        PropertyId = property.PropertyId.Value,
                        IsActive = true
                    };

                    //Make sure that it is not one of the IgnoredChargeCodes in the IgnoredChargeCodes.xml file
                    if (!YardiCommon.IsValidFeeChargeCode(mfee.ChargeCode)) continue;
                    //If the imported charge is a new Fee, add it to the dictionary
                    if (!monthlyFees.ContainsKey(cst.Detail.ChargeCode))
                    {
                        monthlyFees.Add(cst.Detail.ChargeCode, mfee);
                    }
                    else
                    {
                        //if it's not new update the Fee to match the Id in the database and update the dictionary
                        mfee.MonthlyFeeId = monthlyFees[cst.Detail.ChargeCode].MonthlyFeeId;
                        monthlyFees[cst.Detail.ChargeCode] = mfee;
                    }

                    monthlyFees[cst.Detail.ChargeCode].MonthlyFeeId = MonthlyFee.Set(monthlyFees[cst.Detail.ChargeCode]);
                    SetLeaseFee(monthlyFees[cst.Detail.ChargeCode], property, cst.Detail.CustomerID);
                }
            }
        }

        private static void SetLeaseFee(MonthlyFee monthlyFee, Property property, string yardiCustomerId)
        {
            if (!property.PropertyId.HasValue || property.PropertyId.Value < 1) return;

            var resident = Renter.GetRenterListByPropertyId(property.PropertyId.Value).FirstOrDefault(r => r.PmsTypeId == 1 && r.PmsId == yardiCustomerId);
            if (resident == null || resident.RenterId < 1) return;

            var lease = EfxFramework.Lease.GetRenterLeaseByRenterId(resident.RenterId);
            if (lease.LeaseId < 1)
                return;

            //Get existing fees from the database
            var leaseFees = LeaseFee.GetFeesByLeaseId(lease.LeaseId).ToDictionary(fee => fee.MonthlyFeeId);

            var lfee = new LeaseFee
            {
                MonthlyFeeId = monthlyFee.MonthlyFeeId,
                FeeAmount = monthlyFee.DefaultFeeAmount,
                FeeName = monthlyFee.FeeName,
                LeaseId = lease.LeaseId,
                PropertyId = monthlyFee.PropertyId,
                RenterId = resident.RenterId,
                IsActive = monthlyFee.IsActive
            };

            //If it's a new fee, add it to the Lease Fees, otherwise update the fee in the dictionary
            if (!leaseFees.ContainsKey(monthlyFee.MonthlyFeeId))
                leaseFees.Add(monthlyFee.MonthlyFeeId, lfee);
            else
                leaseFees[monthlyFee.MonthlyFeeId].FeeAmount += lfee.FeeAmount;  

            //Save the fee in the dictionary to the database
            LeaseFee.Set(leaseFees[monthlyFee.MonthlyFeeId]);
        }

        // Salcedo - 2/20/2014 - add PropertyId to call
        private static Renter SetRenter(Renter importedRenter, int PropertyId)
        {
            var renter = Renter.GetRenterByPmsIdAndPmsTypeId(importedRenter.PmsId, 1, PropertyId);

            if (renter.PmsPropertyId != importedRenter.PmsPropertyId)
                renter.RenterId = 0;

            //Salcedo - 12/31/2014 - logic for duplicate email address
            var checkForDupEmail = new Renter(importedRenter.PrimaryEmailAddress);
            if (renter.RenterId == 0 && checkForDupEmail.RenterId > 0)
            {
                var currentEmailAddress = importedRenter.PrimaryEmailAddress;
                importedRenter.PrimaryEmailAddress = $"{Guid.NewGuid()}@unknown.com";
                var log = new RPO.ActivityLog();
                log.WriteLog("Yardi Integration", "Duplicate email address detected during import of " + importedRenter.PmsId + 
                    ": '" + currentEmailAddress + "'. Replacing with '" +
                    importedRenter.PrimaryEmailAddress + "'.", 0, "0", false);
                Console.WriteLine(string.Concat("Duplicate email address detected during import of new resident ",
                    importedRenter.PmsId, ": '", currentEmailAddress, "'. Replacing with '",
                    importedRenter.PrimaryEmailAddress + "'."));
            }

            if ((renter.RenterId > 0 && !renter.UserModified) || renter.RenterId < 1)
            {
                try
                {
                    renter = new Renter(Renter.Set(importedRenter, "Yardi Import Service"));

                    //Salcedo - 12/31/2014 - check to make sure we really have a valid renter record after calling the Renter.Set statement
                    if (renter.RenterId > 0)
                    {
                        if (!renter.PrimaryEmailAddress.IsValidEmailAddress())
                        {
                            Renter.AddNoteForRenter(0, renter.RenterId,
                                $"Invalid Primary Email Address used: {renter.PrimaryEmailAddress}, moved to Notes and replaced by a generated email address.  Please update Primary Email Address with the unique email address of the Resident.",
                                "Yardi Import Service", (int) RoleType.EfxAdministrator);
                            renter.PrimaryEmailAddress = $"{Guid.NewGuid()}@unknown.com";
                            Renter.Set(renter, "Yardi Import Service");
                        }
                    }
                }
                catch (SqlException ex)
                {
                    if (ex.Number != 2601) return renter;

                    importedRenter.AlternateEmailAddress1 = importedRenter.PrimaryEmailAddress;
                    importedRenter.PrimaryEmailAddress = $"{Guid.NewGuid()}@unknown.com";
                    renter = new Renter(Renter.Set(importedRenter, "Yardi Import Service"));
                    Renter.AddNoteForRenter(0, renter.RenterId,
                        $"Duplicate Primary Email Address used, moved {renter.AlternateEmailAddress1} to Alternate Email Address 1 and generated a new Primary Email Address.  Please update Primary Email Address with the unique email address of the Resident.",
                        "Yardi Import Service", (int) RoleType.EfxAdministrator);
                }
            }
            else
            {
                //Salcedo - 2/20/2014 - no reason to update the database if it didn't change
                if (renter.AcceptedPaymentTypeId == importedRenter.AcceptedPaymentTypeId) return renter;
                renter.AcceptedPaymentTypeId = importedRenter.AcceptedPaymentTypeId;
                Renter.Set(renter, "Yardi Import Service");

                //dwillis Task 00076
                var log = new RPO.ActivityLog();
                var PaymentType = "";
                switch (renter.AcceptedPaymentTypeId)
                {
                    case 1:
                        PaymentType = "Any";
                        break;
                    case 2:
                        PaymentType = "Check";
                        break;
                    case 3:
                        PaymentType = "Cash Equivalent";
                        break;
                    case 4:
                        PaymentType = "Do Not Accept";
                        break;
                }
                log.WriteLog("Yardi Integration", "Accepted Payment Type was updated to '" + PaymentType + "'", 0, renter.RenterId.ToString(), true);
            }
            return renter;
        }

        private static void SetPayer(Renter renter)
        {
            if (!renter.PayerId.HasValue || renter.PayerId.Value < 1)
                Payer.SetPayerFromRenter(renter);
        }

        private static void SetLease(Lease lease, Lease importedLease, int renterId, int propertyId)
        {
            if (lease.LeaseId > 0)
            {
                //Salcedo - 2/20/2014 - no point hitting the database if nothing changed
                var changeFlag = false;

                if (lease.RentDueDayOfMonth > 28)
                {
                    lease.RentDueDayOfMonth = 1;
                    changeFlag = true;
                }
                if (lease.CurrentBalanceDue != importedLease.CurrentBalanceDue)
                {
                    lease.CurrentBalanceDue = importedLease.CurrentBalanceDue;
                    changeFlag = true;
                }

                //Salcedo - 6/3/2014 - task # 0000121 - update the Rent Amount if it has changed
                if (lease.RentAmount != importedLease.RentAmount)
                {
                    lease.RentAmount = importedLease.RentAmount;
                    changeFlag = true;
                }

                //Salcedo - 2/10/2014 - Nick said we should always be pulling the Start/End lease dates from Yardi
                if (lease.StartDate != importedLease.StartDate || lease.EndDate != importedLease.EndDate)
                {
                    lease.StartDate = importedLease.StartDate;
                    lease.EndDate = importedLease.EndDate;
                    changeFlag = true;
                }

                if (changeFlag)
                {
                    Lease.Set(lease);
                }
            }
            else
            {
                var staff = PropertyStaff.GetPropertyStaffListByPropertyId(propertyId);

                if (staff.Count < 1 || (staff.Count > 0 && (!staff[0].PropertyStaffId.HasValue || staff[0].PropertyStaffId.Value < 1)))
                {
                    // ReSharper disable LocalizableElement
                    Console.WriteLine(@"Cannot add lease without at least one Property Staff Member");
                    // ReSharper restore LocalizableElement
                    return;
                }

                lease = importedLease;
				if (lease.RentDueDayOfMonth > 28)
					lease.RentDueDayOfMonth = 1;
                lease.PropertyId = propertyId;
                // ReSharper disable PossibleInvalidOperationException
                lease.PropertyStaffId = staff[0].PropertyStaffId.Value;  //method will have returned before getting here if this staff member exists, but has a null PropertyStaffId
                // ReSharper restore PossibleInvalidOperationException
                Lease.AssignRenterToLease(Lease.Set(lease), renterId);
            }
        }

        private static void SetProperty(Property property, int importPropertyId, int renterId)
        {
            if ((!property.PropertyId.HasValue || property.PropertyId.Value < 1) && importPropertyId > 0)
                Renter.AssignRenterToProperty(importPropertyId, renterId);
        }

        private static void SetRoommates(YardiResidentData resident, int renterId)
        {
            if (renterId < 1) return;

            foreach (var res in resident.Roommates.Values)
            {
                res.RoommateId = Roommate.GetRoommateByRenterIdAndPmsId(renterId, res.PmsId).RoommateId;
                res.RenterId = renterId;

                Roommate.Set(res);
            }
        }
    }
}
