﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Net;

namespace EfxFramework
{
    //public enum LogPriority  
    //    {
    //        LogAlways = 0,
    //        LogImportant = 1,
    //        LogInformational = 2,
    //        LogDebugging = 3
    //    };

    //public class ActivityLog 
    //{
        
    //    public Boolean IsNumeric(String s)
    //    {
    //        if (s == null)
    //            return false;

    //        Boolean value = true;
    //        foreach (Char c in s.ToCharArray())
    //        {
    //            value = value && Char.IsDigit(c);
    //        }
    //        return value;
    //    }

    //    public short cPriority;
    //    public ActivityLog()
    //    {
    //        string Priority = ConfigurationManager.AppSettings["LogPriority"];
    //        if (!IsNumeric(Priority))
    //        {
    //            Priority = "0";
    //        }
    //        cPriority = System.Convert.ToInt16(Priority);
    //        if (cPriority < 0)
    //        {
    //            cPriority = 0;
    //        }
    //    }

    //    public string SafeStr(string Input)
    //    {
    //        return Input.Replace("'", "''").Replace(";", "");
    //    }

    //    //In the AppSettings section of web.config:

    //    //To log only important messages, set LogPriority to 1 
    //    //To log both important and informational messages, set LogPriority to 2
    //    //To log important, informational, and debugging messages, set LogPriority to 3 or higher

    //    //In the application, message priorities are set like this:
    //    //0. Always log this message, regardless of the setting in AppSettings
    //    //1. Important messages
    //    //2. Informational messages
    //    //3. Debugging messages

        
    //    public bool WriteLog(String UserName, string Description, short Priority)
    //    {
    //        bool functionReturnValue = false;

    //        //The passed in priority must be <= LogPriority for the message to be logged
    //        if (Priority > cPriority)
    //        {
    //            return true;
    //        }

    //        string sql = null;

    //        try
    //        {
    //            //Make sure the string is not too long to place in the database ...
    //            //And, make sure any single apostrophes are replaced with two apostrophes ...

    //            //The column holds 7000 characters, but let's keep it to 6990 just to allow a little
    //            //breathing room ...
    //            if (SafeStr(Description).Length > 6990)
    //            {
    //                Description =  SafeStr(Description).Substring(0, 6990);
    //            }
    //            else
    //            {
    //                Description = SafeStr(Description);
    //            }

    //            string Path = "Unknown source context";
    //            try
    //            {
    //                Path = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
    //            }
    //            catch
    //            {
    //            }

    //            string _IpAddress = GetIPAddress();
    //            int _IpNumber = GetIPNumber();

    //            //cakel: BUGID00133 - Updated sql for insert into new columns
    //            sql = "Insert into ActivityLog (UserName, Location, Description, IPAddress, IPNumber) values ('" + UserName + "','" + Path + "','" + Description + "','" + _IpAddress + "','" + _IpNumber + "')";

    //            // Execute statement 
    //            SqlCommand sqlCmd = new SqlCommand();
    //            sqlCmd.Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());
    //            sqlCmd.Connection.Open();
    //            sqlCmd.CommandText = sql;
    //            int RowsReturned = sqlCmd.ExecuteNonQuery();
    //            sqlCmd.Connection.Close();
    //            if (RowsReturned > 0)
    //                return true;
    //            else
    //                return false;

    //        }
    //        catch 
    //        {
    //        }
    //        return functionReturnValue;

    //    }

    //    //cakel: BUGID00133 - Gets the IP Address from user
    //    public string GetIPAddress()
    //    {
    //        string _IPaddress = null;

    //        _IPaddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

    //        if (_IPaddress == null)
    //        {
    //            _IPaddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
    //        }
    //        return _IPaddress;
    //    }

    //    //cakel: BUGID00133
    //    //Returns IP Address in Bytes - This will be used for the Geo Database to return location by range
    //    public int GetIPNumber()
    //    {
    //        int _ipnum = BitConverter.ToInt32(IPAddress.Parse(GetIPAddress()).GetAddressBytes(), 0);
    //        return _ipnum;
    //    }





    //}
}
