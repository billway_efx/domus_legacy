﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace EfxFramework.WordPress
{
    [DataContract]
    public class Post
    {
        [DataMember]
        public string title { get; set; }
        
        [DataMember]
        public string excerpt { get; set; }
        
        [DataMember]
        public string URL { get; set; }

        public string InternalUrl { get { return String.Format("/News.aspx?url={0}", URL); } }

        //Empty constructor for Json build
        public Post()
        {
        }
    }
}
