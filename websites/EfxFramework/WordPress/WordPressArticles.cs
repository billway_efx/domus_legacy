﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Mb2x.ExtensionMethods;

namespace EfxFramework.WordPress
{
    public static class WordPressArticles
    {
        private static List<Post> WordPressPosts {get; set;}
        
        /// <summary>
        /// Gets a list of posts by calling the WordPress API
        /// </summary>
        /// <param name="blogUrl">The URL of the specific blog site to get posts from</param>
        /// <returns>A list of WordPress Posts</returns>
        public static List<Post> GetPosts(string blogUrl)
        {
            //Instantiate WordPressPosts
            WordPressPosts = new List<Post>();

            //Call method to get the web response
            var PostList = GetJsonResponse(blogUrl);

            
            //If there are less than 3 posts in the list, add each post and return
            if (PostList.posts.Count < 3)
            {
                //for (var Post = 0; Post < PostList.posts.Count; Post++)
                //{
                //    WordPressPosts.Add(PostList.posts[Post]);
                //}

                foreach (var Post in PostList.posts)
                {
                    WordPressPosts.Add(Post);
                }
                return WordPressPosts;
            }

            //Otherwise, loop through the first 3 posts and add it to the list
            for (var Post = 0; Post < 3; Post++)
            {
                WordPressPosts.Add(PostList.posts[Post]);
            }

            //Return the list
            return WordPressPosts;  
        }

        /// <summary>
        /// Gets a list of posts on a WordPress blog
        /// </summary>
        /// <param name="blogUrl">The URL of the specific blog to get posts from</param>
        /// <returns>A Posts object containing a list of individual posts</returns>
        private static Posts GetJsonResponse(string blogUrl)
        {
            //Create a webclient
            using (var Client = new WebClient())
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //Format the request URL
                var RequestUrl = string.Format(Resources.WordPressTemplates.GetPostListTemplate, blogUrl.UrlEncode());

                //Download the data
                var Response = Client.DownloadData(RequestUrl);

                //Call method to deserialize the response from Json into classes
                var PostList = GetJsonFromResponse(Response);

                //Return PostList
                return PostList;
            }
        }

        /// <summary>
        /// Deserializes a Json Response to a type of Posts
        /// </summary>
        /// <param name="response">A byte array response from a webclient download</param>
        /// <returns>A Posts object</returns>
        private static Posts GetJsonFromResponse(byte[] response)
        {
            //Create memory stream
            using (var MStream = new MemoryStream(response))
            {
                //Deseralize the response
                var JsonResponse = new DataContractJsonSerializer(typeof(Posts));

                //Return the deserialized Posts object
                return (Posts)JsonResponse.ReadObject(MStream);
            }
        }
    }
}
