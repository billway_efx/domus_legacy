﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace EfxFramework.WordPress
{
    [DataContract]
    class Posts
    {
        [DataMember]
        public List<Post> posts { get; set; }

        //Empty constructor for Json build
        public Posts()
        {
        }
    }
}
