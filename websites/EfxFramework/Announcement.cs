using EfxFramework.ExtensionMethods;
using Mb2x.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using Mb2x.ExtensionMethods;

namespace EfxFramework
{
    public class Announcement : BaseEntity
    {
        //Public Properties
        [DisplayName(@"AnnouncementId"), Required(ErrorMessage = "AnnouncementId is required.")]
        public int AnnouncementId { get; set; }

        [DisplayName(@"AnnouncementTypeId"), Required(ErrorMessage = "AnnouncementTypeId is required.")]
        public int AnnouncementTypeId { get; set; }

        [DisplayName(@"AnnouncementDate")]
        public DateTime? AnnouncementDate { get; set; }

        [DisplayName(@"AnnouncementTitle"), StringLength(100)]
        public string AnnouncementTitle { get; set; }

        [DisplayName(@"AnnouncementText")]
        public string AnnouncementText { get; set; }

        [DisplayName(@"IsActive"), Required(ErrorMessage = "IsActive is required.")]
        public bool IsActive { get; set; }

        [DisplayName("@AnnouncmentUrl")]
        public string AnnouncementUrl { get; set; }

        [DisplayName(@"PostedBy"), Required(ErrorMessage = "PostedBy is required.")]
        public string PostedBy { get; set; }
        [DisplayName(@"ExpirationDate")] //CMallory - Task 00462 - Added
        public DateTime? ExpirationDate { get; set; }

        public string AnnouncementStatus { get { return IsActive ? "Published" : "Saved"; } }
        public string EditUrl { get; set; }
        public string DeleteUrl { get; set; }
        public string TruncateNews { get { return AnnouncementText.TruncateSentence(200); } }
        public string AboutUsNews { get { return String.Format("~/News.aspx?Post={0}", AnnouncementId); } }
        public string RedirectNews { get { return String.Format("https://rentpaidonline.com/News.aspx?Post={0}", AnnouncementId); } }
        public string EditAnnounceementUrl { get { return String.Format("~/renters/community-connect.aspx?AnnouncementId={0}&AnnouncementTypeId={1}", AnnouncementId, AnnouncementTypeId); } }
        public string EditResidentsRoomUrl { get { return String.Format("{0}#ResidentsRoomTab", EditAnnounceementUrl); } }
        public string EditProsCornerUrl { get { return String.Format("{0}#ProsCornerTab", EditAnnounceementUrl); } }
        public string EditNewsAndEventsUrl { get { return String.Format("{0}#NewsAndEventsTab", EditAnnounceementUrl); } }
        public string DeleteAnnouncementUrlBase { get { return String.Format("~/Handlers/GeneralHandler.ashx?RequestType=14&AnnouncementId={0}&AnnouncementTypeId={1}", AnnouncementId, AnnouncementTypeId); } }
        public string DeleteAnnouncementUrl { get { return String.Format("{0}#AnnouncementTab", DeleteAnnouncementUrlBase); } }
        public string DeleteResidentsRoomUrl { get { return String.Format("{0}#ResidentsRoomTab", DeleteAnnouncementUrlBase); } }
        public string DeleteProsCornerUrl { get { return String.Format("{0}#ProsCornerTab", DeleteAnnouncementUrlBase); } }
        public string DeleteNewsAndEventsUrl { get { return String.Format("{0}#NewsAndEventsTab", DeleteAnnouncementUrlBase);}}
        public string ArchiveDate { get { return AnnouncementDate.HasValue ? AnnouncementDate.Value.ToString("Y") : DateTime.UtcNow.ToString("Y"); } }
        public string AnnouncementDateShortDate { get { return String.Format("Posted on {0}", AnnouncementDate.HasValue ? AnnouncementDate.Value.ToString("MMMM dd, yyyy").HtmlEncodeCrLf() : DateTime.UtcNow.ToString("MMMM dd, yyyy").HtmlEncodeCrLf()); } }

        public bool IsRecentPostDate(DateTime announcementDate)
        {
            return (String.CompareOrdinal(announcementDate.ToString("Y"), DateTime.Today.ToString("Y")) == 0);
        }

        public string BlogText { get { return AnnouncementText.HtmlEncodeCrLf(); } }
        public string BlogTitle { get { return AnnouncementTitle.HtmlEncodeCrLf(); } }

        public bool IsArchivedDate(int years)
        {
            var Date = DateTime.Today;
            var Beg = new DateTime(Date.AddYears(-years).Year, 1, 1);
            var End = new DateTime(Date.Year, Date.Month, 1);

            if (!AnnouncementDate.HasValue)
                return false;

            return DateTime.Compare(AnnouncementDate.Value, Beg) > 0 && DateTime.Compare(AnnouncementDate.Value, End) < 0;
        }

        public static List<Announcement> GetAllAnnouncements()
        {
            return DataAccess.GetTypedList<Announcement>(EfxSettings.ConnectionString, "dbo.usp_Announcement_GetAllAnnouncements");
        }

        public static List<Announcement> GetAllAnnouncementsByPropertyId(int propertyId)
        {
            return DataAccess.GetTypedList<Announcement>(EfxSettings.ConnectionString, "dbo.usp_Announcement_GetAllAnnouncementsByPropertyId", new SqlParameter("@PropertyId", propertyId));
        }

        public static List<Announcement> GetActiveAdminAnnouncements(int propertyId)
        {
            return GetAllAnnouncementsByPropertyId(propertyId).Where(a => a.AnnouncementTypeId == (int) AnnouncementType.Admin && a.IsActive).ToList();
        }

        public static List<Announcement> GetAllAdminAnnouncements(int propertyId)
        {
            return GetAllAnnouncementsByPropertyId(propertyId).Where(a => a.AnnouncementTypeId == (int) AnnouncementType.Admin).ToList();
        }

        public static List<Announcement> GetActivePropertyAnnouncementsByPropertyId(int propertyId)
        {
            return GetAllAnnouncementsByPropertyId(propertyId).Where(a => (a.AnnouncementTypeId == (int) AnnouncementType.Property && a.IsActive)).ToList();
        }

        public static List<Announcement> GetAllPropertyAnnouncementsByPropertyId(int propertyId)
        {
            return GetAllAnnouncements().Where(a => a.AnnouncementTypeId == (int)AnnouncementType.Property).ToList();
        }

        public static List<Announcement> GetAllActiveResidentsRoomAnnouncements()
        {
            return GetAllAnnouncements().Where(a => a.AnnouncementTypeId == (int) AnnouncementType.ResidentsRoom && a.IsActive).ToList();
        }

        public static List<Announcement> GetAllActiveProsCornerAnnouncements()
        {
            return GetAllAnnouncements().Where(a => a.AnnouncementTypeId == (int) AnnouncementType.ProsCorner && a.IsActive).ToList();
        }

        public static List<Announcement> GetAllActiveNewsAndEvents()
        {
            return GetAllAnnouncements().Where(a => (a.AnnouncementTypeId == (int) AnnouncementType.News || a.AnnouncementTypeId == (int) AnnouncementType.Events) && a.IsActive).ToList();
        }

        public static List<Announcement> GetAllActiveNewsAndEventsByPropertyId(int propertyId)
        {
            return GetAllAnnouncementsByPropertyId(propertyId).Where(a => (a.AnnouncementTypeId == (int) AnnouncementType.News || a.AnnouncementTypeId == (int) AnnouncementType.Events) && a.IsActive).ToList();
        }

        public static Announcement GetAnnouncementById(int announcementId)
        {
            return GetAllAnnouncements().FirstOrDefault(a => a.AnnouncementId == announcementId);
        }

        public static void DeleteAnnouncement(int announcementId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_Announcement_DeleteSingleObject", new SqlParameter("@AnnouncementId", announcementId));
        }

        public static int Set(int announcementId, int announcementTypeId, DateTime? announcementDate, string announcementText, bool isActive, string announcementTitle, string announcementUrl, string postedBy, DateTime? ExpDate)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_Announcement_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@AnnouncementId", announcementId),
	                    new SqlParameter("@AnnouncementTypeId", announcementTypeId),
	                    new SqlParameter("@AnnouncementDate", announcementDate),
	                    new SqlParameter("@AnnouncementText", announcementText),
	                    new SqlParameter("@IsActive", isActive),
                        new SqlParameter("@AnnouncementTitle", announcementTitle),
                        new SqlParameter("@AnnouncementUrl", announcementUrl),
                        new SqlParameter("@PostedBy", postedBy),
                        //CMallory - Task 00462 - Added Expiration Date.
                        new SqlParameter("@ExpirationDate", ExpDate)
                    });
        }

        public static int Set(Announcement a)
        {
	        return Set(
		        a.AnnouncementId,
		        a.AnnouncementTypeId,
		        a.AnnouncementDate,
		        a.AnnouncementText,
		        a.IsActive,
                a.AnnouncementTitle,
                a.AnnouncementUrl,
                a.PostedBy,
                a.ExpirationDate //Cmallory - Task 00462 - Added
	        );
        }
          
        public Announcement(int announcementId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_Announcement_GetSingleObject", new [] { new SqlParameter("@AnnouncementId", announcementId) });

            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                DateTime? announcementDateTime = (DateTime?)ResultSet[2];
                AnnouncementDate = announcementDateTime.HasValue ? Helpers.DateTimeHelper.setDateTimeZone(announcementDateTime.Value, "Eastern Standard Time") : announcementDateTime;
                //Otherwise, populate this object
                AnnouncementId = (int)ResultSet[0];
                AnnouncementTypeId = (int)ResultSet[1];
                //AnnouncementDate = ResultSet[2] as DateTime?;
                AnnouncementText = ResultSet[3] as string;
                IsActive = (bool)ResultSet[4];
                AnnouncementTitle = ResultSet[5] as string;
                AnnouncementUrl = ResultSet[6] as string;
                PostedBy = ResultSet[7] as string;
                //CMallory - Task 00462 - Added Expiration Date.
                ExpirationDate = ResultSet[8] == Convert.DBNull ? (DateTime?)null : Convert.ToDateTime(ResultSet[8].ToString());

            }
        }

		public Announcement()
		{
			InitializeObject();
		}

	}
}
