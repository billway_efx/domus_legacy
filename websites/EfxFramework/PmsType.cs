//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EfxFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class PmsType
    {
        public PmsType()
        {
            this.Properties = new HashSet<Property>();
            this.Renters = new HashSet<Renter>();
        }
    
        public int PmsTypeId { get; set; }
        public string PmsTypeName { get; set; }
    
        public virtual ICollection<Property> Properties { get; set; }
        public virtual ICollection<Renter> Renters { get; set; }
    }
}
