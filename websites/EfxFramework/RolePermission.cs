using System;
using System.Collections.Generic;
using Mb2x.Data;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class RolePermission : BaseEntity
	{
		//Public Properties
		[DisplayName(@"RoleId"), Required(ErrorMessage = "RoleId is required.")]
		public int RoleId { get; set; }

		[DisplayName(@"PermissionId"), Required(ErrorMessage = "PermissionId is required.")]
		public int PermissionId { get; set; }

        public static List<Permission> GetActivePermissionByRoleTypeId(int roleTypeId)
        {
            return DataAccess.GetTypedList<Permission>(EfxSettings.ConnectionString, "dbo.usp_RolePermission_GetActivePermissionByRoleTypeId", new SqlParameter("@RoleTypeId", roleTypeId));
        }

        public static List<Permission> GetRolePermissionsByRoleTypeId(int roleTypeId)
        {
            return DataAccess.GetTypedList<Permission>(EfxSettings.ConnectionString, "dbo.usp_RolePermission_GetPermissionByRoleTypeId", new SqlParameter("@RoleTypeId", roleTypeId));
        }

        public static List<Permission> GetRolePermissionsByRoleId(int roleId)
        {
            return DataAccess.GetTypedList<Permission>(EfxSettings.ConnectionString, "dbo.usp_RolePermission_GetPermissionByRoleId", new SqlParameter("@RoleId", roleId));
        }

        public static int Set(int roleId, int permissionId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_RolePermission_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@RoleId", roleId),
	                    new SqlParameter("@PermissionId", permissionId)
                    });
        }

        public static int Set(RolePermission r)
        {
	        return Set(
		        r.RoleId,
		        r.PermissionId
	        );
        }

        public RolePermission(int roleId, int permissionId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_RolePermission_GetSingleObject", new [] { new SqlParameter("@RoleId", roleId), new SqlParameter("@PermissionId", permissionId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        RoleId = (int)ResultSet[0];
		        PermissionId = (int)ResultSet[1];
            }
        }

		public RolePermission()
		{
			InitializeObject();
		}

	}
}
