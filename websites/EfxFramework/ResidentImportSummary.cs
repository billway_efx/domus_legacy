using Mb2x.Data;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using Mb2x.ExtensionMethods;

namespace EfxFramework
{
	public class ResidentImportSummary : BaseEntity
	{
		//Public Properties
        [DisplayName(@"ResidentImportSummaryId"), Required(ErrorMessage = "ResidentImportSummaryId is required.")]
		public int ResidentImportSummaryId { get; set; }

        [DisplayName(@"PropertyId"), Required(ErrorMessage = "PropertyId is required.")]
		public int PropertyId { get; set; }

        [DisplayName(@"ImportDatetime"), Required(ErrorMessage = "ImportDatetime is required.")]
		public DateTime ImportDatetime { get; set; }

        [DisplayName(@"SuccessfulRecordCount"), Required(ErrorMessage = "SuccessfulRecordCount is required.")]
		public int SuccessfulRecordCount { get; set; }

        [DisplayName(@"FailedRecordCount"), Required(ErrorMessage = "FailedRecordCount is required.")]
		public int FailedRecordCount { get; set; }

        [DisplayName(@"RequestXml"), Required(ErrorMessage = "RequestXml is required.")]
		public string RequestXml { get; set; }

        [DisplayName(@"ResponseXml"), Required(ErrorMessage = "ResponseXml is required.")]
		public string ResponseXml { get; set; }

        public static void DeleteResidentImportSummaryByResidentImportSummaryId(int residentImportSummaryId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_ResidentImportSummary_DeleteSingleObject", new SqlParameter("@ResidentImportSummaryId", residentImportSummaryId));
        }

        public static int Set(int residentImportSummaryId, int propertyId, DateTime importDatetime, int successfulRecordCount, int failedRecordCount, string requestXml, string responseXml)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
                EfxSettings.ConnectionString, 
                "dbo.usp_ResidentImportSummary_SetSingleObject", 
                new[]
                {
	                new SqlParameter("@ResidentImportSummaryId", residentImportSummaryId),
	                new SqlParameter("@PropertyId", propertyId),
	                new SqlParameter("@ImportDatetime", importDatetime),
	                new SqlParameter("@SuccessfulRecordCount", successfulRecordCount),
	                new SqlParameter("@FailedRecordCount", failedRecordCount),
	                new SqlParameter("@RequestXml", requestXml),
	                new SqlParameter("@ResponseXml", responseXml)
                });
        }

        public static int Set(ResidentImportSummary r)
        {
	        return Set(
		        r.ResidentImportSummaryId,
		        r.PropertyId,
		        r.ImportDatetime,
		        r.SuccessfulRecordCount,
		        r.FailedRecordCount,
		        r.RequestXml,
		        r.ResponseXml
	        );
        }

        public ResidentImportSummary(int residentImportSummaryId)
        {
	        //Attempt to pull the data from the database
	        var ResultSet = DataAccess.GetDynamicTypedObject<ResidentImportSummary>(EfxSettings.ConnectionString, "dbo.usp_ResidentImportSummary_GetSingleObject", true, new [] { new SqlParameter("@ResidentImportSummaryId", residentImportSummaryId) });
            if (ResultSet == null)
                //If we don't get a record, initialize the object
                InitializeObject();
            else
            //Otherwise, populate this object
            ResultSet.CopyLikeProperties(this);
        }

	    public ResidentImportSummary()
	    {
		    InitializeObject();
	    }

    }
}
