﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Linq;
using EfxFramework.Logging;

namespace EfxFramework.Services
{
    public class DefaultServiceBehavior : Attribute, IErrorHandler, IServiceBehavior
    {
        /// <summary>
        /// This method catchs all exceptions thrown by WCF
        /// This method is currently used for logging WCF exceptions
        /// </summary>
        /// <param name="error">THe thrown exception</param>
        /// <returns>Returns true to let other error handlers perform their tasks</returns>
        public bool HandleError(Exception error)
        {
            //Log the exception
            ExceptionLogger.LogException(error);

            //Return
            return true;
        }

        /// <summary>
        /// This method allows us to create a message to send back to the client when an exception has been thrown.
        /// </summary>
        /// <param name="error">The thrown exception</param>
        /// <param name="version"></param>
        /// <param name="fault"></param>
        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
            //This code will allow us to format the message back to the client.  Currently it sends XML, we need to change it to JSON.

            //If this is a FaultException, exit
            if (error is FaultException)
            {
                return;
            }

            // Creates the exception we want to send back to the client
            //var exception = new FaultException<TimeExceptionDetails>(TimeExceptionDetails.Default,new FaultReason(TimeExceptionDetails.Default.Message));
            var Exception = new FaultException(error.Message);

            // Creates a message fault
            var MessageFault = Exception.CreateMessageFault();

            // Creates the new message based on the message fault
            fault = Message.CreateMessage(version, MessageFault, Exception.Action);
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters) { }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {
            //Add the DefaultServiceBehavior to each ChannelDispatcher
            foreach (var ChannelDispatcher in serviceHostBase.ChannelDispatchers.Select(channelDispatcherBase => channelDispatcherBase as ChannelDispatcher))
            {
                ChannelDispatcher.ErrorHandlers.Add(new DefaultServiceBehavior());
            }
        }

        public void Validate(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase) { }
    }
}