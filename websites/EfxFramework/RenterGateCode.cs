using System.Web;
using EfxFramework.Web.Caching;
using Mb2x.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;

namespace EfxFramework
{
	public class RenterGateCode : BaseEntity
	{
		//Public Properties
        [DisplayName(@"InternalGateCodeId")]
        public int InternalGateCodeId { get; set; }

        [DisplayName(@"RenterId")]
        public int RenterId { get; set; }

        [DisplayName(@"AccessUserId")]
        public string AccessUserId { get; set; }

        [DisplayName(@"FirstName")]
        public string FirstName { get; set; }

        [DisplayName(@"MiddleName")]
        public string MiddleName { get; set; }

        [DisplayName(@"LastName")]
        public string LastName { get; set; }

        [DisplayName(@"MobilePhoneNumber")]
        public string MobilePhoneNumber { get; set; }

        [DisplayName(@"PrimaryEmailAddress")]
        public string PrimaryEmailAddress { get; set; }
        
        [DisplayName(@"GateCode")]
        public string GateCode { get; set; }

        public RenterGateCode()
		{
			InitializeObject();
		}
	}
}
