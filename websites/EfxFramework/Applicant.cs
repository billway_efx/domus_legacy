using EfxFramework.Web.Caching;
using Mb2x.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class Applicant : BaseUser
	{
	    //Public Properties
        public override int? UserId { get { return ApplicantId; } }

		[DisplayName(@"ApplicantId"), Required(ErrorMessage = "ApplicantId is required.")]
		public int ApplicantId { get; set; }

        public string MainPhoneNumber { get; set; }

	    public List<ApplicantApplication> Applications { get { return ApplicantApplication.GetAllApplicantApplicationsByApplicantId(ApplicantId); } }

        protected override bool IsValidUser()
        {
            return ApplicantId > 0;
        }

        public override BaseUser GetUserById(int userId)
        {
            return new Applicant(userId);
        }

        public override BaseUser GetUserByEmailAddress(string emailAddress)
        {
            return new Applicant(emailAddress);
        }

        public override int SetUser(BaseUser user, string authenticatedUser)
        {
            return Set((user as Applicant));
        }

        public static List<Applicant> GetAllApplicantsByPropertyId(int propertyId)
        {
            return DataAccess.GetTypedList<Applicant>(EfxSettings.ConnectionString, "dbo.usp_ApplicantApplication_GetApplicantByPropertyId", new SqlParameter("@PropertyId", propertyId));
        }

        public static List<Applicant> GetAllApplicants()
        {
            var Applicants = CacheManager.GetCacheItem<List<Applicant>>(CacheKey.ApplicantCache);

            if (Applicants == null || Applicants.Count < 1)
                CacheManager.AddItemToCache(CacheKey.ApplicantCache, DataAccess.GetTypedList<Applicant>(EfxSettings.ConnectionString, "dbo.usp_Applicant_GetAllApplicants"), 60);

            return CacheManager.GetCacheItem<List<Applicant>>(CacheKey.ApplicantCache);
        }

        public static int Set(int applicantId, string firstName, string lastName, string primaryEmailAddress, byte[] passwordHash, byte[] salt, string mainPhoneNumber)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_Applicant_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@ApplicantId", applicantId),
	                    new SqlParameter("@FirstName", firstName),
	                    new SqlParameter("@LastName", lastName),
	                    new SqlParameter("@PrimaryEmailAddress", primaryEmailAddress),
	                    new SqlParameter("@PasswordHash", passwordHash),
	                    new SqlParameter("@Salt", salt),
                        new SqlParameter("@MainPhoneNumber", mainPhoneNumber)
                    });
        }

        public static int Set(Applicant a)
        {
	        return Set(
		        a.ApplicantId,
		        a.FirstName,
		        a.LastName,
		        a.PrimaryEmailAddress,
		        a.PasswordHash,
		        a.Salt,
                a.MainPhoneNumber
	        );
        }

        public Applicant(int? applicantId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_Applicant_GetSingleObject", new [] { new SqlParameter("@ApplicantId", applicantId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        ApplicantId = (int)ResultSet[0];
		        FirstName = ResultSet[1] as string;
		        LastName = ResultSet[2] as string;
		        PrimaryEmailAddress = ResultSet[3] as string;
		        PasswordHash = ResultSet[4] as byte[];
		        Salt = ResultSet[5] as byte[];
                MainPhoneNumber = ResultSet[6] as string;
            }
        }

        //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
	    public Applicant(string emailAddress)
	        : this(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_Applicant_GetApplicantByPrimaryEmailAddress", new SqlParameter("@PrimaryEmailAddress", emailAddress)))
	    {
	    }

	    public Applicant()
		{
			InitializeObject();
		}

	}
}
