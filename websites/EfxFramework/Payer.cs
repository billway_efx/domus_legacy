using System;
using System.Data;
using System.Globalization;
using Mb2x.Data;
using Mb2x.ExtensionMethods;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EfxFramework
{
    public class Payer : BaseEntity
    {
        //Private Members
        private TelephoneNumber _MainPhoneNumber;
        private TelephoneNumber _MobilePhoneNumber;
        private TelephoneNumber _FaxNumber;

        //Public Properties
        public int PayerId { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string MainPhoneNumber
        {
            get
            {
                return _MainPhoneNumber == null ? string.Empty : _MainPhoneNumber.Digits;
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _MainPhoneNumber = null;
                else
                {
                    if (_MainPhoneNumber == null)
                        _MainPhoneNumber = new TelephoneNumber(value);
                    else
                        _MainPhoneNumber.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }
        public string MobilePhoneNumber
        {
            get
            {
                return _MobilePhoneNumber == null ? string.Empty : _MobilePhoneNumber.Digits;
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _MobilePhoneNumber = null;
                else
                {
                    if (_MobilePhoneNumber == null)
                        _MobilePhoneNumber = new TelephoneNumber(value);
                    else
                        _MobilePhoneNumber.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }
        public string FaxNumber
        {
            get
            {
                return _FaxNumber == null ? string.Empty : _FaxNumber.Digits;
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _FaxNumber = null;
                else
                {
                    if (_FaxNumber == null)
                        _FaxNumber = new TelephoneNumber(value);
                    else
                        _FaxNumber.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }
        public string PrimaryEmailAddress { get; set; }
        public string AlternateEmailAddress1 { get; set; }
        public string AlternateEmailAddress2 { get; set; }
        public string StreetAddress { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public int? StateProvinceId { get; set; }
        public string PostalCode { get; set; }
        public bool? EmailNotifications { get; set; }
        public bool? TextNotifications { get; set; }

        public string DisplayName
        {
            get { return string.Format("{0} {1}", FirstName, LastName); }
        }

        public string DisplayAddress
        {
            get 
            {
                var Prov = StateProvince.AK;
                if (StateProvinceId.HasValue)
                    Prov = (StateProvince)Enum.Parse(typeof(StateProvince), StateProvinceId.Value.ToString(CultureInfo.InvariantCulture));
                
                return string.Format("{0} {1} {2} {3} {4}", StreetAddress, StreetAddress2, City, Prov, PostalCode);
            }
        }
        
        //Public Methods
        public static byte[] GetPhoto(int payerId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<byte[]>(
                EfxSettings.ConnectionString,
                "dbo.usp_Payer_GetPhoto",
                new SqlParameter("@PayerId", payerId));
        }
        public static void SetPhoto(int payerId, byte[] photo)
        {
            byte[] ResizedPhoto = null;
			//Resize the photo
            if(photo != null)
	            ResizedPhoto = photo.StandardizeImage(300);

            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(
                EfxSettings.ConnectionString,
                "dbo.usp_Payer_SetPhoto",
                new[]
					{
						new SqlParameter("@PayerId", payerId),
                        new SqlParameter{ParameterName = "@Photo", Value = ResizedPhoto, DbType = DbType.Binary}
					});
        }
        public static List<Payer> GetAllPayerList()
        {
            return DataAccess.GetTypedList<Payer>(
                EfxSettings.ConnectionString,
                "dbo.usp_Payer_GetAll"
                );
        }
        public static Payer GetPayerByRenterId(int renterId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            var PayerId = (NewDataAccess.ExecuteScalar(
                EfxSettings.ConnectionString,
                "dbo.usp_Payer_GetByRenterId",
                new SqlParameter("@RenterId", renterId)
                ) as int?);

            if (PayerId.HasValue)
                return new Payer(PayerId.Value);
            
            return new Payer(0);
        }

        public static Payer SetPayerFromRenter(Renter renter, int? payerId = null)
        {
            var Pid = payerId.HasValue ? payerId.Value : 0;
            var Payer = new Payer(Set(Pid, renter.Prefix, renter.FirstName, renter.MiddleName, renter.LastName, renter.Suffix, renter.MainPhoneNumber, renter.MobilePhoneNumber, renter.FaxNumber, renter.PrimaryEmailAddress,
                      renter.AlternateEmailAddress1, renter.AlternateEmailAddress2, renter.StreetAddress, renter.Unit, renter.City, renter.StateProvinceId, renter.PostalCode, renter.EmailNotifications, renter.TextNotifications));

            renter.PayerId = Payer.PayerId;
            Renter.Set(renter, "System Process");

            return Payer;
        }

        public static int Set(int payerId, string prefix, string firstName, string middleName, string lastName, string suffix, string mainPhoneNumber, string mobilePhoneNumber, string faxNumber, string primaryEmailAddress, string alternateEmailAddress1, string alternateEmailAddress2, string streetAddress, string streetAddress2, string city, int? stateProvinceId, string postalCode, bool? emailNotifications, bool? textNotifications)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
                EfxSettings.ConnectionString,
                "dbo.usp_Payer_SetSingleObject",
            new[]
                {
	                new SqlParameter("@PayerId", payerId),
	                new SqlParameter("@Prefix", prefix),
	                new SqlParameter("@FirstName", firstName),
	                new SqlParameter("@MiddleName", middleName),
	                new SqlParameter("@LastName", lastName),
	                new SqlParameter("@Suffix", suffix),
	                new SqlParameter("@MainPhoneNumber", mainPhoneNumber.EmptyStringToNull()),
	                new SqlParameter("@MobilePhoneNumber", mobilePhoneNumber.EmptyStringToNull()),
	                new SqlParameter("@FaxNumber", faxNumber.EmptyStringToNull()),
	                new SqlParameter("@PrimaryEmailAddress", primaryEmailAddress),
	                new SqlParameter("@AlternateEmailAddress1", alternateEmailAddress1),
	                new SqlParameter("@AlternateEmailAddress2", alternateEmailAddress2),
	                new SqlParameter("@StreetAddress", streetAddress),
	                new SqlParameter("@StreetAddress2", streetAddress2),
	                new SqlParameter("@City", city),
	                new SqlParameter("@StateProvinceId", stateProvinceId),
	                new SqlParameter("@PostalCode", postalCode),
	                new SqlParameter("@EmailNotifications", emailNotifications),
	                new SqlParameter("@TextNotifications", textNotifications)
                });
        }
        public static int Set(Payer p)
        {
            return Set(
                p.PayerId,
                p.Prefix,
                p.FirstName,
                p.MiddleName,
                p.LastName,
                p.Suffix,
                p.MainPhoneNumber,
                p.MobilePhoneNumber,
                p.FaxNumber,
                p.PrimaryEmailAddress,
                p.AlternateEmailAddress1,
                p.AlternateEmailAddress2,
                p.StreetAddress,
                p.StreetAddress2,
                p.City,
                p.StateProvinceId,
                p.PostalCode,
                p.EmailNotifications,
                p.TextNotifications
            );
        }
        public Payer(int? payerId)
        {
            //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_Payer_GetSingleObject", new[] { new SqlParameter("@PayerId", payerId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
                PayerId = (int)ResultSet[0];
                Prefix = ResultSet[1] as string;
                FirstName = ResultSet[2] as string;
                MiddleName = ResultSet[3] as string;
                LastName = ResultSet[4] as string;
                Suffix = ResultSet[5] as string;
                MainPhoneNumber = ResultSet[6] as string;
                MobilePhoneNumber = ResultSet[7] as string;
                FaxNumber = ResultSet[8] as string;
                PrimaryEmailAddress = ResultSet[9] as string;
                AlternateEmailAddress1 = ResultSet[10] as string;
                AlternateEmailAddress2 = ResultSet[11] as string;
                StreetAddress = ResultSet[12] as string;
                StreetAddress2 = ResultSet[13] as string;
                City = ResultSet[14] as string;
                StateProvinceId = ResultSet[15] as int?;
                PostalCode = ResultSet[16] as string;
                EmailNotifications = ResultSet[17] as bool?;
                TextNotifications = ResultSet[18] as bool?;
            }
        }

        //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
        public Payer(string primaryEmailAddress)
            : this(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_Payer_GetPayerByPrimaryEmailAddress", new SqlParameter("@PrimaryEmailAddress", primaryEmailAddress)))
        {
        }

        public static int GetWalletCount(string RenterID)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
                EfxSettings.ConnectionString,
                "dbo.usp_Payer_GetAchCreditCardCount",
                new SqlParameter("@RenterID", RenterID));
        }

        public Payer()
        {
            InitializeObject();
        }

    }
}
