using System.Data.SqlTypes;
using Mb2x.Data;
using System;
using System.Data.SqlClient;

namespace EfxFramework
{
    public class Lease : BaseEntity
    {
        private int? _RentDue;

        //Public Properties
        public int LeaseId { get; set; }
        public int PropertyId { get; set; }
        public int PropertyStaffId { get; set; }
        public string UnitNumber { get; set; }
        public decimal RentAmount { get; set; }

        public int RentDueDayOfMonth
        {
            get
            {
                if (!_RentDue.HasValue || _RentDue.Value.Equals(0))
                {
                    _RentDue = 1; // Default to first to make date calculations happy
                }

                return _RentDue.Value;
            }
            set
            {
                _RentDue = value;
            }
        }

        public int NumberOfTenants { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal BeginningBalance { get; set; }
        public DateTime BeginningBalanceDate { get; set; }
        public DateTime? ExpectedMoveInDate { get; set; }
        public DateTime? ExpectedMoveOutDate { get; set; }
        public DateTime? ActualMoveInDate { get; set; }
        public DateTime? ActualMoveOutDate { get; set; }
        public DateTime? LeaseSignDate { get; set; }
        public decimal? CurrentBalanceDue { get; set; }
        public DateTime? PaymentDueDate { get; set; }
        public DateTime CurrentBalanceDueLastUpdated { get; set; }
        public decimal TotalFees { get; set; }
        public bool UsesUpdatedCurrentBalanceCalc { get; set; }
        public DateTime? ScriptRunDate { get; set; }

        public static void DeleteLeaseByLeaseId(int leaseId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_Lease_DeleteLeaseByLeaseId", new SqlParameter("@LeaseId", leaseId));
        }

        //Public Methods
        /// <summary>
        /// Assigns a renter to a lease.
        /// </summary>
        public static void AssignRenterToLease(int leaseId, int renterId)
        {
            //Set the assignment
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_LeaseRenter_AssignRenterToLease", new[] { new SqlParameter("@LeaseId", leaseId), new SqlParameter("@RenterId", renterId) });
        }
        /// <summary>
        /// Assigns a renter to a lease.
        /// </summary>
        public static void RemoveRenterFromLease(int leaseId, int renterId)
        {
            //Set the assignment
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_LeaseRenter_RemoveRenterFromLease", new[] { new SqlParameter("@LeaseId", leaseId), new SqlParameter("@RenterId", renterId) });
        }

        /// <summary>
        /// Updates the TotalFees field of a non-integrated lease
        /// </summary>
        public static void UpdateLeaseFees()
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_Lease_UpdateTotalFeesForNonIntegratedProperties");
        }

        /// <summary>
        /// Updates non-integrated leases
        /// </summary>
        public static void UpdateNonIntegratedLeases()
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_Lease_AddCurrentBalanceDueForNonIntegratedProperties");
        }

        public static Lease GetRenterLeaseByRenterId(int renterId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new Lease(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_LeaseRenter_GetLeaseByRenterId", new SqlParameter("@RenterId", renterId)));
        }

        private static DateTime? ConvertToSqlDate(DateTime? date)
        {
            if (!date.HasValue)
                return null;

            if (date < SqlDateTime.MinValue.Value)
                return SqlDateTime.MinValue.Value;

            if (date > SqlDateTime.MaxValue.Value)
                return SqlDateTime.MaxValue.Value;

            return date;
        }

        public static int Set(int leaseId, int propertyId, int propertyStaffId, string unitNumber, decimal rentAmount, int rentDueDayOfMonth, int numberOfTenants, DateTime startDate, DateTime endDate, decimal beginningBalance, DateTime beginningBalanceDate, DateTime? expectedMoveInDate, DateTime? expectedMoveOutDate, DateTime? actualMoveInDate, DateTime? actualMoveOutDate, DateTime? leaseSignDate, decimal? currentBalanceDue, DateTime? paymentDueDate, DateTime currentBalanceDueLastUpdated)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            int returnValue = NewDataAccess.ExecuteScalar<int>(
                EfxSettings.ConnectionString,
                "dbo.usp_Lease_SetSingleObject",
            new[]
                {
	                new SqlParameter("@LeaseId", leaseId),
	                new SqlParameter("@PropertyId", propertyId),
	                new SqlParameter("@PropertyStaffId", propertyStaffId),
	                new SqlParameter("@UnitNumber", unitNumber),
	                new SqlParameter("@RentAmount", rentAmount),
	                new SqlParameter("@RentDueDayOfMonth", rentDueDayOfMonth),
	                new SqlParameter("@NumberOfTenants", numberOfTenants),
	                new SqlParameter("@StartDate", ConvertToSqlDate(startDate)),
	                new SqlParameter("@EndDate", ConvertToSqlDate(endDate)),
                    new SqlParameter("@BeginningBalance", beginningBalance),
                    new SqlParameter("@BeginningBalanceDate", ConvertToSqlDate(beginningBalanceDate)),
                    new SqlParameter("@ExpectedMoveInDate", ConvertToSqlDate(expectedMoveInDate)),
                    new SqlParameter("@ExpectedMoveOutDate", ConvertToSqlDate(expectedMoveOutDate)),
                    new SqlParameter("@ActualMoveInDate", ConvertToSqlDate(actualMoveInDate)),
                    new SqlParameter("@ActualMoveOutDate", ConvertToSqlDate(actualMoveOutDate)),
                    new SqlParameter("@LeaseSignDate", ConvertToSqlDate(leaseSignDate)),
                    new SqlParameter("@CurrentBalanceDue", currentBalanceDue),
                    new SqlParameter("@PaymentDueDate", ConvertToSqlDate(paymentDueDate)),
                    new SqlParameter("@CurrentBalanceDueLastUpdated", ConvertToSqlDate(currentBalanceDueLastUpdated))
                });

            //Salcedo - 11/7/2014 - ridiciulous that we were runing this SP every time we updated a lease - this would really slow down the yardi import process
            //This change I made is not much better, but at least now it only runs when we're actually updating a non-integrated lease
            //Property TheProperty = new Property(propertyId);

            //Salcedo - 12/31/2014 - new leases won't be associated with a renter record yet, so only do those for existing leases.
            //New leases are those where leaseId = 0.
            if (leaseId != 0)
            {
                Renter TheRenter = Renter.GetRenterListByLeaseId(leaseId)[0];
                if (TheRenter.PmsId == null)
                {
                    UpdateNonIntegratedLeases();
                }
                //Salcedo - task 00076
                var log = new RPO.ActivityLog();
                log.WriteLog("System", "Lease record was updated", 0, TheRenter.RenterId.ToString(), true);
            }

            return returnValue;
        }

        public static int Set(Lease l)
        {
            return Set(
                l.LeaseId,
                l.PropertyId,
                l.PropertyStaffId,
                l.UnitNumber,
                l.RentAmount,
                l.RentDueDayOfMonth,
                l.NumberOfTenants,
                l.StartDate,
                l.EndDate,
                l.BeginningBalance,
                l.BeginningBalanceDate,
                l.ExpectedMoveInDate,
                l.ExpectedMoveOutDate,
                l.ActualMoveInDate,
                l.ActualMoveOutDate,
                l.LeaseSignDate,
                l.CurrentBalanceDue,
                l.PaymentDueDate,
                l.CurrentBalanceDueLastUpdated
            );
        }

        public static string GetRentDueDate(int renterId)
        {
            var LeaseObj = GetRenterLeaseByRenterId(renterId);

            if (LeaseObj.LeaseId < 1)
                return DateTime.Now.ToString(EfxSettings.MobileDateFormatString);

            if (DateTime.Now.Day < LeaseObj.RentDueDayOfMonth)
                return new DateTime(DateTime.Now.Year, DateTime.Now.Month, GetModifiedRentDueDayOfMonth(DateTime.Now.Month, LeaseObj.RentDueDayOfMonth)).ToString(EfxSettings.MobileDateFormatString);
            
            if (DateTime.Now.Day > LeaseObj.RentDueDayOfMonth)
                return DateTime.Now.Month == 12 ? new DateTime(DateTime.Now.AddYears(1).Year, DateTime.Now.AddMonths(1).Month, GetModifiedRentDueDayOfMonth(DateTime.Now.AddMonths(1).Month, LeaseObj.RentDueDayOfMonth)).ToString(EfxSettings.MobileDateFormatString) : new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(1).Month, GetModifiedRentDueDayOfMonth(DateTime.Now.AddMonths(1).Month, LeaseObj.RentDueDayOfMonth)).ToString(EfxSettings.MobileDateFormatString);
                
            return DateTime.Now.ToString(EfxSettings.MobileDateFormatString);
        }

        public static DateTime? GetRentDueDateWeb(int renterId)
        {
            var LeaseObj = GetRenterLeaseByRenterId(renterId);

            if (LeaseObj.LeaseId < 1)
                return null;

            if (DateTime.Now.Day < LeaseObj.RentDueDayOfMonth)
                return new DateTime(DateTime.Now.Year, DateTime.Now.Month, GetModifiedRentDueDayOfMonth(DateTime.Now.Month, LeaseObj.RentDueDayOfMonth));

            if (DateTime.Now.Day > LeaseObj.RentDueDayOfMonth)
                return DateTime.Now.Month == 12 ? new DateTime(DateTime.Now.AddYears(1).Year, DateTime.Now.AddMonths(1).Month, GetModifiedRentDueDayOfMonth(DateTime.Now.AddMonths(1).Month, LeaseObj.RentDueDayOfMonth)) : new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(1).Month, GetModifiedRentDueDayOfMonth(DateTime.Now.AddMonths(1).Month, LeaseObj.RentDueDayOfMonth));

            return null;
        }

        private static int GetModifiedRentDueDayOfMonth(int rentDueMonth, int rentDueDayOfMonth)
        {
            switch (rentDueMonth)
            {
                case 2:
                    if (DateTime.IsLeapYear(DateTime.Now.Year))
                    {
                        if (rentDueDayOfMonth > 29)
                            return 29;
                    }
                    else
                    {
                        if (rentDueDayOfMonth > 28)
                            return 28;
                    }
                    break;

                case 4:
                case 6:
                case 9:
                case 11:
                    if (rentDueDayOfMonth > 30)
                        return 30;
                    break;
            }

            return rentDueDayOfMonth;
        }

        public Lease(int? leaseId)
        {
            //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_Lease_GetSingleObject", new[] { new SqlParameter("@LeaseId", leaseId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
                LeaseId = (int)ResultSet[0];
                PropertyId = (int)ResultSet[1];
                PropertyStaffId = (int)ResultSet[2];
                UnitNumber = ResultSet[3] as string;
                RentAmount = (decimal)ResultSet[4];
                RentDueDayOfMonth = (int)ResultSet[5];
                NumberOfTenants = (int)ResultSet[6];
                StartDate = (DateTime)ResultSet[7];
                EndDate = (DateTime)ResultSet[8];
                BeginningBalance = (decimal) ResultSet[9];
                BeginningBalanceDate = (DateTime) ResultSet[10];
                ExpectedMoveInDate = ResultSet[11] as DateTime?;
                ExpectedMoveOutDate = ResultSet[12] as DateTime?;
                ActualMoveInDate = ResultSet[13] as DateTime?;
                ActualMoveOutDate = ResultSet[14] as DateTime?;
                LeaseSignDate = ResultSet[15] as DateTime?;
                CurrentBalanceDue = ResultSet[16] as decimal?;
                PaymentDueDate = ResultSet[17] as DateTime?;
                CurrentBalanceDueLastUpdated = (DateTime) ResultSet[18];
                TotalFees = Decimal.Parse(ResultSet[19].ToString());
                UsesUpdatedCurrentBalanceCalc = Boolean.Parse(ResultSet[20].ToString());
                ScriptRunDate = ResultSet[21] as DateTime?;
            }
        }

        public Lease()
        {
            InitializeObject();
        }

    }
}
