﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

//Salcedo - 3/8/2015 - added to assist with fine tuning exception handling in ExecuteScalar function
using System.Net.Mail;

// MB2X REPLACEMENT FUNCTIONS

namespace EfxFramework
{
    public class NewDataAccess
    {
        ///////////////////
        //
        // ExecuteNoResults
        //
        ///////////////////

        // This function works differently than the MB2X call, in that it does not return
        // anything, whereas the MB2X function returns a special object that contains the
        // number of rows affected and so on. It doesn't look like we ever use that result,
        // though, and I don't have much documentation on how it works, so I didn't bother
        // with it for now.

        public static void ExecuteNoResults(string connectionString, string storedProcedure)
        {
            ExecuteNoResults(connectionString, storedProcedure, (SqlParameter[])null);
        }

        public static void ExecuteNoResults(string connectionString, string storedProcedure, SqlParameter parameter)
        {
            ExecuteNoResults(connectionString, storedProcedure, new SqlParameter[] { parameter });
        }

        public static void ExecuteNoResults(string connectionString, string storedProcedure, SqlParameter[] parameters)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(storedProcedure, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        foreach (SqlParameter TheParam in parameters)
                        {
                            if (TheParam.Value != null)
                                cmd.Parameters.AddWithValue(TheParam.ParameterName, TheParam.Value);
                            else
                                cmd.Parameters.AddWithValue(TheParam.ParameterName, DBNull.Value);
                        }
                        //cmd.Parameters.AddRange(parameters);
                    }
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }

        ///////////////////
        //
        // ExecuteScalar
        //
        ///////////////////

        // Hopefully this function is the same as the equivalent MB2X call.

        public static object ExecuteScalar(string connectionString, string storedProcedure)
        {
            return ExecuteScalar(connectionString, storedProcedure, (SqlParameter[])null);
        }

        public static object ExecuteScalar(string connectionString, string storedProcedure, SqlParameter parameter)
        {
            return ExecuteScalar(connectionString, storedProcedure, new SqlParameter[] { parameter });
        }

        public static object ExecuteScalar(string connectionString, string storedProcedure, SqlParameter[] parameters)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(storedProcedure, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        foreach (SqlParameter TheParam in parameters)
                        {
                            if (TheParam.Value != null)
                                cmd.Parameters.AddWithValue(TheParam.ParameterName, TheParam.Value);
                            else
                                cmd.Parameters.AddWithValue(TheParam.ParameterName, DBNull.Value);
                        }
                        //cmd.Parameters.AddRange(parameters);
                    }
                    conn.Open();
                    //return cmd.ExecuteScalar();
                    object ReturnValue = cmd.ExecuteScalar();
                    conn.Close();
                    return ReturnValue;
                }
            }
        }

        ///////////////////
        //
        // ExecuteScalar<T>
        //
        ///////////////////

        // Hopefully this function is the same as the equivalent MB2X call.

        public static T ExecuteScalar<T>(string connectionString, string storedProcedure)
        {
            return ExecuteScalar<T>(connectionString, storedProcedure, (SqlParameter[])null);
        }

        public static T ExecuteScalar<T>(string connectionString, string storedProcedure, SqlParameter parameter)
        {
            return ExecuteScalar<T>(connectionString, storedProcedure, new SqlParameter[] { parameter });
        }

        public static T ExecuteScalar<T>(string connectionString, string storedProcedure, SqlParameter[] parameters)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(storedProcedure, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        foreach (SqlParameter TheParam in parameters)
                        {
                            if (TheParam.Value != null)
                                cmd.Parameters.AddWithValue(TheParam.ParameterName, TheParam.Value);
                            else
                                cmd.Parameters.AddWithValue(TheParam.ParameterName, DBNull.Value);
                        }
                        //cmd.Parameters.AddRange(parameters);
                    }
                    conn.Open();
                    //return (T)cmd.ExecuteScalar();

                    T ReturnValue; //= (T)cmd.ExecuteScalar();
                    try
                    {
                        ReturnValue = (T)cmd.ExecuteScalar();
                    }
                    //Salcedo - 3/8/2015 - added code to log the exception generated, and send an email when it occurs
                    catch (Exception ex)
                    {
                        // Salcedo - 4/20/2015 - don't send email for this predictable behavior. We know we can get this exception on these SP's,
                        // until we change the code that uses them and/or the SP's themselves.
                        string TheException = ex.ToString();
                        if (TheException.Contains("System.InvalidCastException: Unable to cast object of type 'System.DBNull' to type 'System.Byte[]'.") && 
                                (TheException.Contains("usp_Company_GetLogo") ||
                                TheException.Contains("usp_Property_GetApplicationForResidency") ||
                                TheException.Contains("usp_Renter_GetPhoto") ||
                                TheException.Contains("usp_Property_GetPhoto1") ||
                                TheException.Contains("usp_Property_GetPhoto2") ||
                                TheException.Contains("usp_Property_GetPhoto3") ||
                                TheException.Contains("usp_Property_GetPhoto4")))
                        {
                            ReturnValue = default(T);
                        }
                        else
                        { 
                            string TheMessage = "The following error exception occurred in ExecuteScalar. ConnectionString: " +
                                connectionString + ". Procedure: " + storedProcedure + ". Parameters: ";
                            if (parameters != null)
                            {
                                foreach (SqlParameter TheParam in parameters)
                                {
                                    if (TheParam.Value != null)
                                        TheMessage += TheParam.ParameterName + "->" + TheParam.Value.ToString() + "; ";
                                    else
                                        TheMessage += TheParam.ParameterName + "->null; ";
                                }
                            }
                            TheMessage += ". Exception detail: " + ex.ToString();
                            RPO.ActivityLog AL = new RPO.ActivityLog();
                            AL.WriteLog("EfxFramework", TheMessage, (short)RPO.LogPriority.LogAlways);
                            EfxFramework.BulkMail.BulkMail.SendMailMessage(
                                null,
                                "EFX",
                                false,
                                EfxFramework.EfxSettings.SendMailUsername,
                                EfxFramework.EfxSettings.SendMailPassword,
                                new MailAddress(EfxFramework.EfxSettings.SupportEmail),
                                "EfxFramework ExecuteScalar Exception - Please review",
                                TheMessage,
                                new List<MailAddress> { new MailAddress(EfxSettings.TechSupportEmail) });

                            ReturnValue = default(T);
                        }
                    }
                    //object Temp = (T)cmd.ExecuteScalar();
                    //if (Temp != null)
                    //    ReturnValue = (T)Temp;
                    //else
                    //    ReturnValue = default(T);

                    conn.Close();
                    return ReturnValue;
                }
            }
        }

        ///////////////////
        //
        // ExecuteOneRow
        //
        ///////////////////

        // Hopefully this function is the same as the equivalent MB2X call.

        public static object[] ExecuteOneRow(string connectionString, string storedProcedure)
        {
            return ExecuteOneRow(connectionString, storedProcedure, (SqlParameter[])null);
        }

        public static object[] ExecuteOneRow(string connectionString, string storedProcedure, SqlParameter parameter)
        {
            return ExecuteOneRow(connectionString, storedProcedure, new SqlParameter[] { parameter });
        }

        public static object[] ExecuteOneRow(string connectionString, string storedProcedure, SqlParameter[] parameters)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(storedProcedure, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        foreach (SqlParameter TheParam in parameters)
                        {
                            if (TheParam.Value != null)
                                cmd.Parameters.AddWithValue(TheParam.ParameterName, TheParam.Value);
                            else
                                cmd.Parameters.AddWithValue(TheParam.ParameterName, DBNull.Value);
                        }
                        //cmd.Parameters.AddRange(parameters);
                    }
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    try
                    {
                        while (dr.Read())
                        {
                            try
                            {
                                object[] row = new object[dr.FieldCount];
                                dr.GetValues(row);
                                return row;
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("exception reading data row: " + e.Message);
                                return null;
                            }
                        }
                    }
                    finally
                    {
                        dr.Close();
                    }
                    conn.Close();
                }
            }
            return null;
        }

        ///////////////////
        //
        // ExecuteOneRowAsDictionary
        //
        ///////////////////

        // This is an alternative to ExecuteOneRow. Instead of returning the columns as an
        // array, it constructs a dictionary keyed by column name, and returns that. I
        // personally don't like this approach, since it requires columns to be named
        // uniquely, and is less efficient, but it's there for use if necessary.

        public static Dictionary<string, object> ExecuteOneRowAsDictionary(string connectionString, string storedProcedure)
        {
            return ExecuteOneRowAsDictionary(connectionString, storedProcedure, (SqlParameter[])null);
        }

        public static Dictionary<string, object> ExecuteOneRowAsDictionary(string connectionString, string storedProcedure, SqlParameter parameter)
        {
            return ExecuteOneRowAsDictionary(connectionString, storedProcedure, new SqlParameter[] { parameter });
        }

        public static Dictionary<string, object> ExecuteOneRowAsDictionary(string connectionString, string storedProcedure, SqlParameter[] parameters)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(storedProcedure, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        foreach (SqlParameter TheParam in parameters)
                        {
                            if (TheParam.Value != null)
                                cmd.Parameters.AddWithValue(TheParam.ParameterName, TheParam.Value);
                            else
                                cmd.Parameters.AddWithValue(TheParam.ParameterName, DBNull.Value);
                        }
                        //cmd.Parameters.AddRange(parameters);
                    }
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    try
                    {
                        while (dr.Read())
                        {
                            try
                            {
                                Dictionary<string, object> dict = new Dictionary<string, object>();
                                for (int i = 0; i < dr.FieldCount; i++)
                                {
                                    string name = dr.GetName(i);
                                    if (name != null) dict[name] = dr.GetValue(i);
                                }
                                return dict;
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("exception reading data row: " + e.Message);
                                return null;
                            }
                        }
                    }
                    finally
                    {
                        dr.Close();
                    }
                    conn.Close();
                }
            }
            return null;
        }

        ///////////////////
        //
        // GetTypedListFromDataRows
        //
        ///////////////////

        // This is designed as a replacement for the MB2X GetTypedList. It has a different
        // name to draw attention to the fact that changes are required. The MB2X function
        // expected to be passed a stored procedure returning a list of unique ID numbers.
        // It then called the constructor for each row, passing the unique ID -- thus
        // generating another database query for each row.
        //
        // This function, by contrast, expects a stored procedure returning all the data.
        // It then calls a constructor for each row which takes an SqlDataReader as an
        // argument. The constructor gets the data from the SqlDataReader and uses it to
        // initialize the object. This should be more efficient, a single database query
        // replaces a query for each row (plus one to get the list of IDs).
        //
        // In order to switch to this function, two new things are required: a stored
        // procedure returning all the data, and a constructor which takes an
        // SqlDataReader as the single argument. Additionally, the type must implement the
        // INewDataAccessTypedListType interface, to guarantee to the generic template
        // that the type implements the required constructor.

        public static List<T> GetTypedListFromDataRows<T>(string connectionString, string storedProcedure) where T : INewDataAccessTypedListType
        {
            return GetTypedListFromDataRows<T>(connectionString, storedProcedure, (SqlParameter[])null);
        }

        public static List<T> GetTypedListFromDataRows<T>(string connectionString, string storedProcedure, SqlParameter parameter) where T : INewDataAccessTypedListType
        {
            return GetTypedListFromDataRows<T>(connectionString, storedProcedure, new SqlParameter[] { parameter });
        }

        public static List<T> GetTypedListFromDataRows<T>(string connectionString, string storedProcedure, SqlParameter[] parameters) where T : INewDataAccessTypedListType
        {
            List<T> list = new List<T>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(storedProcedure, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        foreach (SqlParameter TheParam in parameters)
                        {
                            if (TheParam.Value != null)
                                cmd.Parameters.AddWithValue(TheParam.ParameterName, TheParam.Value);
                            else
                                cmd.Parameters.AddWithValue(TheParam.ParameterName, DBNull.Value);
                        }
                        //cmd.Parameters.AddRange(parameters);
                    }
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    try
                    {
                        while (dr.Read())
                        {
                            try
                            {
                                list.Add((T)Activator.CreateInstance(typeof(T), dr));
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("exception reading data row: " + e.Message);
                            }
                        }
                    }
                    finally
                    {
                        dr.Close();
                    }
                    conn.Close();
                }
            }
            return list;
        }

        //cakel: TASK 00513 Added Generic Sqldatareader to avoid ordered list 
        //msalcedo: 10/30/2015 - This new function doesn't actually return one row - it returns a SqlDataReader, so I'm 
        //renaming the function from ExecuteOneRow to ExecuteProcedure
        public static SqlDataReader ExecuteProcedure(string _ConnectionString, string _StoreProcedure, SqlParameter[] parameters)
        {
            SqlConnection con = new SqlConnection(_ConnectionString);
            SqlCommand com = new SqlCommand(_StoreProcedure, con);
            com.CommandType = CommandType.StoredProcedure;

            if (parameters != null)
            {
                foreach (SqlParameter TheParam in parameters)
                {
                    if (TheParam.Value != null)
                        com.Parameters.AddWithValue(TheParam.ParameterName, TheParam.Value);
                    else
                        com.Parameters.AddWithValue(TheParam.ParameterName, DBNull.Value);
                }

            }

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

        }


    }

    //Salcedo - 10/30/2015
    //Moved this logic into MiscDatabase - it doesn't really belong in a general data access class file
    //public class RPO_DataAccess
    //{
    //    //msalcedo - 10/30/2015 - this function is redundant - removing altogether
    //    //cakel: Added Generic Sqldatareader to avoid ordered list 
    //    //public static SqlDataReader ReturnOneRow(string _ConnectionString, string _StoreProcedure, SqlParameter[] parameters)
    //    //{
    //    //    SqlConnection con = new SqlConnection(_ConnectionString);
    //    //    SqlCommand com = new SqlCommand(_StoreProcedure, con);
    //    //    com.CommandType = CommandType.StoredProcedure;

    //    //    if (parameters != null)
    //    //    {
    //    //        foreach (SqlParameter TheParam in parameters)
    //    //        {
    //    //            if (TheParam.Value != null)
    //    //                com.Parameters.AddWithValue(TheParam.ParameterName, TheParam.Value);
    //    //            else
    //    //                com.Parameters.AddWithValue(TheParam.ParameterName, DBNull.Value);
    //    //        }

    //    //    }

    //    //    try
    //    //    {
    //    //        con.Open();
    //    //        return com.ExecuteReader(CommandBehavior.CloseConnection);
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        con.Close();
    //    //        throw ex;
    //    //    }

    //    //}

    //    //cakel: TASK 00506 - This is to ensure the record is updated with the correct payment status.
    //    //public static void UpdateVoidStatusForPayment(int PaymentID, int PaymentStatusID)
    //    //{
    //    //    SqlConnection con = new SqlConnection(Settings.ConnectionString);
    //    //    SqlCommand com = new SqlCommand("Payment_UpdatePaymentStatus", con);
    //    //    com.CommandType = CommandType.StoredProcedure;

    //    //    com.Parameters.Add("@PaymentID", SqlDbType.Int).Value = PaymentID;
    //    //    com.Parameters.Add("@PaymentStatusID", SqlDbType.Int).Value = PaymentStatusID;

    //    //    try
    //    //    {
    //    //        con.Open();
    //    //        com.ExecuteNonQuery();
    //    //        con.Close();
    //    //    }
    //    //    catch(Exception ex)
    //    //    {
    //    //        con.Close();
    //    //        throw ex;
    //    //    }

    //    //}



    //}
}
