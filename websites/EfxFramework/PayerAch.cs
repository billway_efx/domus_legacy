using System.Collections.Generic;
using System.Data.SqlClient;
using EfxFramework.ExtensionMethods;
using Mb2x.Data;
using System;

namespace EfxFramework
{
	public class PayerAch : BaseEntity
	{
		//Public Properties
		public int PayerAchId { get; set; }
		public int PayerId { get; set; }
		public string BankAccountNumber { get; set; }
		public string BankRoutingNumber { get; set; }
        public int BankAccountTypeId { get; set; }
		public bool IsPrimary { get; set; }

        public string MaskedBankAccountNumber { get { return BankAccountNumber.MaskAccountNumber(); } }

	    public string BankAccountType
	    {
	        get
	        {
	            switch (BankAccountTypeId)
	            {
	                case 1:
	                    return "CHECKING";
                    case 2:
	                    return "SAVINGS";
                    default:
	                    return "CHECKING";
	            }
	        }
	    }

        public static List<PayerAch> GetAllPayerAchByPayerId(int payerId)
        {
            return DataAccess.GetTypedList<PayerAch>(EfxSettings.ConnectionString, "dbo.usp_PayerAch_GetAllPayerAchByPayerId", new SqlParameter("@PayerId", payerId));
        }

	    public static PayerAch GetPrimaryPayerAchByPayerId(int payerId)
	    {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
	        return new PayerAch(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_PayerAch_GetPrimaryPayerAchByPayerId", new SqlParameter("@PayerId", payerId)));
	    }

	    public static int Set(int payerAchId, int payerId, string bankAccountNumber, string bankRoutingNumber, int bankAccountTypeId, bool isPrimary)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_PayerAch_SetSingleObject",
                new[]
                {
	                new SqlParameter("@PayerAchId", payerAchId),
	                new SqlParameter("@PayerId", payerId),
	                new SqlParameter("@BankAccountNumber", bankAccountNumber),
	                new SqlParameter("@BankRoutingNumber", bankRoutingNumber),
                    new SqlParameter("@BankAccountTypeId", bankAccountTypeId),
	                new SqlParameter("@IsPrimary", isPrimary)
                });
        }

        public static int Set(PayerAch p)
        {
	        return Set(
		        p.PayerAchId,
		        p.PayerId,
		        p.BankAccountNumber,
		        p.BankRoutingNumber,
                p.BankAccountTypeId,
		        p.IsPrimary
	        );
        }

        public PayerAch(int? payerAchId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_PayerAch_GetSingleObject", new [] { new SqlParameter("@PayerAchId", payerAchId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
                BankAccountTypeId = 1;
            }
            else
            {
                //Otherwise, populate this object
		        PayerAchId = (int)ResultSet[0];
		        PayerId = (int)ResultSet[1];
		        BankAccountNumber = ResultSet[2] as string;
		        BankRoutingNumber = ResultSet[3] as string;
                BankAccountTypeId = (int) ResultSet[4];
		        IsPrimary = (bool)ResultSet[5];
            }
        }

	    public PayerAch()
	    {
	    }

        //CMallory - Task 00173 - Added method below.
        public static void DeletePayerSavedACH(int renterId)
	    {
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_PayerAch_DeleteSingleObject", new SqlParameter("@RenterID", renterId));
	    }

        public static int CheckIfRenterACHExist(string RenterId, string AccountNumber, string RoutingNumber)
        {
            int RecordCount = 0;
            RecordCount = (int)EfxFramework.NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "usp_PayerAch_CheckIfRenterACHExist",
            new[] { 
                    new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) ,

                    //The @PaymentMethodId is used to update the correct record in the PayerCreditCard table (where PayerCreditCardId = @PaymentMethodId)
                    //It can be retrieved from the Pay_PayableControllerWallet table's PayerMethodId column.
                    new SqlParameter("@AccountNumber", AccountNumber) ,
                    new SqlParameter("@RoutingNumber", RoutingNumber)});
            return RecordCount;
        }

        public static void MakePrimaryACH(int PayerACHID)
	    {
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_PayerAch_MakePrimaryACH", 
                new[]{
                        //new SqlParameter("@RenterID", RenterId),
                        new SqlParameter("@PayerACHID", PayerACHID)});
                    
	    }
    }
}
