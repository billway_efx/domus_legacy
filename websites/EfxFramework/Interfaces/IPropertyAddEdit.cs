﻿using EfxFramework.Interfaces.UserControls.ImageAdder;
using EfxFramework.Interfaces.UserControls.Property;
using EfxFramework.ViewPresentation.Interfaces.UserControls;

namespace EfxFramework.Interfaces
{
    public interface IPropertyAddEdit : IBasePage
    {
        int CurrentPropertyId { get; set; }
        IPropertyDetailsAddEdit PropertyDetailInfo { get; }
        IImageAdder ImageAdder { get; }
        IPmsAddEdit PmsSelection { get; }
        IProgramInformationAddEdit ProgramInformation { get; }
        IBankingInformationAddEdit BankingInformation { get; }
        //IAddStaffToProperty AddStaffToProperty { get; }
    }
}
