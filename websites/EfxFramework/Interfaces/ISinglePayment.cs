﻿using System;
using System.Web.UI.WebControls;
using EfxFramework.Interfaces.UserControls.PaymentSteps;

namespace EfxFramework.Interfaces
{
    public interface ISinglePayment : IBasePage
    {
        MultiView PaymentView { get; }
        View StepOne { get; }
        View StepTwo { get; }
        EventHandler ConfirmButtonClicked { set; }
        EventHandler NextButtonClicked { set; }
        EventHandler BackButtonClicked { set; }
        IOneTimePaymentStepOne StepOneControl { get; }
        IOneTimePaymentStepTwo StepTwoControl { get; }
    }
}
