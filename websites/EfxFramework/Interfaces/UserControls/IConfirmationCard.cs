﻿using System;
using System.Collections.Generic;

namespace EfxFramework.Interfaces.UserControls
{
    public interface IConfirmationCard
    {
        EventHandler EditButtonClick { set; }
        string ConfirmationTextHtml { set; }
        List<string> DataSource { get; set; }
    }
}