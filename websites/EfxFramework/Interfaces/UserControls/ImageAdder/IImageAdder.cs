﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.UserControls.ImageAdder
{
    public interface IImageAdder
    {
        EventHandler AddPhotoClicked { set; }
        EventHandler<ListViewCommandEventArgs> ItemCommandClicked { set; }
        List<byte[]> CurrentPhotos { get; set; }
        List<Photo> PhotoDataSource { set; }
        byte[] FileUploadBytes { get; }
        int MaxNumberOfPhotos { get; set; }
        bool CanAddMorePhotos { set; }

        void ClearPhotos();
    }
}
