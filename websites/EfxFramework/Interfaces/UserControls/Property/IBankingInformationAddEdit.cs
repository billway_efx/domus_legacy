﻿namespace EfxFramework.Interfaces.UserControls.Property
{
    public interface IBankingInformationAddEdit
    {
        int PropertyId { get; set; }
        string RentalBankNameText { get; set; }
        string RentalBankNameAccountText { get; set; }
        string RentalBankNameRoutingText { get; set; }
        string SecurityBankNameText { get; set; }
        string SecurityBankNameAccountText { get; set; }
        string SecurityBankNameRoutingText { get; set; }
        string FeeBankNameText { get; set; }
        string FeeBankNameAccountText { get; set; }
        string FeeBankNameRoutingText { get; set; }
        //cakel: BUGID00213 added new properties
        string CCDepositBankNameText { get; set; }
        string CCDepositAccountNumberText { get; set; }
        string CCDepositRoutingNumberText { get; set; }
        //cakel: BUGID00266 added class properties
        string CashDepositBankNameText { get; set; }
        string CashDepositAccountNumberText { get; set; }
        string CashDepositRoutingNumberText { get; set; }
        
    }
}
