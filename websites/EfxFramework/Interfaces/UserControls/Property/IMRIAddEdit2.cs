﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfxFramework.Interfaces.UserControls.Property
{
    //This will replace the old MRI FTP code
    //cakel: 00382 - MRI

    public interface IMRIAddEdit2
    {
        int PropertyId { get; set; }
        string MRIClientID { get; set; }
        string MRIDatabaseName { get; set; }
        string MRIWebServiceUserName { get; set; }
        string MRIPartnerKey { get; set; }
        string MRIImportEndPoint { get; set; }
        string MRIExportEndPoint { get; set; }
        string MRIWebServicePassword { get; set; }
    }
}
