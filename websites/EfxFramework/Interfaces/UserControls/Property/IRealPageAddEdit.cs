﻿
//cakel: 00351 RealPage integration
namespace EfxFramework.Interfaces.UserControls.Property
{
    public interface IRealPageAddEdit
    {
        int PropertyId { get; set; }
        string UsernameText { get; set; }
        string PasswordText { get; set; }
        string RealPagePMCID { get; set; }
        string RealPageImportEndPoint { get; set; }
        string RealPageExportEndPoint { get; set; }
        string RealPageInternalUser { get; set; }
        string RealPageSiteID { get; set; }
    }
}