﻿using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.UserControls.Property
{
    public interface IPmsAddEdit
    {
        int PropertyId { get; set; }
        DropDownList PmsDropdownList { get; }
        MultiView PmsViews { get; }
        View Blank { get; }
        View Yardi { get; }
        View Amsi { get; }
        View RealPage { get; }
        View MRI { get; }
        IYardiAddEdit YardiControl { get; }
        //IMRIAddEdit MRIControl { get; }
        //cakel: 00382 MRI
        IMRIAddEdit2 MRIControl2 { get; }
        //cakel: BUGID00316
        IAmsiAddEdit AmsiControl { get; }
        //cakel: 00351 RealPage
        IRealPageAddEdit RealPageControl { get; }
    }
}
