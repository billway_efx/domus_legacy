﻿namespace EfxFramework.Interfaces.UserControls.Property
{

    public interface IMRIAddEdit
    {
        int PropertyId { get; set; }
        bool EnableFtpUpload { get; set; }          //Property.MRIEnableFtpUpload
        bool EnableFtpDownload { get; set; }        //Property.MRIEnableFtpDownload
        string MRIPropertyIDText { get; set; }      //Property.PmsId
        string FTPSiteAddress { get; set; }         //Property.MRIFtpSiteAddress
        string FTPPort { get; set; }                //Property.MRIFtpPort
        string FTPAccountUserName { get; set; }     //Property.MRIFTPAccountUserName
        string FTPAccountPassword { get; set; }     //Property.MRIFTPAccountPassword
    }

}
