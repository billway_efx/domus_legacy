﻿namespace EfxFramework.Interfaces.UserControls.Property
{
    public interface IEfxSalesAddEdit
    {
        int CompanyId { get; set; }
        string FirstNameText { get; set; }
        string LastNameText { get; set; }
        string TelephoneNumberText { get; set; }
    }
}
