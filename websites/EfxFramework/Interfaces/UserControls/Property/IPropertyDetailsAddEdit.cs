﻿using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.UserControls.Property
{
    public interface IPropertyDetailsAddEdit
    {
        int PropertyId { get; set; }
        string PropertyCodeText { get; set; }
        string PropertyNameText { get; set; }
        //cakel: 00498
        string PropertyNameTwoText { get; set; }
        string NumberOfUnitsText { get; set; }
        string PropertyAddressText { get; set; }
        string PropertyAddress2Text { get; set; }
        string CityText { get; set; }
        DropDownList StateList { get; }
        DropDownList TimeZoneList { get; }
        string PostalCodeText { get; set; }
        string MainTelephoneText { get; set; }
        string AlternateTelephoneText { get; set; }
        string FaxNumberText { get; set; }
        string OfficeEmailText { get; set; }
        // Patrick Whittingham - 9/2/15 - task #371 : add notification email address 
        string NotificationEmailText { get; set; }
        string AverageRentText { get; set; }
        string ConvenienceFeeText { set; }
        List<Company> PmCompanyDataSource { set; }
        string PmCompanySelectedValue { get; set; }
        bool LineItemDetailChecked { get; set; }
        bool RentReportersVerifiedChecked { get; set; }
        bool AllowPartialPayments { get; set; }
        //cakel: BUGID00019, BUGID00276
        bool AllowPartialPaymentsResidentPortal { get; set; }
        bool AcceptACHPayments { get; set; }

        //cakel: Added BugID 0007
        string NumOfPrePayDays { get; set; }
        string PaymentDueDay { get; set; }
        bool AllowPrePayments { get; set; }

        bool AcceptCCPayments { get; set; }
        bool PNMEnabled { get; set; }
        //CMallory - Task 00425
        bool DisableAllPayments { get; set; }
        //CMallory - Task 00554 
        bool DisablePayByText { get; set; }
        //CMallory - Task 00166 - Added
        bool PropertyIsPublic { get; set; }
    }
}
