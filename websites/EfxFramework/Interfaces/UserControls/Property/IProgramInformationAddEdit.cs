﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.UserControls.Property
{
    public interface IProgramInformationAddEdit
    {
        int PropertyId { get; set; }
        EventHandler ProgramSelected { set; }
        string ProgramSelectedValue { get; set; }
        List<PropertyPaidProgram> PropertyPaidProgramDataSource { set; }
        List<AirProgram> AirProgramDataSource { set; }
        List<SpaProgram> SpaProgramDataSource { set; }
        MultiView ProgramMultiview { get; }
        View NoneSelectedView { get; }
        View PropertyPaidView { get; }
        View AirView { get; }
        View SpaView { get; }
    }
}
