﻿namespace EfxFramework.Interfaces.UserControls.Property
{
    public interface IYardiAddEdit
    {
        int PropertyId { get; set; }
        string UsernameText { get; set; }
        string PasswordText { get; set; }
        string ServerNameText { get; set; }
        string DatabaseNameText { get; set; }
        string PlatformText { get; set; }
        string EndpointText { get; set; }
        string PropertyIdText { get; set; }

        //Salcedo - Issue # 0000179
        bool YardiDepositDateCheckbox { get; set; }
    }

}
