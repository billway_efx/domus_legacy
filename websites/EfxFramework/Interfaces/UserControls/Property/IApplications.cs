﻿using EfxFramework.Interfaces.UserControls.PaymentControlsV2;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.UserControls.Property
{
    public interface IApplications
    {
        int PropertyId { get; }
        int? ApplicantApplicationId { get; }
        Page ParentPage { get; }
        byte[] ApplicationBytes { get; }
        string ApplicationLabelText { set; }
        EventHandler UploadButtonClicked { set; }
        bool DeleteButtonVisible { set; }
        EventHandler DeleteButtonClicked { set; }
        string FirstNameText { get; }
        string LastNameText { get; }
        string EmailAddressText { get; }
        DropDownList ApplicationStatusList { get; }
        DropDownList ApplicationReceivedList { get; }
        EventHandler AddApplicantClicked { set; }
        GridView ApplicantsGrid { get; }
        EventHandler SaveApplicantsClicked { set; }
        IPersonalInformation PersonalInformationControl { get; }
        IResidentBillingInformation BillingInformationControl { get; }
        ISelectPaymentType SelectPaymentControl { get; }
        IPaymentAmountsAlternate PaymentAmountsControl { get; }
        EventHandler SubmitPaymentClicked { set; }
        bool IsPropertyManagerSite { get; }
        bool PaymentButtonEnabled { set; }
        GridViewRowEventHandler RowBound { set; }
    }
}
