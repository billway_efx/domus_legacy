﻿namespace EfxFramework.Interfaces.UserControls.Property
{
    //cakel: BUGID00316
    public interface IAmsiAddEdit
    {
        int PropertyId { get; set; }
        string UsernameText { get; set; }
        string PasswordText { get; set; }
        string DatabaseNameText { get; set; }
        string PropertyIdText { get; set; }
        string ClientMerchantID { get; set; }
        //cakel: 00551
        string AchClientMerchantID { get; set; }
        string CashClientMerchantID { get; set; }

        string AmsiImportEndPoint { get; set; }
        string AmsiExportEndPoint { get; set; }
    }
}


