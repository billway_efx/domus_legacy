﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.UserControls.Blogs
{
    public interface IBlog
    {
        Page ParentPage { get; }
        AnnouncementType TypeOfAnnouncement { get; set; }
        int AnnouncementId { get; }
        int AnnouncementTypeId { get; }
        MultiView BlogsView { get; }
        View BlogsOverview { get; }
        View BlogDetails { get; }
        EventHandler MultiViewChanged { set; }
        List<Announcement> BlogsGridDataSource { set; }
        EventHandler AddNewBlogClicked { set; }
        string BlogTitleText { get; set; }
        string BlogContentText { get; set; }
        EventHandler SaveClicked { set; }
        EventHandler PreviewClicked { set; }
        EventHandler CancelClicked { set; }
        bool PublishChecked { get; set; }
        string PreviewLocation { set; }
    }
}
