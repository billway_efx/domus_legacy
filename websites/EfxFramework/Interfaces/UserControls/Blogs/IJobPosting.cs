﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.UserControls.Blogs
{
    public interface IJobPosting
    {
        Page ParentPage { get; }
        int JobPostingId { get; }
        MultiView JobPostingsView { get; }
        View JobsOverview { get; }
        View JobDetails { get; }
        EventHandler MultiViewChanged { set; }
        List<JobPosting> JobsGridDataSource { set; }
        EventHandler AddNewJobClicked { set; }
        string JobTitleText { get; set; }
        string JobDescriptionText { get; set; }
        string DutiesText { get; set; }
        string SkillsText { get; set; }
        EventHandler SaveClicked { set; }
        bool PublishChecked { get; set; }
    }
}
