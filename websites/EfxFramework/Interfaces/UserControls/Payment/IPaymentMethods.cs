﻿using System;
using System.Web.UI;

namespace EfxFramework.Interfaces.UserControls.Payment
{
    public interface IPaymentMethods
    {
        EventHandler PaymentMethodsSelected { set; }
        EventHandler UpdatePanelPreRender { set; }
        UpdatePanel PaymentMethodsPanel { get; }
        string PaymentMethodsSelectedValue { get; set; }
        string PaymentMethodsSelectedText { get; }
        string BankAccountNumberText { get; set; }
        string RoutingNumberText { get; set; }
        string NameOnCardText { get; set; }
        string CreditCardNumberText { get; set; }
        string ExpirationDateText { get; set; }
        string CvvText { get; set; }
        bool EcheckPanelVisible { get; set; }
        bool CreditCardPanelVisible { get; set; }
        string CurrentRenterId { get; set; }
    }
}
