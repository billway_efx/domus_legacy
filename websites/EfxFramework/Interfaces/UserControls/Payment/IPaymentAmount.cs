﻿namespace EfxFramework.Interfaces.UserControls.Payment
{
    public interface IPaymentAmount
    {
        string DisplayNameText { set; }
        string RentAmountText { get; set; }
        string RentDescriptionText { get; set; }
        string OtherAmount1Text { get; set; }
        string OtherDescription1Text { get; set; }
        string OtherAmount2Text { get; set; }
        string OtherDescription2Text { get; set; }
        string OtherAmount3Text { get; set; }
        string OtherDescription3Text { get; set; }
        string OtherAmount4Text { get; set; }
        string OtherDescription4Text { get; set; }
        string OtherAmount5Text { get; set; }
        string OtherDescription5Text { get; set; }
    }
}
