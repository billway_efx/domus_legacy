﻿namespace EfxFramework.Interfaces.UserControls.Payment
{
    public interface IRenterSitePaymentMethods : IPaymentMethods
    {
        string ConvenienceFeeText { get; set; }
        bool ConvenienceFeeVisible { set; }
    }
}
