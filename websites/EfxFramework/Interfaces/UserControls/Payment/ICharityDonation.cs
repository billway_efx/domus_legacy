﻿namespace EfxFramework.Interfaces.UserControls.Payment
{
    public interface ICharityDonation
    {
        string DonationAmountText { get; set; }
        string SelectedCharityValue { get; set; }
        string SelectedCharityText { get; }
        object CharityDropdownDataSource { set; }
    }
}
