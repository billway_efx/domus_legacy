﻿using System;
using System.Web.UI.WebControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls;

namespace EfxFramework.Interfaces.UserControls.PaymentControlsV2
{
    public interface IPaymentAmountsAlternate
    {
        int RenterId { get; set; }
        IPayment ParentPage { get; }
        MultiView PaymentAmountsView { get; }
        View RentAmountsView { get; }
        View ApplicationAmountsView { get; }
        string RentAmountText { get; set; }
        string ConvenienceFeeText { get; set; }
        string TotalAmountText { set; }
        string PastDueHeaderText { set; }
        string PastDueAmountText { get; set; }
        bool PastDueVisible { set; }
        ListView MonthlyFeesList { get; }
        ListView ApplicationFeesList { get; }
        ISelectPaymentType PaymentTypes { get; set; }
        decimal ConvenienceFeeAmount { get; set; }
        decimal RentAmount { get; set; }
        CheckBox WaiveRent { get; }
        CheckBox WaivePastDue { get; }
        CheckBox WaiveConvenienceFee { get; }
        EventHandler UpdateTotalClicked { set; }
        TextBox OverrideText { get; }
        Label OverrideLabel { get; }
		EventHandler<System.Web.UI.WebControls.ListViewItemEventArgs> FeeListViewItemDataBind { set; }
        //CMallory - AppFee
        string AppConvenienceFeeText { get; set; }
        CheckBox AppWaiveConvenienceFee { get; }
        decimal AppConvenienceFeeAmount { get; set; }
    }
}
