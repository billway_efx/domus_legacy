﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.UserControls.PaymentControlsV2
{
    public interface IPaymentInfoTab
    {
        Page ParentPage { get; }
		PlaceHolder PaymentSuccessPlaceHolder { get; }
        IPersonalInformation PersonalInformationControl { get; }
        ISelectPaymentType SelectPaymentTypeControl { get; }
        IResidentBillingInformation ResidentBillingInformationControl { get; }
        IPaymentAmounts PaymentAmountsControl { get; }
        IAutoPayTile AutoPayTileControl { get; }
        ITextToPayTile TextToPayTileControl { get; }
        EventHandler SaveButtonClicked { set; }
        bool IsPropertyManagerSite { get; }
    }
}
