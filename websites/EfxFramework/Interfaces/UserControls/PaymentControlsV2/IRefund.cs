﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.UserControls.PaymentControlsV2
{
    public interface IRefund
    {
        Page ParentPage { get; }
        string TransactionId { get; }
        string ResidentNameText { set; }
        string TransactionIdText { set; }
        string RefundAmountText { get; set; }
        string ProcessingDateText { set; }
        string RefundDateText { set; }
        string PaymentMethodText { set; }
        Button SubmitRefundBtn { get; }
        HiddenField AchProcessingConfirmedField { get; }
    }
}
