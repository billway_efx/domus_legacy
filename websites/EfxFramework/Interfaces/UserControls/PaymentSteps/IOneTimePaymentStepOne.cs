﻿using EfxFramework.Interfaces.UserControls.Payer;
using EfxFramework.Interfaces.UserControls.Payment;
using EfxFramework.Interfaces.UserControls.Renter;

namespace EfxFramework.Interfaces.UserControls.PaymentSteps
{
    public interface IOneTimePaymentStepOne
    {
        IRenterSelection RenterSelectionControl { get; }
        IPayerInformation PayerInformationControl { get; }
        IPaymentMethods PaymentMethodsControl { get; }
        IPaymentAmount PaymentAmountControl { get; }
        ICharityDonation CharityDonationControl { get; }
    }
}
