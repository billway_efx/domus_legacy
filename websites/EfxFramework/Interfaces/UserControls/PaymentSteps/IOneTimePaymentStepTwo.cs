﻿namespace EfxFramework.Interfaces.UserControls.PaymentSteps
{
    public interface IOneTimePaymentStepTwo
    {
        bool TermsAccepted { get; set; }
        string TermsAndConditionsText { set; }
        IConfirmationCard RenterInformationConfirmation { get; }
        IConfirmationCard PayerInformationConfirmation { get; }
        IConfirmationCard LeasingAgentConfirmation { get; }
        IConfirmationCard PaymentInformationConfirmation { get; }
        IConfirmationCard PropertyInformationConfirmation { get; }
        IConfirmationCard CharityDonationConfirmation { get; }
    }
}
