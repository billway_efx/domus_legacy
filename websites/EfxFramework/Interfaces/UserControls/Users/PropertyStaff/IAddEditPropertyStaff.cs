﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.UserControls.Users.PropertyStaff
{
    public interface IAddEditPropertyStaff
    {
        Page ParentPage { get;  }
        int PropertyId { get; set; }
        int PropertyStaffId { get; set; }
        DropDownList StaffList { get; }
        EventHandler StaffListChanged { set; }
        bool ShowStaffSelection { get; set; }
        string FirstNameText { get; set; }
        string MiddleNameText { get; set; }
        string LastNameText { get; set; }
        string Address1Text { get; set; }
        string Address2Text { get; set; }
        string CityText { get; set; }
        DropDownList StateList { get; }
        string ZipCodeText { get; set; }
        string PrimaryEmailAddressText { get; set; }
        EventHandler EmailAddressChanged { set; }
        string OfficePhoneText { get; set; }
        string MobilePhoneText { get; set; }
        string FaxNumberText { get; set; }
        bool IsMainContactChecked { get; set; }
        bool IsGeneralManagerChecked { get; set; }
        string UsernameText { get; set; }
        string PasswordText { get; }
        string ConfirmPasswordText { get; }
        DropDownList RolesList { get; }
        EventHandler RolesListChanged { set; }
        List<Permission> PermissionsDataSource { set; }
        EventHandler SaveButtonClicked { set; }
        string ZipCodeClientId { get; }
        string OfficePhoneClientId { get; }
        string MobilePhoneClientId { get; }
        string FaxPhoneClientId { get; }
		PlaceHolder SuccessMessagePlaceHolder { get; }

        void BuildPropertyStaffList();
    }
}
