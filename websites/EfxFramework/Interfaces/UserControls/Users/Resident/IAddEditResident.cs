﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.UserControls.Users.Resident
{
    public interface IAddEditResident
    {
        Page ParentPage { get; }
        int ResidentId { get; set; }
        int PropertyId { get; set; }
        string FirstNameText { get; set; }
        string MiddleNameText { get; set; }
        string LastNameText { get; set; }
        string Address1Text { get; set; }
        string Address2Text { get; set; }
        string CityText { get; set; }
        DropDownList StateList { get; }
        string ZipCodeText { get; set; }
        string MainPhoneText { get; set; }
        string MobilePhoneText { get; set; }
        string FaxNumberText { get; set; }
        string PrimaryEmailAddressText { get; set; }
        EventHandler PrimaryEmailAddressChanged { set; }
        bool PmsPanelVisible { set; }
        string PmsIdText { get; set; }
        DropDownList PmsTypeList { get; }
        string UsernameText { get; set; }
        string PasswordText { get; }
        string ConfirmPasswordText { get; }
        EventHandler SaveButtonClicked { set; }
        string ZipCodeClientId { get; }
        string MainPhoneClientId { get; }
        string MobilePhoneClientId { get; }
        string FaxPhoneClientId { get; }
		PlaceHolder SuccessMessagePlaceHolder { get; }
    }
}
