﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.UserControls.Users.Admin
{
    public interface IAddEditEfxAdministrator
    {
        Page ParentPage { get; }
        int EfxAdministratorId { get; set; }
        string FirstNameText { get; set; }
        string LastNameText { get; set; }
        string PrimaryEmailAddressText { get; set; }
        EventHandler PrimaryEmailAddressChanged { set; }
        string UsernameText { get; set; }
        string PasswordText { get; }
        string ConfirmPasswordText { get; }
        DropDownList RolesList { get; }
        EventHandler RolesListChanged { set; }
        List<Permission> PermissionsDataSource { set; }
        EventHandler SaveButtonClicked { set; }
		PlaceHolder SuccessMessagePlaceHolder { get; }
    }
}
