﻿using System.Collections.Generic;

namespace EfxFramework.Interfaces.UserControls
{
    public interface IDetailTile
    {
        string HeaderText { set; }
        string MainDetailHtml { get; set; }
        string DetailsLinkUrl { set; }
        List<string> DataSource { get; set; }
        string AdditionalLinksHtml { set; }
    }
}
