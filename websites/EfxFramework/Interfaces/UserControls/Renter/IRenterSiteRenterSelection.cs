﻿namespace EfxFramework.Interfaces.UserControls.Renter
{
    public interface IRenterSiteRenterSelection : IRenterSelection
    {
        bool RenterIsPayerPanelVisible { get; set; }
        bool RenterIsNotPayerPanelVisible { get; set; }
    }
}
