﻿using System;

namespace EfxFramework.Interfaces.UserControls.Renter
{
    public interface IRenterSelection
    {
        EventHandler AddRenterClicked { set; }
        string RenterDropdownSelectedValue { get; set; }
        string RenterDropdownSelectedText { get; }
        string RenterImageUrl { set; }
        string RenterDisplayNameText { set; }
        string RenterMainPhoneNumberText { set; }
        string RenterPrimaryEmailAddressText { set; }
        bool RenterInformationPanelVisible { get; set; }
        object RenterDropdownDataSource { set; }
    }
}
