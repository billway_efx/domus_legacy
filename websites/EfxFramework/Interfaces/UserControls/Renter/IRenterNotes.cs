﻿using System;
using System.Collections.Generic;
using System.Web.UI;

namespace EfxFramework.Interfaces.UserControls.Renter
{
    public interface IRenterNotes
    {
        int NoteId { get; }
        int RenterId { get; }
        Page ParentPage { get; }
        string NoteTextInput { get; }
        List<RenterNote> NotesGridDataSource { set; }
        string NoteTextView { set; }
        EventHandler SaveButtonClicked { set; }
        bool IsPropertyManagerSite { get; }
        bool ResidentNotesVisible { set; }
    }
}
