﻿using System;

namespace EfxFramework.Interfaces.UserControls.Renter
{
    public interface IPropertyStaffSiteRenterSelection : IRenterSelection
    {
        EventHandler PropertySelected { set; }
        string PropertyDropdownSelectedValue { get; set; }
        string PropertyDropdownSelectedText { get; }
        object PropertyDropdownDataSource { set; }
        bool RenterDropdownListEnabled { get; set; }
    }
}
