﻿using System;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.UserControls.Payer
{
    public interface IPayerInformation
    {
        EventHandler RenterInfoPayerInfoChecked { set; }
        string PayersFirstName { get; set; }
        string PayersMiddleName { get; set; }
        string PayersLastName { get; set; }
        string PayersSuffix { get; set; }
        string PayersEmail { get; set; }
        string PayersAddress { get; set; }
        string PayersAddress2 { get; set; }
        string PayersCity { get; set; }
        string PayersZipCode { get; set; }
        string PayersCellPhone { get; set; }
        string PayersHomePhone { get; set; }
        DropDownList StateDropdown { get; }
        string CurrentRenterId { get; set; }
        bool RenterIsPayerChecked { get; }
    }
}
