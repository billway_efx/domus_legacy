﻿using System;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.UserControls.Account
{
    public interface ILeaseInfo
    {
        IBasePage ParentPage { get; }
        EventHandler PropertySelected { set; }
        EventHandler LeasingAgentSelected { set; }
        EventHandler SaveButtonClicked { set; }
        DropDownList PropertyList { get; }
        DropDownList LeasingAgentList { get; }
        string UnitNumberText { get; set; }
        string RentAmountText { get; set; }
        string DueDayOfMonthText { get; set; }
        string LeaseStartText { get; set; }
        string LeaseEndText { get; set; }
        string NumberOfPeopleOnLeaseText { get; set; }
        string SelectedPropertyName { set; }
        string SelectedPropertyPhotoUrl { set; }
        string SelectedPropertyAddress { set; }
        string SelectedPropertyPhone { set; }
        string SelectedLeasingAgentName { set; }
        string SelectedLeasingAgentPhotoUrl { set; }
        string SelectedLeasingAgentMainPhone { set; }
        string SelectedLeasingAgentCellPhone { set; }
        string SelectedLeasingAgentEmail { set; }
    }
}
