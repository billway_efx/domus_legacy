﻿using EfxFramework.Interfaces.UserControls.Payment;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.UserControls.Account
{
    public interface IPaymentInfo
    {
        IBasePage ParentPage { get; }
        IPaymentAmount AmountControl { get; }
        ICharityDonation CharityControl { get; }
        MultiView PaymentMultiView { get; }
        View CreditCardPaymentView { get; }
        View EcheckPaymentView { get; }
        View SmsPaymentView { get; }
        UpdatePanel PaymentInfoUpdatePanel { get; }
        DropDownList CarrierDropdownList { get; }
        EventHandler PaymentInfoPreRender { set; }
        EventHandler PaymentMethodSelectionChanged { set; }
        EventHandler SaveButtonClicked { set; }
        EventHandler EcheckAutoPaymentChanged { set; }
        EventHandler CreditCardAutoPaymentChanged { set; }
        EventHandler SmsScheduleChanged { set; }
        string PaymentMethodSelectedValue { get; set; }
        string PaymentMethodSelectedText { get; }
        string DayOfEachMonthText { get; set; }
        string BankAccountNumberText { get; set; }
        string RoutingNumberText { get; set; }
        string NameOnCardText { get; set; }
        string CreditCardNumberText { get; set; }
        string ExpirationDateText { get; set; }
        string SmsCellNumberText { get; set; }
        string SmsPaymentMethodSelectedValue { get; set; }
        string SmsPaymentMethodSelectedText { get; }
        string SmsReminderDay { get; set; }
        bool IsPrimaryCheckingAccount { get; set; }
        bool IsPrimaryCreditCard { get; set; }
        bool AutoDebitPanelVisible { set; }
        bool EcheckAutoPaymentChecked { get; set; }
        bool CreditCardAutoPaymentChecked { get; set; }
        bool CreditCardAutoPaymentCheckboxVisible { get; set; }
        bool AutoPaymentAmountVisible { set; }
        bool SmsReminderPanelVisible { set; }
        bool SmsReminderChecked { get; set; }
    }
}
