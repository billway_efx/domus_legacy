﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.UserControls.Account
{
    public interface ILeaseInfoV2
    {
        int RenterId { get; }
        Page ParentPage { get; }
		PlaceHolder SuccessMessagePlaceHolder { get; }
        string RentAmountText { get; set; }
        string FirstMonthStartText { set; }
        IDatePicker FirstMonthStartControl { get; }
        DropDownList FeeList { get; }
        string FeeAmountText { get; set; }
        EventHandler AddFeeClicked { set; }
        List<LeaseFee> FeeGridDataSource { set; }
        DropDownList LeasingAgentList { get; }
        string AgentNameText { set; }
        string AgentPhoneText { set; }
        string AgentEmailText { set; }
        string AgentImageUrl { set; }
        IDatePicker LeaseStartControl { get; }
        IDatePicker LeaseEndControl { get; }
        string UnitNumberText { get; set; }
        string NumberOfPeopleOnLeaseText { get; set; }
        DropDownList PetTypeList { get; }
        string BreedText { get; set; }
        string PetNameText { get; set; }
        EventHandler AddPetClicked { set; }
        List<Pet> PetGridDataSource { set; }
        EventHandler SaveButtonClicked { set; }
    }
}
