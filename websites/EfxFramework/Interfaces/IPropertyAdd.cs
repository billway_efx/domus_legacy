﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces
{
    public interface IPropertyAdd : IPropertyAddEdit
    {
        EventHandler SaveButtonClick { set; }
        MultiView NewPropertySteps { get; }
        View StepOne { get; }
        View StepTwo { get; }
		Page ParentPage { get; }
    }
}
