﻿using System.Web.UI;
using EfxFramework.Interfaces.UserControls.PaymentControlsV2;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;
using System;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces
{
    public interface IPayment
    {
        Page ParentPage { get; }
        DropDownList PropertyList { get; }
        CheckBox ApplyToNextMonthCheckBox { get; }
        EventHandler PayerTypeChanged { set; }
        string SelectedPayerType { get; }
        string PayerLabelText { set; }
        DropDownList PayerList { get; }
        EventHandler LoadButtonClicked { set; }
		//EventHandler CancelButtonClicked { set; }
        bool IsDifferentPayerChecked { get; }
        EventHandler DifferentPayerChecked { set; }
        IPersonalInformation PersonalInformationControl { get; }
        IResidentBillingInformation ResidentBillingInformationControl { get; }
        ISelectPaymentType SelectPaymentTypeControl { get; }
        IPaymentAmountsAlternate PaymentAmountsControl { get; }
        //bool AutoPayVisible { set; }
        IAutoPayTile AutoPayTileControl { get; }
        bool TermsChecked { get; set; }
        bool PayNowButtonEnabled { set; }
        EventHandler PayNowClicked { set; }
        HyperLink RedirectApplicantLink { get; }
        bool IsPropertyManagerSite { get; }
        bool IsOverride { get; }
        bool ApplyPaymentToNextMonth { get; set; }
        int PaymentDay { get; set; }
        string UnmaskedCCNumber { get; set; }
        bool IsCsrUser { get;}
        bool AddToAutoPay { get;}
    }
}
