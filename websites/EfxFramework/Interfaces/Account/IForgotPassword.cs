﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces.Account
{
    public interface IForgotPassword : IBasePage
    {
        string EmailAddressText { get; }
        EventHandler SubmitButtonClick { set; }
		Page ParentPage { get; }
		PlaceHolder SuccessMessagePlaceHolder { get; }
    }
}
