﻿using System;
using System.Web.UI;

namespace EfxFramework.Interfaces.Account
{
    public interface ILogin : IBasePage
    {
        string UsernameText { get; }
        string PasswordText { get; }
        bool IsFirstVisit { get; }
        EventHandler LoginButtonClick { set; }
		Page ParentPage { get; }
    }
}
