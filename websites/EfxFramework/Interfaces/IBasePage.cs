﻿using EfxFramework.Web.WebControls;

namespace EfxFramework.Interfaces
{
    public interface IBasePage
    {
        SystemMessageControl PageMessage { get; }
        void SafeRedirect(string redirectTo);
    }
}
