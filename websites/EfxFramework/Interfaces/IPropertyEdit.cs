﻿using EfxFramework.Interfaces.UserControls;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.Interfaces
{
    public interface IPropertyEdit : IPropertyAddEdit
    {
        EventHandler SavePropertyDetailsClicked { set; }
        EventHandler SaveBankingInfoClicked { set; }
        string NameTabText { set; }
        bool IsPropertyManagerSite { get; }
		Page ParentPage { get; }
		PlaceHolder SuccessMessagePlaceHolder { get; }
    }
}
