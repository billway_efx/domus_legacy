﻿namespace EfxFramework.Interfaces
{
    public interface IBasePageV2
    {
        bool IsPostBack { get; }

        void SafeRedirect(string redirectTo);
        bool ValidatePage();
        void DisplaySystemMessage(string message, SystemMessageType messageType);
    }
}
