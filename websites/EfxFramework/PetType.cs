//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EfxFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class PetType
    {
        public PetType()
        {
            this.Pets = new HashSet<Pet>();
        }
    
        public int PetTypeId { get; set; }
        public string PetTypeName { get; set; }
    
        public virtual ICollection<Pet> Pets { get; set; }
    }
}
