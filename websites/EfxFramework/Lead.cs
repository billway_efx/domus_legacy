using EfxFramework.Web.Caching;
using Mb2x.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class Lead : BaseEntity
	{
		//Public Properties
		[DisplayName(@"LeadId"), Required(ErrorMessage = "LeadId is required.")]
		public int LeadId { get; set; }

		[DisplayName(@"FirstName"), Required(ErrorMessage = "FirstName is required."), StringLength(100)]
		public string FirstName { get; set; }

		[DisplayName(@"LastName"), Required(ErrorMessage = "LastName is required."), StringLength(100)]
		public string LastName { get; set; }

		[DisplayName(@"EmailAddress"), Required(ErrorMessage = "EmailAddress is required."), StringLength(100)]
		public string EmailAddress { get; set; }

		[DisplayName(@"Description")]
		public string Description { get; set; }

		[DisplayName(@"DateSubmitted"), Required(ErrorMessage = "DateSubmitted is required.")]
		public DateTime DateSubmitted { get; set; }

		[DisplayName(@"PhoneNumber"), StringLength(10)]
		public string PhoneNumber { get; set; }

		[DisplayName(@"PropertyName"), StringLength(200)]
		public string PropertyName { get; set; }

		[DisplayName(@"LeadTypeId"), Required(ErrorMessage = "LeadTypeId is required.")]
		public int LeadTypeId { get; set; }

        public static List<Lead> GetAllLeads()
        {
            var Leads = CacheManager.GetCacheItem<List<Lead>>(CacheKey.LeadCache);

            if (Leads == null || Leads.Count < 1)
                CacheManager.AddItemToCache(CacheKey.LeadCache, DataAccess.GetTypedList<Lead>(EfxSettings.ConnectionString, "dbo.usp_Lead_GetAll"), 60);

            return CacheManager.GetCacheItem<List<Lead>>(CacheKey.LeadCache);
        }

        public static int Set(int leadId, string firstName, string lastName, string emailAddress, string description, DateTime dateSubmitted, string phoneNumber, string propertyName, int leadTypeId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_Lead_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@LeadId", leadId),
	                    new SqlParameter("@FirstName", firstName),
	                    new SqlParameter("@LastName", lastName),
	                    new SqlParameter("@EmailAddress", emailAddress),
	                    new SqlParameter("@Description", description),
	                    new SqlParameter("@DateSubmitted", dateSubmitted),
	                    new SqlParameter("@PhoneNumber", phoneNumber),
	                    new SqlParameter("@PropertyName", propertyName),
	                    new SqlParameter("@LeadTypeId", leadTypeId)
                    });
        }

        public static int Set(Lead l)
        {
	        return Set(
		        l.LeadId,
		        l.FirstName,
		        l.LastName,
		        l.EmailAddress,
		        l.Description,
		        l.DateSubmitted,
		        l.PhoneNumber,
		        l.PropertyName,
		        l.LeadTypeId
	        );
        }

        public Lead(int? leadId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_Lead_GetSingleObject", new [] { new SqlParameter("@LeadId", leadId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        LeadId = (int)ResultSet[0];
		        FirstName = ResultSet[1] as string;
		        LastName = ResultSet[2] as string;
		        EmailAddress = ResultSet[3] as string;
		        Description = ResultSet[4] as string;
		        DateSubmitted = (DateTime)ResultSet[5];
		        PhoneNumber = ResultSet[6] as string;
		        PropertyName = ResultSet[7] as string;
		        LeadTypeId = (int)ResultSet[8];
            }
        }

		public Lead()
		{
			InitializeObject();
		}

	}
}
