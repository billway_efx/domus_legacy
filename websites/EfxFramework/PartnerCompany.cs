using System;
using System.Collections.Generic;
using Mb2x.Data;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using EfxFramework.PublicApi;
using System.Linq;

namespace EfxFramework
{
	public class PartnerCompany : BaseEntity
	{
		//Public Properties
		[DisplayName(@"PartnerCompanyId"), Required(ErrorMessage = "PartnerCompanyId is required.")]
		public int PartnerCompanyId { get; set; }

		[DisplayName(@"PartnerCompanyName"), Required(ErrorMessage = "PartnerCompanyName is required."), StringLength(100)]
		public string PartnerCompanyName { get; set; }

        public string DeletePartnerUrl { get { return String.Format("~/Handlers/GeneralHandler.ashx?RequestType=17&PartnerCompanyId={0}", PartnerCompanyId); } }

		public Boolean CanDelete
		{
			get
			{
				List<ApiUser> allApiUsers = ApiUser.GetAllApiUsers();
				if (allApiUsers.Any(a => a.PartnerCompanyId == this.PartnerCompanyId))
				{
					return false;
				}
				else
				{
					return true;
				}
			}
		}

        public static int Set(int partnerCompanyId, string partnerCompanyName)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_PartnerCompany_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@PartnerCompanyId", partnerCompanyId),
	                    new SqlParameter("@PartnerCompanyName", partnerCompanyName)
                    });
        }

        public static List<PartnerCompany> GetAllPartnerCompanies()
        {
            return DataAccess.GetTypedList<PartnerCompany>(EfxSettings.ConnectionString, "dbo.usp_PartnerCompany_GetAllPartnerCompanies");
        }

        public static void DeletePartner(int partnerCompanyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_PartnerCompany_DeleteSingleObject", new SqlParameter("@PartnerCompanyId", partnerCompanyId));
        }

        public static int Set(PartnerCompany p)
        {
	        return Set(
		        p.PartnerCompanyId,
		        p.PartnerCompanyName
	        );
        }

        public PartnerCompany(int partnerCompanyId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_PartnerCompany_GetSingleObject", new [] { new SqlParameter("@PartnerCompanyId", partnerCompanyId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        PartnerCompanyId = (int)ResultSet[0];
		        PartnerCompanyName = ResultSet[1] as string;
            }
        }

		public PartnerCompany()
		{
			InitializeObject();
		}
	}
}
