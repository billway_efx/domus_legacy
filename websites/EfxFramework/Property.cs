using System.Data.SqlTypes;
using EfxFramework.ExtensionMethods;
using EfxFramework.PaymentMethods.Ach;
using Mb2x.Data;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;

namespace EfxFramework
{
    [Serializable]
    public class Property : BaseEntity
    {
        //Public Enums
        public enum Program
        {
            [Description("Unassigned")]
            Unassigned = 0,
            [Description("Air Program")]
            AirProgram = 1,
            [Description("Spa Program")]
            SpaProgram = 2,
            //cakel: 10/19/2016
            [Description("Air-2 Legacy")]
            Air2Legacy = 4,
            [Description("Property Paid Program")]
            PropertyPaidProgram = 3            
        }

        //Public Properties
        public int? PropertyId { get; set; }
        public string PropertyCode { get; set; }
        public string PropertyName { get; set; }
        //cakel: TASK 00498
        public string PropertyNameTwo { get; set; }
        public int? NumberOfUnits { get; set; }
        public string StreetAddress { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public int? StateProvinceId { get; set; }
        public string PostalCode { get; set; }
        public string OfficeEmailAddress { get; set; }
        public PhoneNumber MainPhoneNumber { get; set; }
        public PhoneNumber AlternatePhoneNumber { get; set; }
        public PhoneNumber FaxNumber { get; set; }
        public decimal? AverageMonthlyRent { get; set; }
        public decimal? AverageConvenienceFee { get; set; }
        public string RentalDepositBankName { get; set; }
        public string RentalDepositBankAccountNumber { get; set; }
        public string RentalDepositBankRoutingNumber { get; set; }
        public string RentalAccountAchClientId { get; set; }
        public string RentalAccountFriendlyId { get; set; }
        public int? RentalAccountClearingDays { get; set; }
        public string SecurityDepositBankName { get; set; }
        public string SecurityDepositBankAccountNumber { get; set; }
        public string SecurityDepositBankRoutingNumber { get; set; }
        public string SecurityAccountAchClientId { get; set; }
        public string SecurityAccountFriendlyId { get; set; }
        public int? SecurityAccountClearingDays { get; set; }
        public string MonthlyFeeBankName { get; set; }
        public string MonthlyFeeBankAccountNumber { get; set; }
        public string MonthlyFeeBankRoutingNumber { get; set; }
        public string FeeAccountAchClientId { get; set; }
        public string FeeAccountFriendlyId { get; set; }
        public int? FeeAccountClearingDays { get; set; }
        public bool? IsParticipating { get; set; }
        public bool? IsDeleted { get; set; }
        public Program? ProgramId { get; set; }
        public int? PmsTypeId { get; set; }
        public string PmsId { get; set; }
        public string YardiUserName { get; set; }
        public string YardiPassword { get; set; }
        public string YardiServerName { get; set; }
        public string YardiDatabaseName { get; set; }
        public string YardiPlatform { get; set; }
        public string YardiEndpoint { get; set; }
        public bool IsVerifiedRentReportersProperty { get; set; }
        public DateTime DateAdded { get; set; }
        public bool ShowPaymentDetail { get; set; }
        public string DisplayStateProvince { get { return StateProvinceId.HasValue ? Enum.GetName(typeof(StateProvince), StateProvinceId.Value) : "N/A"; } }
        public string TimeZone { get; set; }
        // cakel: added bugID 0007
        public string PaymentDueDay { get; set; }
        // cakel: added bugID 0007
        public bool AllowPrePayments { get; set; }
        public bool AllowPartialPayments { get; set; }
        //cakel: added for bugID 0007
        public string NumOfPrePayDays { get; set; }
        public bool AcceptACHPayments { get; set; }
        public bool AcceptCCPayments { get; set; }
        public bool PNMEnabled { get; set; }

        //Salcedo - 7/11/2014 - added support for MRI
        public bool MRIEnableFtpUpload { get; set; }
        public bool MRIEnableFtpDownload { get; set; }
        public string MRIFtpSiteAddress { get; set; }
        public string MRIFtpPort { get; set; }
        public string MRIFtpAccountUserName { get; set; }
        public string MRIFtpAccountPassword { get; set; }

        //cakel: BUGID00213 - Property -- added new items for CC deposit details
        public string CCDepositBankName { get; set;}
        public string CCDepositAccountNumber { get; set; }
        public string CCDepositRoutingNumber { get; set; }
        public string CCDepositAccountAchClientId { get; set; }
        public string CCDepositAccountFriendlyId { get; set; }
        public int? CCDepositAccountClearingDays { get; set; }
        public bool GroupSettlementPaymentFlag { get; set; }

        //cakel: BUGID00266 - Added new items for Cash deposits(PayNearMe)
        public string CashDepositBankName { get; set; }
        public string CashDepositAccountNumber { get; set; }
        public string CashDepositRoutingNumber { get; set; }
        public string CashDepositAccountAchClientId { get; set; }
        public string CashDepositAccountFriendlyId { get; set; }
        public int? CashDepositAccountClearingDays { get; set; }
        public bool GroupSettlementCashPaymentFlag { get; set; }
        //salcedo: 0000179 - add YardiUseDepositDateAPI
        public bool YardiUseDepositDateAPI { get; set; }
        //cakel: BUGID00019 - add AllowPartialPaymentsResidentPortal
        public bool AllowPartialPaymentsResidentPortal { get; set; }
        
        //cakel: BUGID00316 - AMSI integration
        public string AmsiUserName { get; set; }
        public string AmsiPassword { get; set; }
        public string AmsiDatabaseName { get; set; }
        public string AmsiClientMerchantID { get; set; }
        //cakel: 00551
        public string AmsiAchClientMerchantID { get; set; }
        public string AmsiCashClientMerchantID { get; set; }

        public string AmsiImportEndPoint { get; set; }
        public string AmsiExportEndPoint { get; set; }
        public string merchantCount { get; set; }
        // Patrick Whittingham - 9/2/15 - task #371 : add notification email address 
        public string NotificationEmailAddress { get; set; }

        //cakel: 00382 - MRI Integration
        public string MRIClientID { get; set; }
        public string MRIDatabaseName { get; set; }
        public string MRIWebServiceUserName { get; set; }
        public string MRIPartnerKey { get; set; }
        public string MRIImportEndPoint { get; set; }
        public string MRIExportEndPoint { get; set; }
        public string MRIWebServicePassword { get; set; }

        //cakel: 00351 RealPage integration
        public string RealPageUserName { get; set; }
        public string RealPagePassword { get; set; }
        public string RealPagePMCID { get; set; }
        public string RealPageImportURL { get; set; }
        public string RealPageExportURL { get; set; }
        public string RealPageInternalUser { get; set; }
        public string RealPageSiteID { get; set; }


        //CMallory: Task 00499 - Property Uses Apply Connect
        public bool UsesApplyConnect { get; set; }
        //CMallory: Task 00425 - Property Uses Apply Connect
        public bool DisableAllPayments { get; set; }
        //CMallory - Task 00554
        public bool DisablePayByText { get; set; }
        //CMallory - Task 00166
        public bool PropertyIsPublic { get; set; }
        //CMallory - Task 00600
        public int CompanyID { get; set; }
        /// <summary>
        /// Gets the number of participating units.
        /// </summary>
        public int NumberOfParticipatingUnits
        {
            get
            {
                var UnitsCount = 0;

                var Units = GetParticipatingPropertyList();
                if (Units != null)
                    UnitsCount = Units.Count;

                return UnitsCount;
            }
        }

        /// <summary>
        /// Gets the full name of the main contact.
        /// </summary>
        public string MainContactDisplayName
        {
            get
            {
                //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
                var MainPropertyStaffId = NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "dbo.usp_Property_GetMainPropertyStaffByPropertyId", new SqlParameter("@PropertyId", PropertyId));
                var MainContact = new PropertyStaff(MainPropertyStaffId == null ? -1 : (int)MainPropertyStaffId);

                return string.Format("{0} {1}", MainContact.FirstName, MainContact.LastName);
            }
        }

        /// <summary>
        /// Gets the full street address, including city and state.
        /// </summary>
        public string DisplayAddress
        {
            get
            {
                var Prov = StateProvince.AK;
                if (StateProvinceId.HasValue)
                    Prov = (StateProvince)Enum.Parse(typeof(StateProvince), StateProvinceId.Value.ToString(CultureInfo.InvariantCulture));

                return string.Format("{0} {1} {2} {3}", StreetAddress, City, Prov, PostalCode);
            }
        }

        public string PropertyWithNameAndAddressNoStreet
        {
            get
            {
                var Prov = StateProvince.AK;
                if (StateProvinceId.HasValue)
                    Prov = (StateProvince)Enum.Parse(typeof(StateProvince), StateProvinceId.Value.ToString(CultureInfo.InvariantCulture));

                return string.Format("{0} {1}, {2} {3}", PropertyName, City, Prov, PostalCode);
            }
        }

        //Public Methods
        public static string GetIntegrationStatus(int? propertyId)
        {
            var P = new Property(propertyId);

            if (!P.PropertyId.HasValue || P.PropertyId.Value < 1)
                return String.Empty;

            if (!String.IsNullOrEmpty(P.PmsId) && P.PmsTypeId.HasValue)
                return String.Format("Integrated: {0}", ((PmsType) P.PmsTypeId).GetFriendlyName());

            return "Non-Integrated";
        }

        public static List<Property> GetPropertyListWithApplicationsBySearchString(string searchString)
        {
            return DataAccess.GetTypedList<Property>(EfxSettings.ConnectionString, "dbo.usp_Property_GetPropertiesWithApplicationForResidencyBySearchString", new SqlParameter("@SearchString", searchString));
        }

        public static List<Property> GetAllVerifiedRentReportersProperties()
        {
            return DataAccess.GetTypedList<Property>(EfxSettings.ConnectionString, "dbo.usp_Property_GetAllVerifiedRentReportersProperties");
        }

        public static List<PublicApi.Property.RentReportersProperty> GetAllRentReportersProperties()
        {
            return GetRentReportersPropertiesByEntryDates(SqlDateTime.MinValue.Value, SqlDateTime.MaxValue.Value);
        }

        public static List<PublicApi.Property.RentReportersProperty> GetRentReportersPropertiesByEntryDates(DateTime startDate, DateTime endDate)
        {
            if (startDate < SqlDateTime.MinValue.Value)
                startDate = SqlDateTime.MinValue.Value;

            if (endDate > SqlDateTime.MaxValue.Value)
                endDate = SqlDateTime.MaxValue.Value;

            var Properties = DataAccess.GetTypedList<Property>(EfxSettings.ConnectionString, "dbo.usp_Property_GetAllVerifiedRentReporterPropertiesByDateRange", new[] {new SqlParameter("@BeginningDate", startDate), new SqlParameter("@EndingDate", endDate)});
            var RentReportersProperties = new List<PublicApi.Property.RentReportersProperty>();

            foreach (var P in Properties)
            {
                var RentReportersProperty = new PublicApi.Property.RentReportersProperty();
                
                if (P.PropertyId.HasValue && P.PropertyId.Value > 0)
                {
                    RentReportersProperty.PropertyId = P.PropertyId.Value;
                    RentReportersProperty.PropertyName = P.PropertyName;
                    RentReportersProperty.PropertyManagementCompanyName = Company.GetCompanyListByPropertyId(P.PropertyId.Value)[0].CompanyName;
                    RentReportersProperties.Add(RentReportersProperty);
                }
            }

            return RentReportersProperties;
        }

        public static Property GetFirstPropertyByPropertyName(string propertyName)  
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new Property(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_Property_GetFirstPropertyByPropertyName", new SqlParameter("@PropertyName", propertyName)));
        }

        public static Property GetYardiPropertyByPmsId(string pmsId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new Property(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_Property_GetYardiPropertyByPmsId", new SqlParameter("@PmsId", pmsId)));
        }

        public static void DeleteYardiPropertyIdByPropertyId(string yardiId, int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_YardiProperty_DeleteByYardiPropertyIdByPropertyId", new[] {new SqlParameter("@YardiPropertyId", yardiId), new SqlParameter("@PropertyId", propertyId)});
        }

        //TODO: This method has no code usage -- remove?
        /// <summary>
        /// This needs to be removed
        /// </summary>
        /// <param name="yardiIds"></param>
        /// <param name="propertyId"></param>
        public static void SetYardiPropertyIdsByPropertyId(List<string> yardiIds, int propertyId)
        {
            foreach (var YardiId in yardiIds)
            {
                //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
                NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_YardiProperty_SetSingleObject", new[] {new SqlParameter("@PropertyId", propertyId), new SqlParameter("@YardiPropertyId", YardiId), new SqlParameter("@IsParentYardiPropertyId", false)});
            }
        }

        public static void SetYardiParentPropertyIdByPropertyId(string yardiId, int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_YardiProperty_SetSingleObject", new[] { new SqlParameter("@PropertyId", propertyId), new SqlParameter("@YardiPropertyId", yardiId), new SqlParameter("@IsParentYardiPropertyId", true) });
        }

        public static List<string> GetYardiPropertyIdsByPropertyId(int propertyId)
        {
            var Results = DataAccess.ExecuteDataTable(EfxSettings.ConnectionString, "dbo.usp_YardiProperty_GetYardiPropertyIdsByPropertyId", new SqlParameter("@PropertyId", propertyId));
            return (from DataRow Row in Results.Rows select Row["YardiPropertyId"] as string).ToList();
        }

        public static string GetParentYardiPropertyIdByPropertyId(int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<string>(EfxSettings.ConnectionString, "dbo.usp_YardiProperty_GetParentYardiPropertyIdByPropertyId", new SqlParameter("@PropertyId", propertyId));
        }

        public static int GetFirstPropertyForStaffByStaffId(int staffId)
        {
            var Property = GetPropertyListByPropertyStaffId(staffId).FirstOrDefault();

            if (Property != null && Property.PropertyId.HasValue)
                return Property.PropertyId.Value;

            return 0;
        }
        
        public static PropertyStaff GetMainPropertyContactByPropertyId(int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            var MainPropertyStaffId = NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "dbo.usp_Property_GetMainPropertyStaffByPropertyId", new SqlParameter("@PropertyId", propertyId));
            return new PropertyStaff(MainPropertyStaffId == null ? -1 : (int)MainPropertyStaffId);
        }
        
        public static void DeletePropertyByPropertyId(int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_Property_DeletePropertyByPropertyId", new SqlParameter("@PropertyId", propertyId));
        }

        public static int GetRentersPastDueByPropertyId(int propertyId)
        {
            return DataAccess.ExecuteDataTable(EfxSettings.ConnectionString, "dbo.usp_Property_GetRentersPastDueByPropertyId", new SqlParameter("@PropertyId", propertyId)).Rows.Count;
        }

        public static List<Renter> GetPastDueRenterListByPropertyId(int propertyId)
        {
            return DataAccess.GetTypedList<Renter>(EfxSettings.ConnectionString, "dbo.usp_Property_GetRentersPastDueByPropertyId", new SqlParameter("@PropertyId", propertyId));
        }

        public static decimal GetOutstandingRentByPropertyId(int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            var OutstandingRent = NewDataAccess.ExecuteScalar<decimal?>(EfxSettings.ConnectionString, "dbo.usp_Property_GetOutstandingRentByPropertyId", new SqlParameter("@PropertyId", propertyId));
            return OutstandingRent.HasValue ? OutstandingRent.Value : 0.00M;
        }

        public static decimal GetAverageConvenienceFeeForProperty(string programId, string averageRent)
        {
            var Program = (Program)Enum.Parse(typeof(Program), programId);
            decimal AvgRent;
            Decimal.TryParse(averageRent, out AvgRent);
            var Cf = GetConvenienceFeesByProgram(Program, AvgRent);

            return (Cf.ECheckFee + Cf.CreditCardFee + Cf.AppDepositFee) / 3;
        }

        public static Program GetProgramByProgramEntity(ProgramEntity programEntity)
        {
            return (Program)Enum.Parse(typeof(Program), programEntity.ProgramId.ToString(CultureInfo.InvariantCulture));
        }

        public static decimal GetRentCollectedByMonthByPropertyId(int month, int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<decimal>(EfxSettings.ConnectionString, "dbo.usp_Property_GetRentCollectedByMonthByPropertyId", new[] {new SqlParameter("@Month", month), new SqlParameter("@PropertyId", propertyId)});
        }

        public static int GetNumberOfLeasingAgentsByPropertyId(int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(EfxSettings.ConnectionString, "dbo.usp_Property_GetNumberOfLeasingAgentsByPropertyId", new SqlParameter("@PropertyId", propertyId));
        }

        public static ConvenienceFee GetConvenienceFeesByProperty(Property property)
        {
            return ConvenienceFee.GetConvenienceFeeForProperty(property);
        }


        public static ConvenienceFee GetConvenienceFeesByProperty(Property property, decimal avgRent)
        {
            return ConvenienceFee.GetConvenienceFeeForProperty(property, avgRent);
        }

        public static ConvenienceFee GetConvenienceFeesByProgram(Program programId, decimal avgRent)
        {
            return ConvenienceFee.GetConvenienceFeeForProperty(programId, avgRent);
        }

        public static List<Property> GetPropertyListBySearchString(string searchString)
        {
            return DataAccess.GetTypedList<Property>(EfxSettings.ConnectionString, "dbo.usp_Property_GetPropertiesBySearchString", new SqlParameter("@SearchString", searchString));
        }

        public static List<Mobile.Contact> GetPropertyMobileContactListByRenterId(int renterId)
        {

            return GetPropertyListByRenterId(renterId).Select(p => new Mobile.Contact { EmailAddress = p.OfficeEmailAddress, Name = p.MainContactDisplayName, PhoneNumber = p.MainPhoneNumber.ToString(false) }).ToList();
        }

        public static List<Mobile.Contact> GetPropertyAndPropertyStaffMobileContactListByRenterId(int renterId)
        {
            var Results = new List<Mobile.Contact>();

            foreach (var P in GetPropertyListByRenterId(renterId))
            {

                Results.Add(new Mobile.Contact { EmailAddress = P.OfficeEmailAddress, Name = P.MainContactDisplayName, PhoneNumber = P.MainPhoneNumber.ToString(false) });

                if (P.PropertyId.HasValue) 
                    Results.AddRange(PropertyStaff.GetPropertyStaffMobileContactListByPropertyId(P.PropertyId.Value));
            }

            return Results;
        }

        public static List<Property> GetPropertyListByRenterId(int renterId)
        {
            return DataAccess.GetTypedList<Property>(EfxSettings.ConnectionString, "dbo.usp_PropertyRenter_GetPropertyByRenterId", new SqlParameter("@RenterId", renterId));
        }

        public static Property GetPropertyByRenterId(int renterId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new Property(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_PropertyRenter_GetPropertyByRenterId", new SqlParameter("@RenterId", renterId)));
        }

        public static List<Property> GetPropertyListByPropertyStaffId(int propertyStaffId)
        {
            return DataAccess.GetTypedList<Property>(EfxSettings.ConnectionString, "dbo.usp_PropertyPropertyStaff_GetPropertyByPropertyStaffId", new SqlParameter("@PropertyStaffId", propertyStaffId));
        }

        //Salcedo - 7/11/2014 - new logic for Integration Manager access
        public static List<Property> GetPropertyListByPropertyStaffIdAndPmsTypeId(int propertyStaffId, int PmsTypeId)
        {
            return DataAccess.GetTypedList<Property>(EfxSettings.ConnectionString, "dbo.usp_PropertyPropertyStaff_GetPropertyByPropertyStaffIdAndPmsTypeId", new[] { new SqlParameter("@PropertyStaffId", propertyStaffId), new SqlParameter("@PmsTypeId", PmsTypeId) });
        }

        /// <summary>
        /// Assigns a staff person to a property.
        /// </summary>
        public static DataAccessResults AssignStaffToProperty(int propertyStaffId, int propertyId, bool isMainContact)
        {
            //Set the assignment
            return DataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_Property_AssignStaffToProperty", new[] { new SqlParameter("@PropertyStaffId", propertyStaffId), new SqlParameter("@PropertyId", propertyId), new SqlParameter("@IsMainContact", isMainContact) });
        }

        /// <summary>
        /// Removes an assigned staff person from a property.
        /// </summary>
        public static void RemoveStaffFromProperty(int propertyStaffId, int propertyId)
        {
            //Set the assignment
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_Property_RemoveStaffFromProperty", new[] { new SqlParameter("@PropertyStaffId", propertyStaffId), new SqlParameter("@PropertyId", propertyId) });
        }

        public static List<Property> GetAllPropertyList()
        {
            //Return a list of Properties that are participating
            return DataAccess.GetTypedList<Property>(
                EfxSettings.ConnectionString,
                "dbo.usp_Property_GetAll"
                );
        }

        //CMallory - Task 00166 - Search All Properties that have their PropertyIsPublic flag set to true.
        public static List<Property> GetAllPublicPropertyList()
        {
            //Return a list of Properties that are participating
            return DataAccess.GetTypedList<Property>(
                EfxSettings.ConnectionString,
                "dbo.usp_Property_GetAllPublicProperties"
                );
        }
        public static List<Property> GetParticipatingPropertyList()
        {
            //Return a list of Properties that are participating
            return DataAccess.GetTypedList<Property>(
                EfxSettings.ConnectionString,
                "dbo.usp_Property_GetAllParticipating"
                );
        }  
      
        public static List<Property> GetPropertyListByCompanyId(int companyId)
        {
            return DataAccess.GetTypedList<Property>(
                EfxSettings.ConnectionString,
                "dbo.usp_CompanyProperty_GetPropertyListByCompanyId",
                new SqlParameter("@CompanyId", companyId)
                );
        }

        public static List<Property> GetPropertyListByPostalCode(string postalCode)
        {
            //Return a list of Properties that are participating
            return DataAccess.GetTypedList<Property>(
                EfxSettings.ConnectionString,
                "dbo.usp_Property_GetByPostalCode",
                new SqlParameter("@PostalCode", postalCode)
                );
        }

        public static byte[] GetPhoto1(int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            
            //Salcedo - 4/3/2015 - handle empty byte array case
            //return NewDataAccess.ExecuteScalar<byte[]>(
            //    Settings.ConnectionString,
            //    "dbo.usp_Property_GetPhoto1",
            //    new SqlParameter("@PropertyId", propertyId));
            byte[] returnArray = NewDataAccess.ExecuteScalar<byte[]>(
                EfxSettings.ConnectionString,
                "dbo.usp_Property_GetPhoto1",
                new SqlParameter("@PropertyId", propertyId));
            if (returnArray.Length <= 4)
                return null;
            else
                return returnArray;
        }

        public static byte[] GetPhoto2(int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess

            //Salcedo - 4/3/2015 - handle empty byte array case
            //return NewDataAccess.ExecuteScalar<byte[]>(
            //    Settings.ConnectionString,
            //    "dbo.usp_Property_GetPhoto2",
            //    new SqlParameter("@PropertyId", propertyId));
            byte[] returnArray = NewDataAccess.ExecuteScalar<byte[]>(
                EfxSettings.ConnectionString,
                "dbo.usp_Property_GetPhoto2",
                new SqlParameter("@PropertyId", propertyId));
            if (returnArray.Length <= 4)
                return null;
            else
                return returnArray;
        }

        public static byte[] GetPhoto3(int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess

            //Salcedo - 4/3/2015 - handle empty byte array case
            //return NewDataAccess.ExecuteScalar<byte[]>(
            //    Settings.ConnectionString,
            //    "dbo.usp_Property_GetPhoto3",
            //    new SqlParameter("@PropertyId", propertyId));
            byte[] returnArray = NewDataAccess.ExecuteScalar<byte[]>(
                EfxSettings.ConnectionString,
                "dbo.usp_Property_GetPhoto3",
                new SqlParameter("@PropertyId", propertyId));
            if (returnArray.Length <= 4)
                return null;
            else
                return returnArray;
        }

        public static byte[] GetPhoto4(int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess

            //Salcedo - 4/3/2015 - handle empty byte array case
            //return NewDataAccess.ExecuteScalar<byte[]>(
            //    Settings.ConnectionString,
            //    "dbo.usp_Property_GetPhoto4",
            //    new SqlParameter("@PropertyId", propertyId));
            byte[] returnArray = NewDataAccess.ExecuteScalar<byte[]>(
                EfxSettings.ConnectionString,
                "dbo.usp_Property_GetPhoto4",
                new SqlParameter("@PropertyId", propertyId));

            if (returnArray.Length <= 4)
                return null;
            else
                return returnArray;
        }

        public static byte[] GetApplicationForResidency(int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            //Salcedo - 4/21/2015 - handle null return
            byte[] returnArray = NewDataAccess.ExecuteScalar<byte[]>(EfxSettings.ConnectionString, 
                "dbo.usp_Property_GetApplicationForResidency", new SqlParameter("@PropertyId", propertyId));

            if(returnArray.Length <= 4)
                return null;
            else
                return returnArray;
        }

        public static void SetPhoto1(int propertyId, byte[] photo)
        {
			//Resize the photo
            byte[] ResizedPhoto = null;
            
            if (photo != null)
                ResizedPhoto = photo.StandardizeImage(300);

            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(
                EfxSettings.ConnectionString,
                "dbo.usp_Property_SetPhoto1",
                new[]
					{
						new SqlParameter("@PropertyId", propertyId),
						new SqlParameter{ParameterName = "@Photo", Value = ResizedPhoto, DbType = DbType.Binary}
					});
        }

        public static void SetPhoto2(int propertyId, byte[] photo)
        {
            //Resize the photo
            byte[] ResizedPhoto = null;

            if (photo != null)
                ResizedPhoto = photo.StandardizeImage(300);

            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(
                EfxSettings.ConnectionString,
                "dbo.usp_Property_SetPhoto2",
                new[]
					{
						new SqlParameter("@PropertyId", propertyId),
						new SqlParameter{ParameterName = "@Photo", Value = ResizedPhoto, DbType = DbType.Binary}
					});
        }

        public static void SetPhoto3(int propertyId, byte[] photo)
        {
            //Resize the photo
            byte[] ResizedPhoto = null;

            if (photo != null)
                ResizedPhoto = photo.StandardizeImage(300);

            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(
                EfxSettings.ConnectionString,
                "dbo.usp_Property_SetPhoto3",
                new[]
					{
						new SqlParameter("@PropertyId", propertyId),
						new SqlParameter{ParameterName = "@Photo", Value = ResizedPhoto, DbType = DbType.Binary}
					});
        }

        public static void SetPhoto4(int propertyId, byte[] photo)
        {
            //Resize the photo
            byte[] ResizedPhoto = null;

            if (photo != null)
                ResizedPhoto = photo.StandardizeImage(300);

            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(
                EfxSettings.ConnectionString,
                "dbo.usp_Property_SetPhoto4",
                new[]
					{
						new SqlParameter("@PropertyId", propertyId),
						new SqlParameter{ParameterName = "@Photo", Value = ResizedPhoto, DbType = DbType.Binary}
					});
        }

        public static void SetApplicationForResidency(int propertyId, byte[] application)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_Property_SetApplicationForResidency", new[]
                {
                    new SqlParameter("@PropertyId", propertyId), 
                    new SqlParameter("@ApplicationForResidency", application) { SqlDbType = SqlDbType.VarBinary }
                });
        }

        //cakel: BUGID00266 - Added new values in string for cash (PayNearMe)
        public static Dictionary<string, string> AddAchAccounts(int? propertyId, string propertyName, string propertyPhone, string rentalBankFriendlyId, string rentalBankName, int? rentalBankClearingDays,
            string rentalBankRoutingNumber, string rentalBankAccountNumber, string securityBankFriendlyId, string securityBankName, int? securityBankClearingDays, string securityBankRoutingNumber, string securityBankAccountNumber, 
            string feeBankFriendlyId, string feeBankName, int? feeBankClearingDays, string feeBankRoutingNumber, string feeBankAccountNumber,
            string CCDepositBankFriendlyId, string CCDepositBankName, int? CCDepositBankClearingDays, string CCDepositBankRoutingNumber, string CCDepositBankAccountNumber,
            string CashDepositBankFriendlyId, string CashDepositBankName, int? CashDepositBankClearingDays, string CashDepositBankRoutingNumber, string CashDepositBankAccountNumber
            )
        {
            var AchAccounts = new Dictionary<string, string> { { "Rental", null }, { "Security", null }, { "Fee", null }, {"CCDeposit", null}, {"CashDeposit", null}};
            var PropertyObj = new Property(propertyId);
            AchAccountResponse RentalResponse = null;
            AchAccountResponse SecurityResponse = null;
            AchAccountResponse FeeResponse = null;
            //cakel: BUGID00213
            AchAccountResponse CCDepositResponse = null;
            //cakel: BUGID00266
            AchAccountResponse CashDepositResponse = null;

            // Patrick Whittingham - 4/15/2015 - task #000172 :
            //var merchantCount = getMerchantCount(propertyName);
            // var ResultSet = NewDataAccess.ExecuteOneRow(Settings.ConnectionString, "dbo.usp_Property_GetSingleObject_v9", new[] { new SqlParameter("@PropertyId", propertyId) });
            //var merchantCount = NewDataAccess.ExecuteOneRow(Settings.ConnectionString, "dbo.usp_get_Merchant_Count", new[] { new SqlParameter("@propertyName", propertyName) });
            String vProperty = propertyName + " sett";
            String vProperty2 = propertyName + "1 sett";
            String vProperty3 = propertyName + "2 sett";
            String vProperty4 = propertyName + "3 sett";
            String vProperty5 = propertyName + "4 sett";
            var merchantCount = NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "dbo.usp_get_Merchant_Count", new[] { 
                new SqlParameter("@propertyName", vProperty),
                new SqlParameter("@propertyName2", vProperty2),
                new SqlParameter("@propertyName3", vProperty3),
                new SqlParameter("@propertyName4", vProperty4),
                new SqlParameter("@propertyName5", vProperty5) 
            });

            //CMallory - Task 00603 - Added either true or false to the lines of code that calls the UpdateAchAccount method.
            if (PropertyObj.PropertyId > 0)
            {

                // Patrick Whittingham - 4/15/2015 - task #000172 : fix usermerchantid
                if (String.IsNullOrEmpty(PropertyObj.RentalAccountAchClientId))
                    RentalResponse = AddAchAccount(rentalBankFriendlyId, propertyName, propertyPhone, rentalBankName, rentalBankClearingDays, rentalBankRoutingNumber, rentalBankAccountNumber, merchantCount);
                else if (PropertyObj.PropertyName != propertyName || PropertyObj.RentalDepositBankAccountNumber != rentalBankAccountNumber || PropertyObj.RentalDepositBankRoutingNumber != rentalBankRoutingNumber || PropertyObj.RentalAccountClearingDays != rentalBankClearingDays)
                    RentalResponse = UpdateAchAccount(PropertyObj.RentalAccountAchClientId, propertyName, propertyPhone, rentalBankName, rentalBankClearingDays, rentalBankRoutingNumber, rentalBankAccountNumber, rentalBankFriendlyId, merchantCount, false);
                else
                    AchAccounts["Rental"] = PropertyObj.RentalAccountAchClientId;

                if (String.IsNullOrEmpty(PropertyObj.SecurityAccountAchClientId))
                    SecurityResponse = AddAchAccount(securityBankFriendlyId, propertyName, propertyPhone, securityBankName, securityBankClearingDays, securityBankRoutingNumber, securityBankAccountNumber, merchantCount);
                else if (PropertyObj.PropertyName != propertyName || PropertyObj.SecurityDepositBankAccountNumber != securityBankAccountNumber || PropertyObj.SecurityDepositBankRoutingNumber != securityBankRoutingNumber || PropertyObj.SecurityAccountClearingDays != securityBankClearingDays)
                    SecurityResponse = UpdateAchAccount(PropertyObj.SecurityAccountAchClientId, propertyName, propertyPhone, securityBankName, securityBankClearingDays, securityBankRoutingNumber, securityBankAccountNumber, securityBankFriendlyId, merchantCount, false);
                else
                    AchAccounts["Security"] = PropertyObj.SecurityAccountAchClientId;

                if (String.IsNullOrEmpty(PropertyObj.FeeAccountAchClientId))
                    FeeResponse = AddAchAccount(feeBankFriendlyId, propertyName, propertyPhone, feeBankName, feeBankClearingDays, feeBankRoutingNumber, feeBankAccountNumber, merchantCount);
                else if (PropertyObj.PropertyName != propertyName || PropertyObj.MonthlyFeeBankAccountNumber != feeBankAccountNumber || PropertyObj.MonthlyFeeBankRoutingNumber != feeBankRoutingNumber || PropertyObj.FeeAccountClearingDays != feeBankClearingDays)
                    FeeResponse = UpdateAchAccount(PropertyObj.FeeAccountAchClientId, propertyName, propertyPhone, feeBankName, feeBankClearingDays, feeBankRoutingNumber, feeBankAccountNumber, feeBankFriendlyId, merchantCount, false);
                else
                    AchAccounts["Fee"] = PropertyObj.FeeAccountAchClientId;
                
                //cakel: BUGID00213
                if (String.IsNullOrEmpty(PropertyObj.CCDepositAccountAchClientId))
                    CCDepositResponse = AddAchAccount(CCDepositBankFriendlyId, propertyName, propertyPhone, CCDepositBankName, CCDepositBankClearingDays, CCDepositBankRoutingNumber, CCDepositBankAccountNumber, merchantCount);
                else if (PropertyObj.PropertyName != propertyName || PropertyObj.CCDepositAccountNumber != CCDepositBankAccountNumber || PropertyObj.CCDepositRoutingNumber != CCDepositBankRoutingNumber || PropertyObj.CCDepositAccountClearingDays != CCDepositBankClearingDays)
                    CCDepositResponse = UpdateAchAccount(PropertyObj.CCDepositAccountAchClientId, propertyName, propertyPhone, CCDepositBankName, CCDepositBankClearingDays, CCDepositBankRoutingNumber, CCDepositBankAccountNumber, CCDepositBankFriendlyId, merchantCount, true);
                else
                    AchAccounts["CCDeposit"] = PropertyObj.CCDepositAccountAchClientId;

                //cakel: BUGID00266 
                // Patrick Whittingham - 4/15/15 - task# 000172: remove cashdeposit per Mark.
                //if (String.IsNullOrEmpty(PropertyObj.CashDepositAccountAchClientId))
                    //CashDepositResponse = AddAchAccount(CashDepositBankFriendlyId, propertyName, propertyPhone, CashDepositBankName, CashDepositBankClearingDays, CashDepositBankRoutingNumber, CashDepositBankAccountNumber);
               // else if (PropertyObj.PropertyName != propertyName || PropertyObj.CashDepositAccountNumber != CashDepositBankAccountNumber || PropertyObj.CashDepositRoutingNumber != CashDepositBankRoutingNumber || PropertyObj.CashDepositAccountClearingDays != CashDepositBankClearingDays)
                    //CashDepositResponse = UpdateAchAccount(PropertyObj.CashDepositAccountAchClientId, propertyName, propertyPhone, CashDepositBankName, CashDepositBankClearingDays, CashDepositBankRoutingNumber, CashDepositBankAccountNumber, CashDepositBankFriendlyId);
               // else
                   // AchAccounts["CashDeposit"] = PropertyObj.CashDepositAccountAchClientId;


            }
            else
            {
                RentalResponse = AddAchAccount(rentalBankFriendlyId, propertyName, propertyPhone, rentalBankName, rentalBankClearingDays, rentalBankRoutingNumber, rentalBankAccountNumber, merchantCount);
                SecurityResponse = AddAchAccount(securityBankFriendlyId, propertyName, propertyPhone, securityBankName, securityBankClearingDays, securityBankRoutingNumber, securityBankAccountNumber, merchantCount);
                FeeResponse = AddAchAccount(feeBankFriendlyId, propertyName, propertyPhone, feeBankName, feeBankClearingDays, feeBankRoutingNumber, feeBankAccountNumber, merchantCount);
                //cakel: BUGID00213
                CCDepositResponse = AddAchAccount(CCDepositBankFriendlyId, propertyName, propertyPhone, CCDepositBankName, CCDepositBankClearingDays, CCDepositBankRoutingNumber, CCDepositBankAccountNumber, merchantCount);
                //cakel: BUGID00266
                // Patrick Whittingham - 4/15/15 - task# 000172: remove cashdeposit per Mark.
                //CashDepositResponse = AddAchAccount(CashDepositBankFriendlyId, propertyName, propertyPhone, CashDepositBankName, CashDepositBankClearingDays, CashDepositBankRoutingNumber, CashDepositBankAccountNumber);
            }

            if (RentalResponse != null && RentalResponse.Result == PaymentMethods.GeneralResponseResult.Success)
                AchAccounts["Rental"] = RentalResponse.AchClientId;

            if (SecurityResponse != null && SecurityResponse.Result == PaymentMethods.GeneralResponseResult.Success)
                AchAccounts["Security"] = SecurityResponse.AchClientId;

            if (FeeResponse != null && FeeResponse.Result == PaymentMethods.GeneralResponseResult.Success)
                AchAccounts["Fee"] = FeeResponse.AchClientId;

            //cakel: BUGID00213
            if (CCDepositResponse != null && CCDepositResponse.Result == PaymentMethods.GeneralResponseResult.Success)
                AchAccounts["CCDeposit"] = CCDepositResponse.AchClientId;

            //cakel: BUGID00266
            // Patrick Whittingham - 4/15/15 - task# 000172: remove cashdeposit per Mark.
            //if (CashDepositResponse != null && CashDepositResponse.Result == PaymentMethods.GeneralResponseResult.Success)
                //AchAccounts["CashDeposit"] = CashDepositResponse.AchClientId;

           return AchAccounts;
        }
        
        ////CMallory - Task 00603 - Added CCDepositResponse parameter.
        public static AchAccountResponse UpdateAchAccount(string achClientId, string propertyName, string propertyPhone, string propertyBankName, int? clearingDays, string routingNumber, string accountNumber, string usermerchantid, object merchantCount, bool CCDepositResponse)
        {
            return AchService.UpdateAchAccount(achClientId, propertyName, propertyPhone, propertyBankName, clearingDays, routingNumber, accountNumber, usermerchantid, CCDepositResponse);
        }

        public static AchAccountResponse AddAchAccount(string friendlyId, string propertyName, string propertyPhone, string propertyBankName, int? clearingDays, string routingNumber, string accountNumber, object merchantCount)
        {
            return AchService.CreateAchAccount(friendlyId, propertyName, propertyPhone, propertyBankName, clearingDays, routingNumber, accountNumber);
        }


        //cakelUpdated: Added new variables BugID 0007
        //cakel: BUGID000213 - added variables to SET
        //cakel: BUGID00019 - added variable to SET (allowPartialPaymentsresidentPortal)
        //Salcedo: 0000179 - added YardiUseDepositDateAPI
        //cakel: BUGID00266 - Added new variables for Cash Deposit objects
        public static int Set(int? propertyId, string propertyCode, string propertyName, string propertyName2, int? numberOfUnits, string streetAddress, string streetAddress2, string city, 
            int? stateProvinceId, string postalCode, string officeEmailAddress, PhoneNumber mainPhoneNumber, PhoneNumber alternatePhoneNumber, PhoneNumber faxNumber, 
            decimal? averageMonthlyRent, decimal? averageConvenienceFee, string rentalDepositBankName, string rentalDepositBankAccountNumber, string rentalDepositBankRoutingNumber, 
            int? rentalAccountClearingDays, string securityDepositBankName, string securityDepositBankAccountNumber, string securityDepositBankRoutingNumber, 
            int? securityAccountClearingDays, string monthlyFeeBankName, string monthlyFeeBankAccountNumber, string monthlyFeeBankRoutingNumber, int? feeAccountClearingDays, 
            bool? isParticipating, bool? isDeleted, Program? programId, int? pmsTypeId, string pmsId, string yardiUsername, string yardiPassword, string yardiServerName, 
            string yardiDatabaseName, string yardiPlatform, string yardiEndpoint, bool isVerifiedRentReportersProperty, bool showPaymentDetail, string timeZone, 
            string paymentDueDay, bool allowPrepayments, bool allowPartialPayments, string NumOfPrePayDays, bool acceptACHPayments, bool acceptCCPayments, bool pnmEnabled,
            bool MRIEnableFtpUpload, bool MRIEnableFtpDownload, string MRIFtpSiteAddress, string MRIFtpPort, string MRIFtpAccountUserName, string MRIFtpAccountPassword,
            string CCDepositBankName, string CCDepositBankAccountNumber, string CCDepositBankRoutingNumber, int? CCDepositClearingDays, bool GroupSettlementPaymentFlag, bool YardiUseDepositDateAPI,
            bool allowPartialPaymentsresidentPortal,
            string CashDepositBankName, string CashDepositBankAccountNumber, string CashDepositBankRoutingNumber, int? CashDepositClearingDays,
            //cakel: BUGID00316
            string AmsiUserName, string AmsiPassword, string AmsiDatabaseName, string ClientMerchantID, 
            //cakel: 00551
            string AchClientMerchantID, string CashClientMerchantID,
            string AmsiImportEndPoint, string AmsiExportEndPoint,
            // Patrick Whittingham - 4/16/15 - task# 000172:
            object merchantCount, string RentalAccountFriendlyId, string SecurityAccountFriendlyId, string FeeAccountFriendlyId, string CCDepositFriendlyid, string CashDepositFriendlyid,
            //cakel: 00382 - MRI
            string MRI_ClientID, string MRI_DatabaseName, string MRI_WebServiceUserName, string MRI_PartnerKey, string MRI_ImportEndPoint, string MRI_ExportEndPoint, string MRI_WebServicePassword,
            
            // Patrick Whittingham - 9/2/15 - task #371 : add notification email address 
            
            string NotificationEmailAddress,
            //cakel: 00351 RealPage
            string RealPagePMCID, string RealPageUserName, string RealPagePassword, string RealPageImportUrl, string RealPageExportUrl, string RealPageInternalUser, string RealPageSiteID,
            //CMallory - Task 00425
            bool DisableAllPayments,
            //CMallory - Task 00554
            bool DisablePayByText,
            //CMallory - Task 00166 - Added
            bool PropertyIsPublic,
            //CMallory - Task 00600
            int CompanyID
            )
        {
            // Patrick Whittingham - 4/16/15 - task# 000172:
            //merchantCount = NewDataAccess.ExecuteOneRow(Settings.ConnectionString, "dbo.usp_get_Merchant_Count", new[] { new SqlParameter("@propertyName", propertyName) });
            String vProperty = propertyName + " sett";
            String vProperty2 = propertyName + "1 sett";
            String vProperty3 = propertyName + "2 sett";
            String vProperty4 = propertyName + "3 sett";
            String vProperty5 = propertyName + "4 sett";

            var vCount = NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "dbo.usp_get_Merchant_Count", new[] { 
                new SqlParameter("@propertyName", vProperty),
                new SqlParameter("@propertyName2", vProperty2),
                new SqlParameter("@propertyName3", vProperty3),
                new SqlParameter("@propertyName4", vProperty4),
                new SqlParameter("@propertyName5", vProperty5) 
            });
            String vSuffix = "";
            if (propertyId == null )
            {
                // Patrick Whittingham - 4/16/15 - task# 000172:
                if (vCount.ToString() != "0")
                {
                vSuffix = vCount.ToString();
                }
                RentalAccountFriendlyId = String.Format("{0}"+ vSuffix + " sett", propertyName);
                SecurityAccountFriendlyId = String.Format("{0}" + vSuffix + " sec", propertyName);
                FeeAccountFriendlyId = String.Format("{0}" + vSuffix + " app", propertyName);
                CCDepositFriendlyid = String.Format("{0}" + vSuffix + " cred", propertyName);
                CashDepositFriendlyid = String.Format("{0}" + vSuffix + " cash", propertyName);
            } else if (RentalAccountFriendlyId != null)
            {
            }
            else
            {
                SecurityAccountFriendlyId = String.Format("{0}" + vSuffix + " sec", propertyName);
                FeeAccountFriendlyId = String.Format("{0}" + vSuffix + " app", propertyName);
                CCDepositFriendlyid = String.Format("{0}" + vSuffix + " cred", propertyName);
                CashDepositFriendlyid = String.Format("{0}" + vSuffix + " cash", propertyName);
            }

            var DateAdded = DateTime.UtcNow;
            //cakel:BUGID00213  added variables to function AddAchAccounts
            //cakel: BUGID00266 added variable to function for cash(PayNearMe)
          
            var AchAccounts = AddAchAccounts(propertyId, propertyName, mainPhoneNumber.ToString(false), RentalAccountFriendlyId, rentalDepositBankName, rentalAccountClearingDays,
                rentalDepositBankRoutingNumber, rentalDepositBankAccountNumber, SecurityAccountFriendlyId, securityDepositBankName, securityAccountClearingDays, securityDepositBankRoutingNumber,
                securityDepositBankAccountNumber, FeeAccountFriendlyId, monthlyFeeBankName, feeAccountClearingDays, monthlyFeeBankRoutingNumber, monthlyFeeBankAccountNumber,
               CCDepositFriendlyid,CCDepositBankName,CCDepositClearingDays,CCDepositBankRoutingNumber,CCDepositBankAccountNumber,
               CashDepositFriendlyid, CashDepositBankName, CashDepositClearingDays, CashDepositBankRoutingNumber, CashDepositBankAccountNumber
                );

            if (propertyId.HasValue && propertyId.Value > 0)
            {
                var Property = new Property(propertyId.Value);
                if (Property.PropertyId.HasValue && Property.PropertyId.Value > 0)
                    DateAdded = Property.DateAdded;
            }

            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
                EfxSettings.ConnectionString,
                //cakel BUGID00007 - Updated to new version of SP
                //Salcedo - 7/11/2014 - added support for MRI integration

                //cakel: BUGID00213 - Updated SP to usp_Property_SetSingleObject_v4 from usp_Property_SetSingleObject_v3
                //cakel: BUGID00019 - Updated SP to usp_Property_SetSingleObject_v6 from usp_Property_SetSingleObject_v5
                //Salcedo - 0000179 - added YardiUseDepositDateAPI
                //cakel: BUGID00266 - Updated SP to usp_Property_SetSingleObject_v7 from usp_Property_SetSingleObject_v6
                //cakel: BUGID00316
                //cakel: TASK 00382 MRI - Updated SP
                //cakel: 00498 added v11
                //cakel: 00351 RealPage added v13
                //CMallory - Task 00166 - Changed stored procedure from V13 to V14
                 "dbo.usp_Property_SetSingleObject_v15",
                //"dbo.usp_Property_SetSingleObject_v14",
            new[]
                {
	                new SqlParameter("@PropertyId", propertyId),
	                new SqlParameter("@PropertyCode", propertyCode),
	                new SqlParameter("@PropertyName", propertyName),
                    new SqlParameter("@PropertyName2", propertyName2),
	                new SqlParameter("@NumberOfUnits", numberOfUnits),
	                new SqlParameter("@StreetAddress", streetAddress),
	                new SqlParameter("@StreetAddress2", streetAddress2),
	                new SqlParameter("@City", city),
	                new SqlParameter("@StateProvinceId", stateProvinceId),
	                new SqlParameter("@PostalCode", postalCode),
	                new SqlParameter("@OfficeEmailAddress", officeEmailAddress),
	                new SqlParameter("@MainPhoneNumber", mainPhoneNumber != null ? mainPhoneNumber.ToString(false) : null),
	                new SqlParameter("@AlternatePhoneNumber", alternatePhoneNumber != null ? alternatePhoneNumber.ToString(false) : null),
	                new SqlParameter("@FaxNumber", faxNumber != null ? faxNumber.ToString(false) : null),
	                new SqlParameter("@AverageMonthlyRent", averageMonthlyRent),
	                new SqlParameter("@AverageConvenienceFee", averageConvenienceFee),
	                new SqlParameter("@RentalDepositBankName", rentalDepositBankName),
	                new SqlParameter("@RentalDepositBankAccountNumber", rentalDepositBankAccountNumber),
	                new SqlParameter("@RentalDepositBankRoutingNumber", rentalDepositBankRoutingNumber),
                    new SqlParameter("@RentalAccountAchClientId", AchAccounts["Rental"]),
                    new SqlParameter("@RentalAccountFriendlyId", RentalAccountFriendlyId),
                    new SqlParameter("@RentalAccountClearingDays", rentalAccountClearingDays != null && rentalAccountClearingDays > 0 ? rentalAccountClearingDays : 1),
	                new SqlParameter("@SecurityDepositBankName", securityDepositBankName),
	                new SqlParameter("@SecurityDepositBankAccountNumber", securityDepositBankAccountNumber),
	                new SqlParameter("@SecurityDepositBankRoutingNumber", securityDepositBankRoutingNumber),
                    new SqlParameter("@SecurityAccountAchClientId", AchAccounts["Security"]),
                    new SqlParameter("@SecurityAccountFriendlyId", SecurityAccountFriendlyId),
                    new SqlParameter("@SecurityAccountClearingDays", securityAccountClearingDays != null && securityAccountClearingDays > 0 ? securityAccountClearingDays : 1),
	                new SqlParameter("@MonthlyFeeBankName", monthlyFeeBankName),
	                new SqlParameter("@MonthlyFeeBankAccountNumber", monthlyFeeBankAccountNumber),
	                new SqlParameter("@MonthlyFeeBankRoutingNumber", monthlyFeeBankRoutingNumber),
                    new SqlParameter("@FeeAccountAchClientId", AchAccounts["Fee"]),
                    new SqlParameter("@FeeAccountFriendlyId", FeeAccountFriendlyId),
                    new SqlParameter("@FeeAccountClearingDays", feeAccountClearingDays != null && feeAccountClearingDays > 0 ? feeAccountClearingDays : 1),
	                new SqlParameter("@IsParticipating", isParticipating),
	                new SqlParameter("@IsDeleted", isDeleted),
                    new SqlParameter("@ProgramId", programId),
                    new SqlParameter("@PmsTypeId", pmsTypeId),
                    new SqlParameter("@PmsId", pmsId),
                    new SqlParameter("@YardiUsername", yardiUsername),
                    new SqlParameter("@YardiPassword", yardiPassword),
                    new SqlParameter("@YardiServerName", yardiServerName),
                    new SqlParameter("@YardiDatabaseName", yardiDatabaseName),
                    new SqlParameter("@YardiPlatform", yardiPlatform),
                    new SqlParameter("@YardiEndpoint", yardiEndpoint),
                    new SqlParameter("@IsVerifiedRentReportersProperty", isVerifiedRentReportersProperty),
                    new SqlParameter("@DateAdded", DateAdded),
                    new SqlParameter("@ShowPaymentDetail", showPaymentDetail),
                    new SqlParameter("@TimeZone", timeZone),
                    //cakel: Added BugID 00007
                    new SqlParameter("@PaymentDueDay", paymentDueDay),
                    //cakel: added BugID 00007
                    new SqlParameter("@AllowPrePayments", allowPrepayments),
                    new SqlParameter("@AllowPartialPayments", allowPartialPayments),
                    //cakel: Added BugID 00007
                   new SqlParameter("@NumOfPrePayDays", NumOfPrePayDays),
					new SqlParameter("@AcceptACHPayments", acceptACHPayments),
					new SqlParameter("@AcceptCCPayments", acceptCCPayments),
					new SqlParameter("@PNMEnabled", pnmEnabled),
                    //Salcedo - 7/11/2014 - added support for MRI integration
                    new SqlParameter("@MRIEnableFtpUpload", MRIEnableFtpUpload),
                    new SqlParameter("@MRIEnableFtpDownload", MRIEnableFtpDownload),
                    new SqlParameter("@MRIFtpSiteAddress", MRIFtpSiteAddress),
                    new SqlParameter("@MRIFtpPort", MRIFtpPort),
                    new SqlParameter("@MRIFtpAccountUserName", MRIFtpAccountUserName),
                    new SqlParameter("@MRIFtpAccountPassword", MRIFtpAccountPassword),
                    new SqlParameter("@CCDepositBankName", CCDepositBankName),
                    //cakel: BUGID00213 - added parameter for CC Deposit Account
                    new SqlParameter("@CCDepositBankAccountNumber", CCDepositBankAccountNumber),
                    new SqlParameter("@CCDepositBankRoutingNumber",CCDepositBankRoutingNumber),
                    new SqlParameter("@CCDepositAccountAchClientID",AchAccounts["CCDeposit"]),
                    new SqlParameter("@CCDepositAccountFriendlyID",CCDepositFriendlyid),
                    new SqlParameter("@CCDepositAccountClearingDays",CCDepositClearingDays != null && CCDepositClearingDays > 0 ? CCDepositClearingDays : 1),
                    new SqlParameter("@GroupSettlementPaymentFlag", GroupSettlementPaymentFlag),
                    //Salcedo: 0000179
                    new SqlParameter("@YardiUseDepositDateAPI", YardiUseDepositDateAPI),
                    //cakel: BUGID00019
                    new SqlParameter("@AllowPartialPaymentsResidentPortal",allowPartialPaymentsresidentPortal),

                    //cakel: BUGID00266 - We are using the rental account for Cash
                    new SqlParameter("@CashDepositBankName", rentalDepositBankName),
                    new SqlParameter("@CashDepositBankAccountNumber", rentalDepositBankAccountNumber),
                    new SqlParameter("@CashDepositBankRoutingNumber",rentalDepositBankRoutingNumber),
                    new SqlParameter("@CashDepositAccountAchClientID",AchAccounts["CashDeposit"]),
                    new SqlParameter("@CashDepositAccountFriendlyID",CashDepositFriendlyid),
                    new SqlParameter("@CashDepositAccountClearingDays",CashDepositClearingDays != null && CashDepositClearingDays > 0 ? CashDepositClearingDays : 1),
                    //cakel: BUGID00316 - AMSI Integration
                    new SqlParameter("@AmsiUserID", AmsiUserName),
                    new SqlParameter("@AmsiPassword", AmsiPassword),
                    new SqlParameter("@AmsiDatabaseName", AmsiDatabaseName),
                    new SqlParameter("@AmsiClientMerchantID", ClientMerchantID),
                    new SqlParameter("@AmsiImportEndPoint", AmsiImportEndPoint),
                    new SqlParameter("@AmsiExportEndPoint", AmsiExportEndPoint),
                    
                    //cakel: 00382 MRI -Need to Add Parameters
                    new SqlParameter("@MRIClientID",MRI_ClientID),
                    new SqlParameter("@MRIDatabaseName",MRI_DatabaseName),
                    new SqlParameter("@MRIWebServiceUserName",MRI_WebServiceUserName),
                    new SqlParameter("@MRIPartnerKey",MRI_PartnerKey),
                    new SqlParameter("@MRIImportEndPoint", MRI_ImportEndPoint),
                    new SqlParameter("@MRIExportEndPoint",MRI_ExportEndPoint),
                    new SqlParameter("@MriWebServicePassword",MRI_WebServicePassword),

                    // Patrick Whittingham - 9/2/15 - task #371 : add notification email address 
  	                new SqlParameter("@NotificationEmailAddress", NotificationEmailAddress),    
                    //cakel: 00351 RealPage
                    new SqlParameter("@RealPagePmcID", RealPagePMCID),
                    new SqlParameter("@RealPageUserName",RealPageUserName),
                    new SqlParameter("@RealPagePassword",RealPagePassword),
                    new SqlParameter("@RealPageImportURL",RealPageImportUrl),
                    new SqlParameter("@RealPageExportUrl",RealPageExportUrl),
                    new SqlParameter("@RealPageInternalUser", RealPageInternalUser),
                    new SqlParameter("@RealPageSiteID",RealPageSiteID),
                    //cakel:00551
                    new SqlParameter("@AchClientMerchantID", AchClientMerchantID),
                    new SqlParameter("@CashClientMerchantID", CashClientMerchantID),
                    //CMallory - Task 00425
                    new SqlParameter("@DisableAllPayments", DisableAllPayments),
                    //CMallory - Task 00554
                    new SqlParameter("@DisablePayByText", DisablePayByText),
                    //CMallory - Task 00166 - Added
                    new SqlParameter("@PropertyIsPublic", PropertyIsPublic),
                    //CMallory - Task 0600
                     new SqlParameter("@CompanyID", CompanyID),
                });
        }

        public static int Set(Property p)
        {
            return Set(
                p.PropertyId,
                p.PropertyCode,
                p.PropertyName,
                p.PropertyNameTwo,
                p.NumberOfUnits,
                p.StreetAddress,
                p.StreetAddress2,
                p.City,
                p.StateProvinceId,
                p.PostalCode,
                p.OfficeEmailAddress,
                p.MainPhoneNumber,
                p.AlternatePhoneNumber,
                p.FaxNumber,
                p.AverageMonthlyRent,
                p.AverageConvenienceFee,
                p.RentalDepositBankName,
                p.RentalDepositBankAccountNumber,
                p.RentalDepositBankRoutingNumber,
                p.RentalAccountClearingDays,
                p.SecurityDepositBankName,
                p.SecurityDepositBankAccountNumber,
                p.SecurityDepositBankRoutingNumber,
                p.SecurityAccountClearingDays,
                p.MonthlyFeeBankName,
                p.MonthlyFeeBankAccountNumber,
                p.MonthlyFeeBankRoutingNumber,
                p.FeeAccountClearingDays,
                p.IsParticipating,
                p.IsDeleted,
                p.ProgramId,
                p.PmsTypeId,
                p.PmsId,
                p.YardiUserName,
                p.YardiPassword,
                p.YardiServerName,
                p.YardiDatabaseName,
                p.YardiPlatform,
                p.YardiEndpoint,
                p.IsVerifiedRentReportersProperty,
                p.ShowPaymentDetail,
                p.TimeZone,
                //cakel: added BugID 0007
                p.PaymentDueDay,
                //cakel: added BugID 0007
                p.AllowPrePayments,
                p.AllowPartialPayments,
                //cakel: added BugID 0007
                p.NumOfPrePayDays,
                p.AcceptACHPayments,
                p.AcceptCCPayments,
                p.PNMEnabled,
                //Salcdo - 7/11/2014 - added support for MRI integration
                p.MRIEnableFtpUpload,
                p.MRIEnableFtpDownload,
                p.MRIFtpSiteAddress,
                p.MRIFtpPort,
                p.MRIFtpAccountUserName,
                p.MRIFtpAccountPassword,
                //cakel: BUGID00213
                p.CCDepositBankName,
                p.CCDepositAccountNumber,
                p.CCDepositRoutingNumber,
                p.CCDepositAccountClearingDays,
                p.GroupSettlementPaymentFlag,
                //Salcedo: 0000179
                p.YardiUseDepositDateAPI,
                //cakel: BUGID00019
                p.AllowPartialPaymentsResidentPortal,
                //cakel: BUGID00266
                p.CashDepositBankName,
                p.CashDepositAccountNumber,
                p.CashDepositRoutingNumber,
                p.CashDepositAccountClearingDays,
                //cakel: BUGID00316
                p.AmsiUserName,
                p.AmsiPassword,
                p.AmsiDatabaseName,
                p.AmsiClientMerchantID,
                //cakel: 00551
                p.AmsiAchClientMerchantID,
                p.AmsiCashClientMerchantID,
                p.AmsiImportEndPoint,
                p.AmsiExportEndPoint,
                //Patrick Whittingham - 4/15/15 - task# 000172
                p.merchantCount,
                p.RentalAccountFriendlyId,
                p.SecurityAccountFriendlyId,
                p.FeeAccountFriendlyId,
                p.CCDepositAccountFriendlyId,
                p.CashDepositAccountFriendlyId,
                //cakel: 00382 MRI
                p.MRIClientID,
                p.MRIDatabaseName,
                p.MRIWebServiceUserName,
                p.MRIPartnerKey,
                p.MRIImportEndPoint,
                p.MRIExportEndPoint,
                p.MRIWebServicePassword,

                //Patrick Whittingham - 9/2/15 - task #371 : add notification email address 
                p.NotificationEmailAddress,
                //cakel: 00351
                p.RealPagePMCID,
                p.RealPageUserName,
                p.RealPagePassword,
                p.RealPageImportURL,
                p.RealPageExportURL,
                p.RealPageInternalUser,
                p.RealPageSiteID,
                //CMAllory - Task 00425
                p.DisableAllPayments,
                //CMallory - Task 00554
                p.DisablePayByText,
                //CMallory - Task 00166 - Added
                p.PropertyIsPublic,
                //CMallory - Task 00600
                p.CompanyID
                
                
                
            );
        }

        public Property(int? propertyId)
        {
            //Attempt to pull the data from the database
            //cakel: BUGID00213 updated SP to return new CC deposit values
            //var ResultSet = DataAccess.ExecuteOneRow(Settings.ConnectionString, "dbo.usp_Property_GetSingleObject_v3", new[] { new SqlParameter("@PropertyId", propertyId) });
            //cakel: BUGID00019 - Updated SP to v6 from v5 to return value from AllowPartialPaymentsResidentPortal
            //var ResultSet = DataAccess.ExecuteOneRow(Settings.ConnectionString, "dbo.usp_Property_GetSingleObject_v5", new[] { new SqlParameter("@PropertyId", propertyId) });
            //var ResultSet = DataAccess.ExecuteOneRow(Settings.ConnectionString, "dbo.usp_Property_GetSingleObject_v6", new[] { new SqlParameter("@PropertyId", propertyId) });

            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            //cakel: BUGID00316 - Updated SP for AMSI integration
            //cakel: TASK 00382 Updated SP for MRI Integration

            //var ResultSet = NewDataAccess.ExecuteOneRow(Settings.ConnectionString, "dbo.usp_Property_GetSingleObject_v10", new[] { new SqlParameter("@PropertyId", propertyId) });
            //cakel: TASK 00513 09/23/2015
            //cakel: 00498 added  v11
            //cakel: 00351 RealPage added v13
            //CMallory - Task 00166 - Changed Stored Procedure from version 13 to 14
            // var ResultSet = NewDataAccess.ExecuteProcedure(Settings.ConnectionString, "dbo.usp_Property_GetSingleObject_v14", 
            var ResultSet = NewDataAccess.ExecuteProcedure(EfxSettings.ConnectionString, "dbo.usp_Property_GetSingleObject_v14", 
                new[] 
                { 
                    new SqlParameter("@PropertyId", propertyId) 
                }
                );
            ResultSet.Read();


            if (!ResultSet.HasRows == true)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
                ResultSet.Close();
            }
            else
            {
                //Otherwise, populate this object
                //Switherspoon: Added for PropertyDetails page
                CompanyID = int.Parse(ResultSet["CompanyId"].ToString());
                PropertyId = ResultSet["PropertyId"] as int?;
                PropertyCode = ResultSet["PropertyCode"] as string;
                PropertyName = ResultSet["PropertyName"] as string;
                //cakel: 00498
                PropertyNameTwo = ResultSet["PropertyName2"] as string;
                NumberOfUnits = ResultSet["NumberOfUnits"] as int?;
                StreetAddress = ResultSet["StreetAddress"] as string;
                StreetAddress2 = ResultSet["StreetAddress2"] as string;
                City = ResultSet["City"] as string;
                StateProvinceId = ResultSet["StateProvinceId"] as int?;
                PostalCode = ResultSet["PostalCode"] as string;
                OfficeEmailAddress = ResultSet["OfficeEmailAddress"] as string;
                MainPhoneNumber = new PhoneNumber(ResultSet["MainPhoneNumber"] as string);
                AlternatePhoneNumber = new PhoneNumber(ResultSet["AlternatePhoneNumber"] as string);
                FaxNumber = new PhoneNumber(ResultSet["FaxNumber"] as string);
                AverageMonthlyRent = ResultSet["AverageMonthlyRent"] as decimal?;
                AverageConvenienceFee = ResultSet["AverageConvenienceFee"] as decimal?;
                RentalDepositBankName = ResultSet["RentalDepositBankName"] as string;
                RentalDepositBankAccountNumber = ResultSet["RentalDepositBankAccountNumber"] as string;
                RentalDepositBankRoutingNumber = ResultSet["RentalDepositBankRoutingNumber"] as string;
                RentalAccountAchClientId = ResultSet["RentalAccountAchClientId"] as string;
                RentalAccountFriendlyId = ResultSet["RentalAccountFriendlyId"] as string;
                RentalAccountClearingDays = ResultSet["RentalAccountClearingDays"] as int?;
                SecurityDepositBankName = ResultSet["SecurityDepositBankName"] as string;
                SecurityDepositBankAccountNumber = ResultSet["SecurityDepositBankAccountNumber"] as string;
                SecurityDepositBankRoutingNumber = ResultSet["SecurityDepositBankRoutingNumber"] as string;
                SecurityAccountAchClientId = ResultSet["SecurityAccountAchClientId"] as string;
                SecurityAccountFriendlyId = ResultSet["SecurityAccountFriendlyId"] as string;
                SecurityAccountClearingDays = ResultSet["SecurityAccountClearingDays"] as int?;
                MonthlyFeeBankName = ResultSet["MonthlyFeeBankName"] as string;
                MonthlyFeeBankAccountNumber = ResultSet["MonthlyFeeBankAccountNumber"] as string;
                MonthlyFeeBankRoutingNumber = ResultSet["MonthlyFeeBankRoutingNumber"] as string;
                FeeAccountAchClientId = ResultSet["FeeAccountAchClientId"] as string;
                FeeAccountFriendlyId = ResultSet["FeeAccountFriendlyId"] as string;
                FeeAccountClearingDays = ResultSet["FeeAccountClearingDays"] as int?;
                IsParticipating = ResultSet["IsParticipating"] as bool?;
                IsDeleted = ResultSet["IsDeleted"] as bool?;
                if (ResultSet["ProgramId"] != DBNull.Value)
                    ProgramId = (Program)ResultSet["ProgramId"];
                PmsTypeId = ResultSet["PmsTypeId"] as int?;
                PmsId = ResultSet["PmsId"] as string;
                YardiUserName = ResultSet["YardiUsername"] as string;
                YardiPassword = ResultSet["YardiPassword"] as string;
                YardiServerName = ResultSet["YardiServerName"] as string;
                YardiDatabaseName = ResultSet["YardiDatabaseName"] as string;
                YardiPlatform = ResultSet["YardiPlatform"] as string;
                YardiEndpoint = ResultSet["YardiEndpoint"] as string;
                IsVerifiedRentReportersProperty = (bool)ResultSet["IsVerifiedRentReportersProperty"];
                DateAdded = (DateTime)ResultSet["DateAdded"];
                ShowPaymentDetail = (bool)ResultSet["ShowPaymentDetail"];
                TimeZone = ResultSet["TimeZone"] as string;

                //cakel: added BugID 0007
                PaymentDueDay = ResultSet["PaymentDueDay"].ToString();
                //cakel: added BugID 0007
                AllowPrePayments = (bool)ResultSet["AllowPrePayments"];

                AllowPartialPayments = (bool)ResultSet["AllowPartialPayments"];

                //Added: Update ResultSets index
                NumOfPrePayDays = ResultSet["NumOfPrePayDays"].ToString();

                AcceptACHPayments = (bool)ResultSet["AcceptACH"];
                AcceptCCPayments = (bool)ResultSet["AcceptCC"];
                PNMEnabled = (bool)ResultSet["PayNearMeEnabled"];

                //Salcedo - 7/11/2014 - added support for MRI integration
                MRIEnableFtpUpload = (bool)ResultSet["MRIEnableFtpUpload"];
                MRIEnableFtpDownload = (bool)ResultSet["MRIEnableFtpDownload"];
                MRIFtpSiteAddress = ResultSet["MRIFtpSiteAddress"].ToString();
                MRIFtpPort = ResultSet["MRIFtpPort"].ToString();
                MRIFtpAccountUserName = ResultSet["MRIFtpAccountUserName"].ToString();
                MRIFtpAccountPassword = ResultSet["MRIFtpAccountPassword"].ToString();

                //cakel: BUGID00213
                CCDepositBankName = ResultSet["CCDepositBankName"].ToString();
                CCDepositAccountNumber = ResultSet["CCDepositBankAccountNumber"].ToString();
                CCDepositRoutingNumber = ResultSet["CCDepositBankRoutingNumber"].ToString();
                CCDepositAccountAchClientId = ResultSet["CCDepositAccountAchClientID"].ToString();
                CCDepositAccountFriendlyId = ResultSet["CCDepositAccountFriendlyID"].ToString();
                CCDepositAccountClearingDays = ResultSet["CCDepositAccountClearingDays"] as int?;
                GroupSettlementPaymentFlag = (bool)ResultSet["GroupSettlementPaymentFlag"];

                //Salcedo: 0000179
                YardiUseDepositDateAPI = (bool)ResultSet["YardiUseDepositDateAPI"];
                //cakel: BUGID00019
                AllowPartialPaymentsResidentPortal = (bool)ResultSet["AllowPartialPaymentsResidentPortal"];
                //cakel: BUGID00266
                GroupSettlementCashPaymentFlag = (bool)ResultSet["GroupSettlementCashPaymentFlag"];
                //cakel: BUGID00316
                AmsiUserName = ResultSet["AMSIUserId"].ToString();
                AmsiPassword = ResultSet["AMSIPassword"].ToString();
                AmsiDatabaseName = ResultSet["AMSIDatabase"].ToString();
                AmsiClientMerchantID = ResultSet["AMSIClientMerchantID"].ToString();
                AmsiImportEndPoint = ResultSet["AMSIImportEndPoint"].ToString();
                AmsiExportEndPoint = ResultSet["AMSIExportEndPoint"].ToString();
                //cakel: 00382  MRI Need to add code to return values
                MRIClientID = ResultSet["MRIClientID"].ToString();
                MRIDatabaseName = ResultSet["MRIDatabaseName"].ToString();
                MRIWebServiceUserName = ResultSet["MRIWebServiceUserName"].ToString();
                MRIPartnerKey = ResultSet["MRIPartnerKey"].ToString();
                MRIImportEndPoint = ResultSet["MRIImportEndPoint"].ToString();
                MRIExportEndPoint = ResultSet["MRIExportEndPoint"].ToString();
                MRIWebServicePassword = ResultSet["MRIWebServicePassword"].ToString();
                // Patrick Whittingham - 9/2/15 - task #371 : add notification email address 
                NotificationEmailAddress = ResultSet["NotificationEmailAddress"].ToString();
                //cakel: 00551'
                AmsiAchClientMerchantID = ResultSet["AMSIAchClientMerchantID"].ToString();
                AmsiCashClientMerchantID = ResultSet["AMSICashClientMerchantID"].ToString();

                RealPagePMCID = ResultSet["RealPagePmcID"].ToString();
                RealPageUserName = ResultSet["RealPageUserName"].ToString();
                RealPagePassword = ResultSet["RealPagePassword"].ToString();
                RealPageImportURL = ResultSet["RealPageImportURL"].ToString();
                RealPageExportURL = ResultSet["RealPageExportUrl"].ToString();
                RealPageInternalUser = ResultSet["RealPageInternalUser"].ToString();
                RealPageSiteID = ResultSet["RealPageSiteID"].ToString();

                //CMallory: Task 00499
                UsesApplyConnect = (bool)ResultSet["UsesApplyConnect"];
                //CMallory: Task 00425
                DisableAllPayments = (bool)ResultSet["DisableAllPayments"];
                //Cmallory: Task 00554 
                DisablePayByText = (bool)ResultSet["DisablePayByText"];
                //CMallory: Task 00166 - Added
                PropertyIsPublic = (bool)ResultSet["PropertyIsPublic"];
                ResultSet.Close();

            }
           
           
        }

        public Property()
        {
            InitializeObject();
        }
    }
}
