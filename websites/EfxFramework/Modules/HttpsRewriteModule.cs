﻿using System;
using System.Web;

namespace EfxFramework.Modules
{
    public class HttpsRewriteModule : IHttpModule
    {
        public void Dispose() {
            //clean-up code here.
        }

        public void Init(HttpApplication context)
        {
            //Wire up the BeginRequest event
            context.BeginRequest += ContextBeginRequest;
        }

        static void ContextBeginRequest(object sender, EventArgs e)
        {
            //Get the current scheme
            var CurrentScheme = HttpContext.Current.Request.Url.Scheme;

            //If the current scheme is HTTPS, we are good, so return
            if (string.Equals(CurrentScheme, "https", StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            //Get the current URL
            var NewUrl = HttpContext.Current.Request.Url.AbsoluteUri;

            //Replace the scheme
            NewUrl = NewUrl.Replace(CurrentScheme, "https");

            //Redirect
            HttpContext.Current.Response.Redirect(NewUrl);
        }
    }
}