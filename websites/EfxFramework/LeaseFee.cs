using System;
using Mb2x.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace EfxFramework
{
	public class LeaseFee : BaseEntity
	{
		//Public Properties
		[DisplayName(@"MonthlyFeeId"), Required(ErrorMessage = "MonthlyFeeId is required.")]
		public int MonthlyFeeId { get; set; }

		[DisplayName(@"LeaseId"), Required(ErrorMessage = "LeaseId is required.")]
		public int LeaseId { get; set; }

		[DisplayName(@"RenterId"), Required(ErrorMessage = "RenterId is required.")]
		public int RenterId { get; set; }

		[DisplayName(@"FeeAmount")]
		public decimal? FeeAmount { get; set; }

        [DisplayName(@"PropertyId")]
        public int PropertyId { get; set; }

        [DisplayName(@"FeeName")]
        public string FeeName { get; set; }

        public bool IsActive { get; set; }

        public string FeeAmountText { get { return FeeAmount.HasValue ? FeeAmount.Value.ToString("C") : "$0.00"; } }
        public string DeleteFeeUrl { get { return String.Format("~/Handlers/GeneralHandler.ashx?RequestType=9&MonthlyFeeId={0}&LeaseId={1}&RenterId={2}#LeaseInformationTab", MonthlyFeeId, LeaseId, RenterId); } }

        public static List<LeaseFee> GetFeesByLeaseId(int leaseId)
        {
            using (var Results = DataAccess.ExecuteDataTable(EfxSettings.ConnectionString, "dbo.usp_MonthlyFee_GetMonthlyFeesByLeaseId", new SqlParameter("@LeaseId", leaseId)))
            {
                return (from DataRow Row in Results.Rows
                        select new LeaseFee
                            {
                                MonthlyFeeId = (int) Row["MonthlyFeeId"], 
                                LeaseId = (int) Row["LeaseId"], 
                                RenterId = (int) Row["RenterId"], 
                                FeeAmount = Row["FeeAmount"] as decimal?, 
                                PropertyId = (int) Row["PropertyId"], 
                                FeeName = Row["FeeName"] as string,
                                IsActive = (bool) Row["IsActive"]
                            }).ToList();
            }
        }

        public static List<LeaseFee> GetFeesByRenterId(int renterId)
        {
            using (var Results = DataAccess.ExecuteDataTable(EfxSettings.ConnectionString, "dbo.usp_MonthlyFee_GetMonthlyFeesByRenterId", new SqlParameter("@RenterId", renterId)))
            {
                return (from DataRow Row in Results.Rows
                        select new LeaseFee
                        {
                            MonthlyFeeId = (int) Row["MonthlyFeeId"],
                            LeaseId = (int) Row["LeaseId"],
                            RenterId = (int) Row["RenterId"],
                            FeeAmount = Row["FeeAmount"] as decimal?,
                            PropertyId = (int) Row["PropertyId"],
                            FeeName = Row["FeeName"] as string,
                            IsActive = (bool) Row["IsActive"]
                        }).ToList();
            }
        }

        public static void DeleteLeaseFeesByLeaseId(int leaseId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_LeaseFee_DeleteLeaseFeeByLeaseId", new SqlParameter("@LeaseId", leaseId));
        }

        public static void DeleteLeaseFee(int monthlyFeeId, int leaseId, int renterId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_LeaseFee_DeleteSingleObject", new[]
                {
                    new SqlParameter("@MonthlyFeeId", monthlyFeeId),
                    new SqlParameter("@LeaseId", leaseId),
                    new SqlParameter("@RenterId", renterId)
                });
        }

        public static int Set(int monthlyFeeId, int leaseId, int renterId, decimal? feeAmount, bool isActive)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_LeaseFee_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@MonthlyFeeId", monthlyFeeId),
	                    new SqlParameter("@LeaseId", leaseId),
	                    new SqlParameter("@RenterId", renterId),
	                    new SqlParameter("@FeeAmount", feeAmount),
                        new SqlParameter("@IsActive", isActive)
                    });
        }

        public static int Set(LeaseFee l)
        {
	        return Set(
		        l.MonthlyFeeId,
		        l.LeaseId,
		        l.RenterId,
		        l.FeeAmount,
                l.IsActive
	        );
        }

        //Salcedo - 11/11/2015 - added the IsActive parameter to this function.
        //Apparently, it's always been there, but this function hasn't actually been used until now.
        public LeaseFee(int monthlyFeeId, int leaseId, int renterId, bool IsActive)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_LeaseFee_GetSingleObject", 
                new [] { new SqlParameter("@MonthlyFeeId", monthlyFeeId), 
                    new SqlParameter("@LeaseId", leaseId), 
                    new SqlParameter("@RenterId", renterId),
                    new SqlParameter("@IsActive", IsActive)});
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        MonthlyFeeId = (int)ResultSet[0];
		        LeaseId = (int)ResultSet[1];
		        RenterId = (int)ResultSet[2];
		        FeeAmount = ResultSet[3] as decimal?;
                IsActive = (bool) ResultSet[4];
            }
        }

		public LeaseFee()
		{
			InitializeObject();
		}

	}
}
