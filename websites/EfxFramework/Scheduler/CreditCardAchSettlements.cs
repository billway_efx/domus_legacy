﻿using ACH_Objects;
using System;
using System.Data;
using System.Data.SqlClient;

namespace EfxFramework.Scheduler
{
    public static class CreditCardAchSettlements
    {

        //cakel: BUGID00213 - File for Scheduler -- Added class file for Payment Scheduler to run Batch payments for properties who want batch payments
        public static void ProcessGroupCcAch()
        {
            
            var mySet = GroupPaymentACHDataSet();
            if (mySet.Tables[0].Rows.Count <= 0) return;

            foreach (DataRow dr in mySet.Tables[0].Rows)
            {
                var userId = EfxSettings.AchUserId;
                var password = EfxSettings.AchPassword;
                var achClientId = EfxSettings.CcSettlementAchId;

                var rent = dr["RentAmount"].ToString();

                Console.WriteLine(string.Concat("Processing CC settlement ACH transaction for PropertyId: ",
                    dr["PropertyId"], " (", dr["PropertyName"], ") in the amount of ", rent, " ..."));

                var ccDepositAccountAchClientId = dr["CCDepositAccountAchClientID"].ToString();
                var firstName = "CC Set";
                var lastName = "RentPaidOnline";
                if (ccDepositAccountAchClientId.Length == 36)
                {
                    var ml = new MerchantList();
                    var merchantId = new Guid(ccDepositAccountAchClientId);
                    var theMerchant = ml.GetMerchant(merchantId);
                    if (!string.IsNullOrEmpty(theMerchant.TransactionDescriptor))
                    {
                        firstName =
                            "CC Set " +
                            (theMerchant.TransactionDescriptor.Length > 10 ? theMerchant.TransactionDescriptor.Substring(0, 10) :
                                theMerchant.TransactionDescriptor); // + 
                        lastName = "RPO";
                    }
                }

                var achValidate = 
                    PaymentMethods.Ach.AchService.ProcessAchPayment(
                        EfxSettings.AchUserId,
                        EfxSettings.AchPassword,
                        EfxSettings.CcSettlementAchId,
                        (decimal)dr["RentAmount"],
                        dr["BankRoutingNumber"].ToString(),
                        dr["BankAccountNumber"].ToString(),
                        "Checking",
                        firstName, lastName,
                        dr["StreetAddress"].ToString(),
                        "",
                        dr["City"].ToString(),
                        dr["PropertyState"].ToString(),
                        dr["PostalCode"].ToString(),
                        ""
                    );

                if (!achValidate.ResponseCode.HasValue || (achValidate.ResponseCode.Value != 0))
                {
                    Console.WriteLine(string.Concat("Error creating ACH transaction. CCSettlementAchId=",
                        EfxSettings.CcSettlementAchId, " RentAmount=", ((decimal) dr["RentAmount"]), " FirstName=",
                        firstName, " LastName=", lastName, " RoutingNumber=", dr["BankRoutingNumber"], " Error=", 
                        achValidate.ResponseDescription));
                }
                else
                {
                    Console.WriteLine(@"... CC settlement credit ACH transaction was created. Updating RPO database to flag records as settled ...");

                    var achValResult = achValidate.TransactionId;
                    var propId = (int)dr["PropertyId"];
                    var TransTime = (DateTime)dr["TransTimeEnd"];
                    UpdatedCcSettledFlag(propId, TransTime, achValResult);

                    Console.WriteLine(@"... database updated.");
                }
            }
        }
        public static DataSet GroupPaymentACHDataSet()
        {
            var ds = new DataSet();
            using (var conn = new SqlConnection(ConnString))
            {
                var sqlComm = new SqlCommand("usp_Payment_GetPaymentsForGroupAchbyProperty_V3", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                var da = new SqlDataAdapter
                {
                    SelectCommand = sqlComm
                };
                da.Fill(ds);
            }
            return ds;
        }

        public static void UpdatedCcSettledFlag(int id, DateTime transTime, string achTransId)
        {
            var con = new SqlConnection(ConnString);
            var com = new SqlCommand("usp_Payment_UpdateGroupCCSettledFlagByPropertyID_V3", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            com.Parameters.Add("@ID", SqlDbType.Int).Value = id;
            com.Parameters.Add("@TransTimeEnd", SqlDbType.DateTime).Value = transTime;
            //cakel: ACH Mess
            com.Parameters.Add("@AchTransID", SqlDbType.NVarChar, 200).Value = achTransId;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        private static string ConnString => System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
    }
}
