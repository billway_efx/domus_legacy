﻿using System;
using System.Collections.Generic;
using System.Linq;
using EfxFramework.PaymentMethods.Ach;
using EfxFramework.PublicApi.Payment;
using Mb2x.ExtensionMethods;
using EfxFramework.ExtensionMethods;

namespace EfxFramework.Scheduler
{
    public static class AchStatus
    {
        public static void UpdateAchPaymentStatus()
        {
            var StartDate = Payment.GetEarliestPaymentWithProcessingPendingWorkingStatus().TransactionDateTime;

            if (StartDate.CompareTo(new DateTime(1, 1, 1)) == 0)
            {
                // ReSharper disable LocalizableElement
                Console.WriteLine("No Transactions updated: {0}", DateTime.UtcNow.ToShortDateString());
                // ReSharper restore LocalizableElement
                return;
            }
                
            var EndDate = DateTime.UtcNow;
            var Properties = Property.GetAllPropertyList();
            var StatusCollection = new List<EfxAchStatusCollection>();

            foreach (var Prop in Properties)
            {
                if (!String.IsNullOrEmpty(Prop.FeeAccountAchClientId))
                    StatusCollection.Add(AchService.GetAchStatusesByDate(Prop.FeeAccountAchClientId, StartDate, EndDate));

                if (!String.IsNullOrEmpty(Prop.RentalAccountAchClientId))
                    StatusCollection.Add(AchService.GetAchStatusesByDate(Prop.RentalAccountAchClientId, StartDate, EndDate));

                if (!String.IsNullOrEmpty(Prop.SecurityAccountAchClientId))
                    StatusCollection.Add(AchService.GetAchStatusesByDate(Prop.SecurityAccountAchClientId, StartDate, EndDate));
            }

            foreach (var Status in StatusCollection)
            {
                foreach (var Transactions in Status.Statuses)
                {
                    if (!String.IsNullOrEmpty(Transactions.TransactionId) && Transactions.StatusCode.HasValue)
                    {
                        var TransactionItem = Transaction.GetTransactionByExternalTransactionId(Transactions.TransactionId);
                        var PaymentItem = Payment.GetPaymentByTransactionId(TransactionItem.InternalTransactionId);
                        var StatusConversion = ((PaymentStatus) Transactions.StatusCode).GetFriendlyName().ToInt32();

                        if (TransactionItem.TransactionId > 0)
                        {
                            TransactionItem.PaymentStatusId = StatusConversion;
                            Transaction.Set(TransactionItem);
                            // ReSharper disable LocalizableElement
                            Console.WriteLine("(T)Transaction# {0} - Status Updated", Transactions.TransactionId);
                            // ReSharper restore LocalizableElement
                        }
                        if (PaymentItem.PaymentId > 0)
                        {
                            PaymentItem.PaymentStatus = (PaymentStatus)StatusConversion;
                            Payment.Set(PaymentItem);
                            // ReSharper disable LocalizableElement
                            Console.WriteLine("(P)Transaction# {0} - Status Updated", Transactions.TransactionId);
                            // ReSharper restore LocalizableElement
                        }
                    }
                }
            }
        }
    }
}
