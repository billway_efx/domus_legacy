﻿using System;
using System.Linq;
using Mb2x.ExtensionMethods;

namespace EfxFramework.Scheduler
{
    public static class CreditCardExpirationReminder
    {
        public static void GetExpiredCreditCards()
        {
            var ExpiredCards = PayerCreditCard.GetExpiringCreditCards();

            foreach (var Card in ExpiredCards)
            {
                var CardOwner = new Payer(Card.PayerId);
                var FirstRenter = Renter.GetRenterListByPayerId(Card.PayerId).FirstOrDefault(p => p.IsActive);

                if (FirstRenter == null)
                {
                    Console.WriteLine("There is an error with this Payer's - Renter Record, Payer Name: {0}", CardOwner.DisplayName);
                }
                else
                {
                    var Property = EfxFramework.Property.GetPropertyByRenterId(FirstRenter.RenterId);
                    var EmailAddress = CardOwner.PrimaryEmailAddress;
                    var DisplayName = CardOwner.DisplayName;

                    if (!EmailAddress.IsValidEmailAddress())
                    {
                        EmailAddress = FirstRenter.PrimaryEmailAddress;
                        DisplayName = FirstRenter.DisplayName;
                    }

                    SchedulerBase.SendEmail(Property.PropertyId.HasValue ? Property.PropertyId.Value : 0, "Your RentPaidOnline Account",
                                            String.Format(Resources.EmailTemplates.ExpiredCreditCardReminder, DisplayName), EmailAddress);

                    Console.WriteLine("Card Expiration Date: {0}", Card.CreditCardExpirationDate);
                    Console.WriteLine("Reminder Sent to: {0} on {1}", DisplayName, DateTime.Today.ToShortDateString());
                    Console.WriteLine();
                    Card.ReminderSent = true;
                    PayerCreditCard.Set(Card);
                }

                if (ExpiredCards.Count < 1)
                    Console.WriteLine((string.Format("No Expired CreditCards in this Batch, {0}", DateTime.Today.ToShortDateString())));
            }
        }
    }
}
