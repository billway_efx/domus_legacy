﻿using EfxFramework.PaymentMethods;
using EfxFramework.Pms.Yardi;
using EfxFramework.WebPayments;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;

namespace EfxFramework.Scheduler
{
    public static class CreditCardAutoPayment
    {
        public static void SendAutoPayFailureEmail(EfxFramework.Lease leaseObj, EfxFramework.Renter renterObj, int templateId)
        {
            var dt = new DataTable();
            try
            {
                var reason = "";
                switch (templateId)
                {
                    case 7:
                        reason = "Current Balance Due is $0.00.";
                        break;
                    case 8:
                        reason = "Lease is not active.";
                        break;
                    case 9:
                        reason = "Payment type is invalid for resident.";
                        break;
                }
                Console.WriteLine(string.Concat("Cannot accept payments for RenterId: ", renterObj.RenterId, " Reason: ", reason, " Sending email notification."));

                var commEmail = new SqlCommand("SELECT TEMPLATETEXT FROM EMAILTEMPLATES WHERE TEMPLATEID = " + templateId);
                var connEmail = new SqlConnection(ConnString);
                commEmail.Connection = connEmail;
                connEmail.Close();
                connEmail.Open();
                dt.Load(commEmail.ExecuteReader());
                var result = dt.Rows[0]["TEMPLATETEXT"].ToString();
                var realMessage = result.Replace("##NAME##", renterObj.FirstName + " " + renterObj.LastName);
                realMessage = realMessage.Replace("[[ROOT]]", EmailTemplateService.EmailTemplateService.RootPath);
                var realerMessage = realMessage.Replace("##RECDATE##", DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year);
                const string subject = "RentPaidOnline Auto Payment Could Not Be Processed.";

                BulkMail.BulkMail.SendMailMessage(
                    null,
                    "EFX",
                    false,
                    EfxFramework.EfxSettings.SendMailUsername,
                    EfxFramework.EfxSettings.SendMailPassword,
                    new MailAddress(EfxSettings.SupportEmail),
                    subject,
                    realerMessage,
                    new List<MailAddress> { new MailAddress(renterObj.PrimaryEmailAddress) });
                connEmail.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(@"Error occured while attempting to send Auto Pay Failure Email. Exception:  " + e);
            }
        }
        public static void ProcessCreditCardAutoPayments()
        {
            var counter = 0;

            foreach (var autoPayment in SchedulerBase.GetPaymentList(1))
            {
                Console.WriteLine(string.Concat("Processing Credit Card scheduled payment for Renter ID ", autoPayment.RenterId, ": AutoPaymentID=", autoPayment.AutoPaymentId,
                    ", RentAmount=", autoPayment.RentAmount, ", PayerID=", autoPayment.PayerId, " ..."));
                counter++;

                var renter = new Renter(autoPayment.RenterId);
                var lease = Lease.GetRenterLeaseByRenterId(renter.RenterId);
                var property = Property.GetPropertyByRenterId(autoPayment.RenterId);

                if (!property.PropertyId.HasValue || property.PropertyId.Value < 1)
                {
                    Console.WriteLine(@"This Resident's auto-pay record is missing a PropertyId.");
                    continue;
                }

                //Salcedo - 1/14/2015 - task 00347 - don't process the payment if the property doesn't accept CC payments
                if (!property.AcceptCCPayments)
                {
                    Console.WriteLine(string.Concat("This Resident's property (Property Id ",
                        property.PropertyId.Value,
                        ") does not currently accept ACH payments, and so will be skipped."));
                    continue;
                }

                //Salcedo - 1/14/2015 - task 00347 - don't process the payment if the property has been flagged for deletion
                if (property.IsDeleted.HasValue && property.IsDeleted.Value)
                {
                    Console.WriteLine(string.Concat("This Resident's property (Property Id ",
                        property.PropertyId.Value, ") is flagged for deletion, and so will be skipped."));
                    continue;
                }

                if (lease.CurrentBalanceDue == 0)
                {
                    SendAutoPayFailureEmail(lease, renter, 25);
                    continue;
                }

                if (lease.StartDate > System.DateTime.Now || lease.EndDate < System.DateTime.Now)
                {
                    SendAutoPayFailureEmail(lease, renter, 26);
                    continue;
                }

                if (renter.AcceptedPaymentTypeId == 4)
                {
                    SendAutoPayFailureEmail(lease, renter, 27);
                    continue;
                }

                switch (renter.PmsTypeId)
                {
                    case 1 when renter.PmsId != "":
                        ImportRequest.UpdateRenterBalanceDueRealTimeYardi(renter);
                        break;
                    //cakel: BUGID00316
                    case 2 when renter.PmsId != "":
                        Pms.AMSI.AmsiRequest.AMSI_RequestXmlForRenterImport(2, _RenterID: renter.RenterId);
                        break;
                    //cakel: TASK 00382 MRI
                    case 4 when renter.PmsId != "":
                        Pms.MRI.MriRequest.MRI_ProcessSingleRenter(renter.RenterId);
                        break;
                }

                var renterPayment = SchedulerBase.BuildRenterPaymentInfo(autoPayment, property, PaymentType.CreditCard);

                if (renterPayment == null) continue;

                //Salcedo - 2/27/2014 - call new function, for scheduled payments only
                PaymentRequest.ProcessScheduledPayment(renterPayment, false);
            }
            if (counter == 0)
            {
                Console.WriteLine(@"No Credit Card payments were scheduled for today.");
            }
            else
            {
                Console.WriteLine(counter.ToString() + " total scheduled Credit Card payments were processed today.");
            }


            foreach (var autoPayment in SchedulerBase.GetPaymentList(1))
            {
                var renter = new Renter(autoPayment.RenterId);
                Lease.GetRenterLeaseByRenterId(renter.RenterId);
                Property.GetPropertyByRenterId(autoPayment.RenterId);
            }
        }
        private static string ConnString => System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
    }
}
