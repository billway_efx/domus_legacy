﻿using EfxFramework.PaymentMethods;
using EfxFramework.WebPayments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace EfxFramework.Scheduler
{
    public static class SchedulerBase
    {
        public static RenterPaymentInfo BuildRenterPaymentInfo(AutoPayment autoPayment, Property property, PaymentType paymentType)
        {
            var Renter = new Renter(autoPayment.RenterId);
            var PayerId = Renter.PayerId.HasValue ? Renter.PayerId.Value : 0;

            if (Renter.RenterId < 1)
                return null;

            if (PayerId < 1)
                return null;

            var PayerCreditCard = EfxFramework.PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(PayerId);
            var PayerAch = EfxFramework.PayerAch.GetPrimaryPayerAchByPayerId(PayerId);
            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(autoPayment.RenterId);

            if (Lease.LeaseId < 1)
                return null;

            if (paymentType == PaymentType.CreditCard && PayerCreditCard.PayerCreditCardId < 1)
                return null;

            if (paymentType == PaymentType.ECheck && PayerAch.PayerAchId < 1)
                return null;

            //Salcedo - 5/1/2014 - solve problem with missing CC info
            int CCExpYear = PayerCreditCard.CreditCardExpirationYear;
            int CCExpMonth = PayerCreditCard.CreditCardExpirationMonth;
            if (CCExpYear == 0)
            {
                CCExpYear = 2005;
            }
            if (CCExpMonth == 0) 
            {
                CCExpMonth = 1;
            }

            return new RenterPaymentInfo
            {
                RenterId = Renter.RenterId,
                PaymentType = paymentType,
                BankAccountNumber = PayerAch.BankAccountNumber,
                BankRoutingNumber = PayerAch.BankRoutingNumber,
                CreditCardNumber = PayerCreditCard.CreditCardAccountNumber,
                CreditCardHolderName = PayerCreditCard.CreditCardHolderName,
                Cvv = null,
                //Salcedo - 4/3/2014 - we weren't resolving the credit expiration date using the CreditCardExpirationDate string property
                //ExpirationDate = GetDateTime(PayerCreditCard.CreditCardExpirationDate),
                //ExpirationDate = new DateTime(PayerCreditCard.CreditCardExpirationYear, PayerCreditCard.CreditCardExpirationMonth, 1),
                ExpirationDate = new DateTime(CCExpYear, CCExpMonth, 1),
                CharityAmount = autoPayment.CharityAmount,
                CharityName = autoPayment.CharityName,
                
                //RentAmount = Lease.CurrentBalanceDue.HasValue && Lease.CurrentBalanceDue.Value + Lease.TotalFees > 0.00M ? Lease.CurrentBalanceDue.Value + Lease.TotalFees : 0.00M,
                //cakel: 00584 AutoPay - Ensure balances return correctly
                RentAmount = Lease.CurrentBalanceDue.HasValue && Lease.CurrentBalanceDue > 0.00M ? Lease.CurrentBalanceDue.Value : 0.00M,

                RentAmountDescription = autoPayment.RentAmountDescription,
                OtherAmount = autoPayment.OtherAmount1,
                OtherAmountDescription = autoPayment.OtherDescription1,
                OtherAmount2 = autoPayment.OtherAmount2,
                OtherAmountDescription2 = autoPayment.OtherDescription2,
                OtherAmount3 = autoPayment.OtherAmount3,
                OtherAmountDescription3 = autoPayment.OtherDescription3,
                OtherAmount4 = autoPayment.OtherAmount4,
                OtherAmountDescription4 = autoPayment.OtherDescription4,
                OtherAmount5 = autoPayment.OtherAmount5,
                OtherAmountDescription5 = autoPayment.OtherDescription5,
                DisplayName = Renter.DisplayName,
                PayerId = PayerId,
                PropertyId = property.PropertyId.HasValue ? property.PropertyId.Value : 0
            };
        }

        public static IEnumerable<AutoPayment> GetPaymentList(int paymentType)
        {
            var AutoPayments = new List<AutoPayment>();

            //Salcedo - 2/26/2014 - for testing on the 23rd day of the month
            //AutoPayments.AddRange(AutoPayment.GetAutoPaymentListByPaymentDayOfTheMonth(1).Where(autoPayment => autoPayment.PaymentTypeId == paymentType));

            AutoPayments.AddRange(AutoPayment.GetAutoPaymentListByPaymentDayOfTheMonth(DateTime.UtcNow.Day).Where(autoPayment => autoPayment.PaymentTypeId == paymentType));

            //Need to account for dates after the end of the month for months less than 31 days
            switch (DateTime.UtcNow.Month)
            {
                case 4:
                case 6:
                case 9:
                case 11:
                    if (DateTime.UtcNow.Day == 30)
                        AutoPayments.AddRange(AutoPayment.GetAutoPaymentListByPaymentDayOfTheMonth(31).Where(autoPayment => autoPayment.PaymentTypeId == paymentType));
                    break;

                case 2:
                    if (DateTime.IsLeapYear(DateTime.UtcNow.Year) && DateTime.UtcNow.Day == 29)
                    {
                        AutoPayments.AddRange(AutoPayment.GetAutoPaymentListByPaymentDayOfTheMonth(30).Where(autoPayment => autoPayment.PaymentTypeId == paymentType));
                        AutoPayments.AddRange(AutoPayment.GetAutoPaymentListByPaymentDayOfTheMonth(31).Where(autoPayment => autoPayment.PaymentTypeId == paymentType));
                    }
                    else if (!DateTime.IsLeapYear(DateTime.UtcNow.Year) && DateTime.UtcNow.Day == 28)
                    {
                        AutoPayments.AddRange(AutoPayment.GetAutoPaymentListByPaymentDayOfTheMonth(29).Where(autoPayment => autoPayment.PaymentTypeId == paymentType));
                        AutoPayments.AddRange(AutoPayment.GetAutoPaymentListByPaymentDayOfTheMonth(30).Where(autoPayment => autoPayment.PaymentTypeId == paymentType));
                        AutoPayments.AddRange(AutoPayment.GetAutoPaymentListByPaymentDayOfTheMonth(31).Where(autoPayment => autoPayment.PaymentTypeId == paymentType));
                    }
                    break;
            }

            return AutoPayments;
        }

        //Salcedo - 5/26/2014 - task 0000023 - new function for use by PaymentReminder.cs
        public static IEnumerable<AutoPayment> GetPaymentList(int paymentType, int paymentDayOfMonth)
        {
            var AutoPayments = new List<AutoPayment>();

            AutoPayments.AddRange(AutoPayment.GetAutoPaymentListByPaymentDayOfTheMonth(paymentDayOfMonth).Where(autoPayment => autoPayment.PaymentTypeId == paymentType));

            return AutoPayments;
        }

        public static DateTime GetDateTime(string date)
        {
            DateTime Result;

            if (!DateTime.TryParse(date, out Result))
                Result = new DateTime(DateTime.UtcNow.Year - 1, DateTime.UtcNow.Month, 1);

            return Result;
        }

        public static decimal GetTotalAmount(RenterPaymentInfo payment, Lease lease)
        {
            var Sum = payment.RentAmount.HasValue ? payment.RentAmount.Value : 0.0M;
            Sum += payment.OtherAmount.HasValue ? payment.OtherAmount.Value : 0.0M;
            Sum += payment.OtherAmount2.HasValue ? payment.OtherAmount2.Value : 0.0M;
            Sum += payment.OtherAmount3.HasValue ? payment.OtherAmount3.Value : 0.0M;
            Sum += payment.OtherAmount4.HasValue ? payment.OtherAmount4.Value : 0.0M;
            Sum += payment.OtherAmount5.HasValue ? payment.OtherAmount5.Value : 0.0M;
            return Sum;
        }

        public static decimal GetBaseAmount(RenterPaymentInfo payment, Lease lease)
        {
            var Sum = lease.CurrentBalanceDue.HasValue ? lease.CurrentBalanceDue.Value + lease.TotalFees : 0.00M;

            if (!payment.OtherAmountDescription.ToLower().Contains("convenience fee"))
                Sum += payment.OtherAmount.HasValue ? payment.OtherAmount.Value : 0.0M;
            if (!payment.OtherAmountDescription2.ToLower().Contains("convenience fee"))
                Sum += payment.OtherAmount2.HasValue ? payment.OtherAmount2.Value : 0.0M;
            if (!payment.OtherAmountDescription3.ToLower().Contains("convenience fee"))
                Sum += payment.OtherAmount3.HasValue ? payment.OtherAmount3.Value : 0.0M;
            if (!payment.OtherAmountDescription4.ToLower().Contains("convenience fee"))
                Sum += payment.OtherAmount4.HasValue ? payment.OtherAmount4.Value : 0.0M;
            if (!payment.OtherAmountDescription5.ToLower().Contains("convenience fee"))
                Sum += payment.OtherAmount5.HasValue ? payment.OtherAmount5.Value : 0.0M;

            return Sum;
        }

        public static void SendEmail(int propertyId, string subject, string body, string renterEmail)
        {
            try
            {
                //Salcedo - 2/28/2014 - changed to call function that sends to the property management contacts
                //BulkMail.BulkMail.SendMailMessage(
                //propertyId,
                //"Renter",
                //false,
                //Settings.SendMailUsername,
                //Settings.SendMailPassword,
                //new MailAddress(Settings.PaymentEmail),
                //subject,
                //body,
                //new List<MailAddress> { new MailAddress(renterEmail) }
                //);
                BulkMail.BulkMail.SendMailMessageIncludePropertyEmailContacts(
                    propertyId,
                    "Renter",
                    EfxSettings.SendMailUsername,
                    EfxSettings.SendMailPassword,
                    new MailAddress(EfxSettings.PaymentEmail),
                    subject,
                    body,
                    new List<MailAddress> { new MailAddress(renterEmail) }
                    );
            }
            catch(Exception Ex)
            {
                Console.WriteLine(Ex.Message);
            }
        }
    }
}
