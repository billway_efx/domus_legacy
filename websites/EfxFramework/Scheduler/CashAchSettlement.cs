﻿//Salcedo - 4/16/2015 - task # 00375

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using ACH_Objects;
using EfxFramework.PaymentMethods.Ach;

namespace EfxFramework.Scheduler
{
    public static class CashAchSettlements
    {
        public static void ProcessGroupCCAch()
        {
            var mySet = GroupPaymentAchDataSet();
            if (mySet.Tables[0].Rows.Count <= 0) return;

            foreach (DataRow dr in mySet.Tables[0].Rows)
            {
                var rent = dr["RentAmount"].ToString();
                Console.WriteLine(string.Concat("Processing Cash settlement ACH transaction for PropertyId: ",
                    dr["PropertyId"], " (", dr["PropertyName"], ") in the amount of ", rent, " ..."));

                var pnmachClientId = EfxSettings.PnmachClientId;
                var cashDepositAccountAchClientId = dr["CashDepositAccountACHClientID"].ToString();
                var firstName = "RBC Set";
                var lastName = "RentPaidOnline";
                if (cashDepositAccountAchClientId.Length == 36)
                {
                    var ml = new MerchantList();
                    var MerchantId = new Guid(cashDepositAccountAchClientId);
                    var theMerchant = ml.GetMerchant(MerchantId);
                    if (theMerchant.TransactionDescriptor != null && theMerchant.TransactionDescriptor.Length > 0)
                    {
                        firstName =
                            "RBC Set " +
                            (theMerchant.TransactionDescriptor.Length > 10 ? theMerchant.TransactionDescriptor.Substring(0, 10) :
                                theMerchant.TransactionDescriptor); // +
                        lastName = "RPO";
                    }
                }

                var rentAmount = (decimal)dr["RentAmount"];
                var achValidate =
                    AchService.ProcessAchPayment(
                        EfxSettings.AchUserId,
                        EfxSettings.AchPassword,
                        pnmachClientId,
                        rentAmount,
                        dr["BankRoutingNumber"].ToString(),
                        dr["BankAccountNumber"].ToString(),
                        "Checking",
                        firstName, 
                        lastName,
                        dr["StreetAddress"].ToString(),
                        "",
                        dr["City"].ToString(),
                        dr["PropertyState"].ToString(),
                        dr["PostalCode"].ToString(),
                        ""
                    );

                if (!achValidate.ResponseCode.HasValue || (achValidate.ResponseCode.HasValue && achValidate.ResponseCode.Value != 0))
                {
                    Console.WriteLine(string.Concat("Error creating ACH transaction. PNMACHClientId=", pnmachClientId,
                        " RentAmount=", rentAmount, " FirstName=", firstName, " LastName=", lastName, " RoutingNumber=",
                        dr["BankRoutingNumber"], " Error=", achValidate.ResponseDescription));
                }
                else
                {
                    Console.WriteLine(@"... Cash settlement credit ACH transaction was created. Updating RPO database to flag records as settled ...");

                    var achValResult = achValidate.TransactionId;
                    var propId = (int)dr["PropertyId"];
                    var transTime = (DateTime)dr["TransTimeEnd"];
                    UpdatedCcSettledFlag(propId, transTime, achValResult);

                    Console.WriteLine(@"... database updated.");
                }
            }
        }

        //cakel: BUGID00367 - Updated SP
        public static DataSet GroupPaymentAchDataSet()
        {
            var ds = new DataSet();
            using (var conn = new SqlConnection(ConnString))
            {
                var sqlComm = new SqlCommand("usp_Payment_GetCashPaymentsForGroupAchbyProperty_V3", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                var da = new SqlDataAdapter
                {
                    SelectCommand = sqlComm
                };
                da.Fill(ds);
            }
            return ds;
        }

        public static void UpdatedCcSettledFlag(int id, DateTime transTime, string achTransId)
        {
            var con = new SqlConnection(ConnString);
            var com = new SqlCommand("usp_Payment_UpdateGroupCashSettledFlagByPropertyID_V3", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            com.Parameters.Add("@ID", SqlDbType.Int).Value = id;
            com.Parameters.Add("@TransTimeEnd", SqlDbType.DateTime).Value = transTime;
            com.Parameters.Add("@AchTransID", SqlDbType.NVarChar, 200).Value = achTransId;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        private static string ConnString => ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
    }
}
