﻿using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace EfxFramework.Scheduler
{
    //Salcedo - 5/26/2014 - task 0000023 - new class used to send auto payment reminders
    public static class PaymentReminder
    {
        public static void SendPaymentReminders()
        {
            SendACHReminders();
            SendCcReminders();
        }

        public static void SendACHReminders()
        {
            Console.WriteLine(@"Sending ACH auto payment reminders ...");
            var autoPaymentReminderDays = EfxSettings.AutoPaymentReminderDays;
            var reminderDay = DateTime.UtcNow.AddDays(autoPaymentReminderDays).Day;
            Console.WriteLine(
                $@"Reminders will be sent for payments scheduled for {DateTime.UtcNow.AddDays(autoPaymentReminderDays).ToShortDateString()}");
            foreach (var autoPayment in SchedulerBase.GetPaymentList(2, reminderDay))
            {
                SendMail(autoPayment);
            }
            Console.WriteLine(@"... complete.");
        }

        public static void SendCcReminders()
        {
            Console.WriteLine(@"Sending Credit Card auto payment reminders ...");
            var autoPaymentReminderDays = EfxSettings.AutoPaymentReminderDays;
            var reminderDay = DateTime.UtcNow.AddDays(autoPaymentReminderDays).Day;
            Console.WriteLine(
                $@"Reminders will be sent for payments scheduled for {DateTime.UtcNow.AddDays(autoPaymentReminderDays).ToShortDateString()}");
            foreach (var autoPayment in SchedulerBase.GetPaymentList(1, reminderDay))
            {
                SendMail(autoPayment);
            }
            Console.WriteLine(@"... complete.");
        }

        private static void SendMail(AutoPayment autoPayment)
        {
            var theProperty = Property.GetPropertyByRenterId(autoPayment.RenterId);
            var theRenter = new Renter(autoPayment.RenterId);
            if (theProperty?.PropertyId == null) return;

            const string subject = "Your Scheduled DOMUS Payment Is Approaching";
            var propertyName = theProperty.PropertyName;
            var recipientName = theRenter.DisplayName;
            var autoPaymentReminderDays = EfxSettings.AutoPaymentReminderDays;
            var draftDate = DateTime.UtcNow.AddDays(autoPaymentReminderDays).ToShortDateString();
            const string paymentAmount = "Full Balance Due";
            var paymentType = "";
            switch (autoPayment.PaymentTypeId)
            {
                case 1:
                    paymentType = "Credit Card";
                    break;
                case 2:
                    paymentType = "eCheck";
                    break;
            }

            var body = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-PaymentReminder.html").Replace("[[ROOT]]",
                EmailTemplateService.EmailTemplateService.RootPath), recipientName, propertyName, draftDate, paymentAmount, paymentType);
            if (body == "")
            {
                Console.WriteLine(@"Unable to locate email template file ~/EmailTemplates/DomusMe-PaymentReminder.html. Check configuration");
                return;
            }

            var intPropertyId = theProperty.PropertyId.Value;
            const string type = "Renter";

            //Salcedo - 8/31/2015 - added logic to verify email address before attempting to send email
            if (BulkMail.BulkMail.IsValidEmail(theRenter.PrimaryEmailAddress))
            { 
                Console.WriteLine(
                    $@"Emailing {paymentType} autopayment reminder for {recipientName} to {theRenter.PrimaryEmailAddress} (RenterId={autoPayment.RenterId.ToString()}) ...");
                BulkMail.BulkMail.SendMailMessage(
                    theProperty.PropertyId,
                    type,
                    false,
                    EfxSettings.SendMailUsername,
                    EfxSettings.SendMailPassword,
                    new MailAddress(EfxSettings.PaymentEmail),
                    subject,
                    body,
                    new List<MailAddress> { new MailAddress(theRenter.PrimaryEmailAddress) }
                    );
            }
            else
            {
                Console.WriteLine(
                    $@"Unable to email {paymentType} autopayment reminder for {recipientName} to {theRenter.PrimaryEmailAddress} (RenterId={autoPayment.RenterId.ToString()}) because of invalid email address.");
            }
        }
    }
}
