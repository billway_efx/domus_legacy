﻿using EfxFramework.PaymentMethods;
using EfxFramework.Pms.Yardi;
using EfxFramework.WebPayments;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;

namespace EfxFramework.Scheduler
{
    public static class EcheckAutoPayment
    {
        private static string ConnString => System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public static void SendAutoPayFailureEmail(Lease leaseObj, Renter renterObj, int templateId)
        {
            try
            {
                string reason;
                switch (templateId)
                {
                    case 25:
                        var balance = $"{leaseObj.CurrentBalanceDue.Value:C}";
                        if (leaseObj.CurrentBalanceDue.Value <= 0)
                            balance += " (Credit Balance)";
                        reason = "Current Balance Due is " + balance;
                        break;
                    case 27:
                        reason = "Lease is not active.";
                        break;
                    case 26:
                        reason = "Payment type is invalid for resident.";
                        break;
                    default:
                        reason = "Unspecified reason.";
                        break;
                }
                Console.WriteLine(string.Concat("Cannot accept payments for RenterId: ", renterObj.RenterId, " Reason: ", reason, " Sending email notification."));

                var commEmail = new SqlCommand("SELECT TEMPLATETEXT FROM EMAILTEMPLATES WHERE TEMPLATEID = " +
                                               templateId);
                var connEmail = new SqlConnection(ConnString);
                commEmail.Connection = connEmail;
                connEmail.Close();
                connEmail.Open();
                var dt = new DataTable();
                dt.Load(commEmail.ExecuteReader());
                var result = dt.Rows[0]["TEMPLATETEXT"].ToString();
                var realMessage = result.Replace("##NAME##", renterObj.FirstName + " " + renterObj.LastName);
                realMessage = realMessage.Replace("[[ROOT]]", EmailTemplateService.EmailTemplateService.RootPath);
                var realerMessage = realMessage.Replace("##RECDATE##", DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year);
                const string Subject = "RentPaidOnline Auto Payment Could Not Be Processed.";
                BulkMail.BulkMail.SendMailMessage(
                    null,
                    "EFX",
                    false,
                    EfxSettings.SendMailUsername,
                    EfxSettings.SendMailPassword,
                    new MailAddress(EfxSettings.SupportEmail),
                    Subject,
                    realerMessage,
                    new List<MailAddress> { new MailAddress(renterObj.PrimaryEmailAddress) });
                connEmail.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Concat("Error occured while attempting to send Auto Pay Failure Email. Exception:  ", e.ToString()));
            }
        }
        
        public static void ProcessEcheckAutoPayments()
        {
            var counter = 0;
            foreach (var autoPayment in SchedulerBase.GetPaymentList(2))
            {
                Console.WriteLine(string.Concat("Processing Echeck scheduled payment for Renter ID ",
                    autoPayment.RenterId, ": AutoPaymentID=", autoPayment.AutoPaymentId, ", RentAmount=",
                    autoPayment.RentAmount, ", PayerID=", autoPayment.PayerId, " ..."));
                counter++;

                var renter = new Renter(autoPayment.RenterId);
                var lease = Lease.GetRenterLeaseByRenterId(renter.RenterId);
                var property = Property.GetPropertyByRenterId(autoPayment.RenterId);
                var dayCount = GetLastPaymentForRenter(renter.RenterId);

                if (!property.PropertyId.HasValue || property.PropertyId.Value < 1)
                {
                    Console.WriteLine(@"This Resident's auto-pay record is missing a PropertyId.");
                    continue;
                }

                if (!property.AcceptACHPayments)
                {
                    Console.WriteLine(string.Concat("This Resident's property (Property Id ", property.PropertyId.Value, ") does not currently accept ACH payments, and so will be skipped."));
                    continue;
                }

                if (property.IsDeleted.HasValue && property.IsDeleted.Value)
                {
                    Console.WriteLine(string.Concat("This Resident's property (Property Id ", property.PropertyId.Value, ") is flagged for deletion, and so will be skipped."));
                    continue;
                }

                if (renter.PmsTypeId == 1 && renter.PmsId != "")
                {
                    ImportRequest.UpdateRenterBalanceDueRealTimeYardi(renter);
                }

                if (lease.CurrentBalanceDue.Value <= 0)
                {
                    SendAutoPayFailureEmail(lease, renter, 25);
                    continue;
                }
                if (renter.AcceptedPaymentTypeId == 4 || renter.AcceptedPaymentTypeId == 3)
                {
                    SendAutoPayFailureEmail(lease, renter, 26);
                    continue;
                }

                if(renter.IsActive == false) continue;

                if (renter.PmsId == "")
                {
                    if (lease.StartDate > DateTime.Now || lease.EndDate < DateTime.Now)
                    {
                        //CMallory - Modified For DomusMe
                        SendAutoPayFailureEmail(lease, renter, 27);
                        continue;
                    }
                }
                
                if(dayCount <= 5)
                {
                    Console.WriteLine(string.Concat("This resident has made a recent payment within ", dayCount, "days so we will not process another payment"));
                    continue;
                }

                switch (renter.PmsTypeId)
                {
                    case 2 when renter.PmsId != "":
                        Pms.AMSI.AmsiRequest.AMSI_RequestXmlForRenterImport(2, _RenterID: renter.RenterId);
                        break;
                    //cakel: TASK 00382 MRI
                    case 4 when renter.PmsId != "":
                        Pms.MRI.MriRequest.MRI_ProcessSingleRenter(renter.RenterId);
                        break;
                }

                var renterPayment = SchedulerBase.BuildRenterPaymentInfo(autoPayment, property, PaymentType.ECheck);

                if (renterPayment == null)
                {
                    Console.WriteLine(@"This resident's RenterPayment object is null.");
                    continue;
                }
                try
                {
                    var response = PaymentRequest.ProcessScheduledPayment(renterPayment, false);
                    AutoPayment.LogAutoPayment(autoPayment.AutoPaymentId);

                    var paymentTotal = SchedulerBase.GetTotalAmount(renterPayment, lease);

                    if (paymentTotal > 0.0M)
                    {
                        Console.WriteLine(response.RentAndFeeResult.ToString());
                        Console.WriteLine(response.RentAndFeeResponseMessage);
                        Console.WriteLine(response.RentAndFeeTransactionId);
                    }

                    if (renterPayment.CharityAmount > 0.0M)
                    {
                        Console.WriteLine(response.CharityResult.ToString());
                        Console.WriteLine(response.CharityResponseMessage);
                        Console.WriteLine(response.CharityTransactionId);
                    }

                    if (response.RentAndFeeResult == GeneralResponseResult.Success && paymentTotal > 0.0M)
                    {
                        lease.CurrentBalanceDue -= SchedulerBase.GetTotalAmount(renterPayment, lease);
                        lease.CurrentBalanceDueLastUpdated = DateTime.UtcNow;
                        Lease.Set(lease);
                    }
                }
                catch(Exception ex)
                {
                    var errorString = ex.ToString();
                    Console.WriteLine(string.Concat("Error processing Echeckpayment for Renter ID:", renter.RenterId.ToString()));
                    Console.WriteLine(string.Concat("Error: " + errorString));
                }
            } //end foreach

            if (counter == 0)
            {
                Console.WriteLine(@"No Echeck payments were scheduled for today.");
            }
            else
            {
                Console.WriteLine(string.Concat(counter," total scheduled Echeck payments were processed today."));
            }
        }

        public static int GetLastPaymentForRenter(int renterId)
        {
            var con = new SqlConnection(ConnString);
            var com = new SqlCommand("usp_Payment_GetRenterLastPaymentDetail", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            com.Parameters.Add("@RenterID", SqlDbType.Int).Value = renterId;
            com.Parameters.Add("@Days", SqlDbType.Int).Direction = ParameterDirection.Output;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                var days = (int)com.Parameters["@Days"].Value;
                con.Close();
                return days;
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }
        }
    }
}
