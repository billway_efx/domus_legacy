﻿using EfxFramework.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace EfxFramework.Scheduler
{
    public static class SmsReminder
    {
        public static void SendSmsReminders()
        {
            var Message = "Rent is due";

            Console.WriteLine("{0}: {1}", 17274831023, Message);
            Sms.PaymentRequest.ReplyToSender(Message, 17274831023.ToString(), "0");

            //foreach (var Renter in GetReminderList())
            //{
            //    var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(Renter.RenterId);
            //    var Message = GetSmsReminderText(Lease);

            //    Console.WriteLine("{0}: {1}", Renter.SmsPaymentPhoneNumber, Message);
            //    Sms.PaymentRequest.ReplyToSender(Message, 7274831023.ToString(), "0");
            //}
        }

        private static IEnumerable<Renter> GetReminderList()
        {
            var Renters = new List<Renter>();
            Renters.AddRange(Renter.GetRenterListBySmsReminderDayOfTheMonth(DateTime.UtcNow.Day).Where(renter => !String.IsNullOrEmpty(renter.SmsPaymentPhoneNumber)));

            //Need to account for dates after the end of the month for months less than 31 days
            switch (DateTime.UtcNow.Month)
            {
                case 4:
                case 6:
                case 9:
                case 11:
                    if (DateTime.UtcNow.Day == 30)
                        Renters.AddRange(Renter.GetRenterListBySmsReminderDayOfTheMonth(31).Where(renter => !String.IsNullOrEmpty(renter.SmsPaymentPhoneNumber)));
                    break;

                case 2:
                    if (DateTime.IsLeapYear(DateTime.UtcNow.Year) && DateTime.UtcNow.Day == 29)
                    {
                        Renters.AddRange(Renter.GetRenterListBySmsReminderDayOfTheMonth(30).Where(renter => !String.IsNullOrEmpty(renter.SmsPaymentPhoneNumber)));
                        Renters.AddRange(Renter.GetRenterListBySmsReminderDayOfTheMonth(31).Where(renter => !String.IsNullOrEmpty(renter.SmsPaymentPhoneNumber)));
                    }
                    else if (!DateTime.IsLeapYear(DateTime.UtcNow.Year) && DateTime.UtcNow.Day == 28)
                    {
                        Renters.AddRange(Renter.GetRenterListBySmsReminderDayOfTheMonth(29).Where(renter => !String.IsNullOrEmpty(renter.SmsPaymentPhoneNumber)));
                        Renters.AddRange(Renter.GetRenterListBySmsReminderDayOfTheMonth(30).Where(renter => !String.IsNullOrEmpty(renter.SmsPaymentPhoneNumber)));
                        Renters.AddRange(Renter.GetRenterListBySmsReminderDayOfTheMonth(31).Where(renter => !String.IsNullOrEmpty(renter.SmsPaymentPhoneNumber)));
                    }
                    break;
            }

            return Renters;
        }

        private static string GetSmsReminderText(Lease lease)
        {
            var ReminderText = new StringBuilder();
            ReminderText.Append("This is a reminder that your rent ");

            if (lease.LeaseId > 0)
            {
                if (lease.RentDueDayOfMonth > 0)
                    ReminderText.AppendFormat("is due on the {0} day of the month.", lease.RentDueDayOfMonth.ToString(CultureInfo.InvariantCulture).ToNthValue());
                else
                    ReminderText.Append("is due soon.");

                if (lease.RentAmount > 0.0M)
                    ReminderText.Append("Please text \"PayMyRent\" to 57682 to pay your rent via text message.");
            }
            else
                ReminderText.Append("is due soon.");

            return ReminderText.ToString();
        }
    }
}
