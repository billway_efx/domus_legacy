using System.Web;
using EfxFramework.Web.Caching;
using Mb2x.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;

namespace EfxFramework
{
	public class MaintenanceRequest : BaseEntity
	{
		//Public Properties
		[DisplayName(@"MaintenanceRequestId"), Required(ErrorMessage = "MaintenanceRequestId is required.")]
		public int MaintenanceRequestId { get; set; }

		[DisplayName(@"MaintenanceRequestTypeId"), Required(ErrorMessage = "MaintenanceRequestTypeId is required.")]
		public int MaintenanceRequestTypeId { get; set; }

		[DisplayName(@"MaintenanceRequestPriorityId"), Required(ErrorMessage = "MaintenanceRequestPriorityId is required.")]
		public int MaintenanceRequestPriorityId { get; set; }

		[DisplayName(@"PropertyId"), Required(ErrorMessage = "PropertyId is required.")]
		public int PropertyId { get; set; }

		[DisplayName(@"RenterId"), Required(ErrorMessage = "RenterId is required.")]
		public int RenterId { get; set; }

		[DisplayName(@"MaintenanceRequestDate"), Required(ErrorMessage = "MaintenanceRequestDate is required.")]
		public DateTime MaintenanceRequestDate { get; set; }

		[DisplayName(@"MaintenanceRequestDescription")]
		public string MaintenanceRequestDescription { get; set; }

		[DisplayName(@"IsActive"), Required(ErrorMessage = "IsActive is required.")]
		public bool IsActive { get; set; }

        public string MobileMaintenanceRequestDate { get; set; }

        public static List<MaintenanceRequest> GetAllMaintenanceRequests()
        {
            return DataAccess.GetTypedList<MaintenanceRequest>(EfxSettings.ConnectionString, "dbo.usp_MaintenanceRequest_GetAllMaintenanceRequests");
            //if (HttpContext.Current == null)
                //return DataAccess.GetTypedList<MaintenanceRequest>(Settings.ConnectionString, "dbo.usp_MaintenanceRequest_GetAllMaintenanceRequests");

            //var Requests = CacheManager.GetCacheItem<List<MaintenanceRequest>>(CacheKey.MaintenanceRequestCache);

            //if (Requests == null || Requests.Count < 1)
            //    CacheManager.AddItemToCache(CacheKey.MaintenanceRequestCache, DataAccess.GetTypedList<MaintenanceRequest>(Settings.ConnectionString, "dbo.usp_MaintenanceRequest_GetAllMaintenanceRequests"), 30);

            //return CacheManager.GetCacheItem<List<MaintenanceRequest>>(CacheKey.MaintenanceRequestCache);
        }

        //Salcedo - 4/5/2016 - task # 623 - created new function to call procedure which limits the result set to just this single RenterId, instead of all records
        public static List<MaintenanceRequest> GetAllMaintenanceRequestsForRenterId(int renterId)
        {
            return DataAccess.GetTypedList<MaintenanceRequest>(EfxSettings.ConnectionString, "dbo.usp_MaintenanceRequest_GetAllMaintenanceRequestsByRenterId",new SqlParameter("RenterId",renterId));
        }

        public static List<MaintenanceRequest> GetAllMaintenanceRequestsByRenterId(int renterId)
        {
            //Salcedo - 4/5/2016 - task # 623 - modified to call new procedure which limits the result set to just this RenterId
            return GetAllMaintenanceRequestsForRenterId(renterId).Where(r => r.RenterId == renterId).ToList();
        }

        public static List<MaintenanceRequest> GetActiveMaintenanceRequestsByRenterId(int renterId)
        {
            //Salcedo - 4/5/2016 - task # 623 - modified to call new procedure which limits the result set to just this RenterId
            return GetAllMaintenanceRequestsForRenterId(renterId).Where(r => r.RenterId == renterId && r.IsActive).ToList();
        }

        public static List<MaintenanceRequest> GetAllMaintenanceRequestsByPropertyId(int propertyId)
        {
            return GetAllMaintenanceRequests().Where(r => r.PropertyId == propertyId).ToList();
        }

        public static List<MaintenanceRequest> GetActiveMaintenanceRequestsByPropertyId(int propertyId)
        {
            return GetAllMaintenanceRequests().Where(r => r.PropertyId == propertyId && r.IsActive).ToList();
        }

        public static int Set(int maintenanceRequestId, int maintenanceRequestTypeId, int maintenanceRequestPriorityId, int propertyId, int renterId, DateTime maintenanceRequestDate, string maintenanceRequestDescription, bool isActive)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_MaintenanceRequest_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@MaintenanceRequestId", maintenanceRequestId),
	                    new SqlParameter("@MaintenanceRequestTypeId", maintenanceRequestTypeId),
	                    new SqlParameter("@MaintenanceRequestPriorityId", maintenanceRequestPriorityId),
	                    new SqlParameter("@PropertyId", propertyId),
	                    new SqlParameter("@RenterId", renterId),
	                    new SqlParameter("@MaintenanceRequestDate", maintenanceRequestDate),
	                    new SqlParameter("@MaintenanceRequestDescription", maintenanceRequestDescription),
	                    new SqlParameter("@IsActive", isActive)
                    });
        }

        public static int Set(MaintenanceRequest m)
        {
	        return Set(
		        m.MaintenanceRequestId,
		        m.MaintenanceRequestTypeId,
		        m.MaintenanceRequestPriorityId,
		        m.PropertyId,
		        m.RenterId,
		        m.MaintenanceRequestDate,
		        m.MaintenanceRequestDescription,
		        m.IsActive
	        );
        }

        public MaintenanceRequest(int maintenanceRequestId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_MaintenanceRequest_GetSingleObject", new [] { new SqlParameter("@MaintenanceRequestId", maintenanceRequestId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        MaintenanceRequestId = (int)ResultSet[0];
		        MaintenanceRequestTypeId = (int)ResultSet[1];
		        MaintenanceRequestPriorityId = (int)ResultSet[2];
		        PropertyId = (int)ResultSet[3];
		        RenterId = (int)ResultSet[4];
		        MaintenanceRequestDate = (DateTime)ResultSet[5];
		        MaintenanceRequestDescription = ResultSet[6] as string;
		        IsActive = (bool)ResultSet[7];
                MobileMaintenanceRequestDate = MaintenanceRequestDate.ToString("MM/dd/yy");
            }
        }

		public MaintenanceRequest()
		{
			InitializeObject();
		}

	}
}
