using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;

namespace EfxFramework
{
    public class Transaction : BaseEntity
	{
		//Public Properties
		[DisplayName(@"TransactionId"), Required(ErrorMessage = "TransactionId is required.")]
		public int TransactionId { get; set; }

		[DisplayName(@"InternalTransactionId"), Required(ErrorMessage = "InternalTransactionId is required."), StringLength(6)]
		public string InternalTransactionId { get; set; }

		[DisplayName(@"ExternalTransactionId"), StringLength(20)]
		public string ExternalTransactionId { get; set; }

		[DisplayName(@"PaymentTypeId"), Required(ErrorMessage = "PaymentTypeId is required.")]
		public int PaymentTypeId { get; set; }

		[DisplayName(@"FirstName"), Required(ErrorMessage = "FirstName is required."), StringLength(100)]
		public string FirstName { get; set; }

		[DisplayName(@"LastName"), Required(ErrorMessage = "LastName is required."), StringLength(100)]
		public string LastName { get; set; }

		[DisplayName(@"StreetAddress"), Required(ErrorMessage = "StreetAddress is required."), StringLength(100)]
		public string StreetAddress { get; set; }

		[DisplayName(@"StreetAddress2"), StringLength(100)]
		public string StreetAddress2 { get; set; }

		[DisplayName(@"City"), Required(ErrorMessage = "City is required."), StringLength(100)]
		public string City { get; set; }

		[DisplayName(@"StateProvinceId"), Required(ErrorMessage = "StateProvinceId is required.")]
		public int StateProvinceId { get; set; }

		[DisplayName(@"PostalCode"), Required(ErrorMessage = "PostalCode is required."), StringLength(10)]
		public string PostalCode { get; set; }

		[DisplayName(@"PhoneNumber"), Required(ErrorMessage = "PhoneNumber is required."), StringLength(10)]
		public string PhoneNumber { get; set; }

		[DisplayName(@"EmailAddress"), Required(ErrorMessage = "EmailAddress is required."), StringLength(100)]
		public string EmailAddress { get; set; }

		[DisplayName(@"Memo")]
		public string Memo { get; set; }

		[DisplayName(@"PaymentAmount"), Required(ErrorMessage = "PaymentAmount is required.")]
		public decimal PaymentAmount { get; set; }

		[DisplayName(@"ConvenienceFeeAmount")]
		public decimal? ConvenienceFeeAmount { get; set; }

		[DisplayName(@"CreditCardHolderName")]
		public string CreditCardHolderName { get; set; }

		[DisplayName(@"CreditCardAccountNumber")]
		public string CreditCardAccountNumber { get; set; }

		[DisplayName(@"CreditCardExpirationMonth")]
		public int? CreditCardExpirationMonth { get; set; }

		[DisplayName(@"CreditCardExpirationYear")]
		public int? CreditCardExpirationYear { get; set; }

		[DisplayName(@"BankAccountNumber")]
		public string BankAccountNumber { get; set; }

		[DisplayName(@"BankRoutingNumber")]
		public string BankRoutingNumber { get; set; }

		[DisplayName(@"BankAccountTypeId")]
		public int? BankAccountTypeId { get; set; }

		[DisplayName(@"PaymentId")]
		public int? PaymentId { get; set; }

		[DisplayName(@"PaymentStatusId"), Required(ErrorMessage = "PaymentStatusId is required.")]
		public int PaymentStatusId { get; set; }

		[DisplayName(@"ResponseResult")]
		public int? ResponseResult { get; set; }

		[DisplayName(@"ResponseMessage"), StringLength(200)]
		public string ResponseMessage { get; set; }

		[DisplayName(@"TransactionDate"), Required(ErrorMessage = "TransactionDate is required.")]
		public DateTime TransactionDate { get; set; }

		[DisplayName(@"PropertyId"), Required(ErrorMessage = "PropertyId is required.")]
        public int PropertyId { get; set; }

        //cakel: BUGID00213
        [DisplayName(@"CCAchSettledFlag")]
        public bool CCAchSettledFlag { get; set; }

        //cakel: BUGID00213 - Added bool to set true or false depending on Case where it is used - default to false
        public static void SetTransactionFromPayment(Payment payment, int responseResult, string  responseMessage, bool SetCCAchSettlement = false)
        {
            var Resident = new Renter(payment.RenterId);
            var Property = EfxFramework.Property.GetPropertyByRenterId(Resident.RenterId);
            var Address = GetRenterAddress(Resident);

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                return;

            var ConvenienceFee = 0.00M;

            if (!String.IsNullOrEmpty(payment.OtherDescription2) && payment.OtherDescription2.ToLower() == "convenience fee")
                ConvenienceFee = payment.OtherAmount2.HasValue ? payment.OtherAmount2.Value : 0.00M;

            var PhoneNumber = Address.PrimaryPhoneNumber;
            if (String.IsNullOrEmpty(PhoneNumber))
                PhoneNumber = Resident.MainPhoneNumber;
            //cakel: TESTING
            string testResp = responseResult.ToString();
            //cakel: BUGID00213 - Transaction Function
            var Transaction = new Transaction
                {
                    ExternalTransactionId = payment.TransactionId,
                    PaymentTypeId = payment.PaymentTypeId,
                    FirstName = Resident.FirstName,
                    LastName = Resident.LastName,
                    EmailAddress = Resident.PrimaryEmailAddress,
                    StreetAddress = Address.AddressId == -1 ? "" : Address.AddressLine1,
                    StreetAddress2 = Address.AddressId == -1 ? "" : Address.AddressLine2,
                    City = Address.AddressId == -1 ? "" : Address.City,
                    StateProvinceId = Address.AddressId == -1 ? 1 : Address.StateProvinceId,
                    PostalCode = Address.AddressId == -1 ? "" : Address.PostalCode,
                    PhoneNumber = String.IsNullOrEmpty(PhoneNumber) ? "" : PhoneNumber,
                    PaymentAmount = BuildPaymentAmount(payment),
                    ConvenienceFeeAmount = ConvenienceFee,
                    CreditCardHolderName = payment.CreditCardHolderName,
                    CreditCardAccountNumber = payment.CreditCardAccountNumber,
                    CreditCardExpirationMonth = payment.CreditCardExpirationMonth,
                    CreditCardExpirationYear = payment.CreditCardExpirationYear,
                    BankAccountNumber = payment.BankAccountNumber,
                    BankRoutingNumber = payment.BankRoutingNumber,
                    BankAccountTypeId = 1,
                    PaymentId = payment.PaymentId,
                    PaymentStatusId = payment.PaymentStatus.HasValue ? (int) payment.PaymentStatus.Value : 1,
                    ResponseResult = responseResult,
                    ResponseMessage = responseMessage,
                    TransactionDate = payment.TransactionDateTime,
                    PropertyId = Property.PropertyId.Value,
                    CCAchSettledFlag = SetCCAchSettlement
                };

            Transaction = new Transaction(Set(Transaction));
            payment.TransactionId = Transaction.InternalTransactionId;
            Payment.Set(payment);

            //cakel:BUGID00213 ACH INSERT - This will insert record into ACH DB and updated Transaction record (Single Payment Record) if False
            var property = EfxFramework.Property.GetPropertyByRenterId(Resident.RenterId);
            var CCResponseResult = Transaction.ResponseResult;
            //cakel: BUGID00213 rev2
            if (payment.PaymentTypeId == 1)
            {
                if (property.GroupSettlementPaymentFlag == false)
                {

                    //cakel: BUGID00213 - only insert record if approved
                    if ((int)payment.PaymentStatus == 1 && CCResponseResult == 0)
                    {
                        //negative amount added for credit to property - Negative amount comes from SP
                        //BUGID00213***Confirm amount - review with mark
                        decimal PropertyPaymentAmount = (Convert.ToDecimal(payment.RentAmount)) * -1;
                        var ACH_Validate =
                        EfxFramework.PaymentMethods.Ach.AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword, EfxSettings.CcSettlementAchId, PropertyPaymentAmount, property.CCDepositRoutingNumber, property.CCDepositAccountNumber, "Checking",
                             Transaction.FirstName, Transaction.LastName, property.StreetAddress, "", property.City, property.DisplayStateProvince, property.PostalCode, "");

                        //cakel: Need to Add code for response from ACH DB - This will send email to manager about the ACH insert failure.
                        var ACHDB_Result = ACH_Validate.Result;
                        if (ACHDB_Result != 0)
                        {
                            string _result = ACH_Validate.Result.ToString();
                            SendAchErrorEmail(_result, property.PropertyName, Transaction.FirstName, Transaction.LastName, Transaction.TransactionDate.ToString(), Transaction.InternalTransactionId);
                        }

                        //cakel: TASKID00394 rev2 - This will update CC transactions with ACH trans id for properties
                        //who do not group settle
                        var ACHTransID = ACH_Validate.TransactionId;


                        //cakel: BUGID00213 - Updates Transaction table
                        //cakel: TASKID00394 rev2
                        UpdateCCSettledFlagByTransaction(Transaction.InternalTransactionId, ACHTransID);

                       
                    }
                }
            }

        }


        //cakel: TASKID0394 - Rev2
        private static void UpdateCCSettledFlagByTransaction(string InternalTransactionId, string ACHTransID)
        {
            string conString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand com = new SqlCommand("usp_Payment_UpdateCCSettledFlagByTransaction_v2", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ID", SqlDbType.NVarChar, 10).Value = InternalTransactionId;
            com.Parameters.Add("@AchTransID", SqlDbType.NVarChar, 200).Value = ACHTransID;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }



        //cakel: BUGID00213 - Sends Email to Support when ACH DB insert fails 
        private static void SendAchErrorEmail(string Transtatus, string propertyName, string Firstname, string Lastname, string transDate, string transID)
        {
            string MailBody = "<p>An error occured when trying to insert record into ACH DB for submission.  Please check the properties Credit Card Account on file.</p><br />";
            MailBody += "Status: " + Transtatus + "<br />";
            MailBody += "Property Name: " + propertyName + "<br />";
            MailBody += "Payer First Name: " + Firstname + "<br />";
            MailBody += "Payer Last Name: " + Lastname + "<br />";
            MailBody += "Transaction Date: " + transDate + "<br />";
            MailBody += "Transaction ID: " + transID + "<br />";

            MailMessage ACH_ErrorMail = new MailMessage();
            ACH_ErrorMail.Subject = "ACH DB Insert Error";
            ACH_ErrorMail.Body = MailBody;
            ACH_ErrorMail.From = new MailAddress("support@rentpaidonline.com", "RPO Support");
            ACH_ErrorMail.To.Add(new MailAddress(EfxSettings.SupportEmail, "RPO Support Admin"));
            ACH_ErrorMail.IsBodyHtml = true;
            SmtpClient RpoSmtp = new SmtpClient();

            //Salcedo - 4/3/2015 - add support for dynamic host (mainly for testing)
            RpoSmtp.Host = EfxSettings.SmtpHost;

            //Salcedo - 7/6/2016 - capture/log error if any
            try
            {
                RpoSmtp.Send(ACH_ErrorMail);
            }
            catch (Exception ex)
            {
                var log = new RPO.ActivityLog();
                log.WriteLog("Transaction.SendAchErrorEmail", "Unable to send email. Error: " + ex.ToString() + "; Recipients: " + 
                    EfxSettings.SupportEmail + "; Subject: " + ACH_ErrorMail.Subject + "; Body: " + ACH_ErrorMail.Body
                    , 1, "", false);
            }

        }


        


        private static Address GetRenterAddress(Renter renter)
        {
            var BillingAddress = Address.GetAddressByRenterIdAndAddressType(renter.RenterId, (int)TypeOfAddress.Billing);
            if (BillingAddress.AddressId > 0)
                return BillingAddress;

            var AddressId = 0;

            if (String.IsNullOrEmpty(renter.StreetAddress) || String.IsNullOrEmpty(renter.City) || !renter.StateProvinceId.HasValue || String.IsNullOrEmpty(renter.PostalCode))
                AddressId = -1;

            return new Address
            {
                AddressId = AddressId,
                AddressTypeId = (int)TypeOfAddress.Primary,
                AddressLine1 = renter.StreetAddress,
                AddressLine2 = renter.Unit,
                City = renter.City,
                StateProvinceId = renter.StateProvinceId.HasValue ? renter.StateProvinceId.Value : 1,
                PostalCode = renter.PostalCode
            };
        }

        private static decimal BuildPaymentAmount(Payment payment)
        {
            var PaymentAmount = payment.RentAmount.HasValue ? payment.RentAmount.Value : 0.00M;
            PaymentAmount += payment.CharityAmount.HasValue ? payment.CharityAmount.Value : 0.00M;
            PaymentAmount += payment.OtherAmount1.HasValue ? payment.OtherAmount1.Value : 0.00M;
            PaymentAmount += payment.OtherAmount3.HasValue ? payment.OtherAmount3.Value : 0.00M;
            PaymentAmount += payment.OtherAmount4.HasValue ? payment.OtherAmount4.Value : 0.00M;
            PaymentAmount += payment.OtherAmount5.HasValue ? payment.OtherAmount5.Value : 0.00M;

            if (!String.IsNullOrEmpty(payment.OtherDescription2) && payment.OtherDescription2.ToLower() == "convenience fee")
                return PaymentAmount;

            PaymentAmount += payment.OtherAmount2.HasValue ? payment.OtherAmount2.Value : 0.00M;

            return PaymentAmount;
        }

        public static Transaction GetTransactionByInternalTransactionId(string internalTransactionId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new Transaction(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_Transaction_GetTransactionByInternalTransactionId", new SqlParameter("@InternalTransactionId", internalTransactionId)));
        }

        public static Transaction GetTransactionByExternalTransactionId(string externalTransactionId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new Transaction(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_Transaction_GetTransactionByExternalTransactionId", new SqlParameter("@ExternalTransactionId", externalTransactionId)));
        }

        public static string GetNewInternalTransactionId()
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<string>(EfxSettings.ConnectionString, "dbo.usp_Transaction_GetNewInternalTransactionId");
        }

        //cakel: BUGID00213 - Transaction --  add bool "CCAchSettledFlag"
        public static int Set(int transactionId, string internalTransactionId, string externalTransactionId, int paymentTypeId, string firstName, string lastName, 
            string streetAddress, string streetAddress2, string city, int stateProvinceId, string postalCode, string phoneNumber, string emailAddress, string memo, 
            decimal paymentAmount, decimal? convenienceFeeAmount, string creditCardHolderName, string creditCardAccountNumber, int? creditCardExpirationMonth, int? 
            creditCardExpirationYear, string bankAccountNumber, string bankRoutingNumber, int? bankAccountTypeId, int? paymentId, int paymentStatusId, int?
            responseResult, string responseMessage, DateTime transactionDate, int propertyId, bool CCAchSettledFlag = false)
        {
            if (transactionId < 1 || String.IsNullOrEmpty(internalTransactionId))
                //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
                //cakel: 00436 - Update SP to check if Internal ID exists and if it does generate new one.  This will ensure we pass a new id
                //internalTransactionId = NewDataAccess.ExecuteScalar<string>(Settings.ConnectionString, "dbo.usp_Transaction_GetNewInternalTransactionId");
                internalTransactionId = NewDataAccess.ExecuteScalar<string>(EfxSettings.ConnectionString, "dbo.usp_Transaction_GetNewInternalTransactionId_v2");

            try
            {
                //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
                var Result = NewDataAccess.ExecuteScalar<int>(
                    EfxSettings.ConnectionString,
                    //cakel: BUGID00213 - added new SP
                    //"dbo.usp_Transaction_SetSingleObject_v2",
                    //cakel: BUGID00278 - This will improve report Speed
                    "dbo.usp_Transaction_SetSingleObject_v3",
                    new[]
                        {
                            new SqlParameter("@TransactionId", transactionId),
                            new SqlParameter("@InternalTransactionId", internalTransactionId),
                            new SqlParameter("@ExternalTransactionId", externalTransactionId),
                            new SqlParameter("@PaymentTypeId", paymentTypeId),
                            new SqlParameter("@FirstName", firstName),
                            new SqlParameter("@LastName", lastName),
                            new SqlParameter("@StreetAddress", streetAddress),
                            new SqlParameter("@StreetAddress2", streetAddress2),
                            new SqlParameter("@City", city),
                            new SqlParameter("@StateProvinceId", stateProvinceId),
                            new SqlParameter("@PostalCode", postalCode),
                            new SqlParameter("@PhoneNumber", phoneNumber),
                            new SqlParameter("@EmailAddress", emailAddress),
                            new SqlParameter("@Memo", memo),
                            new SqlParameter("@PaymentAmount", paymentAmount),
                            new SqlParameter("@ConvenienceFeeAmount", convenienceFeeAmount),
                            new SqlParameter("@CreditCardHolderName", creditCardHolderName),
                            new SqlParameter("@CreditCardAccountNumber", creditCardAccountNumber),
                            new SqlParameter("@CreditCardExpirationMonth", creditCardExpirationMonth),
                            new SqlParameter("@CreditCardExpirationYear", creditCardExpirationYear),
                            new SqlParameter("@BankAccountNumber", bankAccountNumber),
                            new SqlParameter("@BankRoutingNumber", bankRoutingNumber),
                            new SqlParameter("@BankAccountTypeId", bankAccountTypeId),
                            new SqlParameter("@PaymentId", paymentId),
                            new SqlParameter("@PaymentStatusId", paymentStatusId),
                            new SqlParameter("@ResponseResult", responseResult),
                            new SqlParameter("@ResponseMessage", responseMessage),
                            new SqlParameter("@TransactionDate", transactionDate),
                            new SqlParameter("@PropertyId", propertyId),
                            //cakel: BUGID00213 - Adeded New Parameter
                            new SqlParameter("@CCAchSettledFlag", CCAchSettledFlag)
                        });
                
                return Result;
            }
            catch (SqlException Ex)
            {
                if (Ex.Number == 2601 || Ex.Number == 2627)
                {
                    return Set(transactionId, internalTransactionId, externalTransactionId, paymentTypeId, firstName, lastName, streetAddress, streetAddress2, city, stateProvinceId, postalCode,
                               phoneNumber, emailAddress, memo, paymentAmount, convenienceFeeAmount, creditCardHolderName, creditCardAccountNumber, creditCardExpirationMonth, creditCardExpirationYear,
                               bankAccountNumber, bankRoutingNumber, bankAccountTypeId, paymentId, paymentStatusId, responseResult, responseMessage, transactionDate, propertyId, CCAchSettledFlag);
                }
                else
                {
                    //Salcedo - 7/1/2014 - hack to try and solve the timeout problem
                    //(I would change the timeout, but I can't easily because we're using MB2x's dll - I would need to switch out the code)
                    if (Ex.Message == "The wait operation timed out")
                    {
                        //Wait an indefinte amount of time, between 1 and 30 seconds and try again ...
                        System.Threading.Thread.Sleep(RandomNumber(1, 30) * 1000);
                        try
                        {
                            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
                            var Result = NewDataAccess.ExecuteScalar<int>(
                                EfxSettings.ConnectionString,
                                //"dbo.usp_Transaction_SetSingleObject",
                                //cakel: BUGID00278 - This will improve report Speed
                                "dbo.usp_Transaction_SetSingleObject_v3",
                                new[]
                                    {
                                        new SqlParameter("@TransactionId", transactionId),
                                        new SqlParameter("@InternalTransactionId", internalTransactionId),
                                        new SqlParameter("@ExternalTransactionId", externalTransactionId),
                                        new SqlParameter("@PaymentTypeId", paymentTypeId),
                                        new SqlParameter("@FirstName", firstName),
                                        new SqlParameter("@LastName", lastName),
                                        new SqlParameter("@StreetAddress", streetAddress),
                                        new SqlParameter("@StreetAddress2", streetAddress2),
                                        new SqlParameter("@City", city),
                                        new SqlParameter("@StateProvinceId", stateProvinceId),
                                        new SqlParameter("@PostalCode", postalCode),
                                        new SqlParameter("@PhoneNumber", phoneNumber),
                                        new SqlParameter("@EmailAddress", emailAddress),
                                        new SqlParameter("@Memo", memo),
                                        new SqlParameter("@PaymentAmount", paymentAmount),
                                        new SqlParameter("@ConvenienceFeeAmount", convenienceFeeAmount),
                                        new SqlParameter("@CreditCardHolderName", creditCardHolderName),
                                        new SqlParameter("@CreditCardAccountNumber", creditCardAccountNumber),
                                        new SqlParameter("@CreditCardExpirationMonth", creditCardExpirationMonth),
                                        new SqlParameter("@CreditCardExpirationYear", creditCardExpirationYear),
                                        new SqlParameter("@BankAccountNumber", bankAccountNumber),
                                        new SqlParameter("@BankRoutingNumber", bankRoutingNumber),
                                        new SqlParameter("@BankAccountTypeId", bankAccountTypeId),
                                        new SqlParameter("@PaymentId", paymentId),
                                        new SqlParameter("@PaymentStatusId", paymentStatusId),
                                        new SqlParameter("@ResponseResult", responseResult),
                                        new SqlParameter("@ResponseMessage", responseMessage),
                                        new SqlParameter("@TransactionDate", transactionDate),
                                        new SqlParameter("@PropertyId", propertyId),
                                        new SqlParameter("@CCAchSettledFlag", CCAchSettledFlag)
                                    });

                            return Result;
                        }
                        catch (SqlException Ex2)
                        {
                            if (Ex.Number == 2601 || Ex.Number == 2627)
                            {
                                return Set(transactionId, internalTransactionId, externalTransactionId, paymentTypeId, firstName, lastName, streetAddress, streetAddress2, city, stateProvinceId, postalCode,
                                           phoneNumber, emailAddress, memo, paymentAmount, convenienceFeeAmount, creditCardHolderName, creditCardAccountNumber, creditCardExpirationMonth, creditCardExpirationYear,
                                           bankAccountNumber, bankRoutingNumber, bankAccountTypeId, paymentId, paymentStatusId, responseResult, responseMessage, transactionDate, propertyId);
                            }
                            else
                            {
                                EfxFramework.Logging.ExceptionLogger.LogException(Ex2);
                                EfxFramework.Logging.ExceptionLogger.LogException(new Exception("Error occurred in Transaction.Set (Ex2). transactionid=" + transactionId.ToString()
                                    + ", internalTransactionId=" + internalTransactionId.ToString()
                                    + ", paymentId=" + paymentId.ToString()
                                    + ", propertyId=" + propertyId.ToString()));
                            }
                        }
                    }
                    else
                    {
                        EfxFramework.Logging.ExceptionLogger.LogException(Ex);
                        EfxFramework.Logging.ExceptionLogger.LogException(new Exception("Error occurred in Transaction.Set. transactionid=" + transactionId.ToString()
                            + ", internalTransactionId=" + internalTransactionId.ToString()
                            + ", paymentId=" + paymentId.ToString()
                            + ", propertyId=" + propertyId.ToString()));
                    }
                }
                return 0;
            }
        }

        private static int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        public static int SetRPOTransaction(Transaction rpoTransaction, string parentInternalId)
        {
            try
            {
                //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
                var Result = NewDataAccess.ExecuteScalar<int>(
                    EfxSettings.ConnectionString,
                    //cakel: BUGID00213 - Added new SP
                    //"dbo.usp_Transaction_SetSingleRPOObject_V2",
                    //cakel: BUGID00278 - This will improve report Speed
                    "dbo.usp_Transaction_SetSingleRPOObject_V3",
                    new[]
                        {
                            new SqlParameter("@TransactionId", rpoTransaction.TransactionId),
                            new SqlParameter("@InternalTransactionId", rpoTransaction.InternalTransactionId),
                            new SqlParameter("@ExternalTransactionId", rpoTransaction.ExternalTransactionId),
                            new SqlParameter("@PaymentTypeId", rpoTransaction.PaymentTypeId),
                            new SqlParameter("@FirstName", rpoTransaction.FirstName),
                            new SqlParameter("@LastName", rpoTransaction.LastName),
                            new SqlParameter("@StreetAddress", rpoTransaction.StreetAddress),
                            new SqlParameter("@StreetAddress2", rpoTransaction.StreetAddress2),
                            new SqlParameter("@City", rpoTransaction.City),
                            new SqlParameter("@StateProvinceId", rpoTransaction.StateProvinceId),
                            new SqlParameter("@PostalCode", rpoTransaction.PostalCode),
                            new SqlParameter("@PhoneNumber", rpoTransaction.PhoneNumber),
                            new SqlParameter("@EmailAddress", rpoTransaction.EmailAddress),
                            new SqlParameter("@Memo", rpoTransaction.Memo),
                            new SqlParameter("@PaymentAmount", rpoTransaction.PaymentAmount),
                            new SqlParameter("@ConvenienceFeeAmount", rpoTransaction.ConvenienceFeeAmount),
                            new SqlParameter("@CreditCardHolderName", rpoTransaction.CreditCardHolderName),
                            new SqlParameter("@CreditCardAccountNumber", rpoTransaction.CreditCardAccountNumber),
                            new SqlParameter("@CreditCardExpirationMonth", rpoTransaction.CreditCardExpirationMonth),
                            new SqlParameter("@CreditCardExpirationYear", rpoTransaction.CreditCardExpirationYear),
                            new SqlParameter("@BankAccountNumber", rpoTransaction.BankAccountNumber),
                            new SqlParameter("@BankRoutingNumber", rpoTransaction.BankRoutingNumber),
                            new SqlParameter("@BankAccountTypeId", rpoTransaction.BankAccountTypeId),
                            new SqlParameter("@PaymentId", rpoTransaction.PaymentId),
                            new SqlParameter("@PaymentStatusId", rpoTransaction.PaymentStatusId),
                            new SqlParameter("@ResponseResult", rpoTransaction.ResponseResult),
                            new SqlParameter("@ResponseMessage", rpoTransaction.ResponseMessage),
                            new SqlParameter("@TransactionDate", rpoTransaction.TransactionDate),
                            new SqlParameter("@PropertyId", rpoTransaction.PropertyId),
                            new SqlParameter("@ParentInternalTransId", parentInternalId),
                            new SqlParameter("CCAchSettledFlag",rpoTransaction.CCAchSettledFlag)
                        });

                return Result;
            }
            //catch (SqlException Ex)
            catch
            {
                //if (Ex.Number == 2601 || Ex.Number == 2627)
                //{
                //    return Set(transactionId, internalTransactionId, externalTransactionId, paymentTypeId, firstName, lastName, streetAddress, streetAddress2, city, stateProvinceId, postalCode,
                //               phoneNumber, emailAddress, memo, paymentAmount, convenienceFeeAmount, creditCardHolderName, creditCardAccountNumber, creditCardExpirationMonth, creditCardExpirationYear,
                //               bankAccountNumber, bankRoutingNumber, bankAccountTypeId, paymentId, paymentStatusId, responseResult, responseMessage, transactionDate, propertyId);
                //}

                return 0;
            }
        }

        public static int Set(Transaction t)
        {
	        return Set(
		        t.TransactionId,
                t.InternalTransactionId,
		        t.ExternalTransactionId,
		        t.PaymentTypeId,
		        t.FirstName,
		        t.LastName,
		        t.StreetAddress,
		        t.StreetAddress2,
		        t.City,
		        t.StateProvinceId,
		        t.PostalCode,
		        t.PhoneNumber,
		        t.EmailAddress,
		        t.Memo,
		        t.PaymentAmount,
		        t.ConvenienceFeeAmount,
		        t.CreditCardHolderName,
		        t.CreditCardAccountNumber,
		        t.CreditCardExpirationMonth,
		        t.CreditCardExpirationYear,
		        t.BankAccountNumber,
		        t.BankRoutingNumber,
		        t.BankAccountTypeId,
		        t.PaymentId,
		        t.PaymentStatusId,
		        t.ResponseResult,
		        t.ResponseMessage,
		        t.TransactionDate,
		        t.PropertyId,
                //cakel: BUGID00213
                t.CCAchSettledFlag
	        );
        }

        public Transaction(int? transactionId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_Transaction_GetSingleObject", new [] { new SqlParameter("@TransactionId", transactionId) });

            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        TransactionId = (int)ResultSet[0];
		        InternalTransactionId = ResultSet[1] as string;
		        ExternalTransactionId = ResultSet[2] as string;
		        PaymentTypeId = (int)ResultSet[3];
		        FirstName = ResultSet[4] as string;
		        LastName = ResultSet[5] as string;
		        StreetAddress = ResultSet[6] as string;
		        StreetAddress2 = ResultSet[7] as string;
		        City = ResultSet[8] as string;
		        StateProvinceId = (int)ResultSet[9];
		        PostalCode = ResultSet[10] as string;
		        PhoneNumber = ResultSet[11] as string;
		        EmailAddress = ResultSet[12] as string;
		        Memo = ResultSet[13] as string;
		        PaymentAmount = (decimal)ResultSet[14];
		        ConvenienceFeeAmount = ResultSet[15] as decimal?;
		        CreditCardHolderName = ResultSet[16] as string;
		        CreditCardAccountNumber = ResultSet[17] as string;
		        CreditCardExpirationMonth = ResultSet[18] as int?;
		        CreditCardExpirationYear = ResultSet[19] as int?;
		        BankAccountNumber = ResultSet[20] as string;
		        BankRoutingNumber = ResultSet[21] as string;
		        BankAccountTypeId = ResultSet[22] as int?;
		        PaymentId = ResultSet[23] as int?;
		        PaymentStatusId = (int)ResultSet[24];
		        ResponseResult = ResultSet[25] as int?;
		        ResponseMessage = ResultSet[26] as string;
		        TransactionDate = (DateTime)ResultSet[27];
		        PropertyId = (int)ResultSet[28];
            }
        }

		public Transaction()
		{
			InitializeObject();
		}

	}
}
