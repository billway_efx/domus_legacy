﻿using EfxFramework.Security.Authentication;

namespace EfxFramework
{
	public abstract class BaseUser : BaseEntity
	{
		//Private Member Variables
	    private string _PrimaryEmailAddress;

		//Public Properties
        public abstract int? UserId { get; }
		public string UserName { get; set; }
		public byte[] PasswordHash { get; set; }
		public byte[] Salt { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
	    public bool IsValid { get { return IsValidUser(); } }
	    public string PrimaryEmailAddress
        {
            get
            {
                return _PrimaryEmailAddress;
            }
            set
            {
                _PrimaryEmailAddress = value;
                UserName = value;
            }
        }
	    public virtual string DisplayName
	    {
            get { return string.Format("{0} {1}", FirstName, LastName); }
	    }

	    protected virtual bool IsValidUser()
	    {
            return false;
	    }

		public override string ToString()
		{
			return string.Format("{0} - {1} {2}", PrimaryEmailAddress, FirstName, LastName);
		}

	    public abstract BaseUser GetUserById(int userId);
	    public abstract BaseUser GetUserByEmailAddress(string emailAddress);
	    public abstract int SetUser(BaseUser user, string authenticatedUser);

	    //Public Methods
		public void SetPassword(string newPassword)
		{
			//Generate a new salt
			Salt = AuthenticationTools.GenerateSalt();

			//Hash the password
			PasswordHash = AuthenticationTools.HashPassword(newPassword, Salt);
		}

		public string ResetPassword()
		{
			//Generate a new random password
			var TempPassword = AuthenticationTools.GenerateRandomPassword();
	
			//Generate a new salt and set it
			Salt = AuthenticationTools.GenerateSalt();

			//Hash the new password and set it
			PasswordHash = AuthenticationTools.HashPassword(TempPassword, Salt);

			//Return the new password
			return TempPassword;
		}

		public bool Authenticate(string password)
		{
			return AuthenticationTools.ValidatePasswordHash(password, PasswordHash, Salt);
		}
	}
}