﻿using Mb2x.ExtensionMethods;
using System;

namespace EfxFramework
{
    public class PhoneNumber
    {
        private string _FormatString;
        private string _AreaCode;
        private string _CityCode;
        private string _SubscriberNumber;

        public string AreaCode
        {
            get { return _AreaCode; } 
            set { _AreaCode = String.IsNullOrEmpty(value) || value.Length != 3 ? null : value; }
        }

        public string CityCode
        {
            get { return _CityCode; }
            set { _CityCode = String.IsNullOrEmpty(value) || value.Length != 3 ? null : value; }
        }

        public string SubscriberNumber
        {
            get { return _SubscriberNumber; }
            set { _SubscriberNumber = String.IsNullOrEmpty(value) || value.Length != 4 ? null : value; }
        }

        public string FormatString
        {
            get { return String.IsNullOrEmpty(_FormatString) ?  "{0}{1}{2}" : _FormatString; }
            set { _FormatString = value.TrimNull(); }
        }

        public PhoneNumber()
        {
        }

        public PhoneNumber(string phoneNumber)
        {
            var Result = RemoveFormatting(phoneNumber);

            if (!IsValidPhoneNumber(Result))
                return;

            SetValues(Result);
        }

        public PhoneNumber(string phoneNumber, string formatString)
        {
            var Result = RemoveFormatting(phoneNumber);

            if (!IsValidPhoneNumber(Result))
                return;

            SetValues(Result);
            FormatString = formatString;
        }

        public bool IsValidPhoneNumber()
        {
            if (String.IsNullOrEmpty(AreaCode) || AreaCode.Length != 3 || !IsInteger(AreaCode))
                return false;

            if (String.IsNullOrEmpty(CityCode) || CityCode.Length != 3 || !IsInteger(CityCode))
                return false;

            if (String.IsNullOrEmpty(SubscriberNumber) || SubscriberNumber.Length != 4 || !IsInteger(SubscriberNumber))
                return false;

            return true;
        }

        public static bool IsValidPhoneNumber(string phoneNumber)
        {
            var Result = RemoveFormatting(phoneNumber);
            long Number;

            return Result.Length == 10 && Int64.TryParse(Result, out Number);
        }

        public static string RemoveFormatting(string phoneNumber)
        {
            var Result = phoneNumber.ExtractDigits();
            return String.IsNullOrEmpty(Result) ? String.Empty : Result;
        }

        public override string ToString()
        {
            if (String.IsNullOrEmpty(AreaCode) || String.IsNullOrEmpty(CityCode) || String.IsNullOrEmpty(SubscriberNumber))
                return String.Empty;

            return String.Format(FormatString, AreaCode, CityCode, SubscriberNumber);
        }

        public string ToString(string formatString)
        {
            if (String.IsNullOrEmpty(AreaCode) || String.IsNullOrEmpty(CityCode) || String.IsNullOrEmpty(SubscriberNumber))
                return String.Empty;

            return String.Format(formatString, AreaCode, CityCode, SubscriberNumber);
        }

        public string ToString(bool useFormatting)
        {
            return useFormatting ? ToString() : ToString("{0}{1}{2}");
        }

        private static bool IsInteger(string input)
        {
            int Result;
            return Int32.TryParse(input, out Result);
        }

        private void SetValues(string phoneDigits)
        {
            AreaCode = phoneDigits.Substring(0, 3);
            CityCode = phoneDigits.Substring(3, 3);
            SubscriberNumber = phoneDigits.Substring(6, 4);
        }
    }
}
