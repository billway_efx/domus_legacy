using EfxFramework.Pms.Yardi;
using EfxFramework.Security.Authentication;
using Mb2x.Data;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace EfxFramework
{
    public class Renter : BaseUser
    {
        private TelephoneNumber _mainPhoneNumber;
        private TelephoneNumber _mobilePhoneNumber;
        private TelephoneNumber _faxNumber;
        private TelephoneNumber _smsPaymentPhoneNumber;

        //Public Properties
        public override int? UserId => RenterId;

        public int RenterId { get; set; }

        public int? PayerId { get; set; }

        public string Prefix { get; set; }

        public string MiddleName { get; set; }

        public string Suffix { get; set; }

        public string MainPhoneNumber
        {
            get
            {
                return _mainPhoneNumber == null ? string.Empty : _mainPhoneNumber.Digits;
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _mainPhoneNumber = null;
                else
                {
                    if (_mainPhoneNumber == null)
                        _mainPhoneNumber = new TelephoneNumber(value);
                    else
                        _mainPhoneNumber.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }

        public string MobilePhoneNumber
        {
            get
            {
                return _mobilePhoneNumber == null ? string.Empty : _mobilePhoneNumber.Digits;
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _mobilePhoneNumber = null;
                else
                {
                    if (_mobilePhoneNumber == null)
                        _mobilePhoneNumber = new TelephoneNumber(value);
                    else
                        _mobilePhoneNumber.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }

        public string FaxNumber
        {
            get
            {
                return _faxNumber == null ? string.Empty : _faxNumber.Digits;
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _faxNumber = null;
                else
                {
                    if (_faxNumber == null)
                        _faxNumber = new TelephoneNumber(value);
                    else
                        _faxNumber.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }

        public string AlternateEmailAddress1 { get; set; }

        public string AlternateEmailAddress2 { get; set; }

        public string StreetAddress { get; set; }

        public string Unit { get; set; }

        public string City { get; set; }

        public int? StateProvinceId { get; set; }

        public string PostalCode { get; set; }

        public bool? EmailNotifications { get; set; }

        public bool? TextNotifications { get; set; }

        public bool? IsParticipating { get; set; }

        public string AccountCodePin { get; set; }

        public bool IsCreditCardTestRenter { get; set; }

        public bool IsCreditCardApproved { get; set; }

        public string SmsPaymentPhoneNumber
        {
            get => _smsPaymentPhoneNumber == null ? string.Empty : _smsPaymentPhoneNumber.Digits;

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _smsPaymentPhoneNumber = null;
                else
                {
                    if (_smsPaymentPhoneNumber == null)
                        _smsPaymentPhoneNumber = new TelephoneNumber(value);
                    else
                        _smsPaymentPhoneNumber.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }

        public int? SmsPaymentTypeId { get; set; }

        public int? SmsPaymentCarrierId { get; set; }

        public string PmsId { get; set; }

        public int? PmsTypeId { get; set; }

        public bool IsActive { get; set; }

        public int? SmsReminderDayOfMonth { get; set; }

        public string PmsPropertyId { get; set; }

        public long PublicRenterId { get; private set; }

        public bool UserModified { get; private set; }

        public int AcceptedPaymentTypeId { get; set; }

        public string RentReporterId { get; set; }

        public bool HasRegistered { get; set; }

        public string DisplayAddress
        {
            get
            {
                var prov = StateProvince.AK;
                if (StateProvinceId.HasValue)
                    prov = (StateProvince)Enum.Parse(typeof(StateProvince), StateProvinceId.Value.ToString(CultureInfo.InvariantCulture));

                return $"{StreetAddress} {Unit} {City} {prov} {PostalCode}";
            }
        }

        public bool IsRentLate
        {
            get
            {
                var lease = Lease.GetRenterLeaseByRenterId(RenterId);
                if (lease.LeaseId < 1)
                    return false;

                if (!lease.CurrentBalanceDue.HasValue || lease.CurrentBalanceDue.Value + lease.TotalFees < 0.01M)
                    return false;

                return !(lease.PaymentDueDate >= DateTime.UtcNow);
            }
        }

        public Renter()
        {
            InitializeObject();
        }

        public Renter(int? renterId)
        {
            var resultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_Renter_GetSingleObject",
                new[] {new SqlParameter("@RenterId", renterId)});
            if (resultSet == null)
            {
                InitializeObject();
            }
            else
            {
                RenterId = (int)resultSet[0];
                PayerId = resultSet[1] as int?;
                Prefix = resultSet[2] as string;
                FirstName = resultSet[3] as string;
                MiddleName = resultSet[4] as string;
                LastName = resultSet[5] as string;
                Suffix = resultSet[6] as string;
                MainPhoneNumber = (resultSet[7] as string);
                MobilePhoneNumber = (resultSet[8] as string);
                FaxNumber = resultSet[9] as string;
                PrimaryEmailAddress = resultSet[10] as string;
                AlternateEmailAddress1 = resultSet[11] as string;
                AlternateEmailAddress2 = resultSet[12] as string;
                StreetAddress = resultSet[13] as string;
                Unit = resultSet[14] as string;
                City = resultSet[15] as string;
                StateProvinceId = resultSet[16] as int?;
                PostalCode = resultSet[17] as string;
                EmailNotifications = resultSet[18] as bool?;
                TextNotifications = resultSet[19] as bool?;
                IsParticipating = resultSet[20] as bool?;
                PasswordHash = resultSet[21] as byte[];
                Salt = resultSet[22] as byte[];
                AccountCodePin = resultSet[23] as string;
                IsCreditCardTestRenter = (bool)resultSet[24];
                IsCreditCardApproved = (bool)resultSet[25];
                SmsPaymentPhoneNumber = resultSet[26] as string;
                SmsPaymentTypeId = resultSet[27] as int?;
                SmsPaymentCarrierId = resultSet[28] as int?;
                PmsId = resultSet[29] as string;
                PmsTypeId = resultSet[30] as int?;
                IsActive = (bool)resultSet[31];
                SmsReminderDayOfMonth = resultSet[32] as int?;
                PmsPropertyId = resultSet[33] as string;
                PublicRenterId = Convert.ToInt64(resultSet[34]);
                UserModified = (bool)resultSet[35];
                AcceptedPaymentTypeId = (int)resultSet[36];
                RentReporterId = resultSet[37] as string;
                HasRegistered = (bool)resultSet[38];
            }
        }

        public Renter(string primaryEmailAddress)
            : this(NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "dbo.usp_Renter_GetRenterByEmailAddress",
                new SqlParameter("@PrimaryEmailAddress", primaryEmailAddress)) as int?)
        { }

        protected override bool IsValidUser()
        {
            return RenterId > 0;
        }

        public override BaseUser GetUserById(int userId)
        {
            return new Renter(userId);
        }

        public override BaseUser GetUserByEmailAddress(string emailAddress)
        {
            return new Renter(emailAddress);
        }

        public override int SetUser(BaseUser user, string authenticatedUser)
        {
            return Set((user as Renter), authenticatedUser);
        }

        public static Renter PromoteApplicantToRenter(int applicantId, string authenticatedUser, int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            var result = NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_ApplicantRenter_PromoteApplicantToRenter", new[]
                {
                    new SqlParameter("@ApplicantId", applicantId),
                    new SqlParameter("@AuthenticatedUser", authenticatedUser)
                });

            return new Renter(result);
        }

        public static void AddNoteForRenter(int renterNoteId, int renterId, string note, string editor, int roleTypeId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_RenterNote_SetSingleObject", new[]
                {
                    new SqlParameter("@RenterNoteId", renterNoteId),
                    new SqlParameter("@RenterId", renterId),
                    new SqlParameter("@Note", note),
                    new SqlParameter("@LastEditedDate", DateTime.UtcNow),
                    new SqlParameter("@LastEditedBy", editor),
                    new SqlParameter("@RoleTypeId", roleTypeId)
                });
        }

        public static int? GetRenterIdByPublicRenterId(long publicRenterId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_Renter_GetRenterIdByPublicRenterId", new SqlParameter("@PublicRenterId", publicRenterId));
        }

        public static List<Renter> GetRenterListBySmsReminderDayOfTheMonth(int reminderDay)
        {
            return DataAccess.GetTypedList<Renter>(EfxSettings.ConnectionString, "dbo.usp_Renter_GetRenterBySmsReminderDayOfMonth", new SqlParameter("@SmsReminderDayOfMonth", reminderDay));
        }

        public static void ImportPmsData(List<YardiResidentData> residents)
        {
            var table = BuildRenterLeaseTable();

            foreach (var yardiResident in residents)
            {
                if (string.IsNullOrEmpty(yardiResident.Resident.PmsId)) continue;
                var row = table.NewRow();
                row["PayerId"] = yardiResident.Resident.PayerId;
                row["Prefix"] = yardiResident.Resident.Prefix;
                row["FirstName"] = yardiResident.Resident.FirstName;
                row["MiddleName"] = yardiResident.Resident.MiddleName;
                row["LastName"] = yardiResident.Resident.LastName;
                row["Suffix"] = yardiResident.Resident.Suffix;
                row["MainPhoneNumber"] = yardiResident.Resident.MainPhoneNumber;
                row["MobilePhoneNumber"] = yardiResident.Resident.MobilePhoneNumber;
                row["FaxNumber"] = yardiResident.Resident.FaxNumber;
                row["PrimaryEmailAddress"] = yardiResident.Resident.PrimaryEmailAddress;
                row["AlternateEmailAddress1"] = yardiResident.Resident.AlternateEmailAddress1;
                row["AlternateEmailAddress2"] = yardiResident.Resident.AlternateEmailAddress2;
                row["Photo"] = null;
                row["StreetAddress"] = yardiResident.Resident.StreetAddress;
                row["Unit"] = yardiResident.Resident.Unit;
                row["City"] = yardiResident.Resident.City;
                row["StateProvinceId"] = yardiResident.Resident.StateProvinceId;
                row["PostalCode"] = yardiResident.Resident.PostalCode;
                row["EmailNotifications"] = yardiResident.Resident.EmailNotifications ?? false;
                row["TextNotifications"] = yardiResident.Resident.TextNotifications ?? false;
                row["IsParticipating"] = yardiResident.Resident.IsParticipating ?? false;
                row["PasswordHash"] = yardiResident.Resident.PasswordHash;
                row["Salt"] = yardiResident.Resident.Salt;
                row["AccountCodePin"] = yardiResident.Resident.AccountCodePin;
                row["IsCreditCardTestRenter"] = yardiResident.Resident.IsCreditCardTestRenter;
                row["IsCreditCardApproved"] = yardiResident.Resident.IsCreditCardApproved;
                row["SmsPaymentPhoneNumber"] = null;
                row["SmsPaymentTypeId"] = yardiResident.Resident.SmsPaymentTypeId ?? 1;
                row["SmsPaymentCarrierId"] = yardiResident.Resident.SmsPaymentCarrierId ?? 1;
                row["PmsId"] = yardiResident.Resident.PmsId;
                row["PmsTypeId"] = yardiResident.Resident.PmsTypeId ?? 1;
                row["IsActive"] = yardiResident.Resident.IsActive;
                row["PmsPropertyId"] = yardiResident.Resident.PmsPropertyId;
                row["UnitNumber"] = yardiResident.ResidentLease.UnitNumber;
                row["RentAmount"] = yardiResident.ResidentLease.RentAmount;
                row["RentDueDayOfMonth"] = yardiResident.ResidentLease.RentDueDayOfMonth;
                row["StartDate"] = yardiResident.ResidentLease.StartDate;
                row["EndDate"] = yardiResident.ResidentLease.EndDate;
                row["BeginningBalance"] = yardiResident.ResidentLease.BeginningBalance;
                row["BeginningBalanceDate"] = yardiResident.ResidentLease.BeginningBalanceDate;
                table.Rows.Add(row);
            }

            try
            {
                NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_Renter_ImportPmsData",
                    new SqlParameter
                    {
                        SqlDbType = SqlDbType.Structured, ParameterName = "@RenterTableImport", Value = table
                    });
            }
            catch
            {
                // ReSharper disable LocalizableElement
                Console.WriteLine(@"Duplicate renters found");
                // ReSharper restore LocalizableElement
            }
            finally
            {
                table.Dispose();
            }
        }

        private static DataTable BuildRenterLeaseTable()
        {
            var table = new DataTable();
            table.Columns.Add(new DataColumn("PayerId"));
            table.Columns.Add(new DataColumn("Prefix", typeof(string)));
            table.Columns.Add(new DataColumn("FirstName", typeof(string)));
            table.Columns.Add(new DataColumn("MiddleName", typeof(string)));
            table.Columns.Add(new DataColumn("LastName", typeof(string)));
            table.Columns.Add(new DataColumn("Suffix", typeof(string)));
            table.Columns.Add(new DataColumn("MainPhoneNumber", typeof(string)));
            table.Columns.Add(new DataColumn("MobilePhoneNumber", typeof(string)));
            table.Columns.Add(new DataColumn("FaxNumber", typeof(string)));
            table.Columns.Add(new DataColumn("PrimaryEmailAddress", typeof(string)));
            table.Columns.Add(new DataColumn("AlternateEmailAddress1", typeof(string)));
            table.Columns.Add(new DataColumn("AlternateEmailAddress2", typeof(string)));
            table.Columns.Add(new DataColumn("Photo", typeof(byte[])));
            table.Columns.Add(new DataColumn("StreetAddress", typeof(string)));
            table.Columns.Add(new DataColumn("Unit", typeof(string)));
            table.Columns.Add(new DataColumn("City", typeof(string)));
            table.Columns.Add(new DataColumn("StateProvinceId"));
            table.Columns.Add(new DataColumn("PostalCode", typeof(string)));
            table.Columns.Add(new DataColumn("EmailNotifications", typeof(bool)));
            table.Columns.Add(new DataColumn("TextNotifications", typeof(bool)));
            table.Columns.Add(new DataColumn("IsParticipating", typeof(bool)));
            table.Columns.Add(new DataColumn("PasswordHash", typeof(byte[])));
            table.Columns.Add(new DataColumn("Salt", typeof(byte[])));
            table.Columns.Add(new DataColumn("AccountCodePin", typeof(string)));
            table.Columns.Add(new DataColumn("IsCreditCardTestRenter", typeof(bool)));
            table.Columns.Add(new DataColumn("IsCreditCardApproved", typeof(bool)));
            table.Columns.Add(new DataColumn("SmsPaymentPhoneNumber", typeof(string)));
            table.Columns.Add(new DataColumn("SmsPaymentTypeId", typeof(int)));
            table.Columns.Add(new DataColumn("SmsPaymentCarrierId", typeof(int)));
            table.Columns.Add(new DataColumn("PmsId", typeof(string)));
            table.Columns.Add(new DataColumn("PmsTypeId", typeof(int)));
            table.Columns.Add(new DataColumn("IsActive", typeof(bool)));
            table.Columns.Add(new DataColumn("PmsPropertyId", typeof(string)));
            table.Columns.Add(new DataColumn("UnitNumber", typeof(string)));
            table.Columns.Add(new DataColumn("RentAmount", typeof(decimal)));
            table.Columns.Add(new DataColumn("RentDueDayOfMonth", typeof(int)));
            table.Columns.Add(new DataColumn("StartDate", typeof(DateTime)));
            table.Columns.Add(new DataColumn("EndDate", typeof(DateTime)));
            table.Columns.Add(new DataColumn("BeginningBalance", typeof(decimal)));
            table.Columns.Add(new DataColumn("BeginningBalanceDate", typeof(DateTime)));

            return table;
        }

        public static DataTable GetRenterPastDueByRenterId(int renterId)
        {
            return DataAccess.ExecuteDataTable(EfxSettings.ConnectionString, "dbo.usp_Renter_GetRenterPastDueByRenterId", new SqlParameter("@RenterId", renterId));
        }

        public static decimal GetPercentageOfParticipatingRentersByPropertyId(int propertyId)
        {
            return NewDataAccess.ExecuteScalar<decimal>(EfxSettings.ConnectionString,
                "dbo.usp_Property_GetPercentageOfParticipatingRentersPropertyId",
                new SqlParameter("@PropertyId", propertyId));
        }

        // Salcedo - 2/20/2014 - added PropertyId to function
        public static Renter GetRenterByPmsIdAndPmsTypeId(string pmsId, int pmsTypeId, int propertyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new Renter(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_Renter_GetRenterByPmsIdAndPmsTypeId_V2",
                new[] { new SqlParameter("@PmsId", pmsId), new SqlParameter("@PmsTypeId", pmsTypeId), new SqlParameter("@PropertyId", propertyId) }));
        }

        public static DataAccessResults DeleteRenterByRenterId(int renterId, string authenticatedUser)
        {
            return DataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_Renter_DeleteRenterByRenterId", new[] { new SqlParameter("@RenterId", renterId), new SqlParameter("@AuthenticatedUser", authenticatedUser) });
        }

        /// <summary>
        /// Associates a renter with a property.
        /// </summary>
        public static void AssignRenterToProperty(int propertyId, int renterId)
        {
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString,
                "dbo.usp_PropertyRenter_AssignRenterToProperty",
                new[] {new SqlParameter("@PropertyId", propertyId), new SqlParameter("@RenterId", renterId)});

            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_Renter_AssignPropertyToRenter",
                new[] {new SqlParameter("@PropertyId", propertyId), new SqlParameter("@RenterId", renterId)});
        }

        public static byte[] GetPhoto(int renterId)
        {
            var returnArray = NewDataAccess.ExecuteScalar<byte[]>(
                EfxSettings.ConnectionString,
                "dbo.usp_Renter_GetPhoto",
                new SqlParameter("@RenterId", renterId));

            return returnArray.Length <= 4 ? null : returnArray;
        }
        public static void SetPhoto(int renterId, byte[] photo, string authenticatedUser)
        {
            var standardizedImage = photo.StandardizeImage(300);

            NewDataAccess.ExecuteNoResults(
                EfxSettings.ConnectionString,
                "dbo.usp_Renter_SetPhoto",
                new[]
                    {
                        new SqlParameter("@RenterId", renterId),
                        new SqlParameter("@Photo", standardizedImage),
                        new SqlParameter("@AuthenticatedUser", authenticatedUser)
                    });
        }

        public static List<Renter> GetAllRenterList()
        {
            return DataAccess.GetTypedList<Renter>(
                EfxSettings.ConnectionString,
                "dbo.usp_Renter_GetAll"
                );
        }

        public static int? GetPropertyByRenterId(int renterId)
        {
            return (int?) NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString,
                "dbo.usp_PropertyRenter_GetPropertyByRenterId", new SqlParameter("@Renterid", renterId));
        }

        public static List<Renter> GetRenterListByLeaseId(int leaseId)
        {
            return DataAccess.GetTypedList<Renter>(
                EfxSettings.ConnectionString,
                "dbo.usp_LeaseRenter_GetRenterListByLeaseId",
                new SqlParameter("@LeaseId", leaseId)
                );
        }

        public static List<Renter> GetRenterListByPayerId(int payerId)
        {
            return DataAccess.GetTypedList<Renter>(
                EfxSettings.ConnectionString,
                "dbo.usp_Renter_GetByPayerId",
                new SqlParameter("@PayerId", payerId)
                );
        }

        public static List<Renter> GetRenterListByPropertyId(int propertyId)
        {
            return DataAccess.GetTypedList<Renter>(
                EfxSettings.ConnectionString,
                "dbo.usp_PropertyRenter_GetRenterListByPropertyId",
                new SqlParameter("@PropertyId", propertyId)
                );
        }
        public static List<Renter> GetRenterListByPropertyIdForEmailMarketing(int propertyId, int residentGroup)
        {
            return DataAccess.GetTypedList<Renter>(
                EfxSettings.ConnectionString,
                "dbo.usp_PropertyRenter_GetRenterListByPropertyIdForEmailMarketing",
                new[]
                    {
                        new SqlParameter("@PropertyId", propertyId),
                        new SqlParameter("@ResidentGroup", residentGroup)
                    });
        }
        //CMallory - Task 0079 - Added
        public static List<Renter> GetRenterListByPropertyId_v2(int? propertyId = 0)
        {
            //Return a list of renters for the given property
            return DataAccess.GetTypedList<Renter>(
                EfxSettings.ConnectionString,
                "dbo.usp_PropertyRenter_GetRenterListByPropertyId",
                new SqlParameter("@PropertyId", propertyId)
                );
        }

        public static List<Renter> GetActiveRenterListByPropertyId(int propertyId)
        {
            //Return a list of renters for the given property
            return DataAccess.GetTypedList<Renter>(
                EfxSettings.ConnectionString,
                "dbo.usp_PropertyRenter_GetActiveRenterListByPropertyId",
                new SqlParameter("@PropertyId", propertyId)
                );
        }

        public static Renter GetRenterBySmsPaymentPhoneNumber(string smsPaymentPhoneNumber)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new Renter(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString,
                "dbo.usp_Renter_GetRenterBySmsPaymentPhoneNumber",
                new SqlParameter("@SmsPaymentPhoneNumber", smsPaymentPhoneNumber)));
        }

        private static Dictionary<string, string> CreateAccountCodePin(int renterId, string accountCodePin)
        {
            var results = new Dictionary<string, string>();
            string acctCodePin = null;

            if (renterId > 0)
            {
                var renter = new Renter(renterId);
                acctCodePin = renter.AccountCodePin;
            }

            if (string.IsNullOrEmpty(accountCodePin))
            {
                if (string.IsNullOrEmpty(acctCodePin))
                    acctCodePin = AuthenticationTools.GenerateRandomNumberString(4, true);
            }
            else
                acctCodePin = accountCodePin;

            results.Add("AccountCodePin", acctCodePin);

            return results;
        }

        public static int Set(int renterId, int? payerId, string prefix, string firstName, string middleName,
            string lastName, string suffix, string mainPhoneNumber, string mobilePhoneNumber,
            string faxNumber, string primaryEmailAddress, string alternateEmailAddress1, string alternateEmailAddress2,
            string streetAddress, string unit, string city, int? stateProvinceId,
            string postalCode, bool? emailNotifications, bool? textNotifications, bool? isParticipating,
            byte[] passwordHash, byte[] salt, string accountCodePin, bool isCreditCardTestRenter,
            bool isCreditCardApproved, string smsPaymentPhoneNumber, int? smsPaymentTypeId, int? smsPaymentCarrierId,
            string pmsId, int? pmsTypeId, bool isActive, string authenticatedUser,
            int? smsReminderDayOfMonth, string pmsPropertyId, int acceptedPaymentTypeId, string rentReporterId)
        {
            var generatedCodes = CreateAccountCodePin(renterId, accountCodePin);

            if (acceptedPaymentTypeId < 1 || acceptedPaymentTypeId > 4)
                acceptedPaymentTypeId = 1;

            //Salcedo - Task 00076
            Renter oldRenterObject = null;
            var logChange = false;
            if (renterId > 0)
            {
                oldRenterObject = new Renter(renterId);
            }
            else
            {
                logChange = true;
            }

            //Salcedo - Task 00076
            if (oldRenterObject != null)
            {
                if (oldRenterObject.Prefix != prefix
                    || oldRenterObject.FirstName != firstName
                    || oldRenterObject.LastName != lastName
                    || oldRenterObject.Suffix != suffix
                    || oldRenterObject.MainPhoneNumber != mainPhoneNumber
                    || oldRenterObject.FaxNumber != faxNumber
                    || oldRenterObject.PrimaryEmailAddress != primaryEmailAddress
                    || oldRenterObject.AlternateEmailAddress1 != alternateEmailAddress1
                    || oldRenterObject.AlternateEmailAddress2 != alternateEmailAddress2
                    || oldRenterObject.StreetAddress != streetAddress
                    || oldRenterObject.Unit != unit
                    || oldRenterObject.City != city
                    || oldRenterObject.PostalCode != postalCode
                    || oldRenterObject.AccountCodePin != accountCodePin
                    || oldRenterObject.IsCreditCardTestRenter != isCreditCardTestRenter
                    || oldRenterObject.IsCreditCardApproved != isCreditCardApproved
                    || oldRenterObject.IsActive != isActive
                    || oldRenterObject.PmsPropertyId != pmsPropertyId
                    || oldRenterObject.AcceptedPaymentTypeId != acceptedPaymentTypeId
                    || oldRenterObject.RentReporterId != rentReporterId
                    || oldRenterObject.PmsId != pmsId
                )
                {
                    logChange = true;
                }
            }
            else
            {
                logChange = true;
            }

            // Checking for existing Primary Email Address from dbo.Renter data table
            // if email exists throw/handle an exception
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());
            //SqlCommand cmd = new SqlCommand("select primaryemailaddress from renter where primaryemailaddress = '" + primaryEmailAddress + "'", con);
            //SqlDataAdapter da = new SqlDataAdapter();
            //DataTable dt = new DataTable("emailAddresses");

            //con.Open();
            //da.SelectCommand = cmd;
            //da.Fill(dt);
            //con.Close();

            //if (dt.Rows.Count == 1)
            //{
                //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
                var returnedRenterId = NewDataAccess.ExecuteScalar<int>(EfxSettings.ConnectionString,
                    "dbo.usp_Renter_SetSingleObject",
                    new[]
                    {
                        new SqlParameter("@RenterId", renterId),
                        new SqlParameter("@PayerId", payerId),
                        new SqlParameter("@Prefix", prefix),
                        new SqlParameter("@FirstName", firstName),
                        new SqlParameter("@MiddleName", middleName),
                        new SqlParameter("@LastName", lastName),
                        new SqlParameter("@Suffix", suffix),
                        new SqlParameter("@MainPhoneNumber", mainPhoneNumber.EmptyStringToNull()),
                        new SqlParameter("@MobilePhoneNumber", mobilePhoneNumber.EmptyStringToNull()),
                        new SqlParameter("@FaxNumber", faxNumber.EmptyStringToNull()),
                        new SqlParameter("@PrimaryEmailAddress", primaryEmailAddress),
                        new SqlParameter("@AlternateEmailAddress1", alternateEmailAddress1),
                        new SqlParameter("@AlternateEmailAddress2", alternateEmailAddress2),
                        new SqlParameter("@StreetAddress", streetAddress),
                        new SqlParameter("@Unit", unit),
                        new SqlParameter("@City", city),
                        new SqlParameter("@StateProvinceId", stateProvinceId),
                        new SqlParameter("@PostalCode", postalCode),
                        new SqlParameter("@EmailNotifications", emailNotifications),
                        new SqlParameter("@TextNotifications", textNotifications),
                        new SqlParameter("@IsParticipating", isParticipating),
                        new SqlParameter("@PasswordHash", passwordHash),
                        new SqlParameter("@Salt", salt),
                        new SqlParameter("@AccountCodePin", generatedCodes["AccountCodePin"]),
                        new SqlParameter("@IsCreditCardTestRenter", isCreditCardTestRenter),
                        new SqlParameter("@IsCreditCardApproved", isCreditCardApproved),
                        new SqlParameter("@SmsPaymentPhoneNumber", smsPaymentPhoneNumber.EmptyStringToNull()),
                        new SqlParameter("@SmsPaymentTypeId", smsPaymentTypeId),
                        new SqlParameter("@SmsPaymentCarrierId", smsPaymentCarrierId),
                        new SqlParameter("@PmsId", pmsId),
                        new SqlParameter("@PmsTypeId", pmsTypeId),
                        new SqlParameter("@IsActive", isActive),
                        new SqlParameter("@AuthenticatedUser", authenticatedUser),
                        new SqlParameter("@SmsReminderDayOfMonth", smsReminderDayOfMonth),
                        new SqlParameter("@PmsPropertyId", pmsPropertyId),
                        new SqlParameter("@AcceptedPaymentTypeId", acceptedPaymentTypeId),
                        new SqlParameter("@RentReporterId", rentReporterId)
                    });

                //Salcedo - 12/31/2014 - only update if there's something to update
                if (returnedRenterId > 0)
                {
                    if (!payerId.HasValue)
                    {
                        Payer.SetPayerFromRenter(new Renter(returnedRenterId));
                    }
                }

                //Salcedo - Task 00076
                //Salcedo - 12/31/2014 - only update if there's something to update
                if (!logChange || returnedRenterId <= 0) return returnedRenterId;
                var log = new RPO.ActivityLog();
                const string logDescription = "Renter Profile Updated";
                log.WriteLog(authenticatedUser, logDescription, 0, renterId.ToString(), true);
                return returnedRenterId;
            //}
            //else
            //{
            //    int returnedRenterId = 0;
            //    return returnedRenterId;
            //}
        }

        public static int Set(Renter renter, string authenticatedUser)
        {
            return Set(
                renter.RenterId,
                renter.PayerId,
                renter.Prefix,
                renter.FirstName,
                renter.MiddleName,
                renter.LastName,
                renter.Suffix,
                renter.MainPhoneNumber,
                renter.MobilePhoneNumber,
                renter.FaxNumber,
                renter.PrimaryEmailAddress,
                renter.AlternateEmailAddress1,
                renter.AlternateEmailAddress2,
                renter.StreetAddress,
                renter.Unit,
                renter.City,
                renter.StateProvinceId,
                renter.PostalCode,
                renter.EmailNotifications,
                renter.TextNotifications,
                renter.IsParticipating,
                renter.PasswordHash,
                renter.Salt,
                renter.AccountCodePin,
                renter.IsCreditCardTestRenter,
                renter.IsCreditCardApproved,
                renter.SmsPaymentPhoneNumber,
                renter.SmsPaymentTypeId,
                renter.SmsPaymentCarrierId,
                renter.PmsId,
                renter.PmsTypeId,
                renter.IsActive,
                authenticatedUser,
                renter.SmsReminderDayOfMonth,
                renter.PmsPropertyId,
                renter.AcceptedPaymentTypeId,
                renter.RentReporterId
            );
        }

        public static void SetHasRegistered(int renterId)
        {
            if (renterId > 0)
            {
                //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
                NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_Renter_SetHasRegistered", new[]
                {
                    new SqlParameter("@RenterId", renterId),
                });
            }
        }

        public static int AddPayerCreditCard(int payerCreditCardId, int payerId, string rentersName, string cardNumber,
            int cardExpirationMonth, int cardExpirationYear, bool primary, bool reminder)
        {
            //usp_Renter_GetMaintenanceRequestListBYRenterID
            var con = new SqlConnection(EfxSettings.ConnectionString);
            var com = new SqlCommand("usp_PayerCreditCard_SetSingleObject", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            com.Parameters.Add("@PayerCreditCardId", SqlDbType.Int).Value = payerCreditCardId;
            com.Parameters.Add("@PayerId", SqlDbType.Int).Value = payerId;
            com.Parameters.Add("@CreditCardHolderName", SqlDbType.NVarChar, 200).Value = rentersName;
            com.Parameters.Add("@CreditCardAccountNumber", SqlDbType.NVarChar, 20).Value = cardNumber;
            com.Parameters.Add("@CreditCardExpirationMonth", SqlDbType.Int).Value = cardExpirationMonth;
            com.Parameters.Add("@CreditCardExpirationYear", SqlDbType.Int).Value = cardExpirationYear;
            com.Parameters.Add("@IsPrimary", SqlDbType.Bit).Value = primary;
            com.Parameters.Add("@ReminderSent", SqlDbType.Bit).Value = reminder;
            try
            {
                con.Open();
                //return com.ExecuteReader(CommandBehavior.CloseConnection);
                return (int)com.ExecuteScalar();

            }
            catch (Exception)
            {
                con.Close();
                throw;
            }
        }

        public static int AddPayerAch(int payerAchd, int payerId, string bankAccountNumber, string bankRoutingNumber, int bankAccountTypeId, bool primary)
        {
            var con = new SqlConnection(EfxSettings.ConnectionString);
            var com = new SqlCommand("usp_PayerAch_SetSingleObject", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            com.Parameters.Add("@PayerAchId", SqlDbType.Int).Value = payerAchd;
            com.Parameters.Add("@PayerId", SqlDbType.Int).Value = payerId;
            com.Parameters.Add("@BankAccountNumber", SqlDbType.NVarChar, 20).Value = bankAccountNumber;
            com.Parameters.Add("@BankRoutingNumber", SqlDbType.NVarChar, 20).Value = bankRoutingNumber;
            com.Parameters.Add("@BankAccountTypeId", SqlDbType.Int).Value = bankAccountTypeId;
            com.Parameters.Add("@IsPrimary", SqlDbType.Bit).Value = primary;
            try
            {
                con.Open();
                return (int)com.ExecuteScalar();

            }
            catch (Exception)
            {
                con.Close();
                throw;
            }
        }

        public static void InsertAutoPayment(int renterId, int payerId, int paymentTypeId, int paymentMethodId, int paymentDayOfMonth)
        {
            var con = new SqlConnection(EfxSettings.ConnectionString);
            var com = new SqlCommand("usp_AutoPayment_InsertUpdate", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            com.Parameters.Add("@RenterID", SqlDbType.Int).Value = renterId;
            com.Parameters.Add("@PayerId", SqlDbType.Int).Value = payerId;
            com.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = paymentTypeId;
            com.Parameters.Add("@PaymentMethodID", SqlDbType.Int).Value = paymentMethodId;
            com.Parameters.Add("@PaymentDayOfMonth", SqlDbType.Int).Value = paymentDayOfMonth;
            try
            {
                con.Open();
                com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception)
            {
                con.Close();
                throw;
            }
        }
    }
}
