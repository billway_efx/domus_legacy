﻿using System;

namespace EfxFramework.BulkMail
{
	public class MailMessageTemplate
	{
		//Public Properties
		public int? MailMessageTemplateId { get; set; }
		public DateTime DateCreated { get; set; }
		public DateTime ExpirationDate { get; set; }
		public bool IsVoid { get; set; }
		public string Subject { get; set; }
		public string Body { get; set; }
		public string UnsubscribeUrl { get; set; }
		public string SenderEmailAddress { get; set; }
		public string SenderName { get; set; }
		public string SenderCompany { get; set; }
		public string SenderStreetAddress { get; set; }
		public string SenderCityStateZip { get; set; }
		public string SenderPhoneNumber { get; set; }
		public string AuthenticatedUser { get; set; }
	}
}