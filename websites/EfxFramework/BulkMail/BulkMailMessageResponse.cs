﻿namespace EfxFramework.BulkMail
{
	public class BulkMailMessageResponse
	{
		public enum BulkMailMessageResponseStatusCode
		{
			Success = 0,
			GeneralFailure = 1
		}

		public BulkMailMessageResponseStatusCode StatusCode { get; set; }
		public string StatusDescription { get; set; }
	}
}