﻿using System.Collections.Generic;

namespace EfxFramework.BulkMail
{
	public class BulkMailMessageRequest
	{
		public string Username { get; set; }
		public string Password { get; set; }
		public int MailMessageTemplateId { get; set; }
		public List<string> Recipients { get; set; } 
	}
}