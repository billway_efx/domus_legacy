﻿namespace EfxFramework.BulkMail
{
	public class MailMessageTemplateRequest
	{
		public string Username { get; set; }
		public string Password { get; set; }
		public MailMessageTemplate MessageTemplate { get; set; }
	}
}