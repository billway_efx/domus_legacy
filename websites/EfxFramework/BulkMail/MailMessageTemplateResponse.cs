﻿namespace EfxFramework.BulkMail
{
	public class MailMessageTemplateResponse
	{
		public enum MailMessageTemplateResponseStatusCode
		{
			Success = 0,
			GeneralFailure = 1
		}

		public MailMessageTemplateResponseStatusCode StatusCode { get; set; }
		public string StatusDescription { get; set; }
		public int? MailMessageTemplateId { get; set; }
	}
}