﻿using System;
using System.Linq;
using Mb2xMail;
using System.Collections.Generic;
using System.Data;
using System.Net.Mail;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Text.RegularExpressions;
using Elmah.Assertions;

namespace EfxFramework.BulkMail
{
    public static class BulkMail
    {
        public static bool invalidEmail = false;

        private static Guid SendMailMessage(string userName, string password, MailAddress fromAddress, string subject, string messageBody, List<MailAddress> recipients)
        {
            //Send the mail message
            //return Mail.SendMailMessage(
            //    Settings.SendMailServerName,
            //    userName,
            //    password,
            //    fromAddress,
            //    subject,
            //    messageBody,
            //    recipients
            //    );

            MailMessage message = new MailMessage();
            message.From = fromAddress;
            //SWitherspoon 10/25/2017: Adding From address to BCC list so user gets a copy
            message.Bcc.Add(fromAddress);

            //Salcedo - 3/22/2014 - if there's only 1 recipient, no need to send BCC
            bool SingleRecipient = false;
            if (recipients.Count == 1)
                SingleRecipient = true;

            //Salcedo - 7/6/2016 - capture for error message, if necessary
            string TextRecipientList = "";

            foreach (MailAddress ma in recipients)
            {
                //message.To.Add(ma);
                if (!ma.Address.Contains("unknown.com") && !ma.Address.Contains("test.com"))
                {
                    if (!SingleRecipient)
                    {
                        message.Bcc.Add(ma);
                        TextRecipientList = TextRecipientList + "; " + ma.Address;
                    }
                    else
                    {
                        message.To.Add(ma);
                        TextRecipientList = TextRecipientList + "; " + ma.Address;
                    }
                }

            }
            message.IsBodyHtml = true;
            message.Subject = subject;
            message.Body = messageBody;
            SmtpClient client = new SmtpClient();

            //Salcedo - 4/3/2015 - add support for dynamic host (mainly for testing)
            client.Host = EfxSettings.SmtpHost;

            //Salcedo - 1/14/2014 - added logic to make sure there's actually somebody to send this to
            if (message.Bcc.Count > 0 || message.CC.Count > 0 || message.To.Count > 0)
            {
                var log = new RPO.ActivityLog();
                //Salcedo - 7/6/2016 - capture/log error if any
                try
                {
                    client.Send(message);
                    log.WriteLog("BulkMail", "Email Successfully Sent to: " + TextRecipientList + "; Subject: " + subject + "; Body: " + messageBody
                       , 1, "", false);
                }
                catch (Exception ex)
                {
                    
                    log.WriteLog("BulkMail", "Unable to send email. Error: " + ex.ToString() + "; Recipients: " + TextRecipientList + "; Subject: " + subject + "; Body: " + messageBody 
                        , 1, "", false);
                }
            }

            Guid returnvalue = new Guid();
            returnvalue = Guid.NewGuid();
            return returnvalue;

        }

        private static Guid SendMailMessage(string userName, string password, MailAddress fromAddress, string subject, string messageBody, List<MailAddress> recipients,
            List<Attachment> attachments)
        {
            //Send the mail message
            //return Mail.SendMailMessage(
            //    Settings.SendMailServerName,
            //    userName,
            //    password,
            //    fromAddress,
            //    subject,
            //    messageBody,
            //    recipients,
            //    attachments
            //    );
            string TextRecipientList = "";
            MailMessage message = new MailMessage();
            message.From = fromAddress;
            //SWitherspoon 10/25/2017: Adding From address to BCC list so user gets a copy
            message.Bcc.Add(fromAddress);

            //Salcedo - 3/22/2014 - if there's only 1 recipient, no need to send BCC
            bool SingleRecipient = false;
            if (recipients.Count == 1)
                SingleRecipient = true;

            foreach (MailAddress ma in recipients)
            {
                //message.To.Add(ma);
                if (!ma.Address.Contains("unknown.com") && !ma.Address.Contains("test.com"))
                {
                    if (!SingleRecipient)
                    {
                        message.Bcc.Add(ma);

                    }
                    else
                    {
                        message.To.Add(ma);
                    }

                    TextRecipientList = TextRecipientList + "; " + ma.Address;
                }
            }

            foreach (Attachment mattachment in attachments)
            {
                //Stream newstream = new MemoryStream(mattachment.FileData);
                //Attachment newAttachment = new Attachment(newstream, mattachment.FileName);
                message.Attachments.Add(mattachment);
            }
            message.IsBodyHtml = true;

            message.Subject = subject;
            message.Body = messageBody;
            SmtpClient client = new SmtpClient();

            //Salcedo - 4/3/2015 - add support for dynamic host (mainly for testing)
            client.Host = EfxSettings.SmtpHost;

            //Salcedo - 1/14/2014 - added logic to make sure there's actually somebody to send this to
            if (message.Bcc.Count > 0 || message.CC.Count > 0 || message.To.Count > 0)
            {
                var log = new RPO.ActivityLog();
                //Salcedo - 7/6/2016 - capture/log error if any
                try
                {
                    client.Send(message);
                    log.WriteLog("BulkMail", "Email Successfully Sent to: " + TextRecipientList + "; Subject: " + subject + "; Body: " + messageBody
                        , 1, "", false);
                }
                catch (Exception ex)
                {

                    log.WriteLog("BulkMail", "Unable to send email. Error: " + ex.ToString() + "; Recipients: " + TextRecipientList + "; Subject: " + subject + "; Body: " + messageBody
                        , 1, "", false);
                }
            }

            Guid returnvalue = new Guid();
            returnvalue = Guid.NewGuid();
            return returnvalue;

        }

        // Original Position - public static void SendMailMessageIncludePropertyEmailContacts(int propertyId, string disseminationGroup, string userName, string password,
        //    MailAddress fromAddress, string subject, string messageBody, List<MailAddress> recipients)
        // To replace see EfxFramework.BulkMail.SendMailMessageIncludePropertyEmailContacts

        public static void SendMailMessageIncludePropertyEmailContacts(int propertyId, string disseminationGroup, string userName, string password,
            MailAddress fromAddress, string subject, string messageBody, List<MailAddress> recipients)
        {
            int counter = 0;
            int emailMaxRecipientLimit = EfxSettings.EmailMaxRecipientLimit;

            var shortRecipientList = Enumerable.Empty<MailAddress>().ToList();

            // Add main recipients to the list
            for (counter = 0; counter < recipients.Count; counter++)
            {
                MailAddress ma;
                ma = recipients[counter];
                shortRecipientList.Add(ma);
            }

            if (propertyId != 0 && !string.IsNullOrEmpty((propertyId.ToString())))
            {
                // Add Property staff to the recipient list - everyone marked as IsMainContact (Receive Resident Notifications) will get the email
                // Converted to a stored procedure - 04/22/2020 BW
                int tempCounter = 0;

                SqlCommand getPropertyStaff = new SqlCommand();     // Changed/Added to replace inline SQL - 04/22/2020 - BW
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());
                SqlDataReader dr;

                getPropertyStaff.CommandType = CommandType.StoredProcedure;
                getPropertyStaff.Connection = con;
                getPropertyStaff.CommandText = "usp_PropertyPropertyStaff_GetMainContactByPropertyId";
                getPropertyStaff.Parameters.AddWithValue("@propertyId", propertyId);

                con.Open();

                dr = getPropertyStaff.ExecuteReader();

                while (dr.Read())
                {
                    shortRecipientList.Add(new MailAddress(dr[0].ToString()));
                    tempCounter++;
                }
                con.Close();

                if (tempCounter == 0)
                {
                    shortRecipientList.Add(new MailAddress(EfxSettings.SupportEmail));
                }
            }

            // Send the email - this method will BCC everyone in the list
            var messageId = SendMailMessage(userName, password, fromAddress, subject, messageBody, shortRecipientList);
            var emailLog = new EmailLog
            {
                MailMessageId = messageId,
                Dissemination = disseminationGroup,
                DateSent = DateTime.UtcNow,
                IsBulkMail = false
            };

            var emailLogId = EmailLog.Set(emailLog);
            EmailLog.AddPropertyToEmailLog(emailLogId, propertyId);
        }

        public static void SendMailMessage(int? propertyId, string disseminationGroup, bool isBulkMail, string userName, string password,
            MailAddress fromAddress, string subject, string messageBody, List<MailAddress> recipients)
        {
            int Counter = 0;
            int TempCounter = 0;
            int EmailMaxRecipientLimit = EfxSettings.EmailMaxRecipientLimit;

            if (recipients.Count == 0)
                return;

            var ShortRecipientList = new List<MailAddress> { recipients[0] };

            for (Counter = 0; Counter < recipients.Count; Counter++)
            {
                if (TempCounter == 0)
                {
                    ShortRecipientList = new List<MailAddress> { recipients[Counter] };
                }
                else
                {
                    MailAddress ma;
                    ma = recipients[Counter];
                    ShortRecipientList.Add(ma);
                }

                TempCounter++;
                if (TempCounter >= EmailMaxRecipientLimit || Counter == recipients.Count - 1)
                {
                    var MessageId = SendMailMessage(userName, password, fromAddress, subject, messageBody, ShortRecipientList);
                    var EmailLog = new EmailLog
                    {
                        MailMessageId = MessageId,
                        Dissemination = disseminationGroup,
                        DateSent = DateTime.UtcNow,
                        IsBulkMail = isBulkMail
                    };

                    var EmailLogId = EmailLog.Set(EmailLog);

                    if (propertyId.HasValue)
                        EmailLog.AddPropertyToEmailLog(EmailLogId, propertyId.Value);

                    TempCounter = 0;
                }
            }
        }

        public static void SendMultiPropertyMailMessage(List<int?> propertyIds, string disseminationGroup, bool isBulkMail, string userName, string password,
            MailAddress fromAddress, string subject, string messageBody, List<MailAddress> recipients)
        {
            int Counter = 0;
            int TempCounter = 0;
            int EmailMaxRecipientLimit = EfxSettings.EmailMaxRecipientLimit;

            if (recipients.Count == 0)
                return;

            var ShortRecipientList = new List<MailAddress> { recipients[0] };

            for (Counter = 0; Counter < recipients.Count; Counter++)
            {
                if (TempCounter == 0)
                {
                    ShortRecipientList = new List<MailAddress> { recipients[Counter] };
                }
                else
                {
                    MailAddress ma;
                    ma = recipients[Counter];
                    ShortRecipientList.Add(ma);
                }

                TempCounter++;
                if (TempCounter >= EmailMaxRecipientLimit || Counter == recipients.Count - 1)
                {
                    var log = new RPO.ActivityLog();
                    var MessageId = SendMailMessage(userName, password, fromAddress, subject, messageBody, ShortRecipientList);

                    var EmailLog = new EmailLog
                    {
                        MailMessageId = MessageId,
                        Dissemination = disseminationGroup,
                        DateSent = DateTime.UtcNow,
                        IsBulkMail = isBulkMail
                    };

                    var EmailLogId = EmailLog.Set(EmailLog);

                    foreach (var Pid in propertyIds.Where(pid => pid.HasValue))
                    {
                        EmailLog.AddPropertyToEmailLog(EmailLogId, Pid.Value);
                    }

                    TempCounter = 0;
                }
            }
        }

        public static void SendMailMessage(int? propertyId, string disseminationGroup, bool isBulkMail, string userName, string password,
            MailAddress fromAddress, string subject, string messageBody, List<MailAddress> recipients, List<Attachment> attachments)
        {
            int Counter = 0;
            int TempCounter = 0;
            int EmailMaxRecipientLimit = EfxSettings.EmailMaxRecipientLimit;

            if (recipients.Count == 0)
                return;

            var ShortRecipientList = new List<MailAddress> { recipients[0] };

            for (Counter = 0; Counter < recipients.Count; Counter++)
            {
                if (TempCounter == 0)
                {
                    ShortRecipientList = new List<MailAddress> { recipients[Counter] };
                }
                else
                {
                    MailAddress ma;
                    ma = recipients[Counter];
                    ShortRecipientList.Add(ma);
                }

                TempCounter++;
                if (TempCounter >= EmailMaxRecipientLimit || Counter == recipients.Count - 1)
                {
                    var MessageId = SendMailMessage(userName, password, fromAddress, subject, messageBody, ShortRecipientList, attachments);
                    var EmailLog = new EmailLog
                    {
                        MailMessageId = MessageId,
                        Dissemination = disseminationGroup,
                        DateSent = DateTime.UtcNow,
                        IsBulkMail = isBulkMail
                    };

                    var EmailLogId = EmailLog.Set(EmailLog);

                    if (propertyId.HasValue)
                        EmailLog.AddPropertyToEmailLog(EmailLogId, propertyId.Value);

                    TempCounter = 0;
                }
            }
        }

        public static void SendMultiPropertyMailMessage(List<int?> propertyIds, string disseminationGroup, bool isBulkMail, string userName, string password, MailAddress fromAddress, string subject, string messageBody,
            List<MailAddress> recipients, List<Attachment> attachments)
        {
            int Counter = 0;
            int TempCounter = 0;
            int EmailMaxRecipientLimit = EfxSettings.EmailMaxRecipientLimit;

            if (recipients.Count == 0)
                return;

            var ShortRecipientList = new List<MailAddress> { recipients[0] };

            for (Counter = 0; Counter < recipients.Count; Counter++)
            {
                if (TempCounter == 0)
                {
                    ShortRecipientList = new List<MailAddress> { recipients[Counter] };
                }
                else
                {
                    MailAddress ma;
                    ma = recipients[Counter];
                    ShortRecipientList.Add(ma);
                }

                TempCounter++;
                if (TempCounter >= EmailMaxRecipientLimit || Counter == recipients.Count - 1)
                {
                    var MessageId = SendMailMessage(userName, password, fromAddress, subject, messageBody, ShortRecipientList, attachments);
                    var EmailLog = new EmailLog
                    {
                        MailMessageId = MessageId,
                        Dissemination = disseminationGroup,
                        DateSent = DateTime.UtcNow,
                        IsBulkMail = isBulkMail
                    };

                    var EmailLogId = EmailLog.Set(EmailLog);

                    foreach (var Pid in propertyIds.Where(pid => pid.HasValue))
                    {
                        EmailLog.AddPropertyToEmailLog(EmailLogId, Pid.Value);
                    }

                    TempCounter = 0;
                }
            }
        }

        public static DataTable GetMessageDeliveryRate()
        {
            var Messages = Report.GetMessageDeliveryRate(EfxSettings.SendMailServerName, EfxSettings.SendMailUsername, EfxSettings.SendMailPassword);
            var ReportTable = BuildDeliveryReportTable();
            var EmailLogs = EmailLog.GetAllBulkEmailLogs();

            foreach (DataRow Row in Messages.Rows)
            {
                var Id = (Guid)Row["MessageIdentifier"];

                if (EmailLogs.Any(el => el.MailMessageId == Id))
                {
                    var NewRow = ReportTable.NewRow();
                    NewRow["NumberSent"] = Row["RecipientsSent"];
                    NewRow["NumberDelivered"] = Row["RecipientsDelivered"];
                    NewRow["Subject"] = Row["Subject"];
                    NewRow["Sender"] = Row["SenderEmailAddress"];

                    var LogMessage = EmailLogs.FirstOrDefault(el => el.MailMessageId == Id);

                    NewRow["Date"] = LogMessage != null ? LogMessage.DateSent : DateTime.MinValue;
                    NewRow["Description"] = LogMessage != null ? LogMessage.Dissemination : string.Empty;
                    NewRow["PercentDelivered"] = Row["DeliveryPercentage"];

                    ReportTable.Rows.Add(NewRow);
                }
            }

            return ReportTable;
        }

        private static DataTable BuildDeliveryReportTable()
        {
            var ReportTable = new DataTable();
            ReportTable.Columns.Add(new DataColumn("NumberSent", typeof(int)));
            ReportTable.Columns.Add(new DataColumn("NumberDelivered", typeof(int)));
            ReportTable.Columns.Add(new DataColumn("Subject", typeof(string)));
            ReportTable.Columns.Add(new DataColumn("Date", typeof(DateTime)));
            ReportTable.Columns.Add(new DataColumn("Sender", typeof(string)));
            ReportTable.Columns.Add(new DataColumn("Description", typeof(string)));
            ReportTable.Columns.Add(new DataColumn("PercentDelivered", typeof(decimal)));

            return ReportTable;
        }

        //Salcedo - 8/31/2015 - added function that can be used to determine if email address is valid
        public static bool IsValidEmail(string strIn)
        {
            invalidEmail = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names. 
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalidEmail)
                return false;

            // Return true if strIn is in valid e-mail format. 
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        private static string DomainMapper(Match match)
        {

            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalidEmail = true;
            }
            return match.Groups[1].Value + domainName;
        }

        //CMallory - Task 0079 - Simple Email
        public static void SendSimpleMailMessage(string userName, string password, string fromAddress, string subject, string messageBody, List<string> recipients)
        {
            //Send the mail message
            //return Mail.SendMailMessage(
            //    Settings.SendMailServerName,
            //    userName,
            //    password,
            //    fromAddress,
            //    subject,
            //    messageBody,
            //    recipients
            //    );

            MailMessage message = new MailMessage();
            message.From = new MailAddress(fromAddress);

            //Salcedo - 3/22/2014 - if there's only 1 recipient, no need to send BCC
            bool SingleRecipient = false;
            if (recipients.Count == 1)
                SingleRecipient = true;

            foreach (string ma in recipients)
            {
                //message.To.Add(ma);
                if (!ma.Contains("unknown.com") && !ma.Contains("test.com"))
                {
                    if (!SingleRecipient)
                    {
                        message.Bcc.Add(ma);
                    }
                    else
                    {
                        message.To.Add(ma);
                    }
                }

            }
            message.IsBodyHtml = true;
            message.Subject = subject;
            message.Body = messageBody;
            SmtpClient client = new SmtpClient();

            //Salcedo - 4/3/2015 - add support for dynamic host (mainly for testing)
            client.Host = EfxSettings.SmtpHost;

            //Salcedo - 1/14/2014 - added logic to make sure there's actually somebody to send this to
            if (message.Bcc.Count > 0 || message.CC.Count > 0 || message.To.Count > 0)
            {
                client.Send(message);
            }

            Guid returnvalue = new Guid();
            returnvalue = Guid.NewGuid();
            //return returnvalue;

        }

    }
}
