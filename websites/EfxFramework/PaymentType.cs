//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EfxFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class PaymentType
    {
        public PaymentType()
        {
            this.AutoPayments = new HashSet<AutoPayment>();
            this.Payments = new HashSet<Payment>();
            this.PaymentExportSummaries = new HashSet<PaymentExportSummary>();
            this.PaymentQueues = new HashSet<PaymentQueue>();
            this.Renters = new HashSet<Renter>();
            this.Transactions = new HashSet<Transaction>();
        }
    
        public int PaymentTypeId { get; set; }
        public string PaymentTypeName { get; set; }
    
        public virtual ICollection<AutoPayment> AutoPayments { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
        public virtual ICollection<PaymentExportSummary> PaymentExportSummaries { get; set; }
        public virtual ICollection<PaymentQueue> PaymentQueues { get; set; }
        public virtual ICollection<Renter> Renters { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
