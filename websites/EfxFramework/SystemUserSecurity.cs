using EfxFramework.Data;
using System.Data.SqlClient;

namespace EfxFramework
{
    public class SystemUserSecurity : BaseEntity
    {
        //Public Properties
        public int SystemUserSecurityId { get; set; }
        public int SystemUserId { get; set; }
        public bool CanViewPropertyDetail { get; set; }
        public bool IsPropertyGeneralManager { get; set; }
        public bool CanCreateNewSalesStaffAccounts { get; set; }
        public bool CanViewSalesStaffAccounts { get; set; }
        public bool CanCreateNewRenterAccounts { get; set; }
        public bool CanViewRenterDetails { get; set; }
        public bool CanSendBulkMail { get; set; }

        public static SystemUserSecurity GetSystemUserSecurityBySystemUserId(int? systemUserId)
        {
            var SystemUserSecurityId = (int?)DataAccess.ExecuteScalar(
                Settings.ConnectionString,
                "dbo.usp_SystemUserSecurity_GetSystemUserSecurityBySystemUserId",
                new SqlParameter("@SystemUserId", systemUserId)
                );

            //Return
            return SystemUserSecurityId.HasValue ? new SystemUserSecurity((int)SystemUserSecurityId) : null;
        }
        public static int Set(int systemUserSecurityId, int systemUserId, bool canViewPropertyDetail, bool isPropertyGeneralManager, bool canCreateNewSalesStaffAccounts, bool canViewSalesStaffAccounts, bool canCreateNewRenterAccounts, bool canViewRenterDetails, bool canSendBulkMail)
        {
            return DataAccess.ExecuteScalar<int>(
                Settings.ConnectionString,
                "dbo.usp_SystemUserSecurity_SetSingleObject",
            new[]
                {
	                new SqlParameter("@SystemUserSecurityId", systemUserSecurityId),
	                new SqlParameter("@SystemUserId", systemUserId),
	                new SqlParameter("@CanViewPropertyDetail", canViewPropertyDetail),
	                new SqlParameter("@IsPropertyGeneralManager", isPropertyGeneralManager),
	                new SqlParameter("@CanCreateNewSalesStaffAccounts", canCreateNewSalesStaffAccounts),
	                new SqlParameter("@CanViewSalesStaffAccounts", canViewSalesStaffAccounts),
	                new SqlParameter("@CanCreateNewRenterAccounts", canCreateNewRenterAccounts),
	                new SqlParameter("@CanViewRenterDetails", canViewRenterDetails),
	                new SqlParameter("@CanSendBulkMail", canSendBulkMail)
                });
        }
        public static int Set(SystemUserSecurity s)
        {
            return Set(
                s.SystemUserSecurityId,
                s.SystemUserId,
                s.CanViewPropertyDetail,
                s.IsPropertyGeneralManager,
                s.CanCreateNewSalesStaffAccounts,
                s.CanViewSalesStaffAccounts,
                s.CanCreateNewRenterAccounts,
                s.CanViewRenterDetails,
                s.CanSendBulkMail
            );
        }
        public SystemUserSecurity(int systemUserSecurityId)
        {
            //Attempt to pull the data from the database
            var ResultSet = DataAccess.ExecuteOneRow(Settings.ConnectionString, "dbo.usp_SystemUserSecurity_GetSingleObject", new[] { new SqlParameter("@SystemUserSecurityId", systemUserSecurityId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
                SystemUserSecurityId = (int)ResultSet[0];
                SystemUserId = (int)ResultSet[1];
                CanViewPropertyDetail = (bool)ResultSet[2];
                IsPropertyGeneralManager = (bool)ResultSet[3];
                CanCreateNewSalesStaffAccounts = (bool)ResultSet[4];
                CanViewSalesStaffAccounts = (bool)ResultSet[5];
                CanCreateNewRenterAccounts = (bool)ResultSet[6];
                CanViewRenterDetails = (bool)ResultSet[7];
                CanSendBulkMail = (bool)ResultSet[8];
            }
        }
        public SystemUserSecurity()
        {
            InitializeObject();
        }

    }
}
