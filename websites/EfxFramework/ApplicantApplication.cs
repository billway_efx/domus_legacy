using System;
using System.Data.SqlTypes;
using Mb2x.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class ApplicantApplication : BaseEntity
	{
		//Public Properties
		[DisplayName(@"ApplicantApplicationId"), Required(ErrorMessage = "ApplicantApplicationId is required.")]
		public int ApplicantApplicationId { get; set; }

		[DisplayName(@"ApplicantId"), Required(ErrorMessage = "ApplicantId is required.")]
		public int ApplicantId { get; set; }

		[DisplayName(@"CompletedApplication")]
		public byte[] CompletedApplication { get; set; }

		[DisplayName(@"PropertyId"), Required(ErrorMessage = "PropertyId is required.")]
		public int PropertyId { get; set; }

		[DisplayName(@"ApplicationStatusId"), Required(ErrorMessage = "ApplicationStatusId is required.")]
		public int ApplicationStatusId { get; set; }

		[DisplayName(@"FeesPaid"), Required(ErrorMessage = "FeesPaid is required.")]
		public bool FeesPaid { get; set; }

        [DisplayName(@"ApplicationSubmissionTypeId"), Required(ErrorMessage = "ApplicationSubmissionTypeId is required")]
        public int ApplicationSubmissionTypeId { get; set; }

        [DisplayName(@"ApplicationDate"), Required(ErrorMessage = "ApplicationDate is required")]
        public DateTime ApplicationDate { get; set; }

        [DisplayName(@"ApplicationStatusDate"), Required(ErrorMessage = "ApplicationStatusDate is required")]
        public DateTime ApplicationStatusDate { get; set; }

        [DisplayName(@"StatusUpdatedBy")]
        public string StatusUpdatedBy { get; set; }

        public string ApplicationDateString { get { return ApplicationDate.ToShortDateString(); } }
        public string ApplicationStatusDateString { get { return ApplicationStatusDate.ToShortDateString(); } }
        public string EmailApplicantUrl { get { return String.Format("~/Handlers/GeneralHandler.ashx?RequestType=16&ApplicantId={0}&PropertyId={1}&StatusId={2}#ApplicationTab", ApplicantId, PropertyId, ApplicationStatusId); } }
        public string ApplicationUrl { get { return GetCompletedApplication(ApplicantApplicationId) != null ? String.Format("~/Handlers/ApplicationDownloadHandler.ashx?ApplicantApplicationId={0}", ApplicantApplicationId) : null; } }
        public string Status { get { return ((ApplicationStatus)ApplicationStatusId).ToString(); } }
        public string TakeAPaymentUrl { get { return FeesPaid ? String.Empty : String.Format("~/MyProperties/PropertyDetails.aspx?propertyId={0}&ApplicantApplicationId={1}#ApplicationTab", PropertyId, ApplicantApplicationId); } }
        public string TakeAPaymentText { get { return FeesPaid ? "Received" : "Take a Payment"; } }
	    public string DownloadLinkText { get { return GetCompletedApplication(ApplicantApplicationId) != null ? "Download" : "N/A"; } }
        
	    public string ApplicantDisplayName
	    {
	        get
	        {
	            var Applicant = new Applicant(ApplicantId);
	            return Applicant.ApplicantId < 1 ? String.Empty : Applicant.DisplayName;
	        }
	    }

	    public string PropertyDisplayName
	    {
	        get
	        {
	            var Property = new Property(PropertyId);

	            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
	                return String.Empty;

	            return String.Format("{0} {1}, {2}", Property.PropertyName, Property.City, GetState(Property.StateProvinceId));
	        }
	    }
        //cakel: BUGID00033 - Created new SP to return last 30 days
        public static List<ApplicantApplication> GetAllApplicantApplicationsByPropertyId(int propertyId)
        {
            return DataAccess.GetTypedList<ApplicantApplication>(EfxSettings.ConnectionString, "dbo.usp_ApplicantApplication_GetApplicantApplicationByPropertyId_v2", new SqlParameter("@PropertyId", propertyId));
        }

        public static List<ApplicantApplication> GetAllApplicantApplicationsByApplicantId(int applicantId)
        {
            return DataAccess.GetTypedList<ApplicantApplication>(EfxSettings.ConnectionString, "dbo.usp_ApplicantApplication_GetByApplicantId", new SqlParameter("@ApplicantId", applicantId));
        }

        public static byte[] GetCompletedApplication(int applicantApplicationId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<byte[]>(EfxSettings.ConnectionString, "dbo.usp_ApplicantApplication_GetCompletedApplication", new SqlParameter("@ApplicantApplicationId", applicantApplicationId));
        }

        public static void SetCompletedApplication(int applicantApplicationId, byte[] completedApplication)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_ApplicantApplication_SetCompletedApplication", new[]
                {
                    new SqlParameter("@ApplicantApplicationId", applicantApplicationId),
                    new SqlParameter("@CompletedApplication", completedApplication)
                });
        }

        private static DateTime GetSqlDate(DateTime value)
        {
            if (value < SqlDateTime.MinValue.Value)
                return SqlDateTime.MinValue.Value;
            if (value > SqlDateTime.MaxValue.Value)
                return SqlDateTime.MaxValue.Value;

            return value;
        }

        public static int Set(int applicantApplicationId, int applicantId, int propertyId, int applicationStatusId, bool feesPaid, int applicationSubmissionTypeId, DateTime applicationDate, DateTime applicationStatusDate, string statusUpdatedBy)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_ApplicantApplication_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@ApplicantApplicationId", applicantApplicationId),
	                    new SqlParameter("@ApplicantId", applicantId),
	                    new SqlParameter("@PropertyId", propertyId),
	                    new SqlParameter("@ApplicationStatusId", applicationStatusId),
	                    new SqlParameter("@FeesPaid", feesPaid),
                        new SqlParameter("@ApplicationSubmissionTypeId", applicationSubmissionTypeId),
                        new SqlParameter("@ApplicationDate", GetSqlDate(applicationDate)),
                        new SqlParameter("@ApplicationStatusDate", GetSqlDate(applicationStatusDate)),
                        new SqlParameter("@StatusUpdatedBy", statusUpdatedBy)
                    });
        }

        public static int Set(ApplicantApplication a)
        {
	        return Set(
		        a.ApplicantApplicationId,
		        a.ApplicantId,
		        a.PropertyId,
		        a.ApplicationStatusId,
		        a.FeesPaid,
                a.ApplicationSubmissionTypeId,
                a.ApplicationDate,
                a.ApplicationStatusDate,
                a.StatusUpdatedBy
	        );
        }

        private static string GetState(int? stateProvinceId)
        {
            if (!stateProvinceId.HasValue || stateProvinceId.Value < 1)
                return String.Empty;

            return ((StateProvince) stateProvinceId.Value).ToString();
        }

        public ApplicantApplication(int? applicantApplicationId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_ApplicantApplication_GetSingleObject", new [] { new SqlParameter("@ApplicantApplicationId", applicantApplicationId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        ApplicantApplicationId = (int)ResultSet[0];
		        ApplicantId = (int)ResultSet[1];
		        PropertyId = (int)ResultSet[2];
		        ApplicationStatusId = (int)ResultSet[3];
		        FeesPaid = (bool)ResultSet[4];
                ApplicationSubmissionTypeId = (int) ResultSet[5];
                ApplicationDate = (DateTime) ResultSet[6];
                ApplicationStatusDate = (DateTime) ResultSet[7];
                StatusUpdatedBy = ResultSet[8] as string;
            }
        }

		public ApplicantApplication()
		{
			InitializeObject();
		}

	}
}
