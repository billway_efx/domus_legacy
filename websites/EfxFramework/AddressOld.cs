﻿using System.Collections.Generic;
using EfxFramework.ExtensionMethods;
using Mb2x.ExtensionMethods;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;


namespace EfxFramework
{
	public class AddressOld : BaseEntity
	{
		//Public Properties
// ReSharper disable LocalizableElement
        [StringLength(100, ErrorMessage = "Street address can be no greater than 100 characters."), DisplayName(@"Street Address")]
		public string StreetAddress { get; set; }

        [StringLength(100, ErrorMessage = "Address 2 can be no greater than 100 characters."), DisplayName(@"Address 2")]
		public string Address2 { get; set; }

        [StringLength(100, ErrorMessage = "City can be no greater than 100 characters."), DisplayName(@"City")]
		public string City { get; set; }

		public StateProvince? CurrentStateProvince { get; set; }

		public string StateProvinceAbbreviation
		{
			get { return CurrentStateProvince.GetFriendlyName(); }
		}

        [StringLength(10, ErrorMessage = "Postal code can be no greater than 10 characters."), DisplayName(@"Postal Code")]
		public string PostalCode { get; set; }
// ReSharper restore LocalizableElement

		public string CityStatePostalCode
		{
			get { return string.Format("{0}, {1} {2}", City, StateProvinceAbbreviation, PostalCode); }
		}

		public bool IsValid
		{
			get { return !StreetAddress.IsTrimNullOrEmpty() && !City.IsTrimNullOrEmpty() && CurrentStateProvince.HasValue && !PostalCode.IsTrimNullOrEmpty(); }
		}

		public string FullAddress
		{
			get { 
				
				var RetVal = new StringBuilder();
				RetVal.Append(StreetAddress);
				if (!Address2.IsTrimNullOrEmpty())
				{
					RetVal.AppendFormat(", {0}", Address2);
				}
				RetVal.AppendFormat(", {0}", CityStatePostalCode);
				return RetVal.ToString();
			}
		}

        public string GetStateProvinceString()
        {
            if (!CurrentStateProvince.HasValue)
                return "N/A";

            return CurrentStateProvince.Value.ToString();
        }

		//Public Methods
		public static StateProvince? GetStateProvinceFromString(string input)
		{
			//Create an instance of the StateProvince enum
            const StateProvince currentStateProvince = new StateProvince();

			//Get the list of id, name and description from the enum
			var IdNameDescriptionList = currentStateProvince.GetIdNameDescription();

			//Check against the value for the enum
			var ValueList = IdNameDescriptionList.Where(i => string.Equals(input, i.Item1.ToString(EfxSettings.DefaultCulture), StringComparison.OrdinalIgnoreCase));
		    var List = ValueList as IList<Tuple<int, string, string>> ?? ValueList.ToList();
		    if (List.Any())
			{
                return (StateProvince)List.First().Item1;
			}

			//Check against the name for the enum
			var NameList = IdNameDescriptionList.Where(i => string.Equals(input, i.Item2, StringComparison.OrdinalIgnoreCase));
		    var Enumerable = NameList as IList<Tuple<int, string, string>> ?? NameList.ToList();
		    if (Enumerable.Any())
			{
                return (StateProvince)Enumerable.First().Item1;
			}

			//Check against the description for the enum
			var DescriptionList = IdNameDescriptionList.Where(i => string.Equals(input, i.Item3, StringComparison.OrdinalIgnoreCase));
		    var Tuples = DescriptionList as IList<Tuple<int, string, string>> ?? DescriptionList.ToList();
		    if (Tuples.Any())
			{
                return (StateProvince)Tuples.First().Item1;
			}

			//If we don't find a match, return null
			return null;
		}
		
		//Overrides
		public override string ToString()
		{
			var RetVal = new StringBuilder();

			RetVal.Append(StreetAddress);
			RetVal.Append(", ");
			if (!Address2.IsTrimNullOrEmpty())
			{
				RetVal.Append(Address2);
				RetVal.Append(", ");
			}
			RetVal.Append(City);
			RetVal.Append(", ");
			RetVal.Append(StateProvinceAbbreviation);
			RetVal.Append(" ");
			RetVal.Append(PostalCode);

			return RetVal.ToString();
		}
	}
}