using Mb2x.Data;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EfxFramework
{
    public class EfxAdministrator : BaseUser
    {
        //Public Properties
        public override int? UserId => EfxAdministratorId;

        public int EfxAdministratorId { get; set; }

        protected override bool IsValidUser()
        {
            return EfxAdministratorId > 0;
        }

        public override BaseUser GetUserById(int userId)
        {
            return new EfxAdministrator(userId);
        }

        public override BaseUser GetUserByEmailAddress(string emailAddress)
        {
            return new EfxAdministrator(emailAddress);
        }

        public override int SetUser(BaseUser user, string authenticatedUser)
        {
            return Set((user as EfxAdministrator), authenticatedUser);
        }

        public static void DeleteEfxAdministratorByEfxAdministratorId(int efxAdministratorId, string authenticatedUser)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(
                EfxSettings.ConnectionString,
                "dbo.usp_EfxAdministrator_DeleteEfxAdminstratorByEfxAdministratorId",
                new[]
                    {
                        new SqlParameter("@EfxAdministratorId", efxAdministratorId),
                        new SqlParameter("@AuthenticatedUser", authenticatedUser)
                    });
        }

        public static List<EfxAdministrator> GetAllEfxAdministrators()
        {
            return DataAccess.GetTypedList<EfxAdministrator>(EfxSettings.ConnectionString, "dbo.usp_EfxAdministrator_GetAllEfxAdministrator");
        }

        // , string bccEmailAddress
        public static int Set(int efxAdministratorId, string firstName, string lastName, string primaryEmailAddress, byte[] passwordHash, byte[] salt, string authenticatedUser)
        {
            //	 new SqlParameter("@BccEmailAddress", bccEmailAddress),
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
                EfxSettings.ConnectionString,
                "dbo.usp_EfxAdministrator_SetSingleObject",
                new[]
                {
                    new SqlParameter("@EfxAdministratorId", efxAdministratorId),
                    new SqlParameter("@FirstName", firstName),
                    new SqlParameter("@LastName", lastName),
                    new SqlParameter("@PrimaryEmailAddress", primaryEmailAddress),
                    new SqlParameter("@PasswordHash", passwordHash),
                    new SqlParameter("@Salt", salt),
                    new SqlParameter("@AuthenticatedUser", authenticatedUser)

                });
        }

        public static int Set(EfxAdministrator e, string authenticatedUser)
        {
            //   e.BccEmailAddress
            return Set(
                e.EfxAdministratorId,
                e.FirstName,
                e.LastName,
                e.PrimaryEmailAddress,
                e.PasswordHash,
                e.Salt,
                authenticatedUser

            );
        }

        public EfxAdministrator(int? efxAdministratorId)
        {
            //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_EfxAdministrator_GetSingleObject", new[] { new SqlParameter("@EfxAdministratorId", efxAdministratorId) });

            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
                EfxAdministratorId = (int)ResultSet[0];
                FirstName = ResultSet[1] as string;
                LastName = ResultSet[2] as string;
                PrimaryEmailAddress = ResultSet[3] as string;
                PasswordHash = ResultSet[4] as byte[];
                Salt = ResultSet[5] as byte[];
                //BccEmailAddress = ResultSet[6] as string;
            }
        }

        //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
        public EfxAdministrator(string primaryEmailAddress)
            : this(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_EfxAdministrator_GetEfxAdministratorByPrimaryEmailAddress", new SqlParameter("@PrimaryEmailAddress", primaryEmailAddress)))
        {
        }

        public EfxAdministrator()
        {
        }
    }
}
