﻿using EfxFramework.ExtensionMethods;
using EfxFramework.ViewPresentation.Interfaces.Pages.FindPropertyPages;
using EfxFramework.Web.Caching;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EfxFramework.ViewPresentation.Presenters.Pages.FindPropertyPresenters
{ 
    public abstract class FindPropertyPresenterBase
    {
        protected IFindProperty _BaseView;

        protected FindPropertyPresenterBase(IFindProperty baseView)
        {
            _BaseView = baseView;
            _BaseView.FindPropertyControl.SearchButtonClick = SearchButtonClicked;
        }

        public virtual void InitializeValues()
        {
            _BaseView.FindPropertyControl.State.BindToSortedEnum(typeof(StateProvince));
            GetAllProperties();
        }
        //CMallory - Task 00166 - Changed line from Property.GetAllPropertyList() so the search only comes up with properties with their PropertyIsPublic flag is set to true
        protected virtual List<Property> GetAllProperties()
        {
            var Properties = CacheManager.GetCacheItem<List<Property>>(CacheKey.PropertyCache);

            //CMallory - Task 00166 - Commented out If statement so that a user gets fresh data everytime they search for a property.
            //if (Properties == null || Properties.Count < 1)
                CacheManager.AddItemToCache(CacheKey.PropertyCache, Property.GetAllPublicPropertyList());  
            
            return CacheManager.GetCacheItem<List<Property>>(CacheKey.PropertyCache);
        }

        protected void PopulateGrid(string nextStepUrl, string linkText)
        {
            var FilterResult = new List<SearchGridProperty>();

            if (string.IsNullOrEmpty(nextStepUrl) && string.IsNullOrEmpty(linkText))
                FilterResult.RemoveAll(p => p.PropertyId > 0);
            else
                FilterResult.AddRange(GetAllProperties().Select(p => new SearchGridProperty(p, nextStepUrl, linkText)));

            _BaseView.FindPropertyControl.PropertyListDataSource = FilterResult;
        }

        protected abstract void FilterPropertyByName(string nextStepUrl, string linkText);
        protected abstract void FilterPropertyByZip(string nextStepUrl, string linkText);
        protected abstract void FilterPropertyByCityAndState(string nextStepUrl, string linkText);
        protected abstract void FilterPropertyByCity(string nextStepUrl, string linkText);
        protected abstract void FilterPropertyByState(string nextStepUrl, string linkText);

        protected void SearchButtonClicked(object sender, EventArgs e)
        {

            PerformSearch();
        }

        protected virtual void PerformSearch()
        {
            
        }
    }
}
