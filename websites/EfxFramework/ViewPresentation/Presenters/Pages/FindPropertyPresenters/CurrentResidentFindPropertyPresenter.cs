﻿using EfxFramework.ViewPresentation.Interfaces.Pages.FindPropertyPages;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EfxFramework.ViewPresentation.Presenters.Pages.FindPropertyPresenters
{
    internal class CurrentResidentFindPropertyPresenter : FindPropertyPresenterBase
    {
        private readonly ICurrentResidentFindProperty _View;

        public CurrentResidentFindPropertyPresenter(ICurrentResidentFindProperty view)
            : base(view)
        {
            _View = view;
        }

        protected override void PerformSearch()
        {
            if (string.IsNullOrEmpty(_BaseView.FindPropertyControl.PropertyName) &&
                string.IsNullOrEmpty(_BaseView.FindPropertyControl.ZipCode) &&
                _BaseView.FindPropertyControl.State.SelectedIndex == 0 &&
                string.IsNullOrEmpty(_BaseView.FindPropertyControl.City))
                PopulateGrid(string.Empty, string.Empty);
            else if (_BaseView.FindPropertyControl.PropertyName.Length > 0)
                FilterPropertyByName("CurrentResidentsRegister.aspx", "Register Today");
            else if (_BaseView.FindPropertyControl.ZipCode.Length > 0)
                FilterPropertyByZip("CurrentResidentsRegister.aspx", "Register Today");
            else if (_BaseView.FindPropertyControl.State.SelectedIndex > 0 && _BaseView.FindPropertyControl.City.Length > 0)
                FilterPropertyByCityAndState("CurrentResidentsRegister.aspx", "Register Today");
            else if (_BaseView.FindPropertyControl.State.SelectedIndex > 0 && _BaseView.FindPropertyControl.City.Length < 1)
                FilterPropertyByState("CurrentResidentsRegister.aspx", "Register Today");
            else if (_BaseView.FindPropertyControl.State.SelectedIndex < 1 && _BaseView.FindPropertyControl.City.Length > 0)
                FilterPropertyByCity("CurrentResidentsRegister.aspx", "Register Today");
        }

        // Patrick Whittingham - 4/6/15 - task #000355 : use contains and not startswith
        protected override void FilterPropertyByName(string nextStepUrl, string linkText)
        {
            var PropertyList = Property.GetAllPropertyList();
            var FilteredResult = new List<SearchGridProperty>();

            if (!string.IsNullOrEmpty(nextStepUrl) && !string.IsNullOrEmpty(linkText))
            {
                FilteredResult.AddRange(from P in PropertyList.Where(p => !string.IsNullOrEmpty(p.PropertyName) &&
                                        p.PropertyName.Contains(_BaseView.FindPropertyControl.PropertyName, StringComparison.OrdinalIgnoreCase) &&
                                        !string.IsNullOrEmpty(p.PropertyName))
                                        select new SearchGridProperty(P, nextStepUrl, linkText));
            }

            _BaseView.FindPropertyControl.PropertyListDataSource = FilteredResult;
        }

        protected override void FilterPropertyByZip(string nextStepUrl, string linkText)
        {
            var PropertyList = GetAllProperties();
            var FilteredResult = new List<SearchGridProperty>();

            if (!string.IsNullOrEmpty(nextStepUrl) && !string.IsNullOrEmpty(linkText))
            {
                FilteredResult.AddRange(from P in PropertyList.Where(p => !string.IsNullOrEmpty(p.PostalCode) &&
                                        p.PostalCode.StartsWith(_BaseView.FindPropertyControl.ZipCode) &&
                                        p.PropertyId.HasValue)
                                        select new SearchGridProperty(P, nextStepUrl, linkText));
            }

            _BaseView.FindPropertyControl.PropertyListDataSource = FilteredResult;
        }

        protected override void FilterPropertyByCityAndState(string nextStepUrl, string linkText)
        {
            var PropertyList = GetAllProperties();
            var FilteredResult = new List<SearchGridProperty>();

            if (!string.IsNullOrEmpty(nextStepUrl) && !string.IsNullOrEmpty(linkText))
                FilteredResult.AddRange(from P in PropertyList.Where(p => !string.IsNullOrEmpty(p.City) &&
                                        p.StateProvinceId.HasValue &&
                                        p.City.StartsWith(_BaseView.FindPropertyControl.City, StringComparison.OrdinalIgnoreCase) &&
                                        p.StateProvinceId == _BaseView.FindPropertyControl.State.SelectedValue.ToInt32())
                                        select new SearchGridProperty(P, nextStepUrl, linkText));

            _BaseView.FindPropertyControl.PropertyListDataSource = FilteredResult;
        }

        protected override void FilterPropertyByCity(string nextStepUrl, string linkText)
        {
            var PropertyList = GetAllProperties();
            var FilteredResult = new List<SearchGridProperty>();

            if (!string.IsNullOrEmpty(nextStepUrl) && !string.IsNullOrEmpty(linkText))
                FilteredResult.AddRange(from P in PropertyList.Where(p => !string.IsNullOrEmpty(p.City) &&
                                        p.City.StartsWith(_BaseView.FindPropertyControl.City, StringComparison.OrdinalIgnoreCase))
                                        select new SearchGridProperty(P, nextStepUrl, linkText));

            _BaseView.FindPropertyControl.PropertyListDataSource = FilteredResult;
        }

        protected override void FilterPropertyByState(string nextStepUrl, string linkText)
        {
            var PropertyList = GetAllProperties();
            var FilteredResult = new List<SearchGridProperty>();

            if (!string.IsNullOrEmpty(nextStepUrl) && !string.IsNullOrEmpty(linkText))
                FilteredResult.AddRange(from P in PropertyList.Where(p => p.StateProvinceId.HasValue &&
                                        p.StateProvinceId == _BaseView.FindPropertyControl.State.SelectedValue.ToInt32())
                                        select new SearchGridProperty(P, nextStepUrl, linkText));

            _BaseView.FindPropertyControl.PropertyListDataSource = FilteredResult;
        }
    }
}
