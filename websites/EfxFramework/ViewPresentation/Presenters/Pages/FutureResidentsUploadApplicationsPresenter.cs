﻿using System.Collections.Generic;
using System.Net.Mail;
using EfxFramework.PaymentMethods;
using EfxFramework.Resources;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.WebPayments;
using System;
using System.Linq;
using Mb2x.ExtensionMethods;
using System.Windows.Forms;
using EfxFramework.PublicApi.Payment;

namespace EfxFramework.ViewPresentation.Presenters.Pages
{
    public class FutureResidentsUploadApplicationsPresenter
    {
        private readonly IFutureResidentsUploadApplications _View;

        public FutureResidentsUploadApplicationsPresenter(IFutureResidentsUploadApplications view)
        {
            _View = view;
            _View.UploadButtonClicked = UploadButtonClicked;
            _View.CompleteButtonClicked = CompleteButtonClick;
        }

        public void InitializeValues()
        {
            //If no PropId value found in the query string, throw exception
            if (_View.PropertyId < 1 || _View.ApplicantId < 1)
                throw new Exception("FutureResidentsRegister: Query string missing Property or Applicant Id.");

            //Initialize property application fees
            var Fees = ApplicationFee.GetAllApplicationFeesByPropertyId(_View.PropertyId);
            _View.FeesDataSource = Fees;
            var Property = new Property(_View.PropertyId);
            _View.PropertyNameText = Property.PropertyWithNameAndAddressNoStreet;
        }
       
        private void CompleteButtonClick(object sender, EventArgs e)
        {
            if (!_View.AcceptTermsChecked)
            {
                _View.DisplaySystemMessage("You must accept the terms and conditions to continue.", SystemMessageType.Error);
                return;
            }

            if (!_View.ValidatePage())
                return;

            var Response = ProcessApplicationPayment();

            

            //Salcedo - 1/19/2014 - added support for displaying error results to user
            if (Response.Result == GeneralResponseResult.Success)
            {
                //CMallory - Task 00499 - Retrieve property object based on Property ID.
                Property propertyRetrieved = new Property(_View.PropertyId);

                //CMallory - Task 00499 - Added If/Else statement.  If the property uses Apply Connect, the user will be transferred to Apply Connect's site once their payment has been processed.  If not, they'll be directed to the receipt page.
                if (propertyRetrieved.UsesApplyConnect)
                {
                    MessageBox.Show("Your payment was received.  You will now be transferred to Apply Connect to complete your background check.");
                    _View.SafeRedirect("https://rpo.applyconnect.com/");
                    
                }

                else
                {
                    
                    _View.SafeRedirect(String.Format("~/FutureResidentsApplicationReceipt.aspx?TransactionId={0}&PropId={1}&ApplicantId={2}",
                    Response.TransactionId, _View.PropertyId, _View.ApplicantId));

                    //cakel: BUGID00032 - Replaced with New Function SendEmailX2
                    //SendEmailX();

                    //cakel: BUGID00032 added new variable for New sendEmailX2

                    string PayStatus = "Payment Approved";
                    var _amount = Response.TransactionAmount;
                    int _propID = _View.PropertyId;
                    int _resident = _View.ApplicantId;
                    string _RenterFirstName = _View.PayerBillingInformationControl.FirstNameText.ToString();
                    string _RenterLastName = _View.PayerBillingInformationControl.LastNameText.ToString();
                    var _rent = new Renter(_View.PaymentTypeControl.RenterId);

                    var _newApp = new Applicant(_resident);
                    var _email = _newApp.PrimaryEmailAddress;
                    var _phone = _newApp.MainPhoneNumber;

                    var property = new EfxFramework.Property(_propID);

                    SendEmailX2(_propID, _amount, _RenterFirstName, _RenterLastName, _email, _phone, _rent, PayStatus);
                }
            }

            else
            {
                //cakel: BUGID00032 - added Send mail function for testing only.   should be reomved for production
                string PayStatus = "Payment Declined";
                decimal _amount = 0;
                int _propID = _View.PropertyId;
                int _resident = _View.ApplicantId;
                string _RenterFirstName = _View.PayerBillingInformationControl.FirstNameText.ToString();
                string _RenterLastName = _View.PayerBillingInformationControl.LastNameText.ToString();
                var _rent = new Renter(_View.PaymentTypeControl.RenterId);

                var _newApp = new Applicant(_resident);
                var _email = _newApp.PrimaryEmailAddress;
                var _phone = _newApp.MainPhoneNumber;

                SendEmailX2(_propID, _amount, _RenterFirstName, _RenterLastName, _email, _phone, _rent, PayStatus);
                

                _View.SafeRedirect(String.Format("~/FutureResidentsPaymentFailed.aspx?Message={0}", Response.Message));
            }

            //// ReSharper disable ConvertIfStatementToConditionalTernaryExpression
            //if (Response.Result == GeneralResponseResult.Success)
            //// ReSharper restore ConvertIfStatementToConditionalTernaryExpression
            //{
            //    _View.SafeRedirect(String.Format("~/FutureResidentsApplicationReceipt.aspx?TransactionId={0}&PropId={1}&ApplicantId={2}", Response.TransactionId, _View.PropertyId, _View.ApplicantId));
            //    SendEmail();
            //}
            //else
            //    _View.SafeRedirect("~/FutureResidentsPaymentFailed.aspx");
        }

        private void UploadButtonClicked(object sender, EventArgs e)
        {
            if (!_View.FileUploadControl.HasFile)
            {
                _View.DisplaySystemMessage("No file found to upload, please try again", SystemMessageType.Error);
                return;
            }

            if (!_View.FileUploadControl.PostedFile.FileName.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase))
            {
                _View.DisplaySystemMessage("File must be in PDF format to upload", SystemMessageType.Error);
                return;
            }

            ApplicantApplication.SetCompletedApplication(BuildApplicantApplication(), _View.FileUploadControl.FileBytes);
            _View.DisplaySystemMessage("Your file has successfully been uploaded", SystemMessageType.Success);
        }

        private int BuildApplicantApplication()
        {
            var Applicant = new Applicant(_View.ApplicantId);
            var ApplicantApplication = Applicant.Applications.FirstOrDefault(a => a.PropertyId == _View.PropertyId);

            var ApplicantApp = new ApplicantApplication
            {
                ApplicantApplicationId = ApplicantApplication != null ? ApplicantApplication.ApplicantApplicationId : 0,
                ApplicantId = _View.ApplicantId,
                PropertyId = _View.PropertyId,
                ApplicationStatusId = (int)ApplicationStatus.Pending,
                ApplicationSubmissionTypeId = (int) ApplicationSubmissionType.Online,
                ApplicationStatusDate = DateTime.UtcNow
            };

            return ApplicantApplication.Set(ApplicantApp);
        }

        private ApplicantPaymentResponse ProcessApplicationPayment()
        {
            var Applicant = new Applicant(_View.ApplicantId);

            if (Applicant.ApplicantId < 1)
            {
                _View.DisplaySystemMessage("No applicant found to process a payment against, please login and try again", SystemMessageType.Error);
                return new ApplicantPaymentResponse
                    {
                        Result = GeneralResponseResult.ApplicantNotFound, 
                        Message = ServiceResponseMessages.ApplicantNotFound, 
                        TransactionDate = DateTime.UtcNow
                    };
            }

            return ApplicantPaymentRequest.ProcessPayment(BuildApplicantPaymentInfo());
        }

        private ApplicantPaymentInfo BuildApplicantPaymentInfo()
        {
            var Fees = ApplicationFee.GetAllApplicationFeesByPropertyId(_View.PropertyId);
            var Application = new Applicant(_View.ApplicantId).Applications.FirstOrDefault(a => a.PropertyId == _View.PropertyId);

            return new ApplicantPaymentInfo
                {
                    Fees = Fees,
                    TypeOfPayment = _View.PaymentTypeControl.PaymentTypeSelectedValue == "1" ? PaymentType.CreditCard : PaymentType.ECheck,
                    CreditCardInfo = BuildCreditCardInfo(),
                    EcheckInfo = BuildEcheckInfo(),
                    PropertyId = _View.PropertyId,
                    ApplicantId = _View.ApplicantId,
                    ApplicantApplicationId = Application != null ? Application.ApplicantApplicationId : 0,

                    //Salcedo - 3/21/2014 - fixed a tiny but rather critical flaw brought to us courtesy of MB2x
                    //that was causing all our applicant fees to be processed in demo mode
                    //IsTestUser = Settings.UseTestMode || Settings.ProcessCreditCards
                    IsTestUser = EfxSettings.UseTestMode || !EfxSettings.ProcessCreditCards
                };
        }

        private CreditCardDetails BuildCreditCardInfo()
        {
            var ExpirationMonth = _View.PaymentTypeControl.CreditCardDetailsControl.ExpirationMonthSelectedValue.ToInt32();
            var ExpirationYear = _View.PaymentTypeControl.CreditCardDetailsControl.ExpirationYearList.SelectedItem.Text.ToInt32();

            return new CreditCardDetails
                {
                    BillingFirstName = _View.PayerBillingInformationControl.FirstNameText,
                    BillingLastName = _View.PayerBillingInformationControl.LastNameText,
                    BillingAddress = BuildBillingAddress(),
                    CardHolderName = _View.PaymentTypeControl.CreditCardDetailsControl.NameOnCardText,
                    CreditCardNumber = _View.PaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText,
                    ExpirationDate = new DateTime(ExpirationYear, ExpirationMonth, 1),
                    CvvCode = _View.PaymentTypeControl.CreditCardDetailsControl.CvvText
                };
        }

        private EcheckDetails BuildEcheckInfo()
        {
            return new EcheckDetails
                {
                    BillingFirstName = _View.PayerBillingInformationControl.FirstNameText,
                    BillingLastName = _View.PayerBillingInformationControl.LastNameText,
                    BillingAddress = BuildBillingAddress(),
                    BankAccountNumber = _View.PaymentTypeControl.ECheckDetailsControl.AccountNumberText,
                    BankRoutingNumber = _View.PaymentTypeControl.ECheckDetailsControl.RoutingNumberText,
                    TypeOfAccount = AccountType.Checking
                };
        }

        private Address BuildBillingAddress()
        {
            return new Address
                {
                    AddressLine1 = _View.PayerBillingInformationControl.BillingAddressText,
                    AddressLine2 = null,
                    City = _View.PayerBillingInformationControl.CityText,
                    StateProvinceId = _View.PayerBillingInformationControl.StateDropDown.SelectedValue.ToInt32(),
                    PostalCode = _View.PayerBillingInformationControl.ZipText
                };
        }
        

        private void SendEmailX()
        {
            var CurrentProperty = new Property(_View.PropertyId);

            var Staff = PropertyStaff.GetStaffMemberHierarchicaly(PropertyStaff.GetPropertyStaffListByPropertyId(_View.PropertyId), _View.PropertyId);
            if (!Staff.PropertyStaffId.HasValue || Staff.PropertyStaffId.Value < 1)
                return;

            //Salcedo - 2/28/2014 - changed to send email only to the property management email contacts
            var Recipients = Enumerable.Empty<MailAddress>().ToList();

            //BulkMail.BulkMail.SendMailMessage(null, "EFX", false, Settings.SendMailUsername, Settings.SendMailPassword, new MailAddress(Settings.ContactUsEmail),
            //    String.Format("Payment Received - Application Fees/Deposits - {0}", CurrentProperty.PropertyName), 
            //    String.Format(EmailTemplates.ApplicationPaymentConfirmation,!String.IsNullOrEmpty(Staff.DisplayName) ? Staff.DisplayName : "User" ), 
            //    new List<MailAddress>{new MailAddress(Staff.PrimaryEmailAddress)});
            BulkMail.BulkMail.SendMailMessageIncludePropertyEmailContacts(_View.PropertyId, "EFX", EfxSettings.SendMailUsername, EfxSettings.SendMailPassword, new MailAddress(EfxSettings.ContactUsEmail),
                String.Format("Payment Received - Application Fees/Deposits - {0}", CurrentProperty.PropertyName),
                String.Format(EmailTemplates.ApplicationPaymentConfirmation, !String.IsNullOrEmpty(Staff.DisplayName) ? Staff.DisplayName : "User"),
                Recipients);
        }


        //cakel: BUGID00032 added new fuction to use email template
        private static void SendEmailX2(int _PropertyID, decimal amount, string FirstName, string LastName, string PrimaryEmail, string MainPhone, Renter Resident, string PaymentStatus)
        {
            var CurrentProperty = new Property(_PropertyID);
            var Staff = PropertyStaff.GetStaffMemberHierarchicaly(PropertyStaff.GetPropertyStaffListByPropertyId(_PropertyID), _PropertyID);
            if (!Staff.PropertyStaffId.HasValue || Staff.PropertyStaffId.Value < 1)
                return;

            string Subject;
            string Body = "";
            
            var Property = EfxFramework.Property.GetPropertyByRenterId(Resident.RenterId);
            
            String RecipientName = Staff.DisplayName.ToString();
            
                String RenterProperty = CurrentProperty.PropertyName.ToString();
                String TransactionDate = DateTime.UtcNow.ToShortDateString();
                String RenterFullName = FirstName + " " + LastName;
                String PaymentAmount = amount.ToString("C");
                String PayDate = TransactionDate.ToString();
                int currentProp = (int)CurrentProperty.PropertyId;
                Subject = "Payment Transaction - Application Fees/Deposits: " + RenterFullName;

                //Body = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-Application-Payment.html").Replace("[[ROOT]]",
                //    EmailTemplateService.EmailTemplateService.RootPath), RecipientName, RenterProperty, FirstName, LastName, PayDate, PaymentAmount);

            Body = EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-Application-Payment.html").ToString();
            String NewBody = Body;
            NewBody = NewBody.Replace("##PropertyManager##", RecipientName);
            NewBody = NewBody.Replace("##PropertyName##", RenterProperty);
            NewBody = NewBody.Replace("##FirstName##", FirstName);
            NewBody = NewBody.Replace("##LastName##", LastName);
            NewBody = NewBody.Replace("##Email##", PrimaryEmail);
            NewBody = NewBody.Replace("##Phone##", MainPhone);
            NewBody = NewBody.Replace("##TransactionDate##", PayDate);
            NewBody = NewBody.Replace("##TotalFees##", PaymentAmount);
            NewBody = NewBody.Replace("##PaymentStatus##", PaymentStatus);

            var Recipients = Enumerable.Empty<MailAddress>().ToList();
            
            int IntPropertyId = Property.PropertyId.HasValue ? Property.PropertyId.Value : 0;
            BulkMail.BulkMail.SendMailMessageIncludePropertyEmailContacts(
                currentProp,
                "Payment",
                EfxSettings.SendMailUsername,
                EfxSettings.SendMailPassword,
                new MailAddress(EfxSettings.PaymentEmail),
                Subject,
                NewBody,
                //cakel: BUGID00032 rev 2 updated email to include applicants email
                new List<MailAddress> { new MailAddress(PrimaryEmail) }
                );



        }




    }
}
