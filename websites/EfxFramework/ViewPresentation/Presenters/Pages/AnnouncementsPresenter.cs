﻿using System.Web.UI.WebControls;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using Mb2x.ExtensionMethods;
using System.Web;

namespace EfxFramework.ViewPresentation.Presenters.Pages
{
    public class AnnouncementsPresenter
    {
        private readonly IAnnouncements _View;
        private static int? RenterId { get { return HttpContext.Current.User.Identity.Name.ToNullInt32(); } }

        public AnnouncementsPresenter(IAnnouncements view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            if (!RenterId.HasValue || RenterId.Value < 1)
            {
                _View.SafeRedirect("~/Account/Login.aspx");
            }
            else
            {
                var Property = EfxFramework.Property.GetPropertyByRenterId(RenterId.Value);

                if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                    return;

                _View.PropertyAnnouncementsListView = Announcement.GetActivePropertyAnnouncementsByPropertyId(Property.PropertyId.Value);
                _View.RpoAnnouncementsListView = Announcement.GetActiveAdminAnnouncements(Property.PropertyId.Value);
            }
        }    
    }
}
