﻿using System;
using EfxFramework.ViewPresentation.Interfaces.Pages;

namespace EfxFramework.ViewPresentation.Presenters.Pages
{
    public class PaymentIvrPresenter
    {
        private readonly IPaymentIvr _View;

        public void InitializeValues()
        {
            var CurrentPaymentQueue = PaymentQueue.GetPaymentQueueByPaymentQueueNumber(_View.PaymentQueueNumber);
            var CurrentProperty = Property.GetPropertyByRenterId(CurrentPaymentQueue.RenterId);
            var CurrentRenter = new Renter(CurrentPaymentQueue.RenterId);
            var State = CurrentRenter.StateProvinceId.HasValue ? (StateProvince)CurrentRenter.StateProvinceId.Value : StateProvince.AK;

            _View.PaymentQueueNumber = CurrentPaymentQueue.PaymentQueueNumber;
            _View.RentAmount = CurrentPaymentQueue.RentAmount.HasValue ? CurrentPaymentQueue.RentAmount.Value.ToString("C") : "$0.00";
            _View.FeeAmount = CurrentPaymentQueue.OtherAmount2.HasValue ? CurrentPaymentQueue.OtherAmount2.Value.ToString("C") : "$0.00";
            _View.TotalPayment = GetSumOfPayments(CurrentPaymentQueue);
            _View.PropertyName = CurrentProperty.PropertyName;
            _View.PropertyAddress = CurrentProperty.StreetAddress;
            _View.PropertyCityStateZip = string.Format("{0}, {1} {2}", CurrentProperty.City, CurrentProperty.DisplayStateProvince, CurrentProperty.PostalCode);
            _View.FirstName = CurrentRenter.FirstName;
            _View.MiddleName = CurrentRenter.MiddleName;
            _View.LastName = CurrentRenter.LastName;
            _View.Suffix = CurrentRenter.Suffix;
            _View.BillingAddress = CurrentRenter.StreetAddress;
            _View.BillingCityStateZip = string.Format("{0}, {1} {2}", CurrentRenter.City, State, CurrentRenter.PostalCode);
            _View.Phone = new PhoneNumber(CurrentRenter.MainPhoneNumber).ToString("{0}-{1}-{2}");
        }

        private static string GetSumOfPayments(PaymentQueue payment)
        {
            var Total = 0M;
            Total += payment.RentAmount.HasValue ? payment.RentAmount.Value : 0;
            Total += payment.OtherAmount1.HasValue ? payment.OtherAmount1.Value : 0;
            Total += payment.OtherAmount2.HasValue ? payment.OtherAmount2.Value : 0;
            Total += payment.OtherAmount3.HasValue ? payment.OtherAmount3.Value : 0;
            Total += payment.OtherAmount4.HasValue ? payment.OtherAmount4.Value : 0;
            Total += payment.OtherAmount5.HasValue ? payment.OtherAmount5.Value : 0;
            Total += payment.CharityAmount.HasValue ? payment.CharityAmount.Value : 0;
            
            return Total.ToString("C");
        }

        public PaymentIvrPresenter(IPaymentIvr view)
        {
            _View = view;

            if (string.IsNullOrEmpty(_View.PaymentQueueNumber))
                throw new Exception("Payment queue number was not found.");
        }
    }
}
