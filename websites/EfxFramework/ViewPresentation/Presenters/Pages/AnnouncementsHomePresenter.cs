﻿using EfxFramework.ViewPresentation.Interfaces.Pages;
using Mb2x.ExtensionMethods;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EfxFramework.ViewPresentation.Presenters.Pages
{
    public class AnnouncementsHomePresenter
    {
        private readonly IAnnouncementsHome _View;
        private static int? RenterId { get { return HttpContext.Current.User.Identity.Name.ToNullInt32(); } }
        private static Property Property { get { return RenterId.HasValue ? Property.GetPropertyByRenterId(RenterId.Value) : new Property(); } }

        public AnnouncementsHomePresenter(IAnnouncementsHome view)
        {
            _View = view;
            SetLocationInformation();
        }

        public void InitializeValues()
        {
            _View.PropertyManagerAnnouncementDataSource = GetAndFilterPropertyManagerAnnouncements();
            _View.RpoAnnouncementDataSource = GetAndFilterRpoAnnouncements();
        }

        private void SetLocationInformation()
        {
            if (!RenterId.HasValue)
                return;

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                return;

            _View.ZipCode = Property.PostalCode;
            _View.CityState = string.Format("{0}, {1}", Property.City.UrlEncode(), Property.DisplayStateProvince);
        }

        private static List<Announcement> GetAndFilterPropertyManagerAnnouncements()
        {
            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                return new List<Announcement>();

            return Announcement.GetActivePropertyAnnouncementsByPropertyId(Property.PropertyId.Value).OrderByDescending(pm => pm.AnnouncementDate).
                Take(EfxSettings.RecentAnnouncmentCount).ToList();
        }

        private static List<Announcement> GetAndFilterRpoAnnouncements()
        {
            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                return new List<Announcement>();

            return Announcement.GetActiveAdminAnnouncements(Property.PropertyId.Value).OrderByDescending(pm => pm.AnnouncementDate).
                Take(EfxSettings.RecentAnnouncmentCount).ToList();
        }
    }
}
