﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using EfxFramework.Resources;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using Mb2x.ExtensionMethods;
using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace EfxFramework.ViewPresentation.Presenters.Pages
{
    public class CurrentResidentsRegisterPresenter
    {
        //dwillis task 00220
        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }
        private static int PropId
        {
            get
            {
                var Id = HttpContext.Current.Request.QueryString["PropId"].ToNullInt32();
                return Id.HasValue ? Id.Value : 0;
            }
        }

        private readonly ICurrentResidentsRegister _View;

        public void InitializeValues()
        {
            //If no property id was found in query string, throw exception
            if (PropId < 1)
                throw new Exception("CurrentResidentsRegister: Property ID not found by query string.");

            _View.ResidentRegisterControl.PropertyName = new Property(PropId).PropertyName;
        }

        public CurrentResidentsRegisterPresenter(ICurrentResidentsRegister view)
        {
            _View = view;
            _View.ResidentRegisterControl.SubmitClick = SubmitPage;
            _View.ResidentRegisterControl.UnitNumberServerValidate = ValidateUnitNumber;
            _View.ResidentRegisterControl.ResidentIdNumberServerValidate = ValidateResidentIdNumber;
			_View.ResidentRegisterControl.DisplayResidentId = IsPropertyIntegrated(PropId);
        }

        protected void SubmitPage(object sender, EventArgs e)
        {
            //Validate the page
            if (!_View.ValidatePage())
                return;

            Renter retrievedRenter = GetRenterIdByPublicRenterId(_View.ResidentRegisterControl.ResidentIdNumber);
            
            //cakel: BUGID00011 - need to check for integrated
            bool IntegratedCheck = IsPropertyIntegrated(PropId);

            //cakel: BUGID00011 - Check resident if integrated property
            if (IntegratedCheck == true)
            {
                //cakel: BUGID00011 - check to make sure renter exists if renter is associated with integrated property
                if (retrievedRenter.RenterId != 0)
                {
                    if (!retrievedRenter.HasRegistered)
                    {
                        if (retrievedRenter.RenterId <= 0)
                        {
                            //Salcedo - 4/7/2014 - additional logic around registering
                            retrievedRenter = new Renter(_View.ResidentRegisterControl.Email);
                            if (retrievedRenter.RenterId > 0)
                            {
                                //LIkely the user has entered the wrong PMSId or the wrong PublicId, but since the email they entered already exists, display this error:
                                _View.ExistingUser.Visible = true;
                                _View.ExistingUser.Text = "You are trying to register with a username already in use. Please login using your credentials or contact support for help.";
                            }
                            else
                            {
                                BuildRenter(new Renter());
                            }
                        }
                        else
                        {   //Salcedo - task 00338 - 1/14/2015 - add logic to make sure email address is not already in the system
                            Renter testRenter = new Renter(_View.ResidentRegisterControl.Email);
                            if (testRenter.RenterId > 0)
                            {
                                //LIkely the user has entered the wrong PMSId or the wrong PublicId, but since the email they entered already exists, display this error:
                                _View.ExistingUser.Visible = true;
                                _View.ExistingUser.Text = "The email address you provided has been previously registered. Please login using your credentials or contact support for help.";
                            }
                            else
                            {
                                BuildRenter(retrievedRenter);
                            }
                        }
                    }
                    else
                    {
                        _View.ExistingUser.Visible = true;
                        _View.ExistingUser.Text = "An account associated with that Resident ID has already been registered, please check your ID and try again. You can also contact RentPaidOnline support team for further assistance.";
                    }
                }
                else
                {
                    //cakel: BUGID00011 - If no renter exists with ID provided display message to try again
                    _View.ExistingUser.Visible = true;
                    _View.ExistingUser.Text = "The Resdident ID supplied does not exist, please check your ID and try again. You can also contact RentPaidOnline support team for further assistance.";
                }
            } //cakel: BUGID00011 - End integrated check
            else
            {
                //cakel: BUGID00011 - if resident is not integrated execute this code
                if (!retrievedRenter.HasRegistered)
                {
                    if (retrievedRenter.RenterId <= 0)
                    {
                        //Salcedo - 4/7/2014 - additional logic around registering
                        retrievedRenter = new Renter(_View.ResidentRegisterControl.Email);
                        if (retrievedRenter.RenterId > 0)
                        {
                            //LIkely the user has entered the wrong PMSId or the wrong PublicId, but since the email they entered already exists, display this error:
                            _View.ExistingUser.Visible = true;
                            _View.ExistingUser.Text = "You are trying to register with a username already in use. Please login using your credentials or contact support for help.";
                        }
                        else
                        {
                            BuildRenter(new Renter());
                        }
                    }
                    else
                        BuildRenter(retrievedRenter);
                }
                else
                {
                    _View.ExistingUser.Visible = true;
                    _View.ExistingUser.Text = "An account associated with that Resident ID has already been registered, please check your ID and try again. You can also contact RentPaidOnline support team for further assistance.";
                }

            }



        }

        private static bool IsPropertyIntegrated(int propertyId)
        {
            //Get all properties
            var Prop = new Property(propertyId);
            return !string.IsNullOrEmpty(Prop.PmsId) && Prop.PmsTypeId.HasValue;
        }


        //cakel: Task 00456 -- Updated code below to return renters no matter what their PMS ID is
        private static Renter GetRenterIdByPublicRenterId(string renterId)
        {
            if (!string.IsNullOrEmpty(renterId))
            {
                long _RenterID = 0;

				//try to get by pms id
				var currentProperty = new Property(PropId);
				if (!string.IsNullOrEmpty(currentProperty.PmsId) && currentProperty.PmsTypeId.HasValue)
				{
                    //Salcedo - 2/20/2014 - add PropertyId to call
                    Renter retrievedRenter = Renter.GetRenterByPmsIdAndPmsTypeId(renterId, currentProperty.PmsTypeId.Value, PropId);
                    //cakel: TASK - 00456
                        _RenterID = retrievedRenter.PublicRenterId;
                 
					if (retrievedRenter != null && retrievedRenter.RenterId > 0 && retrievedRenter.PmsPropertyId == currentProperty.PmsId)
					{
						return retrievedRenter;
					}
				}

				//try to get by public renter id
				long renterIdInput = 0;
				int? ResidentId = null;
                //cake: TASK 00456
				if (long.TryParse(_RenterID.ToString(), out renterIdInput))
				{
					ResidentId = Renter.GetRenterIdByPublicRenterId(renterIdInput);
				}
                
                return ResidentId.HasValue ? new Renter(ResidentId) : new Renter();
            }

            return new Renter();
        }

        private void BuildRenter(Renter renter)
        {
            renter.RenterId = renter.RenterId;
            renter.FirstName = _View.ResidentRegisterControl.FirstName;
            renter.LastName = _View.ResidentRegisterControl.LastName;
            renter.Unit = _View.ResidentRegisterControl.UnitNumber;
            renter.MainPhoneNumber = _View.ResidentRegisterControl.PhoneNumber;
            renter.PrimaryEmailAddress = _View.ResidentRegisterControl.Email;
            renter.SetPassword(_View.ResidentRegisterControl.PasswordConfirm);

            var RenterId = Renter.Set(renter, "resident registration");

            Renter.AssignRenterToProperty(PropId, RenterId);
            EmailPropertyManager(RenterId);
			Renter.SetHasRegistered(renter.RenterId);

            if (!string.IsNullOrEmpty(_View.ResidentRegisterControl.ResidentIdNumber))
                _View.SafeRedirect(string.Format("~/SuccessfulRegistration.aspx{0}&{1}", "?PublicResidentId=" + _View.ResidentRegisterControl.ResidentIdNumber, "PropId=" + PropId));
            else
                _View.SafeRedirect("~/SuccessfulRegistration.aspx");
			//todo update successfulregistration.aspx if passing a publicresidentid value or just change the value sent to the page from here
        }

        protected void ValidateUnitNumber(object sender, ServerValidateEventArgs e)
        {
            if (!IsPropertyIntegrated(PropId) && string.IsNullOrEmpty(_View.ResidentRegisterControl.UnitNumber))
                e.IsValid = false;
            else
                e.IsValid = true;
        }

        protected void ValidateResidentIdNumber(object sender, ServerValidateEventArgs e)
        {
            if (IsPropertyIntegrated(PropId))
            {
				var currentProperty = new Property(PropId);
                //Salcedo - 2/20/2014 - add PropertyId to call
				Renter retrievedRenter = Renter.GetRenterByPmsIdAndPmsTypeId(_View.ResidentRegisterControl.ResidentIdNumber, currentProperty.PmsTypeId.Value, PropId);
				if (retrievedRenter != null && retrievedRenter.RenterId > 0)
				{
					e.IsValid = true;
				}

				if (!e.IsValid)
				{
					long renterIdInput = 0;
					int? ResidentId = null;
					if (long.TryParse(_View.ResidentRegisterControl.ResidentIdNumber, out renterIdInput))
					{
						ResidentId = Renter.GetRenterIdByPublicRenterId(renterIdInput);
						e.IsValid = ResidentId > 0;
					}
				}
            }

			return;
        }


        private static void EmailPropertyManager(int residentId)
        {
            var Resident = new Renter(residentId);
            if (Resident.RenterId < 1)
                return;
            //dwillis task 184
            var property = new Property(PropId);
            //If property is integrated with anyone, they will not receive an email. 
            if (property.PmsTypeId != null || property.PmsTypeId.ToString() != "")
            {
                return;
            }

            var Staff = PropertyStaff.GetStaffMemberHierarchicaly(PropertyStaff.GetPropertyStaffListByPropertyId(PropId), PropId);
            if (!Staff.PropertyStaffId.HasValue || Staff.PropertyStaffId.Value < 1)
                return;

            //Salcedo - 2/28/2014 - changed to send email only to the property management email contacts
            var Recipients = Enumerable.Empty<MailAddress>().ToList();

            //dwillis task 00220
            SqlConnection connTemplate = new SqlConnection(ConnString);
            //CMallory - Modified For DomusMe
            SqlCommand cmdTemplate = new SqlCommand("SELECT TEMPLATETEXT FROM EMAILTEMPLATES WHERE TEMPLATEID = 32", connTemplate);
            connTemplate.Open();
            DataTable dt = new DataTable();
            dt.Load(cmdTemplate.ExecuteReader());
            connTemplate.Close();
            var result = dt.Rows[0]["TEMPLATETEXT"].ToString();
            string result1 = result.Replace("##NAME##", Staff.FirstName +" "+ Staff.LastName);
            string result2 = result1.Replace("##RESFNAME##", Resident.FirstName);
            
           result2 = result2.Replace("##RESLNAME##", Resident.LastName);
            result2 = result2.Replace("##RESUNIT##", Resident.Unit.ToString());
           result2 =  result2.Replace("##RESEMAIL##", Resident.PrimaryEmailAddress);//Cmallory - Task 00530 and Task 00455 - Corrected the name of the RESEmail Value so that it'd populate the email correctly.
           result2 = result2.Replace("[[ROOT]]", EmailTemplateService.EmailTemplateService.RootPath);

            //BulkMail.BulkMail.SendMailMessage(PropId, "Property Manager", false, Settings.SendMailUsername, Settings.SendMailPassword, new MailAddress(Settings.SupportEmail),
            //    "New Resident Registration", String.Format(EmailTemplates.PropertyManagerSetupLease, Staff.DisplayName, Resident.FirstName, Resident.LastName, Resident.Unit, Resident.PrimaryEmailAddress),
            //    new List<MailAddress> {new MailAddress(Staff.PrimaryEmailAddress)});
            BulkMail.BulkMail.SendMailMessageIncludePropertyEmailContacts(PropId, "Property Manager", EfxSettings.SendMailUsername, EfxSettings.SendMailPassword, new MailAddress(EfxSettings.SupportEmail),
                "New Resident Registration", result2, Recipients);
        }
    }
}
