﻿using EfxFramework.ViewPresentation.Interfaces.Pages;
using System;
using System.Globalization;
using System.Web.Security;

namespace EfxFramework.ViewPresentation.Presenters.Pages
{
    public class LoginPresenter<T> where T : BaseUser, new()
    {
        private readonly ILoginV2 _View;

        public LoginPresenter(ILoginV2 view)
        {
            _View = view;
            _View.LoginButtonClick = ProcessLogin;
        }

        private void ProcessLogin(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_View.UsernameText) && !string.IsNullOrEmpty(_View.PasswordText))
            {
                var User = new T().GetUserByEmailAddress(_View.UsernameText);

                //User.IsValid checks for a null UserId
                if (User.IsValid && User.Authenticate(_View.PasswordText))
                {
                    // ReSharper disable PossibleInvalidOperationException
                    var renter = new Renter(User.UserId);
                    // Salcedo - 1/20/2014 - fixed problem with a null renter object preventing a new applicant from logging in to complete an application
                    if (!renter.IsActive && renter.IsValid)
                    {
                        _View.DisplaySystemMessage("Your account is no longer active.", SystemMessageType.Error);
                    }
                    else
                    {
                        FormsAuthentication.SetAuthCookie(User.UserId.Value.ToString(CultureInfo.InvariantCulture), true);
                        _View.LoggedInUserId = User.UserId.Value;
                    }
                    // ReSharper restore PossibleInvalidOperationException
                }
                else
                {
                    _View.DisplaySystemMessage("Invalid username/password, please try again.", SystemMessageType.Error);
                }
            }
            else
            {
                _View.DisplaySystemMessage("Username or password missing, please try again.", SystemMessageType.Error);
            }
        }
    }
}
