﻿using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web.Security;

namespace EfxFramework.ViewPresentation.Presenters.Pages
{
    public class FutureResidentRegisterPresenter
    {
        private readonly IFutureResidentRegister _View;

        public FutureResidentRegisterPresenter(IFutureResidentRegister view)
        {
            _View = view;
            _View.ResidentRegisterControl.SubmitClick = SubmitClick;

            _View.ForgotPasswordControl.SubmitClick = ForgotPasswordSubmitClick;
        }

        public void InitializeValues()
        {
            //If no PropId value found in the query string, throw exception
            if (_View.PropertyId < 1)
                throw new Exception("FutureResidentsRegister: Property ID not found by query string.");

            _View.ResidentRegisterControl.PropertyName = new Property(_View.PropertyId).PropertyName;
        }

        private void ForgotPasswordSubmitClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_View.ForgotPasswordControl.Email))
            {
                BasePageV2.DisplaySystemMessage(_View.ForgotPasswordControl.ParentPage, "Email address is required.", SystemMessageType.Information);
                return;
            }

            var CurrentUser = new Applicant(_View.ForgotPasswordControl.Email);

            if (!CurrentUser.IsValid)
            {
                BasePageV2.DisplaySystemMessage(_View.ForgotPasswordControl.ParentPage, "No user was found with this email address.", SystemMessageType.Information);
                return;
            }

            var NewPassword = CurrentUser.ResetPassword();
            CurrentUser.SetUser(CurrentUser, "Password Reset Tool");
            
            //Salcedo - 3/12/2014 - replaced with new html email template
            var Message = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-password-reset.html").Replace("[[ROOT]]",
                EmailTemplateService.EmailTemplateService.RootPath), CurrentUser.DisplayName, NewPassword);

            BulkMail.BulkMail.SendMailMessage(
                null,
                "EFX",
                false,
                EfxSettings.SendMailUsername,
                EfxSettings.SendMailPassword,
                new MailAddress(EfxSettings.SupportEmail),
                "Forgot Password",
                Message,
                new List<MailAddress> { new MailAddress(_View.ForgotPasswordControl.Email) }
                );

            BasePageV2.DisplaySystemMessage(_View.ForgotPasswordControl.ParentPage, "An email has been sent with your new temporary password. <br/>", SystemMessageType.Information);
        }

        public void SaveApplication(int applicantId)
        {
            var Applicant = new Applicant(applicantId);

            if (Applicant.ApplicantId < 1)
                return;

            var Applications = Applicant.Applications.Where(a => a.PropertyId == _View.PropertyId);

            if (Applications.Any())
            {
                Redirect(Applicant.ApplicantId);
            }
            else
            {
                if (!CreateApplication(Applicant))
                    _View.DisplaySystemMessage("Failed to create application record.", SystemMessageType.Error);
                else
                    Redirect(Applicant.ApplicantId);
            }
        }

        private void Redirect(int applicantId)
        {
            _View.SafeRedirect(String.Format("~/FutureResidentsDownloadApplication.aspx?PropId={0}&ApplicantId={1}", _View.PropertyId, applicantId));
        }

        private void SubmitClick(object sender, EventArgs e)
        {
            if (!_View.ValidatePage())
                return;

            string ErrorMessage;
            var Applicant = BuildApplicant(out ErrorMessage);

            if (Applicant.ApplicantId < 1)
            {
                _View.DisplaySystemMessage(ErrorMessage, SystemMessageType.Error);
                return;
            }

            if (!CreateApplication(Applicant))
            {
                _View.DisplaySystemMessage("Failed to create application record.", SystemMessageType.Error);
                return;
            }

            ProcessLogin();
            _View.SafeRedirect(string.Format("~/FutureResidentsDownloadApplication.aspx?PropId={0}&ApplicantId={1}", _View.PropertyId, Applicant.ApplicantId));
        }

        private Applicant BuildApplicant(out string errorMessage)
        {
            errorMessage = String.Empty;

            var Applicant = new Applicant
                {
                    FirstName = _View.ResidentRegisterControl.FirstName, 
                    LastName = _View.ResidentRegisterControl.LastName,
                    MainPhoneNumber = _View.ResidentRegisterControl.PhoneNumber,
                    PrimaryEmailAddress = _View.ResidentRegisterControl.Email
                };

            
            Applicant.SetPassword(_View.ResidentRegisterControl.PasswordConfirm);

            try
            {
                Applicant = new Applicant(Applicant.Set(Applicant));
            }
            catch (SqlException Ex)
            {
                errorMessage = Ex.Number == 2601 || Ex.Number == 2627 || Ex.Number == 50000 ? "An account with this email already exists." : "Failed to create the applicant record.";
            }
            catch
            {
                errorMessage = "Failed to create the applicant record.";
            }

            return Applicant;
        }

        private bool CreateApplication(Applicant applicant)
        {
            var App = new ApplicantApplication
                {
                    ApplicantId = applicant.ApplicantId,
                    ApplicationStatusId = (int)ApplicationStatus.Pending,
                    PropertyId = _View.PropertyId,
                    FeesPaid = false,
                    ApplicationSubmissionTypeId = (int) ApplicationSubmissionType.Online,
                    ApplicationDate = DateTime.UtcNow,
                    ApplicationStatusDate = DateTime.UtcNow
                };

            try
            {
                ApplicantApplication.Set(App);
            }
            catch
            {
                return false;
            }

            return true;
        }

        private void ProcessLogin()
        {
            if (!string.IsNullOrEmpty(_View.ResidentRegisterControl.Email) && !string.IsNullOrEmpty(_View.ResidentRegisterControl.Password))
            {
                var User = new Applicant(_View.ResidentRegisterControl.Email);

                //User.IsValid checks for a null UserId
                if (User.IsValid && User.Authenticate(_View.ResidentRegisterControl.Password))
                {
                    // ReSharper disable PossibleInvalidOperationException
                    FormsAuthentication.SetAuthCookie(User.UserId.Value.ToString(CultureInfo.InvariantCulture), true);
                    (_View as ILoginV2).LoggedInUserId = User.UserId.Value;
                    // ReSharper restore PossibleInvalidOperationException
                }
                else
                {
                    _View.DisplaySystemMessage("Invalid username/password, please try again.", SystemMessageType.Error);
                }
            }
            else
            {
                _View.DisplaySystemMessage("Username or password missing, please try again.", SystemMessageType.Error);
            }
        }
    }
}
