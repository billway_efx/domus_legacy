﻿using System.Collections.Generic;
using EfxFramework.ViewPresentation.Interfaces.Pages.BlogPages;

namespace EfxFramework.ViewPresentation.Presenters.Pages.BlogPresenters
{
    internal class PublicProsCornerPresenter : BlogPresenterBase
    {
        private readonly IPublicProsCorner _View;

        public PublicProsCornerPresenter(IPublicProsCorner view)
            : base(view)
        {
            _View = view;
        }

        protected override List<Announcement> AnnouncementType()
        {
            return Announcement.GetAllActiveProsCornerAnnouncements();
        }
    }
}
