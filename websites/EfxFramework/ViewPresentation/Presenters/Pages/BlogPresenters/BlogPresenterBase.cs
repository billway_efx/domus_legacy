﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using EfxFramework.ViewPresentation.Interfaces.Pages.BlogPages;
using EfxFramework.ViewPresentation.Interfaces.UserControls.Blogs;

namespace EfxFramework.ViewPresentation.Presenters.Pages.BlogPresenters
{
    public abstract class BlogPresenterBase
    {
        protected IBlogPage BaseView;

        protected BlogPresenterBase(IBlogPage view)
        {
            BaseView = view;
            BaseView.BlogViewUserControl.ArchiveItemDataBound = ArchiveItemDataBound;
        }

        public virtual void InitializeValues()
        {
            RecentPost();
            DisplayArchived();
            LoadDefaultPost();
        }

        protected virtual void LoadDefaultPost()
        {
            var DefaultPost = AnnouncementType().OrderByDescending(blog => blog.AnnouncementDate).FirstOrDefault();

            if (DefaultPost == null)
                return;

            ScriptManager.RegisterStartupScript(BaseView.BlogViewUserControl.ParentPage,
                GetType(), "DefaultPost", String.Format("ShowPostData('{0}', '{1}', '{2}')", DefaultPost.BlogText.Replace("'", "\\'"), DefaultPost.AnnouncementDateShortDate.Replace("'", "\\'"), DefaultPost.BlogTitle.Replace("'", "\\'")), true);
        }

        protected virtual void RecentPost()
        {
            BaseView.BlogViewUserControl.RecentBlogPostListUserControl.WireUpDataBinding();
            BaseView.BlogViewUserControl.RecentBlogPostListUserControl.BlogPostList.DataSource = AnnouncementType().OrderByDescending(blog => blog.AnnouncementDate).Where(blog => blog.IsRecentPostDate(blog.AnnouncementDate.Value));
            BaseView.BlogViewUserControl.RecentBlogPostListUserControl.BlogPostList.DataBind();
        }

        protected abstract List<Announcement> AnnouncementType();

        protected virtual void DisplayArchived()
        {
            var Query = from Blog in AnnouncementType().Where(blog => blog.IsArchivedDate(EfxSettings.ArchivedYears))
                        orderby Blog.AnnouncementDate descending
                        group Blog by Blog.ArchiveDate

                            into Archive
                            select new
                            {
                                Year = Archive.Key,
                                Count = Archive.Count(),
                            };

            BaseView.BlogViewUserControl.ArchivedBlogPostList.DataSource = Query;
            BaseView.BlogViewUserControl.ArchivedBlogPostList.DataBind();
        }
        
        protected virtual void ArchiveItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var Year = e.Item.FindControl("ArchiveYear") as Label;
            var HideControl = e.Item.FindControl("HidePost") as HtmlControl;
            var ArchiveYearLink = e.Item.FindControl("ArchiveYearLink") as HtmlAnchor;
            if (Year == null || HideControl == null || ArchiveYearLink == null)
                return;

            ArchiveYearLink.Attributes.Add("onclick", String.Format("$('#{0}').toggle();", HideControl.ClientID));

            var ArchiveBlogPost = e.Item.FindControl("ArchiveBlogPosts") as IBlogPostListing;
            if (ArchiveBlogPost == null)
                return;

            ArchiveBlogPost.WireUpDataBinding();
            ArchiveBlogPost.BlogPostList.DataSource = AnnouncementType().FindAll(blog => blog.ArchiveDate == Year.Text).OrderByDescending(blog => blog.AnnouncementDate);
            ArchiveBlogPost.BlogPostList.DataBind();
        }
    }
}
