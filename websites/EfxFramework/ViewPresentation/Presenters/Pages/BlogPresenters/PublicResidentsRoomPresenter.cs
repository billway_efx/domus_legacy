﻿using System.Collections.Generic;
using EfxFramework.ViewPresentation.Interfaces.Pages.BlogPages;

namespace EfxFramework.ViewPresentation.Presenters.Pages.BlogPresenters
{
    internal class PublicResidentsRoomPresenter : BlogPresenterBase
    {
        private readonly IPublicResidentsRoom _View;

        public PublicResidentsRoomPresenter(IPublicResidentsRoom view)
            : base(view)
        {
            _View = view;
        }

        protected override List<Announcement> AnnouncementType()
        {
            return Announcement.GetAllActiveResidentsRoomAnnouncements();
        }
    }
}
