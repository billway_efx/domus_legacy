﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using EfxFramework.ViewPresentation.Interfaces.Pages.BlogPages;
using Mb2x.ExtensionMethods;

namespace EfxFramework.ViewPresentation.Presenters.Pages.BlogPresenters
{
    internal class PublicNewsAndEventsPresenter : BlogPresenterBase
    {
        private readonly IPublicNewsAndEvents _View;

        public PublicNewsAndEventsPresenter(IPublicNewsAndEvents view)
            : base(view)
        {
            _View = view;
        }

        protected override void LoadDefaultPost()
        {
            if (_View.NewsBlogUrl.ToNullInt32() > 0)
            {
                var DefaultPost = Announcement.GetAnnouncementById(_View.NewsBlogUrl.ToInt32());

                if (DefaultPost == null)
                {
                    base.LoadDefaultPost();
                    return;
                }
                if (DefaultPost.IsActive && DefaultPost.AnnouncementTypeId == 5 || DefaultPost.AnnouncementTypeId == 6)
                {
                    ScriptManager.RegisterStartupScript(BaseView.BlogViewUserControl.ParentPage,
                        GetType(), "DefaultPost", String.Format("ShowPostData('{0}', '{1}', '{2}')", DefaultPost.BlogText.Replace("'", "\\'"), DefaultPost.AnnouncementDateShortDate.Replace("'", "\\'"), DefaultPost.BlogTitle.Replace("'", "\\'")), true);
                    return;
                }
            }

            base.LoadDefaultPost();
        }

        protected override List<Announcement> AnnouncementType()
        {
            return Announcement.GetAllActiveNewsAndEvents();
        }
    }
}
