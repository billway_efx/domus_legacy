﻿using EfxFramework.ExtensionMethods;
using EfxFramework.PaymentMethods;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.Web;
using EfxFramework.Web.Facade;
using EfxFramework.WebPayments;
using Mb2x.ExtensionMethods;
using RPO;
using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Presenters.Pages
{
    public class PaymentPresenter
    {
        private readonly IPayment _View;
        private bool _IsSave;
        private Renter _Resident;
        private Payer _Payer;

        private ActivityLog AL = new ActivityLog();

        private Renter Resident
        {
            get
            {
                var Id = HttpContext.Current.User.Identity.Name.ToNullInt32();

                if (!Id.HasValue || Id.Value < 1)
                    BasePageV2.SafePageRedirect("~/Account/Login.aspx");

                if (_Resident == null || _Resident.RenterId < 1)
                    _Resident = new Renter(Id.HasValue ? Id.Value : 0);

                if (_Resident.RenterId < 1)
                    BasePageV2.SafePageRedirect("~/Account/Login.aspx");

                return _Resident;
            }
        }

        private Payer Payer
        {
            get
            {
                if (_Payer == null || _Payer.PayerId < 1)
                    _Payer = new Payer(Resident.PayerId.HasValue ? Resident.PayerId.Value : 0);

                return _Payer.PayerId < 1 ? new Payer() : _Payer;
            }
        }

        public PaymentPresenter(IPayment view)
        {
            _View = view;
            _View.PaymentSaveButtonClick = SavePaymentMethods;
            _View.PaymentPayNowButtonClick = PayNow;
            _View.PaymentCancelButtonClick = Cancel;
            SetModalText();
            _View.RequiredCvv = SetRequiredCvv;
        }

        private void SetModalText()
        {
            _View.ConfirmationWindowControl.ConfirmationHeaderText = "Confirmation";
            _View.ConfirmationWindowControl.ConfirmationLabelText = String.Format("AutoPay setup has been completed. Your payments will automatically process on the {0} of each month",
                _View.AutoPayTileControl.DayOfTheMonthDropdown.SelectedValue.ToNthValue());
            _View.ConfirmationWindowControl.LinkUrl = "~/Payments.aspx";
        }

        private void SavePaymentMethods(object sender, EventArgs e)
        {
            if (!_View.TermsAccepted)
            {
                _View.DisplaySystemMessage("You must accept the terms before we can save your information.", SystemMessageType.Error);
                return;
            }

            _IsSave = true;
            if (!BasePageV2.ValidatePage(_View.ViewPage))
                return;

            var CreditCard = SaveCreditCardInfo();
            var Echeck = SaveEcheckInfo();
            SaveBillingAddress();

            if (_View.TextToPayTileControl.IsOn)
                SaveTextToPay();
            else
                TurnOffTextToPay();

            if (_View.AutoPayTileControl.IsOn)
            {
                SaveAutoPay(CreditCard, Echeck);
                ScriptManager.RegisterStartupScript(_View.ViewPage, _View.ViewPage.GetType(), "test", "<script type='text/javascript'>ShowModal(\"#ConfirmationWindowRequest\");</script>", false);
            }
            else
            {
                TurnOffAutoPay();
                ScriptManager.RegisterStartupScript(_View.ViewPage, _View.ViewPage.GetType(), "test", "<script type='text/javascript'>ShowModal(\"#CancellationWindowRequest\");</script>", false);
            }
        }

        private void PayNow(object sender, EventArgs e)
        {
            if (!_View.TermsAccepted)
            {
                _View.DisplaySystemMessage("You must accept the terms before we can process your payment.", SystemMessageType.Error);
                return;
            }

            _IsSave = false;
            if (!BasePageV2.ValidatePage(_View.ViewPage))
                return;

            ProcessPayment();
        }

        private void TurnOffAutoPay()
        {
            var AutoPay = AutoPayment.GetAutoPaymentByRenterId(Resident.RenterId);
            if (AutoPay.AutoPaymentId < 1)
                return;

            //TODO: When AutoPayment has an Active bit, just set it to false
            AutoPayment.DeleteAutoPayment(AutoPay.AutoPaymentId);
        }

        private void TurnOffTextToPay()
        {
            Resident.SmsPaymentPhoneNumber = null;
            Resident.SmsPaymentTypeId = null;
            Resident.SmsReminderDayOfMonth = null;
            Renter.Set(Resident, UIFacade.GetAuthenticatedUserString(UserType.Renter));
        }

        private void Cancel(object sender, EventArgs e)
        {
            _View.SafeRedirect("~/Default.aspx");
        }

        private int SaveCreditCardInfo()
        {
            var CreditCard = PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(Payer.PayerId);
            var CardNumber = _View.SelectPaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText;

            CreditCard.PayerId = Payer.PayerId;
            CreditCard.CreditCardHolderName = _View.SelectPaymentTypeControl.CreditCardDetailsControl.NameOnCardText;
            CreditCard.CreditCardAccountNumber = !CardNumber.Contains("*") ? CardNumber : CreditCard.CreditCardAccountNumber;
            CreditCard.CreditCardExpirationMonth = _View.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationMonthSelectedValue.ToInt32();
            CreditCard.CreditCardExpirationYear = _View.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationYearList.SelectedValue.ToInt32();
            CreditCard.IsPrimary = true;
            CreditCard.ReminderSent = false;

            return PayerCreditCard.Set(CreditCard);
        }

        private int SaveEcheckInfo()
        {
            var Echeck = PayerAch.GetPrimaryPayerAchByPayerId(Payer.PayerId);
            var AccountNumber = _View.SelectPaymentTypeControl.ECheckDetailsControl.AccountNumberText;

            Echeck.PayerId = Payer.PayerId;
            Echeck.BankAccountNumber = !AccountNumber.Contains("*") ? AccountNumber : Echeck.BankAccountNumber;
            Echeck.BankRoutingNumber = _View.SelectPaymentTypeControl.ECheckDetailsControl.RoutingNumberText;
            Echeck.IsPrimary = true;

            return PayerAch.Set(Echeck);
        }

        private void SaveBillingAddress()
        {
            var AddressId = EfxFramework.Address.GetAddressByRenterIdAndAddressType(Resident.RenterId, (int) TypeOfAddress.Billing).AddressId;

            var Address = new Address(AddressId)
                {
                    AddressTypeId = (int)TypeOfAddress.Billing,
                    AddressLine1 = _View.ResidentBillingInformationControl.BillingAddressText,
                    City = _View.ResidentBillingInformationControl.CityText,
                    StateProvinceId = _View.ResidentBillingInformationControl.StateDropDown.SelectedValue.ToInt32(),
                    PostalCode = _View.ResidentBillingInformationControl.ZipText,
                    PrimaryPhoneNumber = PhoneNumber.RemoveFormatting(_View.PersonalInformationControl.PhoneNumberText)
                };

            AddressId = EfxFramework.Address.Set(Address);
            EfxFramework.Address.AssignAddressToRenter(Resident.RenterId, AddressId);

            //Salcedo Task 00076
            var Log = new RPO.ActivityLog();
            Log.WriteLog(_View.ViewPage.Page.User.Identity.Name, "Address has been updated (Payment)", 0, Resident.RenterId.ToString(), true);
        }

        private void SaveAutoPay(int payerCreditCardId, int payerAchId)
        {
            var AutoPay = AutoPayment.GetAutoPaymentByRenterId(Resident.RenterId);
            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(Resident.RenterId);
            if (Lease.LeaseId < 1)
            {
                _View.DisplaySystemMessage("Lease not found.  Unable to save Auto Payment.", SystemMessageType.Error);
                return;
            }

            AutoPay.PaymentDayOfMonth = _View.AutoPayTileControl.DayOfTheMonthDropdown.SelectedValue.ToInt32();
            AutoPay.PaymentTypeId = _View.AutoPayTileControl.PaymentTypeDropdown.SelectedValue.ToInt32();
            AutoPay.RenterId = Resident.RenterId;
            AutoPay.PayerAchId = payerAchId;
            AutoPay.PayerCreditCardId = payerCreditCardId;
            AutoPay.PayerId = Payer.PayerId;
            AutoPay.RentAmount = Lease.RentAmount + LeaseFee.GetFeesByLeaseId(Lease.LeaseId).Sum(lf => lf.FeeAmount).Value;
            AutoPay.RentAmountDescription = "Rent And Fees";
            AutoPay.OtherAmount2 = _View.PaymentAmountsControl.ConvenienceFeeAmount;
            AutoPay.OtherDescription2 = "Convenience Fee";
            AutoPayment.Set(AutoPay);
        }

        private void SaveTextToPay()
        {
            Resident.SmsPaymentPhoneNumber = PhoneNumber.RemoveFormatting(_View.TextToPayTileControl.MobileNumberText);
            Resident.SmsPaymentTypeId = _View.TextToPayTileControl.PaymentTypeDropdown.SelectedValue.ToInt32();
            Resident.SmsReminderDayOfMonth = _View.TextToPayTileControl.DayOfTheMonthDropdown.SelectedValue.ToInt32();
            Renter.Set(Resident, UIFacade.GetAuthenticatedUserString(UserType.Renter));
        }

        //TODO Replacing old PaymentResponse with new one
        //Salcedo - this function is not used
        //private void ProcessPaymentV2()
        //{
        //    var Property = EfxFramework.Property.GetPropertyByRenterId(Resident.RenterId);
        //    if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
        //    {
        //        _View.DisplaySystemMessage("Property Not Found, cannot process the payment.", SystemMessageType.Error);
        //        return;
        //    }

        //    if (_View.PaymentAmountsControl.RentAmount < 0.01M)
        //    {
        //        _View.DisplaySystemMessage("There is currently no positive amount to be paid.  Payment cannot be accepted.", SystemMessageType.Error);
        //        _View.EnablePayNow = false;
        //        return;
        //    }

        //    var Response = RentPaymentRequestV2.ProcessPayment(PaymentChannel.ResidentSite, BuildPaymentInfo(Property.PropertyId.Value));

        //    if (Response.Result == (int)GeneralResponseResult.Success)
        //        _View.SafeRedirect(String.Format("~/PaymentStatus.aspx?TransactionId={0}", Response.TransactionId));
        //        //TODO Get PaymentQue
        //    else if (Response.Result == (int)GeneralResponseResult.Success)
        //        _View.SafeRedirect(String.Format("~/PaymentIvr.aspx?PaymentQueueNumber={0}", Response.Message));
        //    else
        //        _View.SafeRedirect("~/PaymentFailed.aspx");
        //}

        private void ProcessPayment()
        {
            //Salcedo - task 000076
            AL.WriteLog(_View.ViewPage.Page.User.Identity.Name, "Payment initiated.", (short)LogPriority.LogAlways, Resident.RenterId.ToString(), true);

            var Property = EfxFramework.Property.GetPropertyByRenterId(Resident.RenterId);
            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                _View.DisplaySystemMessage("Property Not Found, cannot process the payment.", SystemMessageType.Error);
                //Salcedo - task 000076
                AL.WriteLog(_View.ViewPage.Page.User.Identity.Name, "Payment failed because Property was not found.", (short)LogPriority.LogAlways, 
                    Resident.RenterId.ToString(), true);
                return;
            }

            if (_View.PaymentAmountsControl.RentAmount < 0.01M)
            {
                _View.DisplaySystemMessage("There is currently no positive amount to be paid.  Payment cannot be accepted.", SystemMessageType.Error);
                //Salcedo - task 000076
                AL.WriteLog(_View.ViewPage.Page.User.Identity.Name, "Payment cannot be accepted because there is currently no positive amount to be paid.", (short)LogPriority.LogAlways
                    , Resident.RenterId.ToString(), true);
                _View.EnablePayNow = false;
                return;
            }

			Company currentCompany = Company.GetCompanyByRenterId(Resident.RenterId);

			if (_View.SelectPaymentTypeControl.PaymentTypeSelectedValue == "3")
			{
				if (Resident != null && currentCompany != null && !string.IsNullOrEmpty(currentCompany.PNMSiteId) && !string.IsNullOrEmpty(currentCompany.PNMSecretKey))
				{
                    //Salcedo - task 00337 - 1/12/2015 - moved appSettings to database
					//decimal PNMLimit = ConfigurationManager.AppSettings["PNMLimit"].ToDecimal();
                    decimal PNMLimit = EfxSettings.PNMLimit.ToDecimal();

					decimal amountToCharge = Math.Round(_View.PaymentAmountsControl.RentAmount, 2);
					if (amountToCharge <= PNMLimit)
					{
						var Address = EfxFramework.Address.GetAddressByRenterIdAndAddressType(Resident.RenterId, (int)TypeOfAddress.Billing);
						Address.AddressTypeId = (int)TypeOfAddress.Billing;
						Address.AddressLine1 = _View.ResidentBillingInformationControl.BillingAddressText;
						Address.City = _View.ResidentBillingInformationControl.CityText;
						Address.StateProvinceId = _View.ResidentBillingInformationControl.StateDropDown.SelectedValue.ToInt32();
						Address.PostalCode = _View.ResidentBillingInformationControl.ZipText;
						Address.PrimaryEmailAddress = _View.PersonalInformationControl.EmailAddressText;
						Address.PrimaryPhoneNumber = PhoneNumber.RemoveFormatting(_View.PersonalInformationControl.PhoneNumberText);

						string creator = "";
						if (EfxFramework.Helpers.Helper.IsRpoAdmin)
						{
							EfxAdministrator currentAdmin = new EfxAdministrator();
							BaseUser baseAdmin = currentAdmin.GetUserById(EfxFramework.Helpers.Helper.CurrentUserId);
							creator = baseAdmin.FirstName + " " + baseAdmin.LastName;
						}
						else
						{
							PropertyStaff currentPS = new PropertyStaff(EfxFramework.Helpers.Helper.CurrentUserId);
							creator = currentPS.FirstName + " " + currentPS.LastName;
						}

						PayNearMeRequest pnmRequest = new PayNearMeRequest();
						pnmRequest.Amount = amountToCharge;
						pnmRequest.CreatorIdentifier = creator;
						pnmRequest.Currency = "USD";
						pnmRequest.CustomerCity = Address.City;
						pnmRequest.CustomerEmail = Address.PrimaryEmailAddress;
						pnmRequest.CustomerIdentifier = Resident.PublicRenterId.ToString();
						pnmRequest.CustomerName = Resident.FirstName + " " + Resident.LastName;
						pnmRequest.CustomerPhone = Address.PrimaryPhoneNumber;
						pnmRequest.CustomerPostalCode = Address.PostalCode;
						pnmRequest.CustomerState = _View.ResidentBillingInformationControl.StateDropDown.SelectedItem.Text;
						pnmRequest.CustomerStreet = Address.AddressLine1;
						pnmRequest.MinimumPaymentAmount = 1;
						pnmRequest.MinimumPaymentCurrency = "USD";
						pnmRequest.OrderDescription = "Resident Payment";
                        //CMallory - Task 00281 - Changed OrderDuration from 1 to 7
						pnmRequest.OrderDuration = 7;
						pnmRequest.OrderIdentifier = Guid.NewGuid().ToString();
						pnmRequest.OrderType = PayNearMeRequest.PNMOrderType.Exact;
						pnmRequest.RenterId = Resident.RenterId;
						pnmRequest.PropertyId = Property.PropertyId.Value;
						pnmRequest.CompanyId = currentCompany.CompanyId;
						pnmRequest.SiteIdentifier = currentCompany.PNMSiteId;
						pnmRequest.SecretKey = currentCompany.PNMSecretKey;
                        //Salcedo - task 00337 - 1/12/2015 - moved appSettings to database
						//pnmRequest.Version = ConfigurationManager.AppSettings["PNMVersion"].ToString();
                        pnmRequest.Version = EfxSettings.PNMVersion;

						//Update the associated address
						var AddressId = EfxFramework.Address.Set(Address);
						EfxFramework.Address.AssignAddressToRenter(Resident.RenterId, AddressId);

                        //Salcedo Task 00076
                        var Log = new RPO.ActivityLog();
                        Log.WriteLog(_View.ViewPage.Page.User.Identity.Name, "Address has been updated (VPNM)", 0, Resident.RenterId.ToString(), true);

						RentByCash rentByCash = new RentByCash();
                        //Salcedo - task 00337 - 1/12/2015 - moved appSettings to database
						//rentByCash.SandBoxMode = ConfigurationManager.AppSettings["PNMUseSandboxMode"].ToBool();
                        rentByCash.SandBoxMode = EfxSettings.PNMUseSandboxMode.ToBool();

						string response = rentByCash.CreatePayNearMeRequest(pnmRequest, rentByCash.SandBoxMode);

						if (response.ToLower().Equals("error"))
						{
							EfxFramework.Logging.ExceptionLogger.LogException(new Exception("Received an error from PayNearMe"));
                            //Salcedo - task 000076
                            AL.WriteLog(_View.ViewPage.Page.User.Identity.Name, "Received an error from PayNearMe.", (short)LogPriority.LogAlways
                                , Resident.RenterId.ToString(), true);
						}
						else
						{
							String csname1 = "PNMPopupScript";
							Type cstype = _View.ViewPage.GetType();
							ClientScriptManager cs = _View.ViewPage.ClientScript;

							if (!cs.IsStartupScriptRegistered(cstype, csname1))
							{
								//set the window address to the new url
								String cstext1 = "window.open('" + response + "','_blank');";
								cs.RegisterStartupScript(cstype, csname1, cstext1, true);
							}
						}
					}
					else
					{
						//display an error for pnm setup

                        //Salcedo - task 00337 - 1/12/2015 - moved appSettings to database
						//string pnmLimitError = Decimal.Parse(ConfigurationManager.AppSettings["PNMLimit"].ToString()).ToString("C");
                        string pnmLimitError = Decimal.Parse(EfxSettings.PNMLimit).ToString("C");

						CustomValidator err = new CustomValidator();
						err.IsValid = false;
						err.ErrorMessage = "You cannot create a RentByCash payment greater than " + pnmLimitError;
                        //Salcedo - task 000076
                        AL.WriteLog(_View.ViewPage.Page.User.Identity.Name, "Received a validation error from PayNearMe: " + err.ErrorMessage, (short)LogPriority.LogAlways
                            , Resident.RenterId.ToString(), true);
						_View.ViewPage.Validators.Add(err);
						return;
					}
				}
				else
				{
					//display an error for pnm setup
					CustomValidator err = new CustomValidator();
					err.IsValid = false;
					err.ErrorMessage = "You must select a Property and a Resident to use Rent By Cash.  If you have selected both, please have an RPO administrator verify the parent company is configured to use Rent by Cash.";
                    AL.WriteLog(_View.ViewPage.Page.User.Identity.Name, "Validation error: " + err.ErrorMessage, (short)LogPriority.LogAlways);
					_View.ViewPage.Validators.Add(err);
					return;
				}
			}
			else
			{
				var Response = PaymentRequest.ProcessPayment(BuildPaymentInfo(Property.PropertyId.Value));

                //CMallory - Test
                Response.RentAndFeeResult = GeneralResponseResult.GeneralFailure;

                //Salcedo - 1/18/2014 - added support for displaying error results to user
                if (Response.RentAndFeeResult == PaymentMethods.GeneralResponseResult.Success)
                {
                    if (!Response.RedirectToIvr)
                    {
                        //Salcedo - task 000076
                        AL.WriteLog(_View.ViewPage.Page.User.Identity.Name, "Successful payment. Redirecting for confirmation message to " +
                            String.Format("~/PaymentStatus.aspx?TransactionId={0}", Response.RentAndFeeTransactionId), (short)LogPriority.LogAlways, Resident.RenterId.ToString(), true);
                        _View.SafeRedirect(String.Format("~/PaymentStatus.aspx?TransactionId={0}", Response.RentAndFeeTransactionId));
                    }
                    else
                    {
                        //Salcedo - task 000076
                        AL.WriteLog(_View.ViewPage.Page.User.Identity.Name, "Payment successfully queued. Redirecting for confirmation to " +
                            String.Format("~/PaymentIvr.aspx?PaymentQueueNumber={0}", Response.PaymentQueueNumber), (short)LogPriority.LogAlways, Resident.RenterId.ToString(), true);
                        _View.SafeRedirect(String.Format("~/PaymentIvr.aspx?PaymentQueueNumber={0}", Response.PaymentQueueNumber));
                    }
                }
                else
                {
                    //string CharityFailureMessage = "";
                    string RentAndFeeFailureMessage = "";
                    //if (Response.CharityResult != PaymentMethods.GeneralResponseResult.Success)
                    //{
                    //    CharityFailureMessage = Response.CharityResponseMessage;
                    //}
                    //else
                    //{
                    //    if (Response.CharityTransactionId != null)
                    //        CharityFailureMessage = "Success";
                    //}
                    if (Response.RentAndFeeResult != PaymentMethods.GeneralResponseResult.Success)
                    {
                        RentAndFeeFailureMessage = Response.RentAndFeeResponseMessage;
                    }
                    //Salcedo - task 000076
                    AL.WriteLog(_View.ViewPage.Page.User.Identity.Name, "Payment failed. Redirecting for failure message to " + 
                        String.Format("~/PaymentFailed.aspx?RentAndFeeFailureMessage={0}", RentAndFeeFailureMessage), (short)LogPriority.LogAlways, Resident.RenterId.ToString(), true);
                    BasePage.SafePageRedirect(String.Format("~/PaymentFailed.aspx?RentAndFeeFailureMessage={0}", RentAndFeeFailureMessage));
                }
                //if (Response.RentAndFeeResult == PaymentMethods.GeneralResponseResult.Success && !Response.RedirectToIvr)
                //    _View.SafeRedirect(String.Format("~/PaymentStatus.aspx?TransactionId={0}", Response.RentAndFeeTransactionId));
                //else if (Response.RentAndFeeResult == PaymentMethods.GeneralResponseResult.Success && Response.RedirectToIvr)
                //    _View.SafeRedirect(String.Format("~/PaymentIvr.aspx?PaymentQueueNumber={0}", Response.PaymentQueueNumber));
                //else
                //    _View.SafeRedirect("~/PaymentFailed.aspx");
            }
        }

        private RenterPaymentInfo BuildPaymentInfo(int propertyId)
        {
            var CardNumber = _View.SelectPaymentTypeControl.CreditCardDetailsControl.CreditCardNumberText;
            var AccountNumber = _View.SelectPaymentTypeControl.ECheckDetailsControl.AccountNumberText;
            var CreditCard = PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(Payer.PayerId);
            var Echeck = PayerAch.GetPrimaryPayerAchByPayerId(Payer.PayerId);

            if (CardNumber.Contains("*"))
                CardNumber = CreditCard.CreditCardAccountNumber;
            if (AccountNumber.Contains("*"))
                AccountNumber = Echeck.BankAccountNumber;

            return new RenterPaymentInfo
                {
                    RenterId = Resident.RenterId,
                    PaymentType = (EfxFramework.PaymentMethods.PaymentType)_View.SelectPaymentTypeControl.PaymentTypeSelectedValue.ToInt32(),
                    BankAccountNumber = AccountNumber,
                    BankRoutingNumber = _View.SelectPaymentTypeControl.ECheckDetailsControl.RoutingNumberText,
                    CreditCardHolderName = _View.SelectPaymentTypeControl.CreditCardDetailsControl.NameOnCardText,
                    CreditCardNumber = CardNumber,
                    Cvv = _View.SelectPaymentTypeControl.CreditCardDetailsControl.CvvText,
                    ExpirationDate = new DateTime(_View.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationYearList.SelectedValue.ToInt32(), _View.SelectPaymentTypeControl.CreditCardDetailsControl.ExpirationMonthSelectedValue.ToInt32(), 1),
                    RentAmount = _View.PaymentAmountsControl.RentAmount,
                    RentAmountDescription = "Rent and Fees",
                    OtherAmount2 = _View.PaymentAmountsControl.ConvenienceFeeAmount,
                    OtherAmountDescription2 = "Convenience Fee",
                    DisplayName = String.Format("{0} {1}", _View.PersonalInformationControl.FirstNameText, _View.PersonalInformationControl.LastNameText),
                    PayerId = Payer.PayerId,
                    PropertyId = propertyId
                };
        }

        private void SetRequiredCvv(object sender, ServerValidateEventArgs arg)
        {
            if (_View.SelectPaymentTypeControl.CreditCardDetailsControl.CvvText.Length < 1 && _IsSave == false && _View.SelectPaymentTypeControl.PaymentTypeSelectedValue == "1")
                arg.IsValid = false;
        }
    }
}
