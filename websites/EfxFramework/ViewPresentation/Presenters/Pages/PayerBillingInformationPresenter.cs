﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;

namespace EfxFramework.ViewPresentation.Presenters.Pages
{
    public class PayerBillingInformationPresenter
    {
        private readonly IPayerBillingInformation _View;

        public PayerBillingInformationPresenter(IPayerBillingInformation view)
        {
            _View = view;    
        }

        public void InitializeValues()
        {
            //Initialize StateDropDown values
            var StateList = StateProvinceTable.GetAllStates();
            _View.StateDropDown.DataSource = StateList;
            _View.StateDropDown.DataBind();
        }
    }
}
