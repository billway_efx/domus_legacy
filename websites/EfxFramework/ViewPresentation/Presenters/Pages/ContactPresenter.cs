﻿using System;
using System.Collections.Generic;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using System.Net.Mail;
using EfxFramework.Resources;
using Mb2x.ExtensionMethods;


namespace EfxFramework.ViewPresentation.Presenters.Pages
{
    public class ContactPresenter
    {
        private readonly IContact _View;

        public ContactPresenter(IContact view)
        {            
            _View = view;
            _View.ContactUserControl.SendButtonClick = SendEmailX;
        }

        public void InitializeValues()
        {
            _View.ContactRpoRadioButtonList.SelectedValue = _View.ContactType;
            _View.ContactRpoRadioButtonList.Visible = true;
            _View.ContactUserControl.SetContactMultiView = 0;
        }

        private void SendEmailX(object sender, EventArgs e)
        {
            if (_View.ContactRpoRadioButtonList.SelectedIndex < 0)
                _View.ContactRpoRadioButtonList.SelectedIndex = 4;
            _View.ValidatePage();
            if (!_View.ValidatePage()) return;
            SendEmailX();
            GenerateLead();            
            ClearForm();
            _View.ContactRpoRadioButtonList.Visible = false;
            _View.ContactUserControl.SetContactMultiView = 1;
        }

        private void SendEmailX()
        {
            if (!_View.ContactUserControl.EmailAddressText.IsValidEmailAddress())
                return;

            BulkMail.BulkMail.SendMailMessage(null, "EFX", false, EfxSettings.SendMailUsername, EfxSettings.SendMailPassword, new MailAddress(_View.ContactUserControl.EmailAddressText),
               String.Format("\"Contact Us\" Message from {0}, {1}", _View.ContactRpoRadioButtonList.SelectedItem.Text, _View.ContactUserControl.NameText), String.Format(EmailTemplates.PublicContactFormV2, 
               _View.ContactRpoRadioButtonList.SelectedItem.Text, _View.ContactUserControl.NameText, _View.ContactUserControl.EmailAddressText, _View.ContactUserControl.PhoneNumberText, 
               _View.ContactUserControl.PropertyNameText, _View.ContactUserControl.MessageText, "", ""), new List<MailAddress> { new MailAddress(EfxSettings.ContactUsEmail), 
               new MailAddress(_View.ContactUserControl.EmailAddressText) });
        }

        private void ClearForm()
        {
            _View.ContactRpoRadioButtonList.ClearSelection();
            _View.ContactUserControl.EmailAddressText = String.Empty;
            _View.ContactUserControl.MessageText = String.Empty;
            _View.ContactUserControl.NameText = String.Empty;
            _View.ContactUserControl.PhoneNumberText = String.Empty;
            _View.ContactUserControl.PropertyNameText = String.Empty;
        }

        private void GenerateLead()
        {
            var Name = _View.ContactUserControl.NameText.Split(' ');

            if (Name.Length < 1)
            {
                _View.DisplaySystemMessage("Name is required.", SystemMessageType.Information);
                return;
            }

            var FirstName = Name[0].Trim();
            string LastName = String.Empty;

            if (Name.Length > 1)
            {
                for (var I = 1; I < Name.Length; I++)
                {
                    LastName = String.Format("{0} {1}", LastName, Name[I]);
                }
            }
     
            LastName = !String.IsNullOrEmpty(LastName) ? LastName.Trim() : LastName;

            var NewLead = new Lead
            {
                FirstName = FirstName,
                LastName = LastName,
                EmailAddress = _View.ContactUserControl.EmailAddressText,
                Description = "Lead generated from Public site Contact Form",
                DateSubmitted = DateTime.Now,
                PhoneNumber = !String.IsNullOrEmpty(_View.ContactUserControl.PhoneNumberText) ? _View.ContactUserControl.PhoneNumberText : null,
                PropertyName = !String.IsNullOrEmpty(_View.ContactUserControl.PropertyNameText) ? _View.ContactUserControl.PropertyNameText : null,
                LeadTypeId = 2
            };

            try
            {
                Lead.Set(NewLead);
            }
            catch
            {
                _View.SafeRedirect("/Contact.aspx");
            }
        }

    }
}
