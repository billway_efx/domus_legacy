﻿using EfxFramework.ViewPresentation.Interfaces.Pages;
using System;
using System.Web;

namespace EfxFramework.ViewPresentation.Presenters.Pages
{
    public class DownloadApplicationPresenter
    {
        private readonly IApplicationDownload _View;

        public DownloadApplicationPresenter(IApplicationDownload view)
        {
            _View = view;
            _View.DownloadButtonClick = DownloadFile;
        }

        public void InitializeValues()
        {
            if (_View.PropertyId < 1)
                throw new Exception("FutureResidentsDownloadApplication: Property ID was not found in the query string.");

            var Property = new Property(_View.PropertyId);
            var IsValidProperty = !(!Property.PropertyId.HasValue || Property.PropertyId.Value < 1);

            _View.PropertyImageUrl = String.Format("~/Handlers/PhotoHandler.ashx?RequestType=0&propertyId={0}&photoIndex=0", Property.PropertyId.HasValue ? Property.PropertyId.Value : 0);
            _View.PropertyNameText = String.Format("{0} {1}, {2} {3}", Property.PropertyName, Property.City, GetState(Property.StateProvinceId), Property.PostalCode);
            _View.NextButtonUrlText = String.Format("~/FutureResidentsUploadApplications.aspx?PropId={0}&ApplicantId={1}", _View.PropertyId, _View.ApplicantId);

            SetControlVisibility(IsValidProperty);
        }

        private void SetControlVisibility(bool isValidProperty)
        {
            if (!isValidProperty)
            {
                _View.NoPropertyFoundVisible = true;
                _View.ApplicationDownloadPanelVisible = false;
            }
            else
            {
                _View.NoPropertyFoundVisible = false;
                _View.ApplicationDownloadPanelVisible = true;
            }
        }

        private void DownloadFile(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Write("<script>");
            HttpContext.Current.Response.Write(String.Format("window.open('Handlers/ApplicationDownloadHandler.ashx?PropertyId={0}&IsPropertyApp=1','_blank')", _View.PropertyId));
            HttpContext.Current.Response.Write("</script>");
        }

        private string GetState(int? stateProvinceId)
        {
            if (!stateProvinceId.HasValue || stateProvinceId.Value < 1)
                return " ";

            return ((StateProvince) stateProvinceId.Value).ToString();
        }
    }
}
