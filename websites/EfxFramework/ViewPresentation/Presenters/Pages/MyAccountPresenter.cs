﻿using EfxFramework.ExtensionMethods;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using EfxFramework.Web.Facade;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Presenters.Pages
{
    public class MyAccountPresenter
    {
        private readonly IMyAccount _View;

        public MyAccountPresenter(IMyAccount view)
        {
            _View = view;

            if (_View.UserId < 1)
                throw new Exception("User ID could not be found");
            
            _View.PinValidation = ValidatePin;
            _View.PasswordValidation = ValidatePassword;
            _View.SaveButtonClicked = SaveButtonClick;
        }

        public void InitializeValues()
        {
            _View.StateDropDown.BindToSortedEnum(typeof (StateProvince));
            var Property = EfxFramework.Property.GetPropertyByRenterId(_View.UserId);
            _View.IsRentReportersIdVisible = Property.IsVerifiedRentReportersProperty;

            SetAddressMultiView(Property);
            PopulateFields();
        }

        private void SetAddressMultiView(Property property)
        {


            if (string.IsNullOrEmpty(property.PmsId) || !property.PmsTypeId.HasValue)
                _View.AddressView = 0;
            else
                _View.AddressView = 1;
        }

        private void PopulateFields()
        {
            var Resident = new Renter(_View.UserId);

            _View.ResidentIdText = Resident.PublicRenterId.ToString(CultureInfo.InvariantCulture);
            _View.RentReportersId = Resident.RentReporterId;
            _View.Email = Resident.PrimaryEmailAddress;
            _View.FirstName = Resident.FirstName;
            _View.MiddleName = Resident.MiddleName;
            _View.LastName = Resident.LastName;
            _View.Suffix = Resident.Suffix;
            _View.MainPhone = Resident.MainPhoneNumber;
            _View.AltPhone = Resident.MobilePhoneNumber;
            _View.CurrentPin = Resident.AccountCodePin;
            _View.PhotoUrl = String.Format("~/Handlers/PhotoHandler.ashx?RequestType=0&renterId={0}", Resident.RenterId);
            
            SetAddress(Resident);
        }

        private void SetAddress(Renter currentResident)
        {
            if (_View.AddressView == 0)
            {
                _View.MailingAddress = currentResident.StreetAddress;
                _View.MailingCity = currentResident.City;
                _View.StateDropDown.SelectedValue = currentResident.StateProvinceId.HasValue ? currentResident.StateProvinceId.Value.ToString(CultureInfo.InvariantCulture) : "1";
                _View.MailingZip = currentResident.PostalCode;
                return;
            }

            var State = currentResident.StateProvinceId.HasValue ? ((StateProvince) currentResident.StateProvinceId.Value).ToString() : "N/A";
            _View.IntegratedMailingAddress = currentResident.StreetAddress;
            _View.IntegratedCityStateZip = string.Format("{0}, {1} {2}", currentResident.City, State, currentResident.PostalCode);
        }

        private void ValidatePin(object sender, ServerValidateEventArgs e)
        {
            e.IsValid = true;

            if (!string.IsNullOrEmpty(_View.NewPin))
                e.IsValid = string.Equals(_View.NewPin, _View.NewPinConfirm, StringComparison.Ordinal);
        }

        private void ValidatePassword(object sender, ServerValidateEventArgs e)
        {
            e.IsValid = true;

            if (!string.IsNullOrEmpty(_View.Password))
                e.IsValid = string.Equals(_View.Password, _View.PasswordConfirm, StringComparison.Ordinal);
        }

        private void SaveButtonClick(object sender, EventArgs e)
        {
            if (_View.IsRefresh)
                return;

            if (!_View.ValidatePage())
                return;

            SavePhoto();
            var CurrentRenter = new Renter(_View.UserId);

            if (_View.AddressView == 0)
            {
                CurrentRenter.City = _View.MailingCity;
                CurrentRenter.StreetAddress = _View.MailingAddress;
                CurrentRenter.StateProvinceId = _View.StateDropDown.SelectedValue.ToInt32();
                CurrentRenter.PostalCode = _View.MailingZip;
            }

            CurrentRenter.RentReporterId = _View.RentReportersId;
            CurrentRenter.PrimaryEmailAddress = _View.Email;
            CurrentRenter.FirstName = _View.FirstName;
            CurrentRenter.MiddleName = _View.MiddleName;
            CurrentRenter.LastName = _View.LastName;
            CurrentRenter.Suffix = _View.Suffix;
            CurrentRenter.MainPhoneNumber = _View.MainPhone;
            CurrentRenter.MobilePhoneNumber = _View.AltPhone;

            if (!string.IsNullOrEmpty(_View.NewPin))
                CurrentRenter.AccountCodePin = _View.NewPinConfirm;

            if (!string.IsNullOrEmpty(_View.Password))
                CurrentRenter.SetPassword(_View.PasswordConfirm);

            Renter.Set(CurrentRenter, UIFacade.GetAuthenticatedUserString(UserType.Renter));
            _View.DisplaySystemMessage("Account sucessfully updated.", SystemMessageType.Success);
        }

        private void SavePhoto()
        {
            if (_View.ImageAdderControl.FileUploadBytes != null)
            {
                Renter.SetPhoto(_View.UserId, _View.ImageAdderControl.FileUploadBytes, UIFacade.GetAuthenticatedUserString(UserType.Renter));
            }
            else
            {
                var Photos = HttpContext.Current.Session[SessionVariable.ImageBytes.ToString()] as List<Photo>;

                if (Photos != null && Photos.Count > 0)
                    Renter.SetPhoto(_View.UserId, Photos[0].PhotoBytes, UIFacade.GetAuthenticatedUserString(UserType.Renter));
            }

            _View.ImageAdderControl.ClearPhotos();
        }
    }
}
