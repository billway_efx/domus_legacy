﻿using EfxFramework.ViewPresentation.Interfaces.Pages;
using System;

namespace EfxFramework.ViewPresentation.Presenters.Pages
{
    public class PaymentStatusPresenter
    {
        private readonly IPaymentStatus _View;

        public void InitializeValues()
        {
            var Payment = GetPaymentForView();
            if (Payment.PaymentId < 1)
                return;

            var CurrentProperty = Property.GetPropertyByRenterId(Payment.RenterId);
            if (!CurrentProperty.PropertyId.HasValue || CurrentProperty.PropertyId.Value < 1)
                return;

            var Resident = new Renter(Payment.RenterId);
            var Transaction = EfxFramework.Transaction.GetTransactionByExternalTransactionId(Payment.TransactionId);
            BuildReceiptView(Transaction, Payment, CurrentProperty, Resident);
        }

        public Payment GetPaymentForView()
        {
            var Transaction = EfxFramework.Transaction.GetTransactionByInternalTransactionId(_View.TransactionId);

            if (Transaction.TransactionId < 1)
                Transaction = EfxFramework.Transaction.GetTransactionByExternalTransactionId(_View.TransactionId);

            return Transaction.TransactionId < 1 ? Payment.GetPaymentByTransactionId(_View.TransactionId) : new Payment(Transaction.PaymentId);
        }

        private void BuildReceiptView(Transaction transaction, Payment payment, Property property, Renter resident)
        {
            var Address = GetResidentAddress(resident);

            _View.TransactionId = transaction.TransactionId < 1 ? payment.TransactionId : transaction.InternalTransactionId;
            _View.RentAmount = payment.RentAmount.HasValue ? payment.RentAmount.Value.ToString("C") : "$0.00";
            _View.ConvenienceFee = payment.OtherAmount2.HasValue && (!String.IsNullOrEmpty(payment.OtherDescription2) && payment.OtherDescription2.ToLower() == "convenience fee") ? payment.OtherAmount2.Value.ToString("C") : "$0.00";
            _View.PaymentTotal = GetTotalPayment(payment).ToString("C");
            _View.PropertyName = property.PropertyName;
            _View.PropertyAddress = GetPropertyAddressString(property);
            _View.FirstName = resident.FirstName;
            _View.MiddleName = resident.MiddleName;
            _View.LastName = resident.LastName;
            _View.Suffix = resident.Suffix;
            _View.BillingAddress = String.Format("{0} {1}", Address.AddressLine1, Address.AddressLine2);
            _View.BillingCityStateZip = string.Format("{0}, {1} {2}", Address.City, Address.GetStateAbbeviation(), Address.PostalCode);
            _View.PhoneNumber = new PhoneNumber(resident.MainPhoneNumber).ToString("{0}-{1}-{2}");
            _View.PaymentMethod = ((RenterPaymentType)payment.PaymentTypeId).ToString();
        }

        private static Address GetResidentAddress(Renter resident)
        {
            var BillingAddress = Address.GetAddressByRenterIdAndAddressType(resident.RenterId, (int)TypeOfAddress.Billing);
            if (BillingAddress.AddressId > 0)
                return BillingAddress;

            return new Address
            {
                AddressTypeId = (int)TypeOfAddress.Primary,
                AddressLine1 = resident.StreetAddress,
                AddressLine2 = resident.Unit,
                City = resident.City,
                StateProvinceId = resident.StateProvinceId.HasValue ? resident.StateProvinceId.Value : 1,
                PostalCode = resident.PostalCode
            };
        }

        private static string GetPropertyAddressString(Property property)
        {
            var PropertyState = property.StateProvinceId.HasValue ? ((StateProvince) property.StateProvinceId.Value).ToString() : "N/A";
            var Address = string.Format("{0} {1}<br />", property.StreetAddress, property.StreetAddress2);
            Address = String.Format("{0}{1}, {2} {3}", Address, property.City, PropertyState, property.PostalCode);
            return Address;
        }

        private static decimal GetTotalPayment(Payment trans)
        {
            var Total = 0.0M;

            Total += trans.RentAmount.HasValue ? trans.RentAmount.Value : 0;
            Total += trans.CharityAmount.HasValue ? trans.CharityAmount.Value : 0;
            Total += trans.OtherAmount1.HasValue ? trans.OtherAmount1.Value : 0;
            Total += trans.OtherAmount2.HasValue ? trans.OtherAmount2.Value : 0;
            Total += trans.OtherAmount3.HasValue ? trans.OtherAmount3.Value : 0;
            Total += trans.OtherAmount4.HasValue ? trans.OtherAmount4.Value : 0;
            Total += trans.OtherAmount5.HasValue ? trans.OtherAmount5.Value : 0;

            return Total;
        }

        private void PrintButtonClicked(object sender, EventArgs e)
        {
            
        }

        public PaymentStatusPresenter(IPaymentStatus view)
        {
            _View = view;
            _View.PrintClicked = PrintButtonClicked;

            //cakel: added function to check if local for CSS style without running a transaction
            bool isLocalDev = System.Web.HttpContext.Current.Request.IsLocal;

            if (isLocalDev == false)
            {
                if (string.IsNullOrEmpty(_View.TransactionId))
                    throw new Exception("No Transaction ID could be found");
            }
            
        }
    }
}
