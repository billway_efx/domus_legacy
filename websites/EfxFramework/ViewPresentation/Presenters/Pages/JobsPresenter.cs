﻿using EfxFramework.Resources;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using Mb2x.ExtensionMethods;
using Mb2xMail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace EfxFramework.ViewPresentation.Presenters.Pages
{
    public class JobsPresenter
    {
        private readonly IJobs _View;

        public JobsPresenter(IJobs view)
        {
            _View = view;
            _View.JobsListedSelectedIndexChanged = JobsListSelectIndexChange;
            _View.ApplyNowModal.SendButtonClick = SendEmailX;
        }

        public void InitializeValues()
        {
            BindListBox();
            _View.ApplyNowModal.ToText = EfxSettings.ContactUsEmail;
        }
        
        private void BindListBox()
        {
            //Bind JobList to control
            _View.JobsListed.DataSource = JobPosting.GetAllActiveJobPostings().OrderBy(job => job.JobTitle);
            _View.JobsListed.DataBind();

            if (_View.JobsListed.Items.Count > 1)
            {
                _View.ShowText = true;
                _View.JobsListed.Visible = true;
                _View.AreJobsAvailable = false;
                _View.JobsListed.SelectedValue = _View.JobId.ToString();
            }

            if (_View.JobsListed.Items.Count > 1)
                PopulateFields(_View.JobId < 1 ? _View.JobsListed.Items[0].Value.ToInt32() : _View.JobId);
        }

        private void PopulateFields(int jobId)
        {
            var Job = JobPosting.GetAllActiveJobPostings().FirstOrDefault(j => j.JobPostingId == jobId);

            if (Job == null)
                return;

            _View.JobTitleText = Job.JobTitle;
            _View.JobPostDateText = Job.DatePosted.ToString("MM/d/y");
            _View.JobDescriptionLiteralText = Job.JobDescription;
            _View.DutiesLiteralText = Job.PrincipleDuties;
            _View.SkillsLiteralText = Job.SkillsAndAbilities;
            _View.ApplyNowModal.SubjectText = _View.JobTitleText;
            _View.JobPostingVisible = true;
        }

        private void JobsListSelectIndexChange(object sender, EventArgs e)
        {
            if (_View.JobsListed.SelectedIndex == 0)
            {
                _View.JobPostingVisible = false;
            }
            else
            {
                _View.JobPostingVisible = true;
                PopulateFields(_View.JobsListed.SelectedValue.ToInt32());
            }
        }

        private void SendEmailX(object sender, EventArgs e)
        {
            if (!_View.ValidatePage())
                return;
            SendEmailX();
            _View.DisplaySystemMessage("Your Email was successfully sent", SystemMessageType.Success);
        }

        private void SendEmailX()
        {
            var AcceptedFormats = new List<string>
                                      {
                                          ".pdf",
                                          ".doc",
                                          ".docx",
                                          ".rtf",
                                      };
            var FileBytes = _View.ApplyNowModal.FileUploadControl.FileBytes;
            var FileUploadName = _View.ApplyNowModal.FileUploadControl.FileName;

            if (FileBytes.Length > 0 && AcceptedFormats.Any(FileUploadName.EndsWith))
            {
                String ContentType = "";
                if (FileUploadName.EndsWith("pdf")) ContentType = "application/pdf";
                if (FileUploadName.EndsWith("doc")) ContentType = "application/msword";
                if (FileUploadName.EndsWith("docx")) ContentType = "application/msword";
                if (FileUploadName.EndsWith("rtf")) ContentType = "application/rtf";

                BulkMail.BulkMail.SendMailMessage(null,
                                                  "EFX",
                                                  false,
                                                  EfxSettings.SendMailUsername,
                                                  EfxSettings.SendMailPassword,
                                                  new MailAddress(_View.ApplyNowModal.FromText),
                                                  String.Format("Job Application for {0} from {1}",
                                                                _View.ApplyNowModal.SubjectText,
                                                                _View.ApplyNowModal.FromText),
                                                  String.Format(EmailTemplates.PublicContactFormV2,
                                                                _View.ApplyNowModal.SubjectText,
                                                                null,
                                                                _View.ApplyNowModal.FromText,
                                                                null,
                                                                null,
                                                                _View.ApplyNowModal.BodyText,
                                                                null,
                                                                null),
                                                  new List<MailAddress> { new MailAddress(EfxSettings.ContactUsEmail) },
                                                  new List<Attachment> { new Attachment(_View.ApplyNowModal.FileUploadControl.FileContent, ContentType) });
                BulkMail.BulkMail.SendMailMessage(null,
                                                  "EFX",
                                                  false,
                                                  EfxSettings.SendMailUsername,
                                                  EfxSettings.SendMailPassword,
                                                  new MailAddress(_View.ApplyNowModal.FromText),
                                                  String.Format("Re: Your Application for {0}",
                                                                _View.ApplyNowModal.SubjectText),
                                                  String.Format(EmailTemplates.JobApplicationEmail),
                                                  new List<MailAddress> { new MailAddress(_View.ApplyNowModal.FromText) });
                _View.SafeRedirect("~/Jobs.aspx");
            }
            else if (FileBytes.Length < 1)
            {
                BulkMail.BulkMail.SendMailMessage(null,
                                                  "EFX",
                                                  false,
                                                  EfxSettings.SendMailUsername,
                                                  EfxSettings.SendMailPassword,
                                                  new MailAddress(_View.ApplyNowModal.FromText),
                                                  String.Format("Contact Us Message From {0} {1}",
                                                                _View.ApplyNowModal.SubjectText,
                                                                _View.ApplyNowModal.FromText),
                                                  String.Format(EmailTemplates.PublicContactFormV2,
                                                                _View.ApplyNowModal.SubjectText,
                                                                null,
                                                                _View.ApplyNowModal.FromText,
                                                                null,
                                                                null,
                                                                _View.ApplyNowModal.BodyText,
                                                                null,
                                                                null),
                                                  new List<MailAddress> { new MailAddress(EfxSettings.ContactUsEmail) });

                BulkMail.BulkMail.SendMailMessage(null,
                                                  "EFX",
                                                  false,
                                                  EfxSettings.SendMailUsername,
                                                  EfxSettings.SendMailPassword,
                                                  new MailAddress(_View.ApplyNowModal.FromText),
                                                  String.Format("Re: Your Application for {0}",
                                                                _View.ApplyNowModal.SubjectText),
                                                  String.Format(EmailTemplates.JobApplicationEmail),
                                                  new List<MailAddress> { new MailAddress(_View.ApplyNowModal.FromText) });
                _View.SafeRedirect("~/Jobs.aspx");
            }
            else
                _View.DisplaySystemMessage("The file upload format must be one of the following:<br/>.pdf, .doc, .docx, .rtf", SystemMessageType.Information);
        }
    }
}
