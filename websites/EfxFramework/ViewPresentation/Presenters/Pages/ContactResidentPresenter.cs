﻿using EfxFramework.Resources;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.UI.WebControls;
using Mb2x.ExtensionMethods;
using System.Web;

namespace EfxFramework.ViewPresentation.Presenters.Pages
{
    public class ContactResidentPresenter
    {
        private readonly IContactResident _View;
        private readonly int RenterId = HttpContext.Current.User.Identity.Name.ToInt32();
        //CMallory - Task 00539 - Created CurrentPropertyName here to be used in multiple methods.
        private string CurrentPropertyName;
        public ContactResidentPresenter(IContactResident view)
        {
            _View = view;
            _View.ContactUserControl.SendButtonClick = SendEmailX;
            _View.IsCheckBoxChecked = AtLeastOneCheckBoxIsChecked;
            
        }

        public void InitializeValues()
        {
            _View.ContactRentPaidOnlineList.Visible = true;
            _View.ContactUserControl.SetContactMultiView = 0;            
        }

        private void SendEmailX(object sender, EventArgs e)
        {
            if (!_View.ValidatePage()) return;
            SendEmailX();
            Clear();
            _View.ContactRentPaidOnlineList.Visible = false;
            _View.ContactUserControl.SetContactMultiView = 1;
        }

        private List<MailAddress> FindPropertyEmail()
        {
            var Recipients = new List<MailAddress>();
            var CurrentProperty = Property.GetPropertyByRenterId(RenterId);
            //CMallory - Task 00539 - Instantiate CurrentPropertyName here since the database is retrieving the object here.
            CurrentPropertyName = CurrentProperty.PropertyName.ToString();
            if (!CurrentProperty.PropertyId.HasValue || CurrentProperty.PropertyId.Value < 1)
                return Recipients;

            var PropertyStaffList = PropertyStaff.GetPropertyStaffListByPropertyId(CurrentProperty.PropertyId.Value);
            
            if (_View.ContactRentPaidOnlineList.Items[0].Selected)
            {
                var Company = EfxFramework.Company.GetCompanyListByPropertyId(CurrentProperty.PropertyId.Value);

                if ((Company.Count > 0 && Company[0] != null) && Company[0].EmailAddress.IsValidEmailAddress())
                    Recipients.Add(new MailAddress(Company[0].EmailAddress));
                else if (CurrentProperty.OfficeEmailAddress != null && CurrentProperty.OfficeEmailAddress.IsValidEmailAddress())
                    Recipients.Add(new MailAddress(CurrentProperty.OfficeEmailAddress));
                else
                {
                    var Staff = PropertyStaffList.FirstOrDefault(s => s.IsPropertyGeneralManager);

                    if (Staff != null && Staff.PrimaryEmailAddress.IsValidEmailAddress())
                        Recipients.Add(new MailAddress(Staff.PrimaryEmailAddress));
                }
            }

            return Recipients;
        }

        private List<MailAddress> RpoEmail(List<MailAddress> recipients)
        {
            if (_View.ContactRentPaidOnlineList.Items[1].Selected)
            {
                recipients.Add(new MailAddress(EfxSettings.ContactUsEmail));
                return recipients;
            }

            return recipients;
        }
        
        private void SendEmailX()
        {            
            var Recipients = FindPropertyEmail();
            var AllRecipients = RpoEmail(Recipients);

            if (_View.ContactUserControl.EmailAddressText.IsValidEmailAddress())
                AllRecipients.Add(new MailAddress(_View.ContactUserControl.EmailAddressText));

            //CMallory - Task 00539 - Modified to insert Property Name on it's own line above the message.
            BulkMail.BulkMail.SendMailMessage(null, "EFX", false, EfxSettings.SendMailUsername, EfxSettings.SendMailPassword, new MailAddress(_View.ContactUserControl.EmailAddressText), "Contact Us",
                    String.Format(EmailTemplates.RenterContactForm, _View.ContactUserControl.NameText, _View.ContactUserControl.EmailAddressText, "Contact Us",
                    CurrentPropertyName, _View.ContactUserControl.MessageText), AllRecipients);
        }

        private void AtLeastOneCheckBoxIsChecked(object source, ServerValidateEventArgs args)
        {
            args.IsValid = false;

            foreach (var Item in _View.ContactRentPaidOnlineList.Items.Cast<ListItem>().Where(item => item.Selected))
            {
                args.IsValid = true;
                break;
            }
        }

        private void Clear()
        {
            _View.ContactUserControl.EmailAddressText = String.Empty;
            _View.ContactUserControl.NameText = String.Empty;
            _View.ContactUserControl.PhoneNumberText = String.Empty;
            _View.ContactUserControl.PropertyNameText = String.Empty;
            _View.ContactUserControl.MessageText = String.Empty;
            _View.ContactRentPaidOnlineList.SelectedIndex = -1;
        }
    }
}
