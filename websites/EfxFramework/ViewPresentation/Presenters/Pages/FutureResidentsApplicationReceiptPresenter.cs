﻿using EfxFramework.ViewPresentation.Interfaces.Pages;
using System;
using System.Linq;

namespace EfxFramework.ViewPresentation.Presenters.Pages
{
    public class FutureResidentsApplicationReceiptPresenter
    {
        public IFutureResidentsApplicationReceipt _View;

        public FutureResidentsApplicationReceiptPresenter(IFutureResidentsApplicationReceipt view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            if (string.IsNullOrEmpty(_View.TransactionId))
                throw new Exception("Transaction was ID missing from the query string");
            if(_View.PropertyId < 1)
                throw new Exception("Property ID was missing from the query string");
            if(_View.ApplicantId < 1)
                throw new Exception("Applicant ID was missing from the query string");

            SetPropertyInfo();
            SetApplicationInfo();
            SetTransactionInfo();
            _View.StatusButtonNavigateUrl = String.Format("~/FutureResidentsStatus.aspx?ApplicantId={0}", _View.ApplicantId);
            _View.ApplyConnectButtonNavigateUrl = @"https://rpo.applyconnect.com/";

        }
        
        private void SetTransactionInfo()
        {
            var Trans = Transaction.GetTransactionByInternalTransactionId(_View.TransactionId);
            _View.Fees = FeePayment.GetAllFeePaymentsByTransactionId(Trans.TransactionId).Select(fee => new ApplicationFee(fee.ApplicationFeeId)).Where(appFee => appFee.ApplicationFeeId > 0).ToList();
        }

        private void SetApplicationInfo()
        {
            var AppList = new Applicant(_View.ApplicantId).Applications.FirstOrDefault(a => a.PropertyId == _View.PropertyId);

            if (AppList != null && ApplicantApplication.GetCompletedApplication(AppList.ApplicantApplicationId) != null)
                _View.UploadStatus = "Uploaded";
            else
                _View.UploadStatus = "Not Uploaded";
        }

        private void SetPropertyInfo()
        {
            var CurrentProperty = new Property(_View.PropertyId);
            _View.PropertyName = CurrentProperty.PropertyName;
        }
    }
}
