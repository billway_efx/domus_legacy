﻿using EfxFramework.NewsApi;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.WeatherApi;
using System.Linq;
using EfxFramework;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class NewsAndWeatherSectionPresenter
    {
        private readonly INewsAndWeatherSection _View;

        public void InitializeValues()
        {
            //Get property id from user id on page
            var Prop = Property.GetPropertyByRenterId(_View.UserId);

            //Set initial City value
            var CurrentCity = Prop.City;

            if (!Prop.StateProvinceId.HasValue)
                return;

            //Salcedo - 1/9/2014 - work around wunderground weather problem
            if (EfxSettings.EnableWeather)
            {
                try
                {
                    //Get weather for the current city
                    var CurrentWeather = Weather.GetWeather(CurrentCity, ((StateProvince)Prop.StateProvinceId).ToString());

                    //If the API returns null due to invalid city
                    if (CurrentWeather.current_observation == null)
                    {
                        //Set the current city to the capital of the property's state
                        CurrentCity = ((CapitalCity)Prop.StateProvinceId.Value).ToString();

                        //Call the API to get weather based on the capital
                        CurrentWeather = Weather.GetWeather(CurrentCity, ((StateProvince)Prop.StateProvinceId).ToString());
                    }

                    _View.Temp = CurrentWeather.current_observation.temperature_string;
                    _View.RelativeHumidity = CurrentWeather.current_observation.relative_humidity;
                    _View.WindSpeed = CurrentWeather.current_observation.wind_mph;
                    _View.GustingWindSpeed = CurrentWeather.current_observation.wind_gust_mph;
                    _View.WeatherString = CurrentWeather.current_observation.weather;
                    //CMallory - Task 00624 - Added an 's' on to the url that gets the weather image.
                    //_View.WeatherImageUrl = CurrentWeather.current_observation.icon_url;
                     _View.WeatherImageUrl = CurrentWeather.current_observation.icon_url.Replace("http","https");
                    //cakel: Adding weather location
                    _View.LocationCity = CurrentCity;
                   

                }
                catch
                {
                }
            }

            //Salcedo - 1/9/2014 - add same code for the News feed, just in case we eve have a problem with them and need to turn them off...
            if (EfxSettings.EnableNews)
            {
                try
                {
                    //Call the news API to get news based on the current city
                    var News = BingNews.GetNews(string.Format("{0}, {1}", CurrentCity, (StateProvince)Prop.StateProvinceId));

                    //Populate fields on the page
                    _View.NewsResultDataSource = News.d.results.Take(EfxSettings.BingNewsResultsCount);
                }
                catch
                {
                }
            }
        }

        //View constructor
        public NewsAndWeatherSectionPresenter(INewsAndWeatherSection view)
        {
            _View = view;
        }
    }
}
