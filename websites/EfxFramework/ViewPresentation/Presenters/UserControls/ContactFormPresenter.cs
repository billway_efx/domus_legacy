﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class ContactFormPresenter
    {
        private readonly IContactForm _View;

        public ContactFormPresenter(IContactForm view)
        {
            _View = view;
        }
    }
}
