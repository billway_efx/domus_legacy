﻿using System.Collections.Generic;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System.Linq;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class PaymentAmountsPresenter
    {
        private readonly IPaymentAmounts _View;
		private ConvenienceFee convFee = null;

        private Lease Lease
        {
            get { return Lease.GetRenterLeaseByRenterId(_View.RenterId); }
        }

        public Property Property
        {
            get { return Property.GetPropertyByRenterId(_View.RenterId); }
        }

        //3/30/2014 - Salcedo - Issue #1 - added visibility into which payment type is selected
        public string PaymentTypeSelectedValue
        {
            get
            {
                //Salcedo - 5/30/2014 - I went a little nuts avoiding an "Object reference not set to an instance of an object" error message.
                if (_View != null)
                {
                    if (_View.PaymentTypes != null)
                    {
                        if (_View.PaymentTypes.PaymentTypeSelectedValue != null)
                        {
                            return (_View.PaymentTypes.PaymentTypeSelectedValue);
                        }
                        else
                        {
                            return "";
                        }
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return "";
                }
            }
        }

        public PaymentAmountsPresenter(IPaymentAmounts view)
        {
            _View = view;

            if (Lease.LeaseId < 1)
            {
                _View.PastDueVisible = false;
                SetValuesZero();
                return;
            }

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                _View.PastDueVisible = false;
                SetValuesZero();
                return;
            }

            PopulateFields();
        }

        public void InitializeValues()
        {
        }

        private void SetValuesZero()
        {
            _View.RentAmountText = "$0.00";
            _View.ConvenienceFeeText = "$0.00";
            _View.TotalAmountText = "$0.00";
            _View.RentAmount = 0.00M;
            _View.ConvenienceFeeAmount = 0.00M;
            _View.MonthlyFeesDataSource = new List<LeaseFee>();
        }

		

		public ConvenienceFee Fees
		{
			get
			{
				if(convFee == null)
					convFee = Property.GetConvenienceFeesByProperty(Property);
				return convFee;
			}
		}

		public decimal ConvenienceFee
		{
			get
			{
				return (_View.PaymentTypes.PaymentTypeSelectedValue == "1" ? Fees.CreditCardFee : Fees.ECheckFee);
			}
		}

        private void PopulateFields()
        {
            //cakel: TASK 00407
            var Total = 0.00M;

            if (Property.ShowPaymentDetail)
            {
                _View.RentAmountText = Lease.RentAmount.ToString("C");
                _View.MonthlyFeesDataSource = LeaseFee.GetFeesByLeaseId(Lease.LeaseId);
            }
            else
                _View.RentAmountText = Total.ToString("C");


            //cakel: 00407
            decimal _RentAmount = Lease.RentAmount;
            var _CurrentBalanceDue = Lease.CurrentBalanceDue;
            var _TotalFees = LeaseFee.GetFeesByLeaseId(Lease.LeaseId).Sum(lf => lf.FeeAmount).Value;

            //cakel: 00407 rev3
            bool _IsIntegrated = RPO.Helpers.Helper.IsIntegrated(Property.PmsId);

            
            _View.ConvenienceFeeText = ConvenienceFee.ToString("C");
            _View.RentAmount = _RentAmount;
            _View.ConvenienceFeeAmount = ConvenienceFee;

            if (!Lease.CurrentBalanceDue.HasValue)
            {
                _View.PastDueVisible = false;
                SetValuesZero();
                return;
            }

            if (Total != (_RentAmount + _TotalFees))
            {
                if (Lease.CurrentBalanceDue + Lease.TotalFees < 0.01M)
                {
                    _View.PastDueVisible = false;
                    SetValuesZero();
                    return;
                }

                //cakel: TASK 00407
                var Difference = 0.00M;

                if (Lease.CurrentBalanceDue.Value != 0.00M)
                {
                    Difference = _CurrentBalanceDue.Value - (_RentAmount + _TotalFees);
                }
                else
                {
                    Difference = Lease.CurrentBalanceDue.Value;
                }

                if (Difference > 0.00M)
                {
                    _View.PastDueVisible = true;
                    _View.PastDueHeaderText = "Past Due Amount:";
                    _View.PastDueAmountText = Difference.ToString("C");
                }
                else if (Difference < 0.00M)
                {
                    _View.PastDueVisible = true;
                    _View.PastDueHeaderText = "Credit Amount:";
                    _View.PastDueAmountText = Difference.ToString("C");
                }
                else
                    _View.PastDueVisible = false;

                //cakel: 00407
                Total = Lease.CurrentBalanceDue.Value;
                _View.RentAmount = Total;
                
            }

			if (_View.ParentPage != null && _View.ParentPage.SelectPaymentTypeControl != null && _View.ParentPage.SelectPaymentTypeControl.PaymentTypeSelectedValue != "3")
			{
				Total += ConvenienceFee;
			}
			else if(_View.PaymentTypes.PaymentTypeSelectedValue != "3")
			{
				Total += ConvenienceFee;
			}
            
            _View.TotalAmountText = Total.ToString("C");
        }
    }
}
