﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using EfxFramework.PublicApi;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System.Web;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class APIUsersPresenter
    {
        private readonly IAPIUsers _View;

        public APIUsersPresenter(IAPIUsers view)
        {
            _View = view;
            _View.ApiUserData.RowDataBound += RowDataBound;
            _View.SelectAllProperties.CheckedChanged += CheckedChanged;
            _View.AddNewApiUser = AddNewApiUser;
            _View.SaveButtonClick = SaveButtonClick;
            _View.CancelButtonClick = CancelButtonClick;
            _View.OnlyOnePartner = OnlyOnePartner;
        }

        public void InitializeValues()
        {
            SetApiUsersGrid();
            _View.PropertiesCheckListDataSource = GetPropertiesNames();
            _View.PartnersCheckListDataSource = GetPartnershipsNames();
            _View.EditEnabled.Value = "0";

            if (_View.ApiUserId <= 0) return;
            EditApiUser();
            _View.EditEnabled.Value = "1";
        }

        private static void RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var ApiUserId = e.Row.Cells[0].FindControl("HiddenApiUserId") as HiddenField;

            if (ApiUserId == null)
                return;

            var Properties = new ApiUser().GetApiUserPropertyListByApiUserId(ApiUserId.Value.ToInt32());
            var RowProperties = (e.Row.Cells[2].FindControl("PropertiesDropDown") as DropDownList);

            var RowEmpty = (e.Row.Cells[2].FindControl("NoProperties") as Label);

            if (Properties.Count > 0)
            {
                RowProperties.DataSource = Properties;
                RowProperties.DataBind();
            }
            else
            {
                RowProperties.Visible = false;
                RowEmpty.Visible = true;
            }
        }

        private void SetApiUsersGrid()
        {
            _View.ApiUsersDataSource = ApiUser.GetAllApiUsers().Where(user => user.IsActive).ToList();
        }

        private void CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListItem Item in _View.PropertiesCheckList.Items)
            {
                Item.Selected = _View.SelectAllProperties.Checked;
            }
        }

        private void EditApiUser()
        {
            var Id = _View.ApiUserId;
            var CurrentApiUser = new ApiUser(Id);

            _View.UsernameText = CurrentApiUser.Username;
            if (CurrentApiUser.GetApiUserPropertyListByApiUserId(CurrentApiUser.ApiUserId).Count > 0)
            {
                foreach (var Item in from Property in CurrentApiUser.GetApiUserPropertyListByApiUserId(CurrentApiUser.ApiUserId)
                                     from ListItem Item in _View.PropertiesCheckList.Items
                                     where Item.Value == Property.PropertyId.ToString()
                                     select Item)
                {
                    Item.Selected = true;
                }
            }
            if (CurrentApiUser.PartnerCompanyId != null || CurrentApiUser.PartnerCompanyId > 0)
                _View.PartnershipsCheckList.SelectedValue = CurrentApiUser.PartnerCompanyId.ToString();

            _View.SelectedView = 1;
        }

        private void AddNewApiUser(object sender, EventArgs e)
        {
            _View.SelectedView = 1;
        }

        private static List<Property> GetPropertiesNames()
        {
            var Properties = Property.GetAllPropertyList();
            return Properties;
        }

        private static List<PartnerCompany> GetPartnershipsNames()
        {
            var Partnerships = PartnerCompany.GetAllPartnerCompanies();
            return Partnerships;
        }

        private void SetNewUser()
        {
            var NewApiUser = new ApiUser
                                {
                                    Username = _View.UsernameText,
                                    IsActive = true
                                };

            NewApiUser.SetPassword(_View.PasswordText);
            if (_View.PartnershipsCheckList.Items.Cast<ListItem>().Any(item => item.Selected))
                NewApiUser.PartnerCompanyId = _View.PartnershipsCheckList.SelectedValue.ToInt32();

            var ApiUserId = ApiUser.Set(NewApiUser);

            var User = new ApiUser(ApiUserId);

            if (_View.PropertiesCheckList.Items.Cast<ListItem>().Any(item => item.Selected))
                foreach (var Item in _View.PropertiesCheckList.Items.Cast<ListItem>().Where(item => item.Selected))
                {
                    User.AddPropertyToApiUser(Item.Value.ToInt32());
                }
        }

        private void EditUser()
        {
            var EditApiUser = new ApiUser(_View.ApiUserId);

            if (!string.IsNullOrEmpty(_View.UsernameText))
                EditApiUser.Username = _View.UsernameText;

            if (!string.IsNullOrEmpty(_View.PasswordText) && !string.IsNullOrEmpty(_View.ConfirmPasswordText) 
                && string.Equals(_View.PasswordText, _View.ConfirmPasswordText, StringComparison.OrdinalIgnoreCase))
                    EditApiUser.SetPassword(_View.PasswordText);
            else if (!string.Equals(_View.PasswordText, _View.ConfirmPasswordText, StringComparison.OrdinalIgnoreCase) || _View.PasswordText.Length > 0 || _View.ConfirmPasswordText.Length > 0)
            {
                _View.ParentPage.Validate("ApiUserSave");
                if (!_View.ParentPage.IsValid)
                    return;
            }

            if (_View.PartnershipsCheckList.Items.Cast<ListItem>().Any(item => item.Selected))
                EditApiUser.PartnerCompanyId = _View.PartnershipsCheckList.SelectedValue.ToInt32();
            ApiUser.Set(EditApiUser);

            if (_View.PropertiesCheckList.Items.Cast<ListItem>().Any(item => item.Selected) || !_View.SelectAllProperties.Checked)
                foreach (ListItem Item in _View.PropertiesCheckList.Items)
                {
                    if (Item.Selected)
                        EditApiUser.AddPropertyToApiUser(Item.Value.ToInt32());
                    else               
                        EditApiUser.DeletePropertyFromApiUser(Item.Value.ToInt32());
                }
        }

        private void SaveButtonClick(object sender, EventArgs e)
        {
            if (_View.EditEnabled.Value == "0")
            {
                _View.ParentPage.Validate("ApiUserSave");
                if (!_View.ParentPage.IsValid)
                    return;
                SetNewUser();
            }
            else
            {
                EditUser();
            }

            if (!_View.ParentPage.IsValid)
                return;

            SetApiUsersGrid();
            Reset();
        }
        
        private void CancelButtonClick(object sender, EventArgs e)
        {
            Reset();
        }

        private void Reset()
        {
            _View.UsernameText = String.Empty;
            _View.PasswordText = String.Empty;
            _View.ConfirmPasswordText = String.Empty;
            _View.PropertiesCheckList.ClearSelection();
            _View.PartnershipsCheckList.ClearSelection();
            
            _View.EditEnabled.Value = "0";
            _View.SelectedView = 0;
			_View.SetApiUserTabAsActive = true;
        }

        private void OnlyOnePartner(object sender, ServerValidateEventArgs e)
        {
            var Count = _View.PartnershipsCheckList.Items.Cast<ListItem>().Count(Item => Item.Selected);

            e.IsValid = Count <= 1;
        }
    }
}
