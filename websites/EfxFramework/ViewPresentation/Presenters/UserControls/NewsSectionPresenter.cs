﻿using System.Collections.Generic;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System.Linq;
using System.Web;
using Mb2x.ExtensionMethods;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class NewsSectionPresenter
    {
        private readonly INewsSection _View;
        private readonly int? RenterId = HttpContext.Current.User.Identity.Name.ToNullInt32();

        public NewsSectionPresenter(INewsSection view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            SetNewsGrid();
        }

        private void SetNewsGrid()
        {
            if (!RenterId.HasValue || RenterId.Value < 1)
            {
                _View.NewsListView = new List<Announcement>();
                return;
            }

            _View.NewsListView = Announcement.GetAllActiveNewsAndEvents().OrderByDescending(n => n.AnnouncementDate).Take(EfxSettings.RecentNewsCount).ToList();
        }
    }
}
