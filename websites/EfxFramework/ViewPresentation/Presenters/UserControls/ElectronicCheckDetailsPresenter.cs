﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class ElectronicCheckDetailsPresenter
    {
        private readonly IElectronicCheckDetails _View;

        public ElectronicCheckDetailsPresenter(IElectronicCheckDetails view)
        {
            _View = view;
        }

        public void IntializeValues()
        {
            
        }
    }
}
