﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class ApplyNowWindowPresenter
    {
        private readonly IApplyNowWindow _View;

        public ApplyNowWindowPresenter(IApplyNowWindow view)
        {
            _View = view;
        }        
    }
}
