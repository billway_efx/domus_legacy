﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System.Collections.Generic;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class FeesListPresenter
    {
        private readonly IFeesList _View;

        private Lease Lease
        {
            get { return Lease.GetRenterLeaseByRenterId(_View.RenterId); }
        }

        public FeesListPresenter(IFeesList view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            _View.RenterMonthlyFees = SetFeesListGrid();
        }

        protected List<LeaseFee> SetFeesListGrid()
        {
            var Fees = LeaseFee.GetFeesByLeaseId(Lease.LeaseId);
            if (Fees.Count < 1)
                return new List<LeaseFee>();
            return Fees;
        }
    }
}
