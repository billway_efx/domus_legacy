﻿using System;
using System.Collections.Generic;
using System.Linq;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.Web;
using EfxFramework.Web.Session;
using Mb2x.ExtensionMethods;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class RentAndFeesPresenter
    {
        private readonly IRentAndFees _View;
        private Property CurrentProperty { get { return new Property(_View.PropertyId); } }
        private static List<ApplicationFee> ApplicationFees { get { return SessionManager.GetSessionItem<List<ApplicationFee>>(SessionKey.FeesApplicantFees); } set { SessionManager.AddItemToSession(SessionKey.FeesApplicantFees, value); } }
        private static List<MonthlyFee> MonthlyFees { get { return SessionManager.GetSessionItem<List<MonthlyFee>>(SessionKey.FeesMonthlyFees); } set { SessionManager.AddItemToSession(SessionKey.FeesMonthlyFees, value); } }

        public RentAndFeesPresenter(IRentAndFees view)
        {
            _View = view;
            _View.AddApplicationFeeClicked = SetAddApplicationButtonClick;
            _View.AddMonthlyFeeClicked = SetAddMonthlyButtonClick;
            _View.SaveButtonClicked = SavedClicked;
        }

        public void InitializeValues()
        {
            SetApplicationFeeGrid();
            SetMonthlyFeeGrid();
            _View.IsIntegratedChecked = CurrentProperty.ShowPaymentDetail;
            _View.IsIntegratedProperty = EnableFeeBreakDown();
        }

        public void SetApplicationFeeGrid()
        {
            var DbFees = ApplicationFee.GetAllApplicationFeesByPropertyId(_View.PropertyId);
            var AppFees = SessionManager.GetSessionItem<List<ApplicationFee>>(SessionKey.FeesApplicantFees);
            var AppFeesDictionary = new Dictionary<int, ApplicationFee>();

            if (AppFees.Count > 0 && AppFees[0].PropertyId != _View.PropertyId)
            {
                SessionManager.RemoveItemFromSession(SessionKey.FeesApplicantFees);
                AppFees = SessionManager.GetSessionItem<List<ApplicationFee>>(SessionKey.FeesApplicantFees);
            }

            foreach (var P in AppFees.Where(p => !AppFeesDictionary.ContainsKey(p.ApplicationFeeId)))
            {
                AppFeesDictionary.Add(P.ApplicationFeeId, P);
            }

            foreach (var P in DbFees.Where(p => !AppFeesDictionary.ContainsKey(p.ApplicationFeeId)))
            {
                AppFeesDictionary.Add(P.ApplicationFeeId, P);
            }

            ApplicationFees = AppFeesDictionary.Select(pd => pd.Value).ToList();
            _View.ListOfApplicationFees = ApplicationFees;
        }

        private void SetAddApplicationButtonClick(object sender, EventArgs e)
        {
            _View.ViewPage.Validate("ApplicationFees");
            if (!_View.ViewPage.IsValid)
                return;

            if (ApplicationFees.Count < 1)
                ApplicationFees = EfxFramework.ApplicationFee.GetAllApplicationFeesByPropertyId(_View.PropertyId);

            var Lcd = ApplicationFees.Count > 0 ? ApplicationFees.Select(p => p.ApplicationFeeId).Min() : 0;
            if (Lcd > 0)
                Lcd = 0;
            else
                Lcd -= 1;

            var ApplicationFee = new ApplicationFee
                {   
                    ApplicationFeeId = Lcd,
                    PropertyId = _View.PropertyId,
                    FeeName = _View.ApplicationFeeName,
                    FeesPaid = _View.ApplicationFeeAmount.ToDecimal(),
                    IsActive =  true
                };
            ApplicationFees.Add(ApplicationFee);
            ApplicationFees = ApplicationFees;
            _View.ListOfApplicationFees = ApplicationFees;
            ClearApplicationFeeText();
        }

        private void ClearApplicationFeeText()
        {
            _View.ApplicationFeeName = String.Empty;
            _View.ApplicationFeeAmount = String.Empty;
        }

        public void SetMonthlyFeeGrid()
        {
            var DbFees = MonthlyFee.GetMonthlyFeesByPropertyId(_View.PropertyId);
            var MonthFees = SessionManager.GetSessionItem<List<MonthlyFee>>(SessionKey.FeesMonthlyFees);
            var MonthFeesDictionary = new Dictionary<int, MonthlyFee>();

            if (MonthFees.Count > 0 && MonthFees[0].PropertyId != _View.PropertyId)
            {
                SessionManager.RemoveItemFromSession(SessionKey.FeesMonthlyFees);
                MonthFees = SessionManager.GetSessionItem<List<MonthlyFee>>(SessionKey.FeesMonthlyFees);
            }

            foreach (var P in MonthFees.Where(p => !MonthFeesDictionary.ContainsKey(p.MonthlyFeeId)))
            {
                MonthFeesDictionary.Add(P.MonthlyFeeId, P);
            }

            foreach (var P in DbFees.Where(p => !MonthFeesDictionary.ContainsKey(p.MonthlyFeeId)))
            {
                MonthFeesDictionary.Add(P.MonthlyFeeId, P);
            }

            MonthlyFees = MonthFeesDictionary.Select(pd => pd.Value).ToList();
            _View.ListOfMonthlyFees = MonthlyFees;
        }

        private void SetAddMonthlyButtonClick(object sender, EventArgs e)
        {
            _View.ViewPage.Validate("MonthlyFees");
            if (!_View.ViewPage.IsValid)
                return;

            if (MonthlyFees.Count < 1)
                MonthlyFees = EfxFramework.MonthlyFee.GetMonthlyFeesByPropertyId(_View.PropertyId);

            var Lcd = MonthlyFees.Count > 0 ? MonthlyFees.Select(p => p.MonthlyFeeId).Min() : 0;
            if (Lcd > 0)
                Lcd = 0;
            else
                Lcd -= 1;

            var MonthlyFee = new MonthlyFee
            {
                MonthlyFeeId = Lcd,
                PropertyId = _View.PropertyId,
                FeeName = _View.MonthlyFeeName,
                DefaultFeeAmount = _View.MonthlyFeeAmount.ToDecimal(),
                IsActive = true
            };
            MonthlyFees.Add(MonthlyFee);
            MonthlyFees = MonthlyFees;
            _View.ListOfMonthlyFees = MonthlyFees;
            ClearMonthlyFeeText();
        }

        private void ClearMonthlyFeeText()
        {
            _View.MonthlyFeeName = String.Empty;
            _View.MonthlyFeeAmount = String.Empty;
        }

        private bool EnableFeeBreakDown()
        {
            return !String.IsNullOrEmpty(CurrentProperty.PmsId) && CurrentProperty.PmsTypeId.HasValue;
        }

        private void SetFeeDetails()
        {
            var AlteredProperty = new Property(_View.PropertyId);

            if (_View.IsIntegratedChecked && !CurrentProperty.ShowPaymentDetail)
            {
                AlteredProperty.ShowPaymentDetail = true;
                Property.Set(AlteredProperty);
            }
            else if (!_View.IsIntegratedChecked && CurrentProperty.ShowPaymentDetail)
            {
                AlteredProperty.ShowPaymentDetail = false;
                Property.Set(AlteredProperty);
            }
        }

        private void SavedClicked(object sender, EventArgs e)
        {
            try
            {
                SaveApplicationFees();
                SaveMonthlyFees();
                SetFeeDetails();
                //BasePage.GetPageMessage(_View.ViewPage).DisplayMessage(SystemMessageType.Success, "Successfully Saved");
				_View.SuccessMessagePlaceHolder.Visible = true;
            }
            catch
            {
                BasePage.SafePageRedirect("~/ErrorPage.html");
            }
        }

        private void SaveApplicationFees()
        {
            foreach (var Fee in ApplicationFees)
            {
                ApplicationFee.Set(Fee);
            }
            SessionManager.RemoveItemFromSession(SessionKey.FeesApplicantFees);
            _View.ListOfApplicationFees = ApplicationFee.GetAllApplicationFeesByPropertyId(_View.PropertyId);
        }
        //todo 352 this is where we'll want to kick off running the stored procedure
        private void SaveMonthlyFees()
        {
            if (_View.IsFeeForAllResidents)
            {
                var Unique = new HashSet<int>();
                var Renters = Renter.GetRenterListByPropertyId(_View.PropertyId);

                var List = (from R in Renters
                            let Leases = Lease.GetRenterLeaseByRenterId(R.RenterId)
                            where Unique.Add(Leases.LeaseId)
                            select new MonthlyToLeaseRecord {LeaseId = Leases.LeaseId, RenterId = R.RenterId}).ToList();

                foreach (var Fee in MonthlyFees.Where(p => p.MonthlyFeeId <= 0))
                {
                    var MonthlyFeeId = MonthlyFee.Set(Fee);

                    foreach (var Record in List.Where(record => record.LeaseId >= 1))
                    {
                        var AllLeases = new LeaseFee
                                            {
                                                MonthlyFeeId = MonthlyFeeId,
                                                LeaseId = Record.LeaseId,
                                                RenterId = Record.RenterId,
                                                FeeAmount = Fee.DefaultFeeAmount,
                                                IsActive = true
                                            };

                        LeaseFee.Set(AllLeases);
                    }
                }
            }
            else
            {
                foreach (var Fee in MonthlyFees.Where(p => p.MonthlyFeeId <= 0))
                {
                    if (Fee.ChargeCode == null)
                        Fee.ChargeCode = String.Empty;

                    MonthlyFee.Set(Fee);
                }
            }

            SessionManager.RemoveItemFromSession(SessionKey.FeesMonthlyFees);
            _View.ListOfMonthlyFees = MonthlyFee.GetMonthlyFeesByPropertyId(_View.PropertyId);

            //update the TotalFees on qualifying leases
            Lease.UpdateLeaseFees();
        }
    }
}
