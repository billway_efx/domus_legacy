﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.Web;
using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class ForgotPasswordPresenter<T> where T : BaseUser, new()
    {
        private readonly IForgotPassword _View;

        public void InitializeValues() {}

        public ForgotPasswordPresenter(IForgotPassword view)
        {
            _View = view;
            _View.SubmitClick = SubmitButtonClicked;
        }

        public void SubmitButtonClicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_View.Email))
            {
                BasePageV2.DisplaySystemMessage(_View.ParentPage, "Email address is required.", SystemMessageType.Information);
                return;
            }

            var CurrentUser = new T().GetUserByEmailAddress(_View.Email);

            if (!CurrentUser.IsValid)
            {
                BasePageV2.DisplaySystemMessage(_View.ParentPage, "No user was found with this email address.", SystemMessageType.Information); 
                return;                
            }

            var NewPassword = CurrentUser.ResetPassword();
            CurrentUser.SetUser(CurrentUser, "Password Reset Tool");

            //Salcedo - 3/12/2014 - replaced with new html email template
            var Message = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-password-reset.html").Replace("[[ROOT]]",
                EmailTemplateService.EmailTemplateService.RootPath), CurrentUser.DisplayName, NewPassword);

            BulkMail.BulkMail.SendMailMessage(
                null,
                "EFX",
                false,
                EfxSettings.SendMailUsername,
                EfxSettings.SendMailPassword,
                new MailAddress(EfxSettings.SupportEmail),
                "Forgot Password",
                Message,
                new List<MailAddress> {new MailAddress(_View.Email)}
                );

            BasePageV2.DisplaySystemMessage(_View.ParentPage, "An email has been sent with your new temporary password. <br/>", SystemMessageType.Information);
        }
    }
}
