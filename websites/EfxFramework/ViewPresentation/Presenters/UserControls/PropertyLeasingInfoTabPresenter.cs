﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System;
using System.Web;
using Mb2x.ExtensionMethods;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class PropertyLeasingInfoTabPresenter
    {
        private readonly IPropertyLeasingInfoTab _View;
        private readonly int _UserId;
        private static int? RenterId = HttpContext.Current.User.Identity.Name.ToNullInt32();

        public PropertyLeasingInfoTabPresenter(IPropertyLeasingInfoTab view)
        {
            _View = view;
            _UserId = _View.UserId;
        }

        public void InializeValues()
        {
            SetAccountBalanceSection();
        }

        private void SetAccountBalanceSection()
        {
            if (_UserId < 1)
                return;

            var CurrentProperty = Property.GetPropertyByRenterId(_UserId);
            var CurrentLease = Lease.GetRenterLeaseByRenterId(_UserId);

            _View.PropertyNameLabelText = CurrentProperty.PropertyName;
            _View.PropertyAddressLabelText = CurrentProperty.StreetAddress;
            _View.PropertyCityStateZipLabelText = String.Format("{0}, {1} {2}", CurrentProperty.City, (StateProvince)CurrentProperty.StateProvinceId, CurrentProperty.PostalCode);
            
            _View.LeaseStartDateLabelText = CurrentLease.StartDate.ToString("MM/dd/yy");
            _View.LeaseEndDateLabelText = CurrentLease.EndDate.ToString("MM/dd/yy");
            _View.DueDateMonthLabelText = CurrentLease.RentDueDayOfMonth.ToString();
            _View.RentAmountLabelText = CurrentLease.RentAmount.ToString("C");
        }
    }
}
