﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System.Linq;
using EfxFramework;
using System.Data.SqlClient;
using System.Data;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class MaintenanceSectionPresenter
    {
        private readonly IMaintenanceSection _View;
         
        public MaintenanceSectionPresenter(IMaintenanceSection view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            //Salcedo - 2/11/2015 - task # 00366 - added ability to use/not use native maintenance request system
            if (_View.RenterId.HasValue)
            {
                if (_View.RenterId.Value > 0)
                {
                    SqlDataReader reader = MiscDatabase.GetPropertyMaintenanceSystemForRenter(_View.RenterId.Value);
                    reader.Read();
                    if (reader.HasRows)
                    {
                        if ((bool)reader["UseNativeMaintenanceSystem"])
                            SetGridView();
                        reader.Close();
                    }
                    else
                    {
                        SetGridView();
                    }
                }
            }
        }

        private void SetGridView()
        {
            if (!_View.RenterId.HasValue || _View.RenterId.Value < 1)
                return;
            //cakel: BUGID00078 commented old code and replaced with code below
            //This will now return in descending order 

            //var DefaultGridView = MaintenanceRequest.GetActiveMaintenanceRequestsByRenterId(_View.RenterId.Value).
            //    OrderByDescending(m => m.MaintenanceRequestId).Take(_View.GridForMaintenance.RecordsToDisplay).Reverse().ToList();
            //_View.GridForMaintenance.RequestGrid = DefaultGridView;

            var DefaultGridView = MaintenanceRequest.GetActiveMaintenanceRequestsByRenterId(_View.RenterId.Value).
                OrderByDescending(m => m.MaintenanceRequestDate).Take(_View.GridForMaintenance.RecordsToDisplay).ToList();
            _View.GridForMaintenance.RequestGrid = DefaultGridView;




            var ModalGridView = MaintenanceRequest.GetAllMaintenanceRequestsByRenterId(_View.RenterId.Value);
            _View.ModalGrid = ModalGridView;
        }        
    }
}
