﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System;
using System.Collections.Generic;
using System.Web;
using Mb2x.ExtensionMethods;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class OccupantListPresenter
    {
        private readonly IOccupantList _View;
        private readonly int? RenterId = HttpContext.Current.User.Identity.Name.ToNullInt32();

        public OccupantListPresenter(IOccupantList view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            _View.ListOfPeople = GetOccupants();
        }

        private List<Occupant> GetOccupants()
        {
            var OccupantsList = new List<Occupant>();
            var CurrentLease = Lease.GetRenterLeaseByRenterId(RenterId.Value);
            var RentersOnLease = Renter.GetRenterListByLeaseId(CurrentLease.LeaseId);
            var Roommates = Roommate.GetRoommatesByRenterId(RenterId.Value);

            foreach (var Re in RentersOnLease)
            {
                OccupantsList.Add(new Occupant { OccupantType = "Tenant: ", OccupantName = Re.DisplayName });
            }

            foreach (var Ro in Roommates)
            {
                if (Ro.IsResponsibleForLease)
                {
                    OccupantsList.Add(new Occupant { OccupantType = "Tenant: ", OccupantName = String.Format("{0} {1}", Ro.FirstName, Ro.LastName) });
                }
                else if (!Ro.IsResponsibleForLease)
                {
                    OccupantsList.Add(new Occupant { OccupantType = "Occupant: ", OccupantName = String.Format("{0} {1}", Ro.FirstName, Ro.LastName) });
                }
            }

            return OccupantsList;
        }
    }
}
