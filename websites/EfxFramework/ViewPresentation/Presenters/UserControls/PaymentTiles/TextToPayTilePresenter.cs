﻿using EfxFramework.ExtensionMethods;
using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;
using System;
using System.Globalization;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Presenters.UserControls.PaymentTiles
{
    internal class TextToPayTilePresenter : PaymentTilePresenterBase
    {
        private readonly ITextToPayTile _View;

        public TextToPayTilePresenter(ITextToPayTile view)
            : base(view)
        {
            _View = view;
            _View.RequiredMobileNumberValidation = ValidateMobileNumber;
        }

        public override void InitializeValues()
        {
            var Resident = new Renter(BaseView.RenterId);
            if (Resident.RenterId < 1)
                return;

            if ((!String.IsNullOrEmpty(Resident.SmsPaymentPhoneNumber) && Resident.SmsReminderDayOfMonth.HasValue) && Resident.SmsPaymentTypeId.HasValue)
                PopulateFields(Resident.SmsPaymentPhoneNumber, Resident.SmsPaymentTypeId.Value, Resident.SmsReminderDayOfMonth.Value);
            else
                TurnOff();
        }

        private void PopulateFields(string number, int paymentType, int reminderDay)
        {
            TurnOn();
            _View.MobileNumberText = number.FormatPhoneNumber();
            _View.PaymentTypeDropdown.SelectedValue = paymentType.ToString(CultureInfo.InvariantCulture);
            _View.DayOfTheMonthDropdown.SelectedValue = reminderDay.ToString(CultureInfo.InvariantCulture);
        }

        protected virtual void ValidateMobileNumber(object source, ServerValidateEventArgs args)
        {
            if (!_View.IsOn) return;
            
            if (!PhoneNumber.IsValidPhoneNumber(_View.MobileNumberText))
                args.IsValid = false;
        }
    }
}
