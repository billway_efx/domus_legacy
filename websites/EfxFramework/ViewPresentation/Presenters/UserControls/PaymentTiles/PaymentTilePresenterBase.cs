﻿using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;
using System;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Presenters.UserControls.PaymentTiles
{
    public abstract class PaymentTilePresenterBase
    {
        protected IPaymentTile BaseView;
		protected static string OnClass { get { return "btn btn-primary orangeButton buttonInline"; } }
		protected static string OffClass { get { return "btn btn-blank grayOffButton buttonInline"; } }

        protected PaymentTilePresenterBase(IPaymentTile view)
        {
            BaseView = view;
            BaseView.OffButtonClick = OffClicked;
            BaseView.OnButtonClick = OnClicked;
            BaseView.IsOn = BaseView.OnButtonCssClass == OnClass;

        }

        public virtual void InitializeValues()
        {}

        protected virtual void OffClicked(object sender, EventArgs e)
        {
            TurnOff();

        }

        protected virtual void OnClicked(object sender, EventArgs e)
        {
            TurnOn();

        }

        protected virtual void TurnOn()
        {
            BaseView.IsOn = true;
            BaseView.OnButtonCssClass = OnClass;
            BaseView.OffButtonCssClass = OffClass;

            string test = "";

        }

        protected virtual void TurnOff()
        {
            BaseView.IsOn = false;
            BaseView.OnButtonCssClass = OffClass;
            BaseView.OffButtonCssClass = OnClass;
        }
    }
}
