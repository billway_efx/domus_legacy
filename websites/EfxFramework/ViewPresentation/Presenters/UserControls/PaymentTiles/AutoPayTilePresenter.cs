﻿using System;
using System.Globalization;
using System.Linq;
using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;

namespace EfxFramework.ViewPresentation.Presenters.UserControls.PaymentTiles
{
    internal class AutoPayTilePresenter : PaymentTilePresenterBase
    {
        private readonly IAutoPayTile _View;

        public AutoPayTilePresenter(IAutoPayTile view)
            : base(view)
        {
            _View = view;
            _View.PaymentTypeChanged = PaymentTypeChanged;
        }

        public override void InitializeValues()
        {
            var Resident = new Renter(BaseView.RenterId);
            if (Resident.RenterId < 1)
                return;

            var AutoPay = AutoPayment.GetAutoPaymentByRenterId(Resident.RenterId);

            if (AutoPay.AutoPaymentId > 1)
                PopulateFields(Resident.RenterId, AutoPay);
            else
                TurnOff();
        }

        private void PopulateFields(int renterId, AutoPayment autoPay)
        {
            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(renterId);
            if (Lease.LeaseId < 1)
                return;

            TurnOn();

            _View.PaymentTypeDropdown.SelectedValue = autoPay.PaymentTypeId.ToString(CultureInfo.InvariantCulture);
            _View.DayOfTheMonthDropdown.SelectedValue = autoPay.PaymentDayOfMonth.ToString(CultureInfo.InvariantCulture);

            //var Total = Lease.RentAmount + CalculateMonthlyFees(Lease.LeaseId) + CalculateConvenienceFees(renterId);
            //_View.PaymentAmountText = Total.ToString("C");
        }

        private void PaymentTypeChanged(object sender, EventArgs e)
        {
            var Resident = new Renter(BaseView.RenterId);
            if (Resident.RenterId < 1)
                return;

            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(Resident.RenterId);
            if (Lease.LeaseId < 1)
                return;

            //var Total = Lease.RentAmount + CalculateMonthlyFees(Lease.LeaseId) + CalculateConvenienceFees(Resident.RenterId);
            //_View.PaymentAmountText = Total.ToString("C");
        }

        private static decimal CalculateMonthlyFees(int leaseId)
        {
            return LeaseFee.GetFeesByLeaseId(leaseId).Sum(lf => lf.FeeAmount).Value;
        }

        private decimal CalculateConvenienceFees(int renterId)
        {
            var Property = EfxFramework.Property.GetPropertyByRenterId(renterId);
            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                return 0.00M;

            var Fee = Property.GetConvenienceFeesByProperty(Property);
            return _View.PaymentTypeDropdown.SelectedValue == "1" ? Fee.CreditCardFee : Fee.ECheckFee;
        }
    }
}
