﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.Web;
using System.Linq;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class PaymentHistoryPresenter
    {
        private readonly IPaymentHistory _View;

        public PaymentHistoryPresenter(IPaymentHistory view)
        {
            _View = view;

            if (_View.RenterId == 0)
                BasePageV2.SafePageRedirect("~/Account/Login.aspx");
        }

        public void InitializeValues()
        {
            _View.PaymentList =  Payment.GetPaymentListByRenterId(_View.RenterId).OrderByDescending(p => p.TransactionDateTime)
                                 .Where(p => p.OtherDescription1 != "Waived Payments").Select(p => new PaymentHistory(p)).ToList(); 
        }
    }
}
