﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using Mb2x.ExtensionMethods;
using RPO;
using System.Data.SqlClient;
using System.Data;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class PropertyAnnouncementsPresenter
    {
        private readonly IPropertyAnnouncements _View;
        private List<int> PropertyIDs = new List<int>();
        private List<int> PropertyID_list = new List<int>();
        public PropertyAnnouncementsPresenter(IPropertyAnnouncements view)
        {
            _View = view;
            _View.SaveButtonClicked = SaveAnnouncement;
            _View.EditRow = RowEditEvent;
            _View.DeleteRow = RowDeleteEvent;
            _View.ViewAnnouncementsButtonClicked = UpdateGrid;
        }

        public void InitializeValues()
        {
            _View.CheckBoxIsVisible = IsPostToAllAvailable();
            _View.PostedAnnoucementGrid.DataSource = GetAllAnnouncementsGrid();
            _View.PostedAnnoucementGrid.DataBind();
            _View.EditEnabled.Value = "0";
        }

        //CMallory - Task 00462 - Called when a user clicks the View Announcement button.
        public void UpdateGrid(object sender, EventArgs e)
        {
            _View.PostedAnnoucementGrid.DataSource = GetAllAnnouncementsGrid();
            // _View.PostedAnnoucementGrid.DataBind();
        }
        private bool IsPostToAllAvailable()
        {
            if (_View.IsAdminSite)
                return true;

            return Role.GetRolesByStaffId(_View.CurrentStaff).FirstOrDefault() != null && Role.GetRolesByStaffId(_View.CurrentStaff).FirstOrDefault().RoleId == 1;
        }
        private static String ConnString
        {
            get
            {
                return EfxFramework.EfxSettings.ReportConnectionString;
            }
        }
        //CMallory - Task 00462 - Creates list of ints.
        public String GetPropertyString()
        {
            string PropList = String.Join(",", _View.PropertyIDs.ToArray());
            return PropList;
        }
        //CMallory - Task 00462 - Changed logic that populates the grid on the view to run from a dataset of a stored procedure.
        private DataSet GetAllAnnouncementsGrid()
        {
            //return !Helpers.Helper.IsRPOAdmin ? Announcement.GetActivePropertyAnnouncementsByPropertyId(_View.PropertyId) : Announcement.GetActiveAdminAnnouncements(_View.PropertyId);
            SqlConnection con = new SqlConnection(ConnString);
            {
                SqlCommand com = new SqlCommand("usp_Announcement_GetAllAnnouncementsByPropertyIDs", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("@PropertyIdList", SqlDbType.NVarChar, -1).Value = GetPropertyString();
                com.CommandTimeout = 360;
                con.Open();
                SqlDataAdapter SqlAdapt = new SqlDataAdapter(com);
                //SqlAdapt.SelectCommand = com;
                DataSet DS = new DataSet();
                SqlAdapt.Fill(DS, "AnnouncementData");
                con.Close();
                return DS;
            }
        }


        private void SaveAnnouncement(object sender, EventArgs e)
        {
            string PostedBy;
            //CMallory - Task 00462 - Added
            PropertyIDs = _View.PropertyIDs;

            _View.ViewPage.Validate("AnnouncementTab");
            if (!_View.ViewPage.IsValid)
                return;

            var AnnouncementType = !_View.IsAdminSite ? 1 : 2;

            var Message = new Announcement()
            {
                AnnouncementTypeId = AnnouncementType,
                AnnouncementDate = DateTime.Today,
                AnnouncementText = _View.BodyText,
                AnnouncementUrl = _View.UrlText,
                IsActive = true,
                AnnouncementTitle = _View.TitleText,
                PostedBy = GetStaffMember(_View.CurrentStaff),
                ExpirationDate = _View.ExpirationDate //CMallory - Task 00462 - Added expiration Date to the object.
            };

            if (_View.EditEnabled.Value == "0")
            {
                //creat a new announcement
                int newAnnouncementId = Announcement.Set(Message);
                if (!_View.CheckBoxIsChecked)
                    //CMallory - Task 00462 - Adds a record to the PropertyAnnouncement for each Property selected.
                    foreach (int prop in PropertyIDs)
                    {
                        PropertyAnnouncement.Set(prop, newAnnouncementId);
                    }
                //PropertyAnnouncement.Set(_View.PropertyId, newAnnouncementId);
                else
                {
                    List<PropertyListItem> properties = null;
                    using (var entity = new RPOEntities())
                    {
                        properties = entity.Properties
                        .Select(p => new PropertyListItem
                        {
                            PropertyId = p.PropertyId,
                            PropertyName = p.PropertyName
                        })
                        .ToList();

                        if (!Helpers.Helper.IsRpoAdmin)
                        {
                            var userProperties = entity.PropertyPropertyStaffs.Where(pps => pps.PropertyStaffId == Helpers.Helper.CurrentUserId).ToList();
                            properties = properties.Where(p => userProperties.Exists(up => up.PropertyId == p.PropertyId)).ToList();
                        }

                        properties.ForEach(p => PropertyAnnouncement.Set(p.PropertyId, newAnnouncementId));
                    }
                }
            }
            else
            {
                //edit existing announcments
                Message.AnnouncementId = _View.AnnouncementId.Value.ToInt32();
                Announcement.Set(Message);
                //CMallory - Task 00462 - Adds a record to the PropertyAnnouncement for each Property selected.
                foreach (int prop in PropertyIDs)
                {
                    PropertyAnnouncement.Set(prop, Message.AnnouncementId);
                }
                _View.EditEnabled.Value = "0";
            }

            ClearAnnouncements();
            _View.PostedAnnoucementGrid.DataSource = GetAllAnnouncementsGrid();
            _View.PostedAnnoucementGrid.DataBind();
        }

        private string GetStaffMember(int currentStaff)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection con = new SqlConnection();
            SqlDataAdapter adp = new SqlDataAdapter();
            DataTable dt = new DataTable();
            string StaffMember = "";

            cmd.CommandText = "usp_PropertyStaff_GetSingleObject";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@PropertyStaffId", SqlDbType.Int).Value = currentStaff;

            con.ConnectionString = ConnString;
            cmd.Connection = con;
            
            con.Open();
            adp.SelectCommand = cmd;
            adp.Fill(dt);
            con.Close();

            StaffMember = dt.Rows[0].ItemArray[1].ToString() + " " + dt.Rows[0].ItemArray[3].ToString();

            return StaffMember;

        }

        private void RowEditEvent(object sender, GridViewSelectEventArgs e)
        {
            var Id = ((HiddenField)_View.PostedAnnoucementGrid.Rows[e.NewSelectedIndex].FindControl("HiddenAnnouncementId")).Value.ToNullInt32();
            var CurrentAnnouncement = Announcement.GetAnnouncementById(Id.HasValue ? Id.Value : 0);

            _View.TitleText = CurrentAnnouncement.AnnouncementTitle;
            _View.BodyText = CurrentAnnouncement.AnnouncementText;
            _View.UrlText = CurrentAnnouncement.AnnouncementUrl;
            _View.AnnouncementId.Value = Id.ToString();
            _View.EditEnabled.Value = "1";
        }

        private void RowDeleteEvent(object sender, GridViewDeleteEventArgs e)
        {
            var Id = ((HiddenField)_View.PostedAnnoucementGrid.Rows[e.RowIndex].FindControl("HiddenAnnouncementId")).Value.ToNullInt32();
            var CurrentAnnouncement = Announcement.GetAnnouncementById(Id.HasValue ? Id.Value : 0);

            if (!_View.IsAdminSite)
            {
                CurrentAnnouncement.IsActive = false;
                Announcement.Set(CurrentAnnouncement);
                _View.PostedAnnoucementGrid.DataSource = Announcement.GetActivePropertyAnnouncementsByPropertyId(_View.PropertyId);
                _View.PostedAnnoucementGrid.DataBind();
            }
            else
            {
                CurrentAnnouncement.IsActive = false;
                Announcement.Set(CurrentAnnouncement);
                _View.PostedAnnoucementGrid.DataSource = Announcement.GetActiveAdminAnnouncements(_View.PropertyId);
                _View.PostedAnnoucementGrid.DataBind();
            }
        }

        private void ClearAnnouncements()
        {
            _View.CheckBoxIsChecked = false;
            _View.TitleText = String.Empty;
            _View.BodyText = String.Empty;
            _View.UrlText = String.Empty;
        }
    }
}
