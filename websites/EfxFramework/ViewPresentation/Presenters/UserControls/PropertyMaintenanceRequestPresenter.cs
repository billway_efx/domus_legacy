﻿using EfxFramework.ExtensionMethods;
using EfxFramework.Resources;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.Web;
using Mb2x.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class PropertyMaintenanceRequestPresenter
    {
        private readonly IPropertyMaintenanceRequest _View;

        public void InitializeValues()
        {
            _View.RequestType.BindToSortedValue(typeof (MaintenanceRequestType));
            _View.RequestPriority.BindToSortedValue(typeof (MaintenanceRequestPriority));

        }

        public PropertyMaintenanceRequestPresenter(IPropertyMaintenanceRequest view)
        {
            _View = view;
            _View.SubmitClick = SendMaintenanceMessageClicked;
        }

        private void SendMaintenanceMessageClicked(object sender, EventArgs e)
        {

            if (!BasePageV2.ValidatePage(_View.ParentPage))
            {
                var myScript = "$(document).ready(function() {changePanels(2);});";
                _View.ParentPage.ClientScript.RegisterStartupScript(GetType(), "myKey", myScript, true);
                return;
            }
            var Property = EfxFramework.Property.GetPropertyByRenterId(_View.UserId);
            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                return;

            var StaffList = PropertyStaff.GetPropertyStaffListByPropertyId(Property.PropertyId.Value);
            var Manager = StaffList.FirstOrDefault(s => s.IsPropertyGeneralManager);

            
            if (Manager == null || !Manager.PropertyStaffId.HasValue || Manager.PropertyStaffId.Value < 1)
                Manager = StaffList.FirstOrDefault(s => s.PropertyStaffId.HasValue && s.PropertyStaffId.Value > 0);

            if (Manager == null)
                return;

            SetMaintenanceRequest(Property.PropertyId.Value);

            //Salcedo - 1/10/2014 - capture additional data automatically for maintenance request
            var Renter = new Renter(_View.UserId);
            String Name = Renter.DisplayName;
            String Phone = Renter.MainPhoneNumber;
            String AddressUnit = Renter.Unit;  //Renter.DisplayAddress + " #" + Renter.Unit;
            String AddressLine1 = Renter.StreetAddress;
            //cakel: BUGID00087 - added string value to return value from drop down list
            String ReqPermission = _View.RequestPermission.SelectedValue.ToString();

            var Sender = new MailAddress(new Renter(_View.UserId).PrimaryEmailAddress);
            //Salcedo - 2/23/2014 - we're not including the general manager (IsPropertyGeneralManager) anymore. Instead, the new SendMailX will automatically
            // include all IsMainContact staff members by calling the new SendMailMessageIncludePropertyEmailContacts function.
            //var Recipients = new List<MailAddress>
            //    {
            //        new MailAddress(Manager.PrimaryEmailAddress)
            //    };
            var Recipients = Enumerable.Empty<MailAddress>().ToList();

            if (_View.CarbonCopy)
                Recipients.Add(Sender);

            int IntPropertyId = Property.PropertyId.HasValue ? Property.PropertyId.Value : 0;
            SendMailX(IntPropertyId, Sender, ((MaintenanceRequestType)_View.RequestType.SelectedValue.ToInt32()).ToString(), ((MaintenanceRequestPriority)_View.RequestPriority.SelectedValue.ToInt32()).ToString(),
                     _View.ProblemDescription, Name, Phone, AddressUnit, AddressLine1, ReqPermission, Recipients);

            _View.ProblemDescription = string.Empty;
            _View.CarbonCopy = false;
            BasePageV2.SafePageRedirect("~/Default.aspx");
        }
        
        private void SetMaintenanceRequest(int propertyId)
        {
            MaintenanceRequest.Set(
                0, 
                _View.RequestType.SelectedValue.ToInt32(), 
                _View.RequestPriority.SelectedValue.ToInt32(),
                propertyId, 
                _View.UserId, DateTime.Now,
                _View.ProblemDescription, 
                true);
        }

        // Salcedo - 1/10/2014 - Added additional parameters for the maintenance request email
        //cakel: BUGID00087 added parameter for Permission to enter
        private static void SendMailX(int PropertyId, MailAddress sender, string type, string priority, string description, String Name, String Phone, String AddressUnit, 
            String AddressLine1, String PermissionToEnter, List<MailAddress> recipients)
        {
            //BulkMail.BulkMail.SendMailMessage(
            //    null,
            //    "EFX",
            //    false,
            //    Settings.SendMailUsername,
            //    Settings.SendMailPassword,
            //       sender,
            //    "Maintenance Request",
            //    string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-600-servicesrequest.html").Replace("[[ROOT]]", EmailTemplateService.EmailTemplateService.RootPath), type, priority, description, Name, sender.Address, Phone, AddressUnit),
            //       recipients);


            
            //cakel: BUGID00087 - added Permission to enter parameter within function
            BulkMail.BulkMail.SendMailMessageIncludePropertyEmailContacts(
                PropertyId, 
                "EFX", 
                EfxSettings.SendMailUsername, 
                EfxSettings.SendMailPassword,
                sender,
                "Maintenance Request",
                string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-600-servicesrequest.html").Replace("[[ROOT]]",
                    EmailTemplateService.EmailTemplateService.RootPath), type, priority, description, Name, sender.Address, Phone, AddressUnit, AddressLine1,PermissionToEnter),
                   recipients);
        }
    }
}
