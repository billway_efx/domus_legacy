﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class ResidentRegisterPresenter
    {
        private readonly IResidentRegister _View;

        public void InitializeValues()
        {
            
        }

        public ResidentRegisterPresenter(IResidentRegister view)
        {
            _View = view;
        }
    }
}
