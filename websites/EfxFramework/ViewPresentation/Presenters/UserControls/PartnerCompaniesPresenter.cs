﻿using System;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.PublicApi;
using System.Collections.Generic;
using System.Linq;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class PartnerCompaniesPresenter
    {
        private readonly IPartnerCompanies _View;

        public PartnerCompaniesPresenter(IPartnerCompanies view)
        {
            _View = view;
            _View.AddButtonClick = AddButtonClick;
        }

        public void InitializeValues()
        {
			_View.PartnerCompanyList = PartnerCompany.GetAllPartnerCompanies();
        }

        private void AddButtonClick(object sender, EventArgs e)
        {
            _View.ParentPage.Validate("Partners");
            if (!_View.ParentPage.IsValid)
                return;

            var NewPartnership = new PartnerCompany
                                     {
                                         PartnerCompanyName = _View.PartnerNameText
                                     };
            PartnerCompany.Set(NewPartnership);

            _View.PartnerCompanyList = PartnerCompany.GetAllPartnerCompanies();
        }
    }
}
