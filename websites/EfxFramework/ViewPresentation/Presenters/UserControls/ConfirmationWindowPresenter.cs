﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class ConfirmationWindowPresenter
    {
        private readonly IConfirmationWindow _View;

        public ConfirmationWindowPresenter(IConfirmationWindow view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            
        }
    }
}
