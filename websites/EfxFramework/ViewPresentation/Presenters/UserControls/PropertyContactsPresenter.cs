﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System;
using System.Linq;

//Salcedo - 5/5/2014 - Needed for new SQL to get contact where PropertyPropertyStaff.IsMainContact = 1
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class PropertyContactsPresenter
    {
        private readonly IPropertyContacts _View;
        private readonly int _UserId;

        public void InitializeValues()
        {
            var Prop = Property.GetPropertyByRenterId(_UserId);
            SetRpoSupportDetails();

            if (!Prop.PropertyId.HasValue || Prop.PropertyId.Value < 1)
                return;

            SetPropertyDetails(Prop);

            var StaffList = PropertyStaff.GetPropertyStaffListByPropertyId(Prop.PropertyId.Value);

            //Salcedo - 5/5/2014 - Get contact where PropertyPropertyStaff.IsMainContact = 1
            //if (StaffList.Count > 0)
            //    SetPropertyManagerDetails(StaffList.FirstOrDefault(s => s.IsPropertyGeneralManager));
            SetPropertyManagerDetails_v2(Prop.PropertyId.Value);
            
            var LeasingAgentId = EfxFramework.Lease.GetRenterLeaseByRenterId(_UserId).PropertyStaffId;

            //if (LeasingAgentId > 0)
                SetLeasingAgentDetails(StaffList.FirstOrDefault(s => s.PropertyStaffId == LeasingAgentId));
        }

        public PropertyContactsPresenter(IPropertyContacts view)
        {
            _View = view;
            _UserId = _View.UserId;

            if (_View.UserId < 1)
                throw new Exception("User ID could not be found");

            InitializeValues();
        }

        private void SetPropertyDetails(Property property)
        {
            _View.PropertyName = property.PropertyName;
            _View.PropertyAddress = string.Format("{0} {1}", property.StreetAddress, property.StreetAddress2);
            _View.PropertyCityStateZip = string.Format("{0}, {1} {2}", property.City, (StateProvince)property.StateProvinceId, property.PostalCode);
            _View.PropertyPhone = property.MainPhoneNumber.ToString();
            _View.PropertyFax = property.FaxNumber.ToString();
            _View.PropertyEmail = property.OfficeEmailAddress;
            _View.PropertyImageUrl = "/Handlers/PhotoHandler.ashx?RequestType=0&photoIndex=1&propertyId=" + property.PropertyId;
        }

        //Salcedo - 5/5/2014 - Get contact where PropertyPropertyStaff.IsMainContact = 1
        private void SetPropertyManagerDetails_v2(int propertyId)
        {
            string sql = null;
            sql = "SELECT FirstName, LastName, isnull(MainPhoneNumber,'') as MainPhoneNumber, PrimaryEmailAddress, b.PropertyStaffId, propertyid " +
                "FROM propertystaff a " +
                "JOIN propertypropertystaff b on a.propertystaffid = b.propertystaffid and b.ismaincontact = 1 " +
                "WHERE b.propertyid = " + propertyId.ToString();

            SqlConnection sc = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());
            sc.Open();
            SqlDataAdapter da = new SqlDataAdapter(sql, sc);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                _View.PropertyManagerName = (string)dt.Rows[0]["FirstName"] + " " + (string)dt.Rows[0]["LastName"]; 
                _View.PropertyManagerPhone = (string)dt.Rows[0]["MainPhoneNumber"]; 
                _View.PropertyManagerEmail = (string)dt.Rows[0]["PrimaryEmailAddress"]; 
                _View.PropertyManagerImageUrl = "/Handlers/PhotoHandler.ashx?RequestType=0&propertyStaffId=" + dt.Rows[0]["PropertyStaffId"].ToString();  
            }
            sc.Close();
        }

        private void SetPropertyManagerDetails(PropertyStaff manager)
        {
            _View.PropertyManagerName = manager.DisplayName;
            _View.PropertyManagerPhone = manager.MainPhoneNumber;
            _View.PropertyManagerEmail = manager.PrimaryEmailAddress;
            _View.PropertyManagerImageUrl = "/Handlers/PhotoHandler.ashx?RequestType=0&propertyStaffId=" + manager.PropertyStaffId;
        }

        private void SetLeasingAgentDetails(PropertyStaff agent)
        {
            _View.LeasingAgentImageUrl = "/Images/IndividualImageDefault.png";

            if (agent == null) return;
            _View.LeasingAgentImageUrl = "/Handlers/PhotoHandler.ashx?RequestType=0&propertyStaffId=" + agent.PropertyStaffId;
            _View.LeasingAgentName = agent.DisplayName;
            _View.LeasingAgentPhone = agent.MainPhoneNumber;
            _View.LeasingAgentEmail = agent.PrimaryEmailAddress;
        }

        private void SetRpoSupportDetails()
        {
            _View.RpoSupportEmail = EfxSettings.SupportEmail;
            //_View.RpoSupportPhone = Settings.SupportPhone;
            //cakel: TASK 00514
            _View.RpoSupportPhone = EfxSettings.SupportPhoneResident;

        }
    }
}
