﻿using EfxFramework.ExtensionMethods;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using Mb2x.ExtensionMethods;
using System;
using System.Globalization;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class SelectPaymentTypePresenter
    {
        private readonly ISelectPaymentType _View;

		public CheckBox applyToNextMonthChk { get; set; }
		public Property currentProperty { get; set; }
		public Button PayNowButton { get; set; }
		public LinkButton LinkPayNowButton { get; set; }
		public LinkButton LinkSaveButton { get; set; }

        public SelectPaymentTypePresenter(ISelectPaymentType view)
        {
            _View = view;
            _View.PaymentTypeChanged = PaymentTypeChanged;
        }

        public void InitializeValues()
        {
            PopulateFields();
            SetActiveViewIndex();
			_View.AcceptACHPayments = true;
			_View.AcceptCCPayments = true;
        }

        public void ClearFields()
        {
            _View.CreditCardDetailsControl.NameOnCardText = String.Empty;
            _View.CreditCardDetailsControl.CreditCardNumberText = String.Empty;
            _View.CreditCardDetailsControl.CvvText = String.Empty;
            _View.CreditCardDetailsControl.ExpirationMonthSelectedValue = "1";
            _View.CreditCardDetailsControl.ExpirationYearList.SelectedIndex = 0;
            _View.ECheckDetailsControl.AccountNumberText = String.Empty;
            _View.ECheckDetailsControl.RoutingNumberText = String.Empty;
        }

        public void InitializeExpirationYears()
        {
            for (var I = 0; I < 10; I++)
            {
                var Year = (DateTime.UtcNow.Year + I).ToString(CultureInfo.InvariantCulture);
                _View.CreditCardDetailsControl.ExpirationYearList.Items.Add(new ListItem(Year, Year));
            }
        }

        private void PopulateFields()
        {
            if (_View.IsApplicant)
                return;

            var Resident = new Renter(_View.RenterId);
            if (Resident.RenterId < 1 || !Resident.PayerId.HasValue || Resident.PayerId.Value < 1)
                return;

            var Payer = new Payer(Resident.PayerId);
            if (Payer.PayerId < 1)
                return;

            var HasCreditCard = BuildCreditCardInfo(Payer.PayerId);
            var HasEcheck = BuildAchInfo(Payer.PayerId);

            if (HasEcheck && !HasCreditCard)
                _View.PaymentTypeSelectedValue = "1";
        }

        private bool BuildCreditCardInfo(int payerId)
        {
            InitializeExpirationYears();
            var PayerCreditCard = EfxFramework.PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(payerId);

            if (PayerCreditCard.PayerCreditCardId < 1)
                return false;
            
            _View.CreditCardDetailsControl.NameOnCardText = PayerCreditCard.CreditCardHolderName;
            _View.CreditCardDetailsControl.CreditCardNumberText = PayerCreditCard.CreditCardAccountNumber.MaskAccountNumber();
            _View.CreditCardDetailsControl.ExpirationMonthSelectedValue = PayerCreditCard.CreditCardExpirationMonth.ToString(CultureInfo.InvariantCulture);
            _View.CreditCardDetailsControl.ExpirationYearList.SelectedValue = PayerCreditCard.CreditCardExpirationYear.ToString(CultureInfo.InvariantCulture);

            return true;
        }

        private bool BuildAchInfo(int payerId)
        {
            var PayerAch = EfxFramework.PayerAch.GetPrimaryPayerAchByPayerId(payerId);

            if (PayerAch.PayerAchId < 1)
                return false;

            _View.ECheckDetailsControl.AccountNumberText = PayerAch.BankAccountNumber.MaskAccountNumber();
            _View.ECheckDetailsControl.RoutingNumberText = PayerAch.BankRoutingNumber;

            return true;
        }

        private void PaymentTypeChanged(object sender, EventArgs e)
        {
            SetActiveViewIndex();
			int selectedValue = 0;
			if (Int32.TryParse(_View.PaymentTypeSelectedValue, out selectedValue))
			{
				if (selectedValue == 3)
				{
					if (applyToNextMonthChk != null)
					{
						applyToNextMonthChk.Enabled = false;
						applyToNextMonthChk.Checked = false;
					}
					if (PayNowButton != null)
					{
						PayNowButton.Text = "Create PayNearMe Ticket";
					}
					if (LinkPayNowButton != null)
					{
						LinkPayNowButton.Text = "Create PayNearMe Ticket";
					}
					if (LinkSaveButton != null)
					{
						LinkSaveButton.Visible = false;
					}
				}
				else
				{
					if (applyToNextMonthChk != null && currentProperty != null && string.IsNullOrEmpty(currentProperty.PmsId) && !currentProperty.PmsTypeId.HasValue)
					{
						applyToNextMonthChk.Enabled = true;
					}
					if (PayNowButton != null)
					{
						PayNowButton.Text = "Pay Now";
					}
					if (LinkPayNowButton != null)
					{
						LinkPayNowButton.Text = "Pay Now";
					}
					if (LinkSaveButton != null)
					{
						LinkSaveButton.Visible = true;
					}
				}
			}
        }

        private void SetActiveViewIndex()
        {
            _View.PaymentTypeSelectedIndex = _View.PaymentTypeSelectedValue.ToInt32() - 1;
        }
    }
}
