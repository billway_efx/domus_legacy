﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using Mb2x.ExtensionMethods;
using System;
using System.Globalization;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class CreditCardDetailsPresenter
    {
        private readonly ICreditCardDetails _View;

        public CreditCardDetailsPresenter(ICreditCardDetails view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            PopulateExpirationYearDropDown();
        }

        private void PopulateExpirationYearDropDown()
        {
            for (var I = 0; I < 10; I++)
            {
                _View.ExpirationYearList.Items.Add(new ListItem(string.Concat("20", GetTwoDigitYear(DateTime.Now.Year + I)), GetTwoDigitYear(DateTime.Now.Year + I)));
            }
        }

        private static string GetTwoDigitYear(int year)
        {
            return year.ToString(CultureInfo.InvariantCulture).Right(2);
        }
    }
}
