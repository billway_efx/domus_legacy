﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System.Collections.Generic;
using System.Web;
using Mb2x.ExtensionMethods;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class PetsListPresenter
    {
        private readonly IPetsList _View;
        private static int? RenterId { get { return HttpContext.Current.User.Identity.Name.ToNullInt32(); } }

        public PetsListPresenter(IPetsList view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            _View.RenterPets = SetPetsListGrid();
        }

        protected List<Pet> SetPetsListGrid()
        {
            var CurrentLease = Lease.GetRenterLeaseByRenterId(RenterId.Value);
            var Fees = Pet.GetPetsByLeaseId(CurrentLease.LeaseId);
            return Fees;
        }
    }
}
