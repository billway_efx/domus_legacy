﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System;
using System.Web;
using Mb2x.ExtensionMethods;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class AccountSummarySectionPresenter
    {
        private readonly IAccountSummarySection _View;
        private readonly int? RenterId = HttpContext.Current.User.Identity.Name.ToNullInt32(); 

        public AccountSummarySectionPresenter(IAccountSummarySection view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            SetAccountSummarySection();
        }

        private void SetAccountSummarySection()
        {
            var CurrentRenter = new Renter(RenterId);
            var CurrentProperty = Property.GetPropertyByRenterId(RenterId.Value);
            var CurrentLease = Lease.GetRenterLeaseByRenterId(RenterId.Value);
            var RenterPaymentList = Payment.GetPaymentListByRenterId(RenterId.Value);
            var AutoPay = AutoPayment.GetAutoPaymentByRenterId(RenterId.Value);

            _View.NameLabel = CurrentRenter.DisplayName;
            _View.PropertyLabel = CurrentProperty.PropertyName;
            _View.LastPaymentLabel = (RenterPaymentList.Count > 0) ? RenterPaymentList[RenterPaymentList.Count - 1].
                TransactionDateTime.ToShortDateString() : "No Payments Currently Made";
            _View.NextPaymentLabel = (CurrentLease.PaymentDueDate.HasValue) ? ((DateTime)CurrentLease.PaymentDueDate).ToShortDateString()
                : "Currently Not Available";

            if (/*!AutoPay.IsActive*/ AutoPay.AutoPaymentId < 1)
                _View.AutoPayLabel = "Off";
            else
                _View.AutoPayLabel = "On";

            _View.RentLabel = CurrentLease.RentAmount.ToString("C");
        }
    }
}
