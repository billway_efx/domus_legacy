﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class AccountBalanceSectionPresenter
    {
        private readonly IAccountBalanceSection _View;

        public AccountBalanceSectionPresenter(IAccountBalanceSection view)
        {
            _View = view;
        }

        public void InitializeValues()
        {
            SetAccountBalanceSection();
        }

        private void SetAccountBalanceSection()
        {
            var CurrentLease = Lease.GetRenterLeaseByRenterId(_View.RenterId);
            var AutoPay = AutoPayment.GetAutoPaymentByRenterId(_View.RenterId);
            var AutoPayTransactions = AutoPayment.GetAutoPaymentLogTransactionDate(AutoPay.AutoPaymentId);

            //Salcedo - 5/23/2014 - Task # 0000007 - update CurrentBalanceDue from Yardi
            //cakel: BUGID00074 Function Name Update - “UpdateRenterBalanceDueForPrePay” to “UpdateRenterBalanceDueRealTimeYardi”. 
            var Resident = new Renter(_View.RenterId);
            if (Resident != null)
            {
                if (Resident.PmsTypeId == 1 && Resident.PmsId != "")
                {
                    EfxFramework.Pms.Yardi.ImportRequest.UpdateRenterBalanceDueRealTimeYardi(Resident);
                }
                //cakel: BUGID00316
                if (Resident.PmsTypeId == 2 && Resident.PmsId != "")
                {
                    
                    EfxFramework.Pms.AMSI.AmsiRequest.AMSI_RequestXmlForRenterImport(2, _RenterID: Resident.RenterId);
                }
            }

            //cakel: TASK 00407
            _View.RentLabel = CurrentLease.CurrentBalanceDue.HasValue ? (CurrentLease.CurrentBalanceDue.Value).ToString("C") : "$0.00"; //CalculateRentAmount(CurrentLease).ToString("C");
			
            if (AutoPay.IsActive && AutoPayTransactions.Count > 0)
            {
                _View.WillAutoPayVisible = true;
                _View.PayLabel = AutoPayTransactions[AutoPayTransactions.Count - 1].AddMonths(1).ToShortDateString();
            }
            else if (AutoPay.IsActive && AutoPay.AutoPaymentId == 0 && AutoPayTransactions.Count < 1)
            {
                _View.WillAutoPayVisible = true;
                _View.PayLabel = DateTime.Today < new DateTime(DateTime.Today.Year, DateTime.Today.Month,  AutoPay.PaymentDayOfMonth) ? 
                    String.Format("{0}/{1}/{2}", DateTime.Today.Month, AutoPay.PaymentDayOfMonth, DateTime.Today.Year) : 
                    String.Format("{0}/{1}/{2}", (DateTime.Today.AddMonths(1).Month), AutoPay.PaymentDayOfMonth, DateTime.Today.Year);
            }
            else
            {
                _View.WillAutoPayVisible = false;
                _View.SetUpAutoPay = "~/Payments.aspx?AutoPay=1";
                _View.PayLabel = (CurrentLease.PaymentDueDate.HasValue) ? ((DateTime)CurrentLease.PaymentDueDate).ToShortDateString()
                    : "Currently Not Available";
            }

            if (CurrentLease.CurrentBalanceDue + CurrentLease.TotalFees > 0)
                _View.SetPayment = "~/Payments.aspx?AutoPay=0";
        }

        private static decimal CalculateRentAmount(Lease lease)
        {
            if (lease.LeaseId < 1)
                return 0.00M;

            var Rent = lease.RentAmount;

            return Rent;
        }
    }
}
