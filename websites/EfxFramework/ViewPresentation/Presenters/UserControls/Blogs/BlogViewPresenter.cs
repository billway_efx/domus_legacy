﻿using EfxFramework.ViewPresentation.Interfaces.UserControls.Blogs;

namespace EfxFramework.ViewPresentation.Presenters.UserControls.Blogs
{
    public class BlogViewPresenter
    {
        private readonly IBlogView _View;

        public BlogViewPresenter(IBlogView view)
        {
            _View = view;
        }
    }
}
