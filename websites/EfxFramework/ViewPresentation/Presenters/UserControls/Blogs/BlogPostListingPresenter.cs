﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls.Blogs;

namespace EfxFramework.ViewPresentation.Presenters.UserControls.Blogs
{
    public class BlogPostListingPresenter
    {
        private readonly IBlogPostListing _View;

        public BlogPostListingPresenter(IBlogPostListing view)
        {
            _View = view;
        }

        public void WireUpDataBinding()
        {
            _View.BlogPostItemDataBound = BlogPostItemDataBound;
        }

        protected void BlogPostItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var Link = e.Item.FindControl("PostTitleLink") as HtmlAnchor;
            var Text = e.Item.FindControl("AnnouncementText") as Literal;
            var Date = e.Item.FindControl("AnnouncementDateShortDate") as Literal;
            var Title = e.Item.FindControl("AnnouncementTitle") as Literal;
            if (Link == null || Text == null || Date == null || Title == null)
                return;

            Link.Attributes.Add("onclick", String.Format("ShowPostData('{0}', '{1}', '{2}')", Text.Text.Replace("'", "\\'"), Date.Text.Replace("'", "\\'"), Title.Text.Replace("'", "\\'")));
        }
    }
}
