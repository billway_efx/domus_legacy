﻿using System;
using EfxFramework.ExtensionMethods;
using EfxFramework.ViewPresentation.Interfaces.UserControls;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
   public class PersonalInformationPresenter
   {
      private readonly IPersonalInformation _View;

      public PersonalInformationPresenter(IPersonalInformation view)
      {
         _View = view;
      }

      public void InitializeValues()
      {
         var Resident = new Renter(_View.RenterId);

         if (Resident.RenterId < 1)
            return;

         PopulateFields(Resident);
      }

      private void PopulateFields(Renter resident)
      {
         _View.FirstNameText = resident.FirstName;
         _View.MiddleNameText = resident.MiddleName;
         _View.LastNameText = resident.LastName;
         _View.SuffixText = resident.Suffix;
         _View.EmailAddressText = resident.PrimaryEmailAddress;

         if (String.IsNullOrEmpty(resident.MainPhoneNumber))
         {
            _View.PhoneNumberText = resident.MainPhoneNumber.FormatPhoneNumber();
         }
         else
         {
            _View.PhoneNumberText = resident.MobilePhoneNumber.FormatPhoneNumber();
         }


      }
   }
}
