﻿using EfxFramework.ExtensionMethods;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System;
using System.Globalization;

namespace EfxFramework.ViewPresentation.Presenters.UserControls
{
    public class ResidentBillingInformationPresenter
    {
        private readonly IResidentBillingInformation _View1;
        private readonly IPersonalInformation _View2;

        public ResidentBillingInformationPresenter(IResidentBillingInformation view)
        {
            _View1 = view;
            _View1.SameAsMailingChecked = ToggleSameAsMailing;
        }
        
        public void InitializeValues()
        {
            _View1.StateDropDown.BindToSortedEnum(typeof (StateProvince));
            PopulateFields();
        }

        private void PopulateFields()
        {
            if (_View1.IsSameAsMailingChecked)
                PopulateMailingAddress();
            else
                PopulateBillingAddress();
        }

        private void PopulateBillingAddress()
        {
            var Address = EfxFramework.Address.GetAddressByRenterIdAndAddressType(_View1.RenterId, (int) TypeOfAddress.Billing);
            _View1.BillingAddressText = Address.AddressLine1;
            _View1.CityText = Address.City;
            _View1.StateDropDown.SelectedValue = Address.StateProvinceId > 0 ? Address.StateProvinceId.ToString(CultureInfo.InvariantCulture) : "1";
            _View1.ZipText = Address.PostalCode;
            _View1.BillingPhone = Address.PrimaryPhoneNumber;
        }

        private void PopulateMailingAddress()
        {
            if (_View1.RenterId < 1)
                return;

            var Resident = new Renter(_View1.RenterId);
            if (Resident.RenterId < 1)
                return;

            _View1.BillingAddressText = String.Format("{0} {1}", Resident.StreetAddress, Resident.Unit);
            _View1.CityText = Resident.City;
            _View1.StateDropDown.SelectedValue = Resident.StateProvinceId.HasValue ? Resident.StateProvinceId.Value.ToString(CultureInfo.InvariantCulture) : "1";
            _View1.ZipText = Resident.PostalCode;
        }

        private void ToggleSameAsMailing(object sender, EventArgs e)
        {
            PopulateFields();
        }
    }
}
