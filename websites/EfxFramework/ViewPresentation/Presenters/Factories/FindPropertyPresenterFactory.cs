﻿using EfxFramework.ViewPresentation.Interfaces.Pages.FindPropertyPages;
using EfxFramework.ViewPresentation.Presenters.Pages.FindPropertyPresenters;

namespace EfxFramework.ViewPresentation.Presenters.Factories
{
    public static class FindPropertyPresenterFactory
    {
        public static FindPropertyPresenterBase GetFindPropertyPresenter(ICurrentResidentFindProperty view)
        {
            return new CurrentResidentFindPropertyPresenter(view);
        }

        public static FindPropertyPresenterBase GetFindPropertyPresenter(IFutureResidentFindProperty view)
        {
            return new FutureResidentFindPropertyPresenter(view);
        }
    }
}
