﻿using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;
using EfxFramework.ViewPresentation.Presenters.UserControls.PaymentTiles;

namespace EfxFramework.ViewPresentation.Presenters.Factories
{
    public static class PaymentTilePresenterFactory
    {
        public static PaymentTilePresenterBase GetPresenter(IAutoPayTile view)
        {
            return new AutoPayTilePresenter(view);
        }

        public static PaymentTilePresenterBase GetPresenter(ITextToPayTile view)
        {
            return new TextToPayTilePresenter(view);
        }
    }
}
