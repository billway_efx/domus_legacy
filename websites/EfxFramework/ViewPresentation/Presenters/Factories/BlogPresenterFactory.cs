﻿using EfxFramework.ViewPresentation.Interfaces.Pages.BlogPages;
using EfxFramework.ViewPresentation.Presenters.Pages.BlogPresenters;

namespace EfxFramework.ViewPresentation.Presenters.Factories
{
    public static class BlogPresenterFactory
    {
        public static BlogPresenterBase GetBlogPresenter(IPublicNewsAndEvents view)
        {
            return new PublicNewsAndEventsPresenter(view);
        }

        public static BlogPresenterBase GetBlogPresenter(IPublicProsCorner view)
        {
            return new PublicProsCornerPresenter(view);
        }

        public static BlogPresenterBase GetBlogPresenter(IPublicResidentsRoom view)
        {
            return new PublicResidentsRoomPresenter(view);
        }
    }
}
