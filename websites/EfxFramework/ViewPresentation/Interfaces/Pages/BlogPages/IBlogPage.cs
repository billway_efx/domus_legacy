﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EfxFramework.ViewPresentation.Interfaces.UserControls.Blogs;

namespace EfxFramework.ViewPresentation.Interfaces.Pages.BlogPages
{
    public interface IBlogPage
    {
        IBlogView BlogViewUserControl { get; }
    }
}
