﻿
namespace EfxFramework.ViewPresentation.Interfaces.Pages.BlogPages
{
    public interface IPublicNewsAndEvents : IBlogPage
    {
        string NewsBlogUrl { get; }
    }
}
