﻿using EfxFramework.Interfaces;
using EfxFramework.ViewPresentation.Interfaces.UserControls;

namespace EfxFramework.ViewPresentation.Interfaces.Pages
{
    public interface IFutureResidentRegister : IBasePageV2
    {
        int PropertyId { get; }
        IResidentRegister ResidentRegisterControl { get; }
        IForgotPassword ForgotPasswordControl { get; }
    }
}
