﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using EfxFramework.Interfaces;
using EfxFramework.ViewPresentation.Interfaces.UserControls;

namespace EfxFramework.ViewPresentation.Interfaces.Pages
{
    public interface IJobs : IBasePageV2
    {
        int JobId { get; }
        bool AreJobsAvailable { set; }
        EventHandler JobsListedSelectedIndexChanged { set; }
        DropDownList JobsListed { get; }
        string JobTitleText { get; set; }
        string JobPostDateText { get; set; }
        string JobDescriptionLiteralText { set; }
        string DutiesLiteralText { set; }
        string SkillsLiteralText { set; }
        IApplyNowWindow ApplyNowModal {get;}
        bool JobPostingVisible { set; }
        bool ShowText { set; }
    }
}
