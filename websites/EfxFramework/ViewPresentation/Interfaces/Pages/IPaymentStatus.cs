﻿using EfxFramework.Interfaces;
using System;

namespace EfxFramework.ViewPresentation.Interfaces.Pages
{
    public interface IPaymentStatus : IBasePageV2
    {
        int UserId { get; }
        string TransactionId { get; set; }
        string RentAmount { set; }
        string ConvenienceFee { set; }
        string PaymentTotal { set; }
        string PropertyName { set; }
        string PropertyAddress { set; }
        string PropertyCityStateZip { set; }
        string FirstName { set; }
        string MiddleName { set; }
        string LastName { set; }
        string Suffix { set; }
        string BillingAddress { set; }
        string BillingCityStateZip { set; }
        string PhoneNumber { set; }
        string PaymentMethod { set; }
        EventHandler PrintClicked { set; }
    }
}
