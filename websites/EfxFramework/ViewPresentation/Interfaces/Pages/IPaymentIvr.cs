﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfxFramework.ViewPresentation.Interfaces.Pages
{
    public interface IPaymentIvr
    {
        string PaymentQueueNumber { get; set; }
        string RentAmount { set; }
        string FeeAmount { set; }
        string TotalPayment { set; }
        string PropertyName { set; }
        string PropertyAddress { set; }
        string PropertyCityStateZip { set; }
        string FirstName { set; }
        string MiddleName { set; }
        string LastName { set; }
        string Suffix { set; }
        string BillingAddress { set; }
        string BillingCityStateZip { set; }
        string Phone { set; }
    }
}
