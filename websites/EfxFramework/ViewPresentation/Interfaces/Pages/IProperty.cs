﻿using EfxFramework.Interfaces;
using EfxFramework.ViewPresentation.Interfaces.UserControls;

namespace EfxFramework.ViewPresentation.Interfaces.Pages
{
    public interface IProperty : IBasePageV2
    {
        int UserId { get; }
        IPropertyContacts PropertyContactsControl { get; }
        IPropertyMaintenanceRequest PropertyMaintenanceRequestControl { get; }
    }
}
