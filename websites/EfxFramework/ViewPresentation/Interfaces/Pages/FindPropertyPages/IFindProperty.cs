﻿using EfxFramework.ViewPresentation.Interfaces.UserControls;

namespace EfxFramework.ViewPresentation.Interfaces.Pages.FindPropertyPages
{
    public interface IFindProperty
    {
        IFindPropertyUserControl FindPropertyControl { get; }
    }
}
