﻿using System.Web.UI.WebControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.Interfaces;

namespace EfxFramework.ViewPresentation.Interfaces.Pages
{
    public interface IContact: IBasePageV2
    {
        string ContactType { get; }
        RadioButtonList ContactRpoRadioButtonList { get; }
        IContactForm ContactUserControl { get; }
    }
}
