﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.Interfaces;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles;

namespace EfxFramework.ViewPresentation.Interfaces.Pages
{
    public interface IPayment : IBasePageV2
    {
        Page ViewPage { get; }
        IPersonalInformation PersonalInformationControl { get; }
        ISelectPaymentType SelectPaymentTypeControl { get; }
        IResidentBillingInformation ResidentBillingInformationControl { get; }
        IPaymentAmounts PaymentAmountsControl { get; }
        IAutoPayTile AutoPayTileControl { get; }
        ITextToPayTile TextToPayTileControl { get; }
        IPaymentHistory PaymentHistoryControl { get; }
        IConfirmationWindow ConfirmationWindowControl { get; }
        bool TermsAccepted { get; }
        EventHandler PaymentCancelButtonClick { set; }
        EventHandler PaymentSaveButtonClick { set; }
        EventHandler PaymentPayNowButtonClick { set; }
        ServerValidateEventHandler RequiredCvv { set; }
        bool EnablePayNow { set; }
    }
}
