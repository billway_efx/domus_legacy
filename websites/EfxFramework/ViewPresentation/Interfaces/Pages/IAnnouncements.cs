﻿using System.Web.UI.WebControls;
using EfxFramework.Interfaces;
using System.Collections.Generic;

namespace EfxFramework.ViewPresentation.Interfaces.Pages
{
    public interface IAnnouncements : IBasePageV2
    {
        List<Announcement> PropertyAnnouncementsListView { set; }
        List<Announcement> RpoAnnouncementsListView { set; }
    }
}
