﻿using EfxFramework.Interfaces;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.Pages
{
    public interface IContactResident: IBasePageV2
    {
        int RenterId { get; }
        IContactForm ContactUserControl { get; }
        CheckBoxList ContactRentPaidOnlineList { get; }
        ServerValidateEventHandler IsCheckBoxChecked { set; }
    }
}
