﻿using EfxFramework.Interfaces;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.Pages
{
    public interface ICurrentResidentsRegister : IBasePageV2
    {
        IResidentRegister ResidentRegisterControl { get; }
		Label ExistingUser { get; }
    }
}
