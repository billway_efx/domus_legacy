﻿using System.Collections.Generic;

namespace EfxFramework.ViewPresentation.Interfaces.Pages
{
    public interface IAnnouncementsHome
    {
        List<Announcement> PropertyManagerAnnouncementDataSource { set; }
        List<Announcement> RpoAnnouncementDataSource { set; }
        string ZipCode { get; set; }
        string CityState { get; set; }
    }
}
