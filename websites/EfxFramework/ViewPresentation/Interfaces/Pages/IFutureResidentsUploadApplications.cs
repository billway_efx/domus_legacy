﻿using System.Web.UI;
using EfxFramework.Interfaces;
using EfxFramework.ViewPresentation.Interfaces.UserControls;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.Pages
{
    public interface IFutureResidentsUploadApplications : IBasePageV2
    {
        int PropertyId { get; }
        int ApplicantId { get; }
        FileUpload FileUploadControl { get; }
        EventHandler UploadButtonClicked { set; }
        ISelectPaymentType PaymentTypeControl { get; }
        IPayerBillingInformation PayerBillingInformationControl { get; }
        List<ApplicationFee> FeesDataSource { set; }
        bool AcceptTermsChecked { get; }
        EventHandler CompleteButtonClicked { set; }
        Page ViewPage { get; }
        string PropertyNameText {set;}
    }
}
