﻿using System;

namespace EfxFramework.ViewPresentation.Interfaces.Pages
{
    public interface IApplicationDownload
    {
        int PropertyId { get; }
        int ApplicantId { get; }
        EventHandler DownloadButtonClick { set; }
        bool NoPropertyFoundVisible { set; }
        bool ApplicationDownloadPanelVisible { set; }
        string PropertyNameText { set; }
        string PropertyImageUrl { set; }
        string NextButtonUrlText { set; }
    }
}
