﻿using EfxFramework.Interfaces;
using EfxFramework.Interfaces.UserControls.ImageAdder;
using System;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.Pages
{
    public interface IMyAccount : IBasePageV2
    {
        bool IsRefresh { get; }
        int AddressView { get; set; }
        int UserId { get; }
        bool IsRentReportersIdVisible { set; }
        string ResidentIdText { get; set; }
        string RentReportersId { get; set; }
        string Email { get; set; }
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string LastName { get; set; }
        string Suffix { get; set; }
        string MailingAddress { get; set; }
        string MailingCity { get; set; }
        string MailingZip { get; set; }
        string IntegratedMailingAddress { set; }
        string IntegratedCityStateZip { set; }
        string MainPhone { get; set; }
        string AltPhone { get; set; }
        string CurrentPin { set; }
        string NewPin { get; }
        string NewPinConfirm { get; }
        string Password { get; }
        string PasswordConfirm { get; }
        string PhotoUrl { set; }
        EventHandler SaveButtonClicked { set; }
        IImageAdder ImageAdderControl { get; }
        ServerValidateEventHandler PinValidation { set; }
        ServerValidateEventHandler PasswordValidation { set; }
        DropDownList StateDropDown { get; }
    }
}
