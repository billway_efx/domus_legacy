﻿using EfxFramework.Interfaces;
using System;

namespace EfxFramework.ViewPresentation.Interfaces.Pages
{
    public interface ILoginV2 : IBasePageV2
    {
        string UsernameText { get; }
        string PasswordText { get; }
        int LoggedInUserId { set; }
        EventHandler LoginButtonClick { set; }
    }
}
