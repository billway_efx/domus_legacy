﻿using EfxFramework.Interfaces;
using System;
using System.Collections.Generic;

namespace EfxFramework.ViewPresentation.Interfaces.Pages
{
    public interface IFutureResidentsApplicationReceipt : IBasePageV2
    {
        int PropertyId { get; }
        string PropertyName { set; }
        string TransactionId { get; }
        int ApplicantId { get; }
        string UploadStatus { set; }
        List<ApplicationFee> Fees { set; }
        EventHandler PrintClick { set; }
        string StatusButtonNavigateUrl { set; }
        //CMallory - Task 00499
        string ApplyConnectButtonNavigateUrl { set; }
    }
}
