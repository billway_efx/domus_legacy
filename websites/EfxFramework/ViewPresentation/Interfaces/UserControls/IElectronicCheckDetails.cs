﻿
namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IElectronicCheckDetails
    {
        string RoutingNumberText { get; set; }
        string AccountNumberText { get; set; }
        string HiddenACHAccountNumber { get; set; }
    }
}
