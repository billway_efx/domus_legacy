﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{ 
    public interface IPropertyAnnouncements
    {
        string TitleText { get; set; }
        string BodyText { get; set; }
        string UrlText { get; set; }
        bool CheckBoxIsVisible { get; set; }
        bool CheckBoxIsChecked { get; set; }
        EventHandler SaveButtonClicked {set;}
        GridView PostedAnnoucementGrid { get; set; }
        Page ViewPage { get; }
        GridViewSelectEventHandler EditRow { set; }
        GridViewDeleteEventHandler DeleteRow { set; }
        int PropertyId { get; }
        int CurrentStaff { get; }
        bool IsAdminSite { get; }
        HiddenField EditEnabled { get; set; }
        HiddenField AnnouncementId { get; set; }
        List<int> PropertyIDs { get; set; }
        DateTime ExpirationDate { get; set; } //CMallory - Task00462 - Added
        EventHandler ViewAnnouncementsButtonClicked {set;} //CMallory - Task00462 - Added
    }
}
 