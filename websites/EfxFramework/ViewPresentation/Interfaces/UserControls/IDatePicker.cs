﻿using System;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IDatePicker
    {
        int StartYear { get; set; }
        int NumberOfYears { get; set; }
        DropDownList MonthList { get; }
        DropDownList DayList { get; }
        DropDownList YearList { get; }

        void SetYearList();
        void SetDefaults();
        DateTime GetDate();
    }
}
