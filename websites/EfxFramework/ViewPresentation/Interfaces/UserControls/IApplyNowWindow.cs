﻿using System;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IApplyNowWindow
    {
        EventHandler SendButtonClick { set; }
        string ToText { get; set; }
        string FromText { get; set; }
        string SubjectText { get; set; }
        string BodyText { get; set; }
        FileUpload FileUploadControl { get; }
    }
}
