﻿
namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IPropertyContacts
    {
        int UserId { get; }

        //Property properties
        string PropertyName { set; }
        string PropertyAddress { set; }
        string PropertyCityStateZip { set; }
        string PropertyPhone { set; }
        string PropertyFax { set; }
        string PropertyEmail { set; }
        string PropertyImageUrl { set; }

        //Property manager properties
        string PropertyManagerName { set; }
        string PropertyManagerPhone { set; }
        string PropertyManagerEmail { set; }
        string PropertyManagerImageUrl { set; }

        //Leasing agent properties
        string LeasingAgentName { set; }
        string LeasingAgentPhone { set; }
        string LeasingAgentEmail { set; }
        string LeasingAgentImageUrl { set; }

        //RPO properties
        string RpoSupportPhone { set; }
        string RpoSupportEmail { set; }
    }
}
