﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls.Blogs
{
    public interface IBlogView
    {
        IBlogPostListing RecentBlogPostListUserControl { get; }
        ListView ArchivedBlogPostList { get; set; }
        EventHandler<ListViewItemEventArgs> ArchiveItemDataBound { set; }
        Page ParentPage { get; }
    }
}
