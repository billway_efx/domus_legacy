﻿using System;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls.Blogs
{
    public interface IBlogPostListing
    {
        ListView BlogPostList { get; }
        EventHandler<ListViewItemEventArgs> BlogPostItemDataBound { set; }

        void WireUpDataBinding();
    }
}
