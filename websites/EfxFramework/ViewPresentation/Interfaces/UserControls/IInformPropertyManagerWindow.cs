﻿namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IInformPropertyManagerWindow
    {
        string OfficePhoneClientId { get; }
    }
}
