﻿using System;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles
{
    public interface IPaymentTile
    {
        int RenterId { get; set; }
        EventHandler OnButtonClick { set; }
        EventHandler OffButtonClick { set; }
        string OnButtonCssClass { get;  set; }
        string OffButtonCssClass { get;  set; }
        DropDownList DayOfTheMonthDropdown { get; }
        DropDownList PaymentTypeDropdown { get; }
        bool IsOn { get; set; }
    }
}
