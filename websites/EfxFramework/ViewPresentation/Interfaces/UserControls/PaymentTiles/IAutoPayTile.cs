﻿using System;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles
{
    public interface IAutoPayTile : IPaymentTile
    {
        string PaymentAmountText { set; }
        EventHandler PaymentTypeChanged { set; }
    }
}
