﻿using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls.PaymentTiles
{
    public interface ITextToPayTile : IPaymentTile
    {
        string MobileNumberText { get; set; }
        ServerValidateEventHandler RequiredMobileNumberValidation { set; }
        string MobileNumberClientId { get; }
    }
}
