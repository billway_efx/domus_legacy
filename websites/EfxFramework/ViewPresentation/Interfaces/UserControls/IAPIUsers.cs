﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using EfxFramework.PublicApi;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IAPIUsers
    {
        Page ParentPage { get; }
        int SelectedView { set; }
        List<ApiUser> ApiUsersDataSource { set; }
        CheckBox SelectAllProperties { get; }
        GridView ApiUserData { get; }
        GridViewSelectEventHandler EditRow { set; }
        GridViewDeleteEventHandler DeleteRow { set; }
        HiddenField EditEnabled {get; set;}
        HiddenField HiddenId { get; set; }
        EventHandler AddNewApiUser { set; }
        string UsernameText { get; set; }
        string PasswordText { get; set; }
        string ConfirmPasswordText { get; set; }
        List<Property> PropertiesCheckListDataSource { set; }
        CheckBoxList PropertiesCheckList { get; }
        List<PartnerCompany> PartnersCheckListDataSource { set; }
        CheckBoxList PartnershipsCheckList { get; }
        EventHandler SaveButtonClick { set; }
        EventHandler CancelButtonClick { set; }
        int ApiUserId { get; }
        ServerValidateEventHandler OnlyOnePartner { set; }
		bool SetApiUserTabAsActive { get; set; }
    }
}
