﻿using System;
using System.Collections.Generic;
using System.Web.UI;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IPartnerCompanies
    {
        List<PartnerCompany> PartnerCompanyList { set; }
        EventHandler AddButtonClick { set; }
        string PartnerNameText { get; }
        Page ParentPage { get; }
    }
}
