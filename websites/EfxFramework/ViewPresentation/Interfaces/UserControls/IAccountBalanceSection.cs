﻿
namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IAccountBalanceSection
    {
        string RentLabel { set; }
        string WillAutoPayLabel { set; }
        bool WillAutoPayVisible { set; }
        string PayLabel { set; }
        int RenterId { get; }
        string SetUpAutoPay { set; }
        string SetPayment { set; }
    }
}
