﻿using System;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IResidentRegister
    {
        string FirstName { get; }
        string LastName { get; }
        string PropertyName { set; }
        string UnitNumber { get; }
        string ResidentIdNumber { get; }
        string PhoneNumber { get; }
        string Email { get; }
        string Password { get; }
        string PasswordConfirm { get; }
        EventHandler SubmitClick { set; }
        ServerValidateEventHandler UnitNumberServerValidate { set; }
        ServerValidateEventHandler ResidentIdNumberServerValidate { set; }
        string PhoneNumberClientId { get; }
		bool DisplayResidentId { get; set; }
    }
}
