﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IRentAndFees
    {
        int PropertyId { get; }
        Page ViewPage { get; }
        List<ApplicationFee> ListOfApplicationFees { set; }
        EventHandler AddApplicationFeeClicked { set; }
        string ApplicationFeeName { get; set; }
        string ApplicationFeeAmount { get; set; }
        List<MonthlyFee> ListOfMonthlyFees { set; }
        EventHandler AddMonthlyFeeClicked { set; }
        string MonthlyFeeName { get; set; }
        string MonthlyFeeAmount { get; set; }
        bool IsFeeForAllResidents { get; set; }
        bool IsIntegratedProperty { get; set; }
        bool IsIntegratedChecked { get; set; }
        EventHandler SaveButtonClicked { set; }
		PlaceHolder SuccessMessagePlaceHolder { get; }
    }

    public class MonthlyToLeaseRecord
    {
       public int RenterId {get; set;}
       public int LeaseId { get; set; }
    }
}
