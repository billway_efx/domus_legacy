﻿using System.Collections.Generic;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IPaymentHistory
    {
        List<PaymentHistory> PaymentList { set; }
        int RenterId { get; }
    }
}
