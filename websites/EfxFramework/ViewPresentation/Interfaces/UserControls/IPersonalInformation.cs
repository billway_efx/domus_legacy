﻿namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IPersonalInformation
    {
        int RenterId { get; set; }
        string FirstNameText { get; set; }
        string MiddleNameText { get; set; }
        string LastNameText { get; set; }
        string SuffixText { get; set; }
        string EmailAddressText { get; set; }
        string PhoneNumberText { get; set; }
    }
}
