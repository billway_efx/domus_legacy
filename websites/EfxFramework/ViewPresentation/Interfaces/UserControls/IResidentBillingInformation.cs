﻿using System;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IResidentBillingInformation
    {
        int RenterId { get; set; }
        EventHandler SameAsMailingChecked { set; }
        bool IsSameAsMailingChecked { get; set; }
        string BillingAddressText { get; set; }
        string CityText { get; set; }
        DropDownList StateDropDown { get; }
        string ZipText { get; set; }
        string BillingPhone { get; set; }
        //string EmailAddressText { get; set; }
        string ZipCodeClientId { get; }
        //string PhoneNumberClientId { get; }
    }
}
