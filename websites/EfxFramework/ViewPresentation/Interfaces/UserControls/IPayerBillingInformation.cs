﻿using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IPayerBillingInformation
    {
        string FirstNameText { get; }
        string LastNameText { get; }
        string BillingAddressText { get; }
        string CityText { get; }
        DropDownList StateDropDown { get; }
        string ZipText { get; }
        string ZipCodeClientId { get; }
    }
}
