﻿namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IAccountSummarySection
    {
        string NameLabel { set; }
        string PropertyLabel { set; }
        string LastPaymentLabel { set; }
        string NextPaymentLabel { set; }
        string AutoPayLabel { set; }
        string RentLabel { set; }
    }
}
