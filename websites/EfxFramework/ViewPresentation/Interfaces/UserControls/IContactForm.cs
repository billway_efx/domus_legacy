﻿using System;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IContactForm
    {
        string NameText { get; set; }
        string EmailAddressText { get; set; }
        string PhoneNumberText { get; set; }
        string PropertyNameText { get; set; }
        string MessageText { get; set; }
        EventHandler SendButtonClick { set; }
        int SetContactMultiView { set ;}
        string PhoneNumberClientId { get; }
    }
}
