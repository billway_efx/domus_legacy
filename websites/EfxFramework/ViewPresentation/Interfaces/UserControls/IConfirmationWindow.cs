﻿namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IConfirmationWindow
    {
        string ConfirmationHeaderText { set; }
        string ConfirmationLabelText { set; }
        string LinkUrl { set; }
    }
}
