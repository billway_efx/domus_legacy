﻿using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface ICreditCardDetails
    {
        string NameOnCardText { get; set; }
        string CreditCardNumberText { get; set; }
        string ExpirationMonthSelectedValue { get; set; }
        DropDownList ExpirationYearList { get; }
        string CvvText { get; set; }
        string HiddenCreditCard { get; set; }
    }
}
