﻿using System.Collections.Generic;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IOccupantList
    {
        List<Occupant> ListOfPeople {set;}
    }

    public class Occupant
    {
        public string OccupantType { get; set; }
        public string OccupantName { get; set; }
    }
}
