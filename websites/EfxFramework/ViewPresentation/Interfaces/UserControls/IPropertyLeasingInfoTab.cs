﻿namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IPropertyLeasingInfoTab
    {
        int UserId { get; }

        string PropertyNameLabelText { set; }
        string PropertyAddressLabelText { set; }
        string PropertyCityStateZipLabelText { set; }
        string LeaseStartDateLabelText { set; }
        string LeaseEndDateLabelText { set; }
        string DueDateMonthLabelText { set; }
        string RentAmountLabelText { set; }        
    }
}
