﻿using System.Collections.Generic;
using EfxFramework.ViewPresentation.Interfaces.Pages;
using System.Web.UI.WebControls;
using System;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IPaymentAmounts
    {
        int RenterId { get; set; }
        IPayment ParentPage { get; }
        string RentAmountText { set; }
        string ConvenienceFeeText { set; }
        string TotalAmountText { set; }
        string PastDueHeaderText { set; }
        string PastDueAmountText { set; }
        bool PastDueVisible { set; }
        List<LeaseFee> MonthlyFeesDataSource { set; }
        ISelectPaymentType PaymentTypes { get; set; }
        decimal ConvenienceFeeAmount { get; set; }
        decimal RentAmount { get; set; }
    }
}
