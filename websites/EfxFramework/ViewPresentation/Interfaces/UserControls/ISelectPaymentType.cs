﻿using System;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface ISelectPaymentType
    {
        int RenterId { get; set; }
        EventHandler PaymentTypeChanged { set; }
        string PaymentTypeSelectedValue { get; set; }
        int PaymentTypeSelectedIndex { set; }
        ICreditCardDetails CreditCardDetailsControl { get; }
        IElectronicCheckDetails ECheckDetailsControl { get; }
        bool IsApplicant { get; set; }
		bool AcceptACHPayments { get; set; }
		bool AcceptCCPayments { get; set; }
		bool PNMEnabled { get; set; }
		int PropertyId { get; set; }

        void InitializeCreditCardExpiration();
    }
}
