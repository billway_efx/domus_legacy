﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IPropertyMaintenanceRequest
    {
        int UserId { get; }
        DropDownList RequestType { get; }
        DropDownList RequestPriority { get;}
        //cakel: BUGID00087
        DropDownList RequestPermission { get; }
        bool CarbonCopy { get; set; }
        string ProblemDescription { get; set; }
        EventHandler SubmitClick { set; }
        Page ParentPage { get; }
    }
}
