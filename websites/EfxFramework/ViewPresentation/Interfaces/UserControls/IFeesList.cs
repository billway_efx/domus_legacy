﻿using System.Collections.Generic;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IFeesList
    {
        List<LeaseFee> RenterMonthlyFees { set; }
        int RenterId { get; }
    }
}
