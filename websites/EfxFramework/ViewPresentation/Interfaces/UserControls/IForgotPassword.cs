﻿using System;
using System.Web.UI;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IForgotPassword
    {
        string Email { get; }
        EventHandler SubmitClick { set; }
        Page ParentPage { get; }
    }
}
