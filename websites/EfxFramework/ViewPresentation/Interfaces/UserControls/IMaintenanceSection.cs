﻿using System;
using System.Collections.Generic;
using System.Web.UI;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IMaintenanceSection
    {
        Page PageView { get; }
        EventHandler ViewMoreRequestButtonClick { set; }
        EventHandler SubmitMaintenanceRequestButtonClick { set; }
        IMaintenanceRequestGrid GridForMaintenance { get; }
        List<MaintenanceRequest> ModalGrid { set; }
        int? RenterId { get; }
    }
}
