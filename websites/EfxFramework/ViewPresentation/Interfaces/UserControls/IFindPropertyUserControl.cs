﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IFindPropertyUserControl
    {
        List<SearchGridProperty> PropertyListDataSource { set; }
        EventHandler SearchButtonClick { set; }
        string PropertyName { get; set; }
        string City { get; set; }
        DropDownList State { get; set; }
        string ZipCode { get; set; }
        string PropertyNameClientId { get; }
        string ZipCodeClientId { get; }
    }
}
