﻿using System.Collections.Generic;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IPetsList
    {
        List<Pet> RenterPets { set; }
    }
}
