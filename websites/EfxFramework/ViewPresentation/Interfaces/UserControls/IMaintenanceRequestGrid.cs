﻿using System.Collections.Generic;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface IMaintenanceRequestGrid
    {
        List<MaintenanceRequest> RequestGrid {set; }
        int RecordsToDisplay { get; }
    }
}
