﻿using EfxFramework.NewsApi.ResponseStructure;
using System.Collections.Generic;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface INewsAndWeatherSection 
    {
        int UserId { get; }
        IEnumerable<NewsResult> NewsResultDataSource { set; }
        string Temp { set; }
        string RelativeHumidity { set; }
        string WeatherString { set; }
        string WindSpeed { set; }
        string GustingWindSpeed { set; }
        string WeatherImageUrl { set; }
        string LocationCity { set; }
    }
}
