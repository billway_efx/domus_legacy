﻿using System.Collections.Generic;

namespace EfxFramework.ViewPresentation.Interfaces.UserControls
{
    public interface INewsSection
    {
        List<Announcement> NewsListView { set; }
    }
}
