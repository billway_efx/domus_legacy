﻿using System.Net.Mail;
using EfxFramework.PaymentMethods;
using EfxFramework.PaymentMethods.Ach;
using EfxFramework.PaymentMethods.CreditCard;
using System;
using System.Collections.Generic;
using System.Globalization;
using EfxFramework.PublicApi.Payment;
using EfxFramework.Resources;

namespace EfxFramework.Sms
{
    public class PaymentRequest
    {
        public string Phone { get; set; }
        public string Message { get; set; }
        public string Id { get; set; }
        public Carrier? Carrier { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }

        public PaymentRequest()
        {
            Id = "0";
        }

        public PaymentRequest(string phone, string message, string id, string carrier)
        {
            if (phone.StartsWith("1"))
                phone = phone.Remove(0, 1);

            Phone = phone;
            Message = message;
            Id = id;

            try
            {
                Carrier = (Carrier) Enum.Parse(typeof (Carrier), carrier);
            }
            catch
            {
                Carrier = null;
            }
        }

        public void ProcessPayment()
        {
            var Renter = EfxFramework.Renter.GetRenterBySmsPaymentPhoneNumber(Phone);
            if (Renter.RenterId < 1)
            {
                ReplyToSender(ServiceResponseMessages.PhoneNumberNotFound, Phone, Id);
                return;
            }

            Renter.IsParticipating = true;
            EfxFramework.Renter.Set(Renter, "System Process");

            NotifyRenter(ProcessPayment(Renter), this, Renter);
        }

        private PaymentResponse ProcessPayment(Renter renter)
        {
            var Lease = EfxFramework.Lease.GetRenterLeaseByRenterId(renter.RenterId);
            var Property = EfxFramework.Property.GetPropertyByRenterId(renter.RenterId);

            if (!renter.PayerId.HasValue || renter.PayerId.Value < 1)
                return new PaymentResponse {Result = SmsResponseResult.PayerNotFound, ResponseMessage = ServiceResponseMessages.Ccpayernotfound};

            if (Lease.LeaseId < 1)
                return new PaymentResponse {Result = SmsResponseResult.LeaseNotFound, ResponseMessage = ServiceResponseMessages.LeaseNotFound};

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
                return new PaymentResponse {Result = SmsResponseResult.PropertyNotFound, ResponseMessage = ServiceResponseMessages.PropertyNotFound};

            if (renter.AcceptedPaymentTypeId == 4)
                return new PaymentResponse {Result = SmsResponseResult.CannotAcceptRenterPayment, ResponseMessage = ServiceResponseMessages.CannotAcceptRenterPayment};

            if (!Lease.CurrentBalanceDue.HasValue)
                Amount = 0.00M;
            //cakel 00595: Updated code to remove the lease.totalFees - this was adding the fee twice
            //else if (Lease.CurrentBalanceDue.Value + Lease.TotalFees > 0.00M)
            else if (Lease.CurrentBalanceDue.Value > 0.00M)
                //Amount = Lease.CurrentBalanceDue.Value + Lease.TotalFees;
                Amount = Lease.CurrentBalanceDue.Value;
            else if (!Lease.PaymentDueDate.HasValue)
                Amount = 0.00M;
            //cakel: 00595
            //else if (Lease.CurrentBalanceDue.Value + Lease.TotalFees <= 0.00M && Lease.PaymentDueDate.Value.Subtract(DateTime.UtcNow) <= new TimeSpan(10, 0, 0, 0))
            else if (Lease.CurrentBalanceDue.Value <= 0.00M && Lease.PaymentDueDate.Value.Subtract(DateTime.UtcNow) <= new TimeSpan(10, 0, 0, 0))
                Amount = Lease.RentAmount + Lease.CurrentBalanceDue.Value;
            else
                Amount = 0.00M;

            if (Amount < 0.00M)
                Amount = 0.00M;

            switch (renter.SmsPaymentTypeId)
            {
                case 1:
                    var PayerCreditCardObj = PayerCreditCard.GetPrimaryPayerCreditCardByPayerId(renter.PayerId.Value);
                    return ProcessPayment(this, PayerCreditCardObj, renter, Lease);
                case 2:
                    if (renter.AcceptedPaymentTypeId == 3)
                        return new PaymentResponse {Result = SmsResponseResult.CashEquivalentRequired, ResponseMessage = ServiceResponseMessages.CashEquivalentRequired};

                    var PayerAch = EfxFramework.PayerAch.GetPrimaryPayerAchByPayerId(renter.PayerId.Value);
                    return ProcessPayment(this, PayerAch, renter, Lease);
                default:
                    return new PaymentResponse {Result = SmsResponseResult.UnknownPaymentType, ResponseMessage = ServiceResponseMessages.UnknownPaymentTypeEntered};
            }
        }

        private static PaymentResponse ProcessPayment(PaymentRequest request, PayerCreditCard payerCreditCard, Renter renter, Lease lease)
        {
            var Property = EfxFramework.Property.GetPropertyByRenterId(renter.RenterId);
            var PaymentAmount = new PaymentAmount(request.Amount, "Convenience Fee", PaymentType.CreditCard, renter.RenterId);

            var CustomFields = new List<CustomField>
                {
                    new CustomField("customfield1", Property.PropertyId.HasValue ? Property.PropertyId.Value.ToString(CultureInfo.InvariantCulture) : "0"), 
                    new CustomField("customfield2", lease.UnitNumber), 
                    new CustomField("customfield3", String.Format("{0}{1}", DateTime.Now.Month.ToString(CultureInfo.InvariantCulture), DateTime.Now.Year.ToString(CultureInfo.InvariantCulture))), 
                    new CustomField("customfield5", (Convert.ToInt32(PaymentAmount.FeeAmount * 100)).ToString(CultureInfo.InvariantCulture)), 
                    new CustomField("customfield6", (Convert.ToInt32(PaymentAmount.BaseAmount * 100)).ToString(CultureInfo.InvariantCulture)),
                    new CustomField("customfield10", Property.PropertyCode)
                };

            var Response = new PaymentResponse();

            if (PaymentAmount.BaseAmount > 0.0M)
            {
                Response = ConvertToSmsPaymentResponse(CreditCardPayment.ProcessCreditCard(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, renter.IsCreditCardTestRenter, PaymentAmount.TotalAmount,
                    payerCreditCard.CreditCardAccountNumber, payerCreditCard.CreditCardExpirationDate, null, payerCreditCard.CreditCardHolderName, CustomFields));
            }

            if (Response.ResponseMessage == ServiceResponseMessages.Ccaccepted)
            {
                ProcessVoid(Response.TransactionId);
            }

            if (Response.Result == SmsResponseResult.Success)
            {
                lease.CurrentBalanceDue -= PaymentAmount.BaseAmount;
                lease.CurrentBalanceDueLastUpdated = DateTime.UtcNow;
                //lease.PaymentDueDate = lease.PaymentDueDate.HasValue ? lease.PaymentDueDate.Value.AddMonths(1) : DateTime.UtcNow.AddMonths(1);
                Lease.Set(lease);
                SetPayment(renter, PaymentAmount, request.Description, payerCreditCard, request.Phone, request.Carrier, Response);

            }

            return Response;
        }

        private static PaymentResponse ProcessPayment(PaymentRequest request, PayerAch payerAch, Renter renter, Lease lease)
        {
            // Salcedo - 1/17/2014 - Payment Type should be ECheck for ACH payments,
            var PaymentAmount = new PaymentAmount(request.Amount, "Convenience Fee", PaymentType.ECheck, renter.RenterId);
            var AchClientId = GetAchClientId(renter.RenterId, PaymentApplication.Rent.ToString());
            var Address = GetRenterAddress(renter);
            var Response = new PaymentResponse();

            if (Address.AddressId == -1)
                return new PaymentResponse {Result = SmsResponseResult.AddressNotFound, ResponseMessage = ServiceResponseMessages.AddressNotFound};

            if (PaymentAmount.BaseAmount > 0.0M)
            {
                Response = ConvertToSmsPaymentResponse(AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword, AchClientId, PaymentAmount.TotalAmount, payerAch.BankRoutingNumber,
                    payerAch.BankAccountNumber, payerAch.BankAccountType, renter.FirstName, renter.LastName, Address.AddressLine1, Address.AddressLine2, Address.City, 
                    Address.GetStateAbbeviation(), Address.PostalCode, renter.MainPhoneNumber));
            }

            if (Response.Result == SmsResponseResult.Success)
            {
                lease.CurrentBalanceDue -= PaymentAmount.BaseAmount;
                lease.CurrentBalanceDueLastUpdated = DateTime.UtcNow;
                //lease.PaymentDueDate = lease.PaymentDueDate.HasValue ? lease.PaymentDueDate.Value.AddMonths(1) : DateTime.UtcNow.AddMonths(1);
                Lease.Set(lease);
                SetPayment(renter, PaymentAmount, request.Description, payerAch, request.Phone, request.Carrier, Response);
            }

            return Response;
        }

        private static Address GetRenterAddress(Renter renter)
        {
            var BillingAddress = Address.GetAddressByRenterIdAndAddressType(renter.RenterId, (int)TypeOfAddress.Billing);
            if (BillingAddress.AddressId > 0)
                return BillingAddress;

            var AddressId = 0;

            if (String.IsNullOrEmpty(renter.StreetAddress) || String.IsNullOrEmpty(renter.City) || !renter.StateProvinceId.HasValue || String.IsNullOrEmpty(renter.PostalCode))
                AddressId = -1;

            return new Address
            {
                AddressId = AddressId,
                AddressTypeId = (int)TypeOfAddress.Primary,
                AddressLine1 = renter.StreetAddress,
                AddressLine2 = renter.Unit,
                City = renter.City,
                StateProvinceId = renter.StateProvinceId.HasValue ? renter.StateProvinceId.Value : 1,
                PostalCode = renter.PostalCode
            };
        }

        private static void ProcessVoid(string transactionId)
        {
            CreditCardPayment.VoidCreditCardPayment(EfxSettings.CreditCardProcessorCustomerId, EfxSettings.CreditCardProcessorCustomerPassword, transactionId);
        }

        private static void NotifyRenter(PaymentResponse response, PaymentRequest request, Renter renter)
        {
            var Property = EfxFramework.Property.GetPropertyByRenterId(renter.RenterId);
            var PropertyConvenienceFee = EfxFramework.Property.GetConvenienceFeesByProperty(Property);
            var ConvenienceFee = renter.SmsPaymentTypeId == 1 ? PropertyConvenienceFee.CreditCardFee : PropertyConvenienceFee.ECheckFee;
            var Total = ConvenienceFee + request.Amount;


            // ReSharper disable ConvertIfStatementToConditionalTernaryExpression
            if (response.Result == SmsResponseResult.Success)
            // ReSharper restore ConvertIfStatementToConditionalTernaryExpression
            {
                //Salcedo - 3/12/2014 - replaced with new html email template
                //SendEmailX(Property.PropertyId.HasValue ? Property.PropertyId.Value : 0, "Payment Confirmation", string.Format(EmailTemplates.OneTimeRentPayment, renter.DisplayName,
                //    "Sms Payment", DateTime.UtcNow.ToShortDateString(), response.TransactionId, ConvenienceFee.ToString("C"), Total.ToString("C")), renter.PrimaryEmailAddress);
                String RecipientName = renter.DisplayName;
                //cakel: BUGID000180 - Added PropertyName
                String RenterProperty = Property.PropertyName;
                String PaymentAmount = "Sms Payment";
                String TransactionDate = DateTime.UtcNow.ToShortDateString();
                String InternalTransactionId = response.TransactionId;
                String ConvenienceAmount = ConvenienceFee.ToString("C");
                String TotalAmount = Total.ToString("C");
                //cakel: BUGID000180 - Added PropertyName
                String Body = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-payment-success.html").Replace("[[ROOT]]",
                    EmailTemplateService.EmailTemplateService.RootPath), RecipientName, RenterProperty,PaymentAmount, TransactionDate, InternalTransactionId, ConvenienceAmount, TotalAmount);
                SendEmailX(Property.PropertyId.HasValue ? Property.PropertyId.Value : 0, "Payment Confirmation", Body, renter.PrimaryEmailAddress);
            }
            else
            {
                //Salcedo - 3/12/2014 - replaced with new html email template
                //SendEmailX(Property.PropertyId.HasValue ? Property.PropertyId.Value : 0, "Payment Confirmation", string.Format(EmailTemplates.OneTimeRentPaymentFailed, renter.DisplayName,
                //    "Sms Payment", DateTime.UtcNow.ToShortDateString(), response.TransactionId, ConvenienceFee.ToString("C")), renter.PrimaryEmailAddress);
                String RecipientName = renter.DisplayName;

                //cakel: BUGID000180 - Added PropertyName
                String RenterProperty = Property.PropertyName;

                String PaymentAmount = "Sms Payment";
                String TransactionDate = DateTime.UtcNow.ToShortDateString();
                String InternalTransactionId = response.TransactionId;
                String ConvenienceAmount = ConvenienceFee.ToString("C");
                String TotalAmount = Total.ToString("C");
                //cakel: BUGID000180 - Added PropertyName
                String Body = string.Format(EmailTemplateService.EmailTemplateService.GetEmailHtml("~/EmailTemplates/DomusMe-payment-failure.html").Replace("[[ROOT]]",
                    EmailTemplateService.EmailTemplateService.RootPath), RecipientName, RenterProperty, PaymentAmount, TransactionDate, InternalTransactionId, ConvenienceAmount, TotalAmount);
                SendEmailX(Property.PropertyId.HasValue ? Property.PropertyId.Value : 0, "Payment Confirmation", Body, renter.PrimaryEmailAddress);
            }

            ReplyToSender(response.ResponseMessage, request.Phone, request.Id);
        }

        public static void ReplyToSender(string responseMessage, string phoneNumber, string id)
        {
            if (!phoneNumber.StartsWith("1"))
                phoneNumber = phoneNumber.Insert(0, "1");

            using (var Client = new EfxSms.MessageMediaService())
            {
                var Auth = new EfxSms.AuthenticationType {userId = EfxSettings.EfxSmsUserId, password = EfxSettings.EfxSmsPassword};
                var Recipient = new EfxSms.RecipientType{Value = phoneNumber, uid = UInt32.Parse(id ?? "0")};
                var Message = new EfxSms.MessageType { content = responseMessage };
                var MessageList = new EfxSms.MessageListType();
                var MessageBody = new EfxSms.SendMessagesBodyType();

                Message.recipients = new [] {Recipient};
                MessageList.message = new [] {Message};
                MessageBody.messages = MessageList;

                Client.sendMessages(Auth, MessageBody);
            }
        }

        private static void SetPayment(Renter renter, PaymentAmount amount, string description, PayerCreditCard payerCreditCard, string smsPhoneNumber, Carrier? carrier, PaymentResponse response)
        {
            if (!renter.PayerId.HasValue)
                renter.PayerId = 0;

            var PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, PaymentType.CreditCard, null, null, payerCreditCard.CreditCardHolderName, payerCreditCard.CreditCardAccountNumber,
                        payerCreditCard.CreditCardExpirationDate, amount, description, null, null, null, null, response.TransactionId, true, smsPhoneNumber,
                        ((int)response.Result).ToString(CultureInfo.InvariantCulture), response.ResponseMessage, carrier, PaymentChannel.Text, PaymentStatus.Approved));

            Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int) response.Result, response.ResponseMessage);
        }

        private static void SetPayment(Renter renter, PaymentAmount amount, string description, PayerAch payerAch, string smsPhoneNumber, Carrier? carrier, PaymentResponse response)
        {
            if (!renter.PayerId.HasValue)
                renter.PayerId = 0;

            var PaymentId = Payment.Set(Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, PaymentType.ECheck, payerAch.BankAccountNumber, payerAch.BankRoutingNumber, null, null,
                        null, amount, description, null, null, null, null, response.TransactionId, true, smsPhoneNumber,
                        ((int)response.Result).ToString(CultureInfo.InvariantCulture), response.ResponseMessage, carrier, PaymentChannel.Text, PaymentStatus.Processing));

            Transaction.SetTransactionFromPayment(new Payment(PaymentId), (int) response.Result, response.ResponseMessage);
        }

        private static string GetAchClientId(int renterId, string description)
        {
            var Property = EfxFramework.Property.GetPropertyByRenterId(renterId);

            switch (SetPaymentApplication(description))
            {
                case PaymentApplication.Rent:
                    return Property.RentalAccountAchClientId;
                case PaymentApplication.Charity:
                    return Property.RentalAccountAchClientId;
                case PaymentApplication.Other:
                    return Property.FeeAccountAchClientId;
                default:
                    return Property.RentalAccountAchClientId;
            }
        }

        private static PaymentApplication SetPaymentApplication(string description)
        {
            if (!string.IsNullOrEmpty(description))
            {
                if (description.ToLower().Contains("rent"))
                    return PaymentApplication.Rent;
                if (description.ToLower().Contains("charity"))
                    return PaymentApplication.Charity;
            }

            return PaymentApplication.Other;
        }

        private static PaymentResponse ConvertToSmsPaymentResponse(CreditCardPaymentResponse response)
        {
            return new PaymentResponse {Result = ConvertResponseResult(response.Result), ResponseMessage = response.ResponseMessage, TransactionId = response.TransactionId};
        }

        private static PaymentResponse ConvertToSmsPaymentResponse(AchPaymentResponse response)
        {
            return new PaymentResponse {Result = ConvertResponseResult(response.Result), ResponseMessage = response.ResponseDescription, TransactionId = response.TransactionId};
        }

        private static SmsResponseResult ConvertResponseResult(GeneralResponseResult result)
        {
            if (result == GeneralResponseResult.Success)
                return SmsResponseResult.Success;

            try
            {
                return (SmsResponseResult)((int)result);
            }
            catch
            {
                return SmsResponseResult.GeneralFailure;
            }
        }

        private static void SendEmailX(int propertyId, string subject, string body, string renterEmail)
        {
            //Salcedo - 2/28/2014 - changed to use function that includes property management contacts
            //BulkMail.BulkMail.SendMailMessage(
            //    propertyId,
            //    "Renter",
            //    false,
            //    Settings.SendMailUsername,
            //    Settings.SendMailPassword,
            //    new MailAddress(Settings.PaymentEmail),
            //    subject,
            //    body,
            //    new List<MailAddress> { new MailAddress(renterEmail) }
            //    );
            BulkMail.BulkMail.SendMailMessageIncludePropertyEmailContacts(
                propertyId,
                "Renter",
                EfxSettings.SendMailUsername,
                EfxSettings.SendMailPassword,
                new MailAddress(EfxSettings.PaymentEmail),
                subject,
                body,
                new List<MailAddress> { new MailAddress(renterEmail) }
                );
        }
    }
}
