﻿using EfxFramework.PaymentMethods;

namespace EfxFramework.Sms
{
    public class PaymentResponse
    {
        public SmsResponseResult Result { get; set; }
        public string ResponseMessage { get; set; }
        public string TransactionId { get; set; }
    }
}
