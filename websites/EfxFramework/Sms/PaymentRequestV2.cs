﻿using System;
using EfxFramework.PaymentMethods;
using Mb2x.ExtensionMethods;

namespace EfxFramework.Sms
{
    public class PaymentRequestV2 : PaymentProcessingBase
    {
        public string Phone { get; set; }
        public string Message { get; set; }
        public string Id { get; set; }
        public Carrier? Carrier { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }

        public PaymentRequestV2(PaymentChannel channel, string phone, string message, string id, string carrier)
            : base(channel)
        {
            if (phone.StartsWith("1"))
                phone = phone.Remove(0, 1);

            Phone = phone;
            Message = message;
            Id = id;

            try
            {
                Carrier = (Carrier)Enum.Parse(typeof(Carrier), carrier);
            }
            catch
            {
                Carrier = null;
            }
        }

        public PaymentRequestV2(PaymentChannel channel, int propertyId) 
            : base(channel, propertyId){}

        public PaymentRequestV2(PaymentChannel channel, int propertyId, int residentId) 
            : base(channel, propertyId, residentId){}

        public void ProcessPayment()
        {
            var Renter = EfxFramework.Renter.GetRenterBySmsPaymentPhoneNumber(Phone);
            if (Renter.RenterId < 1)
            {
                ReplyToSender(ServiceResponseMessages.PhoneNumberNotFound, Phone, Id);
                return;
            }

            ProcessPayment(PaymentChannel.Text, Renter);
        }

        public PaymentResponseBase ProcessPayment(PaymentChannel channel, Renter info)
        {
            var P = Property.GetPropertyByRenterId(info.RenterId);
            if (P.PropertyId.HasValue)
                PropertyId = P.PropertyId.Value;

            ResidentId = info.RenterId;
            ReplyToSender(Message, Phone, Id);

            if (info.SmsPaymentTypeId == 1)
            {
                return ProcessResidentPayment(new CreditCardDetails(GetResidentAddress(), Resident.FirstName, Resident.LastName, PayerCc.CreditCardHolderName,
                    PayerCc.CreditCardAccountNumber, PayerCc.CreditCardExpirationDate.ToDateTime(), null));
            }

            return ProcessResidentPayment(new EcheckDetails(GetResidentAddress(), Resident.FirstName, Resident.LastName, PayerAch.BankAccountNumber,
                PayerAch.BankRoutingNumber, AccountType.Checking));

        }

        public static void ReplyToSender(string responseMessage, string phoneNumber, string id)
        {
            if (!phoneNumber.StartsWith("1"))
                phoneNumber = phoneNumber.Insert(0, "1");

            using (var Client = new EfxSms.MessageMediaService())
            {
                var Auth = new EfxSms.AuthenticationType { userId = EfxSettings.EfxSmsUserId, password = EfxSettings.EfxSmsPassword };
                var Recipient = new EfxSms.RecipientType { Value = phoneNumber, uid = UInt32.Parse(id ?? "0") };
                var Message = new EfxSms.MessageType { content = responseMessage };
                var MessageList = new EfxSms.MessageListType();
                var MessageBody = new EfxSms.SendMessagesBodyType();

                Message.recipients = new[] { Recipient };
                MessageList.message = new[] { Message };
                MessageBody.messages = MessageList;

                Client.sendMessages(Auth, MessageBody);
            }
        }

        protected override bool Validate(out PaymentResponseBase response)
        {
            if (!base.Validate(out response))
                return false;

            if (!Resident.PayerId.HasValue || Resident.PayerId.Value < 1)
            {
                response.Result = (int) SmsResponseResult.PayerNotFound;
                response.Message = ServiceResponseMessages.Ccpayernotfound;
                return false;
            }

            if (Lease.LeaseId < 1)
            {
                response.Result = (int) SmsResponseResult.LeaseNotFound;
                response.Message = ServiceResponseMessages.LeaseNotFound;
                return false;
            }

            if (!Property.PropertyId.HasValue || Property.PropertyId.Value < 1)
            {
                response.Result = (int) SmsResponseResult.PropertyNotFound;
                response.Message = ServiceResponseMessages.PropertyNotFound;
                return false;
            }

            if (Resident.AcceptedPaymentTypeId == 4)
            {
                response.Result = (int) SmsResponseResult.CannotAcceptRenterPayment;
                response.Message = ServiceResponseMessages.CannotAcceptRenterPayment;
                return false;
            }

            return true;
        }
    }
}
