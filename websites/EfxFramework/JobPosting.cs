using System.Collections.Generic;
using Mb2x.Data;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using EfxFramework.ExtensionMethods;

namespace EfxFramework
{
	public class JobPosting : BaseEntity
	{
		//Public Properties
		[DisplayName(@"JobPostingId"), Required(ErrorMessage = "JobPostingId is required.")]
		public int JobPostingId { get; set; }

		[DisplayName(@"Headline"), Required(ErrorMessage = "Headline is required."), StringLength(100)]
		public string Headline { get; set; }

		[DisplayName(@"JobDescription"), Required(ErrorMessage = "JobDescription is required.")]
		public string JobDescription { get; set; }

		[DisplayName(@"DisplaySummary"), Required(ErrorMessage = "DisplaySummary is required."), StringLength(100)]
		public string DisplaySummary { get; set; }

		[DisplayName(@"JobTitle"), Required(ErrorMessage = "JobTitle is required."), StringLength(50)]
		public string JobTitle { get; set; }

        [DisplayName(@"PrincipleDuties"), Required(ErrorMessage = "PrincipleDuties is required.")]
        public string PrincipleDuties { get; set; }

        [DisplayName(@"SkillsAndAbilities"), Required(ErrorMessage = "SkillsAndAbilities is required.")]
        public string SkillsAndAbilities { get; set; }

		[DisplayName(@"DatePosted"), Required(ErrorMessage = "DatePosted is required.")]
		public DateTime DatePosted { get; set; }

		[DisplayName(@"IsActive"), Required(ErrorMessage = "IsActive is required.")]
		public bool IsActive { get; set; }

        [DisplayName(@"PostedBy"), Required(ErrorMessage = "PostedBy is required.")]
        public string PostedBy { get; set; }

        public string JobPostingStatus { get { return IsActive ? "Published" : "Saved"; } }
        public string TruncateBlog { get { return JobDescription.TruncateSentence(200); } }
        public string ViewJobPostingUrl { get { return String.Format("{0}?JobId={1}", EfxSettings.ViewJobPostingUrl, JobPostingId); } }
        public string AdminJobPostingUrl { get { return String.Format("{0}#JobPostsTab", ViewJobPostingUrl); } }
        public string DeleteJobPostingUrl { get { return String.Format("~/Handlers/GeneralHandler.ashx?RequestType=13&JobPostingId={0}#JobPostsTab", JobPostingId); } }

        public static List<JobPosting> GetAllJobPostings()
        {
            return DataAccess.GetTypedList<JobPosting>(EfxSettings.ConnectionString, "dbo.usp_JobPosting_GetAllJobPostings");
        }

        public static List<JobPosting> GetMostRecentActiveJobPostings()
        {
            return GetAllJobPostings().Where(j => j.IsActive).OrderByDescending(j => j.DatePosted).Take(3).ToList();
        }

        public static List<JobPosting> GetAllActiveJobPostings()
        {
            return GetAllJobPostings().Where(j => j.IsActive).ToList();
        }

        public static void DeleteJobPosting(int jobPostingId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_JobPosting_DeleteSingleObject", new SqlParameter("@JobPostingId", jobPostingId));
        }

        public static int Set(int jobPostingId, string headline, string jobDescription, string displaySummary, string jobTitle, DateTime datePosted, bool isActive, string principleDuties, string skillsAndAbilities, string postedBy)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_JobPosting_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@JobPostingId", jobPostingId),
	                    new SqlParameter("@Headline", headline),
	                    new SqlParameter("@JobDescription", jobDescription),
	                    new SqlParameter("@DisplaySummary", displaySummary),
	                    new SqlParameter("@JobTitle", jobTitle),
	                    new SqlParameter("@DatePosted", datePosted),
	                    new SqlParameter("@IsActive", isActive),
                        new SqlParameter("@PrincipleDuties", principleDuties),
                        new SqlParameter("@SkillsAndAbilities", skillsAndAbilities),
                        new SqlParameter("@PostedBy", postedBy)
                    });
        }

        public static int Set(JobPosting j)
        {
	        return Set(
		        j.JobPostingId,
		        j.Headline,
		        j.JobDescription,
		        j.DisplaySummary,
		        j.JobTitle,
		        j.DatePosted,
		        j.IsActive,
                j.PrincipleDuties,
                j.SkillsAndAbilities,
                j.PostedBy
	        );
        }

        public JobPosting(int jobPostingId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_JobPosting_GetSingleObject", new [] { new SqlParameter("@JobPostingId", jobPostingId) });

            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        JobPostingId = (int)ResultSet[0];
		        Headline = ResultSet[1] as string;
		        JobDescription = ResultSet[2] as string;
		        DisplaySummary = ResultSet[3] as string;
		        JobTitle = ResultSet[4] as string;
		        DatePosted = Helpers.DateTimeHelper.setDateTimeZone((DateTime)ResultSet[5], "Eastern Standard Time");
		        IsActive = (bool)ResultSet[6];
                PrincipleDuties = ResultSet[7] as string;
                SkillsAndAbilities = ResultSet[8] as string;
                PostedBy = ResultSet[9] as string;
            }
        }

		public JobPosting()
		{
			InitializeObject();
		}

	}
}
