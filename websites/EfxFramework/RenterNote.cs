using System.Collections.Generic;
using Mb2x.Data;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class RenterNote : BaseEntity
	{
		//Public Properties
		[DisplayName(@"RenterNoteId"), Required(ErrorMessage = "RenterNoteId is required.")]
		public int RenterNoteId { get; set; }

		[DisplayName(@"RenterId"), Required(ErrorMessage = "RenterId is required.")]
		public int RenterId { get; set; }

		[DisplayName(@"Note"), Required(ErrorMessage = "Note is required.")]
		public string Note { get; set; }

		[DisplayName(@"LastEditedDate"), Required(ErrorMessage = "LastEditedDate is required.")]
		public DateTime LastEditedDate { get; set; }

		[DisplayName(@"LastEditedBy"), Required(ErrorMessage = "LastEditedBy is required."), StringLength(100)]
		public string LastEditedBy { get; set; }

        [DisplayName("@RoleTypeId"), Required(ErrorMessage = "RoleTypeId is required.")]
        public int RoleTypeId { get; set; }

        public string PostedDate { get { return LastEditedDate.ToShortDateString(); } }
        public string PostedTime { get { return LastEditedDate.ToShortTimeString(); } }
        public string TimeZone { get; set; }
        public string NoteTextUrl { get { return String.Format("~/Renters/RenterDetails.aspx?renterId={0}&RenterNoteId={1}#NotesTab", RenterId, RenterNoteId); } }
        public string DeleteNoteUrl { get { return String.Format("~/Handlers/GeneralHandler.ashx?RequestType=15&RenterNoteId={0}#NotesTab", RenterNoteId); } }

        public static List<RenterNote> GetRenterNoteByRenterId(int renterId)
        {
            return DataAccess.GetTypedList<RenterNote>(EfxSettings.ConnectionString, "dbo.usp_RenterNote_GetRenterNoteByRenterId", new SqlParameter("@RenterId", renterId));
        }

        public static List<RenterNote> GetRenterNoteByRenterIdAndRoleTypeId(int renterId, int roleTypeId)
        {
            return DataAccess.GetTypedList<RenterNote>(EfxSettings.ConnectionString, "dbo.usp_RenterNote_GetRenterNoteByRenterIdAndRoleTypeId", new[]
                {
                    new SqlParameter("@RenterId", renterId),
                    new SqlParameter("@RoleTypeId", roleTypeId)
                });
        }

        public static void DeleteNote(int renterNoteId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_RenterNote_DeleteSingleObject", new SqlParameter("@RenterNoteId", renterNoteId));
        }

        public static int Set(int renterNoteId, int renterId, string note, DateTime lastEditedDate, string lastEditedBy, int roleTypeId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_RenterNote_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@RenterNoteId", renterNoteId),
	                    new SqlParameter("@RenterId", renterId),
	                    new SqlParameter("@Note", note),
	                    new SqlParameter("@LastEditedDate", lastEditedDate),
	                    new SqlParameter("@LastEditedBy", lastEditedBy),
                        new SqlParameter("@RoleTypeId", roleTypeId)
                    });
        }

        public static int Set(RenterNote r)
        {
	        return Set(
		        r.RenterNoteId,
		        r.RenterId,
		        r.Note,
		        r.LastEditedDate,
		        r.LastEditedBy,
                r.RoleTypeId
	        );
        }

        public RenterNote(int? renterNoteId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_RenterNote_GetSingleObject", new [] { new SqlParameter("@RenterNoteId", renterNoteId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        RenterNoteId = (int)ResultSet[0];
		        RenterId = (int)ResultSet[1];
		        Note = ResultSet[2] as string;
		        LastEditedDate = !String.IsNullOrEmpty(ResultSet[6].ToString()) ? setDateTimeZone((DateTime)ResultSet[3], ResultSet[6].ToString()) : setDateTimeZone((DateTime)ResultSet[3], "Eastern Standard Time");
		        LastEditedBy = ResultSet[4] as string;
                RoleTypeId = (int) ResultSet[5];
                TimeZone = ResultSet[6] as string;
            }
        }

        private DateTime setDateTimeZone(DateTime timeUtc, String zone)
        {
            TimeZoneInfo tzone = TimeZoneInfo.FindSystemTimeZoneById(zone);
            return TimeZoneInfo.ConvertTimeFromUtc(timeUtc, tzone);
        }

		public RenterNote()
		{
			InitializeObject();
		}

	}
}
