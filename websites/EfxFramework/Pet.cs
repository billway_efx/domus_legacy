using System;
using System.Collections.Generic;
using EfxFramework.ExtensionMethods;
using EfxFramework.Web.Caching;
using Mb2x.Data;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;

namespace EfxFramework
{
	public class Pet : BaseEntity
	{
		//Public Properties
		[DisplayName(@"PetId"), Required(ErrorMessage = "PetId is required.")]
		public int PetId { get; set; }

		[DisplayName(@"LeaseId"), Required(ErrorMessage = "LeaseId is required.")]
		public int LeaseId { get; set; }

		[DisplayName(@"PetBreed"), StringLength(100)]
		public string PetBreed { get; set; }

		[DisplayName(@"PetName"), StringLength(100)]
		public string PetName { get; set; }

        public int PetTypeId { get; set; }
        public string PetTypeName { get { return ((PetType) PetTypeId).GetFriendlyName(); } }
        public string DeletePetUrl { get { return String.Format("~/Handlers/GeneralHandler.ashx?RequestType=10&PetId={0}#LeaseInformationTab", PetId); } }

        public static List<Pet> GetAllPets()
        {
            return DataAccess.GetTypedList<Pet>(EfxSettings.ConnectionString, "dbo.usp_Pet_GetAllPets");
        }

        public static List<Pet> GetPetsByLeaseId(int leaseId)
        {
            return GetAllPets().Where(p => p.LeaseId == leaseId).ToList();
        }

        public static void DeletePet(int petId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_Pet_DeleteSingleObject", new SqlParameter("@PetId", petId));
        }

        public static int Set(int petId, int leaseId, string petBreed, string petName, int petTypeId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_Pet_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@PetId", petId),
	                    new SqlParameter("@LeaseId", leaseId),
	                    new SqlParameter("@PetBreed", petBreed),
	                    new SqlParameter("@PetName", petName),
                        new SqlParameter("@PetTypeId", petTypeId)
                    });
        }

        public static int Set(Pet p)
        {
	        return Set(
		        p.PetId,
		        p.LeaseId,
		        p.PetBreed,
		        p.PetName,
                p.PetTypeId
	        );
        }

        public Pet(int petId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_Pet_GetSingleObject", new [] { new SqlParameter("@PetId", petId) });
            
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        PetId = (int)ResultSet[0];
		        LeaseId = (int)ResultSet[1];
		        PetBreed = ResultSet[2] as string;
		        PetName = ResultSet[3] as string;
                PetTypeId = (int) ResultSet[4];
            }
        }

		public Pet()
		{
			InitializeObject();
		}

	}
}
