using EfxFramework.ExtensionMethods;
using Mb2x.Data;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EfxFramework
{
	public class Address : BaseEntity
	{
		//Public Properties
		[DisplayName(@"AddressId"), Required(ErrorMessage = "AddressId is required.")]
		public int AddressId { get; set; }

		[DisplayName(@"AddressTypeId"), Required(ErrorMessage = "AddressTypeId is required.")]
		public int AddressTypeId { get; set; }

		[DisplayName(@"CareOf"), StringLength(50)]
		public string CareOf { get; set; }

		[DisplayName(@"AddressLine1"), Required(ErrorMessage = "AddressLine1 is required."), StringLength(100)]
		public string AddressLine1 { get; set; }

		[DisplayName(@"AddressLine2"), StringLength(100)]
		public string AddressLine2 { get; set; }

		[DisplayName(@"AddressLine3"), StringLength(100)]
		public string AddressLine3 { get; set; }

		[DisplayName(@"City"), Required(ErrorMessage = "City is required."), StringLength(100)]
		public string City { get; set; }

		[DisplayName(@"StateProvinceId"), Required(ErrorMessage = "StateProvinceId is required.")]
		public int StateProvinceId { get; set; }

		[DisplayName(@"PostalCode"), Required(ErrorMessage = "PostalCode is required."), StringLength(10)]
		public string PostalCode { get; set; }

		[DisplayName(@"PrimaryPhoneNumber"), StringLength(10)]
		public string PrimaryPhoneNumber { get; set; }

		[DisplayName(@"PrimaryPhoneNumberExtension"), StringLength(5)]
		public string PrimaryPhoneNumberExtension { get; set; }

		[DisplayName(@"AlternatePhoneNumber"), StringLength(10)]
		public string AlternatePhoneNumber { get; set; }

		[DisplayName(@"AlternatePhoneNumberExtension"), StringLength(5)]
		public string AlternatePhoneNumberExtension { get; set; }

		[DisplayName(@"MobilePhoneNumber"), StringLength(10)]
		public string MobilePhoneNumber { get; set; }

		[DisplayName(@"FaxNumber"), StringLength(10)]
		public string FaxNumber { get; set; }

		[DisplayName(@"PrimaryEmailAddress"), StringLength(100)]
		public string PrimaryEmailAddress { get; set; }

		[DisplayName(@"AlternateEmailAddress1"), StringLength(100)]
		public string AlternateEmailAddress1 { get; set; }

		[DisplayName(@"AlternateEmailAddress2"), StringLength(100)]
		public string AlternateEmailAddress2 { get; set; }

        public static Address GetAddressByRenterIdAndAddressType(int renterId, int addressTypeId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return new Address(NewDataAccess.ExecuteScalar<int?>(EfxSettings.ConnectionString, "dbo.usp_RenterAddress_GetRenterAddressByAddressType", new[] {new SqlParameter("@RenterId", renterId), new SqlParameter("@AddressTypeId", addressTypeId)}));
        }

        public static void AssignAddressToRenter(int renterId, int addressId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_RenterAddress_SetSingleObject", new[] {new SqlParameter("@RenterId", renterId), new SqlParameter("AddressId", addressId)});

            
        }

        public string GetStateAbbeviation()
        {
            return ((StateProvince) StateProvinceId).ToString();
        }

        public string GetStateName()
        {
            return ((StateProvince) StateProvinceId).GetFriendlyName();
        }

        public static int Set(int addressId, int addressTypeId, string careOf, string addressLine1, string addressLine2, string addressLine3, string city, int stateProvinceId, string postalCode, string primaryPhoneNumber, string primaryPhoneNumberExtension, string alternatePhoneNumber, string alternatePhoneNumberExtension, string mobilePhoneNumber, string faxNumber, string primaryEmailAddress, string alternateEmailAddress1, string alternateEmailAddress2)
        {
            if (addressTypeId < 1 || addressTypeId > 2)
                addressTypeId = 1;

            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
	            EfxSettings.ConnectionString,
	            "dbo.usp_Address_SetSingleObject",
                    new[]
                    {
	                    new SqlParameter("@AddressId", addressId),
	                    new SqlParameter("@AddressTypeId", addressTypeId),
	                    new SqlParameter("@CareOf", careOf),
	                    new SqlParameter("@AddressLine1", addressLine1),
	                    new SqlParameter("@AddressLine2", addressLine2),
	                    new SqlParameter("@AddressLine3", addressLine3),
	                    new SqlParameter("@City", city),
	                    new SqlParameter("@StateProvinceId", stateProvinceId),
	                    new SqlParameter("@PostalCode", postalCode),
	                    new SqlParameter("@PrimaryPhoneNumber", primaryPhoneNumber),
	                    new SqlParameter("@PrimaryPhoneNumberExtension", primaryPhoneNumberExtension),
	                    new SqlParameter("@AlternatePhoneNumber", alternatePhoneNumber),
	                    new SqlParameter("@AlternatePhoneNumberExtension", alternatePhoneNumberExtension),
	                    new SqlParameter("@MobilePhoneNumber", mobilePhoneNumber),
	                    new SqlParameter("@FaxNumber", faxNumber),
	                    new SqlParameter("@PrimaryEmailAddress", primaryEmailAddress),
	                    new SqlParameter("@AlternateEmailAddress1", alternateEmailAddress1),
	                    new SqlParameter("@AlternateEmailAddress2", alternateEmailAddress2)
                    });
        }

        public static int Set(Address a)
        {
	        return Set(
		        a.AddressId,
		        a.AddressTypeId,
		        a.CareOf,
		        a.AddressLine1,
		        a.AddressLine2,
		        a.AddressLine3,
		        a.City,
		        a.StateProvinceId,
		        a.PostalCode,
		        a.PrimaryPhoneNumber,
		        a.PrimaryPhoneNumberExtension,
		        a.AlternatePhoneNumber,
		        a.AlternatePhoneNumberExtension,
		        a.MobilePhoneNumber,
		        a.FaxNumber,
		        a.PrimaryEmailAddress,
		        a.AlternateEmailAddress1,
		        a.AlternateEmailAddress2
	        );
        }

        public Address(int? addressId)
        {
	        //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
	        var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_Address_GetSingleObject", new [] { new SqlParameter("@AddressId", addressId) });

            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                //Otherwise, populate this object
		        AddressId = (int)ResultSet[0];
		        AddressTypeId = (int)ResultSet[1];
		        CareOf = ResultSet[2] as string;
		        AddressLine1 = ResultSet[3] as string;
		        AddressLine2 = ResultSet[4] as string;
		        AddressLine3 = ResultSet[5] as string;
		        City = ResultSet[6] as string;
		        StateProvinceId = (int)ResultSet[7];
		        PostalCode = ResultSet[8] as string;
		        PrimaryPhoneNumber = ResultSet[9] as string;
		        PrimaryPhoneNumberExtension = ResultSet[10] as string;
		        AlternatePhoneNumber = ResultSet[11] as string;
		        AlternatePhoneNumberExtension = ResultSet[12] as string;
		        MobilePhoneNumber = ResultSet[13] as string;
		        FaxNumber = ResultSet[14] as string;
		        PrimaryEmailAddress = ResultSet[15] as string;
		        AlternateEmailAddress1 = ResultSet[16] as string;
		        AlternateEmailAddress2 = ResultSet[17] as string;
            }
        }

		public Address()
		{
			InitializeObject();
		}

	}
}
