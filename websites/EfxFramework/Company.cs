using EfxFramework.ExtensionMethods;
using Mb2x.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Mb2x.ExtensionMethods;
using RPO;
//using System.Linq;

namespace EfxFramework
{
    public class Company : BaseEntity
    {
        //Private Members
        private TelephoneNumber _Phone;
        private TelephoneNumber _Mobile;
        private TelephoneNumber _Fax;
        private TelephoneNumber _SalesPhone;

        //Public Properties
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public StateProvince? StateProvinceId { get; set; }
        public string PostalCode { get; set; }
        public string TextualPhone { get; set; }
		public string PNMSiteId { get; set; }
		public string PNMSecretKey { get; set; }
        public string DisplayPhone
        {
            get
            {
                return !String.IsNullOrEmpty(TextualPhone) ? TextualPhone : Phone.FormatPhoneNumber();
            }
        }
        public string Phone
        {
            get
            {
                return _Phone == null ? string.Empty : _Phone.Digits;
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _Phone = null;
                else
                {
                    if (_Phone == null)
                        _Phone = new TelephoneNumber(value);
                    else
                        _Phone.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }
        public string Mobile
        {
            get
            {
                return _Mobile == null ? string.Empty : _Mobile.Digits;
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _Mobile = null;
                else
                {
                    if (_Mobile == null)
                        _Mobile = new TelephoneNumber(value);
                    else
                        _Mobile.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }
        public string Fax
        {
            get
            {
                return _Fax == null ? string.Empty : _Fax.Digits;
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _Fax = null;
                else
                {
                    if (_Fax == null)
                        _Fax = new TelephoneNumber(value);
                    else
                        _Fax.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }
        public string WebSite { get; set; }
        public string EmailAddress { get; set; }
        public bool IsDeleted { get; set; }
        public string SalesFirstName { get; set; }
        public string SalesLastName { get; set; }
        public string SalesPhone
        {
            get
            {
                return _SalesPhone== null ? string.Empty : _SalesPhone.Digits;
            }

            set
            {
                if (value.TrimNull().LengthNull() == 0)
                    _SalesPhone = null;
                else
                {
                    if (_SalesPhone == null)
                        _SalesPhone = new TelephoneNumber(value);
                    else
                        _SalesPhone.Digits = TelephoneNumber.RemovePhoneNumberFormatting(value);
                }
            }
        }
        public string Status { get; set; }
        
        /// <summary>
        /// Gets the full street address, including city and state.
        /// </summary>
        public string DisplayAddress
        {
            get
            {
                var Prov = StateProvince.AK;
                if (StateProvinceId.HasValue)
                    Prov = (StateProvince)Enum.Parse(typeof(StateProvince), StateProvinceId.Value.ToString());

                return string.Format("{0} {1} {2} {3}", Address, Address2, City, Prov.ToString());
            }
        }

        /// <summary>
        /// Gets the contact first and lastname.
        /// </summary>
        public string ContactFullName
        {
            get
            {
                return string.Concat(ContactFirstName, " ", ContactLastName);
            }
        }

        public int NumberOfUnits
        {
            get
            {
                var Count = 0;
                var Units = Property.GetPropertyListByCompanyId(CompanyId);
                if (Units != null)
                    Count = Units.Count;

                return Count;
            }
        }

        public int NumberOfParticipatingUnits
        {
            get
            {
                var Units = Property.GetPropertyListByCompanyId(CompanyId).Where(p => p.IsParticipating.HasValue && p.IsParticipating.Value.Equals(true)).ToList();
                return Units.Count;
            }
        }
        //CMallory - Task 00499 - Added Apply Connect property
        public bool UsesApplyConnect { get; set; }

        //Public Methods
        public static List<Company> GetAllCompanyList()
        {            
            return DataAccess.GetTypedList<Company>(
                EfxSettings.ConnectionString,
                "dbo.usp_Company_GetAllCompanyList"
                );
        }
        //Salcedo - 7/14/2014 - new function for MRI integration
        public static List<Company> GetAllMRICompanyList()
        {
            return DataAccess.GetTypedList<Company>(
                EfxSettings.ConnectionString,
                "dbo.usp_Company_GetAllMRICompanyList"
                );
        }
        //Salcedo - 7/14/2014 - new function for MRI integration
        public static int GetCompanyIdForStaffId(int StaffId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(EfxSettings.ConnectionString,
                "dbo.usp_GetCompanyIdForStaffId",
                new SqlParameter("@StaffId", StaffId)
                );
        }
        public static List<Company> GetCompanyListByPropertyId(int propertyId)
        {            
            return DataAccess.GetTypedList<Company>(
                EfxSettings.ConnectionString,
                "dbo.usp_CompanyProperty_GetCompanyListByPropertyId",
                new SqlParameter("@PropertyId", propertyId)
                );
        }
        public static Company GetCompanyByRenterId(int renterId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            var CompanyId = (int?)(NewDataAccess.ExecuteScalar(
                EfxSettings.ConnectionString,
                "dbo.usp_PropertyRenter_GetCompanyByRenterId",
                new SqlParameter("@RenterId", renterId)
                ));

            return CompanyId.HasValue ? new Company(CompanyId.Value) : null;
        }
        public static byte[] GetLogo(int companyId)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            //Salcedo - 4/21/2015 - handle null return
            byte[] returnArray = NewDataAccess.ExecuteScalar<byte[]>(
                EfxSettings.ConnectionString,
                "dbo.usp_Company_GetLogo",
                new SqlParameter("@CompanyId", companyId));
            if (returnArray.Length <= 4)
                return null;
            else
                return returnArray;
        }
        public static void SetLogo(int companyId, byte[] logo)
        {
            if (logo.Length < 1)
                return;

            var StandarizedImage = logo.StandardizeImage(300);

            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(
                EfxSettings.ConnectionString,
                "dbo.usp_Company_SetLogo",
                new[]
					{
						new SqlParameter("@CompanyId", companyId),
						new SqlParameter("@Logo", StandarizedImage)
					});
        }
        public static void AssignPropertyToCompany(int propertyId, int companyId)
        {
            //Set the assignment
            //DataAccess.ExecuteNoResults(Settings.ConnectionString, "dbo.usp_CompanyProperty_AssignPropertyToCompany", new[] { new SqlParameter("@PropertyId", propertyId), new SqlParameter("@CompanyId", companyId) });
            using (var entity = new RPOEntities()) 
            {
                //cakel: 04/29/2016 - trying to catch errors
                try
                {
                var property = entity.Properties.First(p => p.PropertyId == propertyId);
                property.CompanyId = companyId;
                entity.SaveChanges();
            }
                catch(Exception ex)
                {
                    throw ex;
                }
                
            }
        }
        public static void RemovePropertyFromCompany(int propertyId, int companyId)
        {
            //Remove the assignment
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_CompanyProperty_RemovePropertyFromCompany", new[] { new SqlParameter("@PropertyId", propertyId), new SqlParameter("@CompanyId", companyId) });
        }
        //CMallory - Task 00593 - Added
        public static void DeleteCompanyProperties(int companyId)
        {
            NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "dbo.usp_Company_DeleteProperties", new[] { new SqlParameter("@CompanyId", companyId)});
        }
        public static int Set(int companyId, string companyName, string contactFirstName, string contactLastName, string address, string address2, string city, StateProvince? stateProvinceId, string postalCode, string phone, string mobile, string fax, string webSite, string emailAddress, bool isDeleted, string salesFirstName, string salesLastName, string salesPhone, string status, string pnmSiteId, string pnmSecretKey, bool UsesApplyConnect)
        {
            //Salcedo - 12/21/2014 - remove reference to MB2x DataAccess
            return NewDataAccess.ExecuteScalar<int>(
                EfxSettings.ConnectionString,
                "dbo.usp_Company_SetSingleObject",
            new[]
                {
	                new SqlParameter("@CompanyId", companyId),
	                new SqlParameter("@CompanyName", companyName),
	                new SqlParameter("@ContactFirstName", contactFirstName),
	                new SqlParameter("@ContactLastName", contactLastName),
	                new SqlParameter("@Address", address),
	                new SqlParameter("@Address2", address2),
	                new SqlParameter("@City", city),
	                new SqlParameter("@StateProvinceId", stateProvinceId),
	                new SqlParameter("@PostalCode", postalCode),
	                new SqlParameter("@Phone", phone.EmptyStringToNull()),
	                new SqlParameter("@Mobile", mobile.EmptyStringToNull()),
	                new SqlParameter("@Fax", fax.EmptyStringToNull()),
	                new SqlParameter("@WebSite", webSite),
                    new SqlParameter("@EmailAddress", emailAddress),
	                new SqlParameter("@IsDeleted", isDeleted),
	                new SqlParameter("@SalesFirstName", salesFirstName),
	                new SqlParameter("@SalesLastName", salesLastName),
	                new SqlParameter("@SalesPhone", salesPhone.EmptyStringToNull()),
	                new SqlParameter("@Status", status),
					new SqlParameter("@PNMSiteId", pnmSiteId),
					new SqlParameter("@PNMSecretKey", pnmSecretKey),
                    new SqlParameter("@UsesApplyConnect", UsesApplyConnect)
                });
        }
        public static int Set(Company c)
        {
            return Set(
                c.CompanyId,
                c.CompanyName,
                c.ContactFirstName,
                c.ContactLastName,
                c.Address,
                c.Address2,
                c.City,
                c.StateProvinceId,
                c.PostalCode,
                c.Phone,
                c.Mobile,
                c.Fax,
                c.WebSite, 
                c.EmailAddress,
                c.IsDeleted,
                c.SalesFirstName,
                c.SalesLastName,
                c.SalesPhone,
                c.Status,
				c.PNMSiteId,
				c.PNMSecretKey,
                c.UsesApplyConnect
            );
        }
        public Company(int? companyId)
        {
            //Attempt to pull the data from the database
            //Salcedo - 12/20/2014 - remove reference to MB2x DataAccess object
            var ResultSet = NewDataAccess.ExecuteOneRow(EfxSettings.ConnectionString, "dbo.usp_Company_GetSingleObject", new[] { new SqlParameter("@CompanyId", companyId) });
            if (ResultSet == null)
            {
                //If we don't get a record, initialize the object
                InitializeObject();
            }
            else
            {
                
                //Otherwise, populate this object
                CompanyId = (int)ResultSet[0];
                CompanyName = ResultSet[1] as string;
                ContactFirstName = ResultSet[2] as string;
                ContactLastName = ResultSet[3] as string;
                Address = ResultSet[4] as string;
                Address2 = ResultSet[5] as string;
                City = ResultSet[6] as string;
                if (ResultSet[7] != null && ResultSet[7].ToString().Length > 0)
                {
                    StateProvinceId = (StateProvince)Enum.Parse(typeof(StateProvince), ResultSet[7].ToString());
                }
                PostalCode = ResultSet[8] as string;
                Phone = (ResultSet[9] as string);
                Mobile = ResultSet[10] as string;
                Fax = ResultSet[11] as string;
                WebSite = ResultSet[12] as string;
                EmailAddress = ResultSet[13] as string;                
                IsDeleted = (bool)ResultSet[14];
                SalesFirstName = ResultSet[15] as string;
                SalesLastName = ResultSet[16] as string;
                SalesPhone = ResultSet[17] as string;
                Status = ResultSet[18] as string;
				PNMSiteId = ResultSet[19] as string;
				PNMSecretKey = ResultSet[20] as string;
                UsesApplyConnect = (bool)ResultSet[21];
            }
        }
        public Company()
        {
            InitializeObject();
        }
    }
}
