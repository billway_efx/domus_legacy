﻿using System.Drawing;
using System.IO;

namespace EfxFramework
{
    /// <summary>
    /// This class is used to test a byte array to make certain that it is an image
    /// </summary>
    public class Photo
    {
        private byte[] _PhotoBytes;

        public string PhotoUrl { get; set; }
        public string Description { get; set; }

        public byte[] PhotoBytes
        {
            get { return _PhotoBytes; }
            set
            {
                try
                {
                    using (var InputStream = new MemoryStream(value))
                    {
                        Image.FromStream(InputStream);
                        _PhotoBytes = value;
                    }
                }
                catch
                {
                    _PhotoBytes = null;
                }
            }
        }
    }
}
