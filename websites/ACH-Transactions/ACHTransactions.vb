﻿'Salcedo - 3/31/2015 - brought external ACH services into the RPO code, to improve performance and enable easier testing/debugging
'With this change, the system will no longer call out to our web service to create ACH transactions. Instead, the database is now called directly from this code.
'When I ported the code, I removed the ASP authentication and authorization related security checks, since the code calling this code is trusted.
'There are doubtless many other improvements that can be made here. For instance, some of the data validation performed here is now redundant.

Imports System.Configuration
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Xml
Imports ACH_Objects
Imports RPO

Public Class AchTransaction

    Private _ClientID As String
    Private _Password As String
    Private _Amount As String
    Private _ABANumber As String
    Private _AccountNumber As String
    Private _AccountType As String
    Private _FirstName As String
    Private _LastName As String
    Private _Address1 As String
    Private _Address2 As String
    Private _City As String
    Private _State As String
    Private _Zip As String
    Private _Phone As String

    '<DataMember()>
    Public Property ClientID() As String
        Get
            Return _ClientID
        End Get
        Set(value As String)
            _ClientID = value
        End Set
    End Property
    '<DataMember()>
    Public Property Password() As String
        Get
            Return _Password
        End Get
        Set(value As String)
            _Password = value
        End Set
    End Property
    '<DataMember()>
    Public Property Amount() As String
        Get
            Return _Amount
        End Get
        Set(value As String)
            _Amount = value
        End Set
    End Property
    '<DataMember()>
    Public Property ABANumber() As String
        Get
            Return _ABANumber
        End Get
        Set(value As String)
            _ABANumber = value
        End Set
    End Property
    '<DataMember()>
    Public Property AccountNumber() As String
        Get
            Return _AccountNumber
        End Get
        Set(value As String)
            _AccountNumber = value
        End Set
    End Property
    '<DataMember()>
    Public Property AccountType() As String
        Get
            Return _AccountType
        End Get
        Set(value As String)
            _AccountType = value
        End Set
    End Property
    '<DataMember()>
    Public Property FirstName() As String
        Get
            Return _FirstName
        End Get
        Set(value As String)
            _FirstName = value
        End Set
    End Property
    '<DataMember()>
    Public Property LastName() As String
        Get
            Return _LastName
        End Get
        Set(value As String)
            _LastName = value
        End Set
    End Property
    '<DataMember()>
    Public Property Address1() As String
        Get
            Return _Address1
        End Get
        Set(value As String)
            _Address1 = value
        End Set
    End Property
    '<DataMember()>
    Public Property Address2() As String
        Get
            Return _Address2
        End Get
        Set(value As String)
            _Address2 = value
        End Set
    End Property
    '<DataMember()>
    Public Property City() As String
        Get
            Return _City
        End Get
        Set(value As String)
            _City = value
        End Set
    End Property
    '<DataMember()>
    Public Property State() As String
        Get
            Return _State
        End Get
        Set(value As String)
            _State = value
        End Set
    End Property
    '<DataMember()>
    Public Property Zip() As String
        Get
            Return _Zip
        End Get
        Set(value As String)
            _Zip = value
        End Set
    End Property
    '<DataMember()>
    Public Property Phone() As String
        Get
            Return _Phone
        End Get
        Set(value As String)
            _Phone = value
        End Set
    End Property

End Class

Public Class EFXTransactionResponse

    Private _TransactionID As String
    Private _ResponseCode As String
    Private _ResponseDescription As String

    '<DataMember()>
    Public Property TransactionID() As String
        Get
            Return _TransactionID
        End Get
        Set(value As String)
            _TransactionID = value
        End Set
    End Property
    '<DataMember()>
    Public Property ResponseCode() As String
        Get
            Return _ResponseCode
        End Get
        Set(value As String)
            _ResponseCode = value
        End Set
    End Property
    '<DataMember()>
    Public Property ResponseDescription() As String
        Get
            Return _ResponseDescription
        End Get
        Set(value As String)
            _ResponseDescription = value
        End Set
    End Property

End Class

Public Class EFXTransactionStatus

    Private _StatusCode As String
    Private _StatusDescription As String
    Private _StatusDate As String

    '<DataMember()>
    Public Property StatusCode() As String
        Get
            Return _StatusCode
        End Get
        Set(value As String)
            _StatusCode = value
        End Set
    End Property
    '<DataMember()>
    Public Property StatusDescription() As String
        Get
            Return _StatusDescription
        End Get
        Set(value As String)
            _StatusDescription = value
        End Set
    End Property
    '<DataMember()>
    Public Property StatusDate() As String
        Get
            Return _StatusDate
        End Get
        Set(value As String)
            _StatusDate = value
        End Set
    End Property


End Class

Public Class EFXTransactionStatusExtended

    Private _TransactionID As String
    Private _StatusCode As String
    Private _StatusDescription As String
    Private _TransactionType As String
    Private _TransactionAmount As String
    Private _LastStatusChangeTime As String
    Private _ReturnCode As String
    Private _ReasonDescription As String

    '<DataMember()>
    Public Property TransactionID() As String
        Get
            Return _TransactionID
        End Get
        Set(value As String)
            _TransactionID = value
        End Set
    End Property
    '<DataMember()>
    Public Property StatusCode() As String
        Get
            Return _StatusCode
        End Get
        Set(value As String)
            _StatusCode = value
        End Set
    End Property
    '<DataMember()>
    Public Property StatusDescription() As String
        Get
            Return _StatusDescription
        End Get
        Set(value As String)
            _StatusDescription = value
        End Set
    End Property
    '<DataMember()>
    Public Property TransactionType() As String
        Get
            Return _TransactionType
        End Get
        Set(value As String)
            _TransactionType = value
        End Set
    End Property
    '<DataMember()>
    Public Property TransactionAmount() As String
        Get
            Return _TransactionAmount
        End Get
        Set(value As String)
            _TransactionAmount = value
        End Set
    End Property
    '<DataMember()>
    Public Property LastStatusChangeTime() As String
        Get
            Return _LastStatusChangeTime
        End Get
        Set(value As String)
            _LastStatusChangeTime = value
        End Set
    End Property
    '<DataMember()>
    Public Property ReturnCode() As String
        Get
            Return _ReturnCode
        End Get
        Set(value As String)
            _ReturnCode = value
        End Set
    End Property
    '<DataMember()>
    Public Property ReasonDescription() As String
        Get
            Return _ReasonDescription
        End Get
        Set(value As String)
            _ReasonDescription = value
        End Set
    End Property


End Class

Public Class EFXFileUploadResponseDetail

    Private _StatusCode As String
    Private _StatusDescription As String
    Private _FileID As String
    Private _FileName As String
    Private _FileSizeBytes As String
    Private _UploadDateTime As String

    '<DataMember()>
    Public Property StatusCode() As String
        Get
            Return _StatusCode
        End Get
        Set(value As String)
            _StatusCode = value
        End Set
    End Property
    '<DataMember()>
    Public Property StatusDescription() As String
        Get
            Return _StatusDescription
        End Get
        Set(value As String)
            _StatusDescription = value
        End Set
    End Property
    '<DataMember()>
    Public Property FileID() As String
        Get
            Return _FileID
        End Get
        Set(value As String)
            _FileID = value
        End Set
    End Property
    Public Property FileName() As String
        Get
            Return _FileName
        End Get
        Set(value As String)
            _FileName = value
        End Set
    End Property
    Public Property FileSizeBytes() As String
        Get
            Return _FileSizeBytes
        End Get
        Set(value As String)
            _FileSizeBytes = value
        End Set
    End Property
    Public Property UploadDateTime() As String
        Get
            Return _UploadDateTime
        End Get
        Set(value As String)
            _UploadDateTime = value
        End Set
    End Property

End Class

Public Class ACHTransactions

    Private Sub WriteLog(ClientID As String, LogDescription As String)

        Dim AL As New ActivityLog
        Dim TheDescription As String = LogDescription

        Call AL.WriteLog(ClientID, TheDescription, 2)

    End Sub

    Private Function ValidateData(ByVal TheTransaction As ACHTransaction, ByRef ErrorDescription As String, Optional ValidateRPOUserName As String = "") As Boolean

        Dim DV As New DataValidation()
        Dim Required As Boolean = True
        Dim NotRequired As Boolean = False

        Dim TheMerchantList As New MerchantList
        Dim TheMerchant As New Merchant

        'See if the ClientID is a GUID
        Dim ClientIDGuid As Guid = Guid.Empty
        Try
            ClientIDGuid = New Guid(TheTransaction.ClientID)
        Catch ex As Exception
        End Try

        If ClientIDGuid = Guid.Empty Then
            'The client ID is not a guid, so we've passed a UserMerchantID value
            TheMerchant = TheMerchantList.GetMerchant(TheTransaction.ClientID)
        Else
            'The client ID is a guid, so use that value instead
            TheMerchant = TheMerchantList.GetMerchant(ClientIDGuid)
        End If

        If ValidateRPOUserName <> "" And ClientIDGuid = Guid.Empty Then
            ErrorDescription = "Error processing Client ID: '" & TheTransaction.ClientID & ". Standard GUID format is expected."
            Return False
        End If

        If TheMerchant Is Nothing Then
            ErrorDescription = "Error processing Client ID: '" & TheTransaction.ClientID & " or incorrect password."
            Return False
        End If

        If TheMerchant.UserMerchantID = "" Then
            ErrorDescription = "Unknown Client ID: '" & TheTransaction.ClientID & "' or incorrect password."
            Return False
        End If

        'If Not TheMerchant.WebUser.ASPMembership Is Nothing Then
        '    Dim UserName As String = TheMerchant.WebUser.ASPMembership.UserName
        '    If ValidateRPOUserName <> "" Then
        '        If UCase(UserName) <> UCase(ValidateRPOUserName) Then
        '            ErrorDescription = "Unauthorized RPO user name or password."
        '            Return False
        '        End If
        '    End If
        '    If Not Membership.ValidateUser(UserName, TheTransaction.Password) Then
        '        ErrorDescription = "Unauthorized User ID or wrong password."
        '        Return False
        '    End If
        'Else
        '    ErrorDescription = "Invalid Client ID or incorrect password."
        '    Return False
        'End If

        If Not TheMerchant.EnableTransactionProcessingFlag Then
            ErrorDescription = "Client '" & TheTransaction.ClientID & "' is not enabled for transaction processing."
            Return False
        End If
        If TheMerchant.MerchantActiveDate > Now Then
            ErrorDescription = "Client '" & TheTransaction.ClientID & "' is not active."
            Return False
        End If
        If TheMerchant.SettlementBankABA = "" Or TheMerchant.SettlementBankDDA = "" Then
            ErrorDescription = "Client '" & TheTransaction.ClientID & "' bank info is unavailable."
            Return False
        End If

        If Not DV.ValidateLength("Amount", TheTransaction.Amount, 1, 15, Required) Then
            ErrorDescription = DV.ErrorDescription
            Return False
        End If

        If Not DV.ValidateIsNumeric("Amount", TheTransaction.Amount, -1000000000, 1000000000) Then
            ErrorDescription = DV.ErrorDescription
            Return False
        Else
            Dim TheValue As Double = Math.Abs(CDbl(TheTransaction.Amount))
            If Not DV.ValidateIsNumeric("Amount", TheValue.ToString, 0.05, 1000000000) Then
                ErrorDescription = DV.ErrorDescription
                Return False
            End If
        End If

        If Not DV.ValidateLength("ABA Number", TheTransaction.ABANumber, 9, 9, Required) Then
            ErrorDescription = DV.ErrorDescription
            Return False
        End If

        Dim i As Int16
        For i = 0 To TheTransaction.ABANumber.Length - 1
            If Not IsNumeric(TheTransaction.ABANumber.Substring(i, 1)) Then
                ErrorDescription = "ABA Number does not contain 9 numeric digits."
                Return False
            End If
        Next
        'ABA number must pass checksum algorithm
        Dim n As Double = 0
        For i = 0 To TheTransaction.ABANumber.Length - 1 Step 3
            n = n + (Convert.ToInt16(TheTransaction.ABANumber.Substring(i, 1)) * 3)
            n = n + (Convert.ToInt16(TheTransaction.ABANumber.Substring(i + 1, 1)) * 7)
            n = n + (Convert.ToInt16(TheTransaction.ABANumber.Substring(i + 2, 1)) * 1)
        Next
        If n Mod 10 <> 0 Then
            ErrorDescription = "Invalid ABA number - invalid checksum digit."
            Return False
        End If

        'Salcedo - 12/10/2013 - added logic to validate ABA against the Fed's database
        Dim mdb As New MiscDatabase
        If Not mdb.ValidateABA(TheTransaction.ABANumber) Then
            ErrorDescription = "Invalid ABA number - not found in Fed ACH Database."
            Return False
        End If

        If Not DV.ValidateLength("Account Number", TheTransaction.AccountNumber, 2, 50, Required) Then
            ErrorDescription = DV.ErrorDescription
            Return False
        End If
        If UCase(TheTransaction.AccountType) <> UCase("checking") Then
            If UCase(TheTransaction.AccountType) <> UCase("savings") Then
                ErrorDescription = "Invalid AccountType value - expected value 'CHECKING' or 'SAVINGS'"
                Return False
            End If
        End If
        If Not DV.ValidateLength("First Name", TheTransaction.FirstName, 1, 50, Required) Then
            ErrorDescription = DV.ErrorDescription
            Return False
        End If
        If Not DV.ValidateLength("Last Name", TheTransaction.LastName, 2, 50, Required) Then
            ErrorDescription = DV.ErrorDescription
            Return False
        End If
        If Not DV.ValidateLength("Address 1", TheTransaction.Address1, 2, 50, Required) Then
            ErrorDescription = DV.ErrorDescription
            Return False
        End If
        If Not DV.ValidateLength("Address 2", TheTransaction.Address2, 2, 50, NotRequired) Then
            ErrorDescription = DV.ErrorDescription
            Return False
        End If
        If Not DV.ValidateLength("City", TheTransaction.City, 2, 50, Required) Then
            ErrorDescription = DV.ErrorDescription
            Return False
        End If
        If Not DV.ValidateLength("State", TheTransaction.State, 2, 2, Required) Then
            ErrorDescription = DV.ErrorDescription
            Return False
        End If
        Dim MyAddressStateCodeList As New AddressStateCodeList
        If Not MyAddressStateCodeList.StateExists(TheTransaction.State) Then
            ErrorDescription = "Invalid State code."
            Return False
        End If

        If Not DV.ValidateLength("Zip", TheTransaction.Zip, 5, 10, Required) Then
            ErrorDescription = DV.ErrorDescription
            Return False
        End If
        If Not DV.ValidateLength("Phone Number", TheTransaction.Phone, 2, 50, NotRequired) Then
            ErrorDescription = DV.ErrorDescription
            Return False
        End If

        Return True
    End Function

    Private Sub AddElement(Destination As XmlTextWriter, ElementName As String, ElementValue As String)
        Destination.WriteStartElement(ElementName)
        Destination.WriteString(ElementValue)
        Destination.WriteEndElement()
        Return
    End Sub

    Private Function ValidateUserAndPassword(UserID As String, Password As String) As Boolean
        Dim bGoodCredentials As Boolean = True
        Return bGoodCredentials


        'Dim MyWebUser As WebUser
        'Dim MyWebUserList As New WebUserList
        'MyWebUser = MyWebUserList.GetWebUser(UserID)
        'If MyWebUser Is Nothing Then
        '    bGoodCredentials = False
        'Else
        '    If Not Membership.ValidateUser(UserID, Password) Then
        '        bGoodCredentials = False
        '    End If
        'End If

        'Return bGoodCredentials
    End Function

    Private Sub SetError(ByRef ETR As EFXTransactionResponse, ResponseCode As String, ResponseDescription As String)
        ETR.ResponseCode = ResponseCode
        ETR.ResponseDescription = ResponseDescription
    End Sub

    Public Function RPO_CreateACHTransaction(ByVal RPO_UserID As String, RPO_Password As String, ByVal ACHClientID As String, _
        Amount As String, ABANumber As String, AccountNumber As String, _
        AccountType As String, FirstName As String, LastName As String, Address1 As String, _
        Address2 As String, City As String, State As String, Zip As String, Phone As String) As String

        'A negative Transaction Amount will create a credit transaction, if allowed for client.

        Dim ETR As EFXTransactionResponse
        ETR = New EFXTransactionResponse
        ETR.ResponseCode = "-9998"
        ETR.ResponseDescription = "Unable to add new transaction to database."
        ETR.TransactionID = ""

        Dim TheTransaction As New ACHTransaction
        TheTransaction.ClientID = ACHClientID
        TheTransaction.Password = RPO_Password
        TheTransaction.Amount = Amount
        TheTransaction.ABANumber = ABANumber
        TheTransaction.AccountNumber = AccountNumber
        TheTransaction.AccountType = AccountType
        TheTransaction.FirstName = FirstName
        TheTransaction.LastName = LastName
        TheTransaction.Address1 = Address1
        TheTransaction.Address2 = Address2
        TheTransaction.City = City
        TheTransaction.State = State
        TheTransaction.Zip = Zip
        TheTransaction.Phone = Phone

        Dim x As String = TheTransaction.ClientID

        WriteLog(TheTransaction.ClientID, "RPO_CreateACHTransaction entered.")

        Dim ErrorDescription As String = ""


        If Not ValidateData(TheTransaction, ErrorDescription, RPO_UserID) Then
            ETR.ResponseCode = "-9999"
            ETR.ResponseDescription = ErrorDescription
            ETR.TransactionID = ""
            WriteLog(TheTransaction.ClientID, ETR.ResponseDescription)

        Else

            Dim TL As New TransactionList
            Dim NewTransaction As New ACH_Objects.Transaction

            Dim MyMerchant As Merchant
            Dim MyMerchants As New MerchantList

            'Assumption is made that the ClientID is in GUID format
            Dim ClientIDGuid As Guid = New Guid(TheTransaction.ClientID)
            MyMerchant = MyMerchants.GetMerchant(ClientIDGuid)

            ETR.ResponseCode = "-9997"
            ETR.ResponseDescription = "Invalid Client ID: '" & TheTransaction.ClientID & "'"
            ETR.TransactionID = ""

            If Not MyMerchant Is Nothing Then
                If MyMerchant.UserMerchantID <> "" Then


                    NewTransaction.InternalMerchantID = MyMerchant.InternalMerchantID
                    NewTransaction.TransactionStatus = 1     'Pending transaction

                    Dim TheCurrentDate As Date = Now

                    'Merchant Transaction ID - create a unique transaction ID, based on the current time
                    NewTransaction.MerchantTransactionID = TheCurrentDate.Ticks.ToString

                    NewTransaction.MerchantCustomerID = MyMerchant.UserMerchantID

                    NewTransaction.RoutingNumber = TheTransaction.ABANumber
                    NewTransaction.AccountNumber = TheTransaction.AccountNumber

                    If UCase(TheTransaction.AccountType) = "CHECKING" Then
                        NewTransaction.AccountType = 1 '1=Checking, 2=Savings
                    Else
                        NewTransaction.AccountType = 2
                    End If

                    NewTransaction.TransactionAmount = CDbl(TheTransaction.Amount)
                    If NewTransaction.TransactionAmount < 0 Then
                        If MyMerchant.AllowCreditTransactions Then
                            NewTransaction.TransactionType = 1 ' Credit transaction
                            NewTransaction.TransactionAmount = NewTransaction.TransactionAmount * -1
                        Else
                            ETR.ResponseCode = "-9978"
                            ETR.ResponseDescription = "Credit transactions are not allowed for this Client ID."
                            ETR.TransactionID = ""
                            NewTransaction.TransactionAmount = 0
                        End If
                    Else
                        NewTransaction.TransactionType = 2  'Debit transactions only
                    End If

                    NewTransaction.TransactionTime = TheCurrentDate

                    NewTransaction.FirstName = TheTransaction.FirstName
                    NewTransaction.LastName = TheTransaction.LastName
                    NewTransaction.Address1 = TheTransaction.Address1
                    NewTransaction.Address2 = TheTransaction.Address2
                    NewTransaction.City = TheTransaction.City
                    NewTransaction.State = TheTransaction.State
                    NewTransaction.Zip = TheTransaction.Zip
                    NewTransaction.PhoneNumber = TheTransaction.Phone

                    NewTransaction.ReceivedDate = TheCurrentDate
                    NewTransaction.StatusChangeDate = TheCurrentDate

                    'Make sure this user has not made a previous payment for the same amount within the last "12" hours (configurable window)
                    'This is to avoid accidental double payments

                    'Salcedo - 8/24/2014 - changed logic to only check for dups on debit transactions ...
                    If NewTransaction.TransactionType = 2 And TL.HasMatchingTransaction(NewTransaction, CInt(DuplicateTransactionHourWindow)) Then
                        ETR.ResponseCode = "-9979"
                        Dim sAccountType As String = ""
                        If NewTransaction.AccountType = 1 Then
                            sAccountType = "Checking"
                        Else
                            sAccountType = "Savings"
                        End If
                        ETR.ResponseDescription = "Unable to process - a transaction was recently processed against this " & sAccountType & " account. " & _
                            "Last 4 digits of account number: " & Right(NewTransaction.AccountNumber, 4) & ". Internal Transaction ID " & _
                            NewTransaction.MerchantTransactionID & " was recently processed at " & NewTransaction.TransactionTime.ToShortDateString & " " & _
                            NewTransaction.TransactionTime.ToShortTimeString & " for " & NewTransaction.TransactionAmount & _
                            ". Please make a note of the Internal Transaction ID, and if necessary, contact customer support for assistance."
                        ETR.TransactionID = ""
                        WriteLog(TheTransaction.ClientID, ETR.ResponseDescription)
                    Else
                        If NewTransaction.TransactionAmount <> 0 Then
                            Dim NewGuid As Guid = TL.AddTransaction(NewTransaction)

                            If NewGuid = Guid.Empty Then
                                ETR.ResponseCode = "-9998"
                                ETR.ResponseDescription = "Unable to add new transaction to database."
                                ETR.TransactionID = ""
                                WriteLog(TheTransaction.ClientID, ETR.ResponseDescription)
                            Else
                                ETR.ResponseCode = "0"
                                ETR.ResponseDescription = "Transaction successfully submitted."
                                ETR.TransactionID = NewTransaction.MerchantTransactionID
                                WriteLog(TheTransaction.ClientID, "Transaction successfully submitted. ID: " & NewTransaction.MerchantTransactionID & ";  Amount: " & NewTransaction.TransactionAmount.ToString("c"))
                            End If
                        End If
                    End If
                End If
            End If
        End If

        Dim TheReturn As String


        'Format the XML response string

        Dim memory_stream As New MemoryStream
        Dim xml_text_writer As New XmlTextWriter(memory_stream, System.Text.Encoding.UTF8)

        ' Use indentation to make the result look nice.
        xml_text_writer.Formatting = Formatting.Indented
        xml_text_writer.Indentation = 4

        ' Write the XML declaration.
        xml_text_writer.WriteStartDocument(True)

        xml_text_writer.WriteStartElement("RPOCreateACHTransaction")

        xml_text_writer.WriteStartElement("ResponseDetail")
        AddElement(xml_text_writer, "ResponseCode", ETR.ResponseCode)
        AddElement(xml_text_writer, "ResponseDescription", ETR.ResponseDescription)
        AddElement(xml_text_writer, "TransactionID", ETR.TransactionID)
        xml_text_writer.WriteEndElement()

        xml_text_writer.WriteStartElement("TransactionDetail")
        AddElement(xml_text_writer, "Amount", TheTransaction.Amount)
        AddElement(xml_text_writer, "ABANumber", TheTransaction.ABANumber)
        AddElement(xml_text_writer, "AccountNumber", TheTransaction.AccountNumber)
        AddElement(xml_text_writer, "AccountType", TheTransaction.AccountType)
        AddElement(xml_text_writer, "FirstName", TheTransaction.FirstName)
        AddElement(xml_text_writer, "LastName", TheTransaction.LastName)
        AddElement(xml_text_writer, "Address1", TheTransaction.Address1)
        AddElement(xml_text_writer, "Address2", TheTransaction.Address2)
        AddElement(xml_text_writer, "City", TheTransaction.City)
        AddElement(xml_text_writer, "State", TheTransaction.State)
        AddElement(xml_text_writer, "Zip", TheTransaction.Zip)
        AddElement(xml_text_writer, "Phone", TheTransaction.Phone)
        xml_text_writer.WriteEndElement()

        xml_text_writer.WriteEndElement()

        ' End the document.
        xml_text_writer.WriteEndDocument()
        xml_text_writer.Flush()

        ' Use a StreamReader to get the result.
        Dim stream_reader As New StreamReader(memory_stream)

        memory_stream.Seek(0, SeekOrigin.Begin)


        TheReturn = stream_reader.ReadToEnd()

        ' Close the XmlTextWriter.
        xml_text_writer.Close()


        Return TheReturn

    End Function

    Public Function RPO_GetTransactionStatus(ByVal RPO_UserID As String, RPO_Password As String, ByVal ACHClientID As String, ByVal TransactionID As String) As String

        WriteLog(RPO_UserID, "RPO_GetTransactionStatus method entered.")

        Dim ETS As EFXTransactionStatus
        ETS = New EFXTransactionStatus
        ETS.StatusCode = "-9999"
        ETS.StatusDescription = "Transaction not found."
        ETS.StatusDate = Now.ToShortDateString

        Dim bGoodCredentials As Boolean = True

        'Try to convert the ClientID into a GUID
        Dim ClientIDGuid As Guid = Guid.Empty
        Try
            ClientIDGuid = New Guid(ACHClientID)
        Catch ex As Exception
        End Try

        Dim MyMerchant As Merchant
        Dim MyMerchants As New MerchantList

        If ClientIDGuid = Guid.Empty Then
            'The ACH Client ID param we were given is not a guid ...
            ETS.StatusDescription = "Error processing ACH Client ID: '" & ACHClientID & "'. Invalid GUID format."
            bGoodCredentials = False
            MyMerchant = Nothing
        Else
            MyMerchant = MyMerchants.GetMerchant(ClientIDGuid)

            If MyMerchant Is Nothing Then
                ETS.StatusDescription = "Error processing Client ID: '" & ACHClientID & "' or incorrect password."
                bGoodCredentials = False
            End If

            If MyMerchant.UserMerchantID = "" Then
                ETS.StatusDescription = "Unknown Client ID: '" & ACHClientID & "' or incorrect password."
                bGoodCredentials = False
            End If

            'If Not MyMerchant.WebUser.ASPMembership Is Nothing Then
            '    Dim UserName As String = MyMerchant.WebUser.ASPMembership.UserName
            '    If UCase(UserName) <> UCase(RPO_UserID) Then
            '        ETS.StatusDescription = "Unauthorized RPO User ID or wrong password."
            '        bGoodCredentials = False
            '    Else
            '        If Not Membership.ValidateUser(UserName, RPO_Password) Then
            '            ETS.StatusDescription = "Unauthorized Client ID or wrong password."
            '            bGoodCredentials = False
            '        End If
            '    End If
            'Else
            '    ETS.StatusDescription = "Invalid Client ID or incorrect password."
            '    bGoodCredentials = False
            'End If

        End If

        If bGoodCredentials Then

            'If the credentials were ok, then lookup the transaction ...

            'By default, we couldn't find the transaction ...
            ETS.StatusCode = "-9997"
            ETS.StatusDescription = "Transaction not found."

            If Not MyMerchant Is Nothing Then
                If MyMerchant.UserMerchantID <> "" Then

                    Dim MyTL As New TransactionList
                    Dim MyTransaction As ACH_Objects.Transaction = MyTL.GetMerchantTransaction(MyMerchant.InternalMerchantID, TransactionID)

                    If Not MyTransaction Is Nothing Then
                        If MyTransaction.MerchantTransactionID <> "" Then

                            ETS.StatusCode = MyTransaction.TransactionStatus.ToString
                            Select Case MyTransaction.TransactionStatus
                                Case 1
                                    ETS.StatusDescription = "Pending"
                                Case 2
                                    ETS.StatusDescription = "Working"
                                Case 3
                                    ETS.StatusDescription = "In Process"
                                Case 4
                                    ETS.StatusDescription = "Cleared"
                                Case 5
                                    ETS.StatusDescription = "Rejected"
                                Case 6
                                    ETS.StatusDescription = "Returned"
                                Case 7
                                    ETS.StatusDescription = "Chargedback"
                                Case 8
                                    ETS.StatusDescription = "Cancelled"
                                Case Else
                                    ETS.StatusDescription = "Unknown"
                            End Select

                            ETS.StatusDate = MyTransaction.StatusChangeDate.ToShortDateString
                        End If
                    End If
                End If
            End If
        End If


        'Format the XML response string
        Dim TheReturn As String

        Dim memory_stream As New MemoryStream
        Dim xml_text_writer As New XmlTextWriter(memory_stream, System.Text.Encoding.UTF8)

        ' Use indentation to make the result look nice.
        xml_text_writer.Formatting = Formatting.Indented
        xml_text_writer.Indentation = 4

        ' Write the XML declaration.
        xml_text_writer.WriteStartDocument(True)

        xml_text_writer.WriteStartElement("RPOGetTransactionStatus")

        xml_text_writer.WriteStartElement("StatusDetail")
        AddElement(xml_text_writer, "StatusCode", ETS.StatusCode)
        AddElement(xml_text_writer, "StatusDescription", ETS.StatusDescription)
        AddElement(xml_text_writer, "StatusDate", ETS.StatusDate)
        xml_text_writer.WriteEndElement()

        xml_text_writer.WriteEndElement()

        ' End the document.
        xml_text_writer.WriteEndDocument()
        xml_text_writer.Flush()

        ' Use a StreamReader to get the result.
        Dim stream_reader As New StreamReader(memory_stream)
        memory_stream.Seek(0, SeekOrigin.Begin)
        TheReturn = stream_reader.ReadToEnd()

        ' Close the XmlTextWriter.
        xml_text_writer.Close()


        Return TheReturn

    End Function

    Function RPO_NewACHAccountForProperty(ByVal RPO_UserID As String, RPO_Password As String, ByVal RPOPropertyAccountID As String, ByVal RPOPropertyName As String,
        RPOPropertyPhoneNumber As String, ACHProcessingBankName As String, ClearingDays As String,
        RoutingNumber As String, AccountNumber As String) As String

        WriteLog(RPO_UserID, "RPO_NewACHAccountForProperty method entered.")

        Dim theReturn As String = ""

        Dim etr As EFXTransactionResponse
        etr = New EFXTransactionResponse With {
            .ResponseCode = "0",
            .ResponseDescription = "Success",
            .TransactionID = ""
        }

        Dim bPramsOk = True

        ''Validate RPO_UserID and RPO_Password ...
        'If Membership.ValidateUser(RPO_UserID, RPO_Password) Then
        Dim wu As New WebUser(RPO_UserID)
        If wu.AllowMerchantCreate Then
            'Access allowed, so attempt to create a new merchant

            Dim ml As New MerchantList
            If Not ml.MerchantExists(RPOPropertyAccountID) Then

                'Validate RPOPropertyAccountID
                Dim reg = New Regex("^[ a-zA-Z0-9]{3,45}$")
                ' Patrick WHittingham: task# 000172 : 
                If bPramsOk And (Not reg.IsMatch(RPOPropertyAccountID)) Then
                    bPramsOk = False
                    Call SetError(etr, "-9996", "An invalid RPOPropertyAccountID was received - must contain between 3 and 45 alpha-numeric characters.")
                End If

                'Validate RPOPropertyName
                If bPramsOk And (RPOPropertyName.Length > 45 Or RPOPropertyName.Length < 2) Then
                    bPramsOk = False
                    Call SetError(etr, "-9995", "An invalid RPOPropertyName was received - must contain between 2 and 45 alpha-numeric digits.")
                End If

                'Validate RPOPropertyPhoneNumber
                RPOPropertyPhoneNumber = Regex.Replace(RPOPropertyPhoneNumber, "[^\d]", "")  'Strip out all non-numeric characters from the phone number
                'Salcedo - 4/29/2016 - fixed logic error in phone number length comparison
                If bPramsOk And (RPOPropertyPhoneNumber.Length > 45 Or RPOPropertyPhoneNumber.Length < 2) Then
                    bPramsOk = False
                    Call SetError(ETR, "-9994", "An invalid RPOPropertyPhoneNumber was received - must contain between 2 and 45 numeric digits.")
                End If

                'Validate ClearingDays
                If bPramsOk And ClearingDays <> "" Then
                    If IsNumeric(ClearingDays) Then
                        If CLng(ClearingDays) < 1 Or CLng(ClearingDays) > 30 Then
                            bPramsOk = False
                            Call SetError(ETR, "-9993", "An invalid ClearingDays was received - must contain a value between 1 and 30")
                        End If
                    Else
                        bPramsOk = False
                        Call SetError(ETR, "-9992", "An invalid ClearingDays was received - must contain a numeric string.")
                    End If

                Else
                    'Set default value
                    ClearingDays = DefaultClearingDays
                End If

                'Validate RoutingNumber
                RoutingNumber = LTrim(RTrim(RoutingNumber))
                If bPramsOk And (RoutingNumber.Length <> 9) Then
                    bPramsOk = False
                    Call SetError(ETR, "-9991", "An invalid RoutingNumber was received - must be exactly 9 digits, including check digit.")
                End If

                'ABA (RoutingNumber) number must also pass checksum algorithm
                If bPramsOk Then
                    Dim n As Double = 0
                    For i = 0 To RoutingNumber.Length - 1 Step 3
                        n = n + (Convert.ToInt16(RoutingNumber.Substring(i, 1)) * 3)
                        n = n + (Convert.ToInt16(RoutingNumber.Substring(i + 1, 1)) * 7)
                        n = n + (Convert.ToInt16(RoutingNumber.Substring(i + 2, 1)) * 1)
                    Next
                    If n Mod 10 <> 0 Then
                        bPramsOk = False
                        Call SetError(ETR, "-9990", "An invalid RoutingNumber was received - invalid checksum digit.")
                    End If
                End If

                'Validate AccountNumber
                If bPramsOk And (AccountNumber.Length > 45 Or AccountNumber.Length < 2) Then
                    bPramsOk = False
                    Call SetError(ETR, "-9989", "An invalid AccountNumber was received - must contain between 2 and 45 digits.")
                End If

                'Validate and determine ODFIName from ACHProcessingBankName
                Dim ODFIGuid As Guid
                If ACHProcessingBankName = "" Then
                    'Set default
                    ACHProcessingBankName = DefaultACHProcessingBankName
                End If
                If bPramsOk Then
                    Dim ol As New ODFIList
                    Call ol.FillCollection()

                    Dim ODFI As ODFI
                    Dim bFound As Boolean = False
                    For Each ODFI In ol
                        If UCase(ODFI.ODFIName) = UCase(ACHProcessingBankName) Then
                            ODFIGuid = ODFI.ODFIID
                            bFound = True
                            Exit For
                        End If
                    Next
                    If Not bFound Then
                        bPramsOk = False
                        Call SetError(ETR, "-9988", "An invalid ACHProcessingBankName was received.")
                    End If
                End If

                'If everything checks out ok, then create the new merchant and contact
                If bPramsOk Then
                    Dim TheNewMerchant As New Merchant

                    TheNewMerchant.WebUser = wu
                    TheNewMerchant.UserMerchantID = RPOPropertyAccountID
                    TheNewMerchant.TransactionDescriptor = RPOPropertyName
                    TheNewMerchant.TransactionCSNumber = RPOPropertyPhoneNumber
                    TheNewMerchant.ClearDays = CInt(ClearingDays)
                    TheNewMerchant.SettlementBankABA = RoutingNumber
                    TheNewMerchant.SettlementBankDDA = AccountNumber
                    TheNewMerchant.AssignedODFIID = ODFIGuid
                    TheNewMerchant.AllowCreditTransactions = True
                    TheNewMerchant.EnableTransactionProcessingFlag = True
                    TheNewMerchant.MerchantActiveDate = Now

                    'Add new merchant to database
                    Dim NewGuid As Guid = ml.AddMerchant(TheNewMerchant)

                    If NewGuid <> Guid.Empty Then
                        Dim NewContactInfo As New ContactInfo
                        NewContactInfo.ParentID = TheNewMerchant.InternalMerchantID
                        NewContactInfo.FirstName = "RPO"
                        NewContactInfo.LastName = RPOPropertyName
                        NewContactInfo.PrimaryPhone = RPOPropertyPhoneNumber
                        NewContactInfo.ContactType = ContactInfo.ContactTypes.ClientInformation

                        'Add new contact to database
                        Dim cl As New ContactInfoList
                        Dim NewContactGuid As Guid = cl.AddContactInfo(NewContactInfo)
                        If NewContactGuid = Guid.Empty Then
                            bPramsOk = False
                            Call SetError(ETR, "-9983", "System exception occurred during Client Contact creation process.")
                        Else

                            'Success!
                            ETR.TransactionID = NewGuid.ToString
                            WriteLog(RPO_UserID, "New RPO ACHAccountForProperty merchant record was created. RPOPropertyAccountID: " &
                                RPOPropertyAccountID & "Guid ID: " & NewGuid.ToString)

                        End If
                    Else
                        bPramsOk = False
                        Call SetError(ETR, "-9984", "System exception occurred during Client creation process.")
                    End If

                End If

            Else
                bPramsOk = False
                Call SetError(ETR, "-9987", "Invalid RPOPropertyAccountID value - the specified new value already exists in the system.")
            End If

        Else
            'Credentials are valid, but user is not authorized to create merchants
            bPramsOk = False
            Call SetError(ETR, "-9986", "Unauthorized user or credentials")
        End If

        Dim memoryStream As New MemoryStream
        ' Use indentation to make the result look nice.
        Dim xmlTextWriter As New XmlTextWriter(memoryStream, System.Text.Encoding.UTF8) With {
            .Formatting = Formatting.Indented,
            .Indentation = 4
        }

        ' Write the XML declaration.
        xmlTextWriter.WriteStartDocument(True)

        xmlTextWriter.WriteStartElement("RPONewACHAccountForProperty")

        xmlTextWriter.WriteStartElement("ResponseDetail")
        AddElement(xmlTextWriter, "ResponseCode", etr.ResponseCode)
        AddElement(xmlTextWriter, "ResponseDescription", etr.ResponseDescription)
        AddElement(xmlTextWriter, "ACHClientID", etr.TransactionID)
        xmlTextWriter.WriteEndElement()

        xmlTextWriter.WriteStartElement("AccountDetail")
        AddElement(xmlTextWriter, "RPOPropertyAccountID", RPOPropertyAccountID)
        AddElement(xmlTextWriter, "RPOPropertyName", RPOPropertyName)
        AddElement(xmlTextWriter, "RPOPropertyPhoneNumber", RPOPropertyPhoneNumber)
        AddElement(xmlTextWriter, "ACHProcessingBankName", ACHProcessingBankName)
        AddElement(xmlTextWriter, "ClearingDays", ClearingDays)
        AddElement(xmlTextWriter, "RoutingNumber", LTrim(RTrim(RoutingNumber)))
        AddElement(xmlTextWriter, "AccountNumber", AccountNumber)
        xmlTextWriter.WriteEndElement()

        xmlTextWriter.WriteEndElement()

        ' End the document.
        xmlTextWriter.WriteEndDocument()
        xmlTextWriter.Flush()

        ' Use a StreamReader to get the result.
        Dim streamReader As New StreamReader(memoryStream)
        memoryStream.Seek(0, SeekOrigin.Begin)
        theReturn = streamReader.ReadToEnd()

        xmlTextWriter.Close()

        'Return the XML string
        Return theReturn
    End Function
    'CMallory - Task 00603 - Added CCDepositResponse variable to the signature.
    Function RPO_UpdateACHAccountForProperty(ByVal rpoUserId As String, rpoPassword As String, ByVal ACHClientID As String, _
                                             ByVal rpoPropertyName As String, rpoPropertyPhoneNumber As String, achProcessingBankName As String, _
                                             clearingDays As String, routingNumber As String, accountNumber As String, rpoUserMerchantid As String, _
                                             ByVal ccDepositResponse As Boolean) As String

        WriteLog(rpoUserId, "RPO_UpdateACHAccountForProperty method entered.")

        Dim theReturn = ""

        Dim etr As New EFXTransactionResponse With {
            .ResponseCode = "0",
            .ResponseDescription = "Success",
            .TransactionID = ""
        }

        Dim bParmsOk = True

        ''Validate RPO_UserID and RPO_Password ...
        'If Membership.ValidateUser(RPO_UserID, RPO_Password) Then
        Dim wu As New WebUser(rpoUserId)
        If wu.AllowMerchantCreate Then
            'Access allowed, so attempt to update the merchant

            Dim clientIdGuid As Guid = Guid.Empty
            Try
                clientIdGuid = New Guid(ACHClientID)
            Catch ex As Exception
            End Try

            Dim ml As New MerchantList
            If Not clientIdGuid = Guid.Empty Then

                If ml.MerchantExists(clientIdGuid) Then

                    'Validate RPOPropertyName
                    If bParmsOk And (RPOPropertyName.Length > 45 Or RPOPropertyName.Length < 2) Then
                        bParmsOk = False
                        Call SetError(etr, "-9995", "An invalid RPOPropertyName was received - must contain between 2 and 45 alpha-numeric digits.")
                    End If

                    'Validate RPOPropertyPhoneNumber
                    RPOPropertyPhoneNumber = Regex.Replace(RPOPropertyPhoneNumber, "[^\d]", "")  
                    If bParmsOk And (RPOPropertyPhoneNumber.Length > 45 Or RPOPropertyPhoneNumber < 2) Then
                        bParmsOk = False
                        Call SetError(etr, "-9994", "An invalid RPOPropertyPhoneNumber was received - must contain between 2 and 45 numeric digits.")
                    End If

                    'Validate ClearingDays
                    If bParmsOk And ClearingDays <> "" Then
                        If IsNumeric(ClearingDays) Then
                            If CLng(ClearingDays) < 1 Or CLng(ClearingDays) > 30 Then
                                bParmsOk = False
                                Call SetError(etr, "-9993", "An invalid ClearingDays was received - must contain a value between 1 and 30")
                            End If
                        Else
                            bParmsOk = False
                            Call SetError(etr, "-9992", "An invalid ClearingDays was received - must contain a numeric string.")
                        End If

                    Else
                        'Set default value
                        ClearingDays = DefaultClearingDays
                    End If

                    'Validate RoutingNumber
                    RoutingNumber = LTrim(RTrim(RoutingNumber))
                    If bParmsOk And (RoutingNumber.Length <> 9) Then
                        bParmsOk = False
                        Call SetError(etr, "-9991", "An invalid RoutingNumber was received - must be exactly 9 digits, including check digit.")
                    End If

                    'ABA (RoutingNumber) number must also pass checksum algorithm
                    If bParmsOk Then
                        Dim n As Double = 0
                        For i = 0 To RoutingNumber.Length - 1 Step 3
                            n += (Convert.ToInt16(RoutingNumber.Substring(i, 1)) * 3)
                            n += (Convert.ToInt16(RoutingNumber.Substring(i + 1, 1)) * 7)
                            n += (Convert.ToInt16(RoutingNumber.Substring(i + 2, 1)) * 1)
                        Next
                        If n Mod 10 <> 0 Then
                            bParmsOk = False
                            Call SetError(etr, "-9990", "An invalid RoutingNumber was received - invalid checksum digit.")
                        End If
                    End If

                    'Validate AccountNumber
                    If bParmsOk And (AccountNumber.Length > 45 Or AccountNumber.Length < 2) Then
                        bParmsOk = False
                        Call SetError(etr, "-9989", "An invalid AccountNumber was received - must contain between 2 and 45 digits.")
                    End If

                    'Validate and determine ODFIName from ACHProcessingBankName
                    Dim odfiGuid As Guid
                    If ACHProcessingBankName = "" Then
                        'Set default
                        ACHProcessingBankName = DefaultACHProcessingBankName
                    End If
                    If bParmsOk Then
                        Dim ol As New ODFIList
                        Call ol.FillCollection()

                        Dim odfi As ODFI
                        Dim bFound As Boolean = False
                        For Each odfi In ol
                            If UCase(odfi.ODFIName) = UCase(ACHProcessingBankName) Then
                                odfiGuid = odfi.ODFIID
                                bFound = True
                                Exit For
                            End If
                        Next
                        If Not bFound Then
                            bParmsOk = False
                            Call SetError(etr, "-9988", "An invalid ACHProcessingBankName was received.")
                        End If
                    End If

                    'If everything checks out ok, then update the merchant and contact
                    If bParmsOk Then
                        'CMallory - Task 00453 - Wrapping database call in Try/Catch to prevent the user from seeing an error if one occurs.  Because of this, if they're trying to update a property name,
                        '                        it should still update.
                        Dim theMerchant As Merchant
                        Try
                            theMerchant = ml.GetMerchant(clientIdGuid)

                            'Patrick Whittingham - 4/15/15 - task# 000172: set usermerchantid 
                            theMerchant.WebUser = wu
                            theMerchant.TransactionDescriptor = RPOPropertyName
                            theMerchant.UserMerchantID = RPOUserMerchantid
                            theMerchant.TransactionCSNumber = RPOPropertyPhoneNumber
                            theMerchant.ClearDays = CInt(ClearingDays)
                            theMerchant.SettlementBankABA = RoutingNumber
                            theMerchant.SettlementBankDDA = AccountNumber
                            theMerchant.AssignedODFIID = odfiGuid

                            ' Update merchant in database
                            Try
                                Dim updateReturn = ml.UpdateMerchant(theMerchant)

                                If updateReturn Then
                                    Dim cl As New ContactInfoList
                                    Dim theContactInfo As ContactInfo = New ContactInfo() ' TheMerchant.Contacts(0)

                                    ' CMallory - Task 00603 - Execute existing logic if this function is passed with the CCDepositAccountAchClientId variable be false
                                    If (theMerchant.Contacts.Count <> 0) Then
                                        theContactInfo = theMerchant.Contacts(0)
                                    End If
                                    theContactInfo.FirstName = "RPO"
                                    theContactInfo.LastName = RPOPropertyName
                                    theContactInfo.PrimaryPhone = RPOPropertyPhoneNumber
                                    theContactInfo.ContactType = ContactInfo.ContactTypes.ClientInformation

                                    'CMallory - Task 00453 - Wrapping retrieval of item from Merchant array Try/Catch to prevet the user from seeing an error if the array is empty  Because of this, if they're trying to update a property name,
                                    '                        it should still update.
                                    'Update contact in database
                                    Dim contactUpdateReturns As Boolean
                                    Try
                                        'CMallory - Task 00603 - Execute existi`ng logic if this function is passed with the CCDepositAccountAchClientId variable be false
                                        If (CCDepositResponse = False) Then
                                            cl.UpdateContactInfo(theContactInfo)
                                        Else
                                            contactUpdateReturns = True
                                        End If

                                    Catch ex As Exception
                                        WriteLog(rpoUserId, "Error occured while trying to update the Contact Information for RPOPropertyAccountID: " &
                                            theMerchant.UserMerchantID & "Guid ID: " & theMerchant.InternalMerchantID.ToString + ".  Error Message: " + ex.Message.ToString())
                                    End Try

                                    If Not contactUpdateReturns Then
                                        bParmsOk = False
                                        Call SetError(etr, "-9980", "System exception occurred during Client Contact update process.")
                                    Else

                                        'Success!
                                        etr.TransactionID = ACHClientID
                                        WriteLog(rpoUserId, "RPO ACHAccountForProperty merchant record was updated. RPOPropertyAccountID: " &
                                            theMerchant.UserMerchantID & "Guid ID: " & theMerchant.InternalMerchantID.ToString)
                                    End If
                                Else
                                    bParmsOk = False
                                    Call SetError(etr, "-9979", "System exception occurred during Client update process.")
                                End If
                            Catch ex As Exception
                                WriteLog(rpoUserId, "Error occured while trying to update the Merchant Information for the ACH Merchant table. UserMerchantID: " &
                                        theMerchant.UserMerchantID & "; InternalMerchantID: " & theMerchant.InternalMerchantID.ToString + ".  Error Message: " + ex.Message.ToString())
                            End Try

                        Catch ex As Exception
                            WriteLog(rpoUserId, "Error occured while trying to get Merchant information for ClientID Guid: " &
                                    clientIdGuid.ToString() + ".  Error Message: " + ex.Message.ToString())
                        End Try
                    End If

                Else
                    bParmsOk = False
                    Call SetError(etr, "-9981", "Invalid ACHClientID value - the specified value does not exist in the system.")
                End If
            Else
                bParmsOk = False
                Call SetError(etr, "-9982", "Error processing Client ID: '" & ACHClientID & "'. Invalid GUID format.")
            End If
        Else
            'Credentials are valid, but user is not authorized to create merchants
            bParmsOk = False
            Call SetError(etr, "-9986", "Unauthorized user or credentials")
        End If

        Dim memoryStream As New MemoryStream
        Dim xmlTextWriter As New XmlTextWriter(memoryStream, System.Text.Encoding.UTF8)

        ' Use indentation to make the result look nice.
        xmlTextWriter.Formatting = Formatting.Indented
        xmlTextWriter.Indentation = 4

        ' Write the XML declaration.
        xmlTextWriter.WriteStartDocument(True)

        xmlTextWriter.WriteStartElement("RPOUpdateACHAccountForProperty")

        xmlTextWriter.WriteStartElement("ResponseDetail")
        AddElement(xmlTextWriter, "ResponseCode", etr.ResponseCode)
        AddElement(xmlTextWriter, "ResponseDescription", etr.ResponseDescription)
        AddElement(xmlTextWriter, "ACHClientID", etr.TransactionID)
        xmlTextWriter.WriteEndElement()

        xmlTextWriter.WriteStartElement("AccountDetail")
        AddElement(xmlTextWriter, "ACHClientID", ACHClientID)
        AddElement(xmlTextWriter, "RPOPropertyName", RPOPropertyName)
        AddElement(xmlTextWriter, "RPOPropertyPhoneNumber", RPOPropertyPhoneNumber)
        AddElement(xmlTextWriter, "ACHProcessingBankName", ACHProcessingBankName)
        AddElement(xmlTextWriter, "ClearingDays", ClearingDays)
        AddElement(xmlTextWriter, "RoutingNumber", LTrim(RTrim(RoutingNumber)))
        AddElement(xmlTextWriter, "AccountNumber", AccountNumber)
        xmlTextWriter.WriteEndElement()

        xmlTextWriter.WriteEndElement()

        ' End the document.
        xmlTextWriter.WriteEndDocument()
        xmlTextWriter.Flush()

        ' Use a StreamReader to get the result.
        Dim streamReader As New StreamReader(memoryStream)

        memoryStream.Seek(0, SeekOrigin.Begin)

        theReturn = streamReader.ReadToEnd()

        ' Close the XmlTextWriter.
        xmlTextWriter.Close()

        'Return the XML string
        Return theReturn
    End Function

    Function GetTransactionStatusesForDateRange(ByVal userId As String, ByVal password As String,
        ByVal achClientId As String, ByVal startDate As String, ByVal endDate As String) As String

        WriteLog(UserID, "GetTransactionStatusesForDateRange method entered.")

        Dim bGoodCredentials As Boolean = True

        Dim fileUploadResponseDetail As New EFXFileUploadResponseDetail With {
            .UploadDateTime = Now.ToShortDateString & " " & Now.ToShortTimeString
        }

        If Not ValidateUserAndPassword(UserID, Password) Then
            fileUploadResponseDetail.StatusCode = "20"
            fileUploadResponseDetail.StatusDescription = "Authentication failure."
            bGoodCredentials = False
        End If

        'Verify ACHClientID is GUID
        Dim ACHClientIDGuid As Guid = Guid.Empty

        If bGoodCredentials Then
            Try
                ACHClientIDGuid = New Guid(ACHClientID)
            Catch ex As Exception
            End Try

            If ACHClientIDGuid = Guid.Empty Then
                bGoodCredentials = False
                fileUploadResponseDetail.StatusCode = "21"
                fileUploadResponseDetail.StatusDescription = "Invalid Client ID - expected Guid format."
            End If
        End If

        If bGoodCredentials Then
            Dim MyMerchant As Merchant
            Dim MyMerchants As New MerchantList

            'Verify user has access to Client ID
            MyMerchant = MyMerchants.GetMerchant(ACHClientIDGuid)
            If Not MyMerchant Is Nothing Then
                If MyMerchant.InternalMerchantID <> Guid.Empty Then
                    'If UCase(MyMerchant.WebUser.ASPMembership.UserName) <> UCase(UserID) Then
                    '    FileUploadResponseDetail.StatusCode = "20"
                    '    FileUploadResponseDetail.StatusDescription = "Authentication failure."
                    '    bGoodCredentials = False
                    'End If
                Else
                    bGoodCredentials = False
                    fileUploadResponseDetail.StatusCode = "21"
                    fileUploadResponseDetail.StatusDescription = "Invalid Client ID."
                End If
            Else
                bGoodCredentials = False
                fileUploadResponseDetail.StatusCode = "21"
                fileUploadResponseDetail.StatusDescription = "Invalid Client ID."
            End If
        End If

        If bGoodCredentials Then
            Dim DV As New DataValidation()

            'Verify StartDate is a date
            If Not DV.ValidateIsDate("StartDate", StartDate) Then
                bGoodCredentials = False
                fileUploadResponseDetail.StatusCode = "24"
                fileUploadResponseDetail.StatusDescription = "Invalid Start Date - expected MM/DD/YY format."
            Else
                'Verify EndDate is a date
                If Not DV.ValidateIsDate("EndDate", EndDate) Then
                    bGoodCredentials = False
                    fileUploadResponseDetail.StatusCode = "25"
                    fileUploadResponseDetail.StatusDescription = "Invalid End Date - expected MM/DD/YY format."
                End If
            End If
        End If

        'Format the XML response string
        Dim TheReturn As String

        Dim memory_stream As New MemoryStream
        Dim xml_text_writer As New XmlTextWriter(memory_stream, System.Text.Encoding.UTF8)

        ' Use indentation to make the result look nice.
        xml_text_writer.Formatting = Formatting.Indented
        xml_text_writer.Indentation = 4

        ' Write the XML declaration.
        xml_text_writer.WriteStartDocument(True)
        xml_text_writer.WriteStartElement("EFXTransactionStatusesForDateRange")

        If bGoodCredentials Then

            Dim MyTL As New TransactionList

            xml_text_writer.WriteStartElement("StatusDetail")
            AddElement(xml_text_writer, "StatusCode", "0")
            AddElement(xml_text_writer, "StatusDescription", "Success")
            xml_text_writer.WriteEndElement()

            xml_text_writer.WriteStartElement("TransactionCollectionCriteria")
            AddElement(xml_text_writer, "ClientID", ACHClientID)
            AddElement(xml_text_writer, "StartDate", CDate(StartDate).ToShortDateString)
            AddElement(xml_text_writer, "EndDate", CDate(EndDate).ToShortDateString)

            'Populate list of transactions for file
            MyTL = MyTL.GetTransactionListByDateRange(ACHClientIDGuid.ToString, CDate(StartDate), CDate(EndDate))

            If Not MyTL Is Nothing Then
                AddElement(xml_text_writer, "CollectionCount", MyTL.Count.ToString)
            Else
                AddElement(xml_text_writer, "CollectionCount", "0")
            End If
            xml_text_writer.WriteEndElement()

            If Not MyTL Is Nothing Then
                If MyTL.Count > 0 Then

                    xml_text_writer.WriteStartElement("TransactionStatusCollection")
                    Dim MyTransaction As ACH_Objects.Transaction
                    For Each MyTransaction In MyTL
                        xml_text_writer.WriteStartElement("StatusDetail")
                        'AddElement(xml_text_writer, "TransactionID", MyTransaction.InternalTransactionID.ToString)
                        AddElement(xml_text_writer, "TransactionID", MyTransaction.MerchantTransactionID)
                        AddElement(xml_text_writer, "StatusCode", MyTransaction.TransactionStatus.ToString)
                        Dim StatusDescription As String = ""
                        Select Case MyTransaction.TransactionStatus
                            Case 1
                                StatusDescription = "Pending"
                            Case 2
                                StatusDescription = "Working"
                            Case 3
                                StatusDescription = "In Process"
                            Case 4
                                StatusDescription = "Cleared"
                            Case 5
                                StatusDescription = "Rejected"
                            Case 6
                                StatusDescription = "Returned"
                            Case 7
                                StatusDescription = "Chargedback"
                            Case 8
                                StatusDescription = "Cancelled"
                            Case Else
                                StatusDescription = "Unknown"
                        End Select
                        AddElement(xml_text_writer, "StatusDescription", StatusDescription)
                        Dim TransactionType As String = ""
                        Select Case MyTransaction.TransactionType
                            Case 1
                                TransactionType = "Credit"
                            Case 2
                                TransactionType = "Debit"
                        End Select
                        AddElement(xml_text_writer, "TransactionType", TransactionType)
                        AddElement(xml_text_writer, "TransactionAmount", MyTransaction.TransactionAmount.ToString)
                        AddElement(xml_text_writer, "LastStatusChangeTime", MyTransaction.StatusChangeDate.ToShortDateString & " " & MyTransaction.StatusChangeDate.ToShortTimeString)
                        AddElement(xml_text_writer, "ReturnCode", MyTransaction.ReturnReasonCode)
                        Dim ReasonDescription As String = ""
                        If MyTransaction.ReturnReasonCode <> "" And MyTransaction.ReturnID <> Guid.Empty Then
                            Dim ROL As New ReturnObjectList
                            Dim RO As ReturnObject = ROL.GetReturnObject(MyTransaction.ReturnID)
                            ReasonDescription = RO.ReturnReasonDescription
                        End If
                        AddElement(xml_text_writer, "ReasonDescription", ReasonDescription)
                        xml_text_writer.WriteEndElement()
                    Next
                    xml_text_writer.WriteEndElement()
                End If
            End If
        Else
            xml_text_writer.WriteStartElement("StatusDetail")
            AddElement(xml_text_writer, "StatusCode", fileUploadResponseDetail.StatusCode)
            AddElement(xml_text_writer, "StatusDescription", fileUploadResponseDetail.StatusDescription)
            xml_text_writer.WriteEndElement()
        End If

        xml_text_writer.WriteEndElement()

        ' End the document.
        xml_text_writer.WriteEndDocument()
        xml_text_writer.Flush()

        ' Use a StreamReader to get the result.
        Dim stream_reader As New StreamReader(memory_stream)
        memory_stream.Seek(0, SeekOrigin.Begin)
        TheReturn = stream_reader.ReadToEnd()

        ' Close the XmlTextWriter.
        xml_text_writer.Close()

        Return TheReturn

    End Function

    Function RPO_CancelPendingACHTransaction(ByVal UserID As String, ByVal Password As String, _
        ByVal TransactionID As String) As String

        WriteLog(UserID, "RPO_CancelPendingACHTransaction method entered.")

        Dim bGoodCredentials As Boolean = True

        Dim ETS As EFXTransactionStatusExtended
        ETS = New EFXTransactionStatusExtended
        ETS.LastStatusChangeTime = Now.ToShortDateString & " " & Now.ToShortTimeString

        If Not ValidateUserAndPassword(UserID, Password) Then
            ETS.StatusCode = "20"
            ETS.StatusDescription = "Authentication failure."
            bGoodCredentials = False
        End If

        Dim MyMerchant As Merchant
        Dim MyMerchants As New MerchantList

        If bGoodCredentials Then

            ETS.StatusCode = "21"
            ETS.StatusDescription = "Invalid Transaction ID."

            Dim MyTL As New TransactionList

            'See if the TransactionID is a GUID
            Dim TransactionIDGuid As Guid = Guid.Empty
            Try
                TransactionIDGuid = New Guid(TransactionID)
            Catch ex As Exception
            End Try

            Dim MyTransaction As New ACH_Objects.Transaction
            If TransactionIDGuid = Guid.Empty Then
                If IsNumeric(TransactionID) Then
                    MyTransaction = MyTL.GetTransactionByMerchantTransactionId(TransactionID)
                Else
                    bGoodCredentials = False
                End If
            Else
                MyTransaction = MyTL.GetTransaction(TransactionIDGuid)
            End If

            'Set the return TransactionID value initially to the passed in value, string or GUID format - whatever was passed in
            'If the transaction is valid, it will be overridden later in this logic with the actual internal GUID value
            ETS.TransactionID = TransactionID

            If Not MyTransaction Is Nothing And bGoodCredentials Then

                If MyTransaction.MerchantTransactionID <> "" Then

                    'Verify user has access to transaction
                    Dim MerchantID As Guid = MyTransaction.InternalMerchantID
                    MyMerchant = MyMerchants.GetMerchant(MerchantID)

                    'If UCase(MyMerchant.WebUser.ASPMembership.UserName) = UCase(UserID) Then ' User has access

                    If MyTransaction.TransactionStatus = 1 Then
                        'Transaction is pending, so it is ok to Cancel
                        MyTransaction.TransactionStatus = 8 'Cancelled
                        MyTransaction.StatusChangeDate = Now
                        If Not MyTL.UpdateTransaction(MyTransaction) Then
                            ETS.StatusCode = "22"
                            ETS.StatusDescription = "System error - unable to cancel transaction."
                            bGoodCredentials = False
                        Else
                            ETS.StatusCode = "0"
                            ETS.StatusDescription = "Success"
                            WriteLog(UserID, "RPO_CancelPendingACHTransaction - transaction cancelled - transaction id: " & ETS.TransactionID)
                        End If
                    Else
                        'Transaction is not available to be cancelled
                        ETS.StatusCode = "23"
                        ETS.StatusDescription = "Transaction not available for cancellation."
                        bGoodCredentials = False
                    End If
                    'End If
                End If
            End If
        End If


        'Format the XML response string
        Dim TheReturn As String

        Dim memory_stream As New MemoryStream
        Dim xml_text_writer As New XmlTextWriter(memory_stream, System.Text.Encoding.UTF8)

        ' Use indentation to make the result look nice.
        xml_text_writer.Formatting = Formatting.Indented
        xml_text_writer.Indentation = 4

        ' Write the XML declaration.
        xml_text_writer.WriteStartDocument(True)

        xml_text_writer.WriteStartElement("EFXCancelTransaction")

        xml_text_writer.WriteStartElement("StatusDetail")
        AddElement(xml_text_writer, "TransactionID", ETS.TransactionID)
        AddElement(xml_text_writer, "StatusCode", ETS.StatusCode)
        AddElement(xml_text_writer, "StatusDescription", ETS.StatusDescription)
        xml_text_writer.WriteEndElement()

        xml_text_writer.WriteEndElement()

        ' End the document.
        xml_text_writer.WriteEndDocument()
        xml_text_writer.Flush()

        ' Use a StreamReader to get the result.
        Dim stream_reader As New StreamReader(memory_stream)
        memory_stream.Seek(0, SeekOrigin.Begin)
        TheReturn = stream_reader.ReadToEnd()

        ' Close the XmlTextWriter.
        xml_text_writer.Close()


        Return TheReturn


    End Function

    'Salcedo - 3/31/2015 - These properties and functions were pulled in here from EFXFramework to avoid a circular reference to EFXFramework, since it references this project
    Private ReadOnly Property ConnectionString() As String
        Get
            Return ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        End Get
    End Property
    Private Function GetKeyValue(key As String) As String
        Dim con As New SqlConnection(ConnectionString)
        Dim com As New SqlCommand("SELECT SettingValue from SystemSetting Where SettingKey = '" & key & "'", con)
        com.CommandType = CommandType.Text
        Dim ReturnValue As String
        Try
            con.Open()
            ReturnValue = com.ExecuteScalar()
            con.Close()
            Return ReturnValue
        Catch ex As Exception
            con.Close()
            Throw ex
        End Try
    End Function
    Private ReadOnly Property DuplicateTransactionHourWindow() As String
        Get
            Return GetKeyValue("DuplicateTransactionHourWindow")
        End Get
    End Property
    Private ReadOnly Property DefaultClearingDays() As String
        Get
            Return GetKeyValue("DefaultClearingDays")
        End Get
    End Property
    Private ReadOnly Property DefaultACHProcessingBankName() As String
        Get
            Return GetKeyValue("DefaultACHProcessingBankName")
        End Get
    End Property
    Private ReadOnly Property ACHAcquireConnectionString() As String
        Get
            Return GetKeyValue("ACHAcquireConnectionString")
        End Get
    End Property

End Class

Public Class DataValidation

    Private _cErrorDescription As String

    Property ErrorDescription As String
        Get
            Return _cErrorDescription
        End Get
        Set(value As String)
            _cErrorDescription = value
        End Set
    End Property

    Public Function ValidateLength(ByVal FieldName As String, ByVal FieldValue As String, _
        ByVal MinLength As Short, ByVal MaxLength As Short, ByVal Required As Boolean) As Boolean

        If Required Then
            If FieldValue.Length < MinLength Or FieldValue.Length > MaxLength Then
                _cErrorDescription = "The required " & FieldName & " field must be between " & MinLength.ToString & " and " & _
                    MaxLength.ToString & " digits in length."
                Return False
            End If
        End If

        If Not Required And FieldValue.Length > 0 Then
            If FieldValue.Length < MinLength Or FieldValue.Length > MaxLength Then
                _cErrorDescription = "The optional " & FieldName & " field must be between " & MinLength.ToString & " and " & _
                    MaxLength.ToString & " digits in length."
                Return False
            End If
        End If

        Return True
    End Function

    Public Function ValidateAllNumeric(ByVal FieldName As String, ByVal FieldValue As String) As Boolean

        Dim i As Short
        For i = 0 To FieldValue.Length - 1
            If Not IsNumeric(FieldValue.Substring(i, 1)) Then
                _cErrorDescription = "The " & FieldName & " field may only contain numeric digits."
                Return False
            End If
        Next

        Return True
    End Function

    Public Function ValidateIsNumeric(ByVal FieldName As String, ByVal FieldValue As String, _
        ByVal MinValue As Double, ByVal MaxValue As Double) As Boolean

        If Not IsNumeric(FieldValue) Then
            _cErrorDescription = "The " & FieldName & " field must be numeric."
            Return False
        End If

        Dim TheNumber As Double
        TheNumber = System.Convert.ToDouble(FieldValue)

        If TheNumber < MinValue Or TheNumber > MaxValue Then
            _cErrorDescription = "The " & FieldName & " field must have a value between " & MinValue.ToString & _
                " and " & MaxValue.ToString & "."
            Return False
        End If

        Return True
    End Function

    Public Function ValidateIsDate(ByVal FieldName As String, ByVal FieldValue As String) As Boolean

        If Not IsDate(FieldValue) Then
            _cErrorDescription = "Please specify a valid date value for the " & FieldName & " field."
            Return False
        End If

        Return True
    End Function

End Class

Public Class AddressStateCode

    Sub New(sName As String, sAbbrev As String)
        _StateName = sName
        _Abbreviation = sAbbrev
    End Sub

    Private _StateName As String
    Public Property StateName() As String
        Get
            Return _StateName
        End Get
        Set(ByVal value As String)
            _StateName = value
        End Set
    End Property

    Private _Abbreviation As String
    Public Property Abbreviation() As String
        Get
            Return _Abbreviation
        End Get
        Set(ByVal value As String)
            _Abbreviation = value
        End Set
    End Property

End Class

Public Class AddressStateCodeList

    Public Sub New()
        _States = New List(Of AddressStateCode)
        FillStates()
    End Sub

    Private _States As List(Of AddressStateCode)
    Public Property States() As List(Of AddressStateCode)
        Get
            If _States Is Nothing Then
                _States = New List(Of AddressStateCode)
            End If

            If _States.Count = 0 Then
                FillStates()
            End If

            Return _States
        End Get
        Set(ByVal value As List(Of AddressStateCode))
            _States = value
        End Set
    End Property

    Private Sub FillStates()
        _States.Add(New AddressStateCode("Alabama", "AL"))
        _States.Add(New AddressStateCode("Alaska", "AK"))
        _States.Add(New AddressStateCode("Arizona", "AZ"))
        _States.Add(New AddressStateCode("Arkansas", "AR"))
        _States.Add(New AddressStateCode("California", "CA"))
        _States.Add(New AddressStateCode("Colorado", "CO"))
        _States.Add(New AddressStateCode("Connecticut", "CT"))
        _States.Add(New AddressStateCode("Delaware", "DE"))
        _States.Add(New AddressStateCode("District Of Columbia", "DC"))
        _States.Add(New AddressStateCode("Florida", "FL"))
        _States.Add(New AddressStateCode("Georgia", "GA"))
        _States.Add(New AddressStateCode("Hawaii", "HI"))
        _States.Add(New AddressStateCode("Idaho", "ID"))
        _States.Add(New AddressStateCode("Illinois", "IL"))
        _States.Add(New AddressStateCode("Indiana", "IN"))
        _States.Add(New AddressStateCode("Iowa", "IA"))
        _States.Add(New AddressStateCode("Kansas", "KS"))
        _States.Add(New AddressStateCode("Kentucky", "KY"))
        _States.Add(New AddressStateCode("Louisiana", "LA"))
        _States.Add(New AddressStateCode("Maine", "ME"))
        _States.Add(New AddressStateCode("Maryland", "MD"))
        _States.Add(New AddressStateCode("Massachusetts", "MA"))
        _States.Add(New AddressStateCode("Michigan", "MI"))
        _States.Add(New AddressStateCode("Minnesota", "MN"))
        _States.Add(New AddressStateCode("Mississippi", "MS"))
        _States.Add(New AddressStateCode("Missouri", "MO"))
        _States.Add(New AddressStateCode("Montana", "MT"))
        _States.Add(New AddressStateCode("Nebraska", "NE"))
        _States.Add(New AddressStateCode("Nevada", "NV"))
        _States.Add(New AddressStateCode("New Hampshire", "NH"))
        _States.Add(New AddressStateCode("New Jersey", "NJ"))
        _States.Add(New AddressStateCode("New Mexico", "NM"))
        _States.Add(New AddressStateCode("New York", "NY"))
        _States.Add(New AddressStateCode("North Carolina", "NC"))
        _States.Add(New AddressStateCode("North Dakota", "ND"))
        _States.Add(New AddressStateCode("Ohio", "OH"))
        _States.Add(New AddressStateCode("Oklahoma", "OK"))
        _States.Add(New AddressStateCode("Oregon", "OR"))
        _States.Add(New AddressStateCode("Pennsylvania", "PA"))
        _States.Add(New AddressStateCode("Rhode Island", "RI"))
        _States.Add(New AddressStateCode("South Carolina", "SC"))
        _States.Add(New AddressStateCode("South Dakota", "SD"))
        _States.Add(New AddressStateCode("Tennessee", "TN"))
        _States.Add(New AddressStateCode("Texas", "TX"))
        _States.Add(New AddressStateCode("Utah", "UT"))
        _States.Add(New AddressStateCode("Vermont", "VT"))
        _States.Add(New AddressStateCode("Virginia", "VA"))
        _States.Add(New AddressStateCode("Washington", "WA"))
        _States.Add(New AddressStateCode("West Virginia", "WV"))
        _States.Add(New AddressStateCode("Wisconsin", "WI"))
        _States.Add(New AddressStateCode("Wyoming", "WY"))
    End Sub

    Public Function StateExists(StateCode As String) As Boolean
        Dim TheStateCode As AddressStateCode
        For Each TheStateCode In States
            If UCase(TheStateCode.Abbreviation) = UCase(StateCode) Then
                Return True
            End If
        Next
        Return False
    End Function
End Class
