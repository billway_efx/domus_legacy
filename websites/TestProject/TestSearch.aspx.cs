﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using TestProject.AMSI_ServiceReference1;
using System.Xml;
using System.Text;
using System.Net;
using System.Net.WebRequest;
using System.Net.HttpWebRequest;
using System.Net.Mail;
using RPO.PayNearMe;
using EfxFramework;

namespace RentPaidOnline
{
    public partial class TestSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //<EDEX><propertyid></propertyid><includemarketingsources>1</includemarketingsources><includeamenities>0</includeamenities><includeunittypes>0</includeunittypes><includememo></includememo></EDEX>
     


        //    ////for testing only
        //    RPO.PayNearMe.ConfirmationCallBackContainer container = new ConfirmationCallBackContainer();
        //    container.pnm_order_identifier = "89363028435";
        //    container.pnm_payment_identifier = "184199369346";
        //    container.site_payment_identifier = ""; //"8747";
        //    container.site_order_identifier = "1b0ca7df-1de5-42e8-9ff9-b8890bb696b8";
        //    container.site_customer_identifier = "1290869963";
        //    container.site_order_annotation = "";
        //    container.payment_amount = "646.99";
        //    container.payment_currency = "USD";
        //    container.net_payment_amount = "643.00";
        //    container.net_payment_currency = "USD";
        //    container.pnm_withheld_amount = "3.75";
        //    container.pnm_withheld_currency = "USD";
        //    container.due_to_site_amount = "643.24";
        //    container.due_to_site_currency = "USD";
        //    container.site_processing_fee = "0.24";
        //    container.site_processing_currency = "";
        //    container.pnm_processing_fee = "3.75";
        //    container.pnm_processing_currency = "";
        //    container.status = "payment";

        //    if (container != null || !string.IsNullOrEmpty(container.pnm_order_identifier))
        //    {
        //        //update and retrieve the PayNearMeOrder record
        //        RPO.PayNearMeOrder pnmOrder = RPO.PayNearMeOrder.UpdatePNMOrderForConfirmationCallBack(container);
        //        decimal netPaymentAmount = 0;

        //        if (pnmOrder != null && pnmOrder.Status == "payment" && Decimal.TryParse(container.net_payment_amount, out netPaymentAmount))
        //        {
        //            RecordPayment(netPaymentAmount, new EfxFramework.Renter(pnmOrder.RenterId), "Rent By Cash successfully submitted");
        //            EfxFramework.Lease lease = EfxFramework.Lease.GetRenterLeaseByRenterId(pnmOrder.RenterId);
        //            AdjustCurrentBalanceDue(netPaymentAmount, lease);


        //            //get the property
        //            EfxFramework.Property currentProperty = new EfxFramework.Property(pnmOrder.PropertyId);
        //            if (currentProperty != null)
        //            {

        //                //cakel: BUGID00266 - Check if Property wants to settle by group or individual
        //                if (currentProperty.GroupSettlementCashPaymentFlag == false)
        //                {

        //                }
        //            }
        //        }
        //        else if (pnmOrder != null && pnmOrder.Status == "decline")
        //        {
        //            RecordPayment(netPaymentAmount, new EfxFramework.Renter(pnmOrder.RenterId), "Rent By Cash declined");
        //        }
        //    }
       }






        private static void RecordPayment(decimal amount, EfxFramework.Renter renter, string responseMessage)
        {
            SetPayment(renter, EfxFramework.PaymentMethods.PaymentAmount.GetPaymentAmount(amount, renter, EfxFramework.PaymentMethods.PaymentType.RentByCash, false), "Rent By Cash", EfxFramework.PaymentMethods.PaymentApplication.Rent, responseMessage);
        }

        private static void SetPayment(EfxFramework.Renter renter, EfxFramework.PaymentMethods.PaymentAmount amount, string description, EfxFramework.PaymentMethods.PaymentApplication paymentApplication, string responseMessage)
        {
            if (!renter.PayerId.HasValue)
                renter.PayerId = 0;

            int PaymentId;

            PaymentId = EfxFramework.Payment.Set(EfxFramework.Payment.BuildPayment(renter.RenterId, renter.PayerId.Value, EfxFramework.PaymentMethods.PaymentType.RentByCash, null, null, null, null,
                null, amount, description, null, null, null, null, null, false, null,
                "0", responseMessage, null, EfxFramework.PaymentChannel.Web, EfxFramework.PublicApi.Payment.PaymentStatus.Cleared));
            EfxFramework.Transaction.SetTransactionFromPayment(new EfxFramework.Payment(PaymentId), 0, responseMessage);


        }

        private static void AdjustCurrentBalanceDue(decimal amount, EfxFramework.Lease lease)
        {
            lease.CurrentBalanceDue -= amount;
            lease.CurrentBalanceDueLastUpdated = DateTime.UtcNow;

            if (lease.CurrentBalanceDue <= 0.00M)
                lease.PaymentDueDate = lease.PaymentDueDate.HasValue ? lease.PaymentDueDate.Value.AddMonths(1) : DateTime.UtcNow.AddMonths(1);

            EfxFramework.Lease.Set(lease);
        }

        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

        protected void ServerButton_Click(object sender, EventArgs e)
        {
            gvRenters.DataSource = new SqlDataSource(ConnString, "select top 5 RenterId, FirstName, LastName from Renter");
            gvRenters.DataBind();
            mpe.Show();
        }

        protected void ibtnSelectUser_Click(object sender, EventArgs e)
        {
            GridViewRow gvRow = (GridViewRow)((Button)sender).NamingContainer;
            int RenterId = Convert.ToInt32(gvRenters.DataKeys[gvRow.RowIndex].Value);
            mpe.Hide();

            txtRenterId.Text = RenterId.ToString();

            SqlConnection con = new SqlConnection(ConnString);
            string _sql = "Select FirstName, LastName from Renter where renterid = " + txtRenterId.Text;

            con.Open();
            SqlDataAdapter SqlAdapt = new SqlDataAdapter(_sql, con);
            DataSet ds = new DataSet();
            SqlAdapt.Fill(ds, "MyData");

            txtFirstName.Text = (string)ds.Tables[0].Rows[0]["FirstName"];
            txtLastName.Text = (string)ds.Tables[0].Rows[0]["LastName"];

        }

        protected void testbtn_Click(object sender, EventArgs e)
        {
            TestProject.AMSI_ServiceReference1.LeasingSoapClient Test = new TestProject.AMSI_ServiceReference1.LeasingSoapClient();
            string _new = Test.GetPropertyList("rentpaidonline", "rentpaid", "RentPaidOnline", "").ToString();
            string _new2 = _new;
        }
 
    }
}
