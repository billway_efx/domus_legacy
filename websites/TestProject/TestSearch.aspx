﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestSearch.aspx.cs" Inherits="RentPaidOnline.TestSearch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">

        <title>AJAX Control Toolkit</title>

        <script type="text/javascript">
            var launch = false;
            function launchModal() {
                launch = true;
            }

            //function pageLoad() {
            //    if (launch) {
            //        $find("mpe").show();
            //    }
            //}
        </script>

        <style>
            .modalBackground {
                height:100%;
                background-color:#EBEBEB;
                filter:alpha(opacity=70);
                opacity:0.7;
            }
            .modalPanel {
                background-color:white;
            }
        </style>

    </head>

    <body>
        <form id="form1" runat="server">
    
            <div id="divOpenner" runat="server" />
            <div>
                <%--<asp:Button ID="ClientButton" runat="server" Text="Launch Modal Popup (Client)"  />--%>
                <asp:Button ID="ServerButton" runat="server" Text="Search For Renter" OnClick="ServerButton_Click" />
                <br />
                <br />

                <div>
                    <asp:Label Text="Selected Renter Id: " runat="server" ID="Label1"></asp:Label>
                    <asp:TextBox Text="[RenterId]" ID="txtRenterId" runat="server"></asp:TextBox><br />

                    <asp:Label Text="First Name: " runat="server" ID="Label2"></asp:Label>
                    <asp:TextBox Text="[txtFirstName]" ID="txtFirstName" runat="server"></asp:TextBox><br />

                    <asp:Label Text="Last Name: " runat="server" ID="Label3"></asp:Label>
                    <asp:TextBox Text="[txtLastName]" ID="txtLastName" runat="server"></asp:TextBox><br />

                </div>

            </div>

            <asp:Panel ID="ModalPanel" runat="server" Width="500px" CssClass="modalPanel">
                Select a Renter<br />

                <div style="margin-top:12px;">
                    <asp:GridView ID="gvRenters" runat="server" Width="100%" 
                        AutoGenerateColumns="False"
                        GridLines="None"  
                        PageSize="50"  
                        DataKeyNames="RenterID">
                        <Columns>
                            <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                            <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                            <asp:TemplateField HeaderText="Select">
                                <ItemTemplate>
                                    <asp:Button ID="ibtnSelectUser" runat="server" Text="Select"
                                        ToolTip="Select" 
                                        OnClick="ibtnSelectUser_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        </asp:GridView>

                </div>

                <asp:Button ID="OKButton" runat="server" Text="Close" />
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpe" runat="server"
               TargetControlId="divOpenner" PopupControlID="ModalPanel" OkControlID="OKButton" BackgroundCssClass="modalBackground" />
            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></ajaxToolkit:ToolkitScriptManager>

            <div>
                <asp:Button ID="testbtn" runat="server" Text="Button" OnClick="Button1_Click" />
            </div>


        </form>
    </body>

</html>

