﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RPO;
using System.Linq;

namespace RPOServices.Controllers
{
    public class CompanyController : ApiController
    {
        // GET api/values
        public List<Company> Index()
        {
            using (var entity = new RPOEntities())
            {
                return entity.Companies.ToList();
            }
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }
    }
}
