Imports System.ComponentModel
Imports System.Configuration.Install

' Installer class used to create two event sources for the 
' Exception Management Application Block to function correctly.

<RunInstaller(True)> Public Class ExceptionManagerInstaller
    Inherits System.Configuration.Install.Installer

    Private exceptionManagerEventLogInstaller As System.Diagnostics.EventLogInstaller
    Private exceptionManagementEventLogInstaller As System.Diagnostics.EventLogInstaller
    Private dmExceptionManagerEventLogInstaller As System.Diagnostics.EventLogInstaller
    Private pccaExceptionManagerEventLogInstaller As System.Diagnostics.EventLogInstaller
    Private epacExceptionManagerEventLogInstaller As System.Diagnostics.EventLogInstaller
    Private pccaProcessorExceptionManagerEventLogInstaller As System.Diagnostics.EventLogInstaller
    Private pccaRepCodesExceptionManagerEventLogInstaller As System.Diagnostics.EventLogInstaller
    Private bpiExceptionManagerEventLogInstaller As System.Diagnostics.EventLogInstaller
    Private Shared m_resourceManager As ResourceManager = New ResourceManager(GetType(ExceptionManager).Namespace + ".ExceptionManagerText", [Assembly].GetAssembly(GetType(ExceptionManager)))

    'Constructor with no params.
    Public Sub New()
        MyBase.New()

        'Initialize variables.
        InitializeComponent()

    End Sub

    'Installer overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    ' Initialization function to set internal variables.
    Private Sub InitializeComponent()

        Me.exceptionManagerEventLogInstaller = New System.Diagnostics.EventLogInstaller()
        Me.exceptionManagementEventLogInstaller = New System.Diagnostics.EventLogInstaller()
        Me.dmExceptionManagerEventLogInstaller = New System.Diagnostics.EventLogInstaller()
        Me.pccaExceptionManagerEventLogInstaller = New System.Diagnostics.EventLogInstaller
        Me.epacExceptionManagerEventLogInstaller = New System.Diagnostics.EventLogInstaller
        Me.pccaProcessorExceptionManagerEventLogInstaller = New System.Diagnostics.EventLogInstaller
        Me.pccaRepCodesExceptionManagerEventLogInstaller = New System.Diagnostics.EventLogInstaller
        Me.bpiExceptionManagerEventLogInstaller = New System.Diagnostics.EventLogInstaller

        ' exceptionManagerEventLogInstaller
        Me.exceptionManagerEventLogInstaller.Log = "Application"
        Me.exceptionManagerEventLogInstaller.Source = m_resourceManager.GetString("RES_EXCEPTIONMANAGER_INTERNAL_EXCEPTIONS")

        ' exceptionManagementEventLogInstaller
        Me.exceptionManagementEventLogInstaller.Log = "Application"
        Me.exceptionManagementEventLogInstaller.Source = m_resourceManager.GetString("RES_EXCEPTIONMANAGER_PUBLISHED_EXCEPTIONS")

        ' dmnetExceptionManagerEventLogInstaller
        Me.dmExceptionManagerEventLogInstaller.Log = "DM Log"
        Me.dmExceptionManagerEventLogInstaller.Source = "DM Web"

        ' pccanetExceptionManagerEventLogInstaller
        Me.pccaExceptionManagerEventLogInstaller.Log = "PCCA Log"
        Me.pccaExceptionManagerEventLogInstaller.Source = "PCCA Web"

        ' epacExceptionManagerEventLogInstaller
        Me.epacExceptionManagerEventLogInstaller.Log = "ePac Log"
        Me.epacExceptionManagerEventLogInstaller.Source = "ePac Web"

        Me.pccaProcessorExceptionManagerEventLogInstaller.Log = "PCCAProcessor Log"
        Me.pccaProcessorExceptionManagerEventLogInstaller.Source = "PCCAProcessor"

        Me.pccaRepCodesExceptionManagerEventLogInstaller.Log = "PCCARepCodes Log"
        Me.pccaRepCodesExceptionManagerEventLogInstaller.Source = "PCCARepCodes"

        Me.bpiExceptionManagerEventLogInstaller.Log = "BPI Log"
        Me.bpiExceptionManagerEventLogInstaller.Source = "BPI"

        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.exceptionManagerEventLogInstaller, _
                                                                             Me.exceptionManagementEventLogInstaller, _
                                                                             Me.dmExceptionManagerEventLogInstaller, _
                                                                             Me.pccaExceptionManagerEventLogInstaller, _
                                                                             Me.epacExceptionManagerEventLogInstaller, _
                                                                             Me.pccaProcessorExceptionManagerEventLogInstaller, _
                                                                             Me.pccaRepCodesExceptionManagerEventLogInstaller, _
                                                                             Me.bpiExceptionManagerEventLogInstaller})

    End Sub

End Class
