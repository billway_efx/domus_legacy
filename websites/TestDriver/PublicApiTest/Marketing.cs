﻿namespace TestDriver.PublicApiTest
{
    public class Marketing
    {
        public static MarketingServiceTest.AddPropertyLeadResponse1 AddLead()
        {
            using (var Client = new MarketingServiceTest.MarketingServiceClient())
            {
                return Client.AddPropertyLead(new MarketingServiceTest.AddPropertyLeadRequest1
                                                  {
                                                      request = new MarketingServiceTest.AddPropertyLeadRequest
                                                                    {
                                                                        Username = "TestUser",
                                                                        Password = "Test1234",
                                                                        PropertyManagementCompanyName = "Evil Empire",
                                                                        PropertyName = "Wild Space",
                                                                        Address1 = "123 Any St",
                                                                        Address2 = "Suite A",
                                                                        City = "Tampa",
                                                                        State = "FL",
                                                                        PostalCode = "33647",
                                                                        ContactFirstName = "Brian",
                                                                        ContactLastName = "Madigan",
                                                                        EmailAddress = "brian.madigan@mb2x.com",
                                                                        FaxNumber = "123-555-1313",
                                                                        TelephoneNumber = "123-456-7890"
                                                                    }
                                                  });
            }
        }
    }
}
