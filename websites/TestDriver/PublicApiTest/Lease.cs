﻿using System;

namespace TestDriver.PublicApiTest
{
    public static class Lease
    {
        public static LeaseServiceTest.Lease ProcessLease(int propertyId, int propertyStaffId, long residentId)
        {
            var LeaseId = CreateLease(propertyId, propertyStaffId, residentId);
            var Lease = GetLease(LeaseId);

            Lease.NumberOfTenants = 2;

            LeaseId = UpdateLease(Lease, residentId);
            Lease = GetLease(LeaseId);
            return GetLease(residentId);
        }

        public static LeaseServiceTest.DeleteLeaseResponse DeleteLease(int leaseId)
        {
            using (var Client = new LeaseServiceTest.LeaseServiceClient())
            {
                return Client.DeleteLease(new LeaseServiceTest.DeleteLeaseRequest1 {request = new LeaseServiceTest.DeleteLeaseRequest {Username = "TestUser", Password = "Test1234", LeaseId = leaseId}}).DeleteLeaseResult;
            }
        }

        private static int CreateLease(int propertyId, int propertyStaffId, long residentId)
        {
            var Lease = new LeaseServiceTest.Lease
                {
                    PropertyId = propertyId,
                    PropertyStaffId = propertyStaffId,
                    UnitNumber = "5",
                    RentAmount = 500.00M,
                    RentDueDayOfTheMonth = 1,
                    NumberOfTenants = 1,
                    StartDate = DateTime.UtcNow,
                    EndDate = DateTime.UtcNow.AddYears(1),
                    BeginningBalance = 10.00M,
                    BeginningBalanceDate = DateTime.UtcNow,
                    ExpectedMoveInDate = DateTime.UtcNow,
                    ExpectedMoveOutDate = DateTime.UtcNow.AddYears(1),
                    ActualMoveInDate = DateTime.UtcNow,
                    ActualMoveOutDate = DateTime.UtcNow.AddYears(1),
                    LeaseSignDate = DateTime.UtcNow
                };

            using (var Client = new LeaseServiceTest.LeaseServiceClient())
            {
                return Client.CreateLease(new LeaseServiceTest.CreateLeaseRequest1 {request = new LeaseServiceTest.CreateLeaseRequest {Username = "TestUser", Password = "Test1234", Lease = Lease, PublicResidentId = residentId}}).CreateLeaseResult.LeaseId;
            }
        }

        private static int UpdateLease(LeaseServiceTest.Lease lease, long residentId)
        {
            using (var Client = new LeaseServiceTest.LeaseServiceClient())
            {
                return Client.UpdateLease(new LeaseServiceTest.UpdateLeaseRequest1 {request = new LeaseServiceTest.UpdateLeaseRequest {Username = "TestUser", Password = "Test1234", Lease = lease, PublicResidentId = residentId}}).UpdateLeaseResult.LeaseId;
            }
        }

        public static LeaseServiceTest.Lease GetLease(int leaseId)
        {
            using (var Client = new LeaseServiceTest.LeaseServiceClient())
            {
                return Client.GetLease(new LeaseServiceTest.GetLeaseRequest1 {request = new LeaseServiceTest.GetLeaseRequest {Username = "TestUser", Password = "Test1234", LeaseId = leaseId}}).GetLeaseResult.Lease;
            }
        }

        private static LeaseServiceTest.Lease GetLease(long residentId)
        {
            using (var Client = new LeaseServiceTest.LeaseServiceClient())
            {
                return Client.GetLease(new LeaseServiceTest.GetLeaseRequest1 { request = new LeaseServiceTest.GetLeaseRequest { Username = "TestUser", Password = "Test1234", PublicResidentId = residentId } }).GetLeaseResult.Lease;
            }
        }
    }
}
