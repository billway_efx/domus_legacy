﻿namespace TestDriver.PublicApiTest
{
    public static class Property
    {
        public static PropertyServiceTest.Property ProcessProperty()
        {
            var PropertyId = CreateProperty();
            var Property = GetProperty(PropertyId);

            Property.PropertyName = "Service Test Property 2";

            PropertyId = UpdateProperty(Property);
            Property = GetProperty(PropertyId);
            return GetProperty(Property.PropertyName);
        }

        public static PropertyServiceTest.DeletePropertyResponse DeleteProperty(int propertyId)
        {
            using (var Client = new PropertyServiceTest.PropertyServiceClient())
            {
                return Client.DeleteProperty(new PropertyServiceTest.DeletePropertyRequest1 {request = new PropertyServiceTest.DeletePropertyRequest {Username = "TestUser", Password = "Test1234", PropertyId = propertyId}}).DeletePropertyResult;
            }
        }

        private static int CreateProperty()
        {
            var Property = new PropertyServiceTest.Property
                {
                    PropertyName = "Service Test Property 1",
                    PropertyCode = "1",
                    NumberOfUnits = 10,
                    StreetAddress = "123 Any St",
                    StreetAddress2 = "1",
                    City = "Tampa",
                    State = "FL",
                    PostalCode = "33647",
                    EmailAddress = "servicetestproperty@test.com",
                    MainPhoneNumber = "1234567890"
                };

            using (var Client = new PropertyServiceTest.PropertyServiceClient())
            {
                return Client.CreateProperty(new PropertyServiceTest.CreatePropertyRequest1 {request = new PropertyServiceTest.CreatePropertyRequest {Username = "TestUser", Password = "Test1234", Property = Property}}).CreatePropertyResult.PropertyId;
            }
        }

        private static int UpdateProperty(PropertyServiceTest.Property property)
        {
            using (var Client = new PropertyServiceTest.PropertyServiceClient())
            {
                return Client.UpdateProperty(new PropertyServiceTest.UpdatePropertyRequest1 {request = new PropertyServiceTest.UpdatePropertyRequest {Username = "TestUser", Password = "Test1234", Property = property}}).UpdatePropertyResult.PropertyId;
            }
        }

        private static PropertyServiceTest.Property GetProperty(int propertyId)
        {
            using (var Client = new PropertyServiceTest.PropertyServiceClient())
            {
                return Client.GetProperty(new PropertyServiceTest.GetPropertyRequest1 {request = new PropertyServiceTest.GetPropertyRequest {Username = "TestUser", Password = "Test1234", PropertyId = propertyId}}).GetPropertyResult.Property;
            }
        }

        private static PropertyServiceTest.Property GetProperty(string propertyName)
        {
            using (var Client = new PropertyServiceTest.PropertyServiceClient())
            {
                return Client.GetProperty(new PropertyServiceTest.GetPropertyRequest1 { request = new PropertyServiceTest.GetPropertyRequest { Username = "TestUser", Password = "Test1234", PropertyName = propertyName } }).GetPropertyResult.Property;
            }
        }

        public static PropertyServiceTest.GetAllRentReportersPropertiesResponse GetRentReportersProperties()
        {
            using (var Client = new PropertyServiceTest.PropertyServiceClient())
            {
                return Client.GetAllRentReportersProperties(new PropertyServiceTest.GetAllRentReportersPropertiesRequest
                                                                {
                                                                    request = new PropertyServiceTest.GetRentReportersPropertiesRequest
                                                                                  {
                                                                                      Username = "TestUser",
                                                                                      Password = "Test1234",
                                                                                      StartDate = null,
                                                                                      EndDate = null
                                                                                  }
                                                                });
            }
        }
    }
}
