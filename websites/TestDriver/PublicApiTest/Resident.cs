﻿namespace TestDriver.PublicApiTest
{
    public static class Resident
    {
        public static ResidentServiceTest.Resident ProcessResident(int propertyId)
        {
            var ResidentId = CreateResident(propertyId);
            var Resident = GetResident(ResidentId);

            Resident.LastName = "Test 2";

            ResidentId = UpdateResident(Resident, propertyId);
            Resident = GetResident(ResidentId);
            return GetResident(Resident.PrimaryEmailAddress);
        }

        public static ResidentServiceTest.DeleteResidentResponse DeleteResident(long residentId)
        {
            using (var Client = new ResidentServiceTest.ResidentServiceClient())
            {
                return Client.DeleteResident(new ResidentServiceTest.DeleteResidentRequest1 {request = new ResidentServiceTest.DeleteResidentRequest {Username = "TestUser", Password = "Test1234", PublicResidentId = residentId}}).DeleteResidentResult;
            }
        }

        private static long CreateResident(int propertyId)
        {
            var Resident = new ResidentServiceTest.Resident
                {
                    Prefix = "Dr",
                    FirstName = "Resident",
                    MiddleName = "Service",
                    LastName = "Test",
                    Suffix = "1",
                    Password = "123",
                    MainPhoneNumber = "1234567890",
                    MobilePhoneNumber = "1234567891",
                    PrimaryEmailAddress = "residentservicetest1@test.com",
                    StreetAddress = "123 Any St",
                    StreetAddress2 = "200",
                    City = "Tampa",
                    State = "FL",
                    PostalCode = "33647"
                };

            using (var Client = new ResidentServiceTest.ResidentServiceClient())
            {
                return Client.CreateResident(new ResidentServiceTest.CreateResidentRequest1 {request = new ResidentServiceTest.CreateResidentRequest {Username = "TestUser", Password = "Test1234", Resident = Resident, PropertyId = propertyId}}).CreateResidentResult.PublicResidentId;
            }
        }

        private static long UpdateResident(ResidentServiceTest.Resident resident, int propertyId)
        {
            using (var Client = new ResidentServiceTest.ResidentServiceClient())
            {
                return Client.UpdateResident(new ResidentServiceTest.UpdateResidentRequest1 {request = new ResidentServiceTest.UpdateResidentRequest {Username = "TestUser", Password = "Test1234", Resident = resident, PropertyId = propertyId}}).UpdateResidentResult.PublicResidentId;
            }
        }

        private static ResidentServiceTest.Resident GetResident(long residentId)
        {
            using (var Client = new ResidentServiceTest.ResidentServiceClient())
            {
                return Client.GetResident(new ResidentServiceTest.GetResidentRequest1 {request = new ResidentServiceTest.GetResidentRequest {Username = "TestUser", Password = "Test1234", PublicResidentId = residentId}}).GetResidentResult.Resident;
            }
        }

        private static ResidentServiceTest.Resident GetResident(string emailAddress)
        {
            using (var Client = new ResidentServiceTest.ResidentServiceClient())
            {
                return Client.GetResident(new ResidentServiceTest.GetResidentRequest1 { request = new ResidentServiceTest.GetResidentRequest { Username = "TestUser", Password = "Test1234", EmailAddress = emailAddress } }).GetResidentResult.Resident;
            }
        }
    }
}
