﻿namespace TestDriver.PublicApiTest
{
    public class Transaction
    {
        public static TransactionServiceTest.GetTransactionHistoryResponse1 GetTransactionHistory()
        {
            using (var Client = new TransactionServiceTest.TransactionServiceClient())
            {
                return Client.GetTransactionHistory(new TransactionServiceTest.GetTransactionHistoryRequest1
                                                        {
                                                            request = new TransactionServiceTest.GetTransactionHistoryRequest
                                                                          {
                                                                              Username = "TestUser",
                                                                              Password = "Test1234",
                                                                              StartDate = null,
                                                                              EndDate = null
                                                                          }
                                                        });
            }
        }
    }
}
