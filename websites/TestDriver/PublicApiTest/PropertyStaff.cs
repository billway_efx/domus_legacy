﻿namespace TestDriver.PublicApiTest
{
    public static class PropertyStaff
    {
        public static PropertyStaffServiceTest.PropertyStaff ProcessPropertyStaff(int propertyId)
        {
            var PropertyStaffId = CreatePropertyStaff(propertyId);
            var Staff = GetPropertyStaff(PropertyStaffId);

            Staff.LastName = "Staff 2";

            PropertyStaffId = UpdatePropertyStaff(Staff, propertyId);
            Staff = GetPropertyStaff(PropertyStaffId);
            return GetPropertyStaff(Staff.EmailAddress);
        }

        public static PropertyStaffServiceTest.DeletePropertyStaffResponse DeletePropertyStaff(int propertyStaffId)
        {
            using (var Client = new PropertyStaffServiceTest.PropertyStaffServiceClient())
            {
                return Client.DeletePropertyStaff(new PropertyStaffServiceTest.DeletePropertyStaffRequest1 {request = new PropertyStaffServiceTest.DeletePropertyStaffRequest {Username = "TestUser", Password = "Test1234", PropertyStaffId = propertyStaffId}}).DeletePropertyStaffResult;
            }
        }

        private static int CreatePropertyStaff(int propertyId)
        {
            var Staff = new PropertyStaffServiceTest.PropertyStaff
                {
                    FirstName = "Service",
                    MiddleName = "Test",
                    LastName = "Staff 1",
                    Password = "123",
                    StreetAddress = "123 Any St",
                    StreetAddress2 = "#100",
                    City = "Tampa",
                    State = "FL",
                    PostalCode = "33647",
                    EmailAddress = "servicestaff1@test.com",
                    MainPhoneNumber = "1234567890",
                    MobilePhoneNumber = "1234567891"
                };

            using (var Client = new PropertyStaffServiceTest.PropertyStaffServiceClient())
            {
                return Client.CreatePropertyStaff(new PropertyStaffServiceTest.CreatePropertyStaffRequest1 {request = new PropertyStaffServiceTest.CreatePropertyStaffRequest {Username = "TestUser", Password = "Test1234", PropertyStaff = Staff, PropertyId = propertyId}}).CreatePropertyStaffResult.PropertyStaffId;
            }
        }

        private static int UpdatePropertyStaff(PropertyStaffServiceTest.PropertyStaff staff, int propertyId)
        {
            using (var Client = new PropertyStaffServiceTest.PropertyStaffServiceClient())
            {
                return Client.UpdatePropertyStaff(new PropertyStaffServiceTest.UpdatePropertyStaffRequest1 {request = new PropertyStaffServiceTest.UpdatePropertyStaffRequest {Username = "TestUser", Password = "Test1234", PropertyStaff = staff, PropertyId = propertyId}}).UpdatePropertyStaffResult.PropertyStaffId;
            }
        }

        private static PropertyStaffServiceTest.PropertyStaff GetPropertyStaff(int propertyStaffId)
        {
            using (var Client = new PropertyStaffServiceTest.PropertyStaffServiceClient())
            {
                return Client.GetPropertyStaff(new PropertyStaffServiceTest.GetPropertyStaffRequest1 {request = new PropertyStaffServiceTest.GetPropertyStaffRequest {Username = "TestUser", Password = "Test1234", PropertyStaffId = propertyStaffId}}).GetPropertyStaffResult.PropertyStaff;
            }
        }

        private static PropertyStaffServiceTest.PropertyStaff GetPropertyStaff(string propertyStaffEmail)
        {
            using (var Client = new PropertyStaffServiceTest.PropertyStaffServiceClient())
            {
                return Client.GetPropertyStaff(new PropertyStaffServiceTest.GetPropertyStaffRequest1 { request = new PropertyStaffServiceTest.GetPropertyStaffRequest { Username = "TestUser", Password = "Test1234", EmailAddress = propertyStaffEmail } }).GetPropertyStaffResult.PropertyStaff;
            }
        }
    }
}
