﻿using System;

namespace TestDriver.PublicApiTest
{
    public class Payment
    {
        public static PaymentServiceTest.ProcessOneTimeCreditCardPaymentResponse ProcessCreditCard()
        {
            using (var Client = new PaymentServiceTest.PaymentServiceClient())
            {
                return Client.ProcessOneTimeCreditCardPayment(new PaymentServiceTest.ProcessOneTimeCreditCardPaymentRequest 
                                                                    {request = new PaymentServiceTest.OneTimeCreditCardPaymentRequest
                                                                                   {
                                                                                       Username = "TestUser",
                                                                                       Password = "Test1234",
                                                                                       Address1 = "123 Any St",
                                                                                       Address2 = "Apt 1",
                                                                                       City = "Tampa",
                                                                                       State = "FL",
                                                                                       PostalCode = "33647",
                                                                                       FirstName = "Oliver",
                                                                                       LastName = "Phillips",
                                                                                       EmailAddress = "brian.madigan@mb2x.com",
                                                                                       NameOnCard = "Oliver Phillips",
                                                                                       IsTestTransaction = true,
                                                                                       PaymentAmount = 350.00M,
                                                                                       ConvenienceFeeAmount = 10.00M,
                                                                                       PaymentCategory = (int) EfxFramework.PublicApi.Payment.PaymentCategory.Rent,
                                                                                       PaymentMemo = "Test Public API",
                                                                                       PropertyId = 1,
                                                                                       PublicResidentId = 5173697720,
                                                                                       TelephoneNumber = "123-456-7890",
                                                                                       UnitNumber = "1",
                                                                                       CreditCardNumber = "4111111111111111",
                                                                                       CreditCardExpiration = new DateTime(2019, 4, 1),
                                                                                       CvvCode = "123"
                                                                                   }
                                                                    });
            }
        }

        public static PaymentServiceTest.ProcessOneTimeAchPaymentResponse ProcessAch()
        {
            using (var Client = new PaymentServiceTest.PaymentServiceClient())
            {
                return Client.ProcessOneTimeAchPayment(new PaymentServiceTest.ProcessOneTimeAchPaymentRequest
                                                           {
                                                               request = new PaymentServiceTest.OneTimeAchPaymentRequest
                                                                             {
                                                                                 Username = "TestUser",
                                                                                 Password = "Test1234",
                                                                                 Address1 = "123 Any St",
                                                                                 Address2 = "Apt 1",
                                                                                 City = "Tampa",
                                                                                 State = "FL",
                                                                                 PostalCode = "33647",
                                                                                 FirstName = "Oliver",
                                                                                 LastName = "Phillips",
                                                                                 EmailAddress = "brian.madigan@mb2x.com",
                                                                                 PaymentAmount = 350.00M,
                                                                                 ConvenienceFeeAmount = 10.00M,
                                                                                 PaymentCategory = (int)EfxFramework.PublicApi.Payment.PaymentCategory.Rent,
                                                                                 PaymentMemo = "Test Public API",
                                                                                 PropertyId = 1,
                                                                                 PublicResidentId = 5173697720,
                                                                                 TelephoneNumber = "123-456-7890",
                                                                                 AccountNumber = "12345677",
                                                                                 RoutingNumber = "072414019",
                                                                                 BankAccountType = 1
                                                                             }
                                                           });
            }
        }

        public static PaymentServiceTest.ProcessRecurringCreditCardPaymentResponse ProcessRecurringCreditCard()
        {
            using (var Client = new PaymentServiceTest.PaymentServiceClient())
            {
                return Client.ProcessRecurringCreditCardPayment(new PaymentServiceTest.ProcessRecurringCreditCardPaymentRequest
                                                                    {
                                                                        request = new PaymentServiceTest.RecurringCreditCardPaymentRequest
                                                                                      {
                                                                                          Username = "TestUser",
                                                                                          Password = "Test1234",
                                                                                          Address1 = "123 Any St",
                                                                                          Address2 = "Apt 1",
                                                                                          City = "Tampa",
                                                                                          State = "FL",
                                                                                          PostalCode = "33647",
                                                                                          FirstName = "Oliver",
                                                                                          LastName = "Phillips",
                                                                                          EmailAddress = "brian.madigan@mb2x.com",
                                                                                          NameOnCard = "Oliver Phillips",
                                                                                          PaymentAmount = 350.00M,
                                                                                          ConvenienceFeeAmount = 10.00M,
                                                                                          PaymentCategory = (int)EfxFramework.PublicApi.Payment.PaymentCategory.Rent,
                                                                                          PaymentMemo = "Test Public API",
                                                                                          PropertyId = 1,
                                                                                          PublicResidentId = 5173697720,
                                                                                          TelephoneNumber = "123-456-7890",
                                                                                          CreditCardNumber = "4111111111111111",
                                                                                          CreditCardExpiration = new DateTime(2019, 4, 1),
                                                                                          StartDate = DateTime.UtcNow.AddMonths(1),
                                                                                          EndDate = DateTime.UtcNow.AddMonths(1).AddYears(1)
                                                                                      }
                                                                    });
            }
        }

        public static PaymentServiceTest.ProcessRecurringAchPaymentResponse ProcessRecurringAch()
        {
            using (var Client = new PaymentServiceTest.PaymentServiceClient())
            {
                return Client.ProcessRecurringAchPayment(new PaymentServiceTest.ProcessRecurringAchPaymentRequest
                                                             {
                                                                 request = new PaymentServiceTest.RecurringAchPaymentRequest
                                                                               {
                                                                                   Username = "TestUser",
                                                                                   Password = "Test1234",
                                                                                   Address1 = "123 Any St",
                                                                                   Address2 = "Apt 1",
                                                                                   City = "Tampa",
                                                                                   State = "FL",
                                                                                   PostalCode = "33647",
                                                                                   FirstName = "Oliver",
                                                                                   LastName = "Phillips",
                                                                                   EmailAddress = "brian.madigan@mb2x.com",
                                                                                   PaymentAmount = 350.00M,
                                                                                   ConvenienceFeeAmount = 10.00M,
                                                                                   PaymentCategory = (int)EfxFramework.PublicApi.Payment.PaymentCategory.Rent,
                                                                                   PaymentMemo = "Test Public API",
                                                                                   PropertyId = 1,
                                                                                   PublicResidentId = 5173697720,
                                                                                   TelephoneNumber = "123-456-7890",
                                                                                   AccountNumber = "12345677",
                                                                                   RoutingNumber = "072414019",
                                                                                   BankAccountType = 1,
                                                                                   StartDate = DateTime.UtcNow.AddMonths(1),
                                                                                   EndDate = DateTime.UtcNow.AddMonths(1).AddYears(1)
                                                                               }
                                                             });
            }
        }

        public static PaymentServiceTest.ProcessReversalResponse ProcessReversal(string transactionId)
        {
            using (var Client = new PaymentServiceTest.PaymentServiceClient())
            {
                return Client.ProcessReversal(new PaymentServiceTest.ProcessReversalRequest
                                                  {
                                                      request = new PaymentServiceTest.ReversalRequest
                                                                    {
                                                                        Username = "TestUser",
                                                                        Password = "Test1234",
                                                                        TransactionId = transactionId,
                                                                        ReversalAmount = 0.00M
                                                                    }
                                                  });
            }
        }
    }
}
