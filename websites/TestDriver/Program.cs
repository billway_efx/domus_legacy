﻿using System.Drawing;
using System.Net.Mail;
using System.Linq;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using EfxFramework;
using EfxFramework.BulkMail;
using EfxFramework.Mobile;
using EfxFramework.PaymentMethods.Ach;
using EfxFramework.Pms.Yardi;
using EfxFramework.Pms.Yardi.Core;
using EfxFramework.PublicApi;
using EfxFramework.Scheduler;
using EfxFramework.Security.Authentication;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using EfxFramework.Web.Facade;
using Mb2x.ExtensionMethods;
using Renter = EfxFramework.Renter;
using EfxFramework.ExtensionMethods;
using Property = EfxFramework.Property;

namespace TestDriver
{
	class Program
	{
	    //private const string BaseUrl = "localhost:3308";
		//private const string BaseUrl = "localhost:49716";
        //private const string BaseUrl = "localhost:49725";
		//private const string BaseUrl = "efxrenter.mb2x.com";
	    //private const string BaseUrl = "renter.rentpaidonline.com";
	    private const string BaseUrl = "localhost:44301"; //USE FOR HTTPS TESTING
	    //private const string BaseUrl = "efxapi.mb2x.com";


		private static void TestPassword()
		{
			var CurrentRenter = new Renter(2);
            CurrentRenter.SetPassword("123");
		    CurrentRenter = new Renter(Renter.Set(CurrentRenter, UIFacade.GetAuthenticatedUserString(UserType.Renter)));
            var Bad = AuthenticationTools.ValidatePasswordHash("1234", CurrentRenter.PasswordHash, CurrentRenter.Salt);
            var Good = AuthenticationTools.ValidatePasswordHash("123", CurrentRenter.PasswordHash, CurrentRenter.Salt);
		}
		private static AchPaymentResponse TestAch()
		{
		    var Property = new EfxFramework.Property(1);
		    var Renter = new Renter(1);

		    if (!Renter.PayerId.HasValue)
		        Renter.PayerId = 0;

		    var PayerAch = EfxFramework.PayerAch.GetPrimaryPayerAchByPayerId(Renter.PayerId.Value);

            var State = "";
            if (Renter.StateProvinceId.HasValue)
                State = ((StateProvince)Enum.Parse(typeof(StateProvince), Renter.StateProvinceId.Value.ToString(CultureInfo.InvariantCulture))).ToString();

            return AchService.ProcessAchPayment(EfxSettings.AchUserId, EfxSettings.AchPassword, Property.RentalAccountAchClientId, .05M, PayerAch.BankRoutingNumber, PayerAch.BankAccountNumber, 
                PayerAch.BankAccountType, Renter.FirstName, Renter.LastName, Renter.StreetAddress, Renter.Unit, Renter.City, State, Renter.PostalCode, Renter.MainPhoneNumber);
		}

		/* BEGIN WCF TESTING*/
		private static AuthenticationResponse TestAuthenticate()
        {
			//Create the login request
            //var Request = new AuthenticationRequest { Username = "Appletest@test.com", Password = "APPLETEST1234" };
            var Request = new AuthenticationRequest { Username = "ana@mb2x.com", Password = "4591a0" };

			//Create the WebClient
			using (var Client = new WebClient())
			{
				//Set the content-type header for the request
				Client.Headers["content-type"] = "application/json";

				using (var InputStream = new MemoryStream())
				{
					//Serialize the input to the MemoryStream
					var Serializer = new DataContractJsonSerializer(typeof(AuthenticationRequest));
					Serializer.WriteObject(InputStream, Request);

					//Post the request
					var RawResponse = Client.UploadData(string.Format("https://{0}/Mobile/MobileApi.svc/Authenticate", BaseUrl), "POST", InputStream.ToArray());

					using (var OutputStream = new MemoryStream(RawResponse))
					{
						//Deserialize and return
						var DeSerializer = new DataContractJsonSerializer(typeof(AuthenticationResponse));
						return (AuthenticationResponse)DeSerializer.ReadObject(OutputStream);
					}
				}
			}
		}

        private static bool IgnoreCertificateErrorHandler(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

		private static PaymentResponse TestSubmitScanCreditCardPayment(string sessionId)
		{
			//Create the payment request
            var Request = new PaymentRequest { PaymentItems = new List<PaymentItem> {new PaymentItem{Amount = .01M, Description = "rent"}}, PaymentMethod = PaymentRequest.PaymentMethodEnum.ScanCreditCard, SessionId = sessionId, CreditCardNumber = "4111111111111111", CvvCode = "123", ExpirationDate = "04/19", NameOnCard = "Yoes Karlsen" };

			//Create the WebClient
            using (var Client = new WebClient())
            {
                //Set the content-type header for the request
                Client.Headers["content-type"] = "application/json";

                using (var InputStream = new MemoryStream())
                {
                    //Serialize the input to the MemoryStream
                    var Serializer = new DataContractJsonSerializer(typeof(PaymentRequest));
                    Serializer.WriteObject(InputStream, Request);

                    var RawResponse = Client.UploadData(string.Format("https://{0}/Mobile/MobileApi.svc/SubmitPayment", BaseUrl), "POST", InputStream.ToArray());

                    using (var OutputStream = new MemoryStream(RawResponse))
                    {
                        //Deserialize and return
                        var DeSerializer = new DataContractJsonSerializer(typeof(PaymentResponse));
                        return (PaymentResponse)DeSerializer.ReadObject(OutputStream);
                    }
                }
            }
		}

        private static PaymentResponse TestSubmitCreditCardOnFilePayment(string sessionId)
        {
            //Create the payment request
            var Request = new PaymentRequest { PaymentItems = new List<PaymentItem> { new PaymentItem { Amount = .01M, Description = "rent" } }, PaymentMethod = PaymentRequest.PaymentMethodEnum.CreditCardOnFile, SessionId = sessionId };

            //Create the WebClient
            using (var Client = new WebClient())
            {
                //Set the content-type header for the request
                Client.Headers["content-type"] = "application/json";

                using (var InputStream = new MemoryStream())
                {
                    //Serialize the input to the MemoryStream
                    var Serializer = new DataContractJsonSerializer(typeof(PaymentRequest));
                    Serializer.WriteObject(InputStream, Request);

                    var RawResponse = Client.UploadData(string.Format("https://{0}/Mobile/MobileApi.svc/SubmitPayment", BaseUrl), "POST", InputStream.ToArray());

                    using (var OutputStream = new MemoryStream(RawResponse))
                    {
                        //Deserialize and return
                        var DeSerializer = new DataContractJsonSerializer(typeof(PaymentResponse));
                        return (PaymentResponse)DeSerializer.ReadObject(OutputStream);
                    }
                }
            }
        }

        private static PaymentResponse TestSubmitEcheckPayment(string sessionId)
        {
            //Create the payment request
            var Request = new PaymentRequest
                {
                    PaymentItems = new List<PaymentItem>
                        {
                            new PaymentItem
                                {
                                    Amount = .01M, 
                                    Description = "rent"
                                },
                            new PaymentItem
                                {
                                    Amount = .79M,
                                    Description = "Convenience Fees"
                                }
                        }, 
                    PaymentMethod = PaymentRequest.PaymentMethodEnum.ECheck, 
                    SessionId = sessionId
                };

            //Create the WebClient
            using (var Client = new WebClient())
            {
                //Set the content-type header for the request
                Client.Headers["content-type"] = "application/json";

                using (var InputStream = new MemoryStream())
                {
                    //Serialize the input to the MemoryStream
                    var Serializer = new DataContractJsonSerializer(typeof(PaymentRequest));
                    Serializer.WriteObject(InputStream, Request);

                    var RawResponse = Client.UploadData(string.Format("https://{0}/Mobile/MobileApi.svc/SubmitPayment", BaseUrl), "POST", InputStream.ToArray());

                    using (var OutputStream = new MemoryStream(RawResponse))
                    {
                        //Deserialize and return
                        var DeSerializer = new DataContractJsonSerializer(typeof(PaymentResponse));
                        return (PaymentResponse)DeSerializer.ReadObject(OutputStream);
                    }
                }
            }
        }

		private static PaymentHistoryResponse TestGetPaymentHistory(string sessionId)
		{
			//Create the login request
			var Request = new PaymentHistoryRequest {SessionId = sessionId};

			//Create the WebClient
			using (var Client = new WebClient())
			{
				//Set the content-type header for the request
				Client.Headers["content-type"] = "application/json";

				using (var InputStream = new MemoryStream())
				{
					//Serialize the input to the MemoryStream
					var Serializer = new DataContractJsonSerializer(typeof(PaymentHistoryRequest));
					Serializer.WriteObject(InputStream, Request);

					//Post the request
					var RawResponse = Client.UploadData(string.Format("https://{0}/Mobile/MobileApi.svc/GetPaymentHistory", BaseUrl), "POST", InputStream.ToArray());

					using (var OutputStream = new MemoryStream(RawResponse))
					{
						//Deserialize and return
						var DeSerializer = new DataContractJsonSerializer(typeof(PaymentHistoryResponse));
						return (PaymentHistoryResponse)DeSerializer.ReadObject(OutputStream);
					}
				}
			}
		}
		private static LogoutResponse TestLogout(string sessionId)
		{
			//Create the login request
			var Request = new LogoutRequest { SessionId = sessionId };

			//Create the WebClient
			using (var Client = new WebClient())
			{
				//Set the content-type header for the request
				Client.Headers["content-type"] = "application/json";

				using (var InputStream = new MemoryStream())
				{
					//Serialize the input to the MemoryStream
					var Serializer = new DataContractJsonSerializer(typeof(LogoutRequest));
					Serializer.WriteObject(InputStream, Request);

					//Post the request
					var RawResponse = Client.UploadData(string.Format("https://{0}/Mobile/MobileApi.svc/Logout", BaseUrl), "POST", InputStream.ToArray());

					using (var OutputStream = new MemoryStream(RawResponse))
					{
						//Deserialize and return
						var DeSerializer = new DataContractJsonSerializer(typeof(LogoutResponse));
						return (LogoutResponse)DeSerializer.ReadObject(OutputStream);
					}
				}
			}
		}

        private static EfxFramework.Ivr.UserInformationResponse GetIvrInformation()
        {
            //EfxFramework.Ivr.UserInformationRequest request = new EfxFramework.Ivr.UserInformationRequest("900185001", "9500");
            //return request.GetUserInformation("testuser", "test1234");

            using (var Client = new WebClient())
            {
                var RawResponse = Client.DownloadData(string.Format("https://{0}/WebServices/IvrWebService.svc/GetUserInformation/testuser/test1234/4636442214/6593", BaseUrl));

                using (var OutputStream = new MemoryStream(RawResponse))
                {
                    var DeSerializer = new DataContractJsonSerializer(typeof(EfxFramework.Ivr.UserInformationResponse));
                    return (EfxFramework.Ivr.UserInformationResponse)DeSerializer.ReadObject(OutputStream);
                }
            }
        }

        private static EfxFramework.Ivr.PaymentResponse ProcessIvrPayment(EfxFramework.Ivr.UserInformationResponse user)
        {
            if (user.Result == EfxFramework.PaymentMethods.IvrResponseResult.Success)
            {
                //EfxFramework.Ivr.PaymentRequest request = new EfxFramework.Ivr.PaymentRequest("9000190001", "9000");
                //return request.ProcessPayment("500.00", "testuser", "test1234");

                using (var Client = new WebClient())
                {
                    var RawResponse = Client.DownloadData(string.Format("https://{0}/WebServices/IvrWebService.svc/SubmitPayment/testuser/test1234/0110101101/0110", BaseUrl));

                    using (var OutputStream = new MemoryStream(RawResponse))
                    {
                        var DeSerializer = new DataContractJsonSerializer(typeof(EfxFramework.Ivr.PaymentResponse));
                        return (EfxFramework.Ivr.PaymentResponse)DeSerializer.ReadObject(OutputStream);
                    }
                }
            }
                
            return new EfxFramework.Ivr.PaymentResponse() { RentAndFeeResult = EfxFramework.PaymentMethods.IvrResponseResult.UserNotFound, RentAndFeeResponseMessage = ServiceResponseMessages.Usernotfound };
        }

        private static List<string> GetPropertiesBySearchString()
        {
            using (var Client = new WebClient())
            {
                var RawResponse = Client.DownloadData(string.Format("https://{0}/WebServices/TypeAheadService.svc/GetPropertiesBySearchString/Ar", BaseUrl));

                using (var OutputStream = new MemoryStream(RawResponse))
                {
                    var DeSerializer = new DataContractJsonSerializer(typeof (List<string>));
                    return (List<string>) DeSerializer.ReadObject(OutputStream);
                }
            }
        }

	    public static string ProcessSmsPayment()
	    {
            using (var Client = new WebClient())
            {
                var Response = Client.DownloadString(string.Format("https://{0}/WebServices/SmsWebService.svc/SubmitPayment?", BaseUrl));
                return Client.DownloadString(string.Format("http://{0}/WebServices/SmsWebService.svc/SubmitPayment?phone=8134281151&message=test", BaseUrl));
            }
	    }

        private static MobileServiceUrlResponse GetMobileServiceUrlByVersion(int version)
        {
            var Request = new MobileServiceUrlRequest {MobileVersionNumber = version};

            using (var Client = new WebClient())
            {
                //Set the content-type header for the request
                Client.Headers["content-type"] = "application/json";

                using (var InputStream = new MemoryStream())
                {
                    //Serialize the input to the MemoryStream
                    var Serializer = new DataContractJsonSerializer(typeof(MobileServiceUrlRequest));
                    Serializer.WriteObject(InputStream, Request);

                    var RawResponse = Client.UploadData(string.Format("https://{0}/Mobile/MobileApi.svc/GetMobileServiceUrl", BaseUrl), "POST", InputStream.ToArray());

                    using (var OutputStream = new MemoryStream(RawResponse))
                    {
                        //Deserialize and return
                        var DeSerializer = new DataContractJsonSerializer(typeof(MobileServiceUrlResponse));
                        return (MobileServiceUrlResponse)DeSerializer.ReadObject(OutputStream);
                    }
                }
            }
        }

        private static AuthenticatePropertyStaffResponse AuthenticateStaff()
        {
            //var Request = new AuthenticatePropertyStaffRequest {Username = "appletest3@test.com", Password = "APPLETEST1234"};
            //var Request = new AuthenticatePropertyStaffRequest {Username = "ana@mb2x.com", Password = "123"};
            var Request = new AuthenticatePropertyStaffRequest { Username = "ncloonen@yahoo.com", Password = "q1w2e3r4" };

            //Create the WebClient
            using (var Client = new WebClient())
            {
                //Set the content-type header for the request
                Client.Headers["content-type"] = "application/json";

                using (var InputStream = new MemoryStream())
                {
                    //Serialize the input to the MemoryStream
                    var Serializer = new DataContractJsonSerializer(typeof(AuthenticatePropertyStaffRequest));
                    Serializer.WriteObject(InputStream, Request);

                    System.Net.ServicePointManager.ServerCertificateValidationCallback =
                        ((sender, certificate, chain, sslPolicyErrors) => true);

                    var RawResponse = Client.UploadData(string.Format("https://{0}/Mobile/MobileApi.svc/AuthenticatePropertyStaff", BaseUrl), "POST", InputStream.ToArray());

                    using (var OutputStream = new MemoryStream(RawResponse))
                    {
                        //Deserialize and return
                        var DeSerializer = new DataContractJsonSerializer(typeof(AuthenticatePropertyStaffResponse));
                        return (AuthenticatePropertyStaffResponse)DeSerializer.ReadObject(OutputStream);
                    }
                }
            }
        }

        private static EcheckPaymentByRenterIdResponse MobilePayRentAch(AuthenticatePropertyStaffResponse session)
        {
            var Request = new EcheckPaymentByRenterIdRequest();
            Request.Amount = 1.00M;
            Request.BankAccountNumber = "012345677";
            Request.RoutingNumber = "072414019";
            Request.RenterId = 156; //ncloonen@yahoo.com
            Request.SessionId = session.SessionId;

            //Create the WebClient
            using (var Client = new WebClient())
            {
                //Set the content-type header for the request
                Client.Headers["content-type"] = "application/json";

                using (var InputStream = new MemoryStream())
                {
                    //Serialize the input to the MemoryStream
                    var Serializer = new DataContractJsonSerializer(typeof(EcheckPaymentByRenterIdRequest));
                    Serializer.WriteObject(InputStream, Request);

                    var RawResponse = Client.UploadData(string.Format("https://{0}/Mobile/MobileApi.svc/EcheckPaymentByRenterId", BaseUrl), "POST", InputStream.ToArray());

                    using (var OutputStream = new MemoryStream(RawResponse))
                    {
                        //Deserialize and return
                        var DeSerializer = new DataContractJsonSerializer(typeof(EcheckPaymentByRenterIdResponse));
                        return (EcheckPaymentByRenterIdResponse)DeSerializer.ReadObject(OutputStream);
                    }
                }
            }
        }

        private static CreditCardPaymentByRenterIdResponse MobilePayRentCc(AuthenticatePropertyStaffResponse session)
        {
            var Request = new CreditCardPaymentByRenterIdRequest();
            Request.Amount = 10.23M;
            Request.CardholderName = "Steve Abraham";
            Request.CardNumber = "4111-1111-1111-1111";
            Request.ExpirationDate = "0419";
            Request.Cvv = "123";
            Request.RenterId = 156; //ncloonen@yahoo.com
            Request.SessionId = session.SessionId;

            
            //Create the WebClient
            using (var Client = new WebClient())
            {
                //Set the content-type header for the request
                Client.Headers["content-type"] = "application/json";

                using (var InputStream = new MemoryStream())
                {
                    //Serialize the input to the MemoryStream
                    var Serializer = new DataContractJsonSerializer(typeof(CreditCardPaymentByRenterIdRequest));
                    Serializer.WriteObject(InputStream, Request);

                    var RawResponse = Client.UploadData(string.Format("https://{0}/Mobile/MobileApi.svc/CreditCardPaymentByRenterId", BaseUrl), "POST", InputStream.ToArray());

                    using (var OutputStream = new MemoryStream(RawResponse))
                    {
                        //Deserialize and return
                        var DeSerializer = new DataContractJsonSerializer(typeof(CreditCardPaymentByRenterIdResponse));
                        return (CreditCardPaymentByRenterIdResponse)DeSerializer.ReadObject(OutputStream);
                    }
                }
            }
        }

        private static GetRentersByPropertyIdResponse MobileGetRentersByPropertyId(AuthenticatePropertyStaffResponse session)
        {
            var Request = new GetRentersByPropertyIdRequest();
            Request.SessionId = session.SessionId;
            Request.PropertyId = "2";
            Request.OnlyLate = false;

            //Create the WebClient
            using (var Client = new WebClient())
            {
                //Set the content-type header for the request
                Client.Headers["content-type"] = "application/json";

                using (var InputStream = new MemoryStream())
                {
                    //Serialize the input to the MemoryStream
                    var Serializer = new DataContractJsonSerializer(typeof(GetRentersByPropertyIdRequest));
                    Serializer.WriteObject(InputStream, Request);

                    var RawResponse = Client.UploadData(string.Format("https://{0}/Mobile/MobileApi.svc/GetRentersByPropertyId", BaseUrl), "POST", InputStream.ToArray());

                    using (var OutputStream = new MemoryStream(RawResponse))
                    {
                        //Deserialize and return
                        var DeSerializer = new DataContractJsonSerializer(typeof(GetRentersByPropertyIdResponse));
                        return (GetRentersByPropertyIdResponse)DeSerializer.ReadObject(OutputStream);
                    }
                }
            }
        }

        private static GetRentersByPropertyIdResponse TestMobileMaintenanceHistory()
        {
            var Auth = AuthenticateStaff();
            var Request = new GetRentersByPropertyIdRequest();
            Request.SessionId = Auth.SessionId;
            Request.PropertyId = "2";
            Request.OnlyLate = false;

            //Create the WebClient
            using (var Client = new WebClient())
            {
                //Set the content-type header for the request
                Client.Headers["content-type"] = "application/json";

                using (var InputStream = new MemoryStream())
                {
                    //Serialize the input to the MemoryStream
                    var Serializer = new DataContractJsonSerializer(typeof(GetRentersByPropertyIdRequest));
                    Serializer.WriteObject(InputStream, Request);

                    var RawResponse = Client.UploadData(string.Format("https://{0}/Mobile/MobileApi.svc/GetRentersByPropertyId", BaseUrl), "POST", InputStream.ToArray());

                    using (var OutputStream = new MemoryStream(RawResponse))
                    {
                        //Deserialize and return
                        var DeSerializer = new DataContractJsonSerializer(typeof(GetRentersByPropertyIdResponse));
                        return (GetRentersByPropertyIdResponse)DeSerializer.ReadObject(OutputStream);
                    }
                }
            }
            //var Request = new MaintenanceHistoryRequest();
            //Request.SessionId = Auth.SessionId;
            //Request.RenterId = 12;

            ////Create the WebClient
            //using (var Client = new WebClient())
            //{
            //    //Set the content-type header for the request
            //    Client.Headers["content-type"] = "application/json";

            //    using (var InputStream = new MemoryStream())
            //    {
            //        //Serialize the input to the MemoryStream
            //        var Serializer = new DataContractJsonSerializer(typeof(MaintenanceHistoryRequest));
            //        Serializer.WriteObject(InputStream, Request);

            //        var RawResponse = Client.UploadData(string.Format("https://{0}/Mobile/MobileApi.svc/GetMaintenanceHistory", BaseUrl), "POST", InputStream.ToArray());

            //        using (var OutputStream = new MemoryStream(RawResponse))
            //        {
            //            //Deserialize and return
            //            var DeSerializer = new DataContractJsonSerializer(typeof(MaintenanceHistoryResponse));
            //            return (MaintenanceHistoryResponse)DeSerializer.ReadObject(OutputStream);
            //        }
            //    }
            //}

            //var Auth = new AuthenticatePropertyStaffRequest {Username = "brian.madigan@mb2x.com", Password = "123"};
            //var Request = new GetRentersByPropertyIdRequest {PropertyId = "1", SessionId = Auth.AuthenticatePropertyStaff().SessionId};
            //var Results = Request.GetRentersByPropertyId();
            //return Results;
        }

        private static SetRenterResponse MobileRenterSet(AuthenticatePropertyStaffResponse session)
        {
            var Request = new SetRenterRequest();
            Request.SessionId = session.SessionId;
            Request.Renter = new RenterByPropertyIdRenter();
            Request.Renter.RentDueDate = "20130124135944";
            Request.Renter.FirstName = "test";
            Request.Renter.LastName = "Foster";
            Request.Renter.RenterId = 24;
            Request.Renter.IsRentLate = false;
            Request.Renter.PhoneNumber = "(905) 444-4444";
            Request.Renter.EmailAddress = "test@test.com";
            Request.Renter.Address = new RenterAddress();
            Request.Renter.Address.StreetAddress = "123 test";
            Request.Renter.Address.StateProvince = "FL";
            Request.Renter.Address.PostalCode = "32256";
            Request.Renter.Address.City = "jax";
            Request.Renter.Address.Unit = "123";

            //Create the WebClient
            using (var Client = new WebClient())
            {
                //Set the content-type header for the request
                Client.Headers["content-type"] = "application/json";

                using (var InputStream = new MemoryStream())
                {
                    //Serialize the input to the MemoryStream
                    var Serializer = new DataContractJsonSerializer(typeof(SetRenterRequest));
                    Serializer.WriteObject(InputStream, Request);

                    var RawResponse = Client.UploadData(string.Format("https://{0}/Mobile/MobileApi.svc/SetRenter", BaseUrl), "POST", InputStream.ToArray());

                    using (var OutputStream = new MemoryStream(RawResponse))
                    {
                        //Deserialize and return
                        var DeSerializer = new DataContractJsonSerializer(typeof(SetRenterResponse));
                        return (SetRenterResponse)DeSerializer.ReadObject(OutputStream);
                    }
                }
            }
        }

        private static void YardiTest()
        {
            //foreach (var R in Renter.GetAllRenterList().Where(R => R.RenterId > 177))
            //{
            //    Renter.DeleteRenterByRenterId(R.RenterId, "Brian Madigan");
            //}

            ImportRequest.ImportRenters();
            ExportRequest.ExportPayments();
        }

        private static void TestPublicApi()
        {
            //var Property = PublicApiTest.Property.ProcessProperty();
            //var Staff = PublicApiTest.PropertyStaff.ProcessPropertyStaff(Property.PropertyId);
            //var Resident = PublicApiTest.Resident.ProcessResident(Property.PropertyId);
            //var Lease = PublicApiTest.Lease.ProcessLease(Property.PropertyId, Staff.PropertyStaffId, Resident.PublicResidentId);
            //var LeaseDelete = PublicApiTest.Lease.DeleteLease(Lease.LeaseId);
            //var ResidentDelete = PublicApiTest.Resident.DeleteResident(Resident.PublicResidentId);
            //var StaffDelete = PublicApiTest.PropertyStaff.DeletePropertyStaff(Staff.PropertyStaffId);
            //var PropertyDelete = PublicApiTest.Property.DeleteProperty(Property.PropertyId);
            //var Marketing = PublicApiTest.Marketing.AddLead();
            //var RentReportersProperties = PublicApiTest.Property.GetRentReportersProperties();
            //var TransactionHistory = PublicApiTest.Transaction.GetTransactionHistory();
            //var CcPayment = PublicApiTest.Payment.ProcessCreditCard();
            //var AchPayment = PublicApiTest.Payment.ProcessAch();
            //var RecurringCc = PublicApiTest.Payment.ProcessRecurringCreditCard();
            //var RecurringAch = PublicApiTest.Payment.ProcessRecurringAch();
            //var Reversal = PublicApiTest.Payment.ProcessReversal(CcPayment.ProcessOneTimeCreditCardPaymentResult.TransactionId);
        }

	    /* END WCF TESTING */

		/* BEGIN AUTHENTICATION TESTING */
		private static void TestRenter()
		{
			//Set the password
			var CurrentUser = new Renter(1);
			//CurrentUser.SetPassword("Hello World");
			//Renter.Set(CurrentUser);

			//Reset the password
			//var TempPassword = CurrentUser.ResetPassword();
			//Renter.Set(CurrentUser);

			var Temp = CurrentUser.Authenticate("Hello World");

            //int j = 0;
		}



		/* END AUTHENTICATION TESTING */

		static void Main()
		{


		    //Transaction.Set(0, "0123456789", "9876543210", 1, "Jonathan", "Hupp", "5050 Royal Cypress Cir", null, "Tampa", 9, "33647", "9049822919", "jonathan.hupp@mb2x.com", null, 1000.00M, 15.00M, "Jonathan Hupp", "1234567890123456", 1, 15, "6543210987654321", "123456789", 1, 1, 1, 1, "Success", DateTime.Now, 1);

		    ////var MyAddress = new Address
		    ////                    {
		    ////                        StreetAddress = "2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.2818 Cypress Ridge Blvd.", 
		    ////                        Address2 = "Suite 110", 
		    ////                        City = "Wesley Chapel", 
		    ////                        CurrentStateProvince = StateProvince.FL, 
		    ////                        PostalCode = "33544"
		    ////                    };

		    ////var ErrorList = MyAddress.CheckDataValidations<Address>();
		    ////int j = 0;


		    //ServicePointManager.ServerCertificateValidationCallback = IgnoreCertificateErrorHandler;

		    //var RecipientList = new List<MailAddress>
		    //                        {
		    //                            new MailAddress("steve@mb2x.com"),
		    //                            new MailAddress("steve@macrobright.com"),
		    //                            new MailAddress("steve@minicities.com"),
		    //                            new MailAddress("steve@invalid.com")
		    //                        };

		    //BulkMail.SendMailMessage("Steve", "Password", new MailAddress("steve@mb2x.com", "Sabraham"), "New Subject", Guid.NewGuid().ToString(), RecipientList);

		    //TestPassword();
		    //TestRenter();
		    //return;

		    //var AchPaymentResponse = TestAch();
		    //var IvrInformationResponse = GetIvrInformation();
		    //var IvrPaymentResponse = ProcessIvrPayment(IvrInformationResponse);
            //var AuthenticateResponse = TestAuthenticate();
            //var SubmitCreditCardOnFilePaymentResponse = TestSubmitCreditCardOnFilePayment(AuthenticateResponse.SessionId);
		    //var SubmitScanCreditCardPaymentResponse = TestSubmitScanCreditCardPayment(AuthenticateResponse.SessionId);
		    //var SubmitEcheckPaymentResponse = TestSubmitEcheckPayment(AuthenticateResponse.SessionId);
		    //var PaymentHistoryResponse = TestGetPaymentHistory(AuthenticateResponse.SessionId);
		    //var LogoutUserResponse = TestLogout(AuthenticateResponse.SessionId);
		    //var SmsPayment = ProcessSmsPayment();
		    //var results = GetPropertiesBySearchString();
		    //YardiTest();
		    //var MobileResults = MobileRenterSet(AuthenticateStaff());
		    
            var MobileResults2 = MobilePayRentAch(AuthenticateStaff());
		    
            //var MobileResults3 = MobilePayRentCc(AuthenticateStaff());
		    //var MobileResult4 = MobileGetRentersByPropertyId(AuthenticateStaff());
		    //BulkMail.GetMessageDeliveryRate();
		    //TestPublicApi();
		    //var Results = TestMobileMaintenanceHistory();
            //var Results = GetMobileServiceUrlByVersion(2);
            //int j = 0;
		}
	}
}
