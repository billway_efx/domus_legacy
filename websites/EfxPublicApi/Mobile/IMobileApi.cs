﻿using EfxFramework.Mobile;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace EfxPublicApi.Mobile
{
    [ServiceContract]
    public interface IMobileApi
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Authenticate", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        AuthenticationResponse Authenticate(AuthenticationRequest request);

        //Salcedo - 4/28/2016 - new contract to accept parameters in URL, and return XML instead of Json
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Authenticate/{username}/{password}")]
        AuthenticationResponse Authenticate2(string Username, string Password);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/SubmitPayment", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PaymentResponse SubmitPayment(PaymentRequest request);

        //Salcedo - 4/28/2016 - new contract to accept parameters in URL, and return XML instead of Json
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/SubmitPayment/{SessionId}/{Amount}/{Description}/{CreditCardNumber}/{CvvCode}/{ExpirationDate}/{NameOnCard}/{PaymentMethod}")]
        PaymentResponse SubmitPayment2(string SessionId, string Amount, string Description, string CreditCardNumber, string CvvCode, string ExpirationDate, string NameOnCard, string PaymentMethod);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetPaymentHistory", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PaymentHistoryResponse GetPaymentHistory(PaymentHistoryRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/Logout", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        LogoutResponse Logout(LogoutRequest request);

        //Salcedo - 4/28/2016 - new contract to accept parameters in URL, and return XML instead of Json
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Logout/{SessionId}")]
        LogoutResponse Logout2(string SessionId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/AuthenticatePropertyStaff", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        AuthenticatePropertyStaffResponse AuthenticatePropertyStaff(AuthenticatePropertyStaffRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetRentersByPropertyId", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GetRentersByPropertyIdResponse GetRentersByPropertyId(GetRentersByPropertyIdRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetRenterByRenterId", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GetRenterByRenterIdResponse GetRenterByRenterId(GetRenterByRenterIdRequest request);

        //Salcedo - 4/28/2016 - new contract to accept parameters in URL, and return XML instead of Json
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetRenterByRenterId/{SessionId}/{RenterId}")]
        GetRenterByRenterIdResponse GetRenterByRenterId2(string SessionId, string RenterId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/SetRenter", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        SetRenterResponse SetRenter(SetRenterRequest request);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/CreditCardPaymentByRenterId", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        CreditCardPaymentByRenterIdResponse CreditCardPaymentByRenterId(CreditCardPaymentByRenterIdRequest request);

        //Salcedo - 4/28/2016 - new contract to accept parameters in URL, and return XML instead of Json
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/CreditCardPaymentByRenterId/{SessionId}/{RenterId}/{Amount}/{CardholderName}/{CardNumber}/{ExpirationDate}/{Cvv}")]
        CreditCardPaymentByRenterIdResponse CreditCardPaymentByRenterId2(string SessionId, string RenterId, string Amount, string CardholderName, string CardNumber, string ExpirationDate, string Cvv);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/EcheckPaymentByRenterId", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        EcheckPaymentByRenterIdResponse EchekPaymentByRenterId(EcheckPaymentByRenterIdRequest request);

        //Salcedo - 4/28/2016 - new contract to accept parameters in URL, and return XML instead of Json
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/EcheckPaymentByRenterId/{SessionId}/{RenterId}/{Amount}/{RoutingNumber}/{BankAccountNumber}")]
        EcheckPaymentByRenterIdResponse EchekPaymentByRenterId2(string SessionId, string RenterId, string Amount, string RoutingNumber, string BankAccountNumber);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/SubmitMaintenanceRequest", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MaintenanceResponse SubmitMaintenanceRequest(MaintenanceRequest request);

        //Salcedo - 6/9/2016 - new contracts to accept parameters in URL, and return XML instead of Json
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/SubmitMaintenanceRequest/{SessionId}/{RenterId}/{MaintenanceTypeId}/{MaintenancePriorityId}/{MaintenanceSubmissionDate}/{MaintenanceDescription}/{CcResident}")]
        MaintenanceResponse SubmitMaintenanceRequest2(string SessionId, string RenterId, string MaintenanceTypeId, string MaintenancePriorityId,
            string MaintenanceSubmissionDate, string MaintenanceDescription, string CcResident);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetMaintenanceHistory", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MaintenanceHistoryResponse GetMaintenanceHistory(MaintenanceHistoryRequest request);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetMaintenanceHistory/{SessionId}/{RenterId}")]
        MaintenanceHistoryResponse GetMaintenanceHistory2(string SessionId, string RenterId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/GetMobileServiceUrl", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        MobileServiceUrlResponse GetMobileServiceUrl(MobileServiceUrlRequest request);

        //Salcedo - 4/29/2016 - new contracts for new mobile app

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPayableItemsForRenter/{SessionId}/{RenterId}")]
        PayablesResponse GetPayablesForRenter(string SessionId, string RenterId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPayableFeeDetail/{SessionId}/{RenterId}")]
        GetPayableFeeDetailResponse GetPayableFeeDetail(string SessionId, string RenterId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_GetPaymentMethodsForRenter/{SessionId}/{RenterId}")]
        Wallet_GetPaymentMethodsResponse Wallet_GetPaymentMethodsForRenter(string SessionId, string RenterId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_AddCCPaymentMethodForRenter/{SessionId}/{RenterId}/{CCHolderName}/{CCAccountNumber}/{CCExpMonth}/{CCExpYear}/{CardSecurityCode}")]
        Wallet_AddCCPaymentMethodResponse Wallet_AddCCPaymentMethodForRenter(string SessionId, string RenterId, string CCHolderName, string CCAccountNumber, string CCExpMonth, string CCExpYear,
            string CardSecurityCode);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_AddACHPaymentMethodForRenter/{SessionId}/{RenterId}/{AccountTypeId}/{AccountNumber}/{RoutingNumber}")]
        Wallet_AddACHPaymentMethodResponse Wallet_AddACHPaymentMethodForRenter(string SessionId, string RenterId, string AccountTypeId, string AccountNumber, string RoutingNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_UpdateCCPaymentMethodForRenter/{SessionId}/{RenterId}/{PaymentMethodId}/{CCHolderName}/{CCAccountNumber}/{CCExpMonth}/{CCExpYear}/{CardSecurityCode}")]
        Wallet_UpdateCCPaymentMethodResponse Wallet_UpdateCCPaymentMethodForRenter(string SessionId, string RenterId, string PaymentMethodId, string CCHolderName, string CCAccountNumber, 
            string CCExpMonth, string CCExpYear, string CardSecurityCode);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_UpdateACHPaymentMethodForRenter/{SessionId}/{RenterId}/{PaymentMethodId}/{AccountTypeId}/{AccountNumber}/{RoutingNumber}")]
        Wallet_UpdateACHPaymentMethodResponse Wallet_UpdateACHPaymentMethodForRenter(string SessionId, string RenterId, string PaymentMethodId, string AccountTypeId, string AccountNumber, string RoutingNumber);

        //cakel: 07/28/2016 added variable for PaymentMethod to indicate whether it is a credit card or ACH
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_RemovePaymentMethodForRenter/{SessionId}/{RenterId}/{PaymentMethodId}/{PaymentMethod}")]
        Wallet_RemovePaymentMethodResponse Wallet_RemovePaymentMethodForRenter(string SessionId, string RenterId, string PaymentMethodId, string PaymentMethod);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_PaySingleItemForRenter/{SessionId}/{RenterId}/{PayerWalletId}/{PayableItemId}/{AmountToPay}")]
        Wallet_PaySingleItemResponse Wallet_PaySingleItemForRenter(string SessionId, string RenterId, string PayerWalletId, string PayableItemId, string AmountToPay);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_PaySplitWithWalletAch/{SessionId}/{RenterId}/{PayerWalletId}/{PayableItemId}/{AmountToPay}/{SplitPaymentGroupId}/{SplitPaymentGroupCompleteFlag}")]
        Wallet_PaySingleItemResponse Wallet_PaySplitWithWalletAch(string SessionId, string RenterId, string PayerWalletId, string PayableItemId, string AmountToPay,
            string SplitPaymentGroupId, string SplitPaymentGroupCompleteFlag);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_PaySingleItemForRenterWithCSC/{SessionId}/{RenterId}/{PayerWalletId}/{PayableItemId}/{AmountToPay}/{CardSecurityCode}")]
        Wallet_PaySingleItemResponse Wallet_PaySingleItemForRenterWithCSC(string SessionId, string RenterId, string PayerWalletId, string PayableItemId, string AmountToPay,
            string CardSecurityCode);


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_PaySingleItemForRenterWithNewCC/{SessionId}/{RenterId}/{PayableItemId}/{AmountToPay}/{CCHolderName}/{CCAccountNumber}/{CCExpMonth}/{CCExpYear}/{CardSecurityCode}")]
        Wallet_PaySingleItemResponse Wallet_PaySingleItemForRenterWithNewCC(string SessionId, string RenterId, string PayableItemId, string AmountToPay,
            string CCHolderName, string CCAccountNumber, string CCExpMonth, string CCExpYear, string CardSecurityCode);


        //Salcedo - 7/18/2016 - added new Pre/Post Auth methods
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_PreAuthItemForRenterCC/{SessionId}/{RenterId}/{PayerWalletId}/{PayableItemId}/{AmountToPay}/{SplitPaymentGroupId}/{CardSecurityCode=null}")]
        Wallet_PreAuthCCResponse Wallet_PreAuthItemForRenterCC(string SessionId, string RenterId, string PayerWalletId, string PayableItemId, string AmountToPay, string SplitPaymentGroupId, 
            string CardSecurityCode);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_PreAuthItemForRenterWithNewCC/{SessionId}/{RenterId}/{PayableItemId}/{AmountToPay}/{CCHolderName}/{CCAccountNumber}/{CCExpMonth}/{CCExpYear}/{SplitPaymentGroupId}/{CardSecurityCode=null}")]
        Wallet_PreAuthCCResponse Wallet_PreAuthItemForRenterWithNewCC(string SessionId, string RenterId, string PayableItemId, string AmountToPay,
            string CCHolderName, string CCAccountNumber, string CCExpMonth, string CCExpYear, string SplitPaymentGroupId, string CardSecurityCode);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_PostAuthItemForRenter/{SessionId}/{RenterId}/{TransactionId}/{SplitPaymentGroupCompleteFlag}")]
        Wallet_PostAuthCCResponse Wallet_PostAuthItemForRenter(string SessionId, string RenterId, string TransactionId, string SplitPaymentGroupCompleteFlag);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_ReverseAuthorization/{SessionId}/{RenterId}/{TransactionId}/{PaymentAmount}")]
        Wallet_ReverseAuthResponse Wallet_ReverseAuthorization(string SessionId, string RenterId, string TransactionId, string PaymentAmount);

        //Salcedo - 7/18/2016 - end add new Pre/Post Auth methods


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_PaySingleItemForRenterWithNewAch/{SessionId}/{RenterId}/{PayableItemId}/{AmountToPay}/{AccountTypeId}/{AccountNumber}/{RoutingNumber}")]
        Wallet_PaySingleItemResponse Wallet_PaySingleItemForRenterWithNewAch(string SessionId, string RenterId, string PayableItemId, string AmountToPay,
            string AccountTypeId, string AccountNumber, string RoutingNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_PaySplitWithNewAch/{SessionId}/{RenterId}/{PayableItemId}/{AmountToPay}/{AccountTypeId}/{AccountNumber}/{RoutingNumber}/{SplitPaymentGroupId}/{SplitPaymentGroupCompleteFlag}")]
        Wallet_PaySingleItemResponse Wallet_PaySplitWithNewAch(string SessionId, string RenterId, string PayableItemId, string AmountToPay,
            string AccountTypeId, string AccountNumber, string RoutingNumber, string SplitPaymentGroupId, string SplitPaymentGroupCompleteFlag);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_PaySingleItemForRenterWithUpdateToCCMethod/{SessionId}/{RenterId}/{PaymentMethodId}/{PayableItemId}/{AmountToPay}/{CCHolderName}/{CCAccountNumber}/{CCExpMonth}/{CCExpYear}/{CardSecurityCode}")]
        Wallet_PaySingleItemResponse Wallet_PaySingleItemForRenterWithUpdateToCCMethod(string SessionId, string RenterId, string PaymentMethodId, string PayableItemId, string AmountToPay,
            string CCHolderName, string CCAccountNumber, string CCExpMonth, string CCExpYear, string CardSecurityCode);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_PaySingleItemForRenterWithUpdateToAchMethod/{SessionId}/{RenterId}/{PaymentMethodId}/{PayableItemId}/{AmountToPay}/{AccountTypeId}/{AccountNumber}/{RoutingNumber}")]
        Wallet_PaySingleItemResponse Wallet_PaySingleItemForRenterWithUpdateToAchMethod(string SessionId, string RenterId, string PaymentMethodId, string PayableItemId, string AmountToPay,
            string AccountTypeId, string AccountNumber, string RoutingNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_GetRenterPaymentHistory/{SessionId}/{RenterId}")]
        Wallet_GetRenterPaymentHistoryResponse Wallet_GetRenterPaymentHistory(string SessionId, string RenterId);

        //Salcedo - 5/31/2016 - Added Gate Code methods for use by new mobile app

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Renter_GateCode_GetAll/{SessionId}/{RenterId}")]
        Renter_GateCode_GetAll_Response Renter_GateCode_GetAll(string SessionId, string RenterId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Renter_GateCode_GetByInternalId/{SessionId}/{RenterId}/{InternalId}")]
        Renter_GateCode_GetByInternalId_Response Renter_GateCode_GetByInternalId(string SessionId, string RenterId, string InternalId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Renter_GateCode_GetByUserId/{SessionId}/{RenterId}/{UserId}")]
        Renter_GateCode_GetByUserId_Response Renter_GateCode_GetByUserId(string SessionId, string RenterId, string UserId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Renter_GateCode_Add/{SessionId}/{RenterId}/{UserId}/{FirstName}/{MiddleName}/{LastName}/{MobilePhoneNumber}/{PrimaryEmailAddress}/{GateCode}")]
        Renter_GateCode_Add_Response Renter_GateCode_Add(string SessionId, string RenterId, string UserId, string FirstName, string MiddleName, string LastName, string MobilePhoneNumber, string PrimaryEmailAddress, string GateCode);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Renter_GateCode_UpdateByInternalId/{SessionId}/{RenterId}/{InternalId}/{UserId}/{FirstName}/{MiddleName}/{LastName}/{MobilePhoneNumber}/{PrimaryEmailAddress}/{GateCode}")]
        Renter_GateCode_UpdateByInternalId_Response Renter_GateCode_UpdateByInternalId(string SessionId, string RenterId, string InternalId, string UserId, string FirstName, string MiddleName, string LastName, string MobilePhoneNumber, string PrimaryEmailAddress, string GateCode);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Renter_GateCode_UpdateByUserId/{SessionId}/{RenterId}/{UserId}/{FirstName}/{MiddleName}/{LastName}/{MobilePhoneNumber}/{PrimaryEmailAddress}/{GateCode}")]
        Renter_GateCode_UpdateByUserId_Response Renter_GateCode_UpdateByUserId(string SessionId, string RenterId, string UserId, string FirstName, string MiddleName, string LastName, string MobilePhoneNumber, string PrimaryEmailAddress, string GateCode);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Renter_GateCode_DeleteByInternalId/{SessionId}/{RenterId}/{InternalId}")]
        Renter_GateCode_DeleteByInternalId_Response Renter_GateCode_DeleteByInternalId(string SessionId, string RenterId, string InternalId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Renter_GateCode_DeleteByUserId/{SessionId}/{RenterId}/{UserId}")]
        Renter_GateCode_DeleteByUserId_Response Renter_GateCode_DeleteByUserId(string SessionId, string RenterId, string UserId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetCalendarEventsForRenter/{SessionId}/{RenterId}")]
        GetCalendarEventsForRenter_Response GetCalendarEventsForRenter(string SessionId, string RenterId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetConvenienceFeeForPaymentByProgramID/{ProgramID}/{PaymentMethod}/{Amount}")]
        GetConvFeeForPayment_Response GetConvFeeForPayment(string ProgramID, string PaymentMethod, string Amount);

        //cakel:09/20/2016
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetConvenienceFeeForPaymentByRenterID/{RenterID}/{PaymentMethod}/{Amount}")]
        GetConvFeeForPayment_Response GetConvFeeForPaymentByRenterID(string RenterID, string PaymentMethod, string Amount);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Wallet_PayWithCash/{RenterID}/{Amount}")]
        Wallet_PayByCash CreatePaynearMeTicket(string RenterID, string Amount);




    }
}
