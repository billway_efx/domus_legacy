﻿using EfxFramework.Mobile;
using EfxFramework.Services;
using System.ServiceModel;
using EfxFramework;
using System.Collections.Generic;
using EfxFramework.PaymentMethods;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Messaging;
using EfxFramework.PaymentMethods.CreditCard;
using ACH_Objects;
using RPO;

namespace EfxPublicApi.Mobile
{
    [DefaultServiceBehavior]
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class MobileApi : IMobileApi
    {
        public AuthenticationResponse Authenticate(AuthenticationRequest request)
        {
            //Attempt to authenticate the user and return
            AuthenticationResponse TheResponse = request.AuthenticateUser();
            //Salcedo - 7/15/2016 - changes to accomodate queue
            if (TheResponse.Result == MobileResponseResult.Success)
            {
                ActivityLog AL = new ActivityLog();
                
                int RenterId = TheResponse.AuthenticatedRenter.RenterId;
                //cakel: 08/17/2016 - Added code to update wallet and items when user logs in
                AL.WriteLog("RenterID=" + RenterId, "User Logged in Renter Site.", (short)LogPriority.LogAlways, RenterId.ToString(), true);

                UpdateWalletAndPayables(RenterId);
                //cakel: added logging around authenticate
                
                QueueRenterAction(RenterId, 1);
            }
            return TheResponse;
        }

        //Salcedo - 4/28/2016 - new function to handle new service operation contract
        
        public AuthenticationResponse Authenticate2(string UserName, string Password)
        {
            AuthenticationRequest request = new AuthenticationRequest();
            request.Username = UserName;
            request.Password = Password;

            //Attempt to authenticate the user and return
            //Salcedo - 7/15/2016 - changes to accomodate queue
            AuthenticationResponse TheResponse = request.AuthenticateUser();
            if (TheResponse.Result == MobileResponseResult.Success)
            {
                int RenterId = TheResponse.AuthenticatedRenter.RenterId;
                //cakel: 08/17/2016 - Added code to update wallet and items when user logs in
                UpdateWalletAndPayables(RenterId);
                QueueRenterAction(RenterId, 1);
            }
            return TheResponse;
        }

        public PaymentResponse SubmitPayment(PaymentRequest request)
        {
            //Attempt to process the payment
            return request.ProcessPayment();
        }

        //Salcedo - 4/28/2016 - new function to handle new service operation contract
        public PaymentResponse SubmitPayment2(string SessionId, string Amount, string Description, string CreditCardNumber, string CvvCode, string ExpirationDate, string NameOnCard, string PaymentMethod)
        {
            PaymentRequest request = new PaymentRequest();
            request.SessionId = SessionId;

            PaymentItem pi = new PaymentItem();
            pi.Amount = System.Convert.ToDecimal(Amount);
            pi.Description = Description;
            pi.IsCharityPayment = false;

            request.CreditCardNumber = CreditCardNumber;
            request.CvvCode = CvvCode;
            request.ExpirationDate = ExpirationDate;
            request.NameOnCard = NameOnCard;

            //public enum PaymentMethodEnum
            //{
            //    ECheck = 1,
            //    CreditCardOnFile = 2,
            //    ScanCreditCard = 3
            //}
            request.PaymentMethod = (PaymentRequest.PaymentMethodEnum)System.Convert.ToInt16(PaymentMethod);
            
            request.PaymentItems = new System.Collections.Generic.List<PaymentItem>();
            request.PaymentItems.Add(pi);

            //Attempt to process the payment
            return request.ProcessPayment();
        }

        public PaymentHistoryResponse GetPaymentHistory(PaymentHistoryRequest request)
        {
            //Return the payment history
            return request.GetPaymentHistory();
        }

        public LogoutResponse Logout(LogoutRequest request)
        {
            //Attempt to logout the user and return
            return request.LogoutUser();
        }

        //Salcedo - 4/28/2016 - new function to handle new service operation contract
        public LogoutResponse Logout2(string SessionId)
        {
            LogoutRequest request = new LogoutRequest();
            request.SessionId = SessionId;

            //Attempt to logout the user and return
            return request.LogoutUser();
        }

        public AuthenticatePropertyStaffResponse AuthenticatePropertyStaff(AuthenticatePropertyStaffRequest request)
        {
            return request.AuthenticatePropertyStaff();
        }

        public GetRentersByPropertyIdResponse GetRentersByPropertyId(GetRentersByPropertyIdRequest request)
        {
            return request.GetRentersByPropertyId();
        }

        public GetRenterByRenterIdResponse GetRenterByRenterId(GetRenterByRenterIdRequest request)
        {
            return request.GetRenterByRenterId();
        }

        //Salcedo - 4/28/2016 - new function to handle new service operation contract
        public GetRenterByRenterIdResponse GetRenterByRenterId2(string SessionId, string RenterId)
        {
            GetRenterByRenterIdRequest request = new GetRenterByRenterIdRequest();
            request.RenterId = System.Convert.ToInt32(RenterId);
            request.SessionId = SessionId;

            return request.GetRenterByRenterId();
        }

        public SetRenterResponse SetRenter(SetRenterRequest request)
        {
            return request.SetRenter();
        }

        public CreditCardPaymentByRenterIdResponse CreditCardPaymentByRenterId(CreditCardPaymentByRenterIdRequest request)
        {
            return request.ProcessPayment();
        }

        //Salcedo - 4/28/2016 - new function to handle new service operation contract
        public CreditCardPaymentByRenterIdResponse CreditCardPaymentByRenterId2(string SessionId, string RenterId, string Amount, string CardholderName, string CardNumber, string ExpirationDate, string Cvv)
        {
            CreditCardPaymentByRenterIdRequest request = new CreditCardPaymentByRenterIdRequest();
            request.SessionId = SessionId;
            request.RenterId = System.Convert.ToInt32(RenterId);
            request.Amount = System.Convert.ToDecimal(Amount);
            request.CardholderName = CardholderName;
            request.CardNumber = CardNumber;
            request.ExpirationDate = ExpirationDate;
            request.Cvv = Cvv;
            
            return request.ProcessPayment();
        }

        public EcheckPaymentByRenterIdResponse EchekPaymentByRenterId(EcheckPaymentByRenterIdRequest request)
        {
            return request.ProcessPayment();
        }

        //Salcedo - 4/28/2016 - new function to handle new service operation contract
        public EcheckPaymentByRenterIdResponse EchekPaymentByRenterId2(string SessionId, string RenterId, string Amount, string RoutingNumber, string BankAccountNumber)
        {
            EcheckPaymentByRenterIdRequest request = new EcheckPaymentByRenterIdRequest();
            request.SessionId = SessionId;
            request.RenterId = System.Convert.ToInt32(RenterId);
            request.Amount = System.Convert.ToDecimal(Amount);
            request.RoutingNumber = RoutingNumber;
            request.BankAccountNumber = BankAccountNumber;
            
            return request.ProcessPayment();
        }

        public MaintenanceResponse SubmitMaintenanceRequest(EfxFramework.Mobile.MaintenanceRequest request)
        {
            return request.SubmitMaintenanceRequest();
        }

        //Salcedo - 6/9/2016 - new function to handle URL friendly service call
        public MaintenanceResponse SubmitMaintenanceRequest2(string IncomingSessionId, string IncomingRenterId, string IncomingMaintenanceTypeId, string IncomingMaintenancePriorityId,
            string IncomingMaintenanceSubmissionDate, string IncomingMaintenanceDescription, string IncomingCcResident)
        {
            EfxFramework.Mobile.MaintenanceRequest TheRequest = new EfxFramework.Mobile.MaintenanceRequest();

            if (!ValidGuidFormat(IncomingSessionId))
                return new MaintenanceResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };
            TheRequest.SessionId = IncomingSessionId;

            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(TheRequest.SessionId)))
                return new MaintenanceResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(TheRequest.SessionId, IncomingRenterId))
                return new MaintenanceResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };
            TheRequest.RenterId = Convert.ToInt32(IncomingRenterId);

            if (!ValidateInteger(IncomingMaintenanceTypeId))
                return new MaintenanceResponse { Result = MobileResponseResult.InvalidMaintenanceTypeId, Message = ServiceResponseMessages.InvalidMaintenanceTypeId };
            TheRequest.MaintenanceTypeId = Convert.ToInt32(IncomingMaintenanceTypeId);

            if (!ValidateInteger(IncomingMaintenancePriorityId))
                return new MaintenanceResponse { Result = MobileResponseResult.InvalidMaintenancePriorityId, Message = ServiceResponseMessages.InvalidMaintenancePriorityId };
            TheRequest.MaintenancePriorityId = Convert.ToInt32(IncomingMaintenancePriorityId);

            TheRequest.MaintenanceSubmissionDate = IncomingMaintenanceSubmissionDate;
            TheRequest.MaintenanceDescription = IncomingMaintenanceDescription;

            if (IncomingCcResident != "0" && IncomingCcResident != "1")
                return new MaintenanceResponse { Result = MobileResponseResult.InvalidCcResident, Message = ServiceResponseMessages.InvalidCcResident };
            TheRequest.CcResident = false;
            if (IncomingCcResident == "1")
                TheRequest.CcResident = true;

            MaintenanceResponse TheResponse = TheRequest.CreateMaintenanceRequest();

            return TheResponse;
        }

        public MaintenanceHistoryResponse GetMaintenanceHistory2(string IncomingSessionId, string IncomingRenterId)
        {
            EfxFramework.Mobile.MaintenanceHistoryRequest TheRequest = new EfxFramework.Mobile.MaintenanceHistoryRequest();

            if (!ValidGuidFormat(IncomingSessionId))
                return new MaintenanceHistoryResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };
            TheRequest.SessionId = IncomingSessionId;

            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(TheRequest.SessionId)))
                return new MaintenanceHistoryResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(TheRequest.SessionId, IncomingRenterId))
                return new MaintenanceHistoryResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };
            TheRequest.RenterId = Convert.ToInt32(IncomingRenterId);

            MaintenanceHistoryResponse TheResponse = TheRequest.GetMaintenanceHistory();

            return TheResponse;
        }

        public MaintenanceHistoryResponse GetMaintenanceHistory(MaintenanceHistoryRequest request)
        {
            return request.GetMaintenanceHistory();
        }

        public MobileServiceUrlResponse GetMobileServiceUrl(MobileServiceUrlRequest request)
        {
            return request.GetMobileServiceUrlBase();
        }


        //Salcedo - 4/29/2016 - added new functions to handle new methods for mobile app services

        private static bool ValidGuidFormat(string ValueToTest)
        {
            Guid GuidResult;
            bool ParseAttempt = Guid.TryParse(ValueToTest, out GuidResult);
            return ParseAttempt;
        }
        private static bool ValidRenterId(string SessionId, string RenterId)
        {
            EfxFramework.RenterSession MyTempSession;
            MyTempSession = EfxFramework.RenterSession.GetRenterSessionBySessionGuid(Guid.Parse(SessionId));
            int IntResult;
            bool ParseAttempt = Int32.TryParse(RenterId, out IntResult);

            if (!ParseAttempt) return false;
            if (MyTempSession.RenterId != IntResult) return false;

            return true;
        }
        private static bool ValidMonth(string Month)
        {
            int IntResult;
            bool ParseAttempt = Int32.TryParse(Month, out IntResult);

            if (!ParseAttempt) return false;
            if (IntResult < 1 || IntResult > 12) return false;

            return true;
        }
        private static bool ValidYear(string Year)
        {
            int IntResult;
            bool ParseAttempt = Int32.TryParse(Year, out IntResult);

            if (!ParseAttempt) return false;
            if (IntResult < 2000 || IntResult > 2100) return false;

            return true;
        }
        private static bool ValidAccountTypeId(string AccountTypeId)
        {
            int IntResult;
            bool ParseAttempt = Int32.TryParse(AccountTypeId, out IntResult);

            if (!ParseAttempt) return false;
            if (IntResult != 0 && IntResult != 1) return false;

            return true;
        }

        public PayablesResponse GetPayablesForRenter(string SessionId, string RenterId)
        {
            if (!ValidGuidFormat(SessionId))
                return new PayablesResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new PayablesResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new PayablesResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            PayablesRequest pRequest = new PayablesRequest();
            pRequest.SessionId = SessionId;
            pRequest.RenterId = System.Convert.ToInt32(RenterId);
            
            PayablesResponse pResponse = pRequest.GetPayablesForRenter();
            return pResponse;
        }

        public GetPayableFeeDetailResponse GetPayableFeeDetail(string SessionId, string RenterId)
        {
            if (!ValidGuidFormat(SessionId))
                return new GetPayableFeeDetailResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new GetPayableFeeDetailResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new GetPayableFeeDetailResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            GetPayableFeeDetailResponse pmResponse = new GetPayableFeeDetailResponse();
            pmResponse.PayableItem_FeeDetailItems = new List<EfxFramework.PayableItem_FeeDetail>();

            try
            {
                SqlDataReader dr = EfxFramework.NewDataAccess.ExecuteProcedure(EfxSettings.ConnectionString, "Pay_GetFeeDetailForPayable",
                     new[] { new SqlParameter("@RenterID", Convert.ToInt32(RenterId)) });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        PayableItem_FeeDetail FeeDetail = new PayableItem_FeeDetail();

                        FeeDetail.MonthlyFeeId = dr.GetInt32(dr.GetOrdinal("MonthlyFeeId")); 
                        FeeDetail.FeeAmount = dr.GetDecimal(dr.GetOrdinal("FeeAmount"));
                        FeeDetail.FeeName = dr.GetString(dr.GetOrdinal("FeeName"));
                        FeeDetail.ChargeCode = dr.GetString(dr.GetOrdinal("ChargeCode"));

                        pmResponse.PayableItem_FeeDetailItems.Add(FeeDetail);
                    }
                    pmResponse.Message = "Fee details successfully retrieved.";
                    pmResponse.Result = MobileResponseResult.Success;
                }
                else
                {
                    PayableItem_FeeDetail FeeDetail = new PayableItem_FeeDetail();

                    FeeDetail.MonthlyFeeId = 0;
                    FeeDetail.FeeName = "No fee details found.";
                    pmResponse.PayableItem_FeeDetailItems.Add(FeeDetail);

                    pmResponse.Message = "No fee details found.";
                    pmResponse.Result = MobileResponseResult.NoFeeDetails;
                }
            }
            catch (Exception ex)
            {
                pmResponse.Message = "Error reading database: " + ex.Message;
                pmResponse.Result = MobileResponseResult.GeneralFailure;
            }

            return pmResponse;
        }

        public Wallet_GetPaymentMethodsResponse Wallet_GetPaymentMethodsForRenter(string SessionId, string RenterId)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_GetPaymentMethodsResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_GetPaymentMethodsResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_GetPaymentMethodsResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            Wallet_GetPaymentMethodsResponse pmResponse = new Wallet_GetPaymentMethodsResponse();
            pmResponse.Wallet_PaymentMethods = new List<EfxFramework.Wallet_PaymentMethod>();

            try
            {
                SqlDataReader dr = EfxFramework.NewDataAccess.ExecuteProcedure(EfxSettings.ConnectionString, "wallet_GetPaymentMethodsForRenter",
                     new[] { new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Wallet_PaymentMethod PaymentMethod = new Wallet_PaymentMethod();

                        PaymentMethod.PayerWalletId = dr.GetInt32(dr.GetOrdinal("PayerWalletId"));
                        PaymentMethod.PayerId = dr.GetInt32(dr.GetOrdinal("PayerId"));
                        PaymentMethod.PayerMethodId = dr.GetInt32(dr.GetOrdinal("PayerMethodId"));
                        PaymentMethod.PaymentMethod = dr.GetBoolean(dr.GetOrdinal("PaymentMethod"));    // True = Credit Card payment method, False = ACH payment method
                        PaymentMethod.IsPrimary = dr.GetBoolean(dr.GetOrdinal("IsPrimary"));
                        PaymentMethod.AutoSet = dr.GetBoolean(dr.GetOrdinal("AutoSet"));
                        PaymentMethod.EXP_Date = dr.GetString(dr.GetOrdinal("EXP_Date"));
                        PaymentMethod.Editable = dr.GetBoolean(dr.GetOrdinal("Editable"));
                        PaymentMethod.PayChoose = dr.GetBoolean(dr.GetOrdinal("PayChoose"));
                        PaymentMethod.UsedLastTime = dr.GetBoolean(dr.GetOrdinal("UsedLastTime"));
                        PaymentMethod.Description = dr.GetString(dr.GetOrdinal("Pay_Method_Description"));
                        PaymentMethod.RequestSecurityCode = dr.GetBoolean(dr.GetOrdinal("TravelScore"));
                        //Salcedo - 6/14/2016 - Add Convenience fee
                        //Salcedo - 7/11/2016 - removed ConvenienceFee and replaced with ProgramId
                        //PaymentMethod.ConvenienceFee = dr.GetDecimal(dr.GetOrdinal("ConvenienceFee"));
                        PaymentMethod.ProgramId = dr.GetInt32(dr.GetOrdinal("ProgramId"));
                        pmResponse.Wallet_PaymentMethods.Add(PaymentMethod);
                    }
                    pmResponse.Message = "Wallet payment methods successfully retrieved.";
                    pmResponse.Result = MobileResponseResult.Success;
                }
                else
                {
                    Wallet_PaymentMethod PaymentMethod = new Wallet_PaymentMethod();
                    PaymentMethod.PayerWalletId = 0;
                    PaymentMethod.Description = "No payment methods found.";
                    pmResponse.Wallet_PaymentMethods.Add(PaymentMethod);

                    pmResponse.Message = "No payment methods found.";
                    pmResponse.Result = MobileResponseResult.NoPaymentMethods;
                }
            }
            catch (Exception ex)
            {
                pmResponse.Message = "Error reading database: " + ex.Message;
                pmResponse.Result = MobileResponseResult.GeneralFailure;
            }

            return pmResponse;
        }
        public Wallet_AddCCPaymentMethodResponse Wallet_AddCCPaymentMethodForRenter(string SessionId, string RenterId, string CCHolderName, string CCAccountNumber, string CCExpMonth, 
            string CCExpYear, string CardSecurityCode)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_AddCCPaymentMethodResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_AddCCPaymentMethodResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_AddCCPaymentMethodResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            // Verify Expiration month/year
            if (!ValidMonth(CCExpMonth))
                return new Wallet_AddCCPaymentMethodResponse { Result = MobileResponseResult.InvalidExpiration, Message = ServiceResponseMessages.InvalidExpiration };
            if (!ValidYear(CCExpYear))
                return new Wallet_AddCCPaymentMethodResponse { Result = MobileResponseResult.InvalidExpiration, Message = ServiceResponseMessages.InvalidExpiration };

            Wallet_AddCCPaymentMethodResponse pmResponse = new Wallet_AddCCPaymentMethodResponse();

            //Add the new data to the database ...
            try
            {
                int NewId = (int)EfxFramework.NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "wallet_AddCCPaymentMethodForRenter",
                     new[] { 
                    new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) ,
                    new SqlParameter("@CCHolderName", CCHolderName) ,
                    new SqlParameter("@CCAccountNumber", CCAccountNumber) ,
                    new SqlParameter("@CCExpMonth", CCExpMonth) ,
                    new SqlParameter("@CCExpYear", CCExpYear),
                    new SqlParameter("@Csc", CardSecurityCode)});

                pmResponse.Message = "Payment method successfully added.";
                pmResponse.Result = MobileResponseResult.Success;
                pmResponse.Wallet_PaymentMethodId = NewId;

                //Salcedo - 7/15/2016 - added call to queue
                QueueRenterAction(Convert.ToInt32(RenterId), 2);
            }
            catch (Exception ex)
            {
                pmResponse.Message = "Database call failed: " + ex.Message;
                pmResponse.Result = MobileResponseResult.GeneralFailure;
                pmResponse.Wallet_PaymentMethodId = 0;
            }

            return pmResponse;
        }
        public Wallet_AddACHPaymentMethodResponse Wallet_AddACHPaymentMethodForRenter(string SessionId, string RenterId, string AccountTypeId, string AccountNumber, string RoutingNumber)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_AddACHPaymentMethodResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_AddACHPaymentMethodResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_AddACHPaymentMethodResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            if (!ValidAccountTypeId(AccountTypeId))
                return new Wallet_AddACHPaymentMethodResponse { Result = MobileResponseResult.InvalidAccountTypeId, Message = ServiceResponseMessages.InvalidAccountTypeId };

            Wallet_AddACHPaymentMethodResponse pmResponse = new Wallet_AddACHPaymentMethodResponse();

            //Add the new data to the database ...
            try
            {
                int NewId = (int)EfxFramework.NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "wallet_AddACHPaymentMethodForRenter",
                     new[] { 
                    new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) ,
                    new SqlParameter("@AccountTypeId", Convert.ToInt32(AccountTypeId)) ,
                    new SqlParameter("@AccountNumber", AccountNumber) ,
                    new SqlParameter("@RoutingNumber", RoutingNumber) });

                pmResponse.Message = "Payment method successfully added.";
                pmResponse.Result = MobileResponseResult.Success;
                pmResponse.Wallet_PaymentMethodId = NewId;

                //Salcedo - 7/15/2016 - added call to queue
                QueueRenterAction(Convert.ToInt32(RenterId), 2);

            }
            catch (Exception ex)
            {
                pmResponse.Message = "Database call failed: " + ex.Message;
                pmResponse.Result = MobileResponseResult.GeneralFailure;
                pmResponse.Wallet_PaymentMethodId = 0;
            }

            return pmResponse;
        }
        
        public Wallet_UpdateCCPaymentMethodResponse Wallet_UpdateCCPaymentMethodForRenter(string SessionId, string RenterId, string PaymentMethodId, string CCHolderName, 
            string CCAccountNumber, string CCExpMonth, string CCExpYear, string CardSecurityCode)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_UpdateCCPaymentMethodResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_UpdateCCPaymentMethodResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_UpdateCCPaymentMethodResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            //Verify the PaymentMethodId is integer
            if (!ValidateInteger(PaymentMethodId))
                return new Wallet_UpdateCCPaymentMethodResponse { Result = MobileResponseResult.InvalidPaymentMethodId, Message = ServiceResponseMessages.InvalidPaymentMethodId };

            Wallet_UpdateCCPaymentMethodResponse pmResponse = new Wallet_UpdateCCPaymentMethodResponse();

            //Update the data in the database ...
            try
            {
                EfxFramework.NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "wallet_UpdateCCPaymentMethodForRenter",
                     new[] { 
                    new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) ,

                    //The @PaymentMethodId is used to update the correct record in the PayerCreditCard table (where PayerCreditCardId = @PaymentMethodId)
                    //It can be retrieved from the Pay_PayableControllerWallet table's PayerMethodId column.
                    new SqlParameter("@PaymentMethodId", Convert.ToInt32(PaymentMethodId)) , 

                    new SqlParameter("@CCHolderName", CCHolderName) ,
                    new SqlParameter("@CCAccountNumber", CCAccountNumber) ,
                    new SqlParameter("@CCExpMonth", CCExpMonth) ,
                    new SqlParameter("@CCExpYear", CCExpYear) });

                pmResponse.Message = "Payment method successfully updated.";
                pmResponse.Result = MobileResponseResult.Success;

                //Salcedo - 7/15/2016 - added call to queue
                QueueRenterAction(Convert.ToInt32(RenterId), 2);

            }
            catch (Exception ex)
            {
                pmResponse.Message = "Database call failed: " + ex.Message;
                pmResponse.Result = MobileResponseResult.GeneralFailure;
            }

            return pmResponse;
        }
        public Wallet_UpdateACHPaymentMethodResponse Wallet_UpdateACHPaymentMethodForRenter(string SessionId, string RenterId, string PaymentMethodId, string AccountTypeId, string AccountNumber, string RoutingNumber)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_UpdateACHPaymentMethodResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_UpdateACHPaymentMethodResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_UpdateACHPaymentMethodResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            //Verify the PaymentMethodId is integer
            if (!ValidateInteger(PaymentMethodId))
                return new Wallet_UpdateACHPaymentMethodResponse { Result = MobileResponseResult.InvalidPaymentMethodId, Message = ServiceResponseMessages.InvalidPaymentMethodId };

            if (!ValidAccountTypeId(AccountTypeId))
                return new Wallet_UpdateACHPaymentMethodResponse { Result = MobileResponseResult.InvalidAccountTypeId, Message = ServiceResponseMessages.InvalidAccountTypeId };

            Wallet_UpdateACHPaymentMethodResponse pmResponse = new Wallet_UpdateACHPaymentMethodResponse();

            //Update the data in the database ...
            try
            {
                EfxFramework.NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "wallet_UpdateAchPaymentMethodForRenter",
                     new[] { 
                    new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) ,

                    //The @PaymentMethodId is used to update the correct record in the PayerAch table (where PayerAchId = @PaymentMethodId)
                    //It can be retrieved from the Pay_PayableControllerWallet table's PayerMethodId column.
                    new SqlParameter("@PaymentMethodId", Convert.ToInt32(PaymentMethodId)) , 

                    new SqlParameter("@AccountTypeId", Convert.ToInt32(AccountTypeId)) ,
                    new SqlParameter("@AccountNumber", AccountNumber) ,
                    new SqlParameter("@RoutingNumber", RoutingNumber) });

                pmResponse.Message = "Payment method successfully updated.";
                pmResponse.Result = MobileResponseResult.Success;

                //Salcedo - 7/15/2016 - added call to queue
                QueueRenterAction(Convert.ToInt32(RenterId), 2);

            }
            catch (Exception ex)
            {
                pmResponse.Message = "Database call failed: " + ex.Message;
                pmResponse.Result = MobileResponseResult.GeneralFailure;
            }

            return pmResponse;
        }

        //cakel: 07/28/2016 Updated Code to accept PaymentMethod so that we can remove the item from the PayerACH or PayerCreditCard table
        //We need to know which table to reomve the item from since both tables contain an item id which is also an identity column.  there could be crossover of id's
        public Wallet_RemovePaymentMethodResponse Wallet_RemovePaymentMethodForRenter(string SessionId, string RenterId, string PaymentMethodId, string PaymentMethod)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_RemovePaymentMethodResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_RemovePaymentMethodResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_RemovePaymentMethodResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            Wallet_RemovePaymentMethodResponse pmResponse = new Wallet_RemovePaymentMethodResponse();

            //cakel: 07/28/2016 added code to perform action of removal

            try
            {
                RemoveWalletItemDBCommand(RenterId, PaymentMethodId, PaymentMethod);
                pmResponse.Message = "Payment method successfully removed.";
                pmResponse.Result = MobileResponseResult.Success;
            }
            catch(Exception ex)
            {
                pmResponse.Message = "Error: Payment method not removed." + ex.Message;
                pmResponse.Result = MobileResponseResult.FailToProcess;
            }

            //Salcedo - 7/15/2016 - added call to queue
            QueueRenterAction(Convert.ToInt32(RenterId), 2);


            return pmResponse;
        }

        //cakel: Added SQL Proc to Archive and Remove item from wallet table and payer tables
        public static void RemoveWalletItemDBCommand(string RenterID, string PaymentMethodID, string PaymentMethod)
        {
            SqlConnection con = new SqlConnection(EfxFramework.EfxSettings.ConnectionString);
            SqlCommand com = new SqlCommand("wallet_RemovePaymentMethodForRenter", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@RenterId", SqlDbType.Int).Value = RenterID;
            com.Parameters.Add("@PaymentMethodId", SqlDbType.Int).Value = PaymentMethodID;
            com.Parameters.Add("@PaymentMethod", SqlDbType.NVarChar, 10).Value = PaymentMethod;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Close();
            }
            catch(Exception ex)
            {
                con.Close();
                throw ex;
            }

        }



        private bool ValidateInteger(string StringIntegerValue)
        {
            int IntegerPaymentMethodId;
            bool ParseAttempt = Int32.TryParse(StringIntegerValue, out IntegerPaymentMethodId);
            if (!ParseAttempt)
                return false;
            else
                return true;
        }
        private bool ValidateDecimal(string StringDecimalValue)
        {
            decimal DecimalValue;
            bool ParseAttempt = decimal.TryParse(StringDecimalValue, out DecimalValue);
            if (!ParseAttempt)
                return false;
            else
                return true;
        }
        private bool ValidateBoolean(string StringBooleanValue)
        {
            bool BooleanValue;
            bool ParseAttempt = bool.TryParse(StringBooleanValue, out BooleanValue);
            if (!ParseAttempt)
                return false;
            else
                return true;
        }

        private bool GetPaymentMethodInfo(string PaymentMethodId, out bool PaymentMethod, out int PayerMethodId, out int PayerId, out int BankAccountTypeId)
        {
            int IntegerPaymentMethodId;
            bool ParseAttempt = Int32.TryParse(PaymentMethodId, out IntegerPaymentMethodId);

            SqlDataReader dr = EfxFramework.NewDataAccess.ExecuteProcedure(EfxSettings.ConnectionString, "wallet_GetPaymentMethodForRenter",
                 new[] { new SqlParameter("@PayerWalletId", IntegerPaymentMethodId) });
            PaymentMethod = false;
            PayerMethodId = 0;
            PayerId = 0;
            BankAccountTypeId = 0;
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    PaymentMethod = dr.GetBoolean(dr.GetOrdinal("PaymentMethod"));    // True = Credit Card payment method, False = ACH payment method
                    PayerMethodId = dr.GetInt32(dr.GetOrdinal("PayerMethodId"));
                    PayerId = dr.GetInt32(dr.GetOrdinal("PayerId"));
                    BankAccountTypeId = dr.GetInt32(dr.GetOrdinal("BankAccountTypeId")); // 0 = credit card, 1 = checking, 2 = savings
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        private bool GetPayableItemInfo(string PayableItemId, out string PaymentAmountDescription, out int PropertyId)
        {
            int IntegerPayableItemId;
            bool ParseAttempt = Int32.TryParse(PayableItemId, out IntegerPayableItemId);
            PaymentAmountDescription = "";
            PropertyId = 0;

            SqlDataReader dr = EfxFramework.NewDataAccess.ExecuteProcedure(EfxSettings.ConnectionString, "pay_GetPayableControllerItem",
                 new[] { new SqlParameter("@PayableItemId", IntegerPayableItemId) });
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    PaymentAmountDescription = dr.GetString(dr.GetOrdinal("Pay_Description"));
                    PropertyId = dr.GetInt32(dr.GetOrdinal("PropertyId"));
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        private Wallet_PaySingleItemResponse MakePaymentUsingWallet(string SessionId, string RenterId, string PayerWalletId, string PayableItemId, string AmountToPay, string CardSecurityCode)
        {
            bool PaymentMethod = false;
            int PayerMethodId = 0;
            int PayerId = 0;
            EfxFramework.PaymentMethods.PaymentType PaymentType;
            string BankAccountNumber = "";
            string BankRoutingNumber = "";
            string CreditCardHolderName = "";
            string CreditCardNumber = "";
            DateTime ExpirationDate = new DateTime(1995, 1, 1);
            decimal PaymentAmount = 0;
            string PaymentAmountDescription = "";
            string DisplayName = "";
            int PropertyId = 0;

            var TheRenter = new EfxFramework.Renter(Convert.ToInt32(RenterId));
            var TheProperty = EfxFramework.Property.GetPropertyByRenterId(TheRenter.RenterId);
            if (TheProperty.PropertyId.HasValue)
                PropertyId = TheProperty.PropertyId.Value;
            DisplayName = TheRenter.DisplayName;
            int BankAccountTypeId = 0;

            if (!GetPaymentMethodInfo(PayerWalletId, out PaymentMethod, out PayerMethodId, out PayerId, out BankAccountTypeId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPayerWalletId, Message = ServiceResponseMessages.InvalidPayerWalletId };

            if (PayerMethodId == 0 || PayerId == 0)
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPayerWalletId, Message = ServiceResponseMessages.InvalidPayerWalletId };

            if (!GetPayableItemInfo(PayableItemId, out PaymentAmountDescription, out PropertyId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPayableItemId, Message = ServiceResponseMessages.InvalidPayableItemId };

            PaymentAmount = Convert.ToDecimal(AmountToPay);

            // Setup payment method details

            if (PaymentMethod == true)
            {   // true = Credit card payment method
                var CreditCard = new EfxFramework.PayerCreditCard(PayerMethodId);
                PaymentType = EfxFramework.PaymentMethods.PaymentType.CreditCard;
                CreditCardHolderName = CreditCard.CreditCardHolderName;
                CreditCardNumber = CreditCard.CreditCardAccountNumber;
                ExpirationDate = new DateTime(CreditCard.CreditCardExpirationYear, CreditCard.CreditCardExpirationMonth, 1);
            }
            else
            {   // false = ACH payment method
                var ACHAccount = new EfxFramework.PayerAch(PayerMethodId);
                PaymentType = EfxFramework.PaymentMethods.PaymentType.ECheck;
                BankAccountNumber = ACHAccount.BankAccountNumber;
                BankRoutingNumber = ACHAccount.BankRoutingNumber;
            }


            // Make the payment
            return ProcessPaymentOnFramework(
                Convert.ToInt32(RenterId), PaymentType,
                BankAccountNumber, BankRoutingNumber,
                CreditCardHolderName, CreditCardNumber, CardSecurityCode, ExpirationDate,
                PaymentAmount, PaymentAmountDescription,
                DisplayName, PayerId, PropertyId);
        }

        private Wallet_PaySingleItemResponse ProcessPaymentOnFramework(int RenterId, EfxFramework.PaymentMethods.PaymentType PaymentType, string BankAccountNumber, string BankRoutingNumber,
            string CreditCardHolderName, string CreditCardNumber, string CardSecurityCode, DateTime ExpirationDate, decimal PaymentAmount, string PaymentAmountDescription,
            string DisplayName, int PayerId, int PropertyId)
        {
            // Make the payment
            EfxFramework.WebPayments.PaymentResponse PaymentResponse = ProcessPayment(
                RenterId, PaymentType,
                BankAccountNumber, BankRoutingNumber,
                CreditCardHolderName, CreditCardNumber, CardSecurityCode, ExpirationDate,
                PaymentAmount, PaymentAmountDescription,
                DisplayName, PayerId, PropertyId);

            // Return the result
            Wallet_PaySingleItemResponse pmResponse = new Wallet_PaySingleItemResponse();
            if (PaymentResponse.RentAndFeeResult == GeneralResponseResult.Success)
            {
                pmResponse.Message = "Payment successfully submitted.";
                pmResponse.Result = MobileResponseResult.Success;
                pmResponse.TransactionId = PaymentResponse.RentAndFeeTransactionId;
            }
            else
            {
                pmResponse.Message = PaymentResponse.RentAndFeeResponseMessage;
                pmResponse.Result = MobileResponseResult.GeneralFailure;
                pmResponse.TransactionId = "";
            }

            return pmResponse;

        }
        public Wallet_PaySingleItemResponse Wallet_PaySingleItemForRenter(string SessionId, string RenterId, string PayerWalletId, string PayableItemId, string AmountToPay)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            // Validate PayerWalletId is integer
            if (!ValidateInteger(PayerWalletId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPayerWalletId, Message = ServiceResponseMessages.InvalidPayerWalletId };

            // Validate PayableItemId is integer
            if (!ValidateInteger(PayableItemId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPayableItemId, Message = ServiceResponseMessages.InvalidPayableItemId };

            Wallet_PaySingleItemResponse TheResponse = MakePaymentUsingWallet(SessionId, RenterId, PayerWalletId, PayableItemId, AmountToPay, "");

            //Salcedo - 7/15/2016 - added call to queue
            QueueRenterAction(Convert.ToInt32(RenterId), 2);

            return TheResponse;
        }
        public Wallet_PaySingleItemResponse Wallet_PaySplitWithWalletAch(string SessionId, string RenterId, string PayerWalletId, string PayableItemId, string AmountToPay,
            string SplitPaymentGroupId, string SplitPaymentGroupCompleteFlag)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            // Validate PayerWalletId is integer
            if (!ValidateInteger(PayerWalletId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPayerWalletId, Message = ServiceResponseMessages.InvalidPayerWalletId };

            // Validate PayableItemId is integer
            if (!ValidateInteger(PayableItemId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPayableItemId, Message = ServiceResponseMessages.InvalidPayableItemId };

            if (!ValidGuidFormat(SplitPaymentGroupId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidSplitPaymentGroupId, Message = ServiceResponseMessages.InvalidSplitPaymentGroupId };

            if (!ValidateBoolean(SplitPaymentGroupCompleteFlag))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidSplitPaymentGroupCompleteFlag, Message = ServiceResponseMessages.InvalidSplitPaymentGroupCompleteFlag };

            // Validate AmoutToPay
            if (!ValidateDecimal(AmountToPay))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPaymentAmount, Message = ServiceResponseMessages.InvalidPaymentAmount };
            if (Convert.ToDouble(AmountToPay) <= 0)
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPaymentAmount, Message = ServiceResponseMessages.InvalidPaymentAmount };

            bool PaymentMethod = false;
            int PayerMethodId = 0;
            int PayerId = 0;
            string BankAccountNumber = "";
            string BankRoutingNumber = "";
            DateTime ExpirationDate = new DateTime(1995, 1, 1);
            decimal PaymentAmount = 0;
            string PaymentAmountDescription = "";
            int PropertyId = 0;
            int BankAccountTypeId = 0;

            if (!GetPaymentMethodInfo(PayerWalletId, out PaymentMethod, out PayerMethodId, out PayerId, out BankAccountTypeId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPayerWalletId, Message = ServiceResponseMessages.InvalidPayerWalletId };

            if (PaymentMethod) // PaymentMethod == true == Credit Card Payment method == not valid for this function
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPayerWalletId, Message = ServiceResponseMessages.InvalidPayerWalletId };

            if (PayerMethodId == 0 || PayerId == 0)
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPayerWalletId, Message = ServiceResponseMessages.InvalidPayerWalletId };

            if (!GetPayableItemInfo(PayableItemId, out PaymentAmountDescription, out PropertyId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPayableItemId, Message = ServiceResponseMessages.InvalidPayableItemId };

            PaymentAmount = Convert.ToDecimal(AmountToPay);

            // Setup payment method details
            var ACHAccount = new EfxFramework.PayerAch(PayerMethodId);
            BankAccountNumber = ACHAccount.BankAccountNumber;
            BankRoutingNumber = ACHAccount.BankRoutingNumber;

            // Validate routing number
            if (!EfxFramework.PaymentMethods.Ach.AchService.ValidateRoutingCheckDigit(BankRoutingNumber))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidRoutingNumber, Message = ServiceResponseMessages.InvalidRoutingNumber };

            // Validate account number
            if (String.IsNullOrEmpty(BankRoutingNumber))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidAccountNumber, Message = ServiceResponseMessages.InvalidAccountNumber };

            // Verify not duplicate transaction
            TransactionList tl = new TransactionList();
            ACH_Objects.Transaction TestTransaction = new ACH_Objects.Transaction();
            short Hours = Convert.ToInt16(EfxFramework.EfxSettings.DuplicateTransactionHourWindow); // 24;
            TestTransaction.RoutingNumber = BankRoutingNumber;
            TestTransaction.AccountNumber = BankAccountNumber;
            TestTransaction.TransactionAmount = Convert.ToDouble(AmountToPay);
            TestTransaction.TransactionType = 2; //2 = debit
            if (tl.HasMatchingTransaction(ref TestTransaction, ref Hours))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.DuplicateAchTransaction, Message = ServiceResponseMessages.DuplicateAchTransaction };

            Wallet_PaySingleItemResponse TheResponse = new Wallet_PaySingleItemResponse();

            // Store renter, payment amount, routing number, account number
            // Generate QueueId, queue entire ACH transaction, return ID to client
            string NewQueueId = GenerateUniqueQueueId();

            try
            {
                EfxFramework.NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "usp_QueuedAchTransactions_Insert",
                     new[] { 
                        new SqlParameter("@QueuedAchTransactionId", NewQueueId) ,
                        new SqlParameter("@SessionId", SessionId) ,
                        new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) ,
                        new SqlParameter("@PayableItemId", Convert.ToInt32(PayableItemId)) ,
                        new SqlParameter("@AmountToPay", Convert.ToDecimal(AmountToPay)) ,
                        new SqlParameter("@RoutingNumber", BankRoutingNumber) ,
                        new SqlParameter("@AccountNumber", BankAccountNumber) ,
                        new SqlParameter("@AccountTypeId", BankAccountTypeId) ,
                        new SqlParameter("@SplitPaymentGroupId", SplitPaymentGroupId) 
                     });

                //Queue the transaction for actual submission to the ACH system, as well as for updating to payment/transaction tables, email notification, etc.
                SendMessageToQueue("<ProcessAchQueue TransactionId='"  + NewQueueId + "' SplitPaymentGroupCompleteFlag='" + SplitPaymentGroupCompleteFlag + "' />");

                TheResponse.Message = "Payment successfully submitted.";
                TheResponse.Result = MobileResponseResult.Success;
                TheResponse.TransactionId = NewQueueId;
            }
            catch (Exception ex)
            {
                TheResponse.Message = "Error submitting eCheck transaction for payment.";
                TheResponse.Result = MobileResponseResult.GeneralFailure;
                TheResponse.TransactionId = "";
                var log = new RPO.ActivityLog();
                log.WriteLog("MobileApi", "Wallet_PaySplitWithWalletAch failed. SessionId=" + SessionId +
                    ". Exception: " + ex.ToString(), 1, "", false);
            }

            return TheResponse;
        }

        private string GenerateUniqueQueueId()
        {
            //This 16 digit value should be unique for over 10,000,000 uses
            //Ref: http://madskristensen.net/post/generate-unique-strings-and-numbers-in-c
            long i = 1;
            foreach (byte b in Guid.NewGuid().ToByteArray())
            {
                i *= ((int)b + 1);
            }

            string ReturnValue = string.Format("{0:x}", i - DateTime.Now.Ticks);

            //Add the year, day of year, and hour to make value even more unique, providing for at at least 10,000,000 unique values per hour
            ReturnValue = DateTime.Now.Year.ToString().Substring(2,2) + DateTime.Now.DayOfYear.ToString("000") + DateTime.Now.Hour.ToString("00") + "-" + ReturnValue;

            return ReturnValue;
        }

        //This method used to provide a card security code with credit card payment
        public Wallet_PaySingleItemResponse Wallet_PaySingleItemForRenterWithCSC(string SessionId, string RenterId, string PayerWalletId, string PayableItemId, string AmountToPay,
            string CardSecurityCode)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            // Validate PaymentMethodId is integer
            if (!ValidateInteger(PayerWalletId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPayerWalletId, Message = ServiceResponseMessages.InvalidPayerWalletId };

            // Validate PayableItemId is integer
            if (!ValidateInteger(PayableItemId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPayableItemId, Message = ServiceResponseMessages.InvalidPayableItemId };

            Wallet_PaySingleItemResponse TheResponse = MakePaymentUsingWallet(SessionId, RenterId, PayerWalletId, PayableItemId, AmountToPay, CardSecurityCode);
            
            //Salcedo - 7/15/2016 - added call to queue
            QueueRenterAction(Convert.ToInt32(RenterId), 2);

            return TheResponse;
        }
 


        //This method used to make a payment with a new credit card
        public Wallet_PaySingleItemResponse Wallet_PaySingleItemForRenterWithNewCC(string SessionId, string RenterId, string PayableItemId, string AmountToPay,
            string CCHolderName, string CCAccountNumber, string CCExpMonth, string CCExpYear, string CardSecurityCode)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            // Validate PayableItemId is integer
            if (!ValidateInteger(PayableItemId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPayableItemId, Message = ServiceResponseMessages.InvalidPayableItemId };

            Wallet_PaySingleItemResponse pmResponse = new Wallet_PaySingleItemResponse();

            //Make the payment ...
            int PayerId = 0;
            EfxFramework.PaymentMethods.PaymentType PaymentType = EfxFramework.PaymentMethods.PaymentType.CreditCard;
            string BankAccountNumber = "";
            string BankRoutingNumber = "";
            string CreditCardHolderName = CCHolderName;
            string CreditCardNumber = CCAccountNumber;
            DateTime ExpirationDate = new DateTime(Convert.ToInt32(CCExpYear), Convert.ToInt32(CCExpMonth), 1);
            decimal PaymentAmount = Convert.ToDecimal(AmountToPay);
            string PaymentAmountDescription = "";
            string DisplayName = "";
            int PropertyId = 0;

            var TheRenter = new EfxFramework.Renter(Convert.ToInt32(RenterId));
            var TheProperty = EfxFramework.Property.GetPropertyByRenterId(TheRenter.RenterId);
            //SWitherspoon 08-31-2017: PayerID was not being set to a proper value instead of 0, causing ghost payments. Added code to use PayerID of TheRenter to avoid this issue
            if (TheProperty.PropertyId.HasValue)
            { PropertyId = TheProperty.PropertyId.Value; }
            if (TheRenter.PayerId.HasValue)
            { PayerId = TheRenter.PayerId.Value; }
            DisplayName = TheRenter.DisplayName;

            pmResponse = ProcessPaymentOnFramework(
                Convert.ToInt32(RenterId), PaymentType,
                BankAccountNumber, BankRoutingNumber,
                CreditCardHolderName, CreditCardNumber, CardSecurityCode, ExpirationDate,
                PaymentAmount, PaymentAmountDescription,
                DisplayName, PayerId, PropertyId);

            if (pmResponse.Result == MobileResponseResult.Success)
            {
                //Payment was successful, so save the new CC payment method
                Wallet_AddCCPaymentMethodResponse TheAddResponse = 
                    Wallet_AddCCPaymentMethodForRenter(SessionId, RenterId, CCHolderName, CCAccountNumber, CCExpMonth, CCExpYear, CardSecurityCode);

                //Salcedo - 7/15/2016 - added call to queue
                QueueRenterAction(Convert.ToInt32(RenterId), 2);

                if (TheAddResponse.Result != MobileResponseResult.Success)
                {
                    pmResponse.Message = "Payment processed, but payer information was not updated.";
                    pmResponse.Result = MobileResponseResult.PaymentProcessedPayerNotSaved;

                    return pmResponse;
                }
            }

            return pmResponse;
            
        }

        //This method used to make a payment with a new checking/savings account
        public Wallet_PaySingleItemResponse Wallet_PaySingleItemForRenterWithNewAch(string SessionId, string RenterId, string PayableItemId, string AmountToPay,
            string AccountTypeId, string AccountNumber, string RoutingNumber)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            Wallet_PaySingleItemResponse pmResponse = new Wallet_PaySingleItemResponse();

            //Make the payment ...
            int PayerId = 0;
            EfxFramework.PaymentMethods.PaymentType PaymentType = EfxFramework.PaymentMethods.PaymentType.ECheck;
            string BankAccountNumber = AccountNumber;
            string BankRoutingNumber = RoutingNumber;
            string CreditCardHolderName = "";
            string CreditCardNumber = "";
            DateTime ExpirationDate = new DateTime(1995, 1, 1);
            decimal PaymentAmount = Convert.ToDecimal(AmountToPay);
            string PaymentAmountDescription = "";
            string DisplayName = "";
            int PropertyId = 0;

            var TheRenter = new EfxFramework.Renter(Convert.ToInt32(RenterId));
            var TheProperty = EfxFramework.Property.GetPropertyByRenterId(TheRenter.RenterId);
            //SWitherspoon 08-31-2017: PayerID was not being set to a proper value instead of 0, causing ghost payments. Added code to use PayerID of TheRenter to avoid this issue
            if (TheProperty.PropertyId.HasValue)
            { PropertyId = TheProperty.PropertyId.Value; }
            if (TheRenter.PayerId.HasValue)
            { PayerId = TheRenter.PayerId.Value; }
            DisplayName = TheRenter.DisplayName;

            pmResponse = ProcessPaymentOnFramework(
                Convert.ToInt32(RenterId), PaymentType,
                BankAccountNumber, BankRoutingNumber,
                CreditCardHolderName, CreditCardNumber, "", ExpirationDate,
                PaymentAmount, PaymentAmountDescription,
                DisplayName, PayerId, PropertyId);

            if (pmResponse.Result == MobileResponseResult.Success)
            {
                //Payment was successful, so save the new ACH payment method
                Wallet_AddACHPaymentMethodResponse TheAddResponse =
                    Wallet_AddACHPaymentMethodForRenter(SessionId, RenterId, AccountTypeId, AccountNumber, RoutingNumber);

                //Salcedo - 7/15/2016 - added call to queue
                QueueRenterAction(Convert.ToInt32(RenterId), 2);

                if (TheAddResponse.Result != MobileResponseResult.Success)
                {
                    pmResponse.Message = "Payment processed, but payer information was not updated.";
                    pmResponse.Result = MobileResponseResult.PaymentProcessedPayerNotSaved;
                    return pmResponse;
                }
            }

            return pmResponse;
        }
        public Wallet_PaySingleItemResponse Wallet_PaySplitWithNewAch(string SessionId, string RenterId, string PayableItemId, string AmountToPay,
            string AccountTypeId, string AccountNumber, string RoutingNumber, string SplitPaymentGroupId, string SplitPaymentGroupCompleteFlag)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            // Validate PayableItemId is integer
            if (!ValidateInteger(PayableItemId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPayableItemId, Message = ServiceResponseMessages.InvalidPayableItemId };

            // Validate AccountTypeId
            if (AccountTypeId != "1" && AccountTypeId != "2")
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidAccountTypeId, Message = ServiceResponseMessages.InvalidAccountTypeId };

            if (!ValidGuidFormat(SplitPaymentGroupId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidSplitPaymentGroupId, Message = ServiceResponseMessages.InvalidSplitPaymentGroupId };

            if (!ValidateBoolean(SplitPaymentGroupCompleteFlag))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidSplitPaymentGroupCompleteFlag, Message = ServiceResponseMessages.InvalidSplitPaymentGroupCompleteFlag };

            // Validate AmoutToPay
            if (!ValidateDecimal(AmountToPay))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPaymentAmount, Message = ServiceResponseMessages.InvalidPaymentAmount };
            if (Convert.ToDouble(AmountToPay) <= 0)
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPaymentAmount, Message = ServiceResponseMessages.InvalidPaymentAmount };

            //Queue the payment ...
            string BankAccountNumber = AccountNumber;
            string BankRoutingNumber = RoutingNumber;
            string BankAccountTypeId = AccountTypeId;
            decimal PaymentAmount = Convert.ToDecimal(AmountToPay);

            // Validate routing number
            if (!EfxFramework.PaymentMethods.Ach.AchService.ValidateRoutingCheckDigit(BankRoutingNumber))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidRoutingNumber, Message = ServiceResponseMessages.InvalidRoutingNumber };

            // Validate account number
            if (String.IsNullOrEmpty(BankRoutingNumber))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidAccountNumber, Message = ServiceResponseMessages.InvalidAccountNumber };

            // Verify not duplicate transaction
            TransactionList tl = new TransactionList();
            ACH_Objects.Transaction TestTransaction = new ACH_Objects.Transaction();
            short Hours = Convert.ToInt16(EfxFramework.EfxSettings.DuplicateTransactionHourWindow); // 24;
            TestTransaction.RoutingNumber = BankRoutingNumber;
            TestTransaction.AccountNumber = BankAccountNumber;
            TestTransaction.TransactionAmount = Convert.ToDouble(AmountToPay);
            TestTransaction.TransactionType = 2; //2 = debit
            if (tl.HasMatchingTransaction(ref TestTransaction, ref Hours))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.DuplicateAchTransaction, Message = ServiceResponseMessages.DuplicateAchTransaction };

            Wallet_PaySingleItemResponse TheResponse = new Wallet_PaySingleItemResponse();

            // Generate QueueId, queue entire ACH transaction, return this ID to client
            string NewQueueId = GenerateUniqueQueueId();
            TheResponse.TransactionId = NewQueueId;

            try
            {
                // Store renter, payment amount, routing number, account number, etc. so that this payment can be processed by the queue processor service
                EfxFramework.NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "usp_QueuedAchTransactions_Insert",
                     new[] { 
                        new SqlParameter("@QueuedAchTransactionId", NewQueueId) ,
                        new SqlParameter("@SessionId", SessionId) ,
                        new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) ,
                        new SqlParameter("@PayableItemId", Convert.ToInt32(PayableItemId)) ,
                        new SqlParameter("@AmountToPay", Convert.ToDecimal(AmountToPay)) ,
                        new SqlParameter("@RoutingNumber", BankRoutingNumber) ,
                        new SqlParameter("@AccountNumber", BankAccountNumber) ,
                        new SqlParameter("@AccountTypeId", Convert.ToInt32(BankAccountTypeId)) ,
                        new SqlParameter("@SplitPaymentGroupId", SplitPaymentGroupId) 
                     });

                //Queue the transaction for actual submission to the ACH system, as well as for updating to payment/transaction tables, email notification, etc.
                SendMessageToQueue("<ProcessAchQueue TransactionId='" + NewQueueId + "' SplitPaymentGroupCompleteFlag='" + SplitPaymentGroupCompleteFlag + "' />");

                TheResponse.Message = "Payment successfully submitted.";
                TheResponse.Result = MobileResponseResult.Success;

                //Now, save the new wallet payment method for future use
                Wallet_AddACHPaymentMethodResponse TheAddResponse =
                    Wallet_AddACHPaymentMethodForRenter(SessionId, RenterId, AccountTypeId, AccountNumber, RoutingNumber);

                //To synchronize the wallet so it has the new payment method in it
                QueueRenterAction(Convert.ToInt32(RenterId), 2);

                if (TheAddResponse.Result != MobileResponseResult.Success)
                {
                    TheResponse.Message = "Payment processed, but payer information was not updated.";
                    TheResponse.Result = MobileResponseResult.PaymentProcessedPayerNotSaved;
                }
            }
            catch (Exception ex)
            {
                TheResponse.Message = "Error submitting new eCheck transaction for payment.";
                TheResponse.Result = MobileResponseResult.GeneralFailure;
                var log = new RPO.ActivityLog();
                log.WriteLog("MobileApi", "Wallet_PaySplitWithNewAch failed. SessionId=" + SessionId +
                    ". Exception: " + ex.ToString(), 1, "", false);
            }

            return TheResponse;
        }

        public Wallet_PaySingleItemResponse Wallet_PaySingleItemForRenterWithUpdateToCCMethod(string SessionId, string RenterId, string PaymentMethodId, string PayableItemId, string AmountToPay,
            string CCHolderName, string CCAccountNumber, string CCExpMonth, string CCExpYear, string CardSecurityCode)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            // Validate PayableItemId is integer
            if (!ValidateInteger(PayableItemId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPayableItemId, Message = ServiceResponseMessages.InvalidPayableItemId };

            //Verify the PaymentMethodId is integer
            if (!ValidateInteger(PaymentMethodId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidPaymentMethodId, Message = ServiceResponseMessages.InvalidPaymentMethodId };

            Wallet_PaySingleItemResponse pmResponse = new Wallet_PaySingleItemResponse();

            //Make the payment ...
            int PayerId = 0;
            EfxFramework.PaymentMethods.PaymentType PaymentType = EfxFramework.PaymentMethods.PaymentType.CreditCard;
            string BankAccountNumber = "";
            string BankRoutingNumber = "";
            string CreditCardHolderName = CCHolderName; 
            string CreditCardNumber = CCAccountNumber; 
            DateTime ExpirationDate = new DateTime(Convert.ToInt32(CCExpYear), Convert.ToInt32(CCExpMonth), 1);
            decimal PaymentAmount = Convert.ToDecimal(AmountToPay);
            string PaymentAmountDescription = "";
            string DisplayName = "";
            int PropertyId = 0;

            var TheRenter = new EfxFramework.Renter(Convert.ToInt32(RenterId));
            var TheProperty = EfxFramework.Property.GetPropertyByRenterId(TheRenter.RenterId);
            //SWitherspoon 08-31-2017: PayerID was not being set to a proper value instead of 0, causing ghost payments. Added code to use PayerID of TheRenter to avoid this issue
            if (TheProperty.PropertyId.HasValue)
            { PropertyId = TheProperty.PropertyId.Value; }
            if (TheRenter.PayerId.HasValue)
            { PayerId = TheRenter.PayerId.Value; }
            DisplayName = TheRenter.DisplayName;

            pmResponse = ProcessPaymentOnFramework(
                Convert.ToInt32(RenterId), PaymentType,
                BankAccountNumber, BankRoutingNumber,
                CreditCardHolderName, CreditCardNumber, CardSecurityCode, ExpirationDate,
                PaymentAmount, PaymentAmountDescription,
                DisplayName, PayerId, PropertyId);

            if (pmResponse.Result == MobileResponseResult.Success)
            {
                //Payment was successful, so update the CC payment method
                Wallet_UpdateCCPaymentMethodResponse TheUpdateResponse =
                    Wallet_UpdateCCPaymentMethodForRenter(SessionId, RenterId, PaymentMethodId, CCHolderName, CCAccountNumber, CCExpMonth, CCExpYear, CardSecurityCode);

                //Salcedo - 7/15/2016 - added call to queue
                QueueRenterAction(Convert.ToInt32(RenterId), 2);

                if (TheUpdateResponse.Result != MobileResponseResult.Success)
                {
                    pmResponse.Message = "Payment processed, but payer information was not updated.";
                    pmResponse.Result = MobileResponseResult.PaymentProcessedPayerNotSaved;
                    return pmResponse;
                }
            }

            return pmResponse;
        }
        public Wallet_PaySingleItemResponse Wallet_PaySingleItemForRenterWithUpdateToAchMethod(string SessionId, string RenterId, string PaymentMethodId, string PayableItemId, string AmountToPay,
            string AccountTypeId, string AccountNumber, string RoutingNumber)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_PaySingleItemResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            Wallet_PaySingleItemResponse pmResponse = new Wallet_PaySingleItemResponse();

            //Make the payment ...
            int PayerId = 0;
            EfxFramework.PaymentMethods.PaymentType PaymentType = EfxFramework.PaymentMethods.PaymentType.ECheck;
            string BankAccountNumber = AccountNumber; 
            string BankRoutingNumber = RoutingNumber; 
            string CreditCardHolderName = "";
            string CreditCardNumber = "";
            DateTime ExpirationDate = new DateTime(1995, 1, 1);
            decimal PaymentAmount = Convert.ToDecimal(AmountToPay);
            string PaymentAmountDescription = "";
            string DisplayName = "";
            int PropertyId = 0;

            var TheRenter = new EfxFramework.Renter(Convert.ToInt32(RenterId));
            var TheProperty = EfxFramework.Property.GetPropertyByRenterId(TheRenter.RenterId);
            //SWitherspoon 08-31-2017: PayerID was not being set to a proper value instead of 0, causing ghost payments. Added code to use PayerID of TheRenter to avoid this issue
            if (TheProperty.PropertyId.HasValue)
            { PropertyId = TheProperty.PropertyId.Value; }
            if (TheRenter.PayerId.HasValue)
            { PayerId = TheRenter.PayerId.Value; }
            DisplayName = TheRenter.DisplayName;

            pmResponse = ProcessPaymentOnFramework(
                Convert.ToInt32(RenterId), PaymentType,
                BankAccountNumber, BankRoutingNumber,
                CreditCardHolderName, CreditCardNumber, "", ExpirationDate,
                PaymentAmount, PaymentAmountDescription,
                DisplayName, PayerId, PropertyId);

            if (pmResponse.Result == MobileResponseResult.Success)
            {
                //Payment was successful, so update the ACH payment method
                Wallet_UpdateACHPaymentMethodResponse TheUpdateResponse =
                    Wallet_UpdateACHPaymentMethodForRenter(SessionId, RenterId, PaymentMethodId, AccountTypeId, AccountNumber, RoutingNumber);

                //Salcedo - 7/15/2016 - added call to queue
                QueueRenterAction(Convert.ToInt32(RenterId), 2);

                if (TheUpdateResponse.Result != MobileResponseResult.Success)
                {
                    pmResponse.Message = "Payment processed, but payer information was not updated.";
                    pmResponse.Result = MobileResponseResult.PaymentProcessedPayerNotSaved;
                    return pmResponse;
                }
            }

            return pmResponse;
        }
        public Wallet_GetRenterPaymentHistoryResponse Wallet_GetRenterPaymentHistory(string SessionId, string RenterId)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_GetRenterPaymentHistoryResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_GetRenterPaymentHistoryResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_GetRenterPaymentHistoryResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            Wallet_GetRenterPaymentHistoryResponse Response;
            Response = new Wallet_GetRenterPaymentHistoryResponse();

            Response.Wallet_PaymentHistoryItems = new List<EfxFramework.Wallet_PaymentHistoryItem>();

            try
            {
                SqlDataReader dr = EfxFramework.NewDataAccess.ExecuteProcedure(EfxSettings.ConnectionString, "Wallet_GetPaymentHistoryItemsForRenter",
                     new[] { new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Wallet_PaymentHistoryItem ThePaymentHistoryItem = new Wallet_PaymentHistoryItem();

                        ThePaymentHistoryItem.PaymentDate = dr.GetDateTime(dr.GetOrdinal("PaymentDate"));
                        ThePaymentHistoryItem.PaymentItemDescription = dr.GetString(dr.GetOrdinal("PaymentItemDescription"));
                        ThePaymentHistoryItem.PaymentMethod = dr.GetString(dr.GetOrdinal("PaymentMethod"));
                        ThePaymentHistoryItem.PaymentMethodDescription = dr.GetString(dr.GetOrdinal("PaymentMethodDescription"));
                        ThePaymentHistoryItem.PaymountAmount = dr.GetDecimal(dr.GetOrdinal("PaymentAmount"));

                        Response.Wallet_PaymentHistoryItems.Add(ThePaymentHistoryItem);
                    }
                    Response.Message = "Successfully retrieved all payment history items.";
                    Response.Result = MobileResponseResult.Success;
                }
                else
                {
                    Wallet_PaymentHistoryItem ThePaymentHistoryItem = new Wallet_PaymentHistoryItem();
                    ThePaymentHistoryItem.PaymentItemDescription = "No payment history items found.";
                    Response.Wallet_PaymentHistoryItems.Add(ThePaymentHistoryItem);

                    Response.Message = "No payment history items found.";
                    Response.Result = MobileResponseResult.NoPaymentHistoryItems;
                }

                dr.Close();
            }
            catch (Exception ex)
            {
                Response.Message = "Error reading database: " + ex.Message;
                Response.Result = MobileResponseResult.GeneralFailure;
            }

            return Response;
        }




        //Salcedo - 7/18/2016 - added new Pre/Post Auth methods
        public Wallet_PreAuthCCResponse Wallet_PreAuthItemForRenterCC(string SessionId, string RenterId, string PayerWalletId, string PayableItemId, string AmountToPay, 
            string SplitPaymentGroupId, string CardSecurityCode = "")
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            // Validate PaymentMethodId is integer
            if (!ValidateInteger(PayerWalletId))
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.InvalidPayerWalletId, Message = ServiceResponseMessages.InvalidPayerWalletId };

            // Validate PayableItemId is integer
            if (!ValidateInteger(PayableItemId))
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.InvalidPayableItemId, Message = ServiceResponseMessages.InvalidPayableItemId };

            // Validate AmoutToPay
            if (!ValidateDecimal(AmountToPay))
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.InvalidPaymentAmount, Message = ServiceResponseMessages.InvalidPaymentAmount };

            if (!ValidGuidFormat(SplitPaymentGroupId))
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.InvalidSplitPaymentGroupId, Message = ServiceResponseMessages.InvalidSplitPaymentGroupId };

            bool PaymentMethod = false;
            int PayerMethodId = 0;
            int PayerId = 0;
            string PaymentAmountDescription = "";
            int PropertyId = 0;
            int BankAccountTypeId = 0;

            if (!GetPaymentMethodInfo(PayerWalletId, out PaymentMethod, out PayerMethodId, out PayerId, out BankAccountTypeId))
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.InvalidPayerWalletId, Message = ServiceResponseMessages.InvalidPayerWalletId };

            if (PaymentMethod != true) // This function is only valid for Credit Cards. PaymentMethod = true for Credit Cards, false for eCheck
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.InvalidPayerWalletId, Message = ServiceResponseMessages.InvalidPayerWalletId };

            if (PayerMethodId == 0 || PayerId == 0)
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.InvalidPayerWalletId, Message = ServiceResponseMessages.InvalidPayerWalletId };

            if (!GetPayableItemInfo(PayableItemId, out PaymentAmountDescription, out PropertyId))
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.InvalidPayableItemId, Message = ServiceResponseMessages.InvalidPayableItemId };

            var CreditCard = new EfxFramework.PayerCreditCard(PayerMethodId);

            //Fields for preauthorize
            string gatewayUsername = EfxSettings.CreditCardProcessorCustomerId;
            string gatewayPassword = EfxSettings.CreditCardProcessorCustomerPassword;
            bool isTestUser = !EfxSettings.ProcessCreditCards || EfxSettings.UseTestMode;
            decimal amount = Convert.ToDecimal(AmountToPay);
            string creditCardAccountNumber = CreditCard.CreditCardAccountNumber;
            string creditCardExpirationDate = CreditCard.CreditCardExpirationMonth.ToString("D2") + CreditCard.CreditCardExpirationYear.ToString().Substring(2,2);
            string cvv = CardSecurityCode;
            string creditCardHolderName = CreditCard.CreditCardHolderName;
            List<CustomField> customFields = null;

            CreditCardPaymentResponse PreAuthResponse =
                EfxFramework.PaymentMethods.CreditCard.CreditCardPayment.PreAuthorizePayment(gatewayUsername, gatewayPassword, isTestUser, amount,
                    creditCardAccountNumber, creditCardExpirationDate, cvv, creditCardHolderName, customFields);

            Wallet_PreAuthCCResponse TheResponse = new Wallet_PreAuthCCResponse();
            TheResponse.TransactionId = "";

            if (PreAuthResponse.Result == GeneralResponseResult.Success)
            {
                TheResponse.Result = MobileResponseResult.Success;
                TheResponse.Message = PreAuthResponse.ResponseMessage;
                TheResponse.TransactionId = PreAuthResponse.TransactionId;

                try
                {
                    EfxFramework.NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "usp_PreAuthTransactions_Insert",
                        new[] { 
                            new SqlParameter("@ExternalPreAuthTransactionId", TheResponse.TransactionId),
                            new SqlParameter("@SessionId", SessionId),
                            new SqlParameter("@RenterId", Convert.ToInt32(RenterId)),
                            new SqlParameter("@PayableItemId", Convert.ToInt32(PayableItemId)),
                            new SqlParameter("@AmountToPay", Convert.ToDecimal(AmountToPay)),
                            new SqlParameter("@CreditCardHolderName", creditCardHolderName),
                            new SqlParameter("@CreditCardAccountNumber", creditCardAccountNumber),
                            new SqlParameter("@CreditCardExpirationMonth", CreditCard.CreditCardExpirationMonth),
                            new SqlParameter("@CreditCardExpirationYear", CreditCard.CreditCardExpirationYear),
                            new SqlParameter("@SplitPaymentGroupId", SplitPaymentGroupId)
                        });
                }
                catch (Exception ex)
                {
                    // We were unable to record details of the preauth
                    var log = new RPO.ActivityLog();
                    log.WriteLog("MobileApi", "usp_PreAuthTransactions_Insert Failed. ExternalPreAuthTransactionId=" + PreAuthResponse.TransactionId + 
                        ". Exception: " + ex.ToString(), 1, "", false);
                    TheResponse.Result = MobileResponseResult.GeneralFailure;
                    TheResponse.Message = "PreAuthorizePayment was not successful";

                    // Since we could not store the preauth details, we need to attempt a reversal on the successful preauth
                    try
                    { 
                        CreditCardPaymentResponse ReversalResponse =
                            EfxFramework.PaymentMethods.CreditCard.CreditCardPayment.ReverseAuthorizedCreditCardPayment(
                                gatewayUsername, gatewayPassword, PreAuthResponse.TransactionId, amount, isTestUser);
                        log.WriteLog("MobileApi", "ReverseAuthorizedCreditCardPayment Response: " + ReversalResponse.ResponseMessage, 1, "", false);
                    }
                    catch (Exception exx)
                    {
                        log.WriteLog("MobileApi", "Attempt to ReverseAuthorizedCreditCardPayment failed. ExternalPreAuthTransactionId=" + PreAuthResponse.TransactionId + 
                            ". Exception: " + exx.ToString(), 1, "", false);
                    }
                }
            }
            else
            {
                TheResponse.Result = MobileResponseResult.GeneralFailure;
                TheResponse.Message = PreAuthResponse.ResponseMessage;
                var log = new RPO.ActivityLog();
                log.WriteLog("MobileApi", "Wallet_PreAuthItemForRenterCC failed.", 1, "", false);
            }

            return TheResponse;
        }
        public  Wallet_PreAuthCCResponse Wallet_PreAuthItemForRenterWithNewCC(string SessionId, string RenterId, string PayableItemId, string AmountToPay,
            string CCHolderName, string CCAccountNumber, string CCExpMonth, string CCExpYear, string SplitPaymentGroupId, string CardSecurityCode = "")
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            // Validate PayableItemId is integer
            if (!ValidateInteger(PayableItemId))
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.InvalidPayableItemId, Message = ServiceResponseMessages.InvalidPayableItemId };

            // Validate AmoutToPay
            if (!ValidateDecimal(AmountToPay))
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.InvalidPaymentAmount, Message = ServiceResponseMessages.InvalidPaymentAmount };

            if (!ValidGuidFormat(SplitPaymentGroupId))
                return new Wallet_PreAuthCCResponse { Result = MobileResponseResult.InvalidSplitPaymentGroupId, Message = ServiceResponseMessages.InvalidSplitPaymentGroupId };

            //Fields for preauthorize
            string gatewayUsername = EfxSettings.CreditCardProcessorCustomerId;
            string gatewayPassword = EfxSettings.CreditCardProcessorCustomerPassword;
            bool isTestUser = !EfxSettings.ProcessCreditCards || EfxSettings.UseTestMode;
            decimal amount = Convert.ToDecimal(AmountToPay);
            string creditCardAccountNumber = CCAccountNumber;
            string creditCardExpirationDate = ("00" + CCExpMonth).Substring(("00" + CCExpMonth).Length-2) +
                ("00" + CCExpYear).Substring(("00" + CCExpYear).Length - 2); 
            string cvv = CardSecurityCode;
            string creditCardHolderName = CCHolderName;
            List<CustomField> customFields = null;

            CreditCardPaymentResponse PreAuthResponse =
                EfxFramework.PaymentMethods.CreditCard.CreditCardPayment.PreAuthorizePayment(gatewayUsername, gatewayPassword, isTestUser, amount,
                    creditCardAccountNumber, creditCardExpirationDate, cvv, creditCardHolderName, customFields);

            Wallet_PreAuthCCResponse TheResponse = new Wallet_PreAuthCCResponse();
            TheResponse.TransactionId = "";

            if (PreAuthResponse.Result == GeneralResponseResult.Success)
            {
                TheResponse.Result = MobileResponseResult.Success;
                TheResponse.Message = PreAuthResponse.ResponseMessage;
                TheResponse.TransactionId = PreAuthResponse.TransactionId;

                //PreAuth was successful, so save the new CC payment method
                Wallet_AddCCPaymentMethodResponse TheAddResponse =
                    Wallet_AddCCPaymentMethodForRenter(SessionId, RenterId, CCHolderName, CCAccountNumber, CCExpMonth, CCExpYear, CardSecurityCode);

                QueueRenterAction(Convert.ToInt32(RenterId), 2);

                if (TheAddResponse.Result != MobileResponseResult.Success)
                {
                    TheResponse.Result = MobileResponseResult.GeneralFailure;
                    TheResponse.Message = "Unable to store new wallet payment method.";

                    var log = new RPO.ActivityLog();
                    log.WriteLog("MobileApi", "Unable to store new wallet payment method. Response: " + TheAddResponse.Message, 1, "", false);
                    try
                    {
                        CreditCardPaymentResponse ReversalResponse =
                            EfxFramework.PaymentMethods.CreditCard.CreditCardPayment.ReverseAuthorizedCreditCardPayment(
                                gatewayUsername, gatewayPassword, PreAuthResponse.TransactionId, amount, isTestUser);
                        
                        log.WriteLog("MobileApi", "ReverseAuthorizedCreditCardPayment Response: " + ReversalResponse.ResponseMessage, 1, "", false);
                    }
                    catch (Exception exx)
                    {
                        log.WriteLog("MobileApi", "Attempt to ReverseAuthorizedCreditCardPayment failed. ExternalPreAuthTransactionId=" + PreAuthResponse.TransactionId +
                            ". Exception: " + exx.ToString(), 1, "", false);
                    }
                }
                else
                {
                    try
                    {
                        EfxFramework.NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "usp_PreAuthTransactions_Insert",
                            new[] { 
                            new SqlParameter("@ExternalPreAuthTransactionId", PreAuthResponse.TransactionId),
                            new SqlParameter("@SessionId", SessionId),
                            new SqlParameter("@RenterId", Convert.ToInt32(RenterId)),
                            new SqlParameter("@PayableItemId", Convert.ToInt32(PayableItemId)),
                            new SqlParameter("@AmountToPay", Convert.ToDecimal(AmountToPay)),
                            new SqlParameter("@CreditCardHolderName", creditCardHolderName),
                            new SqlParameter("@CreditCardAccountNumber", creditCardAccountNumber),
                            new SqlParameter("@CreditCardExpirationMonth", Convert.ToInt32(CCExpMonth)),
                            new SqlParameter("@CreditCardExpirationYear", Convert.ToInt32(CCExpYear)),
                            new SqlParameter("@SplitPaymentGroupId", SplitPaymentGroupId)
                        });
                    }
                    catch (Exception ex)
                    {
                        // We were unable to record details of the preauth
                        var log = new RPO.ActivityLog();
                        log.WriteLog("MobileApi", "usp_PreAuthTransactions_Insert Failed. ExternalPreAuthTransactionId=" + PreAuthResponse.TransactionId +
                            ". Exception: " + ex.ToString(), 1, "", false);
                        TheResponse.Result = MobileResponseResult.GeneralFailure;
                        TheResponse.Message = "Unable to save preauthorization payment details.";

                        // Since we could not store the preauth details, we need to attempt a reversal on the successful preauth
                        try
                        {
                            CreditCardPaymentResponse ReversalResponse =
                                EfxFramework.PaymentMethods.CreditCard.CreditCardPayment.ReverseAuthorizedCreditCardPayment(
                                    gatewayUsername, gatewayPassword, PreAuthResponse.TransactionId, amount, isTestUser);
                            log.WriteLog("MobileApi", "ReverseAuthorizedCreditCardPayment Response: " + ReversalResponse.ResponseMessage, 1, "", false);
                        }
                        catch (Exception exx)
                        {
                            log.WriteLog("MobileApi", "Attempt to ReverseAuthorizedCreditCardPayment failed. ExternalPreAuthTransactionId=" + PreAuthResponse.TransactionId +
                                ". Exception: " + exx.ToString(), 1, "", false);
                        }
                    }
                }
            }

            return TheResponse;
        }
        public Wallet_PostAuthCCResponse Wallet_PostAuthItemForRenter(string SessionId, string RenterId, string TransactionId, string SplitPaymentGroupCompleteFlag)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_PostAuthCCResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_PostAuthCCResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_PostAuthCCResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            // Verify the Transaction is eligible for PostAuth
            try 
            {
                int IsPostAuthCompleted = (int)EfxFramework.NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "usp_PreAuthTransactions_IsPostAuthCompleted",
                    new[] { 
                        new SqlParameter("@ExternalPreAuthTransactionId", TransactionId)
                    });
                if (IsPostAuthCompleted == 1)
                    return new Wallet_PostAuthCCResponse { Result = MobileResponseResult.TransactionNotEligible, Message = ServiceResponseMessages.TransactionNotEligible };
            }
            catch 
            {
                return new Wallet_PostAuthCCResponse { Result = MobileResponseResult.GeneralFailure, Message = "System unavailable. Please try again." };
            }

            if (!ValidateBoolean(SplitPaymentGroupCompleteFlag))
                return new Wallet_PostAuthCCResponse { Result = MobileResponseResult.InvalidSplitPaymentGroupCompleteFlag, Message = ServiceResponseMessages.InvalidSplitPaymentGroupCompleteFlag };

            string gatewayUsername = EfxSettings.CreditCardProcessorCustomerId;
            string gatewayPassword = EfxSettings.CreditCardProcessorCustomerPassword;
            bool isTestUser = !EfxSettings.ProcessCreditCards || EfxSettings.UseTestMode;
            bool bSplitPaymentGroupCompleteFlag = Convert.ToBoolean(SplitPaymentGroupCompleteFlag);

            Wallet_PostAuthCCResponse TheResponse = new Wallet_PostAuthCCResponse();

            try
            {
                CreditCardPaymentResponse PostAuthResponse =
                    EfxFramework.PaymentMethods.CreditCard.CreditCardPayment.PostAuthorizePayment(gatewayUsername, gatewayPassword, isTestUser, null, TransactionId);

                if (PostAuthResponse.Result == GeneralResponseResult.Success)
                {
                    TheResponse.Result = MobileResponseResult.Success;
                    TheResponse.Message = PostAuthResponse.ResponseMessage;
                    TheResponse.TransactionId = PostAuthResponse.TransactionId;

                    // This SP does the following ...
                    // Update the PreAuthTransactions.PostAuthCompleted to 1, DateTimeCompleted to now
                    // If bSplitPaymentGroupCompleteFlag is set, Update the SplitPaymentGroup.CompleteFlag to 1, CompletedDateTime to now,
                    // Archives the PostAuth Transaction Id.
                    EfxFramework.NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "usp_PreAuthTransactions_SetPostAuth",
                     new[] { 
                        new SqlParameter("@ExternalPreAuthTransactionId", TransactionId) ,
                        new SqlParameter("@SplitPaymentGroupCompleteFlag", bSplitPaymentGroupCompleteFlag) ,
                        new SqlParameter("@PostAuthTransactionId", TheResponse.TransactionId)
                     });

                    //Queue the post auth message type for processing to payment/transaction tables, email notification, etc.
                    SendMessageToQueue("<FinalizePostAuth>" + TransactionId + "</FinalizePostAuth>");
                }
                else
                {
                    var log = new RPO.ActivityLog();
                    log.WriteLog("MobileApi", "PostAuthorizePayment Failed. ExternalPreAuthTransactionId=" + TransactionId + 
                        ". Response: " + PostAuthResponse.ResponseMessage, 1, "", false);
                    TheResponse.Result = MobileResponseResult.GeneralFailure;
                    TheResponse.Message = PostAuthResponse.ResponseMessage;
                }
            }
            catch (Exception ex)
            {
                var log = new RPO.ActivityLog();
                log.WriteLog("MobileApi", "Error processing post authorize payment: " + ex.ToString(), 1, "", false);
                TheResponse.Result = MobileResponseResult.GeneralFailure;
                TheResponse.Message = "Error processing post authorize payment";
            }

            return TheResponse;
        }
        public Wallet_ReverseAuthResponse Wallet_ReverseAuthorization(string SessionId, string RenterId, string TransactionId, string PaymentAmount)
        {
            if (!ValidGuidFormat(SessionId))
                return new Wallet_ReverseAuthResponse { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Wallet_ReverseAuthResponse { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Wallet_ReverseAuthResponse { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            // Validate PaymentAmount
            if (!ValidateDecimal(PaymentAmount))
                return new Wallet_ReverseAuthResponse { Result = MobileResponseResult.InvalidPaymentAmount, Message = ServiceResponseMessages.InvalidPaymentAmount };

            string gatewayUsername = EfxSettings.CreditCardProcessorCustomerId;
            string gatewayPassword = EfxSettings.CreditCardProcessorCustomerPassword;
            bool isTestUser = !EfxSettings.ProcessCreditCards || EfxSettings.UseTestMode;
            decimal amount = Convert.ToDecimal(PaymentAmount);

            Wallet_ReverseAuthResponse TheResponse = new Wallet_ReverseAuthResponse();

            try
            {
                CreditCardPaymentResponse ReversalResponse =
                    EfxFramework.PaymentMethods.CreditCard.CreditCardPayment.ReverseAuthorizedCreditCardPayment(
                        gatewayUsername, gatewayPassword, TransactionId, amount, isTestUser);

                TheResponse.TransactionId = ReversalResponse.TransactionId;

                var log = new RPO.ActivityLog();
                log.WriteLog("MobileApi", "ReverseAuthorizedCreditCardPayment Response: " + ReversalResponse.ResponseMessage, 1, "", false);

                if (ReversalResponse.Result == GeneralResponseResult.Success)
                {
                    TheResponse.Result = MobileResponseResult.Success;
                    TheResponse.Message = ReversalResponse.ResponseMessage;

                    try
                    {
                        EfxFramework.NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "usp_PreAuthTransactions_Reverse",
                            new[] { 
                            new SqlParameter("@ExternalPreAuthTransactionId", TransactionId)
                        });
                    }
                    catch (Exception ex)
                    {
                        log.WriteLog("MobileApi", "Attempt to record ReverseAuthorizedCreditCardPayment failed. TransactionId=" + TransactionId +
                            ". Exception: " + ex.ToString(), 1, "", false);
                    }
                }
                else
                {
                    TheResponse.Result = MobileResponseResult.GeneralFailure;
                    TheResponse.Message = ReversalResponse.ResponseMessage;
                }
            }
            catch (Exception exx)
            {
                TheResponse.Result = MobileResponseResult.GeneralFailure;
                TheResponse.Message = "Attempt to reverse authorized credit card payment failed.";

                var log = new RPO.ActivityLog();
                log.WriteLog("MobileApi", "Attempt to ReverseAuthorizedCreditCardPayment failed. TransactionId=" + TransactionId +
                    ". Exception: " + exx.ToString(), 1, "", false);
            }

            return TheResponse;
        }

        public Renter_GateCode_GetAll_Response Renter_GateCode_GetAll(string SessionId, string RenterId)
        {

            if (!ValidGuidFormat(SessionId))
                return new Renter_GateCode_GetAll_Response { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Renter_GateCode_GetAll_Response { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Renter_GateCode_GetAll_Response { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            Renter_GateCode_GetAll_Response pmResponse = new Renter_GateCode_GetAll_Response();
            pmResponse.RenterGateCodes = new List<EfxFramework.RenterGateCode>();

            try
            {
                SqlDataReader dr = EfxFramework.NewDataAccess.ExecuteProcedure(EfxSettings.ConnectionString, "Renter_GateCode_GetAll",
                     new[] { new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        RenterGateCode RenterGateCode = new RenterGateCode();

                        RenterGateCode.InternalGateCodeId = dr.GetInt32(dr.GetOrdinal("GateCodeId"));
                        RenterGateCode.RenterId = dr.GetInt32(dr.GetOrdinal("RenterId"));
                        RenterGateCode.AccessUserId = dr.GetString(dr.GetOrdinal("AccessUserId"));
                        RenterGateCode.FirstName = dr.GetString(dr.GetOrdinal("FirstName"));
                        RenterGateCode.MiddleName = dr.GetString(dr.GetOrdinal("MiddleName"));
                        RenterGateCode.LastName = dr.GetString(dr.GetOrdinal("LastName"));
                        RenterGateCode.MobilePhoneNumber = dr.GetString(dr.GetOrdinal("MobilePhoneNumber"));
                        RenterGateCode.PrimaryEmailAddress = dr.GetString(dr.GetOrdinal("PrimaryEmailAddress"));
                        RenterGateCode.GateCode = dr.GetString(dr.GetOrdinal("GateCode"));

                        pmResponse.RenterGateCodes.Add(RenterGateCode);
                    }
                    pmResponse.Message = "Renter Gate Codes successfully retrieved.";
                    pmResponse.Result = MobileResponseResult.Success;
                }
                else
                {
                    RenterGateCode RenterGateCode = new RenterGateCode();
                    RenterGateCode.InternalGateCodeId = 0;
                    RenterGateCode.RenterId = 0;
                    RenterGateCode.AccessUserId = "No Renter Gate Codes found.";
                    pmResponse.RenterGateCodes.Add(RenterGateCode);

                    pmResponse.Message = "No Renter Gate Codes found.";
                    pmResponse.Result = MobileResponseResult.NoRenterGateCodes;
                }
            }
            catch (Exception ex)
            {
                pmResponse.Message = "Error reading database: " + ex.Message;
                pmResponse.Result = MobileResponseResult.GeneralFailure;
            }

            return pmResponse;
        }
        public Renter_GateCode_GetByInternalId_Response Renter_GateCode_GetByInternalId(string SessionId, string RenterId, string InternalId)
        {
            if (!ValidGuidFormat(SessionId))
                return new Renter_GateCode_GetByInternalId_Response { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Renter_GateCode_GetByInternalId_Response { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Renter_GateCode_GetByInternalId_Response { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            //Verify the InternalId is integer
            if (!ValidateInteger(InternalId))
                return new Renter_GateCode_GetByInternalId_Response { Result = MobileResponseResult.InvalidInternalId, Message = ServiceResponseMessages.InvalidInternalId };

            Renter_GateCode_GetByInternalId_Response pmResponse = new Renter_GateCode_GetByInternalId_Response();

            try
            {
                SqlDataReader dr = EfxFramework.NewDataAccess.ExecuteProcedure(EfxSettings.ConnectionString, "Renter_GateCode_GetByInternalId",
                     new[] { new SqlParameter("@RenterId", Convert.ToInt32(RenterId)), new SqlParameter("@InternalId", Convert.ToInt32(InternalId)) });
                if (dr.HasRows)
                {
                    while (dr.Read())   //Should be only 1 row returned
                    {
                        RenterGateCode RenterGateCode = new RenterGateCode();

                        RenterGateCode.InternalGateCodeId = dr.GetInt32(dr.GetOrdinal("GateCodeId"));
                        RenterGateCode.RenterId = dr.GetInt32(dr.GetOrdinal("RenterId"));
                        RenterGateCode.AccessUserId = dr.GetString(dr.GetOrdinal("AccessUserId"));
                        RenterGateCode.FirstName = dr.GetString(dr.GetOrdinal("FirstName"));
                        RenterGateCode.MiddleName = dr.GetString(dr.GetOrdinal("MiddleName"));
                        RenterGateCode.LastName = dr.GetString(dr.GetOrdinal("LastName"));
                        RenterGateCode.MobilePhoneNumber = dr.GetString(dr.GetOrdinal("MobilePhoneNumber"));
                        RenterGateCode.PrimaryEmailAddress = dr.GetString(dr.GetOrdinal("PrimaryEmailAddress"));
                        RenterGateCode.GateCode = dr.GetString(dr.GetOrdinal("GateCode"));

                        pmResponse.RenterGateCode = RenterGateCode;
                    }
                    pmResponse.Message = "Renter Gate Code successfully retrieved.";
                    pmResponse.Result = MobileResponseResult.Success;
                }
                else
                {
                    RenterGateCode RenterGateCode = new RenterGateCode();
                    RenterGateCode.InternalGateCodeId = 0;
                    RenterGateCode.RenterId = 0;
                    RenterGateCode.AccessUserId = "Renter Gate Code not found.";
                    pmResponse.RenterGateCode = RenterGateCode;

                    pmResponse.Message = "Renter Gate Code not found.";
                    pmResponse.Result = MobileResponseResult.RenterGateCodeNotFound;
                }
            }
            catch (Exception ex)
            {
                pmResponse.Message = "Error reading database: " + ex.Message;
                pmResponse.Result = MobileResponseResult.GeneralFailure;
            }

            return pmResponse;
        }
        public Renter_GateCode_GetByUserId_Response Renter_GateCode_GetByUserId(string SessionId, string RenterId, string UserId)
        {
            if (!ValidGuidFormat(SessionId))
                return new Renter_GateCode_GetByUserId_Response { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Renter_GateCode_GetByUserId_Response { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Renter_GateCode_GetByUserId_Response { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            Renter_GateCode_GetByUserId_Response pmResponse = new Renter_GateCode_GetByUserId_Response();

            try
            {
                SqlDataReader dr = EfxFramework.NewDataAccess.ExecuteProcedure(EfxSettings.ConnectionString, "Renter_GateCode_GetByUserId",
                     new[] { new SqlParameter("@RenterId", Convert.ToInt32(RenterId)), new SqlParameter("@UserId", UserId) });
                if (dr.HasRows)
                {
                    while (dr.Read())   //Should be only 1 row returned
                    {
                        RenterGateCode RenterGateCode = new RenterGateCode();

                        RenterGateCode.InternalGateCodeId = dr.GetInt32(dr.GetOrdinal("GateCodeId"));
                        RenterGateCode.RenterId = dr.GetInt32(dr.GetOrdinal("RenterId"));
                        RenterGateCode.AccessUserId = dr.GetString(dr.GetOrdinal("AccessUserId"));
                        RenterGateCode.FirstName = dr.GetString(dr.GetOrdinal("FirstName"));
                        RenterGateCode.MiddleName = dr.GetString(dr.GetOrdinal("MiddleName"));
                        RenterGateCode.LastName = dr.GetString(dr.GetOrdinal("LastName"));
                        RenterGateCode.MobilePhoneNumber = dr.GetString(dr.GetOrdinal("MobilePhoneNumber"));
                        RenterGateCode.PrimaryEmailAddress = dr.GetString(dr.GetOrdinal("PrimaryEmailAddress"));
                        RenterGateCode.GateCode = dr.GetString(dr.GetOrdinal("GateCode"));

                        pmResponse.RenterGateCode = RenterGateCode;
                    }
                    pmResponse.Message = "Renter Gate Code successfully retrieved.";
                    pmResponse.Result = MobileResponseResult.Success;
                }
                else
                {
                    RenterGateCode RenterGateCode = new RenterGateCode();
                    RenterGateCode.InternalGateCodeId = 0;
                    RenterGateCode.RenterId = 0;
                    RenterGateCode.AccessUserId = "Renter Gate Code not found.";
                    pmResponse.RenterGateCode = RenterGateCode;

                    pmResponse.Message = "Renter Gate Code not found.";
                    pmResponse.Result = MobileResponseResult.RenterGateCodeNotFound;
                }
            }
            catch (Exception ex)
            {
                pmResponse.Message = "Error reading database: " + ex.Message;
                pmResponse.Result = MobileResponseResult.GeneralFailure;
            }

            return pmResponse;
        }
        public Renter_GateCode_Add_Response Renter_GateCode_Add(string SessionId, string RenterId, string UserId, string FirstName, string MiddleName, string LastName, 
            string MobilePhoneNumber, string PrimaryEmailAddress, string GateCode)
        {
            if (!ValidGuidFormat(SessionId))
                return new Renter_GateCode_Add_Response { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Renter_GateCode_Add_Response { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Renter_GateCode_Add_Response { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            Renter_GateCode_Add_Response pmResponse = new Renter_GateCode_Add_Response();

            //Add the new data to the database ...
            try
            {
                int NewId = (int)EfxFramework.NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "Renter_GateCode_Add",
                     new[] { 
                    new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) ,
                    new SqlParameter("@UserId", UserId) ,
                    new SqlParameter("@FirstName", FirstName) ,
                    new SqlParameter("@MiddleName", MiddleName) ,
                    new SqlParameter("@LastName", LastName) ,
                    new SqlParameter("@MobilePhoneNumber", MobilePhoneNumber) ,
                    new SqlParameter("@PrimaryEmailAddress", PrimaryEmailAddress) ,
                    new SqlParameter("@GateCode", GateCode)});

                pmResponse.Message = "Gate Code successfully added.";
                pmResponse.Result = MobileResponseResult.Success;
                pmResponse.GateCodeId = NewId;
            }
            catch (Exception ex)
            {
                pmResponse.Message = "Database call failed: " + ex.Message;
                pmResponse.Result = MobileResponseResult.GeneralFailure;
                pmResponse.GateCodeId = 0;
            }

            return pmResponse;
        }
        public Renter_GateCode_UpdateByInternalId_Response Renter_GateCode_UpdateByInternalId(string SessionId, string RenterId, string InternalId, string UserId, 
            string FirstName, string MiddleName, string LastName, string MobilePhoneNumber, string PrimaryEmailAddress, string GateCode)
        {
            if (!ValidGuidFormat(SessionId))
                return new Renter_GateCode_UpdateByInternalId_Response { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Renter_GateCode_UpdateByInternalId_Response { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Renter_GateCode_UpdateByInternalId_Response { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            //Verify the InternalId is integer
            if (!ValidateInteger(InternalId))
                return new Renter_GateCode_UpdateByInternalId_Response { Result = MobileResponseResult.InvalidInternalId, Message = ServiceResponseMessages.InvalidInternalId };

            Renter_GateCode_UpdateByInternalId_Response pmResponse = new Renter_GateCode_UpdateByInternalId_Response();

            //Update the data in the database ...
            try
            {
                EfxFramework.NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "Renter_GateCode_UpdateByInternalId",
                     new[] { 
                    new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) ,
                    new SqlParameter("@InternalId", Convert.ToInt32(InternalId)) , 
                    new SqlParameter("@UserId", UserId) ,
                    new SqlParameter("@FirstName", FirstName) ,
                    new SqlParameter("@MiddleName", MiddleName) ,
                    new SqlParameter("@LastName", LastName) ,
                    new SqlParameter("@MobilePhoneNumber", MobilePhoneNumber) ,
                    new SqlParameter("@PrimaryEmailAddress", PrimaryEmailAddress) ,
                    new SqlParameter("@GateCode", GateCode)});

                pmResponse.Message = "Gate Code successfully updated.";
                pmResponse.Result = MobileResponseResult.Success;
            }
            catch (Exception ex)
            {
                pmResponse.Message = "Database call failed: " + ex.Message;
                pmResponse.Result = MobileResponseResult.GeneralFailure;
            }

            return pmResponse;
        }
        public Renter_GateCode_UpdateByUserId_Response Renter_GateCode_UpdateByUserId(string SessionId, string RenterId, string UserId, string FirstName, string MiddleName, string LastName, 
            string MobilePhoneNumber, string PrimaryEmailAddress, string GateCode)
        {
            if (!ValidGuidFormat(SessionId))
                return new Renter_GateCode_UpdateByUserId_Response { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Renter_GateCode_UpdateByUserId_Response { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Renter_GateCode_UpdateByUserId_Response { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            Renter_GateCode_UpdateByUserId_Response pmResponse = new Renter_GateCode_UpdateByUserId_Response();

            //Update the data in the database ...
            try
            {
                EfxFramework.NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "Renter_GateCode_UpdateByUserId",
                     new[] { 
                    new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) ,
                    new SqlParameter("@UserId", UserId) ,
                    new SqlParameter("@FirstName", FirstName) ,
                    new SqlParameter("@MiddleName", MiddleName) ,
                    new SqlParameter("@LastName", LastName) ,
                    new SqlParameter("@MobilePhoneNumber", MobilePhoneNumber) ,
                    new SqlParameter("@PrimaryEmailAddress", PrimaryEmailAddress) ,
                    new SqlParameter("@GateCode", GateCode)});

                pmResponse.Message = "Gate Code successfully updated.";
                pmResponse.Result = MobileResponseResult.Success;
            }
            catch (Exception ex)
            {
                pmResponse.Message = "Database call failed: " + ex.Message;
                pmResponse.Result = MobileResponseResult.GeneralFailure;
            }

            return pmResponse;
        }
        public Renter_GateCode_DeleteByInternalId_Response Renter_GateCode_DeleteByInternalId(string SessionId, string RenterId, string InternalId)
        {
            if (!ValidGuidFormat(SessionId))
                return new Renter_GateCode_DeleteByInternalId_Response { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Renter_GateCode_DeleteByInternalId_Response { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Renter_GateCode_DeleteByInternalId_Response { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            //Verify the InternalId is integer
            if (!ValidateInteger(InternalId))
                return new Renter_GateCode_DeleteByInternalId_Response { Result = MobileResponseResult.InvalidInternalId, Message = ServiceResponseMessages.InvalidInternalId };

            Renter_GateCode_DeleteByInternalId_Response pmResponse = new Renter_GateCode_DeleteByInternalId_Response();

            //Delete the data from the database ...
            try
            {
                EfxFramework.NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "Renter_GateCode_DeleteByInternalId",
                     new[] { 
                        new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) ,
                        new SqlParameter("@InternalId", Convert.ToInt32(InternalId)) });

                pmResponse.Message = "Gate Code successfully deleted.";
                pmResponse.Result = MobileResponseResult.Success;
            }
            catch (Exception ex)
            {
                pmResponse.Message = "Database call failed: " + ex.Message;
                pmResponse.Result = MobileResponseResult.GeneralFailure;
            }

            return pmResponse;
        }
        public Renter_GateCode_DeleteByUserId_Response Renter_GateCode_DeleteByUserId(string SessionId, string RenterId, string UserId)
        {
            if (!ValidGuidFormat(SessionId))
                return new Renter_GateCode_DeleteByUserId_Response { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new Renter_GateCode_DeleteByUserId_Response { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new Renter_GateCode_DeleteByUserId_Response { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            Renter_GateCode_DeleteByUserId_Response pmResponse = new Renter_GateCode_DeleteByUserId_Response();

            //Delete the data from the database ...
            try
            {
                EfxFramework.NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "Renter_GateCode_DeleteByUserId",
                    new[] { 
                        new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) ,
                        new SqlParameter("@UserId", UserId) });

                pmResponse.Message = "Gate Code successfully deleted.";
                pmResponse.Result = MobileResponseResult.Success;
            }
            catch (Exception ex)
            {
                pmResponse.Message = "Database call failed: " + ex.Message;
                pmResponse.Result = MobileResponseResult.GeneralFailure;
            }

            return pmResponse;
        }

        private EfxFramework.WebPayments.PaymentResponse ProcessPayment(
            int RenterId,  EfxFramework.PaymentMethods.PaymentType PaymentType,
            string BankAccountNumber, string BankRoutingNumber,
            string CreditCardHolderName, string CreditCardNumber, string CardSecurityCode, DateTime ExpirationDate,
            decimal PaymentAmount, string PaymentAmountDescription,
            string DisplayName, int PayerId, int PropertyId)
        {

            EfxFramework.WebPayments.RenterPaymentInfo ThePaymentInfo = BuildPaymentInfo(RenterId, PaymentType, BankAccountNumber, BankRoutingNumber, CreditCardHolderName, CreditCardNumber, CardSecurityCode,
                ExpirationDate, PaymentAmount, PaymentAmountDescription, DisplayName, PayerId, PropertyId);

            EfxFramework.WebPayments.PaymentResponse TheReturn = EfxFramework.WebPayments.PaymentRequest.ProcessPayment(ThePaymentInfo, false, false);

            return TheReturn;

        }

        private EfxFramework.WebPayments.RenterPaymentInfo BuildPaymentInfo(
             int RenterId, EfxFramework.PaymentMethods.PaymentType PaymentType,
             string BankAccountNumber, string BankRoutingNumber,
             string CreditCardHolderName, string CreditCardNumber, string CardSecurityCode, DateTime ExpirationDate,
             decimal PaymentAmount, string PaymentAmountDescription,
             string DisplayName, int PayerId, int PropertyId)
        {
            // Code plagarized from EfxFramework\Presenters\PaymentPresenter.cs -> ProcessResidentPayment() function

            return new EfxFramework.WebPayments.RenterPaymentInfo
            {
                RenterId = RenterId,
                PaymentType = PaymentType,
                BankAccountNumber = BankAccountNumber,
                BankRoutingNumber = BankRoutingNumber,
                CreditCardHolderName = CreditCardHolderName,
                CreditCardNumber = CreditCardNumber,
                Cvv = CardSecurityCode,
                ExpirationDate = ExpirationDate,
                RentAmount = PaymentAmount,
                RentAmountDescription = PaymentAmountDescription,
                OtherAmount2 = 0,
                OtherAmountDescription2 = "",
                DisplayName = DisplayName,
                PayerId = PayerId,
                PropertyId = PropertyId,
                WaivedAmount = 0,
                WaiveConvenienceFee = false
            };
        }

        public GetCalendarEventsForRenter_Response GetCalendarEventsForRenter(string SessionId, string RenterId)
        {
            if (!ValidGuidFormat(SessionId))
                return new GetCalendarEventsForRenter_Response { Result = MobileResponseResult.InvalidSessionId, Message = ServiceResponseMessages.InvalidSessionId };

            // Verify SessionId is valid, extend session if it is, return failure if not
            if (!EfxFramework.RenterSession.RefreshSessionBySessionGuid(Guid.Parse(SessionId)))
                return new GetCalendarEventsForRenter_Response { Result = MobileResponseResult.SessionTimeout, Message = ServiceResponseMessages.Sessiontimeout };

            // Verify the Renter Id is valid for the session
            if (!ValidRenterId(SessionId, RenterId))
                return new GetCalendarEventsForRenter_Response { Result = MobileResponseResult.InvalidRenterId, Message = ServiceResponseMessages.InvalidRenterId };

            GetCalendarEventsForRenter_Response pmResponse = new GetCalendarEventsForRenter_Response();
            pmResponse.CalendarEventItems = new List<EfxFramework.CalendarEvent>();

            try
            {
                SqlDataReader dr = EfxFramework.NewDataAccess.ExecuteProcedure(EfxSettings.ConnectionString, "usp_CalendarEvent_GetAllForRenter",
                     new[] { new SqlParameter("@RenterId", Convert.ToInt32(RenterId)) });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EfxFramework.CalendarEvent CalendarEvent = new EfxFramework.CalendarEvent();

                        CalendarEvent.CalendarEventId = dr.GetInt32(dr.GetOrdinal("CalendarEventId"));
                        CalendarEvent.Title = dr.GetString(dr.GetOrdinal("Title"));
                        CalendarEvent.Location = dr.GetString(dr.GetOrdinal("Location"));
                        CalendarEvent.Details = dr.GetString(dr.GetOrdinal("Details"));
                        if (!dr.IsDBNull(dr.GetOrdinal("CalendarEventTypeId")))
                            CalendarEvent.CalendarEventTypeId = dr.GetInt32(dr.GetOrdinal("CalendarEventTypeId"));
                        CalendarEvent.StartDate = dr.GetDateTime(dr.GetOrdinal("StartDate"));
                        CalendarEvent.EndDate = dr.GetDateTime(dr.GetOrdinal("EndDate"));
                        if (!dr.IsDBNull(dr.GetOrdinal("Contact")))
                            CalendarEvent.Contact = dr.GetString(dr.GetOrdinal("Contact"));
                        CalendarEvent.DateCreated = dr.GetDateTime(dr.GetOrdinal("DateCreated"));
                        CalendarEvent.DateLastModified = dr.GetDateTime(dr.GetOrdinal("DateLastModified"));
                        CalendarEvent.Active = dr.GetBoolean(dr.GetOrdinal("Active"));
                        if (!dr.IsDBNull(dr.GetOrdinal("PropertyId")))
                            CalendarEvent.PropertyId = dr.GetInt32(dr.GetOrdinal("PropertyId"));
                        CalendarEvent.allproperties = dr.GetBoolean(dr.GetOrdinal("allproperties"));
                        if (!dr.IsDBNull(dr.GetOrdinal("CompanyId"))) 
                            CalendarEvent.CompanyId = dr.GetInt32(dr.GetOrdinal("CompanyId"));
                        if (!dr.IsDBNull(dr.GetOrdinal("Recurring"))) 
                            CalendarEvent.Recurring = dr.GetBoolean(dr.GetOrdinal("Recurring"));
                        if (!dr.IsDBNull(dr.GetOrdinal("AssociatedCalendarEventId"))) 
                            CalendarEvent.AssociatedCalendarEventId = dr.GetInt32(dr.GetOrdinal("AssociatedCalendarEventId")); 

                        pmResponse.CalendarEventItems.Add(CalendarEvent);
                    }
                    pmResponse.Message = "Calendar events successfully retrieved.";
                    pmResponse.Result = MobileResponseResult.Success;
                }
                else
                {
                    EfxFramework.CalendarEvent CalendarEvent = new EfxFramework.CalendarEvent();
                    CalendarEvent.CalendarEventId = 0;
                    CalendarEvent.Title = "No calendar events found.";
                    pmResponse.CalendarEventItems.Add(CalendarEvent);

                    pmResponse.Message = "No calendar events found.";
                    pmResponse.Result = MobileResponseResult.NoCalendarEvents;
                }
            }
            catch (Exception ex)
            {
                pmResponse.Message = "Error reading database: " + ex.Message;
                pmResponse.Result = MobileResponseResult.GeneralFailure;
            }
            return pmResponse;
        }

        //Salcedo - 7/12/2016 - new functions for queueing tasks that don't necessarily need to be performed synchronosly. 
        //As a step 1, perhaps we'll just forgo the async queue process to get it working.

        //Salcedo - 7/12/2016 - new function to queue the synchronize the Pay_PayableControllerWallet and Pay_PayableControllerItems*tables for the specified renter
        private void Queue_Wallet_And_Payables_Synchronize(int RenterId)
        {
            try
            {
                EfxFramework.NewDataAccess.ExecuteScalar(EfxSettings.ConnectionString, "Pay_PayableControllerWalletItemsBuild",
                    new[] { new SqlParameter("@RenterId", RenterId) });
            }
            catch (Exception ex)
            {
                var log = new RPO.ActivityLog();
                log.WriteLog("MobileApi", "Error executing 'Pay_PayableControllerWalletItemsBuild': " + ex.ToString() 
                    , 1, "", false);
            }
        }

        //Salcedo - 7/12/2016 - new function to queue update of latest balance information from Yardi/MRI/RealPage/etc for the specified renter
        private void Queue_Update_From_PMS(int RenterId)
        {
            //For Yardi
            //  Call: EfxFramework.Pms.Yardi.ImportRequest.UpdateRenterBalanceDueRealTimeYardi(Resident);

            //For AMSI
            //  Call: EfxFramework.Pms.AMSI.AmsiRequest.AMSI_RequestXmlForRenterImport(2, _RenterID: Renter.RenterId);

            //For MRI
            //  Call: EfxFramework.Pms.MRI.MriRequest.MRI_ProcessSingleRenter(Renter.RenterId);

            //For RealPage
            //  RealTime update not available
        }

        //Salcedo - 7/15/2016 - new functions for queue actions
        // Action = 1 = RenterLogin
        // Action = 2 = RenterPaymentProcessed
        private void QueueRenterAction(int ValueId, int Action)
        {
            //If UseDomusQueue is set to false in the SystemSetting table, then we'll skip the queue and just run the processes directly
            //Action = 3 is always processed through the queue
            if (EfxSettings.UseDomusQueue || Action == 3)
            { 
                try
                {
                    //Salcedo - 7/19/2016 - Replaced use of Sql Server Service Broker queuing with local/private MSMQ
                    //EfxFramework.NewDataAccess.ExecuteNoResults(Settings.ConnectionString, "Queue_Renter_Action",
                    //    new[] { new SqlParameter("@RenterId", RenterId), new SqlParameter("@Action", Action) });

                    //string Message = Convert.ToString(Action) + ":" + Convert.ToString(RenterId);
                    string Message = "";
                    if (Action == 1)
                        Message = "<RenterLogin>" + Convert.ToString(ValueId) + "</RenterLogin>";
                    if (Action == 2)
                        Message = "<RenterPaymentProcessed>" + Convert.ToString(ValueId) + "</RenterPaymentProcessed>";
                    SendMessageToQueue(Message);
                }
                catch (Exception ex)
                {
                    var log = new RPO.ActivityLog();
                    log.WriteLog("MobileApi", "Error executing 'Queue_Renter_Action': " + ex.ToString()
                        , 1, "", false);
                }
            }
            else
            {
                if (Action == 1)
                    ProcessRenterLogin(ValueId.ToString());
                if (Action == 2)
                    ProcessRenterPaymentProcessed(ValueId.ToString());
            }
        }

        //Salcedo - 7/19/2016 - new function to support MSMQ
        private static void SendMessageToQueue(string queueMessage)
        {
            string queueName = @".\private$\DomusQueue";

            // check if queue exists, if not create it
            MessageQueue msMq = null;
            if (!MessageQueue.Exists(queueName))
            {
                msMq = MessageQueue.Create(queueName,(bool)true); //Create a transactional queue
            }
            else
            {
                msMq = new MessageQueue(queueName);
            }
            MessageQueueTransaction transaction = new MessageQueueTransaction();
            try
            {
                Message msg = new Message(queueMessage);

                //Begin transaction, send message, commit transaction
                transaction.Begin();
                msMq.Send(queueMessage, queueMessage, transaction);
                transaction.Commit();
            }
            catch (MessageQueueException ee)
            {
                transaction.Abort();
                var log = new RPO.ActivityLog();
                log.WriteLog("MobileApi", "Error executing 'SendMessageToQueue': " + ee.ToString(), 1, "", false);
            }
            catch (Exception eee)
            {
                transaction.Abort();
                var log = new RPO.ActivityLog();
                log.WriteLog("MobileApi", "Error executing 'SendMessageToQueue': " + eee.ToString(), 1, "", false);
            }
            finally
            {
                transaction.Dispose();
                msMq.Close();
            }
            var outputlog = new RPO.ActivityLog();
            outputlog.WriteLog("MobileApi", "Message sent .....': " + queueMessage, 1, "", false);
        }

        private void ProcessRenterLogin(string StringRenterId)
        {
            // When a user logs in, we want to retrieve the latest balances from the third party property management system.
            // After doing that, we also need to update the wallet and payables tables for that user.

            try
            {
                int RenterId = Convert.ToInt32(StringRenterId);

                int? PropertyId = EfxFramework.Renter.GetPropertyByRenterId(RenterId);
                if (PropertyId.HasValue)
                {
                    var Property = new EfxFramework.Property(PropertyId.Value);
                    if (Property.PmsTypeId.HasValue)
                    {
                        switch (Property.PmsTypeId)
                        {
                            case 1:
                                // Yardi
                                // EfxFramework.Pms.Yardi.ImportRequest.UpdateRenterBalanceDueRealTimeYardi(Resident);
                                var Resident = new EfxFramework.Renter(RenterId);
                                EfxFramework.Pms.Yardi.ImportRequest.UpdateRenterBalanceDueRealTimeYardi(Resident);
                                break;
                            case 2:
                                // AMSI
                                //EfxFramework.Pms.AMSI.AmsiRequest.AMSI_RequestXmlForRenterImport(2, _RenterID: Resident.RenterId);
                                EfxFramework.Pms.AMSI.AmsiRequest.AMSI_RequestXmlForRenterImport(2, _RenterID: RenterId);
                                break;
                            case 3:
                                // RealPage - not supported for real time updates
                                break;
                            case 4:
                                // MRI
                                // EfxFramework.Pms.MRI.MriRequest.MRI_ProcessSingleRenter(Renter.RenterId);
                                EfxFramework.Pms.MRI.MriRequest.MRI_ProcessSingleRenter(RenterId);
                                break;
                            default:
                                break;
                        }

                        // Update/synchronize wallet/payables tables
                        UpdateWalletAndPayables(RenterId);
                    }
                }
            }
            catch (Exception ex)
            {
                var log = new RPO.ActivityLog();
                log.WriteLog("MobileApi", "Exception in ProcessRenterLogin: " + ex.ToString()
                    , 1, "", false);
            }

        }

        private void ProcessRenterPaymentProcessed(string StringRenterId)
        {
            int RenterId = Convert.ToInt32(StringRenterId);

            // If a payment has been made, we need to update the wallet and the payables tables
            UpdateWalletAndPayables(RenterId);
        }

        private void UpdateWalletAndPayables(int RenterId)
        {
            try
            {
                ActivityLog Log = new ActivityLog();
                EfxFramework.NewDataAccess.ExecuteNoResults(EfxSettings.ConnectionString, "Pay_PayableControllerWalletItemsBuild",
                    new[] { new SqlParameter("@RenterId", RenterId) });

                Log.WriteLog("RenterID=" + RenterId, "Called Pay_PayableControllerWalletItemsBuild for Renter.", (short)LogPriority.LogAlways, RenterId.ToString(), false);

            }
            catch (Exception ex)
            {
                var log = new RPO.ActivityLog();
                log.WriteLog("MobileApi", "Exception in UpdateWalletAndPayables: " + ex.ToString()
                    , 1, "", false);

            }
        }


        //cakel: 08/09/2016
        public GetConvFeeForPayment_Response GetConvFeeForPayment(string ProgramID, string PaymentMethod, string Amount)
        {
            GetConvFeeForPayment_Response Response = new GetConvFeeForPayment_Response();

            try
            {
                int _ProgramID = int.Parse(ProgramID);
                decimal _Amount = decimal.Parse(Amount);

                Response.ConvenienceFee = PaymentAmount.GetConvFeeByProgramID(_ProgramID, PaymentMethod, _Amount).ToString();

            }
            catch
            {
                Response.Message = "Error: Bad data submitted";
                Response.Result = MobileResponseResult.GeneralFailure;
            }

            return Response;

        }

        //cakel: 09/20/2016
        public GetConvFeeForPayment_Response GetConvFeeForPaymentByRenterID(string RenterID, string PaymentMethod, string Amount)
        {
            GetConvFeeForPayment_Response Response = new GetConvFeeForPayment_Response();

            try
            {
                int _RenterID = int.Parse(RenterID);
                decimal _Amount = decimal.Parse(Amount);

                Response.ConvenienceFee = GetConvFeeByRenterID(_RenterID, PaymentMethod, _Amount).ToString();
            }
            catch
            {
                Response.Message = "Error: Bad data submitted";
                Response.Result = MobileResponseResult.GeneralFailure;
            }

            return Response;

        }



        public static decimal GetConvFeeByRenterID(int renterID, string PaymentMethod, decimal amount)
        {
            string PM = "";
            switch (PaymentMethod)
            {
                case "true":
                    PM = "CC";
                    break;
                case "false":
                    PM = "CK";
                    break;
                default:
                    PM = "CK";
                    break;

            }

            SqlConnection con = new SqlConnection(EfxFramework.EfxSettings.ConnectionString);
            SqlCommand com = new SqlCommand("Wallet_GetConvFeeByRenterID", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@RenterID", SqlDbType.Int).Value = renterID;
            com.Parameters.Add("@PaymentMethod", SqlDbType.NVarChar, 30).Value = PM;
            com.Parameters.Add("@Amount", SqlDbType.Decimal).Value = amount;
            com.Parameters.Add("@ConvFee_output", SqlDbType.Money).Direction = ParameterDirection.Output;

            decimal ConvFeeAmount = 0m;
            try
            {

                con.Open();
                com.ExecuteNonQuery();
                ConvFeeAmount = (decimal)com.Parameters["@ConvFee_output"].Value;
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }

            return ConvFeeAmount;

        }




        public Wallet_PayByCash CreatePaynearMeTicket(string RenterID, string AmountBeingPaid)
        {
            Wallet_PayByCash Response = new Wallet_PayByCash();

            int _RenterID = int.Parse(RenterID);

            EfxFramework.Renter Resident = new EfxFramework.Renter(_RenterID);

            EfxFramework.Company currentCompany = EfxFramework.Company.GetCompanyByRenterId(Resident.RenterId);
            var Property = EfxFramework.Property.GetPropertyByRenterId(Resident.RenterId);

            var Address = EfxFramework.Address.GetAddressByRenterIdAndAddressType(Resident.RenterId, (int)TypeOfAddress.Billing);
            Address.AddressTypeId = (int)TypeOfAddress.Billing;
            Address.AddressLine1 = Resident.StreetAddress;
            Address.City = Resident.City;
            Address.StateProvinceId = (int)Resident.StateProvinceId;
            Address.PostalCode = Resident.PostalCode;
            Address.PrimaryEmailAddress = Resident.PrimaryEmailAddress;
            Address.PrimaryPhoneNumber = Resident.MainPhoneNumber;

            RPO.PayNearMeRequest pnmRequest = new RPO.PayNearMeRequest();
            pnmRequest.Amount = decimal.Parse(AmountBeingPaid);
            pnmRequest.CreatorIdentifier = Resident.PublicRenterId.ToString();
            pnmRequest.Currency = "USD";
            pnmRequest.CustomerCity = Address.City;
            pnmRequest.CustomerEmail = Address.PrimaryEmailAddress;
            pnmRequest.CustomerIdentifier = Resident.PublicRenterId.ToString();
            pnmRequest.CustomerName = Resident.FirstName + " " + Resident.LastName;
            pnmRequest.CustomerPhone = Address.PrimaryPhoneNumber;
            pnmRequest.CustomerPostalCode = Address.PostalCode;
            pnmRequest.CustomerState = Address.GetStateName();
            pnmRequest.CustomerStreet = Address.AddressLine1;
            pnmRequest.MinimumPaymentAmount = 1;
            pnmRequest.MinimumPaymentCurrency = "USD";
            pnmRequest.OrderDescription = "Resident Payment";
            pnmRequest.OrderDuration = 1;
            pnmRequest.OrderIdentifier = Guid.NewGuid().ToString();
            pnmRequest.OrderType = RPO.PayNearMeRequest.PNMOrderType.Exact;
            pnmRequest.RenterId = Resident.RenterId;
            pnmRequest.PropertyId = (int)Property.PropertyId;
            pnmRequest.CompanyId = currentCompany.CompanyId;
            pnmRequest.SiteIdentifier = currentCompany.PNMSiteId;
            pnmRequest.SecretKey = currentCompany.PNMSecretKey;
            pnmRequest.Version = EfxSettings.PNMVersion;

            var Log = new RPO.ActivityLog();
            Log.WriteLog(Resident.PrimaryEmailAddress, "PNM Create New Ticket", 0, Resident.RenterId.ToString(), false);
            

            RPO.RentByCash rentByCash = new RPO.RentByCash();
            rentByCash.SandBoxMode = bool.Parse(EfxSettings.PNMUseSandboxMode);
            string[] arg = new string[1];
            string PNM_Response = rentByCash.CreatePayNearMeRequest(pnmRequest, rentByCash.SandBoxMode);
            arg = PNM_Response.ToString().Split(';');

            Response.Status = arg[0];
            Response.OrderUrl = arg[1];

            if(Response.OrderUrl == "Error")
            {
                
                Response.Result = MobileResponseResult.GeneralFailure;

            }
            else
            {
                Response.Message = Response.OrderUrl;
                Response.Result = MobileResponseResult.Success;
            }

            return Response;


        }





    }
}
