﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace EfxPublicApi.IvrWebServices
{
    [ServiceContract]
    public interface IIvrWebService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetUserInformation/{username}/{password}/{accountCode}/{pin}", ResponseFormat = WebMessageFormat.Json)]
        EfxFramework.Ivr.UserInformationResponse GetUserInformation(string username, string password, string accountCode, string pin);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/SubmitPayment/{username}/{password}/{accountCode}/{pin}", ResponseFormat = WebMessageFormat.Json)]
        EfxFramework.Ivr.PaymentResponse SubmitPayment(string username, string password, string accountCode, string pin);
    }
}
