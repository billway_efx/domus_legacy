﻿using EfxFramework.Services;
using System.ServiceModel;

namespace EfxPublicApi.IvrWebServices
{
    [DefaultServiceBehavior]
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class IvrWebService : IIvrWebService
    {
        public EfxFramework.Ivr.UserInformationResponse GetUserInformation(string username, string password, string accountId, string pin)
        {
            var Request = new EfxFramework.Ivr.UserInformationRequest(accountId);
            return Request.GetUserInformation(username, password, pin, accountId);
        }

        public EfxFramework.Ivr.PaymentResponse SubmitPayment(string username, string password, string accountId, string pin)
        {
            return EfxFramework.Ivr.PaymentRequest.ProcessPayment(accountId, username, password, pin);
        }
    }
}
