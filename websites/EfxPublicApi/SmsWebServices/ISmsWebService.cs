﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace EfxPublicApi.SmsWebServices
{
    [ServiceContract]
    public interface ISmsWebService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/SubmitPayment?phone={phone}&message={message}&id={id}&carrier={carrier}")]
        Stream SubmitPayment(string phone, string message, string id, string carrier);
    }
}
