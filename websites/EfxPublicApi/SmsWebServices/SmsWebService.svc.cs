﻿using EfxFramework.Services;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace EfxPublicApi.SmsWebServices
{
    [DefaultServiceBehavior]
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class SmsWebService : ISmsWebService
    {
        public Stream SubmitPayment(string phone, string message, string id, string carrier)
        {
            if (!string.IsNullOrEmpty(phone))
            {
                var Request = new EfxFramework.Sms.PaymentRequest(phone, message, id, carrier);
                Request.ProcessPayment();
            }

            if (WebOperationContext.Current != null)
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";

            return new MemoryStream(Encoding.ASCII.GetBytes("OK"));
        }
    }
}
