﻿using System.ServiceModel;
using EfxFramework.PublicApi.Transaction;
using EfxFramework.Services;

namespace EfxPublicApi.TransactionServices
{
    [DefaultServiceBehavior]
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class TransactionService : ITransactionService
    {
        public GetTransactionHistoryResponse GetTransactionHistory(GetTransactionHistoryRequest request)
        {
            return request.GetTransactionHistory();
        }
    }
}
