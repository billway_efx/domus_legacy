﻿using EfxFramework.PublicApi.Transaction;
using System.ServiceModel;

namespace EfxPublicApi.TransactionServices
{
    [ServiceContract(Namespace = "PublicApi")]
    public interface ITransactionService
    {
        [OperationContract]
        GetTransactionHistoryResponse GetTransactionHistory(GetTransactionHistoryRequest request);
    }
}
