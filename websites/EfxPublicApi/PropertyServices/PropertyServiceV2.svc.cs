﻿using System.ServiceModel;
using EfxFramework.PublicApi.Property;
using EfxFramework.Services;

namespace EfxPublicApi.PropertyServices
{
    [DefaultServiceBehavior]
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class PropertyServiceV2 : IPropertyServiceV2
    {
        public GetPropertyResponseV2 GetProperty(GetPropertyRequestV2 request)
        {
            return request.GetProperty();
        }

        // CreatePropertyResponseV2 is different than CreatePropertyResponse in that it adds support for Fax number
        public CreatePropertyResponseV2 CreateProperty(CreatePropertyRequestV2 request)
        {
            return request.CreateProperty();
        }

        public UpdatePropertyResponse UpdateProperty(UpdatePropertyRequestV2 request)
        {
            return request.UpdateProperty();
        }

        public DeletePropertyResponse DeleteProperty(DeletePropertyRequest request)
        {
            return request.DeleteProperty();
        }

        public GetRentReportersPropertiesResponse GetAllRentReportersProperties(GetRentReportersPropertiesRequest request)
        {
            return request.GetRentReportersProperties();
        }
    }
}
