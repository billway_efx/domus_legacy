﻿using System.ServiceModel;
using EfxFramework.PublicApi.Property;
using EfxFramework.Services;

namespace EfxPublicApi.PropertyServices
{
    [DefaultServiceBehavior]
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class PropertyService : IPropertyService
    {
        public GetPropertyResponse GetProperty(GetPropertyRequest request)
        {
            return request.GetProperty();
        }

        public CreatePropertyResponse CreateProperty(CreatePropertyRequest request)
        {
            return request.CreateProperty();
        }

        public UpdatePropertyResponse UpdateProperty(UpdatePropertyRequest request)
        {
            return request.UpdateProperty();
        }

        public DeletePropertyResponse DeleteProperty(DeletePropertyRequest request)
        {
            return request.DeleteProperty();
        }

        public GetRentReportersPropertiesResponse GetAllRentReportersProperties(GetRentReportersPropertiesRequest request)
        {
            return request.GetRentReportersProperties();
        }
    }
}
