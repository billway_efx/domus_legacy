﻿using EfxFramework.PublicApi.Property;
using System.ServiceModel;

namespace EfxPublicApi.PropertyServices
{
    [ServiceContract(Namespace = "PublicApi")]
    public interface IPropertyService
    {
        [OperationContract]
        GetPropertyResponse GetProperty(GetPropertyRequest request);

        [OperationContract]
        CreatePropertyResponse CreateProperty(CreatePropertyRequest request);

        [OperationContract]
        UpdatePropertyResponse UpdateProperty(UpdatePropertyRequest request);

        [OperationContract]
        DeletePropertyResponse DeleteProperty(DeletePropertyRequest request);

        [OperationContract]
        GetRentReportersPropertiesResponse GetAllRentReportersProperties(GetRentReportersPropertiesRequest request);
    }
}
