﻿using System.ServiceModel;
using EfxFramework.PublicApi.Property;
using EfxFramework.Services;

namespace EfxPublicApi.PropertyServices
{
    [DefaultServiceBehavior]
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class PropertyStaffService : IPropertyStaffService
    {
        public GetPropertyStaffResponse GetPropertyStaff(GetPropertyStaffRequest request)
        {
            return request.GetPropertyStaff();
        }

        public CreatePropertyStaffResponse CreatePropertyStaff(CreatePropertyStaffRequest request)
        {
            return request.CreatePropertyStaff();
        }

        public UpdatePropertyStaffResponse UpdatePropertyStaff(UpdatePropertyStaffRequest request)
        {
            return request.UpdatePropertyStaff();
        }

        public DeletePropertyStaffResponse DeletePropertyStaff(DeletePropertyStaffRequest request)
        {
            return request.DeletePropertyStaff();
        }
    }
}
