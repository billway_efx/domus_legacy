﻿using EfxFramework.PublicApi.Property;
using System.ServiceModel;

namespace EfxPublicApi.PropertyServices
{
    [ServiceContract(Namespace = "PublicApi")]
    public interface IPropertyFeeService
    {
        [OperationContract]
        GetPropertyFeesResponse GetPropertyFees(GetPropertyFeesRequest request);

        [OperationContract]
        CreatePropertyFeeResponse CreatePropertyFee(CreatePropertyFeeRequest request);

        [OperationContract]
        DeletePropertyFeeResponse DeletePropertyFee(DeletePropertyFeeRequest request);

    }
}
