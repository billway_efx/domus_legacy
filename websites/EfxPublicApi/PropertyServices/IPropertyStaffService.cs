﻿using EfxFramework.PublicApi.Property;
using System.ServiceModel;

namespace EfxPublicApi.PropertyServices
{
    [ServiceContract(Namespace = "PublicApi")]
    public interface IPropertyStaffService
    {
        [OperationContract]
        GetPropertyStaffResponse GetPropertyStaff(GetPropertyStaffRequest request);

        [OperationContract]
        CreatePropertyStaffResponse CreatePropertyStaff(CreatePropertyStaffRequest request);

        [OperationContract]
        UpdatePropertyStaffResponse UpdatePropertyStaff(UpdatePropertyStaffRequest request);

        [OperationContract]
        DeletePropertyStaffResponse DeletePropertyStaff(DeletePropertyStaffRequest request);
    }
}
