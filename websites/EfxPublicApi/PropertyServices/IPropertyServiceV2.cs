﻿using EfxFramework.PublicApi.Property;
using System.ServiceModel;

namespace EfxPublicApi.PropertyServices
{
    [ServiceContract(Namespace = "PublicApi")]
    public interface IPropertyServiceV2
    {
        [OperationContract]
        GetPropertyResponseV2 GetProperty(GetPropertyRequestV2 request);

        [OperationContract]
        CreatePropertyResponseV2 CreateProperty(CreatePropertyRequestV2 request);

        [OperationContract]
        UpdatePropertyResponse UpdateProperty(UpdatePropertyRequestV2 request);

        [OperationContract]
        DeletePropertyResponse DeleteProperty(DeletePropertyRequest request);

        [OperationContract]
        GetRentReportersPropertiesResponse GetAllRentReportersProperties(GetRentReportersPropertiesRequest request);
    }
}
