﻿using System.ServiceModel;
using EfxFramework.PublicApi.Property;
using EfxFramework.Services;

namespace EfxPublicApi.PropertyServices
{
    [DefaultServiceBehavior]
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class PropertyFeeService : IPropertyFeeService
    {
        public GetPropertyFeesResponse GetPropertyFees(GetPropertyFeesRequest request)
        {
            return request.GetPropertyFees();
        }

        public CreatePropertyFeeResponse CreatePropertyFee(CreatePropertyFeeRequest request)
        {
            return request.CreatePropertyFee();
        }

        public DeletePropertyFeeResponse DeletePropertyFee(DeletePropertyFeeRequest request)
        {
            return request.DeletePropertyFee();
        }
    }
}
