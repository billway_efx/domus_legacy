﻿using System.ServiceModel;
using EfxFramework.PublicApi.Resident;
using EfxFramework.Services;

namespace EfxPublicApi.ResidentServices
{
    [DefaultServiceBehavior]
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class ResidentService : IResidentService
    {
        public GetResidentResponse GetResident(GetResidentRequest request)
        {
            return request.GetResident();
        }

        public CreateResidentResponse CreateResident(CreateResidentRequest request)
        {
            return request.CreateResident();
        }

        public UpdateResidentResponse UpdateResident(UpdateResidentRequest request)
        {
            return request.UpdateResident();
        }

        public DeleteResidentResponse DeleteResident(DeleteResidentRequest request)
        {
            return request.DeleteResident();
        }
    }
}
