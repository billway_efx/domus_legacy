﻿using EfxFramework.PublicApi.Resident;
using System.ServiceModel;

namespace EfxPublicApi.ResidentServices
{
    [ServiceContract(Namespace = "PublicApi")]
    public interface IResidentService
    {
        [OperationContract]
        GetResidentResponse GetResident(GetResidentRequest request);

        [OperationContract]
        CreateResidentResponse CreateResident(CreateResidentRequest request);

        [OperationContract]
        UpdateResidentResponse UpdateResident(UpdateResidentRequest request);

        [OperationContract]
        DeleteResidentResponse DeleteResident(DeleteResidentRequest request);
    }
}
