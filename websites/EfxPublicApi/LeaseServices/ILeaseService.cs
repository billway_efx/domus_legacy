﻿using EfxFramework.PublicApi.Lease;
using System.ServiceModel;

namespace EfxPublicApi.LeaseServices
{
    [ServiceContract(Namespace = "PublicApi")]
    public interface ILeaseService
    {
        [OperationContract]
        GetLeaseResponse GetLease(GetLeaseRequest request);

        [OperationContract]
        CreateLeaseResponse CreateLease(CreateLeaseRequest request);

        [OperationContract]
        UpdateLeaseResponse UpdateLease(UpdateLeaseRequest request);

        [OperationContract]
        DeleteLeaseResponse DeleteLease(DeleteLeaseRequest request);
    }
}
