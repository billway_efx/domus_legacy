﻿using EfxFramework.PublicApi.Lease;
using System.ServiceModel;

namespace EfxPublicApi.LeaseServices
{
    [ServiceContract(Namespace = "PublicApi")]
    public interface ILeaseFeeService
    {
        [OperationContract]
        GetLeaseFeesResponse GetLeaseFees(GetLeaseFeesRequest request);

        [OperationContract]
        AddPropertyFeeToLeaseResponse AddPropertyFeeToLease(AddPropertyFeeToLeaseRequest request);

        [OperationContract]
        RemovePropertyFeeFromLeaseResponse RemovePropertyFeeFromLease(RemovePropertyFeeFromLeaseRequest request);

    }
}
