﻿using System.ServiceModel;
using EfxFramework.PublicApi.Lease;
using EfxFramework.Services;

namespace EfxPublicApi.LeaseServices
{
    [DefaultServiceBehavior]
    [ServiceBehavior(AddressFilterMode=AddressFilterMode.Any)]
    public class LeaseService : ILeaseService
    {
        public GetLeaseResponse GetLease(GetLeaseRequest request)
        {
            return request.GetLease();
        }

        public CreateLeaseResponse CreateLease(CreateLeaseRequest request)
        {
            return request.CreateLease();
        }

        public UpdateLeaseResponse UpdateLease(UpdateLeaseRequest request)
        {
            return request.UpdateLease();
        }

        public DeleteLeaseResponse DeleteLease(DeleteLeaseRequest request)
        {
            return request.DeleteLease();
        }
    }
}
