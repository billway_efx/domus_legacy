﻿using System.ServiceModel;
using EfxFramework.PublicApi.Lease;
using EfxFramework.Services;

namespace EfxPublicApi.LeaseServices
{
    [DefaultServiceBehavior]
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class LeaseFeeService : ILeaseFeeService
    {
        public GetLeaseFeesResponse GetLeaseFees(GetLeaseFeesRequest request)
        {
            return request.GetLeaseFees();
        }

        public AddPropertyFeeToLeaseResponse AddPropertyFeeToLease(AddPropertyFeeToLeaseRequest request)
        {
            return request.AddPropertyFeeToLease();
        }

        public RemovePropertyFeeFromLeaseResponse RemovePropertyFeeFromLease(RemovePropertyFeeFromLeaseRequest request)
        {
            return request.RemovePropertyFeeFromLease();
        }
    }
}
