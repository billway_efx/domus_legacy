﻿using EfxFramework.PublicApi.Lease;
using System.ServiceModel;

namespace EfxPublicApi.LeaseServices
{
    [ServiceContract(Namespace = "PublicApi")]
    public interface ILeaseServiceV2
    {
        [OperationContract]
        GetLeaseResponseV2 GetLease(GetLeaseRequestV2 request);

        [OperationContract]
        CreateLeaseResponse CreateLease(CreateLeaseRequest request);

        [OperationContract]
        UpdateLeaseResponseV2 UpdateLease(UpdateLeaseRequestV2 request);

        [OperationContract]
        DeleteLeaseResponse DeleteLease(DeleteLeaseRequest request);
    }
}
