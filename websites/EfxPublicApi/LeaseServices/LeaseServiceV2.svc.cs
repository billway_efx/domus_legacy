﻿using System.ServiceModel;
using EfxFramework.PublicApi.Lease;
using EfxFramework.Services;

namespace EfxPublicApi.LeaseServices
{
    [DefaultServiceBehavior]
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class LeaseServiceV2 : ILeaseServiceV2
    {
        public GetLeaseResponseV2 GetLease(GetLeaseRequestV2 request)
        {
            return request.GetLease();
        }

        public CreateLeaseResponse CreateLease(CreateLeaseRequest request)
        {
            return request.CreateLease();
        }

        public UpdateLeaseResponseV2 UpdateLease(UpdateLeaseRequestV2 request)
        {
            return request.UpdateLease();
        }

        public DeleteLeaseResponse DeleteLease(DeleteLeaseRequest request)
        {
            return request.DeleteLease();
        }
    }
}
