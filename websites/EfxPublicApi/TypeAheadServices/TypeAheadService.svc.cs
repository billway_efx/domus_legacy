﻿using EfxFramework;
using EfxFramework.Services;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

namespace EfxPublicApi.TypeAheadServices
{
    [DefaultServiceBehavior]
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class TypeAheadService : ITypeAheadService
    {
        public List<string> GetPropertiesWithApplicationsBySearchString(string searchString)
        {
            return Property.GetPropertyListWithApplicationsBySearchString(searchString).Select(p => p.PropertyName).ToList();
        }

        public List<string> GetPropertiesBySearchString(string searchString)
        {
            return Property.GetPropertyListBySearchString(searchString).Select(p => p.PropertyName).ToList();
        }
    }
}
