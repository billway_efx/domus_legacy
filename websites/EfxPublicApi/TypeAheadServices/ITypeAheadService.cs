﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace EfxPublicApi.TypeAheadServices
{
    [ServiceContract]
    public interface ITypeAheadService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPropertiesWithApplicationsBySearchString/{searchString}", ResponseFormat = WebMessageFormat.Json)]
        List<string> GetPropertiesWithApplicationsBySearchString(string searchString);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPropertiesBySearchString/{searchString}", ResponseFormat = WebMessageFormat.Json)]
        List<string> GetPropertiesBySearchString(string searchString);
    }
}
