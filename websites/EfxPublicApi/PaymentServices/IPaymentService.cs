﻿using EfxFramework.PublicApi.Payment;
using System.ServiceModel;

namespace EfxPublicApi.PaymentServices
{
    [ServiceContract(Namespace = "PublicApi")]
    public interface IPaymentService
    {
        [OperationContract]
        OneTimePaymentResponse ProcessOneTimeCreditCardPayment(OneTimeCreditCardPaymentRequest request);

        [OperationContract]
        OneTimePaymentResponse ProcessOneTimeAchPayment(OneTimeAchPaymentRequest request);

        [OperationContract]
        RecurringPaymentResponse ProcessRecurringAchPayment(RecurringAchPaymentRequest request);

        [OperationContract]
        RecurringPaymentResponse ProcessRecurringCreditCardPayment(RecurringCreditCardPaymentRequest request);

        [OperationContract]
        RecurringPaymentResponse CancelRecurringCreditCardPayment(RecurringCreditCardPaymentRequest request);

        [OperationContract]
        RecurringPaymentResponse CancelRecurringAchPayment(RecurringAchPaymentRequest request);

        [OperationContract]
        ReversalResponse ProcessReversal(ReversalRequest request);
    }
}
