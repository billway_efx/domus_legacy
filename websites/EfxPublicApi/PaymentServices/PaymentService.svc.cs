﻿using System.ServiceModel;
using EfxFramework.PublicApi.Payment;
using EfxFramework.Services;

namespace EfxPublicApi.PaymentServices
{
    [DefaultServiceBehavior]
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class PaymentService : IPaymentService
    {
        public OneTimePaymentResponse ProcessOneTimeCreditCardPayment(OneTimeCreditCardPaymentRequest request)
        {
            return request.ProcessPayment();
        }

        public OneTimePaymentResponse ProcessOneTimeAchPayment(OneTimeAchPaymentRequest request)
        {
            return request.ProcessPayment();
        }

        public RecurringPaymentResponse ProcessRecurringAchPayment(RecurringAchPaymentRequest request)
        {
            return request.ProcessRecurringPayment();
        }

        public RecurringPaymentResponse ProcessRecurringCreditCardPayment(RecurringCreditCardPaymentRequest request)
        {
            return request.ProcessRecurringPayment();
        }

        public RecurringPaymentResponse CancelRecurringCreditCardPayment(RecurringCreditCardPaymentRequest request)
        {
            return request.CancelRecurringPayment();
        }

        public RecurringPaymentResponse CancelRecurringAchPayment(RecurringAchPaymentRequest request)
        {
            return request.CancelRecurringPayment();
        }

        public ReversalResponse ProcessReversal(ReversalRequest request)
        {
            return request.ProcessReversal();
        }
    }
}
