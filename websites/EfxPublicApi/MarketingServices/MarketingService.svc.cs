﻿using System.ServiceModel;
using EfxFramework.PublicApi.Marketing;
using EfxFramework.Services;

namespace EfxPublicApi.MarketingServices
{
    [DefaultServiceBehavior]
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class MarketingService : IMarketingService
    {
        public AddPropertyLeadResponse AddPropertyLead(AddPropertyLeadRequest request)
        {
            return request.AddPropertyLead();
        }
    }
}
