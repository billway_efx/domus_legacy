﻿using EfxFramework.PublicApi.Marketing;
using System.ServiceModel;

namespace EfxPublicApi.MarketingServices
{
    [ServiceContract(Namespace = "PublicApi")]
    public interface IMarketingService
    {
        [OperationContract]
        AddPropertyLeadResponse AddPropertyLead(AddPropertyLeadRequest request);
    }
}
