﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace EfxPublicApi.ConvenienceFeeServices
{
    [ServiceContract]
    public interface IConvenienceFeeService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetAverageConvenienceFeeForProperty/{programId}/{averageRent}", ResponseFormat = WebMessageFormat.Json)]
        decimal GetAverageConvenienceFeeForProperty(string programId, string averageRent);
    }
}
