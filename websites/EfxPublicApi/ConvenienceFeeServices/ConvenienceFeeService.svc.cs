﻿using EfxFramework;
using EfxFramework.Services;
using System.ServiceModel;

namespace EfxPublicApi.ConvenienceFeeServices
{
    [DefaultServiceBehavior]
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class ConvenienceFeeService : IConvenienceFeeService
    {
        public decimal GetAverageConvenienceFeeForProperty(string programId, string averageRent)
        {
            return Property.GetAverageConvenienceFeeForProperty(programId, averageRent);
        }
    }
}
