﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Microsoft.VisualBasic.FileIO;

namespace MRI_Import_File_Parser
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            long fileId = 0;

            if (args.Length > 0)
            {
                // If an argument is specified, it should be a FileId, so parse that single MRI file only
                try
                {
                    fileId = Convert.ToInt64(args[0]);
                    Console.WriteLine("Parsing FileId " + fileId);
                }
                catch {
                    Console.WriteLine(@"Error parsing command line argument. Expected an integer FileId value.");
                    return;
                }
            }
            else
            {
                // Otherwise, parse all MRI files that are uploaded and waiting for parsing
                Console.WriteLine(@"Parsing all uploaded files");
            }

            Console.WriteLine(@"");

            ParseUploadedFiles(fileId);
        }

        private static string ConnString => ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        private static void ParseUploadedFiles(long inputFileId)
        {
            var theSql = "";
            var con = new SqlConnection(ConnString);

            //Get list of uploaded files ...
            theSql = "select FileId, CompanyId, FileName from MRIImportFiles where Status = 1";   // Status = 1 = Uploaded
            if (inputFileId != 0)
            {
                theSql = "select FileId, CompanyId, FileName from MRIImportFiles where Status = 1 and FileId = " + inputFileId;
            }

            var com = new SqlCommand(theSql, con)
            {
                CommandType = CommandType.Text
            };
            con.Open();
            var dt = new DataTable();
            var sda = new SqlDataAdapter(com);
            sda.Fill(dt);

            //For each uploaded file in the list ...
            foreach (DataRow dr in dt.Rows)
            {
                var recordSequenceId = 0;

                var fileId = dr[0].ToString();
                var fileIdNumeric = (long)dr[0];
                var companyId = (int)dr[1];
                var fileName = dr[2].ToString();

                Console.WriteLine("Begin FileId " + fileId + " (" + fileName + ") ...");
 
                //Get the byte stream from the database
                com = new SqlCommand("select FileData from MRIImportFiles where FileId = " + fileId, con);
                SqlDataReader sdr = null;
                sdr = com.ExecuteReader();
                sdr.Read();
                var result = (byte[])sdr.GetValue(0);
                sdr.Close();

                //Read the file as a memory stream
                var ms = new MemoryStream(result);
                var tfp = new TextFieldParser(ms);
                tfp.SetDelimiters(",");

                while (!tfp.EndOfData)
                {
                    recordSequenceId++;
                    var TheFields = tfp.ReadFields();
                    ParseDetailRecordIntoDatabase(TheFields, fileIdNumeric, con, recordSequenceId);
                }

                //Update status of file to "Parsed"
                theSql = "UPDATE MRIImportFiles set Status = 2 where FileId = " + fileId;       // Status = 2 = Parsed
                com = new SqlCommand(theSql, con);
                com.ExecuteNonQuery();

                Console.WriteLine(recordSequenceId + " records parsed.");
                Console.WriteLine(@"");
            }
            con.Close();
        }

        private static string SafeStr(string input)
        {
            return input.Replace("'", "''").Replace(";", "");
        }

        private static void ParseDetailRecordIntoDatabase(IEnumerable<string> recordFields, long fileId, SqlConnection con, int recordSequenceId)
        {
            // The first record in every file should be the header record, but we'll insert it anyway.  There should be 23 fields in every record, but we're not doing any error checking in here.
            //If there are fewer fields, we'll insert nulls for the missing.  If there are more fields, we'll ignore them. If field lengths are longer than 50 characters, we'll truncate them.
            //All the error checking/validation will be done in the Stored Procedure

            var fieldNumber = 0;
            var rmPropId = "";
            var rmBldgId = "";
            var rmLease = "";
            var branchId = "";
            var companyId = "";
            var streetAdd = "";
            var unitId = "";
            var city = "";
            var state = "";
            var zip = "";
            var firstName = "";
            var lastName = "";
            var totalAmtDue = "";
            var chgCodeAmt = "";
            var chgCode = "";
            var chgCodeDesc = "";
            var noPayments = "";
            var noChecks = "";
            var residentId = "";
            var leaseStart = "";
            var leaseEnd = "";
            var phone = "";
            var email = "";

            foreach (var theField in recordFields)
            {
                fieldNumber++;
                switch (fieldNumber)
                {
                    case 1:
                        rmPropId = SafeStr(theField);
                        break;
                    case 2:
                        rmBldgId = SafeStr(theField);
                        break;
                    case 3:
                        rmLease = SafeStr(theField);
                        break;
                    case 4:
                        branchId = SafeStr(theField);
                        break;
                    case 5:
                        companyId = SafeStr(theField);
                        break;
                    case 6:
                        streetAdd = SafeStr(theField);
                        break;
                    case 7:
                        unitId = SafeStr(theField);
                        break;
                    case 8:
                        city = SafeStr(theField);
                        break;
                    case 9:
                        state = SafeStr(theField);
                        break;
                    case 10:
                        zip = SafeStr(theField);
                        break;
                    case 11:
                        firstName = SafeStr(theField);
                        break;
                    case 12:
                        lastName = SafeStr(theField);
                        break;
                    case 13:
                        totalAmtDue = SafeStr(theField);
                        break;
                    case 14:
                        chgCodeAmt = SafeStr(theField);
                        break;
                    case 15:
                        chgCode = SafeStr(theField);
                        break;
                    case 16:
                        chgCodeDesc = SafeStr(theField);
                        break;
                    case 17:
                        noPayments = SafeStr(theField);
                        break;
                    case 18:
                        noChecks = SafeStr(theField);
                        break;
                    case 19:
                        residentId = SafeStr(theField);
                        break;
                    case 20:
                        leaseStart = SafeStr(theField);
                        break;
                    case 21:
                        leaseEnd = SafeStr(theField);
                        break;
                    case 22:
                        phone = SafeStr(theField);
                        break;
                    case 23:
                        email = SafeStr(theField);
                        break;
                }
            }

            const string d1 = "', '";
            var theSql = 
                "INSERT INTO MRIImportFileDetail ( " +
                    "FileId, RecordSequenceId, RecordStatus, " +
                    "RmPropId, RmBldgId, RmLease, BranchId, " +
                    "CompanyId, StreetAdd, UnitId, City, " +
                    "State, Zip, FirstName, LastName, " +
                    "TotalAmtDue, ChgCodeAmt, ChgCode, ChgCodeDesc, " +
                    "NoPayments, NoChecks, ResidentId, LeaseStart, " +
                    "LeaseEnd, Phone, Email ) " +
                "VALUES (" +
                    fileId + ", " + recordSequenceId + ", 1" + ", '" +
                    rmPropId + d1 + rmBldgId + d1 + rmLease + d1 + branchId + d1 +
                    companyId + d1 + streetAdd + d1 + unitId + d1 + city + d1 +
                    state + d1 + zip + d1 + firstName + d1 + lastName + d1 +
                    totalAmtDue + d1 + chgCodeAmt + d1 + chgCode + d1 + chgCodeDesc + d1 +
                    noPayments + d1 + noChecks + d1 + residentId + d1 + leaseStart + d1 +
                    leaseEnd + d1 + phone + d1 + email + "')";
            var com = new SqlCommand(theSql, con)
            {
                CommandType = CommandType.Text
            };
            com.ExecuteNonQuery();
        }
    }
}
