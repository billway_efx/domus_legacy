﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TestReportProject.Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>



<%@ Register assembly="DropDownCheckBoxes" namespace="Saplin.Controls" tagprefix="cc1" %>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="StyleSheet1.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">

        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>

    <div>
 
        <asp:Label ID="Label1" runat="server" Text="Start"></asp:Label>     <asp:TextBox ID="TextBox1" runat="server">10/01/2014</asp:TextBox><br />
        <asp:Label ID="Label2" runat="server" Text="End"></asp:Label>     <asp:TextBox ID="TextBox2" runat="server">10/08/2014</asp:TextBox>
        <br />
        <br />
        <br />
        
            <cc1:DropDownCheckBoxes ID="DropDownCheckBoxes1" runat="server" UseButtons="true" AddJQueryReference="true" BackColor="#FF99FF">
                 <Texts SelectBoxCaption="Select Property" />
                <Style SelectBoxCssClass="DB_Font2" DropDownBoxCssClass="DDB_Font" />
                <Style2 SelectBoxCssClass="DB_Font2" DropDownBoxCssClass="DDB_Font" SelectBoxWidth="300" DropDownBoxBoxWidth="300" />
  

            </cc1:DropDownCheckBoxes>
        

        <br />
        <asp:Button ID="Button3" runat="server" Text="Get Property List" OnClick="Button3_Click" />
        <br />
        <asp:Label ID="PropertyIDListlbl" runat="server" Text=""></asp:Label>
        <br />
        <asp:Label ID="PropertyNameListlbl" runat="server" Text="PropertyIDListlbl"></asp:Label>
        <br />
        <br />
        <asp:Label ID="Label3" runat="server" Text="PropertyID"></asp:Label>     <asp:TextBox ID="TextBox3" runat="server">1146,1147,1148,1149,1150,1151,1152,1153,1154,1155,1156,1157,1158,1159,1160,1161,1162,1163,1164,1165,1166,1167,1168,1169,1170</asp:TextBox>
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="PDF" />
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Excel" />
        <br />
        <br />
    </div>
        <div>
<%--            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="584px">
               <LocalReport ReportEmbeddedResource="TestReportProject.Report1.rdlc" ReportPath="Report1.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DetailDeposit" />
                    </DataSources>
                </LocalReport>
            </rsweb:ReportViewer>--%>


 <%--           <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="TestReportProject.EFXDataSetTableAdapters.usp_Report_DetailedDepositReport_v3TableAdapter">
                <SelectParameters>
                    <asp:ControlParameter ControlID="TextBox1" Name="StartDate" PropertyName="Text" Type="DateTime" />
                    <asp:ControlParameter ControlID="TextBox2" Name="EndDate" PropertyName="Text" Type="DateTime" />
                    <asp:ControlParameter ControlID="TextBox3" Name="PropertyIdList" PropertyName="Text" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>--%>

            <asp:Button ID="Button6" runat="server" Text="Select" CausesValidation="False" UseSubmitBehavior="False" />
               <asp:LinkButton ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
               <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    
                    <asp:Panel ID="Panel1" runat="server" Width="334px">
                        <div>

                           

                                    <div style="background-color: #C0C0C0; width: 326px;">

                                   
                                    <asp:Button ID="Button4" runat="server" Text="Button" OnClick="Button4_Click" UseSubmitBehavior="False" />
                                    <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                                        <asp:ListItem Value="1">Test1</asp:ListItem>
                                        <asp:ListItem Value="2">Test2</asp:ListItem>
                                        <asp:ListItem Value="3">Test</asp:ListItem>
                                    </asp:CheckBoxList>
                                         </div>

                            <asp:Button ID="Button5" runat="server" Text="Close" OnClick="Button5_Click" />
                        </div>

                    </asp:Panel>
                     

                  </ContentTemplate>
            </asp:UpdatePanel>
                   <asp:PopupControlExtender ID="PopupControlExtender1" runat="server" TargetControlID="LinkButton1" PopupControlID="Panel1" OffsetX="10" Position="Bottom" ></asp:PopupControlExtender>
 
           


        </div>


    </form>
</body>
</html>
