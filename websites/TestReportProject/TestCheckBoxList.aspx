﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestCheckBoxList.aspx.cs" Inherits="TestReportProject.TestCheckBoxList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>

    <div>
        <asp:Button ID="Button1" runat="server" Text="Show List" />

        <asp:Panel ID="Panel1" runat="server" Width="360px" Height="500px">

            <asp:Panel ID="Panel2" runat="server" Width="350" Height="450" ScrollBars="Vertical">
                <asp:Button ID="Button3" runat="server" Text="Select All" OnClick="Button3_Click1"/>
            <asp:CheckBoxList ID="CheckBoxList1" runat="server" AutoPostBack="True" CssClass="form-control multiselect" OnSelectedIndexChanged="CheckBoxList1_SelectedIndexChanged" ></asp:CheckBoxList>

            </asp:Panel>
            
            
            
                
            <asp:Button ID="Button2" runat="server" Text="Close" />
        </asp:Panel>
        <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button1" PopupControlID="Panel1" CancelControlID="Button2"></asp:ModalPopupExtender>
        
    </div>
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>

                            </ContentTemplate>
        </asp:UpdatePanel>
        
    </form>
</body>
</html>
