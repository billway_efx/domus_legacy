﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;

namespace TestReportProject
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //DataSet ds = GetReportDataSet();
            //ReportDataSource RDS = new ReportDataSource("DetailDeposit", ds.Tables[0]);
            //ReportViewer1.LocalReport.DataSources.Clear();
            //ReportViewer1.LocalReport.DataSources.Add(RDS);
            //ReportViewer1.LocalReport.ReportEmbeddedResource = Server.MapPath("~/Report1.rdlc");
            //ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Report1.rdlc");
            //ReportViewer1.LocalReport.DataSources.Clear();
            //ReportViewer1.LocalReport.DataSources.Add(RDS);
            //ReportViewer1.LocalReport.Refresh();
            if (!Page.IsPostBack)
            {
                BindDropDownData();
               
            }

            
        }

        private DataSet GetReportDataSet()
        {
            string StartDate = "10/01/2014";
            string EndDate = "10/08/2014";
            string propertyID = "10,11,12,13,14,15,1016,1020,1021,1023,1051,1092,1122,1125,1126,1127,1129,1130,1131,1137,1139,1140,1141,1186,1187,1188,1189,1190,1195,1196,1197,1198,1199,1205,1206,1207,1208,1209,1210,1211,1212,1213,1214,1215,1216,1217,1233,1234,1235,1236,1240,1323,1390,1492,1516,0";

            SqlConnection con = new SqlConnection(ConnString);
            {
            SqlCommand com = new SqlCommand("usp_Report_DetailedDepositReport_v3", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@StartDate", SqlDbType.Date).Value = StartDate;
            com.Parameters.Add("@EndDate", SqlDbType.Date).Value = EndDate;
            com.Parameters.Add("@PropertyIdList", SqlDbType.NVarChar,-1).Value = propertyID;
            com.CommandTimeout = 240;

            con.Open();
            SqlDataAdapter SqlAdapt = new SqlDataAdapter(com);
            //SqlAdapt.SelectCommand = com;
            DataSet DS = new DataSet();
            SqlAdapt.Fill(DS, "DetailDeposit");
            con.Close();
            return DS;
            }

        }


        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            // Setup DataSet
            DataSet ds = GetReportDataSet();


            // Create Report DataSource
            ReportDataSource rds = new ReportDataSource("DetailDeposit", ds.Tables[0]);


            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = "application/pdf";
            string encoding = "UTF-8";
            string extension = string.Empty;


            // Setup the report viewer object and get the array of bytes
            ReportViewer viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.ReportPath = "~/Reports/Report1.rdlc";
            viewer.LocalReport.DataSources.Add(rds); // Add datasource here


            byte[] bytes = viewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            string fileName = "My Report Test";

            // Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName + "." + extension);
            Response.BinaryWrite(bytes); // create the file
            Response.Flush(); // send it to the client to download

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            // Setup DataSet
            DataSet ds = GetReportDataSet();

            // Create Report DataSource
            ReportDataSource rds = new ReportDataSource("DetailDeposit", ds.Tables[0]);

            // Variables
            Warning[] warnings;
            string[] streamIds;
            string mimeType = "application/excel";
            string encoding = "UTF-8";
            string extension = string.Empty;

            // Setup the report viewer object and get the array of bytes
            ReportViewer viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.ReportPath = "Reports/Report1.rdlc";
            viewer.LocalReport.DataSources.Add(rds); // Add datasource here

            byte[] bytes = viewer.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            string fileName = "My Report Test";

            // Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName + "." + extension);
            Response.BinaryWrite(bytes); // create the file
            Response.Flush(); // send it to the client to download
        }



        public void BindDropDownData()
        {
         
        DataSet ds = new DataSet();
        SqlConnection conn = new SqlConnection(ConnString);
        string cmdstr = "select PropertyID,PropertyName from Property where CompanyID = 7";

        SqlDataAdapter adp = new SqlDataAdapter(cmdstr, conn);

        adp.Fill(ds);

                    if (ds.Tables[0].Rows.Count > 0)

        {

            DropDownCheckBoxes1.DataSource = ds.Tables[0];

            DropDownCheckBoxes1.DataTextField = "PropertyName";

            DropDownCheckBoxes1.DataValueField = "PropertyID";

            DropDownCheckBoxes1.DataBind();


            CheckBoxList1.DataSource = ds.Tables[0];
            CheckBoxList1.DataTextField = "PropertyName";
            CheckBoxList1.DataValueField = "PropertyID";
            CheckBoxList1.DataBind();

        }


        }

        protected void Button3_Click(object sender, EventArgs e)
        {
        List<String> PropertyID_list = new List<string>();
        List<String> PropertyName_list = new List<string>();

        
        foreach (System.Web.UI.WebControls.ListItem item in DropDownCheckBoxes1.Items)
        {
            if (item.Selected)

            {

                PropertyID_list.Add(item.Value);
                PropertyName_list.Add(item.Text);

            }

            PropertyIDListlbl.Text = "Country ID: "+ String.Join(",", PropertyID_list.ToArray());
            PropertyNameListlbl.Text = "Country Name: "+ String.Join(",", PropertyName_list.ToArray());

        } 

        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in CheckBoxList1.Items)
            {
                item.Selected = true;
            }
            

        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            PopupControlExtender1.Cancel();
        }


    }
}