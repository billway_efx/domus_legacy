﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;

namespace TestReportProject
{
    public partial class TestCheckBoxList : System.Web.UI.Page
    {

        int PropertyCount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindDropDownData();

            }
        }

        public void BindDropDownData()
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConnString);
            string cmdstr = "select PropertyID,PropertyName from Property where CompanyID = 7";

            SqlDataAdapter adp = new SqlDataAdapter(cmdstr, conn);

            adp.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {

                CheckBoxList1.DataSource = ds.Tables[0];
                CheckBoxList1.DataTextField = "PropertyName";
                CheckBoxList1.DataValueField = "PropertyID";
                CheckBoxList1.DataBind();

            }
        }


        private DataSet GetReportDataSet()
        {
            string StartDate = "10/01/2014";
            string EndDate = "10/08/2014";
            string propertyID = "10,11,12,13,14,15,1016,1020,1021,1023,1051,1092,1122,1125,1126,1127,1129,1130,1131,1137,1139,1140,1141,1186,1187,1188,1189,1190,1195,1196,1197,1198,1199,1205,1206,1207,1208,1209,1210,1211,1212,1213,1214,1215,1216,1217,1233,1234,1235,1236,1240,1323,1390,1492,1516,0";

            SqlConnection con = new SqlConnection(ConnString);
            {
                SqlCommand com = new SqlCommand("usp_Report_DetailedDepositReport_v3", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("@StartDate", SqlDbType.Date).Value = StartDate;
                com.Parameters.Add("@EndDate", SqlDbType.Date).Value = EndDate;
                com.Parameters.Add("@PropertyIdList", SqlDbType.NVarChar, -1).Value = propertyID;
                com.CommandTimeout = 240;

                con.Open();
                SqlDataAdapter SqlAdapt = new SqlDataAdapter(com);
                //SqlAdapt.SelectCommand = com;
                DataSet DS = new DataSet();
                SqlAdapt.Fill(DS, "DetailDeposit");
                con.Close();
                return DS;
            }

        }


        private static String ConnString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

 

        protected void Button3_Click1(object sender, EventArgs e)
        {
            foreach (ListItem item in CheckBoxList1.Items)
            {
                item.Selected = true;
                PropertyCount += 1;
                Label1.Text = PropertyCount.ToString();
            }
            ModalPopupExtender1.Show();

            

        }

        protected void CheckBoxList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ModalPopupExtender1.Show();
            foreach (ListItem item in CheckBoxList1.Items)
            {

                if (item.Selected)
                {
                    PropertyCount += 1;

                }
                Label1.Text = PropertyCount.ToString();
            }
        }



    }
}