﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Mail;
using EfxFramework;
using EfxFramework.BulkMail;
using EfxFramework.Scheduler;

namespace EfxPaymentScheduler
{
    internal class Program
    {
        private static void SendExceptionEmail(string message)
        {
            BulkMail.SendMailMessage(
                    null,
                    "EFX",
                    false,
                    EfxSettings.SendMailUsername,
                    EfxSettings.SendMailPassword,
                    new MailAddress(EfxSettings.SupportEmail),
                    "Critical Payment Scheduler Exception",
                    message,
                    new List<MailAddress> { new MailAddress(EfxSettings.TechSupportEmail) });
        }

        private static void Main(string[] args)
        {
            var sendSmsReminders = bool.Parse(ConfigurationManager.AppSettings["SendSMSReminders"]);
            var processEcheckAutoPayments = bool.Parse(ConfigurationManager.AppSettings["ProcessEcheckAutoPayments"]);
            var processCreditCardAutoPayments = bool.Parse(ConfigurationManager.AppSettings["ProcessCreditCardAutoPayments"]);
            var getExpiredCreditCards = bool.Parse(ConfigurationManager.AppSettings["GetExpiredCreditCards"]);
            var updateAchPaymentStatus = bool.Parse(ConfigurationManager.AppSettings["UpdateAchPaymentStatus"]);
            var sendPaymentReminders = bool.Parse(ConfigurationManager.AppSettings["SendPaymentReminders"]);
            var processCreditCardPayments = bool.Parse(ConfigurationManager.AppSettings["ProcessGroupCCAch"]);
            var processRentByCashPayments = bool.Parse(ConfigurationManager.AppSettings["ProcessRentByCash"]);

            Console.WriteLine(@"EFX Payment Scheduler application started on " + DateTime.Now.ToShortDateString() + @" at " + DateTime.Now.ToShortTimeString() + ".\n");

            if (sendSmsReminders)
            {
                Console.WriteLine(@"Sending SMS Reminders ...");
                try
                {
                    SmsReminder.SendSmsReminders();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(@"SMS Reminder process Failed! Exception details: " + ex + "\n");
                    SendExceptionEmail("SMS Reminder process failed! Exception details: " + ex + "\n");
                }
                Console.WriteLine(@"... complete");
            }
            else
            {
                Console.WriteLine(@"SendSMSReminders is currently disabled in the configuration file.");
            }

            if (processEcheckAutoPayments)
            {
                Console.WriteLine(@"Processing Echeck Auto Payments ...");
                try
                {
                    EcheckAutoPayment.ProcessEcheckAutoPayments();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(@"Echeck Auto Payments process Failed! Exception details: " + ex + "\n");
                    SendExceptionEmail("Echeck Auto Payments process failed! Exception details: " + ex + "\n");
                }
                Console.WriteLine(@"... complete ");
            }
            else
            {
                Console.WriteLine(@"ProcessEcheckAutoPayments is currently disabled in the configuration file.");
            }

            if (processCreditCardAutoPayments)
            {
                Console.WriteLine(@"Processing Credit Card Auto Payments ...");
                try
                {
                    CreditCardAutoPayment.ProcessCreditCardAutoPayments();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(@"Credit Card Auto Payments process Failed! Exception details: " + ex + "\n");
                    SendExceptionEmail("Credit Card Auto Payments process failed! Exception details: " + ex + "\n");
                }
                Console.WriteLine(@"... complete ");
            }
            else
            {
                Console.WriteLine(@"ProcessCreditCardAutoPayments is currently disabled in the configuration file.");
            }

            if (getExpiredCreditCards)
            {
                Console.WriteLine(@"Retrieving Expired Credit Cards ...");
                try
                {
                    CreditCardExpirationReminder.GetExpiredCreditCards();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(@"Expired Credit Cards process Failed! Exception details: " + ex + "\n");
                    SendExceptionEmail("Expired Credit Cards process failed! Exception details: " + ex + "\n");
                }
                Console.WriteLine(@"... complete ");
            }
            else
            {
                Console.WriteLine(@"GetExpiredCreditCards is currently disabled in the configuration file.");
            }

            if (updateAchPaymentStatus)
            {
                Console.WriteLine(@"Updating ACH Payment statuses is permanently disabled in this application ...");
                try
                {
                }
                catch (Exception ex)
                {
                    Console.WriteLine(@"Updating ACH Payment Status process Failed! Exception details: " + ex + "\n");
                    SendExceptionEmail("Updating ACH Payment Status process failed! Exception details: " + ex + "\n");
                }
                Console.WriteLine(@"... complete ");
            }
            else
            {
                Console.WriteLine(@"UpdateAchPaymentStatus is currently disabled in the configuration file.");
            }

            if (sendPaymentReminders)
            {
                Console.WriteLine(@"Sending Payment reminders ...");
                try
                {
                    PaymentReminder.SendPaymentReminders();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(@"Payment reminder process Failed! Exception details: " + ex + "\n");
                    SendExceptionEmail("Payment reminder process failed! Exception details: " + ex + "\n");
                }
                Console.WriteLine(@"... complete ");
            }
            else
            {
                Console.WriteLine(@"SendPaymentReminders is currently disabled in the configuration file.");
            }
            
            //cakel:BUGID00213
            if (processCreditCardPayments)
            {
                Console.WriteLine(@"Batching Grouped Payments ...");
                try
                {
                    CreditCardAchSettlements.ProcessGroupCcAch();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Concat("Batching Grouped Payment process Failed! Exception details: ", ex,"\n"));
                    SendExceptionEmail("Batching Grouped Payment process failed! Exception details: " + ex + "\n");
                }
                Console.WriteLine(@"... complete
");
            }
            else
            {
                Console.WriteLine(@"ProcessGroupCCAch is currently disabled in the configuration file.");
            }

            //cakel: BUGID00266
            if (processRentByCashPayments)
            {
                Console.WriteLine(@"Batching Group Cash ACH Payments...");
                try
                {
                    CashAchSettlements.ProcessGroupCCAch();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Concat("Group Cash ACH Payments process Failed! Exception details: ",ex,"\n"));
                    SendExceptionEmail("Group Cash ACH Payments process failed! Exception details: " + ex + "\n");
                }
                Console.WriteLine(@"... complete
");
            }
            else
            {
                Console.WriteLine(@"ProcessRentByCash is currently disabled in the configuration file.");
            }

            //Salcedo - 3/17/15 - added date/time to log
            Console.WriteLine(string.Concat("\nEFX Payment Scheduler application completed on ", DateTime.Now.ToShortDateString(), " at ", DateTime.Now.ToShortTimeString(), ".\n"));
        }//End Main
    }
}
